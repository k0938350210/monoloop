# README #
## Monoloop Auth Requirements ##

* PHP 5.6.x

### System dependencies ###
* [Composer](https://getcomposer.org/)

* Initial setup
	git clone https://monoloop.git.beanstalkapp.com/monoloopui.git auth
	cd auth/src/api
	composer install