var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */
/*
elixir(function(mix) {
    mix.less('app.less');
});
*/

elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.sass('monoloop.scss' , 'public/css/monoloop.css');
    mix.sass('extjs3.scss' , 'public/css/extjs3.css');
});