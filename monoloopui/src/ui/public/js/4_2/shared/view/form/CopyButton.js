Ext.define('UI.shared.view.form.CopyButton', {
	alias: 'widget.copybutton',
	extend: 'Ext.button.Button', 
	xtype : 'copybutton',
	clip : null, 
	
	initComponent: function() { 
		this.callParent(arguments); 
	}, 
	
	afterRender: function() {
		var me = this ; 
        this.callParent(arguments);  
     
        me.clip = new ZeroClipboard(this.getEl().dom); 
        me.clip.on( "load", function(client) {
        	client.on( "mouseDown", Ext.Function.bind( me.clipMouseDown , me ));
        	client.on( "mouseUp", Ext.Function.bind( me.clipMouseUp , me ));
        	client.on( "mouseOver", Ext.Function.bind( me.clipMouseOver , me ));
        	client.on( "mouseOut", Ext.Function.bind( me.clipMouseOut , me ));
       	}) ;  
    },

	onDestroy: function() {
        this.clip.destroy();
        this.callParent(arguments);
    },
    
    // private
    clipMouseDown: function(client , args) { 
    	var me = this ; 
        me.clip.setText(me.getValue());
        Ext.getCmp(me.id).addClass('x-btn-default-small-pressed') ; 
    },

    // private
    clipMouseUp: function(client , args) {
    	var me = this ;  
        Ext.getCmp(me.id).removeCls('x-btn-default-small-pressed') ; 
    },

    // private
    clipMouseOver: function() {
        var me = this ;  
        Ext.getCmp(me.id).addClass('x-btn-default-small-over') ; 
    },

    // private
    clipMouseOut: function() {
        var me = this ;  
        Ext.getCmp(me.id).removeCls('x-btn-default-small-over') ; 
    },

    /**
     * Sets the value that should be used when the button is pressed
     * @param {String} value The value to set
     */
    setValue: function(value) {
        this.value = String(value);
    },

    /**
     * Get the value that will be used when the user clicks the button
     * @return {String} value The value used to copy to the clipboard
     */
    getValue: function() {
        return this.value;
    }


}) ; 