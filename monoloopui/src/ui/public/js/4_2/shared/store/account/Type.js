Ext.define('UI.shared.store.account.Type', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: 'name'} , {name: 'uid', type: 'int'}
    ] ,
    autoLoad : false , 
	proxy: {
        type: 'ajax',
        url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/getAccountType',
        actionMethods: {
            read: 'POST'
        } ,
        reader: {
            idProperty  : 'uid' , 
            type: 'json',
            root: 'topics', 
            totalProperty: 'totalCount'  
        } 
        
    }
});