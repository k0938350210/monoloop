Ext.define('UI.shared.store.account.Rights', {
    extend : 'Ext.data.TreeStore' ,
    fields: [ 
        {name: 'value', type: 'int'}  , 
        {name: 'text'}
    ] ,
    autoLoad : false , 
	proxy: {
        type: 'ajax',
        url: 'index.php?eID=monoloopaccounting&pid=480&cmd=rights/treeList',
        actionMethods: {
            read: 'POST'
        } ,
        reader: {
            type: 'json' 
        }  
        
    }
});