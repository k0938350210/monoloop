Ext.define('Tracker.view.tracker.Edit2SearchWindow', { 
    extend: 'Tracker.view.tracker.Edit2BaseWindow',
    alias: 'widget.trackeredit2search',
    
    searchTextbox_xpath : '' , 
    searchButton_xpath : '' , 
    
    activeFilterType : 0 , 
    activePostUserFunc : '' , 
    
    filterStore : null , 
    
    initComponent: function(){ 
        this.callParent(arguments); 
    } , 
    
    addBodyItems : function(){
        var me = this ; 
        
        me.filterStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [ 
                {name : '--- None --- ',   value: 0 } , 
                {name : 'Custom filter',   value: 1 } 
            ]
        }) ;
        
        var field = Ext.create('Ext.form.field.Display', {
            hideLabel : true , 
            value : 'Search textbox x-path:' , 
            style : { marginBottom: '0px' }
        }) ; 
        
        me.bodyPanel.add(field) ; 
        
        field = Ext.create('Ext.form.field.Text', { 
            hideLabel : true , 
            disabled : false ,  
            name: 'searchTextbox_xpath', 
            id : me.id + '-searchTextbox-xpath' ,    
            allowBlank: false  ,   
            value : me.searchTextbox_xpath 
            
        }) ; 
        
        me.bodyPanel.add(field) ; 
         
        var field = Ext.create('Ext.form.field.Display', {
            hideLabel : true , 
            value : 'Search button x-path:' , 
            style : { marginBottom: '0px' }
        }) ; 
        
        me.bodyPanel.add(field) ; 
        
        field = Ext.create('Ext.form.field.Text', { 
            hideLabel : true , 
            disabled : false ,  
            name: 'searchButton_xpath', 
            id : me.id + '-searchButton-xpath' ,    
            allowBlank: true  ,   
            value : me.searchButton_xpath  
        }) ; 
        
        me.bodyPanel.add(field) ; 
        
        me.bodyPanel.add([{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Filter Type:' , 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: true  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id+ '-ft'  ,  
                name : 'activeFilterType' , 
                displayField:   'name', 
                valueField : 'value' , 
                emptyText : 'Please select filter type' ,  
                store:   me.filterStore , 
                listeners : {
                    'afterrender' : function( combo , eObj){  
                        if( me.activeFilterType == '' || me.activeFilterType == 0 || me.activeFilterType == null || isNaN(me.activeFilterType) ){
                            combo.setValue(0) ; 
                        }else{ 
                            combo.setValue(parseInt(me.activeFilterType)) ;  
                        }  
                    },
                    'change' : function( combo , newVal , oldVal , eObj ){ 
                        if( newVal > 0){
                            Ext.getCmp(me.id + '-post-user-func').setVisible(true) ; 
                        }else{
                            Ext.getCmp(me.id + '-post-user-func').setVisible(false) ; 
                        }  
                    }
                }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-post-user-func' ,   
                name : 'activePostUserFunc' , 
                allowBlank : true , 
                listeners : {
                    'afterrender' : function(textArea , eObj){  
                        if( me.activePostUserFunc == undefined || me.activePostUserFunc == ''){
                            //textArea.setValue('return MONOloop.jq(selector).html() ; ') ;
                            textArea.setValue('return result ;') ;  
                        }else{
                            textArea.setValue(me.activePostUserFunc) ; 
                        }
                    }
                }
            }]) ; 
    }
}); 