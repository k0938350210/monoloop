Ext.define('Tracker.view.tracker.Edit2ValueWindow', { 
    extend: 'Tracker.view.tracker.Edit2BaseWindow',
    alias: 'widget.trackeredit2value',
    
    typeStore : null , 
    
    type_id2 : 0 , 
    typeData2 : 0 ,
    
    activeFilterType : 0 , 
    activePostUserFunc : '' , 
    
    activeFilterType2 : 0 , 
    activePostUserFunc2 : '' ,  
    
    filterStore : null , 
    
    initComponent: function(){ 
        this.callParent(arguments); 
    } , 
    
    addBodyItems : function(){
        var me = this ; 
        
        me.typeStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [
                {name : 'Metadata',   value: 1 },
                {name : 'Element',   value: 2 } ,  
                {name : 'Regular expression',   value: 3 } ,
                {name : 'Custom function',   value: 4 }
            ]
        }) ; 
        
        me.filterStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [ 
                {name : '--- None --- ',   value: 0 } , 
                {name : 'Price filter' , value: 2 } , 
                {name : 'Image filter' , value: 3 } , 
                {name : 'Custom filter',   value: 1 } 
            ]
        }) ;
        
        if( me.type_id == 0)
        	me.type_id = null ;
        
        var field = Ext.create('Ext.form.FieldSet', {
            title: 'Name',
            frame: true,
            bodyStyle: 'padding:5px 5px 0', 
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
            },
            items : [{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Tracker Type:' , 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id + '-type-id'  ,
                name: 'activeType', 
                displayField:   'name',
                valueField:     'value' ,
                invalidText : 'Tracker type must be selected' , 
                emptyText : 'Please select tracker type' , 
                store:  me.typeStore , 
                value : me.type_id 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Data:' , 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-typeData' , 
                name : 'activeTypeData' , 
                allowBlank : true ,
                value : me.typeData 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Filter Type:' , 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: true  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id+ '-ft'  ,  
                name : 'activeFilterType' , 
                displayField:   'name',
                valueField:     'value' ,
                emptyText : 'Please select filter type' , 
                store:   me.filterStore , 
                listeners : {
                    'afterrender' : function( combo , eObj){
                        if( me.activeFilterType == '' || me.activeFilterType == 0 || me.activeFilterType == null || isNaN(me.activeFilterType)){
                            combo.setValue(0) ; 
                        }else{
                            combo.setValue(me.activeFilterType) ; 
                        }  
                    },
                    'change' : function( combo , newVal , oldVal , eObj ){
                        if( newVal > 0){
                            Ext.getCmp(me.id + '-post-user-func').setVisible(true) ; 
                        }else{
                            Ext.getCmp(me.id + '-post-user-func').setVisible(false) ; 
                        }  
                    }
                }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-post-user-func' ,   
                name : 'activePostUserFunc' , 
                allowBlank : true , 
                listeners : {
                    'afterrender' : function(textArea , eObj){  
                        if( me.activePostUserFunc == undefined || me.activePostUserFunc == ''){
                            //textArea.setValue('return MONOloop.jq(selector).html() ; ') ;
                            textArea.setValue('') ;  
                        }else{
                            textArea.setValue(me.activePostUserFunc) ; 
                        }
                    }
                }
            }
            ]
        }) ; 
        
        me.bodyPanel.add(field) ; 
        
        me.addValueGroup() ; 
        
        me.addCategoryGroup() ; 
    } , 
    
    addCategoryGroup : function(){
    	
    } , 
    
    addValueGroup : function(){
        var me = this;  
        
        if ( me.type_id2 == 0)
        	me.type_id2 = null ;
        
        field = Ext.create('Ext.form.FieldSet', {
            title: 'Value',
            frame: true,
            bodyStyle: 'padding:5px 5px 0', 
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
            },
            items : [{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Tracker Type:' , 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id + '-type-id2'  ,
                name: 'activeType2', 
                displayField:   'name',
                valueField:     'value' ,
                invalidText : 'Tracker type must be selected' , 
                emptyText : 'Please select tracker type' , 
                store:  me.typeStore , 
                value : me.type_id2 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Data:' , 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-typeData2' , 
                name : 'activeTypeData2' , 
                allowBlank : true ,
                value : me.typeData2 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Filter Type:' , 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: true  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id+ '-ft2'  ,  
                name : 'activeFilterType2' , 
                displayField:   'name',
                valueField:     'value' ,
                emptyText : 'Please select filter type' , 
                store:   me.filterStore , 
                listeners : {
                    'afterrender' : function( combo , eObj){
                        if( me.activeFilterType2 == '' || me.activeFilterType2 == 0 || me.activeFilterType2 == null || isNaN(me.activeFilterType)){
                            combo.setValue(0) ; 
                        }else{
                            combo.setValue(me.activeFilterType2) ; 
                        }  
                    },
                    'change' : function( combo , newVal , oldVal , eObj ){
                        if( newVal > 0){
                            Ext.getCmp(me.id + '-post-user-func2').setVisible(true) ; 
                        }else{
                            Ext.getCmp(me.id + '-post-user-func2').setVisible(false) ; 
                        }  
                    }
                }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-post-user-func2' ,   
                name : 'activePostUserFunc2' , 
                allowBlank : true , 
                listeners : {
                    'afterrender' : function(textArea , eObj){  
                        if( me.activePostUserFunc2 == undefined || me.activePostUserFunc2 == ''){
                            //textArea.setValue('return MONOloop.jq(selector).html() ; ') ; 
                            textArea.setValue('') ; 
                        }else{
                            textArea.setValue(me.activePostUserFunc2) ; 
                        }
                    }
                }
            }
            ]
        }) ; 
        
        me.bodyPanel.add(field) ; 
    }
}) ; 