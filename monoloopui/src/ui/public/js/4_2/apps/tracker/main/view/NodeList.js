Ext.define('Tracker.view.NodeList', { 
    extend: 'Ext.panel.Panel',
    alias: 'widget.trackernodelist',
    
    listGrid : null , 
    listGridStore : null , 
    pagingToolbar : null ,
    
    activeUid : 0 , 
    
    treeID : '' , 
    searchID : '' , 
    
    initComponent: function(){ 
        var me = this ; 
        
        
        me.listGridStore = Ext.create('Ext.data.JsonStore', {
            remoteSort: true,
            pageSize : 15 ,   
            fields: [  
                        {name: 'uid', type: 'int'} , 
                        {name: 'type'}  , {name : 'typename'} , {name : 'name'} , 
                        {name: 'crdate' , mapping: 'crdate', type: 'date', dateFormat: 'timestamp'} , 
                        {name: 'nid', type: 'int'} , 
                        {name: 'tid', type: 'int'} , 
                        {name: 'folder_id' , type : 'int'}, 
                        {name: 'deletable' , type : 'int'}, 
                        {name: 'hidden' , type : 'int'}
                        ] , 
                        
            proxy: {
                type: 'ajax',
                //url : 'index.php?eID=monoloop_trackers&pid=480&cmd=node/list',
                url : 'index.php/trackers?eID=ml_tracker' , 
                reader: {
                    idProperty  : 'uid' , 
                    type: 'json',
                    root: 'topics', 
                    totalProperty: 'totalCount' 
                    
                } , 
                actionMethods: {
                    read: 'POST'
                }
            } , 
            sorters: [{
		        property: 'crdate',
		        direction: 'DESC'
		    }]
        }) ;
        
        me.pagingToolbar = Ext.create('Ext.PagingToolbar', {
            pageSize: 15,
            store: me.listGridStore,
            displayInfo: true,
            plugins: Ext.create('Monoloop.plugins.ProgressBarPager', {})
       }) ; 
 
        function renderAction(value, p, r){
            var placement = '<td><div onClick="Ext.getCmp(\''+me.id+'\').editTracker(\''+r.data['uid']+'\') ;" title="Edit" class="placement-placement"></div></td>' ;
            //var edit = '<td><div onClick="Ext.getCmp(\''+me.id+'\').propertyTracker(\''+r.data['uid']+'\') ;" title="Edit" class="placement-property"></div></td>' ;
            var edit2 = '<td><div onClick="Ext.getCmp(\''+me.id+'\').edit2Tracker(\''+r.data['uid']+'\') ;" title="Edit" class="placement-property"></div></td>' ;
            if(r.data['typename'] == 'asset'){
            	edit2 = '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>' ;  
            }
			var deleteData = '' ; 
            if( r.data['deletable'] == 1 )
                deleteData = '<td><div onClick="Ext.getCmp(\''+me.id+'\').deleteTracker(\''+r.data['uid']+'\')" title="Delete" class="placement-delete"></div></td>' ; 
            else 
                deleteData = '<td><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>' ; 
	    	var ret =  '<table width="100%" ><tr align="center"> '  
                         
                         + placement
                         + edit2 
                         //+ edit  
	    			     + deleteData
                         +
					   '</tr></table>'  ;
 
	 
			return ret ;   
         }
         
         function renderStatus(value, p, r){
            var hidden = '' ;
            if( r.data['hidden'] == 0 ){
				hidden = '<td><div onClick="Ext.getCmp(\''+me.id+'\').setHidden('+r.data['uid']+' , 1)"  class="placement-active"></div></td>' ; 
			}else{
				hidden = '<td><div onClick="Ext.getCmp(\''+me.id+'\').setHidden('+r.data['uid']+' , 0)"   class="placement-inactive" ></div></td>' ; 
			}
            	var ret =  '<table width="100%" ><tr align="center"> ' 
						 + hidden +
					   '</tr></table>'  ; 
			return ret ;
        }
        
        me.listGrid = Ext.create('Ext.grid.Panel', {
            store:  me.listGridStore, 
            width : '100%' ,  
            columns: [
              {header:'Name',width : 200 ,dataIndex:'name',sortable:true , flex : 1 }, 
              {header:'type',width : 200 , dataIndex : 'type' ,  sortable : true  } ,
              {header:'Date',dataIndex : 'crdate' ,  sortable : true ,  renderer: Ext.util.Format.dateRenderer('d M Y') } ,
              {header: 'Action' ,  dataIndex : 'uid' , sortable : false , renderer: renderAction } , 
              {header: 'Status',dataIndex : 'hidden' ,sortable : true , renderer: renderStatus  }
           ] ,
		   viewConfig: {
                plugins: {
                	ptype: 'gridviewdragdrop',
					ddGroup:	'selDD'
                } 
            } ,
           bbar: me.pagingToolbar
        }) ;
        
        Ext.apply(me ,{ 
            layout: 'fit',  
            items :  me.listGrid
        }) ; 
        
        me.mLoadData() ; 
        
        this.callParent(arguments); 
        
    } , 
    
    mLoadData : function(){   
        var me = this ;   
        var searchText = '' ; 
        if( Ext.getCmp(me.searchID) != undefined){
            searchText = Ext.getCmp(me.searchID).getValue() ; 
        }
        
        var treeSelectID = 0 ; 
        if( Ext.getCmp(me.treeID) != undefined){
            treeSelectID = Ext.getCmp(me.treeID).selectedID  ; 
        }
        
        me.listGridStore.proxy.extraParams = {'search' : searchText, 'folderID' : treeSelectID } ;   
        me.listGridStore.load( {limit: me.pagingToolbar.pageSize} );
    } , 
    
    
    editTracker : function(uid){
        var me = this ;  
        me.fireEvent('editNode' , {
        	uid : uid 
        }) ; 
    } , 
    
    edit2Tracker : function(uid){
    	var me = this ;  
        me.fireEvent('nodeEdit2' , {
        	uid : uid 
        }) ; 
    } , 
    
    deleteTracker : function(uid){ 
        var me = this ;   
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete this tracker?',function(btn){
            if (btn == 'yes'){
		        me.fireEvent('nodeDelete' , {
		        	uid : uid  
		        }) ; 
        	}
       	}) ;
        return ;  
    } , 
    
    setHidden : function( uid , hidden ){
        var me = this ;   
        me.fireEvent('nodeSetHidden' , {
        	uid : uid  , 
        	hidden : hidden 
        }) ; 
        return ;  
    } , 
    
    propertyTracker : function(uid){
        var me = this ;    
        var record = me.listGridStore.getById(uid) ;  
        if( record.get('tracker_product') == 1){
        	tracker_product.resetActiveData() ; 
            tracker_product.disableName = true ; 
            tracker_product.activeUID  = record.get('uid') ; 
            tracker_product.activeName =  record.get('name') ; 
            tracker_product.activeType =  record.get('type_id') ; 
            tracker_product.activeURL =  record.get('url') ; 
            tracker_product.activeTypeData =  record.get('typeData') ; 
            tracker_product.activeRegex =  record.get('regex') ;
            tracker_product.activeUrlOption =  record.get('urlOption') ;
            tracker_product.activeInc_www =  record.get('inc_www') ; 
            tracker_product.activeAsset = record.get('asset_id') ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                var jsonData = Ext.JSON.decode( record.get('addition_data') ) ;  
                tracker_product.activeType2 = parseInt(jsonData.activeType2) ;
                tracker_product.activeTypeData2 =  jsonData.activeTypeData2 ; 
                
                tracker_product.activeType3 = parseInt(jsonData.activeType3) ;
                tracker_product.activeTypeData3 =  jsonData.activeTypeData3 ; 
            } 
            tracker_product.showFlowStep4() ;
        }else if( record.get('tracker_type') <= 4 ){
            tracker_value.resetActiveData() ; 
            tracker_value.disableName = true ; 
            tracker_value.activeUID  = record.get('uid') ; 
            tracker_value.activeName =  record.get('name') ; 
            tracker_value.activeType =  record.get('type_id') ; 
            tracker_value.activeURL =  record.get('url') ; 
            tracker_value.activeTypeData =  record.get('typeData') ; 
            tracker_value.activeRegex =  record.get('regex') ;
            tracker_value.activeUrlOption =  record.get('urlOption') ;
            tracker_value.activeInc_www =  record.get('inc_www') ; 
            tracker_value.activeAsset = record.get('asset_id') ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                var jsonData = Ext.JSON.decode( record.get('addition_data') ) ;  
                tracker_value.activeType2 = parseInt(jsonData.activeType2) ;
                tracker_value.activeTypeData2 =  jsonData.activeTypeData2 ; 
            } 
            tracker_value.showFlowStep4() ; 
        }else if( record.get('tracker_type') == 5){
            tracker_value.resetActiveData() ; 
            tracker_value.activeUID  = record.get('uid') ; 
            tracker_value.activeName =  record.get('name') ; 
            tracker_value.activeType =  record.get('type_id') ; 
            tracker_value.activeURL =  record.get('url') ; 
            tracker_value.activeTypeData =  record.get('typeData') ; 
            tracker_value.activeRegex =  record.get('regex') ;
            tracker_value.activeUrlOption =  record.get('urlOption') ;
            tracker_value.activeInc_www =  record.get('inc_www') ; 
            tracker_value.activeAsset = record.get('asset_id') ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                var jsonData = Ext.JSON.decode( record.get('addition_data') ) ;  
                tracker_value.activeType2 = parseInt(jsonData.activeType2) ;
                tracker_value.activeTypeData2 =  jsonData.activeTypeData2 ; 
            } 
            tracker_value.showFlowStep4() ; 
        }
        else if( record.get('tracker_type') == 6 ){
            /*
            tracker_search.activeUID  = record.get('uid') ; 
            tracker_search.activeName =  record.get('name') ; 
            tracker_search.activeType =  record.get('type_id') ; 
            tracker_search.activeURL =  record.get('url') ; 
            tracker_search.activeTypeData =  record.get('typeData') ; 
            tracker_search.activeRegex =  record.get('regex') ;
            tracker_search.activeUrlOption =  record.get('urlOption') ;
            tracker_search.activeInc_www =  record.get('inc_www') ;
            tracker_search.showFlowStep4() ; 
            */
            tracker_search.activeUID  = record.get('uid') ; 
            tracker_search.activeName =  record.get('name') ; 
            tracker_search.activeURL =  record.get('url') ; 
            tracker_search.activeRegex =  record.get('regex') ;
            tracker_search.activeUrlOption =  record.get('urlOption') ;
            tracker_search.activeInc_www =  record.get('inc_www') ;
            tracker_search.tracker_type = record.get('tracker_type') ; 
            tracker_search.activeAsset = record.get('asset_id') ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                 var jsonData = Ext.JSON.decode( record.get('addition_data') ) ;     
                 tracker_search.activeAdditionDataObj =  jsonData ; 
            }
            tracker_basket.showFlowStep4() ; 
        }
        else if( record.get('tracker_type') == 7  ){
            tracker_basket.activeUID  = record.get('uid') ; 
            tracker_basket.activeName =  record.get('name') ; 
            tracker_basket.activeURL =  record.get('url') ; 
            tracker_basket.activeRegex =  record.get('regex') ;
            tracker_basket.activeUrlOption =  record.get('urlOption') ;
            tracker_basket.activeInc_www =  record.get('inc_www') ;
            tracker_basket.tracker_type = record.get('tracker_type') ; 
            tracker_basket.activeAsset = record.get('asset_id') ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                 var jsonData = Ext.JSON.decode( record.get('addition_data') ) ;     
                 tracker_basket.activeAdditionDataObj =  jsonData ; 
            }
            tracker_basket.showFlowStep4() ; 
        }else if( record.get('tracker_type') == 8 ){
            tracker_purchase.activeUID  = record.get('uid') ; 
            tracker_purchase.activeName =  record.get('name') ; 
            tracker_purchase.activeURL =  record.get('url') ; 
            tracker_purchase.activeRegex =  record.get('regex') ;
            tracker_purchase.activeUrlOption =  record.get('urlOption') ;
            tracker_purchase.activeInc_www =  record.get('inc_www') ;
            tracker_purchase.tracker_type = record.get('tracker_type') ; 
			tracker_purchase.activeAsset = record.get('asset_id') ;    
            tracker_purchase.createFlowStep4() ; 
        }else if( record.get('tracker_type') == 9 ){
            tracker_download.activeUID  = record.get('uid') ; 
            tracker_download.activeName =  record.get('name') ; 
            tracker_download.activeURL =  record.get('url') ; 
            tracker_download.activeRegex =  record.get('regex') ;
            tracker_download.activeUrlOption =  record.get('urlOption') ;
            tracker_download.activeInc_www =  record.get('inc_www') ;
            tracker_download.tracker_type = record.get('tracker_type') ;  
            tracker_download.activeTypeData =  record.get('typeData') ; 
            tracker_download.activeAsset = record.get('asset_id') ; 
            tracker_download.createFlowStep4() ; 
        }else{
            alert('Not support yet') ; 
        } 
    } , 
    
    edit2TrackerBak : function(uid){
        var me = this ;    
        var record = me.listGridStore.getById(uid) ;  
        if(record.get('tracker_type') == 1){
        	var nameDisabled = false  ;  
            
            var jsonData = {} ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                jsonData = Ext.JSON.decode( record.get('addition_data') ) ;   
            }else{
                jsonData.activeType2 = 0 ; 
                jsonData.activeType3 = 0 ; 
            } 
            
            Ext.create('Trackers.views.trackerEdit2Product', {
                id : 'product-edit2-value' ,  
                title : 'Product Tracker' , 
                width :  400  , 
                height : 500  , 
                // Model property ; 
                name : record.get('name') , 
                uid : record.get('uid') , 
                url : record.get('url') ,
                regex : record.get('regex')  ,
                urlOption : record.get('urlOption') , 
                inc_www : record.get('inc_www') , 
                type_id : record.get('type_id') , 
                typeData : record.get('typeData') , 
                type_id2 : parseInt(jsonData.activeType2)  , 
                typeData2 : jsonData.activeTypeData2 , 
                activeFilterType :  parseInt(jsonData.activeFilterType), 
                activePostUserFunc :  jsonData.activePostUserFunc, 
                activeFilterType2 :  parseInt(jsonData.activeFilterType2), 
                activePostUserFunc2 : jsonData.activePostUserFunc2, 
                type_id3 : parseInt(jsonData.activeType3)  , 
                typeData3 : jsonData.activeTypeData3 , 
                activeFilterType3 :  parseInt(jsonData.activeFilterType3), 
                activePostUserFunc3 : jsonData.activePostUserFunc3, 
                asset : record.get('asset_id') , 
                nameDisabled : nameDisabled , 
                saveDataURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveEdit2Value' , 
                saveCompleteProcess : function(){
                    Ext.getCmp('product-edit2-value').close() ;  
                    me.mLoadData() ; 
                }
            }).show() ;  
        }else if( record.get('tracker_type') <= 4 ){
            var nameDisabled = true  ; 
            if( record.get('tracker_type') == 0 )
                nameDisabled = false ; 
            
            var jsonData = {} ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                jsonData = Ext.JSON.decode( record.get('addition_data') ) ;   
            }else{
                jsonData.activeType2 = 0 ; 
            } 
            
            Ext.create('Trackers.views.trackerEdit2Value', {
                id : 'edit2-value' ,  
                title : 'Value Tracker' , 
                width :  400  , 
                height : 500  , 
                // Model property ; 
                name : record.get('name') , 
                uid : record.get('uid') , 
                url : record.get('url') ,
                regex : record.get('regex')  ,
                urlOption : record.get('urlOption') , 
                inc_www : record.get('inc_www') , 
                type_id : record.get('type_id') , 
                typeData : record.get('typeData') , 
                type_id2 : parseInt(jsonData.activeType2)  , 
                typeData2 : jsonData.activeTypeData2 , 
                activeFilterType :  parseInt(jsonData.activeFilterType), 
                activePostUserFunc :  jsonData.activePostUserFunc, 
                activeFilterType2 :  parseInt(jsonData.activeFilterType2), 
                activePostUserFunc2 : jsonData.activePostUserFunc2, 
                nameDisabled : nameDisabled , 
                asset : record.get('asset_id') , 
                saveDataURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveEdit2Value' , 
                saveCompleteProcess : function(){
                    Ext.getCmp('edit2-value').close() ;  
                    me.mLoadData() ; 
                }
            }).show() ;   
        }else if( record.get('tracker_type') == 6 ){ 
            var jsonData = {} ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                jsonData = Ext.JSON.decode( record.get('addition_data') ) ;   
            }else{
                jsonData.activeFilterType = 0 ; 
            } 
            
            Ext.create('Trackers.views.trackerEdit2Search', {
                id : 'edit2-search' ,  
                title : 'Search Tracker' , 
                width :  400  , 
                height : 450  , 
                // Model property ; 
                name : record.get('name') , 
                uid : record.get('uid') , 
                url : record.get('url') ,
                regex : record.get('regex')  ,
                urlOption : record.get('urlOption') , 
                inc_www : record.get('inc_www') , 
                type_id : record.get('type_id') , 
                typeData : record.get('typeData') , 
                searchTextbox_xpath : jsonData.searchTextbox_xpath   , 
                searchButton_xpath : jsonData.searchButton_xpath ,  
                activeFilterType : parseInt (  jsonData.activeFilterType ) , 
                activePostUserFunc : jsonData.activePostUserFunc ,  
                asset : record.get('asset_id') , 
                saveDataURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveEdit2Search' , 
                saveCompleteProcess : function(){
                    Ext.getCmp('edit2-search').close() ;  
                    me.mLoadData() ; 
                }
            }).show() ;   
        }else if( record.get('tracker_type') == 7 ){ 
            var jsonData = {} ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                jsonData = Ext.JSON.decode( record.get('addition_data') ) ;   
            } 
            
            Ext.create('Trackers.views.trackerEdit2Basket', {
                id : 'edit2-basket' ,  
                title : 'Basket Tracker' , 
                width :  400  , 
                height : 590  , 
                // Model property ; 
                name : record.get('name') , 
                uid : record.get('uid') , 
                url : record.get('url') ,
                regex : record.get('regex')  ,
                urlOption : record.get('urlOption') , 
                inc_www : record.get('inc_www') , 
                type_id : record.get('type_id') , 
                typeData : record.get('typeData') ,  
                dataObj : jsonData , 
                asset : record.get('asset_id') , 
                saveDataURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveEdit2Basket' , 
                saveCompleteProcess : function(){
                    Ext.getCmp('edit2-basket').close() ;  
                    me.mLoadData() ; 
                }
            }).show() ;   
        }else if( record.get('tracker_type') == 8 ){  
            Ext.create('Trackers.views.trackerEdit2Base', {
                id : 'edit2-purchase' ,  
                title : 'Purchase Tracker' , 
                width :  400  , 
                height : 390  , 
                // Model property ; 
                name : record.get('name') , 
                uid : record.get('uid') , 
                url : record.get('url') ,
                regex : record.get('regex')  ,
                urlOption : record.get('urlOption') , 
                inc_www : record.get('inc_www') , 
                type_id : record.get('type_id') , 
                typeData : record.get('typeData') ,  
                asset : record.get('asset_id') , 
                saveDataURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveEdit2Purchase', 
                saveCompleteProcess : function(){
                    Ext.getCmp('edit2-purchase').close() ;  
                    me.mLoadData() ; 
                }
            }).show() ;   
        }else if( record.get('tracker_type') == 9 ){  
            var jsonData = {} ; 
            if( Ext.String.trim(record.get('addition_data')) != ''  ){
                jsonData = Ext.JSON.decode( record.get('addition_data') ) ;   
            }else{
                jsonData.activeFilterType = 0 ; 
            }  
            Ext.create('Trackers.views.trackerEdit2Download', {
                id : 'edit2-download' ,  
                title : 'Download Tracker' , 
                width :  400  , 
                height : 390  , 
                // Model property ; 
                name : record.get('name') , 
                uid : record.get('uid') , 
                url : record.get('url') ,
                regex : record.get('regex')  ,
                urlOption : record.get('urlOption') , 
                inc_www : record.get('inc_www') , 
                type_id : record.get('type_id') , 
                typeData : record.get('typeData') ,  
                asset : record.get('asset_id') , 
                saveDataURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveEdit2Download', 
                activeFilterType : parseInt (  jsonData.activeFilterType ) , 
                activePostUserFunc : jsonData.activePostUserFunc ,  
                saveCompleteProcess : function(){
                    Ext.getCmp('edit2-download').close() ;  
                    me.mLoadData() ; 
                }
            }).show() ;   
        }
    }
}) ; 