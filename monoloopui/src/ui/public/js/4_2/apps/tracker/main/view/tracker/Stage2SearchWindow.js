Ext.define('Tracker.view.tracker.Stage2SearchWindow', { 
    extend: 'Tracker.view.tracker.Stage2Window',
    alias: 'widget.trackerflowstage2search', 
    
    myStep : 1  , 
    dataObj : {} ,
    
    step1Title : 'Step1' , 
    step2Title : 'Step2' , 
    step3Title : 'Step3' , 
    
    stepS1Text : 'Browse to the page that contains the search box and click Select Mode ' ,
    step2Text : 'Select searchbox' ,
    step3Text : 'Select searchbutton' ,
    
    initComponent : function(){
        this.callParent(arguments); 
    } , 
    
    initPosMessageListener : function(){
    	var data;
    	var me = this ; 
    	me.postMessageListener = function(event) {
			var data;
			data = JSON.parse(event.data);
			var iframe = document.getElementById("pw-browse") ;  
	        switch (data.t) {
	          case 'confirm':
	          	me.enable = true ;   
	            me.url = data.url ; 
	            Ext.getCmp('pw-url').setValue(data.displayURL) ; 
	            monoloopLog.log('Found invocation in customer site') ; 
                monoloopLog.log('Start loading bundle.js');
                
                
                var time =  new Date().getTime() ; 
	            iframe.contentWindow.postMessage(JSON.stringify({
			        t: "loadbundle" , 
                    domain : tracker_main.baseRelativeUrl , 
			        url : tracker_main.baseRelativeUrl + 'fileadmin/cbr/editor-bundle.js?' + time
				}), me.url);
			  break;  
			  case 'loadbundle-success' : 
			    monoloopLog.log('Init tracker search asset'); 
			  	iframe.contentWindow.postMessage(JSON.stringify({
			        t: "init-tracker-search" 
				}), me.url); 
			  break ; 
			  case 'tracker-search-loaded' : 
			    monoloopLog.log('Tracker search load complete');
			  
			  break ; 
			  case 'tracker-search-checkNext_callBack' : 
			  	me.checkNext_callBack(data.msg) ; 
			  break ; 
			  case 'tracker-search-getDataObj_callBack' : 
			  	me.getDataObj_callBack(data.dObj) ; 
			  break ;
	  		}
		};
		if (window.addEventListener) {
	        return addEventListener("message", me.postMessageListener , false);
		} else {
	        attachEvent("onmessage", me.postMessageListener ); 
		}
    } , 
    
    
    
    getListeners : function(){
        var me = this ; 
        var obj = { 
            afterrender : function(w){  
                if( me.activeURL != ''){
                    Ext.getCmp('pw-url').setValue(me.activeURL) ; 
                    me.pushURL() ;
               	    me.processURL() ; 
                }else{
                    Ext.getCmp('pw-url').setValue(me.firstDomain) ; 
                    me.pushURL() ;
               	    me.processURL() ; 
                }    
                 
                Ext.getCmp( me.id + '_local_Back').setVisible(false) ;  
            
                
                monoloop_base.showMessage( me.step1Title , me.stepS1Text ) ; 
            } ,  
            beforeclose : function(w,eOpts){
            	Ext.util.History.add('');
            	if (window.addEventListener) {
			        return removeEventListener("message", me.postMessageListener, false);
				} else {
			        detachEvent("onmessage", me.postMessageListener); 
				} 
				
            } , 
             close : function(w){  
                Ext.get('main-msg-div').dom.innerHTML = '' ;
             }
        } ; 
        return obj ; 
    } , 
    
    
    
    setDefaultData : function(){
        var me = this ; 
        /*
        if( document.getElementById("pw-browse").contentWindow.monoloop_select_view == undefined)
            return ; 
        
        document.getElementById("pw-browse").contentWindow.monoloop_select_view.setDefaultData(me.dataObj) ;
        */
        var iframe = document.getElementById("pw-browse") ;  
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "tracker-search-setDefaultData" , 
	        dataObj : me.dataObj  
		}), me.url) ; 
    } , 
    
    setTitle : function(){
        var me = this ; 
        Ext.get('main-msg-div').dom.innerHTML = '' ; 
        
        if( me.myStep == 1 ){
            me.bodyPanel.setTitle(me.step1Title + ' : ' + me.step1Text) ; 
            monoloop_base.showMessage( me.step1Title , me.step1Text ) ; 
        }else if( me.myStep == 2 ){
            me.bodyPanel.setTitle(me.step2Title + ' : ' + me.step2Text) ; 
            monoloop_base.showMessage( me.step2Title , me.step2Text ) ; 
        }else if( me.myStep == 3 ){
            me.bodyPanel.setTitle(me.step3Title + ' : ' + me.step3Text) ; 
            monoloop_base.showMessage( me.step3Title , me.step3Text ) ; 
        }
    } , 
    
    setMyStep : function(){
        var me = this ; 
        /*
        if( document.getElementById("pw-browse").contentWindow.monoloop_select_view == undefined)
            return ; 
        //console.debug(me.myStep) ;
        document.getElementById("pw-browse").contentWindow.monoloop_select_view.setMode(me.myStep) ;
        */
        var iframe = document.getElementById("pw-browse") ;  
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "tracker-search-setMode" , 
	        myStep : me.myStep  
		}), me.url) ; 
    } , 
    
    moveNext : function(){
        var me = this ; 
        var iframe = document.getElementById("pw-browse") ;  
        if( me.myStep == 2 ){
        	/*
            if( document.getElementById("pw-browse").contentWindow.monoloop_select_view != undefined){
                var msg = document.getElementById("pw-browse").contentWindow.monoloop_select_view.checkNext() ;  
            }
            */
            iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-search-checkNext"  
			}), me.url) ; 
			return ; 
        }
        
        if( me.myStep == 3){ 
        	/*
            if( document.getElementById("pw-browse").contentWindow.monoloop_select_view != undefined){  
                me.dataObj = document.getElementById("pw-browse").contentWindow.monoloop_select_view.getDataObj() ;  
            } 
    		*/
    		iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-search-getDataObj"  
			}), me.url) ;  
			return ; 
        }
        me.moveNext_2() ; 
    } , 
    
    moveNext_2 : function(){
    	var me = this ; 
    	me.myStep++  ; 
        if( me.myStep == 2){
            me.setDefaultData() ; 
        }
        if( me.myStep > 1){
            Ext.getCmp( me.id + '_local_Back').setVisible(true) ;    
        }
        me.setTitle() ; 
        me.setMyStep() ; 
    } , 
    
    checkNext_callBack : function(msg){
    	var me = this ; 
    	if( msg != '' ){
            Ext.MessageBox.alert('Fail',msg );
            return ;
        }  
        me.moveNext_2() ; 
    } , 
    
    getDataObj_callBack : function(dataObj){
    	var me = this ; 
    	me.dataObj = dataObj ; 
    	me.activeURL = Ext.getCmp('pw-url').getValue() ; 
        me.processNext() ; 
        Ext.get('main-msg-div').dom.innerHTML = '' ; 
    } , 
    
    moveBack : function(){
        var me = this ;  
        me.myStep-- ; 
        if( me.myStep == 1){
            Ext.getCmp( me.id + '_local_Back').setVisible(false) ;    
        }
        me.setTitle() ; 
        me.setMyStep()  ; 
    } , 
    
    initBodyPanel : function(){
        var me = this ; 
        var contentHTML = "<iframe id='pw-browse' name='pw-browse' src ='' width='100%' height='100%' onLoad=\"Ext.getCmp('"+me.id+"').browserSrcChange();\" frameborder='0'><p>Your browser does not support iframes.</p></iframe>"  ;
        me.bodyPanel = Ext.create('Ext.panel.Panel' , {
            title : me.step1Title + ' : ' + me.step1Text , 
            region: 'center', 
            split: false, 
			margins:'0',
            cmargins:'0',  
            border : false ,  
			bodyStyle : 'border:0;',
			html: contentHTML , 
            buttons: [{
                id : me.id + '_local_Back' , 
                cls : 'monoloop-submit-form' , 
                text: 'BACK' , 
                handler: function(){ 
                    me.moveBack() ; 
                }
            },{
                id : me.id + '_local_Next' , 
                cls : 'monoloop-submit-form' , 
                text: 'NEXT' , 
                handler: function(){ 
                    me.moveNext() ; 
                }
            } ]                   
        }) ; 
    }  , 
    
    processURL : function(){  
        var me = this ; 
        if( document.getElementById('pw-browse') != null){ 
            if( me.myStep == 1 ){
            	me.enable = false ; 
            	monoloopLog.log('Start render site');
                //document.getElementById('pw-browse').src = me.temPath + 'tracker_search.php?l='+me.urlencode(Ext.getCmp('pw-url').getValue()) ;  
                var renderURL = me.processPosmessageURL(Ext.getCmp('pw-url').getValue(),'tracker_search.php?l=')  ; 
            	monoloopLog.log('render : ' + renderURL);
                document.getElementById('pw-browse').src = renderURL ;  
            } 
        }  
    } , 
    
    browserSrcChange : function(){ 
    	var me = this ; 
    	monoloopLog.log('placement : browserSrcChange');
    	  
        
        if( document.URL.indexOf(document.getElementById('pw-browse').src) == -1 &&  me.enable == false ){
            // Change to proxy mode ;  
            // Wait 1 second to swith ; 
            setTimeout(function(){
            	if(me.enable == false){
            		monoloopLog.log('placement : Change to proxy mode');
            		
            		var url = document.getElementById('pw-browse').src ; 
            		if( document.getElementById('pw-browse') != null){  
		                document.getElementById('pw-browse').src = me.temPath + 'tracker_search.php?l='+me.urlencode(url) ; 
		                me.enable = true ;  
		                me.showProxyWarning() ; 
						me.addProxyList(url) ;  
		            }
		            monoloopLog.log('placement : add domain to proxy list - ' + url) ;  
            	}
			}, 1500);
            
            return ; 
         } 
    	
    	return ;  ; 
    	// New placement not use below ; 
        var me = this ; 
        if( document.getElementById("pw-browse").contentWindow.previewView == undefined)
            return ; 
        var url = document.getElementById("pw-browse").contentWindow.previewView.getUrl()  ; 
        Ext.getCmp('pw-url').setValue(url) ;   
    }  , 
    
    // Inteface . 
    processNext : function(){
        
    } 
}) ; 