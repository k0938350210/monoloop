Ext.define('Tracker.view.asset.ExampleWindow', { 
    extend: 'Tracker.view.tracker.Stage2Window', 
    result_id : '' , 
    
    initComponent: function(){ 
        var me = this ;  
        this.callParent(arguments); 
    } , 
    
    select_btn : function(){
    	var me = this ; 
   	 	me.activeURL =  Ext.getCmp('pw-url').getValue() ;
   	 	Ext.getCmp(me.result_id).setValue(me.activeURL) ; 
   	 	me.close() ; 
    } 
}) ; 