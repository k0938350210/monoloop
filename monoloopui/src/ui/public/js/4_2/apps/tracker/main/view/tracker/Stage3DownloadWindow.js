Ext.define('Tracker.view.tracker.Stage3DownloadWindow', { 
    extend: 'Tracker.view.tracker.Stage3BaseWindow',
    alias: 'widget.trackerflowstage3download',
    
    bodyPanel : null ,
    
    activeURL : null ,  
    activeTypeData : null ,
    activeFilterType : null , 
    activePostUserFunc : '' ,
    activeSaveFunction : 0 , 
    activeExample : '' , 
    
    filterStore : null , 
    
    initComponent: function(){ 
        var me = this ; 
        //console.debug(me.activeTypeData) ; 
        me.title = 'TRACKER : Download ' ;
        me.closable = true ; 
        me.width = 370 ; 
        me.height = 375 ;
        me.modal = true ;
        me.maximizable = false ; 
        me.resizable = false ; 
        me.plain = true ; 
        
        me.buttons = [
        {
            text: 'CANCEL' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){ 
               me.close() ; 
        	}       
	    } , 
        {
            text: 'NEXT' , 
            cls : 'monoloop-submit-form' , 
            handler: function(){
               if(Ext.getCmp( me.id + '-form').getForm().isValid()){   
                   me.activeFilterType = Ext.getCmp(me.id + '-ft').getValue() ; 
                   me.activePostUserFunc = Ext.getCmp(me.id + '-post-user-func').getValue() ;  
                   me.processNext() ; 
               }
            } 
        }
        ] ; 
        
        me.listeners = {
            'afterrender' : function(w){
                monoloop_base.showMessage('Step 3 :' , 'Check that the tracker captures the information you want and apply filters if appropriate ') ;
            }
        }
        
        me.initBodyPanel() ; 
        
        Ext.apply(me ,{   
            layout : 'fit' , 
            items :  [   me.bodyPanel ]
        }) ; 
        
        this.callParent(arguments); 
    } , 
    
    initBodyPanel : function(){
        var me = this ; 
        
        me.filterStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [ 
                {name : '--- None --- ',   value: 0 } , 
                {name : 'Custom filter',   value: 1 } 
            ]
        }) ; 
        
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
        width: '100%', 
        bodyStyle: 'padding : 10px ;' , 
        id: me.id + '-form', 
        border : false , 
        bodyBorder : false , 
        //bodyCls : 'cse-panel-body' , 
        autoScroll : true , 
        labelWidth: 100,
        defaults: {
            anchor: '95%',
            allowBlank: false,
            msgTarget: 'side'
        },
  
        items: [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<b>Example URL :</b>', 
                    style : { marginBottom: '0px' }
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    name : 'mc_regex[url]'  ,
                    id : 'mc-regex-url' ,  
                    allowBlank: false  , 
                    disabled  : false  , //--
                    value : me.activeURL   
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<hr><b>Filter Type :</b>', 
                    style : { marginBottom: '0px' }
                } ,{
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: true  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : me.id+ '-ft'  ,  
                    displayField:   'name',
                    valueField:     'value' ,
                    emptyText : 'Please select filter type' , 
                    store:   me.filterStore , 
                    listeners : {
                        'afterrender' : function( combo , eObj){
                            if( me.activeFilterType == '' || me.activeFilterType == 0 || me.activeFilterType == null){
                                combo.setValue(0) ; 
                            }else{
                                combo.setValue(me.activeFilterType) ; 
                            }  
                        },
                        'change' : function( combo , newVal , oldVal , eObj ){
                            if( newVal > 0){
                                Ext.getCmp(me.id + '-post-user-func').setVisible(true) ; 
                            }else{
                                Ext.getCmp(me.id + '-post-user-func').setVisible(false) ; 
                            } 
                            Ext.getCmp( me.id + '-ft-btn').fireEvent('click') ; 
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-post-user-func' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){  
                            if( me.activePostUserFunc == undefined || me.activePostUserFunc == ''){
                                textArea.setValue('return result ; ') ; 
                            }else{
                                textArea.setValue(me.activePostUserFunc) ; 
                            }
                        }
                    }
                },{
                    xtype : 'button' , 
                    hideLabel : true ,
                    cls : 'monoloop-submit-form' , 
                    id : me.id + '-ft-btn' , 
                    style : 'margin-right : 150px' , 
                    text : 'test ( Cick all link ) '  , 
                    listeners : {
                        'click' : function(button , e , eOpt){
                            if( Ext.getCmp(me.id + '-ft').getValue() == 0){ 
                                Ext.getCmp( me.id + '-post-user-ret').setValue( document.getElementById("pw-browse").contentWindow.category_preview.downloadMatch( me.activeTypeData ) ); 
                            }else{ 
                                Ext.getCmp( me.id + '-post-user-ret' ).setValue( document.getElementById("pw-browse").contentWindow.category_preview.downloadMatchPostFunc( me.activeTypeData  ,  Ext.getCmp(me.id + '-post-user-func').getValue() ) ) ;
                            }
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-post-user-ret'  , 
                    allowBlank : true 
                }
				]

	    });
    } , 
    
    // interface ; 
    
    processNext : function(){
        
    }
}) ; 