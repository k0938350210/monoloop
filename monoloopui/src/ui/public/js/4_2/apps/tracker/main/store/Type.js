Ext.define('Tracker.store.Type', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: 'tid', type: 'int'} , 
        {name: 'pid', type: 'int'} , 
        {name: 'vid', type: 'int'} ,
		{name: 'label'} , {name: 'name'} 
        
    ] , 
    remoteSort: true,
    pageSize : 15 ,   
    proxy: {
        type: 'ajax',
        url: 'index.php/term_data/1?eID=ml_vocab' , 
        reader: {
            idProperty  : 'uid' , 
            type: 'json',
            root: 'topics', 
            totalProperty: 'totalCount'  
        } , 
        actionMethods: {
            read: 'POST'
        }
    } , 
    sorters: [{
        property: 'name',
        direction: 'ASC'
    }] , 
    autoLoad : true 
    
});