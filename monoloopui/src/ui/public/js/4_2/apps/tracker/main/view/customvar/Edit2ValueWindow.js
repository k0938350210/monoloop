Ext.define('Tracker.view.customvar.Edit2ValueWindow', { 
    extend: 'Tracker.view.tracker.Edit2ValueWindow',
    alias: 'widget.customvardit2value',
    
    activeListFieldID : 0  , 
    saveFunctionStore : null , 
    listFieldStore : null , 
    activeSaveFunction : 0  ,  
    
    initComponent: function(){ 
        var me = this ; 
        me.listFieldStore = Ext.create('Ext.data.Store', {
        fields: [{name: 'label'},
	 			{name: 'value' , type: 'int'}] 
        }) ; 
        
        me.listeners = {
            'afterrender' : function(w){  
                w.setLoading('Loading data') ; 
            }
        } ; 
        
        this.callParent(arguments); 
         
        Ext.Ajax.request({
            url: 'index.php?eID=monoloop_trackers&pid=480&cmd=customvars/getField' , 
			// process the response object to add it to the TabPanel:
			success: function(xhr) {
			    var data = Ext.JSON.decode(xhr.responseText) ; 
                me.listFieldStore.loadData(data.data) ; 
 
                if( me.activeListFieldID != 0 ){
                    Ext.getCmp(me.id + 'listfield-id').setValue(me.activeListFieldID) ; 
                }
                me.setLoading(false)  ;  
			},
			failure: function() { 
                Ext.MessageBox.alert('Error', '#502' ) ; 
                me.setLoading(false)  ;
			}
		});
		
		me.saveFunctionStore = Ext.create('Ext.data.Store', {
            fields: ['name' , { name : 'value' , type : 'int'}],
            data : [
                {name : 'Override' , value : 1  },
                {name : 'Add'  , value : 2 } ,  
                {name : 'Substract' , value : 3 } ,
                {name : 'Push' , value : 4 }
            ]
        }) ; 
		
		me.bodyPanel.add({
			xtype : 'displayfield' , 
			value : 'Save method'	
		},{
            xtype: 'combo',
            mode: 'local',
            hideLabel : true , 
            allowBlank: false  ,
            triggerAction:  'all',
            editable:       false,  
            displayField:   'name',
            valueField:     'value' , 
            emptyText : 'Please select' , 
            id : me.id + '-savefunction' , 
            name : 'activeSaveFunction' , 
            store:  me.saveFunctionStore ,  
            allowBlank : false , 
            listeners : {
            	afterrender : function(cb){
            		if(me.activeSaveFunction == 0 || me.activeSaveFunction == null || isNaN(me.activeSaveFunction))
            			return ; 
           			cb.setValue(me.activeSaveFunction) ; 
            	}
            }
        });
    } , 
    
    addName : function(){
        var me = this ; 
        me.bodyPanel.add([{
            xtype : 'displayfield' , 
            hideLabel : true , 
            value : 'Customvar:', 
            style : { marginBottom: '0px' } 
        },{
            xtype:          'combo',
            mode:           'local',
            hideLabel : true , 
            allowBlank: false  , 
            triggerAction:  'all',
            queryMode: 'local',
            editable:       false,
            id : me.id + 'listfield-id'  ,
            name : 'activeListFieldID' , 
            displayField:   'label',
            valueField:     'value',  
            emptyText : 'Please select field' , 
            store:   me.listFieldStore
        } ]) ;  
    } , 
    
    addBodyItems : function(){
        var me = this ; 
        me.callParent(arguments); 
    } , 
    
    addValueGroup : function(){
        // Do nothing , we not need value group here . 
    }
    
    
}) ; 