Ext.define('Tracker.controller.TrackerDownload', {
	extend: 'Ext.app.Controller',
	
    activeUID : 0 , 
    activeName : '' , 
    activeType : 0 ,
    activeURL : '' , 
    activeTypeData : '' , 
    activeHidden : 0 , 
    activeRegex : '' , 
    activeUrlOption : 0 , 
    activeInc_www : 0 ,
    
    activeAdditionDataObj : {} ,
    
    init: function() {
		var me = this; 
		me.control({  	
			'#main-panel' : {
				'trackerDownloadEdit' : me.trackerDownloadEdit  
			}  
		});	
	} , 
	
	trackerDownloadEdit : function(config){
		var me = this ; 
		me.activeUID  = parseInt(config.data.uid)  ; 
        me.activeName =  config.data.name ; 
        me.activeURL = config.data.url ; 
        me.activeRegex = config.data.regex ;
        me.activeUrlOption = parseInt(config.data.urlOption) ;
        me.activeInc_www = parseInt(config.data.inc_www) ;
        me.tracker_type = parseInt(config.data.tracker_type) ;  
        me.activeTypeData = config.data.typeData ; 
        if( Ext.String.trim( config.data.addition_data ) != ''  ){
            var jsonData = Ext.JSON.decode( config.data.addition_data ) ;   
            me.activeFilterType = parseInt(jsonData.activeFilterType) ; 
            me.activePostUserFunc =  jsonData.activePostUserFunc ;  
        } 
        me.createFlow() ; 
	} , 
	
	resetActiveData : function(){ 
        var me = this ; 
        me.activeUID = 0 ; 
        me.activeName = '' ; 
        me.activeType = 0 ; 
        me.activeURL = '' ; 
        me.activeTypeData = '' ; 
        me.activeHidden = 0 ; 
        me.activeRegex = ''; 
        me.activeUrlOption = 0 ; 
        me.activeInc_www = 0 ; 
    } ,  
    
    createFlow : function(){
        var me = this ; 
        me.createFlowStep1() ; 
    } , 
    
    createFlowStep1 : function(){
        var me = this ;  
        Ext.create('Tracker.view.tracker.Stage1DownloadWindow', { 
            activeTypeData : me.activeTypeData , 
            id : 'flow1' , 
            processNext : function(){  
                me.activeTypeData = Ext.getCmp('flow1').activeTypeData ; 
                me.createFlowStep2() ; 
            } 
        }).show() ; 
    } , 
    
    createFlowStep2 : function(){
        var me = this ; 
        tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2Window', { 
            id : 'stage2-flow' , 
            activeType : 1 ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData , 
            temPath : tracker_main.temPath , 
            firstDomain : tracker_main.firstDomain  , 
            resetActiveData : function(){  
                //tracker_value.resetActiveData() ; 
            } , 
            select_process : function(){  
                me.activeURL = Ext.getCmp('stage2-flow').activeURL ;    
                me.createFlowStep3() ; 
            } 
        }).show() ; 
    } , 
    
    createFlowStep3 : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3DownloadWindow' , {
            id : 'stage3-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData , 
            activeFilterType : me.activeFilterType , 
            activePostUserFunc : me.activePostUserFunc , 
            processNext : function(){ 
                me.activeFilterType = Ext.getCmp('stage3-flow').activeFilterType ;  
                me.activePostUserFunc  = Ext.getCmp('stage3-flow').activePostUserFunc ;  
                Ext.getCmp('stage3-flow').close() ; 
                Ext.getCmp('stage2-flow').close();
                Ext.getCmp('flow1').close() ;
                me.createFlowStep4() ; 
            }
        }).show() ; 
    } , 
    
    createFlowStep4 : function(){
        var me = this ; 
        
        var postData = {} ; 
        postData['activeUID'] = me.activeUID ;   
        postData['activeTypeData'] = me.activeTypeData ; 
        postData['activeHidden'] = me.activeHidden ; 
        postData['activePostUserFunc'] = me.activePostUserFunc ; 
        postData['activeFilterType'] = me.activeFilterType ;  
        
        var postURL = 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveTrackersDownload' ; 
        
        Ext.create('Tracker.view.tracker.Stage4Window' , {
            id : 'stage4' , 
            activeRegex : me.activeRegex , 
            activeUrlOption : me.activeUrlOption , 
            activeInc_www : me.activeInc_www ,
            activeURL : me.activeURL , 
            postData : postData , 
            postURL : postURL , 
            resetActiveData : function(){ 
                me.resetActiveData() ; 
            } , 
            saveCompleteProcess : function(){
                Ext.getCmp('stage4').close() ;  
                Ext.getCmp('trackers-list').mLoadData() ;  
            }
        }).show() ;
    }
});