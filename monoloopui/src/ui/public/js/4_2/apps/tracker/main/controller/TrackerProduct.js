Ext.define('Tracker.controller.TrackerProduct', {
	extend: 'Ext.app.Controller',
	
	activeUID : 0 , 
    activeName : '' , 
    activeType : 0 ,
    activeURL : '' , 
    activeTypeData : '' , 
    activeHidden : 0 , 
    activeRegex : '' , 
    activeUrlOption : 0 , 
    activeInc_www : 0 ,
    activeType2 : 0 ,
    activeTypeData2 : '' , 
    activeFilterType : 0 , 
    activePostUserFunc : '' , 
    activeFilterType2 : 0 , 
    activePostUserFunc2 : '' , 
    activeAsset : 0 , 
    disableName  : false , 
    
    activeType3 : 0 ,
    activeTypeData3 : '' , 
    activeFilterType3 : 0 , 
    activePostUserFunc3 : '' , 
    
    init: function() {
		var me = this; 
		me.control({  	
			'#main-panel' : {
				'trackerProductEdit' : me.trackerProductEdit  
			}  
		});	
	} , 
    
    resetActiveData : function(){ 
		var me = this ; 
        me.activeUID = 0 ; 
        me.activeName = '' ; 
        me.activeType = 0 ; 
        me.activeURL = '' ; 
        me.activeTypeData = '' ; 
        me.activeHidden = 0 ; 
        me.activeRegex = ''; 
        me.activeUrlOption = 0 ; 
        me.activeInc_www = 0 ;
        me.activeType2 = 0 ;
        me.activeTypeData2 = '' ; 
        
        me.activeType3 = 0 ;
        me.activeTypeData3 = '' ; 
        me.activeAsset = 0 ; 
    } , 
    
    trackerProductEdit : function(config){
    	var me = this ; 
    	me.resetActiveData() ; 
        me.disableName = true ; 
        me.activeUID  = parseInt(config.data.uid)  ; 
        me.activeName = config.data.name  ; 
        me.activeType = parseInt(config.data.type_id) ; 
        me.activeURL = config.data.url ; 
        me.activeTypeData = config.data.typeData ; 
        me.activeRegex = config.data.regex ;
        me.activeUrlOption = parseInt(config.data.urlOption) ;
        me.activeInc_www = parseInt(config.data.inc_www) ; 
        me.activeAsset = parseInt(config.data.asset_id) ; 
        if( Ext.String.trim( config.data.addition_data ) != ''  ){
            var jsonData = Ext.JSON.decode( config.data.addition_data ) ;  
            me.activeFilterType = parseInt(jsonData.activeFilterType) ; 
            me.activePostUserFunc =  jsonData.activePostUserFunc ; 
            
            me.activeType2 = parseInt(jsonData.activeType2) ;
            me.activeTypeData2 =  jsonData.activeTypeData2 ;  
            me.activeFilterType2 = parseInt(jsonData.activeFilterType2) ; 
            me.activePostUserFunc2 =  jsonData.activePostUserFunc2 ; 
            
            me.activeType3 = parseInt(jsonData.activeType3) ;
            me.activeTypeData3 =  jsonData.activeTypeData3 ;  
            me.activeFilterType3 = parseInt(jsonData.activeFilterType3) ; 
            me.activePostUserFunc3 =  jsonData.activePostUserFunc3 ; 
        }
        me.showEditStep1() ; 
    } , 
    
    showEditStep1 : function(){ 
		var me = this ;  
        Ext.create('Tracker.view.tracker.Stage1ProductWindow', {
            id : 'product-stage1-flow' , 
            activeName : me.activeName , 
            activeType : me.activeType ,
            activeType2 : me.activeType2 ,
            activeType3 : me.activeType3 , 
            activeAsset : me.activeAsset  , 
            disableName : me.disableName ,
            process1 : function(){
                me.activeType = Ext.getCmp('product-stage1-flow').activeType  ; 
                me.showFlowStep2_1() ; 
            } , 
            process2 : function(){ 
                me.activeType2 = Ext.getCmp('product-stage1-flow').activeType2  ; 
                me.showFlowStep2_2() ; 
            } , 
            process3 : function(){ 
                me.activeType3 = Ext.getCmp('product-stage1-flow').activeType3  ; 
                me.showFlowStep2_3() ; 
            } , 
            nextProcess : function(){
                me.activeName = Ext.getCmp('product-stage1-flow').activeName ; 
                me.activeType = Ext.getCmp('product-stage1-flow').activeType ; 
                me.activeType2 = Ext.getCmp('product-stage1-flow').activeType2 ; 
                me.activeType3 = Ext.getCmp('product-stage1-flow').activeType3 ; 
                me.activeAsset = Ext.getCmp('product-stage1-flow').activeAsset ; 
                me.showFlowStep4() ; 
            }
        }).show() ; 
	}  , 
	
	// +++++++++++++++ Select name 
    
    showFlowStep2_1 : function(){ 
        var me = this ;  
        tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2Window', { 
            id : 'stage2_1-flow' , 
            activeType : me.activeType ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData , 
            activeName : me.activeName , 
            temPath : tracker_main.temPath , 
            firstDomain : tracker_main.firstDomain  , 
            resetActiveData : function(){  
                //tracker_value.resetActiveData() ; 
            } , 
            select_process : function(){ 
                me.activeURL = Ext.getCmp('stage2_1-flow').activeURL ; 
                me.activeTypeData = Ext.getCmp('stage2_1-flow').activeTypeData ;  
                if(  me.activeType == 1 ){
                    me.showFlowStep3_1_meta() ; 
                }else if( me.activeType == 2  ){
                    me.showFlowStep3_1_xpath() ; 
                }else if( me.activeType == 3  ){
                    me.showFlowStep3_1_regex() ; 
                }else if( me.activeType == 4  ){
                    me.showFlowStep3_1_customfunction() ; 
                }
            } 
        }).show() ;  
    } , 
    
    showFlowStep3_1_meta : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3MetaWindow', { 
            id : 'stage3_1-meta-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData , 
            activeFilterType : me.activeFilterType , 
            activePostUserFunc : me.activePostUserFunc , 
            processNext : function(){
                me.activeTypeData = Ext.getCmp('stage3_1-meta-flow').activeTypeData ; 
                me.activeFilterType = Ext.getCmp('stage3_1-meta-flow').activeFilterType ;  
                me.activePostUserFunc  = Ext.getCmp('stage3_1-meta-flow').activePostUserFunc ;  
                Ext.getCmp('stage3_1-meta-flow').close() ; 
                Ext.getCmp('stage2_1-flow').close() ; 
                
            }
        }).show() ;  
    } , 
    
    showFlowStep3_1_xpath : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3XpathWindow' , {
            id : 'stage3_1-xpath-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData , 
            activeFilterType : me.activeFilterType , 
            activePostUserFunc : me.activePostUserFunc , 
            processNext : function(){
                me.activeTypeData = Ext.getCmp('stage3_1-xpath-flow').activeTypeData ;  
                me.activeFilterType = Ext.getCmp('stage3_1-xpath-flow').activeFilterType ;  
                me.activePostUserFunc  = Ext.getCmp('stage3_1-xpath-flow').activePostUserFunc ;  
                Ext.getCmp('stage3_1-xpath-flow').close() ; 
                Ext.getCmp('stage2_1-flow').close() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep3_1_regex : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3RegexWindow' , {
            id : 'stage3_1-regex-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData ,  
            activeFilterType : me.activeFilterType , 
            activePostUserFunc : me.activePostUserFunc , 
            processNext : function(){
                me.activeTypeData = Ext.getCmp('stage3_1-regex-flow').activeTypeData ; 
                me.activeFilterType = Ext.getCmp('stage3_1-regex-flow').activeFilterType ;  
                me.activePostUserFunc  = Ext.getCmp('stage3_1-regex-flow').activePostUserFunc ;  
                Ext.getCmp('stage3_1-regex-flow').close() ; 
                Ext.getCmp('stage2_1-flow').close() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep3_1_customfunction : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3CfWindow' , {
            id : 'stage3_1-cf-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData ,  
            processNext : function(){
                me.activeTypeData = Ext.getCmp('stage3_1-cf-flow').activeTypeData ; 
                Ext.getCmp('stage3_1-cf-flow').close() ; 
                Ext.getCmp('stage2_1-flow').close() ; 
            }
        }).show() 
    } , 
    
    // +++++++++++++++ Select value 
    
    showFlowStep2_2 : function(){
        var me = this ; 
        tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2Window', { 
            id : 'stage2_2-flow' , 
            activeType : me.activeType2 ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData2 , 
            temPath : tracker_main.temPath , 
            firstDomain : tracker_main.firstDomain  , 
            resetActiveData : function(){  
                //tracker_value.resetActiveData() ; 
            } , 
            select_process : function(){ 
                me.activeURL = Ext.getCmp('stage2_2-flow').activeURL ; 
                me.activeTypeData2 = Ext.getCmp('stage2_2-flow').activeTypeData ;  
                if( me.activeType2 == 1 ){
                    me.showFlowStep3_2_meta() ; 
                }else if(me.activeType2 == 2  ){
                    me.showFlowStep3_2_xpath() ; 
                }else if(me.activeType2 == 3  ){
                    me.showFlowStep3_2_regex() ; 
                }else if(me.activeType2 == 4  ){
                    me.showFlowStep3_2_customfunction() ; 
                }
            } 
        }).show() ; 
    } , 
    
    showFlowStep3_2_meta : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3MetaWindow', { 
            id : 'stage3_2-meta-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData2 , 
            activeFilterType : me.activeFilterType2 , 
            activePostUserFunc : me.activePostUserFunc2 , 
            processNext : function(){
                me.activeTypeData2 = Ext.getCmp('stage3_2-meta-flow').activeTypeData ; 
                me.activeFilterType2 =   Ext.getCmp('stage3_2-meta-flow').activeFilterType  ; 
                me.activePostUserFunc2 =  Ext.getCmp('stage3_2-meta-flow').activePostUserFunc  ; 
                Ext.getCmp('stage3_2-meta-flow').close() ; 
                Ext.getCmp('stage2_2-flow').close() ; 
                
            }
        }).show() ;  
    } , 
    
    showFlowStep3_2_xpath : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3XpathWindow' , {
            id : 'stage3_2-xpath-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData2 , 
            activeFilterType : me.activeFilterType2 , 
            activePostUserFunc : me.activePostUserFunc2 , 
            processNext : function(){
                me.activeTypeData2 = Ext.getCmp('stage3_2-xpath-flow').activeTypeData ; 
                me.activeFilterType2 =   Ext.getCmp('stage3_2-xpath-flow').activeFilterType  ; 
                me.activePostUserFunc2 =  Ext.getCmp('stage3_2-xpath-flow').activePostUserFunc  ; 
                Ext.getCmp('stage3_2-xpath-flow').close() ; 
                Ext.getCmp('stage2_2-flow').close() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep3_2_regex : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3RegexWindow' , {
            id : 'stage3_2-regex-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData2 ,  
            activeFilterType : me.activeFilterType2 , 
            activePostUserFunc : me.activePostUserFunc2 , 
            processNext : function(){
                me.activeTypeData2 = Ext.getCmp('stage3_2-regex-flow').activeTypeData ; 
                me.activeFilterType2 =   Ext.getCmp('stage3_2-regex-flow').activeFilterType  ; 
                me.activePostUserFunc2 =  Ext.getCmp('stage3_2-regex-flow').activePostUserFunc  ; 
                Ext.getCmp('stage3_2-regex-flow').close() ; 
                Ext.getCmp('stage2_2-flow').close() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep3_2_customfunction : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3CfWindow' , {
            id : 'stage3_2-cf-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData2 ,  
            processNext : function(){
                me.activeTypeData2 = Ext.getCmp('stage3_2-cf-flow').activeTypeData ; 
                Ext.getCmp('stage3_2-cf-flow').close() ; 
                Ext.getCmp('stage2_2-flow').close() ; 
            }
        }).show() 
    } , 
     
    //-----------------------------
    
    // +++++++++++++++ Select category 
    
    showFlowStep2_3 : function(){
        var me = this ; 
        tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2Window', { 
            id : 'stage2_3-flow' , 
            activeType : me.activeType3 ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData3 , 
            temPath : tracker_main.temPath , 
            firstDomain : tracker_main.firstDomain  , 
            resetActiveData : function(){  
                //tracker_value.resetActiveData() ; 
            } , 
            select_process : function(){ 
                me.activeURL = Ext.getCmp('stage2_3-flow').activeURL ; 
                me.activeTypeData3 = Ext.getCmp('stage2_3-flow').activeTypeData ;  
                if( me.activeType3 == 1 ){
                    me.showFlowStep3_3_meta() ; 
                }else if(me.activeType3 == 2  ){
                    me.showFlowStep3_3_xpath() ; 
                }else if(me.activeType3 == 3  ){
                    me.showFlowStep3_3_regex() ; 
                }else if(me.activeType3 == 4  ){
                    me.showFlowStep3_3_customfunction() ; 
                }
            } 
        }).show() ; 
    } , 
    
    showFlowStep3_3_meta : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3MetaWindow', { 
            id : 'stage3_3-meta-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData3 , 
            activeFilterType : me.activeFilterType3 , 
            activePostUserFunc : me.activePostUserFunc3 , 
            processNext : function(){
                me.activeTypeData3 = Ext.getCmp('stage3_3-meta-flow').activeTypeData ; 
                me.activeFilterType3 =   Ext.getCmp('stage3_3-meta-flow').activeFilterType  ; 
                me.activePostUserFunc3 =  Ext.getCmp('stage3_3-meta-flow').activePostUserFunc  ; 
                Ext.getCmp('stage3_3-meta-flow').close() ; 
                Ext.getCmp('stage2_3-flow').close() ; 
                
            }
        }).show() ;  
    } , 
    
    showFlowStep3_3_xpath : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3XpathWindow' , {
            id : 'stage3_3-xpath-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData3 , 
            activeFilterType : me.activeFilterType3 , 
            activePostUserFunc : me.activePostUserFunc3 , 
            processNext : function(){
                me.activeTypeData3 = Ext.getCmp('stage3_3-xpath-flow').activeTypeData ; 
                me.activeFilterType3 =   Ext.getCmp('stage3_3-xpath-flow').activeFilterType  ; 
                me.activePostUserFunc3 =  Ext.getCmp('stage3_3-xpath-flow').activePostUserFunc  ; 
                Ext.getCmp('stage3_3-xpath-flow').close() ; 
                Ext.getCmp('stage2_3-flow').close() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep3_3_regex : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3RegexWindow' , {
            id : 'stage3_3-regex-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData3 ,  
            activeFilterType : me.activeFilterType3 , 
            activePostUserFunc : me.activePostUserFunc3 , 
            processNext : function(){
                me.activeTypeData3 = Ext.getCmp('stage3_3-regex-flow').activeTypeData ; 
                me.activeFilterType3 =   Ext.getCmp('stage3_3-regex-flow').activeFilterType  ; 
                me.activePostUserFunc3 =  Ext.getCmp('stage3_3-regex-flow').activePostUserFunc  ; 
                Ext.getCmp('stage3_3-regex-flow').close() ; 
                Ext.getCmp('stage2_3-flow').close() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep3_3_customfunction : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3CfWindow' , {
            id : 'stage3_3-cf-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData3 ,  
            processNext : function(){
                me.activeTypeData3 = Ext.getCmp('stage3_3-cf-flow').activeTypeData ; 
                Ext.getCmp('stage3_3-cf-flow').close() ; 
                Ext.getCmp('stage2_3-flow').close() ; 
            }
        }).show() 
    } , 
     
    //-----------------------------
    
    showFlowStep4 : function(){
        var me = this ; 
         
        
        var postData = {} ; 
        postData['activeUID'] = me.activeUID ; 
        postData['activeURL'] = me.activeURL ; 
        postData['activeName'] = me.activeName ; 
        postData['activeType'] = me.activeType ;
        postData['activeTypeData'] = me.activeTypeData ; 
        postData['activeHidden'] = me.activeHidden ; 
        postData['activeType2'] = me.activeType2 ;
        postData['activeTypeData2'] = me.activeTypeData2 ;
        postData['activePostUserFunc'] = me.activePostUserFunc ; 
        postData['activeFilterType'] = me.activeFilterType ; 
        postData['activePostUserFunc2'] = me.activePostUserFunc2 ; 
        postData['activeFilterType2'] = me.activeFilterType2 ; 
        postData['activeAsset'] = me.activeAsset ; 
        postData['folder_id'] =Ext.getCmp('trackers-tree').selectedID ; 
        
        postData['activeType3'] = me.activeType3 ;
        postData['activeTypeData3'] = me.activeTypeData3 ;
        postData['activePostUserFunc3'] = me.activePostUserFunc3 ; 
        postData['activeFilterType3'] = me.activeFilterType3 ;  
        
        var postURL = 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveTrackersValue' ; 
        
        Ext.create('Tracker.view.tracker.Stage4Window' , {
            id : 'stage4' , 
            activeRegex : me.activeRegex , 
            activeUrlOption : me.activeUrlOption , 
            activeInc_www : me.activeInc_www ,
            activeURL : me.activeURL , 
            postData : postData , 
            postURL : postURL , 
            resetActiveData : function(){ 
                me.resetActiveData() ; 
            } , 
            saveCompleteProcess : function(){
                Ext.getCmp('stage4').close() ; 
                if( Ext.getCmp('product-stage1-flow') != undefined){
                    Ext.getCmp('product-stage1-flow').close() ; 
                }
                Ext.getCmp('trackers-list').mLoadData() ;  
            }
        }).show() 
    }
}) ; 