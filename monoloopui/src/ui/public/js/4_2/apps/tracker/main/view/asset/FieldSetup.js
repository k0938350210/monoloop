Ext.define('Tracker.view.asset.FieldSetup', {
    extend : 'Ext.window.Window' ,
    alias: 'widget.assetfieldsetup',
    title : 'Asset tracker' , 
    modal : true , 
    resizable : false , 
    width : 500 ,
    height : 500 ,  
    layout : 'fit' , 
    
    bodyPanel : null , 
    typeStore : null ,  
    autoScroll : true , 
    
    originalURL : '' , 
    activeRegex : null , 
    activeUrlOption : 0 ,
    activeInc_www : null ,  
    activeInc_http_https : null ,
    activeURL : '' ,  
    
    name : null ,
    ttl : null , 
    
    uid : 0 , 
    
    initComponent: function(){ 
        var me = this ; 
        
        me.buttons = [{
            text : 'CANCEL' ,
            cls : 'monoloop-submit-form'  
        },{
            text : 'NEXT'  , 
            cls : 'monoloop-submit-form'  
        }] ;
        
        me.initBodyPanel() ; 
        
        me.items = [me.bodyPanel] ; 
        
        this.callParent(arguments); 
        
    } , 
    
    initBodyPanel : function(){
        var me = this ;
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
            width: '100%',  
            id: me.id + '-form',
            border : false , 
            bodyBorder : false ,  
            bodyStyle: 'padding : 15px ;' ,
            autoScroll : true ,
            defaults: {
                anchor: '100%',
                allowBlank: false ,
                msgTarget: 'side'
            },  
            items: [{
                xtype : 'displayfield' , 
                value : '<b>Name :</b>'
            },{
                xtype : 'textfield' , 
                id : me.id + '-name'  , 
                value : me.name 
            },{
                xtype : 'displayfield' , 
                value : '<b>Update tracked information after (in days)</b>'
            },{
                xtype : 'numberfield' , 
                id : me.id + '-ttl' ,
                value : me.ttl ,
            },{
            	xtype : 'displayfield' , 
            	value : '<b>Example page</b>'
            },{
            	xtype : 'fieldcontainer' , 
				layout: 'hbox',
            	items : [{
            		xtype : 'textfield' , 
					id : me.id + '-example-url' , 
					flex : 1 , 
					value : me.activeURL 
            	},{
            		xtype : 'button' , 
            		cls : 'monoloop-submit-form' , 
            		//iconCls : 'placement-icon' , 
            		text : '<img style="margin-top:-2px;" src="./typo3conf/ext/t3p_base/css/../image/placement.GIF" />'  ,
            		handler : function(){
            			Ext.create('Tracker.view.asset.ExampleWindow',{
            				result_id : me.id + '-example-url' , 
            				activeURL : Ext.getCmp( me.id + '-example-url').getValue() , 
            				temPath : assets.temPath  
            			}).show() ; 
            		}
            	}]
            },{
                xtype : 'fieldset' , 
                title: 'Fields', 
                id : me.id + '-root-fieldset' , 
                items : [{
                    xtype : 'displayfield' , 
                    value : 'No fields added' , 
                    id : me.id + '-nofield'
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    style : 'float : right ;' , 
                    text : 'Add new field'  
                }]
            }]
        }) ; 
        
        me.typeStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [
                {name : 'Metadata',   value: 1 },
                {name : 'Element',   value: 2 } ,  
                {name : 'Regular expression',   value: 3 } ,
                {name : 'Custom function',   value: 4 }
            ]
        }) ; 
    } , 
    
    addField : function( uid , fieldName , fieldType , typeId , typeData , filterType , postUserFunc , saveFunction , exampleData  ){ 
        var me = this ;
        if( Ext.getCmp(me.id + '-nofield') != undefined){
            Ext.getCmp(me.id + '-root-fieldset').remove(Ext.getCmp(me.id + '-nofield')) ; 
        }
        
        var insertIndex = Ext.getCmp(me.id + '-root-fieldset').items.length -1 ; 
        
        if(typeId != null ) 
            typeId = parseInt(typeId) ; 
        
        var newFieldSet = Ext.create('Ext.form.FieldSet',{  
            defaults: {
                anchor: '100%',
                allowBlank: false ,
                msgTarget: 'side'
            }, 
            uid : uid , 
            title : fieldName ,  
            fieldType : fieldType , 
            activeFilterType : filterType , 
            activeTypeData : typeData , 
            activePostUserFunc : postUserFunc , 
            activeSaveFunction : saveFunction ,  
            activeExample : exampleData , 
            items : [{
                xtype : 'displayfield' , 
                value : '<b>Tracker type :</b>' 
            },{
                xtype: 'combo',
                mode: 'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false, 
                name: 'activeType', 
                displayField:   'name',
                valueField:     'value' ,
                invalidText : 'Tracker type must be selected' , 
                emptyText : 'Please select tracker type' , 
                store:  me.typeStore  , 
                value : typeId 
            },{
                xtype : 'panel' , 
                height : 20 , 
                border : false , 
                layout : {
                    type : 'hbox' , 
                    align : 'stretch' 
                } , 
                items : [{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    text : 'Process' , 
                    flex : 1 , 
                    handler : function(){  
                        var cb = this.up('panel').previousNode() ; 
                        if( cb.isValid() ){
                        	me.activeURL = Ext.getCmp( me.id + '-example-url').getValue() ; 
           
                            assets.xpathObj =  Ext.create('Tracker.view.asset.BrowserWindow',{
                                parent_id : me.id , 
                                parent_field_id : this.up('fieldset').id ,
                                activeURL : me.activeURL , 
                                activeType : cb.getValue() , 
                                activeTypeData : this.up( 'fieldset' ).activeTypeData ,  
                                activeSaveFunction : this.up( 'fieldset' ).activeSaveFunction , 
                                activePostUserFunc : this.up( 'fieldset' ).activePostUserFunc , 
                                activeFilterType : this.up( 'fieldset' ).activeFilterType , 
                                fieldType : this.up('fieldset').fieldType ,
                                temPath : assets.temPath
                            }).show() ; 
                        }  
                    }
                },{
                    xtype : 'displayfield' , 
                    value : '&nbsp;'
                },{
                    xtype : 'button'  ,
                    cls : 'monoloop-submit-form' , 
                    text : 'Edit' , 
                    handler : function(){
                        var btnObj = this ; 
                        var parentFieldSet = this.up('fieldset') ; 
                        Ext.create('Tracker.view.asset.NewFieldWindow',{
                        	title : 'Edit Field' , 
                            parent_id : me.id , 
                            editfield_id : parentFieldSet.id , 
                            fieldName : parentFieldSet.title , 
                            fieldType : parentFieldSet.fieldType  
                        }).show() ; 
                    }
                },{
                    xtype : 'displayfield' , 
                    value : '&nbsp;'
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    text : 'Delete' , 
                    handler : function(){
                        var btnObj = this ; 
                        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete '+ this.up('fieldset').title + ' field ?',function(btn){
                            if(btn == 'yes'){
                            	btnObj.up('fieldset').flagDelete = 1 ; 
                            	btnObj.up('fieldset').setVisible(false) ; 
                            	/*
                                Ext.getCmp(me.id + '-root-fieldset').remove(btnObj.up('fieldset')) ;  
                                Ext.getCmp(me.id + '-root-fieldset').doLayout() ; 
                                */
                            }
                        }) ; 
                    }
                }
                ]
            }]
        }) ; 
        
         
        
        Ext.getCmp(me.id + '-root-fieldset').insert(insertIndex , newFieldSet) ; 
        me.bodyPanel.doLayout();
    }
}) ; 