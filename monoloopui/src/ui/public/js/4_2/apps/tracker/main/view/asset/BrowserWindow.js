Ext.define('Tracker.view.asset.BrowserWindow', { 
    extend: 'Tracker.view.tracker.Stage2Window', 
    
    parent_id : '' , 
    parent_field_id : '' , 
    
    fieldType : 0 , 
    
    initComponent: function(){ 
        var me = this ;  
        this.callParent(arguments); 
    } , 
    
    select_process : function(){
        monoloopLog.log('Asset select process');
        var me = this ;  
        if( me.activeFilterType == 0   && me.activeType == 2){
            if( me.fieldType == 'Image'){
                me.activeFilterType = 3 ; 
            }
        } 
    
        if( me.activeType == 1){ 
           
            Ext.create('Tracker.view.tracker.Stage3MetaWindow', {  
                activeURL : me.activeURL , 
                activeTypeData : me.activeTypeData , 
                activeFilterType : me.activeFilterType , 
                activePostUserFunc : me.activePostUserFunc , 
                activeSaveFunction : me.activeSaveFunction, 
                hideSaveFunction : true ,
                processNext : function(){ 
                     me.saveDataToField(this)  ;
                     this.close() ; 
                     me.close() ; 
                }
            }).show() ; 
            
        }else if( me.activeType == 2){ 
            Ext.create('Tracker.view.tracker.Stage3XpathWindow' , { 
                activeURL : me.activeURL , 
                activeTypeData : me.activeTypeData , 
                activeFilterType : me.activeFilterType , 
                activePostUserFunc : me.activePostUserFunc , 
                activeSaveFunction : me.activeSaveFunction, 
                hideSaveFunction : true , 
                processNext : function(){ 
                    me.saveDataToField(this)  ;
                    this.close() ; 
                    me.close() ; 
                }
            }).show() ; 
        }else if( me.activeType == 3){ 
            Ext.create('Tracker.view.tracker.Stage3RegexWindow' , { 
                activeURL : me.activeURL , 
                activeTypeData : me.activeTypeData , 
                activeFilterType : me.activeFilterType , 
                activePostUserFunc : me.activePostUserFunc , 
                activeSaveFunction : me.activeSaveFunction, 
                hideSaveFunction : true , 
                processNext : function(){ 
                    me.saveDataToField(this)  ;
                    this.close() ; 
                    me.close() ; 
                }
            }).show() ; 
        }else if( me.activeType == 4){ 
            Ext.create('Tracker.view.tracker.Stage3CfWindow' , { 
                activeURL : me.activeURL , 
                activeTypeData : me.activeTypeData , 
                activeFilterType : me.activeFilterType , 
                activePostUserFunc : me.activePostUserFunc , 
                activeSaveFunction : me.activeSaveFunction, 
                hideSaveFunction : true , 
                processNext : function(){ 
                    me.saveDataToField(this)  ;
                    this.close() ; 
                    me.close() ; 
                }
            }).show() ; 
        }
    }   , 
    
    setAfterLoadAsset : function(){ 
    	var me = this ; 
    	var iframe = document.getElementById("pw-browse") ;
	    iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-xpath-setMode"   , 
		        mode : 2 
		}), me.url); 
        
        if( me.activeType == 2){ 
            if( me.activeTypeData != '' ){ 
                //document.getElementById("pw-browse").contentWindow.monoloop_select_view.setPreXPath(me.activeTypeData) ; 
                
                iframe.contentWindow.postMessage(JSON.stringify({
				        t: "tracker-xpath-setPreXPath"   , 
				        activeTypeData : me.activeTypeData 
				}), me.url); 
            }
        } 
    } , 
    
    afterPreviewLoaded : function(){
    	var me = this ; 
    	me.setAfterLoadAsset() ; 
    } , 
    
    afterXpathLoaded : function(){
    	var me = this ; 
    	me.setAfterLoadAsset() ; 
    } , 
    
    getListeners : function(){
        var me = this ; 
        var obj = { 
            afterrender : function(w){  
                if( me.activeURL != ''){
                    Ext.getCmp('pw-url').setValue(me.activeURL) ; 
                    me.pushURL() ;
               	    me.processURL() ; 
                }else{
                    Ext.getCmp('pw-url').setValue(me.firstDomain) ; 
                    me.pushURL() ;
               	    me.processURL() ; 
                }    
                Ext.getCmp(me.id + '-back2browe-btn').setVisible(false) ; 
				Ext.getCmp(me.id + '-tosectmode-btn').setVisible(false) ;  
                
                
            } ,  
            beforeclose : function(w,eOpts){
            	if (window.addEventListener) {
			        return removeEventListener("message", me.postMessageListener, false);
				} else {
			        detachEvent("onmessage", me.postMessageListener); 
				} 
            } , 
             close : function(w){
                if(Ext.get('main-msg-div') != undefined)
                	Ext.get('main-msg-div').dom.innerHTML = '' ;
                me.resetActiveData() ;   
             }
        } ; 
        return obj ; 
    } , 
    
    browserSrcChange : function(){
        var me = this ;  
        
        monoloopLog.log('placement : browserSrcChange');
        
        if( document.URL.indexOf(document.getElementById('pw-browse').src) == -1 &&  me.enable == false ){
            // Change to proxy mode ;  
            // Wait 1 second to swith ; 
            setTimeout(function(){
            	if(me.enable == false){
            		monoloopLog.log('placement : Change to proxy mode');
            		
            		var url = document.getElementById('pw-browse').src ; 
            		if( document.getElementById('pw-browse') != null){  
		                if( me.activeType != 2){ 
			                document.getElementById('pw-browse').src = me.temPath + 'category_preview.php?l='+me.urlencode(url) ; 
			            }else{
			                document.getElementById('pw-browse').src = me.temPath + 'category_xpath.php?l='+me.urlencode(url) ; 
			            }
		                me.enable = true ;  
		                me.showProxyWarning() ; 
						me.addProxyList(url) ;  
		            }
		            monoloopLog.log('placement : add domain to proxy list - ' + url) ;  
            	}
			}, 1500);
            
            return ; 
         } 
         
        return ; 
        Ext.getCmp(me.id +  '-select-btn').setDisabled(false) ;
        if( document.getElementById("pw-browse").contentWindow.previewView == undefined)
            return ; 
        var url = document.getElementById("pw-browse").contentWindow.previewView.getUrl()  ;
 
        Ext.getCmp('pw-url').setValue(url) ; 
        
        
        
        
    } ,
    
    saveDataToField : function(obj){
        var me = this ;  
        Ext.getCmp(me.parent_field_id).activeTypeData = obj.activeTypeData ;
        Ext.getCmp(me.parent_field_id).activeFilterType = obj.activeFilterType ;
        Ext.getCmp(me.parent_field_id).activePostUserFunc = obj.activePostUserFunc ; 
        Ext.getCmp(me.parent_field_id).activeSaveFunction = obj.activeSaveFunction ;
        Ext.getCmp(me.parent_field_id).activeExample = obj.activeExample ; 
        monoloopLog.log('field data : ') ; 
        monoloopLog.log(obj.activeExample) ; 
        Ext.getCmp(me.parent_id).activeURL = obj.activeURL ;
    }
});