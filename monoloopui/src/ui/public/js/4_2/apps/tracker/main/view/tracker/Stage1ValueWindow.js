Ext.define('Tracker.view.tracker.Stage1ValueWindow', { 
    extend: 'Ext.window.Window',
    alias: 'widget.trackerstage1valuewindow',
    
    bodyPanel : null  , 
    
    activeName : '' , 
    activeType : 0 ,
    activeType2 : 0 , 
    activeAsset : 0 , 
    
    disableName : false ,
    
    initComponent: function(){ 
        var me = this ; 
        me.title = 'TRACKER' ;
        me.closable = true ; 
        me.width = 370 ; 
        me.height = 455 ; 
        me.layout = 'border' ;  
        me.modal = true ;
        me.maximizable = false ; 
        me.resizable = false ; 
        me.plain = true ;
        
        me.initPanel() ;  
        
        me.buttons = [
            {
	            text: 'CANCEL' , 
	            cls : 'monoloop-submit-form'        
		    }, 
            {
	            text: 'NEXT' ,  
	            cls : 'monoloop-submit-form' , 
	            handler: function(){
	                 if(me.bodyPanel.getForm().isValid()){  
	                     me.activeName = Ext.getCmp('mac-name').getValue() ;  
                         me.nextProcess() ; 
	                } 
	            }

	        }
         ] ;
         
        me.listeners = {
            'afterrender' : function(w){ 
                if( me.activeName != '' ){
                    Ext.getCmp('mac-name').setValue(me.activeName) ; 
                } 
                if( me.activeType != 0 ){
                    Ext.getCmp('mac-type').setValue(me.activeType) ; 
                }  
                if( me.activeType2 != 0 ){
                    Ext.getCmp('mac-type2').setValue(me.activeType2) ; 
                } 
                if( me.activeAsset != 0 ){ 
                	Ext.getCmp('mac-asset').store.load(function(){
             			Ext.getCmp('mac-asset').setValue(me.activeAsset) ;
                	}) ; 
               	}
            } , 
            'beforeclose' : function(w){
            	Ext.util.History.add('');
            }
         }  ; 
         
        Ext.apply(me ,{   
            items :  [ me.bodyPanel  ]
        }) ; 
        
        this.callParent(arguments); 
    } , 
    
    initPanel : function(){
        var me = this ; 
         me.bodyPanel = Ext.create('Ext.form.Panel' , {
            width: '100%', 
            height : 320 , 
            id: me.id + 'create_new_content', 
            border : false , 
            bodyBorder : false , 
            region : 'center' , 
            bodyStyle: 'padding : 10px ;' , 
            //bodyCls : 'cse-panel-body' , 
            //labelWidth: 100,
            defaults: {
                anchor: '95%',
                allowBlank: false ,
                msgTarget: 'side'
            },
            items: [ {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<b>Name :</b>'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    name : 'mac[name]'  ,
                    id : 'mac-name' ,  
                    disabled  : me.disableName , 
                    allowBlank: false  
                }, 
                
                {
                    xtype: 'fieldset',  
                    title : 'Name' , 
                    defaults: { 
                        anchor: '100%' 
                    },
                    items : [
                        {
                            xtype : 'displayfield' , 
                            hideLabel : true , 
                            value : '<b>Tracker type :</b>'
                        },{
                            xtype:          'combo',
                            mode:           'local',
                            queryMode : 'local' , 
                            hideLabel : true , 
                            allowBlank: false  ,
                            triggerAction:  'all',
                            editable:       false,
                            id : 'mac-type'  ,
                            hiddenName: 'mac[type]', 
                            displayField:   'name',
                            valueField:     'value' ,
                            invalidText : 'Tracker type must be selected' , 
                            emptyText : 'Please select tracker type' , 
                            store: 
                                Ext.create('Ext.data.Store', {
                                fields: ['name', {name: 'value', type: 'int'}],
                                data : [
                                    {name : 'Metadata',   value: 1 },
                                    {name : 'Element',   value: 2 } ,  
                                    {name : 'Regular expression',   value: 3 } ,
                                    {name : 'Custom function',   value: 4 }
                                ]
                            })
                        }, 
                        {
                            xtype : 'button' , 
                            cls : 'monoloop-submit-form' , 
                            hideLabel : true , 
                            text : 'Process' ,
                            handler : function(btn){
                                 if( Ext.getCmp('mac-type').isValid()){
                                    me.activeType = Ext.getCmp('mac-type').getValue() ; 
                                    me.process1(btn) ; 
                                 }
                            }
                        }
                   ]
               } , 
                
                {
                    xtype: 'fieldset',  
                    title : 'Value' , 
                    defaults: { 
                        anchor: '100%' 
                    },
                    items : [
                        {
                            xtype : 'displayfield' , 
                            hideLabel : true , 
                            value : '<b>Tracker type :</b>'
                        },{
                            xtype:          'combo',
                            mode:           'local',
                            hideLabel : true , 
                            allowBlank: true  ,
                            triggerAction:  'all',
                            editable:       false,
                            id : 'mac-type2'  ,
                            hiddenName: 'mac[type2]', 
                            displayField:   'name',
                            valueField:     'value' ,
                            invalidText : 'Tracker type must be selected' , 
                            emptyText : 'Please select tracker type' , 
                            store: 
                                Ext.create('Ext.data.Store', {
                                fields: ['name', {name: 'value', type: 'int'}],
                                data : [
                                    {name : 'Metadata',   value: 1 },
                                    {name : 'Element',   value: 2 } ,  
                                    {name : 'Regular expression',   value: 3 } ,
                                    {name : 'Custom function',   value: 4 }
                                ]
                            })
                        }, 
                        {
                            xtype : 'button' , 
                            cls : 'monoloop-submit-form' , 
                            hideLabel : true , 
                            text : 'Process' ,
                            handler : function(){
                                if( Ext.getCmp('mac-type2').isValid()){
                                    me.activeType2 = Ext.getCmp('mac-type2').getValue() ; 
                                    me.process2() ; 
                                }
                            }
                        }
                   ]
               } , {
           			xtype : 'displayfield' , 
           			value : '<b>Asset</b> (optional)'
               } , {
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: true  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : 'mac-asset'  ,
                    hiddenName: 'mac[asset]', 
                    displayField:   'name',
                    valueField:     'uid' , 
                    emptyText : 'Please select asset' , 
                    store: 
                        Ext.create('Ext.data.Store', {
                        fields: ['name', {name: 'uid', type: 'int'}],
                        proxy: {
			                type: 'ajax',
			                url : 'index.php?eID=monoloop_assets&pid=480&cmd=assets/list',
			                reader: { 
			                    type: 'json',
			                    root: 'topics', 
			                    totalProperty: 'totalCount' 
			                    
			                } , 
			                actionMethods: {
			                    read: 'POST'
			                } , 
			                pageParam: undefined , 
			                limitParam: undefined
			            }  
			            
                    }) , 
                    listeners : {
                    	'change' : function(cb , newV , oldV ){
                    		me.activeAsset = newV ; 
                    	}
                    }
                }
            ]
        }) ; 
    } , 
     
    
    // Interface fucntion 
    process1 : function(){
        ;
    } , 
    
    process2 : function(){
        ;
    } , 
    
    nextProcess : function(){
        
    }
    
    
}) ; 