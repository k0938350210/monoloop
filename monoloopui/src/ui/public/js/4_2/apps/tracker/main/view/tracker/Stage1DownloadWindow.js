Ext.define('Tracker.view.tracker.Stage1DownloadWindow', { 
    extend: 'Ext.window.Window',
    alias: 'widget.trackerflowstage1download',
    
    bodyPanel : null  , 
    extensionStore : null  , 
    
    activeTypeData : '' , 
    
    initComponent: function(){ 
        var me = this ; 
        
        me.title = 'Download extension selector' ;
        me.closable = true ; 
        me.width = 370 ; 
        me.height = 255 ;
        
        me.modal = true ;
        me.maximizable = false ; 
        me.resizable = true ; 
        me.plain = true ; 
        
        me.buttons = [
        {
            text: 'CANCEL' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){ 
               me.close() ; 
        	}       
	    } , 
        {
            text: 'NEXT' , 
            cls : 'monoloop-submit-form' , 
            handler: function(){
                var total =  me.extensionStore.getCount() ;  
                var extStr = '' ; 
                for( var i = 0 ; i < total ; i++){
                    var rec = me.extensionStore.getAt(i) ;
                    if( extStr == ''){
                        extStr = rec.get('name') ; 
                    }else{
                        extStr = extStr + ',' + rec.get('name') ; 
                    } 
                }   
                me.activeTypeData = extStr ;  
                
                me.processNext() ;  
            } 
        }
        ] ; 
        
        me.initBodyPanel() ; 
        
        Ext.apply(this, {
            layout: 'fit' , 
            height: 300 ,
            items: [me.bodyPanel] 
        });
        
        this.callParent(arguments); 
     } , 
     
     initBodyPanel : function(){
        var me = this ; 
        
        me.extensionStore = Ext.create('Ext.data.JsonStore', {
            fields: [ {name: 'name' } ]
        }) ; 
        //    pdf exe zip rar tar.gz doc docx xls xlsx
        if( me.activeTypeData == ''){
            me.extensionStore.loadData([{name:'pdf'} , {name:'exe'} , {name:'zip'} , {name:'rar'} , {name:'tar.gz'} , {name:'doc'} , {name:'docx'} , {name:'xls'} , {name:'xlsx'}])
        }else{ 
            var items = me.activeTypeData.split(',') ; 
            for( var i = 0 ; i < items.length ; i++){ 
                me.extensionStore.add({name : items[i]}) ; 
            }
        }
        
        function renderAction(value, p, r){
            deleteData = '<td><div onClick="Ext.getCmp(\''+me.id+'\').deleteExtension(\''+r.data['name']+'\')" title="Delete" class="placement-delete"></div></td>' ; 
	    	var ret =  '<table width="100%" ><tr align="center"> '   
	    			     + deleteData
                         +
					   '</tr></table>'  ;
 
	 
			return ret ;   
         }
        
        me.bodyPanel = Ext.create('Ext.panel.Panel' , {
            width: '100%',
            id: me.id + '-form', 
            border : false , 
            bodyBorder : false , 
            layout : 'border' , 
            labelWidth: 100,
            defaults: {
                anchor: '95%',
                allowBlank: false,
                msgTarget: 'side'
            },
            items : [ 
            { 
                xtype: 'fieldset', 
                collapsible: false,
                region : 'north' ,  
                defaults: {
                    labelWidth: 89,
                    anchor: '100%',
                    layout: {
                        type: 'hbox'
                    }
                },
                padding : 10 , 
                items : [
                {
                    xtype: 'fieldcontainer', 
                    fieldLabel : 'Extension' ,  
                    items : [ 
                    {
                        xtype: 'textfield',
                        width: 150,
                        id : me.id + '-text' ,
                        allowBlank: false
                    },
                    {
                        xtype: 'button' , 
                        text : 'add' , 
                        cls : 'monoloop-submit-form' , 
                        width : 80 , 
                        handler : function(){
                            if( Ext.getCmp(me.id + '-text').isValid() ){
                                me.addExtension( Ext.getCmp(me.id + '-text').getValue()) ; 
                            }else{
                                
                            }
                        } 
                    } 
                    ]
                }
                ]
            },{
                region : 'center' , 
                xtype : 'grid' , 
                store:  me.extensionStore,
                disableSelection: true,
                width : '100%' ,  
                columns: [
                  {header:'Name',width : 200 ,dataIndex:'name',sortable:true},  
                  {header: 'Action' ,  dataIndex : 'name' , sortable : false , renderer: renderAction } ,  
               ]  
            }] 
        }) ; 
     } , 
     
     deleteExtension : function(name){
        var me = this ; 
        var total =  me.extensionStore.getCount() ;  
        for( var i = 0 ; i < total ; i++){
            var rec = me.extensionStore.getAt(i) ;
            if( name == rec.get('name')){
                 me.extensionStore.removeAt(i) ; 
                 return ; 
            }
        } 
     } , 
     
     addExtension : function(name){
        var me = this ; 
        // check for duplicate data ; 
        var total =  me.extensionStore.getCount() ;  
        for( var i = 0 ; i < total ; i++){
            var rec = me.extensionStore.getAt(i) ;
            if( name == rec.get('name')){ 
                Ext.getCmp(me.id + '-text').markInvalid('Duplicate data.')
                 return ; 
            }
        }  
        me.extensionStore.add({name : name}) ; 
        
     } , 
     
     // Interface , 
     
     processNext : function(){
        
     }
}) ; 