Ext.define('Tracker.view.tracker.Stage3CfWindow', { 
    extend: 'Tracker.view.tracker.Stage3BaseWindow',
    alias: 'widget.trackerflowstage3cf',
    
    bodyPanel : null ,
    
    activeURL : '' , 
    activeTypeData : '' , 
    activeSaveFunction : 0 , 
    activeExample : '' , 
    
    initComponent: function(){
        var me = this ; 
        me.preload() ; 
        me.title = 'TRACKER : custom function ' ;
        me.closable = true ;
        me.width = 370 ;
        me.height = 425 ; 
        //id : 'mc-customfunction-window' , 
        /*
        me.baseCls = 'ces-window' ; 
        me.iconCls = 'ces-window-header-icon' ;  
        */
        //border:false,
        me.modal = true ; 
        me.maximizable = false ;
        me.resizable = false ; 
        me.plain = true ; 
        //items : [myForm] , 
        me.buttons = [
        {
            text: 'CANCEL' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){ 
               me.close() ; 
        	}       
        } , 
        {
            text :'NEXT' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){
               if(Ext.getCmp( me.id + '-form').getForm().isValid()){  
                   me.activeTypeData = Ext.getCmp('mc-customfunction-typedata').getValue() ; 
                   me.activeSaveFunction = Ext.getCmp(me.id + '-savefunction').getValue() ; 
                   me.activeExample =  Ext.getCmp('mc-customfunction-testResult').getValue() ; 
                   me.processNext() ; 
               }
            }
        
        }
        ] ;
        
        me.listeners = {
            'afterrender' : function(w){
                var iframe = document.getElementById("pw-browse") ;
                if( me.activeTypeData != ''){
                    Ext.getCmp('mc-customfunction-typedata').setValue(me.activeTypeData) ;  
                    //Ext.getCmp('mc-customfunction-testResult').setValue( document.getElementById("pw-browse").contentWindow.category_preview.catCustomFunc( Ext.getCmp('mc-customfunction-typedata').getValue()))  ; 
                    iframe.contentWindow.postMessage(JSON.stringify({
    				        t: "tracker-catCustomFunc"   , 
    				        activeTypeData : Ext.getCmp('mc-customfunction-typedata').getValue() , 
                            setBack : true , 
                            setBackID : 'mc-customfunction-testResult'
    				}), me.activeURL); 
                }else{
                    Ext.getCmp('mc-customfunction-typedata').setValue(' return \'Your category\' ; ') ; 
                    //Ext.getCmp('mc-customfunction-testResult').setValue( document.getElementById("pw-browse").contentWindow.category_preview.catCustomFunc( Ext.getCmp('mc-customfunction-typedata').getValue()))  ; 
                    iframe.contentWindow.postMessage(JSON.stringify({
    				        t: "tracker-catCustomFunc"   , 
    				        activeTypeData : Ext.getCmp('mc-customfunction-typedata').getValue() , 
                            setBack : true , 
                            setBackID : 'mc-customfunction-testResult'
    				}), me.activeURL);
                }
                monoloop_base.showMessage('Step 3 :' , 'Check that the tracker captures the information you want and apply filters if appropriate ') ;
            }
        }  ; 
        
        me.initBodyPanel() ; 
        
        Ext.apply(me ,{   
            items :  [   me.bodyPanel ]
        }) ; 
        
        this.callParent(arguments); 
    } , 
    
    initBodyPanel : function(){
        var me = this ; 
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
        width: '100%',
        height: 275, 
        id: me.id + '-form', 
        border : false , 
        bodyBorder : false , 
        //bodyCls : 'cse-panel-body' , 
        bodyStyle: 'padding : 10px ;' , 
        labelWidth: 100,
        defaults: {
            anchor: '95%',
            allowBlank: false,
            msgTarget: 'side'
        },
  
        items: [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    hidden : true ,
                    value : '<b>Example URL :</b>'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    hidden : true ,
                    name : 'mc_customfunction[url]'  ,
                    id : 'mc-customfunction-url' ,  
                    allowBlank: false  , 
                    disabled  : false  , //--
                    value : me.activeURL   
                }, 
                {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<b>Custom function :</b>'
                },{
                    xtype: 'textarea', 
                    width: 320,
                    hideLabel : true ,  
                    height: 100,
                    id : 'mc-customfunction-typedata'  
                },{
                    xtype: 'button',  
                    cls : 'monoloop-submit-form' , 
                    id : 'mc-customfunction-testbtn'  ,  
                    style : 'margin-right : 200px' , 
                    text : 'Test' , 
    	            handler : function(){ 
    	               //Ext.getCmp('mc-customfunction-testResult').setValue( document.getElementById("pw-browse").contentWindow.category_preview.catCustomFunc( Ext.getCmp('mc-customfunction-typedata').getValue()))  ; 
                       
                       var iframe = document.getElementById("pw-browse") ;
                       iframe.contentWindow.postMessage(JSON.stringify({
    				        t: "tracker-catCustomFunc"   , 
    				        activeTypeData : Ext.getCmp('mc-customfunction-typedata').getValue() , 
                            setBack : true , 
                            setBackID : 'mc-customfunction-testResult'
    				}), me.activeURL);
    	        	} 
                },{
                    xtype: 'textarea', 
                    width: 320,
                    allowBlank : true ,
                    hideLabel : true ,  
                    height: 100,
                    disabled  : false  , //--
                    id : 'mc-customfunction-testResult'  
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<hr><b>Save Function :</b>', 
                    hidden : me.hideSaveFunction ,
                    style : { marginBottom: '0px' }
                },{
                    xtype: 'combo',
                    mode: 'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,  
                    displayField:   'name',
                    valueField:     'value' , 
                    emptyText : 'Please select' , 
                    id : me.id + '-savefunction' , 
                    store:  me.saveFunctionStore , 
                    hidden : me.hideSaveFunction , 
                    allowBlank :  me.hideSaveFunction  , 
                    listeners : {
                    	afterrender : function(cb){
                    		if(me.activeSaveFunction == 0 || me.activeSaveFunction == null)
                    			return ; 
                   			cb.setValue(me.activeSaveFunction) ; 
                    	}
                    }
                }
				]

	    });
    } , 
    
    processNext : function(){
        
    }
}) ; 