Ext.define('Tracker.view.tracker.Stage3BaseWindow', { 
    
    extend: 'Ext.window.Window',

    msgCt : null , 
    saveFunctionStore : null , 
    
    layout : 'fit' ,
    
    hideSaveFunction : true ,
    
    refStag2Obj : null , 
    activeURL : '' , 
    
    createBox : function(t, s){ 
        return '<div class="monoloop-msg monoloop-msg-inner"><h3>' + t + '</h3><p>' + s + '</p></div>'; 
    } , 
    
    postMessageListenner : null ,
    
    showMessage : function(title, format){
        var me = this ; 
        
        if(!me.msgCt){
            me.msgCt = Ext.core.DomHelper.insertFirst(me.getEl(), {id: me.id + '-msg-div'}, true);
        } 
        var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.core.DomHelper.append(me.msgCt, me.createBox(title, s), true);
        m.hide();
        m.slideIn('t') ; //.ghost("t", { delay: 5000, remove: true});  
       
        m.addListener('mouseover' , function(){
            m.ghost("t", {});
        }) ; 
        
         
    } , 
    
    clearMessage : function(){
        var me = this ; 
         Ext.get('main-msg-div').dom.innerHTML = '' ; 
    } ,
    
    preload : function(){
        var me = this ; 
        me.saveFunctionStore = Ext.create('Ext.data.Store', {
            fields: ['name' , { name : 'value' , type : 'int'}],
            data : [
                {name : 'Override' , value : 1  },
                {name : 'Add'  , value : 2 } ,  
                {name : 'Substract' , value : 3 } ,
                {name : 'Push' , value : 4 }
            ]
        }) ; 
    }  , 
    
    initComponent: function(){ 
        var me = this ;  
        me.initPosMessage() ;        
        me.listeners['beforeclose'] = function(w){
            if (window.addEventListener) {
		        return removeEventListener("message", me.postMessageListener, false);
			} else {
		        detachEvent("onmessage", me.postMessageListener); 
			}
        } ;
        this.callParent(arguments);  
    } , 
    
    initPosMessage : function(){
        var data;
    	var me = this ;  
    	me.postMessageListenner = function(event) {
			var data;
			data = JSON.parse(event.data);
	        switch (data.t) {
	          case 'tracker-setvalue-to-id':
                if(data.setbackID != ''){ 
                    Ext.getCmp(data.setbackID).setValue(data.data) ; 
                }      
			  break;   
	  		}
		};
		if (window.addEventListener) {
	        return addEventListener("message", me.postMessageListenner, false);
		} else {
	        attachEvent("onmessage", me.postMessageListenner); 
		}
    }
}) ;     