Ext.define('Tracker.view.tracker.Stage2Window', { 
    extend: 'Ext.window.Window',
    alias: 'widget.trackerflowstage2',
    
    navPanel : null , 
    bodyPanel : null  , 
    
    temPath : ''  ,
    firstDomain : '' , 
    // data in , data out ; 
    activeType : 0 , 
    activeURL : '' , 
    activeTypeData : '' ,
    activeName : '' , 
    activeSaveFunction : null , 
    // local variable
    urlList : new Array() , 
    urlIndex : 0 , 
     
    
    showCookieBtn : false , 
    
    step1Title : 'Step1' , 
    step2Title : 'Step2' ,
    step1Text : '' ,
    step1_1Text : '' ,
    step2Text : 'Select the element on the page you want to track and click Select' ,
    
    
    url : '' , 
    enable : false ,  
    postMessageListener : '' , 
    proxyList : [] , 
    
    
    initComponent: function(){ 
        var me = this ; 
        
        var scnWid = 0 ;
        var scnHei = 0 ;
    	if (self.innerHeight) // all except Explorer
    	{
    		scnWid = self.innerWidth;
    		scnHei = self.innerHeight;
    	}
    	else if (document.documentElement && document.documentElement.clientHeight)
    		// Explorer 6 Strict Mode
    	{
    		scnWid = document.documentElement.clientWidth;
    		scnHei = document.documentElement.clientHeight;
    	}
    	else if (document.body) // other Explorers
    	{
    		scnWid = document.body.clientWidth;
    		scnHei = document.body.clientHeight;
    	}
    	
        
        me.width = scnWid - 50,
        me.height = scnHei - 50,
        me.title = 'Browse to the page you want to use as example page' ;
        me.closable = true ; 
        me.modal = true ; 
        me.maximizable = true ; 
        me.plain = true ; 
        me.layout = 'border' ; 
         
        me.initClientNav() ; 
        me.initBodyPanel() ; 
         
        // 'Auto events' 
        me.listeners = me.getListeners() ; 
        
        Ext.apply(me ,{   
            items :  [ me.navPanel , me.bodyPanel ]
        }) ; 
        
        var name =  me.activeName.replace('tracker','') ; 
        name = (name+'').toLowerCase() ;
        me.step1Text = 'Browse to a '+ name +' page and click Select Mode ' ; 
        me.step1_1Text = 'Browse to a '+ name +' page and click Select ' ; 
        
        me.initPosMessageListener() ; 
        
        this.callParent(arguments); 
    } , 
 
    
    initPosMessageListener : function(){
    	var data;
    	var me = this ; 
    	me.postMessageListener = function(event) {
			var data;
			data = JSON.parse(event.data);
	        switch (data.t) {
	          case 'confirm':
	          	me.enable = true ;   
	            me.url = data.url ; 
	            Ext.getCmp('pw-url').setValue(data.displayURL) ; 
	            monoloopLog.log('Found invocation in customer site') ; 
                monoloopLog.log('Start loading bundle.js');
                
                var iframe = document.getElementById("pw-browse") ;  
                var time =  new Date().getTime() ; 
	            iframe.contentWindow.postMessage(JSON.stringify({
			        t: "loadbundle" , 
                    domain : tracker_main.baseRelativeUrl , 
			        url : tracker_main.baseRelativeUrl + 'fileadmin/cbr/editor-bundle.js?' + time
				}), me.url);
			  break;  
			  case 'loadbundle-success' :   
			  	var iframe = document.getElementById("pw-browse") ;  
			  	if( me.activeType != 2){
			  		monoloopLog.log('Init tracker preview js');
			  		iframe.contentWindow.postMessage(JSON.stringify({
				        t: "init-tracker-previewmode"  
					}), me.url);
		  		}else{
		  			monoloopLog.log('Init tracker select js');
		  			iframe.contentWindow.postMessage(JSON.stringify({
				        t: "init-tracker-selectmode"  
					}), me.url);
		  		} 
			  break  ; 
			  case 'tracker-preview-asset-loaded' : 
			  	Ext.getCmp(me.id +  '-select-btn').setDisabled(false) ;
        		Ext.getCmp(me.id +  '-tosectmode-btn').setDisabled(false) ; 
        		me.afterPreviewLoaded() ; 
			  break ; 
			  case 'tracker-xpath-asset-loaded' : 
			  	var iframe = document.getElementById("pw-browse") ;
			    iframe.contentWindow.postMessage(JSON.stringify({
				        t: "tracker-xpath-setPreXPath"   , 
				        activeTypeData : me.activeTypeData
				}), me.url); 
				Ext.getCmp(me.id +  '-select-btn').setDisabled(false) ;
        		Ext.getCmp(me.id +  '-tosectmode-btn').setDisabled(false) ; 
        		me.afterXpathLoaded() ; 
			  break ; 
			  case 'tracker-changeSelectorTxt' : 
			    me.changeSelectorTxt(  data.selector  ) ;
			  break ; 
	  		}
		};
		if (window.addEventListener) {
	        return addEventListener("message", me.postMessageListener, false);
		} else {
	        attachEvent("onmessage", me.postMessageListener); 
		}
    } , 
    
    afterPreviewLoaded : function(){
    	
    } , 
    
    afterXpathLoaded : function(){
    	
    } , 
    
    getListeners : function(){
        var me = this ; 
        var obj = { 
            afterrender : function(w){  
                if( me.activeURL != ''){
                    Ext.getCmp('pw-url').setValue(me.activeURL) ; 
                    me.pushURL() ;
               	    me.processURL() ; 
                }else{
                    Ext.getCmp('pw-url').setValue(me.firstDomain) ; 
                    me.pushURL() ;
               	    me.processURL() ; 
                }    
                if( me.activeType != 2 ){
                    Ext.getCmp(me.id + '-back2browe-btn').setVisible(false) ; 
                    Ext.getCmp(me.id + '-tosectmode-btn').setVisible(false) ; 
                    
                    monoloop_base.showMessage( me.step1Title , me.step1_1Text ) ; 
                }else{
                    Ext.getCmp(me.id + '-back2browe-btn').setVisible(false) ; 
                    Ext.getCmp(me.id + '-select-btn').setVisible(false) ; 
                    
                    monoloop_base.showMessage( me.step1Title , me.step1Text ) ; 
                } 
                
                
            } ,  
            beforeclose : function(w,eOpts){
            	if (window.addEventListener) {
			        return removeEventListener("message", me.postMessageListener, false);
				} else {
			        detachEvent("onmessage", me.postMessageListener); 
				} 
            } , 
             close : function(w){ 
             	if(Ext.get('main-msg-div') != undefined)
                	Ext.get('main-msg-div').dom.innerHTML = '' ;
                me.resetActiveData() ;   
             }
        } ; 
        return obj ; 
    } , 
    
    
    initClientNav : function(){
        var me = this ; 
        me.navPanel = Ext.create('Ext.form.Panel' , {
            id : me.id + '_nav' , 
            region: 'north' ,  
            split: false, 
			margins:'0',
            cmargins:'0',
            collapsible: false,
            border : false , 
            bodyBorder : false , 
            bodyStyle  : 'background-color: rgb(189,199,194);padding : 5px 0 0 0 ; ' , 
            height: 35 ,  
            items: [
            {
                xtype: 'fieldset', 
                border : false , 
                id : me.id + '-nav-fieldset' , 
                defaults: {
                    labelWidth: 89,
                    anchor: '100%',
                    layout: {
                        type: 'hbox' 
                    }
                },
                items : [
                {
                    xtype: 'fieldcontainer',
                    msgTarget : 'side',     
                    width : 900  ,  
                    defaults: { 
                        hideLabel: true
                    },
                    items: [{
                        xtype: 'button' ,  
                        cls : 'monoloop-submit-form' , 
                       	text : '<span class="icon icon-angle-left"></span>',
                       	listeners : {
                       	    'click' : function(button , e){
                       	        me.urlMoveBack() ; 
                       	    }
                       	} 
                    },{
                        xtype: 'displayfield' ,   
                        value : '&nbsp;' , 
                        width : 5 
                    } ,  {
                        xtype: 'button' ,
                        cls : 'monoloop-submit-form' , 
                       	text : '<span class="icon icon-angle-right"></span>',
                       	listeners : {
                       	    'click' : function(button , e){
                       	        me.urlMoveForword()  ; 
                       	    }
                       	} 
                    } ,{
                        xtype: 'displayfield' ,   
                        value : '&nbsp;' , 
                        width : 5 
                    }, {
                        xtype: 'button' ,
                        cls : 'monoloop-submit-form' , 
                       	text : '<span class="icon icon-refresh"></span>',
                        width : 25 , 
                       	listeners : {
                       	    'click' : function(button , e){
                       	        me.pushURL() ;
                       	        me.processURL() ; 
                       	    }
                       	} 
                    },{
                        xtype: 'displayfield' ,   
                        value : '&nbsp;' , 
                        width : 5 
                    }, {
                        xtype     : 'textfield',
                        name      : 'url',
                        id        : 'pw-url',
                        allowBlank: false ,
                        enableKeyEvents : true , 
                        width    : 500  ,
                        listeners : {
                            'keypress' : function(textField ,e){
                                if(e.keyCode == e.ENTER) {
                                   me.pushURL() ;
                                   me.processURL() ;        
                                }
                            }
                        }
                    },{
                        xtype: 'displayfield' ,   
                        value : '&nbsp;' , 
                        width : 5 
                    },{
                        xtype: 'button' ,
                        id : 'pw-go-btn' ,  
                        cls : 'monoloop-submit-form' , 
                        text : 'GO' , 
                       	listeners : {
                       	    'click' : function(button , e){
                                me.pushURL() ;
                                me.processURL() ;        
                       	    }
                       	}  
                    },{
                        xtype: 'displayfield' ,   
                        value : '&nbsp;' , 
                        width : 5 
                    },{
                        xtype: 'button' ,
                        id : me.id + '-cookie-btn' ,  
                        text : 'Show cookies' ,  
                        cls : 'monoloop-submit-form' , 
                        hidden : ! me.showCookieBtn , 
                       	listeners : {
                       	    'click' : function(button , e){
                                 me.showCookieWindow() ; 
                       	    }
                       	}  
                    }]
                }] 
            } 
            ]  
        });
        
        
    } ,
    
    showCookieWindow : function(){
         Ext.create('Monoloop.util.monoloopCookieEditor' , {
            id : 'monoloop-cookie-editor'
         }).show() ; 
    } ,
    
    initBodyPanel : function(){
        var me = this ; 
        var contentHTML = '' ; 
        if( me.activeType != 2 ){
            contentHTML = "<iframe id='pw-browse' name='pw-browse' src ='' width='100%' height='100%' onLoad=\"Ext.getCmp('"+me.id+"').browserSrcChange();\" frameborder='0'><p>Your browser does not support iframes.</p></iframe>"  ; 
        }else{
            contentHTML = "<iframe id='pw-browse' name='pw-browse' src ='' width='100%' height='100%' onLoad=\"Ext.getCmp('"+me.id+"').browserSrcChange();\" frameborder='0'><p>Your browser does not support iframes.</p></iframe>" ; 
        }
        
        me.bodyPanel = Ext.create('Ext.panel.Panel' , {
            region: 'center', 
            split: false, 
			margins:'0',
            cmargins:'0',  
            border : false , 
			html:  contentHTML  , 
            buttons: me.getButtons() 
        }); 
    } , 
    
    getButtons : function(){
    	var me = this ; 
    	return [  
            {
	           id : me.id + '-back2browe-btn' , 
               disabled : false ,
	            text: 'BACK TO BROWSE MODE' , 
	            cls : 'monoloop-submit-form' , 
	            handler: function(){
	                me.backtobrowse_btn() ; 
	            }   
	        } , {
                id : me.id +  '-select-btn' , 
                disabled : true ,
                cls : 'monoloop-submit-form' , 
	            text: 'SELECT' , 
	            handler: function(){
	                me.select_btn() ; 
	            }   
	        } ,  {
	           id : me.id +  '-tosectmode-btn' , 
               disabled : true ,
               cls : 'monoloop-submit-form' , 
	            text: 'SELECT MODE' , 
	            handler: function(){
                   me.toselect_btn() ; 
	            } 
	        }
	         ] ;
    } , 
    
    backtobrowse_btn : function(){
    	var me = this ; 
    	/*
    	 if( document.getElementById("pw-browse").contentWindow.previewView == undefined)
            return ; 
         document.getElementById("pw-browse").contentWindow.monoloop_select_view.setMode(1) ; 
         */
          var iframe = document.getElementById("pw-browse") ;
	    iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-xpath-setMode"   , 
		        mode : 1 
		}), me.url); 
         Ext.getCmp( me.id + '-tosectmode-btn').setVisible(true) ; 
         Ext.getCmp( me.id + '-back2browe-btn').setVisible(false) ; 
         Ext.getCmp( me.id + '-select-btn').setVisible(false) ; 
         Ext.get( 'main-msg-div').dom.innerHTML = '' ; 
         monoloop_base.showMessage( me.step1Title , me.step1Text ) ; 
    } , 
    
    select_btn : function(){
    	var me = this ; 
    	if( Ext.getCmp('pw-url').isValid()){
			me.activeURL =  Ext.getCmp('pw-url').getValue() ;
			me.select_process() ;
			if( Ext.get( me.id + '-msg-div') != null )
				Ext.get('main-msg-div').dom.innerHTML = '' ; 
		}
    } , 
    
    toselect_btn : function(){
    	var me = this ; 
    	 /*
    	 if( document.getElementById("pw-browse").contentWindow.previewView == undefined)
            return ; 
         */
		 var iframe = document.getElementById("pw-browse") ;
	    iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-xpath-setMode"   , 
		        mode : 2 
		}), me.url); 
		    
         //document.getElementById("pw-browse").contentWindow.monoloop_select_view.setMode(2) ; 
         Ext.getCmp( me.id + '-tosectmode-btn').setVisible(false) ; 
         Ext.getCmp( me.id + '-back2browe-btn').setVisible(true) ; 
         Ext.getCmp( me.id + '-select-btn').setVisible(true) ; 
         Ext.get( 'main-msg-div').dom.innerHTML = '' ; 
         monoloop_base.showMessage( me.step2Title , me.step2Text ) ; 
    } , 
    
    
    select_process : function(){
        // Inteface ...
    } , 
    
    resetActiveData : function(){
        //console.debug('p1') ; 
    } , 
    
    // helper function 
    
    pushURL : function(){
        var me = this ; 
        //alert(pwMain.urlList.length) ; 
        if( me.urlIndex == 0 || me.urlIndex == me.urlList.length -1){
            me.urlList.push(Ext.getCmp('pw-url').getValue()) ;
            me.urlIndex = me.urlList.length -1 ; 
        }else{
            me.urlList = me.urlList.slice(0 , me.urlIndex ) ; 
            me.urlList.push(Ext.getCmp('pw-url').getValue()) ;
            me.urlIndex = me.urlList.length -1 ; 
        }
    } , 
    
    urlMoveBack : function(){
        var me = this ; 
        if( me.urlIndex <= 0 ){
            me.urlIndex = 0 ; 
            return ; 
        }
        me.urlIndex-- ; 
        Ext.getCmp('pw-url').setValue(me.urlList[me.urlIndex]) ; 
        me.processURL() ;
    } , 
    
    urlMoveForword : function(){
        var me = this ; 
        if( me.urlIndex >=  me.urlList.length - 1){
            me.urlIndex = me.urlList.length - 1; 
            return ; 
        }
        me.urlIndex++ ; 
        Ext.getCmp('pw-url').setValue(me.urlList[me.urlIndex]) ; 
        me.processURL() ;
    }  , 
    
    browserSrcChange : function(){
        var me = this ; 
        
        monoloopLog.log('placement : browserSrcChange');
        
        if( document.URL.indexOf(document.getElementById('pw-browse').src) == -1 &&  me.enable == false ){
            // Change to proxy mode ;  
            // Wait 1 second to swith ; 
            setTimeout(function(){
            	if(me.enable == false){
            		monoloopLog.log('placement : Change to proxy mode');
            		
            		var url = document.getElementById('pw-browse').src ; 
            		if( document.getElementById('pw-browse') != null){  
		                if( me.activeType != 2){ 
			                document.getElementById('pw-browse').src = me.temPath + 'category_preview.php?l='+me.urlencode(url) ; 
			            }else{
			                document.getElementById('pw-browse').src = me.temPath + 'category_xpath.php?l='+me.urlencode(url) ; 
			            }
		                me.enable = true ;  
		                me.showProxyWarning() ; 
						me.addProxyList(url) ;  
		            }
		            monoloopLog.log('placement : add domain to proxy list - ' + url) ;  
            	}
			}, 1500);
            
            return ; 
         } 
        
        
        
        return ;     
        // Old placement below ; 
        if( document.getElementById("pw-browse").contentWindow.previewView == undefined)
            return ; 
            
        
        var url = document.getElementById("pw-browse").contentWindow.previewView.getUrl()  ;
 
        Ext.getCmp('pw-url').setValue(url) ; 
        
        if( me.activeType == 2){ 
            if( me.activeTypeData != '' ){ 
                document.getElementById("pw-browse").contentWindow.monoloop_select_view.setPreXPath(me.activeTypeData) ; 
            }
        }
        
        Ext.getCmp(me.id +  '-select-btn').setDisabled(false) ;
        Ext.getCmp(me.id +  '-tosectmode-btn').setDisabled(false) ; 
    } ,
    
    processURL : function(){ 
        //alert(pwMain.urlencode(Ext.getCmp('pw-url').getValue())) ; 
        var me = this ; 
        if( document.getElementById('pw-browse') != null){
            Ext.getCmp(me.id +  '-select-btn').setDisabled(true) ;
            me.enable = false ; 
            monoloopLog.log('Start render site');
            var proxyURL = 'category_xpath.php?l=' ; 
            if( me.activeType != 2){
            	proxyURL = 'category_preview.php?l=' ; 
           	}
            /*
            if( me.activeType != 2){
                //document.getElementById('pw-browse').src = me.temPath + 'category_preview.php?l='+me.urlencode(Ext.getCmp('pw-url').getValue()) ; 
                
            }else{
                //document.getElementById('pw-browse').src = me.temPath + 'category_xpath.php?l='+me.urlencode(Ext.getCmp('pw-url').getValue()) ;  
            }
            */
            var renderURL = me.processPosmessageURL(Ext.getCmp('pw-url').getValue(),proxyURL)  ; 
            monoloopLog.log('render : ' + renderURL);
            document.getElementById('pw-browse').src =  renderURL ; 
        } 
    } , 
    
    // util function 
    
    changeSelectorTxt : function(xPath){
        var me = this ; 
        me.activeTypeData = xPath ;     
    }  , 
    
    urlencode : function(str) {
        return escape(str).replace(/\+/g,'%2B').replace(/%20/g, '+').replace(/\*/g, '%2A').replace(/\//g, '%2F').replace(/@/g, '%40');
    } , 
    
    urldecode : function(str) {
        return unescape(str.replace('+', ' '));
    }  , 
    
    //Post Message Helper 
    
    processPosmessageURL : function(url , proxy){
		var me = this ; 
		
		// This function for filter url ; 
		if(Ext.util.Format.trim(url) == '')
			return url ; 
			
		for(var i = 0 ; i < me.proxyList.length ; i++ ){
			if(me.proxyList[i] == url)
				return me.temPath + proxy+ me.urlencode(url) ;  ; 
		}	
		
		if(url.toLowerCase().indexOf('http') != 0){
			url = '//' + url ; 
		}
		return url ; 
	} , 
	
	/* N pending : as some customer not using invocation entire domain */ 
	
	addProxyList : function(url){
		var me = this ;  
		for(var i = 0 ; i < me.proxyList.length ; i++ ){ 
			if(me.proxyList[i] == url){  
				return ; 
			}
		}
		me.proxyList.push(url) ;  
	} , 
	
	showProxyWarning : function(){
		var ret = Ext.query('#msg-div-2') ;  
		if(ret.length == 0 || ret[0].innerHTML == '' )
			monoloop_base.msgCloseOnClick(document.getElementsByTagName("body")[0],'Notice' ,'This site does not have our code in the page, so we will pull your site content through our proxy. This can cause certain features, content not being available or styling missing. To avoid this - please place the invocation code from Account/Codes & Scripts section into the header section of your site template.') ;  
	}
}) ; 