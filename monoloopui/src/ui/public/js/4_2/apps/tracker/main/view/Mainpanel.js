Ext.define('Tracker.view.Mainpanel', {
	alias: 'widget.trackermainpanel',
	extend: 'Ext.tab.Panel',  
	
	requires: [
        'Tracker.view.NodeList' , 
        'Monoloop.util.monoloopFolderTree'
    ],
	 
    initComponent: function() { 
        Ext.apply(this, {   
        	layout : 'fit' , 
            height : 500 ,   
            cls : 'MainPanel' , 
            plain : true ,
            border : false , 
            items : [{
                xtype : 'container' , 
                preventHeader : true ,  
                border : false , 
                width: '100%',
                title : 'Tracker List' ,
                id : 'monoloop-tracker-panel' , 
                layout : 'border' , 
                items: [{
                    region : 'west' , 
                    border : true , 
                    xtype : 'monoloopfoldertree' , 
                    cls : 'monoloop-folder-tree' , 
                    animCollapse: true, 
                    title : 'Folder' ,  
                    width: 200,
                    minWidth: 150,
                    maxWidth: 400, 
                    collapsible: true , 
                    split: true , 
                    id : 'trackers-tree' , 
                    treeEditURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/editFolder' , 
                    treeNewURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/newFolder' , 
                    treeDeleteURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/deleteFolder' , 
                    treeTrigger : function(){
                         Ext.getCmp('trackers-list').mLoadData( ) ; 
                    }, 
                    moveNode : function(gridNodeID , treeNodeID){
                    	var me = this ;  
                    	me.setLoading('Moving data ...') ; 
                    	Ext.Ajax.request({
                            url: 'index.php/moveTree?eID=ml_tracker' , 
                			// process the response object to add it to the TabPanel:
                            params : {
                            	from : gridNodeID , 
                            	to : treeNodeID
                            } , 
                			success: function(xhr) {
                	 			me.setLoading(false) ;  
                			},
                			failure: function() { 
                                Ext.MessageBox.alert('Error', '#502' ) ; 
                                me.setLoading(false)  ;
                			}
                		});
                    }
				},{
					xtype : 'trackernodelist' , 
                	region : 'center' ,  
                	border : false , 
                    id : 'trackers-list' , 
                    treeID : 'trackers-tree' , 
                    searchID : 'tracker-search-text-id'
                }] 
            }] 
        });
        this.callParent();
    }
}) ; 