Ext.define('Tracker.controller.CustomVar', {
	extend: 'Ext.app.Controller',
	
	baseUrl : '' , 
    temPath : '' ,  
    firstDomain : '' ,  
    // Active data ; 
    activeUID : 0 , 
    activeName : '' , 
    activeListFieldID : '' , 
    activeType : 0 ,
    activeURL : '' , 
    activeTypeData : '' , 
    activeHidden : 0 , 
    activeRegex : '' , 
    activeUrlOption : 0 , 
    activeInc_www : 0 ,
    activeInc_http_https : 0 , 
    actoveDatatype : 'string' , 
    activeSaveFunction : 0 , 
    
    activeFilterType : '' , 
    activePostUserFunc : '' ,
	
	init: function() {
		var me = this; 
		me.control({  	
			'#main-panel' : {
				'newCustomvar' : me.newCustomvar , 
				'customvarEdit' : me.customvarEdit , 
				'customvarEdit2' : me.customvarEdit2
			} , 
			
			'customvarsstage1' : {
				beforeclose : 'onRemoveStage'
			}
		});	
	} , 
	
	onRemoveStage : function(){
		Ext.util.History.add(''); 
	} ,
	
	resetActiveData : function(){
        var me = this ; 
        me.activeUID = 0 ; 
        me.activeName = '' ; 
        me.activeListFieldID = '' ; 
        me.activeType = 0 ;
        me.activeURL = '' ; 
        me.activeTypeData = '' ; 
        me.activeHidden = 0 ; 
        me.activeRegex = '' ; 
        me.activeUrlOption = 0 ; 
        me.activeInc_www = 0 ;
        me.activeInc_http_https = 0 ; 
        me.actoveDatatype = 'string' ; 
        me.activeSaveFunction = 0 ;
    } ,  
	
	newCustomvar : function(config){
		var me = this ; 
		me.activeName  = config.activeName ; 
        //Ext.app.customvarsFlowStage1
        Ext.create('Tracker.view.customvar.Stage1Window',{
            id : 'stage1-flow' ,
            activeName :  me.activeName  , 
            activeListFieldID : me.activeListFieldID , 
            activeDatatype : me.activeDatatype , 
            activeType : me.activeType ,  
            processNext : function(){ 
                me.activeListFieldID = Ext.getCmp('stage1-flow').activeListFieldID ; 
                me.activeType = Ext.getCmp('stage1-flow').activeType ; 
                me.activeDatatype = Ext.getCmp('stage1-flow').activeDatatype ; 
                if( me.activeType != 5){
                	me.showEditStep2() ; 
                	Ext.getCmp('stage1-flow').close() ; 
                }else{
                	var postData = {} ; 
			        postData['activeUID'] = me.activeUID ; 
			        postData['activeURL'] = me.activeURL ; 
			        postData['activeListFieldID'] = me.activeListFieldID ; 
			        postData['activeDatatype'] = me.activeDatatype ; 
			        postData['activeType'] = me.activeType ;
			        postData['activeTypeData'] = me.activeTypeData ; 
			        postData['activeHidden'] = me.activeHidden ;  
			        postData['folder_id'] =Ext.getCmp('customvars-tree').selectedID ; 
			        var postURL = 'index.php?eID=monoloop_trackers&pid=480&cmd=customvars/saveCustomvars' ;  
			        
			        Ext.getCmp('stage1-flow').setLoading('Saving custom var ...') ;    
                    Ext.Ajax.request({
                        url: postURL , 
            			// process the response object to add it to the TabPanel:
                        params : postData , 
            			success: function(xhr) {
            			    var data = Ext.JSON.decode(xhr.responseText) ; 
                            Ext.getCmp('stage1-flow').setLoading(false)  ;  
                            Ext.getCmp('customvars-list').mLoadData() ;  
                            me.resetActiveData() ; 
                            Ext.getCmp('stage1-flow').close() ; 
            			},
            			failure: function() { 
                            Ext.MessageBox.alert('Error', '#502' ) ; 
                            Ext.getCmp('stage1-flow').setLoading(false)  ;
            			}
            		});
                } 
            }
        }).show() ;
	} , 
	
	customvarEdit : function(config){
		var me = this ;
		me.activeUID  = config.data.uid  ; 
        me.activeListFieldID = parseInt( config.data.listfield_id ) ; 
        me.activeType = parseInt(  config.data.type_id ) ; 
        me.activeURL = config.data.url ; 
        me.activeTypeData =  config.data.typeData ; 
        me.activeRegex =  config.data.regex ;
        me.activeUrlOption = config.data.urlOption ;
        me.activeInc_www = config.data.inc_www ;
        me.activeInc_http_https = config.data.inc_http_https ;
        me.activeDatatype = config.data.datatype ; 
        
        if( Ext.String.trim(config.data.addition_data)  != ''  ){
            var jsonData = Ext.JSON.decode( config.data.addition_data ) ;   
            me.activeFilterType = parseInt(jsonData.activeFilterType) ; 
            me.activePostUserFunc =  jsonData.activePostUserFunc ;  
            me.activeSaveFunction = parseInt( jsonData.activeSaveFunction ) ; 
        } 
        me.newCustomvar({
        	activeName : name
        }) ; 
	} , 
	
	customvarEdit2 : function(config){
		var me = this ;     
        
        var jsonData = {} ; 
        if( Ext.String.trim( config.data.addition_data) != ''  ){
            jsonData = Ext.JSON.decode( config.data.addition_data ) ;   
        }else{
            jsonData.activeFilterType = 0 ; 
            jsonData.activeFilterType2 = 0 ; 
            jsonData.activeType2 = 0 ; 
        }  
        
        var nameDisabled = false ; 
        
        Ext.create('Tracker.view.customvar.Edit2ValueWindow', {
            id : 'edit2-value' ,  
            title : 'Customvar' , 
            width :  400  , 
            height : 500  , 
            // Model property ; 
            name : config.data.name , 
            activeListFieldID : parseInt( config.data.listfield_id ) ,
            uid : parseInt(config.data.uid) , 
            url : config.data.url ,
            regex : config.data.regex ,
            urlOption : parseInt(config.data.urlOption) , 
            inc_www : config.data.inc_www , 
            inc_http_https : config.data.inc_http_https ,
            type_id :parseInt( config.data.type_id ), 
            typeData : config.data.typeData , 
            type_id2 : parseInt(jsonData.activeType2)  , 
            typeData2 : jsonData.activeTypeData2 , 
            activeFilterType :  parseInt(jsonData.activeFilterType), 
            activePostUserFunc :  jsonData.activePostUserFunc, 
            activeFilterType2 :  parseInt(jsonData.activeFilterType2), 
            activePostUserFunc2 : jsonData.activePostUserFunc2, 
			activeSaveFunction : parseInt( jsonData.activeSaveFunction ) , 
            nameDisabled : nameDisabled , 
            saveDataURL : 'index.php?eID=monoloop_trackers&pid=480&cmd=customvars/saveEdit2Value' , 
            saveCompleteProcess : function(){
                Ext.getCmp('edit2-value').close() ;  
                Ext.getCmp('main-panel').fireEvent('refreshData') ;  
            }
        }).show() ; 
	} ,
	
	showEditStep2 : function(){
        var me = this ;   
        tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2Window', { 
            id : 'stage2-flow' , 
            activeType : me.activeType ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData , 
            temPath : customvars_main.temPath , 
            firstDomain : customvars_main.firstDomain  , 
            resetActiveData : function(){  
                //tracker_value.resetActiveData() ; 
            } , 
            select_process : function(){ 
                me.activeURL = Ext.getCmp('stage2-flow').activeURL ; 
                me.activeTypeData = Ext.getCmp('stage2-flow').activeTypeData ;  
                if(  me.activeType == 1 ){
                    me.showFlowStep3_meta() ; 
                }else if( me.activeType == 2  ){
                    me.showFlowStep3_xpath() ; 
                }else if( me.activeType == 3  ){
                    me.showFlowStep3_regex() ; 
                }else if( me.activeType == 4  ){
                    me.showFlowStep3_customfunction() ; 
                }
            } 
        }).show() ;
    }  , 
    
    showFlowStep3_meta : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3MetaWindow', { 
            id : 'stage3-meta-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData ,
			activeSaveFunction : me.activeSaveFunction ,  
            hideSaveFunction : false , 
            processNext : function(){
                me.activeTypeData = Ext.getCmp('stage3-meta-flow').activeTypeData ; 
                me.activeSaveFunction = Ext.getCmp('stage3-meta-flow').activeSaveFunction ; 
                Ext.getCmp('stage3-meta-flow').close() ;  
                me.showFlowStep4() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep3_xpath : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3XpathWindow' , {
            id : 'stage3-xpath-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData ,  
            activeFilterType : me.activeFilterType , 
            activePostUserFunc : me.activePostUserFunc ,
            activeSaveFunction : me.activeSaveFunction ,  
            hideSaveFunction : false , 
            processNext : function(){ 
                me.activeTypeData = Ext.getCmp('stage3-xpath-flow').activeTypeData ;  
                me.activeFilterType = Ext.getCmp('stage3-xpath-flow').activeFilterType ;  
                me.activePostUserFunc = Ext.getCmp('stage3-xpath-flow').activePostUserFunc ;  
                me.activeSaveFunction = Ext.getCmp('stage3-xpath-flow').activeSaveFunction ;  
                Ext.getCmp('stage3-xpath-flow').close() ;  
                me.showFlowStep4() ; 
            }
        }).show() ;
    } , 
    
    showFlowStep3_regex : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3RegexWindow' , {
            id : 'stage3-regex-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData ,  
            activeFilterType : me.activeFilterType , 
            activePostUserFunc : me.activePostUserFunc ,
            activeSaveFunction : me.activeSaveFunction ,  
            hideSaveFunction : false , 
            processNext : function(){
                me.activeTypeData = Ext.getCmp('stage3-regex-flow').activeTypeData ; 
                me.activeFilterType = Ext.getCmp('stage3-regex-flow').activeFilterType ;  
                me.activePostUserFunc = Ext.getCmp('stage3-regex-flow').activePostUserFunc ; 
				me.activeSaveFunction = Ext.getCmp('stage3-regex-flow').activeSaveFunction ;  
                Ext.getCmp('stage3-regex-flow').close() ; 
               // Ext.getCmp('stage2-flow').close() ; 
               me.showFlowStep4() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep3_customfunction : function(){
        var me = this ; 
        Ext.create('Tracker.view.tracker.Stage3CfWindow' , {
            id : 'stage3-cf-flow' ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData ,  
            activeSaveFunction : me.activeSaveFunction ,  
            hideSaveFunction : false , 
            processNext : function(){
                me.activeTypeData = Ext.getCmp('stage3-cf-flow').activeTypeData ; 
                me.activeSaveFunction = Ext.getCmp('stage3-cf-flow').activeSaveFunction ; 
                Ext.getCmp('stage3-cf-flow').close() ; 
                //Ext.getCmp('stage2-flow').close() ; 
                me.showFlowStep4() ; 
            }
        }).show() ;
    } , 
    
    showFlowStep4 : function(){
        var me = this ; 
        
        var postData = {} ; 
        postData['activeUID'] = me.activeUID ; 
        postData['activeURL'] = me.activeURL ; 
        postData['activeListFieldID'] = me.activeListFieldID ; 
        postData['activeDatatype'] = me.activeDatatype ; 
        postData['activeType'] = me.activeType ;
        postData['activeTypeData'] = me.activeTypeData ; 
        postData['activeHidden'] = me.activeHidden ;  
        postData['activeSaveFunction'] = me.activeSaveFunction ; 
        //postData['folder_id'] =Ext.getCmp('customvars-tree').selectedID ; 
        var postURL = 'index.php?eID=monoloop_trackers&pid=480&cmd=customvars/saveCustomvars' ; 
        
        Ext.create('Tracker.view.tracker.Stage4Window' , {
            id : 'stage4' , 
            activeRegex : me.activeRegex , 
            activeUrlOption : me.activeUrlOption , 
            activeInc_www : me.activeInc_www ,
            activeInc_http_https : me.activeInc_http_https ,
            activeURL : me.activeURL , 
            postData : postData , 
            postURL : postURL , 
            resetActiveData : function(){ 
                me.resetActiveData() ; 
            } , 
            saveCompleteProcess : function(){
                Ext.getCmp('stage4').close() ; 
                Ext.getCmp('stage2-flow').close() ; 
                Ext.getCmp('main-panel').fireEvent('refreshData') ; 
            }
        }).show() 
    }
}) ;