Ext.define('Tracker.view.tracker.Stage2BasketWindow', { 
    extend: 'Tracker.view.tracker.Stage2Window',
    alias: 'widget.trackerflowstage2basket', 
    
    baseketStep : 1 , 
    dataObj : {} ,
    
    step1Title : 'Step1' , 
    step2Title : 'Step2' , 
    step3Title : 'Step3' , 
    step4Title : 'Step4.1' , 
    step5Title : 'Step4.2' , 
    step6Title : 'Step4.3' , 
    step7Title : 'Step4.4' , 
    
    step1Text : 'Add product & goto basket page' ,
    step2Text : 'Select shopping cart table' ,
    step3Text : 'Select first row of product' ,
    step4Text : 'Select product name' ,
    step5Text : 'Select SKU' ,
    step6Text : 'Select price' ,
    step7Text : 'Select quantity' , 
    
    showCookieBtn  : true ,
    
    initComponent : function(){
        this.callParent(arguments); 
    } , 
    
    initPosMessageListener : function(){
    	var data;
    	var me = this ; 
    	me.postMessageListener = function(event) {
			var data;
			data = JSON.parse(event.data);
	        switch (data.t) {
	          case 'confirm':
	          	me.enable = true ;   
	            me.url = data.url ; 
	            Ext.getCmp('pw-url').setValue(data.displayURL) ; 
	            monoloopLog.log('Found invocation in customer site') ; 
                monoloopLog.log('Start loading bundle.js');
                
                var iframe = document.getElementById("pw-browse") ;  
                var time =  new Date().getTime() ; 
	            iframe.contentWindow.postMessage(JSON.stringify({
			        t: "loadbundle" , 
                    domain : tracker_main.baseRelativeUrl , 
			        url : tracker_main.baseRelativeUrl + 'fileadmin/cbr/editor-bundle.js?' + time
				}), me.url);
			  break;  
			  case 'loadbundle-success' :  
			  	monoloopLog.log('Start init basket js'); 
			  	var iframe = document.getElementById("pw-browse") ;   
			  	iframe.contentWindow.postMessage(JSON.stringify({
			        t: "init-tracker-basket"  
				}), me.url); 
			  break  ;  
			  case 'tracker-basket-loaded' : 
			  	monoloopLog.log('Basket loaded');
                me.bodyPanel.setLoading(false);
                Ext.getCmp(me.id + '_local1').setDisabled(false);
			  break ; 
			  case 'tracker-basket-checkNext_callBack' : 
			  	me.checkNext_callBack(data.msg);
			  break ;
			  case 'tracker-basket-getDataObj_callBack' : 
			    me.getDataObj_callBack(data.dObj) ;
			  break ; 
	  		}
		};
		if (window.addEventListener) {
	        return addEventListener("message", me.postMessageListener, false);
		} else {
	        attachEvent("onmessage", me.postMessageListener); 
		}
    } , 
    
    initClientNav : function(){
        var me = this ; 
        me.callParent(arguments);  
  
    } , 
    
    getListeners : function(){
        var me = this ; 
        var obj = { 
            afterrender : function(w){  
                if( me.activeURL != ''){
                    Ext.getCmp('pw-url').setValue(me.activeURL) ; 
                    me.pushURL() ;
               	    me.processURL() ; 
                }else{
                    Ext.getCmp('pw-url').setValue(me.firstDomain) ; 
                    me.pushURL() ;
               	    me.processURL() ; 
                }    
                
                Ext.getCmp( me.id + '_local2_Back').setVisible(false) ; 
                Ext.getCmp( me.id + '_local2_Next').setVisible(false) ; 
                Ext.getCmp(  me.id + '_local1').setDisabled(true);
                
                monoloop_base.showMessage( me.step1Title , me.step1Text ) ; 
            } ,  
            beforeclose : function(w,eOpts){
            	Ext.util.History.add('');
            	if (window.addEventListener) {
			        return removeEventListener("message", me.postMessageListener, false);
				} else {
			        detachEvent("onmessage", me.postMessageListener); 
				} 
				
            } , 
             close : function(w){
                //me.resetActiveData() ;   
                //Ext.get( 'main-msg-div').destroy() ; 
                Ext.get('main-msg-div').dom.innerHTML = '' ;
             }
        } ; 
        return obj ; 
    } , 
    
    moveNext : function(){
        var me = this ;  
        var iframe = document.getElementById("pw-browse") ;  
        if(me.baseketStep == 7){
        	/*
            if( document.getElementById("pw-browse").contentWindow.monoloop_select_view != undefined){
                var dObj = document.getElementById("pw-browse").contentWindow.monoloop_select_view.getDataObj() ; 
                for (variable in dObj){
                    me.dataObj[variable] = dObj[variable] ; 
                }
                //me.dataObj
            } 
            me.processNext() ; 
            return ; 
            */
            iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-basket-getDataObj"  
			}), me.url);
			return ; 
        } 
        
        
        /*
        if( document.getElementById("pw-browse").contentWindow.monoloop_select_view != undefined){
            var msg = document.getElementById("pw-browse").contentWindow.monoloop_select_view.checkNext() ; 
            if( msg != '' ){
                Ext.MessageBox.alert('Fail',msg );
                return ;
            } 
        }
        */
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "tracker-basket-checkNext"  
		}), me.url);
        
    } ,
    
    getDataObj_callBack : function(dObj){
    	var me = this ; 
    	for (variable in dObj){
            me.dataObj[variable] = dObj[variable] ; 
        }
        me.processNext() ; 
    } , 
    
    checkNext_callBack : function(msg){
    	var me = this ;  
    	if( msg != '' ){
            Ext.MessageBox.alert('Fail',msg );
            return ;
        } 
    	
    	me.baseketStep = me.baseketStep + 1 ;   
        me.setBasketTitle() ; 
        me.setBasketStep() ; 
    } , 
    
    setBasketStep : function(){
        var me = this ; 
        /*
        if( document.getElementById("pw-browse").contentWindow.monoloop_select_view == undefined)
            return ; 
        document.getElementById("pw-browse").contentWindow.monoloop_select_view.setMode(me.baseketStep) ;
        */
        var iframe = document.getElementById("pw-browse") ;  
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "tracker-basket-setMode"   , 
	        baseketStep : me.baseketStep 
		}), me.url);
    } , 
    
    setBasketTitle : function(){
        var me = this ; 
        Ext.get('main-msg-div').dom.innerHTML = '' ;  
        
        if( me.baseketStep == 1 ){
            me.bodyPanel.setTitle(me.step1Title + ' : ' + me.step1Text) ; 
            monoloop_base.showMessage( me.step1Title , me.step1Text ) ; 
        }else if( me.baseketStep == 2 ){
            me.bodyPanel.setTitle(me.step2Title + ' : ' + me.step2Text) ; 
            monoloop_base.showMessage( me.step2Title , me.step2Text ) ; 
        }else if( me.baseketStep == 3){
            me.bodyPanel.setTitle(me.step3Title + ' : ' + me.step3Text) ; 
            monoloop_base.showMessage( me.step3Title , me.step3Text ) ; 
        }
        else if( me.baseketStep == 4){
            me.bodyPanel.setTitle(me.step4Title + ' : ' + me.step4Text) ; 
            monoloop_base.showMessage( me.step4Title , me.step4Text ) ; 
        }
        else if( me.baseketStep == 5){
            me.bodyPanel.setTitle(me.step5Title + ' : ' + me.step5Text) ;
            monoloop_base.showMessage( me.step5Title , me.step5Text ) ;  
        }
        else if( me.baseketStep == 6){
            me.bodyPanel.setTitle(me.step6Title + ' : ' + me.step6Text) ; 
            monoloop_base.showMessage( me.step6Title , me.step6Text ) ; 
        }
        else if( me.baseketStep == 7){
            me.bodyPanel.setTitle(me.step7Title + ' : ' + me.step7Text) ; 
            monoloop_base.showMessage( me.step7Title , me.step7Text ) ; 
        }
    } ,
    
    initBodyPanel : function(){
        var me = this ; 
        var contentHTML = "<iframe id='pw-browse' name='pw-browse' src ='' width='100%' height='100%' onLoad=\"Ext.getCmp('"+me.id+"').browserSrcChange();\" frameborder='0'><p>Your browser does not support iframes.</p></iframe>"  ;
        me.bodyPanel = Ext.create('Ext.panel.Panel' , {
            title : me.step1Title + ' : ' + me.step1Text , 
            region: 'center', 
            split: false, 
			margins:'0',
			border : false ,  
			bodyStyle : 'border:0;',			
            cmargins:'0',  
			html: contentHTML , 
            buttons: [{
                id : me.id + '_local2_Back' , 
                cls : 'monoloop-submit-form' , 
                text: 'BACK' , 
                handler: function(){
                    me.baseketStep = me.baseketStep - 1 ;
                    if( me.baseketStep == 1){ 
                        Ext.getCmp(me.id + '_local1').setVisible(true) ;
                        Ext.getCmp( me.id + '_local2_Back').setVisible(false) ; 
                        Ext.getCmp( me.id + '_local2_Next').setVisible(false) ;  
                        Ext.getCmp( me.id + '_nav').setVisible(true) ; 
                        
                    }
                    me.setBasketStep() ; 
                    me.setBasketTitle() ; 
                }
            },{
                id : me.id + '_local2_Next' , 
                text: 'NEXT' , 
                cls : 'monoloop-submit-form' , 
                handler: function(){
                    me.moveNext() ; 
                }
            },{
                id : me.id + '_local1' , 
                text: 'NEXT' , 
                cls : 'monoloop-submit-form' , 
                handler: function(){
                    me.baseketStep = 2 ; 
                    me.setDefaultData() ; 
                    me.activeURL =  Ext.getCmp('pw-url').getValue() ; 
                    Ext.getCmp(me.id + '_local1').setVisible(false);
                    Ext.getCmp( me.id + '_local2_Back').setVisible(true); 
                    Ext.getCmp( me.id + '_local2_Next').setVisible(true);  
                    Ext.getCmp( me.id + '_nav').setVisible(false) ; 
                    me.setBasketStep() ; 
                    me.setBasketTitle() ; 
                   
                }
            }]                   
        }) ; 
    } , 
    
    setDefaultData : function(){
        var me = this ; 
        /*
        if( document.getElementById("pw-browse").contentWindow.monoloop_select_view == undefined)
            return ; 
        
        document.getElementById("pw-browse").contentWindow.monoloop_select_view.setDefaultData(me.dataObj) ;
        */
        var iframe = document.getElementById("pw-browse") ;  
	  	iframe.contentWindow.postMessage(JSON.stringify({
	        t: "init-tracker-setDefaultData"  , 
	        data : me.dataObj
		}), me.url);
    } , 
    
    
    
    processURL : function(){ 
        //alert(pwMain.urlencode(Ext.getCmp('pw-url').getValue())) ; 
        var me = this ; 
        if( document.getElementById('pw-browse') != null){ 
            if( me.baseketStep == 1 ){
            	me.enable = false ; 
            	monoloopLog.log('Start render site');
                //document.getElementById('pw-browse').src = me.temPath + 'basket.php?l='+me.urlencode(Ext.getCmp('pw-url').getValue()) ;  
                var renderURL = me.processPosmessageURL(Ext.getCmp('pw-url').getValue(),'basket.php?l=')  ; 
            	monoloopLog.log('render : ' + renderURL);
                document.getElementById('pw-browse').src = renderURL ;  
                me.bodyPanel.setLoading('Loading Data');
            } 
        }  
    } , 
    
    browserSrcChange : function(){ 
    	var me = this ; 
    	monoloopLog.log('placement : browserSrcChange');
        
        if( document.URL.indexOf(document.getElementById('pw-browse').src) == -1 &&  me.enable == false ){
            // Change to proxy mode ;  
            // Wait 1 second to swith ; 
            setTimeout(function(){
            	if(me.enable == false){
            		monoloopLog.log('placement : Change to proxy mode');
            		me.bodyPanel.setLoading('Change to proxy mode');
            		var url = document.getElementById('pw-browse').src ; 
            		if( document.getElementById('pw-browse') != null){  
		                document.getElementById('pw-browse').src = me.temPath + 'basket.php?l='+me.urlencode(url) ; 
		                me.enable = true ;  
		                me.showProxyWarning() ; 
						me.addProxyList(url) ;  
		            }
		            monoloopLog.log('placement : add domain to proxy list - ' + url) ;  
            	}
			}, 2500);
            
            return ; 
         } 
    	
    	return ; 
        
        if( document.getElementById("pw-browse").contentWindow.previewView == undefined)
            return ; 
        var url = document.getElementById("pw-browse").contentWindow.previewView.getUrl()  ;
 
        Ext.getCmp('pw-url').setValue(url) ;   
    }  , 
    
    // Inteface .
    
    processNext : function(){
        
    } 
}) ; 