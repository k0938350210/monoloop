Ext.define('Tracker.view.tracker.Stage3RegexWindow', { 
    extend: 'Tracker.view.tracker.Stage3BaseWindow',
    alias: 'widget.trackerflowstage3regex',
    
    bodyPanel : null ,
    
    activeURL : '' , 
    activeTypeData : '' ,  
    activeFilterType : '' , 
    activePostUserFunc : '' , 
    activeSaveFunction : 0 , 
    activeExample : '' , 
    
    filterStore : null ,

    
    initComponent: function(){ 
        var me = this ;    
        me.preload() ; 
        me.title = 'TRACKER : Regular expression' ;
        me.closable = true;
        me.width = 370;
        me.height = 465;
        //me.id = 'mc-regex-window' , 
        /*
        me.baseCls = 'ces-window' ;
        me.iconCls = 'ces-window-header-icon' ;  
        */
        //border:false,
        me.modal = true;
        me.maximizable = false ; 
        me.resizable = false; 
        me.plain = true ;  
        me.buttons = [
        {
            text: 'CANCEL' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){ 
               me.close() ; 
        	}       
	    } , 
        {
            text: 'NEXT' , 
            cls : 'monoloop-submit-form' , 
            handler: function(){
               if(Ext.getCmp( me.id + '-form').getForm().isValid()){  
                   me.activeTypeData = Ext.getCmp('mc-regex-typedata').getValue() ; 
                   me.activeFilterType = Ext.getCmp(me.id + '-ft').getValue() ; 
                   me.activePostUserFunc = Ext.getCmp(me.id + '-post-user-func').getValue() ; 
                   me.activeSaveFunction = Ext.getCmp(me.id + '-savefunction').getValue() ; 
                   me.processNext() ;  
               }
            } 
        }
        ] ;   
        me.listeners = {
            'afterrender' : function(w){
                if( me.activeTypeData != ''){
                    Ext.getCmp('mc-regex-typedata').setValue(me.activeTypeData) ;  
                    //Ext.getCmp('mc-regex-testResult').setValue( document.getElementById("pw-browse").contentWindow.category_preview.regMatch( Ext.getCmp('mc-regex-typedata').getValue()))  ;
                    var iframe = document.getElementById("pw-browse") ;
    			    iframe.contentWindow.postMessage(JSON.stringify({
    				        t: "tracker-regMatch"   , 
    				        activeTypeData : Ext.getCmp('mc-regex-typedata').getValue() , 
                            setBack : true , 
                            setBackID : 'mc-regex-testResult'
    				}), me.activeURL); 
                }
                monoloop_base.showMessage('Step 3:' , 'Check that the tracker captures the information you want and apply filters if appropriate ') ;
            }
        } ; 
        
        me.initBodyPanel() ; 
        
        Ext.apply(me ,{   
            layout : 'fit' , 
            items :  [   me.bodyPanel ]
        }) ; 
        
        this.callParent(arguments); 
    } , 
    
    initBodyPanel : function(){
        var me = this ; 
        
        me.filterStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [ 
                {name : '--- None --- ',   value: 0 } , 
                {name : 'Custom filter',   value: 1 } 
            ]
        }) ; 
        
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
        width: '100%', 
        id: me.id + '-form', 
        border : false , 
        bodyBorder : false , 
        //bodyCls : 'cse-panel-body' , 
        bodyStyle: 'padding : 10px ;' , 
        autoScroll : true , 
        labelWidth: 100,
        defaults: {
            anchor: '95%',
            allowBlank: false,
            msgTarget: 'side'
        },
  
        items: [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    hidden : true ,
                    value : '<b>Example URL :</b>', 
                    style : { marginBottom: '0px' }
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    hidden : true ,
                    name : 'mc_regex[url]'  ,
                    id : 'mc-regex-url' ,  
                    allowBlank: false  , 
                    disabled  : false  , //--
                    value : me.activeURL   
                }, 
                {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<b>Regular expression :</b>', 
                    style : { marginBottom: '0px' }
                },{
                    xtype: 'textarea', 
                    width: 320,
                    hideLabel : true ,  
                    height: 100,
                    id : 'mc-regex-typedata'  
                },{
                    xtype: 'button',  
                    cls : 'monoloop-submit-form' , 
                    id : 'mc-regex-testbtn'  ,  
                    style : 'margin-right : 200px' , 
                    text : 'Test' , 
                    hidden : true ,
    	            handler : function(){ 
    	               //Ext.getCmp('mc-regex-testResult').setValue( document.getElementById("pw-browse").contentWindow.category_preview.regMatch( Ext.getCmp('mc-regex-typedata').getValue()))  ; 
                       var iframe = document.getElementById("pw-browse") ;
        			    iframe.contentWindow.postMessage(JSON.stringify({
        				        t: "tracker-regMatch"   , 
        				        activeTypeData : Ext.getCmp('mc-regex-typedata').getValue() , 
                                setBack : true , 
                                setBackID : 'mc-regex-testResult'
        				}), me.activeURL); 
    	        	} 
                },{
                    xtype: 'textarea', 
                    width: 320,
                    hideLabel : true ,  
                    height: 100,
                    disabled  : false  , //--
                    allowBlank : true ,
                    hidden : true ,
                    id : 'mc-regex-testResult'  
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<hr><b>Filter Type :</b>', 
                    style : { marginBottom: '0px' }
                } ,{
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: true  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : me.id+ '-ft'  ,  
                    displayField:   'name',
                    valueField:     'value' ,
                    emptyText : 'Please select filter type' , 
                    store:   me.filterStore , 
                    listeners : {
                        'afterrender' : function( combo , eObj){
                            if( me.activeFilterType == '' || me.activeFilterType == 0 || me.activeFilterType == null){
                                combo.setValue(0) ; 
                            }else{
                                combo.setValue(me.activeFilterType) ; 
                            }  
                        },
                        'change' : function( combo , newVal , oldVal , eObj ){
                            if( newVal > 0){
                                Ext.getCmp(me.id + '-post-user-func').setVisible(true) ; 
                            }else{
                                Ext.getCmp(me.id + '-post-user-func').setVisible(false) ; 
                            } 
                            Ext.getCmp( me.id + '-ft-btn').fireEvent('click') ; 
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-post-user-func' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){  
                            if( me.activePostUserFunc == undefined || me.activePostUserFunc == ''){
                                textArea.setValue('return result ; ') ; 
                            }else{
                                textArea.setValue(me.activePostUserFunc) ; 
                            }
                        }
                    }
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    hideLabel : true ,
                    id : me.id + '-ft-btn' , 
                    style : 'margin-right : 200px' , 
                    text : 'test'  , 
                    listeners : {
                        'click' : function(button , e , eOpt){
                            var iframe = document.getElementById("pw-browse") ;
                            if( Ext.getCmp(me.id + '-ft').getValue() == 0){ 
                                //Ext.getCmp( me.id + '-post-user-ret').setValue( document.getElementById("pw-browse").contentWindow.category_preview.regMatch( Ext.getCmp('mc-regex-typedata').getValue()) ) ; 
                                
                			    iframe.contentWindow.postMessage(JSON.stringify({
                				        t: "tracker-regMatch"   , 
                				        activeTypeData : Ext.getCmp('mc-regex-typedata').getValue() , 
                                        setBack : true , 
                                        setBackID : me.id + '-post-user-ret'
                				}), me.activeURL); 
                            }else{
                                var ret = document.getElementById("pw-browse").contentWindow.category_preview.regMatch( Ext.getCmp('mc-regex-typedata').getValue()) ; 
                                var customString = ' var result  = \''+ret+'\' ; ' ; 
                                customString += ' ' + Ext.getCmp(me.id + '-post-user-func').getValue() ; 
                                
                                //Ext.getCmp( me.id + '-post-user-ret' ).setValue(document.getElementById("pw-browse").contentWindow.category_preview.getRegMatchPostFunc(customString)) ;
                                
                                iframe.contentWindow.postMessage(JSON.stringify({
                				        t: "tracker-getRegMatchPostFunc"   , 
                				        activeTypeData : customString , 
                                        setBack : true , 
                                        setBackID : me.id + '-post-user-ret'
                				}), me.activeURL); 
                            }
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-post-user-ret'  , 
                    allowBlank : true  
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<hr><b>Save Function :</b>', 
                    hidden : me.hideSaveFunction , 
                    style : { marginBottom: '0px' }
                },{
                    xtype: 'combo',
                    mode: 'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,  
                    displayField:   'name',
                    valueField:     'value' , 
                    emptyText : 'Please select' , 
                    id : me.id + '-savefunction' , 
                    store:  me.saveFunctionStore , 
                    allowBlank :  me.hideSaveFunction , 
                    hidden : me.hideSaveFunction  , 
                    listeners : {
                    	afterrender : function(cb){
                    		if(me.activeSaveFunction == 0 || me.activeSaveFunction == null)
                    			return ; 
                   			cb.setValue(me.activeSaveFunction) ; 
                    	}
                    } 
                }
				]

	    });
    } , 
    
    // Inteface ; 
    
    processNext : function(){
        
    }
}) ; 