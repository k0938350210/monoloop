Ext.define('Tracker.view.tracker.Stage3XpathWindow', { 
    extend: 'Tracker.view.tracker.Stage3BaseWindow',
    alias: 'widget.trackerflowstage3xpath',
    
    bodyPanel : null ,
    
    activeURL : '' , 
    activeTypeData : '' , 
    activeFilterType : '' , 
    activePostUserFunc : '' , 
    activeSaveFunction : 0 , 
    activeExample : '' , 
    
    filterStore : null , 
    
    initComponent: function(){ 
        var me = this ; 
        me.preload()  ; 
        me.title = 'TRACKER' ; 
        me.closable = true ;
        me.width = 370 ; 
        me.height = 355 ;  
        //me.baseCls = 'ces-window' ;
        //me.iconCls = 'ces-window-header-icon' ;  
        //border:false,
        me.modal = true ;
        me.maximizable = false  ; 
        me.resizable = true ; 
        me.plain = true ; 
        me.buttons = [
        {
            text: 'CANCEL' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){ 
               me.close() ; 
               //Ext.get('main-msg-div').dom.innerHTML = '' ; 
        	}       
	    } , 
        {
            text: 'NEXT' , 
            cls : 'monoloop-submit-form' , 
            handler: function(){
               if(Ext.getCmp( me.id + '-form').getForm().isValid()){  
                   me.activeTypeData = Ext.getCmp('mc-xpath-typedata').getValue() ; 
                   me.activeFilterType = Ext.getCmp(me.id + '-ft').getValue() ; 
                   me.activePostUserFunc = Ext.getCmp(me.id + '-post-user-func').getValue() ;  
                   me.activeSaveFunction = Ext.getCmp(me.id + '-savefunction').getValue() ; 
                   me.activeExample = Ext.getCmp( me.id + '-post-user-ret').getRawValue() ; 
                   me.processNext() ; 
               }
            }

        }
        ] ; 
        me.listeners = {
            'afterrender' : function(w){
                if( me.activeTypeData != ''){
                    Ext.getCmp('mc-xpath-typedata').setValue(me.activeTypeData) ;  
                    // Process sample data ; 
                    //Ext.getCmp('mc-xpath-testResult').setValue(document.getElementById("pw-browse").contentWindow.monoloop_select_view.getXPathContent(me.activeTypeData)) ; 
                    var iframe = document.getElementById("pw-browse") ;
    			    iframe.contentWindow.postMessage(JSON.stringify({
    				        t: "tracker-getXPathContent"   , 
    				        activeTypeData : me.activeTypeData , 
                            setBack : true , 
                            setBackID : 'mc-xpath-testResult'
    				}), me.activeURL); 
                    
                } 
                if( me.activeFilterType == '' || me.activeFilterType == 0 || me.activeFilterType == null || isNaN(me.activeFilterType)){ 
                     Ext.getCmp( me.id+ '-ft' ).setValue(0) ; 
                }else{ 
                    Ext.getCmp( me.id+ '-ft' ).setValue( parseInt ( me.activeFilterType )  ) ; 
                }  
                
                monoloop_base.showMessage('Step 3:' , 'Check that the tracker captures the information you want and apply filters if appropriate ') ;
            } , 
            'beforeclose' : function(p , eOpts){ 
            }
         } ; 
        me.initBodyPanel()  ; 
        
        Ext.apply(me ,{   
            layout: 'fit' ,  
            items :  [   me.bodyPanel ]
        }) ; 
        
       
        
        this.callParent(arguments); 
    } , 
     
    initBodyPanel : function(){
        var me = this ; 
        
        me.filterStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [ 
                {name : '--- None --- ',   value: 0 } , 
                {name : 'Price filter' , value: 2 } , 
                {name : 'Image filter' , value: 3 } , 
                {name : 'Custom filter',   value: 1 } 
            ]
        }) ; 
        
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
        width: '100%', 
        id: me.id + '-form', 
        border : false , 
        bodyBorder : false , 
        //bodyCls : 'cse-panel-body' , 
        bodyStyle: 'padding : 10px ;' , 
        labelWidth: 100,
        autoScroll : true , 
        defaults: {
            anchor: '100%',
            allowBlank: false,
            msgTarget: 'side'
        },
  
        items: [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    hidden : true ,
                    value : '<b>Example URL :</b>'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    hidden : true ,
                    name : 'xpath[url]'  ,
                    id : 'mc-xpath-url' ,  
                    allowBlank: false  , 
                    disabled  : false  , //--
                    value : me.activeURL   
                }, 
                {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<b>X-Path :</b>', 
                    style : { marginBottom: '0px' }
                },{
                    xtype: 'textfield', 
                    hideLabel : true ,   
                    allowBlank: false  , 
                    id : 'mc-xpath-typedata' , 
                    value : me.activeTypeData 
                },{
                    xtype: 'button',  
                    cls : 'monoloop-submit-form' , 
                    id : 'mc-xpath-testbtn'  ,  
                    style : 'margin-right : 200px' , 
                    text : 'Test' , 
                    hidden : true , 
    	            handler : function(){ 
    	                //Ext.getCmp('mc-xpath-testResult').setValue(document.getElementById("pw-browse").contentWindow.monoloop_select_view.getXPathContent(me.activeTypeData)) ;
                        var iframe = document.getElementById("pw-browse") ;
        			    iframe.contentWindow.postMessage(JSON.stringify({
        				        t: "tracker-getXPathContent"   , 
        				        activeTypeData : me.activeTypeData , 
                                setBack : true , 
                                setBackID : 'mc-xpath-testResult'
        				}), me.activeURL); 
    	        	}
                }, 
                {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    hidden : true , 
                    value : '<b>Test Result :</b>', 
                    style : { marginBottom: '0px' }
                },{
                    xtype: 'textarea', 
                    width: 320,
                    hideLabel : true ,  
                    hidden : true , 
                    height: 100,
                    allowBlank : true , 
                    disabled  : false  , //--
                    id : 'mc-xpath-testResult'  
                } , 
                {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<hr><b>Filter Type :</b>', 
                    style : { marginBottom: '0px' }
                },{
                    xtype:          'combo',
                    queryMode: 'local',
                    hideLabel : true , 
                    allowBlank: true  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : me.id+ '-ft'  ,  
                    displayField:   'name',
                    valueField:     'value' ,
                    emptyText : 'Please select filter type' , 
                    store:   me.filterStore , 
                    listeners : {
                        
                        'change' : function( combo , newVal , oldVal , eObj ){
                            if( newVal == 1){
                                Ext.getCmp(me.id + '-post-user-func').setVisible(true) ; 
                            }else{
                                Ext.getCmp(me.id + '-post-user-func').setVisible(false) ; 
                            } 
                            Ext.getCmp( me.id + '-ft-btn').fireEvent('click') ; 
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-post-user-func' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){  
                            if( me.activePostUserFunc == undefined || me.activePostUserFunc == ''){
                                textArea.setValue('return MONOloop.html(MONOloop.jq(selector)); ') ; 
                            }else{
                                textArea.setValue(me.activePostUserFunc) ; 
                            }
                        }
                    }
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    hideLabel : true ,
                    id : me.id + '-ft-btn' , 
                    text : 'test'  , 
                    listeners : {
                        'click' : function(button , e , eOpt){
                            var iframe = document.getElementById("pw-browse") ;
                            if( Ext.getCmp(me.id + '-ft').getValue() == 0){
                                //Ext.getCmp( me.id + '-post-user-ret' ).setValue(document.getElementById("pw-browse").contentWindow.monoloop_select_view.getXPathContent(Ext.getCmp('mc-xpath-typedata').getValue())) ; 
                			    iframe.contentWindow.postMessage(JSON.stringify({
                				        t: "tracker-getXPathContent"   , 
                				        activeTypeData : Ext.getCmp('mc-xpath-typedata').getValue() , 
                                        setBack : true , 
                                        setBackID : me.id + '-post-user-ret'
                				}), me.activeURL); 
                            }else if( Ext.getCmp(me.id + '-ft').getValue() == 2 ){
                                //Ext.getCmp( me.id + '-post-user-ret' ).setValue(document.getElementById("pw-browse").contentWindow.monoloop_select_view.getXPathPrice(Ext.getCmp('mc-xpath-typedata').getValue())) ;
                                iframe.contentWindow.postMessage(JSON.stringify({
                				        t: "tracker-getXPathPrice"   , 
                				        activeTypeData : Ext.getCmp('mc-xpath-typedata').getValue() , 
                                        setBack : true , 
                                        setBackID : me.id + '-post-user-ret'
                				}), me.activeURL); 
                            }else if( Ext.getCmp(me.id + '-ft').getValue() == 3 ){
                                //Ext.getCmp( me.id + '-post-user-ret' ).setValue(document.getElementById("pw-browse").contentWindow.monoloop_select_view.getXPathImageSrc(Ext.getCmp('mc-xpath-typedata').getValue())) ;
                                iframe.contentWindow.postMessage(JSON.stringify({
                				        t: "tracker-getXPathImageSrc"   , 
                				        activeTypeData : Ext.getCmp('mc-xpath-typedata').getValue() , 
                                        setBack : true , 
                                        setBackID : me.id + '-post-user-ret'
                				}), me.activeURL); 
                            }else{
                                var customString = ' var selector = \''+Ext.getCmp('mc-xpath-typedata').getValue()+'\' ; ' ;
                                customString += ' ' + Ext.getCmp(me.id + '-post-user-func').getValue() ;  
                                //Ext.getCmp( me.id + '-post-user-ret' ).setValue(document.getElementById("pw-browse").contentWindow.monoloop_select_view.getXPathPostFunc(customString)) ;
                                iframe.contentWindow.postMessage(JSON.stringify({
                				        t: "tracker-getXPathPostFunc"   , 
                				        activeTypeData : customString , 
                                        setBack : true , 
                                        setBackID : me.id + '-post-user-ret'
                				}), me.activeURL); 
                            }
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-post-user-ret'  , 
                    allowBlank : true  
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<hr><b>Save Function :</b>', 
                    hidden : me.hideSaveFunction ,
                    style : { marginBottom: '0px' }
                },{
                    xtype: 'combo',
                    mode: 'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,  
                    displayField:   'name',
                    valueField:     'value' , 
                    emptyText : 'Please select' , 
                    id : me.id + '-savefunction' , 
                    store:  me.saveFunctionStore , 
                    hidden : me.hideSaveFunction , 
                    allowBlank :  me.hideSaveFunction , 
                    listeners : {
                    	afterrender : function(cb){
                    		if(me.activeSaveFunction == 0 || me.activeSaveFunction == null)
                    			return ; 
                   			cb.setValue(me.activeSaveFunction) ; 
                    	}
                    }
                }
				]

	    });
    } , 
    
    // Interface ; 
    
    processNext : function(){
        
    }
}) ; 