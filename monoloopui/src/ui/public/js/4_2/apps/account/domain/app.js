var DomainApp = null ; 

Ext.onReady(function() { 
	 
	
	DomainApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Domain', 
	  	controllers: ['Domain'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('Domain.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        });
	        
	        Ext.create('Ext.button.Button', { 

				id : 'add-new-domain' , 
				cls : 'monoloop-submit-form' , 
				text : 'CREATE NEW DOMAIN' , 
	        	renderTo: Ext.get('create-btn') 	        		        	
	        });
	        
	        Ext.create('Ext.button.Button', { 

				id : 'test-domain' , 
				cls : 'monoloop-submit-form' , 
				text : 'TEST DOMAINS' , 
	        	renderTo: Ext.get('create-test-btn') 	        		        	
	        });
	        router.name('domainNew', 'domain/new', {controller: 'Domain', action: 'newdomain'});
 			router.name('domainEdit', 'domain/:id', {controller: 'Domain', action: 'edit'});
 			
	    }
	    
	});
}) ; 