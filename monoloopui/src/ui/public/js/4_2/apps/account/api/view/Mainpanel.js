Ext.define('Api.view.Mainpanel', {
	alias: 'widget.apimainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	bodyPadding: 20, 
	
 	requires : ['UI.shared.view.form.CopyButton'] , 
    
    initComponent: function() {
    	var me = this ; 
        Ext.apply(this, {
        	title : 'API Token Overview' , 
        	border : false ,  
            items: [{
            	xtype : 'displayfield' , 
            	fieldLabel : 'Host' , 
            	id : me.id + '-' + 'host' 
            	//value : '302invoke.monoloop.com/api/'
            },{
            	xtype : 'textfield' , 
            	fieldLabel : 'Account ID' , 
            	width : 280 , 
            	id : me.id + '-' + 'cid' 
            },{
            	xtype : 'container' , 
            	layout : 'column' , 
            	style : 'padding : 15px 0 15px 0 ;' , 
            	items : [{
            		xtype : 'copybutton' , 
            		cls : 'monoloop-submit-form' , 
            		text : 'Copy API Token' , 
            		getValue : function(){
            			return Ext.getCmp(me.id + '-token').getValue() ;	
            		} ,
            		width : 150
            	},{
            		xtype : 'displayfield' , 
            		value : '&nbsp' , 
            		width : 10
            	},{
            		xtype : 'displayfield' , 
            		value : 'Copy the token below and use it as your password'
            	}]
            },{
            	xtype : 'textarea' , 
            	height : 200 , 
            	id : me.id + '-' + 'token' , 
            	width : '50%'
            },{
            	xtype : 'displayfield' , 
            	value : 'NOTE: For older browsers you may need to copy the code manually.'
            }] 
        });
        this.callParent();
    }
}) ; 