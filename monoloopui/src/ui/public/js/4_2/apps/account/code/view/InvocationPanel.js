Ext.define('Codes.view.InvocationPanel', {
    alias: 'widget.invocationpanel',
    extend: 'Ext.panel.Panel',
    xtype : 'invocationpanel', 
    
    requires : ['UI.shared.view.form.CopyButton'] , 
    
    initComponent: function() {
        var me = this ; 
        Ext.apply(this, { 
        	layout : 'vbox' , 
        	bodyPadding: 15, 
        	items : [{
        		xtype : 'container' , 
        		layout : 'column' , 
        		width : '100%' , 
        		items : [{
        			xtype : 'copybutton' , 
        			cls : 'monoloop-submit-form' , 
        			getValue : function(){
            			return Ext.getCmp( 'invocation-code-hidden').getValue() ;	
            		} ,
        			text : 'Copy Invocation Code' , 
        			columnWidth : 0.1 
        		},{
        			xtype : 'displayfield' , 
        			value : '&nbsp;' , 
        			columnWidth : 0.025
        		},{
        			xtype : 'displayfield' , 
        			value : 'Copy the code below and paste it into your HTML' , 
        			columnWidth : 0.85 
        		}]
        	},{
        		xtype : 'container' , 
        		layout : 'column' , 
        		width : '100%' , 
				style : 'padding : 10px 0 15px 0;' ,
        		items : [{
        			xtype : 'container' ,
        			cls : 'ml-box' , 
        			height : 200 ,  
					columnWidth : 0.49 , 
					style : 'padding : 0 10px ;' , 
        			id : 'invocation-code'
        		},{
        			xtype : 'hidden' , 
        			id : 'invocation-code-hidden'
        		},{
        			xtype : 'displayfield' , 
        			value : '&nbsp;' , 
        			columnWidth : 0.02
        		},{
        			xtype : 'container' ,  
        			cls : 'ml-box' , 
        			height : 200,  
        			columnWidth : 0.49 , 
        			style : 'padding : 10px 20px ;' ,   
        			items : [{
        				xtype : 'displayfield' ,  
        				value : '<strong>Injection method</strong>' , 
                        style : 'margin : 0 '
        			},{
        				xtype : 'radiogroup' ,  
                        id : 'instanttype' , 
                        columns : 3 , 
                        width : '100%' , 
                        style : 'margin : 0 ' ,
                        padding : 0 , 
                        bodyPadding: 0,
        				layout : 'column' ,
        				items : [{
        					xtype : 'radio' , 
        					boxLabel : '<span title="Inject when Monoloop script is loaded" >Instantly <i class="icon-info-sign"></i></span> '  , 
        					name : 'instant', 
        					inputValue : 1, 
        					width : 100 , 
                            listeners : {
                                change : me.instantChange 
                            }
        				},{
        					xtype : 'radio' , 
        					boxLabel : '<span title="Wait for Page load then Inject">Fast <i class="icon-info-sign"></i></span>'  , 
        					name : 'instant',
        					inputValue : 0 , 
        					width : 80, 
                            listeners : {
                                change : me.instantChange 
                            }
        				},{
        					xtype : 'radio' , 
        					boxLabel : '<span title="Wait for Page ready then Inject">Safe <i class="icon-info-sign"></i></span>' , 
        					name : 'instant',
        					inputValue : 2 , 
        					width : 80, 
                            qtip : 'Wait for Page ready then Inject' , 
                            listeners : {
                                change : me.instantChange  
                            }
        				}]
        			},{
        				xtype : 'displayfield' ,  
        				value : '<strong>Anti.flicker</strong>' , 
                        style : 'margin : 0 '
        			},{
        				xtype : 'radiogroup' ,  
        				columns : 2 , 
                        width : '100%' , 
                        padding : 0 , 
                        bodyPadding: 0,
                        style : 'margin : 0 ' , 
        				layout : 'column' ,
        				items : [{
        					xtype : 'radio' , 
        					id : 'antiflicker-on' , 
        					boxLabel : '<span>On</span>' , 
        					name : 'antiflicker',
        					inputValue : 1 , 
        					width : 50
        				},{
        					xtype : 'radio' , 
        					boxLabel : '<span>Off</span>' , 
        					name : 'antiflicker',
        					inputValue : 0 , 
        					width : 50
        				} ]
        			},/*{
        				xtype : 'displayfield' , 
        				value : 'Displays page content as it is being served by your site and Monoloop<br/><br/>'
        			},*/{
        				xtype : 'displayfield' ,  
        				value : '<strong>Anti-flicker Timeout</strong>' , 
                        style : 'margin : 0 '
        			},{
        				xtype : 'container' ,  
        				layout : 'column' ,
        				items : [{
        					xtype : 'numberfield' ,  
        					name : 'timeout', 
        					hideTrigger : true,
        					width : 90
        				},{
        					xtype : 'displayfield' , 
        					value : '<strong>&nbsp;&nbsp;&nbsp;MS&nbsp;&nbsp;&nbsp;Timeout</strong>' , 
        					flex : 1 
        				}]
        			},{
        				xtype : 'displayfield' , 
        				value : 'Maximum milliseconds to wait for Monoloop content injection (if service fails).'
        			}]
        		}]
        	},{
        		xtype : 'displayfield' , 
        		value : 'NOTE: For older browsers you may need to copy and paste the code manually'
        	}]   
        });        
        this.callParent(arguments);
    } , 
    
    instantChange : function(r , newV , oldV){
        /*
        var me = this ; 
        var instant_text = r.up('radiogroup').next('displayfield') ; 
        //console.debug(instant_text) ; 
        if( newV == true){
            if( r.inputValue == 1){
                instant_text.setValue('Inject when Monoloop script is loaded<br/><br/>') ; 
            }else if(r.inputValue == 0){
                instant_text.setValue('Wait for Page load then Inject<br/><br/>') ; 
            }else{
                instant_text.setValue('Wait for Page ready then Inject<br/><br/>') ; 
            }
        }*/
    }
}) ; 