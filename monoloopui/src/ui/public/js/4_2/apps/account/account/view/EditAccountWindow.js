Ext.define('Account.view.EditAccountWindow', { 
    extend: 'Ext.window.Window',
    alias: 'widget.editaccountwindow',
    xtype : 'editaccountwindow', 
    
    uid : 0 , 
    
    initComponent: function() {
    	var me = this ; 
    	Ext.apply(this, { 
    		closable:true,
            width:400,
            height:300,
    		modal : true ,
    		items : [{
    			xtype : 'form' , 
    			id : me.id + '-form' ,
    			border : false , 
    			bodyPadding: 15 ,  
				layout: 'anchor',
			    defaults: {
			        anchor: '100%'
			    },
			    items : [{
			    	xtype : 'textfield' , 
			    	allowBlank: false  , 
			    	name : 'data[name]' , 
			    	id : me.id + '-name' , 
			    	fieldLabel : 'Account Name *' , 
			    	tooltip: 'The name of the account, used to login to the Monoloop interface',
			    	listeners : {
						render: function(p) {
							p.getEl().down('input').set({'data-qtip': p.tooltip});
						}
					}
			    },{
			    	xtype : 'textfield' , 
			    	allowBlank: false  , 
			    	name : 'data[company]' , 
			    	id : me.id + '-company' , 
			    	fieldLabel : 'Company *' , 
			    	tooltip: 'The name of the Clients legal entity',
			    	listeners : {
						render: function(p) {
							p.getEl().down('input').set({'data-qtip': p.tooltip});
						}
					}
			    },{
			    	xtype : 'textfield' , 
			    	allowBlank: false  , 
			    	name : 'data[contactName]' , 
			    	id : me.id + '-contactname' , 
			    	fieldLabel : 'Contact Name *' , 
			    	tooltip: 'The name of the responsible person at the Client (who enters the terms and conditions)',
			    	listeners : {
						render: function(p) {
							p.getEl().down('input').set({'data-qtip': p.tooltip});
						}
					}
			    },{
			    	xtype : 'textfield' , 
			    	allowBlank: false  , 
			    	name : 'data[contactEmail]' , 
			    	id : me.id + '-contactemail' , 
			    	fieldLabel : 'Contact Email *' , 
			    	tooltip: 'The email of the responsible person at the Client.',
			    	listeners : {
						render: function(p) {
							p.getEl().down('input').set({'data-qtip': p.tooltip});
						}
					}
			    },{
	                xtype : 'checkbox' , 
	                fieldLabel : 'Invoice trough agency' ,  
	                labelWidth : '100px' , 
	                inputValue : true , 
	                id : me.id + '-invoicereseller' , 
	                name : 'data[isReseller]' ,  
	                listeners : {
	                    'change' : function(chkBox , newValue, oldValue, eOpts){
	                        if( newValue ){ 
	                            chkBox.nextSibling('combo').setVisible(true) ; 
	                        }else{ 
	                            chkBox.nextSibling('combo').setVisible(false) ;
	                        }  
	                    } 
	                }
	            },{
                    xtype: 'combo',
                    store : Ext.create('UI.shared.store.account.Type') , 
                    queryMode: 'remote',
                    hidden : true ,
                    displayField: 'name',
    				valueField: 'uid',
                    fieldLabel : 'Account Type*' ,  
					name : 'data[acid]' , 
					id : me.id + '-acid' ,  
                	emptyText : 'Please select account type' , 
                	tooltip: 'The Account type that will be active after the Trial.' , 
                	listeners : {
						render: function(p) {
							p.getEl().set({'data-qtip': p.tooltip});
						}
					}
                },{
                	xtype : 'hiddenfield' , 
                	name : 'data[uid]' , 
                	value : me.uid 
                }]
    		}]  , 
    		buttons : [
    			{
    				'text' : 'Save' ,
    				'f' : me.id + '-form' ,
    				cls : 'monoloop-submit-form' , 
    				'id' : 'save-edituserwindow-btn'
    			}
    		]
   		}) ; 
    	this.callParent(arguments); 
   	}
}) ; 