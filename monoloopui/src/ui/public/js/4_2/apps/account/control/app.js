var ControlApp = null ; 

Ext.onReady(function() { 
	 
	
	ControlApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Control', 
	  	controllers: ['Control'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('Control.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        }); 
	    }
	    
	});
}) ; 