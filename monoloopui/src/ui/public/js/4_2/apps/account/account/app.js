var AccountApp = null ; 

Ext.onReady(function() { 
	 
	
	AccountApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Account', 
	  	controllers: ['Account'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('Account.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        }); 
	        
	        Ext.create('Ext.button.Button', { 

				id : 'add-new-account' , 
				cls : 'monoloop-submit-form' , 
				text : 'CREATE NEW ACCOUNT' , 
	        	renderTo: Ext.get('create-btn') 	        		        	
	        });
	        
	        router.name('accountNew', 'account/new', {controller: 'Account', action: 'newaccount'});
	        router.name('accountEdit', 'account/:id', {controller: 'Account', action: 'edit'});
	        
	    }
	    
	});
}) ; 