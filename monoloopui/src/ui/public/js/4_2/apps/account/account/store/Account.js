Ext.define('Account.store.Account', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: 'uid', type: 'int'},
        {name : 'hidden' , type : 'int'} , 
        {name : 'type_uid' , type : 'int'} , 
    	{name: 'name'} ,  
        {name: 'contact_name' } , 
        {name: 'contact_email' } , 
        {name : 'isReseller' , type : 'int'} , 
        {name : 'invoiceReseller' , type : 'int'} , 
        {name : 'lockAccountType' , type : 'int'} , 
        {name : 'autoActivateAfterTrial' , type : 'int'} , 
        {name : 'company'} , {name : 'type_name'}
    ] , 
    remoteSort: true,
    pageSize : 10 ,   
    proxy: {
        type: 'ajax',
        url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/loadAccountList' , 
        reader: {
            idProperty  : 'uid' , 
            type: 'json',
            root: 'topics', 
            totalProperty: 'totalCount'  
        } , 
        actionMethods: {
            read: 'POST'
        }
    } , 
    sorters: [{
        property: 'company',
        direction: 'ASC'
    }] , 
    autoLoad : true 
    
});