Ext.define('Datapush.view.OptionWindow', {
	alias: 'widget.optionwindow',
	extend: 'Ext.window.Window', 
	height: 340, 
	width : 400,
	
	fieldName : '' , 
	fieldType : 'Text' , 
	refFieldSet : null , 
	 
	
	initComponent: function() { 
		var me = this ; 
		
		var fieldStore = Ext.create('Ext.data.Store', {
            fields: ['name'],
            data : [ 
                {name : 'Text' } , 
                {name : 'Number' } , 
                {name : 'Boolean' } , 
                {name : 'Image' }
            ]
        }) ;
        
        var fieldNameStore = Ext.create('Ext.data.Store', {
            fields: ['name','label'],
            proxy: {
                type: 'ajax',
                url : 'index.php/profilefield?eID=ml_application',
                reader: { 
                    type: 'json',
                    root: 'topics', 
                    totalProperty: 'totalCount' 
                    
                } , 
                actionMethods: {
                    read: 'POST'
                } , 
                pageParam: undefined , 
                limitParam: undefined
            } 
        }) ; 
        
		Ext.apply(this, { 
			modal : true , 
			items : [{
				xtype : 'form' , 
				id: me.id + '-form',
	            border : false , 
	            bodyBorder : false ,  
	            bodyStyle: 'padding : 15px ;' ,
	            defaults: {
	                anchor: '90%',
	                allowBlank: false ,
	                msgTarget: 'side'
	            },  
	            items: [{
	                xtype : 'displayfield' , 
	                value : '<b>Profile field</b>'
	            },{
	                xtype: 'combo',
	                mode:           'local',
		            hideLabel : true , 
		            allowBlank: true  ,
		            triggerAction:  'all',
		            editable:       false,
		            id : me.id + '-field' ,
		            name : 'name' ,
		            displayField:   'label',
		            valueField:     'name' , 
		            emptyText : 'Please select profile field' , 
		            store : fieldNameStore , 
		            listeners : {
		            	'afterrender' : function(cb){
		            		if( me.fieldName != ''){
		            			cb.store.load(function(){
		            				cb.setValue(me.fieldName) ; 
		            			}) ; 
		            		}
		            	}
		            }
	            }/*,{
	                xtype : 'displayfield' , 
	                value : '<b>Field type</b>'
	            },{
	                xtype: 'combo',
	                mode: 'local',
	                hideLabel : true , 
	                allowBlank: false  ,
	                triggerAction: 'all',
	                editable: false,
	                id : me.id+ '-type'  ,  
	                displayField: 'name',
	                valueField: 'name' ,
	                emptyText : 'Please select type' , 
	                store: fieldStore , 
	                value : me.fieldType  
	            }*/ ]  
    		}] , 
            
            buttons:[{
	            text : 'SAVE' , 
	            cls : 'monoloop-submit-form' 
	        },{
	            text : 'CANCEL' , 
	            cls : 'monoloop-submit-form'  
	        }]
		}) ; 
		this.callParent(arguments); 
	}
}) ; 