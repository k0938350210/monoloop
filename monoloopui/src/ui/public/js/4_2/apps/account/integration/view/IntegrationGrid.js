Ext.define('Integration.view.IntegrationGrid', { 
    extend: 'Ext.grid.Panel',
    alias: 'widget.integrationgrid',
    xtype : 'integrationgrid', 
    
    initComponent: function() {
    	var me = this ; 
    	var store = Ext.create('Integration.store.Integration') ; 
    	
    	Ext.apply(this, { 
    		disableSelection: true,
    		store : store , 
    		columns: [
              {header:'Plugin Name',width : 200 , dataIndex: 'name', sortable:true , flex : 1 }, 
              /* 
              {header: 'Action' , dataIndex : 'uid' ,  sortable : false , renderer : function(value, p, r){
              		var edit = '<td><div onClick="Ext.getCmp(\''+me.id+'\').editUser(\''+r.data['uid']+'\')" title="Edit" class="placement-property"></div></td>' ;
		            var deleteData = '<td><div onClick="Ext.getCmp(\''+me.id+'\').deleteUser(\''+r.data['uid']+'\')" title="Delete" class="placement-delete"></div></td>' ; 
			    	var ret =  '<table width="100%" ><tr align="center"> '  
		                         + edit  
			    			     + deleteData
		                         +
							   '</tr></table>'  ; 
					return ret ;   
              }  }*/ 
              {header: 'Status' , dataIndex : 'enabled' , sortable : true , renderer : function(value, p, r){
              		var hidden = '' ; 
		            if( r.data['enabled'] == true ){
						hidden = '<td><div onClick="Ext.getCmp(\''+me.id+'\').setHidden(\''+r.data['_id']+'\' , 0)"  class="placement-active"></div></td>' ; 
					}else{
						hidden = '<td><div onClick="Ext.getCmp(\''+me.id+'\').setHidden(\''+r.data['_id']+'\' , 1)"   class="placement-inactive" ></div></td>' ; 
					}
		            	var ret =  '<table width="100%" ><tr align="center"> ' 
								 + hidden +
							   '</tr></table>'  ; 
					return ret ;	
              }	}
           ] , 
		   bbar: Ext.create('Ext.PagingToolbar', {
	            store: store,
	            displayInfo: true,
	            displayMsg: 'Displaying  {0} - {1} of {2}',
	            emptyMsg: "No data to display",
	            items:[]
	        }) 
   		}) ; 
   		
   		this.callParent(arguments); 
   	} , 
   	
   	editUser : function(uid){
   		alert(1) ; 
   	} , 
   	
   	deleteUser : function(uid){
   		alert(2) ; 
   	} , 
   	
   	setHidden : function(uid , status){
   		var me = this ; 
   		//console.debug('set hidden from cmp') ; 
   		me.fireEvent('setHidden' , this , uid , status) ; 
   	}
}) ; 