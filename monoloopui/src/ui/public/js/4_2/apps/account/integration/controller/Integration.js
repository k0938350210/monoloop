Ext.define('Integration.controller.Integration', {
	extend: 'Ext.app.Controller',
	
	init: function() {
        var me = this; 
        
        me.control({ 
        	'integrationgrid' : {
        		'setHidden' : me.onSetIntegrationStatus
        	}
        });
    },
    
    onLaunch: function(){
         
    },
    
    index: function(config){ 
	    //xx
    }, 
	
	onSetIntegrationStatus : function(cmp , uid , status ){
		 //console.debug(uid) ; 
		 //console.debug(status) ; 
		 Ext.getCmp('main-panel').setLoading('Saving') ; 
		 
		 Ext.Ajax.request({
            success: function(response, opts){
            	Ext.getCmp('main-panel').setLoading(false) ; 
            	Ext.getCmp('main-panel').child('[xtype=integrationgrid]').store.load();
            }, 
            url: '/api/account/plugins/'+uid,
            method: 'POST',
            params: {
            	'enabled' : status
            }
        });
	} 
}) ; 