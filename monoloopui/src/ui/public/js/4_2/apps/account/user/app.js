var UserApp = null ; 

Ext.onReady(function() { 
	Ext.tip.Tip.prototype.minWidth = 300;
	UserApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'User', 
	  	controllers: ['User'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('User.view.Mainpanel', { 
				title : '&nbsp;' ,
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        });
	        
	        Ext.create('Ext.button.Button', {  
				id : 'add-new-user' , 
				cls : 'monoloop-submit-form' , 
				text : 'CREATE NEW USER' , 
	        	renderTo: Ext.get('create-btn') 	        		        	
	        });
	        
	    	// 
	        router.name('userIndex', 'user', {controller: 'User', action: 'index'}); 
	        router.name('userNew', 'user/new', {controller: 'User', action: 'newuser'});
	        router.name('userEdit', 'user/:id', {controller: 'User', action: 'edit'});
	        
	    }
	    
	});
}) ; 