Ext.define('Log.view.Mainpanel', {
	alias: 'widget.logmainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	
	requires: [
        'Log.view.LogGrid'
    ],
	 
    initComponent: function() { 
        Ext.apply(this, { 
        	border : false ,  
        	layout : 'fit' , 
            items: [{
            	xtype : 'loggrid'
            }]  
        });
        this.callParent();
    }
}) ; 