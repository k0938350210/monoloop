Ext.define('Log.store.Log', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: 'uid', type: 'int'} , 
		{name : 'msg'} , 
        {name: 'tstamp' , mapping: 'tstamp', type: 'date', dateFormat: 'timestamp'} , 
        {name: 'ip'} ,  
        {name: 'cruser_id', type: 'int'} 
    ] ,
    remoteSort: true,
    pageSize : 15 ,   
    proxy: {
        type: 'ajax',
        url : 'index.php/logs?eID=ml_acc',
        reader: {
            idProperty  : 'uid' , 
            type: 'json',
            root: 'data', 
            totalProperty: 'totalCount'  
        } , 
        actionMethods: {
            read: 'POST'
        }
    } , 
    sorters: [{
        property: 'tstamp',
        direction: 'DESC'
    }] , 
    autoLoad : true 
    
});