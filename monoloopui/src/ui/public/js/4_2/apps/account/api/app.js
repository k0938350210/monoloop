var ApiApp = null ; 
//http://code.jdempster.com/Ext.ux.CopyButton/
Ext.onReady(function() { 
	ZeroClipboard.setDefaults({
      moviePath: "/zeroclipboard1.2.3/ZeroClipboard.swf"
    }); 
    /*
    var clip = new ZeroClipboard( document.getElementById("copy-button") );
	
	clip.on( "load", function(client) {
		 alert( "movie is loaded" );
	
	  client.on( "mouseDown", function(client, args) {
	    client.setText('xxxxx') ;  
	  } );
	} );
	*/
    
	ApiApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Api', 
	  	controllers: ['Api'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: 'api',
 
	    
	    initRoutes: function(router) {
			Ext.create('Api.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        });
	    	// 
	        router.name('apiIndex', 'api', {controller: 'Api', action: 'index'}); 
	    }
	    
	});
}) ; 