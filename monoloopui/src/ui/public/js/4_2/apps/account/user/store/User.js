Ext.define('User.store.User', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: '_id', type: 'string'},
        {name : 'disable' , type : 'int'} , 
        {name : 'isMonoloopSupport' , type : 'int'} , 
        {name : 'isMonoloopAdmin' , type : 'int'} , 
    	{name: 'name'} ,  
        {name: 'username' } , 
        {name: 'email', mapping: 'current_config.email' } , 
        {name : 'company'}
    ] ,
    remoteSort: true,
    pageSize : 15 ,   
    proxy: {
        type: 'ajax',
        url : '/api/account/users',
        reader: {
            idProperty  : '_id' , 
            type: 'json',
            root: 'topics', 
            totalProperty: 'totalCount'  
        } , 
        actionMethods: {
            read: 'POST'
        }
    } , 
    sorters: [{
        property: 'username',
        direction: 'ASC'
    }] , 
    autoLoad : true 
    
});