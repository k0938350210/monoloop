Ext.define('Datapush.controller.Datapush', {
	extend: 'Ext.app.Controller',
	
	mainLoading : false , 
	
	init: function() {
        var me = this; 
        
        me.control({    
        	'#create-app-btn' : {
        		'click' : me.createNewApp
        	} , 
        	'#wizard-form button#newoption' : {
        		'click' : me.addNewOption
        	}, 
        	
        	'#wizzard-window button[text="SAVE"]' : {
        		'click' : me.wizzardSave 	
       		},
       		'#wizzard-window button[text="CANCEL"]' : {
        		'click' : me.wizzardClose 	
       		},
       		'#wizzard-window' : {
       			'saveNewOption' : me.saveNewOption
       		},
        	
        	// ------ Option window .
			'#optionWindow button[text="CANCEL"]' : {
				'click' : me.optionWindowCancel
			},
			'#optionWindow button[text="SAVE"]' : {
				'click' : me.optionWindowSave
			}, 
            
            // ------ Main Grid 
            'maingrid' : {
                'editApp' : me.editApp , 
                'deleteApp' : me.deleteApp 
            }
        });
    },
 
    
    onLaunch: function(){ 
    	
    },
    
    createNewApp : function(){
    	 Ext.create('Datapush.view.WizzardWindow',{
    	 	title : 'Create new application'
    	 }).show() ; 
    },
    // ---------- Wizzard Form
    // Form 
    addNewOption : function(){
    	Ext.create('Datapush.view.OptionWindow',{
    		id : 'optionWindow' , 
    		title : 'Create new data'
    	}).show() ; 
    },
    
    editOption : function(btn){
    	var name = btn.up('fieldset').title ; 
     	Ext.create('Datapush.view.OptionWindow',{
    		id : 'optionWindow' , 
    		title : 'Edit data - ' + name , 
    		fieldName : name , 
			refFieldSet :  btn.up('fieldset')
    	}).show() ; 
    }, 
    
    wizzardSave : function(btn){ 
        
    	var form = btn.up('window').down('form').getForm();
    	if (form.isValid()) {
    		btn.up('window').setLoading('Saving') ; 
            var fieldsoption = Ext.getCmp('fields-option').items; 
            var params = {} ; 
            for(var i = 0 ; i <  fieldsoption.length  ; i++ ){ 
                var fieldObj = fieldsoption.items[i] ;   
                params['items[' + i + ']'] = fieldObj.title ; 
            } 	   
           
    		form.submit({
            	url : 'index.php/save?eID=ml_application' , 
                params : params ,
                success: function(form, action) {
                    btn.up('window').setLoading('false') ; 
                   btn.up('window').close() ; 
                   Ext.getCmp('main-panel').down('grid').store.load();
                },
                failure: function(form, action) {
                    btn.up('window').setLoading('false') ; 
                    Ext.Msg.alert('Failed', action.result.msg);
                }
            });
   		}
    },
    
    wizzardClose : function(btn){
    	btn.up('window').close() ; 
    } , 
    
    // ------ Option window .
	optionWindowCancel : function(){
		Ext.getCmp('optionWindow').close() ; 
	},
	
	optionWindowSave : function(btn,e,eOpts){
		var me = this ; 
		var form = btn.up('window').down('form').getForm();
		if (form.isValid()) {
			var refFieldSet = btn.up('window').refFieldSet ; 
			var field = form.getFieldValues() ;   
			if( refFieldSet == null){ 
				me.saveNewOption(field['name'] , field['optionWindow-type-inputEl']) ; 
				me.optionWindowCancel() ; 
			}else{
				//Update as edit   
				refFieldSet.setTitle(field['name']) ;
			}
		}
	},
	
	saveNewOption : function(name,type){
		var me = this ; 
		Ext.getCmp('fields-option').add({
			xtype : 'fieldset' , 
			title : name , 
			items : [/*{
				xtype : 'displayfield' , 
				value : '<strong>Type:</strong> '+type
			},*/{
                xtype : 'container' , 
                height : 20 , 
                border : false , 
                layout : {
                    type : 'hbox' , 
                    align : 'stretch' 
                },
                items : [{
                	xtype : 'button' , 
                	cls : 'monoloop-submit-form data-edit-btn' ,
					text : 'Edit' ,
					handler : function(btn){
						me.editOption(btn) ; 
					} 
                },{
                    xtype : 'displayfield' , 
                    value : '&nbsp;'
                },{
                	xtype : 'button' , 
                	cls : 'monoloop-submit-form'  , 
					text : 'Delete' , 
					handler : function(btn){
						Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete this field?',function(btn2){
				            if (btn2 == 'yes'){
						        btn.up('fieldset').destroy() ; 
				        	}
				       	}) ;
					}
                }]
			}]
		})  ; 
	} , 
    
    // ------- Main Grid ; 
    editApp : function(uid){
        Ext.getCmp('main-panel').setLoading('Loading') ; 
        Ext.Ajax.request({
			success: function(response, opts){
				Ext.getCmp('main-panel').setLoading(false) ;    
                var data = Ext.decode(response.responseText); 
                Ext.create('Datapush.view.WizzardWindow',{
            	 	title : 'Edit application - ' + data.result.name , 
                    data : data.result , 
                    data_items : data.items.result 
            	 }).show() ; 
			}, 
			url: 'index.php/'+uid+'?eID=ml_application',
			method: 'GET' 
		});
    } , 
    
    deleteApp : function(uid){
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete this app?',function(btn2){
            if (btn2 == 'yes'){
		        Ext.Ajax.request({
					success: function(response, opts){
						Ext.getCmp('main-panel').down('grid').store.load();
					}, 
					url: 'index.php/'+uid+'?eID=ml_application',
					method: 'DELETE' 
				}) ; 
        	}
       	}) ;
    }
}) ; 