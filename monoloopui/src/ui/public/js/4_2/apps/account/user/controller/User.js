Ext.define('User.controller.User', {
	extend: 'Ext.app.Controller',
	
	requires : [
		'User.view.EditUserWindow'
	] ,
	
	init: function() {
        var me = this; 
        
        me.control({
            'usergrid' : {  
                'setHidden' : me.onSetUserStatus , 
                'deleteUser' : me.onDeleteUser , 
                'editUser' : me.onEditUser
            }, 
			'edituserwindow' : {
				'afterrender' : me.onEditUserAfterrender ,
				'beforeclose' : me.onEditUserBeforeClose  
			}, 
			'edituserwindow button' : {
				'click' : me.onEditUserSave 
			}, 
			'treepanel' : {
				'checkchange' : me.onRightChange 
			}, 
			'#userf-isadmin' : {
				'change' : me.onIsadminChange
			} , 
			'#add-new-user' : {
				click : me.onCreateNewUser
			}
        });
    },
    
    onLaunch: function(){
         
    },
    
    index: function(config){
        //alert(123); 
        
    },
    
    edit : function(config){
    	//console.debug(config) ; 	
    	Ext.create('User.view.EditUserWindow',{
    		title : 'Edit User' , 
    		uid : config.id 
    	}).show() ; 
    },
    
    newuser : function(config){
    	Ext.create('User.view.EditUserWindow',{
    		title : 'Create New User' , 
    		uid : 0
    	}).show() ; 
    } , 
    
    // Controller ; 
	
	onSetUserStatus : function(cmd , uid , status ){ 
		Ext.getCmp('main-panel').setLoading('Saving') ;   
		Ext.Ajax.request({
			success: function(response, opts){
				Ext.getCmp('main-panel').setLoading(false) ;   
				Ext.getCmp('main-panel').child('[xtype=usergrid]').store.load();
			}, 
			url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/setUserDisable',
			method: 'POST',
			params: {
				'uid' : uid  , 
				'disable' : status
			}
		});
	} , 
	
	onDeleteUser : function(cmd , uid){
		//Ext.getCmp('main-panel').setLoading('Deleting') ;   
		Ext.MessageBox.show({
    		title:'Messagebox Title',
    		msg: 'Are you sure want to delete?',
    		buttons: Ext.Msg.YESNO,
    		fn : function(btn){
    			if( btn == 'yes'){
    				Ext.getCmp('main-panel').setLoading('Deleting') ; 
    				Ext.Ajax.request({
						success: function(response, opts){
							Ext.getCmp('main-panel').setLoading(false) ;   
							Ext.getCmp('main-panel').child('[xtype=usergrid]').store.load();
						}, 
						url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/removeUser',
						method: 'POST',
						params: {
							'uid' : uid  
						}
					});
    			}
    		}
		}) ; 
	} , 
	
	onEditUser : function(cmp , uid , isMonoloopSupport){ 
		var url = UserApp.router.generate('userEdit', {controller: 'User', action: 'edit', id: uid  });
		Ext.util.History.add(url); 
		
	} , 
	
	onEditUserAfterrender : function(panel){
		//console.debug(panel.uid) ; 
		var treepanel = panel.query('[xtype=treepanel]')[0] ;    
		treepanel.getStore().proxy.extraParams = {'uid' : panel.uid } ;
		treepanel.store.load({
		    callback: function(records, operation, success) {
		        treepanel.expandAll();
		    }                               
		});  
		 
		
		if( panel.uid != 0 ){
			panel.setLoading('Loading') ; 
			Ext.Ajax.request({
				success: function(response, opts){
					panel.setLoading(false) ;   
					var data = Ext.decode(response.responseText); 
					Ext.getCmp('userf-name').setValue(data.name) ; 
					Ext.getCmp('userf-email').setValue(data.email) ; 
					Ext.getCmp('userf-company').setValue(data.company) ; 
					Ext.getCmp('userf-disable').setValue(data.disable) ; 
					Ext.getCmp('userf-isadmin').setValue(data.isMonoloopAdmin) ;  
					if( data.isMonoloopSupport == '1' ){
						panel.down('fieldset').setVisible(true) ; 
						if( data.support_type == 0)
							data.support_type = null
						panel.down('fieldset combo').setValue(data.support_type) ;
						panel.down('fieldset datefield').setValue(data.support_enddate) ;  
					}else{ 
						panel.down('fieldset').setVisible(false) ; 
					}
				}, 
				url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/getUser',
				method: 'POST',
				params: {
					'uid' : panel.uid  
				}
			});
		}  
	} , 
	
	onEditUserBeforeClose : function(panell , eOpts){
		Ext.util.History.add('');
	} , 
	
	onEditUserSave : function(btn){ 
		if(Ext.getCmp(btn.f).getForm().isValid()){
			var data = {} ; 
            data['data[uid]'] = btn.findParentByType('window').uid ; 
            data['data[name]'] = Ext.getCmp('userf-name').getValue() ; 
            data['data[email]'] = Ext.getCmp('userf-email').getValue() ; 
            data['data[company]'] = Ext.getCmp('userf-company').getValue() ; 
            data['data[disable]'] = Ext.getCmp('userf-disable').getValue() ; 
            data['data[isMonoloopAdmin]'] = Ext.getCmp('userf-isadmin').getValue() ; 
            data['data[support_type]'] = btn.up('window').down('fieldset combo').getValue() ; 
		 	data['data[support_enddate]'] = btn.up('window').down('fieldset datefield').getValue() ; 
 
            
            if( data['data[disable]'] == true)
                data['data[disable]'] = 1 ; 
            else
                data['data[disable]'] = 0 ; 
                
            if( data['data[isMonoloopAdmin]'] == true)
                data['data[isMonoloopAdmin]'] = 1 ; 
            else
                data['data[isMonoloopAdmin]'] = 0 ; 
                
            var rootNode = Ext.getCmp('userf-rights').getRootNode() ; 
            var allrightsNode = rootNode.childNodes[0] ; 
            
            var j = 0 ; 
            
            allrightsNode.eachChild(function(node) {
                node.eachChild(function(subNode) { 
                    if( subNode.data.checked == true){
                      	data['data[rights][' + j + ']'] = subNode.get('value') ; 
                        j++ ; 
                    }
                }) ; 
            });
            
            //console.debug(data) ; 
            
            btn.findParentByType('window').setLoading('saving') ; 
            
            Ext.Ajax.request({
				success: function(response, opts){
					var jsonData = Ext.decode(response.responseText); 
					btn.findParentByType('window').setLoading(false) ;    
					if( jsonData.success == true){ 
		                btn.findParentByType('window').close() ;   
                        Ext.getCmp('main-panel').child('[xtype=usergrid]').store.load(); 
                    }else{
                        Ext.Msg.alert('Fail', jsonData.msg);
                    }
					
				}, 
				url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/saveUser' ,
				method: 'POST',
				params: data
			});
		}
	} , 
	
	onRightChange : function(node , checked ,  eOpts){
	  	node.cascadeBy(function(n){n.set('checked', checked);} );
	} , 
	
	onIsadminChange : function(chk , newV , oldV ){
		var c = chk.up().query('[xtype=treepanel]')[0] ; 
		if( newV){ 
		 	c.setDisabled(true) ; 
		}else{
			c.setDisabled(false) ; 
		}
	} , 
	
	onCreateNewUser : function(){
		var url = UserApp.router.generate('userNew', {controller: 'User', action: 'newuser' });
		Ext.util.History.add(url); 
	}
}) ; 

//http://localhost/monoloop/index.php?eID=monoloopaccounting&pid=480&cmd=account/setUserDisable
//http://localhost/monoloop/index.php?eID=monoloopaccounting&pid=480&cmd=account/removeUser