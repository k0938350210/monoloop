var DatapushApp = null ; 

Ext.onReady(function() {  
	
	DatapushApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Datapush', 
	  	controllers: ['Datapush'], 
	  	
	    //autoCreateViewport: true,
	     
 		defaultHistoryToken: '',
	    
	    initRoutes: function(router) { 
			Ext.create('Datapush.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        });    
	        
	        Ext.create('Ext.button.Button',{
	        	text : 'Create New Application' , 
	        	id : 'create-app-btn' , 
	        	cls : 'monoloop-submit-form'  , 
	        	renderTo : Ext.get('search-text') 
	        }) ; 
	    } 
	});
}) ; 