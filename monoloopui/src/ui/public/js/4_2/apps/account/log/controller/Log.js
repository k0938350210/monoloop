Ext.define('Log.controller.Log', {
	extend: 'Ext.app.Controller',
	
	mainLoading : false , 
	
	init: function() {
        var me = this; 
        
        me.control({   
        	'#user-cb' : {
        		'change' : me.userChange
        	}
        });
    },
 
    
    onLaunch: function(){ 
    	
    }, 
	
	userChange : function(cb,oldV,newV,eOpts){ 
		var store = Ext.getCmp('main-panel').down('loggrid').store ; 
		store.proxy.extraParams = {'uid' : cb.getValue() } ;   
		store.load() ; 
	} 
     
}) ; 