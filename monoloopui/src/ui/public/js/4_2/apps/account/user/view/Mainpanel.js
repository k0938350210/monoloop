Ext.define('User.view.Mainpanel', {
	alias: 'widget.apimainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	
	requires: [
        'User.view.UserGrid' 
    ],
	 
    initComponent: function() { 
        Ext.apply(this, { 
        	border : false ,  
        	layout : 'fit' , 
            items: [{
            	xtype : 'usergrid'
            }]  
        });
        this.callParent();
    }
}) ; 