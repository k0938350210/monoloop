Ext.define('Score.view.Mainpanel', {
	alias: 'widget.scoremainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	bodyPadding: 20, 
	
 	requires : ['UI.shared.view.form.BoxSelect'] , 
    
    initComponent: function() {
    	var me = this ; 
        Ext.apply(this, {
        	title : 'Overview' , 
        	border : false ,  
            items: [{
            	xtype : 'form' , 
            	id : me.id + '-form'  ,
            	border : false , 
            	items : [{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 10px 0 ;' , 
            		border : false , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : 'Click depth:' ,
            			width : 120
            		},{
            			xtype : 'numberfield' , 
            			name : 'ma[clickdepth]' , 
            			id :  me.id + '-clickdepth' ,  
            			hideTrigger : true ,
            			width : 122 , 
            			tooltip: 'Minimum page views in a vist',
				    	listeners : {
							render: function(p) {
								p.getEl().down('input').set({'data-qtip': p.tooltip});
							}
						}
            		},{
            			xtype : 'displayfield' , 
            			id : me.id + '-avg-clickdepth' , 
            			value : ''
            		}]
            	},{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 10px 0 ;' , 
            		border : false , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : 'Duration:' ,
            			width : 120
            		},{
            			xtype : 'numberfield' , 
            			name : 'ma[duration]' , 
            			hideTrigger : true ,
            			id :  me.id + '-duration' ,  
            			width : 122 , 
            			tooltip: 'Minimum lenght of visit in minutes',
				    	listeners : {
							render: function(p) {
								p.getEl().down('input').set({'data-qtip': p.tooltip});
							}
						}
            		},{
            			xtype : 'displayfield' , 
            			id : me.id + '-avg-duration' , 
            			value : ''
            		}]
            	},{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 10px 0 ;' , 
            		hidden : true , 
            		border : false , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : 'Loyalty:' ,
            			width : 122
            		},{
            			xtype : 'numberfield' , 
            			name : 'ma[loyalty]' , 
            			hideTrigger : true ,
            			id :  me.id + '-loyalty' ,  
            			width : 122 , 
            			tooltip: 'Minimum number of visits',
				    	listeners : {
							render: function(p) {
								p.getEl().down('input').set({'data-qtip': p.tooltip});
							}
						}
            		},{
            			xtype : 'displayfield' , 
            			id : me.id + '-avg-loyalty' , 
            			value : ''
            		}]
            	},{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 10px 0 ;' , 
            		border : false , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : 'Recency:' ,
            			width : 120
            		},{
            			xtype : 'numberfield' , 
            			name : 'ma[recency]' , 
            			hideTrigger : true ,
            			id :  me.id + '-recency' ,  
            			width : 122 , 
            			tooltip: 'Number of days to look back',
				    	listeners : {
							render: function(p) {
								p.getEl().down('input').set({'data-qtip': p.tooltip});
							}
						}
            		},{
            			xtype : 'displayfield' , 
            			id : me.id + '-avg-recency' , 
            			value : ''
            		}]
            	},{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 10px 0 ;' , 
            		border : false , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : 'Latency:' ,
            			width : 122
            		},{
            			xtype : 'numberfield' , 
            			name : 'ma[latency]' , 
            			hideTrigger : true ,
            			id :  me.id + '-latency' ,  
            			width : 122 , 
            			tooltip: 'Minimum minutes of visits',
				    	listeners : {
							render: function(p) {
								p.getEl().down('input').set({'data-qtip': p.tooltip});
							}
						}
            		},{
            			xtype : 'displayfield' , 
            			id : me.id + '-avg-latency' , 
            			value : ''
            		}]
            	}/*,{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 5px 0 ;' , 
            		border : false , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : 'Brand:' ,
            			width : 120
            		},{
            			xtype : 'boxselect' , 
           			 	width : 455,
					    growMin : 75,
					    growMax : 120,
					    name : 'ma[brands][]' , 
					    id : me.id + '-brands' , 
					    delimiter: ',',
					    store : [] , 
					    queryMode : 'local' ,
					    forceSelection : false,
					    createNewOnEnter : true,
					    createNewOnBlur : true,
					    filterPickList : true 
            		}]
            	},{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 10px 0 ;' , 
            		border : false , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : '&nbsp;' ,
            			width : 120
            		},{
            			xtype : 'displayfield' , 
           			 	width : 440, 
				     	value : '<span style="color : rgb(181,181,181) ;">Type brand name to add to brands list.</span>'
            		}]
            	}*/,{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 10px 0 ;' , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : '&nbsp;' ,
            			width : 400
            		},{
            			xtype : 'button' , 
            			cls : 'monoloop-submit-form' , 
						text : 'UPDATE AVERAGE' , 
						disabled : true 	
            		},{
            			xtype : 'displayfield' , 
            			value : '&nbsp;' , 
            			width : 10
            		},{
            			xtype : 'button' , 
            			cls : 'monoloop-submit-form' , 
						text : 'SAVE' 	
            		}]
            	}]
            }] 
        });
        this.callParent();
    }
}) ; 