var ProfileApp = null ; 

Ext.onReady(function() { 
	 
	
	ProfileApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Profile', 
	  	controllers: ['Profile'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('Profile.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        }); 
	    }
	    
	});
}) ; 