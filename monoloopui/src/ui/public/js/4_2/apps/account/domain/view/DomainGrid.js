Ext.define('Domain.view.DomainGrid', { 
    extend: 'Ext.grid.Panel',
    alias: 'widget.domaingrid',
    xtype : 'domaingrid', 
    
    initComponent: function() {
    	var me = this ; 
    	var store = Ext.create('Domain.store.Domain') ;  
    	
    	Ext.apply(this, { 
    		disableSelection: true,
    		store : store , 
    		columns: [
              {header:'Domain Name',  flex : 1 , dataIndex: 'domain' },  
              {header:'Privacy URL',  width : 200  , dataIndex: 'url_privacy' },
			  {header:'Privacy Procedure' , width : 250  , dataIndex: 'uid'  , 
			  	renderer : function(value, p, r){
			  		if( r.data['privacy_option'] == 1 ){
		                return 'Automaticly add notificationtab to page' ; 
		            }else{
		                return 'Manually add link to privacy center' ; 
            		}
		  		}
			  }, 
			  {header:'Script Status' , width : 160 , 
			  	renderer : function(value, p, r){ 
			  		var ret = '<span style="color:red;">Not present</span>' ; 	
			  		
		  			if(r.data['status'] == 1){ 
		  				ret = '<span style="color:orange;">Placed incorrectly</span>' ; 
		  				p.tdAttr = 'data-qtip="Script is on page, but not in header"';
	  				}else if(r.data['status'] == 2){
	  					ret = '<span style="color:green;">Present</span>' ; 
	  					p.tdAttr = 'data-qtip="Script is on page and is inside header"';
	  				}else{
	  					p.tdAttr = 'data-qtip="Script not on page"';
	  				}
	  				return ret ; 
		  		}
			  } ,
			  {header:'Action' , width : 80 , cls : 'grid-btn' , 
			  	renderer : function(value, p, r){ 
		            var edit = '<td><div onClick="Ext.getCmp(\''+me.id+'\').editDomain(\''+r.data['uid']+'\')" title="Edit" class="placement-property"></div></td>' ;
		            var deleteData = '<td><div onClick="Ext.getCmp(\''+me.id+'\').deleteDomain(\''+r.data['uid']+'\')" title="Delete" class="placement-delete"></div></td>' ; 
			    	var ret =  '<table width="100%" ><tr align="center"> '  
		                         //+ editP
		                         + edit  
			    			     + deleteData
		                         +
							   '</tr></table>'  ;
		 
			 
					return ret ;   
		  		} 
			  } 
           ] , 
		   bbar: Ext.create('Ext.PagingToolbar', {
	            store: store,
	            displayInfo: true,
	            displayMsg: 'Displaying  {0} - {1} of {2}',
	            emptyMsg: "No data to display",
	            items:[]
	        }) 
   		}) ; 
   		
   		this.callParent(arguments); 
   	} , 
   	
   	editDomain : function(uid){
   		var me = this ;  
   		var rec = me.getStore().getById(parseInt( uid )) ; 
   		//console.debug(rec) ; 
   		me.fireEvent('editDomain' , this , rec.get('domain') ) ; 
   	} , 
   	
   	deleteDomain : function(uid){
   		var me = this ;  
   		var rec = me.getStore().getById(parseInt( uid )) ; 
   		me.fireEvent('deleteDomain' , this , rec.get('domain')  ) ; 
   	}
}) ; 