Ext.define('Domain.view.Mainpanel', {
	alias: 'widget.domainmainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	
	requires: [
        'Domain.view.DomainGrid' 
    ],
	
 
    
    initComponent: function() { 
        Ext.apply(this, {
        	title : '&nbsp;' , 
        	border : false ,  
        	layout : 'fit' , 
            items: [{
            	xtype : 'domaingrid'
            }]  
        });
        this.callParent();
    }
}) ; 