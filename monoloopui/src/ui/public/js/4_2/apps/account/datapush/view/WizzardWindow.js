Ext.define('Datapush.view.WizzardWindow', {
	alias: 'widget.wizzardwindow',
	extend: 'Ext.window.Window', 
	height: 540, 
	width : 900,
 	id : 'wizzard-window' ,  
    data : null , 
    data_items : null , 
 	requires : ['Ext.ux.form.MultiSelect'] , 
    initComponent: function() { 
    	var statusStore = Ext.create('Ext.data.Store', {
		    fields : ['name',  {name: 'value', type: 'int'}],
		    data : [
		        {name : 'Development',   value: 1 },
				{name : 'Live',   value: 2 }  
		        
		    ]
		});
		
		var livestreamStore = Ext.create('Ext.data.Store', {
		    fields : ['name',  {name: 'value', type: 'int'}],
		    data : [
		        {name : 'On',   value: 1 },
				{name : 'Off',   value: 0 }  
		        
		    ]
		});
		
		var typeStore = Ext.create('Ext.data.Store', {
		    fields : ['name',  {name: 'value', type: 'int'}],
		    data : [
		        {name : 'Private',   value: 1 },
				{name : 'Public',   value: 2 }  
		        
		    ]
		});
		
		var authenStore = Ext.create('Ext.data.Store', {
		    fields : ['name',  {name: 'value', type: 'int'}],
		    data : [
		        {name : 'SOAP',   value: 1 },
				{name : 'REST',   value: 2 }  
		        
		    ]
		});
		
		var triggerStore = Ext.create('Ext.data.Store',{
			fields : ['name',  {name: 'value', type: 'int'}],
			data : [
				{name : 'Visit Start' , value : 1} , 
				{name : 'Goal' , value : 2} , 
				{name : 'Visit End' , value : 3}
			]
		}) ; 
    	
        Ext.apply(this, { 
        	border : false ,  
        	layout : 'fit' , 
            items: [{
            	xtype : 'form' , 
            	id : 'wizard-form' , 
            	border : false , 
            	layout : 'border' , 
            	items : [{
            		xtype : 'container' , 
            		region : 'west' , 
            		width : '40%' , 
            		layout : 'form' ,   
            		split: true,
            		style : 'padding : 0 15px' , 
            		defaults : {
            			anchor : '100%'
            		} , 
            		items : [{
          		        xtype : 'hiddenfield' , 
                        name : 'uid'
            		},{
            			xtype : 'displayfield' , 
            			value : 'Name'
            		},{
            			xtype : 'textfield' , 
            			name : 'name' , 
            			allowBlank : false 
            		},{
            			xtype : 'displayfield' , 
            			value : 'Description'
            		},{
            			xtype : 'textarea',
            			name : 'description'
            		},{
            			xtype : 'displayfield' , 
            			value : 'Type'
            		},{
				    	xtype : 'combo' , 
				    	store: typeStore,
				    	queryMode: 'local',
					    displayField: 'name',
					    valueField: 'value',
					    allowBlank : false , 
					    name : 'access_type'  
				    },{
            			xtype : 'displayfield' , 
            			value : 'Status'
            		},{
				    	xtype : 'combo' , 
				    	store: statusStore,
				    	queryMode: 'local',
					    displayField: 'name',
					    valueField: 'value',
					    allowBlank : false , 
					    name : 'status_type'  
				    },{
            			xtype : 'displayfield' , 
            			value : 'Live stream'
            		},{
				    	xtype : 'combo' , 
				    	store: livestreamStore,
				    	queryMode: 'local',
					    displayField: 'name',
					    valueField: 'value',
					    allowBlank : false , 
					    name : 'livestream'  
				    }]
            	},{
            		xtype : 'container' , 
            		region : 'center' , 
            		layout : 'form' , 
            		autoScroll : true , 
            		style : 'padding : 0 25px 0 15px' , 
            		defaults: {
                		anchor: '90%',
            		} , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : 'Authentication type'
            		},{
				    	xtype : 'combo' , 
				    	store: authenStore,
				    	queryMode: 'local',
					    displayField: 'name',
					    valueField: 'value',
					    allowBlank : false , 
					    name : 'authen_type'  
				    },{
				    	xtype : 'displayfield' , 
				    	value : 'Host URL' 
				    	
				    },{
				    	xtype : 'textfield' , 
				    	name : 'host' , 
                        readOnly : true , 
				    	allowBlank : true 
				    },{
				    	xtype : 'displayfield' , 
				    	value : 'Access Token' 
				    	
				    },{
				    	xtype : 'textfield' , 
				    	name : 'api_token' , 
                        readOnly : true , 
				    	allowBlank : true 
				    },{
				    	xtype : 'displayfield' , 
				    	value : 'Trigger'
				    },{
				    	xtype : 'multiselect' , 
				    	store: triggerStore,  
					    displayField: 'name',
					    valueField: 'value',
					    delimiter : ',' , 
                    	allowBlank:false,
					    name : 'option_trigger'  
				    	
				    },{
				    	xtype : 'fieldset' , 
				    	id : 'fields-option' , 
                		title: 'Data',
						items : [] 
				    },{
				    	xtype : 'button' , 
				    	id : 'newoption' ,
				    	cls : 'monoloop-submit-form' , 
				    	text : 'Add new option' 
				    }]
            	}]
            }] , 
			buttons : [{ 
    			xtype : 'button' , 
    			cls : 'monoloop-submit-form' , 
				text : 'SAVE' 	 
			},{ 
    			xtype : 'button' , 
    			cls : 'monoloop-submit-form' , 
				text : 'CANCEL' 	 
			}] , 
            listeners : {
                afterrender : function(w){
                    if(w.data != null){
                        w.down('hiddenfield[name="uid"]').setValue(w.data.uid);
                        w.down('textfield[name="name"]').setValue(w.data.name);
                        w.down('textarea[name="description"]').setValue(w.data.description);
                        w.down('combo[name="access_type"]').setValue(parseInt(w.data.access_type));
                        w.down('combo[name="status_type"]').setValue(parseInt(w.data.status_type));
                        w.down('combo[name="livestream"]').setValue(parseInt(w.data.livestream));
                        w.down('combo[name="authen_type"]').setValue(parseInt(w.data.authen_type));
                        w.down('textfield[name="host"]').setValue(w.data.host);
                        w.down('multiselect[name="option_trigger"]').setValue(w.data.option_trigger);
                        //w.down('textfield[name="api_key"]').setValue(w.data.api_key);
                        w.down('textfield[name="api_token"]').setValue(w.data.api_token);
                    }
                    
                    if(w.data_items != null && w.data_items.length > 0){
                    	for(var i = 0 ; i < w.data_items.length ; i++ ){
                    		w.fireEvent('saveNewOption',w.data_items[i].fieldname,null) ; 
                    	}
                    }
                }
            }
        });
        this.callParent();
    }
}) ; 