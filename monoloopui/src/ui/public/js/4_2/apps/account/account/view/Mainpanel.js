Ext.define('Account.view.Mainpanel', {
	alias: 'widget.accountmainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	
	requires: [
        'Account.view.AccountGrid' 
    ],
	
 
    
    initComponent: function() { 
        Ext.apply(this, {
        	title : '&nbsp;' , 
        	border : false ,  
        	layout : 'fit' , 
            items: [{
            	xtype : 'accountgrid'
            }]  
        });
        this.callParent();
    }
}) ; 