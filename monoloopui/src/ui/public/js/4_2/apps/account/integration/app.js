var ApiApp = null ; 

Ext.onReady(function() {  
    
	ApiApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Integration', 
	  	controllers: ['Integration'],  
	    
	    defaultHistoryToken: 'index',
 
	    
	    initRoutes: function(router) {
			Ext.create('Integration.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        });
	    	// 
	    	
	        router.name('integrationIndex', 'index', {controller: 'Integration', action: 'index'}); 
	    }
	    
	});
}) ; 