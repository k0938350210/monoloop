var UsageApp = null ; 

Ext.onReady(function() { 
	 
	
	UsageApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Usage', 
	  	controllers: ['Usage'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('Usage.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        });
 
	    }
	    
	});
}) ; 