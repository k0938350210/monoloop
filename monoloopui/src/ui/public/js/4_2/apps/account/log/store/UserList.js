Ext.define('Log.store.UserList', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: 'uid', type: 'int'} , 
		{name : 'username'}  
    ] ,
    remoteSort: true,
    pageSize : 15 ,   
    proxy: {
        type: 'ajax',
        url : 'index.php/users/selectorList?eID=ml_acc',
        reader: {
            idProperty  : 'uid' , 
            type: 'json' 
        } , 
        actionMethods: {
            read: 'POST'
        }
    } , 
    sorters: [{
        property: 'username',
        direction: 'ASC'
    }]  
    
});