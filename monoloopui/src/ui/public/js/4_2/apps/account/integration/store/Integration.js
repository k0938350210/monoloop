Ext.define('Integration.store.Integration', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: '_id', type: 'string'},
        {name : 'enabled' , type : 'bool'} ,  
    	{name: 'name', mapping: 'plugin.name'} 
    ] ,
    remoteSort: true,
    pageSize : 15 ,   
    proxy: {
        type: 'ajax',
        url : '/api/account/plugins',
        reader: {
            idProperty  : '_id' , 
            type: 'json',
            root: 'topics', 
            totalProperty: 'totalCount'  
        } , 
        actionMethods: {
            read: 'POST'
        }
    } , 
    sorters: [{
        property: 'name',
        direction: 'ASC'
    }] , 
    autoLoad : true 
    
});