Ext.define('Datapush.view.MainGrid', { 
    extend: 'Ext.grid.Panel',
    alias: 'widget.maingrid',
    xtype : 'maingrid', 
    
    initComponent: function() {
    	var me = this ; 
    	var store = Ext.create('Datapush.store.Datapush') ; 
    	
    	Ext.apply(this, { 
    		disableSelection: true,
    		store : store , 
    		columns: [
              {header:'Application Name',width : 200,dataIndex : 'name'}, 
              {header:'Description',width : 200 , dataIndex : 'description' ,  flex : 1 } , 
              {header:'Type',width : 150 , sortable : false , 
                renderer : function(value, p, r){
                	if( r.data['access_type'] == 1 ){
                        return 'Private' ; 
                    }else{
                        return 'Public' ; 
                	}
                }
              } , 
              {header:'Status',width : 150 , sortable : false , 
                renderer : function(value, p, r){
                	if( r.data['status_type'] == 1 ){
                        return 'Development' ; 
                    }else{
                        return 'Live' ; 
                	}
                }
              } , 
              {header:'Actions',width : 100 , sortable : false , 
			  	renderer : function(value, p, r){ 
		            var edit = '<td><div onClick="Ext.getCmp(\''+me.id+'\').fireEvent(\'editApp\','+r.data['uid']+')" title="Edit" class="placement-property"></div></td>' ;
		            var deleteData = '<td><div onClick="Ext.getCmp(\''+me.id+'\').fireEvent(\'deleteApp\','+r.data['uid']+')" title="Delete" class="placement-delete"></div></td>' ; 
			    	var ret =  '<table width="100%" ><tr align="center"> '  
		                         //+ editP
		                         + edit  
			    			     + deleteData
		                         +
							   '</tr></table>'  ;
		 
			 
					return ret ;   
		  		} 
              } 
           ] , 
		   bbar: Ext.create('Ext.PagingToolbar', {
	            store: store,
	            displayInfo: true,
	            displayMsg: 'Displaying users {0} - {1} of {2}',
	            emptyMsg: "No users to display",
	            items:[]
	        })  , 
            listeners : {
                'editApp' : function(uid){} , 
                'deleteApp' : function(uid){}  
            }
   		}) ;  
   		this.callParent(arguments); 
   	} , 
     
}) ; 