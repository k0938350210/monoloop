Ext.define('Domain.view.EditDomainWindow', { 
    extend: 'Ext.window.Window',
    alias: 'widget.editdomainwindow',
    xtype : 'editdomainwindow', 
    
    uid : 0 , 
    data : null , 
    
    initComponent: function() {
    	var me = this ; 
    	
    	var selectorStore = Ext.create('Ext.data.Store', {
		    fields : ['name',  {name: 'value', type: 'int'}],
		    data : [
		        {name : 'Automaticly add notificationtab to page',   value: 1 },
				{name : 'Manually add link to privacy center',   value: 2 }  
		        
		    ]
		});
		
    	Ext.apply(this, { 
    		closable:true,
    		modal : true ,
    		height : 360 , 
    		width : 500 , 
    		items:[{
    			xtype : 'form' , 
    			id : me.id + '-form' ,
    			border : false , 
    			bodyPadding: 15 ,  
    			layout: 'anchor',
			    defaults: {
			        anchor: '100%'
			    },
			    items : [{
			    	xtype : 'displayfield' , 
			    	value : '<b>Domain :</b>'
			    },{
			    	xtype : 'textfield' , 
			    	name : 'domain' , 
			    	allowBlank : false , 
			    	id : me.id + '-domain'
			    },{
			    	xtype : 'displayfield' , 
			    	value : '<b>Privacy Policy URL :</b>'
			    },{
			    	xtype : 'textfield' , 
			    	name : 'url' , 
			    	id : me.id + '-url'
			    },{
			    	xtype : 'displayfield' , 
			    	value : '<b>Action :</b>'
			    },{
			    	xtype : 'combo' , 
			    	store: selectorStore,
			    	queryMode: 'local',
				    displayField: 'name',
				    valueField: 'value',
				    allowBlank : false , 
				    name : 'option' , 
				    id : me.id + '-option'
			    },{
			    	xtype : 'hidden'  , 
			    	name : 'uid' , 
			    	value : me.uid 
			    }]
    		}] , 
    		buttons : [{
    			text : 'Customize' , 
    			cls : 'monoloop-submit-form' 
    		},{
    			text : 'SAVE' , 
    			cls : 'monoloop-submit-form' 
    		}] 
   		}) ; 
   		this.callParent(arguments); 
	}
}); 