var ScoreApp = null ; 

Ext.onReady(function() { 
	 
	
	ScoreApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Score', 
	  	controllers: ['Score'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('Score.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        }); 
	    }
	    
	});
}) ; 