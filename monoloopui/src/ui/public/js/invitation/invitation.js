var invitation = {
    name : '' , 
    email : '' , 
    comment : '' , 
    baseURL : '' , 
    createnewuser : function(){ 
        Ext.create('Ext.window.Window', {
            title: 'Create new monoloop user',
            id : 'i-window-1' ,  
            resizable : true , 
            modal : true , 
            height: 350,
            width: 300, 
            layout: 'fit',
            items : [{
                xtype  : 'form' , 
                url: '/api/account/users/merge_new',
                id :  'user-form' , 
                waitTitle : 'Saving data.' ,
                height : '300' , 
                bodyStyle : 'padding : 15px' , 
                border : false , 
                items  : [{
                    xtype : 'displayfield' , 
                    html : '<img src="/images/monoloop_logo_clear.gif" /><br><br><br>'  
                },{
                    xtype : 'textfield' , 
                    name : 'user[name]' , 
                    id : 'username' , 
                    fieldLabel : 'Username' , 
                    allowBlank : false 
                } , 
               {
                xtype: 'fieldcontainer',
                hideLabel: true,
                combineErrors: true,
                msgTarget : 'side',
                layout: 'hbox',
                defaults: {
                    flex: 1,
                    hideLabel: true
                },
                items: [
                    {
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    text : 'Check available' ,  
                    handler : function(){
                        if( Ext.String.trim( Ext.getCmp('username').getValue()  ) == ''){ 
                            Ext.getCmp('username').markInvalid('Username can not empty.') ; 
                            return ;  
                        }
                        
                        Ext.getCmp('user-form').setLoading('Checking...') ; 
                        Ext.Ajax.request({
                            url: '/api/users/check',
                            params: { 
                                user : Ext.getCmp('username').getValue()  
                            },
                            success: function(response){
                                Ext.getCmp('user-form').setLoading(false) ; 
                                var jsonData = Ext.JSON.decode(response.responseText) ; 
                                Ext.getCmp('user-check-result').setValue(jsonData.msg) ;  
                            }
                        }) ; 
                    }
                },
                    {
                        xtype     : 'displayfield',  
                        id        : 'user-check-result' , 
                        html      : '' , 
                    }
                ]
            }, {
                    xtype : 'textfield' , 
                    name : 'user[password]' , 
                    id : 'password' , 
                    fieldLabel : 'Password' , 
                    minLength : 6 , 
                    inputType: 'password', 
                    allowBlank : false 
                } ,  
                {
                    xtype : 'textfield' , 
                    name : 'user[repassword]' , 
                    id : 'repassword' , 
                    fieldLabel : 'Retype Password' , 
                    minLength : 6 , 
                    inputType: 'password', 
                    allowBlank : false , 
                    validator : function(){
                        if( Ext.getCmp('repassword').getValue() != Ext.getCmp('password').getValue()){
                            return 'Type password miss match' ; 
                        }
                        return true ; 
                    }
                },{
                    xtype : 'textfield' , 
                    name : 'user[fullname]' , 
                    id : 'name' , 
                    fieldLabel : 'Name' , 
                    allowBlank : false  , 
                    value : invitation.name 
                },{
                    xtype : 'textfield' , 
                    name : 'user[email]' , 
                    id : 'email' , 
                    fieldLabel : 'Email' , 
                    vtype : 'email' , 
                    allowBlank : false  , 
                    value : invitation.email 
                },{
                    xtype : 'hidden' , 
                    name : 'user[comment]' , 
                    id : 'comment' ,    
                    value : invitation.comment 
                } , 
               {
                  xtype : 'button' , 
                  text : 'Save' , 
                  cls : 'monoloop-submit-form' , 
                  style: {
                    marginLeft: '103px'
                  },
                  handler : function(){ 
                    var form = Ext.getCmp('user-form').getForm();
                    if (form.isValid()) {
                      Ext.getCmp('user-form').setLoading('Saving...') ; 
                      // Submit the Ajax request and handle the response
                      form.submit({
                          
                        success: function(form, action) { 
                         Ext.getCmp('user-form').setLoading(false) ; 
                         Ext.getCmp('i-window-1').close() ; 
                         
                         invitation.loadLinkTomainWindow() ; 
                        },
                        failure: function(form, action) {
                          Ext.Msg.alert('Failed', action.result.msg);
                          Ext.getCmp('user-form').setLoading(false) ; 
                        }
                      });
                    }
                  }
                }
                ]  
            }] , 
        }).show();
    } , 
    
    loginuser: function(){
        Ext.create('Ext.window.Window', {
            id : 'i-window-2' , 
            title: 'Login as exists user',
            resizable : true , 
            modal : true , 
            height: 350,
            width: 300, 
            layout: 'fit',
            items : [{
                xtype  : 'form' , 
                url: '/api/account/users/merge',
                id :  'user-form' , 
                waitTitle : 'Saving data.' ,
                height : '300' , 
                bodyStyle : 'padding : 15px' , 
                border : false , 
                items  : [{
                        xtype : 'displayfield' , 
                        html : '<img src="/images/monoloop_logo_clear.gif" /><br><br><div></div><br><br>'  
                    },{
                        xtype : 'textfield' , 
                        name : 'user[name]' , 
                        id : 'username' , 
                        fieldLabel : 'Username' , 
                        allowBlank : false 
                    }, {
                        xtype : 'textfield' , 
                        name : 'user[password]' , 
                        id : 'password' , 
                        fieldLabel : 'Password' , 
                        minLength : 6 , 
                        inputType: 'password', 
                        allowBlank : false 
                    }, {
                        xtype : 'textfield' , 
                        name : 'user[fullname]' , 
                        id : 'name' , 
                        fieldLabel : 'Name' , 
                        allowBlank : false  , 
                        value : invitation.name 
                    },{
                        xtype : 'textfield' , 
                        name : 'user[email]' , 
                        id : 'email' , 
                        fieldLabel : 'Email' , 
                        vtype : 'email' , 
                        allowBlank : false  , 
                        value : invitation.email 
                    },{
                        xtype : 'hidden' , 
                        name : 'user[comment]' , 
                        id : 'comment' ,    
                        value : invitation.comment 
                    }, {
                        xtype : 'button' , 
                        cls : 'monoloop-submit-form' , 
                        text : 'Save' , 
                        style: {
                            marginLeft: '103px'
                        },
                        handler : function(){
                            
                            var form = Ext.getCmp('user-form').getForm();
                            if (form.isValid()) {
                                // Submit the Ajax request and handle the response
                                Ext.getCmp('user-form').setLoading('Saving...') ; 
                                form.submit({
                                    
                                    success: function(form, action) {
                                       Ext.getCmp('user-form').setLoading(false) ; 
                                       Ext.getCmp('i-window-2').close() ; 
                                       
                                       invitation.loadLinkTomainWindow() ; 
                                    },
                                    failure: function(form, action) {
                                        Ext.Msg.alert('Failed', action.result.msg);
                                        Ext.getCmp('user-form').setLoading(false) ; 
                                    }
                                });
                            }
                        }
                    }
                ]
            }
            ]
        }).show() ; 
    } , 
    
    loadLinkTomainWindow : function(){
       Ext.create('Ext.window.Window', {
        id : 'i-window-3' ,  
        resizable : true , 
        headerPosition : 'left' , 
        modal : true , 
        height: 150,
        width: 300, 
        layout: 'fit',
        items : [{
          xtype  : 'form' , 
          height : '300' , 
          bodyStyle : 'padding : 15px' , 
          border : false , 
          items  : [{
            xtype : 'displayfield' , 
            width : '100%' , 
            html : '<img src="/images/monoloop_logo_clear.gif" /><br><br>'   
          }, 
          {
            xtype : 'displayfield' , 
            width : 250 , 
            html : '<div>Complete . <br><a href="'+invitation.baseURL+'">Go to monoloop ui</a></div>' 
          }
          ] 
        }]
       }).show() ; 
    }
} ; 