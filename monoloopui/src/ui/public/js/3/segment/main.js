var segment_main = {
	clearPageIndex : false ,
	treeObj : null ,    
	
	uid : 0 ,
	name : '' , 
	description : '' , 
	condition : '' , 
	
	init : function(){
		var me = this ; 
		me.clearPageIndex = false ; 
		
		Ext.QuickTips.init() ;
        me.initCreateBtn() ; 
        me.initPageTree() ; 
   		me.initGrid() ; 
        
        var tabPanel = new Ext.TabPanel({
            activeTab: 0,
            width: '100%',
            plain:true,
            defaults:{autoScroll: true}, 
	        height:570,
            renderTo: 'mdc-content' , 
            items : [
            {
                title : 'Segments' , 
                id : 'mdc-pagelist' , 
                layout: 'border' ,    
                items :[
                {
                    region: 'west',
        			title: 'Folder',
        	        split: true,
        	        width: 215,
        			margins:'0',
        			style : 'border-right : 1px solid rgb(221,221,221)' , 
        			border : false , 
        	        cmargins:'0',
        	        collapsible: true,
                    tbar: [
                        {  
                            cls : 'tree-button' , 
        			        iconCls: 'new-symbol1',
                            tooltip: 'Create new folder',
        			        handler: function(){
                        		//goal_main.goalTreeObj.openCreateFolderWindow();
        			        }
        			    }, { 
        			        cls : 'tree-button' , 
        			        iconCls: 'edit-symbol1',
                            tooltip: 'Edit folder',
        			        handler: function(){
        			    		//goal_main.goalTreeObj.openEditFolderWindow();
        			        }
        			    }, { 
        			        cls : 'tree-button' , 
        			        iconCls: 'delete-symbol1',
                            tooltip: 'Delete folder',
        			        handler: function(){
        			    		//goal_main.goalTreeObj.openDeleteFolderWindow();
        			        }
        			    }
                    ] , 
                    items :[ me.treeObj.tree]
                } , {
                    region: 'center',
                    margins:'3 3 3 0', 
                    layout : 'fit' ,
	                header : false ,
	                border : false , 
                    items : [ Ext.getCmp('segments-main-grid') ]
                }
                ]
            }]
        }) ; 
	} , 
	
	initCreateBtn : function(){
		var me = this ; 
		var btn = new Ext.Button({
            text : 'Create new segment' ,
            id : 'btn-new-segment' ,
            iconCls : 'newpage-icon' ,  
            height : 23 , 
            cls : 'tabcreate-btn' , 
            renderTo : 'btn-new-segment' ,
            handler : function(){
              me.uid = 0 ; 	
              me.showEditWindow(0) ; 
            }
        }) ; 
        
        var searchBox = new Ext.form.CompositeField({
			width : 203 ,
			renderTo : 'search-text' ,
			items : [{
				xtype : 'textfield' , 
				width : 180 , 
				id : 'serach-textbox' , 
				emptyText : 'Enter Search Keyword' , 
				enableKeyEvents : true , 
	            listeners : {
	                'keypress' : function(textField ,e){
	                    if(e.keyCode == e.ENTER) {  
	                    }
	                }
	            }
			},{
				xtype : 'button' , 
				cls : 'search-btn' , 
				style : 'margin : 0 0 0 -5px ; ' , 
				width : 23 , 
				handler : function(){  
				}
			}]
		})  ;
        /*
        var textBox = new Ext.form.TextField({
            width : 200 ,
            id : 'serach-textbox' , 
            emptyText : 'search' , 
            renderTo : 'search-text' ,
            enableKeyEvents : true , 
            listeners : {
                'keypress' : function(textField ,e){
                    if(e.keyCode == e.ENTER) { 
                    	 
                        goal_main.clearPageIndex = true ;  
                        goal_grid.refrehGrid() ; 
                        goal_main.clearPageIndex = false ;   
                        
                    }
                }
            }
        }) ; 
        */
	} , 
	
	initGrid : function(){
		segment_grid.init_maingrid() ; 
	} , 
	
 	initPageTree : function(){
 		var me = this ; 
 		me.treeObj = new monoloopTreeBase(0 , 'segments') ; 
 	} , 
 	
 	showEditWindow : function(uid){
 		var me = this ; 
 		
 		var panel = new Ext.FormPanel({
            width:  '100%' , 
            height:160,
            id : 'segment-ces-panel' , 
            border : false , 
            bodyBorder : false , 
            bodyCssClass : 'cse-panel-body' , 
            bodyStyle : 'padding : 10px ;' , 
            defaults : {
            	anchor : '100%'
            } ,  
            items : [  
                {
                    xtype     : 'displayfield',
                    hideLabel : true  ,
                    value      : 'Name:' 
                } ,
                {
                    xtype     : 'textfield', 
                    id        : 'segment-name',
                    hideLabel : true  ,
                    name      : 'name' , 
                    allowBlank : false , 
                    width     : 200 ,
                    allowBlank: false  
                },
                {
                    xtype     : 'displayfield',
                    hideLabel : true  ,
                    value      : 'Description:'  
                } ,
                {
                    xtype     : 'textarea', 
                    hideLabel : true  ,
                    id        : 'segment-description',
                    name      : 'description' ,  
                    allowBlank: false  
                } 
            ]
        }) ; 
 		
 		var window = new Ext.Window({
            width:300,
         	height:240,
            title:"Create new blueprint",
            plain:true , 
         	modal:true,
            maximizable : false  , 
            resizable : false , 
         	closable:true , 
            layout : 'fit' , 
            
            constrain : true , 
            id : 'ces-segment-editwindow' , 
            //baseCls : 'ces-window' ,
            //iconCls : 'ces-window-header-icon' ,   
            items : [panel] ,  
            buttons: [{
	            text:'NEXT' , 
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
	                var form = Ext.getCmp('segment-ces-panel') ;
                    if( form.form.isValid() ){
                        // Move to next form ; 
                        me.name = Ext.getCmp('segment-name').getValue() ; 
						me.description = Ext.getCmp('segment-description').getValue() ; 
						if( uid == 0){
							
							window.close() ; 
							condition.stage = 24 ;  
							condition.openMainWindow() ; 
						}else{
							// Save to DB ;
							window.close() ; 
							me.saveData() ; 
						}
                    } 
	            }
	        } ,   
            {
	            text:'CANCEL' ,
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
					window.close() ;
	            }
	        } ] , 
            listeners : {
                'show' : function(w){ 
                    if(uid != 0){
                    	w.buttons[0].setText('Save') ; 
                    	Ext.getCmp('segment-name').setValue(me.name) ; 
                    	Ext.getCmp('segment-description').setValue(me.description) ; 
                    }
                } , 
                'close' : function(panel){
                    //;
                }
            }
        }) ;  
        window.show() ; 
 	} , 
 	
 	saveNewCondition : function(condition){
 		var me = this ; 
 		me.condition = condition ;  
 		me.saveData() ; 
 		
 	} , 
 	
 	saveData : function(){
 		var me = this ; 
 		var data = {} ; 
        data['condition'] = me.condition ; 
        data['uid'] = me.uid ;
		data['name'] = me.name ; 
		data['description'] = me.description ; 
		 
        monoloop_base.showProgressbar('Save data...') ; 
        Ext.Ajax.request({
			url: 'index.php?eID=monoloop_segments&pid='+monoloop_base.pid+'&cmd=main/save'  , 
			method: 'POST',
			timeout: '30000',
            params:   data , 
			success: function(xhr) {
			     monoloop_base.msg('Save data complete.' , 'Save segment.') ; 
			     segment_grid.refrehGrid() ; 
			     Ext.MessageBox.hide();
            }, failure: function() {
                Ext.MessageBox.hide();
			    Ext.MessageBox.alert('Error', '#1201' ) ; 
			}
        }) ; 
        
 	}
} ; 

var segment_grid = {
	init_maingrid : function(){
		var me = this ; 
		var myStore = new Ext.data.JsonStore({ 
	        root: 'topics',
	        totalProperty: 'totalCount',
	        idProperty: 'uid',
	        remoteSort: true,
	
	        fields: [
	        	{name: 'uid', type: 'int'},
                {name: 'hidden', type: 'int'},
	        	{name: 'pid', type: 'int'}, 
	        	{name: 'type_id' , type : 'int'} , 
	        	{name: 'name'} ,{name: 'description'} , {name : 'conditionstr'} ,
	 			{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'}  
	        ],
	
	        // load using script tags for cross domain, if the data in on the same domain as
	        // this page, an HttpProxy would be better
	        proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=monoloop_segments&pid='+monoloop_base.pid+'&cmd=main/getlist'
	        }),
	        
	        baseParams:{
				idfolder : 0
			}
	    });
	    myStore.setDefaultSort('crdate', 'desc');
	    
	    function renderAction(value, p, r){ 
	    	var property = '<td><div onClick="segment_grid.property(\''+r.data['uid']+'\')" title="Condition" class="placement-edit"></div></td>' ;
	        var edit = '<td><div onClick="segment_grid.edit(\''+r.data['uid']+'\')" title="Edit" class="placement-property"></div></td>' ;
	        
	        if( r.data['type_id'] > 0){
       			edit = '<td>&nbsp;</td>' ; 
	        }
			
			var deleteData = '<td><div onClick="segment_grid.deleteRec(\''+r.data['uid']+'\')" title="Delete" class="placement-delete"></div></td>' ; 
	    	var ret =  '<table width="100%" ><tr align="center"> '  
	    				 + property
	                     + edit  
	    			     + deleteData
	                     +
					   '</tr></table>'  ;
	
	 
			return ret ;   
	    }
	    
	    function renderStatus(value, p, r){
            var hidden = '' ;
            if( r.data['hidden'] == 0 ){
				hidden = '<td><div onClick="segment_grid.setHidden('+r.data['uid']+' , 1)"  class="placement-active"></div></td>' ; 
			}else{
				hidden = '<td><div onClick="segment_grid.setHidden('+r.data['uid']+' , 0)"   class="placement-inactive" ></div></td>' ; 
			}
            	var ret =  '<table width="100%" ><tr align="center"> ' 
						 + hidden +
					   '</tr></table>'  ; 
			return ret ;
        }
        
        var myGrid = new Ext.grid.GridPanel({
	        width:'100%',
	        height:530,
	        //title:'Simple newsletter listing',
	        store:  myStore,
	        trackMouseOver:false,
	        //disableSelection:false,
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
	        enableDragDrop: true,
			ddGroup: 'grid2tree',
	        //autoHeight: true,
            id : 'segments-main-grid' , 
	        // grid columns
	        columns:[{
	            header: "Name",
	            dataIndex: 'name',
	            width: 50,
	            sortable: true 
	            //
	        }, {
	            header: "Description",
	            width: 100,
	            dataIndex: 'description',
	            sortable: true
	        }, 
	        {
	            id: 'crdate', // id assigned so we can apply custom css (e.g. .x-grid-col-topic b { color:#333 })
	            header: "Date",
	            dataIndex: 'crdate',
	            width: 40,
	            renderer: Ext.util.Format.dateRenderer('d M Y'),
	            sortable: true
	        } ,{
	            header: "Actions",
                id : 'actions' , 
	            renderer: renderAction, 
	            dataIndex: 'uid',
	            width: 40,
	            sortable: false
	        } ,{
	           header : 'Status' , 
               width : 30 , 
               id : 'status' , 
               renderer: renderStatus, 
               dataIndex: 'hidden',
               sortable: true
	        } ],
	
	        // customize view config
	       
	        viewConfig: {
	            forceFit:true,
	        }, 
	
	        // paging bar on the bottom
	        bbar: new Ext.PagingToolbar1({
                displayMsg : 'Displaying {0} of {1}' ,
                id : 'segments-paging-toolbar' , 
	            pageSize: 15,
	            store: myStore,
	            displayInfo: true,
	            plugins: new Ext.ux.ProgressBarPager() ,
	            emptyMsg: "No topics to display"  
	        })
	    });
        me.refrehGrid() ; 
	}   , 
	
	refrehGrid : function(){
		Ext.getCmp('segments-main-grid').store.baseParams = {
            'search' : Ext.getCmp('serach-textbox').getValue() , 
            'idfolder' : segment_main.treeObj.idFolder
        }
        if( segment_main.clearPageIndex == true )
             Ext.getCmp('segments-main-grid').store.load({params:{start: 0 , limit:15}});
        else 
             Ext.getCmp('segments-main-grid').store.load({params:{start:Ext.getCmp('segments-paging-toolbar').cursor , limit:15}}); 
	} , 
	
	property : function(uid){
		
		var rec = Ext.getCmp('segments-main-grid').getStore().getById(uid);
		segment_main.uid = uid ; 
 		segment_main.name = rec.get('name') ; 
 		segment_main.description = rec.get('description') ; 
		condition.preconditionStr = rec.get('conditionstr') ; 
		condition.stage = 24 ;  
		condition.openMainWindow() ; 
		
		
	} , 
	
	edit : function(uid){
		var rec = Ext.getCmp('segments-main-grid').getStore().getById(uid);
		segment_main.uid = uid ; 
 		segment_main.name = rec.get('name') ; 
 		segment_main.description = rec.get('description') ; 
 		segment_main.condition = rec.get('conditionstr') ; 
 		segment_main.showEditWindow(uid) ; 
	} , 
	
	deleteRec : function(uid){ 
		var me = this ;
        Ext.MessageBox.confirm('Confirm', 'Are you sure to delete this record ?', function(btn){
        	var data = {} ; 
	        if( btn == 'yes'){
	            data['uid'] = uid ; 
	            monoloop_base.showProgressbar('Deleting data...') ; 
	            Ext.Ajax.request({
	    			url: 'index.php?eID=monoloop_segments&pid='+monoloop_base.pid+'&cmd=main/delete'  , 
	    			method: 'POST',
	    			timeout: '30000',
	                params:   data , 
	    			success: function(xhr) { 
	    			     monoloop_base.msg('Delete data complete.' , 'Delete segment.') ; 
	    			     Ext.MessageBox.hide();
	                     me.refrehGrid() ; 
	                }, failure: function() {
	                    Ext.MessageBox.hide();
	    			    Ext.MessageBox.alert('Error', '#1201' ) ; 
	    			}
	            }) ; 
	        }
        }) ;
	} , 
	
	setHidden : function(uid , hidden){
		var me = this ; 
		var data = {} ; 
		data['uid'] = uid ; 
		data['hidden'] = hidden ;
		
		monoloop_base.showProgressbar('Updating data...') ; 
        Ext.Ajax.request({
			url: 'index.php?eID=monoloop_segments&pid='+monoloop_base.pid+'&cmd=main/hidden'  , 
			method: 'POST',
			timeout: '30000',
            params:   data , 
			success: function(xhr) { 
			     monoloop_base.msg('Update data complete.' , 'Update segment.') ; 
			     Ext.MessageBox.hide();
                 me.refrehGrid() ; 
            }, failure: function() {
                Ext.MessageBox.hide();
			    Ext.MessageBox.alert('Error', '#1201' ) ; 
			}
        }) ; 
	}
	
	
}