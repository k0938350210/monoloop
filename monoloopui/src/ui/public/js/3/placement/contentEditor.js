define(['require','extjs/plugins/FileUploadField','extjs/plugins/Multiselect','extjs/plugins/SuperBoxSelect','extjs/plugins/Ext.ux.ColorField','extjs/plugins/ext.ux.color3'],function (require) {
// --- start
 

window.pwContentEditor = {
    editWindow : null , 
    editForm : null , 
    toUid : 0 , 
    contentId : 0  , 
    elementName : '' , 
    condition : '' , 
    returnType : 0 ,  // 0 : placement select grid , 1 : test bench data , 2 : content edit , 3 : placement property
    additionalRecord : null , 
    
    flexFormData : null , 
    disableCondition : false , 
    
    openMainWindow : function(toUid , contentId , returnType){
 		var me = this ; 
        pwContentEditor.toUid = toUid  ; 
        pwContentEditor.contentId = contentId ;
        pwContentEditor.returnType = returnType ;  
        pwContentEditor.initFormData(toUid) ; 
        pwContentEditor.condition = '' ;  
        pwContentEditor.editWindow = new Ext.Window({
            autoScroll : true , 
            width:500,
         	height: 'auto',
            title:"",
            id : 'content-editor-window' , 
            //baseCls : 'ces-window' ,
            //iconCls : 'ces-window-header-icon' , 
            plain:true ,       
         	modal: true,
         	maximizable : true  , 
         	closable:true , 
         	constrain : true , 
            items : [pwContentEditor.editForm] , 
            bodyStyle : 'padding : 5px 15px 15px 15px;background-color: white ; ' , 
            style : '' , 
            buttons: [ 
            {
	            text:'CREATE CONDITION...'  , 
                hidden : me.disableCondition , 
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
					condition.stage = 3 ; 
                    condition.preconditionStr = pwContentEditor.condition ; 
                    condition.openMainWindow() ; 
	            } , 
                listeners : {
                    'render' : function(btn){
                        if( pwContentEditor.contentId != 0){
                            btn.setText('EDIT CONDITION...') ; 
                        }
                    }
                }
	        },
            {
	            text:'PREVIEW'  ,
	            cls: 'monoloop-black-btn' , 
                iconCls : 'pw-menu-inspect-content' , 
	            handler: function(){
				    pwContentEditor.processPreview() ; 
	            }
	        },
            {
	            text:'CANCEL'  ,
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
					pwContentEditor.editWindow.close() ;
                    pwContentEditor.editWindow.destroy() ; 
                    pwMain.clearIframeSelection() ; 
	            }
	        },{
	            width : 40 , 
	            text:'OK'  ,
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
					me.saveForm() ; 
	            }
	        }] ,
            listeners : {
                'afterrender' : function(w){
                    var innerTotal =   pwContentEditor.editForm.items.getCount() ; 
                    var maxWidth = 0 ; 
                    for( var i = 0 ; i < innerTotal ; i++){
                        var curW = pwContentEditor.editForm.getComponent(i).getWidth() ; 
                        if(maxWidth < curW){
                            maxWidth = curW ; 
                        }
                    }  
                    
                    // Set window width ; 
                    if( maxWidth + 45 > 380)
                        pwContentEditor.editWindow.setWidth(maxWidth + 45) ; 
                        
                    var texts = pwContentEditor.editWindow.findByType('textfield') ;
					for( var i = 0 ; i < texts.length ; i++ ){
						texts[i].addListener('keypress' , function(textField ,e){
							if(e.keyCode == e.ENTER) {
                                me.saveForm() ; 
                            }
						}); 
					} 
                },
                'show' : function(w){
                     ;
                }, 
                'close' : function(panel){
                    pwMain.clearIframeSelection() ; 
                }
            }
        }) ; 
        pwContentEditor.initFormData2(toUid) ; 
    }  , 
    
    
    saveForm : function(){
    	var me = this ; 
    	Ext.Ajax.timeout = 300000 ; 
        var data =  pwContentEditor.retrieveDataForSubmit() ; 
        
        var frms = pwContentEditor.editForm.findByType('htmleditor') ; 
        for( var i = 0 ; i < frms.length ; i++ ){
            var t = frms[i].getValue()  ; 
        }
        
        if(pwContentEditor.editForm.getForm().isValid()){
            //alert('Yes . We pass validate ; ') ; 
            if(pwMain.crossDomain){
                monoloop_base.showProgressbar('Save data...') ;  
                var fromDatas = pwContentEditor.editForm.getForm().getValues() ;
                for (var attrname in fromDatas) { data[attrname] = fromDatas[attrname]; }
                data['eID'] = 't3p_dynamic_content_placement' ; 
                data['pid'] = 1 ; 
                data['cmd'] = 'saveContentEditor' ; 
                data['type'] = pwContentEditor.returnType ; 
                data['time'] = new Date().getTime();
                Ext.util.JSONP.request({
                    url: pwMain.baseUrl + 'index.php',
                    callbackKey: 'callback',
                    params : data , 
                    callback: function(result) {
                        Ext.MessageBox.hide();
                        pwContentEditor.saveContentEditor_success(result.contentId) ; 
                    }
                });
            }else{
                pwContentEditor.editForm.getForm().submit({
                    url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=saveContentEditor&type='+pwContentEditor.returnType,
                    waitMsg: 'Saving data...',
                    params : data ,
                    success: function(simple, o){
                    	 //alert('Save success') ;
                         //console.debug(o) ; 
                         //alert(o.result.data) ; 
                         pwContentEditor.saveContentEditor_success(o.result.contentId) ; 
                    } ,
                    failure: function(simple, o) {  
			             Ext.MessageBox.alert('Error', o.result.msg ) ; 
                    	
	    			}
                });
            }
            
        }
    } , 
    
    saveContentEditor_success : function(contentId){
        pwContentEditor.editWindow.close() ;
        pwContentEditor.editWindow.destroy() ;
        monoloop_base.msg('Success', 'New monoloop content has been created') ;
        //pwMain.myStore.load() ;
        if( pwContentEditor.returnType == 4 ){  
            preplaced.refreshPreplacedPanel(pwContentEditor.contentId) ; 
        } 
                
        if(pwContentEditor.returnType == 2){
            mdc_contentmanager.refreshContentGrid() ; 
        }else if(pwContentEditor.returnType == 5){
            contentEditorCE.UpdateCEGrid(contentId) ; 
        }else{
            pwMain.reStartContentGrid() ;
        } 
        
        if(pwContentEditor.returnType == 1){
            testbenchTab.refreshTab() ; 
        }
    } , 
    
    before_retrieveDataForSubmit : function(){
    	return {} ;
    } , 
    
    retrieveDataForSubmit : function(){
    	var me = this; 
        var data = me.before_retrieveDataForSubmit() ; 
        //data['name'] = pwContentEditor.elementName ;
        data['contentId'] = pwContentEditor.contentId ;
        data['condition'] = pwContentEditor.condition ; 
        data['toUid'] = pwContentEditor.toUid ; 
        if(pwContentEditor.returnType == 2 || pwContentEditor.returnType  == 5){
            data['onlyContent'] = 1 ; 
        }else{
            data['url'] = Ext.getCmp('pw-url').getValue();
            data['selector'] = pwMain.selectorTxt ; 
            data['beforeAfter'] = selectTab.beforeAfter ; 
        }
        return data ; 
    } ,
    
    processPreview : function(){
        var data =  pwContentEditor.retrieveDataForSubmit() ;  
                    
        if(pwContentEditor.editForm.getForm().isValid()){
            var frms = pwContentEditor.editForm.findByType('htmleditor') ; 
            for( var i = 0 ; i < frms.length ; i++ ){
                var t = frms[i].getValue()  ; 
            }
            //alert('Yes . We pass validate ; ') ; 
            if( pwMain.crossDomain){
                monoloop_base.showProgressbar('Save data...') ;  
                var fromDatas = pwContentEditor.editForm.getForm().getValues() ;
                for (var attrname in fromDatas) { data[attrname] = fromDatas[attrname]; }
                data['eID'] = 't3p_dynamic_content_placement' ; 
                data['pid'] = pwMain.pid ; 
                data['cmd'] = 'saveContentEditor' ;  
                data['time'] = new Date().getTime() ; 
                Ext.util.JSONP.request({
                    url: pwMain.baseUrl + 'index.php',
                    callbackKey: 'callback',
                    params : data , 
                    callback: function(result) {
                        Ext.MessageBox.hide();
                        pwContentEditor.contentId = result.contentId ; 
                        ml_preview_content.openMainWindow(pwContentEditor.contentId) ;  
                    }
                });
            }else{
                pwContentEditor.editForm.getForm().submit({
                    url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=saveContentEditor',
                    waitMsg: 'Process data...',
                    params : data ,
                    success: function(simple, o){
                    	 //alert('Save success') ;
                         //console.debug(o) ; 
                         //alert(o.result.data) ; 
                         pwContentEditor.contentId = o.result.contentId ; 
                         ml_preview_content.openMainWindow(pwContentEditor.contentId) ; 
                    } ,
                    failure: function(simple, o) { 
    					Ext.MessageBox.alert('Error', '#321' ) ; 
        			}
                });
            }
            
        }
    } ,
    
    test : function(){
        alert('xxxx') ; 
    } , 
    
    initFormData : function(){
    	var me = this ; 
        pwContentEditor.editForm = new Ext.FormPanel({
            fileUpload: false,
            width:  '100%' ,
            autoHeight : true ,  
            id : 'content-element-form' , 
            border : true , 
            bodyBorder : true ,  
            bodyStyle : 'padding : 15px;' , 
            layout : 'form' , 
            defaults : {
            	anchor : '100%'
            } , 
            items: [ ]
        }) ; 
        // Test ;
        var data = {} ;
        data['toUid'] = pwContentEditor.toUid ; 
        //alert(pwContentEditor.contentId ) ; 
        data['contentId'] = pwContentEditor.contentId ; 
        
        monoloop_base.showProgressbar('Initializing...') ;  
        
        
        if(pwContentEditor.returnType == 3){
 
            // Insert placement monoloop property field ; 
            var myLabel = new Ext.form.Label({
                text      : 'X-Path'  , 
                style     : 'color: rgb(20,65,138) ; font-weight: bold'
            }) ;
            pwContentEditor.editForm.add(myLabel) ;
            
            var field = new Ext.form.TextField({ 
                name : 'type3[element_id]'  , 
                ctCls  : 'cef-input'    , 
                hideLabel : true , 
                enableKeyEvents : true , 
                value : pwContentEditor.additionalRecord.get('element_id') 
            }) ;
            //console.debug(pwContentEditor.additionalRecord ) ; 
            pwContentEditor.editForm.add(field) ;  
 
            
            var myLabel = new Ext.form.Label({
                text      : 'Position' , 
                style     : 'color: rgb(20,65,138) ; font-weight: bold'
            }) ;
            pwContentEditor.editForm.add(myLabel) ;
            
            var field = new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all' ,  
                mode: 'local',
                allowBlank: true  ,
                editable: false,
                hiddenName: 'type3[beforeAfter]' ,  
                width : 290 , 
                displayField:   'name',
                valueField:     'value' , 
                hideLabel : true , 
                store : pwMain.beforeAfterStore , 
                value : pwContentEditor.additionalRecord.get('befor_after')
            }) ;
            pwContentEditor.editForm.add(field) ;
            
            var myLabel = new Ext.form.Label({
                text      : 'Display Type' , 
                style     : 'color: rgb(20,65,138) ; font-weight: bold'
            }) ;
            pwContentEditor.editForm.add(myLabel) ; 
            
            var field = new Ext.form.ComboBox({ 
                typeAhead: true,
                triggerAction: 'all' ,  
                mode: 'local',
                allowBlank: true  ,
                editable: false,
                hiddenName: 'type3[displayType]' ,  
                width : 290 , 
                displayField:   'name',
                valueField:     'value' , 
                hideLabel : true , 
                store : pwMain.displayTypeStore, 
                value : pwContentEditor.additionalRecord.get('display_type')
            }) ;
            pwContentEditor.editForm.add(field) ;  
            
            var field = new Ext.form.Hidden({
                name : 'type3[uid]' , 
                value : pwContentEditor.additionalRecord.get('uid') 
            }) ; 
            pwContentEditor.editForm.add(field) ;  
            
            var myLabel = new Ext.form.Label({
                text      : 'Addition JS' , 
                style     : 'color: rgb(20,65,138) ; font-weight: bold'
            }) ;
            pwContentEditor.editForm.add(myLabel) ; 
            
            var field = new Ext.form.TextArea({
                hideLabel : true , 
                allowBlank: true  , 
                width : 290 , 
                name: 'type3[additionJS]' ,  
                value : pwContentEditor.additionalRecord.get('additionJS')
            }) ;
            pwContentEditor.editForm.add(field) ;
            
            var field = new Ext.form.DisplayField({
                value : '<hr>'    , 
                hideLabel : true  
            })  ;
            pwContentEditor.editForm.add(field) ;  
            
            pwContentEditor.editForm.doLayout() ; 
        }
        
        
    }   , 
    
    
    initFormData2 : function(uid){
        var data = {} ;
        data['toUid'] = pwContentEditor.toUid ; 
        //alert(pwContentEditor.contentId ) ; 
        data['contentId'] = pwContentEditor.contentId ; 
        
        if( pwMain.crossDomain){
            data['eID'] = 't3p_dynamic_content_placement' ; 
            data['pid'] = pwMain.pid ; 
            data['cmd'] = 'getContentEditorForm' ; 
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data , 
                callback: function(jsonRoot) {  
                    pwContentEditor.getContentEditorForm_success(jsonRoot) ; 
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=getContentEditorForm'  , 
                //url : 'testxml.php' , 
    			method: 'POST',
    			params:   data ,
    			timeout: '30000',
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
    			    var jsonRoot = Ext.util.JSON.decode(xhr.responseText) ;
     				pwContentEditor.getContentEditorForm_success(jsonRoot) ; 
    			},
    			failure: function() {
    				Ext.MessageBox.hide();
    				Ext.MessageBox.alert('Error', '#322' ) ; 
    			}
    		});	
        } 
    } , 
    
    getContentEditorForm_success : function(jsonRoot){
        var me = this ; 
        var jsonData = jsonRoot.content ; 
       
        if( jsonRoot.display == 1){
            
            contentEditorCE.toUid = me.toUid ; 
            contentEditorCE.contentId = me.contentId ; 
            contentEditorCE.elementName = me.elementName ; 
            contentEditorCE.condition = me.condition ; 
            contentEditorCE.returnType = me.returnType ; 
            contentEditorCE.additionalRecord = me.additionalRecord ; 
            contentEditorCE.title = jsonRoot.exttemplate ; 
            pwContentEditor.editWindow.close() ;    
            Ext.MessageBox.hide(); 
            
            contentEditorCE.openWindow(jsonRoot) ; 
            return ; 
        }else if( jsonRoot.extratype == 1){
            assetElement.toUid = me.toUid ; 
            assetElement.contentId = me.contentId ; 
            assetElement.elementName = me.elementName ; 
            assetElement.returnType = me.returnType ; 
            assetElement.condition = me.condition ; 
            if(  me.returnType == 3){
            	me.elementName = me.additionalRecord.get('header') ; 
            } 
            assetElement.openWindow(me.elementName) ;
            return ; 
        } 
		
		if( jsonRoot.hideName == true && pwContentEditor.elementName == 'Remove'){
			pwContentEditor.elementName = 'Random ' +( pwMain.myStore.getTotalCount() + 1 ) ;
		}    
        
        if( jsonRoot.title != ''){
        	var helpText = jsonRoot.titleHelp ;
			var title =  '<b>' + jsonRoot.title + '</b> Content Element ' ; 
        	if( jsonRoot.title_custom != '' ){
        		title = jsonRoot.title_custom  + ' '; 
        	} 
        	
        	pwContentEditor.editWindow.insert(0 , {
        		xtype : 'displayfield' , 
        		hideLabel : true , 
        		value : title + '<img title="'+helpText+'" src="./typo3conf/ext/t3p_base/image/info.GIF" /> ' ,
        		style : 'font-family: \'PT Sans\';font-size: 18px;color:  rgb(20,65,138) ; margin-bottom:5px;'
        	})
        }
        
        pwContentEditor.editForm.add({
        	xtype : 'displayfield' , 
        	value : '<b>Content Element Name</b>' , 
        	hideLabel : true , 
        	hidden : jsonRoot.hideName , 
        	style 	  : 'color: rgb(239,145,0) ;'
        }) ; 
        
        pwContentEditor.editForm.add({
        	xtype : 'textfield' ,
			enableKeyEvents : true , 
			hidden : jsonRoot.hideName ,  
        	value : pwContentEditor.elementName , 
        	hideLabel : true , 
        	name  : 'name'
        }) ;
       
                for( i = 0 ; i < jsonData.length ; i++){
                	var helpText = '' ; 
 					if( jsonData[i].labelHelp != ''){
 						helpText = '&nbsp;<img style="vertical-align: middle;"  src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="' + jsonData[i].labelHelp + '" />' ;  
 					}
 					
 					var fhelpText = '' ; 
 					if( jsonData[i].labelFHelp != ''){
 						fhelpText = '<img style="vertical-align: middle;" src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="' + jsonData[i].labelFHelp + '" />&nbsp;' ;  
 					}
 					
                    //alert(jsonData[i].type) ;
                    var myLabel = new Ext.form.DisplayField({
                        value      : fhelpText+jsonData[i].label+helpText , 
                        hideLabel : true , 
                        id        : jsonData[i].el + '_label'  , 
                        style 	  : 'color: rgb(20,65,138) ;'
                    }) ;
                    pwContentEditor.editForm.add(myLabel) ;  
                    
                    // Add form element here .
                    var myField = null ;  
                    if( jsonData[i].type == 'input'){
                        myField = new Ext.form.TextField({
                            id : jsonData[i].el , 
                            name : 'cef[' + jsonData[i].el + ']' , 
                            value : jsonData[i].value  , 
                            hideLabel : true , 
                            enableKeyEvents : true , 
                            ctCls  : 'cef-input'  
                        }) ; 
                    }else if(jsonData[i].type == 'text'){
                        myField = new Ext.form.TextArea({
                            id : jsonData[i].el , 
                            name : 'cef[' + jsonData[i].el + ']' , 
                            value : jsonData[i].value  , 
                            hideLabel : true , 
                            anchor : '100%' ,
                            ctCls  : 'cef-input'  ,
                            width : jsonData[i].cols *8 ,
                            height : jsonData[i].rows * 16  
                        }) ; 
                    }else if(jsonData[i].type == 'rte'){
                        myField = new Ext.form.HtmlEditor({
                            autoScroll : true , 
                            id : jsonData[i].el , 
                            name : 'cef[' + jsonData[i].el + ']' , 
                            loadExternalCss : false , 
                            hideLabel : true ,   
                            height: 230 ,  
                            anchor : '100%' ,
                            value : jsonData[i].value , 
                            plugins       : Ext.ux.form.HtmlEditor.pluginsMail() , 
                            listeners :{
                                'initialize' : function(panel){ 
                                }, 
                    
                                'push' : function(myHTMLEditor , html ){  
                                    var $ = myHTMLEditor.getDoc(); // shortcut 
                                    if (myHTMLEditor.loadExternalCss == false)
                                    {
                                        var jsonData = Ext.util.JSON.decode( monoloop_base.cssDomain) ;
                                        for( var i = 0 ; i < jsonData.length ; i++){
                                            var head  = $.getElementsByTagName('head')[0];
                                            var link  = $.createElement('link'); 
                                            link.rel  = 'stylesheet';
                                            link.type = 'text/css';
                                            link.href = jsonData[i];
                                            link.media = 'all';
                                            head.appendChild(link);
                                        }
                                        
                                        myHTMLEditor.loadExternalCss = true ;  
                                    }  
                                    var bodyElement = myHTMLEditor.getEditorBody() ; 
                                    varionHelper.initConditionStr(bodyElement) ;  
                                } , 
                                
                                'sync' : function(myHTMLEditor , html ){ 
                                    myHTMLEditor.el.dom.value = varionHelper.cleanVariation(myHTMLEditor.getEditorBody());
                                }
                            }
                        });
                    }else if(jsonData[i].type == 'file'){
                    	var emptyText = 'Select a file'
                    	if( jsonData[i].emptyText != undefined){
                    		emptyText = jsonData[i].emptyText ; 
                    	}
                        myField = new Ext.ux.form.FileUploadField({
                            id : jsonData[i].el , 
                            name : 'cef[' + jsonData[i].el + ']' , 
                            emptyText: emptyText,
                            hideLabel : true ,  
                            width : 290 , 
                            buttonText: '',
                            buttonCfg: {
                                iconCls: 'upload-icon'
                            } ,
                            value : jsonData[i].value 
                        }) ;
                        if( jsonData[i].value != ''){ 
                            //myLabel.setValue(fhelpText + jsonData[i].label + '(' + jsonData[i].value  + ')'  ) ;
                            myLabel.setValue(fhelpText + jsonData[i].label  ) ;
                        }
                            
                        // Assign value to hidden ;
                        /*
                        var myHidden = new Ext.form.Hidden({
                            name : 'cef[' + jsonData[i].el + '__ov]' , 
                            value : 
                        }) ; 
                        pwContentEditor.editForm.add(myField) ; 
                        */
                    }else if(jsonData[i].type == 'select' ){
                        myJsonStore = new Ext.data.JsonStore({
                            fields : ['k', 'v']  ,
                            autoDestroy: true, 
                            data : jsonData[i].items 
                        }) ; 
                      
                        
                        
                        myField = new Ext.form.ComboBox({ 
                            mode: 'local',
                            triggerAction:  'all', 
                            editable:       false,
                            hideLabel : true ,  
                            id : jsonData[i].el , 
                            hiddenName: 'cef[' + jsonData[i].el + ']' ,  
                            displayField:   'v',
                            valueField:     'k',
                            store : myJsonStore , 
                            width : 290 , 
                            value : jsonData[i].value
                        }) ;
                    }else if(jsonData[i].type == 'check'){
                        myField = new Ext.form.Checkbox({
                            id : jsonData[i].el , 
                            name : 'cef[' + jsonData[i].el + ']' , 
                           	hideLabel : true  
                        }) ; 
                        
                        if(jsonData[i].value){
                            myField.setValue(true) ; 
                        }else{
                            myField.setValue(false) ; 
                        }
                    }
  
                    
                    if(myField != null){
                        pwContentEditor.editForm.add(myField) ;  
                          
                    }
                    
                    var myLineClear = new Ext.form.DisplayField({
                        html : '<div style="clear:both"/>'
                    }) ; 
                    pwContentEditor.editForm.add(myLineClear) ;  
                }
                pwContentEditor.flexFormData = jsonData ; 
                // Process displayCond ;
                for(var i = 0 ; i < jsonData.length ; i++){
                    if( jsonData[i].displayCond != ''){
                        //jsonData[i].el
                        var checkFeildName = jsonData[i].displayCond.field ; 
                        var chkField = Ext.getCmp(checkFeildName) ; 
                        if( chkField == undefined )
                            continue ;
                        if ( chkField.getXType() != 'combo' && chkField.getXType() != 'checkbox' ) 
                            continue ;
  
                             
                        var baseField = Ext.getCmp(jsonData[i].el) ; 
                        if( baseField == undefined)
                            continue ;
                            
                        var baseLabelField = Ext.getCmp(jsonData[i].el +  '_label' ) ; 
                        
                        if( jsonData[i].displayCond.value != chkField.getValue() ){
                            baseField.setVisible(false) ; 
                            baseLabelField.setVisible(false) ; 
                        }else{
                            baseField.setVisible(true) ; 
                            baseLabelField.setVisible(true) ; 
                        }
                        
                        if( chkField.getXType() == 'combo'){
                            chkField.on('select', function(combo , record , index) {
                                for(var i = 0 ; i < pwContentEditor.flexFormData.length ; i++ ){
                                    if( pwContentEditor.flexFormData[i].displayCond != ''){
                                        if( pwContentEditor.flexFormData[i].displayCond.field == combo.getId()){
                                            var baseField = Ext.getCmp(pwContentEditor.flexFormData[i].el) ; 
                                            if( baseField == undefined)
                                                continue ; 
                                            var baseLabelField = Ext.getCmp(jsonData[i].el +  '_label' ) ; 
                                            if ( combo.getValue() == pwContentEditor.flexFormData[i].displayCond.value) {
                                                baseField.setVisible(true) ; 
                                                baseLabelField.setVisible(true) ; 
                                            }else{
                                                baseField.setVisible(false) ; 
                                                baseLabelField.setVisible(false) ; 
                                            } 
                                        }
                                    }
                                }
                                pwContentEditor.editForm.doLayout() ; 
                            }); 
                        }else{
                            chkField.on('check' , function( chk , val){
                                for(var i = 0 ; i < pwContentEditor.flexFormData.length ; i++ ){
                                    if( pwContentEditor.flexFormData[i].displayCond != ''){
                                        if( pwContentEditor.flexFormData[i].displayCond.field == chk.getId()){
                                            var baseField = Ext.getCmp(pwContentEditor.flexFormData[i].el) ; 
                                            if( baseField == undefined)
                                                continue ; 
                                            var baseLabelField = Ext.getCmp(jsonData[i].el +  '_label' ) ; 
                                            if ( chk.getValue() == pwContentEditor.flexFormData[i].displayCond.value) {
                                                baseField.setVisible(true) ; 
                                                baseLabelField.setVisible(true) ; 
                                            }else{
                                                baseField.setVisible(false) ; 
                                                baseLabelField.setVisible(false) ; 
                                            } 
                                        }
                                    }
                                }
                            }) ; 
                        }
                            
                    }
                }
                
                
                var hourStore = new Ext.data.JsonStore({
                    fields : ['name',  {name: 'value', type: 'int'}],
                        data   : [
                            {name : '00',   value: 0 },
                            {name : '01',   value: 1 },
                            {name : '02',   value: 2 },
                            {name : '03',   value: 3 },
                            {name : '04',   value: 4 },
                            {name : '05',   value: 5 }, 
                            {name : '06',   value: 6 }, 
                            {name : '07',   value: 7 }, 
                            {name : '08',   value: 8 }, 
                            {name : '09',   value: 9 }, 
                            {name : '10',   value: 10 }, 
                            {name : '11',   value: 11 }, 
                            {name : '12',   value: 12 }, 
                            {name : '13',   value: 13 }, 
                            {name : '14',   value: 14 }, 
                            {name : '15',   value: 15 }, 
                            {name : '16',   value: 16 }, 
                            {name : '17',   value: 17 }, 
                            {name : '18',   value: 18 }, 
                            {name : '19',   value: 19 }, 
                            {name : '20',   value: 20 }, 
                            {name : '21',   value: 21 }, 
                            {name : '22',   value: 22 }, 
                            {name : '23',   value: 23 } 
                        ]
                    }) ; 
                var minStore = new Ext.data.JsonStore({
                    fields : ['name',  {name: 'value', type: 'int'}],
                        data   : [
                            {name : '00',   value: 0 },
                            {name : '05',   value: 5 },
                            {name : '10',   value: 10 },
                            {name : '15',   value: 15 },
                            {name : '20',   value: 20 }, 
                            {name : '25',   value: 25 }, 
                            {name : '30',   value: 30 }, 
                            {name : '35',   value: 35 }, 
                            {name : '40',   value: 40 }, 
                            {name : '45',   value: 45 }, 
                            {name : '50',   value: 50 }, 
                            {name : '55',   value: 55 }  
                        ]
                    }) ;
                var jsonAdvance = jsonRoot.advance ; 
                if( jsonAdvance == null)
                    jsonAdvance = {} ; 
                
                var newPanel  = Ext.getCmp('content-element-form') ;
                
                var myField = new Ext.form.DisplayField({
                	hideLabel : true , 
                	hidden : jsonRoot.hideName , 
                    html : '<hr style="display: block; height: 1px;border: 0; border-top: 1px solid rgb(204,214,229) ; margin: 1em 0; padding: 0;"/>'
                })
                newPanel.add(myField) ;
                
                var myField = new Ext.form.Checkbox({
                	hideLabel : true , 
                    id : 'advance-enable-daterange' ,  
                    name : 'advance[enableDaterange]' , 
                   	boxLabel  :  '<span style="font-weight:bold;color: rgb(20,65,138) ;">Enable start-stop date and time (in GMT)</span>&nbsp;&nbsp;<img title="–	Set the life span for your Content Element by defining a start and end date." src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png">' ,  
           
                    checked : jsonAdvance.enableDaterange , 
                    listeners : {
                        'check' : function(checkBox , checked){
                            if( checked == true ){
                                Ext.getCmp('advanced-detail-startend-date-panel').expand() ;
                            }else{
                                Ext.getCmp('advanced-detail-startend-date-panel').collapse() ;
                            }
                        }  
                        
                    }
                }) ; 
                newPanel.add(myField) ;
                
                var detailPanel = new Ext.Panel({ 
                    border : false , 
                    id : 'advanced-detail-startend-date-panel' , 
                    bodyBorder : false ,  
                    collapsedCls : 'ml-collab-panel-colapse' ,
                    bodyCssClass : 'cse-panel-body2' , 
                    collapsed : ! jsonAdvance.enableDaterange ,  
                    collapsible : true , 
                    bodyStyle : 'border : none' , 
                    header : false  
                }); 
                
                newPanel.add(detailPanel) ;
 
                
                var myField = new Ext.form.CompositeField({
                    id : 'advance1-1' ,  
                    style : 'margin : 0 0 10px 0' ,
                   	defaults: {
                        flex: 1
                    },
                    items: [
                        {
                            xtype     : 'label',
                            html      : '&nbsp;&nbsp;&nbsp;&nbsp;Start Date' , 
                            width :  80
                        }  , 
                        {
                            xtype     : 'datefield',
                            name      : 'advance[startDate]',
                            id        : 'addvance-startdate' ,  
                            format : 'd/m/Y' ,  
                            showToday : true  ,  
                            value     : jsonAdvance.startDate , 
                            width     : 100
                        } ,
                        {
                            xtype     : 'label',
                            html      : '&nbsp;&nbsp;&nbsp;&nbsp;Hour' , 
                            width :  40
                        } , 
                        {
                            xtype:          'combo',
                            mode:           'local',
                            allowBlank:  true  ,
                            triggerAction:  'all',
                            editable:       false,
                            hiddenName: 'advance[startHour]',
                            fieldLabel:     'Interval',
                            displayField:   'name',
                            valueField:     'value', 
                            store:  hourStore ,  
                            value     : jsonAdvance.startHour , 
                            //hiddenValue: 0, // default value
                            width:40  
        
                        },
                        {
                            xtype     : 'label',
                            html      : '&nbsp;&nbsp;&nbsp;&nbsp;Minute' , 
                            width :  50
                        } , 
                        {
                            xtype:          'combo',
                            mode:           'local',
                            allowBlank: true  ,
                            triggerAction:  'all',
                            editable:       false,
                            hiddenName: 'advance[startMin]',
                            fieldLabel:     'Interval',
                            displayField:   'name',
                            valueField:     'value', 
                            store:  minStore ,  
                            value     : jsonAdvance.startMin , 
                            //hiddenValue: 0, // default value
                            width:40  
        
                        }
                    ]
                }) ; 
                detailPanel.add(myField) ;
                
                
                var myField = new Ext.form.CompositeField({
                    id : 'advance1-2' ,  
                   	defaults: {
                        flex: 1
                    },
                    items: [
                        {
                            xtype     : 'label',
                            html      : '&nbsp;&nbsp;&nbsp;&nbsp;End Date' , 
                            width :  80
                        }  , 
                        {
                            xtype     : 'datefield',
                            name      : 'advance[endDate]',
                            id        : 'advance-enddate' ,  
                            format : 'd/m/Y' ,  
                            showToday : true  ,  
                            value     : jsonAdvance.endDate , 
                            width     : 100
                        } ,
                        {
                            xtype     : 'label',
                            html      : '&nbsp;&nbsp;&nbsp;&nbsp;Hour' , 
                            width :  40
                        } , 
                        {
                            xtype:          'combo',
                            mode:           'local',
                            allowBlank: true  ,
                            triggerAction:  'all',
                            editable:       false,
                            hiddenName: 'advance[endHour]',
                            fieldLabel:     'Interval',
                            displayField:   'name',
                            valueField:     'value', 
                            store:  hourStore ,  
                            value     : jsonAdvance.endHour , 
                            //hiddenValue: 0, // default value
                            width:40  
        
                        },
                        {
                            xtype     : 'label',
                            html      : '&nbsp;&nbsp;&nbsp;&nbsp;Minute' , 
                            width :  50
                        } , 
                        {
                            xtype:          'combo',
                            mode:           'local',
                            allowBlank: true  ,
                            triggerAction:  'all',
                            editable:       false,
                            hiddenName: 'advance[endMin]',
                            fieldLabel:     'Interval',
                            displayField:   'name',
                            valueField:     'value', 
                            value     : jsonAdvance.endMin , 
                            store:  minStore ,  
                            //hiddenValue: 0, // default value
                            width:40  
        
                        }
                    ]
                }) ; 
                detailPanel.add(myField) ;
                
                var myField = new Ext.form.DisplayField({
                    html : '<br/>'
                })
                detailPanel.add(myField) ;
 
                
                
                  
                
                pwContentEditor.ajaxPreProcess() ; 
    } , 
    
    ajaxPreProcess : function(){
        if(pwContentEditor.contentId == 0 ){ 
            pwContentEditor.editWindow.show();
            pwContentEditor.setEditWindowPosition() ; 
            Ext.MessageBox.hide();
        }else{
            data = {} ; 
            data['contentId'] = pwContentEditor.contentId ; 
            if( pwMain.crossDomain){
                data['eID'] = 't3p_dynamic_content_placement' ; 
                data['pid'] = 1 ; 
                data['cmd'] = 'getCondition' ; 
                data['time'] = new Date().getTime();
                Ext.util.JSONP.request({
                    url: pwMain.baseUrl + 'index.php',
                    callbackKey: 'callback',
                    params : data , 
                    callback: function(jsonData) {  
                        pwContentEditor.condition = jsonData.tx_t3pconditionalcontent_condition ;
                        pwContentEditor.editWindow.show();
                        pwContentEditor.setEditWindowPosition() ; 
                        Ext.MessageBox.hide();
                    }
                });
            }else{
                Ext.Ajax.request({
        			url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=getCondition'  , 
                    //url : 'testxml.php' , 
        			method: 'POST',
        			params:   data ,
        			timeout: '30000',
        			// process the response object to add it to the TabPanel:
        			success: function(xhr) {
         			    var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
                        pwContentEditor.condition = jsonData.tx_t3pconditionalcontent_condition ;
                        pwContentEditor.editWindow.show();
                        pwContentEditor.setEditWindowPosition() ; 
                        Ext.MessageBox.hide();
        			},
        			failure: function() {
        				Ext.MessageBox.hide();
    					Ext.MessageBox.alert('Error', '#323' ) ; 
        			}
        		});
            }
            
        }
        
    }    , 
    
    setEditWindowPosition : function(){
        var scnWid,scnHei;
    	if (self.innerHeight) // all except Explorer
    	{
    		scnWid = self.innerWidth;
    		scnHei = self.innerHeight;
    	}
    	else if (document.documentElement && document.documentElement.clientHeight)
    		// Explorer 6 Strict Mode
    	{
    		scnWid = document.documentElement.clientWidth;
    		scnHei = document.documentElement.clientHeight;
    	}
    	else if (document.body) // other Explorers
    	{
    		scnWid = document.body.clientWidth;
    		scnHei = document.body.clientHeight;
    	}
        pwContentEditor.editWindow.setPosition(scnWid/2 -pwContentEditor.editWindow.getWidth() / 2  , 10) ; 
    }
} ; 
return window.pwContentEditor ;
//---	
}) ; 