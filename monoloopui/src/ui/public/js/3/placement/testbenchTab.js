define(function (require) {
// --- start
window.testbenchTab = {
    clip : null , 
    lastURL : '' , 
    fieldArray : {} , // Push ID for detect back ; 
    forceremoveItem : true , 
    forceSession : false , 
    mongoDetail : [] , 
    showTab : function(){
        //testbench-field
        
        if( pwMain.centerPanel.getActiveTab().getId()  != 'pw-tab-test' )
            return ; 
        testbenchTab.forceremoveItem = true ; 
        Ext.getCmp('testbench-field').removeAll() ; 
        
        
        //alert('z1') ; 
        testbenchTab.loadPreviewIframe() ; 
    } , 
    
    loadPreviewIframe : function(){ 
        if( document.getElementById('pw-test-view') != null)
        	//document.getElementById('pw-test-view').src = pwMain.temPath + 'testbenchView.php?l='+pwMain.urlencode(Ext.getCmp('pw-url').getValue()) ; 
        	document.getElementById('pw-test-view').src = mPostMessage.processURL(Ext.getCmp('pw-url').getValue(),'testbenchView.php?l=') ;
    } , 
    
    loadField : function(loadSession){
        if( testbenchTab.forceremoveItem == false){
            //alert('xxx') ; 
            testbenchTab.saveTestBenchValue() ; 
            
            return ; 
        }
        
        Ext.getCmp('testbench-field').removeAll() ; 
        var data = {} ;
        data['url'] = Ext.getCmp('pw-url').getValue();
        monoloop_base.showProgressbar('Initializing...') ; 
        
        
        
        if( pwMain.crossDomain){
            data['eID'] = 't3p_dynamic_content_placement' ; 
            data['cmd'] = 'getTestBenchData' ; 
            data['pid'] = pwMain.pid ; 
            data['l'] = pwMain.urlencode(Ext.getCmp('pw-url').getValue()) ; 
            data['testbench'] = 'loadSession' ; 
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data, 
                callback: function(result) { 
                    testbenchTab.testBenchData_success(result) ; 
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=getTestBenchData&l='+pwMain.urlencode(Ext.getCmp('pw-url').getValue()) + '&testbench='+ loadSession , 
                //url : 'testxml.php' , 
    			method: 'POST',
    			params:   data ,
    			timeout: '30000',
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
     				var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
                    //console.debug(jsonData) ;  
                    testbenchTab.testBenchData_success(jsonData) ; 
                },
    			failure: function() {
    			    Ext.MessageBox.hide();
    				Ext.MessageBox.alert('Error', '#332' ) ; 
    			}
            }) ; 
        } 
    } , 
    
    testBenchData_success : function(jsonData){
    	var me = this ; 
        if(jsonData.success == true){
            me.mongoDetail = jsonData.mongoDetail ;        
            testbenchTab.fieldArray = {} ; 
            // Gen Test bench panel .
            var testBenchObj = jsonData.testbench ; 
            for (var group in testBenchObj) {
               // Group ; 
               var newPanel = testbenchTab.createNewFieldPanel() ; 
               // Create group header ; 
               var headerField = new Ext.form.DisplayField({
                    value : '<h2>' + group + '</h2>'  
               }) ; 
               newPanel.add(headerField) ;
               // Add Field Property ; 
               var groupObj =  testBenchObj[group] ; 
               for(var field in groupObj){
                    //console.debug(groupObj[field]) ; 
                    testbenchTab.createNewField(newPanel , groupObj[field]) ; 
               }
               
               Ext.getCmp('testbench-field').add(newPanel) ;
               
               //console.debug(testbenchTab.fieldArray) ; 
            }
            
            if( pwMain.crossDomain == true){
                ml_testbubble.open9() ; 
            }
        }else{
            // Display empty .
        }
        /*
        var myStatic = testbenchTab.staticLinkPanel() ; 
        Ext.getCmp('testbench-field').add(myStatic) ;
        */
        
        // Add view result button ; 
        /*
        var resultBtn = new Ext.Button({
            text : 'Update and view result >>' , 
            style : 'padding  : 10px ; align : center ;'  , 
            handler : function(){
                testbenchTab.loadPreviewIframe() ;  
                if( pwMain.crossDomain == true){
                    ml_testbubble.open14() ; 
                }
            }
        }) ; 
        
        Ext.getCmp('testbench-field').add(resultBtn) ;
        */
        // Add print button ; 
        var printBtn = new Ext.Button({
            text : 'Print >>' , 
            style : 'padding-left : 10px ; align : center ;'  , 
            handler : function(){
            	/*
                if( document.getElementById('pw-test-view') == null){ 
                    return ;   
                }
                    
                if( document.getElementById("pw-test-view").contentWindow.monoloop_testbench_view == undefined){ 
                    return ;  
                } 
                */
                document.getElementById("pw-test-view").focus() ; 
                
                //document.getElementById("pw-test-view").contentWindow.monoloop_testbench_view.printPage() ; 
                
                var iframe = document.getElementById("pw-test-view") ; 
		        iframe.contentWindow.postMessage(JSON.stringify({
			        t: "test-printPage" , 
			        testData : testData  
				}), mPostMessage.url);
			}
        }) ; 

        
        Ext.getCmp('testbench-field').doLayout() ;
        
        //testbenchTab.clip.glue( 'd_clip_button' );

         
    
        Ext.MessageBox.hide();
        //alert('yyy') ;  
        testbenchTab.saveTestBenchValue() ; 
        testbenchTab.forceremoveItem = false ; 
    } , 
    
    updateTestbench : function(){
    	var me = this ; 
    	testbenchTab.loadPreviewIframe() ;  
        if( pwMain.crossDomain == true){
            ml_testbubble.open14() ; 
        }
    } , 
    
    createNewFieldPanel : function(){
        var newPanel = new Ext.Panel({
            height : 'auto' , 
            width : '95%',
            bodyCssClass : 'testbench-field-group'
        }) ; 
         
        return newPanel ; 
    } , 
    
    staticLinkPanel : function(){
        /*
        var newPanel = new Ext.Panel({
            height : 'auto' , 
            width : '95%',
            bodyCssClass : 'testbench-field-group2'
        }) ;
        */
        
        function my_mouse_down_handler(client) {
                testbenchTab.clip.setText(Ext.getCmp('testbech-static-text').getValue()) ;
        }
        
        testbenchTab.clip.addEventListener( 'onMouseDown', my_mouse_down_handler );
        
        var headerField = new Ext.form.DisplayField({
            value : '<br>'  
       }) ; 
       
       Ext.getCmp('testbench-static-link').add(headerField) ;
 
       
       var valueField = new Ext.form.TextField({
            value : ''  , 
            id : 'testbech-static-text' , 
            width : '90%'
       });
       Ext.getCmp('testbench-static-link').add(valueField) ;
       
       
       
       var headerField = new Ext.form.DisplayField({
            value : '<div id="d_clip_button" ><u>Copy To Clipboard</u></div>'  
       }) ; 
       
       Ext.getCmp('testbench-static-link').add(headerField) ;
 
       
       
       return  ; 
    }  ,
    
    createNewField : function(panel , fieldObject){
    	me = this ; 
        if( fieldObject.type == 'string'){
           var headerField = new Ext.form.DisplayField({
                value : '<span class="field-label">' + fieldObject.text + '</span>'  
           }) ; 
           panel.add(headerField) ; 
           
           var valueField = new Ext.form.TextField({
                value : fieldObject.value  , 
                id : fieldObject.connector + '_' +  fieldObject.id , 
                width : '90%' , 
                enableKeyEvents : true ,
                listeners : {
                	'keyup' : function(text , e){
                		me.updateTestbench() ; 
                	}
                }
           });
           panel.add(valueField) ; 
 
        }else if( fieldObject.type == 'integer'){
           var headerField = new Ext.form.DisplayField({
                value : '<span class="field-label">' + fieldObject.text + '</span>'  
           }) ; 
           panel.add(headerField) ; 
           
           var valueField = new Ext.form.NumberField({
                value : fieldObject.value  , 
                id : fieldObject.connector + '_' +  fieldObject.id , 
                width : '90%', 
                enableKeyEvents : true ,
                listeners : {
                	'keyup' : function(text , e){
                		me.updateTestbench() ; 
                	}
                }
           });
           panel.add(valueField) ;  
        
        }else if( fieldObject.type == 'date'){
           // * need adjust for new line ; 
           var dateField = new Ext.form.CompositeField({
                hideLabel : true , 
                style : 'padding-top : 5px' , 
                width : '90%' ,
                defaults: {
                    flex: 1
                } , 
                items : [
                {
                    xtype : 'displayfield' , 
                    value : fieldObject.text  
                },
                {
                    xtype: 'datefield' ,
                    id : fieldObject.connector + '_' +  fieldObject.id , 
                    format : 'd/m/Y' , 
                    value: '',
                    width : 90 , 
                    showToday : true  , 
                    value : fieldObject.value, 
	                listeners : {
	                	'select' : function(text , date){
	                		me.updateTestbench() ; 
	                	}
	                }
                }
                ] 
            
           }) ;  
           panel.add(dateField) ;  
  
        }else if( fieldObject.type == 'bool' ){
            var checkboxField = new Ext.form.CompositeField({
                hideLabel : true , 
                style : 'padding-top : 5px' , 
                width : '90%' ,
                defaults: {
                    flex: 1
                } , 
                items : [
                {
                    xtype : 'displayfield' , 
                    value : fieldObject.text  
                },
                {
                    xtype: 'checkbox' , 
                    style: 'float : right' , 
                    id : fieldObject.connector + '_' +  fieldObject.id , 
                    checked : fieldObject.value , 
                    listeners : {
                    	'check' : function(cb , checked){
                    		me.updateTestbench() ; 
                    	}
                    }
                }
                ] 
            
           }) ;  
           panel.add(checkboxField) ; 
 
        }else if( fieldObject.type == 'single remote'){
            var singleLocalStore = null ; 
            if( condition.crossDomain  ){ 
                singleLocalStore = new Ext.data.JsonStore({ 
                    root: 'data',  // the root of the array you'll send down
                    idProperty: 'v',
                    proxy: new Ext.data.ScriptTagProxy({
                                url: fieldObject.values
                            }),
                    fields: ['v'] 
                }) ;  
            }else{
                singleLocalStore = new Ext.data.JsonStore({
                    url:fieldObject.values,
                    root: 'data',  // the root of the array you'll send down
                    idProperty: 'v',
                    fields: ['v'] 
                }) ;  
            }
            /*
            var singleLocalStore = new Ext.data.JsonStore({
                url: fieldObject.values,
                root: 'data',  // the root of the array you'll send down
                idProperty: 'v',
                fields: ['v'] 
            }) ; 
            */
            var headerField = new Ext.form.DisplayField({
                value : '<span class="field-label">' + fieldObject.text + '</span>'  
           }) ; 
           panel.add(headerField) ; 
           
           var comboField = new Ext.form.ComboBox({
                width: 150 , 
                triggerAction:  'all', 
                typeAhead: true,
                mode: 'remote', 
                hideTrigger: true,     
                selectOnFocus:true, 
                queryParam: 'query' , 
                hideLabel  : true , 
                minChars : 3 , 
                displayField:   'v',
                valueField:     'v',  
                hiddenName : 'myID' , 
                store:  singleLocalStore ,   
                id : fieldObject.connector + '_' +  fieldObject.id , 
                listeners : {
                    'render' : function(combo){
                        if( fieldObject.value == null){ 
                            ;
                        }else{
                            combo.setValue(fieldObject.value);
                        } 
                    } , 
                    'select' : function( cb , record , index ){
                    	me.updateTestbench() ; 
                    }
                }
            }) ;
            panel.add(comboField) ; 
            
        }else if( fieldObject.type == 'single remote2'){
            
           var singleLocalStore = null ; 
           if( condition.crossDomain ){ 
                singleLocalStore = new Ext.data.JsonStore({  
                    idProperty: 'v',
                    proxy: new Ext.data.ScriptTagProxy({
                                url: fieldObject.values
                            }),
                    fields: ['v' , 'k' ]  
                }) ; 
           }else{
                singleLocalStore = new Ext.data.JsonStore({
                    url:fieldObject.values, 
                    idProperty: 'v',
                    fields: ['v' , 'k' ]  
                }) ; 
           }
           
           /*
            var singleLocalStore = new Ext.data.JsonStore({
                url: fieldObject.values, 
                idProperty: 'v',
                fields: ['v' , 'k'] 
            }) ; 
            */
            if( fieldObject.value != null){ 
                singleLocalStore.loadData(fieldObject.value2) ;     
            }
            
            var headerField = new Ext.form.DisplayField({
                value : '<span class="field-label">' + fieldObject.text + '</span>'  
           }) ; 
           panel.add(headerField) ; 
           
           var comboField = new Ext.form.ComboBox({
                width: 150 , 
                triggerAction:  'all', 
                typeAhead: true,
                mode: 'remote', 
                hideTrigger: true,     
                selectOnFocus:true, 
                queryParam: 'query' , 
                hideLabel  : true , 
                minChars : 2 , 
                displayField:   'k',
                valueField:     'v',  
                hiddenName : 'myID' , 
                store:  singleLocalStore ,   
                id : fieldObject.connector + '_' +  fieldObject.id , 
                listeners : {
                    'render' : function(combo){
                        if( fieldObject.value == null){ 
                            ;
                        }else{
                            combo.setValue(fieldObject.value);
                        } 
                    }, 
                    'select' : function( cb , record , index ){
                    	me.updateTestbench() ; 
                    }
                }
            }) ;
            panel.add(comboField) ; 
           
           
            
        }else if( fieldObject.type == 'single'){
            var singleLocalStore = new Ext.data.JsonStore({
                fields : ['label', 'value' ],
                data   : fieldObject.values
            }) ; 
            
            var headerField = new Ext.form.DisplayField({
                value : '<span class="field-label">' + fieldObject.text + '</span>'  
           }) ; 
           panel.add(headerField) ; 
           
           var comboField = new Ext.form.ComboBox({
                mode:           'local',  
                width: 150 , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,  
                displayField:   'label',
                valueField:     'value',  
                hiddenName : 'myID' , 
                store:  singleLocalStore ,  
                id : fieldObject.connector + '_' +  fieldObject.id , 
                listeners : {
                    'render' : function(combo){
                        if( fieldObject.value == null){
                            record = singleLocalStore.getAt(0) ; 
                            combo.setValue(record.get('value'));
                        }else{
                            combo.setValue(fieldObject.value);
                        } 
                    }, 
                    'select' : function( cb , record , index ){
                    	me.updateTestbench() ; 
                    }
                }
            }) ;
            panel.add(comboField) ; 
            
        }else if( fieldObject.type == 'multi'){
            var singleLocalStore = new Ext.data.JsonStore({
                fields : ['label', 'value' ],
                data   : fieldObject.values
            }) ;
            
            var headerField = new Ext.form.DisplayField({
                value : '<span class="field-label">' + fieldObject.text + '</span>'  
           }) ; 
           panel.add(headerField) ; 
           
           var multiField = new Ext.ux.form.MultiSelect({
                xtype: 'multiselect', 
                width: '97%',
                height: 50,
                id : fieldObject.connector + '_' +  fieldObject.id , 
                displayField:   'label',
                valueField:     'value',  
                delimiter : '||' , 
                allowBlank:false,
                store: singleLocalStore  , 
                value :  fieldObject.value , 
                listeners : {
                	'change' : function(multi , newVal , oldVal){
                		me.updateTestbench() ; 
                	}
                }
            }) ; 
            panel.add(multiField) ; 
        }else if(fieldObject.type == 'function'){
            if( fieldObject.set  == 1){
                var headerField = new Ext.form.DisplayField({
                    value : '<span class="field-label">' + fieldObject.text + '</span>'  
               }) ; 
               panel.add(headerField) ;
            }
            
            if( fieldObject.values[0].Label != undefined){
           	    

           var headerField = new Ext.form.DisplayField({
                value : '<u>Set : ' +   fieldObject.set + '</u>' 
           }) ; 
           
           panel.add(headerField) ; 
           
           for( var i = 0 ; i < fieldObject.values.length ; i++){
           	    
           	    if(  fieldObject.values[i].value_label != undefined ) {
           	    	var headerField = new Ext.form.DisplayField({
	                    value : fieldObject.values[i].Label + ' is ' + fieldObject.values[i].value_label
	               }) ; 
           	    }else{
           	    	var headerField = new Ext.form.DisplayField({
	                    value : fieldObject.values[i].Label + ' is ' + fieldObject.values[i].value
	               }) ; 
           	    }
           	    
                
               panel.add(headerField) ; 
           }
           
           }
           
           if( fieldObject.ReturnValue == 'string'){
                var field = new Ext.form.TextField({ 
                    id : fieldObject.connector + '_' +  fieldObject.id + '_' + fieldObject.set , 
                    width : '90%' , 
                    value : fieldObject.value, 
                    enableKeyEvents : true , 
	                listeners : {
	                	'keyup' : function(text , e){
	                		me.updateTestbench() ; 
	                	}
	                }
                }) ; 
                panel.add(field) ;   
           }else if(fieldObject.ReturnValue == 'integer'){
                var field = new Ext.form.NumberField({ 
                    id : fieldObject.connector + '_' +  fieldObject.id + '_' + fieldObject.set , 
                    width : '90%' , 
                    value : fieldObject.value, 
                    enableKeyEvents : true , 
	                listeners : {
	                	'keyup' : function(text , e){
	                		me.updateTestbench() ; 
	                	}
	                }
                }) ; 
                panel.add(field) 
           }else{
                var field = new Ext.form.Checkbox({ 
                    id : fieldObject.connector + '_' +  fieldObject.id + '_' + fieldObject.set , 
                    width : '90%' , 
                    checked : fieldObject.value , 
                    listeners : {
                    	'check' : function(cb , checked){
                    		me.updateTestbench() ; 
                    	}
                    }
                }) ; 
                panel.add(field) ;     
           }
           // Loop 
           
           
        }
        if(fieldObject.type == 'function'){ 
            testbenchTab.fieldArray[fieldObject.connector + '_' +  fieldObject.id + '_' + fieldObject.set] = {
                connector : fieldObject.connector , 
                field : fieldObject.id , 
                value : '' , 
                type  : fieldObject.type , 
                ReturnValue : fieldObject.ReturnValue , 
                id    : fieldObject.connector + '_' +  fieldObject.id + '_' + fieldObject.set
            }  ;
            testbenchTab.fieldArray[fieldObject.connector + '_' +  fieldObject.id + '_' + fieldObject.set]['params'] = {}
            for( var i = 0 ; i < fieldObject.values.length ; i++){
              //fieldObject.values[i].value
              testbenchTab.fieldArray[fieldObject.connector + '_' +  fieldObject.id + '_' + fieldObject.set]['params']['param' + (i + 1 ) ] = fieldObject.values[i].value ; 
           }
             
        }else{
            testbenchTab.fieldArray[fieldObject.connector + '_' +  fieldObject.id] = {
                connector : fieldObject.connector , 
                field : fieldObject.id , 
                value : '' , 
                type  : fieldObject.type , 
                id    : fieldObject.connector + '_' +  fieldObject.id
            }  ; 
        }
         
        
    } , 
    
    getStaticLink : function(){
        if( Ext.getCmp('testbench-static-link').collapsed)
            return ; 
        
        var static_link = '' ; 
        var fieldList = testbenchTab.fieldArray ; 
        static_link = static_link + '&l=' + pwMain.urlencode(Ext.getCmp('pw-url').getValue()) 
        for (var key in fieldList) {
            if( Ext.getCmp(fieldList[key].id) == undefined)
                continue ; 
 
            static_link =  static_link + '&data[' + key + ']' + '[main]=' + fieldList[key].connector + '|' +   fieldList[key].field + '|' + fieldList[key].type + '|' ;  
            if( fieldList[key].ReturnValue != undefined){
            	static_link =  static_link + fieldList[key].ReturnValue  + '|' ; 
            }else{
            	static_link =  static_link   + '|' ; 
            }
 
            
            if(  fieldList[key].type == 'date'){
                var val = Ext.getCmp(fieldList[key].id).getValue() ;    
                static_link =  static_link + val.format('d/m/Y') ;  
            }else{
                static_link =  static_link + Ext.getCmp(fieldList[key].id).getValue() ;    
            }
            if(  fieldList[key].type == 'function'){
                for (var key2 in fieldList[key].params) {
                    static_link =  static_link + '&data[' + key + ']' + '[params][' + key2 + ']=' + fieldList[key]['params'][key2] ;  
                }
            }
        }
        
        Ext.getCmp('testbech-static-text').setValue( pwMain.testBenchPath + static_link ) ; 
    } , 
    
    saveTestBenchValue : function(){
        // Presend Data ; 
        var me = this ; 
        monoloop_base.showProgressbar('Preview...') ;  
        var data = {} ; 
        var fieldList = testbenchTab.fieldArray ; 
        for (var key in fieldList) {
        	if( data[key] == undefined){
        		data[key] = {} ; 
        	}
            data[key]['connector']= fieldList[key].connector ; 
            data[key]['field'] = fieldList[key].field ; 
            data[key]['type']  = fieldList[key].type ; 
            if( Ext.getCmp(fieldList[key].id) == undefined)
                continue ; 
            if(  fieldList[key].type == 'date'){
                var val = Ext.getCmp(fieldList[key].id).getValue() ;    
                data[key]['value'] = val.format('d/m/Y') ; 
                //console.debug(val.format('d/m/Y')) ; 
            }else{
                data[key]['value'] = Ext.getCmp(fieldList[key].id).getValue() ;    
            }
            if(  fieldList[key].type == 'function'){
            	data[key]['ReturnValue'] =  fieldList[key].ReturnValue ; 
                for (var key2 in fieldList[key].params) {
                	if( data[key]['params'] == undefined){
                		data[key]['params'] = {} ; 
                	}
                    data[key]['params'][key2]  = fieldList[key]['params'][key2] ; 
                }
            }
        }
        
        // Process static link ; 
        testbenchTab.getStaticLink() ;  
        
        me.saveTestBenchField_success(data) ; 
        /*
        if( pwMain.crossDomain ){
            data['eID'] = 't3p_dynamic_content_placement'  ; 
            data['pid'] = pwMain.pid ; 
            data['cmd'] = 'saveTestBenchField' ; 
            data['testbench'] = 1 ; 
            data['l'] = pwMain.urlencode(Ext.getCmp('pw-url').getValue()) ; 
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data , 
                callback: function(result) { 
                    //console.debug(result) ; 
                    testbenchTab.saveTestBenchField_success(result) ;  
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=saveTestBenchField&testbench=1&l='+pwMain.urlencode(Ext.getCmp('pw-url').getValue()) , 
                //url : 'testxml.php' , 
    			method: 'POST',
    			params:   data ,
    			timeout: '30000',
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) { 
                    var jsonData = Ext.util.JSON.decode(xhr.responseText) ; 
                    testbenchTab.saveTestBenchField_success(jsonData) ; 
                }, 
    			failure: function() {
    			    Ext.MessageBox.hide();
    				Ext.MessageBox.alert('Error', '#333' ) ; 
    			}
            }) ; 
        }
        */
        
    } , 
    
 
    saveTestBenchField_success : function(testData){
 		var me = this ;  
        // Call test bench preview to view result ; 
        /*
        if( document.getElementById('pw-test-view') == null){
            Ext.MessageBox.hide(); 
            return ;   
        }
            
        if( document.getElementById("pw-test-view").contentWindow.monoloop_testbench_view == undefined){
            Ext.MessageBox.hide(); 
            return ;  
        } 
        */
        monoloop_base.showProgressbar('Preview...') ;  
        for( var i = 0 ; i < me.mongoDetail.length ; i++){
        	if( me.mongoDetail[i] == null) 
        		continue ; 
			me.mongoDetail[i].placed = undefined ; 
		} 
		
        //document.getElementById("pw-test-view").contentWindow.monoloop_testbench_view.processTestbench2(testData , me.mongoDetail , true  ) ; 
        
        var iframe = document.getElementById("pw-test-view") ; 
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "test-processTestbench2" , 
	        testData : testData , 
	        mongoDetail : me.mongoDetail , 
	        loadDefault : true 
		}), mPostMessage.url);
  
        Ext.MessageBox.hide();
    } ,
    
    selectTabSrcChange : function(){
 		/*
        if(document.getElementById('pw-test-view').src != pwMain.temPath + 'testbenchView.php?l='+pwMain.urlencode(Ext.getCmp('pw-url').getValue()) )
            return ;
        */    
 
        
        //console.log('placement : testbench onload iframe');   
		
        if( pwMain.baseUrl != document.getElementById('pw-test-view').src && mPostMessage.enable == false ){
            // Change to proxy mode ;  
            // Wait 1 second to swith ; 
            setTimeout(function(){
            	if(mPostMessage.enable == false){
            		//console.log('placement : brose tab change to proxy mode');
            		var url = document.getElementById('pw-test-view').src ; 
            		if( document.getElementById('pw-test-view') != null){ 
		                document.getElementById('pw-test-view').src = pwMain.temPath + 'testbenchView.php?l='+pwMain.urlencode(document.getElementById('pw-test-view').src) ; 
		                mPostMessage.enable = true ; 
		                mPostMessage.showProxyWarning() ;  
		                mPostMessage.addProxyList(url) ; 
		            }
            	}
			}, 1500);
            
            return ; 
         }       
    } , 
    
    allassetloadcomplete : function(){
        if(testbenchTab.forceSession){
            //alert('x1') ; 
            testbenchTab.loadField(1) ;
         } 
        else{
            //alert('x2') ; 
            testbenchTab.loadField(0) ;
        }
        
        testbenchTab.forceSession = false ; 
    } , 
    
    bubbleClick : function(contentId){
        var totalRow = pwMain.myStore.getTotalCount() ; 
        var toUid = 0 ;
        for(var i = 0 ; i < totalRow ; i++){
            var record = pwMain.myStore.getAt(i) ; 
            if(contentId == record.get('content_id') ){
                 toUid  = record.get('toUid') ;  
            }
        }
        if( toUid != 0 ){
            pwContentEditor.openMainWindow(toUid , contentId , 1) ; 
        }else{
            variation.openNewWindowWithPreload( contentId , 1 ) ;
        }
        
    } , 
    
    refreshTab : function(){
        testbenchTab.forceremoveItem = true ; 
        testbenchTab.forceSession = true ; 
        testbenchTab.loadPreviewIframe() ; 
    }
} ;
return window.testbenchTab ; 
//---	
}) ; 