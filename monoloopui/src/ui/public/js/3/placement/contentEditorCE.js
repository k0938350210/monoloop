define(function (require) {
// --- start

Ext.DomQuery.matchers[2] = {
    re: /^(?:([\[\{])(?:@)?([\w-]+)\s?(?:(=|.=)\s?(["']?)(.*?)\4)?[\]\}])/,
    select: 'n = byAttribute(n, "{2}", "{5}", "{3}", "{1}");'
};
Ext.override(Ext.form.Radio, {
    getGroupValue : function(){
        var p = this.el.up('form') || Ext.getBody();
        var c = p.child('input[name="'+this.el.dom.name+'"]:checked', true);
        return c ? c.value : null;
    },
    onClick : function(){
        if(this.el.dom.checked != this.checked){
            var els = this.getCheckEl().select('input[name="' + this.el.dom.name + '"]');
            els.each(function(el){
                if(el.dom.id == this.id){
                    this.setValue(true);
                }else{
                    Ext.getCmp(el.dom.id).setValue(false);
                }
            }, this);
        }
    },
    setValue : function(v){
        if (typeof v == 'boolean') {
            Ext.form.Radio.superclass.setValue.call(this, v);
        } else {
            var r = this.getCheckEl().child('input[name="' + this.el.dom.name + '"][value="' + v + '"]', true);
            if(r){
                Ext.getCmp(r.id).setValue(true);
            }
        }
        return this;
    }
});

window.contentEditorCE = {
    window : null , 
    window2 : null ,
    ceGrid : null , 
    id : 'contentEditorCE' , 
    
    toUid : 0 , 
    contentId : 0  , 
    elementName : '' , 
    condition : '' , 
    returnType : 0 ,  // 0 : placement select grid , 1 : test bench data , 2 : content edit , 3 : placement property
    additionalRecord : null , 
    title : '' , 
    
    // ----- CE --------- 
    
    
    openWindow : function(data){
        var me = this;   
        
        me.initCEGrid(data) ; 
        me.window = new Ext.Window({
            width:1050,
            height : 600 , 
            id : me.id , 
            layout : 'border' ,
            title : me.title + ' Content Element' , 
            modal: true,
         	maximizable : true  , 
         	closable:true , 
         	constrain : true , 
            items : [{
                xtype : 'panel' , 
                region : 'north' ,  
                height : 40 ,  
                border : false , 
                bodyStyle : 'padding-top : 10px ; padding-left : 10px ; background-color: rgb(227,227,227);' , 
                layout : 'column' , 
                items : [{
                    xtype : 'compositefield' , 
                    
                    items : [{
                        xtype : 'displayfield' ,
                        value : '<b>Element Name:</b>' , 
                        style : 'margin : 3px 0 0 0'
                    },{
                        xtype : 'textfield' , 
                        enableKeyEvents : true ,
                        width : 200 , 
                        id : me.id + '-header' , 
                        name : 'header'  , 
                        value : me.elementName , 
                        listeners : {
                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                me.saveProcess(0) ;     
	                            }
	                        }
	                    }
                    }] , 
					columnWidth : 0.3 
                },{
                	columnWidth : 0.7  ,
                	xtype : 'container' , 
                	id : me.id + '-top-addin' , 
                	items : []
                }]
            },{
                xtype : 'container' , 
                region : 'center' ,
                
                layout : 'border' , 
                items : [{
                    xtype : 'panel' , 
                    split: true,
        	        width: 328 ,
        			margins:'0',
        	        cmargins:'0',
        	        collapsible: true ,    
        	        id : me.id + '-leftPanel' , 
                    autoScroll : false , 
                    title : me.title + ' Properties' , 
                    region : 'west' , 
                    layout : 'fit' ,
                    items : [{
                        xtype : 'form' , 
                        border : false , 
                        fileUpload : true ,  
                        bodyStyle : 'padding-left : 5px ;padding-right : 5px ; padding-top : 0px ; ' , 
                        id : me.id + '-form' ,  
                        autoScroll : true , 
                        defaults : {
                            anchor : '100%'
                        } , 
                        items : [] 
                    }]
                },{
                    xtype : 'container' , 
                    region : 'center' , 
                    layout : 'border' , 
                    items : [{
                        xtype : 'panel' , 
                        title : 'List of content elements' ,  
                        
                        region : 'center' , 
                        layout : 'fit' , 
                        items : [me.ceGrid] /*, 
                        buttons : [{
                            text : 'Add existing elements' ,
                            handler : function(){
                                predefinedCEWindow.openWindow() ; 
                            }
                        },{
                            text : 'Create new element' , 
                            handler : function(){
      
                                selectTab.showContentElementSelectorWindow(5,0) ;  
                            }
                        }]*/
                    },{
                        xtype : 'panel' , 
                        region : 'south' ,
                        split: true,
                        id : me.id + '-preview' , 
                        height : 300 ,  
                        collapsible: true ,    
                        autoScroll : false , 
                        title : 'Preview' , 
                        html : 'Preview'
                    }] 
                }]
            }] , 
            
 
            listeners : {
                'afterrender' : function(w){
                    me.refreshCEGrid() ; 
                    
                    // Set preview window.
                    if( me.contentId != 0 )
                        me.innerPreview() ; 
                        
                    if( me.title == 'DynamicToolbar'){
			        	dynamicToolbar.afterrender(data) ; 
			        }else if( me.title == 'DynamicContainer' ){
			        	dynamicContainer.afterrender(data) ; 
			        }else if(me.title == 'Grid 2x2' || me.title == 'Grid 2x3' || me.title == 'Grid 2x4' || me.title == 'Grid 2x5' || me.title == 'Grid 3x2' || me.title == 'Grid 3x2 1Big' ){
			        	dynamicContainerFix.afterrender(data) ; 
			        }else if( me.title == 'Super Slider'){
			        	zuperSlider.afterrender(data) ; 
			        }else if( me.title == 'CPHSlider'){
			        	cphSlider.afterrender(data);
			        }else{
			        	sliderTab.afterrender(data) ; 
			        }
			        
			        var firstGridDropTargetEl =  me.ceGrid.getView().scroller.dom;
			        var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
			                ddGroup    : 'firstGridDDGroup',
			                notifyDrop : function(ddSource, e, data){ 
			                	console.debig	
		                        var records =  ddSource.dragData.selections;
		                        Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
		                        me.ceGrid.store.add(records); 
		                        return true
			                }
			        });
					
					me.window.doLayout() ;      
                }
            }
        }) ; 
        
        me.renderLeftPanel(data) ;
        
        if( me.returnType == 3){ 
        	Ext.getCmp(me.id + '-form').add({
        		xtype : 'hidden' , 
        		name : 'type3[element_id]' , 
        		id : 'type3-element-id' , 
        		value : pwContentEditor.additionalRecord.get('element_id') 
        	}) ;
        	
        	Ext.getCmp(me.id + '-form').add({
        		xtype : 'hidden' , 
        		name : 'type3[beforeAfter]' , 
        		id : 'type3-before-after' , 
        		value : pwContentEditor.additionalRecord.get('befor_after') 
        	}) ;
        	
        	Ext.getCmp(me.id + '-form').add({
        		xtype : 'hidden' , 
        		name : 'type3[displayType]' , 
        		id : 'type3-display-type' , 
        		value : pwContentEditor.additionalRecord.get('display_type') 
        	}) ;
        	
        	Ext.getCmp(me.id + '-form').add({
        		xtype : 'hidden' , 
        		name : 'type3[uid]' , 
        		id : 'type3-uid' , 
        		value : pwContentEditor.additionalRecord.get('uid') 
        	}) ;
        	
        	Ext.getCmp(me.id + '-form').add({
        		xtype : 'hidden' , 
        		name : 'type3[additionJS]' , 
        		id : 'type3-addition-js' , 
        		value : pwContentEditor.additionalRecord.get('additionJS') 
        	}) ;
        	
        	me.window.addButton({
        		text : 'POSITION' , 
        		handler : function(){
        			me.openPositionWindow() ; 
        		}
        	}) ; 
        }
        
        
        me.window.addButton({
            text:'CREATE CONDITION...'  , 
            cls: 'monoloop-black-btn' , 
            handler : function(){
                condition.stage = 8 ; 
                condition.preconditionStr = me.condition ; 
                condition.openMainWindow() ; 
            }
        });
		me.window.addButton({
            text:'PREVIEW'  ,
            cls: 'monoloop-black-btn' , 
            iconCls : 'pw-menu-inspect-content' , 
            handler: function(){  
                me.saveProcess(1) ; 
            }
        }) ; 
		me.window.addButton({
		    text:'CANCEL'  ,
		    cls: 'monoloop-black-btn' , 
		    handler: function(){ 
		        Ext.getCmp(me.id).close() ; 
		    }
		});
		me.window.addButton({
            width : 40 , 
            text:'OK'  ,
            cls: 'monoloop-black-btn' , 
            handler : function(){
                me.saveProcess(0) ; 
            }
         }); 
        
        var today = new Date();
        if( me.title == 'DynamicToolbar'){ 
        	require(['placement/element/dynamicToolbar'],function(d) {
        		d.renderForm(data) ; 
				me.ajaxPreProcess() ; 
        	}) ; 
        }else if(me.title == 'DynamicContainer'){  
        	require(['placement/element/dynamicContainer'],function(d) {
        		d.renderForm(data) ; 
				me.ajaxPreProcess() ; 
        	}) ; 
        }else if(me.title == 'Super Slider'){ 
        	require(['placement/element/zuperSlider'],function(z) {
        		z.renderForm(data) ; 
				me.ajaxPreProcess() ; 
        	}) ; 
        }else if(me.title == 'CPHSlider'){
        	require(['placement/element/cphSlider'],function(c) {
        		c.renderForm(data) ; 
				me.ajaxPreProcess() ; 
        	}) ;   
        }else if(me.title == 'Grid 2x2' || me.title == 'Grid 2x3' || me.title == 'Grid 2x4' || me.title == 'Grid 2x5' || me.title == 'Grid 3x2' || me.title == 'Grid 3x2 1Big'){
        	require(['placement/element/dynamicContainerFix'],function(d) {
        		d.renderForm(data) ; 
				me.ajaxPreProcess() ; 
        	}) ; 
        }else{
        	require(['placement/element/sliderTab'],function(s) {
        		s.renderForm(data) ; 
				me.ajaxPreProcess() ; 
        	}) ;   
        } 
    } , 
    
    before_retrieveDataForSubmit : function(){
    	return {} ;
    } , 
     
    retrieveDataForSubmit : function(){
        var me = this ; 
        var data = me.retrieveDataForSubmit() ; 
        data['name'] = Ext.getCmp( me.id + '-header').getValue() ;
        data['contentId'] = me.contentId ;
        data['condition'] = me.condition ; 
        data['toUid'] = me.toUid ; 
        if(me.returnType == 2){
            data['onlyContent'] = 1 ; 
        }else{
            data['url'] = Ext.getCmp('pw-url').getValue();
            data['selector'] = pwMain.selectorTxt ; 
            data['beforeAfter'] = selectTab.beforeAfter ; 
        }
        
        // Set CE data ; 
        var total = me.ceGrid.store.getTotalCount();
        //console.debug(total) ; 
        var contents = ''  ; 
        for(var i = 0 ; i < total ; i++){
            var rec = me.ceGrid.store.getAt(i) ; 
            if( rec == undefined)
                continue ;
            var uid = rec.get('uid') ; 
            if( contents == '')
                contents = contents + uid ; 
            else
                contents = contents + ',' + uid ;
        }   
        Ext.getCmp(me.id + '-ce').setValue(contents) ; 
        
        return data ; 
    } , 
    
    
    saveContentEditor_success : function(){
        var me = this ; 
        me.window.close() ;
        me.window.destroy() ;
        monoloop_base.msg('Success', 'New monoloop content has been created') ;
        //pwMain.myStore.load() ;
        if( me.returnType == 4 ){  
            preplaced.refreshPreplacedPanel(pwContentEditor.contentId) ; 
        } 
        if(me.returnType == 2){
            mdc_contentmanager.refreshContentGrid() ; 
        }else{
            pwMain.reStartContentGrid() ;
        }
        
        
        if(me.returnType == 1){
            testbenchTab.refreshTab() ; 
        }
    } ,
    
    
    // 0 Save for preview ; 1 Save and close
    saveProcess : function(typeId){
        var me = this ; 
        Ext.Ajax.timeout = 300000 ; 
        var data =  me.retrieveDataForSubmit() ; 
        
        var frms = Ext.getCmp( me.id + '-form').findByType('htmleditor') ; 
        for( var i = 0 ; i < frms.length ; i++ ){
            var t = frms[i].getValue()  ; 
        }
		
		var firstValidate = true ; 
		if( me.title == 'DynamicToolbar'){
        	firstValidate = dynamicToolbar.firstValidate() ; 
        }else if( me.title == 'DynamicContainer' || me.title == 'Super Slider' || me.title == 'Grid 2x2' || me.title == 'Grid 2x3' || me.title == 'Grid 2x4' || me.title == 'Grid 2x5' || me.title == 'Grid 3x2' || me.title == 'Grid 3x2 1Big'){
       		;
        }else if( me.title == 'CPHSlider'){
        	;
        }else{
        	firstValidate = sliderTab.firstValidate() ; 
        }
        
        
        if(Ext.getCmp( me.id + '-form').getForm().isValid() && firstValidate){
            //alert('Yes . We pass validate ; ') ; 
            if(pwMain.crossDomain){
                monoloop_base.showProgressbar('Save data...') ;  
                var fromDatas = Ext.getCmp( me.id + '-form').getForm().getValues() ;
                for (var attrname in fromDatas) { data[attrname] = fromDatas[attrname]; }
                data['eID'] = 't3p_dynamic_content_placement' ; 
                data['pid'] = pwMain.pid ; 
                data['cmd'] = 'saveContentEditor' ; 
                data['type'] = me.returnType ; 
                data['time'] = new Date().getTime();
                Ext.util.JSONP.request({
                    url: pwMain.baseUrl + 'index.php',
                    callbackKey: 'callback',
                    params : data , 
                    callback: function(result) {
                        Ext.MessageBox.hide();
                        if( typeId == 0 )
                            me.saveContentEditor_success() ; 
                        else{
                            me.contentId = result.contentId ; 
                            me.innerPreview() ; 
                        }
                    }
                });
            }else{
                Ext.getCmp( me.id + '-form').getForm().submit({
                    url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=saveContentEditor&type='+me.returnType,
                    waitMsg: 'Saving data...',
                    params : data ,
                    success: function(simple, o){
                    	 //alert('Save success') ;
                         //console.debug(o) ; 
                         //alert(o.result.data) ; 
                         if( typeId == 0 )
                            me.saveContentEditor_success() ; 
                         else{
                            me.contentId = o.result.contentId ; 
                            me.innerPreview() ; 
                         }
                    } ,
                    failure: function(simple, o) {  
			             Ext.MessageBox.alert('Error', o.result.msg ) ; 
                    	
	    			}
                });
            }
            
        }
    } ,
    
    renderLeftPanel : function(data){
        var me = this ;
        
        for( var i = 0 ; i < data.content.length ; i++ ){
            var fieldObj = data.content[i] ; 
            var nextFiledObj = {}
            if(i < data.content.length - 1 )
                nextFiledObj = data.content[i+1] 
            
            
            var myField = null ; 
            
            if( fieldObj.type == 'ce'){
                // We not process ce here .
                // Create hidden field 
                myField = new Ext.form.Hidden({
                    name : 'cef[' + fieldObj.el + ']' , 
                    id : me.id + '-ce' , 
                    value : fieldObj.value
                }) ; 
                Ext.getCmp(me.id + '-form').add(myField) ; 
            }
			continue ; 
			if( fieldObj.extjs == 'fieldheader' ){
                myField = new Ext.form.DisplayField({
                    hideLabel : true , 
                    value : fieldObj.label,
                    style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
                }) ;
            }else if( fieldObj.extjs == 'fixedlength' && nextFiledObj.extjs == 'relativesize'){
              
                myField = new Ext.form.CompositeField({
                    hideLabel : true , 
                    items : [{
                        xtype : 'checkbox'  , 
                        name : 'cef[' + fieldObj.el + ']' , 
                        style : 'margin-top : 3px ' ,  
                        labelStyle : 'display : none' ,
                        ctCls : 'ce-checkbox' , 
                        hideLabel : true , 
                        objValue : fieldObj.value ,         
                        listeners : {
                            'afterrender' : function(cb){
                                if( cb.objValue == '' || cb.objValue == null || cb.objValue == undefined || cb.objValue == false || cb.objValue == 'false' ){
                                    ;
                                }else{
                                    cb.setValue('on') ; 
                                }
                            }
                        }
                    },{
                        xtype : 'displayfield' ,  
                        value : 'Fixed length&nbsp;&nbsp;&nbsp;'  
                        
                    },{
                        xtype : 'checkbox'   ,  
                        name : 'cef[' + nextFiledObj.el + ']'  , 
                        style : 'margin-top : 3px ' ,  
                        objValue : nextFiledObj.value , 
                        hideLabel : true ,                         
                        listeners : {
                            'afterrender' : function(cb){
                                if( cb.objValue == '' || cb.objValue == null || cb.objValue == undefined || cb.objValue == false || cb.objValue == 'false' ){
                                    ;
                                }else{
                                    cb.setValue('on') ; 
                                }
                            }
                        }
                    },{
                        xtype : 'displayfield' , 
                        
                        value : 'Relative to window size'
                    }]
                }) ; 
                i++ ; 
            }else if(  fieldObj.extjs == 'length' && nextFiledObj.extjs == 'height'){
                myField = new Ext.form.CompositeField({
                    hideLabel : true , 
                    items : [{
                        xtype : 'displayfield' , 
                        value : 'Length'
                    },{
                        xtype : 'numberfield' , 
                        name : 'cef[' + fieldObj.el + ']' , 
                        width : 50 , 
                        value : fieldObj.value 
                    },{
                        xtype : 'displayfield' , 
                        value : 'x&nbsp;Height'
                    },{
                        xtype : 'numberfield' , 
                        name : 'cef[' + nextFiledObj.el + ']' , 
                        width : 50 , 
                        value : nextFiledObj.value
                    },{
                        xtype : 'displayfield' , 
                        value : 'pixels' , 
                        flex : 1 
                    }]
                }) ; 
                i++ ;
            }else if( fieldObj.extjs == 'topleftpos'){
                myField = new Ext.form.CompositeField({
                    hideLabel : true , 
                    items : [{
                        xtype : 'displayfield' , 
                        value : fieldObj.label + ':'
                    },{
                        xtype : 'numberfield' , 
                        name : 'cef[' + fieldObj.el + ']' , 
                        width : 50 , 
                        value : fieldObj.value 
                    },{
                        xtype : 'displayfield' , 
                        value : 'pixels'
                    }]
                }) ; 
            }else if( fieldObj.extjs == 'color'){
                //console.debug(fieldObj) ; 
                if( fieldObj.value == '' || fieldObj.value == null){
                    fieldObj.value = '#000000' ; 
                }
                //console.debug(fieldObj.value) ; 
                /*
                var fg = new Ext.ux.ColorField({ value : fieldObj.value ,  name : 'cef[' + fieldObj.el + ']' ,  msgTarget: 'qtip' , flex : 1 , 
                    objValue : fieldObj.value  
                });
                */
                
                var fg = new Ext.ux.color.colorField({
                    colorSelector:"mixer" , 
                    flex : 1 , 
                    value : fieldObj.value ,
                     objValue : fieldObj.value  ,
                    msgTarget: 'qtip' ,
                    name : 'cef[' + fieldObj.el + ']'
                }) ; 
                
                myField = new Ext.form.CompositeField({
                    hideLabel : true , 
                    items : [{
                        xtype : 'displayfield' , 
                        value : fieldObj.label + ':'
                    },fg ]
                }) ;  
            }else if( fieldObj.extjs == 'file'){
                myField = new Ext.ux.form.FileUploadField({ 
                    name : 'cef[' + fieldObj.el + ']' , 
                    emptyText: fieldObj.label,
                    hideLabel : true ,   
                    buttonText: '', 
                    buttonCfg: {
                        iconCls: 'upload-icon'
                    }  
                }) ; 
            }else if( fieldObj.extjs == 'position'){
                //console.debug(fieldObj) ; 
                myField = new Ext.Container({
                    layout : 'border' , 
                    height : 80 , 
                    border : false ,
                    items : [{
                        xtype : 'panel' , 
                        region : 'west' , 
                        border : false , 
                        width : 30 , 
                        style : ' padding : 3px ;' , 
                        bodyStyle : 'background-color : rgb(229,229,229) ; padding-top : 25px ; ' ,
                        bodyCfg : { 
                            id : me.id + fieldObj.el + '-left' ,
                            style : 'text-align : center' , 
                            onclick : 'contentEditorCE.changePosition(\''+fieldObj.el+'\' , 1) ;' , 
                            html : 'Left' 
                        } 
                    },{
                        xtype : 'panel' , 
                        border : false , 
                        layout : 'border' ,
                        region : 'center' ,
                        items : [{
                            region : 'north' , 
                            height : 15 ,
                            border : false , 
                            bodyCfg : { 
                                id : me.id + fieldObj.el  + '-top' ,
                                style : 'text-align : center ; background-color : rgb(229,229,229) ;' , 
                                onclick : 'contentEditorCE.changePosition(\''+fieldObj.el+'\',2) ;' , 
                                html : 'Top' 
                            } 
                        },{
                            xtype : 'panel' , 
                            border : false , 
                            
                            region : 'center' , 
                            style : 'padding-top : 3px ; padding-bottom : 3px ;' , 
                            bodyCfg : {  
                                style : 'text-align : center ; background-color : rgb(204,204,204) ; padding-top : 10px ; '
                            }  ,   
                            items : [{
                                xtype : 'displayfield' , 
                                value : '<b>Website</b>' 
                            },{
                                xtype : 'hidden' ,  
                                id : me.id + fieldObj.el , 
                                name : 'cef[' + fieldObj.el + ']' ,  
                                value : fieldObj.value 
                            }]
                        },{
                            region : 'south' , 
                            height : 15 ,
                            border : false , 
                            bodyCfg : { 
                                id : me.id + fieldObj.el  + '-bottom' ,
                                style : 'text-align : center ; background-color : rgb(229,229,229) ;' , 
                                onclick : 'contentEditorCE.changePosition(\''+fieldObj.el+'\',3) ;' , 
                                html : 'Bottom' 
                            } ,
                            objEl : fieldObj.el , 
                            objValue : fieldObj.value , 
                            listeners :  {
                                'afterrender' : function(p){
                                    var v = p.objValue  ;   
                                    if( v == 'left' ){
                                        contentEditorCE.changePosition(p.objEl,1) ;
                                    }else if( v == 'top' ){
                                        //console.debug(p) ; 
                                        contentEditorCE.changePosition(p.objEl,2) ;
                                    }else if( v == 'bottom' ){
                                        contentEditorCE.changePosition(p.objEl,3) ;
                                    }else if( v == 'right' ){
                                        contentEditorCE.changePosition(p.objEl,4) ;
                                    }
                                }
                            } 
                        }]
                    },{
                        xtype : 'panel' , 
                        region : 'east' , 
                        border : false , 
                        width : 30 , 
                        style : ' padding : 3px ;' ,  
                        bodyCfg : { 
                            id : me.id + fieldObj.el  + '-right' ,
                            style : 'text-align : center ;  background-color : rgb(229,229,229) ;padding-top : 25px ; ' , 
                            onclick : 'contentEditorCE.changePosition(\''+fieldObj.el+'\',4) ;' , 
                            html : 'Right' 
                        } 
                    }]    
                }) 
            }else if( fieldObj.extjs == 'radio'){ 
                var rr = [] ; 
                for( var j = 0 ; j < fieldObj.items.length ; j++){ 
                    var checked = false ; 
                    if( fieldObj.value == fieldObj.items[j].k){
                        checked = true ; 
                    }
                    var r = new Ext.form.Radio({ 
                        name : 'cef[' + fieldObj.el + ']' , 
                        boxLabel   :  fieldObj.items[j].v + '&nbsp;&nbsp;' , 
                        inputValue :  fieldObj.items[j].k , 
                        checked: checked 
                    }) ; 
                    rr.push(r) ; 
                }
                
                myField = new Ext.form.CompositeField({
                    hideLabel : true , 
                    items : [{
                        xtype : 'displayfield' , 
                        style : 'margin-top : 2px' , 
                        value : fieldObj.label + ':'
                    }, rr]
                }) ;  
                //console.debug('Process 6') ; 
            }else if( fieldObj.extjs == 'slider'){
                //console.debug(fieldObj) ; 
                myField = new Ext.Container({
                    hideLabel : true , 
                    items : [{
                        xtype : 'displayfield' , 
                        value : fieldObj.label + ': <span style="font-weight: bold;" id="'+fieldObj.el+'"></span>'
                    },{
                        xtype : 'sliderfield' , 
                        name:  'cef[' + fieldObj.el + ']' ,  
                        linkID : fieldObj.el , 
                        objValue : fieldObj.value , 
                        listeners : { 
                            'afterrender' : function(field){
                                if( field.objValue != ''){
                                    field.setValue(field.objValue) ; 
                                }
                                
                                document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ; 
                                field.slider.on('change', function() { 
                                    document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ;  
                                }); 
                                
                                
                            }
                        }
                    }]
                }) ;
                 
                //console.debug('Process 8') ; 
            }else if( fieldObj.extjs == 'line' ){
                myField = new Ext.form.DisplayField({
                    hideLabel : true , 
                    value : '<hr style="color : rgb(221,221,221)">' 
                }) ; 
            }else if ( fieldObj.extjs == 'btnforhtmledior'){
                myField = new Ext.Button({
                    text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">' + fieldObj.label + ' </span><div class="toolbar-btn"></div>' , 
                    anchor : '100%' , 
                    width : '100%' ,
                    linktovalue : me.id + '-value-'+ fieldObj.el , 
                    handler : function(btn){
                        // Open variation form 
                        var w = new Ext.Window({
                            linktovalue : btn.linktovalue , 
                            width:550,
                            height : 400 ,  
                            layout : 'fit' ,
                            title : 'Content editor' ,  
                            modal: true,
                         	maximizable : false  , 
                         	closable:true , 
                            items : [{
                                xtype : 'htmleditor' , 
                                value :  Ext.getCmp(btn.linktovalue).getValue() 
                            }] , 
                            buttons : [{
                                text : 'CANCEL' , 
                                handler : function(){
                                    
                                    w.close() ; 
                                }
                            },{
                                text : 'OK' , 
                                handler : function(){
                                    var h = w.findByType('htmleditor') ; 
                                    Ext.getCmp(w.linktovalue).setValue(h[0].getValue()) ; 
                                    w.close() ;
                                }
                            }]
                        }) ; 
                        
                        w.show() ;
                    }  
                }) ;
                Ext.getCmp(me.id + '-form').add(myField) ; 
                
                myField = new Ext.form.Hidden({
                    id : me.id + '-value-'+ fieldObj.el , 
                    name :  'cef[' + fieldObj.el + ']' ,  
                    value : fieldObj.value 
                }) ; 
                
            }else if( fieldObj.extjs = 'newslidebtn' ){
                myField = new Ext.Button({
                    text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">' + fieldObj.label + ' </span><div class="toolbar-btn"></div>' , 
                    anchor : '100%' , 
                    width : '100%' ,
                    handler : function(){
                        selectTab.showContentElementSelectorWindow(5,0) ;  
                    }
                }) ; 
            }
            
            if( myField != null){ 
                Ext.getCmp(me.id + '-form').add(myField) ; 
                Ext.getCmp(me.id + '-form').add({
                    xtype : 'box' , 
                    height : 10
                }) ;  
            } 
             
        } 
        
        
    } ,
    
    initCEGrid : function(data){
        var me = this ; 
        var fieldObj = {} ; 
        for( var i = 0 ; i < data.content.length ; i++){
            fieldObj = data.content[i] ; 
            if( fieldObj.type == 'ce'){
                break ; 
            }
        }
         
        
        function renderActions(value, p, r){ 
	    	var ret =  '<table width="100%"><tr align="center" >' +  
	    			    '<td><div onClick="contentEditorCE.showEditWindow('+r.data['uid']+' ,'+r.data['toUid']+' )" title="Edit" class="placement-edit" ></a></td>' + 
	    			    '<td><div onClick="contentEditorCE.showPreviewWindow('+r.data['uid']+' )"  title="View" class="placement-content-preview" ></div></td>' + 
	    			    '<td><div onClick="contentEditorCE.deleteContent('+r.data['uid']+' )"  title="Delete" class="placement-delete" ></div></td>' +   
					   '</tr></table>'  ;
			// r.data['uid'] ; 
	 
			return ret ;
	    } 
        
        function renderStatus(value, p, r){
            var hidden = '' ;
            if( r.data['hidden'] == 0 ){
				hidden = '<td><div onClick="contentEditorCE.setCEHidden('+r.data['uid']+' , 1)"  class="placement-active"></div></td>' ; 
			}else{
				hidden = '<td><div onClick="contentEditorCE.setCEHidden('+r.data['uid']+' , 0)"   class="placement-inactive" ></div></td>' ; 
			}
            	var ret =  '<table width="100%" ><tr align="center"> ' 
						 + hidden +
					   '</tr></table>'  ; 
			return ret ;
        }
        
        function renderOrder(value , p , r){
         	var index = me.ceGrid.store.indexOf(r)  ; 
         	var total = me.ceGrid.store.getTotalCount() ; 
         	
         	var moveUp = '<td><div onClick="contentEditorCE.moveUp('+index+' )"  class="placement-moveup"></div></td>' ; 
         	var moveDown = '<td><div onClick="contentEditorCE.moveDown('+index+' )"  class="placement-movedown"></div></td>' ; 
         	
         	if( index == 0 ){
         		moveUp = '<td>&nbsp;</td>' ;  
         	} 
         	
         	if( index == total - 1){
         		moveDown = '<td>&nbsp;</td>' ; 
         	}
         	
        	var ret =  '<table width="100%" ><tr align="center"> ' 
						+ moveUp + moveDown + 
					   '</tr></table>'  ; 
			return ret ;
        }
        
         
        
        
 
        var store = null ; 
        if( pwMain.crossDomain == false){
            store = new Ext.data.JsonStore({
    	        root: 'topics',
    	        totalProperty: 'totalCount',
    	        idProperty: 'uid',
    	        remoteSort: true,
    	
    	        fields:  [
        	        {name: 'uid', type: 'int'},
      	        	{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'},
      	        	{name: 'element_id'},
      	        	{name: 'content_id', type: 'int'},
      	        	{name: 'befor_after', type: 'int'},  	
      	        	{name: 'sort_order', type: 'int'}, 
                    {name: 'hidden', type: 'int'}, 
                    {name: 'tmplType'} , 
      	            {name: 'header'},
      	            {name: 'display_type', type: 'int'} , 
                    {name: 'content_hidden' , type : 'int'} , 
                    {name: 'toUid', type: 'int'} , 
                    {name: 'additionJS'} , 
                    {name: 'CType'}
      	        ],
    	
    	        // load using script tags for cross domain, if the data in on the same domain as
    	        // this page, an HttpProxy would be better
    	        proxy: new Ext.data.HttpProxy({
    	            url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentplacement/monoloopPageListByCE'
    	        }) 
    	    });
        }else{
            store = new Ext.data.JsonStore({
    	        root: 'topics',
    	        totalProperty: 'totalCount',
    	        idProperty: 'uid',
    	        remoteSort: true,
    	
    	        fields:  [
        	        {name: 'uid', type: 'int'},
      	        	{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'},
      	        	{name: 'element_id'},
      	        	{name: 'content_id', type: 'int'},
      	        	{name: 'befor_after', type: 'int'},  	
      	        	{name: 'sort_order', type: 'int'}, 
                    {name: 'hidden', type: 'int'}, 
                    {name: 'tmplType'} , 
      	            {name: 'header'},
      	            {name: 'display_type', type: 'int'} , 
                    {name: 'content_hidden' , type : 'int'} , 
                    {name: 'toUid', type: 'int'} , 
                    {name: 'additionJS'} , 
                    {name: 'CType'}
      	        ],
    	
    	        // load using script tags for cross domain, if the data in on the same domain as
    	        // this page, an HttpProxy would be better
    	        proxy: new Ext.data.ScriptTagProxy({
    	            url: pwMain.baseUrl + 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentplacement/monoloopPageListByCE&content='+fieldObj.value
    	        }) 
    	    });
        }
        
        //store.setDefaultSort('crdate', 'desc');
            
        me.ceGrid = new Ext.grid.GridPanel({
        	ddGroup          : 'firstGridDDGroup',
	        width: '100%', 
            id : me.id + '-grid' , 
	        border : false , 
	        trackMouseOver:false,
	        disableSelection:false,
	        loadMask: true,
	        enableDragDrop   : true,
        	stripeRows       : true,
            store : store , 
	        enableHdMenu: false,
            ctCls : 'contentGrid' , 
            columns:[{
                header: "Name" ,
                dataIndex : 'header' ,
                sortable : false , 
                width : 80
            },{
            	header: 'Ordering' , 
            	dataIndex : 'uid' , 
            	sortable : false , 
            	width : 50 , 
            	renderer: renderOrder
            },{
                header: 'Type' ,
                dataIndex : 'tmplType' ,
                sortable : false , 
                width : 100
            },{
                header: 'Created' ,
                dataIndex : 'crdate' ,
                renderer: Ext.util.Format.dateRenderer('d M Y') ,
                sortable : false , 
                width : 100
            },{
                header: 'Actions' ,
                dataIndex : 'uid' ,
                renderer: renderActions, 
                sortable : false , 
                width : 100
            },{
                header: 'Status' ,
                dataIndex : 'uid' ,
                renderer: renderStatus, 
                sortable : false , 
                width : 100
            }] , 
            viewConfig: {
	            forceFit:true 
	        }  , 
	        sm: new Ext.grid.RowSelectionModel({
                singleSelect: false,
                listeners: {
                    rowselect: function(sm, row, rec) {
                        me.innerPreview() ; 
                    } , 
                    rowdeselect : function(sm , row , rec){
                    	me.innerPreview() ; 
                    }
                }
            }) 
        }); 
        
        
        
        
    } , 
    
    UpdateCEGrid : function(contentID){
        var me = this ; 
        
        var contents = Ext.getCmp(me.id + '-ce').getValue() ; 
        var t = contents.split(',') ; 
        var add = true ; 
        for( var i = 0 ; i < t.length ; i++ ){
            if( t[i] == contentID){
                add = false ; 
                break ; 
            }
        }
        
        if( add ){
            if( contents == '' || contents == null){
                contents = contentID ; 
            }else{
                contents = contents + ',' + contentID ; 
            }
        }
        
        Ext.getCmp(me.id + '-ce').setValue(contents) ; 
        
        me.refreshCEGrid() ; 
        
        
    } ,
    
    // with : content list ( 1 , 2 ,3 )
    UpdateCEGrid2 : function( contentIDs ){
    	var me = this ; 
    	var contents = Ext.getCmp(me.id + '-ce').getValue() ; 
    	if( contents == '' || contents == null){
            contents = contentIDs ; 
        }else{
            contents = contents + ',' + contentIDs ; 
        }
        Ext.getCmp(me.id + '-ce').setValue(contents) ;  
        me.refreshCEGrid() ; 
    } ,
    
    refreshCEGrid : function(){
        var me = this ; 
        me.ceGrid.store.baseParams = {
            'contents' : Ext.get(me.id + '-ce').getValue()
        }
        me.ceGrid.store.load() ;     
    } ,
    
    setCEHidden : function(uid , hidden){
        var me = this ;  
        
        var data = {} ;  
        data[ mdcBase.firstPI + '[uid]'] = uid ; 
        data[ mdcBase.firstPI + '[hidden]'] = hidden ;    
        mdcBase.showProcessBox('Updating data...' , 'Updating data ... ') ;
        Ext.Ajax.request({
			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+mdc_contentmanager.pid+'&cmd=contentmanager/setHidden'  , 
			// send additional parameters to instruct server script
			method: 'POST',
			timeout: '30000',
			params:   data ,
			// process the response object to add it to the TabPanel:
			success: function(xhr) {
				Ext.MessageBox.hide(); 
				me.refreshCEGrid() ; 
			},
			failure: function() {
				Ext.MessageBox.hide();
				Ext.MessageBox.alert('Error', '#309' ) ; 
			}
        });
    } ,
    
    deleteContent : function(uid){
    	var me = this ; 
    	var rec = me.ceGrid.store.getById(uid) ; 
    	me.ceGrid.store.remove(rec) ;
    } , 
    
    showEditWindow : function(uid , toUid){ 
	    var me = this ; 
        if( toUid != 0 ){
            var rec = me.ceGrid.store.getById(uid) ; 
            pwContentEditor.elementName = rec.get('header') ; 
            pwContentEditor.openMainWindow(toUid , uid , 5) ; 
        }else{
            variation.openNewWindowWithPreload( uid , 5 ) ;
        }
    } ,
    
    getSelectSubElement : function(){
    	var me = this; 
    	var length = me.ceGrid.getSelectionModel().selections.length ; 
    	
    	var query = '' ; 
    	me.ceGrid.getSelectionModel().each(function(item , index){ 
    		//console.debug(item) ;
    		if( index == 0){
   				query = item.get('uid') ; 
    		}else{
    			query = query + ',' + item.get('uid') ; 
    		}
    		
    	}) ; 
    	return query
    } ,
    
    showPreviewWindow : function(contentId){
    	var me = this ; 
    	var rec = me.ceGrid.store.getById(contentId) ; 
		var name = rec.get('header') ; 
		var query =  me.getSelectSubElement() ; 
        ml_preview_content.openMainWindow(contentId , name , query ) ; 
    } ,
    
    innerPreview : function(){
        var me = this; 
        if( me.contentId == 0 ){
            me.saveProcess(1) ; 
        }else{
        	var query =  me.getSelectSubElement() ; 
            Ext.getCmp(me.id + '-preview').update('<iframe width="100%" height="100%"  frameborder="0" src="index.php?id=387&no_cache=1&uid='+me.contentId+'&query='+query+'"><p>Your browser does not support iframes.</p></iframe>') ;    
        }
        
    } ,
    
    changePosition : function(el , index){
        var me = this ; 
        Ext.get(me.id + el + '-left' ).setStyle('background-color','rgb(229,229,229)') ;  
        Ext.get(me.id + el + '-top' ).setStyle('background-color','rgb(229,229,229)') ;  
        Ext.get(me.id + el + '-bottom' ).setStyle('background-color','rgb(229,229,229)') ;  
        Ext.get(me.id + el + '-right' ).setStyle('background-color','rgb(229,229,229)') ;  
        
        if( index == 1){
            Ext.get(me.id + el + '-left' ).setStyle('background-color','rgb(204,204,204)') ;  
            Ext.getCmp(me.id +  el).setValue('left') ;
        }
        else if( index == 2){
            Ext.get(me.id + el + '-top' ).setStyle('background-color','rgb(204,204,204)') ;  
            Ext.getCmp(me.id + el).setValue('top') ;
        }else if( index == 3){
            Ext.get(me.id + el + '-bottom' ).setStyle('background-color','rgb(204,204,204)') ;  
            Ext.getCmp(me.id + el).setValue('bottom') ;
        }else if( index == 4){
            Ext.get(me.id + el + '-right' ).setStyle('background-color','rgb(204,204,204)') ;  
            Ext.getCmp(me.id + el).setValue('right') ;
        }
        
    }  , 
    
    ajaxPreProcess : function(){
        var me = this ; 
        if(me.contentId == 0 ){  
            me.window.show() ; 
            Ext.MessageBox.hide();
        }else{
            data = {} ; 
            data['contentId'] = pwContentEditor.contentId ; 
            if( pwMain.crossDomain){
                data['eID'] = 't3p_dynamic_content_placement' ; 
                data['pid'] = pwMain.pid ; 
                data['cmd'] = 'getCondition' ; 
                data['time'] = new Date().getTime();
                Ext.util.JSONP.request({
                    url: pwMain.baseUrl + 'index.php',
                    callbackKey: 'callback',
                    params : data , 
                    callback: function(jsonData) {  
                        me.condition = jsonData.tx_t3pconditionalcontent_condition ;
                        me.window.show() ;  
                        Ext.MessageBox.hide();
                    }
                });
            }else{
                Ext.Ajax.request({
        			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=getCondition'  , 
                    //url : 'testxml.php' , 
        			method: 'POST',
        			params:   data ,
        			timeout: '30000',
        			// process the response object to add it to the TabPanel:
        			success: function(xhr) {
         			    var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
                        me.condition = jsonData.tx_t3pconditionalcontent_condition ;
                        me.window.show() ;   
                        Ext.MessageBox.hide();
        			},
        			failure: function() {
        				Ext.MessageBox.hide();
    					Ext.MessageBox.alert('Error', '#323' ) ; 
        			}
        		});
            }
            
        }
        
    }   , 
	
	moveUp : function(index){
		var me = this ;
		var rec = me.ceGrid.store.getAt(index) ; 
		 me.ceGrid.store.remove(rec) ; 
		 
		 me.ceGrid.store.insert(index-1 , rec) ; 
		  
		  
		  me.ceGrid.getView().refresh();
	} , 
	
	moveDown : function(index){
		var me = this ;
		var rec = me.ceGrid.store.getAt(index) ; 
		 me.ceGrid.store.remove(rec) ; 
		 
		 me.ceGrid.store.insert(index+1 , rec) ; 
		 
		  me.ceGrid.getView().refresh();
	} , 
	
	openPositionWindow : function(){
		var posWindow = new Ext.Window({
            autoScroll : true , 
            width:500,
         	height: 340,
            title:"Edit position",
            id : 'content-editor-window' ,  
            plain:true ,       
         	modal: true,
         	maximizable : true  , 
         	closable:true , 
            items : [{
	            xtype : 'form' , 
	            width:  '100%' ,
	            autoHeight : true ,  
	            id : 'content-element-form' , 
	            border : true , 
	            bodyBorder : true ,  
	            bodyStyle : 'padding : 15px;' , 
	            layout : 'form' , 
	            defaults : {
	            	anchor : '100%'
	            } , 
	            items: [{
	            	xtype : 'label' , 
	                text	: 'X-Path'  , 
	                style   : 'color: rgb(20,65,138) ; font-weight: bold'
	            },{
	            	xtype : 'textfield' , 
	            	name : 'pos_element_id'  ,
					id : 'pos_element_id' ,  
                	ctCls  : 'cef-input'    , 
                	hideLabel : true , 
                	enableKeyEvents : true ,
                	value : Ext.getCmp('type3-element-id').getValue()
	            },{
	            	xtype : 'label' ,	
	            	text      : 'Position' , 
                	style     : 'color: rgb(20,65,138) ; font-weight: bold'
	            },{
	            	xtype : 'combo' , 
	            	typeAhead: true,
	                triggerAction: 'all' ,  
	                mode: 'local',
	                allowBlank: true  ,
	                editable: false,
	                hiddenName: 'pos_beforeafter' ,
					id : 'pos_beforeafter' ,   
	                width : 290 , 
	                displayField:   'name',
	                valueField:     'value' , 
	                hideLabel : true , 
	                store : pwMain.beforeAfterStore , 
	                value : Ext.getCmp('type3-before-after').getValue()
	            },{
	            	xtype : 'label' , 
	            	text      : 'Display Type' , 
                	style     : 'color: rgb(20,65,138) ; font-weight: bold'
	            },{
	            	xtype : 'combo' , 
	            	typeAhead: true,
	                triggerAction: 'all' ,  
	                mode: 'local',
	                allowBlank: true  ,
	                editable: false,
	                hiddenName: 'pos_displaytype' ,
					id : 'pos_displaytype' ,   
	                width : 290 , 
	                displayField:   'name',
	                valueField:     'value' , 
	                hideLabel : true , 
	                store : pwMain.displayTypeStore, 
	                value : Ext.getCmp('type3-display-type').getValue()
	            },{
	            	xtype : 'label' , 
	            	text      : 'Addition JS' , 
                	style     : 'color: rgb(20,65,138) ; font-weight: bold'
	            },{
	            	xtype : 'textarea' , 
	            	hideLabel : true , 
	                allowBlank: true  , 
	                width : 290 , 
	                name: 'pos_addjs' ,  
	                id : 'pos_addjs' , 
	                value :  Ext.getCmp('type3-addition-js').getValue()
	            }]
	        }] , 
            bodyStyle : 'padding : 5px 15px 15px 15px;background-color: white ; ' ,  
            buttons: [ 
            {
	            text:'CANCEL'  ,
	            handler: function(){ 
           			posWindow.close() ; 
	            }
	        },{
	            width : 40 , 
	            text:'OK'  ,
	            handler: function(){ 
           			Ext.getCmp('type3-addition-js').setValue( Ext.getCmp('pos_addjs').getValue() ) ;
					Ext.getCmp('type3-display-type').setValue( Ext.getCmp('pos_displaytype').getValue() ) ;
					Ext.getCmp('type3-before-after').setValue( Ext.getCmp('pos_beforeafter').getValue() ) ;  
					Ext.getCmp('type3-element-id').setValue( Ext.getCmp('pos_element_id').getValue() ) ;   
					posWindow.close() ; 
	            }
	        }] 
        }) ; 
        
        posWindow.show() ; 
	}
} ; 


window.predefinedCEWindow = {
    window : null , 

    panel1 : null , 
    panel2 : null , 
    
    // Tree object 
    idFolder : 0,
    tree : null , 
    typeDefault : 'content',
    
    // Grid Object ; 
    contentStore : null , 
    contentGrid : null , 
    
    openWindow : function(){
        var me =  this ; 
        me.initData() ; 
        
         me.window2 = new Ext.Window({
            width:800,
         	height:400,
            title:"CONTENT LIST (PRE-DEFINED)",
            plain:true , 
         	modal: true,
         	maximizable : true  , 
            resizable : true , 
         	closable:true , 
            id : 'ces-window-predefined2' , 
            //baseCls : 'ces-window' ,
            //iconCls : 'ces-window-header-icon' , 
            layout : 'border' , 
            items : [me.panel2 , me.panel1] , 
            listeners : {
                'close' : function(p){
                
                } 
            }
          }) ; 
          
        me.window2.show() ;   
    } , 
    
    initData : function(){
        var me = this ; 
        if(! pwMain.crossDomain){
            me.renderTree() ; 
        }else{
            me.renderBlankTree() ;
        }
        me.initGrid() ; 
        
        me.panel1 = new Ext.Panel({
	        region: 'center',
	        margins:'3 3 3 0', 
	        header : false  , 
            layout : 'fit' , 
            items : [me.contentGrid]
	    });
		 
		me.panel2 = new Ext.Panel({
	        region: 'west',
			title: 'Folder',
	        split: true,
	        width: 150,
			margins:'0',
	        cmargins:'0',
	        collapsible: true  , 
            items : [me.tree] , 
			listeners: {
				beforecollapse : function(panel){ 
				},
				
				expand : function(panel){
				    
				}
			}
	    });
        
        if( pwMain.crossDomain){
            me.panel2.setVisible(false) ; 
        }
    } , 
    
    initGrid : function(){
        var me = this ; 
        var field = [
	        	{name: 'uid', type: 'int'},
	        	{name: 'pid', type: 'int'},
	        	{name: 'type', type: 'int'},
	        	{name: 'width', type: 'int'},
                {name: 'lock', type: 'int'},
	        	{name: 'height', type: 'int'},
	            {name: 'header'},
	            {name: 'tmplType'} ,
	 			{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'} ,
	 			{name: 'css'}
	        ] ; 
        if(pwMain.crossDomain){
            me.contentStore = new Ext.data.JsonStore({
    	        root: 'topics',
    	        totalProperty: 'totalCount',
    	        idProperty: 'uid',
    	        remoteSort: true,
    	
    	        fields: field,
    	
    	        // load using script tags for cross domain, if the data in on the same domain as
    	        // this page, an HttpProxy would be better
    	        proxy: new Ext.data.ScriptTagProxy({
    	            url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentmanager/dynamicToolbarPredefineContent'
    	        }),
    	        
    	        baseParams:{
    				idfolder : me.idFolder
    			}
    	    });
        }
        else{
            me.contentStore = new Ext.data.JsonStore({
    	        root: 'topics',
    	        totalProperty: 'totalCount',
    	        idProperty: 'uid',
    	        remoteSort: true,
    	
    	        fields: field,
    	
    	        // load using script tags for cross domain, if the data in on the same domain as
    	        // this page, an HttpProxy would be better
    	        proxy: new Ext.data.HttpProxy({
    	            url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentmanager/dynamicToolbarPredefineContent&group'
    	        }),
    	        
    	        baseParams:{
    				idfolder : me.idFolder
    			}
    	    });
        }
        
        
	    me.contentStore.setDefaultSort('crdate', 'desc');
        
        function renderActions(value, id, r){
            var id = Ext.id(); 
            createSelectButton.defer(1, this, ['Select', id + '_1', r]); 
            createPreviewButton.defer(1, this, ['Preview', id + '_2', r]);
            return('<table><tr><td><div id="' + id + '_1"></div></td><td><div id="' + id + '_2"></div></div></td></tr></table>');
        }
        
        function createSelectButton(value, id, record) {
            new Ext.Button({
                text: value , 
                disabled : record.get('lock') 
                ,handler : function(btn, e) { 
                    contentEditorCE.UpdateCEGrid(record.get('uid')) ; 
                    me.window2.close() ; 
                }
            }).render(document.body, id);
        }
        
        function createPreviewButton(value, id, record) {
            new Ext.Button({
                text: value 
                ,iconCls : 'pw-menu-inspect-content'  
                ,handler : function(btn, e) { 
                    ml_preview_content.openMainWindow(record.get('uid') , record.get('header')) ;  
                }
            }).render(document.body, id);
        }
        
        me.contentGrid = new Ext.grid.GridPanel({
	        width:'100%',
	        height:355,
	        //title:'Simple newsletter listing',
	        store:me.contentStore, 
	        trackMouseOver:false,
	        //disableSelection:false,
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
	        //autoHeight: true,
	        enableDragDrop: true,
			ddGroup: 'grid2tree',
	        //clicksToEdit: 1,
 
	        columns:[{
	            header: "Description",
	            width: 120,
	            dataIndex: 'header',
	            sortable: true
	        },{
	            header: "Type",
	            dataIndex: 'tmplType',
	            width: 70,
	            sortable: true 
	        },
	   
	        {
	            id: 'crdate',  
	            header: "Date",
	            dataIndex: 'crdate',
	            width: 55,
	            renderer: Ext.util.Format.dateRenderer('d M Y'),
	            sortable: true   
	            
	        } ,{
	        	id : 'sn_action' , 
	            header: "Actions",
	            renderer: renderActions, 
	            dataIndex: 'uid',
	            width: 75,
	            sortable: false
	        }],
 
	        viewConfig: {
	            forceFit:true 
	        }, 
 
	        bbar: new Ext.PagingToolbar1({
	            pageSize: 15,
	            store: me.contentStore,
	            displayInfo: true,
	            displayMsg: 'Page {0} of {1}',
	            emptyMsg: "No topics to display"  
	        }), 
            
            listeners : {
                'rowclick' : function(grid , rowIndex , e){
                    var record = predefinedWindow.contentStore.getAt(rowIndex) ;  
                    me.window2.close() ; 
                }
            }
	    });
        me.contentGrid.store.load({params:{start:0, limit:15}});
    } , 
    
    refreshGrid : function(){
        var me = this ; 
        me.contentGrid.store.baseParams = {
            idfolder : me.idFolder
        } ; 
        
        me.contentGrid.store.load({params:{start:0, limit:15}});
    } , 
    
    renderBlankTree : function(){
        var me = this ; 
        me.tree = new Ext.Panel({
            html : ''
        }) ; 
    } , 
    
    renderTree : function(){
		// shorthand
        var me = this ; 
	    var Tree = Ext.tree;
	
	    me.tree = new Tree.TreePanel({
	        useArrows: true,
	        autoScroll: true,
	        animate: true,
	        enableDD: true,
	        ddGroup: 'grid2tree',
	        containerScroll: true,
	        border: false,
	        height: 280,
	        // auto create TreeLoader
	        dataUrl: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=listFolder&typedefault='+me.typeDefault,
	
	        root: {
	            nodeType: 'async',
	            text: 'Root',
	            draggable: false,
	            id: '0',
	            cls: 'folder'
	        },
	        
	        listeners: {
	           
	        	click : function( node, e ){
	        		me.changeFolder(node);
	        	} ,
	        	
	        	dblclick : function( node, e ){
	        		me.changeFolder(node);
	        	} ,
	        	
	        	expandnode : function( node ){
	        		me.changeFolder(node);
	        	}  
	        	 
			}
	    });

	    me.tree.getRootNode().expand();
	} , 
    
    changeFolder : function(node){
        var me = this ; 
		var id = node.getPath('id').split('/');
		id = id[id.length-1];
		var cls = node.getPath('cls').split('/');
		cls = cls[cls.length-1];

		if((me.idFolder != id) && (cls == 'folder')){
			me.idFolder = id;
			if(me.typeDefault == 'content')
				me.refreshGrid();//mdc_contentmanager.init();
			else
				me.refreshGrid();//mdc_placement_main.init();
		}
	} , 
	
	openStrategyPredefined : function(ref){
		var me = this ; 
		
		var treeClick = function(node){ 
			var id = node.getPath('id').split('/');
			id = id[id.length-1];
			var cls = node.getPath('cls').split('/');
			cls = cls[cls.length-1]; 
			if((me.idFolder != id || id == 0) && (cls == 'folder')){ 
				me.idFolder = id;
				var sg = Ext.getCmp('ces-strategy-predefined') ; 
				var g = sg.findByType('grid')[0] ; 
				g.refreshData() ; 
			}
		}
		
		function renderActions(value, id, r){
            var id = Ext.id(); 
            createSelectButton.defer(1, this, ['Select', id + '_1', r]);  
            return('<table><tr><td><div id="' + id + '_1"></div></td></tr></table>');
        }
        
        function createSelectButton(value, id, record) {
            new Ext.Button({
                text: value , 
                disabled : record.get('lock') 
                ,handler : function(btn, e) { 
                	//Create Template ; 
                	var content = '' ; 
                	if( Ext.getCmp('type3-element-id') != undefined ){
                		var xPath = Ext.getCmp('type3-element-id').getValue() ;  
                		content = selectTab.getHtmlFormXpath(xPath) ; 
                	} 
                	if( contentEditorCE.contentId == 0){
                	    content = selectTab.getHtmlFormXpath(pwMain.selectorTxt) ; 
                	} 
                	var sendData = {} ; 
                	monoloop_base.showProgressbar('Loading ..' ) ; 
                	sendData['uid'] = record.get('uid');  
                	sendData['selected_content'] = content ; 
                	if(pwMain.crossDomain){
			                    sendData['eID'] = 't3p_dynamic_content_placement' ;
			                    sendData['pid'] = pwMain.pid ; 
			                    sendData['cmd'] = 'contentmanager/createStrategy' ; 
			                    sendData['time'] = new Date().getTime();
			                    Ext.util.JSONP.request({
			                        url: pwMain.baseUrl + 'index.php',
			                        callbackKey: 'callback',
			                        params : sendData , 
			                        callback: function(result) {  
			                            Ext.MessageBox.hide();  
			                            contentEditorCE.UpdateCEGrid2(result.uid) ;   
			                            Ext.getCmp('ces-strategy-predefined').close() ; 
			                        }
			                    });
			                }else{ 
			                    Ext.Ajax.request({
			            			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentmanager/createStrategy' ,
			            			// send additional parameters to instruct server script
			            			method: 'POST',
			            			timeout: '30000',
			            			params:   sendData ,
			            			// process the response object to add it to the TabPanel:
			            			success: function(xhr) {
			            				Ext.MessageBox.hide(); 
										var data = Ext.util.JSON.decode(xhr.responseText) ; 
			    						contentEditorCE.UpdateCEGrid2(data.uid) ;  
			    						Ext.getCmp('ces-strategy-predefined').close() ; 
			            			},
			            			failure: function() {

			            				Ext.MessageBox.hide();   
			            			}
			            		});	
			                }
                    //
                }
            }).render(document.body, id);
        }
        
        var myStore = new Ext.data.JsonStore({ 
	        root: 'topics',
	        totalProperty: 'totalCount',
	        idProperty: 'uid',
	        remoteSort: true,
	
	        fields: [
	        	{name: 'uid', type: 'int'},
                {name: 'hidden', type: 'int'},
	        	{name: 'pid', type: 'int'},  
	        	{name: 'name'} ,{name: 'description'} ,
	 			{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'}  
	        ],
	
	        // load using script tags for cross domain, if the data in on the same domain as
	        // this page, an HttpProxy would be better
	        proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=monoloop_strategy&pid='+pwMain.pid+'&cmd=main/getlist'
	        }),
	        
	        baseParams:{
				idfolder : 0
			}
	    });
	    myStore.setDefaultSort('crdate', 'desc');
		
		var w = new Ext.Window({
            width:800,
         	height:400,
            title:"CONTENT LIST",
            plain:true , 
         	modal: true,
         	maximizable : true  , 
            resizable : true , 
         	closable:true , 
            id : 'ces-strategy-predefined' ,  
            layout : 'border' , 
            items : [{
            	region: 'west',
				title: 'Folder',
		        split: true,
		        width: 150,
				margins:'0',
		        cmargins:'0',
		        layout : 'fit' , 
		        collapsible: true  , 
	            items : [{
	            	xtype : 'treepanel' , 
			        useArrows: true,
			        autoScroll: true,
			        animate: true,
			        enableDD: true,
			        ddGroup: 'grid2tree',
			        containerScroll: true,
			        border: false,
			        height: 280,
			        // auto create TreeLoader
			        dataUrl: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=listFolder&typedefault=strategy', 
			        root: {
			            nodeType: 'async',
			            text: 'Root',
			            draggable: false,
			            id: '0',
			            cls: 'folder'
			        }, 
			        listeners: {
			           
			        	click : function( node, e ){
			        	 	treeClick(node) ; 
			        	} ,
			        	
			        	dblclick : function( node, e ){
			        	 	treeClick(node) ; 
			        	} ,
			        	
			        	expandnode : function( node ){
			        	 	treeClick(node) ; 
			        	}  
			        	 
					}
			    }]  
            },{
            	region: 'center',
		        margins:'3 3 3 0', 
		        header : false  ,
				layout : 'fit' ,  
	            items : [{
	            	xtype : 'grid' , 
	            	border : false , 
	            	width:'100%',  
			        store: myStore, 
			        trackMouseOver:false,
			        //disableSelection:false,
			        loadMask: true,
			        stripeRows: true,
			        enableHdMenu: false,
			        //autoHeight: true,
			        enableDragDrop: true,
					ddGroup: 'grid2tree',
			        //clicksToEdit: 1,
		 
			        columns:[{
			            header: "Name",
			            width: 120,
			            dataIndex: 'name',
			            sortable: true
			        },{
			            header: "Description",
			            dataIndex: 'description',
			            width: 120,
			            sortable: true 
			        }, 
			        {
			            id: 'crdate',  
			            header: "Date",
			            dataIndex: 'crdate',
			            width: 55,
			            renderer: Ext.util.Format.dateRenderer('d M Y'),
			            sortable: true   
			            
			        } ,{
			        	id : 'sn_action' , 
			            header: "Actions",
			            renderer: renderActions, 
			            dataIndex: 'uid',
			            width: 50,
			            sortable: false
			        }],
		 
			        viewConfig: {
			            forceFit:true 
			        }, 
		 
			        bbar: new Ext.PagingToolbar1({
			            pageSize: 15,
			            store: myStore,
			            displayInfo: true,
			            displayMsg: 'Page {0} of {1}',
			            emptyMsg: "No topics to display"  
			        }) , 
					
					refreshData :  function(){ 
						var g = this ; 
						g.store.baseParams = {
				            idfolder : me.idFolder
				        } ; 
				        
				        g.store.load({params:{start:0, limit:15}});
					} 
	            }]
            }]  , 
			listeners : {
				'afterrender' : function(w){
					var t = w.findByType('treepanel')[0] ; 
				 	t.getRootNode().expand();
				}
			} 
          }) ; 
          
        w.show() ;  
	}
 } ; 

return window.contentEditorCE ;
//---	
}) ; 