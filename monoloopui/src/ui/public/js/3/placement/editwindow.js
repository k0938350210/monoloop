define(['require' ] ,function(require){
//---
var obj = {
    data : {} , 
    openWindowByUid : function(uid){
        
    } , 
    openWindowByURL : function(url){
         var me = this ; 
         me.openWindow() ; 
    } , 
    
    openWindow : function(){
        var me = this ; 
        me.editWindow = new Ext.Window({
            title: 'Edit record',
            closable:true,
            width:550,
            height:350,
            layout : 'fit'  ,
             //border:false,
            modal: true,
            plain:true , 
 			items : [{
 			    xtype : 'form' , 
                width: '100%',
    	        frame: true,
    	        autoHeight: true,
    	        bodyStyle: 'padding: 0',
    	        labelWidth: 130,
    	        defaults: {
    	            anchor: '95%',
    	            allowBlank: false,
    	            msgTarget: 'side'
    	        },
                items : [{
            		xtype : 'displayfield' , 
            		fieldLabel : 'PageID' , 
            		value : me.data.mongoid 
            	}, 
                { 
	                fieldLabel: 'URL',
	                name: 'url', 
	                id : 'editwindow_url' , 
	                xtype: 'textfield' , 
	                value : me.data.url 
	            } ,
                { 
                    labelStyle: 'width:120px',
	                fieldLabel: 'Include www',
	                name: 'inc_www', 
	                id : 'editwindow_inc_www' , 
	                xtype: 'checkbox' , 
	                checked: me.data.inc_www  == 1  
	            }  ,
                {
                    xtype:          'combo',
                    mode:           'local', 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,
                    name: 'urlOption', 
                    fieldLabel:     'URL Options',
                    displayField:   'name',
                    valueField:     'value',
                    hiddenName :'urlOption' , 
                    value : me.data.urlOption ,
                    store:           new Ext.data.JsonStore({
                                fields : ['name',  {name: 'value', type: 'int'}],
                                data   : [
                                    {name : 'Exact Match',   value: 0 },
                                    {name : 'Allow Parameters',   value: 1 } ,
                                    {name : 'Starts with',   value: 2 }  
                                ]
                            }) 
	            },
                { 
	                fieldLabel: 'Regular Expression',
                    allowBlank: true  ,
	                name: 'preg_match', 
	                id : 'editwindow_preg_match' , 
	                xtype: 'textfield' , 
	                value : me.data.preg_match 
	            } ,
                { 
	                fieldLabel: 'Addition JS',
                    allowBlank: true  ,
	                name: 'additionJS', 
	                id : 'editwindow_additionJS' , 
	                xtype: 'textfield' , 
	                value : me.data.additionJS 
	            }  , 
                {
                    xtype:'textarea',
                    fieldLabel: 'Remark',
                    allowBlank: true  ,
                   	height  : 130 ,  
                    name: 'remark', 
                    value :  me.data.remark  
                },{
                    xtype : 'hidden' , 
                    name : 'uid', 
                    value : me.data.uid 
                }]
 			}] , 
            buttons: [{
	            text: 'Save' , 
	            cls: 'monoloop-black-btn' , 
	            handler: function(){ 
	               me.windowSave() ; 
	            }

	        },{
	            text: 'Close' , 
	            cls: 'monoloop-black-btn' , 
	            handler : function(){ 
	               me.windowClose() ; 
				}
	        }]
        });
        
        me.editWindow.show() ; 
    } , 
    
    windowSave : function(){
        var me = this ; 
        var form = me.editWindow.findByType('form')[0].getForm() ; 
        if( form.isValid()){
            form.submit({
                url: 'index.php/save/?eID=ml_cp',
                waitMsg: 'Saving your data...',
                success: function(form , o){
                    me.windowClose() ; 
                } , 
                failure: function(form , o) {
                    Ext.MessageBox.hide();   
    				mdcBase.msg('Fail',  o.result.msg); 
    			} 
            });
        } 
    } , 
    
    windowClose : function(){
        
    }
}

return obj ; 
//---	
}) ; 