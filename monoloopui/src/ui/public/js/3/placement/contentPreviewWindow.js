define(['require' ] ,function(require){
//---
var obj = {
    open : function(uid , name ){
        var query = '' ;  
    	if( arguments[2] )
    		query = arguments[2] ;	
        var showWindow = new Ext.Window({
            width:  1000 ,
         	height: 350 , 
         	title:"Preview: " + name,
         	autoScroll:true,
         	id : 'monoloop_preview_window' ,
         	modal:true,
         	constrain : true , 
         	maximizable : true  , 
         	animateTarget:"btnHello" ,
    		html : '<iframe width="100%" height="100%" onload="Ext.MessageBox.hide();" id="monoloop-preview-window"  frameborder="0" src="index.php?id=387&no_cache=1&uid='+uid+'&query='+query+'" ><p>Your browser does not support iframes.</p></iframe>'   , 
            listeners : {
                'close' : function(win){ 
                    Ext.MessageBox.hide();
                } , 
                'afterrender' : function(){ 
 					Ext.MessageBox.hide();
                }
            }
        }) ;  
        
        if( uid != 0){
            monoloop_base.showProgressbar('Retrieving Content...') ;     
        }
        
     	showWindow.show();
    }
} ; 

return obj ; 
//---	
}) ; 