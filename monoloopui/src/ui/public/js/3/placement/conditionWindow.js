define(function (require) {
// --- start

function ISODateString(d){
 function pad(n){return n<10 ? '0'+n : n}
 if( d == undefined || d == '') return ; 
 return d.getUTCFullYear()+'-'
      + pad(d.getUTCMonth()+1)+'-'
      + pad(d.getUTCDate())+'T'
      + pad(d.getUTCHours())+':'
      + pad(d.getUTCMinutes())+':'
      + pad(d.getUTCSeconds())+'Z'}

window.varionHelper = {
    initConditionStr : function(bodyElement){ 
        
        var contents = bodyElement.getElementsByTagName('mlt');
        for (var i=0; i < contents.length; i++) { 
            var conditionAtr = contents[i].getAttribute('condition') ; 
            if( conditionAtr == null)
                continue ; 
            varionHelper.clearConditionDisplay(contents[i]) ;     
            var firstEl = contents[i].firstChild ; 
            var newItem = document.createElement('span') ;  
            varionHelper.ml_addClass(newItem , 'mlt-c') ;
            newItem.innerHTML = '[' ; 
            newItem.style.color = "orange" ; 
            firstEl.parentNode.insertBefore(newItem , firstEl ) ; 
            
            var lastEl = contents[i].lastChild ; 
            var newItem = document.createElement('span') ;  
            varionHelper.ml_addClass(newItem , 'mlt-c') ;
            newItem.innerHTML = ']' ;
            newItem.style.color = "orange" ; 
            lastEl.parentNode.appendChild(newItem ) ;   
        }
    } , 
    
    cleanVariation : function(bodyElement){
        var tempHTML  = bodyElement.innerHTML ; 
        var newEL = document.createElement('div') ; 
        newEL.innerHTML = tempHTML ; 
        var contents = newEL.getElementsByTagName('mlt');
        for (var i=0; i < contents.length; i++) { 
            var conditionAtr = contents[i].getAttribute('condition') ; 
            if( conditionAtr == null)
                continue ; 
            varionHelper.clearConditionDisplay(contents[i]) 
        }
         
        return newEL.innerHTML ; 
    } , 
    
    clearConditionDisplay : function(contentElement){
        var firstEl = contentElement.firstChild ;
        if(varionHelper.ml_hasClass(firstEl , "mlt-c") ){
            contentElement.removeChild(firstEl) ; 
        }
        var lastEl = contentElement.lastChild ; 
        if(varionHelper.ml_hasClass(firstEl , "mlt-c") ){
            contentElement.removeChild(lastEl) ; 
        }
    } , 
    
    ml_hasClass : function(el, className, substring) {
    	if (!el || !el.className) return false;
    	var classes = el.className.trim().split(" ");
    	for (var i = classes.length; --i >= 0;) {
    		if (classes[i] == className || (substring && classes[i].indexOf(className) == 0)) return true;
    	}
    	return false;
    } , 
    
    ml_removeClass : function(el, className, substring) {
    	if (!el || !el.className) return;
    	var classes = el.className.trim().split(" ");
    	var newClasses = new Array();
    	for (var i = classes.length; --i >= 0;) {
    		if (!substring) {
    			if (classes[i] != className) {
    				newClasses[newClasses.length] = classes[i];
    			}
    		} else if (classes[i].indexOf(className) != 0) {
    			newClasses[newClasses.length] = classes[i];
    		}
    	}
    	if (newClasses.length == 0) {
    		if (!Ext.isOpera) {
    			el.removeAttribute("class");
    			if (Ext.isIE) {
    				el.removeAttribute("className");
    			}
    		} else {
    			el.className = '';
    		}
    	} else {
    		el.className = newClasses.join(" ");
    	}
    } ,
    
    ml_addClass : function(el, addClassName) {
    	varionHelper.ml_removeClass(el, addClassName);
    	if (el.className ) {
    		var classNames = el.className.trim().split(" ");
    	}
    	if (el.className) el.className += " " + addClassName;
    		else el.className = addClassName;
    }
} ; 

Ext.form.CompositeField.prototype.initComponent = 
	Ext.form.CompositeField.prototype.initComponent.createSequence(function(){

	if (!this.innerCt.ownerCt) {
		this.innerCt.ownerCt = this;
	} else {
		// The bug has been fixed in this version of Ext, this hack is no longer needed
		//debugger;
	}
});

Ext.ux.ConditionFieldPanel = Ext.extend(Ext.Panel, {
   fieldParams : null , 
   fieldValue : null  , 

   level : 0  , 
   parentPanel : null 
}) ; 

Ext.reg('ConditionFieldPanel', Ext.ux.ConditionFieldPanel);

Ext.ux.DropZonePanel = Ext.extend(Ext.Panel, {
    bodyCssClass : 'dropzone-panel' ,
    style : 'float: right;' , 
    height : 20 , 
    myType : 0 // 0 : OR , 1 : AND  ; 
}) ; 

Ext.reg('DropZonePanel', Ext.ux.DropZonePanel);

Ext.ux.ConditionComposite = Ext.extend(Ext.form.CompositeField, {
   connector : null , 
   monoloopField : null ,   
   monoloopType : null , 
   monoloopReturnValue : null  
}) ; 

Ext.reg('compositefield', Ext.ux.ConditionComposite);


window.condition = {
    
    windowObj : null , 
    
    westPanel : null , 
    centerPanel : null , 
    globalLock : false , 
    
    // Operator ; 
    integerOperatorStore : null ,
    stringOperatorStore : null , 
    dateOperatorStore : null , 
    multiOperatorStore : null , 
    andorOperatorStore : null , 
    
    // Pre condition 
    preconditionStr : null , 
    precondition : null , 
    contentId : 0 , 
    stage : 0 ,
    crossDomain :false , 
    
    // For Object response ; 
    objectReturn : null , 
    
    // Test mode ; 
    showSaveBubble : false , 
    
    openMainWindow : function(){ 
        Ext.QuickTips.init();
        condition.initData() ;  
     
        condition.windowObj = new Ext.Window({
            width:800,
         	height:600,
            title:"Condition Builder",
            plain:true , 
         	modal:true,
         	maximizable : true  , 
         	closable:true  , 
            layout : 'border' , 
            listeners : {
                'resize' : function( win , adjWidth, adjHeight ){
                    condition.processAdjustConditionPanelWidth()  ; 
                    
                } , 
                'afterlayout' : function(container , layout){
                    // Render Line
                    condition.renderBackGroudLayout() ; 
                    if(condition.crossDomain ){
                        Ext.getCmp('c-btn-load').setVisible(false) ; 
                        Ext.getCmp('c-btn-saveas').setVisible(false) ; 
                    }
                } , 
                'beforeclose' : function(w){ 
                    Ext.dd.ScrollManager.unregister(Ext.getCmp('main-panel').body);
                } , 
                
                'show' : function(w){
                    if( condition.crossDomain ){ 
                          if( ml_testbubble != undefined){
                              ml_testbubble.open6(w) ; 
                          }
                     }
                } , 
                
                'move' : function(w,x,y){
                    if( condition.crossDomain ){ 
                          if( ml_testbubble != undefined){
                               var parentPos = w.getPosition() ;  
                               if(Ext.getCmp('test_bubble_6') != undefined){
                                    Ext.getCmp('test_bubble_6').setPosition(parentPos[0] - 250 , parentPos[1] + 100) ; 
                               }
                               if(Ext.getCmp('test_bubble_7') != undefined){
                                    Ext.getCmp('test_bubble_7').close() ; 
                               }
                          }
                     } 
                } , 
                
                'close' : function(w){
                    if( condition.crossDomain ){ 
                          if( ml_testbubble != undefined){
                              if( Ext.getCmp('test_bubble_6') != undefined){
                                  Ext.getCmp('test_bubble_6').close() ;        
                              }
                          }
                     }
                }
            } , 
            items : [condition.westPanel , condition.centerPanel]
        }) ; 
        condition.windowObj.show();
        
        
        
        condition.renderConnector() ;
        
        var mainPanel = Ext.getCmp('main-panel') ;
        Ext.dd.ScrollManager.register(mainPanel.body);
        var field = new Ext.form.DisplayField({
            value : '<div id="cbuilder-layout"></div>'    , 
            hideLabel : true  
        })  ;
        
        mainPanel.add(field) ; 
        
        mainPanel.doLayout() ; 
        
        
        
        //<div class="cbuilder-line-and" style="width:100px ; height:0px;"></div>
    } , 
    
    initData : function(){
        //http://test.monoloop.com/index.php?eID=t3p_conditional_content&pid=1&cmd=getFieldList&connector=43&group=General
        //myTree.dragZone.addToGroup('dropGroup2');
        condition.initStore() ;   
        condition.westPanel = new Ext.Panel({
	        region: 'west',
            id : 'west-panel' , 
			title: 'Condition Parameters',
	        split: true,
	        width: 250,
			margins:'0',
	        cmargins:'0',
	        collapsible: true , 
            autoScroll : true
	    });
        
        condition.centerPanel = new Ext.Panel({
            height : 555 , 
            region: 'center',
            buttons : [{
                text : 'load' , 
                id : 'c-btn-load' , 
                cls: 'monoloop-black-btn' , 
                handler: function(){ 
                    saved_condition.loadWindow() ; 
                }   
            },
            {
                text : 'save as' , 
                cls: 'monoloop-black-btn' , 
                id : 'c-btn-saveas' , 
                handler: function(){
                    if ( Ext.util.Format.trim(Ext.getCmp('condition-result').getValue()) == '' ){
                        alert('Cannot save in blank condition.') ; 
                        return  ;
                    }
                    saved_condition.openWindow() ; 
                }
            },{
                text : 'save' , 
                cls: 'monoloop-black-btn' , 
                handler: function(){
                    if(condition.stage == 1){
                        condition.saveTo_ttContent() ; 
                    }else if(condition.stage == 2){
                        condition.saveTo_variation() ; 
                    }else if(condition.stage == 3){
                        condition.saveTo_contenteditor() ; 
                    }else if(condition.stage == 4){
                        condition.saveTo_triggerMail() ; 
                    }else if(condition.stage == 5){
                        condition.saveTo_htmlEditor() ; 
                    }else if(condition.stage == 6){
                        condition.saveTo_htmlEditor2() ; 
                    }else if(condition.stage == 7){
                        condition.saveTo_ttContent2() ; 
                    }else if(condition.stage == 8){
                        condition.saveTo_dynamiCCE() ; 
                    }else if(condition.stage == 9){
                        condition.saveTo_assetElement() ; 
                    }else if(condition.stage == 11){
                        condition.saveTo_goalCreateNew() ; 
                    }else if(condition.stage == 12){
                        condition.saveTo_goalEdit() ; 
                    }else if(condition.stage == 13){
                        condition.saveTo_goalPlacementEdit() ;  
                    }else if(condition.stage == 21){
                        condition.saveTo_newsletterFilter() ;  
                    }else if(condition.stage == 22){
                        condition.saveTo_newsletterEditSubjectline() ;  
                    }else if(condition.stage == 23){
                        condition.saveTo_newsletterEditTextMode() ; 
                    }else if(condition.stage == 24){
                    	condition.saveTo_segment_new() ; 	
                    }else if(condition.stage == 24){
                    	condition.saveTo_segment_new() ; 	
                    }else if(condition.stage == 25){
                    	condition.saveTo_strategy_new() ; 	
                    }else if(condition.stage == 31){
                    	condition.saveTo_contentlist() ;
                    }else if(condition.stage == 32){
                    	condition.saveTo_contentListPage() ; 
                    }else if(condition.stage == 33){
                    	condition.saveTo_contentMultipleListPage()  ;
                    }else if(condition.stage == 34){
                    	condition.saveTo_multiselectedForm() ; 
                    }else if(condition.stage == 41){
                    	condition.saveTo_experimentSegment() ; 
                    }else if(condition.stage == 42){
                    	condition.saveTo_experimentGoal() ; 
                    }else{
                        alert('Not implement this save stage yet .')
                    }
                }
            }
            ] ,
            
            layout: 'accordion' , 
                   
            items : [
            {
                xtype : 'panel' , 
                id : 'top-panel' ,  
                title : 'Advanced' , 
               	collapsed : true , 
                style : ' background-color: #E7FCFE; ' , 
                layout : 'form' , 
                bodyStyle : 'padding-top : 10px ; padding-left : 10px ; border : none ; background-color: #E7FCFE; ' , 
                items : [{
                    xtype : 'textarea' , 
                    hideLabel  : true  , 
                    disabled : true , 
                    labelStyle  : 'width : 60px ; font-size: 15px ;font-weight: bold;' , 
                    value : '' , 
                    id : 'condition-result' , 
                    width : 500    , 
                    height : 460 
                }] 
                
            },
            {
                xtype : 'panel' , 
                id : 'main-panel' , 
                region: 'center',
                margins:'3 3 3 0',  
                title : 'Condition Builder' ,    
                autoScroll : true , 
                hideCollapseTool : true , 
                height : 455, 
                listeners : {
                    'beforecollapse' : function(panel , animate){
                        return false ;     
                    },    
                    'render' : function(panel){  
                        var formPanelDropTargetEl =  panel.body.dom;
                        var formPanelDropTarget = new Ext.dd.DropTarget(formPanelDropTargetEl, {
                            ddGroup     : 'dropGroup2',
                            notifyEnter : function(ddSource, e, data) {
                              //Add some flare to invite drop.
                              panel.body.stopFx();
                              //panel.body.highlight();
                            },
                            notifyDrop  : function(ddSource, e, data){
                              // center-panel
                              if(condition.globalLock == true)
                                    return false ;
                                    
                              if( data.panel != undefined){
                                    var mainPanel = Ext.getCmp('main-panel') ;
                                    //ddSource.panel.el.dom.parentNode.removeChild(ddSource.panel.el.dom) ; 
                                    //mainPanel.add(ddSource.panel) ; 
                                    mainPanel.doLayout() ; 
                                    return true ; 
                              }else{    
                                    condition.removeDropZone() ;  
                                    condition.addPanel(panel , data.node.attributes.text , ddSource.dragData.node.attributes) ;
                                    if( condition.crossDomain ){ 
                                        if( ml_testbubble != undefined){
                                            if(  Ext.getCmp('main-panel').items.getCount() == 2){
                                                ml_testbubble.open7(condition.windowObj) ; 
                                                //condition.showSaveBubble = true ;
                                            }
                                        }
                                    }
                              }
                               
                              return(true);
                            }
                        });
                    }
                } 
            }
            ]
            
        }) ;
        
       
 
    } , 
    
    processAdjustConditionPanelWidth : function(){
        var mainPanel =  Ext.getCmp('main-panel');
        var allPanel = mainPanel.findByType('ConditionFieldPanel') ;
        if( allPanel.length == 0 )
            return ; 
        //console.debug('window : ' + condition.windowObj.getWidth() ) ; 
        //console.debug('main panel : ' + mainPanel.getWidth() ) ;   
 
        for( var i = 0 ; i < allPanel.length ; i++){
           allPanel[i].setWidth( ( 100 - 5 * (allPanel[i].level  )) + '%' ) ; 
        } 
        mainPanel.doLayout() ; 
    } , 
    
    createBlankPanel : function(){
        var tools = [ {
            id:'close',
            handler: function(e, target, panel){
                panel.ownerCt.remove(panel, true);
                condition.genAndOr() ; 
                
                condition.renderBackGroudLayout() ; 
            }
        }];
        
        var blankPanel = new Ext.ux.ConditionFieldPanel({
           fieldParams : null , 
           title: '',     
           collapsible: true ,  
           width : '95%' ,  
           height : 'auto' ,  
           cls : 'ConditionFieldPanel' , 
           style: 'overflow : auto ;' , 
           layout : 'form' ,  
           draggable : {
                ddGroup : 'dropGroup2' , 
                startDrag : function(x , y){
                    condition.showAllDropZone() ;
                    condition.assignAllDropZone() ; 
                    return true ; 
                } , 
                onBeforeDrag : function(data ,e){
                    //alert('xxx') ; 
                    var mainPanel =  Ext.getCmp('main-panel');
                    condition.insertDropZone3(mainPanel , blankPanel , true) ; 
                    return true;
                },
                endDrag : function(e){
                    condition.removeDropZone() ;
                    condition.genAndOr() ; 
                }
           }  , 
           tools: tools,
           tbar: [
                {
                    xtype : 'combo' ,
                    mode:           'local',  
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false, 
                    hideLabel  : true , 
                    displayField:   'name',
                    valueField:     'value',  
                    store:  condition.andorOperatorStore ,  
                    width:60  , 
                    listeners : {
                        'render' : function(combo){
                            record = condition.andorOperatorStore.getAt(0) ; 
                            combo.setValue(record.get('value'));
                        } , 
                        'select' : function(combo , record , index ){
                            condition.genCondition() ; 
                        }
                    }
                }
            ]  
        });  
        
        return blankPanel ;  
    } ,
    
    addPanel : function(panel1 , title1 , dragAttr){

        // Base is main-panel ; 
        if( panel1.getId() != 'main-panel'){
            //console.debug(panel1.getComponent(0).getXType()) // items 
            var totalCount = panel1.items.getCount() ; 
            if(totalCount > 0){
                if( panel1.getComponent(0).getXType() == 'compositefield'){
                    data = panel1.removeAll(false) ;
                    var blankPanel =  condition.createBlankPanel() ;  
                    blankPanel.add(data) ; 
                    panel1.add(blankPanel) ; 
                }
            }
        }
        //myPanel.add(myPortal.add(conditionPanel))
        //conditionPanel.add(myPortal.add(myPanel)) ; 
        var conditionPanel = condition.createConditionPanel(title1 ,dragAttr) ; 
 
        if( panel1.getId() != 'main-panel'){
            conditionPanel.level = panel1.level + 1 ; 
            conditionPanel.parentPanel = panel1 ; 
        }else{
            conditionPanel.level = 1 ; 
            conditionPanel.parentPanel = panel1 ; 
        }
        
        panel1.add(conditionPanel) ; 
        
        panel1.doLayout() ;
        
        condition.genAndOr() ; 
        
    } , 
    
    createConditionPanel : function(title1 ,dragAttr){
        var tools = [ {
            id:'close',
            handler: function(e, target, panel){
                condition.chageChildPanelToUpper(panel) ; 
                panel.ownerCt.remove(panel, true);
                condition.genAndOr() ; 
                condition.renderBackGroudLayout() ; 
            }
        }]; 
        
        var conditionPanel = new Ext.ux.ConditionFieldPanel({
           fieldParams : dragAttr , 
           title: title1,     
           collapsible: true ,  
           width : '95%' ,  
           height : 'auto' ,  
           cls : 'ConditionFieldPanel' , 
           style: ' margin-left : 20px ;margin-right : 10px ;    overflow : auto ;    float: right;  ' , 
           layout : 'form' ,  
           draggable : {
                ddGroup : 'dropGroup2' , 
                startDrag : function(x , y){
                    condition.removeBackGroundLayout() ; 
                    condition.showAllDropZone() ;
                    condition.assignAllDropZone() ; 
                    return true ; 
                } , 
                onBeforeDrag : function(data ,e){
                    //alert('xxx') ; 
                    var mainPanel =  Ext.getCmp('main-panel');
                    condition.insertDropZone3(mainPanel , conditionPanel , true) ; 
                    return true;
                },
                endDrag : function(e){
                    condition.removeDropZone() ;
                    condition.genAndOr() ; 
                    condition.renderBackGroudLayout() ; 
                }
           }  , 
           tools: tools  , 
           listeners : {
                'collapse' : function(p){
                    p.doLayout() ; 
                    condition.renderBackGroudLayout() ; 
                },
                'expand' : function(p){
                    p.doLayout() ; 
                    condition.renderBackGroudLayout() ; 
                } 
            }
 
        });  
        
        // Ignor if blank type * not used now; 
        if( conditionPanel.fieldParams.type ==  'blank'){
            panel1.add(conditionPanel) ; 
            panel1.doLayout() ;
            condition.genAndOr() ; 
 
            return conditionPanel ;    
        }
        
        if( (conditionPanel.fieldParams.type ==  'function' && conditionPanel.fieldParams.ReturnValue == 'string') || conditionPanel.fieldParams.type ==  'string'){
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type ,  
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text 
                    }, 
                    {
                        xtype : 'combo' ,
                        mode:           'local',  
                        allowBlank: false  ,
                        triggerAction:  'all',
                        editable:       false, 
                        hideLabel  : true , 
                        displayField:   'name',
                        valueField:     'value',  
                        store:  condition.stringOperatorStore ,  
                        width:120  , 
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.operand == null){
                                    record = condition.stringOperatorStore.getAt(0) ; 
                                    combo.setValue(record.get('value'));
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.operand);
                                }
                                
                            } , 
                            'select' : function(combo , record , index ){
                                condition.genCondition() ; 
                            }
                        }
                    },
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    }, 
                    {
                        xtype: 'textfield',
                        value: conditionPanel.fieldParams.value,
                        width: 100 , 
                        listeners : {
                            'change' : function(textBox , newVal , oldVal){
                                condition.genCondition() ; 
                            }
                        }
                    } 
                ]
            }) ; 
        }
        else if( conditionPanel.fieldParams.type ==  'bool' ){
            var checked = conditionPanel.fieldParams.value ; 
            if( checked == 'true' || checked == true){
                checked = true ; 
            }else if(checked == 'false' || checked == false){
                checked = false ; 
            }
            var defaultD = false ; 
            if( conditionPanel.fieldParams['default'] == 'true' || conditionPanel.fieldParams['default'] == true ){
                defaultD = true ; 
            }
            
            if( checked == undefined){
                checked = defaultD ;
            }
            
            //console.debug(checked) ; 
             
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text  
                    }, 
                    {
                        xtype : 'displayfield' ,
                        value : 'is' , 
                        width: 30  
                    } , 
                    {
                        xtype: 'checkbox', 
                        checked : checked, 
                        width: 100 , 
                        listeners : {
                            'check' : function( chk , val){
                                condition.genCondition() ; 
                            }
                        }
                    } 
                ]
            }) ; 
        }
        else if( (conditionPanel.fieldParams.type ==  'function' && conditionPanel.fieldParams.ReturnValue == 'date') || conditionPanel.fieldParams.type ==  'date' ){
        	//console.debug(conditionPanel.fieldParams.value) ; 
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text 
                    }, 
                    {
                        xtype : 'combo' ,
                        mode:           'local',  
                        allowBlank: false  ,
                        triggerAction:  'all',
                        editable:       false, 
                        hideLabel  : true , 
                        displayField:   'name',
                        valueField:     'value',  
                        store:  condition.dateOperatorStore ,  
                        width: 100  ,
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.operand == null){
                                    record = condition.dateOperatorStore.getAt(0) ; 
                                    combo.setValue(record.get('value'));
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.operand);
                                }
                            } , 
                            'select' : function(combo , record , index ){
                                condition.genCondition() ; 
                            }
                        }
                    },
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    }, 
                    {
                    	xtype : 'datetimefield' , 
                    	dateFormat: 'j M Y', 
				        timeFormat: 'g:i A', 
				        picker: {
				            timePicker: new Ext.ux.ExBaseTimePicker({ 
				                hourIncrement: 1,
				                minIncrement: 1
				            })
				        },
				        tmpValue : new Date(conditionPanel.fieldParams.value),
						//value: new Date(conditionPanel.fieldParams.value),
                        showToday : true  , 
                        listeners : {
                            'select' : function(dataField , val){
                                condition.genCondition() ; 
                            } , 
                            'render' : function(field){
                            	if (! isNaN( field.tmpValue.getTime() ) ) {
                            		field.setValue(field.tmpValue) ; 
                           		}
                            }
                        }
                    } /* ,
                    {
                        xtype: 'datefield', 
                        width: 100 ,
                        format : 'd/m/Y' , 
                        value: new Date(conditionPanel.fieldParams.value),
                        showToday : true  , 
                        listeners : {
                            'select' : function(dataField , val){
                                condition.genCondition() ; 
                            }
                        }
                    }  */              
                ]
            });   
        }
        else if( conditionPanel.fieldParams.type ==  'single remote' ){
           //console.debug(conditionPanel.fieldParams.values) ; 
           var singleLocalStore = null ; 
           if( condition.crossDomain  ){ 
                singleLocalStore = new Ext.data.JsonStore({ 
                    root: 'data',  // the root of the array you'll send down
                    idProperty: 'v',
                    proxy: new Ext.data.ScriptTagProxy({
                                url: conditionPanel.fieldParams.values
                            }),
                    fields: ['v'] 
                }) ;  
            }else{
                singleLocalStore = new Ext.data.JsonStore({
                    url:conditionPanel.fieldParams.values,
                    root: 'data',  // the root of the array you'll send down
                    idProperty: 'v',
                    fields: ['v'] 
                }) ;  
            }
            
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text 
                    }, 
                    {
                        xtype : 'combo' ,
                        mode:           'local',  
                        allowBlank: false  ,
                        triggerAction:  'all',
                        editable:       false, 
                        hideLabel  : true , 
                        displayField:   'name',
                        valueField:     'value',  
                        store:  condition.stringOperatorStore ,  
                        width:120  , 
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.operand == null){
                                    record = condition.stringOperatorStore.getAt(0) ; 
                                    combo.setValue(record.get('value'));
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.operand);
                                }
                            }, 
                            'select' : function(combo , record , index ){
                                condition.genCondition() ; 
                            }
                        }
                    },
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    }, 
                    {
                        xtype : 'combo' ,  
                        triggerAction:  'all', 
                        typeAhead: true,
                        mode: 'remote', 
                        hideTrigger: true,     
                        selectOnFocus:true, 
                        queryParam: 'query' , 
                        hideLabel  : true , 
                        minChars : 3 , 
                        displayField:   'v',
                        valueField:     'v',  
                        hiddenName : 'myID' , 
                        store:  singleLocalStore ,  
                        width:100 , 
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.value == null){ 
                                    ;
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.value);
                                }
                                //alert(combo.getValue()) ; 
                            }, 
                            'change' : function(combo , newV , oldV){ 
                                condition.genCondition() ; 
                            }
                        }
                    }
                ]
            }) ;
        }
        else if( conditionPanel.fieldParams.type ==  'single remote2' ){
           //console.debug(conditionPanel.fieldParams.values) ;  
           var singleLocalStore = null ; 
           if( condition.crossDomain ){ 
                singleLocalStore = new Ext.data.JsonStore({ 
                    conditionPanel : conditionPanel ,  
                    idProperty: 'v',
                    proxy: new Ext.data.ScriptTagProxy({
                                url: conditionPanel.fieldParams.values
                            }),
                    fields: ['v' , 'k' ]  
                }) ; 
           }else{
                singleLocalStore = new Ext.data.JsonStore({
                    url:conditionPanel.fieldParams.values,
                    conditionPanel : conditionPanel ,  
                    idProperty: 'v',
                    fields: ['v' , 'k' ]  
                }) ; 
           }
            
            /*
            var data = {} ; 
            data['v'] = 'xxx' ; 
            data['k'] = 'yyy' ;   
            singleLocalStore.loadData(data) ; 
            */ 
            if( conditionPanel.fieldParams.value != null){
                singleLocalStore.loadData(conditionPanel.fieldParams.value2) ;     
            }
            
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text 
                    }, 
                    {
                        xtype : 'combo' ,
                        mode:           'local',  
                        allowBlank: false  ,
                        triggerAction:  'all',
                        editable:       false, 
                        hideLabel  : true , 
                        displayField:   'name',
                        valueField:     'value',  
                        store:  condition.stringOperatorStore ,  
                        width:120  , 
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.operand == null){
                                    record = condition.stringOperatorStore.getAt(0) ; 
                                    combo.setValue(record.get('value'));
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.operand);
                                }
                            }, 
                            'select' : function(combo , record , index ){
                                condition.genCondition() ; 
                            }
                        }
                    },
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    }, 
                    {
                        xtype : 'combo' ,   
                        typeAhead: true,
                        triggerAction:  'all', 
                        mode: 'remote', 
                        hideTrigger: true,     
                        selectOnFocus:true, 
                        queryParam: 'query' , 
                        hideLabel  : true , 
                        minChars : 2 , 
                        displayField:   'k',
                        valueField:     'v',  
                        hiddenName : 'myID' , 
                        store:  singleLocalStore ,  
                        width:100 , 
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.value == null){ 
                                    ;
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.value);
                                }
                                //alert(combo.getValue()) ; 
                            }, 
                            'change' : function(combo , newV , oldV){ 
                                condition.genCondition() ; 
                            }
                        }
                    }
                ]
            }) ; 
        }
        else if( conditionPanel.fieldParams.type ==  'single' ){
            //console.debug(conditionPanel.fieldParams.values) ; 
            singleLocalStore = new Ext.data.JsonStore({
                fields : ['label', 'value' ],
                data   : conditionPanel.fieldParams.values
            }) ; 
            
            var comp = {
                xtype : 'combo' ,
                mode:           'local',  
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false, 
                hideLabel  : true , 
                displayField:   'name',
                valueField:     'value',  
                store:  condition.stringOperatorStore ,  
                width:120  , 
                listeners : {
                    'render' : function(combo){
                        if( conditionPanel.fieldParams.operand == null){
                            record = condition.stringOperatorStore.getAt(0) ; 
                            combo.setValue(record.get('value'));
                        }else{
                            combo.setValue(conditionPanel.fieldParams.operand);
                        }
                    }, 
                    'select' : function(combo , record , index ){
                        condition.genCondition() ; 
                    }
                }
            } ; 
            
            
            
            var valueField = {
                xtype : 'combo' ,
                mode:           'local',  
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false, 
                hideLabel  : true , 
                displayField:   'label',
                valueField:     'value',  
                hiddenName : 'myID' , 
                store:  singleLocalStore ,  
                width:100 , 
                listeners : {
                    'render' : function(combo){
                        if( conditionPanel.fieldParams.value == null){
                            record = singleLocalStore.getAt(0) ; 
                            combo.setValue(record.get('value'));
                        }else{
                            combo.setValue(conditionPanel.fieldParams.value);
                        }
                        //alert(combo.getValue()) ; 
                    }, 
                    'select' : function(combo , record , index ){
                        condition.genCondition() ; 
                    }
                }
            } ;
            
            if( conditionPanel.fieldParams.hidecompare == true){
            	comp.hidden = true ; 
            	comp.width = 10 ; 
            	valueField.width = 210 ; 
            }
            
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text 
                    }, 
                    comp,
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    }, 
                    valueField
                ]
            }) ; 
        }
        else if( conditionPanel.fieldParams.type ==  'multi' ){
            //console.debug(conditionPanel.fieldParams.values) ; 
            var singleLocalStore = new Ext.data.JsonStore({
                fields : ['label', 'value' ],
                data   : conditionPanel.fieldParams.values
            }) ; 
            
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text 
                    }, 
                    {
                        xtype : 'combo' ,
                        mode:           'local',  
                        allowBlank: false  ,
                        triggerAction:  'all',
                        editable:       false, 
                        hideLabel  : true , 
                        displayField:   'name',
                        valueField:     'value',  
                        store:  condition.multiOperatorStore ,  
                        width:120  , 
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.operand == null){
                                    record = condition.multiOperatorStore.getAt(0) ; 
                                    combo.setValue(record.get('value'));
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.operand);
                                }
                            }, 
                            'select' : function(combo , record , index ){
                                condition.genCondition() ; 
                            }
                        }
                    },
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    }, 
                    {
                        xtype: 'multiselect', 
                        width: 100,
                        height: 50,
                        displayField:   'label',
                        valueField:     'value',  
                        delimiter : '||' , 
                        allowBlank:false,
                        store: singleLocalStore ,
                        value: conditionPanel.fieldParams.value,
                        listeners : {
                             
                            'click' : function(combo  ){
                                condition.genCondition() ; 
                            }
                        }
                    }
                ]
            }) ; 
        }else if( (conditionPanel.fieldParams.type ==  'function' && conditionPanel.fieldParams.ReturnValue == 'integer') || conditionPanel.fieldParams.type ==  'integer' ){
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text  
                    }, 
                    {
                        xtype : 'combo' ,
                        mode:           'local',  
                        allowBlank: false  ,
                        triggerAction:  'all',
                        editable:       false, 
                        hideLabel  : true , 
                        displayField:   'name',
                        valueField:     'value',   
                        store:  condition.integerOperatorStore ,  
                        width:60  , 
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.operand == null){
                                    record = condition.integerOperatorStore.getAt(0) ; 
                                    combo.setValue(record.get('value'));
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.operand);
                                }
                            } , 
                            'select' : function(combo , record , index ){
                                condition.genCondition() ; 
                            }
                        } 
                    },
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    }, 
                    {
                        xtype: 'numberfield',
                        value: conditionPanel.fieldParams.value ,
                        width: 100 , 
                        listeners : {
                            'change' : function(textBox , newVal , oldVal){
                                condition.genCondition() ; 
                            }
                        }
                    }                
                ]
            });
        }else if( conditionPanel.fieldParams.type ==  'function' ){
             
            var checked = conditionPanel.fieldParams.value ;  
            if( checked == 'true' || checked == true){
                checked = true ; 
            }else if( checked == 'false' || checked == false){
                checked = false ;
            }
            
            var defaultD = false ;
            if( conditionPanel.fieldParams['default'] == 'true' || conditionPanel.fieldParams['default'] == true)
                defaultD = true ; 
            
            if( checked == undefined )    
                 checked = defaultD ;  
             var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text + '()' 
                    } , 
                    {
                        xtype : 'displayfield' ,
                        value : 'is' , 
                        width: 30  
                    } , 
                    {
                        xtype: 'checkbox', 
                        checked : checked , 
                        width: 100  , 
                        listeners : {
                            'check' : function( chk , val){
                                condition.genCondition() ; 
                            }
                        }
                    } 
                ]
             }) ; 
        }
        else {
            var myField = new Ext.ux.ConditionComposite({ 
                connector : conditionPanel.fieldParams.connector , 
                monoloopField : conditionPanel.fieldParams.id , 
                monoloopType : conditionPanel.fieldParams.type , 
                monoloopReturnValue :  conditionPanel.fieldParams.ReturnValue ,  
                msgTarget : 'side', 
                hideLabel : true , 
                defaults: {
                    flex: 1
                } , 
                items : [
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    } ,
                    {
                        xtype: 'displayfield',
                        value: conditionPanel.fieldParams.text  
                    }, 
                    {
                        xtype : 'combo' ,
                        mode:           'local',  
                        allowBlank: false  ,
                        triggerAction:  'all',
                        editable:       false, 
                        hideLabel  : true , 
                        displayField:   'name',
                        valueField:     'value',   
                        store:  condition.integerOperatorStore ,  
                        width:60  , 
                        listeners : {
                            'render' : function(combo){
                                if( conditionPanel.fieldParams.operand == null){
                                    record = condition.integerOperatorStore.getAt(0) ; 
                                    combo.setValue(record.get('value'));
                                }else{
                                    combo.setValue(conditionPanel.fieldParams.operand);
                                }
                            } , 
                            'select' : function(combo , record , index ){
                                condition.genCondition() ; 
                            }
                        }
                    },
                    {
                        xtype: 'displayfield',
                        value: '',
                        width: 15 
                    }, 
                    {
                        xtype: 'numberfield',
                        value: conditionPanel.fieldParams.value ,
                        width: 100 ,
                        listeners : {
                            'change' : function(textBox , newVal , oldVal){
                                condition.genCondition() ; 
                            }
                        }
                    }                
                ]
            });
        }
        if( myField != undefined )
            conditionPanel.add(myField) ; 
        
        if( conditionPanel.fieldParams.type ==  'function' ){
            var   line = new Ext.form.DisplayField({
                value : '<hr>'    , 
                hideLabel : true  
            })  ; 
            conditionPanel.add(line) ;
            
            //console.debug(dragAttr) ; 
            if( dragAttr.values == null ){
            	return conditionPanel ; 
            }
            
            for( i =0 ; i < dragAttr.values.length ; i++){
                propertyObj = dragAttr.values[i] ; 
                //console.debug(propertyObj) ; 
                //alert(propertyObj.Type) ; 
                if( propertyObj.Type == 'integer'){
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            }, 
                            {
                                xtype: 'numberfield', 
                                width: 100 , 
                                tempValue : dragAttr.values[i].value , 
                                defaultValue : propertyObj.Default ,
                                listeners : {
                                    'change' : function(textBox , newVal , oldVal){
                                        condition.genCondition() ; 
                                    },
                                    'render' : function(textBox){
                                        if(  textBox.tempValue != undefined){
                                            textBox.setValue(textBox.tempValue) ;
                                        }else{
                                            textBox.setValue(textBox.defaultValue) ;
                                        }
                                    } 
                                }
                            } 
                            
                        ] 
                    }) ;  
                    conditionPanel.add(myProperty) ;
                }else if( propertyObj.Type == 'string'){
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            }, 
                            {
                                xtype: 'textfield',
                                tempValue : dragAttr.values[i].value , 
                                defaultValue : propertyObj.Default ,
                                width: 100 , 
                                listeners : {
                                    'change' : function(textBox , newVal , oldVal){
                                        condition.genCondition() ; 
                                    } , 
                                    'render' : function(textBox){
                                        if(  textBox.tempValue != undefined){
                                            textBox.setValue(textBox.tempValue) ;
                                        }else{
                                            textBox.setValue(textBox.defaultValue) ;
                                        }
                                    } 
                                }
                            } 
                            
                        ] 
                    }) ;
                    conditionPanel.add(myProperty) ;
                }else if( propertyObj.Type == 'bool'){
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            }, 
                            {
                                xtype: 'checkbox',
                                tempValue : dragAttr.values[i].value , 
                                defaultValue : propertyObj.Default ,
                                width: 100 , 
                                listeners : {
                                    
                                    'check' : function( chk , val){
                                        condition.genCondition() ; 
                                    }, 
                                    'render' : function(checkBox){
                                        var temval = checkBox.tempValue ; 
                 
                                        if( temval == 'true'){
                                            temval = true ; 
                                        }else if( temval == 'false'){
                                            temval = false ; 
                                        }
                                         
                                        
                                        var temdefaultval = checkBox.defaultValue ; 
                                        if( temdefaultval == 'true' || temdefaultval == true){
                                            temdefaultval = true ; 
                                        }else{
                                            temdefaultval = false ; 
                                        } 
        
                                        if(  checkBox.tempValue != undefined){
                                            checkBox.setValue(temval) ;
                                        }else{
                                            checkBox.setValue(temdefaultval) ;
                                        }
                                    } 
                                }
                            } 
                            
                        ] 
                    }) ;
                    conditionPanel.add(myProperty) ;
                }
                else if( propertyObj.Type == 'date'){
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            },{
                            	xtype : 'datetimefield' , 
		                    	dateFormat: 'j M Y', 
						        timeFormat: 'g:i A', 
						        picker: {
						            timePicker: new Ext.ux.ExBaseTimePicker({ 
						                hourIncrement: 1,
						                minIncrement: 1
						            })
						        },
								tempValue : new Date(dragAttr.values[i].value) , 
                                defaultValue : new Date(propertyObj.Default) ,
		                        showToday : true  , 
		                        width: 160 ,
		                        listeners : {
                                    'select' : function(dataField , val){
                                        condition.genCondition() ; 
                                    }, 
                                    'render' : function(dateField){
                                        if(  dateField.tempValue != undefined){
                                        	if (! isNaN( dateField.tempValue.getTime() ) ) {
                                            	dateField.setValue(dateField.tempValue) ;
                                        	}
                                        }else{
                                        	if (! isNaN( dateField.defaultValue.getTime() ) ) {
                                            	dateField.setValue(dateField.defaultValue) ;
                                        	}
                                        }
                                    } 
                                }
							}/*, 
                            {
                            	
                                xtype: 'datefield',
                                format : 'd/m/Y' , 
                                tempValue : new Date(dragAttr.values[i].value) , 
                                defaultValue : new Date(propertyObj.Default) ,
                                width: 100 ,
                                listeners : {
                                    'select' : function(dataField , val){
                                        condition.genCondition() ; 
                                    }, 
                                    'render' : function(dateField){
                                        if(  dateField.tempValue != undefined){
                                            dateField.setValue(dateField.tempValue) ;
                                        }else{
                                            dateField.setValue(dateField.defaultValue) ;
                                        }
                                    } 
                                }
                            } */
                            
                        ] 
                    }) ;
                    conditionPanel.add(myProperty) ;
                }
                else if( propertyObj.Type == 'single'){
 
                    
                    var singleLocalStore = new Ext.data.JsonStore({
                        fields : ['label', 'value' ],
                        data   : dragAttr.values[i].values  
                    }) ; 
                     
                    
                    if( dragAttr.values[i].value == ''){
                        var record = singleLocalStore.getAt(0) ; 
                        var comboVal = '' ;
                        if(record != undefined)
                            comboVal = record.get('value') ;
                    }else{
                        var comboVal = dragAttr.values[i].value ;
                    } 
                    
                    var width = 100 ; 
                    if ( propertyObj.Label == '' ) 
                    	width = 150 ; 
                     
                    
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            } , 
                            {
                                xtype : 'combo' ,
                                mode:           'local',  
                                allowBlank: true  ,
                                triggerAction:  'all',
                                editable:       false, 
                                hideLabel  : true , 
                                displayField:   'label',
                                valueField:     'value',  
                                hiddenName : 'myID' , 
                                store:  singleLocalStore ,  
                                value : comboVal , 
                                width:width , 
                                listeners : {
                                      
                                    'select' : function(combo , record , index ){
                                        condition.genCondition() ; 
                                    }
                                }
                            } 
                            
                        ] 
                    }) ;
                    conditionPanel.add(myProperty) ;
                }else if( propertyObj.Type == 'single remote2'){
                    
                     
                    var singleLocalStore = new Ext.data.JsonStore({
                        fields : ['label', 'value' ],
                        proxy: new Ext.data.HttpProxy({
	                       url: dragAttr.values[i].values  
	                    }) 
                    }) ;  
                    var comboVal = '' ; 
                    if( dragAttr.values[i].value == ''){
                        var record = singleLocalStore.getAt(0) ; 
                        var record = singleLocalStore.getAt(0) ; 
                        if(record == undefined)
                        	comboVal = null ; 
                       	else
                        	comboVal = record.get('value') ;
                    }else{
                      	comboVal = dragAttr.values[i].value ;
                        //console.debug(conditionPanel.fieldParams.value2) ; 
                        if( dragAttr.values[i].value2 != undefined){
                            singleLocalStore.loadData(dragAttr.values[i].value2) ;
                        }
                        //singleLocalStore.loadData(dragAttr.values[i].value2) ;    
                    }  
                    //console.debug(dragAttr.values[i].values ) ;
                    
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            } , 
                            {
                                xtype : 'combo' ,
                                mode:           'local',  
                                allowBlank: false  ,
                                autoLoad : false , 
                                triggerAction:  'all',
                                editable:       true, 
                                queryParam: 'query' , 
                                hideLabel  : true , 
                                minChars : 2 , 
                                displayField:   'label',
                                valueField:     'value',  
                                hiddenName : 'myID' , 
                                store:  singleLocalStore ,  
                                //value : comboVal , 
                                comboVal : comboVal , 
                                width:100 ,  
                                listeners : {
                                    'change' : function(combo , newV , oldV){ 
                                    	combo.comboVal = newV ; 
                                        condition.genCondition() ; 
                                    } , 
                                    'afterrender' : function(combo){
                                    	combo.getStore().load();
										combo.getStore().on('load', function(){
											if( combo.comboVal == undefined)
												return ; 
											combo.setValue(combo.comboVal);
											condition.genCondition() ; 											
										});
                                    }
                                }
                            } 
                            
                        ] 
                    }) ;
                    conditionPanel.add(myProperty) ;
                      
                }else if( propertyObj.Type == 'single remote3'){
                    
                     
                    var singleLocalStore = new Ext.data.JsonStore({
                        fields : ['label', 'value' ],
                        proxy: new Ext.data.HttpProxy({
	                       url: dragAttr.values[i].values  
	                    }) 
                    }) ;  
                    var comboVal
                    if( dragAttr.values[i].value == ''){ 
                        var record = singleLocalStore.getAt(0) ; 
                        if(record == undefined)
                        	comboVal = null ; 
                       	else
                        	comboVal = record.get('value') ;
                    }else{
                        comboVal = dragAttr.values[i].value ;
    
                    }  
                    //console.debug(dragAttr.values[i].values ) ;
                    
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            } , 
                            {
                                xtype : 'combo' ,
                                mode:           'local',  
                                allowBlank: false  ,
                                autoLoad : false , 
                                triggerAction:  'all',
                                editable:       true, 
                                queryParam: 'query' , 
                                hideLabel  : true , 
                                minChars : 2 , 
                                displayField:   'label',
                                valueField:     'value',  
                                hiddenName : 'myID' , 
                                store:  singleLocalStore ,  
                                //value : comboVal , 
                                comboVal : comboVal , 
                                width:100 ,  
                                listeners : {
                                    'change' : function(combo , newV , oldV){ 
                                    	//Reload below data ; 
                                    	 
                                    	var parent = combo.findParentByType('ConditionFieldPanel' ) ; 
                                        var childs = parent.findByType('compositefield') ;  
                                        var cb  = childs[2].items.items[3] ; 
                                        cb.store.baseParams['field'] = newV ; 
                                        cb.store.load() ; 
                                        
                                        condition.genCondition() ; 
                                    } , 
                                    'afterrender' : function(combo){
                                    	combo.getStore().load();
										combo.getStore().on('load', function(){
											if( combo.comboVal == undefined)
												return ; 
											var oldV = combo.getValue();  
											combo.setValue(combo.comboVal);
											combo.fireEvent( 'change' , combo , combo.comboVal , '0') ;
											condition.genCondition() ; 											
										});
                                    }
                                }
                            } 
                            
                        ] 
                    }) ;
                    conditionPanel.add(myProperty) ;
                      
                }else if( propertyObj.Type == 'lookup'){ 
                    var singleLocalStore = null ;
                    if( condition.crossDomain  ){ 
                        singleLocalStore = new Ext.data.JsonStore({  
                            idProperty: 'value',
                            proxy: new Ext.data.ScriptTagProxy({
                                        url: dragAttr.values[i].values  
                                    }),
                            fields: ['label', 'value'] 
                        }) ;  
                    }else{
                        singleLocalStore = new Ext.data.JsonStore({
                            url:dragAttr.values[i].values  , 
                            idProperty: 'value',
                            fields: ['label', 'value'] 
                        }) ;  
                    }
                    /* 
                    var singleLocalStore = new Ext.data.JsonStore({
                        fields : ['label', 'value' ],
                        proxy: new Ext.data.HttpProxy({
	                       url: dragAttr.values[i].values  
	                    }) , 
                        listeners : {
                            'beforeload' : function(thisStore , options){ 
                            }
                        } 
                    }) ;  
                    */ 
                    if( dragAttr.values[i].value == ''){
                        var record = singleLocalStore.getAt(0) ; 
                        var comboVal = record.get('value') ;
                    }else{
                        var comboVal = dragAttr.values[i].value ;
                        //console.debug(conditionPanel.fieldParams.value2) ; 
                        if( dragAttr.values[i].value2 != undefined){
                            singleLocalStore.loadData(dragAttr.values[i].value2) ;
                        }
                        //singleLocalStore.loadData(dragAttr.values[i].value2) ;    
                    }  
                    //console.debug(dragAttr.values[i].values ) ;
                    
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            } , 
                            {
                                xtype : 'combo' ,
                                mode:           'remote',  
                                allowBlank: true  , 
                                minChars : 2 , 
                                triggerAction:  'all',
                                hideTrigger: true,     
                                selectOnFocus:true, 
                                editable:       true, 
                                hideLabel  : true , 
                                displayField:   'label',
                                valueField:     'value',  
                                hiddenName : 'myID' , 
                                store:  singleLocalStore ,  
                                value : comboVal , 
                                width:100 , 
                                listeners : {
                                      
                                    'change' : function(combo , newV , oldV){  
                                        condition.genCondition() ; 
                                    }  , 
                                    'beforequery' : function( queryEvent){
                                        var curCombo = queryEvent.combo ;
                                        curCombo.lastQuery = '' ; 
                                        //curCombo.store.removeAll()  ; 
                                        //console.debug(curCombo) ; 
                                        var parent = curCombo.findParentByType('ConditionFieldPanel' ) ; 
                                        var childs = parent.findByType('compositefield') ; 
                                        
                                        for( var i = 1 ; i < childs.length ; i++){ 
                                            curCombo.store.baseParams['panel[' + i + '][display]'] = childs[i].items.items[1].getValue() ; 
                                            curCombo.store.baseParams['panel[' + i + '][value]'] = childs[i].items.items[3].getValue() ; 
                                        }
                                    } 
                                }
                            } 
                            
                        ] 
                    }) ;
                    conditionPanel.add(myProperty) ;
                      
                }else if( propertyObj.Type == 'multi'){
                    var singleLocalStore = new Ext.data.JsonStore({
                        fields : ['label', 'value' ],
                        data   : dragAttr.values[i].values  
                    }) ;
                    
                    
                    
                    //console.debug(dragAttr.values[i]) ; 
                    
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            } , 
                            {
                                xtype: 'multiselect', 
                                width: 100,
                                height: 50,
                                displayField:   'label',
                                valueField:     'value',  
                                delimiter : '||' , 
                                allowBlank:false,
                                value : dragAttr.values[i].value , 
                                store: singleLocalStore ,
                                listeners : {
                                    'click' : function(combo  ){
                                        condition.genCondition() ; 
                                    }
                                }
                            } 
                            
                        ] 
                    }) ; 
                    conditionPanel.add(myProperty) ;
                }else if(propertyObj.Type ==  'superboxselect'){
                    //console.debug(propertyObj) ; 
                    //console.debug(dragAttr.values[i]) ; 
                    
                    
                    
                    var comboVal = null ; 
                    if( dragAttr.values[i].value == ''){ 
                        comboVal = null ;
                    }else{
                        comboVal = dragAttr.values[i].value ;
                    } 
                    
                    
         
                    var myProperty  = new Ext.form.CompositeField({ 
                        msgTarget : 'side', 
                        hideLabel : true , 
                        defaults: {
                            flex: 1
                        } , 
                        items : [
                            {
                            xtype: 'displayfield',
                            value: '',
                            width: 15 
                            } ,
                            {
                                xtype: 'displayfield',
                                value: propertyObj.Label 
                            } ,
                            {
                                xtype : 'hidden' , 
                                value : propertyObj.Name 
                            } ,{
                                xtype : 'textfield' , 
                                hidden : true , 
                                value : comboVal 
                            } ,{
                                xtype : 'button' , 
                                text : 'Select...' , 
                                objTitle : propertyObj.Label ,  
                                storeURL : dragAttr.values[i].values , 
                                objW : null ,   
                                width : 100 , 
                                handler : function(){
                                    
                                    
                                    var mode = 'local' ; 
                                    var singleLocalStore = '' ; 
                                    if(this.storeURL  == ''){
                                        singleLocalStore = new Ext.data.SimpleStore({
                                            fields : ['label', 'value' ],
                                            data: [] 
                                        }); 
                                    }else{
                                        
                                        if( condition.crossDomain  ){ 
                                            singleLocalStore = new Ext.data.JsonStore({  
                                                idProperty: 'value',
                                                proxy: new Ext.data.ScriptTagProxy({
                                                            url: this.storeURL
                                                        }),
                                                fields: ['label', 'value'] 
                                            }) ;  
                                        }else{ 
                                            singleLocalStore = new Ext.data.JsonStore({
                                                fields : ['label', 'value' ],
                                                proxy: new Ext.data.HttpProxy({
                        	                       url: this.storeURL
                        	                    }) 
                                            }) ;
                                        }
                                        mode = 'remote' ;  
                                        /*
                                        singleLocalStore = new Ext.data.JsonStore({
                                            fields : ['label', 'value' ],
                                            proxy: new Ext.data.HttpProxy({
                    	                       url: this.storeURL
                    	                    }) 
                                        }) ;
                                        */
                                    }
                                   // this.objRelate.value = this.findParentByType('compositefield').items.items[3].getValue() ; 
                                    //this.objRelate.store = this.objStore.cloneConfig() ; 
                                    //if( this.objW == null ){
                                        this.objW = new Ext.Window({
                                            width : 300 , 
                                            height : 250 , 
                                            title : this.objTitle ,   
                                            returnObj : this.findParentByType('compositefield').items.items[3] , 
                                            padding : 20 , 
                                            layout : 'fit' , 
                                            autoScroll : true , 
                                            items : [{ 
                                                xtype:'superboxselect', 
                                                allowBlank:true, 
                                                allowAddNewData: true, 
                                                valueDelimiter : '||' ,  
                                                emptyText: '',
                                                resizable: true, 
                                                border : false ,  
                                                autoScroll : true ,
                                                anchor:'100%',
                                                store: singleLocalStore, 
                                                mode: mode,
                                                displayField: 'label', 
                                                valueField: 'value', 
                                				queryDelay: 0,
                                				triggerAction: 'all',
                                				minChars: 1,  //this.findParentByType('compositefield').items.items[3].getValue()  , 
                                    			listeners : { 
                                    			    beforeadditem: function(bs,v){ 
                                                    },
                                                    additem: function(bs,v){
                                                    },
                                                    beforeremoveitem: function(bs,v){ 
                                                    },
                                                    removeitem: function(bs,v){ 
                                                    },
                                                    newitem: function(bs,v, f){   
                                                        var newV = v.split(this.valueDelimiter) ; 
                                                        for( var i = 0 ; i < newV.length ; i++){
                                                            var newObj = {
                                                                label: newV[i],
                                                                value: newV[i]
                                                            };
                                                            bs.addItem(newObj, true); 
                                                        }
                                                        
                                                    } 	
                                    		    }
                                            } ] , 
                                            buttons : [{
                                                text : 'OK' , 
                                                cls: 'monoloop-black-btn' , 
                                                handler : function(){ 
                                                    var w =  this.findParentByType('window') ;
                                                    var val = w.findByType('superboxselect')[0].getValue() ;
                                                    w.returnObj.setValue(val) ; 
                                                    w.close() ;  
                                                    condition.genCondition() ;   
                                                }
                                            }]
                                              
                                        }) ; 
                                    //} 
                                     this.objW.show() ;  
                                     var comboVal = this.findParentByType('compositefield').items.items[3].getValue() ; 
                                     if(comboVal != undefined){
                                        var temp_01 = comboVal.split('||') ; 
                                        for(var j = 0 ; j < temp_01.length ; j++){
                                            var v = temp_01[j] ;  
                                            //console.log(v);
                                            var newObj = {
                                                label: v,
                                                value: v
                                            };
                                            this.objW.findByType('superboxselect')[0].addItems(newObj) ; 
                                            
                                        }
                                    }
                                    
                                }
                            } /*, 
                            {
                                allowBlank:true, 
                                allowAddNewData: true, 
                                valueDelimiter : '||' ,  
                                xtype:'superboxselect', 
                                emptyText: '',
                                resizable: true, 
                                autoScroll : true ,
                                anchor:'100%',
                                store: singleLocalStore,
                                height : 60 ,
                                mode: mode,
                                displayField: 'label', 
                                valueField: 'value', 
                				queryDelay: 0,
                				triggerAction: 'all',
                				minChars: 1,
              
                    			listeners : { 
                    			     beforeadditem: function(bs,v){ 
                                    },
                                    additem: function(bs,v){
                                        condition.genCondition() ;   
                                    },
                                    beforeremoveitem: function(bs,v){ 
                                    },
                                    removeitem: function(bs,v){
                                        condition.genCondition() ; 
                                    },
                                    newitem: function(bs,v, f){  
                                        var newObj = {
                                            label: v,
                                            value: v
                                        };
                                        bs.addItem(newObj, true); 
                                    } 	
                    		    }
                            } */
                            
                        ] 
                    }) ;
                    
                    conditionPanel.add(myProperty) ;
                }
            } 
            //var value = dragAttr.node.attributes ; 
        } 
        return conditionPanel ; 
    } , 
    
    genAndOr : function(){
        var main = Ext.getCmp('main-panel') ; 
        //Ext.ux.ConditionFieldPanel
        //alert(main.getId()) ; 
        //
        //condition.processDisplayAndOr(main) ; 
        
        //console.debug( main.findByType('ConditionFieldPanel') ) ; 
        condition.genCondition(); 
        condition.setTitleAndOr() ; 
    } , 
 
    
    getIntegerOperator : function(){
        var selectBox = new Ext.form.ComboBox({
            mode:           'local',
            allowBlank: false  ,
            triggerAction:  'all',
            editable:       false, 
            hideLabel  : true , 
            displayField:   'name',
            valueField:     'value',  
            store:  condition.integerOperatorStore , 
            width:40  
        }) ; 
        
        return selectBox ; 
    } , 
    
    genCondition : function(){
        condition.removeDropZone() ;
        main = Ext.getCmp('main-panel') ;
        var conditionStr = condition.getCondition2(main) ; 
        if( Ext.util.Format.trim(conditionStr) == ''){
            conditionStr = '' ; 
        }else{
            conditionStr = ' if ( ' + conditionStr + ' ){|}' ; 
        }
        
        Ext.getCmp('condition-result').setValue(conditionStr) ;     
        
        if( condition.crossDomain ){ 
            if( ml_testbubble != undefined && condition.showSaveBubble == true ){
                if(Ext.getCmp('test_bubble_10') == undefined   )
                    ml_testbubble.open10(condition.windowObj) ;  
            }
        }
    } , 
    
    setTitleAndOr : function(){
        var mainPanel =  Ext.getCmp('main-panel');
        var allPanel = mainPanel.findByType('ConditionFieldPanel') ;   
        for( var i = 1 ; i < allPanel.length ; i++ ){
            if(allPanel[i-1].level < allPanel[i].level  ){
                allPanel[i].setTitle(' AND ' + allPanel[i].title ) ; 
            }else{
                allPanel[i].setTitle(' OR ' + allPanel[i].title ) ; 
            }
        }
    } , 
    
    getConditionTitle : function(){
    	var me = this ; 
    	var mainPanel =  Ext.getCmp('main-panel');
    	var title = '' ; 
    	var allPanel = mainPanel.findByType('ConditionFieldPanel') ;   
        for( var i = 0 ; i < allPanel.length ; i++ ){
            title += allPanel[i].title ;    
        }
        return title ; 
    } , 
    
    getPanelChild : function(parentPanel){
        var mainPanel =  Ext.getCmp('main-panel');
        var ret =new Array();
        var allPanel = mainPanel.findByType('ConditionFieldPanel') ;   
        var j = 0 ; 
        for( var i = 0 ; i < allPanel.length ; i++){
            if( allPanel[i].parentPanel.getId() == parentPanel.getId() ){
                ret[j] = allPanel[i] ; 
                j++ ; 
            }
        } 
        return ret ;
    } , 
    
    getCondition2 : function(panel){
        var panelSelected = condition.getPanelChild(panel) ; 
        if( panelSelected.length == 0)
            return '' ;
        
        var retStr = ''  ;
        
        for( var i = 0 ; i < panelSelected.length ; i++){
            var totalCount = panelSelected[i].items.getCount() ;  
            if( totalCount == 0 )
                continue ;
                
            var operand = ' ' ; 
            if( i > 0)
                operand = ' || ' ;     
            
            var subPanelSelected = condition.getPanelChild(panelSelected[i]) ; 
            
            if( totalCount > 1){
                if( subPanelSelected.length == 0 )
                    retStr += operand   + ' ( ' + condition.getFunctionScope(panelSelected[i]) + ' ) '   ;
                else
                    retStr += operand   + ' ( (' + condition.getFunctionScope(panelSelected[i]) + ') && ' + condition.getCondition2(panelSelected[i]) + ' ) ' ; 
            }else{
                if( subPanelSelected.length == 0 )
                    retStr += operand   + ' ( ' + condition.getSingleScope(panelSelected[i] , panelSelected[i].getComponent(0)) + ' ) ' ;
                else
                    retStr += operand   + ' ( (' + condition.getSingleScope(panelSelected[i] , panelSelected[i].getComponent(0)) + ') && ' + condition.getCondition2(panelSelected[i]) + ' ) ' ; 
                    
            } 
        }
        
        if ( panel.getId() != 'main-panel' ){
            if (panelSelected.length > 1   ){
                retStr = ' ( ' + retStr + ' ) ' ; 
            }
        }
        
         
        return retStr ; 
        var totalCount = panel.items.getCount() ;  
        if( totalCount == 0 )
            return ; 
            
        if ( panel.getComponent(0).getXType() != 'ConditionFieldPanel' ){
            //alert(panel.getComponent(0).monoloopType )  ;
            if( totalCount > 1){
                return ' ( ' + condition.getFunctionScope(panel ) + ' ) ' ;
            }else{
                return ' ( ' + condition.getSingleScope(panel , panel.getComponent(0)) + ' ) ' ;
            }
        }
        var resultStr = '' ; 
        for(var i = 0 ; i < totalCount ; i++){ 
            // This should not happen .
            //alert('i' + i + 'total' + totalCount) ; 
            if ( panel.getComponent(i).getXType() != 'ConditionFieldPanel' )
                continue ; 
                
            innerTotal = panel.getComponent(i).items.getCount() ;  
            if( innerTotal == 0)
                continue ; 
            
            var toolBar =   panel.getComponent(i).getTopToolbar() ; 
            
            var operand = '' ; 
  
            if( ( innerTotal == 1) || (panel.getComponent(i).getComponent(0).getXType() != 'ConditionFieldPanel' ))
                resultStr = resultStr + ' '+ operand + ' ' + condition.getCondition2(panel.getComponent(i)) ; 
            else
                resultStr = resultStr + ' '+ operand + ' ( ' + condition.getCondition2(panel.getComponent(i)) + ' ) ' ;    
        }
        
        return resultStr ; 
    } , 
    
    getSingleScope : function(parentPanel , panel){
    	// Change to JS Style
        var monoloopItem = panel.connector + '.' +  panel.monoloopField ; 
        
        var toolBar =   parentPanel.getTopToolbar() ;  
        var operand = '' ; 
  
        if( panel.monoloopType == 'bool'){

            parentPanel.setTitle(operand + ' ' + panel.items.get(1).getValue() + '   ' + panel.items.get(2).getValue() + '   ' + panel.items.get(3).getValue()) ;
   
        }else if(panel.monoloopType == 'date'){
            var val1 = panel.items.get(4).getValue() ; 
            if( val1 != ''){
                val1 = ISODateString(val1);
            }
            parentPanel.setTitle(operand + ' ' + panel.items.get(1).getValue() + '   ' + panel.items.get(2).getValue() + '   ' + val1 );
        }else if(panel.monoloopType == 'single' || panel.monoloopType == 'single remote'  || panel.monoloopType == 'single remote2'){
            parentPanel.setTitle(operand + ' ' + panel.items.get(1).getValue() + '   ' + panel.items.get(2).getValue() + '   ' + panel.items.get(4).getRawValue()) ;   
        }else{
            parentPanel.setTitle(operand + ' ' + panel.items.get(1).getValue() + '   ' + panel.items.get(2).getValue() + '   ' + panel.items.get(4).getValue()) ;
            //console.debug(panel) ;  
        }
        
        if( panel.monoloopType == 'integer'){
            return monoloopItem + ' ' + panel.items.get(2).getValue() + ' ' + panel.items.get(4).getValue() ; 
        }else if( ( panel.monoloopType == 'string' ) || ( panel.monoloopType == 'single' )  || ( panel.monoloopType == 'single remote' ) || ( panel.monoloopType == 'single remote2' ) ){
            var operand = panel.items.get(2).getValue() ; 
            if( operand == 'is' ){
 
                return monoloopItem + ' === ' + condition.addQoute(panel.items.get(4).getValue()) ; 
            }else if( operand == 'is not'  ){
                return monoloopItem + ' !== ' + condition.addQoute(panel.items.get(4).getValue()) ; 
            }else if( operand == 'contains'  ){
                return ' ml_contains(' + monoloopItem + ',' + condition.addQoute(panel.items.get(4).getValue()) + ')' ; 
            }else if( operand == 'does not contain'  ){
                return ' ml_dose_not_contains(' + monoloopItem + ',' + condition.addQoute(panel.items.get(4).getValue()) + ')' ; 
            }else if( operand == 'starts with'  ){
                return ' ml_starts_with(' + monoloopItem + ',' + condition.addQoute(panel.items.get(4).getValue()) + ')' ; 
            }else if( operand == 'ends with'  ){
                return ' ml_ends_with(' + monoloopItem + ',' + condition.addQoute(panel.items.get(4).getValue()) + ')' ; 
            }
        }else if(panel.monoloopType == 'date'){
            var operand = panel.items.get(2).getValue()  ; 
            var val1 = panel.items.get(4).getValue() ; 
            if( val1 != ''){
            	val1 = ISODateString(val1);
            }
            var value = condition.addQoute(val1) ; 
            if( operand == 'comes on' ){
                return ' ml_comes_on(' + monoloopItem + ',' + value + ')' ; 
            }else if( operand == 'different from' ){
                return ' ml_different_from(' + monoloopItem + ',' + value + ')' ; 
            }else if( operand == 'comes after' ){
                return ' ml_comes_after(' + monoloopItem + ',' + value + ')' ; 
            }else if( operand == 'comes before' ){
                return ' ml_comes_before(' + monoloopItem + ',' + value + ')' ; 
            }else if( operand == 'advanced' ){
                return ' ml_advanced(' + monoloopItem + ',' + value + ')' ; 
            }
        } else if(panel.monoloopType == 'multi'){
            var operand = panel.items.get(2).getValue()  ; 
            if( operand == 'contain exactly' ){
                return ' ml_contain_exactly(' + monoloopItem + ',' + condition.addQoute(panel.items.get(4).getValue()) + ')' ; 
            }else if( operand == 'contain all' ){
                return ' ml_contain_all(' + monoloopItem + ',' + condition.addQoute(panel.items.get(4).getValue()) + ')' ; 
            }else if( operand == 'contain some' ){
                return ' ml_contain_some(' + monoloopItem + ',' + condition.addQoute(panel.items.get(4).getValue()) + ')' ; 
            }else if( operand == 'not contain' ){
                return ' ml_not_contain(' + monoloopItem + ',' + condition.addQoute(panel.items.get(4).getValue()) + ')' ; 
            }
            
        }else if(panel.monoloopType == 'bool'){
        	if( panel.items.get(3).getValue() === true )
            	return monoloopItem + ' === ' +  panel.items.get(3).getValue() ; 
        	else
        		return monoloopItem + ' !=  true '  ; 
        }
        
        
    } , 
    
    addQoute : function(str){
        return '\'' + str + '\'' ; 
    } ,
    
    getFunctionScope : function(panel){
    	// Change to JS style
        var mainField = panel.getComponent(0) ; 
        var monoloopItem = mainField.connector + '.' +  mainField.monoloopField + '(' ; 
        
        var toolBar =   panel.getTopToolbar() ;  
        var operand = '' ; 
        /*
        if( toolBar.isVisible() ) 
            operand = toolBar.getComponent(0).getValue() ;
        */
        var title = '[ ' ; 
        
        var totalCount = panel.items.getCount() ;  
        for ( var i = 2 ; i < totalCount ; i++ ){
            var curField = panel.getComponent(i) ; 
            //console.debug(curField) ; 
            if( i == 2){
                if( curField.items.get(3).isXType('checkbox'))
                    monoloopItem +=  curField.items.get(3).getValue()  ;
                else if( curField.items.get(3).isXType('datefield') || curField.items.get(3).isXType('datetimefield') ){
                    monoloopItem +=  condition.addQoute( ISODateString(curField.items.get(3).getValue()))  ;
                }
                else
                    monoloopItem +=  condition.addQoute( curField.items.get(3).getValue() ) ;
                    
                if( curField.items.get(3).isXType('combo') && ! curField.items.get(3).isXType('superboxselect') ){
                    title += ' ' + curField.items.get(1).getValue() + ' is ' + curField.items.get(3).getRawValue() ; 
                }
                else if( curField.items.get(3).isXType('datefield') || curField.items.get(3).isXType('datetimefield')){
                	title += ' ' + curField.items.get(1).getValue() + ' is ' + ISODateString(curField.items.get(3).getValue()) ; 
                }
				else{
                    title += ' ' + curField.items.get(1).getValue() + ' is ' + curField.items.get(3).getValue() ; 
                }
                
            }
            else { 
                if( curField.items.get(3).isXType('checkbox'))
                    monoloopItem += ',' +  curField.items.get(3).getValue()  ;
                else if( curField.items.get(3).isXType('datefield') || curField.items.get(3).isXType('datetimefield')){
                    monoloopItem +=  ',' + condition.addQoute( ISODateString(curField.items.get(3).getValue()))  ;
                }else{
                    monoloopItem += ',' + condition.addQoute(  curField.items.get(3).getValue() ); 
                }
                if( curField.items.get(3).isXType('combo') && ! curField.items.get(3).isXType('superboxselect') ){
                    title += ', ' + curField.items.get(1).getValue() + ' is ' + curField.items.get(3).getRawValue() ; 
                }
				else if( curField.items.get(3).isXType('datefield') || curField.items.get(3).isXType('datetimefield')){
                	title += ', ' + curField.items.get(1).getValue() + ' is ' + ISODateString(curField.items.get(3).getValue()) ; 
                }else{
                    title += ', ' + curField.items.get(1).getValue() + ' is ' + curField.items.get(3).getValue() ;
                }
            }
        }
        
        title += ' ]' ;
        //return monoloopItem += ') == ' + mainField.items.get(3).getValue()   ; 
        //alert(mainField.monoloopReturnValue) ;
        if( mainField.monoloopReturnValue == 'integer' || mainField.monoloopReturnValue == 'date' || mainField.monoloopReturnValue == 'string'){
            title =  operand + ' ' + mainField.items.get(1).getValue() + ' ' + mainField.items.get(2).getValue() + ' ' + mainField.items.get(4).getValue()  + title  ;
        }else{
            title =  operand + ' ' +  mainField.items.get(1).getValue()+ ' ' + mainField.items.get(2).getValue() + ' ' + mainField.items.get(3).getValue() + title   ;
        }
        
        panel.setTitle(title) ; 
         
        if(mainField.monoloopReturnValue == 'integer' ){
            return monoloopItem += ') ' + mainField.items.get(2).getValue() + ' ' + mainField.items.get(4).getValue()   ; 
        }else if(mainField.monoloopReturnValue == 'date' ){
            var operand = mainField.items.get(2).getValue()  ; 
            var val1 = mainField.items.get(4).getValue() ; 
            if( val1 != ''){
                val1 = ISODateString(val1);
            }
            var value = condition.addQoute(val1) ; 
            if( operand == 'comes on' ){
                return ' ml_comes_on(' + monoloopItem + ') ,' + value + ')' ; 
            }else if( operand == 'different from' ){
                return ' ml_different_from(' + monoloopItem + ') ,' + value + ')' ; 
            }else if( operand == 'comes after' ){
                return ' ml_comes_after(' + monoloopItem + ') ,' + value + ')' ; 
            }else if( operand == 'comes before' ){
                return ' ml_comes_before(' + monoloopItem + ') ,' + value + ')' ; 
            }else if( operand == 'advanced' ){
                return ' ml_advanced(' + monoloopItem + ') ,' + value + ')' ; 
            }
        }else if(mainField.monoloopReturnValue == 'string' ){
            var operand = mainField.items.get(2).getValue()  ; 
            var value = condition.addQoute(mainField.items.get(4).getValue()) ; 
            if( operand == 'is' ){
                return monoloopItem + ') === ' + value  ; 
            }else if( operand == 'is not'  ){
                return monoloopItem + ') !== ' + value ; 
            }else if( operand == 'contains'  ){
                return ' ml_contains(' + monoloopItem + ') ,' + value + ')' ; 
            }else if( operand == 'does not contain'  ){
                return ' ml_dose_not_contains(' + monoloopItem + ') ,' + value + ')' ; 
            }else if( operand == 'starts with'  ){
                return ' ml_starts_with(' + monoloopItem + ') ,' + value + ')' ; 
            }else if( operand == 'ends with'  ){
                return ' ml_ends_with(' + monoloopItem + ') ,' +value + ')' ; 
            }
        }else{
            return monoloopItem += ') == ' + mainField.items.get(3).getValue()   ; 
        }
    } , 
 
    initStore : function(){
        
        condition.integerOperatorStore = new Ext.data.JsonStore({
            fields : ['name', 'value' ],
            data   : [
                {name : '==',   value: '==' },
                {name : '!=',   value: '!=' },
                {name : '<',   value: '<' },
                {name : '>',   value: '>' },
                {name : '<=',   value: '<=' }, 
                {name : '>=',   value: '>=' }   
            ]
        }) ; 
        
        condition.stringOperatorStore = new Ext.data.JsonStore({
            fields : ['name', 'value' ],
            data   : [
                {name : 'is', value: 'is' },
                {name : 'is not' , value: 'is not' },
                {name : 'contains' , value: 'contains' },
                {name : 'does not contain' ,   value: 'does not contain' },
                {name : 'starts with',   value: 'starts with' }, 
                {name : 'ends with',   value: 'ends with' }  
            ]
        }) ;
        
        condition.dateOperatorStore = new Ext.data.JsonStore({
            fields : ['name', 'value' ],
            data   : [
                {name : 'comes on', value: 'comes on' },
                {name : 'different from' , value: 'different from' },
                {name : 'comes after' , value: 'comes after' },
                {name : 'comes before' ,   value: 'comes before' },
                {name : 'advanced',   value: 'advanced' }    
            ]
        }) ;
        
        condition.multiOperatorStore  = new Ext.data.JsonStore({
            fields : ['name', 'value' ],
            data   : [
                {name : 'contain exactly', value  : 'contain exactly' },
                {name : 'contain all'    , value  : 'contain all' },
                {name : 'contain some'   , value  : 'contain some' },
                {name : 'not contain'    ,   value: 'not contain' } 
            ]
        }) ;
        
        //andorOperatorStore
        
        condition.andorOperatorStore  = new Ext.data.JsonStore({
            fields : ['name', 'value' ],
            data   : [
                {name : 'AND', value  : 'AND' },
                {name : 'OR'    , value  : 'OR' }  
            ]
        }) ;
    } ,
    
    renderConnector: function(){ 
        var data = {} ; 
        data['condition'] = condition.preconditionStr ; 
        if( condition.crossDomain){
            data['eID'] = 't3p_conditional_content' ; 
            data['cmd'] = 'getConnectorGroupList' ; 
            data['pid'] = 1 ;  
            data['time'] = new Date().getTime(); 
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data, 
                callback: function(jsonData) {  
                    condition.getConnectorGroupList_sucess(jsonData) ; 
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=getConnectorGroupList'  , 
    			// send additional parameters to instruct server script
    			method: 'POST',
    			timeout: '30000',
                params:   data ,
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
            		var retData = Ext.util.JSON.decode(xhr.responseText) ;
                    condition.getConnectorGroupList_sucess(retData) ; 
            	 
    			},
    			failure: function() {
                    Ext.MessageBox.hide();
    			    Ext.MessageBox.alert('Error', '#401' ) ; 
    			}
    		});
        }
        
	} , 
    
    getConnectorGroupList_sucess : function(retData){
        condition.precondition = retData.conditional ; 
        //console.debug(condition.precondition ) ; 
        var groupPanel = new Ext.Panel({
	        region: 'center',
			title: '', 
	        border: false,
            layout : 'fit' , 
			margins:'0',
	        cmargins:'0',
            height : 538 ,   
            layout: 'accordion' ,
            items: [] 
	    }); 
        
        for (var group in retData.groupList) {
            var groupItemPanel = new Ext.Panel({ 
                title : group , 
                layout : 'fit' ,  
                listeners :{
                    'expand' : function(panel){
                        //groupItemPanel.doLayout() ; 
                    }
                } , 
                
                autoScroll  : false 
            }) ; 
            //console.debug( retData.groupList[group]  ) ; 
            var myTree = new Ext.tree.TreePanel({
		        useArrows: true,
		        autoScroll: true,
		        animate: true,
		        enableDD: true , 
                enableSort : false,
                ddAppendOnly : true , 
                id : 'connector-tree-'+group , 
                ddGroup     : 'dropGroup2',
		        containerScroll: true,
		        border: false, 
		        
                loader: new Ext.tree.TreeLoader(),
                root: new Ext.tree.AsyncTreeNode({
                    expanded: true,
                    children: retData.groupList[group] 
                }),
                rootVisible: false , 
                listeners: {
                    startdrag : function(tree , node , e){
                        condition.removeBackGroundLayout(); 
                        condition.globalLock = false ; 
                        var mainPanel =  Ext.getCmp('main-panel');
                        var innerTotal = mainPanel.items.getCount() ; 
                        if( innerTotal == 1  ){
                            return ; 
                        }
                            
                        if( mainPanel.getComponent(1).getXType() == 'ConditionFieldPanel'){
                            condition.insertDropZone1(mainPanel , false) ; 
                            //Ext.dd.ScrollManager.register(mainPanel);
                            
                        }  
                        //condition.renderBackGroudLayout() ; 
                    } , 
                    
                    enddrag : function(tree , node , e){
                        // Remove drop zone
                        condition.removeDropZone() ; 
                        var mainPanel =  Ext.getCmp('main-panel');
                        mainPanel.doLayout() ;
                        condition.genAndOr() ;  
                        
                        condition.renderBackGroudLayout() ; 
                    }
                    
		        }
                
		    });
            
            groupItemPanel.add(myTree) ;

            groupPanel.add(groupItemPanel) ; 
        }
        
        
        
        condition.westPanel.add(groupPanel) ; 
        
        condition.westPanel.doLayout() ; 
        
        condition.reengineerCondition() ; 
        
        condition.renderBackGroudLayout() ;  
        
        groupPanel.items.items[2].expand() ;   
        
        return ; 
    }  , 
    
    removeDropZone : function(){
        var mainPanel =  Ext.getCmp('main-panel');
                                
        var DropPanalArray = mainPanel.findByType('DropZonePanel') ;
        //console.debug(DropPanalArray) ; 
        for( var i = 0 ; i < DropPanalArray.length ; i++){
            DropPanalArray[i].destroy() ; 
        }    
    } , 
    
    hideAllDropZone : function(){
        var mainPanel =  Ext.getCmp('main-panel');
                                
        var DropPanalArray = mainPanel.findByType('DropZonePanel') ;
        //console.debug(DropPanalArray) ; 
        for( var i = 0 ; i < DropPanalArray.length ; i++){
            DropPanalArray[i].hide() ; 
        } 
    } , 
    
    showAllDropZone : function(){
        var mainPanel =  Ext.getCmp('main-panel');
                                
        var DropPanalArray = mainPanel.findByType('DropZonePanel') ;
        //console.debug(DropPanalArray) ; 
        for( var i = 0 ; i < DropPanalArray.length ; i++){
            DropPanalArray[i].show() ; 
        } 
    } , 
    
    assignAllDropZone : function(){
       var mainPanel =  Ext.getCmp('main-panel');
                                
        var DropPanalArray = mainPanel.findByType('DropZonePanel') ;
        //console.debug(DropPanalArray) ; 
        for( var i = 0 ; i < DropPanalArray.length ; i++){
           condition.assignDropZone( DropPanalArray[i] ) ;  
        } 
         
        
        
    } , 
    
    insertDropZone1 : function(panel , hidden){
        //console.debug(panel) ; 
        // N.V3 New layout of dropzone ; 
        var mainPanel =  Ext.getCmp('main-panel');
        var allPanel = mainPanel.findByType('ConditionFieldPanel') ;
        
        for( var i = 0 ; i < allPanel.length ; i++){
           condition.insertDropZone1_or( allPanel[i]  , hidden) ;  
           condition.insertDropZone1_and( allPanel[i] , hidden ) ;  
        } 
        mainPanel.doLayout() ; 
        /*
        var innerTotal = panel.items.getCount() ; 
        
        if( innerTotal == 0)
            return ; 
            
        if( panel.getComponent(0).getXType() != 'ConditionFieldPanel' ){
            condition.insertDropZone2(panel , hidden ) ; 
            return ;
        }
        
        for( var i = 0 ; i < innerTotal ; i++){
            if(panel.getComponent(i).getXType() == 'ConditionFieldPanel')
                condition.insertDropZone1(panel.getComponent(i)) ;
        }
        
        panel.insert(0 , condition.getDropPanel(panel , 0 , hidden)) ; 
 
        for( var i = 1 ; i < innerTotal  ; i++){
            panel.insert(i * 2 , condition.getDropPanel(panel , 0 , hidden)) ; 
        }
        
        panel.add( condition.getDropPanel(panel , 0 , hidden)) ; 
        panel.doLayout() ; 
        */
    } , 
    
    insertDropZone1_or : function(panel , hidden){
        var parentPanel = panel.parentPanel ; 
        var mainPanel = Ext.getCmp('main-panel') ; 
        //Get item index ; 
        var insertIndex = condition.getIndexofComponent(panel) ; 
        var dropzone = condition.getDropPanel(parentPanel , 0 , hidden , 'OR')
        if( parentPanel.getId() == 'main-panel' ){
            dropzone.setWidth( (100  ) + '%' ) ; 
        }else{
            dropzone.setWidth( (100 - ( parentPanel.level  ) * 5) + '%' ) ; 
        }
        
        
        
        mainPanel.insert(insertIndex + 1 , dropzone)
    } , 
    
    insertDropZone1_and : function(panel , hidden){
        var mainPanel = Ext.getCmp('main-panel') ; 
        //Get item index ; 
        var insertIndex = condition.getIndexofComponent(panel) ; 
        //console.debug(insertIndex) ; 
        var dropzone = condition.getDropPanel(panel , 1 , hidden , 'AND')
        dropzone.setWidth( (100 - ( panel.level  )  * 5) + '%' ) ; 
        
        dropzone.addClass('dropzone-panel2') ; 
        
        mainPanel.insert(insertIndex + 2 , dropzone)
    } , 
    
    getIndexofComponent : function(panel){
        var mainPanel = Ext.getCmp('main-panel') ; 
        var innerTotal = mainPanel.items.getCount() ; 
        var insertIndex = 0 ; 
        for( var i = 0 ; i < innerTotal ; i++){
            //console.debug(parentPanel.getComponent(i).getId() + ' : ' + panel.getId() ) ; 
            if( mainPanel.getComponent(i).getId() == panel.getId() ){
                insertIndex = i ; 
            }
        }
        return insertIndex ; 
    } , 
    
    insertDropZone3 : function(panel , dragPanel , hidden){
 
        var allPanel = panel.findByType('ConditionFieldPanel') ;
        
        for( var i = 0 ; i < allPanel.length ; i++){
           if( allPanel[i].getId() != dragPanel.getId()){
                condition.insertDropZone1_or( allPanel[i]  , hidden) ;  
                condition.insertDropZone1_and( allPanel[i] , hidden ) ;  
           }
           
        } 
        panel.doLayout() ; 
         
    } , 
    
    insertDropZone2 : function(panel , hidden){
        var innerTotal = panel.items.getCount() ; 
        
        if( innerTotal == 0)
            return ; 
 
        panel.add( condition.getDropPanel(panel , 1 , hidden)) ; 
 
        panel.doLayout() ; 
    } , 
    
    getDropPanel : function(parentPanel , myType , hidden , dropLabel){
        var dropPanel = new Ext.ux.DropZonePanel({
            html : '<p align="center">'+dropLabel+'</p>' , 
            myType : myType , 
            hidden : hidden , 
            parentPanel : parentPanel , 
            listeners : {
                'render' : function(dropPanel2){ 
                    if( hidden == true)
                        return  ;
                    condition.assignDropZone(dropPanel2 ) ;  
                } , 
                'beforedestroy' : function(dropPanel2){    
                    if(dropPanel2.dd){
                        dropPanel2.dd.unreg();
                    }  
                }
            }
        }) ;
        
        
        
        return dropPanel ; 
    } , 
    
    assignDropZone : function(panel ){
        if( panel.body == undefined)
            return ; 
        var formPanelDropTargetEl =  panel.body.dom;
        panel.dd = new Ext.dd.DropTarget(formPanelDropTargetEl, {
            ddGroup     : 'dropGroup2',
            
            notifyEnter : function(ddSource, e, data) {
              //Add some flare to invite drop.
              //alert(panel.myType) ; 
              panel.body.stopFx();
              panel.body.highlight();
              panel.expand() ;  
              condition.globalLock = false ; 
            },
            notifyDrop  : function(ddSource, e, data){
                // Reference the record (single selection) for read 
                if(condition.globalLock == true)
                return false ;  
                condition.globalLock = true ;  
                
                
                if( data.panel != undefined){
 
                    condition.addConditionBuilder3(panel , panel.parentPanel , data , ddSource) ; 
                    //alert('Sorry drag drop from panel not support yet') ; 
                    return true ;
                }
                condition.addConditionBuilder2(panel , panel.parentPanel , data  ) ; 
                 
                
                return true;
            }
        });
        //panel.dd.maintainOffset = true ; 
    } , 
    
    addConditionBuilder2 : function(dropPanel , parentPanel , data){
        var mainPanel = Ext.getCmp('main-panel') ; 
        var innerTotal = mainPanel.items.getCount() ; 
        //alert(innerTotal) ;
        var dropIndex = -1 ; 
        for( var i = 0 ; i < innerTotal  ; i++){
            if( mainPanel.getComponent(i).getId() == dropPanel.getId()){
                dropIndex = i ; 
            }
        }
        
        if( dropIndex == -1)
            return ; 
 
            
        if( dropPanel.myType == 0){
            // Drop on OR
            var conditionPanel = condition.createConditionPanel('' ,data.node.attributes) ; 
            conditionPanel.parentPanel = parentPanel ; 
            if( parentPanel.getId() == 'main-panel')
                conditionPanel.level = 1 ;
            else{
                conditionPanel.level = parentPanel.level + 1 ;  
                conditionPanel.setWidth( (100 - 5 * conditionPanel.level) + '%') ; 
            }
            
            var beforeIndex = dropIndex - 1  ;
            var beforePanel = mainPanel.getComponent(beforeIndex) ; 
            var panelChilds = condition.getPanelChild(beforePanel) ; 
            
            for( var i = 0 ; i < panelChilds.length ; i++ ){
                panelChilds[i].parentPanel = conditionPanel ; 
            }

            mainPanel.insert(dropIndex , conditionPanel) ; 
             
        
        }else{
            // Drop ON AND
            // V3 // No drop inside ; 
            var conditionPanel = condition.createConditionPanel('' ,data.node.attributes) ; 
            conditionPanel.parentPanel = parentPanel ; 
            conditionPanel.level = parentPanel.level + 1 ;  
            conditionPanel.setWidth( (100 - 5 * conditionPanel.level) + '%') ; 
            mainPanel.insert(dropIndex , conditionPanel) ; 
            /*
            if( parentPanel.getComponent(0).getXType() == 'compositefield'){
                // Check for really condition inside . 
                var conditionPanel = condition.createConditionPanel('' ,data.node.attributes) ; 
                data = parentPanel.removeAll(false) ;
                var blankPanel =  condition.createBlankPanel() ;  
                blankPanel.add(data) ; 
                parentPanel.add(blankPanel) ; 
                parentPanel.add(conditionPanel) ; 
                //condition.removeDropZone() ;
            }
            */
            
        }
 
    } , 
    
    addConditionBuilder3 : function(dropPanel , parentPanel , data , ddSource){
        var mainPanel = Ext.getCmp('main-panel') ; 
        var innerTotal = mainPanel.items.getCount() ; 
          
        var dropIndex = -1 ; 
        var upperPanel = null ; 
        for( var i = 0 ; i < innerTotal  ; i++){
            if( mainPanel.getComponent(i).isXType('ConditionFieldPanel')){
                upperPanel = mainPanel.getComponent(i) ; 
            }
            
            if( mainPanel.getComponent(i).getId() == dropPanel.getId()){
                dropIndex = i ; 
                break  ;
            }
        }
        
        if( dropIndex == -1){
            return ; 
        }
        
        // Get nearby panel ; 
 
        
            
 
        // V3 . Change myType Value    
        if( dropPanel.myType == 0){
            // Drop on OR
            /*
            ddSource.panel.el.dom.parentNode.removeChild(ddSource.panel.el.dom) ; 
            parentPanel.insert(dropIndex , ddSource.panel) ;  
            parentPanel.doLayout() ; 
            */
            condition.chageChildPanelToUpper(ddSource.panel) ; 
            
            ddSource.panel.el.dom.parentNode.removeChild(ddSource.panel.el.dom) ; 
            ddSource.panel.parentPanel = upperPanel.parentPanel ; 
            ddSource.panel.level = upperPanel.level ;  
            ddSource.panel.setWidth( (100 - 5 * ddSource.panel.level) + '%') ;
            
            // Assign child from upper panel  ;
            var panelChilds = condition.getPanelChild(upperPanel) ; 
            for( var i = 0 ; i < panelChilds.length ; i++ ){
                panelChilds[i].parentPanel = ddSource.panel ; 
            }
  
            mainPanel.insert(dropIndex , ddSource.panel) ; 
            mainPanel.doLayout() ; 
            
            
             
        }else{
            condition.chageChildPanelToUpper(ddSource.panel) ; 
            ddSource.panel.el.dom.parentNode.removeChild(ddSource.panel.el.dom) ; 
            ddSource.panel.parentPanel = upperPanel  ; 
            ddSource.panel.level = upperPanel.level + 1 ;  
            ddSource.panel.setWidth( (100 - 5 * ddSource.panel.level) + '%') ;
            mainPanel.insert(dropIndex , ddSource.panel) ; 
            mainPanel.doLayout() ; 
             
        }
        //alert('xxx') ; 
        //condition.genAndOr() ; 
    } , 
    
    chageChildPanelToUpper : function(panel){
        var parentPanel = panel.parentPanel ; 
        
        var panelChilds = condition.getPanelChild(panel) ; 
        
        for( var i = 0 ; i < panelChilds.length ; i++ ){
            panelChilds[i].parentPanel = parentPanel ; 
        }
        
        condition.adjustLevel(parentPanel) ; 
    } , 
    
    adjustLevel : function(panel){
        var baseLevel = 0 ; 
        if( panel.getId() != 'main-panel'){
            baseLevel = panel.level ; 
        }
        
        var panelChilds = condition.getPanelChild(panel) ; 
        for( var i = 0 ; i < panelChilds.length ; i++ ){
            panelChilds[i].level = baseLevel + 1  ;
            panelChilds[i].setWidth( (100 - 5 * panelChilds[i].level) + '%') ; 
            condition.adjustLevel(panelChilds[i]) ;
        }
    } , 
    
    reengineerCondition : function(){
        //alert(condition.precondition) ;
        //console.debug(condition.precondition) ;  
        var mainPanel = Ext.getCmp('main-panel') ; 
        //console.debug(condition.precondition) ; 
        condition.reengineetCondition2(mainPanel , condition.precondition) ;
        condition.genAndOr() ; 
        mainPanel.doLayout() ; 
        // Process Hide Panel ; 
        var conditionPanelArray = mainPanel.findByType('ConditionFieldPanel') ;
        //console.debug(DropPanalArray) ; 
        for( var i = 0 ; i < conditionPanelArray.length ; i++){
           // N V.3 we hide alll condition . 
           conditionPanelArray[i].collapse( false) ; 
        }
        
        mainPanel.doLayout() ; 
 
    } , 
    
    reengineetCondition2 : function(panel , conditionObj){
        var blank = {} ; 
        blank['type'] = 'blank' ; 
        
        //console.debug(conditionObj) ; 
        var mainPanel = Ext.getCmp('main-panel') ; 
 
 
        for( var i = 0 ; i < conditionObj.length ; i++){
            // N. V3 .
            var newPanel = null ; 
            if(conditionObj[i].items.length > 0 ){
                //conditionObj[i].fields['join'] = conditionObj[i].items[0].join ; 
                if( conditionObj[i].join == 'AND' ){
                    
                    if( conditionObj[i].items[0].join == '' &&  conditionObj[i].items[0].items.length >  0){
                        var newPanel = condition.createConditionPanel( '' , conditionObj[i].items[0].items[0].fields ) ;
                        var innerTotal = mainPanel.items.getCount() ; 
                        var lastPanel = mainPanel.getComponent(innerTotal - 1 ) ;
                        
                         newPanel.level = lastPanel.level + 1 ; 
                         newPanel.parentPanel = lastPanel ; 
                         newPanel.setWidth((100 - 5 * newPanel.level) + '%') ; 
                         mainPanel.add(newPanel) ; 
                         mainPanel.doLayout() ;
                         
                         conditionObj[i].items[0].items.shift() ; 
                         
                         condition.reengineetCondition2(newPanel , conditionObj[i].items) ;  
                    }else{
                        var newPanel = condition.createConditionPanel( '' , conditionObj[i].items[0].fields ) ;
                        var innerTotal = mainPanel.items.getCount() ; 
                        var lastPanel = mainPanel.getComponent(innerTotal - 1 ) ;
                        
                         newPanel.level = lastPanel.level + 1 ; 
                         newPanel.parentPanel = lastPanel ; 
                         newPanel.setWidth((100 - 5 * newPanel.level) + '%') ; 
                         mainPanel.add(newPanel) ; 
                         mainPanel.doLayout() ;
                         
                         conditionObj[i].items.shift() ; 
                         
                         condition.reengineetCondition2(newPanel , conditionObj[i].items) ; 
                    } 
                }else{ 
                    //console.debug(conditionObj[i].items);  
                    condition.reengineetCondition2(panel , conditionObj[i].items) ; 
                } 
                 
                mainPanel.doLayout() ;
            }else{
                conditionObj[i].fields['join'] = conditionObj[i].join ; 
                if( conditionObj[i].join == 'AND' ){
                    var newPanel = condition.createConditionPanel( '' , conditionObj[i].fields ) ;
                    var innerTotal = mainPanel.items.getCount() ; 
                    var lastPanel = mainPanel.getComponent(innerTotal - 1 ) ;
                    
                     newPanel.level = lastPanel.level + 1 ; 
                     newPanel.parentPanel = lastPanel ; 
                     newPanel.setWidth((100 - 5 * newPanel.level) + '%') ; 
                     mainPanel.add(newPanel) ; 
                     mainPanel.doLayout() ;
                }else{
                    //console.debug(conditionObj[i].fields) ; 
                    var newPanel = condition.createConditionPanel( '' , conditionObj[i].fields ) ;
                    if( panel.getId() == 'main-panel'){
                        newPanel.level = 1; 
                        newPanel.parentPanel = panel ; 
                    }else{
                        if( conditionObj[i].fields['join'] == 'OR' ){
                            newPanel.level = panel.level ; 
                            newPanel.parentPanel = panel.parentPanel ; 
                        }else{
                            newPanel.level = panel.level + 1 ; 
                            newPanel.parentPanel = panel ; 
                        }
                        
                    }
                    newPanel.setWidth((100 - 5 * newPanel.level) + '%') ; 
                    
                    mainPanel.add(newPanel) ; 
                    mainPanel.doLayout() ;
                }
                
            }
 
        }
    } , 
    
    reengineetCondition3 : function(panel , conditionObj){
        var mainPanel = Ext.getCmp('main-panel') ; 
        var newPanel = condition.createConditionPanel( '' , conditionObj.fields ) ;
        newPanel.level = panel.level + 1 ; 
        newPanel.setWidth((100 - 5 * newPanel.level) + '%') ; 
        newPanel.parentPanel = panel ; 
        mainPanel.add(newPanel) ; 
        mainPanel.doLayout() ;
    } , 
    
    removeBackGroundLayout : function(){
        if( document.getElementById('cbuilder-layout') != null){ 
            document.getElementById('cbuilder-layout').innerHTML  = '' ;
        } 
        // Remove >1 items gaps ; 
        var mainPanel = Ext.getCmp('main-panel') ; 
        var conditionPanelArray = mainPanel.findByType('ConditionFieldPanel') ;
        if( conditionPanelArray.length == 0 )
            return ;
            
        for( var i = 0 ; i < conditionPanelArray.length ; i++){
            conditionPanelArray[i].removeClass('ConditionFieldPanel') ; 
            conditionPanelArray[i].removeClass('ConditionFieldPanel2') ; 
            if( i == 0 ){
                conditionPanelArray[i].addClass('ConditionFieldPanel') ; 
            }else{
                if( conditionPanelArray[i-1].level > conditionPanelArray[i].level)
                    conditionPanelArray[i].addClass('ConditionFieldPanel2') ; 
            } 
        }
    } , 
    
    renderBackGroudLayout : function(){
        //Ext.get('cbuilder-layout').dom.innerHTML = '' ; 
        if( document.getElementById('cbuilder-layout') != null){
            //alert(document.getElementById('cbuilder-layout').innerHTML ) ; 
            document.getElementById('cbuilder-layout').innerHTML  = '' ;
        } 
        
        //Ext.get('cbuilder-layout')) ;  
        var mainPanel = Ext.getCmp('main-panel') ;   
        
        var conditionPanelArray = mainPanel.findByType('ConditionFieldPanel') ;
        
        if( conditionPanelArray.length == 0 )
            return ;
            
        // Change class of panel 
        
        for( var i = 0 ; i < conditionPanelArray.length ; i++){
            conditionPanelArray[i].removeClass('ConditionFieldPanel') ; 
            conditionPanelArray[i].removeClass('ConditionFieldPanel2') ; 
            if( i == 0 ){
                conditionPanelArray[i].addClass('ConditionFieldPanel') ; 
            }else{
                if( conditionPanelArray[i-1].level != conditionPanelArray[i].level)
                    conditionPanelArray[i].addClass('ConditionFieldPanel2') ; 
            }
        }
        
        // ---------------------    
            
        var mainPos = mainPanel.getPosition()  ; 
        //console.debug(DropPanalArray) ; 
        var sPosX = -1 ; 
        var sPosY = -1 ; 
        // Check for total draw > 1 ; 
        var totalDraw = 0 ; 
        for( var i = 0 ; i < conditionPanelArray.length ; i++){
            if( conditionPanelArray[i].level == 1){
                totalDraw++ ;    
            }
        }
        
        var adjustPos = conditionPanelArray[0].getPosition() ; 
        //console.debug(adjustPos) ; 
        var adjustPosY = 12 -( adjustPos[1] - mainPos[1] )  ; 
        
        for( var i = 0 ; i < conditionPanelArray.length ; i++){ 
            if( conditionPanelArray[i].level == 1 && totalDraw > 1 ){
                var pos =  conditionPanelArray[i].getPosition()   ;
                var ePosX = pos[0] - mainPos[0] ; 
                var ePosY = pos[1] - mainPos[1] ; 
                if( sPosX == -1){
                    sPosX = ePosX ; 
                    sPosY = ePosY ;
                }
                var newLayout = Ext.get('cbuilder-layout') ; 
                //var x = newLayout.Element('sssss') ; 
                newLayout.createChild('<div style="top:'+(ePosY + adjustPosY + 5)+'px;left:'+(ePosX-10)+'px;width:8px;" class="cbuilder-line-or"></div>') ; 
                var nextIndex = condition.findNextSameLevelPanelIndex(conditionPanelArray , i) ; 
                if( nextIndex != - 1 ){
                    var pos2 =  conditionPanelArray[nextIndex].getPosition()   ;
                    var ePosX2 = pos2[0] - mainPos[0] ; 
                    var ePosY2 = pos2[1] - mainPos[1] ; 
                    newLayout.createChild('<div style="top:'+(ePosY + (( ePosY2 - ePosY )/2)   + adjustPosY)+'px;left:'+(ePosX-20)+'px;" class="cbuilder-or-icon"></div>') ; 
                }
            }    
            
            condition.renderBackGroudLayout2(conditionPanelArray[i] , adjustPosY) ;         
        }
        // Draw verticle line ;    
        if( totalDraw > 1){ 
            var newLayout = Ext.get('cbuilder-layout') ;
            newLayout.createChild('<div style="top:'+(sPosY+adjustPosY+ 5)+'px;left:'+(sPosX-10)+'px;height:'+(ePosY-sPosY )+'px;" class="cbuilder-line-or"></div>') ;
            
        } 
        
        //console.debug(Ext.getCmp('main-panel')) ;  
    } , 
    
    findNextSameLevelPanelIndex : function(conditionPanelArray , index){
        for( var i = index+1 ; i < conditionPanelArray.length ; i++){
            if( conditionPanelArray[index].level == conditionPanelArray[i].level){
                return i ; 
            }
        }
        return -1  ; 
    } , 
    
    renderBackGroudLayout2 : function(panel , adjustHeight){
        var mainPanel = Ext.getCmp('main-panel') ;  
        var mainPos = mainPanel.getPosition()  ; 
        
        var panelChilds =  condition.getPanelChild(panel) ; 
        if( panelChilds.length == 0 ){
            return ; 
        }
        
        // Draw green line around panel ; 
        var pos =  panelChilds[0].getPosition()   ;
        var sPosX = pos[0] - mainPos[0] ; 
        var sPosY = pos[1] - mainPos[1] ; 
        var width = panelChilds[0].getWidth() ; 
        
        
        var newLayout = Ext.get('cbuilder-layout') ; 
        newLayout.createChild('<div style="top:'+(sPosY-10 + adjustHeight)+'px;left:'+(sPosX-20)+'px;width:'+(width + 22 ) +'px;" class="cbuilder-line-and"></div>') ; 
        var pos =  panelChilds[panelChilds.length -1 ].getPosition() ;
        var ePosX = pos[0] - mainPos[0] ; 
        var ePosY = pos[1] + panelChilds[panelChilds.length -1 ].getHeight() - mainPos[1] ; 
        
        newLayout.createChild('<div style="top:'+(ePosY+6 + adjustHeight)+'px;left:'+(ePosX-20)+'px;width:'+( width + 22) +'px;" class="cbuilder-line-and"></div>') ;
        
        newLayout.createChild('<div style="top:'+(sPosY-10 + adjustHeight)+'px;left:'+(sPosX-20)+'px;height:'+(ePosY - sPosY + 16)+'px;" class="cbuilder-line-and"></div>') ;
        newLayout.createChild('<div style="top:'+(sPosY-10 + adjustHeight)+'px;left:'+(sPosX - 20 + width + 22)+'px;height:'+(ePosY - sPosY + 16)+'px;" class="cbuilder-line-and"></div>') ;
 
        
        // Draw Line between and ; 
        var pos =  panel.getPosition()   ;
        var pPosX = pos[0] - mainPos[0] ; 
        var pPosY = pos[1] + panel.getHeight() - mainPos[1] ; 
        
        newLayout.createChild('<div style="top:'+(pPosY - 2 + adjustHeight)+'px;left:'+(sPosX+20)+'px;height:'+(sPosY - pPosY  )+'px;" class="cbuilder-line-and2"></div>') ;
        
        newLayout.createChild('<div style="top:'+(pPosY + 4 + adjustHeight)+'px;left:'+(sPosX+5)+'px;" class="cbuilder-and-icon"></div>') ;
        
        // Add or layout ; 
        
        var sPosX = -1 ; 
        var sPosY = -1 ; 
        var totalDraw = panelChilds.length ; 
         
        for( var i = 0 ; i < panelChilds.length ; i++){
            if(totalDraw > 1 ){
                var pos =  panelChilds[i].getPosition()   ;
                var ePosX = pos[0] - mainPos[0] ; 
                var ePosY = pos[1] - mainPos[1] ; 
                if( sPosX == -1){
                    sPosX = ePosX ; 
                    sPosY = ePosY ;
                }
                var newLayout = Ext.get('cbuilder-layout') ; 
                //var x = newLayout.Element('sssss') ;  
                newLayout.createChild('<div style="top:'+(ePosY+5 + adjustHeight)+'px;left:'+(ePosX-10)+'px;width:8px;" class="cbuilder-line-or"></div>') ; 
                if( i != panelChilds.length - 1 ){
                    var pos2 =  panelChilds[i + 1].getPosition()   ;
                    var ePosX2 = pos2[0] - mainPos[0] ; 
                    var ePosY2 = pos2[1] - mainPos[1] ; 
                    newLayout.createChild('<div style="top:'+(ePosY + ( ePosY2 - ePosY )/2   + adjustHeight)+'px;left:'+(ePosX-20)+'px;" class="cbuilder-or-icon"></div>') ; 
                }
            }    
            condition.renderBackGroudLayout2(panelChilds[i] , adjustHeight) ; 
        }
        
        if( totalDraw > 1){ 
            var newLayout = Ext.get('cbuilder-layout') ;
            newLayout.createChild('<div style="top:'+(sPosY+5 + adjustHeight)+'px;left:'+(sPosX-10)+'px;height:'+(ePosY-sPosY)+'px;" class="cbuilder-line-or"></div>') ;
        } 
        
        
    } , 
    
    saveTo_ttContent : function(){
       data = {} ; 
       data['contentId'] = condition.contentId ; 
       data['condition'] = Ext.getCmp('condition-result').getValue() ; 
       
       if(condition.crossDomain){
            data['eID'] = 't3p_dynamic_content_placement' ; 
            data['cmd'] = 'setCondition' ; 
            data['pid'] = pwMain.pid ;  
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data, 
                callback: function(result) { 
                    condition.windowObj.close() ; 
                    condition.windowObj.destroy() ; 
                }
            });
       }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=setCondition'  , 
                //url : 'testxml.php' , 
    			method: 'POST',
    			params:   data ,
    			timeout: '30000',
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
     			    condition.windowObj.close() ; 
                    condition.windowObj.destroy() ; 
    			},
    			failure: function() {
    				Ext.MessageBox.hide();
    				Ext.MessageBox.alert('Error', '#402' ) ; 
    			}
    		});
       }
       
       	
    } , 
    
    saveTo_ttContent2 : function(){
       data = {} ; 
       data['contentId'] = condition.contentId ; 
       data['condition'] = Ext.getCmp('condition-result').getValue() ; 
        
       if( condition.crossDomain){
            data['eID'] = 't3p_dynamic_content_placement' ; 
            data['cmd'] = 'setCondition' ; 
            data['pid'] = mn_main.pid ;  
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data, 
                callback: function(result) { 
                    condition.windowObj.close() ; 
                    condition.windowObj.destroy() ; 
                }
            });
       }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+mn_main.pid+'&cmd=setCondition'  , 
                //url : 'testxml.php' , 
    			method: 'POST',
    			params:   data ,
    			timeout: '30000',
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
     			    condition.windowObj.close() ; 
                    condition.windowObj.destroy() ; 
    			},
    			failure: function() {
    				Ext.MessageBox.hide();
    				Ext.MessageBox.alert('Error', '#402' ) ; 
    			}
    		});	
       }
       
    } , 
    
    
    destroyCBuilder : function(){
        condition.windowObj.close() ;  
        //condition.windowObj.destroy() ; 
    } , 
    
    saveTo_variation : function(){
        variation.condition = Ext.getCmp('condition-result').getValue() ;   
        condition.destroyCBuilder() ;      
        
        if( condition.crossDomain ){ 
            if( ml_testbubble != undefined ){
                if(Ext.getCmp('test_bubble_11') == undefined )
                    ml_testbubble.open11(variation.windowObj) ;  
            }
        } 
    } , 
    
    saveTo_contenteditor : function(){
        pwContentEditor.condition = Ext.getCmp('condition-result').getValue() ; 
        condition.destroyCBuilder() ;  
    } , 
    
    saveTo_triggerMail : function(){
        Ext.getCmp('trigger_condition').setValue(Ext.getCmp('condition-result').getValue()) ; 
        condition.destroyCBuilder() ;  
    } , 
    
    saveTo_htmlEditor : function(){
        condition.objectReturn.saveCBuilder(Ext.getCmp('condition-result').getValue()) ; 
        condition.destroyCBuilder() ;  
    } , 
    
    saveTo_htmlEditor2 : function(){
        condition.objectReturn.saveCBuilder2(Ext.getCmp('condition-result').getValue()) ; 
        condition.destroyCBuilder() ;  
    } ,  
    
    saveTo_goalCreateNew : function(){
        goal_controller.conditionStr = Ext.getCmp('condition-result').getValue() ;
        goal_controller.saveCondition() ; 
         condition.destroyCBuilder() ; 
    } , 
    
    saveTo_goalEdit : function(){
        goal_controller.conditionStr = Ext.getCmp('condition-result').getValue() ;
        goal_controller.saveCondition2() ; 
        condition.destroyCBuilder() ; 
    } , 
    
    saveTo_goalPlacementEdit : function(){
        goal_placement.conditionStr = Ext.getCmp('condition-result').getValue() ;
        goal_placement.saveCondition() ; 
        condition.destroyCBuilder() ; 
    } , 
    
    saveTo_newsletterFilter : function(){
        mn_filters.condition = Ext.getCmp('condition-result').getValue() ;
        mn_filters.saveRecord() ; 
        condition.destroyCBuilder() ; 
    } , 
    
    saveTo_newsletterEditSubjectline : function(){
        mn_mail.subjectline_condition_save(Ext.getCmp('condition-result').getValue()) ; 
        condition.destroyCBuilder() ; 
    } , 
    
    saveTo_newsletterEditTextMode : function(){
        mn_mail.htmltextmode_condition_save(Ext.getCmp('condition-result').getValue()) ; 
        condition.destroyCBuilder() ; 
    } , 
    
     
    saveTo_dynamiCCE : function(){
        contentEditorCE.condition = Ext.getCmp('condition-result').getValue() ; 
        condition.destroyCBuilder() ;  
    }  ,
    
    saveTo_assetElement : function(){
        assetElement.condition = Ext.getCmp('condition-result').getValue() ; 
        condition.destroyCBuilder() ;  
    }, 
    
    saveTo_segment_new : function(){
    	segment_main.saveNewCondition( Ext.getCmp('condition-result').getValue() ) ; 
    	condition.destroyCBuilder() ;  
    } , 
    
    saveTo_strategy_new : function(){
    	var me = this ; 
    	strategy_detail_window.saveNewCondition( Ext.getCmp('condition-result').getValue() , me.getConditionTitle() ) ;  
    	condition.destroyCBuilder() ; 
    } , 
    
    saveTo_contentlist : function(){
    	var me = this ; 
    	mdc_contentmanager.saveFormConditionEditor( me.contentId , Ext.getCmp('condition-result').getValue() ) ;  
    	condition.destroyCBuilder() ; 
    } , 
    
    saveTo_contentListPage : function(){
    	var me = this ;
    	Ext.getCmp('placementdetail-grid').saveCondition(  me.contentId , Ext.getCmp('condition-result').getValue() )  ; 
    	condition.destroyCBuilder() ; 
    } , 
    
    saveTo_contentMultipleListPage : function(){
    	var me = this ; 
    	Ext.getCmp('placementdetail-grid').saveMultipleCondition(  me.contentId , Ext.getCmp('condition-result').getValue() )  ; 
    	condition.destroyCBuilder() ; 
    } , 
    
    saveTo_multiselectedForm : function(){
    	var me = this ; 
   	 	multiselected.condition = Ext.getCmp('condition-result').getValue()  ; 
    	condition.destroyCBuilder() ; 
    } , 
    
    saveTo_experimentSegment : function(){
    	var me = this ; 
   	 	Ext.getCmp('wizard-step-2_1').saveCondition( Ext.getCmp('condition-result').getValue() ) ; 
   	 	condition.destroyCBuilder() ; 
    } , 
    
    saveTo_experimentGoal : function(){
    	var me = this ; 
   	 	Ext.getCmp('wizard-step-3_1').saveCondition( Ext.getCmp('condition-result').getValue() ) ; 
   	 	condition.destroyCBuilder() ; 
    }
}


// Saved condition 

saved_condition = {
    uid : 0 , 
    openWindow : function(){
        var panel1 = new Ext.Panel({
            border : true , 
            region : 'north' , 
            height : 60 ,
            bodyStyle : 'padding-top : 10px ; padding-bottom : 10 px' , 
            items : [{
                xtype : 'compositefield' , 
                items : [
                {
                    xtype: 'displayfield',
                    value: '' ,
                    width: 5 
                },{
                    xtype : 'radio' , 
                    boxLabel  : 'Create new condition' ,
                    name : 'chk-c' , 
                    id : 'saved-c-createnew-checkbox' , 
                    checked : true , 
                    listeners : {
                        'check' : function(checkbox ,checked){
                            //console.debug(checked) ; 
                            if( checked == false){
                                Ext.getCmp('saved-condition-grid').enable() ; 
                                Ext.getCmp('new-textfield-savedcondition').disable() ;
                            }else{
                                Ext.getCmp('saved-condition-grid').disable() ; 
                                Ext.getCmp('new-textfield-savedcondition').enable() ;
                            }
                            //Ext.getCmp('saved-condition-grid').disable() ; 
                            //Ext.getCmp('new-textfield-savedcondition').enable() ; 
                        }
                    }
                },{
                    xtype: 'displayfield',
                    value: '' ,
                    width: 20 
                },{
                    xtype : 'textfield' , 
                    width : 200 , 
                    id : 'new-textfield-savedcondition' , 
                    allowBlank : false 
                }]
            } , {
                xtype : 'compositefield' , 
                items : [
                {
                    xtype: 'displayfield',
                    value: '' ,
                    width: 5 
                },{
                    xtype : 'radio' , 
                    name : 'chk-c' , 
                    boxLabel  : 'Overwrite on exits condition' ,
                    checked : false , 
                    
                    listeners : {
                        'check' : function(checkbox ,checked){
                            //Ext.getCmp('saved-condition-grid').enable() ; 
                            //Ext.getCmp('new-textfield-savedcondition').disable() ; 
                       }
                    }
                } ]
            }  ]
        }) ; 
        
        var store = new Ext.data.JsonStore({
	        root: 'topics',
	        totalProperty: 'totalCount',
	        idProperty: 'uid',
	        remoteSort: true,
	
	        fields:  [
    	        {name: 'uid', type: 'int'}, 
  	        	{name: 'name'} ,  
                {name: 'cid' , type : 'int'} , 
                {name: 'conditionStr'} ,  
                {name: 'crdate' , mapping: 'crdate', type: 'date', dateFormat: 'timestamp'} 
  	        ],
	
	        proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=savedcondition/list'
	        })
         }) ;
         
         store.setDefaultSort('crdate', 'desc');
         
 
         
         function renderAction(value, id, r){
            if( r.get['cid'] == -1){
                return '' ; 
            }
            var id = Ext.id(); 
            createSelectButton.defer(1, this, ['Save', id , r]);  
            var id2 = Ext.id() ; 
            createDeleteButton.defer(1, this, ['Delete', id2 , r]);  
            return('<table><tr><td><div id="' + id + '"></div></td><td><div id="' + id2 + '"></div></td></tr></table>');
        }
         
         function createSelectButton(value, id, record) {
            new Ext.Button({
                text: value 
                ,handler : function(btn, e) { 
                    // Start // 
                    var data = {} ; 
                    data['data[uid]'] = record.get('uid') ;  
                    data['data[conditionStr]'] =  Ext.getCmp('condition-result').getValue() ; 
                    
                    Ext.Ajax.request({
            			url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=savedcondition/save'  , 
            			// send additional parameters to instruct server script
            			method: 'POST',
            			timeout: '30000',
                        params:   data ,
            			// process the response object to add it to the TabPanel:
            			success: function(xhr) {
                            Ext.getCmp('save-as-condition').close() ; 
            			},
            			failure: function() {
                            Ext.MessageBox.hide();
            			    Ext.MessageBox.alert('Error', '#401' ) ; 
            			}
            		});
                }
            }).render(document.body, id);
        } 
        
        function createDeleteButton(value , id , record){
            new Ext.Button({
                text: value 
                ,handler : function(btn, e) { 
                    // Start // 
                    saved_condition.uid = record.get('uid') ; 
                    Ext.MessageBox.confirm('Confirm', 'Are you sure to abort this record ?', saved_condition.deleteCondition ) ; 
                }
            }).render(document.body, id);
        }
        
        var grid =  new Ext.grid.EditorGridPanel({
            width: '100%',
            region : 'center' , 
            id : 'saved-condition-grid' , 
            border : false ,
	        store:  store,
	        trackMouseOver:false,
            disabled : true , 
	        disableSelection:false,
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
            columns:[{
	            header: "Name", 
                width : 200 ,
	            dataIndex: 'name',
	            sortable: true 
	        } ,{
	            header: "Condition", 
                width : 200 ,
	            dataIndex: 'conditionStr',
	            sortable: true 
	        } ,{
	            header: 'Date' , 
                dataIndex : 'crdate' , 
                sortable : true , 
                renderer: Ext.util.Format.dateRenderer('d M Y')  
	        },{
	            header: 'Action' , 
                dataIndex : 'uid' , 
                sortable : false , 
                renderer: renderAction  
	        } ],
	
	        // customize view config	       
	        viewConfig: {
	            forceFit:true 
	        } , 
            
            bbar: new Ext.PagingToolbar1({
                displayMsg : 'Displaying {0} of {1}' ,
                id : 'savedcondition-paging-toolbar' , 
	            pageSize: 15,
	            store: store,
	            displayInfo: true,
	            plugins: new Ext.ux.ProgressBarPager() ,
	            emptyMsg: "No topics to display"  
	        })
        }) ; 
        
        var window = new Ext.Window({
            width:600,
         	height:500,
            title:"Save As Condition",
            id : 'save-as-condition' , 
            plain:true , 
         	modal:true,
            layout : 'border' ,
         	maximizable : true  , 
         	closable:true  ,
            items : [panel1 , grid ] , 
            buttons : [
            {
                text : 'Save' , 
                cls: 'monoloop-black-btn' , 
                handler : function(){
                    //console.debug( Ext.getCmp('saved-c-createnew-checkbox').getValue() ) ; 
                    
                    if( Ext.getCmp('saved-c-createnew-checkbox').getValue() == true){
                        if( ! Ext.getCmp('new-textfield-savedcondition').isValid()){
                            Ext.getCmp('new-textfield-savedcondition').markInvalid()  ; 
                            return ; 
                        }
                        var data = {} ; 
                        data['data[uid]'] = 0 ; 
                        data['data[name]'] = Ext.getCmp('new-textfield-savedcondition').getValue() ; 
                        data['data[conditionStr]'] =  Ext.getCmp('condition-result').getValue() ; 
                        
                        Ext.Ajax.request({
                			url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=savedcondition/save'  , 
                			// send additional parameters to instruct server script
                			method: 'POST',
                			timeout: '30000',
                            params:   data ,
                			// process the response object to add it to the TabPanel:
                			success: function(xhr) {
                                Ext.getCmp('save-as-condition').close() ; 
                			},
                			failure: function() {
                                Ext.MessageBox.hide();
                			    Ext.MessageBox.alert('Error', '#401' ) ; 
                			}
                		});
                    }
                }
            }
            ]
        }) ;
        
        window.show() ;  
        
        saved_condition.refreshGrid() ; 
    } ,  
    
    deleteCondition : function(cmd){
        if( cmd != 'yes')
            return ; 
         
        var data = {} ; 
        data['uid'] = saved_condition.uid ; 
        Ext.Ajax.request({
            url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=savedcondition/delete'  , 
            method: 'POST',
            timeout: '30000',
            params:   data , 
            success: function(xhr) {  
                saved_condition.refreshGrid() ; 
                Ext.MessageBox.hide(); 
            }, failure: function() {
                Ext.MessageBox.hide();
                Ext.MessageBox.alert('Error', '#1201' ) ; 
            }
        }) ; 
    } , 
    
    refreshGrid : function(){
        Ext.getCmp('saved-condition-grid').store.load({params:{start:Ext.getCmp('savedcondition-paging-toolbar').cursor , limit:15}});
    } , 
    
    loadWindow : function(){
        
        var store = new Ext.data.JsonStore({
	        root: 'topics',
	        totalProperty: 'totalCount',
	        idProperty: 'uid',
	        remoteSort: true,
	
	        fields:  [
    	        {name: 'uid', type: 'int'}, 
  	        	{name: 'name'} ,  
                {name: 'cid' , type : 'int'} , 
                {name: 'conditionStr'} ,  
                {name: 'crdate' , mapping: 'crdate', type: 'date', dateFormat: 'timestamp'} 
  	        ],
	
	        proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=savedcondition/list'
	        })
         }) ;
         
         store.setDefaultSort('crdate', 'desc');
         
 
         
         function renderAction(value, id, r){
            if( r.get['cid'] == -1){
                return '' ; 
            }
            var id = Ext.id(); 
            createSelectButton.defer(1, this, ['Load', id , r]);   
            return('<table><tr><td><div id="' + id + '"></div></td></tr></table>');
        }
         
         function createSelectButton(value, id, record) {
            new Ext.Button({
                text: value 
                ,handler : function(btn, e) { 
                    // Start // 
                    var data = {} ; 
                    data['condition'] = record.get('conditionStr') ;  
                    condition.preconditionStr = record.get('conditionStr') ;  
                    var mainPanel =  Ext.getCmp('main-panel');
                    var allPanel = mainPanel.findByType('ConditionFieldPanel') ;
                    for(var i = 0 ; i < allPanel.length ; i++){
                        mainPanel.remove(allPanel[i]) ; 
                    }
                    Ext.Ajax.request({
                        url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=getConditional'  , 
                        method: 'POST',
                        timeout: '30000',
                        params:   data , 
                        success: function(xhr) {   
                            var retData = Ext.util.JSON.decode(xhr.responseText) ;
                            Ext.MessageBox.hide(); 
                            //Ext.getCmp('main-panel').removeAll() ;  
                            condition.precondition = retData.conditional ; 
                            //console.debug(condition.preconditionStr) ; 
                            
                            Ext.getCmp('load-condition').close() ; 
                            condition.reengineerCondition() ;
                        }, failure: function() {
                            Ext.MessageBox.hide();
                            Ext.MessageBox.alert('Error', '#1201' ) ; 
                        }
                    }) ; 
                    
                }
            }).render(document.body, id);
        }  
        
        var grid =  new Ext.grid.EditorGridPanel({
            width: '100%',
            region : 'center' , 
            id : 'saved-condition-grid' , 
            border : false ,
	        store:  store,
	        trackMouseOver:false,
            disabled : false , 
	        disableSelection:false,
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
            columns:[{
	            header: "Name", 
                width : 200 ,
	            dataIndex: 'name',
	            sortable: true 
	        } ,{
	            header: "Condition", 
                width : 200 ,
	            dataIndex: 'conditionStr',
	            sortable: true 
	        } ,{
	            header: 'Date' , 
                dataIndex : 'crdate' , 
                sortable : true , 
                renderer: Ext.util.Format.dateRenderer('d M Y')  
	        },{
	            header: 'Action' , 
                dataIndex : 'uid' , 
                sortable : false , 
                renderer: renderAction 
	        } ],
	
	        // customize view config	       
	        viewConfig: {
	            forceFit:true 
	        } , 
            
            bbar: new Ext.PagingToolbar1({
                displayMsg : 'Displaying {0} of {1}' ,
                id : 'savedcondition-paging-toolbar' , 
	            pageSize: 15,
	            store: store,
	            displayInfo: true,
	            plugins: new Ext.ux.ProgressBarPager() ,
	            emptyMsg: "No topics to display"  
	        })
        }) ; 
        
        var window = new Ext.Window({
            width:600,
         	height:500,
            title:"Load Condition",
            id : 'load-condition' , 
            plain:true , 
         	modal:true,
            layout : 'border' ,
         	maximizable : true  , 
         	closable:true  ,
            items : [  grid ]  
        }) ;
        
        window.show() ;   
        
        saved_condition.refreshGrid() ; 
    }
} ; 

//---	
}) ; 