define(function (require) {
// --- start 

window.cphSlider = {
	
	renderForm : function(data){
		var me = this ; 
		
		var main = new Ext.Panel({
			height : 0 , 
			border : false ,   
			baseCls : 'dtb-main' , 
			padding : 8 , 
			items : []
		}); 
		
		Ext.getCmp(contentEditorCE.id + '-form').add(main) ;
		
		Ext.getCmp(contentEditorCE.id + '-top-addin').add({
			xtype : 'compositefield' , 
			width : '100%' ,
			items : [ {
				xtype : 'button' , 
				style : 'margin : 0 0 5px 0' ,  
				iconCls : 'newpage-icon' ,  
				width : 200 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create new content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					selectTab.showContentElementSelectorWindow(5,0) ;  
				}
			},{
				xtype : 'button' ,  
				width : 170 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add pre-defined content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					predefinedCEWindow.openWindow() ; 
				}
			}]		
		}) ;
	} ,
	
	afterrender : function(data){
		var me = this ; 
		for( var i = 0 ; i < data.content.length ; i++){
			var field = data.content[i] ; 
			if( field['type'] == 'ce'){
				continue ; 
			}
			
			if( field['value'] == '' || field['value'] == null){
				continue ; 
			}
			
			if( field['el'] == 'asset_repeatmode' ){  
				Ext.getCmp( me.id + '-' + field['el']+ '-' + field['value']).setValue(true) ;   
				if(  field['value'] == 1){ 
                    me.setDimManaulRepeat(true) ; 
                    me.setDimStepRepeat(false) ;
                }else{  
                    me.setDimManaulRepeat(false) ; 
                    me.setDimStepRepeat(true) ;
                }
			}else{
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef['+field['el']+']').setValue(field['value']) ; 
			}
		}
	}
} ; 

return window.cphSlider ; 
//---	
}) ; 