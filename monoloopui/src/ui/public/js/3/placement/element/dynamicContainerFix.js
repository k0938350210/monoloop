define(function (require) {
// --- start 

window.dynamicContainerFix = {
	id : 'dynamic-container-fix' ,
	
	renderForm : function(data){
		Ext.getCmp(contentEditorCE.id + '-leftPanel').setVisible(false) ; 
		
		Ext.getCmp(contentEditorCE.id + '-top-addin').add({
			xtype : 'compositefield' , 
			width : '100%' ,
			items : [{
				xtype : 'button' , 
				style : 'margin : 0 0 5px 0' ,  
				iconCls : 'newpage-icon' ,  
				width : 200 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create new content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					selectTab.showContentElementSelectorWindow(5,0) ;  
				}
			},{
				xtype : 'button' ,  
				width : 170 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add pre-defined content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					predefinedCEWindow.openWindow() ; 
				}
			}]		
		}) ;
		
	} , 
	
	afterrender : function(data){
		
	}
} ; 

return window.dynamicContainerFix ; 

//---	
}) ; 