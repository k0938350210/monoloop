define(function (require) {
// --- start 
window.sliderTab = {
	firstValidate : function(){ 
		if(  Ext.getCmp('tab_backgroundimage').getValue() == '' && Ext.getCmp('tab_text').getValue() == '' ){
			 Ext.getCmp('tab_text').markInvalid('Cannot empty') ; 
			return false ; 
		}
		return true ; 	
	} , 
	
	renderForm : function(data){
		var me = this ;   
		var main = new Ext.Panel({
			height : 1048 , 
			border : false ,   
			baseCls : 'sl-main-slider' , 
			padding : 8 , 
			items : [{
				xtype : 'displayfield' , 
				value : '<img src="./typo3conf/ext/t3p_base/image/info.GIF" />' , 
				style : 'position: absolute; right : 10px ; top 15px ;'
			},{
				xtype : 'displayfield' , 
				value : '<b>1. Design</b> Slider:' , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'panel' ,
				height : 121 ,  
				border : false ,   
				baseCls : 'sl-design-dimensions' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img id="img-sl-design-dimensions" src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Enter a Width and Height for your Slider, or Choose \'Autosize\' for your Slider to automatically scale to fit your content." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;' 
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Dimensions:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;' ,
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						name : 'cef[slider_dimensions]' , 
						id : 'slider_dimensions-fixed' ,
						style : 'margin : 3px 0 0 0 ;' ,
						inputValue : 'fixed' , 
						width : 15 
					},{
						xtype : 'displayfield' , 
						value : 'Width'
						
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						name : 'cef[slider_dimensions_width]' , 
						enableKeyEvents : true ,
						id : 'slider_dimensions_width' , 
						value : 0 , 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;x&nbsp;Height&nbsp;'
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						enableKeyEvents : true ,
						name : 'cef[slider_dimensions_height]' , 
						id : 'slider_dimensions_height' , 
						value : 0, 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : 'pixels'
					}]
				},{
					xtype : 'compositefield' ,  
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						id : 'slider_dimensions-auto' ,
						inputValue : 'auto' ,
						name : 'cef[slider_dimensions]' , 
						style : 'margin : 3px 0 0 0 ;' ,
						width : 15 , 
						checked : true 
					},{
						xtype : 'displayfield' , 
						value : 'Autosize Slider window (to fit contents).'
					} ]
				},{
                    xtype : 'container' , 
                    layout : 'column' ,  
                    items : [{
                        xtype : 'displayfield' , 
                        value : 'Maximum number of elements' , 
                        columnWidth : 0.6 , 
                    },{
                        xtype : 'spinnerfield' , 
                        allowBlank: false  ,  
                        name: 'cef[maxelements]', 
                        id : me.id + '-maxelements' ,    
                        validateOnBlur : false , 
                        columnWidth : 0.2  , 
                        enableKeyEvents : true , 
                        value : 1 , 
                        allowDecimals: false,
                        minValue: 0,
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                        listeners : {
                            'keyup' : function(textbox , e){     
                            } , 
                            'spin' : function(textbox){ 
                            }  
                        }
                    }]
                }
				]
			},{
				xtype : 'panel' ,
				height : 148 ,  
				border : false ,   
				baseCls : 'sl-design-content' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define padding between content and Slider.\n �	Create or Add the Content for your Slider."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Content:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'container' , 
					style : 'margin : 5px 0 0 0' ,
                    layout : 'column' , 
                    items : [{
                    	xtype : 'displayfield' , 
                    	value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/dtb-padding.png" />' , 
                    	columnWidth : 0.4
                    },{
                    	xtype : 'displayfield' , 
                    	value : '&nbsp;' , 
                    	columnWidth : 0.05 
                    },{
                    	xtype : 'container' , 
	                    items : [{
	                    	xtype : 'displayfield' , 
	                    	value : 'Padding around<br>Slider Content:'  ,
	                    	style : 'margin : 0 0 10px 0 ;' 
	                    },{
	                        xtype : 'spinnerfield' ,  
	                        width : 80 ,  
	                        allowDecimals: false,
	                        name : 'cef[slider_padding]' , 
	                        id : 'slider_padding' , 
	                        minValue: 0,
	                    	decimalPrecision: 0,
	                    	incrementValue: 1,
	                    	alternateIncrementValue: 1,
	                    	accelerate: true , 
	                        value : 0
	                        
	                    }] , 
	                    columnWidth : 0.55
                    }]
				},{
					xtype : 'container' , 
					style : 'padding : 10px 15px 0 25px ;' , 
					defaults : {
						anchor : '100%'	
					} ,
					items : [{
						xtype : 'button' , 
						style : 'margin : 0 0 5px 0' , 
						width : '100%' , 
						iconCls : 'newpage-icon' ,  
						text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create / Add Content</span> <div class="toolbar-btn"></div>' , 
						handler : function(){
							selectTab.showContentElementSelectorWindow(5,0) ;  
						}
					}]
				}
				]
			},{
				xtype : 'panel' ,
				height : 215 ,  
				border : false ,   
				baseCls : 'sl-design-styling' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define the style and transparency for your Slider." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Styling:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' ,
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        width : 185 ,
                        name: 'cef[slider_border]',
                        hiddenId : 'slider_border' , 
                        displayField:   'name',
                        valueField:     'value',   
                        emptyText : 'Select a Border' , 
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['None','None'],['solid', 'solid'],['dotted', 'dotted'],['double', 'double'],['dashed', 'dashed']]
                        }),
                        validateOnBlur : false ,
                        value : 'None'
					},{
						xtype : 'spinnerfield' ,     
                        allowDecimals: false,
                        minValue: 0,
                        name : 'cef[slider_border_position]' , 
                        id : 'slider_border_position' , 
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                    	flex : 1 , 
                        value : 0
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 5px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Slider Border Colour'
					},{
						xtype : 'colorfield' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						colorSelector:"mixer" ,  
						msgTarget: 'qtip' , 
						name : 'cef[slider_bordercolor]'  ,
						flex : 1 
					}]
				},{
					xtype : 'fileuploadfield' ,  
					name : 'cef[slider_backgroundimage]' , 
					id : 'slider_backgroundimage' , 
					width : '100%' , 
                    emptyText: 'Select a Background Image' ,
                    hideLabel : true ,   
                    buttonText: '' , 
                    buttonCfg: {
                        iconCls: 'upload-icon'
                    }  
				},{
					xtype : 'compositefield' ,
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Background Colour:'
					} ,{
						xtype : 'colorfield' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						colorSelector:"mixer" ,    
						msgTarget: 'qtip' , 
						flex : 1 , 
						name : 'cef[slider_bgcolor]' 
					}]
				},{
                    xtype : 'displayfield' , 
                    value : 'Slider Transparency: <span style="font-weight: bold;" id="slider-transparency"></span>'
                },{
                    xtype : 'sliderfield' , 
                    name:  'cef[slider_transparency]' , 
					id : 'slider_transparency' ,  
                    linkID : 'slider-transparency' , 
                    objValue : 0 , 
                    listeners : { 
                        'afterrender' : function(field){
                            if( field.objValue != ''){
                                field.setValue(field.objValue) ; 
                            }else{
                            	field.setValue(0) ; 
                            } 
                            document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ; 
                            field.slider.on('change', function() { 
                                document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ;  
                            });  
                        }
                    }
                }
				]
			},{
				xtype : 'panel' ,
				height : 219 ,  
				border : false ,   
				baseCls : 'sl-design-positioning' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define browser position for your Slider.\n�	Fix the position or scroll Slider with page." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Positioning:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'displayfield' , 
					value : 'Select browser edge for start position (<i>rollover</i>):'
				},{
					xtype : 'panel' ,
					height : 101 ,  
					border : false ,   
					baseCls : 'sl-design-position-select' ,
					html : '<div class="" id="slider-pos-Left" onclick="Ext.getCmp(\'slider_position\').setValue(\'Left\');"></div><div class="" id="slider-pos-Right" onclick="Ext.getCmp(\'slider_position\').setValue(\'Right\');"></div><div class="" id="slider-pos-Top" onclick="Ext.getCmp(\'slider_position\').setValue(\'Top\');"></div><div class="" id="slider-pos-Bottom" onclick="Ext.getCmp(\'slider_position\').setValue(\'Bottom\');"></div>'
				},{
					xtype : 'hidden' , 
					id : 'slider_position' , 
					name : 'cef[slider_position]' , 
					setValue : function(val){
						sliderTab.posClick(val) ; 
						this.setRawValue(val);
						return this; 
					} 
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Position Slider:'
					},{
						xtype : 'spinnerfield' ,  
                        width : 40 ,  
                        allowDecimals: false,
                        minValue: 0,
                        name : 'cef[slider_positioning_slider]' , 
                        id : 'slider_positioning_slider' , 
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                        value : 0
					},{
						xtype : 'displayfield' , 
						value : 'pixels from' 
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 , 
                        name: 'cef[slider_positioning_pixelform]',
                        hiddenId : 'slider_positioning_pixelform' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['Left','Left'],['Right','Right'],['Top','Top'],['Bottom', 'Bottom']]
                        }),
                        validateOnBlur : false 
					}]
				},{
					xtype : 'compositefield' ,
					items : [{
						xtype : 'radio' , 
						width : 15 ,
						style : 'margin : 3px 0 0 0 ;' ,
						name : 'cef[slider_positioning_type]' , 
						id : 'slider_positioning_type-Fixed' , 
						inputValue : 'Fixed' , 
						checked : true 
					},{
						xtype : 'displayfield' , 
						value : 'Fix position'
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;' , 
						width : 35
					},{
						xtype : 'radio' , 
						style : 'margin : 3px 0 0 0 ;' ,
						width : 15 ,
						name : 'cef[slider_positioning_type]' , 
						id : 'slider_positioning_type-Scroll' , 
						inputValue : 'Scroll'
					},{
						xtype : 'displayfield' , 
						value : 'Scroll with page'
					}]
				}
				]
			},{
				xtype : 'panel' ,
				height : 184 ,  
				border : false ,   
				baseCls : 'sl-design-positiontab' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Position your Slider Tab relative to Slider edge."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Position slider tab</b> ( relative to slider edge):' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'container' ,  
					style : 'margin : 5px 0 0 0 ; ' , 
					layout : 'column' ,   
					items : [{
						xtype : 'radio' , 
						width : 15 ,
						style : 'margin : 13px 0 0 0 ; ' , 
						name : 'cef[slider_positioningtab]' , 
						id : 'slider_positioningtab-Top' ,
						inputValue : 'Top' ,  
						columnWidth : 0.1 , 
						checked : true 
					},{
						xtype : 'displayfield' , 
						id : 'slider-positiontab-img1' ,
						value : '<img  src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/positiontab-left1.png" />' , 
						columnWidth : 0.2
					},{
						xtype : 'compositefield' ,  
						style : 'margin : 11px 0 0 0 ; ' ,   
						items : [{
							xtype : 'spinnerfield' ,
							
							width : 50 ,  
							allowDecimals: false,
							minValue: 0,
							name : 'cef[slider_positiontab_pos1]' , 
							id : 'slider_positiontab_pos1' , 
							decimalPrecision: 0,
							incrementValue: 1, 
							alternateIncrementValue: 1,
							accelerate: true , 
							value : 0
						},{
							xtype : 'displayfield' ,  
							value : 'pixels from'
						},{
							xtype : 'textfield' , 
							enableKeyEvents : true ,
							id : 'slider-positiontab-txt1' , 
							disabled : true ,
							flex : 1 , 
							value : 'Top' , 
							listeners : {
		                        'keypress' : function(textField ,e){
		                            if(e.keyCode == e.ENTER) {
		                                contentEditorCE.saveProcess(0) ;     
		                            }
		                        }
		                    }
						}] , 
						columnWidth : 0.7
					}]
				},{
					xtype : 'container' ,  
					style : 'margin : 5px 0 0 0 ; ' , 
					layout : 'column' ,   
					items : [{
						xtype : 'radio' , 
						width : 15 ,
						style : 'margin : 13px 0 0 0 ; ' , 
						name : 'cef[slider_positioningtab]' , 
						id : 'slider_positioningtab-Center' , 
						inputValue : 'Center' , 
						columnWidth : 0.1
					},{
						xtype : 'displayfield' , 
						id : 'slider-positiontab-img2' ,
						value : '<img   src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/positiontab-left2.png" />' , 
						columnWidth : 0.2
					},{
						xtype : 'compositefield' ,  
						style : 'margin : 11px 0 0 0 ; ' ,   
						items : [{
							xtype : 'spinnerfield' ,
							
							width : 50 ,  
							allowDecimals: false,
							minValue: 0,
							name : 'cef[slider_positiontab_pos2]' , 
							id : 'slider_positiontab_pos2' , 
							decimalPrecision: 0,
							incrementValue: 1, 
							alternateIncrementValue: 1,
							accelerate: true , 
							value : 0
						},{
							xtype : 'displayfield' ,  
							value : 'pixels from'
						},{
							xtype : 'textfield' ,  
							enableKeyEvents : true ,
							disabled : true ,
							flex : 1 , 
							id : 'slider-positiontab-txt2' ,
							value : 'Top' , 
							listeners : {
		                        'keypress' : function(textField ,e){
		                            if(e.keyCode == e.ENTER) {
		                                contentEditorCE.saveProcess(0) ;     
		                            }
		                        }
		                    }
						}] , 
						columnWidth : 0.7
					}]
				},{
					xtype : 'container' ,  
					style : 'margin : 5px 0 0 0 ; ' , 
					layout : 'column' ,   
					items : [{
						xtype : 'radio' , 
						width : 15 ,
						style : 'margin : 13px 0 0 0 ; ' , 
						name : 'cef[slider_positioningtab]' , 
						id : 'slider_positioningtab-Bottom' , 
						inputValue : 'Bottom' , 
						columnWidth : 0.1
					},{
						xtype : 'displayfield' , 
						id : 'slider-positiontab-img3' ,
						value : '<img   src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/positiontab-left3.png" />' , 
						columnWidth : 0.2
					},{
						xtype : 'compositefield' ,  
						style : 'margin : 11px 0 0 0 ; ' ,   
						items : [{
							xtype : 'spinnerfield' ,
							
							width : 50 ,  
							allowDecimals: false,
							minValue: 0,
							name : 'cef[slider_positiontab_pos3]' , 
							id : 'slider_positiontab_pos3' , 
							decimalPrecision: 0,
							incrementValue: 1, 
							alternateIncrementValue: 1,
							accelerate: true , 
							value : 0
						},{
							xtype : 'displayfield' ,  
							value : 'pixels from'
						},{
							xtype : 'textfield' ,  
							enableKeyEvents : true ,
							id : 'slider-positiontab-txt3' ,
							disabled : true ,
							flex : 1 , 
							value : 'Top' , 
							listeners : {
		                        'keypress' : function(textField ,e){
		                            if(e.keyCode == e.ENTER) {
		                                contentEditorCE.saveProcess(0) ;     
		                            }
		                        }
		                    }
						}] , 
						columnWidth : 0.7
					}] 
				}
				]
			},{
				xtype : 'panel' ,
				height : 114 ,  
				border : false ,   
				baseCls : 'sl-design-actions' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define the sliding speed for your Slider" />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Actions:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Direction of Slider:'
					},{
						xtype : 'textfield' , 
						enableKeyEvents : true ,
						width : 150 , 
						disabled : true , 
						style : 'color : rgb(239,125,0) ; font-weight : bold ;' , 
						name : 'cef[slider_direction]' , 
						id : 'slider_direction' , 
						value : 'Left to Right', 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					}]
				},{
                    xtype : 'displayfield' , 
                    value : 'Slider Speed: <span style="font-weight: bold;" id="slider-speed"></span>'
                },{
                    xtype : 'sliderfield' , 
                    name:  'cef[slider_speed]' ,  
                    id : 'slider_speed' , 
                    linkID : 'slider-speed' , 
                    objValue : 0 , 
                    listeners : { 
                        'afterrender' : function(field){
                            if( field.objValue != ''){
                                field.setValue(field.objValue) ; 
                            }else{
                            	field.setValue(50) ; 
                            } 
                            document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ; 
                            field.slider.on('change', function() { 
                                document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ;  
                                
                                if( field.getValue() == 0){
                                	field.setValue(1) ; 
                                }
                            });  
                        }
                    }
                }
				]
			}]
		}) ; 
		
		Ext.getCmp(contentEditorCE.id + '-form').add(main) ; 
		
		var main2 =  new Ext.Panel({
			height : 633 , 
			border : false ,   
			baseCls : 'sl-main-slider2' , 
			padding : 8 , 
			items : [{
				xtype : 'displayfield' , 
				value : '<img src="./typo3conf/ext/t3p_base/image/info.GIF" />' , 
				style : 'position: absolute; right : 10px ; top 15px ;'
			},{
				xtype : 'displayfield' , 
				value : '<b>2. Action</b> Tab:' , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'panel' ,
				height : 100 ,  
				border : false ,   
				baseCls : 'sl-tab-dimensions' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Enter a Width and Height for your Tab, or Choose \'Autosize\' for your Tab to automatically scale to fit your content."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Tab Dimensions:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;' ,
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						name : 'cef[tab_dimensions]' , 
						id : 'tab_dimensions-fixed' , 
						inputValue : 'fixed' , 
						style : 'margin : 3px 0 0 0 ;' ,
						width : 15 
					},{
						xtype : 'displayfield' , 
						value : 'Width'
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						enableKeyEvents : true ,
						name  : 'cef[tab_width]' , 
						value : 0, 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;x&nbsp;Height&nbsp;'
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						enableKeyEvents : true ,
						name  : 'cef[tab_height]' , 
						value : 0, 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : 'pixels'
					}]
				},{
					xtype : 'compositefield' ,  
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						name : 'cef[tab_dimensions]' ,
						id : 'tab_dimensions-auto' , 
						inputValue : 'auto' ,  
						style : 'margin : 3px 0 0 0 ;' ,
						width : 15 ,  
						checked : true 
					},{
						xtype : 'displayfield' , 
						value : 'Autosize Tab (to fit Tab text).'
					} ]
				}
				]
			},{
				xtype : 'panel' ,
				height : 106 ,  
				border : false ,   
				baseCls : 'sl-tab-content' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Create or Add the Text to appear on your Tab."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Tab Content:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'container' , 
					style : 'margin : 15px 0 0 0' ,
                    layout : 'column' , 
                    items : [{
                    	xtype : 'displayfield' , 
                    	columnWidth : 0.15
                    },{
                    	xtype : 'container' , 
                    	columnWidth : 0.7 , 
                    	items : [{
                    		xtype : 'button' ,  
							width : '100%' ,  
							text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create / Edit Tab Text</span> <div class="toolbar-btn"></div>' , 
							handler : function(){
								//---- Open toolbar text ; 
								me.openToolbalTextEditor(Ext.getCmp('tab_text').getValue()) ; 
							}
                    	},{
                    		xtype : 'compositefield' , 
                    		style : 'margin : 10px 0 0 0' ,
                    		items : [{
                    			xtype : 'displayfield' , 
                    			value : 'Tab text:'
                    		},{
                    			xtype : 'textfield' , 
                    			enableKeyEvents : true ,
                    			readOnly : true , 
                    			name : 'cef[tab_text]' , 
                    			id : 'tab_text' ,  
                    			style : 'color : black ; font-family: \'PT Sans\';font-size: 12px ;' , 
                    			value : 'My Tab' , 
                    			flex : 1 , 
								listeners : {
			                        'keypress' : function(textField ,e){
			                            if(e.keyCode == e.ENTER) {
			                                contentEditorCE.saveProcess(0) ;     
			                            }
			                        }
			                    }
                    		},{
                    			xtype : 'button' , 
                    			text : '<img src="./typo3conf/ext/t3p_base/image/edit2.png" height="16px" />' ,    
    							width : 22 , 
    							handler : function(){
    								me.openToolbalTextEditor(Ext.getCmp('tab_text').getValue()) ; 
    							}
                    		}]
                    	}]
                    },{
                    	xtype : 'displayfield' , 
                    	columnWidth : 0.15
                    }]
				}
				]
				
			},{
				xtype : 'panel' ,
				height : 215 ,  
				border : false ,   
				baseCls : 'sl-tab-styling' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define the style and transparency for your Tab."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Tab Styling:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 10px 0 ; ' , 
					items : [{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        width : 185 ,
                        name: 'cef[tab_border]',
                        hiddenId : 'tab_border' , 
                        displayField:   'name',
                        valueField:     'value',   
                        emptyText : 'Select a Border' , 
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['None','None'],['solid', 'solid'],['dotted', 'dotted'],['double', 'double'],['dashed', 'dashed']]
                        }),
                        validateOnBlur : false , 
                        value : 'None'
					},{
						xtype : 'spinnerfield' ,     
                        allowDecimals: false,
                        minValue: 0,
                        name : 'cef[tab_border_position]' , 
                        id : 'tab_border_position' , 
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                    	flex : 1 , 
                        value : 0
					}]	
				},{
					xtype : 'compositefield' ,
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Border Colour:'
					} ,{
						xtype : 'colorfield' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						colorSelector:"mixer" ,    
						msgTarget: 'qtip' ,
						flex : 1 , 
						name : 'cef[tab_bordercolor]'  
					}]
				},{
					xtype : 'fileuploadfield' ,  
					name : 'cef[tab_backgroundimage]' , 
					id : 'tab_backgroundimage' , 
					width : '100%' , 
                    emptyText: 'Select a Background Image' ,
                    hideLabel : true ,   
                    buttonText: '' , 
                    buttonCfg: {
                        iconCls: 'upload-icon'
                    }  
				},{
					xtype : 'compositefield' , 
					style : 'margin : 5px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Background Colour'
					},{
						xtype : 'colorfield' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						colorSelector:"mixer" ,  
						msgTarget: 'qtip' ,  
						name : 'cef[tab_bgcolor]' , 
						flex : 1  
						
					}]
				},{
                    xtype : 'displayfield' , 
                    value : 'Toolbar Transparency: <span style="font-weight: bold;" id="tab-transparency"></span>'
                },{
                    xtype : 'sliderfield' , 
                    name:  'cef[tab_transparency]' , 
					id : 'tab_transparency' ,  
                    linkID : 'tab-transparency' , 
                    objValue : 0 , 
                    listeners : { 
                        'afterrender' : function(field){
                            if( field.objValue != ''){
                                field.setValue(field.objValue) ; 
                            }else{
                            	field.setValue(0) ; 
                            } 
                            document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ; 
                            field.slider.on('change', function() { 
                                document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ;  
                            });  
                        }
                    }
                }
				]
			},{
				xtype : 'panel' ,
				height : 68 ,  
				border : false ,   
				padding : '5px 10px 5px 10px' ,
				baseCls : 'dtb2-actions' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define a trigger method for your Tab."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Tab Actions:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px  0 0 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Toolbar Action:'
					},{
						xtype : 'radio' , 
						style : 'margin : 3px 0 0 0 ;' ,
						name : 'cef[tab_action]' , 
						id :  'tab_action-Click' , 
						inputValue : 'Click' , 
						width : 10 , 
						checked : true 
					},{
						xtype : 'displayfield' , 
						value : 'On click&nbsp;&nbsp;&nbsp;'
					},{
						xtype : 'radio' , 
						style : 'margin : 3px 0 0 0 ;' ,
						name : 'cef[tab_action]' , 
						id :  'tab_action-Hover' , 
						inputValue : 'Hover' , 
						width : 10
					},{
						xtype : 'displayfield' , 
						value : 'On hover'
					}]
				}]
			},{
				xtype : 'panel' ,
				height : 104 ,  
				border : false ,   
				padding : '5px 10px 5px 10px' ,
				baseCls : 'dtb2-advance' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Customize Tab and Slider appearance using HTML/CSS editor." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Advance Options:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'container' , 
					style : 'margin : 10px 0 0 0' ,
                    layout : 'column' , 
                    items : [{
                    	xtype : 'displayfield' , 
                    	columnWidth : 0.15
                    },{
                    	xtype : 'container' , 
                    	columnWidth : 0.7 , 
                    	items : [{
                    		xtype : 'button' ,  
							width : '100%' ,  
							text : '<span class="toolbar-label" style="color:  rgb(101,101,101) ;font-weight: bold;">Customize HTML code</span> <div class="toolbar-btn"></div>' , 
							handler : function(){
								var d = Ext.getCmp('custom_html').getValue() ; 
								if(d == ''){
									monoloop_base.showProgressbar('Loading custom template ...') ;
									Ext.Ajax.request({
										url: 'index.php/content/template/advancedSlider?eID=ml_cp'  , 
										method: 'GET',
										timeout: '30000', 
							            success: function(xhr) {   
											d = xhr.responseText ; 
											me.openCustomHtmlWindow(d) ; 
					 						Ext.MessageBox.hide();
										}, failure: function() {
							                Ext.MessageBox.hide();
										    Ext.MessageBox.alert('Error', '#1201' ) ; 
										}
									}) ; 
								}else{
									me.openCustomHtmlWindow(d) ; 
								}
								
							}
                    	}]
                    },{
                    	xtype : 'displayfield' , 
                    	columnWidth : 0.15
                    }]
				}]
			},{
				xtype : 'hidden'  , 
				name : 'cef[custom_html]' , 
				id : 'custom_html'
			}]
		});
		
		Ext.getCmp(contentEditorCE.id + '-form').add(main2) ; 
		
		// -----------------
		
	 
	} , 
	
	afterrender : function(data){ 
		
		Ext.getCmp(contentEditorCE.id + '-top-addin').add({
			xtype : 'compositefield' , 
			width : '100%' ,
			items : [{
				xtype : 'displayfield' , 
				value : '<b>1. Design </b> Slider:' , 
				width : 120 , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'displayfield' , 
				style : 'margin : 5px 0 0 0 ;' , 
				width : 20 , 
				value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/arrow.png" />'
			},{
				xtype : 'displayfield' , 
				value : '<b>2. Set</b> Toolbar:' , 
				width : 100 , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'button' , 
				style : 'margin : 0 0 5px 0' ,  
				width : 200 , 
				iconCls : 'newpage-icon' ,  
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create New Slider Content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					selectTab.showContentElementSelectorWindow(5,0) ;  
				}
			},{
				xtype : 'button' ,  
				width : 170 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add Content from List</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					predefinedCEWindow.openWindow() ; 
				}
			}]		
		}) ;
		
		Ext.getCmp('slider_position').setValue('Top') ;
 		
 		
 		for( var i = 0 ; i < data.content.length ; i++){
			var field = data.content[i] ; 
			if( field['type'] == 'ce'){
				continue ; 
			}
			
			if( field['value'] == '' || field['value'] == null){
				continue ; 
			}
			
			if( field['el'] == 'slider_dimensions' || field['el'] == 'slider_positioning_type' || field['el'] == 'tab_dimensions' || field['el'] == 'slider_positioningtab' || field['el'] == 'tab_action' ){ 
				//console.log(field['el']+ '-' + field['value']) ; 
				Ext.getCmp(field['el']+ '-' + field['value']).setValue(true) ;   
			}else{
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef['+field['el']+']').setValue(field['value']) ; 
			}
		}
	} , 
	
	posClick : function(posID){
		// remove all element class . 
		var me = this ; 
		Ext.get('slider-pos-Left').removeClass('active') ; 
		Ext.get('slider-pos-Right').removeClass('active') ; 
		Ext.get('slider-pos-Top').removeClass('active') ; 
		Ext.get('slider-pos-Bottom').removeClass('active') ; 
		
		
		Ext.get('slider-pos-' + posID).addClass('active') ;
		
		me.chagePos(posID) ; 
	} , 
	
	chagePos : function(posID){
		Ext.getCmp('slider-positiontab-img1').setValue('<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/positiontab-'+posID.toLowerCase()+'1.png" />') ; 
		Ext.getCmp('slider-positiontab-img2').setValue('<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/positiontab-'+posID.toLowerCase()+'2.png" />') ; 
		Ext.getCmp('slider-positiontab-img3').setValue('<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/positiontab-'+posID.toLowerCase()+'3.png" />') ; 
		 
		if( posID == 'Top'){
			Ext.getCmp('slider-positiontab-txt1').setValue('Left') ; 
			Ext.getCmp('slider-positiontab-txt2').setValue('Center') ; 
			Ext.getCmp('slider-positiontab-txt3').setValue('Right') ; 
			
			Ext.getCmp('slider_direction').setValue('Top to Bottom') ; 
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.loadData([['Left','Left'],['Center','Center'],['Right','Right']]) ; 
			if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue() != 'Right')
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').setValue('Left') ; 
		}else if(posID == 'Left'){
			Ext.getCmp('slider-positiontab-txt1').setValue('Top') ; 
			Ext.getCmp('slider-positiontab-txt2').setValue('Middle') ; 
			Ext.getCmp('slider-positiontab-txt3').setValue('Bottom') ; 
			
			Ext.getCmp('slider_direction').setValue('Left to Right') ; 
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.loadData([['Top','Top'],['Center','Center'],['Bottom','Bottom']]) ; 
			
			if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue() != 'Bottom')
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').setValue('Top') ; 
		}else if(posID == 'Right'){
			Ext.getCmp('slider-positiontab-txt1').setValue('Top') ; 
			Ext.getCmp('slider-positiontab-txt2').setValue('Middle') ; 
			Ext.getCmp('slider-positiontab-txt3').setValue('Bottom') ; 
			
			Ext.getCmp('slider_direction').setValue('Right to Left') ; 
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.loadData([['Top','Top'],['Center','Center'],['Bottom','Bottom']]) ; 
			
			if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue() != 'Bottom')
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').setValue('Top') ; 
		}else{
			Ext.getCmp('slider-positiontab-txt1').setValue('Left') ; 
			Ext.getCmp('slider-positiontab-txt2').setValue('Center') ; 
			Ext.getCmp('slider-positiontab-txt3').setValue('Right') ;  
			Ext.getCmp('slider_direction').setValue('Bottom to Top') ; 
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.loadData([['Left','Left'],['Center','Center'],['Right','Right']]) ; 
			
			if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue() != 'Right')
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').setValue('Left') ; 
		}
	} , 
	
	openToolbalTextEditor : function(data){ 
		var w = new Ext.Window({ 
            width:400,
            height : 300 ,  
            layout : 'fit' ,
            title : 'Toolbar text editor' ,  
            modal: true,
         	maximizable : false  , 
         	closable:true , 
            items : [{
                xtype : 'textarea' , 
                value :  data
            }] , 
            buttons : [{
                text : 'CANCEL' , 
                handler : function(){ 
                    w.close() ; 
                }
            },{
                text : 'OK' , 
                handler : function(){
                    var h = w.findByType('textarea') ; 
                    Ext.getCmp('tab_text').setValue(h[0].getValue()) ; 
                    w.close() ;
                }
            }]
        }) ; 
        
        w.show() ;
	} , 
	
	openCustomHtmlWindow : function(data){
		var w = new Ext.Window({ 
            width:500,
            height : 600 ,  
            layout : 'fit' ,
            title : 'Custom HTML editor' ,  
            modal: true,
         	maximizable : false  , 
         	closable:true , 
            items : [{
                xtype : 'textarea' , 
                value :  data
            }] , 
            buttons : [{
                text : 'CANCEL' , 
                handler : function(){ 
                    w.close() ; 
                }
            },{
                text : 'OK' , 
                handler : function(){
                    var h = w.findByType('textarea') ; 
                    Ext.getCmp('custom_html').setValue(h[0].getValue()) ; 
                    w.close() ;
                }
            }]
        }) ; 
        
        w.show() ;
	}
} ; 
return window.sliderTab ;  
//---	
}) ; 