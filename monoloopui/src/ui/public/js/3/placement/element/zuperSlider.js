define(function (require) {
// --- start 
window.zuperSlider = {
	id : 'zuper-slider' , 
	renderForm : function(data){
		var me = this ;
		var main = new Ext.Panel({
			height : 940 , 
			border : false ,   
			baseCls : 'zuper-wrapper' , 
			padding : 8 , 
			items : [{
				xtype : 'displayfield' , 
				value : '<img src="./typo3conf/ext/t3p_base/image/info.GIF" title="Configure the look and feel of your Slider by making selections below." />' , 
				style : 'position: absolute; right : 26px ; top 15px ;'
			},{
				xtype : 'displayfield' , 
				value : '<b>1. Configure </b>Slider Container:' , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ; padding : 0 0 5px 0;'
			},{
				xtype : 'panel' , 
				baseCls : 'zuper-panel' ,
				border : false ,  
				padding : '10px 10px 10px 10px' ,
				items : [{
					xtype : 'compositefield' ,  
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Design the size and style of your slider." />' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : ' Design Slider Container:' , 
						style : 'font-family: \'Arial\'; color:  rgb(20,65,138) ;font-weight: bold;font-size: 12px;'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;line-height:22px;' ,
				
					items : [{
						xtype : 'displayfield' , 
						value : 'Width:'
						
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						name : 'cef[zuper_container_width]' , 
						enableKeyEvents : true ,
						id : 'zuper_container_width' , 
						value : 0 , 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;x&nbsp;Height:&nbsp;'
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						enableKeyEvents : true ,
						name : 'cef[zuper_container_height]' , 
						id : 'zuper_container_height' , 
						value : 0, 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : 'pixels'
					}]
				},{
					xtype : 'compositefield' ,
					style : 'margin : 10px 0 0px 0 ;line-height:22px;' , 
					items : [{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        width : 110 ,
                        hiddenName : 'cef[zuper_container_border]',
                        hiddenId : 'zuper_container_border' , 
                        id : 'zuper_container_border_id' , 
                        displayField:   'name',
                        valueField:     'myid',   
                        emptyText : 'Select a Border' , 
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'myid' ,
                                'name'
                            ],
                            data: [['None','No Border'],['solid', 'Solid Border'],['dotted', 'Dotted Border'],['double', 'Double Border'],['dashed', 'Dashed Border']]
                        }),
                        validateOnBlur : false ,
                        value : 'None'
					},{
						xtype : 'spinnerfield' ,     
                        allowDecimals: false,
                        minValue: 0,
                        name : 'cef[zuper_container_border_width]' , 
                        id : 'zuper_container_border_width' , 
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                    	width : 40 ,
                        value : 0
					},{
						xtype : 'displayfield' , 
						value : 'px' 
					},{
						xtype : 'colorfield' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						colorSelector:"mixer" ,  
						msgTarget: 'qtip' ,  
						name : 'cef[zuper_container_bordercolor]'  ,
						flex : 1 
					}]
				},{
					xtype : 'compositefield' ,
					style : 'margin : 10px 0 10px 0 ;line-height:22px;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Background Colour'  
					},{
						xtype : 'colorfield' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						colorSelector:"mixer" ,  
						msgTarget: 'qtip' ,  
						name : 'cef[zuper_container_bgcolor]'  ,
						flex : 1 
					}]
				},{
					xtype : 'fileuploadfield' ,  
					name : 'cef[zuper_container_backgroundimage]' , 
					id : 'zuper_container_backgroundimage' , 
					width : '100%' , 
                    emptyText: 'Select a Background Image' ,
                    hideLabel : true ,   
                    buttonText: '' , 
                    buttonCfg: {
                        iconCls: 'upload-icon'
                    }  
				}]
			},{
				xtype : 'panel' , 
				baseCls : 'zuper-panel' ,
				border : false ,    
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'compositefield' , 
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Select arrows and a navigation theme for your slider."/>' , 
						width : 18
					},{
						xtype : 'displayfield' , 
						value : ' Select Navigation Theme for Slider:' , 
						style : 'font-family: \'Arial\'; color:  rgb(20,65,138) ;font-weight: bold;font-size: 12px;'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '&nbsp;' , 
						width : 18
					},{
						xtype : 'button' , 
						style : 'margin : 0 ; padding : 0' , 
						flex : 1 ,
						text : '<span class="toolbar-label" style="color:  rgb(201,126,10) ;font-weight: bold;">Select a Navigation Theme from List</span> <div class="toolbar-btn"></div>' , 
						handler : function(){
							zuperSlider_navtheme.show() ; 
						}
					},{
						xtype : 'hidden' , 
						name : 'cef[navigation_themes]' , 
						id : 'navigation_themes' , 
						value :  0
					}]
				},{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/navigation.png" title="Select a vertical position for slider arrows."/>'   
				}]
			},{
				xtype : 'panel' , 
				baseCls : 'zuper-panel' ,
				border : false ,    
				padding : '5px 10px 10px 10px' ,
				items : [{
					xtype : 'compositefield' , 
					items : [{
						xtype : 'checkbox' , 
						name : 'cef[zuper_enable_arrow]' ,
						id : 'zuper_enable_arrow' , 
						checked : true ,
						style : 'margin : 3px 0 0 0' , 
						width : 18 , 
						listeners : {
							'check' : function(chk , newV ){
								 me.enableNavigationArrow(newV) ; 
							}
						}
					},{
						xtype : 'displayfield' , 
						value : ' Positioning Navigation Arrows:' , 
						style : 'font-family: \'Arial\'; color:  rgb(20,65,138) ;font-weight: bold;font-size: 12px;'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 0px 0 0 0 ;line-height:22px;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Select a vertical position for slider arrows." />' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Position Arrows Vertical:' , 
						width : 150
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 ,
                        name: 'cef[zuper_pos_arrow_vertival]',
                        hiddenId : 'zuper_pos_arrow_vertival' , 
                        id : 'zuper_pos_arrow_vertival_id' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['top','top'],['middle', 'middle'],['bottom', 'bottom']]
                        }),
                        validateOnBlur : false ,
                        value : 'middle'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;line-height:22px;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Position arrows inside or outside of this slider?"/>' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Position Arrows Horizontal:' , 
						width : 150
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 ,
                        name: 'cef[zuper_pos_arrow_horizontal]',
                        hiddenId : 'zuper_pos_arrow_horizontal' , 
                        id : 'zuper_pos_arrow_horizontal_id' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['inside','inside'],['outside', 'outside']]
                        }),
                        validateOnBlur : false ,
                        value : 'inside'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;line-height:22px;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="How will Users activate slide arrows?" />' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Activate Method for Slider:' , 
						width : 150
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 ,
                        name: 'cef[zuper_pos_arrow_method]',
                        hiddenId : 'zuper_pos_arrow_method' , 
                        id : 'zuper_pos_arrow_method_id' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['click','click'],['hover', 'hover']]
                        }),
                        validateOnBlur : false ,
                        value : 'click'
					}]
				}]
			},{
				xtype : 'panel' , 
				baseCls : 'zuper-panel' ,
				border : false ,    
				padding : '5px 10px 10px 10px' ,
				items : [{
					xtype : 'compositefield' , 
					items : [{
						xtype : 'checkbox' , 
						name : 'cef[zuper_enable_nav]' ,
						id : 'zuper_enable_nav' , 
						checked : true ,
						style : 'margin : 3px 0 0 0' , 
						width : 18 , 
						listeners : {
							'check' : function(chk , newV){
								me.enableAlternativeNavigation(newV) ; 
							}
						}
					},{
						xtype : 'displayfield' , 
						value : ' Positioning Alternative Navigation:' , 
						style : 'font-family: \'Arial\'; color:  rgb(20,65,138) ;font-weight: bold;font-size: 12px;'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 0px 0 0 0 ;line-height:22px;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Select a vertical position for navigation." />' ,
						style : 'margin : 4px 0 0 0' ,   
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Position Nav. Vertical:' ,  
						width : 150
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 ,
                        name: 'cef[zuper_pos_nav_vertival]',
                        hiddenId : 'zuper_pos_nav_vertival' , 
                        id : 'zuper_pos_nav_vertival_id' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['top','top'],['middle', 'middle'],['bottom', 'bottom']]
                        }),
                        validateOnBlur : false ,
                        value : 'middle'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;line-height:22px;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Select a horizontal position for navigation?" />' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Position Nav. Horizontal:' , 
						width : 150
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 ,
                        name: 'cef[zuper_pos_nav_horizontal]',
                        hiddenId : 'zuper_pos_nav_horizontal' , 
                        id : 'zuper_pos_nav_horizontal_id' ,  
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['left','left'],['center', 'center'],['right','right']]
                        }),
                        validateOnBlur : false ,
                        value : 'center'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;line-height:22px;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="How will Users activate navigation?" />' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Activate Method for Slider:' , 
						width : 150
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 ,
                        name: 'cef[zuper_pos_nav_method]',
                        hiddenId : 'zuper_pos_nav_method' , 
                        id : 'zuper_pos_nav_method_id' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['click','click'],['hover', 'hover']]
                        }),
                        validateOnBlur : false ,
                        value : 'click'
					}]
				}]
			},{
				xtype : 'displayfield' , 
				value : '<img src="./typo3conf/ext/t3p_base/image/info.GIF" title="Configure slide actions for your Slider by making selections below."/>' , 
				style : 'position: absolute; right : 26px ; top :  662px ;'
			},{
				xtype : 'displayfield' , 
				value : '<b>2. Set </b>Slider Actions:' , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ; padding : 15px 0 5px 0 ;'
			},{
				xtype : 'panel' , 
				baseCls : 'zuper-panel2' ,
				border : false ,    
				padding : '10px 10px 10px 10px' ,
				items : [{
					xtype : 'compositefield' , 
					style : 'line-height:22px;' , 
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="The maximum number of slides this Slider will display."/>' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : ' Maximum No. of Slides to Navigate: ' , 
						style : 'font-family: \'Arial\'; color:  rgb(20,65,138) ;font-weight: bold;font-size: 12px;'
					},{
						xtype : 'spinnerfield' ,     
                        allowDecimals: false,
                        minValue: 0,
                        name : 'cef[zuper_max_slide]' , 
                        id : 'zuper_max_slide' , 
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                    	flex : 1 , 
                        value : 0
					}]
				}]				
			},{
				xtype : 'panel' , 
				baseCls : 'zuper-panel' ,
				border : false ,    
				padding : '10px 10px 10px 10px' ,
				items : [{
					xtype : 'compositefield' , 
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Set the speed between slides (default is 60%)"/>' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Slider Speed: <span style="font-weight: bold;color:  rgb(20,65,138) ; font-size:16px " id="slider-speed"></span>'   
					}]
				},{
					xtype : 'compositefield' , 
					items : [{
						xtype : 'displayfield' , 
						value : '&nbsp;' , 
						width : 18
					},{
						xtype : 'sliderfield' , 
	                    name:  'cef[slider_speed]' ,  
	                    id : 'slider_speed' , 
	                    linkID : 'slider-speed' , 
	                    flex : 1 , 
	                    objValue : 0 , 
	                    listeners : { 
	                        'afterrender' : function(field){
	                            if( field.objValue != ''){
	                                field.setValue(field.objValue) ; 
	                            }else{
	                            	field.setValue(50) ; 
	                            } 
	                            document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ; 
	                            field.slider.on('change', function() { 
	                                document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ;  
	                                
	                                if( field.getValue() == 0){
	                                	field.setValue(1) ; 
	                                }
	                            });  
	                        }
	                    }
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Automatically start Slider when page loads."/>' , 
						width : 18
					},{
						xtype : 'checkbox' , 
						stype : 'margin : 3px 0 0 0; ' , 
						width : 18 , 
						checked : true ,
						name : 'cef[enable_autostart]' , 
						id : 'enable_autostart'
					},{
						xtype : 'displayfield' , 
						value : 'Auto Start Slider'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 0 0 0 0 ; line-height:22px;' , 
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Set time delay between slides (default is 2 secs)." />' ,
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Time Delay between Slides:'
					},{
						xtype : 'spinnerfield' ,     
                        allowDecimals: false,
                        minValue: 0,
                        name : 'cef[time_delay]' , 
                        id : 'time_delay' , 
                    	decimalPrecision: 1,
                    	incrementValue: 0.1,
                    	alternateIncrementValue: 0.1,
                    	accelerate: true , 
                    	flex : 1 , 
                        value : 2.0
					},{
						xtype : 'displayfield' , 
						value : 'secs'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ; line-height:22px;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Set direction for slide transitions." />' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Transition style:' , 
						width : 100
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 ,
                        name: 'cef[transition_style]',
                        hiddenId : 'transition_style' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['horisontal','horisontal'],['vertical', 'vertical']]
                        }),
                        validateOnBlur : false ,
                        value : 'horisontal'
					}]
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ; line-height:22px;' ,
					items : [{
						xtype : 'displayfield' , 
						value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Select an effect for transitions." />' , 
						style : 'margin : 4px 0 0 0' ,  
						width : 18
					},{
						xtype : 'displayfield' , 
						value : 'Transition effect:' , 
						width : 100
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 ,
                        hiddenName: 'cef[transition_effect]',
                        hiddenId : 'transition_effect' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['','None'],['linear','Linear'],['easeInElastic','Ease in elastic'],['easeOutElastic','Ease out elastic'],['easeInOutElastic','Ease in-out elastic'],['easeInBack','Ease in back'],['easeOutBack' , 'Ease out back'],['easeInOutBack' , 'Ease in-out back'],['easeInBounce','Ease in bounce'],['easeOutBounce','Ease out bounce'],['easeInOutBounce','Ease in-out bounce'],['easeInCirc','Ease in circ'],['easeOutCirc','Ease out circ'],['easeInOutCirc','Ease in-out circ'],['easeInQuad','Ease in quad'],['easeOutQuad','Ease out quad'],['easeInOutQuad','Ease in-out quad'],['easeInCubic' ,'Ease in cubic'] ,['easeOutCubic' , 'Ease out cubic' ] , ['easeInOutCubic','Ease in-out cubic'] , ['easeInQuart' , 'Ease in quart'] , ['easeOutQuart' , 'Ease out quart'] , ['easeInOutQuart' , 'Ease in-out quart'] , ['easeInQuint' , 'Ease in quint'] , ['easeOutQuint' , 'Ease out quint'] , ['easeInOutQuint' , 'Ease in-out quint'] , ['easeInSine' , 'Ease in sine'] , ['easeOutSine' , 'Ease out sine'] , ['easeInOutSine' , 'Ease in-out sine'] , ['easeInExpo' , 'Ease in expo'] , ['easeOutExpo' , 'Ease out expo'] , ['easeInOutExpo' , 'Ease in-out expo'] ]
                        }),
                        validateOnBlur : false ,
                        value : ''
					}]
				}]		
			}] 	
		}) ; 
		
		Ext.getCmp(contentEditorCE.id + '-form').add(main) ; 
		
		Ext.getCmp(contentEditorCE.id + '-top-addin').add({
			xtype : 'compositefield' , 
			width : '100%' ,
			items : [{
				xtype : 'displayfield' , 
				value : '<b>1. Configure </b> Slider' , 
				width : 138 , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'displayfield' , 
				style : 'margin : 5px 0 0 0 ;' , 
				width : 32 , 
				value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/arrow.png" />'
			},{
				xtype : 'displayfield' , 
				value : '<b>2. Set </b> Actions' , 
				width : 130 , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'button' , 
				style : 'margin : 0 0 5px 0' ,  
				iconCls : 'newpage-icon' ,  
				width : 200 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create New Slider Content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					selectTab.showContentElementSelectorWindow(5,0) ;  
				}
			},{
				xtype : 'button' ,  
				width : 170 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add pre-defined content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					predefinedCEWindow.openWindow() ; 
				}
			}]		
		}) ;
	} , 
	
	afterrender : function(data){
		var me = this ; 
		for( var i = 0 ; i < data.content.length ; i++){
			var field = data.content[i] ; 
			if( field['type'] == 'ce'){
				continue ; 
			}
			
			if( field['el'] == 'zuper_enable_nav' ||  field['el'] == 'zuper_enable_arrow' || field['el'] == 'enable_autostart' ){   
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef['+field['el']+']').setValue(field['value']) ; 
				continue ; 
			} 
			
			if( field['value'] == '' || field['value'] == null){
				continue ; 
			}
			
			
			
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef['+field['el']+']').setValue(field['value']) ; 
			
		}
	} , 
	
	enableNavigationArrow : function(checked){
		Ext.getCmp('zuper_pos_arrow_vertival_id').setDisabled(!checked) ;
		Ext.getCmp('zuper_pos_arrow_horizontal_id').setDisabled(!checked) ;
		Ext.getCmp('zuper_pos_arrow_method_id').setDisabled(!checked) ;
	} ,
	
	enableAlternativeNavigation : function(checked){
		Ext.getCmp('zuper_pos_nav_vertival_id').setDisabled(!checked) ;
		Ext.getCmp('zuper_pos_nav_horizontal_id').setDisabled(!checked) ;
		Ext.getCmp('zuper_pos_nav_method_id').setDisabled(!checked) ;
	}
} ; 


var zuperSlider_navtheme = {
	show : function(){
		var w = new Ext.Window({
            width:400,
            height : 380 , 
            id : 'zuperSlider_navtheme-window' , 
            bodyStyle : 'background-color : white ;' , 
            title : '<img style="vertical-align: middle;" src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/grey-info.png" title="Select a navigation theme for your Slider." />&nbsp;&nbsp;Select a Navigation Theme' , 
            modal: true,
         	maximizable : false,
         	autoScroll : true , 
         	defaults : {
         		anchor : '100%' , 
         		border : false , 
         		height : 124 , 
         		style : 'border-bottom : 1px rgb(217,217,217) solid ;'
         	} , 
         	
         	items : [{
         		xtype : 'panel' , 
         		
         		defaults : {
         			anchor : '100%' 
         		} , 
         		items : [{
         			xtype : 'compositefield' , 
         			items : [{
         				xtype : 'displayfield' , 
         				value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/nav001.png"/>' , 
         				width : '320'
         			},{
         				xtype : 'displayfield' ,  
         				style : 'margin : 50px 0 0 0 ; font-family: \'PT Sans\';font-size: 16px ;color: rgb(20,65,138) ; font-weight : bold ;' , 
         				value : '1.'
         			},{
         				xtype : 'radio' , 
						name : 'zuper_nav_radio' , 
						id : 'zuper_nav_radio-1' ,
						inputValue :1  , 
						style : 'margin : 55px 0 0 0 ;' ,
						width : 15 
         			}]
         		}] , 
         		listeners : {
         			'afterrender' : function(p){
         				p.body.on('mouseover', function(event){
         					p.body.setStyle('background-color', 'rgb(229,229,229)');  
       					});
       					
       					p.body.on('mouseout', function(event){
         					p.body.setStyle('background-color', 'white');  
       					});
         			}
         		}
         	},{
         		xtype : 'panel' , 
         		
         		defaults : {
         			anchor : '100%' 
         		} , 
         		items : [{
         			xtype : 'compositefield' , 
         			items : [{
         				xtype : 'displayfield' , 
         				value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/nav002.png"/>' , 
         				width : '320'
         			},{
         				xtype : 'displayfield' ,  
         				style : 'margin : 50px 0 0 0 ; font-family: \'PT Sans\';font-size: 16px ;color: rgb(20,65,138) ; font-weight : bold ;' , 
         				value : '2.'
         			},{
         				xtype : 'radio' , 
						name : 'zuper_nav_radio' , 
						id : 'zuper_nav_radio-2' ,
						inputValue :2  , 
						style : 'margin : 55px 0 0 0 ;' ,
						width : 15 
         			}]
         		}] , 
         		listeners : {
         			'afterrender' : function(p){
         				p.body.on('mouseover', function(event){
         					p.body.setStyle('background-color', 'rgb(229,229,229)');  
       					});
       					
       					p.body.on('mouseout', function(event){
         					p.body.setStyle('background-color', 'white');  
       					});
         			}
         		}
         	},{
         		xtype : 'panel' , 
         		
         		defaults : {
         			anchor : '100%' 
         		} , 
         		items : [{
         			xtype : 'compositefield' , 
         			items : [{
         				xtype : 'displayfield' , 
         				value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/nav003.png"/>' , 
         				width : '320'
         			},{
         				xtype : 'displayfield' ,  
         				style : 'margin : 50px 0 0 0 ; font-family: \'PT Sans\';font-size: 16px ;color: rgb(20,65,138) ; font-weight : bold ;' , 
         				value : '3.'
         			},{
         				xtype : 'radio' , 
						name : 'zuper_nav_radio' , 
						id : 'zuper_nav_radio-3' ,
						inputValue :3  , 
						style : 'margin : 55px 0 0 0 ;' ,
						width : 15 
         			}]
         		}] , 
         		listeners : {
         			'afterrender' : function(p){
         				p.body.on('mouseover', function(event){
         					p.body.setStyle('background-color', 'rgb(229,229,229)');  
       					});
       					
       					p.body.on('mouseout', function(event){
         					p.body.setStyle('background-color', 'white');  
       					});
         			}
         		}
         	},{
         		xtype : 'panel' , 
         		
         		defaults : {
         			anchor : '100%' 
         		} , 
         		items : [{
         			xtype : 'compositefield' , 
         			items : [{
         				xtype : 'displayfield' , 
         				value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/zuper/nav004.png"/>' , 
         				width : '320'
         			},{
         				xtype : 'displayfield' ,  
         				style : 'margin : 50px 0 0 0 ; font-family: \'PT Sans\';font-size: 16px ;color: rgb(20,65,138) ; font-weight : bold ;' , 
         				value : '4.'
         			},{
         				xtype : 'radio' , 
						name : 'zuper_nav_radio' , 
						id : 'zuper_nav_radio-4' ,
						inputValue :4  , 
						style : 'margin : 55px 0 0 0 ;' ,
						width : 15 
         			}]
         		}] , 
         		listeners : {
         			'afterrender' : function(p){
         				p.body.on('mouseover', function(event){
         					p.body.setStyle('background-color', 'rgb(229,229,229)');  
       					});
       					
       					p.body.on('mouseout', function(event){
         					p.body.setStyle('background-color', 'white');  
       					});
         			}
         		}
         	}  ] , 
         	buttons : [{
                text : 'LOAD SELECTED THEME' , 
                handler : function(){  
                	//Ext.getCmp('navigation_themes').setValue() ;
                	var val = 0 ; 
                	for( var i = 1 ; i <= 4 ; i++){
                		var v = Ext.getCmp('zuper_nav_radio-' + i).getValue() ; 
                		if( v == true){
                			val = i ;
                			break ; 
                		}
                	} 
                  	Ext.getCmp('navigation_themes').setValue(val) ; 
                  	w.close() ; 
                  	contentEditorCE.saveProcess(1) ; 
                }
            },{
            	text : 'CLOSE' , 
            	handler : function(){
            		w.close() ; 
            	}
            }]  , 
            listeners : {
            	'afterrender' : function(){
            		var n = Ext.getCmp('navigation_themes').getValue() ; 
            		if( n > 0){
            			Ext.getCmp('zuper_nav_radio-' + n).setValue(true) ; 
            		}
            	}
            }
  		}) ; 
  		w.show() ; 
	}
}

return window.zuperSlider ; 
//---	
}) ; 