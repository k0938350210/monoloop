define(function (require) {
// --- start
window.dynamicContainer = {
	id : 'dynamic-container' ,
	
	manualStepLayout  : '' , 
	
	renderForm : function(data){
		var me = this ; 
		
		var main = new Ext.Panel({
			height : 175 , 
			border : false ,   
			baseCls : 'dtb-main' , 
			padding : 8 , 
			items : [{
				xtype : 'displayfield' , 
				value : '<img src="./typo3conf/ext/t3p_base/image/info.GIF" />' , 
				style : 'position: absolute; right : 26px ; top 15px ;'
			},{
				xtype : 'displayfield' , 
				value : '<b>1. Define </b> Dynamic Properties:' , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'panel' ,
				height : 129 ,  
				baseCls : 'dtb-dimensions' ,
				border : false ,    
				padding : '15px 10px 5px 10px' ,
				items : [{
                    xtype : 'container' , 
                    layout : 'column' ,  
                    items : [{
                        xtype : 'displayfield' , 
                        value : 'Maximum number of elements' , 
                        columnWidth : 0.6 , 
                    },{
                        xtype : 'spinnerfield' , 
                        allowBlank: false  ,  
                        name: 'cef[maxelements]', 
                        id : me.id + '-maxelements' ,    
                        validateOnBlur : false , 
                        columnWidth : 0.2  , 
                        enableKeyEvents : true , 
                        value : 1 , 
                        allowDecimals: false,
                        minValue: 0,
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                        listeners : {
                            'keyup' : function(textbox , e){     
                            } , 
                            'spin' : function(textbox){ 
                            }  
                        }
                    }]
                },{
                    xtype : 'container' , 
                    layout : 'column' ,  
                    items : [{
                        xtype : 'displayfield' , 
                        value : 'Minimun number of elements' , 
                        columnWidth : 0.6 , 
                    },{
                        xtype : 'spinnerfield' , 
                        allowBlank: false  ,  
                        name: 'cef[minelements]', 
                        id : me.id + '-minelements' ,    
                        validateOnBlur : false , 
                        columnWidth : 0.2  , 
                        enableKeyEvents : true , 
                        value : 1 , 
                        allowDecimals: false,
                        minValue: 0,
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                        listeners : {
                            'keyup' : function(textbox , e){     
                            } , 
                            'spin' : function(textbox){ 
                            }  
                        }
                    }]
                }/*,{
                	xtype : 'container' , 
                	layout : 'column' , 
                	items : [{
                		xtype : 'displayfield' , 
                		value : 'Only show if this number of elements are selected' , 
                		columnWidth : 0.95
                	},{
                		xtype : 'checkbox' , 
                		name: 'cef[visibleifmatched]', 
                		value : 1  , 
                		height : 18 , 
                		columnWidth : 0.05 
                		
                	}]
                }*/,{
					xtype : 'container' , 
					style : 'padding : 10px 15px 0 15px ;' , 
					defaults : {
						anchor : '100%'	
					} ,
					items : [{
						xtype : 'button' , 
						style : 'margin : 0 0 5px 0' , 
						width : '100%' , 
						iconCls : 'newpage-icon' ,  
						text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create new content</span> <div class="toolbar-btn"></div>' , 
						handler : function(){
							selectTab.showContentElementSelectorWindow(5,0) ;  
						}
					},{
						xtype : 'button' , 
						width : '100%' , 
						text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add pre-defined content</span> <div class="toolbar-btn"></div>' , 
						handler : function(){
							predefinedCEWindow.openWindow() ; 
						}
					}]
				}]
			}]
		}); 
		
		Ext.getCmp(contentEditorCE.id + '-form').add(main) ;
		
		var main2 = new Ext.Panel({
			width : 299 ,  
            height : 405 , 
            border : false ,
            cls : 'asset' , 
            style : 'margin-top : 4px ; padding: 5px; position : relative ; ' , 
            baseCls : 'asset-stage-4' , 
            items : [{
                xtype : 'displayfield' , 
                value : '<span class="topic"><b>2. Step & Repeat</b></span>&nbsp;<img   src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png"  />'
            },{
                xtype : 'container' , 
                style : 'padding : 20px 20px 0px 20px ;' , 
                items : [{
                    xtype : 'radio' , 
                    id : me.id + '-asset_repeatmode-define'  , 
                    name : 'cef[asset_repeatmode]' , 
                    inputValue : 'define' , 
                    checked : true , 
                    boxLabel : 'Define a layout to Step & Repeat' , 
                    onClick: function(e){
                    	if( Ext.getCmp(me.id + '-step4-vertical').disabled){
                    		 // me.manualStepLayout == ''
                    		 Ext.Msg.show({
							   title:'Warning ',
							   msg: 'Your change will be lost. Continue?',
							   buttons: Ext.Msg.YESNO,
							   fn : function(btn){
							   		if( btn == 'yes'){
							   			me.manualStepLayout = '' ; 
							   			me.setDimManaulRepeat(true) ; 
                        				me.setDimStepRepeat(false) ;
							   			
							   		}else{
							   			Ext.getCmp(me.id + '-repeat-manual').setValue(true) ; 
							   		}
							   }
							});
                    	} 
                    }
                },{
                    xtype : 'container' , 
                    layout : 'column' , 
                    style : 'margin : 10px 0 0 0 ;' , 
                    items : [{
                        xtype : 'displayfield' , 
                        value : '&nbsp;' , 
                        columnWidth : 0.05
                    },{
                        xtype : 'container' , 
                        items : [{
                            xtype : 'displayfield' , 
                            value : '<div id="grid">&nbsp;</div>'
                        },{
                            xtype : 'displayfield' , 
                            style : 'margin : 2px 0 0 0px ;' , 
                            value : '<p align="center"><b>Spacing</b><br>between elements</p>'
                        },{
                            xtype : 'spinnerfield' ,
                            
                            id : me.id + '-step4-spacing' , 
                            name : 'cef[repeat_spacing]' , 
                            width : 100 , 
                            allowDecimals: false,
                            minValue: 0,
                        	decimalPrecision: 0,
                        	incrementValue: 1,
                        	alternateIncrementValue: 1,
                        	accelerate: true , 
                            value : 0
                            
                        }] , 
                        columnWidth : 0.425
                    },{
                        xtype : 'displayfield' , 
                        value : '&nbsp;' , 
                        columnWidth : 0.1
                    },{
                        xtype : 'container' , 
                        items : [{
                            xtype : 'displayfield' , 
                            value : '<span class="y"><b>Horizontal</b></span><br>No. of Elements:<br>(Left to right)'
                        },{
                            xtype : 'spinnerfield' ,
                            width : 80 , 
                            id : me.id + '-step4-horizontal' , 
                            name : 'cef[repeat_h]' , 
                            allowDecimals: false,
                            minValue: 0,
                        	decimalPrecision: 0,
                        	incrementValue: 1,
                        	alternateIncrementValue: 1,
                        	accelerate: true , 
                            value : 1, 
                            listeners : {
                                'keyup' : function(textbox , e){ 
                                    //me.step4calcurateRowColumn() ;    
                                } , 
                                'spin' : function(textbox){ 
                                    //me.step4calcurateRowColumn() ;  
                                }  
                            }
                        },{
                            xtype : 'displayfield' , 
                            value : '<br><span class="y"><b>Vertical</b></span><br>Rows of Elements'
                        },{
                            xtype : 'spinnerfield' ,
                            width : 80 , 
                            id : me.id + '-step4-vertical' ,
                            name : 'cef[repeat_v]' , 
                            allowDecimals: false,
                            minValue: 0,
                        	decimalPrecision: 0,
                        	incrementValue: 1,
                        	alternateIncrementValue: 1,
                        	accelerate: true , 
                            value : 1 , 
                            listeners : {
                                'keyup' : function(textbox , e){ 
                                    //me.step4calcurateRowColumn2() ;    
                                } , 
                                'spin' : function(textbox){ 
                                    //me.step4calcurateRowColumn2() ;  
                                }  
                            }
                        }] , 
                        columnWidth : 0.36
                    },{
                        xtype : 'displayfield' , 
                        value : '&nbsp;' , 
                        columnWidth : 0.065
                    }]
                },{
                    xtype : 'displayfield' , 
                    value : '<br><hr>'
                },{
                    xtype : 'radio' , 
                    id : me.id + '-asset_repeatmode-manual'  , 
                    name : 'cef[asset_repeatmode]' , 
                    inputValue : 'manual' , 
                    boxLabel : 'Define your own Step & Repeat layout' ,
                    onClick : function(e){
                    	if( Ext.getCmp(me.id + '-manaulrepeat-btn').disabled){
                    		me.manualStepLayout = '' ; 
                    		me.setDimManaulRepeat(false) ; 
                        	me.setDimStepRepeat(true) ;
                   		}
                    }
                },{
                    xtype : 'container' , 
                    layout : 'column' , 
                    items : [{
                        xtype : 'displayfield' , 
                        value : '&nbsp;' , 
                        columnWidth : 0.05 
                    },{
                        xtype : 'container' , 
                        columnWidth : 0.90 , 
                        items : [{
                            xtype : 'displayfield' , 
                            value : '<br><span class="tiny-topic">Advance option - requires knowledge of HTML</span>'
                        },{
                            xtype : 'button' , 
                            width : '100%' , 
                            text : '<b>USE MONOLOOP HTML EDITOR</b>' , 
                            id : me.id + '-manaulrepeat-btn' ,
                            handler : function(){
                                //var content = document.getElementById("asset-mapped-view").contentWindow.assets_mapped.getAssetHTML() ; 
                                //me.manualStepLayout =  '' ; 
                                var data = Ext.getCmp(contentEditorCE.id + '-form').getForm().getFieldValues() ; 
                                data['cef[repeat_spacing]'] = Ext.getCmp( me.id + '-step4-spacing').getValue() ; 
                                data['cef[repeat_v]'] = Ext.getCmp(me.id + '-step4-vertical').getValue() ; 
                                data['cef[repeat_h]'] =  Ext.getCmp( me.id + '-step4-horizontal').getValue() ; 
                                
                                me.manualStepLayout = Ext.getCmp(me.id + '-manual_layout').getValue() ; 
                                
                                if( me.manualStepLayout == '' || me.manualStepLayout == null || me.manualStepLayout == undefined){
                                    // Load default data ; 
                                    monoloop_base.showProgressbar('Loading default data...') ;   
                                    Ext.Ajax.request({
                            			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=dynamiccontainer/getDefaultRepeat'  , 
                                        //url : 'testxml.php' , 
                            			method: 'POST',
                            			params: data   ,
                            			timeout: '30000',
                            			// process the response object to add it to the TabPanel:
                            			success: function(xhr) {
                            			    var jsonRoot = Ext.util.JSON.decode(xhr.responseText) ;
                                            me.manualStepLayout = jsonRoot.content ; 
                             				Ext.MessageBox.hide(); 
                                            me.openManaulRepeatWindow() ; 
                            			},
                            			failure: function() {
                            				Ext.MessageBox.hide();
                            				Ext.MessageBox.alert('Error', '#322' ) ; 
                            			}
                            		});	
                                }else{
                                    me.openManaulRepeatWindow() ;
                                }
                        
                                
                            }
                        }]
                    },{
                        xtype : 'displayfield' , 
                        value : '&nbsp;' , 
                        columnWidth : 0.05
                    }
                    ]
                },{
                	xtype : 'hidden' , 
                	id : me.id + '-manual_layout' , 
                	name : 'cef[manual_layout]'
                }]
            }]
       	}) ; 
		 
		
		Ext.getCmp(contentEditorCE.id + '-form').add(main2) ;
		
		Ext.getCmp(contentEditorCE.id + '-top-addin').add({
			xtype : 'compositefield' , 
			width : '100%' ,
			items : [/*{
				xtype : 'displayfield' , 
				value : '<b>1. Define </b> Dynamic Properties' , 
				width : 205 , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'displayfield' , 
				style : 'margin : 5px 0 0 0 ;' , 
				width : 20 , 
				value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/arrow.png" />'
			},{
				xtype : 'displayfield' , 
				value : '<b>2. Step & Repeat</b>' , 
				width : 130 , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},*/{
				xtype : 'button' , 
				style : 'margin : 0 0 5px 0' ,  
				iconCls : 'newpage-icon' ,  
				width : 200 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add pattern</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
				 	predefinedCEWindow.openStrategyPredefined('') ; 
				}
			},{
				xtype : 'button' , 
				style : 'margin : 0 0 5px 0' ,  
				iconCls : 'newpage-icon' ,  
				width : 200 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create new content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					selectTab.showContentElementSelectorWindow(5,0) ;  
				}
			},{
				xtype : 'button' ,  
				width : 170 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add pre-defined content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					predefinedCEWindow.openWindow() ; 
				}
			}]		
		}) ;
	} , 
	
	afterrender : function(data){
		var me = this ;  
		for( var i = 0 ; i < data.content.length ; i++){
			var field = data.content[i] ; 
			
			if( field['el'] == 'asset_repeatmode'  &&  ( field['value'] == null || field['value'] == ''  ) ){
				Ext.getCmp( me.id + '-' + field['el']+ '-manual' ).setValue(true) ;  
				me.setDimManaulRepeat(false) ; 
				me.setDimStepRepeat(true) ; 
				Ext.getCmp(me.id + '-manual_layout').setValue('{mlt dindex="0"}') ; 
				continue ; 
			}
			
			if( field['type'] == 'ce'){
				continue ; 
			}
			
			if( field['value'] == '' || field['value'] == null){
				continue ; 
			}
		 
			if( field['el'] == 'asset_repeatmode' ){    
				Ext.getCmp( me.id + '-' + field['el']+ '-' + field['value'].toLowerCase()).setValue(true) ;   
				if(  field['value'] == 'define'){ 
                    me.setDimManaulRepeat(true) ; 
                    me.setDimStepRepeat(false) ;
                }else{  
                    me.setDimManaulRepeat(false) ; 
                    me.setDimStepRepeat(true) ;
                }
			}else{
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef['+field['el']+']').setValue(field['value']) ; 
			}
		}
		
		Ext.getCmp(contentEditorCE.id + '-leftPanel').collapse() ; 
	} , 
	
	setDimManaulRepeat : function(isDisable){
    	var me = this ; 
    	Ext.getCmp(me.id + '-manaulrepeat-btn').setDisabled(isDisable) ; 
    	
    } , 
    
    setDimStepRepeat : function(isDisable){
    	var me = this ; 
    	Ext.getCmp(me.id + '-step4-spacing').setDisabled(isDisable) ; 
    	Ext.getCmp(me.id + '-step4-horizontal').setDisabled(isDisable) ; 
    	Ext.getCmp(me.id + '-step4-vertical').setDisabled(isDisable) ; 
    } , 
    
    openManaulRepeatWindow : function(){
        var me = this ; 
        var w3 = new Ext.Window({
            width:550,
            height : 500 , 
            id : me.id + '-step-html-template' , 
            layout : 'fit' ,
            title : 'Define yout own Step & Repeat layout' , 
            modal: true,
         	maximizable : false,
            items : [{
                xtype : 'htmleditor' , 
                id : me.id + '-step-html-template-content' , 
                value : me.manualStepLayout 
            }] , 
            buttons : [{
                text : 'Save' , 
                handler : function(){ 
                    me.manualStepLayout = Ext.getCmp(me.id + '-step-html-template-content').getValue() ;  
                    Ext.getCmp(me.id + '-manual_layout').setValue(me.manualStepLayout) ;
                    w3.close() ;  
                }
            }], 
            listeners : {
            	'afterrender' : function(w){
            		Ext.getCmp(me.id + '-step-html-template-content').toggleSourceEdit(true);
            		Ext.getCmp(me.id + '-step-html-template-content').toggleSourceEdit(true);
            	}
            }
        }) ; 
        
        w3.show() ;     
    }
} ; 
return window.dynamicContainer ; 
//---	
}) ; 