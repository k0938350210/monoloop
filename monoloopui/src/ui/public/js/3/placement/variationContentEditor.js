define(function (require) {
// --- start

window.variation = {
    windowObj : null , 
    editForm  : null , 
    contentName : '' , 
    contentHtml : '' , 
    contentId : 0  , 
    condition : '' , 
    advance : '' , 
    resultType : 0 , // 0 grid in select mode , 1 test banch mode , 2 content edit mode , 3 property edit mode ,  5 dymamic ce edit .  
    additionalRecord : null , 
    
    disableCondition : false , 
    
    openMainWindow : function(contentId){
    	var me =  this ; 
        variation.contentId = contentId ; 
        variation.condition = '' ; 
        
        variation.initField() ; 
        var title = 'Variation' ; 
        if( me.contentHtml == '<!-- Removed by Monoloop -->' )
        	title = 'Remove' ; 
        variation.windowObj = new Ext.Window({
            autoScroll : true , 
            width:710,
         	height:'auto',
            title:"",
            id : 'content-editor-window' , 
            //baseCls : 'ces-window' ,
            //iconCls : 'ces-window-header-icon' , 
            plain:true , 
         	modal: true,
            constrain : true ,  
         	maximizable : true  , 
         	bodyStyle : 'padding : 5px 15px 15px 15px;background-color: white ; ' , 
         	closable:true , 
            items : [ {
        		xtype : 'displayfield' , 
        		hideLabel : true , 
        		value : '<b>'+title+'</b> Content Element <img src="./typo3conf/ext/t3p_base/image/info.GIF" /> ' ,
        		style : 'font-family: \'PT Sans\';font-size: 18px;color:  rgb(20,65,138) ; margin-bottom:5px;'
      	 	},variation.editForm] ,  
            buttons: [ 
            {
	            text:'CREATE CONDITION...' , 
	            cls: 'monoloop-black-btn' , 
	            hidden : me.disableCondition , 
	            handler: function(){
					condition.stage = 2 ; 
                    condition.preconditionStr = variation.condition ; 
                    condition.openMainWindow() ; 
	            } , 
                listeners : {
                    'render' : function(btn){
                        if( variation.contentId != 0){
                            btn.setText('EDIT CONDITION...') ; 
                        }
                    }
                }
	        },
            {
	            text:'PREVIEW'  ,
	            cls: 'monoloop-black-btn' , 
                iconCls : 'pw-menu-inspect-content' , 
	            handler: function(){
	                variation.processPreview() ; 
	            }
	        },
            {
	            text:'CANCEL'  ,
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
					variation.windowObj.close() ;
                    variation.windowObj.destroy() ; 
                    pwMain.clearIframeSelection() ; 
                    
                    if(pwMain.crossDomain){
                        ml_testbubble.closeAll() ;
                    }
	            }
	        },{
	            width : 40 , 
	            text:'OK'  ,
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
	                me.saveForm() ; 
	            }
	        }] , 
            listeners : {
                'resize' : function(win , w , h){
                    Ext.getCmp('v_contentDetail').setWidth(w - 65) ; 
                    if( variation.resultType == 3){  
                        Ext.getCmp('v_contentDetail').setHeight(h - 350 ) ;
                    }else{ 
                        Ext.getCmp('v_contentDetail').setHeight(h - 200 ) ;
                    }
                } , 
                'show' : function(w){
                    if( pwMain != undefined){
                        if(  pwMain.crossDomain){
                            ml_testbubble.open5(w); 
                        }
                    }
                } , 
                'move' : function(w , x,y ){
                    if( pwMain != undefined){
                        if( pwMain.crossDomain){
                            if( Ext.getCmp('test_bubble_5') != undefined)
                                Ext.getCmp('test_bubble_5').close() ;
                            if( Ext.getCmp('test_bubble_11')!= undefined)
                                Ext.getCmp('test_bubble_11').close() ;  
                        }
                    }
                } , 
                
                'close' : function(w){
                    
                }
            }  
        }) ; 
        variation.ajaxPreProcess() ; 
    } , 
    
    saveForm : function(){
    	var t = Ext.getCmp('v_contentDetail').getValue()   ; 
        var data = variation.retrieveDataForSubmit() ;  
        if(variation.editForm.getForm().isValid()){
            //alert('Yes . We pass validate ; ') ; 
            
            if(pwMain.crossDomain){
                monoloop_base.showProgressbar('Save data...') ;  
                var fromDatas = variation.editForm.getForm().getValues() ;
                
                for (var attrname in fromDatas) { data[attrname] = fromDatas[attrname]; } 
                data['eID'] = 't3p_dynamic_content_placement' ; 
                data['pid'] = pwMain.pid ; 
                data['cmd'] = 'saveVariation' ; 
                data['type'] =  variation.resultType ; 
                data['time'] = new Date().getTime();
                Ext.util.JSONP.request({
                    url: pwMain.baseUrl + 'index.php',
                    callbackKey: 'callback',
                    params : data , 
                    callback: function(result) {
                        Ext.MessageBox.hide();
                        variation.saveVariation_success() ; 
                    }
                });
                if( pwMain != undefined){
                    ml_testbubble.closeAll() ;
                    if( pwMain.mainWindowObj != null){ 
                        ml_testbubble.open13() ; 
                    }
                }
            }else{
                
                //   return ; 
                variation.editForm.getForm().submit({
                    url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=saveVariation&type='+ variation.resultType,
                    waitMsg: 'Saving data...',
                    params : data ,
                    success: function(simple, o){
                    	 //alert('Save success') ;
                         
                         variation.saveVariation_success(o.result.contentId) ; 
                    } ,
                    failure: function(simple, o) {
                        Ext.MessageBox.hide();
			            Ext.MessageBox.alert('Error', o.result.msg ) ; 
	    			}
                });
            }
            
        } 
    } , 
    
    saveVariation_success : function(contentId){
        variation.windowObj.close() ;
         variation.windowObj.destroy() ;
         monoloop_base.msg('Success', 'New monoloop content has been created') ;
         //pwMain.myStore.load() ;
         if(variation.resultType == 2 ){
             mdc_contentmanager.refreshContentGrid() ; 
         }else if(variation.resultType == 5 ){
             contentEditorCE.UpdateCEGrid(contentId) ; 
         }else{
             if( variation.resultType == 4 ){
                preplaced.refreshPreplacedPanel(variation.contentId) ; 
             }
             pwMain.reStartContentGrid() ;
             if(variation.resultType == 1){
                testbenchTab.refreshTab() ; 
             }
         }
    } , 
    
    before_retrieveDataForSubmit : function(){
    	return {} ; 	
    } ,
    
    retrieveDataForSubmit : function(){
		var me = this ; 
        var data = me.before_retrieveDataForSubmit() ;            
        if(variation.resultType == 2 || variation.resultType == 5){
            data['onlyContent'] = 1 ; 
        }else{
            
            data['url'] = Ext.getCmp('pw-url').getValue();
            data['selector'] = pwMain.selectorTxt ; 
            data['beforeAfter'] = selectTab.beforeAfter ; 
        }
        data['contentId'] = variation.contentId ; 
        data['condition'] = variation.condition ; 
        return data ;  
    } , 
    
    processPreview : function(){
        var data = variation.retrieveDataForSubmit() ;            
        if(variation.editForm.getForm().isValid()){
            //alert('Yes . We pass validate ; ') ;  
            
            var t = Ext.getCmp('v_contentDetail').getValue()   ; 
            
            if( pwMain.crossDomain){
                monoloop_base.showProgressbar('Save data...') ;  
                var fromDatas = variation.editForm.getForm().getValues() ;
                for (var attrname in fromDatas) { data[attrname] = fromDatas[attrname]; }
                data['eID'] = 't3p_dynamic_content_placement' ; 
                data['pid'] = pwMain.pid ; 
                data['cmd'] = 'saveVariation' ; 
                data['time'] = new Date().getTime();
                Ext.util.JSONP.request({
                    url: pwMain.baseUrl + 'index.php',
                    callbackKey: 'callback',
                    params : data , 
                    callback: function(result) {  
                        variation.contentId = result.contentId ; 
                        ml_preview_content.openMainWindow(variation.contentId) ; 
                        Ext.MessageBox.hide();
                    }
                });
            }else{
                variation.editForm.getForm().submit({
                    url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=saveVariation',
                    waitMsg: 'Process data...',
                    params : data ,
                    success: function(simple, o){
                        variation.contentId = o.result.contentId ; 
                        ml_preview_content.openMainWindow(variation.contentId) ; 
                    } ,
                    failure: function(simple, o) { 
    					Ext.MessageBox.alert('Error', '#335' ) ; 
        			}
                });
            }
            
            
        } 
    } , 
    
    ajaxPreProcess : function(){
        if(variation.contentId == 0 ){
            variation.windowObj.show() ; 
            Ext.MessageBox.hide();
        }else{
            data = {} ; 
            data['contentId'] = variation.contentId ;
            
            if( pwMain.crossDomain){
                data['eID'] = 't3p_dynamic_content_placement' ; 
                data['pid'] = pwMain.pid ; 
                data['cmd'] = 'getCondition' ; 
                data['time'] = new Date().getTime();
                
                Ext.util.JSONP.request({
                    url: pwMain.baseUrl + 'index.php',
                    callbackKey: 'callback',
                    params : data , 
                    callback: function(result) {  
                        variation.getCondition_success(result) ; 
                    }
                });
            }else{
                Ext.Ajax.request({
        			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=getCondition'  , 
                    //url : 'testxml.php' , 
        			method: 'POST',
        			params:   data ,
        			timeout: '30000',
        			// process the response object to add it to the TabPanel:
        			success: function(xhr) {
         			    var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
                        variation.getCondition_success(jsonData) ; 
        			},
        			failure: function() {
        				Ext.MessageBox.hide();
    					Ext.MessageBox.alert('Error', '#336' ) ; 
        			}
        		});
            }  
        } 
    } , 
    
    getCondition_success : function(jsonData){
        variation.condition = jsonData.tx_t3pconditionalcontent_condition ;
        variation.windowObj.show() ; 
        Ext.MessageBox.hide();
    } , 
    
    initField : function(){ 
        var hideEditor = false ; 
        if( variation.contentHtml == '<!-- Removed by Monoloop -->')
        	hideEditor = true ; 
        variation.editForm = new Ext.FormPanel({ 
            autoHeight : true ,  
            id : 'content-element-form' , 
            border : true , 
            bodyBorder : true ,  
			defaults : {
				anchor : '99%'	
			}  , 
            layout : 'form' , 
            bodyStyle : 'padding : 15px;' , 
            items: [  {
                xtype : 'textfield' , 
                id : 'v_contentName' , 
                name : 'v_contentName'  , 
                value : variation.contentName  ,  
                fieldLabel : 'Content Name ' , 
                labelStyle  : 'color: rgb(20,65,138) ; font-weight: bold' , 
                hideLabel : false , 
                ctCls  : 'cef-input'      
            },{
                xtype : 'htmleditor' , 
                id : 'v_contentDetail' , 
                name : 'v_contentDetail' , 
                value :  variation.contentHtml  ,  
                hideLabel : true , 
                hidden : hideEditor , 
                loadExternalCss : false , 
                height: 230 , 
                width : 620 , 
                enableSourceEdit : true  , 
                preventMark : true  ,
                ctCls : 'cef-textarea2' ,
                plugins       : Ext.ux.form.HtmlEditor.pluginsMail() , //Ext.ux.form.HtmlEditor.plugins() , 
                listeners :{
                    'afterrender' : function(w){   
                        
                    } ,
                    'initialize' : function(panel){
                        panel.setWidth(620) ;  
                        /*
                        var cssLink = panel.iframe.contentWindow.document.createElement("link") 
                        cssLink.href = "./typo3conf/ext/t3p_base/js/extjs/plugins/htmlEditor/style.css"; 
                        cssLink.rel = "stylesheet"; 
                        cssLink.type = "text/css";  
                        panel.iframe.contentWindow.document.body.appendChild(cssLink);
                        */
                        //console.debug(panel.iframe.contentWindow.document.body.innerHTML) ; 
                    } , 
                    
                    'push' : function(myHTMLEditor , html ){   
                        var $ = myHTMLEditor.getDoc(); // shortcut 
                        if (myHTMLEditor.loadExternalCss == false)
                        {
                            var jsonData = Ext.util.JSON.decode( monoloop_base.cssDomain) ;
                            for( var i = 0 ; i < jsonData.length ; i++){
                                var head  = $.getElementsByTagName('head')[0];
                                var link  = $.createElement('link'); 
                                link.rel  = 'stylesheet';
                                link.type = 'text/css';
                                link.href = jsonData[i];
                                link.media = 'all';
                                head.appendChild(link);
                            }
                            
                            myHTMLEditor.loadExternalCss = true ;  
                        }  
                        var bodyElement = myHTMLEditor.getEditorBody() ; 
                        varionHelper.initConditionStr(bodyElement) ;  
                       
                    } , 
                    
                    'sync' : function(myHTMLEditor , html ){ 
                        //console.debug('xxxx') ; 
                         
                        var div = document.createElement('div');
                        div.innerHTML = html;
                        //console.debug(html) ; 
                        myHTMLEditor.el.dom.value = varionHelper.cleanVariation(div);
                        
                        //myHTMLEditor.setValue(  varionHelper.cleanVariation(myHTMLEditor.getValue()) ) ; 
                    }
                }
            }]
        }) ; 
        
         
         
        if( variation.resultType == 3){  
            //return ; 
            var field = new Ext.form.DisplayField({
                value : '<hr>'    , 
                hideLabel : true  
            })  ;
            variation.editForm.insert(0 , field) ;  
            
            var field = new Ext.form.TextArea({
                fieldLabel : 'Addition JS' ,    
                labelStyle  : 'color: rgb(20,65,138) ; font-weight: bold' , 
                allowBlank: true  , 
                width : 290 , 
                name: 'type3[additionJS]' ,  
                value : variation.additionalRecord.get('additionJS')
            }) ;
            variation.editForm.insert(0 , field) ;
            
            var field = new Ext.form.ComboBox({
                fieldLabel : 'Display Type' , 
                labelStyle  : 'color: rgb(20,65,138) ; font-weight: bold' , 
                typeAhead: true,
                triggerAction: 'all' ,  
                mode: 'local',
                allowBlank: true  ,
                editable: false,
                hiddenName: 'type3[displayType]' ,  
                width : 290 , 
                displayField:   'name',
                valueField:     'value' , 
                store : pwMain.displayTypeStore, 
                value : variation.additionalRecord.get('display_type')
            }) ;
            variation.editForm.insert(0 , field) ;
            
            var field = new Ext.form.ComboBox({
                fieldLabel : 'Position' , 
                labelStyle  : 'color: rgb(20,65,138) ; font-weight: bold' , 
                typeAhead: true,
                triggerAction: 'all' ,  
                mode: 'local',
                allowBlank: true  ,
                editable: false,
                hiddenName: 'type3[beforeAfter]' ,  
                width : 290 , 
                displayField:   'name',
                valueField:     'value' , 
                store : pwMain.beforeAfterStore , 
                value : variation.additionalRecord.get('befor_after')
            }) ;
            variation.editForm.insert(0 , field) ;  
 
            var field = new Ext.form.TextField({
                fieldLabel : 'X-Path' , 
                labelStyle  : 'color: rgb(20,65,138) ; font-weight: bold' , 
                name : 'type3[element_id]'  , 
                ctCls  : 'cef-input'    , 
                value : variation.additionalRecord.get('element_id') 
            }) ;
            variation.editForm.insert(0 , field) ;  
            
            var field = new Ext.form.Hidden({
                name : 'type3[uid]' , 
                value : variation.additionalRecord.get('uid') 
            }) ; 
            
            variation.editForm.insert(0 , field) ; 
             
            variation.editForm.doLayout() ;
        }
        
        var hourStore = new Ext.data.JsonStore({
            fields : ['name',  {name: 'value', type: 'int'}],
                data   : [
                    {name : '00',   value: 0 },
                    {name : '01',   value: 1 },
                    {name : '02',   value: 2 },
                    {name : '03',   value: 3 },
                    {name : '04',   value: 4 },
                    {name : '05',   value: 5 }, 
                    {name : '06',   value: 6 }, 
                    {name : '07',   value: 7 }, 
                    {name : '08',   value: 8 }, 
                    {name : '09',   value: 9 }, 
                    {name : '10',   value: 10 }, 
                    {name : '11',   value: 11 }, 
                    {name : '12',   value: 12 }, 
                    {name : '13',   value: 13 }, 
                    {name : '14',   value: 14 }, 
                    {name : '15',   value: 15 }, 
                    {name : '16',   value: 16 }, 
                    {name : '17',   value: 17 }, 
                    {name : '18',   value: 18 }, 
                    {name : '19',   value: 19 }, 
                    {name : '20',   value: 20 }, 
                    {name : '21',   value: 21 }, 
                    {name : '22',   value: 22 }, 
                    {name : '23',   value: 23 } 
                ]
            }) ; 
        var minStore = new Ext.data.JsonStore({
            fields : ['name',  {name: 'value', type: 'int'}],
                data   : [
                    {name : '00',   value: 0 },
                    {name : '05',   value: 5 },
                    {name : '10',   value: 10 },
                    {name : '15',   value: 15 },
                    {name : '20',   value: 20 }, 
                    {name : '25',   value: 25 }, 
                    {name : '30',   value: 30 }, 
                    {name : '35',   value: 35 }, 
                    {name : '40',   value: 40 }, 
                    {name : '45',   value: 45 }, 
                    {name : '50',   value: 50 }, 
                    {name : '55',   value: 55 }  
                ]
            }) ;
            
        var jsonAdvance = variation.advance ; 
        if( jsonAdvance == null || jsonAdvance == '')
            jsonAdvance = {} ; 
         
        var newPanel = variation.editForm ;
        
        var myField = new Ext.form.DisplayField({
        	hideLabel : true , 
            html : '<hr style="display: block; height: 1px;border: 0; border-top: 1px solid rgb(204,214,229) ; margin: 1em 0; padding: 0;"/>'
        })
        newPanel.add(myField) ;
        
        var myField = new Ext.form.Checkbox({
            id : 'advance-enable-daterange' , 
			hideLabel : true ,  
            name : 'advance[enableDaterange]' , 
           	boxLabel  : '<span style="font-weight:bold;color: rgb(20,65,138) ;">Enable start-stop date and time</span>&nbsp;&nbsp;<img title="Set the life span for your Content Element by defining a start and end date." src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png">' ,  
            checked : jsonAdvance.enableDaterange , 
            listeners : {
                'check' : function(checkBox , checked){
                    if( checked == true ){
                        Ext.getCmp('advanced-detail-startend-date-panel').expand() ;
                    }else{
                        Ext.getCmp('advanced-detail-startend-date-panel').collapse() ;
                    }
                }  
                
            }
        }) ; 
        newPanel.add(myField) ;
        
        var detailPanel = new Ext.Panel({ 
            border : false , 
            id : 'advanced-detail-startend-date-panel' , 
            bodyBorder : false ,  
            border : false , 
            collapsedCls : 'ml-collab-panel-colapse' ,
            bodyCssClass : 'cse-panel-body2' , 
            collapsed : ! jsonAdvance.enableDaterange ,  
            collapsible : true , 
            bodyStyle : 'border : none' , 
            header : false  
        }); 
        
        newPanel.add(detailPanel) ;
 
        
        var myField = new Ext.form.CompositeField({
            id : 'advance1-1' ,  
            style : 'margin : 0 0 10px 0' ,
           	defaults: {
                flex: 1
            },
            items: [
                {
                    xtype     : 'label',
                    html      : '&nbsp;&nbsp;&nbsp;&nbsp;Start Date' , 
                    width :  80
                }  , 
                {
                    xtype     : 'datefield',
                    name      : 'advance[startDate]',
                    id        : 'addvance-startdate' ,  
                    format : 'd/m/Y' ,  
                    showToday : true  ,  
                    value     : jsonAdvance.startDate , 
                    width     : 100
                } ,
                {
                    xtype     : 'label',
                    html      : '&nbsp;&nbsp;&nbsp;&nbsp;Hour' , 
                    width :  40
                } , 
                {
                    xtype:          'combo',
                    mode:           'local',
                    allowBlank:  true  ,
                    triggerAction:  'all',
                    editable:       false,
                    hiddenName: 'advance[startHour]',
                    fieldLabel:     'Interval',
                    displayField:   'name',
                    valueField:     'value', 
                    store:  hourStore ,  
                    value     : jsonAdvance.startHour , 
                    //hiddenValue: 0, // default value
                    width:40  

                },
                {
                    xtype     : 'label',
                    html      : '&nbsp;&nbsp;&nbsp;&nbsp;Minute' , 
                    width :  50
                } , 
                {
                    xtype:          'combo',
                    mode:           'local',
                    allowBlank: true  ,
                    triggerAction:  'all',
                    editable:       false,
                    hiddenName: 'advance[startMin]',
                    fieldLabel:     'Interval',
                    displayField:   'name',
                    valueField:     'value', 
                    store:  minStore ,  
                    value     : jsonAdvance.startMin , 
                    //hiddenValue: 0, // default value
                    width:40  

                }
            ]
        }) ; 
        detailPanel.add(myField) ;
        
        
        var myField = new Ext.form.CompositeField({
            id : 'advance1-2' ,  
           	defaults: {
                flex: 1
            },
            items: [
                {
                    xtype     : 'label',
                    html      : '&nbsp;&nbsp;&nbsp;&nbsp;End Date' , 
                    width :  80
                }  , 
                {
                    xtype     : 'datefield',
                    name      : 'advance[endDate]',
                    id        : 'advance-enddate' ,  
                    format : 'd/m/Y' ,  
                    showToday : true  ,  
                    value     : jsonAdvance.endDate , 
                    width     : 100
                } ,
                {
                    xtype     : 'label',
                    html      : '&nbsp;&nbsp;&nbsp;&nbsp;Hour' , 
                    width :  40
                } , 
                {
                    xtype:          'combo',
                    mode:           'local',
                    allowBlank: true  ,
                    triggerAction:  'all',
                    editable:       false,
                    hiddenName: 'advance[endHour]',
                    fieldLabel:     'Interval',
                    displayField:   'name',
                    valueField:     'value', 
                    store:  hourStore ,  
                    value     : jsonAdvance.endHour , 
                    //hiddenValue: 0, // default value
                    width:40  

                },
                {
                    xtype     : 'label',
                    html      : '&nbsp;&nbsp;&nbsp;&nbsp;Minute' , 
                    width :  50
                } , 
                {
                    xtype:          'combo',
                    mode:           'local',
                    allowBlank: true  ,
                    triggerAction:  'all',
                    editable:       false,
                    hiddenName: 'advance[endMin]',
                    fieldLabel:     'Interval',
                    displayField:   'name',
                    valueField:     'value', 
                    value     : jsonAdvance.endMin , 
                    store:  minStore ,  
                    //hiddenValue: 0, // default value
                    width:40  

                }
            ]
        }) ; 
        detailPanel.add(myField) ;
        
        var myField = new Ext.form.DisplayField({
            html : '<br/>'
        });
        
        detailPanel.add(myField) ;
 
    
        
    } , 
    
    openNewWindowWithPreload : function(contentId , resultType){
        variation.resultType = resultType  ; 
        data = {} ; 
        data['contentId'] = contentId ; 
        monoloop_base.showProgressbar('Initializing...') ; 
        if( pwMain.crossDomain){
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : {
                   eID : 't3p_dynamic_content_placement' ,  
                   pid : pwMain.pid , 
                   cmd : 'getVariationData' , 
                   time : new Date().getTime() , 
                   contentId : contentId   
                } , 
                callback: function(result) {  
                    variation.getVariationData_success(contentId , result) ; 
                }
            });
        }else{
            Ext.Ajax.request({
            	url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=getVariationData'  , 
                //url : 'testxml.php' , 
            	method: 'POST',
            	params:   data ,
            	timeout: '30000',
            	// process the response object to add it to the TabPanel:
            	success: function(xhr) {
            	    var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
                    
                    variation.getVariationData_success(contentId , jsonData) ; 
            	},
            	failure: function() {
            		;
            	}
            });	
        }
        
    } , 
    
    getVariationData_success : function(contentId , jsonData){
        variation.contentHtml = jsonData.bodytext ;
        variation.contentName = jsonData.header ;  
        variation.advance = jsonData.advance ;   
        variation.openMainWindow(contentId) ; 
    }
} ; 

return window.variation ; 
//---	
}) ; 