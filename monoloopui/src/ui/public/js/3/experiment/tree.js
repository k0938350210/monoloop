define(['require' , 'share/tree' ] ,function (require) {
// --- start	
var obj = require('share/tree') ; 

obj.processChangeFolder = function(){
	Ext.getCmp('experiment-main-grid').refrehGrid() ; 
}

obj.refreshGrid = function(){
	Ext.getCmp('experiment-main-grid').refrehGrid() ; 
}

obj.moveFolderSaveProcess = function(data){
	var me = this ; 
	Ext.Ajax.request({
		url: 'index.php/movefoldersave/?eID=ml_experiment',
		// send additional parameters to instruct server script
		method: 'POST',
		timeout: '30000',
		params:   data ,
		// process the response object to add it to the TabPanel:
		success: function(xhr) {
			Ext.MessageBox.hide();
			me.idFolder = data.pid;  
            Ext.getCmp('experiment-main-grid').refrehGrid() ; 
		},
		failure: function() {
			Ext.MessageBox.hide(); 
            Ext.MessageBox.alert('Error', '#310' ) ; 
		}
	});	
}
 
return obj ; 

// --- end
}) ; 