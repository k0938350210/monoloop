define(['require', 'placement/contentEditor'] ,function (require) {
// --- start

var obj = require('placement/contentEditor') ; 

// Overwrite default ; 
obj.disableCondition = true ; 

obj.before_retrieveDataForSubmit = function(){
	return {
		'experiment_id' : Ext.getCmp('wizzard').data['uid'] 
	} ; 
}

return obj ; 
//---	
}) ; 