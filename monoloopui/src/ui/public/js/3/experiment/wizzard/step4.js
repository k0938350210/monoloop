define(['require' , 'experiment/store/contentlist' , 'experiment/placement/window'] ,function (require) {
// --- start	
var obj = {
    xtype : 'container' , 
	layout : 'border' , 
	border : false , 
	id : 'wizard-step-4' ,
	cls : 'wizard-step' , 
    items : [{
		xtype : 'form' ,  
		region : 'center' , 
		id : 'step4-form' , 
	    defaults : {
	        hideLabel : true  
	    } , 
	    border : false , 
	    bodyCssClass : 'main-form' , 
        items : [{
	        xtype : 'displayfield' ,
	        value : 'step 4 of 6' , 
	        anchor : '0%' ,
	        style : 'position : absolute ; right : 0 ; top : 0 ;'  
	    },{
	        xtype : 'displayfield' , 
	        value : '<h3>Build Content</h3>'
	    },{
	        xtype : 'displayfield' , 
	        value : 'Please assign content to display to your target segment:'
	    } 
        ]
    },{
		xtype : 'container' , 
		region : 'south' ,
		border : false , 
		height : 70 ,  
		layout : 'column' , 
		cls : 'main-form' , 
		items : [{
			xtype : 'container' , 
			columnWidth : 0.65 	, 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-4\').previousStep() ; return false ; "><div class="previous-btn"><span class="icon-chevron-left"></span></div>'
		},{
			xtype : 'container' , 
			columnWidth : 0.35 , 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-4\').nextStep() ; return false ; "><div class="next-btn"><div class="col-a">Next</div><div class="col-b"><span class="icon-chevron-right"></span></div></div></a>' 	
		}]
	}]   , 
    
    // event 
	
	listeners : {
		afterrender : function(f){
            var me = this ; 
			//Add Grid Here  ; 
			var store = require('experiment/store/contentlist') ;
            
            var edit = '<td><div onClick="event.stopPropagation(); Ext.getCmp(\'wizard-step-4\').showPlacementEditWindow(\'{text}\')" title="Edit" class="placement-property"></div></td>' ;
            var deleteData = '<td><div onClick="Ext.getCmp(\'wizard-step-4\').deletePlacement(\'{text}\')" title="Delete" class="placement-delete"></div></td>' ; 
			var placewondow = '<td><div onClick="event.stopPropagation(); Ext.getCmp(\'wizard-step-4\').showPlacement(\'{text}\')" title="Placement Window" class="placement-placement"></div></td>' ;
	    	var placementAction =  '<table width="100%" ><tr align="center"> '  
                         + edit 
	    			     + placewondow
	    			     + deleteData
                         +
					   '</tr></table>'  ; 
                       
			var myGrid = new Ext.grid.GridPanel({ 
		        height:260,  
		        width : '100%' , 
		        store:  store,
		        trackMouseOver:false, 
		        loadMask: true,
		        stripeRows: true,
		        enableHdMenu: false,
		        enableDragDrop: true, 
	            id : 'content-list-grid' , 
		        // grid columns
		        columns:[{
		            header: "Url",dataIndex: 'url' , hidden : true  ,sortable: true 
		        },{
		            header: "Name",dataIndex: 'name', width : 150 ,sortable: true 
		        },{
		            id: 'crdate',header: "Date",dataIndex: 'crdate',width: 80,renderer: Ext.util.Format.dateRenderer('d M Y'),sortable: true
		        },{
		            header: "Type",width: 100,dataIndex: 'tmplType',sortable: true
		        },{
		            header: "Actions",id : 'actions' , dataIndex: 'uid',width: 100,sortable: false , renderer: Ext.getCmp('wizard-step-4').renderContentAction
		        },{
		           header : 'Status' , width : 70 , id : 'status' , dataIndex: 'hidden',sortable: true , renderer : Ext.getCmp('wizard-step-4').renderContentStatus
		        }] , 
		        listeners : {
		        	afterrender : function(g){ 
		        	}
		        } , 
                
                view: new Ext.grid.GroupingView({
                    forceFit:true,
                    groupTextTpl: '<div style="float : left ; width : 68.5% ; overflow : hidden ;">{text}</div><div style="float : left ; width : 21% ;">'+placementAction+'</div><div style="clear:both;"></div>'
                }) 
		    });
		    
		    Ext.getCmp('step4-form').add(myGrid) ;  
		    
		    Ext.getCmp('step4-form').add({
		    	xtype : 'displayfield' , 
		    	width : '48%' , 
		    	value : '<a href="javascript:void(0);" class="create-new-btn" onclick="Ext.getCmp(\'wizard-step-4\').createNewContent() ; return false ;">Create new content</a>'
		    }) ; 
		    
		    //me.createNewContent() ; 
            
            me.refreshGrid() ; 
		}
	} , 
    
    // custom function 
    createNewContent : function(){ 
        var window = require('experiment/placement/window') ; 
        window.contentListURL = 'index.php/'+Ext.getCmp('wizzard').data['uid']+'/contents/?eID=ml_experiment' ; 
        window.openMainWindow() ; 
    } , 
    
    previousStep : function(){ 
    	Ext.getCmp('wizzard').changeStep(4,3) ; 
    } , 
    
    nextStep : function(){  
		Ext.getCmp('wizzard').addCompleteStep(4) ;  
    	Ext.getCmp('wizzard').changeStep(4,5) ; 
    } , 
    
    refreshGrid : function(){
        var grid =  Ext.getCmp('content-list-grid') ; 
        grid.store.baseParams ={
            experiment_id : Ext.getCmp('wizzard').data['uid'] 
        } ; 
        grid.store.load() ; 
    } , 
    
    renderContentAction : function(value, p, r){
		var ret =  '<table width="100%"><tr align="center" >' + 
    			  
    			    '<td ><div onClick="Ext.getCmp(\'wizard-step-4\').showContentEditWindow('+r.data['uid']+' ,'+r.data['tx_templavoila_to']+' )" title="Edit" class="placement-edit" ></a></td>' + 
    			    '<td><div onClick="Ext.getCmp(\'wizard-step-4\').showPreviewWindow('+r.data['uid']+' )"  title="View" class="placement-content-preview" ></div></td>' + 
    			    '<td><div onClick="Ext.getCmp(\'wizard-step-4\').deletePlacementDetail('+r.data['uid']+' )"  title="Delete" class="placement-delete" ></div></td>' +  
				   '</tr></table>'  ;
		// r.data['uid'] ; 
 
		return ret ;
	} ,
    
    renderContentStatus : function(value,p,r){ 
    	var hidden = '' ;
        if( r.data['hidden'] == 0 ){
			hidden = '<td><div onClick="Ext.getCmp(\'wizard-step-4\').setPlacementDetailHidden('+r.data['uid']+' , 1)"  class="placement-active"></div></td>' ; 
		}else{
			hidden = '<td><div onClick="Ext.getCmp(\'wizard-step-4\').setPlacementDetailHidden('+r.data['uid']+' , 0)"   class="placement-inactive" ></div></td>' ; 
		}
        	var ret =  '<table width="100%" ><tr align="center"> ' 
					 + hidden +
				   '</tr></table>'  ; 
		return ret ; 
    } , 
    
    showPlacement : function(url){ 
        url = url.replace('Url: ','') ; 
        var window = require('experiment/placement/window') ; 
        window.contentListURL = 'index.php/'+Ext.getCmp('wizzard').data['uid']+'/contents/?eID=ml_experiment' ; 
        window.openMainWindow(url) ; 
        
    } , 
    
    showPlacementEditWindow : function(url){
        url = url.replace('Url: ','') ; 
        require(['experiment/placement/editwindow' ],function(window){ 
            window.openWindowByURL(url) ; 
        }) ;   
    } , 
    
    deletePlacement : function(url){
        url = url.replace('Url: ','') ; 
        
        var me = this ; 
        // Load Data by URL ; 
        Ext.MessageBox.confirm('Confirm', 'Are you sure to delete this record ?' , function(btn){
           if( btn == 'yes'){
                monoloop_base.showProgressbar('Deleting data...'  ) ;
                Ext.Ajax.request({
            		url: 'index.php/'+Ext.getCmp('wizzard').data['uid']+ '/placementmain/delete/?eID=ml_experiment' , 
            		// send additional parameters to instruct server script
            		method: 'POST',
                    params : {
                        url : url 
                    } , 
            		timeout: '30000', 
            		// process the response object to add it to the TabPanel:
            		success: function(xhr) {
            			Ext.MessageBox.hide(); 
                     	var jsonRoot = Ext.util.JSON.decode(xhr.responseText) ;
                     	if( jsonRoot.success == true){
                            me.refreshGrid() ; 
                        }else{
                            	 
                        }
            		},
            		failure: function() {
            			Ext.MessageBox.hide();
            			Ext.MessageBox.alert('Error', '#309' ) ; 
            		}
                });
           } 
        }) ;  
    } , 
    
    showPreviewWindow : function(uid){
        var me = this ; 
    	var rec = Ext.getCmp('content-list-grid').store.getById(uid) ;
		var name = rec.get('name') ;
        var content_id = rec.get('content_id') ;   
        require(['placement/contentPreviewWindow' ],function(window){ 
            window.open(content_id , name) ; 
        }) ;  
    } , 
    
    deletePlacementDetail : function(uid){
        var me = this ; 
        Ext.MessageBox.confirm('Confirm', 'Are you sure to delete this record ?' , function(btn){
           if( btn == 'yes'){
                monoloop_base.showProgressbar('Deleting data...'  ) ;
                Ext.Ajax.request({
            		url: 'index.php/'+Ext.getCmp('wizzard').data['uid']+ '/placementdetail/delete/?eID=ml_experiment' , 
            		// send additional parameters to instruct server script
            		method: 'POST',
                    params : {
                        uid : uid 
                    } , 
            		timeout: '30000', 
            		// process the response object to add it to the TabPanel:
            		success: function(xhr) {
            			Ext.MessageBox.hide(); 
                     	var jsonRoot = Ext.util.JSON.decode(xhr.responseText) ;
                     	if( jsonRoot.success == true){
                            me.refreshGrid() ; 
                        }else{
                            	 
                        }
            		},
            		failure: function() {
            			Ext.MessageBox.hide();
            			Ext.MessageBox.alert('Error', '#309' ) ; 
            		}
                });
           }
        });
    } , 
    
    setPlacementDetailHidden : function(uid , hidden){
        var me = this ; 
        monoloop_base.showProgressbar('Updating data...'  ) ;
        Ext.Ajax.request({
    		url: 'index.php/detail/sethidden/?eID=ml_cp' , 
    		// send additional parameters to instruct server script
    		method: 'POST',
            params : {
                uid : uid , 
                hidden : hidden 
            } , 
    		timeout: '30000', 
    		// process the response object to add it to the TabPanel:
    		success: function(xhr) {
    			Ext.MessageBox.hide(); 
             	var jsonRoot = Ext.util.JSON.decode(xhr.responseText) ;
             	if( jsonRoot.success == true){
                    me.refreshGrid() ; 
                }else{
                    	 
                }
    		},
    		failure: function() {
    			Ext.MessageBox.hide();
    			Ext.MessageBox.alert('Error', '#309' ) ; 
    		}
        });
    } , 
    
    showContentEditWindow : function(uid , toUid){
        if( toUid != 0 ){
            var rec = Ext.getCmp('content-list-grid').store.getById(uid) ;
            require(['experiment/placement/contentEditor' ],function(window){ 
                window.elementName = rec.get('name') ; 
                window.openMainWindow(toUid , rec.get('content_id') , 2) ; 
            }) ;   
        }else{
        	var rec = Ext.getCmp('content-list-grid').store.getById(uid) ;
            variation.openNewWindowWithPreload( rec.get('content_id') , 2 ) ;
        }
    }
} ; 
return obj ; 
// --- end
}) ; 