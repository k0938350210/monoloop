define(function (require) {
// --- start	
var me = {
	init : function(config){
		me.config = config ; 
		me.id = config.id ;  	
		me.mainWindow = new Ext.Window({
			width:800,
         	height:450,
         	plain:true , 
         	modal:true,
            maximizable : false  , 
            resizable : false , 
            closable : false , 
            id : config.id , 
            data : config.data , 
            layout : 'border' , 
            items : [{
            	xtype : 'container' , 
            	region : 'west' , 
            	html : '444' , 
            	border : false , 
            	cls : 'experiment-left-panel'  , 
            	layout : 'border' , 
            	items : [{
            		xtype : 'container' , 
            		region : 'center' , 
            		items : [{
            			xtype : 'container' , 
            			id : me.id + '-menu1' , 
            			cls : 'experiment-menu first' ,  
            			width : '100%' ,
            			html : '<span class="icon">&nbsp;</span>&nbsp;&nbsp;&nbsp;1. Name<div class="left-arrow"></div><div style="clear:both;"></div>' , 
            			listeners : {
            				'render' : function(panel){
            					Ext.getCmp('wizzard').setUpPanel(panel,1) ; 
            				}
            			}
            		},{
            			xtype : 'container' , 
            			id : me.id + '-menu2' , 
            			cls : 'experiment-menu' ,  
            			width : '100%' ,
            			html : '<span class="icon">&nbsp;</span>&nbsp;&nbsp;&nbsp;2. Define Segment<div class="left-arrow"></div><div style="clear:both;"></div>', 
            			listeners : {
            				'render' : function(panel){
            					Ext.getCmp('wizzard').setUpPanel(panel,2) ; 
            				}
            			}
            		},{
            			xtype : 'container' , 
            			id : me.id + '-menu3' , 
            			cls : 'experiment-menu' ,  
            			width : '100%' ,
            			html : '<span class="icon">&nbsp;</span>&nbsp;&nbsp;&nbsp;3. Define Goal<div class="left-arrow"></div><div style="clear:both;"></div>', 
            			listeners : {
            				'render' : function(panel){
            					Ext.getCmp('wizzard').setUpPanel(panel,3) ; 
            				}
            			}
            		},{
            			xtype : 'container' , 
            			id : me.id + '-menu4' , 
            			cls : 'experiment-menu' ,  
            			width : '100%' ,
            			html : '<span class="icon">&nbsp;</span>&nbsp;&nbsp;&nbsp;4. Build Content<div class="left-arrow"></div><div style="clear:both;"></div>', 
            			listeners : {
            				'render' : function(panel){
            					Ext.getCmp('wizzard').setUpPanel(panel,4) ; 
            				}
            			}
            		},{
            			xtype : 'container' , 
            			id : me.id + '-menu5' , 
            			cls : 'experiment-menu' ,  
            			width : '100%' ,
            			html : '<span class="icon">&nbsp;</span>&nbsp;&nbsp;&nbsp;5. Control Group<div class="left-arrow"></div><div style="clear:both;"></div>', 
            			listeners : {
            				'render' : function(panel){
            					Ext.getCmp('wizzard').setUpPanel(panel,5) ; 
            				}
            			}
            		},{
            			xtype : 'container' , 
            			id : me.id + '-menu6' , 
            			cls : 'experiment-menu' ,  
            			width : '100%' ,
            			html : '<span class="icon">&nbsp;</span>&nbsp;&nbsp;&nbsp;6. Confirm Settings<div class="left-arrow"></div><div style="clear:both;"></div>', 
            			listeners : {
            				'render' : function(panel){
            					Ext.getCmp('wizzard').setUpPanel(panel,6) ; 
            				}
            			}
            		}]
            	},{
            		xtype : 'container' , 
            		region : 'south' , 
            		height : 60 , 
            		cls : 'experiment-exit-panel'  , 
            		html : '<br/><a href="#" onclick="Ext.getCmp(\''+me.id+'\').closeAndRefresh(); return false ; ">Exit setup</a>'
            	}] , 
            	width : 180
            },{
            	xtype : 'container' , 
            	region : 'center' , 
                layout : 'fit' , 
                id : 'wizard-container' , 
             	cls : 'experiment-right-panel' , 
            	items : []
            }] , 
            baseCls : 'experiment-wizzard' ,  
            listeners : {
                afterrender : function(w){
                    //w.data['uid'] = 3 ; 
                    w.changeStep(0,1) ; 
                }
            } , 
            // Custom function .
            changeStep : function(from , to){ 
            	this.removeActive(from) ; 
            	Ext.getCmp(me.id + '-menu' + to).removeClass('complete') ; 
		        Ext.getCmp(me.id + '-menu' + to).addClass('active') ; 
		        Ext.getCmp('wizard-container').removeAll() ; 
		        // If new one we wait for uid return  ;
		        // If exists one we save in background ; 
		        if(from == 0 && to == 1){
		        	Ext.getCmp('wizzard').showStep(to) ;
		        	return ; 
		        }
		        
		        if( Ext.getCmp('wizzard').data['uid'] == undefined || Ext.getCmp('wizzard').data['uid'] == 0 ){
		        	monoloop_base.showProgressbar('Save data ...') ; 
					 Ext.Ajax.request({
						url: 'index.php/save/?eID=ml_experiment'  , 
						method: 'POST',
						timeout: '30000',
			            params: Ext.getCmp('wizzard').data , 
			            success: function(xhr) { 
							var jsonData = Ext.util.JSON.decode(xhr.responseText) ;    
							Ext.getCmp('wizzard').data['uid'] = jsonData.uid ;  
							Ext.getCmp('wizzard').showStep(to) ;
							Ext.MessageBox.hide();
						}, failure: function() {
			                Ext.MessageBox.hide();
						    Ext.MessageBox.alert('Error', '#1201' ) ; 
						}
					}) ; 
	        	}else{
	        		Ext.Ajax.request({
						url: 'index.php/save/?eID=ml_experiment'  , 
						method: 'POST',
						timeout: '30000',
			            params: Ext.getCmp('wizzard').data , 
			            success: function(xhr) {  
						}, failure: function() { 
						    Ext.MessageBox.alert('Error', '#1201' ) ; 
						}
					}) ; 
					Ext.getCmp('wizzard').showStep(to) ;	
	        	}
		        
   			 } , 
   			 
   			 showStep : function(to){
   			 	require(['experiment/wizzard/step' + to],function(wizzard){
		  
		            Ext.getCmp('wizard-container').add(wizzard) ;   
		            Ext.getCmp('wizard-container').doLayout() ;       
		        }) ; 
   			 } , 
				
			 removeActive : function(from){
			 	if( Ext.getCmp(me.id + '-menu' + from) == undefined)
			 		return ; 
			 	Ext.getCmp(me.id + '-menu' + from).removeClass('active') ; 
			 } , 
			 
			 addCompleteStep : function(step){
			 	Ext.getCmp(me.id + '-menu' + step).addClass('complete') ; 
			 } , 
             
             save : function(){
	             monoloop_base.showProgressbar('Save data ...') ; 
				 Ext.Ajax.request({
					url: 'index.php/save/?eID=ml_experiment'  , 
					method: 'POST',
					timeout: '30000',
		            params: Ext.getCmp('wizzard').data , 
		            success: function(xhr) { 
						var jsonData = Ext.util.JSON.decode(xhr.responseText) ;   
						Ext.getCmp(me.id).close() ; 
						Ext.getCmp('experiment-main-grid').refrehGrid() ; 
						Ext.MessageBox.hide();
					}, failure: function() {
		                Ext.MessageBox.hide();
					    Ext.MessageBox.alert('Error', '#1201' ) ; 
					}
				}) ; 
               
             } , 
             
             saveandclose : function(){
             	this.save() ; 
             } , 
             
             getActiveStage : function(){ 
             	for( var i = 1 ; i <= 6 ; i++ ){
             		var className = Ext.getCmp( me.id + '-menu' + i).el.dom.className ; 
             		if((' ' + className + ' ').indexOf(' active ') > -1){
             			return i ; 
             		}
             	}
             	return 0 ; 
             } , 
             
             closeAndRefresh : function(){
             	var me = this ; 
             	Ext.getCmp(me.id).close() ; 
             	Ext.getCmp('experiment-main-grid').refrehGrid() ; 
             } ,
             
             setUpPanel : function(panel,i){
             	panel.el.on('click', function() {
					var active = Ext.getCmp('wizzard').getActiveStage() ;  
 					if(active == i)
 						return ; 
					//Not Allow to click if experiment id == 0 
					if(!("uid" in me.mainWindow.data) || me.mainWindow.data['uid'] == 0)
						return ;
					Ext.getCmp('wizzard').changeStep(active,i) ; 
		       });
             }
		}) ; 
		
		me.mainWindow.show() ; 
		 
	}   
    
} ; 
return me ; 
// --- end
}) ; 