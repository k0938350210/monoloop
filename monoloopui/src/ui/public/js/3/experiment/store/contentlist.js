define(function(){
//---
var myReader = new Ext.data.JsonReader({ 
    idProperty: 'uid',
    
    root: 'topics',
    totalProperty: 'totalCount',
    
    fields: [
    	{name: 'uid', type: 'int'},
        {name: 'hidden', type: 'int'},  
        {name: 'content_id' , type : 'int'} , 
        {name: 'tx_templavoila_to' , type : 'int'} , 
    	{name: 'name'} ,  
        {name: 'tmplType' } ,
        {name: 'url' } ,
		{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'}  
    ]
});

var me = new Ext.data.GroupingStore({  
    
    reader: myReader ,
    proxy: new Ext.data.HttpProxy({
        url: 'index.php/placements/?eID=ml_experiment'
    }),
    baseParams:{
		idfolder : 0
	} , 
    remoteSort: true,
    groupField:'url' ,
    sortInfo:{field: 'crdate', direction: "DESC"}, 
});  
return 	me ; 
//---	
}) ; 