define(function(){
//---
var me = new Ext.data.JsonStore({ 
    root: 'topics',
    totalProperty: 'totalCount',
    idProperty: 'uid',
    remoteSort: true,

    fields: [
    	{name: 'uid', type: 'int'},{name: 'sid', type: 'int'},{name: 'gid', type: 'int'},
        {name: 'hidden', type: 'int'},
    	{name: 'pid', type: 'int'}, 
    	{name: 'name'} , {name: 'type', type: 'int'} ,{name: 'description'} ,  
        {name: 'segment'} , { name : 'goal'} , 
		{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'} , 
        {name: 'ordering' , type : 'int'},{name: 'cg_day' , type : 'int'},{name: 'cg_size' , type : 'int'}, 
        {name: 'significantAction' , type: 'int'}
    ],

    // load using script tags for cross domain, if the data in on the same domain as
    // this page, an HttpProxy would be better
    proxy: new Ext.data.HttpProxy({
        url: 'index.php/?eID=ml_experiment'
    }),
    
    baseParams:{
		idfolder : 0
	}
});
me.setDefaultSort('ordering', 'ASC');
return 	me ; 
//---	
}) ; 