define(function (require) {
// --- start	
var me = {
	tree : null , 
 	init : function(config){
 		me.tree = config.tree ; 
 		require('extjs/plugins/ProgressBarPager') ; 
 		require('placementold/PagingToolbar1') ;
 		var store = require('experiment/store/main') ; 
        
        function renderPriority(value,p,r){
            var index = store.indexOf(r)  ; 
         	var total = store.getTotalCount() ; 
         	
         	var moveUp = '<td><div onClick="Ext.getCmp(\'experiment-main-grid\').prioritySet('+r.data['uid']+' , -1 )"  class="placement-moveup"></div></td>' ; 
         	var moveDown = '<td><div onClick="Ext.getCmp(\'experiment-main-grid\').prioritySet('+r.data['uid']+' , 1 )"  class="placement-movedown"></div></td>' ; 
         	
         	if( index == 0 ){
         		moveUp = '<td>&nbsp;</td>' ;  
         	}  
         	if( index == total - 1){
         		moveDown = '<td>&nbsp;</td>' ; 
         	}
         	
        	var ret =  '<table width="100%" ><tr align="center"> ' 
						+ moveUp + moveDown + 
					   '</tr></table>'  ; 
			return ret ;
        }
 		
 		function renderAction(value, p, r){  
            var edit = '<td><div onClick="Ext.getCmp(\'experiment-main-grid\').editExp(\''+r.data['uid']+'\')" title="Edit" class="placement-property"></div></td>' ;
            var deleteData = '<td><div onClick="Ext.getCmp(\'experiment-main-grid\').deleteExp(\''+r.data['uid']+'\')" title="Delete" class="placement-delete"></div></td>' ; 
	    	var ret =  '<table width="100%" ><tr align="center"> '  
                         + edit  
	    			     + deleteData
                         +
					   '</tr></table>'  ;
 
	 
			return ret ;   
        }
        
        function renderStatus(value, p, r){
            var hidden = '' ;
            if( r.data['hidden'] == 0 ){
				hidden = '<td><div onClick="Ext.getCmp(\'experiment-main-grid\').setHidden('+r.data['uid']+' , 1)"  class="placement-active"></div></td>' ; 
			}else{
				hidden = '<td><div onClick="Ext.getCmp(\'experiment-main-grid\').setHidden('+r.data['uid']+' , 0)"   class="placement-inactive" ></div></td>' ; 
			}
            	var ret =  '<table width="100%" ><tr align="center"> ' 
						 + hidden +
					   '</tr></table>'  ; 
			return ret ;
        } 
        var myGrid = new Ext.grid.GridPanel({
	        width:'100%',
	        height:530,
	        //title:'Simple newsletter listing',
	        store:  store,
	        trackMouseOver:false,
	        //disableSelection:false,
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
	        enableDragDrop: true,
			ddGroup: 'grid2tree',
	        //autoHeight: true,
            id : 'experiment-main-grid' , 
	        // grid columns
	        columns:[{
	            header: "Name",
	            dataIndex: 'name',
	            width: 50,
	            sortable: true 
	            //
	        }, {
	            header: "Segment name",
	            width: 70,
	            dataIndex: 'segment',
	            sortable: true
	        }, {
	            header: "Goal",
	            width: 70,
	            dataIndex: 'goal',
	            sortable: true
	        }, 
	        {
	            id: 'crdate', // id assigned so we can apply custom css (e.g. .x-grid-col-topic b { color:#333 })
	            header: "Date",
	            dataIndex: 'crdate',
	            width: 40,
	            renderer: Ext.util.Format.dateRenderer('d M Y'),
	            sortable: true
	        }, {
	            header: "Priority",
	            width: 30,
	            dataIndex: 'ordering',
                renderer: renderPriority, 
	            sortable: true
	        } ,{
	            header: "Actions",
                id : 'actions' , 
	            renderer: renderAction, 
	            dataIndex: 'uid',
	            width: 30,
	            sortable: false
	        } ,{
	           header : 'Status' , 
               width : 30 , 
               id : 'status' , 
               renderer: renderStatus, 
               dataIndex: 'hidden',
               sortable: true
	        },{
	           id : 'imformation' , 
               width : 10 , 
               sortable : false , 
               dataIndex: 'remark'
	        }],
	
	        // customize view config
	       
	        viewConfig: {
	            forceFit:true,
	        }, 
	
	        // paging bar on the bottom
	        bbar: new Ext.PagingToolbar1({
                displayMsg : 'Displaying {0} of {1}' ,
                id : 'experiment-list-paging-toolbar' , 
	            pageSize: 15,
	            store: store,
	            displayInfo: true,
	            plugins: new Ext.ux.ProgressBarPager() ,
	            emptyMsg: "No topics to display"  
	        }) , 
	        
	        editExp : function(uid){  
	           var rec = this.store.getById(parseInt(uid)) ; 
               if( rec == undefined){
                    return ;  
               }
		    	require(['experiment/wizzard/mainwindow'],function(wizzard){
               		wizzard.init({
               			id : 'wizzard' , 
                        data : {
                            'uid' : rec.data['uid'] , 
                            'name' : rec.data['name'] , 
                            'description' : rec.data['description'] , 
                            'segment' : rec.data['sid'] , 
                            'goal' : rec.data['gid'] , 
                            'controlGroupSize' : rec.data['cg_size'] , 
                            'controlGroupDays' : rec.data['cg_day'] , 
                            'significantAction' : rec.data['significantAction']
                        }
               		}) ; 
           		}) ; 
		    } ,
		    
		    deleteExp : function(uid){
		    	 Ext.MessageBox.confirm('Confirm', 'Are you sure to delete this record ?' , function(btn){
           			if( btn == 'yes'){
           				Ext.Ajax.request({
		            		url: 'index.php/'+uid+ '/?eID=ml_experiment' , 
		            		// send additional parameters to instruct server script
		            		method: 'DELETE', 
		            		timeout: '30000', 
		            		// process the response object to add it to the TabPanel:
		            		success: function(xhr) {
		            			Ext.MessageBox.hide(); 
		                     	var jsonRoot = Ext.util.JSON.decode(xhr.responseText) ;
		                     	if( jsonRoot.success == true){
		                             myGrid.refrehGrid() ; 
		                        }else{
		                            	 
		                        }
		            		},
		            		failure: function() {
		            			Ext.MessageBox.hide();
		            			Ext.MessageBox.alert('Error', '#309' ) ; 
		            		}
		                });
    				}
  				}) ; 
		    } , 
		    
		    setHidden : function(uid , hidden){
                var me = this  ; 
                monoloop_base.showProgressbar('Updating ...') ; 
                Ext.Ajax.request({
                    url: 'index.php/'+uid+'/hidden/?eID=ml_experiment'  , 
        			method: 'POST',
        			timeout: '30000',
                    params:   {
                        hidden : hidden 
                    } , 
                    success: function(xhr) {
                        me.refrehGrid() ; 
                        Ext.MessageBox.hide();
                    }, 
                    failure: function() {
                        Ext.MessageBox.hide();
                    }
                }) ; 
		    } , 
            
            prioritySet : function(uid , priority){
                var me = this  ; 
                monoloop_base.showProgressbar('Updating ...') ; 
                Ext.Ajax.request({
                    url: 'index.php/'+uid+'/priority/?eID=ml_experiment'  , 
        			method: 'POST',
        			timeout: '30000',
                    params:   {
                        priority : priority 
                    } , 
                    success: function(xhr) {
                        me.refrehGrid() ; 
                        Ext.MessageBox.hide();
                    }, 
                    failure: function() {
                        Ext.MessageBox.hide();
                    }
                }) ; 
            } ,
		    
		    refrehGrid : function(){
		        Ext.getCmp('experiment-main-grid').store.baseParams = {
		            'search' : Ext.getCmp('serach-textbox').getValue() , 
		        	'idfolder' : me.tree.idFolder
		        }
		        if( experiment.clearPageIndex == true )
		             Ext.getCmp('experiment-main-grid').store.load({params:{start: 0 , limit:15}});
		        else 
		             Ext.getCmp('experiment-main-grid').store.load({params:{start:Ext.getCmp('experiment-list-paging-toolbar').cursor , limit:15}}); 
		    }  
	    });
	    
	    myGrid.refrehGrid() ; 
 	} 
    
    
}
return me ; 
// --- end
}) ; 