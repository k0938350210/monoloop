define(function (require) {
// --- start	
var me = {
	init : function(config){
		var Tree = Ext.tree;
    	var mtObject = this ;
		me.typeDefault = config.typeDefault ; 
		me.tree = new Tree.TreePanel({ 
			useArrows: true,
	        autoScroll: true,
	        animate: true,
	        enableDD: true,
	        ddGroup: 'grid2tree',
	        containerScroll: true,
	        border: false,
	        height: 480,
	        // auto create TreeLoader
	        dataUrl: 'index.php?eID=ml_folder&typedefault='+me.typeDefault,
	
	        root: {
	            nodeType: 'async',
	            text: 'Root',
	            draggable: false,
	            id: '0',
	            cls: 'folder'
	        } , 
	        
	        listeners: {
		        	beforenodedrop :function(e) {
	
						// e.data.selections is the array of selected records
						if(Ext.isArray(e.data.selections)) {	
							var r;
							var cls = 'file';						
							var id = e.target.getPath('id').split('/');
				    		var pid = id[id.length-1];
							for(var i = 0; i < e.data.selections.length; i++) {	
								// get record from selectons
								r = e.data.selections[i];
		
								var data = {};
								data['uid'] = r.get('uid');
				    			data['pid'] = pid;
				    			data['cls'] = cls;
				    			data['type'] = me.typeDefault;
				    			 
				    			monoloop_base.showProgressbar('Loading data ....' , 'Loading') ;
				    			
				    			me.moveFolderSaveProcess(data) ; 
				    			
				    			
							}
		
							// we want Ext to complete the drop, thus return true
							return true;
						}
		
						// if we get here the drop is automatically cancelled by Ext
					} ,
				
		        	click : function( node, e ){
		        		mtObject.changeFolder(node);
		        	} ,
		        	
		        	dblclick : function( node, e ){
		        		mtObject.changeFolder(node);
		        	} ,
		        	
		        	expandnode : function( node ){
		        		mtObject.changeFolder(node);
		        	} ,
		        	
					movenode : function( tree, node, oldParent, newParent, index ){
			        	var data = {};
			    		var id = node.getPath('id').split('/');
			    		var uid = id[id.length-1];
			    		var pid = id[id.length-2];
			    		var cls = node.getPath('cls').split('/');
						cls = cls[cls.length-1];
						var oldCls = oldParent.getPath('id').split('/');
						oldCls = oldCls[oldCls.length-1];
						var newCls = newParent.getPath('id').split('/');
						newCls = newCls[newCls.length-1];
			    		
			    		if(oldCls != newCls){		    		
			    			data[ 'tx_t3pdynamiccontentplacement_pi1[uid]'] = uid;
			    			data[ 'tx_t3pdynamiccontentplacement_pi1[pid]'] = pid;
			    			data[ 'tx_t3pdynamiccontentplacement_pi1[cls]'] = cls;
			    			data[ 'tx_t3pdynamiccontentplacement_pi1[type]'] = mtObject.typeDefault;
			    			 
			    			
			    			Ext.Ajax.request({
			    				url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=moveFolderSave',
			    				// send additional parameters to instruct server script
			    				method: 'POST',
			    				timeout: '30000',
			    				params:   data ,
			    				// process the response object to add it to the TabPanel:
			    				success: function(xhr) {
			    					Ext.MessageBox.hide();
		    						mtObject.idFolder = pid;  
		    						mtObject.refreshGrid() ; 
			    				},
			    				failure: function() {
			    					Ext.MessageBox.hide(); 
						            Ext.MessageBox.alert('Error', '#311' ) ; 
			    				}
			    			});	
			    		}
					} 
				}
	     }) ; 
	} , 
	
	changeFolder : function(node){
		var id = node.getPath('id').split('/');
		id = id[id.length-1];
		var cls = node.getPath('cls').split('/');
		cls = cls[cls.length-1];
		
		if((this.idFolder != id) && (cls == 'folder')){
			this.idFolder = id;  
			me.processChangeFolder() ; 
		}
	} ,  
	
	openDeleteWindow : function(){
		var treeObj =  me.tree ;
		var sm = treeObj.getSelectionModel().getSelectedNode();
		if(sm != null){
			var cls = sm.getPath('cls').split('/');
			cls = cls[cls.length-1];
			if(cls == 'folder') { 
				Ext.MessageBox.confirm('Confirm', 'Are you sure to delete this content ?', function(btn){
					if(btn == 'yes'){
						me.deleteNodeProcess() ; 
					}
				}) ;
			} else {
				alert('Please select folder.');
			}
		}
	} , 
	
	deleteNodeProcess : function(){
		var data = {};
		var treeObj =  me.tree ;
		var sm = treeObj.getSelectionModel().getSelectedNode();
		if(sm != null){
			var uid = sm.getPath('id');
			
			if(uid != ''){
				data['tx_t3pdynamiccontentplacement_pi1[uid]'] = uid;
				
				monoloop_base.showProgressbar('Loading data .....') ;  
				
				Ext.Ajax.request({
					url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=deleteFolderSave',
					// send additional parameters to instruct server script
					method: 'POST',
					timeout: '30000',
					params:   data ,
					// process the response object to add it to the TabPanel:
					success: function(xhr) {
						var jsonData = Ext.util.JSON.decode(xhr.responseText) ; 
						Ext.MessageBox.hide();
						treeObj.loader.load(treeObj.root);
						treeObj.getRootNode().expand(); 
					},
					failure: function() {
						Ext.MessageBox.hide(); 
					    Ext.MessageBox.alert('Error', '#312' ) ; 
					}
				});	
			} else {
				alert('Please select folder.');
			}
		}
	} , 
	
	openEditWindow : function(){
		var treeObj =  this.tree ;
		var data = {};
		var sm = treeObj.getSelectionModel().getSelectedNode();
		if(sm != null){
			var cls = sm.getPath('cls').split('/');
			cls = cls[cls.length-1];
			var uid = sm.getPath('id');	
			var text = sm.getPath('text').split('/');
			text = text[text.length-1];
			data[ 'tx_t3pdynamiccontentplacement_pi1[uid]'] = uid;
			
			if((uid != '') && (cls == 'folder')){
				var form = new Ext.FormPanel({
			        frame:true,
			        bodyStyle:'padding:5px 5px 0',
			        width: '100%',
			        defaults: {
			            allowBlank: false 
			        },
			
			        items: [ {
		                xtype:'textfield',
		                fieldLabel: 'folder name',
		                allowBlank: false  ,
		                name: 'tx_t3pdynamiccontentplacement_pi1[title]', 
		                anchor: '90%',
		                emptyText: text
		            }],
			
			        buttons: [{
			            text: 'Save' , 
			            handler: function(){
			                if(form.getForm().isValid()){
			                	form.getForm().submit({
				                    url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=editFolderSave',
				                    waitMsg: 'Saving your data...',
				                    success: function(form , o){ 
				                        mainWindow.close() ;
				                        treeObj.loader.load(treeObj.root);
				                        treeObj.getRootNode().expand();
				                    } , 
				                    failure: function(form , o) { 
									} ,
									params : data 
				                });
			                }
			            }
			
			        }]
			    });
				
				mainWindow = new Ext.Window({
			        title: 'Edit folder .',
			        closable:true,
			        width:350,
			        height:110,
			        //border:false,
			        plain:true , 
			        items: [form]
			    });
			    
			    mainWindow.show(this);
			} else {
				alert('Please select folder.');
			}
		}
	} , 
	
	createNewWindow : function(){
		var treeObj =  me.tree ; 
		var form = new Ext.FormPanel({
	        frame:true,
	        bodyStyle:'padding:5px 5px 0',
	        width: '100%',
	        defaults: {
	            allowBlank: false 
	        },
	
	        items: [ {
	            xtype:'textfield',
	            fieldLabel: 'folder name',
	            allowBlank: false  ,
	            name:  'tx_t3pdynamiccontentplacement_pi1[title]',
	            //emptyText: 'Press any value',
	            anchor:'90%'
	        }],
	
	        buttons: [{
	            text: 'Save' , 
	            handler: function(){
	        		var data = {};
	        		var sm = treeObj.getSelectionModel().getSelectedNode();
	        		if(sm == null){
		        		var ownerId = 0;
		        		var pid = 0;
	        		} else {
	        			var ownerId = sm.getPath('owner_id');
		        		var pid = sm.getPath('id');
	        		}
	        		
	        		data[  'tx_t3pdynamiccontentplacement_pi1[owner_id]'] = ownerId;
	        		data[  'tx_t3pdynamiccontentplacement_pi1[pid]'] = pid;
	
	                if(form.getForm().isValid()){
	                	form.getForm().submit({
		                    url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=createFolderSave',
		                    waitMsg: 'Saving your data...',
		                    success: function(form , o){ 
		                        mainWindow.close() ;
	                             
		                        treeObj.loader.load(treeObj.root , function(){
		                           treeObj.getRootNode().expand();    
		                        }); 
		                              
		                        //lmBase.lmGrid.store.load({params:{start:0, limit:mdc_placement_main.pageItemsCount}});
		                    } , 
		                    failure: function(form , o) {
		                    	//mdcBase.msg('Fail',  o.result.msg);
							} ,
							params : data 
		                });
	                }
	            }
	
	        }]
	    });
		
		var mainWindow = new Ext.Window({
	        title: 'Create folder .',
	        closable:true,
	        width:350,
	        height:110,
	        //border:false,
	        plain:true , 
	        items: [form]
	    });
	    
	    mainWindow.show(this);
	} , 
	
	// Extend function ; 
	
	processChangeFolder : function(){} ,
	
	refreshGrid : function(){}  , 
	
	moveFolderSaveProcess : function(){}  
	
}

return me ; 

// --- end
}) ; 