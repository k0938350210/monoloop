/*

This file is part of Ext JS 4

Copyright (c) 2011 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department at http://www.sencha.com/contact.

*/
/**
 * @class Ext.app.PortalPanel
 * @extends Ext.Panel
 * A {@link Ext.Panel Panel} class used for providing drag-drop-enabled portal layouts.
 */
 
// This is drop panel 
Ext.define('Ext.app.MonoloopDropPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.monoloopdroppanel',
    requires: [
        'Ext.layout.component.Body'
    ],
 
    bodyCls: 'dropzone-panel',
    defaultType: 'panel',
    componentLayout: 'body',
    autoScroll: true, 

    style : 'background : #666; cursor : pointer ;' ,  
    bodyStyle : 'background : #666 ; color : white ; cursor : pointer ;' ,
    html : 'Drop HERE'  
});

