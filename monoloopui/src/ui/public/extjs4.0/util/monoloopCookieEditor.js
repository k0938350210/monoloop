Ext.define('Monoloop.util.monoloopCookieEditor', { 
    extend: 'Ext.window.Window',
    alias: 'widget.monoloopcookieeditor',
    
    bodyPanel : null , 
    
    initComponent: function(){  
        var me = this ; 
        me.width = 500,
        me.height =400,
        me.title = 'Monoloop cookie editor' ;
        me.maximizable = false ; 
        me.resizable = false ; 
        me.plain = true ; 
        me.closable = true ;  
        me.modal = true ; 
        me.buttons = [
        {
            text: 'CANCEL' , 
            handler : function(){ 
               me.close() ; 
        	}       
	    } , 
        {
            text: 'SAVE' , 
            handler: function(){
                me.setLoading('Save custom cookie data.') ;
                var data = {} ; 
                data['cookieData'] = Ext.getCmp(me.id + '-text').getValue() ; 
                Ext.Ajax.request({
                    url: 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveCookieData' , 
                    params : data , 
        			// process the response object to add it to the TabPanel:
        			success: function(xhr) {
        			     me.setLoading(false) ;
                         me.close() ;   
        			},
        			failure: function() { 
                        Ext.MessageBox.alert('Error', '#502' ) ; 
                        me.setLoading(false) ; 
        			}
        		});	
            } 
        }
        ] ; 
        
        me.listeners = {
            'afterrender' : function(w){
                w.setLoading('Loading cookie data') ; 
            }
        } ; 
        
        
        me.initBodyPanel() ; 
        Ext.apply(me ,{  
            layout: 'fit' ,  
            items :  [ me.bodyPanel ]
        }) ; 
        
        this.callParent(arguments);
        
        Ext.Ajax.request({
            url: 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/getCookieData' , 
			// process the response object to add it to the TabPanel:
			success: function(xhr) {
			    Ext.getCmp(me.id + '-text').setValue(xhr.responseText)  ; 
                me.setLoading(false) ; 
			},
			failure: function() { 
                Ext.MessageBox.alert('Error', '#502' ) ; 
                me.setLoading(false) ; 
			}
		});	
    } , 
    
    initBodyPanel : function(){
        var me = this ; 
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
            layout: 'fit' ,  
            width: '100%',
            items : [{
                xtype : 'textarea'  , 
                id : me.id + '-text'  
            }]
        }) ; 
    }
}) ; 
    