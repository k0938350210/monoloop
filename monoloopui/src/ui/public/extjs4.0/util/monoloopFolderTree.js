Ext.define('Monoloop.util.monoloopFolderTree', { 
    extend: 'Ext.panel.Panel',
    alias: 'widget.monoloopfoldertree',
    
    // Link to folder tree 
    folderTree : null , 
    // Tracker type at tree DB . 
    typeDefault : 0 ,
    // Link PID ; 
    pid : 1 , 
    
    treeEditURL : '' , 
    treeNewURL : '' , 
    treeDeleteURL : '' , 
    
    selectedID : 0 , 
    
    initComponent: function(){  
        var me = this ; 
        
        var store = Ext.create('Ext.data.TreeStore', { 
            clearOnLoad : true , 
            root: {
            } , 
            proxy : {
                type: 'ajax',
                url : 'index.php?eID=t3p_dynamic_content_placement&pid='+me.pid+'&cmd=listFolder&typedefault='+me.typeDefault,
                reader: {
                    type: 'json',
                    root: 'data'
                } , 
                actionMethods: {
                    read: 'POST'
                }
            } 
        }) ; 
        
        me.tbar = [
            {  
                cls : 'tree-button' , 
                iconCls: 'new-symbol1',
                tooltip: 'Create new folder',
                height : 24 , 
                handler: function(){
            		 me.createNewFolder() ; 
                }
            }, { 
                cls : 'tree-button' , 
                iconCls: 'edit-symbol1',
                height : 24 , 
                tooltip: 'Edit folder',
                handler: function(){
            		me.openEditFolderWindow() ; 
                }
            }, { 
                cls : 'tree-button' , 
                iconCls: 'delete-symbol1',
                tooltip: 'Delete folder',
                height : 24 , 
                handler: function(){
            		me.deleteFolder();
                }
            }
        ]  ;
         
        me.folderTree =  Ext.create('Ext.tree.Panel', {  
            store: store,
            height: 150, 
            id : me.id + '-tree' , 
            rootVisible: true , 
            listeners : {
                'select' : function(RowModel, record, index, eOpts){
                    if( record.data.id == 'root')
                        me.selectedID = 0 ;
                        
                    else
                        me.selectedID = record.data.id  ;  
                    me.treeTrigger() ; 
                }
            } , 
            viewConfig: {
                plugins: {
                	ptype: 'treeviewdragdrop',
					ddGroup:	'selDD', 
					appendOnly : false , 
					enableDrag : false ,
					allowContainerDrops : false , 
					allowParentInserts : false 
                } , 
                listeners : {
                	 beforedrop: function(node, data, overModel, dropPosition, dropHandlers) {
                	 	me.moveNode(data.records[0].getId( ) , overModel.getId( ) ) ;   
						dropHandlers.cancelDrop();
						me.treeTrigger() ;  
					}
                }
            }
        }); 
        
        Ext.apply(me ,{ 
            layout: 'fit',  
            items :  me.folderTree
        }) ;
        
        this.callParent(arguments);
    } , 
    
    createNewFolder : function(){
        var me = this ;  
        
        if( Ext.getCmp( me.id + '-tree' ).getSelectionModel().getSelection()[0] == undefined)
            return ;
            
        var selectData = Ext.getCmp( me.id + '-tree' ).getSelectionModel().getSelection()[0].data    ;   
            
        if(selectData.id == 'root'){
    		var ownerId = 0;
    		var pid = 0;
		} else {
			var ownerId = selectData.owner_id ;
    		var pid = selectData.id;
		}    
             
        var form = Ext.create('Ext.form.Panel' , { 
            bodyStyle:'padding:5px 5px 0',
            border : false , 
            width: '100%',
            defaults: {
                allowBlank: false 
            },
    
            items: [ {
                xtype:'textfield',
                fieldLabel: 'folder name',
                allowBlank: false  ,
                name: 'folder[title]', 
                anchor:'90%'
            }], 
            
            buttons: [{
	            text: 'Save' , 
	            cls : 'monoloop-submit-form',        
	            handler: function(){
	                var data = {};
                     
                    data[ 'folder[owner_id]'] = ownerId;
  		            data[ 'folder[pid]'] = pid;
                    
                    form.form.submit({
		                url: me.treeNewURL , 
		                waitMsg: 'Saving data...',
                        params : data ,
		                success: function(xx, o){  
	                        Ext.getCmp(me.id + '-newwindow').close() ; 
                            me.folderTree.store.load() ; 
		                    me.treeTrigger() ;  
		                } ,
		                failure: function(xx, o) { 
		                     Ext.MessageBox.hide();
		                     Ext.MessageBox.alert('Error', '#502' ) ; 
		    			}   
                    });    
                }
            }]  
        }) ; 
        
        Ext.create('Ext.window.Window', {
            title: 'Create folder .',
            id : me.id + '-newwindow' , 
            closable:true,
            width:350,
            modal : true , 
            height:110,
            //border:false,
            plain:true , 
            items: [form]
        }).show();
     
    } , 
    
    openEditFolderWindow : function(){
        var me = this ;  
        if( Ext.getCmp( me.id + '-tree' ).getSelectionModel().getSelection()[0] == undefined)
            return ; 
        var selectData = Ext.getCmp( me.id + '-tree' ).getSelectionModel().getSelection()[0].data    ;  
        
        
            
        var cls = selectData.cls;
		var uid = selectData.id;	
		var text = selectData.text ;
        
        if( uid == 'root')
            return ; 
        
        var form = new Ext.FormPanel({
	        border : false , 
	        
	        bodyStyle:'padding:5px 5px 0',
	        width: '100%',
	        defaults: {
	            allowBlank: false 
	        },
	
	        items: [ {
                xtype:'textfield',
                fieldLabel: 'folder name',
                allowBlank: false  ,
                name:'folder[title]', 
                anchor: '90%',
                emptyText: text
            }],
	
	        buttons: [{
	            text: 'Save' , 
	            cls : 'monoloop-submit-form',       
	            handler: function(){
                    if(form.getForm().isValid()){
                        var data = {} ; 
                        data['folder[uid]'] = uid ; 
	                    form.form.submit({
    		                url: me.treeEditURL , 
    		                waitMsg: 'Saving data...',
                            params : data ,
    		                success: function(xx, o){  
  		                        Ext.getCmp(me.id + '-editwindow').close() ; 
                                me.folderTree.store.load() ; 
    		                    me.treeTrigger() ;  
    		                } ,
    		                failure: function(xx, o) { 
    		                     Ext.MessageBox.hide();
    		                     Ext.MessageBox.alert('Error', '#502' ) ; 
    		    			}   
                        });     
                    }
                }
            }]
        });
        
        Ext.create('Ext.window.Window', {
            title: 'Edit folder .',
            id : me.id + '-editwindow' , 
            closable:true,
            width:350,
            height:110,
             modal : true , 
            //border:false,
            plain:true , 
            items: [form]
        }).show();
    } , 
    
    deleteFolder : function(){
        var me = this ; 
        if( Ext.getCmp( me.id + '-tree' ).getSelectionModel().getSelection()[0] == undefined){
            Ext.MessageBox.alert('Fail', 'Plese select folder node first.' ) ; 
            return ; 
        }
        
        var selectData = Ext.getCmp( me.id + '-tree' ).getSelectionModel().getSelection()[0].data    ;  
        
        if( selectData.id == 'root'){
            Ext.MessageBox.alert('Fail', 'Cannot delete root node.' ) ;
            return ; 
        }
        
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete this folder ( content inside folder will be removed )?',function(btn){
            if (btn == 'yes'){
                var data = {} ; 
                monoloop_base.showProgressbar('Deleting folder ...') ; 
                data['folder[uid]'] = me.selectedID ; 
                Ext.Ajax.request({
                    url: me.treeDeleteURL ,
                    params:  data,
        			// process the response object to add it to the TabPanel:
        			success: function(xhr) {
                        me.folderTree.store.load() ; 
        				Ext.MessageBox.hide(); 
        			},
        			failure: function() {
        				Ext.MessageBox.hide();
                        Ext.MessageBox.alert('Error', '#502' ) ; 
        			}
        		});	    
            }
        }) ; 
    } , 
    
    // interface ; 
    treeTrigger : function(){
        
    } , 
    
    moveNode : function(gridNodeID , treeNodeID){
    	
    }
}) ; 