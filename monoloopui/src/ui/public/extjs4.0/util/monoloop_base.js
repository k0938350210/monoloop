var monoloop_base = {
    login_refresh : null ,  
    username : null , 
    
    showProgressbar : function(msg1){
         Ext.MessageBox.show({
           title: 'Please wait',
           msg: msg1 ,
           progressText: msg1,
           wait:true,
           width:300,
           waitConfig: {interval:300},
           progress:true,
           closable:false  
       });

    }  , 
    
    msgCt : null , 
    
    createBox : function(t, s){ 
        return '<div class="monoloop-msg"><h3>' + t + '</h3><p>' + s + '</p></div>'; 
    } , 
    
    msgCloseOnClick : function(parentNode , title , s ){
        msgCt = Ext.core.DomHelper.insertFirst(parentNode, {id:'msg-div-2'}, true);
        msgCt.alignTo(document, 't-t');
        //var s = String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.core.DomHelper.append(msgCt, {html:monoloop_base.createBox(title, s)}, true);
        m.setStyle('z-index','80000');
        m.slideIn('t') ; 
		
		msgCt.addListener('click' , function(){ 
            m.ghost("t", {});
        }) ; 	 
         
    } , 
    
    showMessage : function(title, format){
        var me = this ; 
        
        if(!me.msgCt){
            me.msgCt = Ext.core.DomHelper.insertFirst( document.body, {id: 'main-msg-div'}, true);
        } 
        var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.core.DomHelper.append(me.msgCt, me.createBox(title, s), true);
        m.setStyle('z-index','80000');
        m.hide();
        m.slideIn('t') ; //.ghost("t", { delay: 3500, remove: true});  
        m.addListener('mouseover' , function(){
            m.ghost("t", {});
        }) ; 
        //console.debug('xxx') ;
    } 
} ; 

Ext.define('UserSessionCheck', {
    
    extend: 'Ext.util.Observable', 
    
    locked: 0,
	interval: 60,

	constructor: function(config) {
		config = config || {};
		Ext.apply(this, config);
		this.initComponents2();
		this.loadingTask = {
			run: function(){
				// interval run
				Ext.Ajax.request({
					url: "index.php",
					params: {
						"eID": "monoloop_base",
						"skipSessionUpdate": 1 , 
                        "cmd" : "chkSession"
					},
					method: "GET",
					success: function(response, options) {
					    var result = Ext.JSON.decode(response.responseText);
                        if( result.timeout ){
                            this.stopTimer();
					        Ext.getCmp('ml_loginformWindow').show() ; 
                        }
					    
					},
					failure: function() {

					},
					scope: this
				});
			},
			interval: this.interval * 1000,
			scope: this
		};
		this.startTimer();
		UserSessionCheck.superclass.constructor.call(this, config);
	},
    
    initComponents2 : function(){
        var loginPanel = Ext.create('Ext.form.Panel', {
			url: "index.php?eID=monoloopaccounting&pid=1&cmd=login/l2",
			id: "ml_loginform", 
			defaultType: 'textfield',
			scope: this,  
			bodyStyle: "padding: 5px 5px 3px 5px; border-width: 0; margin-bottom: 7px;",
			defaults : {
				anchor : '100%'	
			} ,
			items: [{
					xtype: "panel",
					height : 55 ,
					bodyStyle: "margin-bottom: 7px; border: none;",
					html: '<div id="monoloop-logo"></div><div id="monoloop-login-msg"></div>'
				},{
					fieldLabel: 'Password',
					name: "pass",
					width: 200,
					
					id: "ml_password",
					inputType: "password" , 
                    allowBlank:false , 
                    listeners: {
                      specialkey: function(f,e){
                        if (e.getKey() == e.ENTER) {
                          monoloop_base.login_refresh.submitForm() ;
                        }
                      }
                    }
				},{
					inputType: "hidden",
					name: "userM",
					id: "login_username",
					value: monoloop_base.username
				} 
			] ,
			buttons: [{
				text: 'Log in',
				formBind: true,
				handler: this.submitForm
			}, {
				text: 'Log out',
				formBind: true,
				handler: function() {
				    document.monolooplogout.submit() ;
				}
			}]  
		});
        
        
        this.loginRefreshWindow = Ext.create('Ext.window.Window',{
			id: "ml_loginformWindow",
			width: 340,
			autoHeight: true,
			closable: false,
			resizable: false,
			plain: true,
			border: false,
            baseCls : 'ml-login-window' ,
			modal: true,
			draggable: false,
			layout : 'fit' , 
			items: [loginPanel],
			listeners: {
				activate: function() {
					Ext.getCmp('ml_password').focus(false, 800);
				}
			}
		});
    } , 
    
    submitForm : function(){
        var form = Ext.getCmp('ml_loginform').form ;
        if(! form.isValid())
            return ; 
        form.submit({
			method: "POST",
			waitTitle: 'Processing',
			waitMsg: " ",
			params: {
				"eID": "monoloopaccounting",
                "pid": 1 , 
                "cmd": 'login/l2' ,
				"login_status": "login"
			},
			success: function(form, action) { 
			     monoloop_base.login_refresh.startTimer();
                 Ext.getCmp('ml_password').setValue('') ; 
			     Ext.getCmp('ml_loginformWindow').hide() ; 
                 Ext.fly('monoloop-login-msg').update('') ; 
			},
			failure: function(form, action) { 
				alert(1) ;
			     Ext.fly('monoloop-login-msg').update(action.result.msg) ; 
			}
		});
    } ,
 

	startTimer: function() {
		Ext.TaskManager.start(this.loadingTask);
	},

	stopTimer: function() {
		Ext.TaskManager.stop(this.loadingTask);
	} 
}) ; 