var monoloop_base = {
    loginRefresh : null , 
    username : '' , 
    pid :  0 , 
    enableActiveGroup : false , 
    cssDomain : '' , 
    showProgressbar : function(msg1){
        Ext.MessageBox.show({
           title: 'Please wait',
           msg: 'Monoloop ...',
           progressText: msg1,
           width:300,
           progress:true,
           closable:false 
       });

    } , 
    
    msgCloseOnClick : function(parentNode , title , s ){
        msgCt = Ext.DomHelper.insertFirst(parentNode, {id:'msg-div-2'}, true);
        msgCt.alignTo(document, 't-t');
        //var s = String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.DomHelper.append(msgCt, {html:monoloop_base.createBox(title, s)}, true);
        m.slideIn('t') ; 
		
		msgCt.addListener('click' , function(){ 
            m.pause(1).ghost("t", {remove:true});
        }) ; 	 
         
    } , 
    
    msgWithParentDom : function(parentNode , title , s ){
        msgCt = Ext.DomHelper.insertFirst(parentNode, {id:'msg-div-2'}, true);
        msgCt.alignTo(document, 't-t');
        //var s = String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.DomHelper.append(msgCt, {html:monoloop_base.createBox(title, s)}, true);
        if( Ext.isIE ){ 
            m.slideIn('t').pause(5).ghost("t", {remove:true});
        }else{
            Ext.getBody().addListener('mousemove' , function(){ 
                m.pause(1).ghost("t", {remove:true});
            }) ; 
        }
        //m.slideIn('t') ; 
         
    } , 
     
    msg : function(title, format){
        msgCt = Ext.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
        msgCt.alignTo(document, 't-t');
        var s = String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.DomHelper.append(msgCt, {html:monoloop_base.createBox(title, s)}, true);
        //m.slideIn('t').pause(1).ghost("t", {remove:true});
        m.slideIn('t') ; 
       
        Ext.getBody().addListener('mousemove' , function(){ 
            m.pause(1).ghost("t", {remove:true});
        }) ; 
       
    },
    
    createBox : function(t, s){
        return ['<div class="msg">',
                '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
                '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3>', t, '</h3>', s, '</div></div></div>',
                '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
                '</div>'].join('');
    }  
} ; 

 
Ext.namespace('Ext.ux.MONOLOOP');

Ext.ux.MONOLOOP.loginRefresh = Ext.extend(Ext.util.Observable, {
	locked: 0,
	interval: 60,

	constructor: function(config) {
		config = config || {};
		Ext.apply(this, config);
		this.initComponents2();
		this.loadingTask = {
			run: function(){
				// interval run
				Ext.Ajax.request({
					url: "index.php",
					params: {
						"eID": "monoloop_base",
						"skipSessionUpdate": 1 , 
                        "cmd" : "chkSession"
					},
					method: "GET",
					success: function(response, options) {
					    var result = Ext.util.JSON.decode(response.responseText);
                        if( result.timeout){
                            this.stopTimer();
					        Ext.getCmp('ml_loginformWindow').show() ; 
                        }
					    
					},
					failure: function() {

					},
					scope: this
				});
			},
			interval: this.interval * 1000,
			scope: this
		};
		this.startTimer();
		Ext.ux.MONOLOOP.loginRefresh.superclass.constructor.call(this, config);
	},
    
    initComponents2 : function(){
        var loginPanel = new Ext.FormPanel({
			url: "index.php?eID=monoloopaccounting&pid=1&cmd=login/l2",
			id: "ml_loginform", 
			defaultType: 'textfield',
			scope: this,
			width: "100%",
			bodyStyle: "padding: 5px 5px 3px 5px; border-width: 0; margin-bottom: 7px;",

			items: [{
					xtype: "panel",
					bodyStyle: "margin-bottom: 7px; border: none;",
					html: '<div id="monoloop-logo"></div><div id="monoloop-login-msg"></div>'
				},{
					fieldLabel: 'Password',
					name: "pass",
					width: 200,
					id: "ml_password",
					inputType: "password" , 
                    allowBlank:false , 
                    listeners: {
                      specialkey: function(f,e){
                        if (e.getKey() == e.ENTER) {
                          monoloop_base.loginRefresh.submitForm() ;
                        }
                      }
                    }
				},{
					inputType: "hidden",
					name: "userM",
					id: "login_username",
					value: monoloop_base.username
				} 
			] ,
			buttons: [{
				text: 'Log in',
				formBind: true,
				handler: this.submitForm
			}, {
				text: 'Log out',
				formBind: true,
				handler: function() {
				    document.monolooplogout.submit() ;
				}
			}]  
		});
        
        
        this.loginRefreshWindow = new Ext.Window({
			id: "ml_loginformWindow",
			width: 340,
			autoHeight: true,
			closable: false,
			resizable: false,
			plain: true,
			border: false,
            baseCls : 'ml-login-window' ,
			modal: true,
			draggable: false,
			items: [loginPanel],
			listeners: {
				activate: function() {
					Ext.getCmp('ml_password').focus(false, 800);
				}
			}
		});
    } , 
    
    submitForm : function(){
        var form = Ext.getCmp('ml_loginform').form ;
        if(! form.isValid())
            return ; 
        form.submit({
			method: "POST",
			waitTitle: 'Processing',
			waitMsg: " ",
			params: {
				"eID": "monoloopaccounting",
                "pid": 1 , 
                "cmd": 'login/l2' ,
				"login_status": "login"
			},
			success: function(form, action) { 
			     monoloop_base.loginRefresh.startTimer();
                 Ext.getCmp('ml_password').setValue('') ; 
			     Ext.getCmp('ml_loginformWindow').hide() ; 
                 Ext.fly('monoloop-login-msg').update('') ; 
			},
			failure: function(form, action) { 
			     Ext.fly('monoloop-login-msg').update(action.result.msg) ; 
			}
		});
    } ,
 

	startTimer: function() {
		Ext.TaskMgr.start(this.loadingTask);
	},

	stopTimer: function() {
		Ext.TaskMgr.stop(this.loadingTask);
	} 
});



/**
 * Initialize login expiration warning object
 */
Ext.onReady(function() {
	
});
