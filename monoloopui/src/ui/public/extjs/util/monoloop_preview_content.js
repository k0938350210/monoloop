ml_preview_content = {
    openMainWindow : function(contentId , header){
    	var query = '' ;  
    	if( arguments[2] )
    		query = arguments[2] ;	
        var showWindow = new Ext.Window({
            width:  1000 ,
         	height: 350 , 
         	title:"Preview: " + header,
         	autoScroll:true,
         	id : 'monoloop_preview_window' ,
         	modal:true,
         	constrain : true , 
         	maximizable : true  , 
         	animateTarget:"btnHello" ,
    		html : '<iframe width="100%" height="100%" onload="ml_preview_content.onload();" id="monoloop-preview-window"  frameborder="0" src="index.php?id=387&no_cache=1&uid='+contentId+'&query='+query+'" ><p>Your browser does not support iframes.</p></iframe>'   , 
            listeners : {
                'close' : function(win){
                    showWindow.destroy() ; 
                    Ext.MessageBox.hide();
                } , 
                'afterrender' : function(){
                	
 					Ext.MessageBox.hide();
                }
            }
        }) ; 
        if( contentId != 0){
            monoloop_base.showProgressbar('Retrieving Content...') ;     
        }
        
     	showWindow.show();
    } , 
    
    onload : function(){
        Ext.MessageBox.hide();
        
        var scnWid = 0 ;
        var scnHei = 0 ;
    	if (self.innerHeight) // all except Explorer
    	{
    		scnWid = self.innerWidth;
    		scnHei = self.innerHeight;
    	}
    	else if (document.documentElement && document.documentElement.clientHeight)
    		// Explorer 6 Strict Mode
    	{
    		scnWid = document.documentElement.clientWidth;
    		scnHei = document.documentElement.clientHeight;
    	}
    	else if (document.body) // other Explorers
    	{
    		scnWid = document.body.clientWidth;
    		scnHei = document.body.clientHeight;
    	}
    	scnWid = scnWid - 50 ; 
    	scnHei = scnHei - 50 ; 
    	
    	var newheight;
	    var newwidth;
	
	    if(document.getElementById){
	        newheight=document.getElementById('monoloop-preview-window').contentWindow.document.body.scrollHeight + 50; 
	    } 
	    if( newheight > scnHei ){
	    	newheight = scnHei ; 
	    }
	    Ext.getCmp('monoloop_preview_window').setHeight(newheight) ; 
	    Ext.getCmp('monoloop_preview_window').center() ; 
    }
} ; 