list_customfield = {
    locationID : 0 , 
    objectReturn : null ,
    openWindow : function(){
        var minStore = new Ext.data.JsonStore({
            fields : ['group'] , 
            proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=insertfield/groupList'
	        }) 
        }) ;
        
        var store = new Ext.data.JsonStore({
	        root: 'topics',
	        totalProperty: 'totalCount',
	        idProperty: 'idunique_type',
	        remoteSort: true,
	
	        fields: [
	        	{name: 'label'} , 
	            {name: 'name'},
	 			{name: 'type'},
	 			{name: 'connector'},
	 			{name: 'group'} 
	        ],
	
	        // load using script tags for cross domain, if the data in on the same domain as
	        // this page, an HttpProxy would be better
	        proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=t3p_conditional_content&pid=1&cmd=insertfield/list'
	        })
	    });
	    store.setDefaultSort('name', 'asc');
        
        function renderSendBtn(value, id, r){
            var id = Ext.id();
            createSelectButton.defer(1, this, ['Select', id , r]); 
            return('<table><tr><td><div id="' + id + '"></div></td></tr></table>');
        }
         
         function createSelectButton(value, id, record) {
            new Ext.Button({
                text: value 
                ,handler : function(btn, e) { 
                    if( list_customfield.locationID == 3){
                        list_customfield.objectReturn.saveData('<mlt insert="'+record.get('connector') + '.' + record.get('name')+'"/>') ; 
                    }else{
                        if( list_customfield.locationID == 1)
                            var myQuery = document.getElementById('pw-subjectline') ;
                        else 
                            var myQuery = document.getElementById('mn-mail-textEditor') ;
                        var chaineAj = '<mlt insert="'+record.get('connector') + '.' + record.get('name')+'"/>' ; 
                        if (document.selection) {
                            myQuery.focus();
                            sel = document.selection.createRange();
                            sel.text = chaineAj; 
                        }
                        //MOZILLA/NETSCAPE support
                        else if (myQuery.selectionStart || myQuery.selectionStart == "0") {
                            var startPos = myQuery.selectionStart;
                            var endPos = myQuery.selectionEnd;
                            var chaineSql = myQuery.value;
                        
                            myQuery.value = chaineSql.substring(0, startPos) + chaineAj + chaineSql.substring(endPos, chaineSql.length);
                        } else {
                            myQuery.value += chaineAj;
                        }
                    }
                    
                    
                    Ext.getCmp('list-field-window').close() ;  
                }
            }).render(document.body, id);
        }
        
        
        
        var headerPanel = new Ext.Panel({
            width:  '100%' ,
            border : false , 
            bodyBorder : false , 
            region : 'north' , 
            bodyStyle  : 'background-color: rgb(189,199,194); padding:10px  ;' , 
            height: 40 ,  
            items: [   
                 {
                xtype: 'compositefield',
                msgTarget : 'side', 
                items: [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    html : 'Group : ' , 
                    width : 50 
                }, {
                    xtype:          'combo',  
                    allowBlank: true  ,
                    hideLabel : true , 
                    triggerAction:  'all',
                    editable:       false,
                    emptyText : 'Select group to filter' , 
                    name: 'mail_template', 
                    displayField:   'group',
                    valueField:     'group',
                    id : 'customfield-group' ,   
                    store:  minStore,
                    validateOnBlur : false , 
                    listeners : {
                        'change' : function(combo , newValue , oldValue){
                            list_customfield.refreshGrid() ;    
                        }
                    }  
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    html : '' , 
                    width : 20 
                }, {
                    xtype     : 'textfield',
                    name      : 'url',
                    emptyText : 'search...' , 
                    id        : 'customfield-search', 
                    enableKeyEvents : true , 
                    width    : 120  ,
                    listeners : {
                        'keypress' : function(textField ,e){
                            if(e.keyCode == e.ENTER) {
                                  list_customfield.refreshGrid() ;    
                            }
                        }
                    }
                } ]
            } 
            ] 
        }) ; 
        
        var grid = new Ext.grid.GridPanel({
            //width:735,
            width:'100%',
            height:540,
            id : 'lm-customfield-grid' ,  
            trackMouseOver:false,
            region : 'center' , 
	        //disableSelection:false,
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
	        enableDragDrop: true,
			ddGroup: 'grid2tree',
            store: store , 
            // grid columns
            columns:[/*{
                header: "Connector",
                dataIndex: 'connector',
                width: 80,
                sortable: false
            },{
                header: "Titel",
                dataIndex: 'name',
                width: 80,
                sortable: false
            },*/{
                header: "Goupname",
                dataIndex: 'group',
                width: 80,
                sortable: false 
            },{
                header: "Label",
                dataIndex: 'label',
                width: 80,
                sortable: false 
            }/*,{
                header: "Data type",
                dataIndex: 'type',
                width: 80,
                sortable: false 
            }*/,{
	            header: "Actions",
                id : 'actions' , 
	            renderer: renderSendBtn, 
	            dataIndex: 'idunique_type',
	            width: 55,
	            sortable: false
	        } ], 
           
            viewConfig: {
                forceFit:true 
     
            }  
        });
        
        
        
        var w = new Ext.Window({
            title: 'Select custom field',
            id : 'list-field-window' , 
            closable:true,
            width:500,
            height:450,
            modal : true , 
            //border:false,
            layout : 'border' , 
            plain:true , 
            items: [headerPanel , grid ] 
        }) ;  
        list_customfield.refreshGrid() ;    
        
        w.show() ; 
    } , 
    
    refreshGrid : function(){
        
        Ext.getCmp('lm-customfield-grid').store.baseParams = {
            'search' : Ext.getCmp('customfield-search').getValue() , 
            'group' : Ext.getCmp('customfield-group').getValue()
        }
        
        Ext.getCmp('lm-customfield-grid').store.load({params:{start:0, limit:10000}}); 
    }
} ; 