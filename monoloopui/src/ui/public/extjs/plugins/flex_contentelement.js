flex_contentelement = {
    returnID : 0 , 
    value : 0 , 
    open : function(returnID , value){
        this.returnID = returnID ; 
        this.value = value ;
        var window = new Ext.Window({
            id : 'ce-window' , 
            width : 700 , 
            height : 450 , 
            title:"Content Element",
            plain:true , 
         	modal:true,
         	maximizable : false  , 
         	closable:true ,  
            items:[ this.initData() ]  , 
            listeners : {
                'beforeclose' : function(w){
                     var totalLeft = Ext.getCmp('ce-grid-left').store.getCount() ; 
                     var retStr = '' ; 
                     for( var i = 0 ; i < totalLeft ; i++){
                        var uid = Ext.getCmp('ce-grid-left').store.getAt(i).get('uid') ; 
                        if( retStr == '')
                            retStr = uid ;
                        else
                            retStr = retStr + ',' + uid ;  
                     }
                     
                     Ext.getCmp(flex_contentelement.returnID).setValue(retStr) ; 
                }  
            } 
        }) ; 
        window.show();
    } , 
    
    initData : function(){
        
        function renderAction(value, p, r){
		    var ret =  '<table width="100%"><tr align="center" >' + 
		    			    '<td><a href="javascript:;" onclick="flex_contentelement.removeRecord(\''+r.id+'\')"><img src="typo3conf/ext/t3p_base/image/delete.gif" title="Remove" ></a></td>' +
					   '</tr></table>'  ;   
			return ret ;
	    }
        
        var leftGridStore = new Ext.data.JsonStore({
	        root: 'topics',
	        totalProperty: 'totalCount',
	        idProperty: 'uid',
	        remoteSort: true, 
	        fields:  [
    	        {name: 'uid', type: 'int'}, 
  	            {name: 'header'}  
  	        ],
	        proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=simple_newsletters&pid=1&bn[cmd]=contentSelectedList'
	        }) , 
	        baseParams:{ 
	            value : flex_contentelement.value 
			}
	    });
         
        var leftGrid = new Ext.grid.GridPanel({
            id : 'ce-grid-left' , 
	        width:'100%',
            height:390,
            store: leftGridStore,
	        trackMouseOver:false, 
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
            columns:[ 
            {
	            header: "Title",
	            dataIndex: 'header',
	            width: 150, 
	            sortable: false   
	        },{
	        	id : 'sn_action' , 
	            header: "Action",
	            renderer: renderAction, 
	            dataIndex: 'uid',
	            width: 40,
	            sortable: false
	        }]
        }) ; 
        
        leftGridStore.load({params:{start:0, limit:1000}});
        
        
        function renderAction2(value, p, r){
		    var ret =  '<table width="100%"><tr align="center" >' + 
		    			    '<td><a href="javascript:;" onclick="flex_contentelement.addRecord('+r.id+')"><img src="typo3conf/ext/t3p_base/image/newpage.gif" title="ADD" ></a></td>' +
					   '</tr></table>'  ;   
			return ret ;
	    }
        
        var rightGridStore = new Ext.data.JsonStore({
	        root: 'topics',
	        totalProperty: 'totalCount',
	        idProperty: 'uid',
	        remoteSort: true,
	
	        fields: [
	            'title', 
				'newsname', 
	            'CType' ,
	            {name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'} ,
	            {name: 'uid', type: 'int'},
	            {name: 'pid', type: 'int'},
                {name: 'tx_templavoila_to' , type : 'int'}
	        ],

	        proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=simple_newsletters&pid=1&bn[cmd]=listContent'
	        }),
	        
	        baseParams:{
				idfolder : 0,
				typeDefault : 'content'
			}
	    });
	    rightGridStore.setDefaultSort('crdate', 'desc');
        
        var rightGrid = new Ext.grid.GridPanel({
	        id : 'ce-grid-right' ,   
	        width:'100%',
	        height:390, 
	        store: rightGridStore,
	        trackMouseOver:false, 
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,  
	        columns:[{
	            id: 'titel',
	            header: "Titel",
	            dataIndex: 'title',
	            width: 160,
	            sortable: true
	        },{
	            header: "Type",
	            dataIndex: 'CType',
	            width: 100,
	            sortable: true
	        },{
	            header: "Date",
	            dataIndex: 'crdate',
	            width: 90,
	            renderer: Ext.util.Format.dateRenderer('d M Y'),
	            sortable: true   
	         },{
	        	id : 'sn_action' , 
	            header: "Actions",
	            renderer: renderAction2, 
	            dataIndex: 'uid',
	            width: 40,
	            sortable: true
	        }],
	
	        // customize view config
	       
	        viewConfig: {
	            forceFit:true
	        }, 
	
	        // paging bar on the bottom
	        bbar: new Ext.PagingToolbar1({
	            pageSize: 13,
                pageLimit : 5  , 
	            store: rightGridStore,
	            displayInfo: true,
	            displayMsg: 'Displaying {0} of {1}',
	            emptyMsg: "No topics to display" 
	        })
	    });		
	    // trigger the data store load
	    rightGridStore.load({params:{start:0, limit:13}});
        
        
        
        
        var myPanel = new Ext.Panel({
            id : 'ce-panel' , 
            layout:'column',
            width:'100%' , 
            items:[
            {
                xtype : 'panel' , 
                columnWidth:.3, 
                title : 'Select content element', 
                items :
                [
                leftGrid
                ] 
            },
            {
                xtype : 'panel' , 
                columnWidth:.7, 
                title : 'Content element', 
                items :
                [
                rightGrid
                ] 
            }
            ]    
        }) ; 
        return myPanel ; 
    } , 
    
    addRecord : function(uid){
        var record = Ext.getCmp('ce-grid-right').store.getById(uid) ; 
        //console.debug(record) ; 
        var data = {} ; 
        data['uid'] = record.get('uid')  ; 
        data['header'] =record.get('title') ;  
        var newRecord = new Ext.data.Record() ; 
        newRecord.data = data ; 
        
        Ext.getCmp('ce-grid-left').store.add(newRecord) ;  
        Ext.getCmp('ce-grid-left').store.commitChanges() ;
    } , 
    
    removeRecord : function(id){
        Ext.getCmp('ce-grid-left').store.remove(Ext.getCmp('ce-grid-left').store.getById(id)) ;  
        Ext.getCmp('ce-grid-left').store.commitChanges() ;
    }
} ; 