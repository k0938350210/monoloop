/**
 * @author Shea Frederick - http://www.vinylfox.com
 * @class Ext.ux.form.HtmlEditor.Link
 * @extends Ext.util.Observable
 * <p>A plugin that creates a button on the HtmlEditor for inserting a link.</p>
 */
Ext.ux.form.HtmlEditor.CBuilder = Ext.extend(Ext.util.Observable, {
    // Link language text
    langTitle   : 'Highlight your content and add Conditions (opens the Condition Builder).',
    langInsert  : 'Insert',
    langCancel  : 'Cancel',
    langTarget  : 'Target',
    langURL     : 'URL',
    langText    : 'Text',
    selectEl    : null , 
    displayContition : false , 
    textHighLight : false , 
 
    init: function(cmp){
        cmp.enableLinks = false;
        this.cmp = cmp;
        this.cmp.on('render', this.onRender, this);
    },
    
    saveCBuilder : function(conditionStr){
        var i =  this.findLastIndex(conditionStr , '|') ;
        if( i == -1 )
            return ; 
        //console.debug(i) ;     
        var conStr1 =  conditionStr.substr( 0 , i  ) ; 
        //console.debug(conStr1) ; 
        var sel = this.cmp.getSelectedText(false) ; 
        //New version of inline placement .
        var inlinePlacementHTML = '<mlt  condition="' +  conditionStr + '">' +  sel.html + '</mlt>' ; 
        //var inlinePlacementHTML = '<span class="condition-main" style="display:inline;" ><span class="condition" style="color: #F3A73F;display:inline;">'+conStr1+'</span><span class="condition2" style="color: #F3A73F;display:inline;">[</span><span class="condition-content" style="display:inline;">' + sel.html + '</span><span class="condition2" style="color: #F3A73F;display:inline;" >]<img src="./typo3conf/ext/t3p_base/image/conditionmarker.gif"/></span><span class="condition" style="color: #F3A73F;display:inline;"  >}</span></span>' ;
        //alert(this.cmp.iframe.contentWindow.document.body.innerHTML) ; 
        this.cmp.insertAtCursor('###-monoloop-inline-content-###') ;
        //alert(this.cmp.iframe.contentWindow.document.body.innerHTML) ; 
        this.cmp.iframe.contentWindow.document.body.innerHTML = this.cmp.iframe.contentWindow.document.body.innerHTML.replace("###-monoloop-inline-content-###" , inlinePlacementHTML) ; 
        //this.showHideCondition(false) ; 
    },
    
    saveCBuilder2 : function(conditionStr){
        var i =  this.findLastIndex(conditionStr , '|') ;
        if( i == -1 )
            return ; 
        //console.debug(i) ;     
        var conStr1 =  conditionStr.substr( 0 , i  ) ; 
        
        if( this.selectEl == null)
            return ;
            
        //this.selectEl.childNodes[0].innerHTML = conStr1  ;  
        this.selectEl.setAttribute('condition' , conditionStr ) ; 
    },
    
    findLastIndex : function(str , findStr){
        var index = -1  ; 
        while(1){
            var index2 = str.indexOf(findStr , index + 1) ;
            if(index2 == -1)
                break ; 
            index = index2 ; 
        }
        return index ;  
    }, 
    
    findConditionStr : function(conditionStr){
        var i =  this.findLastIndex(conditionStr , '|') ;
        if( i == -1 )
            return ; 
        //console.debug(i) ;     
        var conStr1 =  conditionStr.substr( 0 , i  ) ; 
        return conStr1 ; 
    } , 
    
    isSelectInsideConditional : function(el){
        //console.debug(el.nodeName) ; 
        while(el.nodeName != "BODY"){
            //console.debug(el.nodeName) ; 
			//if(this.cmp.ml_hasClass(el , "condition-main"))
            if(el.nodeName == "MLT")
				return true ; 
			el = el.parentNode ; 	
		}
        return false ; 
    } , 
    
    getParentConditionalElement : function(el){
        while(el.nodeName != "BODY"){
            //console.debug(el.nodeName) ; 
			//if(this.cmp.ml_hasClass(el , "condition-main"))
            if(el.nodeName == "MLT")
				return el ; 
			el = el.parentNode ; 	
		}
        return false ; 
    } , 
    
    clearConditionDisplay : function(contentElement){
        var firstEl = contentElement.firstChild ;
        if(this.cmp.ml_hasClass(firstEl , "mlt-c") ){
            contentElement.removeChild(firstEl) ; 
        }
        var lastEl = contentElement.lastChild ; 
        if(this.cmp.ml_hasClass(firstEl , "mlt-c") ){
            contentElement.removeChild(lastEl) ; 
        }
    } , 
    
    showHideCondition : function(change){ 
        //alert( this.displayContition ) ; 
        this.displayContition = change; 
 
        var content = this.cmp.iframe.contentWindow.document.body.getElementsByTagName('mlt');
        
        // This should remove all content ; 
        for (var i=0; i < content.length; i++) { 
            var conditionAtr = content[i].getAttribute('condition') ; 
            if( conditionAtr == null)
                continue ; 
                   
            this.clearConditionDisplay(content[i]) ; 
            
            if( content[i].innerHTML == ''){
                // IE not support this thing ; 
                continue  ;     
            }
            
            if(this.displayContition == false){
                var firstEl = content[i].firstChild ; 
                var newItem = document.createElement('span') ;  
                this.cmp.ml_addClass(newItem , 'mlt-c') ;
                newItem.innerHTML = '[' ; 
                newItem.style.color = "orange" ; 
                firstEl.parentNode.insertBefore(newItem , firstEl ) ; 
                
                var lastEl = content[i].lastChild ; 
                var newItem = document.createElement('span') ;  
                this.cmp.ml_addClass(newItem , 'mlt-c') ;
                newItem.innerHTML = ']' ;
                newItem.style.color = "orange" ; 
                lastEl.parentNode.appendChild(newItem ) ;  
            }else{
                var conditionStr = this.findConditionStr(conditionAtr) ;  
                // At first index ; 
                var firstEl = content[i].firstChild ; 
                if( firstEl == null){
                    content[i].innerHTML = '<span class="mlt-c" style="color:orange">' + conditionStr + '</span>' +  content[i].innerHTML + '<span class="mlt-c" style="color:orange">}</span>' ;
                    continue ;  
                }else{
                    var newItem = document.createElement('span') ;  
                    this.cmp.ml_addClass(newItem , 'mlt-c') ;
                    newItem.innerHTML = conditionStr ; 
                    newItem.style.color = "orange" ; 
                    firstEl.parentNode.insertBefore(newItem , firstEl ) ; 
                }
                
                // At last index ; 
                var lastEl = content[i].lastChild ; 
                var newItem = document.createElement('span') ;  
                this.cmp.ml_addClass(newItem , 'mlt-c') ;
                newItem.innerHTML = '}' ;
                newItem.style.color = "orange" ; 
                lastEl.parentNode.appendChild(newItem ) ;  
            }
                
            
        }
        
        return ; 
        var content = this.cmp.iframe.contentWindow.document.body.getElementsByTagName('span');
        if(this.textHighLight){
			this.showTextHighlightCondition();
		}
        if(change)
            this.displayContition = !this.displayContition ; 
        for (var i=0; i < content.length; i++) {
        	//alert( content[i].className ) ; 
        	if(this.cmp.ml_hasClass(content[i] , "condition") ){
        		if (this.displayContition){  
                    content[i].style.display = 'inline' ; 
        		}else{
                    this.cmp.ml_addClass(content[i] , "condition-disable") ;
                    if( Ext.isIE ){
                        content[i].style.display = 'inline' ; 
                    }else{
                        content[i].style.display = 'none' ; 
                    }
        		}
        	}
            
            if(this.cmp.ml_hasClass(content[i] , "condition2") ){
        		if (!this.displayContition){  
                    content[i].style.display = 'inline' ; 
        		}else{
                    this.cmp.ml_addClass(content[i] , "condition-disable") ;
                    if( Ext.isIE ){
                        content[i].style.display = 'inline' ; 
                    }else{
                        content[i].style.display = 'none' ; 
                    }
        		}
        	}
        }
    } , 
    
    showTextHighlightCondition : function(){
        var content = this.cmp.iframe.contentWindow.document.body.getElementsByTagName('span');
		if( this.displayContition){
			this.showHideCondition(true);
		}
		this.textHighLight = !this.textHighLight ;
		for (var i=0; i < content.length; i++) {
 
			if(this.cmp.ml_hasClass(content[i] , "condition-content")){
				if(this.textHighLight){
					this.cmp.ml_addClass(content[i] , "condition-hightlight") ; 
				}else{
					this.cmp.ml_removeClass(content[i] , "condition-hightlight") ; 
				}
			}
            
		}
    } , 
    
    onRender: function(){
        var me = this ; 
        /*
        var d = this.cmp.nutgetdoc() ; 
        console.debug(d) ; 
        d.xhtml  = "0" ; 
        d.designMode = 'on' ; 
        */
        
        //console.debug(this.cmp) ;   
        
        var cmp = this.cmp;
        var btn = this.cmp.getToolbar().insertButton( 0 , {
            iconCls: 'x-edit-wizard',
            cls : 'ml-btn' ,   
            handler: function(){
                var selObj = this.cmp.nuttest() ; 
                var focusNode = null ; 
                if( Ext.isIE ){
                     this.cmp.win.focus();
                     var tRange = selObj.createRange();
                     focusNode = tRange.parentElement() ; 
                }else{
                     focusNode = selObj.focusNode  ;  
                }
               
    
                 
                if( ! this.isSelectInsideConditional(focusNode) ){
                    condition.stage = 5  ; 
                    condition.preconditionStr = '' ; 
                    condition.objectReturn = this ; 
                    condition.openMainWindow() ; 
                    //this.saveCBuilder('if (    ( $Example->FirstName === \'Nut\' )  ){|}') ; 
                }else{
                    
                    var sel = this.cmp.getSelectedText(false) ; 
                    if( sel.html == ''){
                        var el = this.getParentConditionalElement(focusNode) ; 
                        this.selectEl = el ;  
                        //var conditionStr = el.childNodes[0].innerHTML + ' | ' + el.childNodes[4].innerHTML ; 
                        var conditionStr = el.getAttribute('condition') ;  
                        condition.stage = 6  ; 
                        condition.preconditionStr = conditionStr  ; 
                        condition.objectReturn = this ; 
                        condition.openMainWindow() ; 
                    }else{
 
                        condition.stage = 5  ; 
                        condition.preconditionStr = '' ; 
                        condition.objectReturn = this ; 
                        condition.openMainWindow() ; 
                    }
                    
                }
   
            },
            scope: this ,
            tooltip: this.langTitle
        });
        
        var btn = this.cmp.getToolbar().insertButton( 1 , {
            iconCls: 'x-edit-wizard1',
            cls : 'ml-btn3' , 
            id : 'html-editor-condition-show-hide1' , 
            handler: function(){
                this.showHideCondition(true) ; 
                if( this.displayContition){
                    Ext.getCmp('html-editor-condition-show-hide1').setVisible(false) ; 
                    Ext.getCmp('html-editor-condition-show-hide2').setVisible(true) ; 
                }else{
                    Ext.getCmp('html-editor-condition-show-hide1').setVisible(true) ; 
                    Ext.getCmp('html-editor-condition-show-hide2').setVisible(false) ; 
                }
                
            },
            scope: this ,
            tooltip: 'Show \'Conditions\' code view.'
        });
         
        var btn = this.cmp.getToolbar().insertButton( 2 , {
            iconCls: 'x-edit-wizard2',
            cls : 'ml-btn5' , 
            id : 'html-editor-condition-show-hide2' , 
            hidden : true , 
            handler: function(){
                 this.showHideCondition(false) ; 
                 if( this.displayContition){
                    Ext.getCmp('html-editor-condition-show-hide1').setVisible(false) ; 
                    Ext.getCmp('html-editor-condition-show-hide2').setVisible(true) ; 
                }else{
                    Ext.getCmp('html-editor-condition-show-hide1').setVisible(true) ; 
                    Ext.getCmp('html-editor-condition-show-hide2').setVisible(false) ; 
                }
            },
            scope: this ,
            tooltip: 'Hide \'Conditions\' code view.'
        });
         
        var btn = this.cmp.getToolbar().insertButton(3 , {
            iconCls: 'x-edit-wizard3',
            cls : 'ml-btn6' ,   
            handler: function(){
                // Open Asset Selector window ; 
                me.openAssetSelectorWindow() ; 
       
            },
            scope: this ,
            tooltip: 'Add Assets'
        });  
    } , 
    
    saveAssets : function(){
        this.cmp.insertAtCursor('<mlt asset="my_first_asset.product_name" property="SomePropertyFromConditionBuilder" />') ;
    } , 
    
    openAssetSelectorWindow : function(){
        var me = this ; 
        
        var minStore = new Ext.data.JsonStore({
            fields : ['name'] , 
            proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=monoloop_assets&pid=1&cmd=assets/allAssets'
	        }) 
        }) ;
        
        var store = new Ext.data.JsonStore({
	        root: 'topics',
	        totalProperty: 'totalCount',
	        idProperty: 'idunique_type',
	        remoteSort: true,
	
	        fields: [
	        	{name: 'assets_name'} , 
	            {name: 'field_name'},
	 			{name: 'fieldType'} 
	        ],
	
	        // load using script tags for cross domain, if the data in on the same domain as
	        // this page, an HttpProxy would be better
	        proxy: new Ext.data.HttpProxy({
	            url: 'index.php?eID=monoloop_assets&pid=1&cmd=assets/assetSelectorList'
	        })
	    });
	    store.setDefaultSort('m.name', 'asc');
        
        function renderSendBtn(value, id, r){
            var id = Ext.id();
            createSelectButton.defer(1, this, ['Select', id , r]); 
            return('<table><tr><td><div id="' + id + '"></div></td></tr></table>');
        }
         
         function createSelectButton(value, id, record) {
            new Ext.Button({
                text: value 
                ,handler : function(btn, e) { 
                     me.openAssetPropertyWindow() ; 
                }
            }).render(document.body, id);
        }
        
        
        var headerPanel = new Ext.Panel({
            width:  '100%' ,
            border : false , 
            bodyBorder : false , 
            region : 'north' , 
            bodyStyle  : 'background-color: rgb(189,199,194); padding:10px  ;' , 
            height: 40 ,  
            items: [   
                 {
                xtype: 'compositefield',
                msgTarget : 'side', 
                items: [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    html : 'Group : ' , 
                    width : 50 
                }, {
                    xtype:          'combo',  
                    allowBlank: true  ,
                    hideLabel : true , 
                    triggerAction:  'all',
                    editable:       false,
                    emptyText : 'Select group to filter' , 
                    name: 'mail_template', 
                    displayField:   'name',
                    valueField:     'name',
                    id : 'assets-cb' ,   
                    store:  minStore,
                    validateOnBlur : false , 
                    listeners : {
                        'change' : function(combo , newValue , oldValue){
                             me.refreshAssetData() ; 
                        }
                    }  
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    html : '' , 
                    width : 20 
                }, {
                    xtype     : 'textfield',
                    name      : 'url',
                    emptyText : 'search...' , 
                    id        : 'customfield-search', 
                    enableKeyEvents : true , 
                    width    : 120  ,
                    listeners : {
                        'keypress' : function(textField ,e){
                            if(e.keyCode == e.ENTER) {
                                 me.refreshAssetData() ; 
                            }
                        }
                    }
                } ]
            } 
            ] 
        }) ; 
        
        var grid = new Ext.grid.GridPanel({
            //width:735,
            width:'100%',
            height:540,
            id : 'lm-assets-grid' ,  
            trackMouseOver:false,
            region : 'center' ,  
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
	        enableDragDrop: true,
			ddGroup: 'grid2tree',
            store: store ,  
            columns:[{
                header: "Asset Name",
                dataIndex: 'assets_name',
                width: 80,
                sortable: false 
            },{
                header: "Field",
                dataIndex: 'field_name',
                width: 80,
                sortable: false 
            },{
                header  : 'Type' , 
                dataIndex : 'fieldType' , 
                width : 100 , 
                sortable: false 
            },{
	            header: "Actions",
                id : 'actions' , 
	            renderer: renderSendBtn, 
	            dataIndex: 'idunique_type',
	            width: 55,
	            sortable: false
	        } ], 
           
            viewConfig: {
                forceFit:true 
     
            }  
        });
        
        
        
        var w = new Ext.Window({
            title: 'Select asset field',
            id : 'list-assets-window' , 
            closable:true,
            width:500,
            height:450,
            modal : true , 
            //border:false,
            layout : 'border' , 
            plain:true , 
            items: [headerPanel , grid ] 
        }) ;  
        me.refreshAssetData() ; 
        
        w.show() ; 
    } ,
    
    openAssetPropertyWindow : function(){
        var me = this ; 
        var w = new Ext.Window({
            title: 'Asset tracker',
            height : 140 , 
            width : 300 ,  
            bodyStyle : 'padding : 10px;' , 
            layout : 'form' , 
            defaults : {
                anchor : '90%'
            } , 
            items : [{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : '<b>Property</b>'
            },{
                xtype : 'combo' ,   
                hideLabel : true ,
                typeAhead: true,
                triggerAction: 'all' ,  
                mode: 'local',
                allowBlank: true  ,
                editable: false,
                displayField:   'name',
                valueField:     'value' 
            }] , 
            buttons : [{
                text : 'CANCEL' , 
                handler : function(){
                    w.close() ; 
                }
            },{
                text : 'NEXT' , 
                handler : function(){
                    w.close() ; 
                    Ext.getCmp('list-assets-window').close() ; 
                    me.saveAssets() ; 
                } 
            }] 
        }) ; 
        
        w.show();     
        
    } ,
    
    refreshAssetData : function(){
        var me = this ; 
        Ext.getCmp('lm-assets-grid').store.baseParams = {
            'search' : Ext.getCmp('customfield-search').getValue() , 
            'asset' : Ext.getCmp('assets-cb').getValue()
        } 
        Ext.getCmp('lm-assets-grid').store.load({params:{start:0, limit:10000}}); 
    }
});
