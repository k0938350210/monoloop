/**
 * @author Shea Frederick - http://www.vinylfox.com
 * @class Ext.ux.form.HtmlEditor.Divider
 * @extends Ext.util.Observable
 * <p>A plugin that creates a divider on the HtmlEditor. Used for separating additional buttons.</p>
 */
Ext.ux.form.HtmlEditor.listCustomfield = Ext.extend(Ext.util.Observable, {
    // private
    init: function(cmp){
        this.cmp = cmp;
        this.cmp.on('render', this.onRender, this);
    },
    
    saveData : function(data){
        this.cmp.insertAtCursor(data) ;
    } , 
    // private
    onRender: function(){
        var btn = this.cmp.getToolbar().insertButton( 0 , {
            iconCls: 'x-edit-wizard1',
            cls : 'ml-btn2' , 
            handler: function(){
                 list_customfield.objectReturn = this ; 
                 list_customfield.locationID = 3 ; 
                 list_customfield.openWindow() ; 
            },
            scope: this ,
            tooltip: '–	Add a data field from profile list.'
        });
    }
});
