/**
 * @author Shea Frederick - http://www.vinylfox.com
 * @class Ext.ux.form.HtmlEditor.plugins
 * <p>A convenience function that returns a standard set of HtmlEditor buttons.</p>
 * <p>Sample usage:</p>
 * <pre><code>
    new Ext.FormPanel({
        ...
        items : [{
            ...
            xtype           : "htmleditor",
            plugins         : Ext.ux.form.HtmlEditor.plugins()
        }]
    });
 * </code></pre>
 */
Ext.ux.form.HtmlEditor.plugins = function(){
    return [
        new Ext.ux.form.HtmlEditor.CBuilder(),
        new Ext.ux.form.HtmlEditor.Divider() 
  
    ];
};

Ext.ux.form.HtmlEditor.pluginsMail = function(){
    return [
        new Ext.ux.form.HtmlEditor.listCustomfield(),
        new Ext.ux.form.HtmlEditor.CBuilder(),
        new Ext.ux.form.HtmlEditor.Divider() 
  
    ];
};