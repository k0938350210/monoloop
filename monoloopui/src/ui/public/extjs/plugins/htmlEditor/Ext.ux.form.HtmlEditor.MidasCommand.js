/**
 * @author Shea Frederick - http://www.vinylfox.com
 * @class Ext.ux.form.HtmlEditor.MidasCommand
 * @extends Ext.util.Observable
 * <p>A base plugin for extending to create standard Midas command buttons.</p>
 * http://msdn.microsoft.com/en-us/library/ms533049%28v=VS.85%29.aspx
 * http://www.mozilla.org/editor/midas-spec.html
 */
Ext.ns('Ext.ux.form.HtmlEditor');

if (!Ext.isObject) {
    Ext.isObject = function(v){
        return v && typeof v == "object";
    };
}

Ext.override(Ext.form.HtmlEditor, {
    nutgetdoc : function(){
       return this.getDoc() ;  
    } ,
    
    nuttest : function(){
        var doc = this.getDoc(), selDocFrag;
        var txt = '', hasHTML = false, selNodes = [], ret, html = '';
        var sel ; 
        if (this.win.getSelection || doc.getSelection) {
            // FF, Chrome, Safari
            sel = this.win.getSelection();
            if (!sel) {
                sel = doc.getSelection();
            }
            
        }else if (doc.selection) {
                        
            sel = doc.selection  ;    
            
            
        }
        return sel ;
    } , 
    
    ml_hasClass : function(el, className, substring) {
    	if (!el || !el.className) return false;
    	var classes = el.className.trim().split(" ");
    	for (var i = classes.length; --i >= 0;) {
    		if (classes[i] == className || (substring && classes[i].indexOf(className) == 0)) return true;
    	}
    	return false;
    } , 
    
    ml_removeClass : function(el, className, substring) {
    	if (!el || !el.className) return;
    	var classes = el.className.trim().split(" ");
    	var newClasses = new Array();
    	for (var i = classes.length; --i >= 0;) {
    		if (!substring) {
    			if (classes[i] != className) {
    				newClasses[newClasses.length] = classes[i];
    			}
    		} else if (classes[i].indexOf(className) != 0) {
    			newClasses[newClasses.length] = classes[i];
    		}
    	}
    	if (newClasses.length == 0) {
    		if (!Ext.isOpera) {
    			el.removeAttribute("class");
    			if (Ext.isIE) {
    				el.removeAttribute("className");
    			}
    		} else {
    			el.className = '';
    		}
    	} else {
    		el.className = newClasses.join(" ");
    	}
    } ,
    
    ml_addClass : function(el, addClassName) {
    	this.ml_removeClass(el, addClassName);
    	if (el.className ) {
    		var classNames = el.className.trim().split(" ");
    	}
    	if (el.className) el.className += " " + addClassName;
    		else el.className = addClassName;
    } ,
    
    insertAtCursor : function(text){
        
        if(!this.activated){
            return;
        }
        if(Ext.isIE){
            this.win.focus();
            var doc = this.getDoc() ; 
            var r = doc.selection.createRange();
            if(r){
                r.pasteHTML(text);
                //this.syncValue();
                this.deferFocus(); 
            }
        }else{
            var doc = this.getDoc() ;
            this.win.focus();
            var sel = this.win.getSelection();
            if (!sel) {
                
                sel = doc.getSelection();
            }
            var range = sel.getRangeAt(0);
            range.deleteContents(); 
            range.insertNode(doc.createTextNode(text));
            
          

            this.deferFocus();
        }
    },
    
    getSelectedText: function(clip){
        var doc = this.getDoc(), selDocFrag;
        var txt = '', hasHTML = false, selNodes = [], ret, html = '';
        if (this.win.getSelection || doc.getSelection) {
            // FF, Chrome, Safari
            var sel = this.win.getSelection();
            if (!sel) {
                sel = doc.getSelection();
            }
            if (clip) {
                selDocFrag = sel.getRangeAt(0).extractContents();
            } else {
                selDocFrag = this.win.getSelection().getRangeAt(0).cloneContents();
            }
            Ext.each(selDocFrag.childNodes, function(n){
                if (n.nodeType !== 3) {
                    hasHTML = true;
                }
            });
            if (hasHTML) {
                var div = document.createElement('div');
                div.appendChild(selDocFrag);
                html = div.innerHTML;
                txt = this.win.getSelection() + '';
            } else {
                html = txt = selDocFrag.textContent;
            }
            ret = {
                textContent: txt,
                hasHTML: hasHTML,
                html: html
            };
        } else if (doc.selection) {
            // IE
            this.win.focus();
            txt = doc.selection.createRange();
            if (txt.text !== txt.htmlText) {
                hasHTML = true;
            }
            ret = {
                textContent: txt.text,
                hasHTML: hasHTML,
                html: txt.htmlText
            };
        } else {
            return {
                textContent: ''
            };
        }
        
        return ret;
    }
});

Ext.ux.form.HtmlEditor.MidasCommand = Ext.extend(Ext.util.Observable, {
    // private
    init: function(cmp){
        this.cmp = cmp;
        this.btns = [];
        this.cmp.on('render', this.onRender, this);
        this.cmp.on('initialize', this.onInit, this, {
            delay: 100,
            single: true
        });
    },
    // private
    onInit: function(){
        Ext.EventManager.on(this.cmp.getDoc(), {
            'mousedown': this.onEditorEvent,
            'dblclick': this.onEditorEvent,
            'click': this.onEditorEvent,
            'keyup': this.onEditorEvent,
            buffer: 100,
            scope: this
        });
    },
    // private
    onRender: function(){
        var midasCmdButton, tb = this.cmp.getToolbar(), btn, iconCls;
        Ext.each(this.midasBtns, function(b){
            if (Ext.isObject(b)) {
                iconCls = (b.iconCls) ? b.iconCls : 'x-edit-' + b.cmd;
                if (b.value) { iconCls = iconCls+'-'+b.value.replace(/[<>\/]/g,''); }
                midasCmdButton = {
                    iconCls: iconCls,
                    handler: function(){
                        this.cmp.relayCmd(b.cmd, b.value);
                    },
                    scope: this,
                    tooltip: b.tooltip ||
                    {
                        title: b.title
                    },
                    overflowText: b.overflowText || b.title
                };
            } else {
                midasCmdButton = new Ext.Toolbar.Separator();
            }
            btn = tb.addButton(midasCmdButton);
            if (b.enableOnSelection) {
                btn.disable();
            }
            this.btns.push(btn);
        }, this);
    },
    // private
    onEditorEvent: function(){
        var doc = this.cmp.getDoc();
        Ext.each(this.btns, function(b, i){
            if (this.midasBtns[i].enableOnSelection || this.midasBtns[i].disableOnSelection) {
                if (doc.getSelection) {
                    if ((this.midasBtns[i].enableOnSelection && doc.getSelection() !== '') || (this.midasBtns[i].disableOnSelection && doc.getSelection() === '')) {
                        b.enable();
                    } else {
                        b.disable();
                    }
                } else if (doc.selection) {
                    if ((this.midasBtns[i].enableOnSelection && doc.selection.createRange().text !== '') || (this.midasBtns[i].disableOnSelection && doc.selection.createRange().text === '')) {
                        b.enable();
                    } else {
                        b.disable();
                    }
                }
            }
            if (this.midasBtns[i].monitorCmdState) {
                b.toggle(doc.queryCommandState(this.midasBtns[i].cmd));
            }
        }, this);
    }
});
