<?php 

$factory->define(App\User::class, function (Faker\Generator $faker) {
  return [
    'username' => $faker->email, 
    'pass' => bcrypt(str_random(10)) 
  ];
});

$factory->define(App\Account::class, function (Faker\Generator $faker) {
  return [
    'cid' => (string)(new MongoId()), 
    'uid' => $faker->unique()->randomNumber , 
    'deleted' => 0 , 
    'hidden' => 0 , 
    'name' => $faker->company , 
    'is_test' => 0 , 
    'company' => $faker->company , 
    'contact_name' => $faker->name , 
    'contact_email' => $faker->email , 
    'phone' => $faker->phoneNumber , 
    'is_reseller' => false , 
    'invoice_reseller' => false , 
    'lock_account_type' => false , 
    'auto_activate_after_trial' => false , 
    'domains' => [] , 
    'invocation' => ['content_delivery' => 0 , 'anti_flicker' => false , 'timeout' => 0 , 'pre' => '' , 'post' => ''] , 
    'scores' => ['clickdepth' => 0 , 'duration' => 0 , 'loyalty' => 0 , 'recency' => 0 , 'brands' => 0 ] , 
    'control_group' => ['enable' => false , 'size' => 0 , 'days' => 0]
  ];
});

$factory->define(App\AccountUser::class, function (Faker\Generator $faker) {
  return [
    'email' => $faker->email , 
    'company' => $faker->company , 
    'name' => $faker->name , 
    'deleted' => 0 , 
    'hidden' => 0 , 
    'is_monoloop_admin' => 1 , 
    'is_monoloop_support' => 0 , 
    'dashboard_config' => null , 
    'support_type' => 0 , 
    'support_enddate' => 0 , 
  ];
});
