@extends('ui') 
 

@section('customcss')
    <link rel="stylesheet" type="text/css" href="/extjs/resources/css/ext-all-notheme.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/extjs/resources/css/xtheme-gray.css" media="all" />
	<script src="/extjs/adapter/ext/ext-base.js" type="text/javascript"></script>
	<script src="/extjs/ext-all.js" type="text/javascript"></script>
    <script src="/js/extjs3.js" type="text/javascript"></script>
@endsection