<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Monoloop</title>

	<link href="{{ asset('/css/monoloop.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	@yield('customcss')
</head>
<body class="ui">
	<header class="header"> 
		<div class="logo">
			<a href="/"><img src="/images/logo.gif" alt="" /></a>
			<span>REALTIME BEHAVOIRAL TARGETING</span>
		</div> 
	</header>  
    <section id="main" style="min-height: 450px;"> 
        <div class="container-fluid">
            @yield('content')
        </div>
    </section> 
    <footer id="footer">
 		<p>By Monoloop</p> 
    </footer>
    @yield('footerjs')
</body>
</html>