@extends('webix') 

@section('content')


<div style="background: white;position: relative;padding: 42px;border:1px solid rgb(204,204,204) ;">
	<div id="create-btn" style="position:absolute;top:50px;left:50px;z-index:150;"></div>
	<div id="main-content"></div>
</div>
<script type="text/javascript" charset="utf-8">
    var smalltreedata = [
        {id:"root", value:"Films data", open:true, data:[
            { id:"1", open:true, value:"The Shawshank Redemption", data:[
                { id:"1.1", value:"Part 1" },
                { id:"1.2", value:"Part 2", data:[
                    { id:"1.2.1", value:"Page 1" },
                    { id:"1.2.2", value:"Page 2" },
                    { id:"1.2.3", value:"Page 3" },
                    { id:"1.2.4", value:"Page 4" },
                    { id:"1.2.5", value:"Page 5" }
                ]},
                { id:"1.3", value:"Part 3" }
            ]},
            { id:"2", open:true, value:"The Godfather", data:[
                { id:"2.1", value:"Part 1" },
                { id:"2.2", value:"Part 2" }
            ]}
        ]}
    ];
    var tree_data = [
        { id: "1", type: "folder", value: "experiment", css:"folder_experiment", data: [
            { id: "m_0", type: "folder", css: "folder_favorite", value: "test", data: [
                { id: "m_0_1", type: "file", value: "Are You Experienced?"},
            ]},
            { id: "m_1", type: "folder", value: "Default Experiments", data: [
                { id: "m_1_0", type: "file", value: "Vocal Poem"},
                { id: "m_1_1", type: "file", value: "Flying Away"}
            ]},
            { id: "m_2", type: "folder", value: "local_experiment", data: [
                { id: "m_2_0", type: "file", value: "All Is Bright"},
                { id: "m_2_1", type: "file", value: "test"},
            ]}
        ]},
        { id:"2", type: "folder", css: "folder_pictures", value:"Images", data:[
            { id: "p_0", type: "folder", value: "01 - Christmas", data: [
                { id: "p_0_0", type: "file", ext: "png", value: "IMG_10034" },
                { id: "p_0_1", type: "file", ext: "png", value: "IMG_10035" },
                { id: "p_0_2", type: "file", ext: "png", value: "IMG_10036" },
                { id: "p_0_3", type: "file", ext: "png", value: "IMG_10037" },
                { id: "p_0_4", type: "file", ext: "png", value: "IMG_10038" },
                { id: "p_0_5", type: "file", ext: "png", value: "IMG_10039" },
                { id: "p_0_6", type: "file", ext: "png", value: "IMG_10040" },
                { id: "p_0_7", type: "file", ext: "png", value: "IMG_10041" },
                { id: "p_0_8", type: "file", ext: "png", value: "IMG_10042" }
            ]},
            { id: "p_1", type: "folder", value: "02 - New Year's Eve", data: [
                { id: "p_1_0", type: "file", ext: "jpg", value: "DSC10384" },
                { id: "p_1_1", type: "file", ext: "jpg", value: "DSC10385" },
                { id: "p_1_2", type: "file", ext: "jpg", value: "DSC10386" },
                { id: "p_1_3", type: "file", ext: "jpg", value: "DSC10387" },
                { id: "p_1_4", type: "file", ext: "jpg", value: "DSC10388" },
                { id: "p_1_5", type: "file", ext: "jpg", value: "DSC10389" },
                { id: "p_1_6", type: "file", ext: "jpg", value: "DSC10390" }
            ]}

        ]},
        { id: "3", type: "folder", css: "folder_testexperiments", value: "exp", data:[
            { id: "v_0", type: "folder", value: "Fitness", data: [
                { id: "v_0_0", type: "file",  value: "Stepup experiments" },
                { id: "v_0_1", type: "file",  value: "Stepup experiments 1" },
                { id: "v_0_2", type: "file",  value: "Stepup experiments 3" }
            ]}
        ]}
    ];

webix.ui({
	container:"main-content",
	type:"space",
	rows:[
		{ template:"Only X-snap here", type:"header"},
		{ type:"wide", cols:[
			{ view:"portlet", mode:"cols", body:{
				view:"tree", data:smalltreedata
			}},
			{ view:"portlet", mode:"cols", body:{
				view:"list", data:small_film_values_set, select:true, autoheight:true
			}},
			{ view:"portlet", mode:"cols", body:{
				view:"datatable", data:small_film_set, autoConfig:true, autoheight:true
			}},
		]
		}
	]
});


</script>

@endsection


