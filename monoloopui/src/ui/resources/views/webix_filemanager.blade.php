@extends('ui') 


@section('customcss') 
	<script src="/webix/filemanager/webix.js" type="text/javascript"></script>
	<script src="/webix/filemanager/filemanager/filemanager.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="/webix/filemanager/webix.css" media="all" /> 
    <link rel="stylesheet" type="text/css" href="/webix/filemanager/filemanager/filemanager.css" media="all" /> 


    <script src="/extjs/adapter/ext/ext-base.js" type="text/javascript"></script>
	<script src="/extjs/ext-all.js" type="text/javascript"></script>
    <script src="/js/extjs3.js" type="text/javascript"></script>
@endsection