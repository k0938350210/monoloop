<?php

use App\User;
use App\Account;
use App\Services\Hasher;

class ApiBasicTest extends TestCase {
  
  
 
	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicAuthen()
	{ 
    $hasher = new Hasher() ;  
    $account = Account::first() ;  
    $server = [
      'PHP_AUTH_USER' => $account->uid ,
      'PHP_AUTH_PW' => $hasher->generateSign($account->uid) 
    ];
    
		$response = $this->call('GET', '/api/v1/account' , [] , [] , [] , $server); 
		$this->assertEquals(200,$response->getStatusCode());
    
    $content = json_decode($response->getContent(),true) ; 
    $this->assertEquals($content['uid'],$account->uid);
    
	}
  
  public function testFailBasicAuthen(){ 
    $server = [
      'PHP_AUTH_USER' => 12 ,
      'PHP_AUTH_PW' => 'fail' 
    ];
    
		$response = $this->call('GET', '/api/v1/account' , [] , [] , [] , $server); 
		$this->assertEquals(401,$response->getStatusCode());
  }

}
