<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Placement extends Eloquent{ 
	
  protected $collection = 'placements';
  
}