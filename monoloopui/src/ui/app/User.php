<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
  
class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['pass','pwdreset_key','pwdreset_time', 'remember_token' , 'active_account' , 'accounts'];
	 
	
	public function account()
    {
        return $this->belongsTo('\App\Account','active_account');
    }
	
	public function accounts()
    {
        return $this->embedsMany('\App\UserAccount');
    }
	
	public function currentAccountConfig(){  
		foreach($this->accounts  as $account){ 
			var_dump($account->account_id);die;
			if($account->account_id == $this->active_account){  
				return $account ; 
			}
		}
		return null ; 
	} 
    
    #helper class
    public function account_for_selector(){
    	$ret = array() ;  
    	foreach($this->accounts as $account){
    		if($account->account_id == $this->active_account)
    			continue ; 
   			$ret[$account->account->name] = array('id' => $account->account_id , 'name' => $account->account->name);
     	}
     	ksort($ret) ;
     	return $ret ;
    }
    
    public function toArray()
    {
        $array = parent::toArray();
        $currentAccountConfig = $this->currentAccountConfig() ; 
        $array['current_config'] = $currentAccountConfig;
        return $array;
    }
}
