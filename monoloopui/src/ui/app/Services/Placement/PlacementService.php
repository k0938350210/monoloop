<?php namespace App\Services\Placement; 

use MongoId;
use App\Placement;

class PlacementService{ 
  public function getPrivacyCenterObject(){
    $placement = new Placement() ; 
    $placement->deleted = 0 ; 
    $placement->hidden = 0 ; 
    $placement->url = 0 ; 
    $placement->system_type = 'pc' ; 
    $placement->remark = 'Privacy Center' ; 
    $placement->addition_js = 'privacy_center/privacy.js' ; 
    $placement->preg_match = '' ; 
    $placement->cg_size = 0 ; 
    $placement->cg_day = 0 ; 
    $placement->inc_http_https = false ; 
    $placement->inc_www = false ; 
    $placement->url_option = 0 ; 
    $placement->details = [] ; 
    $placement->folder_id = null ; 
    return $placement ; 
  } 
}