<?php namespace App\Services;
  

class Hasher{
	
	/**
	 * Keeps a string for mapping an int to the corresponding
	 * base 64 character.
	 */
	const ITOA64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

	/**
	 * Keeps length of a MD5 salt in bytes.
	 *
	 * @var	integer
	 */
	static protected $saltLengthMD5 = 6;

	/**
	 * Keeps suffix to be appended to a salt.
	 *
	 * @var	string
	 */
	static protected $saltSuffixMD5 = '$';

	/**
	 * Setting string to indicate type of hashing method (md5).
	 *
	 * @var	string
	 */
	static protected $settingMD5 = '$1$';
	
	/**
	 * Returns suffix to be appended to a salt.
	 *
	 * @return	string		suffix of a salt
	 */
	protected function getSaltSuffix() {
		return self::$saltSuffixMD5;
	}
	
	/**
	 * Encodes bytes into printable base 64 using the *nix standard from crypt().
	 *
	 * @param	string		$input: the string containing bytes to encode.
	 * @param	integer		$count: the number of characters (bytes) to encode.
	 * @return	string		encoded string
	 */
	public function base64Encode($input, $count) {
		$output = '';
		$i = 0;
		$itoa64 = $this->getItoa64();
		do {
			$value = ord($input[$i++]);
			$output .= $itoa64[$value & 0x3f];
			if ($i < $count) {
				$value |= ord($input[$i]) << 8;
			}
			$output .= $itoa64[($value >> 6) & 0x3f];
			if ($i++ >= $count) {
				break;
			}
			if ($i < $count) {
				$value |= ord($input[$i]) << 16;
			}
			$output .= $itoa64[($value >> 12) & 0x3f];
			if ($i++ >= $count) {
				break;
			}
			$output .= $itoa64[($value >> 18) & 0x3f];
		} while ($i < $count);
		return $output;
	}
	
	public function isValidSalt($salt) {
		$isValid = $skip = FALSE;

		$reqLenBase64 = $this->getLengthBase64FromBytes($this->getSaltLength());

		if (strlen($salt) >= $reqLenBase64) {
						// salt with prefixed setting
			if (!strncmp('$', $salt, 1)) {
				if (!strncmp($this->getSetting(), $salt, strlen($this->getSetting()))) {
					$isValid = TRUE;
					$salt = substr($salt, strlen($this->getSetting()));
				} else {
					$skip = TRUE;
				}
			}

				// checking base64 characters
			if (!$skip && (strlen($salt) >= $reqLenBase64)) {
				if (preg_match('/^[' . preg_quote($this->getItoa64(),'/') . ']{' . $reqLenBase64 . ',' . $reqLenBase64 . '}$/', substr($salt, 0, $reqLenBase64))) {
					$isValid = TRUE;
				}
			}
		}

		return $isValid;
	}

	/**
	 * Returns length of a MD5 salt in bytes.
	 *
	 * @return	integer		length of a MD5 salt in bytes
	 */
	public function getSaltLength() {
		return self::$saltLengthMD5;
	}

	/**
	 * Method determines required length of base64 characters for a given
	 * length of a byte string.
	 *
	 * @param	integer		$byteLength: length of bytes to calculate in base64 chars
	 * @return	integer		required length of base64 characters
	 */
	protected function getLengthBase64FromBytes($byteLength) {
			// calculates bytes in bits in base64
		return intval(ceil(($byteLength * 8) / 6));
	}

	/**
	 * Returns setting string of MD5 salted hashes.
	 *
	 * @return	string		setting string of MD5 salted hashes
	 */
	public function getSetting() {
		return self::$settingMD5;
	}

	/**
	 * Returns a string for mapping an int to the corresponding base 64 character.
	 *
	 * @return	string		string for mapping an int to the corresponding base 64 character
	 */
	protected function getItoa64() {
		return self::ITOA64;
	}
	
	/**
	 * Generates a random base 64-encoded salt prefixed and suffixed with settings for the hash.
	 *
	 * Proper use of salts may defeat a number of attacks, including:
	 *  - The ability to try candidate passwords against multiple hashes at once.
	 *  - The ability to use pre-hashed lists of candidate passwords.
	 *  - The ability to determine whether two users have the same (or different)
	 *    password without actually having to guess one of the passwords.
	 *
	 * @return	string		a character string containing settings and a random salt
	 */
	protected function getGeneratedSalt() {
		$randomBytes =$this->generateRandomBytes($this->getSaltLength());

		return $this->base64Encode($randomBytes, $this->getSaltLength());
	}
	
	protected function generateRandomBytes($count){ 
			// We initialize with the somewhat random.
		$output = '' ;
		$randomState = 'monoloop'
				. base_convert(memory_get_usage() % pow(10, 6), 10, 2)
				. microtime() . uniqid('') . getmypid();
		while (!isset($output{$count - 1})) {
			$randomState = sha1(microtime() . mt_rand() . $randomState);
			$output .= sha1(mt_rand() . $randomState, TRUE);
		}
		$output = substr($output, strlen($output) - $count, $count); 
		return $output;
	}
	
	/**
	 * Method applies settings (prefix, suffix) to a salt.
	 *
	 * @param	string		$salt: a salt to apply setting to
	 * @return	string		salt with setting
	 */
	protected function applySettingsToSalt($salt) {
		$saltWithSettings = $salt;

		$reqLenBase64 = $this->getLengthBase64FromBytes($this->getSaltLength());

			// salt without setting
		if (strlen($salt) == $reqLenBase64) {
			$saltWithSettings = $this->getSetting() . $salt . $this->getSaltSuffix();

		}

		return $saltWithSettings;
	}
	
	
	/**
	 * Method creates a salted hash for a given plaintext password
	 *
	 * @param	string		$password: plaintext password to create a salted hash from
	 * @param	string		$salt: optional custom salt with setting to use
	 * @return	string		salted hashed password
	 */
	public function getHashedPassword($password, $salt = NULL) {
		$saltedPW = NULL;
		if (!empty($password)) {
			if (empty($salt) || !$this->isValidSalt($salt)) {
				$salt = $this->getGeneratedSalt();
			}
			$saltedPW = crypt($password, $this->applySettingsToSalt($salt));
		}

		return $saltedPW;
	}
	
	
	/**
	 * Generate api token
	 * 
	 * @param string $str
	 * @return
	 */
	public function generateSign($str) {
        $cryptsalt = '130293+109k21jl1c31c9312i3c910202'  ;     
        return hash('sha512',$str . $cryptsalt); 
    }  
}