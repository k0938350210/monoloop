<?php namespace App\Services\Account; 
use Auth;
use MongoId;
use App\Account;
use App\AccountUser;
use App\AccountPlugin; 

use Mail;


class MailService{ 
  public function sendInvitationMail(AccountUser $user){
    Mail::send('emails.invitation.user', ['user' => $user ], function($message)use ($user)
		{ 
		  $message->to($user->email,$user->name)->subject('[monoloop.com] You got invitation from Monoloop');
		}); 
  }
}