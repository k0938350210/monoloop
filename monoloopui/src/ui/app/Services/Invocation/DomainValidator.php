<?php namespace App\Services\Invocation;

use Goutte\Client;

class DomainValidator{
  public function checkInvocation($domain){
    $client = new Client();  
    $crawler = $client->request('GET', 'http://' . $domain);
    
    $status = 0 ;
    
    $crawler->filter('head script')->each(function ($node) use (&$status)  { 
      $content = $node->text() ; 
      $pos = strpos($content , 'ML_vars') ; 
      if( $pos !== false){
        $status = 2 ;  
      } 
    });
    
    $crawler->filter('body script')->each(function ($node) use (&$status)  { 
      $content = $node->text() ; 
      $pos = strpos($content , 'ML_vars') ; 
      if( $pos !== false){
        $status = 1 ;  
      } 
    }); 
    
    return $status ; 
  }
}