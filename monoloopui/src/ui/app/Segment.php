<?php 
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Segment extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $collection = 'Segments';

    /*
     * The Segments associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */	
    public function segments($folder = NULL){

    	$branch = [];

    	$segments = Segment::where('folder_id', '=', new \MongoId($folder))->where('deleted', '=', 0)->get();

        foreach ($segments as $key => $segment) {
            $node = new \stdClass();
            $node->id = "segment__".$segment->_id;
            $node->value = $segment->name;
            $node->type = "file";
            $node->description = (strlen($segment->description) < 50 ? $segment->description : substr($segment->description, 0,47).'...');
            $node->date = date('j F, Y', strtotime($segment->created_at));
            $node->hidden = $segment->attributes['hidden'];
    		array_push($branch, $node);
    	}

    	return $branch;
    }
}