<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class AccountPlugin extends Eloquent{
	 
	
	public function plugin()
    {
        return $this->belongsTo('\App\Plugin','plugin_id');
    }
    
    public function toArray()
    {
        $array = parent::toArray();
        $array['plugin'] = $this->plugin;
        return $array;
    }

}