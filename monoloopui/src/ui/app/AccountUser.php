<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use Auth;
use Carbon ; 
use MongoId ; 

class AccountUser extends Eloquent{ 
	protected $collection = 'account_users';
  
  protected $hidden = ['invitation'];
  protected $dates = array('support_enddate');
  /*------- relation -------- */
  
  public function account()
  {
    return $this->belongsTo('\App\Account','account_id');
  }
  
  public function user(){
    return $this->belongsTo('\App\User','user_id');
  }
  
  /*--- Scope ---*/
  
  public function scopeInvitation($query, $invitation)
  {
    return $query->where('invitation', '=', new MongoId($invitation));
  }
  
  /*------- attributes --------*/
 
  public function getIsAccessibleAttribute(){
    $currentAccount = Auth::user()->account->_id ;  
    if( $this->account_id == $currentAccount){
      return true ; 
    } 
    return false ; 
  }
  
  public function getCanResendInvitationAttribute(){
    if($this->invitation != '')
      return true ;  
      
    if($this->user == null)
      return true ;
      
    return false ;  
  }
  
  public function getInvitationURLAttribute(){
    return url('invitation/'.$this->invitation) ; 
  }
  
  /*------- method ----------*/
  public function toArray()
  {
    $array = parent::toArray(); 
    $array['can_resend_invitation'] = $this->canResendInvitation ;
    if( $array['support_enddate'] != '')
      $array['support_enddate'] = Carbon::parse($array['support_enddate'])->format('d/m/Y');
    return $array;
  }
}