<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Account extends Eloquent{
	protected $collection = 'accounts';
	
	public function plugins()
    {
        return $this->embedsMany('\App\AccountPlugin');
    }
	
	protected $hidden = ['is_reseller','invoice_reseller','lock_account_type','auto_activate_after_trial','trialconvertion','s3_bucket','domains'];
}