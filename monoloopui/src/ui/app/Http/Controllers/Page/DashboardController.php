<?php namespace App\Http\Controllers\Page;

use Auth; 
use Illuminate\Http\Request;

class DashboardController extends PageController {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{ 
		$this->viewData['selected'] = array('Dashboard','Dashboard') ;
		return view('page.dashboard',$this->viewData);
	}
	
	public function funnel(){
		$this->viewData['selected'] = array('Dashboard','Funnel') ;
		return view('page.funnel',$this->viewData);
	}
}