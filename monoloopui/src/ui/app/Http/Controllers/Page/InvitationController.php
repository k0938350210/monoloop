<?php namespace App\Http\Controllers\Page;

use Auth; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;  

use App\AccountUser;
use MongoId ; 

class InvitationController extends Controller {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
  private $viewData = array() ; 
   
	public function index(Request $request , $id)
	{
    if( MongoId::isValid($id) == false){
      abort(404);
    }
   
    $accountUser = AccountUser::invitation($id)->first() ;
    
    if(is_null($accountUser)){
      abort(404);
    }
   
    $this->viewData['user'] = $accountUser ; 
    
		return view('page.invitation',$this->viewData);
	}
 
}