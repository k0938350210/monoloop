<?php namespace App\Http\Controllers\Page;

use Auth;
use Illuminate\Http\Request;

class ContentController extends PageController {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function web()
	{ 
		$this->viewData['selected'] = array('Content','Web') ;
		return view('page.content.web',$this->viewData);
	}
	
	public function bluepronts(){
		$this->viewData['selected'] = array('Content','Blueprints') ;
		return view('page.content.bluepronts',$this->viewData);
	}
	
	public function segment(){
		$this->viewData['selected'] = array('Segment','') ;
		return view('page.content.segment',$this->viewData);
	}
 
 	public function experiment(){
 		$this->viewData['selected'] = array('Experiment','') ;
		return view('page.content.experiment',$this->viewData);
 	}
 	
 	public function goals(){
 		$this->viewData['selected'] = array('Goals','') ;
		return view('page.content.goals',$this->viewData);
 	}
}