<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Segment;
use App\Folder;

use Input;

class SegmentController extends Controller {

 	/*
 	 * Action is finding all segments and associated folders of logged in user
 	 *
 	 * @return Json
 	 */
	public function index()
	{
		$ret = [];
		$account_id = Auth::user()->active_account;
		$folder = new Folder();

		$ret['segments'] = $folder->folders($account_id);
		return response()->json( $ret['segments'] ) ;
	}

	/*
	* Action is a segment from id
	*
	* @return Json
	*/
	public function show()
	{
		$ret = [];
		$srcTypeId = explode("__", Input::get('source'));

		$ret['segment'] = Segment::find($srcTypeId[1]);
		return response()->json( $ret['segment'] ) ;
	}

	/*
	* Action is creating a new segment
	*
	* @return Json
	*/
	public function create(Request $request){
		$ret = ['success' => true , 'msg' => 'Segment created!'] ;

		$srcTypeId = explode("__", $request->input('source'));

		$account_id = Auth::user()->active_account;

		$segment = new Segment();
		$segment->name = $request->input('name');
		$segment->description = $request->input('desc');
		$segment->account_id = $account_id;
		$segment->deleted = 0;
		$segment->hidden = 0;
		$segment->folder_id = new \MongoId($srcTypeId[1]);
		$segment->save() ;
		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a segment
	*
	* @return Json
	*/
	public function update(Request $request){
		$ret = ['success' => true , 'msg' => 'Segment updated!'] ;

		$srcTypeId = explode("__", $request->input('source'));


		$segment = Segment::find($srcTypeId[1]);
		$segment->name = $request->input('name');
		$segment->description = $request->input('desc');
		$segment->save() ;

		$ret['segment'] = $segment;
		return response()->json( $ret ) ;
	}
}
