<?php namespace App\Http\Controllers\Api\Account;
use Auth; 
use MongoId; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Services\ExtjsFilter;

class UserController extends Controller {
	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(Request $request){  
		#return User::where('accounts.username' , '=', 'nutjaa@msn.com' )->count();  <br />
		$query = User::where('accounts.account_id','=',new  MongoId('5523948bf4efec040b00002b'))  ; 
		return response()->json([
			'topics' => ExtjsFilter::filter($query,$request,['email'=>'accounts.email'])->get() , 
			'totalCount' => $query->count() 	
		]);
	}
 
}