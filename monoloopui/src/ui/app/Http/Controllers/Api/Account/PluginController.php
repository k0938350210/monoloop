<?php namespace App\Http\Controllers\Api\Account;
use Auth; 
use MongoId; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PluginController extends Controller {
	
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index(){
		$plugins = Auth::User()->account->plugins ; 
		return response()->json( array('topics' => $plugins , 'totalCount' => count($plugins) )) ; 
	}
	
	public function update(Request $request , $id ){
		$ret = ['success' => true , 'msg' => ''] ;   
		$plugin = Auth::User()->account->plugins->where('_id','=',new  MongoId($id) )->first() ;  
		$plugin->enabled = (bool)$request->input('enabled') ; 
		$plugin->save() ; 
		return response()->json( $ret ) ; 
	}
}