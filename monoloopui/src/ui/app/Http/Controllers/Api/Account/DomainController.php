<?php namespace App\Http\Controllers\Api\Account;
use Auth; 
use MongoId; 
use Carbon;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\ExtjsFilter;
use App\Services\Placement\PlacementService;
use App\Services\PageElement\PageElementService;
use App\Services\Invocation\DomainValidator;

use App\AccountDomain;
use App\Placement ; 

class DomainController extends Controller {
	
	public function __construct()
	{
		$this->middleware('auth');
	}
  
  #--- restful
	
	public function index(Request $request){
		$domains = Auth::User()->account->domains ; 
    
    $testDomains = Auth::User()->account->domains->where('laststatus' , '<' , new DateTime('-1 weeks'))->limit(3)->get() ; 
    foreach($testDomains as $domain){
      $domainValidator = new DomainValidator() ; 
      $domain->status = $domainValidator->checkInvocation($domain->domain) ; 
      $domain->laststatus = Carbon::now() ; 
      $domain->save() ; 
    }
    
		return response()->json([
			'topics' => ExtjsFilter::filter($domains,$request,[])->get() , 
			'totalCount' => $domains->count() 	
		]);
	}
  
  public function show(Request $request , $id){
    $ret = ['success' => false ] ; 
    $domain = Auth::User()->account->domains->where('domain',$id)->first() ;   
    if(empty($domain)){
      return response()->json($ret, 404); ;  
    }
    $ret['success'] = true ; 
    $ret['domain'] = $domain ; 
    return response()->json($ret, 200); 
  }
  
  public function store(Request $request){
    $ret = ['success' => false ] ;  
    $domainObj = Auth::User()->account->domains->where('domain',$request->input('domain'))->first() ;   
    if(!empty($domainObj)){
      $ret['msg'] = 'Domain ' . $request->input('domain') . ' already exists' ; 
      return response()->json($ret, 200); 
    }
    
    $domain = new AccountDomain() ; 
    $domain->domain = $request->input('domain') ; 
    $domain->privacy_option = $request->input('privacy_option') ; 
    $domain->privacy_usecustom = $request->input('privacy_usecustom') ; 
    $domain->url_privacy = $request->input('url_privacy') ; 
    
    Auth::User()->account->domains()->save($domain) ; 
    
    #create placement mapped ; 
    $placementService = new PlacementService() ; 
    $placement = $placementService->getPrivacyCenterObject() ; 
    $placement->url = $domain->url_privacy  ; 
    if(trim($placement->url) == '' || $domain->privacy_option == 'manual'){
      $placement->hidden = 1 ; 
    }
    $placement->save();
    
    $domain->placement_id = new  MongoId( $placement->_id ) ; 
    $domain->save() ; 
    
    #process placement to PageElement 
    $pageElementService = new PageElementService() ; 
    $pageElementService->processByPlacement($placement) ; 
    
    $ret['success'] = true ; 
    return response()->json($ret, 200); 
  }
  
  public function update(Request $request , $id){
    $ret = ['success' => false ] ; 
    $domain = Auth::User()->account->domains->where('domain',$id)->first() ;   
    if(empty($domain)){
      return response()->json($ret, 404); ;  
    }
    
    $total = Auth::User()->account->domains->where('domain',$request->input('domain'))->where('domain','<>',$id)->count() ; 
    if($total == 1){
      $ret['msg'] = 'Domain ' . $request->input('domain') . ' already exists' ; 
      return response()->json($ret, 200); 
    }  
    
    $domain->domain = $request->input('domain') ; 
    $domain->privacy_option = $request->input('privacy_option') ; 
    $domain->privacy_usecustom = $request->input('privacy_usecustom') ; 
    $domain->url_privacy = $request->input('url_privacy') ; 
    $domain->save() ; 
    
    $placement = $domain->placement ; 
    if(is_null($placement)){
      $placementService = new PlacementService() ; 
      $placement = $placementService->getPrivacyCenterObject() ;  
      $placement->save();
      $domain->placement_id = new  MongoId( $placement->_id ) ; 
      $domain->save() ; 
    } 
    $placement->url = $domain->url_privacy  ; 
    if(trim($placement->url) == '' || $domain->privacy_option == 'manual'){
      $placement->hidden = 1 ; 
    }else{
      $placement->hidden = 0 ; 
    }
    $placement->save();
    
    #process placement to PageElement 
    $pageElementService = new PageElementService() ; 
    $pageElementService->processByPlacement($placement) ; 
    
    $ret['success'] = true ; 
    return response()->json($ret, 200); 
  }
  
  public function destroy(Request $request , $id){
    $ret = ['success' => false ] ; 
    $domain = Auth::User()->account->domains->where('domain',$id)->first() ;   
    if(empty($domain)){
      return response()->json($ret, 404); ;  
    }
    
    $placement = $domain->placement ; 
    if(!is_null($placement)){
      $placement->deleted = 1 ; 
      $placement->save() ; 
      
      $pageElementService = new PageElementService() ; 
      $pageElementService->processByPlacement($placement) ;   
    }
    
    $domain->delete();
    $ret['success'] = true ; 
    return response()->json($ret, 200); 
  }
  
  /*--- custom function ---*/
  
  public function testDomain(Request $request , $id){
    $ret = ['success' => false ] ; 
    $domain = Auth::User()->account->domains->where('domain',$id)->first() ;  
    
    if(!empty($domain)){
      $domainValidator = new DomainValidator() ; 
      $domain->status = $domainValidator->checkInvocation($domain->domain) ; 
      $domain->laststatus = Carbon::now() ; 
      $domain->save() ; 
    }
    
    $ret['success'] = true ; 
    return response()->json($ret, 200); 
  }
 
}