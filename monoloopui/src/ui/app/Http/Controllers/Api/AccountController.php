<?php namespace App\Http\Controllers\Api;

use Auth; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Hasher;
use App\Services\Account\Cbr;
use App\Services\Account\Invocation;

class AccountController extends Controller {
	
	public function __construct()
	{
		$this->middleware('auth');
	}
  
	public function profile(){
		$accountObj = Auth::User()->account ; 
		$account = $accountObj->toArray() ; 
		
		$invocation = new Invocation($accountObj);
		$invocationStr = $invocation->getInvocation() ;  
		
		$account['invocation']['str'] = $invocationStr ; 
		$account['invocation']['str2'] = htmlentities($account['invocation']['str']) ; 
        $account['invocation']['str2'] = nl2br($account['invocation']['str2']) ;
        
        $account['apihost'] = $accountObj->uid . 'invoke.monoloop.com/api/' ; 
        $hasher = new Hasher() ; 
        $account['apitoken'] = $hasher->generateSign($accountObj->uid);
        
		return response()->json( $account ) ; 
	}
	
	private function generateCbr($account){
		$cbr = new Cbr() ; 
		$cbr->process($account->_id) ; 
	}
	
	public function controllgroupUpdate(Request $request){
		$ret = ['success' => true , 'msg' => ''] ; 
		$account = Auth::User()->account ;
	 	$control_group = $account->control_group ; 
		$control_group['enable'] = (bool)($request->input('enable-ab')); 
		$control_group['size'] = intval($request->input('size')); 
		$control_group['days'] = intval($request->input('days')); 
		$account->control_group = $control_group ; 
		$account->save() ; 
		
		$this->generateCbr($account) ;
		   
		return $ret ; 
	}
	
	public function invocationUpdate(Request $request){
		$ret = ['success' => true , 'msg' => ''] ; 
		$account = Auth::User()->account ;
		$invocation = $account->invocation ; 
		$invocation['anti_flicker'] = (bool)($request->input('a')) ; 
		$invocation['content_delivery'] = intval($request->input('c')) ; 
		$invocation['timeout'] = intval($request->input('t')) ; 
		$account->invocation = $invocation ; 
		$account->save() ; 
		
		$invocation = new Invocation($account);
		$invocationStr = $invocation->getInvocation() ;  
		
		$ret['invocation']['str'] = $invocationStr ; 
		$ret['invocation']['str2'] = htmlentities($ret['invocation']['str']) ; 
        $ret['invocation']['str2'] = nl2br($ret['invocation']['str2']) ;
		
		$this->generateCbr($account) ;
		
		return $ret ; 
	}
	
	public function prepostUpdate(Request $request){
		$ret = ['success' => true , 'msg' => ''] ; 
		$account = Auth::User()->account ;
		$invocation = $account->invocation ; 
		$invocation['pre'] = $request->input('pre_invocation') ; 
		$invocation['post'] = $request->input('post_invocation') ; 
		$account->invocation = $invocation ; 
		$account->save() ; 
		
		$this->generateCbr($account) ;
		
		return $ret ; 
	}
	
	
}