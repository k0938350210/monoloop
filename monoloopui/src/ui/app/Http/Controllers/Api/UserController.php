<?php namespace App\Http\Controllers\Api;

use Auth; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Hasher;

class UserController extends Controller {
 
	public function profile()
	{  
		$ret = [] ; 
		$ret['user'] = Auth::User() ;
		$ret['config'] = Auth::User()->currentAccountConfig() ;  
		return response()->json( $ret ) ; 
	}
	
	public function profileUpdate(Request $request){
		$ret = ['success' => true , 'msg' => 'Save profile success.'] ;
		
		$config = Auth::User()->currentAccountConfig() ; 
		$config->name = $request->input('name'); 
		$config->email = $request->input('email');	 
		$config->save() ; 
		
		$pass = $request->input('password') ; 
		if($pass != ''){ 
			$user = Auth::User() ; 
			$hasher = new Hasher() ; 
			$user->pass = $hasher->getHashedPassword($pass) ; 
			$user->Save()  ; 
	 	}
		
		return response()->json( $ret ) ; 
	}
	 
}