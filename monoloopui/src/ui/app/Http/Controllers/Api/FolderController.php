<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Segment;
use App\Folder;

class FolderController extends Controller {

	public function index()
	{
		$ret = [];
		return response()->json($ret) ;
	}

	/*
 	 * Action is createing new folder
 	 *
 	 * @return Json
 	 */
	public function create(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder created!'] ;

		$account_id = Auth::user()->active_account;

		// creating new folder
		$folder = new Folder();
		$folder->account_id = $account_id;
		$folder->deleted = 0;
		$folder->hidden = 0;
		$folder->title = $request->input('value');
		$folder->account_id = $request->input('value');
		$folder->parent_id = new \MongoId(explode("__", $request->input('$parent'))[1]);
		$folder->created_at = new \Datetime();
		$folder->updated_at = new \Datetime();
		$folder->save();

		return response()->json( $ret ) ;
	}

	/*
 	 * Action is updating folder / segment
 	 *
 	 * @return Json
 	 */
	public function update(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder updated!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		// performing action in update (folder / segment edit) or status update
		$ret['action'] = $request->input('action');

		// cases for folder and segment
		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);

				switch ($ret['action']) {
					case 'rename':
						$folder->title = $request->input('target');
						break;
					case 'update_hidden':
						$folder->hidden = $request->input('hidden');
						break;

					default:
						# code...
						break;
				}

				$folder->updated_at = new \Datetime();
				$folder->save();
				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				switch ($ret['action']) {
					case 'rename':
            $segment->name = $request->input('name');
					  $segment->description = $request->input('desc');
						break;
					case 'update_hidden':
						$segment->hidden = $request->input('hidden');
						break;

					default:
						# code...
						break;
				}
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}


		return response()->json( $ret ) ;
	}

	/*
 	 * Action is deleting folder / segment
 	 *
 	 * @return Json
 	 */
	public function delete(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder deleted!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);
				$folder->deleted = ($request->input('action') === 'remove') ? 1 : 0;
				$folder->updated_at = new \Datetime();
				$folder->save();
				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				$segment->deleted = ($request->input('action') === 'remove') ? 1 : 0;
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}

		return response()->json( $ret ) ;
	}

	/*
 	 * Action is moving folder / segment to folder / segment
 	 *
 	 * @return Json
 	 */
	public function move(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder deleted!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		$targetTypeId = explode("__", $request->input('target'));



		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);
				// moving folder to targeted folder
				$folder->parent_id = new \MongoId($targetTypeId[1]);
				$folder->updated_at = new \Datetime();
				$folder->save();
				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				// moving segment to targeted folder
				$segment->folder_id = new \MongoId($targetTypeId[1]);
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}

		return response()->json( $ret ) ;
	}
}
