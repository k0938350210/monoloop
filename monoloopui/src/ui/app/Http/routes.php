<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// UI MAP
# Page - Dashboard
Route::get('/', 'Page\DashboardController@index');
Route::get('/dashboard/funnel', 'Page\DashboardController@funnel');
# Page - Account
Route::get('/account/profile', 'Page\AccountController@profile');
Route::get('/account/control-group', 'Page\AccountController@controlgroup');
Route::get('/account/code-and-scripts', 'Page\AccountController@codeandscripts');
Route::get('/account/api-token', 'Page\AccountController@apitoken');
Route::get('/account/plugins', 'Page\AccountController@plugins');
Route::get('/account/users', 'Page\AccountController@users');
Route::get('/account/client-accounts', 'Page\AccountController@clientaccounts');
Route::get('/account/scores', 'Page\AccountController@scores');
Route::get('/account/domains', 'Page\AccountController@domains');
Route::get('/account/applications', 'Page\AccountController@applications');
Route::get('/account/logs', 'Page\AccountController@logs');
# Page Trackers
Route::get('/trackers', 'Page\TrackerController@index');
# Page Content
Route::get('/content/web', 'Page\ContentController@web');
Route::get('/content/blueprints', 'Page\ContentController@bluepronts');
Route::get('/segment', 'Page\ContentController@segment');
Route::get('/experiment', 'Page\ContentController@experiment');
Route::get('/goals', 'Page\ContentController@goals');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


//API
Route::group(['prefix' => 'api'], function()
{
    Route::get('profile','Api\UserController@profile' );
    Route::post('profile','Api\UserController@profileUpdate' );
    // Segment
    Route::get('segments', 'Api\SegmentController@index');
		Route::get('segments/show', 'Api\SegmentController@show');
    Route::post('segments/create', 'Api\SegmentController@create');
		Route::post('segments/update', 'Api\SegmentController@update');
    // Folder / Segments
    Route::post('folders/create', 'Api\FolderController@create');
    Route::post('folders/update', 'Api\FolderController@update');
    Route::post('folders/delete', 'Api\FolderController@delete');
    Route::post('folders/move', 'Api\FolderController@move');
    //Account
    Route::get('account','Api\AccountController@profile' );
    Route::post('account/controllgroup','Api\AccountController@controllgroupUpdate' );
    Route::post('account/invocation','Api\AccountController@invocationUpdate' );
    Route::post('account/invocation/prepost','Api\AccountController@prepostUpdate' );
    //Account - plugins
    Route::post('account/plugins','Api\Account\PluginController@index' );
    Route::post('account/plugins/{id}','Api\Account\PluginController@update' );
    //Account - users
    Route::post('account/users','Api\Account\UserController@index' );
});
