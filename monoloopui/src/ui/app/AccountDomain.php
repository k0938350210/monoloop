<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class AccountDomain extends Eloquent{ 
  
  protected $dates = array('laststatus');
	
  /*------- relation -------- */
  
  public function placement(){
    return $this->belongsTo(\App\Placement::class,'placement_id');
  } 
  
}