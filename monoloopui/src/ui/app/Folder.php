<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use App\Segment;

class Folder extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $collection = 'folders';

    /*
     * The folders hierarchy with their childern for an account
     *
     * @param account Object
     * @rerurn array
     */
    public function folders($account = NULL){

    	$tree = [];
        $segment = new Segment();

    	$folders = Folder::where('account_id', '=', $account)->where('deleted', '=', 0)->where('parent_id', '=', NULL)->get();
    	// $folders = Folder::where('account_id', '=', $account)->get();


    	foreach ($folders as $key => $folder) {
	         $node = new \stdClass();
	         $node->id = "folder__".$folder->_id;
	         $node->value = $folder->title;
	         $node->open = true;
	         $node->type = "folder";
           // hidden showes if it's inactive
           $node->hidden = $folder->attributes['hidden'];
	         $node->date = date('j F, Y', strtotime($folder->created_at));
	         $node->data = $this->buildTree($folder);
           // merging segments associated with this folder
           $node->data = array_merge($node->data, $segment->segments($folder->_id));
    	     array_push($tree, $node);
    	}

    	return $tree;
    }

    /*
     * The folders hierarchy with their childern for an account
     *
     * @param elements Object
     * @rerurn array
     */
    private function buildTree($node){

    	$branch = [];
        $segment = new Segment();
        $childFolders = Folder::where('parent_id', '=', new \MongoId($node->_id))->where('deleted', '=', 0)->get();

	    foreach ($childFolders as $cFkey => $childFolder) {
            $childNode = new \stdClass();
            $childNode->id = "folder__".$childFolder->_id;
            $childNode->value = $childFolder->title;
            $childNode->open = false;
            $childNode->type = "folder";
            // hidden showes if it's inactive
            $childNode->hidden = $childFolder->attributes['hidden'];
            $childNode->date = date('j F, Y', strtotime($childFolder->created_at));
            $childNode->data = $this->buildTree($childFolder);
            // merging segments associated with this folder
            $childNode->data = array_merge($childNode->data, $segment->segments($childFolder->_id));
	    	array_push($branch, $childNode);
	    }

	    return $branch;
    }
}
