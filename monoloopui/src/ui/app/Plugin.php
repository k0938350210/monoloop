<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Plugin extends Eloquent{
	protected $collection = 'plugins'; 
}