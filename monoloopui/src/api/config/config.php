<?php
$configuration = \Logilim\Configuration::getInstance();
$configuration->deprecated_apis = array();
loadConfiguration($configuration);
\Purekid\Mongodm\MongoDB::setConfigBlock('default', array(
  'connection' => array(
    'hostnames' => 'perconamongodb_1a:27018,perconamongodb_1b:27018,perconamongodb_1c:27018',
    'database'  => 'MonoloopUI',
    'options'  => array("replicaSet" => "monoloop_prod_shard_1")
  )
));

// Make Redis.
$params = ['tcp://redis_1a:26379', 'tcp://redis_1b:26379', 'tcp://redis_1c:26379'];
$options = ['replication' => 'sentinel','service' => 'redis-cluster'];

$configuration->redis = new Predis\Client($params, $options);

function loadConfiguration(&$configuration) {
  $env = 'development';
  $info = json_decode(file_get_contents(DOCROOT.DS."config/environments/$env.json"),true);
  array_walk($info, function($value, $key) use($configuration) {
    if (strtoupper($key) == $key) {
      // These are constants. Define them.
      define($key, $value);
      // Unset constants.
    } else {
        $configuration->$key = $value;
    }
});
}
