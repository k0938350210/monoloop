<?php
/**
 * Created as Autoloader.php.
 */

namespace Logilim;

class Autoloader {

    const SOURCE_CODE_DIR = "protected";

    const SOURCE_CODE_SUB_DIR = "classes";

    const SOURCE_MODELS_SUB_DIR = "models";

    const SOURCE_MIDDLEWARE_DIR = 'middleware';

    const SOURCE_CODE_EXT = "Entity";

    const SOURCE_MIDDLEWARE_EXT = 'Middleware';

    const SOURCE_CODE_EXT_SEPARATOR = '.';

    public static function registrar() {
        spl_autoload_register(__NAMESPACE__."\\Autoloader::_autoload");
    }

    public static function _autoload($class) {
        $model_path = DOCROOT.DS.self::SOURCE_CODE_DIR.DS.self::SOURCE_MODELS_SUB_DIR.DS.$class.".php";
        if (file_exists($model_path))
            require_once $model_path;

        $last_char = substr($class,-1);
        if ($last_char == 'E') {

            $clean_name = substr($class,0,-1);
            $class_path = DOCROOT.DS.self::SOURCE_CODE_DIR.DS.self::SOURCE_CODE_SUB_DIR.DS.$clean_name.self::SOURCE_CODE_EXT_SEPARATOR.self::SOURCE_CODE_EXT.".php";
            if (file_exists($class_path))
                require_once $class_path;

        }

        // Lets load Middleware
        if ( strpos($class,'Middleware') !== FALSE) {
            $middleware_name = substr($class,0,strpos($class,'Middleware'));
            $class_path = DOCROOT.DS.self::SOURCE_CODE_DIR.DS.self::SOURCE_MIDDLEWARE_DIR.DS.$middleware_name.'.'.self::SOURCE_MIDDLEWARE_EXT.".php";
            if ( file_exists($class_path))
                require $class_path;

        }
    }
}
