<?php

namespace Logilim;

class Errors {

    const ERR_TOKEN_INVALID = '001';

    const ERR_TOKEN_EXPIRED = '002';

    const ERR_TOKEN_MISSING = '003';

    const ERR_ML_MISSING   = '004';

    const ERR_ML_DEPRECATED = '005';

    const ERR_ML_INVALID    = '006';

    const ERR_INVALID_CONTENT_TYPE = '007';

    const ERR_INVALID_PASSWORD  = '008';

    const ERR_MISSING_PARAMETERS = '009';

    const ERR_NON_EXISTING_EMAIL = '010';

    const ERR_USEROBJID_INVALID  =	'011';

    const ERR_INACTIVE_ACCOUNT  = '012';


    static function errorDescription($errCode) {

        $desc = array(

            self::ERR_TOKEN_INVALID     => 'Invalid Token or does not exist in our system',
            self::ERR_TOKEN_EXPIRED     => 'Your Token has been expired. Please refresh and try again',
            self::ERR_TOKEN_MISSING     => 'Missing token as "ML-Token" in Request Headers',
            self::ERR_ML_MISSING       => 'Missing version as "ML-Version" in Request Headers',
            self::ERR_ML_DEPRECATED    => 'Your API version has been deprecated. Please switch to '.STABLE_API_VERSION,
            self::ERR_ML_INVALID       => 'Seems, you love to send bogus version. No version found as per Request Header',
            self::ERR_INVALID_CONTENT_TYPE  => 'Invalid Content-Type defined in Request Header. Application only accepts "application/json"',
            self::ERR_INVALID_PASSWORD   => 'Invalid Username/Password.',
            self::ERR_MISSING_PARAMETERS    => 'Your Request Body is missing some required Parameters.',
            self::ERR_NON_EXISTING_EMAIL => 'Given email address does not exist.',
            self::ERR_USEROBJID_INVALID	=>	'User ObjectId is Invalid and/or does not exists.',
            self::ERR_INACTIVE_ACCOUNT => 'Account inactive. Please check your email and activate your account.',
        );

        if ( $errCode == null)

            return '';

        if ( array_key_exists($errCode, $desc))

            return $desc[$errCode];

        return '';

    }
}
