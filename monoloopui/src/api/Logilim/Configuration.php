<?php
/**
 * Created as Configuration.php.
 */

namespace Logilim;
/**
 * Class Configuration
 * @package Logilim
 */
class Configuration {

  private static $_instance = null;

  private $_data = array();

  private function __construct(){}

  static function getInstance() {
    if (! static::$_instance instanceof Configuration)
      self::$_instance = new Configuration();

    return self::$_instance;
  }

  public function __set($key, $value) {
    $this->_data[$key] = $value;
  }

  public function __get($key) {
    if ( array_key_exists($key, $this->_data))
      return  $this->_data[$key];
    return null;
  }
}
