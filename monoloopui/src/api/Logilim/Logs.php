<?php

namespace Logilim;

class Logs {

    private static $allMethods = array();

    public static function getClassMethods(){
       try{
           $allMethods = array();

           foreach (new \DirectoryIterator('protected/classes') as $fileInfo) {
               $exploded  = explode('.', $fileInfo->getFilename(),2);
               $className = $exploded[0].'E';
               self::$allMethods[$className] = get_class_methods($className);
           }

       }catch (\Exception $e){
           print_r($e);

       }



    }

}
