<?php

namespace Logilim\Plugins;

use Logilim\Configuration;
use Logilim\Plugin;

class RedisEnginePlugin extends Plugin {

    private $_redis = null;

    private $_data = null;

    private $_userId = null;

    private $_md5Id = null;

    const KEY_TOKEN = 'token';

    const KEY_LAST_LOGIN = 'logged_in_at';

    const KEY_lAST_REFRESH = 'refresh_at';

    const EXPIRE_SESSION_AT = 1209600;

    public function __construct() {
        $this->preparePlugin();
        $this->_redis = Configuration::getInstance()->redis;
    }

    public function __get($arg) {
        if ( array_key_exists($arg, $this->_data))
            return $this->_data[$arg];
        return null;
    }

    public function __set($arg, $val) {
        $this->_data[$arg] = $val;
    }

    /**
     * @param $user_id
     * @return $this
     */
    public function prepareToken($user_id) {
        $time = microtime();
        $this->_userId = $user_id;
        $raw = md5($user_id.$time);
        // $this->_data['token'] = strrev($raw).'_'.$user_id;
        // $this->_data['token'] = '"\\'.strrev($raw).'\\'.$user_id.'\\'.$time.'/"';   // \md5()\user_id\created_on/
        $this->_data['token'] = strrev($raw).'_'.$user_id.'_'.$time;   // \md5()\user_id\created_on/
        return $this;
    }

    public function saveFor() {
        list($key, $logged_in_at, $last_refresh) = array(
            $this->_nameCollection(),time(),time());

        $this->_redis->hmset($key,self::KEY_TOKEN,$this->_data['token'], self::KEY_LAST_LOGIN, $logged_in_at, self::KEY_lAST_REFRESH,$last_refresh);
        $this->_redis->expire($key, self::EXPIRE_SESSION_AT);
        return $this;
    }

    private function _nameCollection($id = null) {
        return sprintf('user:%s', (null === $id ? $this->_userId : $id));
    }



    public function getFor($id) {
        return $this->_redis->hgetAll('user:'.$id);
    }

    public function refreshFor($token) {
        if (!isset($token))
                throw new \Exception('argument missing in refreshFor()');

        $id = ($this->_getIdOutOfToken($token['old_token']));
        if($this->exist($token['old_token'])){
            $this->_redis->hmset($this->_nameCollection($id),self::KEY_TOKEN,$this->prepareToken($id)->token, self::KEY_lAST_REFRESH, time());
            //$this->_redis->expire($this->_md5Id,self::EXPIRE_SESSION_AT);
        } else {
            $this->prepareToken($id)->saveFor()->token;
        }
        return $this;
    }

    public function exist($token) {
        $id = $this->_getIdOutOfToken($token);
        $bucket = $this->_nameCollection($id);
        $ttl = $this->_redis->ttl($bucket);
        if ( $ttl <= 1) {
            $object = $this->_redis->hmget($bucket, 'token');
            // $object = $this->getFor($id);

            if($object == $token)
                return true;

            return false;
        } else {
            return true;
        }
    }

    public function _getIdOutOfToken($token) {
        $breakToken = explode('_', $token);
        $user_id = $breakToken[1];
        // $user_id = str_replace('"', '', $token);
        // $user_id = substr($token, strlen(substr($token, 0, 35)));
        // $user_id =  substr($user_id, 0, stripos($user_id, '\\'));

        return $user_id;
    }

    public function reset() {
        $this->_data = array();
        return $this;
    }

}
