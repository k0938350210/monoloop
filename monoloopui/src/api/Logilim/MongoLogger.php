<?php

namespace Logilim;

class MongoLogger{

    private static $_connect = null;
    private static $_db = null;
    private static $_collection = null;

    static public function getConnection(){
        try{
            self::$_connect = new \MongoClient(MONGO_CONNECTION);
            self::$_db = self::$_connect->selectDB(MONGO_DB);
            $exists = 0 ;
            foreach(self::$_db->listCollections() as $collection)
            {
                if(MONGO_COLLECTION == $collection->getName())
                    $exists = 1;
            }

            if(!$exists)
                self::$_collection = self::$_db->createCollection(MONGO_COLLECTION, TRUE, 1024, 10000);
            else
                self::$_collection = self::$_db->selectCollection(MONGO_COLLECTION);

        }catch (\Exception $e){
            print_r($e);
            exit;
        }

    }


}
