<?php

namespace Logilim;

abstract class Middleware extends \Slim\Middleware {

    public function buildResponse($is_success = 1, $errCode = null, $response = array()) {
        $data['is_success'] = $is_success;
        $data['errCode']  = (is_null($errCode) ? '' : strval($errCode));
        $data['exception']  = Errors::errorDescription($errCode);
        $data['response'] = $response;
        return json_encode($data);
    }



}
