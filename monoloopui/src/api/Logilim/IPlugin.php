<?php

namespace Logilim;

interface IPlugin {

    public function setPluginName($name);
    public function getPluginName();
}
