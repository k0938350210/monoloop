<?php
/**
 * Created as ChangePassword.php
 * Developer: Annum
 * Date:      4/23/15
 */
namespace Logilim\Routes;

class UserResetPasswordRoute extends \Logilim\Route {

    public function __construct() {
        $this->setCallable(function() {
            \UsersE::reset_password();
        });
        $this->setPattern('/api/reset/password');
        $this->via('POST');
    }
}
