<?php
/**
 * Created as UserUpdateRoute.php
 * Developer: Annum
 * Date:      4/23/15
 */

namespace Logilim\Routes;

class UserUpdateRoute extends \Logilim\Route {

    public function __construct() {
        $this->setCallable(function($id) {
            \UsersE::update($id);
        });
        $this->setPattern('/api/user/:id');
        $this->via('PATCH');
    }
}
