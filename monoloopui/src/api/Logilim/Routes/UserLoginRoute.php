<?php
/**
 * Created as UserLoginRoute.php.
 */

namespace Logilim\Routes;

/**
 * This will let API to verify the credentials of the user to get login state. The request should be based upon HTTP Post Method which will give the token + basic user information upon response.
 * @package     Logilim\Routes
 * @pattern    /api/user/login
 * @method      POST
 * @note        Lets user to login in
 * @version     v1.0
 */
class UserLoginRoute extends \Logilim\Route {

    public function __construct() {

        $this->setCallable(function() {

            \UsersE::login();
        });

       $this->setPattern('/api/user/login');
       $this->via('POST');
    }
}
