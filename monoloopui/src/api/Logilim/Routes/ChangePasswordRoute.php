<?php
/**
 * Created as ChangePassword.php
 * Developer: Annum
 * Date:      4/23/15
 */

namespace Logilim\Routes;

class ChangePasswordRoute extends \Logilim\Route {

    public function __construct() {
        $this->setCallable(function($id) {
            \UsersE::changePassword($id);
        });
        $this->setPattern('/api/user/:id/changePassword');
        $this->via('POST');
    }
}

