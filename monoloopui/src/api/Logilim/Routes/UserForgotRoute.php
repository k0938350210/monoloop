<?php
/**
 * Created as UserForgotRoute.php.
 * Developer: Annum
 * Date:      4/23/15
 */

namespace Logilim\Routes;

class UserForgotRoute extends \Logilim\Route {

    public function __construct() {
        $this->setPattern('/api/user/forgot');
        $this->setCallable(function() {
            \UsersE::forgot();
        });
        $this->via('POST');
    }
}