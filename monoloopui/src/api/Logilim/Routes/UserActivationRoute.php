<?php
/**
 * Created as UserUpdateRoute.php
 * Developer: Annum
 * Date:      4/23/15
 * Time:      4:25 PM
 */

namespace Logilim\Routes;


class UserActivationRoute extends \Logilim\Route {

    public function __construct() {
        $this->setCallable(function($id, $code) {
            \UsersE::activate($id, $code);
        });
        $this->setPattern('/api/user/:id/activation/:code');
        $this->via('POST');
    }
}
