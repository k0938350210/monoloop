<?php
/**
 * Created as UserRegistrationRoute.php
 * Developer: Annum
 * Date:      4/23/15
 */

namespace Logilim\Routes;

class UserRegistrationRoute extends \Logilim\Route {

    public function __construct() {
        $this->setCallable(function() {
            \UsersE::register();

        });
        $this->setPattern('/api/user/register');
        $this->via('POST');
    }
}