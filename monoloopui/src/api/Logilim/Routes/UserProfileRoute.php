<?php
/**
 * Created as UserProfileRoute.php
 * Developer: Annum
 * Date:      4/23/15
 */

namespace Logilim\Routes;


class UserProfileRoute extends \Logilim\Route {

    public function __construct() {
        $this->setCallable(function($id) {
            \UsersE::user_detail($id);
        });
        $this->setPattern('/api/user/:id');
        $this->via('GET');
    }
}
