<?php
/**
 * Created as UserLoginRoute.php.
 */

namespace Logilim\Routes;

/**
 * This will let API to validate the token
 * @package     Logilim\Routes
 * @pattern    /api/user/validate
 * @method      POST
 * @note        Lets validate the token
 * @version     v1.0
 */
class ValidateTokenRoute extends \Logilim\Route {

    public function __construct() {

        $this->setCallable(function() {

            \TokenE::validate();
        });

       $this->setPattern('/api/user/validate');
       $this->via('POST');
    }
}
