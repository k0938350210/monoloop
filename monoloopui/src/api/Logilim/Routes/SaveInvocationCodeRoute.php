<?php
/**
 * Created as UserProfileRoute.php
 * Developer: Sheraz
 * Date:      4/23/15
 */

namespace Logilim\Routes;


class SaveInvocationCodeRoute extends \Logilim\Route {

  public function __construct() {

      $this->setCallable(function() {

        // \AccountsE::saveInvocationCode();
          \UsersE::saveInvocationCode();
      });

     $this->setPattern('/api/user/save-invocation-code');
     $this->via('PUT');
  }
}
