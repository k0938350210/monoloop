<?php
/**
 * Created as TokenRefreshRoute.php.
 * Developer: Annum
 * Date:      4/23/15
 */

namespace Logilim\Routes;

class TokenRefreshRoute extends \Logilim\Route {

    public function __construct() {
        $this->setCallable(function() {
            \TokenE::refresh();
        });
        $this->setPattern('/api/token/refresh');
        $this->via('POST');
    }
}