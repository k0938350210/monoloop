<?php
/**
 * Created as RootRoute.php.
 */

namespace Logilim\Routes;

/**
 * This is the entry point of the application.
 * @package     Logilim\Routes
 * @pattern     /
 * @method      GET,POST,PUT,DELETE
 * @note        Entry Point of the Application.
 * @version     v1.0
 */
class RootRoute extends \Logilim\Route {

    public function __construct( ) {
        $this->setCallable(function() {
            echo ("<h1>API is running...</h1>");
        });

        $this->setPattern('/');
        $this->via('GET','POST','PUT','DELETE');
    }
}
