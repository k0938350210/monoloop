<?php

define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
define("DOCROOT", __DIR__);
define("DS", DIRECTORY_SEPARATOR);
define("VENDOR_PATH",getcwd().'/vendor');
// Partial
define("STABLE_API_VERSION",'v1.0');

require 'Slim/Slim.php';
require_once 'Logilim/Autoloader.php';

require_once(VENDOR_PATH.DS.'predis'.DS.'predis'.DS.'autoload.php');
if ( ! file_exists($file = __DIR__.'/vendor/autoload.php')) {
    throw new RuntimeException('Install dependencies to run this script.');
}

$loader = require_once $file;
$loader->add('MongoODM', __DIR__);

// Slim Loader
\Slim\Slim::registerAutoloader();
// Logilim Loader
\Logilim\Autoloader::registrar();

require_once 'config/config.php';

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */

date_default_timezone_set('UTC');

$app = new \Slim\Slim();
$app->config('debug', false);


$app->error(function (\Exception $e) use ($app){
  $message = $e->getMessage() . ' Line ' . $e->getLine() . ' ' . $e->getFile();

  $json_data = json_encode(array('text' => $message));
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_URL, 'https://hooks.slack.com/services/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');

  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: '.strlen($json_data))
  );

  curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
  $content = curl_exec($ch);
  curl_close($ch);

  echo 'Error found';
});
