<?php


/**
 * This is the entry point of the application
 *
 */

date_default_timezone_set('America/Los_Angeles');

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once 'bootstrap.php';



$app->notFound(function() use ($app) {
    $app->halt(404,"Seems, you missed HTTP method or forming a wrong url!");
});

if(!defined('STDIN'))
    $app->add(new OXMiddleware());

$app->router()->map(new \Logilim\Routes\RootRoute());
$app->router()->map(new \Logilim\Routes\UserLoginRoute());
$app->router()->map(new \Logilim\Routes\ValidateTokenRoute());
$app->router()->map(new \Logilim\Routes\UserRegistrationRoute());
$app->router()->map(new \Logilim\Routes\UserUpdateRoute());
$app->router()->map(new \Logilim\Routes\UserProfileRoute());
$app->router()->map(new \Logilim\Routes\TokenRefreshRoute());
$app->router()->map(new \Logilim\Routes\UserForgotRoute());
$app->router()->map(new \Logilim\Routes\UserActivationRoute());
$app->router()->map(new \Logilim\Routes\ChangePasswordRoute());
$app->router()->map(new \Logilim\Routes\SaveInvocationCodeRoute());
$app->router()->map(new \Logilim\Routes\UserResetPasswordRoute());
$app->run();
