<?php

use Logilim\Entity as Entity;
use Logilim\Obligatory as Obligatory;
use Logilim\Errors as Errors;

class AccountsE extends Entity {

  public static function crateDefaultAccountData(Accounts $account){

    date_default_timezone_set('America/Los_Angeles');

    $account->controlgroup = ['enable' => false , 'size' => 0 , 'days' => 0] ;

    // $account->uid = Accounts::max('uid') + 1 ;
    // var_dump(Accounts::all());die;
    $plugins = \Plugins::all() ;
    $p = [] ;
    foreach($plugins as $plugin){
      $p[] = ['_id' => new MongoId() , 'enabled' => false , 'plugin_id' => new MongoId($plugin->_id) ] ;
    }
    $account->cid = null;
    $account->plugins = $p ;
    $maxAccount = Accounts::all()->sortBy(function($a){
      return $a->uid;
    }, false)->first();
    if($maxAccount){
      if($maxAccount->uid >= 2000){
        $maxuid = $maxAccount->uid + 1;
      } else {
        $maxuid = 2000;
      }
    } else {
      $maxuid = 2000;
    }
    $account->uid = $maxuid;
    $account->cid = $account->uid;
    $account->is_test = false ;
    $account->lock_account_type = false ;
    $account->auto_activate_after_trial = false ;
    #trial expire next month;
    $time = strtotime(date("Y-m-d"));
    $final = date("Y-m-d", strtotime("+1 month", $time));
    $account->trialconvertion = $final;
    // $account->cid = (string)(new MongoID());
    // $account->s3_bucket = $account->cid ;
    $account->invocation = ['content_delivery' => 0 , 'anti_flicker' => false , 'timeout' => 0 , 'pre' => '' , 'post' => '' ] ;
    $account->scores = ['clickdepth' => 0 , 'duration' => 0 , 'loyalty' => 0 , 'recency' => 0 , 'brands' => 0] ;
    $account->control_group = ['enabled' => false , 'size' => 0 , 'days' => 0 ] ;
    $account->timezone = 'UTC';
    $account->date_format = 'D MMMM,YYYY';
    $account->time_format = 'HH:mm:ss' ;
  }



}
