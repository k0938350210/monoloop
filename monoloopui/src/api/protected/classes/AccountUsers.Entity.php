<?php

use Logilim\Entity as Entity;
use Logilim\Obligatory as Obligatory;
use Logilim\Errors as Errors;

use Logilim\Algorithm as Algorithm;
use Carbon\Carbon;

class AccountUsersE extends Entity {

  private $data = [];
  public function __construct(){
    #blank data ;
    $this->data['account_id'] = null ;
    $this->data['user_id'] = null;
    $this->data['email'] = '';
    $this->data['name'] = '';
    $this->data['company'] = '';
    $this->data['deleted'] = 0;
    $this->data['hidden'] = 0;
    $this->data['role'] = 'basic';
    $this->data['is_monoloop_admin'] = 0;
    $this->data['is_monoloop_support'] = 0;
    $this->data['dashboard_config'] = null;
    $this->data['support_type'] = 0;
    $this->data['support_enddate'] = null;
    $this->data['invitation'] = null;
  }

  public function createAdmin(Accounts $account){

    $mdate = new \MongoDate(strtotime(date('Y-m-d H:i:s')));

    $data = $this->data;

    $data['account_id'] = (string)$account->getId()->{'$id'};
    $data['email'] = $account->contact_email;
    $data['name'] = $account->contact_name;
    $data['company'] = $account->company;
    $data['is_monoloop_admin'] = 1;
    $data['invitation'] = new MongoId();
    $data['created_at'] = $mdate;
    $data['updated_at'] = $mdate;
    $data['role'] = 'admin';
    return $this->create($data);
  }

  public function createMonoloopSupport(Accounts $account){
    $data = $this->data;

    // $supportUser = Users::one(array('username' => 'monoloop_cs'));
    $mdate = new \MongoDate(strtotime(date('Y-m-d H:i:s')));

    #3 create new user ;
    $supportUser = new Users();
    $supportUser->username = 'su_'.$account->contact_email;

    $supportUser->pass = Algorithm::blowFishRobin('Monoloop083520');
    $supportUser->active_account = new MongoId($account->_id);
    $supportUser->confirmation_id = new MongoId();
    $supportUser->save();

    $data['account_id'] = (string)$account->_id;
    $data['user_id'] = $supportUser->_id;
    $data['email'] = 'support@monoloop.com';
    $data['company'] = 'monoloop';
    $data['name'] = 'monoloop_cs';
    $data['hidden'] = 1;
    $data['is_monoloop_support'] = 1;
    $data['created_at'] =  $mdate;
    $data['updated_at'] =  $mdate;
    $data['role'] = 'admin';
    return $this->create($data);
  }

  private function create($data){

    $mdate = new \MongoDate(strtotime(date('Y-m-d H:i:s')));

    $accountUser = new AccountUsers();
    $accountUser->account_id = (string)$data['account_id'];
    $accountUser->user_id = (string)$data['user_id'];
    $accountUser->email = $data['email'];
    $accountUser->name = $data['name'];
    $accountUser->company = $data['company'];
    $accountUser->deleted = $data['deleted'];
    $accountUser->hidden = $data['hidden'];
    $accountUser->is_monoloop_admin = $data['is_monoloop_admin'];
    $accountUser->is_monoloop_support = $data['is_monoloop_support'];
    $accountUser->dashboard_config = $data['dashboard_config'];
    $accountUser->support_type = $data['support_type'];
    $accountUser->support_enddate = $data['support_enddate'];
    $accountUser->invitation = $data['invitation'];
    $accountUser->created_at = $mdate;
    $accountUser->updated_at = $mdate;
    $accountUser->role = $data['role'];
    $accountUser->save();
    return $accountUser;
  }

}
