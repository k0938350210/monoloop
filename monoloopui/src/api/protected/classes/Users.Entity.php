<?php

use Logilim\Entity as Entity;
use Logilim\Obligatory as Obligatory;
use Logilim\Errors as Errors;
use \Logilim\Plugins\RedisEnginePlugin as RedisEnginePlugin;
use Logilim\Algorithm as Algorithm;
use Carbon\Carbon;

class UsersE extends Entity {

    public static function login() {
        $values = parent::getBody();
        $obligator = Obligatory::newFactoryInstance();
        $obligator->isJson();
        $obligator->username = true;
        $obligator->password = true;
        $obligator->setCollection($values);
        $values = $obligator->getCollection();
        try {
            $obligator->validate();

            if(!isset($values['by_id'])){
              $values['by_id'] = false;
              $values['user_id'] = '';
            }

            $user = new Users();
            if($values['by_id'] === true){
              $data = $user::one( array("_id"=> new \MongoId($values['user_id'])));
            } else {
              $data = $user::one( array("email"=>$values['username']) );
              if(!$data){
                $data = $user::one( array("username" => $values['username']) );
              }
            }


            $found = 0;
            $isSuperUser = false;
            $isActive = false;
            if($data){
                // check if active
                $isActive = empty($data->confirmation_id);

                // check if super-user
                if(strpos($data->username, "su_") === 0){
                  $_splitEmail = explode("_", $data->username);
                  if(count($_splitEmail) > 1){
                    $userAgainstEmail = $user::one(array("username" => $_splitEmail[1]));
                    if(!empty($userAgainstEmail)){
                      $isSuperUser = ($data->active_account == $userAgainstEmail->active_account);
                    }
                  }
                }

                if($isActive || $isSuperUser){
                  $id = $data->getId();
                  $data = $data->cleanData;
                  if($values['by_id'] === false){
                    if (Algorithm::matchPassword($values['password'], $data['pass']) === false) {
                        echo(parent::buildResponse(0, Errors::ERR_INVALID_PASSWORD, array()));
                        exit;
                    }
                  }

                  $found = 1;
                  self::addPlugin(new RedisEnginePlugin());
                  $plugin = self::getPlugin('RedisEnginePlugin');
                  // echo $plugin->prepareToken($id)->saveFor()->token;die;
                  parent::setResponseHeader('ML-Token', $plugin->prepareToken($id)->saveFor()->token);
                  echo(parent::buildResponse(1, "", $data ));
                }else{
                  echo(parent::buildResponse(0, Errors::ERR_INACTIVE_ACCOUNT, array()));
                  exit;
                }
              } else {
                echo(parent::buildResponse(0, Errors::ERR_NON_EXISTING_EMAIL, array()));
                exit;
            }


        } catch (Exception $ex) {
            echo parent::buildResponse(0, $ex->getCode(), array(), $ex->getMessage());
            exit;
        }
    }

    public static function saveInvocationCode() {
        $values = parent::getBody();
        // var_dump($values);die;
        $obligator = Obligatory::newFactoryInstance();
        $obligator->isJson();

        $obligator->content_delivery = true;
        $obligator->anti_flicker = true;
        $obligator->timeout = true;

        $obligator->setCollection($values);
        $values = $obligator->getCollection();
        try {
            $obligator->validate();
            // Register some plugins.

            if(!is_numeric($values['content_delivery']) || !in_array($values['content_delivery'], [0,1,2])){
              throw new \Exception("content_delivery must be numeric and between 1-3", Errors::ERR_MISSING_PARAMETERS);
            }
            if(!is_numeric($values['anti_flicker']) || !in_array($values['anti_flicker'], [0,1])){
              throw new \Exception("anti_flicker must be 0 or 1", Errors::ERR_MISSING_PARAMETERS);
            }
            if(!is_numeric($values['timeout'])){
              throw new \Exception("timeout must be numeric", Errors::ERR_MISSING_PARAMETERS);
            }
            self::addPlugin(new RedisEnginePlugin());
            $plugin = self::getPlugin('RedisEnginePlugin');

            $app = \Slim\Slim::getInstance();
            $token = $app->request()->headers('ML-Token', null);
            $user_id = $plugin->_getIdOutOfToken($token);

            $userM = new Users();
            $user = $userM::one( array("_id"=> new \MongoId($user_id)) );

            $active_account = $user->active_account;

            $accountM = new Accounts();

            $account = $accountM::one( array("_id"=> new \MongoId($active_account)) );

            $invocation = $account->invocation;;

            $invocation['content_delivery'] = $values['content_delivery'];
            $invocation['anti_flicker'] = $values['anti_flicker'];
            $invocation['timeout'] = $values['timeout'];

            $account->invocation = $invocation;
            $account->save();

            echo(parent::buildResponse(1, "", $account->cleanData ));
            exit;


        } catch (Exception $ex) {
            echo parent::buildResponse(0, $ex->getCode(), array(), $ex->getMessage());
            exit;
        }
    }

    public static function register() {

        // $date = \Carbon::now('UTC') ;
        $mdate = new \MongoDate(strtotime(date('Y-m-d H:i:s'))) ;

        $values = parent::getBody();
        $obligator = Obligatory::newFactoryInstance();
        $obligator->isJson();
        $obligator->firstname = true;
        $obligator->lastname = true;
        $obligator->email = true;
        $obligator->username = true;
        $obligator->user_type = true;
        $obligator->password = true;
        $obligator->confirmpassword = true;
        $obligator->setCollection($values);
        $values = $obligator->getCollection();
        try {
            $obligator->validate();

            #pass validate
            #1 create new account
            $account = new Accounts() ;

            \AccountsE::crateDefaultAccountData($account);

            $account->company = $values['firstname'].' '.$values['lastname'];
            $account->contact_email = $values['email'] ;
            $account->contact_name = $values['firstname'].' '.$values['lastname'] ;
            if(isset($values['user_type'])){
              $account->account_type = $values['user_type'];
            } else {
              $account->account_type = 'enterprise';
            }
            // $account->name = $values['domainname'] ;
            $account->is_reseller = 0;
            $account->parent_id = null ;
            $account->created_at = $mdate ;
            $account->updated_at = $mdate ;
            // account should be HIDDEN as default
            // Should be verified by email
            $account->hidden = 1;
            $account->save() ;

            if(isset($values['domainname'])){
              $domainName =  $values['domainname'] ;
              $domains = [];
              if(trim($domainName) != ''){
                $domains[] = ['_id' => new MongoId(), 'domain' => $domainName, 'privacy_option' => 'manual'];
                $account->domains = $domains;
                $account->save();
              }
            }

            #2 create admin account_user ;
            $accountUserService =  new \AccountUsersE() ;
            $adminUser = $accountUserService->createAdmin($account);
            #2.1 create support account
            $accountUserService->createMonoloopSupport($account) ;

            #3 create new user ;
            $user = new Users() ;
            $user->username = $values['email'] ;
            $user->pass =  Algorithm::blowFishRobin($values['password']);
            $user->active_account = new MongoId($account->_id) ;

            $user->confirmation_id = new MongoId() ;
            $user->created_at = $mdate ;
            $user->updated_at = $mdate;
            $user->save() ;




            $data['user_id'] = $user->getId()->{'$id'};
            $data['account_id'] = $account->getId()->{'$id'};
            $data['account_user_id'] = $adminUser->getId()->{'$id'};

            $adminUser->user_id = $data['user_id'];
            $adminUser->created_at = $mdate ;
            $adminUser->updated_at = $mdate ;
            $adminUser->save();

            if($data){
                $id = $user->getId();
                // $data['user'] = $user->cleanData;
                // $data['account'] = $account->cleanData;
                // $data['account_user'] = $adminUser->cleanData;
                self::addPlugin(new RedisEnginePlugin());
                $plugin = self::getPlugin('RedisEnginePlugin');
                parent::setResponseHeader('ML-Token', $plugin->prepareToken($id)->saveFor()->token);
                echo(parent::buildResponse(1, "", $data));
            }

        } catch (Exception $ex) {
            echo parent::buildResponse(0, $ex->getCode(), array(), $ex->getMessage());
            exit;
        }
    }

    public static function delete_user($id){
        if(isset($_GET[$id])){
            $users = Users::find( array( '_id'=> $id ));
            foreach($users as $user){
                $user->delete();
            }
        }
    }

    public static function update($id) {


    }

    public static function user_detail($id) {

    }

    public static function forgot() {

    }

    public static function changePassword($id){
        $values = parent::getBody();
        $obligator = Obligatory::newFactoryInstance();
        $obligator->isJson();
        $obligator->oldPassword = true;
        $obligator->newPassword = true;
        $obligator->setCollection($values);
        $values = $obligator->getCollection();
        try {
            $obligator->validate();
            $data = Users::id( $id );
            $data =  $data->cleanData;
            if (null == $data || empty($data)) {
                echo(parent::buildResponse(0, Errors::ERR_USEROBJID_INVALID, array()));
                exit;

            } elseif (Algorithm::matchPassword($values['oldPassword'], $data['password']) === false) {
                echo(parent::buildResponse(0, Errors::ERR_USEROBJID_INVALID, array()));
                exit;
            }
            $data = Users::one( array("email"=>$data['email']) );
            $data->password = Algorithm::blowFishRobin($values['newPassword']);
            $data->save();
            $user_id = $data->getId();
            $user = $data->cleanData;
            echo parent::buildResponse(1, "", array('id' => $user_id, 'password' => $user['password']));

        } catch (Exception $ex) {
            echo parent::buildResponse(0, Errors::ERR_MISSING_PARAMETERS, array(), $ex->getMessage());
            exit;
        }
    }


}
