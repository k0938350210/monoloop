<?php
/**
 * Entity File of Tokens.
 *
 * PHP version 5
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

use Logilim\Obligatory as Obligatory;
use Logilim\Errors as Errors;

class TokenE extends \Logilim\Entity
{
    public static function refresh()
    {
        $values = parent::getBody();
        $app = \Slim\Slim::getInstance();
        $token = $app->request()->headers('ML-Token', null);

        if (null == $token) {
            echo parent::buildResponse(0, Errors::ERR_TOKEN_MISSING);
            exit;
        }

        self::addPlugin(new \Logilim\Plugins\RedisEnginePlugin());
        $plugin = self::getPlugin('RedisEnginePlugin');
        $user_data = $app->request()->getBody();
        $obligator = Obligatory::newFactoryInstance();
        $obligator->isJson();
        /*
        $obligator->email = true;
        $obligator->password = true;
        */
        $obligator->setCollection($values);
        $values = $obligator->getCollection();

        try {
            /*
            $obligator->validate();
            //Write query to fetch user information
            $data = "";
            if (empty($data)) {
                echo(parent::buildResponse(0, Errors::ERR_NON_EXISTING_EMAIL, array()));
                exit;
            } elseif ($data['password'] !== $values['password']) {
                echo(parent::buildResponse(0, Errors::ERR_INVALID_PASSWORD, array()));
                exit;
            } else {
                $token = array(
                    "username" => $data['email'],
                    "password" => $data['password'],
                    "old_token" => $token
                );

                $new_token = $plugin->refreshFor($token)->token;
                $app->response()->header('ML-Token', $new_token);
                $app->halt(201);
            }
            */
            $obligator->validate();
            $token = array(
                "old_token" => $token
            );
            $new_token = $plugin->refreshFor($token)->token;
            parent::setResponseHeader('ML-Token',$new_token);
            echo(parent::buildResponse(1, "", [] ));

        } catch (Exception $ex) {
            echo parent::buildResponse(0, Errors::ERR_MISSING_PARAMETERS, array(), $ex->getMessage());
            exit;
        }


    }

    public static function validate()
    {
        $app = \Slim\Slim::getInstance();
        $token = $app->request()->headers('ML-Token', null);

        if (null == $token) {
            echo parent::buildResponse(0, Errors::ERR_TOKEN_MISSING);
            exit;
        }

        parent::setResponseHeader('ML-Token', $token);
        echo(parent::buildResponse(1, "Token is valid."));
        exit;
    }
}
