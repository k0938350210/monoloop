<?php
/**
 * Middleware of Rest  Call
 *
 * PHP version 5
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

use Logilim\Errors;
use Logilim\Plugins\RedisEnginePlugin;
use Logilim\Configuration;

/**
 * OXMiddleware that will be executed first.
 *
 * PHP version 5
 *
 * @category Middleware
 * @package  Middleware
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */
class OXMiddleware extends \Logilim\Middleware
{

    private $_app = null;

    /**
     *  Sets the instance of logger.
     */
    public function __construct()
    {
        $this->_app    = \Slim\Slim::getInstance();
    }//end __construct()


    /**
     * Extends parent method to pass request.
     *
     * @todo   Implement call() method.
     * @return null
     */
    public function call()
    {
        $allowedMethods = array(
                           'TOKEN',
                           'SECURE',
                           'TASK',
                          );
        $method = $this->_app->request()->getMethod();
        $uri = $this->_app->request()->getResourceUri();
        if (in_array($method, $allowedMethods) === true) {
            $this->next->call();
        } elseif($uri == '/'){
            $this->next->call();
        }else {
            $this->_acceptApplicationJSONOnly();
            $this->_validateMLVersion();
            $this->_validateMLToken();
            $this->next->call();

        }

    }//end call()


    /**
     * Checks for Content-Type as application/json in request.
     *
     * @return void
     */
    private function _acceptApplicationJSONOnly()
    {
        $contentType = $this->_app->request()->getContentType();
        $haystack = explode(';', $contentType);
        if ( in_array('application/json', $haystack) === false) {
            echo parent::buildResponse(0, Errors::ERR_INVALID_CONTENT_TYPE);
            exit;
        }

    }//end _acceptApplicationJSONOnly()


    /**
     * Validates the request token
     *
     * @return bool
     */
    private function _validateMLToken()
    {
        $token   = $this->_app->request()->headers('ML-Token', null);
        $userId  = null;
        $allowed = array(
            '/api/user/login',
            '/api/user/register',
            '/'
        );
        $uri = $this->_app->request()->getResourceUri();
        if (in_array($uri, $allowed) === true) {
            return true;
        }
        if($token !== null){
          // $token = '/'.$token;
        }

        // If you're in allowed methods, nothing is for you!
        if (($token) === null) {
            if ($uri !== '/api/token/refresh') {
                $plugin = new RedisEnginePlugin();
                if ($plugin->exist($token) === false) {
                    echo parent::buildResponse(0, Errors::ERR_TOKEN_EXPIRED);
                    exit;
                }
            }

            echo parent::buildResponse(0, Errors::ERR_TOKEN_MISSING);
            exit;

            //return true;
        } else {
            // Validate tokens.
            if ($uri == '/api/token/refresh')
                return true;

            $plugin = new RedisEnginePlugin();
            // echo $token; die;
            if ($plugin->exist($token) === false) {
                echo parent::buildResponse(0, Errors::ERR_TOKEN_EXPIRED);
                exit;
            }
        }

    }


    /**
     * Checks for active ML-Version
     *
     * @return bool
     */
    private function _validateMLVersion()
    {
        $deprecatedApis = Configuration::getInstance()->deprecated_apis;
        $fdVersion      = $this->_app->request()->headers('ML-Version', null);

        if (null === $fdVersion) {
            echo parent::buildResponse(0, Errors::ERR_ML_MISSING);
            exit;
        } elseif (in_array($fdVersion, $deprecatedApis) === true) {
            echo parent::buildResponse(0, Errors::ERR_ML_DEPRECATED);
            exit;
        } elseif ($fdVersion !== STABLE_API_VERSION) {
            echo parent::buildResponse(0, Errors::ERR_ML_INVALID);
            exit;
        }

        return true;

    }//end _validateFDVersion()


}//end class
