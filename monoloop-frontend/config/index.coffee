(->
  "use strict"
  nconf = require("nconf")

  # To set node environment variable through shell, execute e.g: 'export NODE_ENV=stagging'
  env = process.env.NODE_ENV or "production"
  nconf.argv().file __dirname + "/" + env + ".json"
  module.exports = nconf
  return
)()
