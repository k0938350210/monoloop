(function() {
  // Routing logic

  // License: see project's LICENSE file
  // Date: 17.07.12

  // Initializer of routers
  var batchCtrl, express, externalCtrl, handleAPI, hashCtrl, infoCtrl, postbackCtrl, profilesCtrl, publisher, responseHandler, testArchivevCtrl, util;

  batchCtrl = require('./app/controller/batch');

  hashCtrl = require('./app/controller/hashcheck');

  postbackCtrl = require('./app/controller/postback');

  profilesCtrl = require('./app/controller/profiles');

  infoCtrl = require('./app/controller/info');

  express = require('express');

  publisher = require('./redis-conn');

  util = new require('./app/utils/helper');

  handleAPI = require('./lib/mainHandler');

  responseHandler = require('./lib/responseHandler');

  externalCtrl = require('./app/controller/external');

  const healthcheck_controller = require('./app/controller/healthcheck');

  testArchivevCtrl = require('./app/controller/test-archive');

  module.exports = function(app) {
    var basicAuthenticate, handler, resHandler;
    handler = '';
    resHandler = '';
    app.use(function(request, res, next) {
      handler = new handleAPI(request.query);
      resHandler = new responseHandler(res);
      return next();
    });
    app.post('/external/', function(request, response) {
      console.log('external endpoint hit');
      return externalCtrl.process(request, response);
    });
    app.get('/healthcheck',function(request, response){
      return healthcheck_controller.check(request, response);
    });
    app.get('/personal-test/',function(request, response){
      return healthcheck_controller.test(request, response);
    });
    app.get('/tracks/', function(request, response) {
      var result;
      result = JSON.parse(handler.trackAPISanity());
      if (result.status === false) {
        return resHandler.buildResponse(result.statusCode, '', result.error);
      } else {
        return batchCtrl.parse(request, response);
      }
    });
    app.get('/hashcheck/', function(request, response) {
      var result;
      result = JSON.parse(handler.hashcheckAPISanity());
      if (result.status === false) {
        return resHandler.buildResponse(result.statusCode, '', result.error);
      } else {
        return hashCtrl.parse(request, response);
      }
    });
    app.get('/postback/', function(request, response) {
      var result;
      result = JSON.parse(handler.postbackAPISanity());
      if (result.status === false) {
        return resHandler.buildResponse(result.statusCode, '', result.error);
      } else {
        return postbackCtrl.parse(request, response);
      }
    });
    app.get('/profiles/', function(request, response) {
      if (basicAuthenticate(request, response)) {
        return profilesCtrl.getAll(request, response);
      }
    });
    app.get('/profiles/:param', function(request, response) {
      if (basicAuthenticate(request, response)) {
        return profilesCtrl.getSingleByID(request, response);
      }
    });
    app.get('/V1/profiles/', function(request, response) {
      if (basicAuthenticate(request, response)) {
        return profilesCtrl.getAll(request, response);
      }
    });
    app.get('/V1/profiles/:param1', function(request, response) {
      if (basicAuthenticate(request, response)) {
        return profilesCtrl.getSingle(request, response);
      }
    });
    app.get('/setInformation', function(request, response) {
      var result;
      if (basicAuthenticate(request, response)) {
        result = JSON.parse(handler.setInformationAPISanity());
        if (result.status === false) {
          return resHandler.buildResponse(result.statusCode, '', result.error);
        } else {
          return infoCtrl.setInformation(request, response);
        }
      }
    });
    app.get('/getInformation', function(request, response) {
      var result;
      if (basicAuthenticate(request, response)) {
        result = JSON.parse(handler.getInformationAPISanity());
        if (result.status === false) {
          return resHandler.buildResponse(result.statusCode, '', result.error);
        } else {
          return infoCtrl.getInformation(request, response);
        }
      }
    });
    app.get('/invalidatekey/', function(request, response) {
      var key, keySplit;
      key = request.query.key;
      if (key && typeof key === "string") {
        keySplit = key.split("_");
        key = keySplit[0] + "_" + keySplit[1];
        return publisher.del(key, function(err, status) {
          if (!err) {
            if (status === 1) {
              console.log(new Date() + ' : ' + key + ' has been deleted from redis');
              response.writeHead(204, {
                'Content-Type': 'text/javascript'
              });
              return response.end('');
            } else {
              console.log(new Date() + ' : ' + 'No key found in redis!');
              response.writeHead(200, {
                'Content-Type': 'text/javascript'
              });
              return response.end('MONOloop.MonoloopData = {"info":"No key found in redis!"};');
            }
          } else {
            response.writeHead(200, {
              'Content-Type': 'text/javascript'
            });
            console.log(new Date() + ' : ' + 'Could not perform delete operation!');
            return response.end('MONOloop.MonoloopData = {"error":"Could not perform delete operation!"};');
          }
        });
      } else {
        response.writeHead(200, {
          'Content-Type': 'text/javascript'
        });
        console.log(new Date() + ' : ' + 'Seems correct parameter is not provided!');
        return response.end('MONOloop.MonoloopData = {"error":"Seems correct parameter is not provided!"};');
      }
    });
    app.get('/emptyProfile', function(request, response) {
      var result;
      result = JSON.parse(handler.emptyProfileAPISanity());
      if (result.status === false) {
        return resHandler.buildResponse(result.statusCode, '', result.error);
      } else {
        return profilesCtrl.emptyProfile(request, response);
      }
    });
    app.get('/updatePrivacy', function(request, response) {
      var result;
      result = JSON.parse(handler.emptyProfileAPISanity());
      if (result.status === false) {
        return resHandler.buildResponse(result.statusCode, '', result.error);
      } else {
        return profilesCtrl.updatePrivacy(request, response);
      }
    });
    app.get('/test-archive/', function(request, response) {
      var result;
      testArchivevCtrl.process(request, response);
      return result = JSON.parse;
    });
    // Default RESTful routing
    app.all('/:param1/:param2?/:param3?/:param4?/', function(request, response) {
      var controller, err, ref;
      if (util.isDefined(request.query.url) && !util.checkIndex(request.query.url, 'trackServerSide')) {
        //Do auth for REST service
        if (1 === 10) {
          try {
            controller = require('./app/controller/' + ((ref = request.params.param3) != null ? ref : request.params.param1));
            return controller[request.method.toLowerCase()].call(controller, request, response);
          } catch (error) {
            err = error;
            console.log(new Date() + ": routes->app.all() - " + err);
            return response.end('MONOloop.MonoloopData = {"error":"Unknown error"};');
          }
        } else {
          return response.end('MONOloop.MonoloopData = {"error":"No Auth"};');
        }
      } else {
        response.writeHead(200, {
          'Content-Type': 'text/javascript'
        });
        return response.end('MONOloop.MonoloopData = {"error":"Not found"};');
      }
    });
    app.get('/archive/:gmid/:cid', function(request, response) {
      var queueJson, status;
      if (basicAuthenticate(request, response)) {
        queueJson = {
          gmid: request.params.gmid,
          cid: request.params.cid
        };
        status = publisher.PING();
        if (status) {
          return publisher.RPUSH('profilelist', JSON.stringify(queueJson));
        }
      }
    });
    return basicAuthenticate = function(request, response) {
      var auth, header, parts, password, token, username;
      header = request.headers['authorization'] || ''; // get the header
      token = header.split(/\s+/).pop() || ''; // and the encoded auth token
      auth = new Buffer(token, 'base64').toString(); // convert from base64
      parts = auth.split(/:/); // split on colon
      request.username = username = parts[0];
      request.password = password = parts[1];
      if (!username || !password) {
        response.writeHead(401, {
          'Content-Type': 'application/json'
        });
        response.end('{"status":"error","message":"No user/token"}');
        return false;
      } else if (util.createHashAndCompare(username, password)) {
        return true;
      } else {
        response.writeHead(403, {
          'Content-Type': 'application/json'
        });
        response.end('{"status":"error","message":"Invalid user/token"}');
        return false;
      }
    };
  };

}).call(this);
