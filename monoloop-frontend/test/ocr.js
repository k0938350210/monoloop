var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');

var mock_profile = require('../mock-data/profile1');

chai.use(chaiHttp);

describe("OCR unit test", function() {

  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("Should have ocr properties", function(done){
    chai.request(app)
    .get('/tracks/?url=nutjaa.win&cid=1001')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      let responce = helper.getResponceJson(res.text);
      let keysObject = {};
      let keysObjectOne = {};
      responce = JSON.parse(responce);
      expect(responce).to.not.have.property('error');
      expect(responce).to.have.property('success');
      expect(responce).to.have.property('eval');
      if (responce && responce.eval !== void 0) {
        responce = responce.eval.split(';');
        responce.forEach(function(obj) {
          obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
          obj = obj.replace(/\"/g, '');
          obj = obj.split(",");
          keysObject[obj[0]] = obj[1];
          keysObjectOne[obj[0]] = obj[1];
        });
        profileModel.findOne({
          _id: keysObject.gmid
        }, function(error, profile) {
          expect(profile).to.have.property('customerID');
          expect(profile).to.have.property('OCR');
          expect(profile).to.have.property('recently_used_device');
          expect(profile).to.have.property('ngmid');
          expect(profile).to.have.property('pgmid');
          expect(profile).to.have.property('profiles');
          profile.profiles.should.be.a('Array');
          expect(profile.profiles).to.have.lengthOf(1);

          let first_profile = profile.profiles[0];
          first_profile.should.have.property('device_info');
          first_profile.should.have.property('visit_server_date');
          first_profile.should.have.property('current');
          let current = first_profile.current;
          current.should.have.property('views');

          first_profile.visits[0].should.have.property('categories');
          first_profile.visits[0].should.have.property('carts');

          done();

        });
      }else{
        done();
      }
    });
  });

});