var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');
var reportModel = require('../app/model/ReportingArchiveAggregated');

var mock_profile = require('../mock-data/profile1');
var mock_page_element = require('../mock-data/page-element-default-experience');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("single experience #001", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleExperience();
  });

  it("should pass",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data.length.should.equal(1);
    ret_obj.data[0].content.should.equal('pass1');
    ret_obj.data[0].uid.should.equal('1');

    profile = await profileModel.findOne({ _id: gmid });
    profile.profiles[0].experiments[65].should.exist;
    profile.profiles[0].visits[0].experiments[65].should.exist;
  });

  it("should pass with archive #0011", async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    const res2 = await chai.request(app).get('/test-archive?gmid='+gmid);
    res2.status.should.equal(200);

    let report = await reportModel.findOne({cid: 1001});

    report.experimentMetrics[65].should.exist;
    report.experimentMetrics[65].experiment.should.exist;
    let experiment = report.experimentMetrics[65].experiment;
    experiment.totalPageViews.should.equal(1);
    experiment.totalVisits.should.equal(1);

    report.experiments[65].should.exist;
    experiment = report.experiments[65];
    experiment.views.should.equal(1);
    experiment.visitors.should.equal(1);
  });

  it("should not halt with signals #0014", async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    const res2 = await chai.request(app).get('/test-archive?gmid='+gmid+'&signals[]=65');
  });

  it("should pass with archive #0012", async function(){

    await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    await chai.request(app).get('/test-archive?gmid='+gmid);

    await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001x/');
    await chai.request(app).get('/test-archive?gmid='+gmid);

    await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    await chai.request(app).get('/test-archive?gmid='+gmid);

    let report = await reportModel.findOne({cid: 1001});

    let profile = await profileModel.findOne({_id: gmid});
    report.experiments[65].views.should.equal(profile.profiles[0].visits[0].experiments['65'].views);
    report.experiments[65].visitors.should.equal(profile.profiles[0].experiments['65'].visitorCount);
    report.experiments[65].visitors.should.equal(1);
  });

  it("should not count with no experience views #0013", async function(){
    await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    await chai.request(app).get('/test-archive?gmid='+gmid);

    for($i = 0 ; $i < 10 ; $i++){
      await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://not-visit-any-matches/');
      await chai.request(app).get('/test-archive?gmid='+gmid);
    }
    let report = await reportModel.findOne({cid: 1001});
    report.experiments['65'].views.should.equal(1);
  });
});

describe("single experience with false segment #0015", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleExperienceWithFalseSegment();
  });

  it("should pass",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));

    ret_obj.data.length.should.equal(0);

    let profile = await profileModel.findOne({ _id: gmid });
    should.not.exist(profile.profiles[0].experiments);
    should.not.exist(profile.profiles[0].visits[0].experiments);
  });
});

describe("single controlgroup #002", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleControlgroup();
  });

  it("should pass",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data.length.should.equal(1);
    ret_obj.data[0].abg.should.equal(true);
    ret_obj.data[0].uid.should.equal('1');

    profile = await profileModel.findOne({ _id: gmid });
    profile.profiles[0].controlgroups[65].should.exist;
    profile.profiles[0].visits[0].experiments[65].should.exist;
  });

  it("should pass with archive #0021", async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    const res2 = await chai.request(app).get('/test-archive?gmid='+gmid);
    res2.status.should.equal(200);

    let report = await reportModel.findOne({cid: 1001});

    let cg = report.experimentMetrics[65].controlGroup;
    cg.totalPageViews.should.equal(1);
    cg.totalVisits.should.equal(1);

    cg = report.experiments[65];
    cg.cg_views.should.equal(1);
    cg.cg_visitors.should.equal(1);

  });
});

describe("single controlgroup with false segment", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleControlgroupWithFalseSegment();
  });

  it("should pass",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));

    ret_obj.data.length.should.equal(0);

    let profile = await profileModel.findOne({ _id: gmid });
    should.not.exist(profile.profiles[0].controlgroups);
    should.not.exist(profile.profiles[0].visits[0].experiments);
  });
});

describe("single experience with normal content", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleExperienceAndNormalContent();
  });

  it("should pass",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data.length.should.equal(2);
    ret_obj.data[0].content.should.equal('pass1');
    ret_obj.data[0].uid.should.equal('1');

    ret_obj.data[1].uid.should.equal('2');
    ret_obj.data[1].content.should.equal('pass2');

    profile = await profileModel.findOne({ _id: gmid });
    profile.profiles[0].experiments[65].should.exist;
    profile.profiles[0].visits[0].experiments[65].should.exist;
  });

  it("should produce experience properties in profile #P001", async function(){
    let res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);

    res = await chai.request(app).get('/profiles/'+gmid).set('Authorization',helper.generateHeader(1001));
    let ret_obj = JSON.parse(res.text);

    let experiences = ret_obj.profiles[0].experiences;
    experiences.length.should.equal(1);
    expect(experiences[0]).to.have.key('uid','tstamp');

  });
});

describe("single controlgroup with normal content", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleControlgroupAndNormalContent();
  });

  it("should pass",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));

    ret_obj.data.length.should.equal(2);
    ret_obj.data[0].abg.should.equal(true);
    ret_obj.data[0].uid.should.equal('1');

    ret_obj.data[1].uid.should.equal('2');
    ret_obj.data[1].content.should.equal('pass2');

    profile = await profileModel.findOne({ _id: gmid });
    profile.profiles[0].controlgroups[65].should.exist;
    profile.profiles[0].visits[0].experiments[65].should.exist;
  });

  it("should produce experience properties in profile #P002", async function(){
    let res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));

    res = await chai.request(app).get('/profiles/'+gmid).set('Authorization',helper.generateHeader(1001));
    ret_obj = JSON.parse(res.text);

    let controlgroups = ret_obj.profiles[0].controlgroups;
    controlgroups.length.should.equal(1);
    expect(controlgroups[0]).to.have.key('uid','tstamp');
  });
});