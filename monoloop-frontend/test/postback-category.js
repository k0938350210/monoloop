var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');

var mock_profile = require('../mock-data/profile1');
var _ = require('underscore');
chai.use(chaiHttp);


let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';
var crypto = require('crypto');
var cryptSalt = '130293+109k21jl1c31c9312i3c910202';
// [{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"}]
var cid = '1001';
var hash = crypto.createHash('sha512').update('1001' + cryptSalt).digest('hex');


describe("Postback category unit test", function() {

  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });


  it("should add category to visit",async function(){
  	const res = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"}]')
  	res.status.should.equal(200);

  	let profile = await profileModel.findOne({_id: gmid});
  	let first_profile = profile.profiles[0];
  	let categories = first_profile.visits[0].categories;

  	let key = helper.crc32('normal');

  	categories.should.have.property('page_'+key);
  	let category = categories['page_'+key];
  	category.c.should.equal(1);
  	category.g.should.equal('page');
  	category.n.should.equal('normal');
  	category.t.should.equal(2);
  	expect(category.v).to.be.null;
  });

  it("should add category duration to visit",async function(){
  	const res = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"}]')
  	res.status.should.equal(200);

  	const res2 = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"},{"g":"page","n":"normal","v":null,"c":null,"t":2,"a":null,"id":"page","d":24}]')
  	res2.status.should.equal(200);
  	//[{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"},{"g":"page","n":"normal","v":null,"c":null,"t":2,"a":null,"id":"page","d":24}]
  	let profile = await profileModel.findOne({_id: gmid});
  	let first_profile = profile.profiles[0];
  	let categories = first_profile.visits[0].categories;

  	let key = helper.crc32('normal');



  	categories.should.have.property('page_'+key);
  	let category = categories['page_'+key];
  	category.c.should.equal(2);
  	category.g.should.equal('page');
  	category.n.should.equal('normal');
  	category.t.should.equal(2);
  	category.d.should.equal(24);


  	let sort_categories = profile.sortCategories(categories);
  	sort_categories[0]['n'].should.equal('normal');
  });


  it("should combine category from diffrence visits",async function(){
  	const res = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"}]')
  	res.status.should.equal(200);

  	const res2 = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"},{"g":"page","n":"normal","v":null,"c":null,"t":2,"a":null,"id":"page","d":24}]')
  	res2.status.should.equal(200);

  	const res3 = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid=&url=http://local-demo1wp.com/')

  	let ret_obj = JSON.parse(helper.getResponceJson(res3.text));
    let responce = ret_obj.eval.split(';');
    let keysObject = {};
    let keysObjectOne = {};

    responce.forEach(function(obj) {
      obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
      obj = obj.replace(/\"/g, '');
      obj = obj.split(",");
      keysObject[obj[0]] = obj[1];
    });

  	const res4 = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+keysObject.vid+'&cats=[{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"}]')
  	res.status.should.equal(200);

  	const res5 = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+keysObject.vid+'&cats=[{"g":"page","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"page"},{"g":"page","n":"normal","v":null,"c":null,"t":2,"a":null,"id":"page","d":50}]')
  	res2.status.should.equal(200);

  	const res6 = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid=&url=http://local-demo1wp.com/')

  	let profile = await profileModel.findOne({_id: gmid});

  	let all_categories = profile.allCategories(2);

  	let key = helper.crc32('normal');

  	all_categories['page_'+key].s.should.equal(1);
  });

  it("should not combine score from diffrence category id",async function(){
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":"category-2","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"}]')
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":"category-2","n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":"category-2","n":"normal","v":null,"c":null,"t":2,"a":null,"d":24,"id":"category-2"}]')

    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":2,"n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"}]')
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":2,"n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":2,"n":"normal","v":null,"c":null,"t":2,"a":null,"d":24,"id":"category-2"}]')

    let profile = await profileModel.findOne({_id: gmid});
    let all_categories = profile.allCategories(2);

    let key = helper.crc32('normal');
    all_categories['category-2_'+key].s.should.equal(1);
    all_categories['2_'+key].s.should.equal(1);

  });

  it("Should sort category 1",async function(){
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"}]');
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":1,"n":"normal","v":null,"c":null,"t":2,"a":null,"d":50,"id":"category-2"}]');

    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"hard","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"}]');
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"hard","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":1,"n":"hard","v":null,"c":null,"t":2,"a":null,"d":25,"id":"category-2"}]');

    let profile = await profileModel.findOne({_id: gmid});
    let sort_categories = profile.summarySortCategories(2);

    sort_categories[0].n.should.equal('normal');
  });

  it("Should sort category 2 #cate002",async function(){
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"}]');
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":1,"n":"normal","v":null,"c":null,"t":2,"a":null,"d":25,"id":"category-2"}]');

    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"hard","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"}]');
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"hard","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":1,"n":"hard","v":null,"c":null,"t":2,"a":null,"d":50,"id":"category-2"}]');

    let profile = await profileModel.findOne({_id: gmid});
    let sort_categories = profile.summarySortCategories(2);

    sort_categories[0].n.should.equal('hard');

    const res = await chai.request(app).get('/profiles/5b48b6f89adb4f0cb2e2ed46').auth(cid,hash);
    res.body.profiles[0].categories[0].n.should.equal('hard');

  });

  it("Monoloop profile should return summary categories",async function(){
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":2,"n":"floor1","d":0,"v":null,"c":null,"t":2,"a":null,"id":""}]');
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"normal","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":2,"n":"floor1","d":0,"v":null,"c":null,"t":2,"a":null,"id":""},{"g":1,"n":"normal","v":null,"c":null,"t":2,"a":null,"d":25,"id":"category-2"},{"g":2,"n":"floor1","v":null,"c":null,"t":2,"a":null,"d":50,"id":""}]');

    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"hard","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"}]');
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":1,"n":"hard","d":0,"v":null,"c":null,"t":2,"a":null,"id":"category-2"},{"g":1,"n":"hard","v":null,"c":null,"t":2,"a":null,"d":50,"id":"category-2"}]');

    let res = await chai.request(app).get('/profiles/'+gmid).set('Authorization',helper.generateHeader(1001));
    let ret_obj = JSON.parse(res.text);

    let profile = ret_obj.profiles[0];
    let categories = profile.categories;

    categories[0].g.should.equal(2);
    categories[0].n.should.equal('floor1');
    categories[0].s.should.equal(1);

    categories[1].g.should.equal(1);
    categories[1].n.should.equal('hard');
    categories[1].s.should.above(0.5);

    categories[2].g.should.equal(1);
    categories[2].n.should.equal('normal');
    categories[2].s.should.below(0.5);
    //We should remove json visit categories
    expect(profile.profiles[0].visits[0].categories).to.be.an('undefined');
  });

  it("Should work with invalid json", async function(){
    const res = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=xxx')
    res.status.should.equal(200);
  });
});
