var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var mock_profile = require('../mock-data/profile1');
var profileModel = require('../app/model/profile');

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

chai.use(chaiHttp);
//  updatePrivacyUrl = "/updatePrivacy/?cid=" + customerID + "&gmid={|} &mid={|} &skey={|} &personalization=0 &vid={|}";
describe('Profiles Privacy Api', function() {
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it('shoud update privacy', function(done) {
    chai.request(app)
    .get('/updatePrivacy?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&personalization=0')
    .end(function(err, res){
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.body.success.should.equal(true);
      res.body.should.have.property('privacyConfiguration');
      res.body.privacyConfiguration.personalization.should.equal(0);
      done();
    });
  });

  it('shoud update privacy2', function(done) {
    chai.request(app)
    .get('/updatePrivacy?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&personalization=1')
    .end(function(err, res){
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.body.success.should.equal(true);
      res.body.should.have.property('privacyConfiguration');
      res.body.privacyConfiguration.personalization.should.equal(1);
      done();
    });
  });

  it('if not pass property', function(done) {
    chai.request(app)
    .get('/updatePrivacy?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'')
    .end(function(err, res){
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.body.success.should.equal(true);
      res.body.should.have.property('privacyConfiguration');
      res.body.privacyConfiguration.personalization.should.equal(0);
      done();
    });
  });

  it('if wrong skey', function(done){
    chai.request(app)
    .get('/updatePrivacy?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'xx&vid='+vid+'')
    .end(function(err, res){
      console.log(res.text);
      res.status.should.equal(200);
      res.text.should.equal('MONOloop.MonoloopData = {"error":"GMID and skey does not match - emptyProfile"};')
      done();
    });
  });
});