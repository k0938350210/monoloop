var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var segment_model = require('../app/model/segment');
var profile_model = require('../app/model/profile');
var account_model = require('../app/model/account');
var mock_segment = require('../mock-data/audience');
var mock_account = require('../mock-data/account');

chai.use(chaiHttp);


describe("Segment unit test", function() {

  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_segments');
    await profile_model.collection.deleteMany();
    await segment_model.collection.deleteMany();
    await mock_segment.seed();
  });

  it('Should generate segment actives after archive',async function(){
  	let res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001');
  	let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    let responce = ret_obj.eval.split(';');
    let keysObject = {};
    let keysObjectOne = {};

    responce.forEach(function(obj) {
      obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
      obj = obj.replace(/\"/g, '');
      obj = obj.split(",");
      keysObject[obj[0]] = obj[1];
    });

 		const res2 = await chai.request(app).get('/test-archive?gmid='+keysObject.gmid);
		res2.status.should.equal(200);

    let profile = await profile_model.findOne({_id: keysObject.gmid});

    let visit = profile.profiles[0].visits[0];
    expect(visit.segments).to.have.all.keys('segment_1001','segment_1001_OCR','segment_1002','segment_1002_OCR');

    res = await chai.request(app).get('/profiles/'+keysObject.gmid).set('Authorization',helper.generateHeader(1001));
    ret_obj = JSON.parse(res.text);

    profile = ret_obj.profiles[0];
    let audiences = profile.audiences;
    audiences.length.should.equal(2);

    visit = profile.profiles[0].visits[0];
    expect(visit.segments).to.be.an('undefined');
    expect(visit.segments_InOut).to.be.an('undefined');
    expect(visit.aggregatedSegments).to.be.an('undefined');
  });
});

describe("Segment event", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_segments');
    await redis.del('1001_account');
    await profile_model.collection.deleteMany();
    await segment_model.collection.deleteMany();
    await account_model.collection.deleteMany();
    await mock_segment.seed();
    await mock_account.seedWithAllPlugins(1001);
  });

  it('Should create event #e001',async function(){
    let res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001');
    let keysObject = helper.getResponseKeyObject(res.text);
    let res2 = await chai.request(app).get('/test-archive?gmid='+keysObject.gmid);
    res2.status.should.equal(200);
    res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001&gmid='+keysObject.gmid+'&mid='+keysObject.mid+'&skey='+keysObject.skey);
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);
    response.events.length.should.equal(2);
    response.events[0].type.should.equal('audience-out');
  });
});

describe("Segment with disable plugin event", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_segments');
    await redis.del('1001_account');
    await profile_model.collection.deleteMany();
    await segment_model.collection.deleteMany();
    await account_model.collection.deleteMany();
    await mock_segment.seed();
    await mock_account.seedWithDisabledPlugins(1001);
  });

  it('Should create event #e002',async function(){
    let res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001');
    let keysObject = helper.getResponseKeyObject(res.text);
    let res2 = await chai.request(app).get('/test-archive?gmid='+keysObject.gmid);
    res2.status.should.equal(200);
    res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001&gmid='+keysObject.gmid+'&mid='+keysObject.mid+'&skey='+keysObject.skey+'&vid='+keysObject.vid);
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);
    response.events.length.should.equal(0);
  });
});

describe("Segment with disable segment event", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_segments');
    await redis.del('1001_account');
    await profile_model.collection.deleteMany();
    await segment_model.collection.deleteMany();
    await account_model.collection.deleteMany();
    await mock_segment.seed();
    await mock_account.seedWithDisabledSegmentOptions(1001);
  });

  it('Should create event #e002',async function(){
    let res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001');
    let keysObject = helper.getResponseKeyObject(res.text);
    let res2 = await chai.request(app).get('/test-archive?gmid='+keysObject.gmid);
    res2.status.should.equal(200);
    res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001&gmid='+keysObject.gmid+'&mid='+keysObject.mid+'&skey='+keysObject.skey+'&vid='+keysObject.vid);
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);
    response.events.length.should.equal(0);
  });
});

