
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;
var caching_experiment = require('../app/utils/caching/_experiment');

var mock_page_element = require('../mock-data/page-element-default-experience');
var mock_experiment = require('../mock-data/experience');
var mock_report = require('../mock-data/report');
var mock_profile = require('../mock-data/profile1');

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');
var experiment_model = require('../app/model/experiments');
var report_model = require('../app/model/ReportingArchiveAggregated');

var experimentCtrl = require('../app/controller/experiment');
var cachedLayer = require('../app/utils/cachingLayer');

var experiment_signal = require('../workers/modules/signals/experiment');
var base_variable = require('../workers/modules/baseVariables');

describe("TTL test", function() {
	beforeEach('Setup mock data',async function() {
		await redis.del('1001_65_experiment');
	});

	this.timeout(3000);
	it("Should expire experiment object in redis", function(done){
		let exp_obj = {
			experimentID: 65,
			customerID: 1001,
			pValue: 0.15
		};

	 	caching_experiment.checkRedisExpiry(1001, 65, exp_obj, function(res){
			res.should.equal('redis set successful');
		});

		setTimeout(function() {
			caching_experiment.checkRedisExpiry(1001, 65, exp_obj, function(res1){
				res1.should.equal('redis get successful');
			});
		}, 800);

		setTimeout(function() {
			caching_experiment.checkRedisExpiry(1001, 65, exp_obj, function(res2){
				res2.should.equal('redis set successful');
				done();
			});
		}, 1100);
	});
});

describe("P value test", function() {
	it("Should correct response1", function(done){
		let pvalue = caching_experiment.calculatePValue(1000,90,1000,100);
		pvalue.should.equal(0.223);
		done();
	});

	it("Should correct response1 invert", function(done){
		let pvalue = caching_experiment.calculatePValue(1000,100,1000,90);
		pvalue.should.equal(0.777);
		done();
	});
});

describe('P value update test', function(){
	beforeEach('Setup mock data',async function() {
		// clear relate collection
		await element_model.collection.deleteMany();
		await experiment_model.deleteMany();
		// create mock data
		await	mock_experiment.seed();
		await mock_page_element.seed2();
    await mock_page_element.seed();

	});

	it("Should get last updated", function(done){
		experimentCtrl.getExperimentLastUpdatedTimestamp(67, 1001, function(error, data){
			data.should.not.equal('');
			done();
		});
	});

	it("Should update pvalue", function(done){
		let exp_obj = {
			experimentID: 65,
			customerID: 1001,
			pValue: 0.15
		};

		experimentCtrl.updatePValue(exp_obj, null, function(err,data){
	  	element_model.findOne({
        _id: '5c131eb92423364d293c9871'
      }, function(error, element) {
        element.Experiment[0].pValue.should.equal(0.15);
        return done();
      });
		});
	});
});


describe('Report pvalue calculate test', function(){

	beforeEach('Setup mock data',async function() {
		// clear redis
		await redis.del('1001_65_experiment');
		// clear relate collection
		await report_model.collection.deleteMany();
		await element_model.deleteMany();
		// create mock data
		await mock_page_element.seed2();
		await mock_report.seed();
	});

	it('it should correct sum', function(done){
		cachedLayer.invokeExperimentCaching(1001, 65, function(er, result) {
				result.should.not.equal(null);
			 let ret = JSON.parse(result);

				ret.experimentID.should.equal(65);
				ret.customerID.should.equal(1001);
				ret.visitors.should.equal(362+311);
				ret.direct.should.equal(72+198);
				ret.cg_visitors.should.equal(150+120);
				ret.cg_direct.should.equal(40+45);
			done();
		});
	});
});

describe('prepareSignal function test', function(){
	beforeEach('Setup mock data',async function() {
		// clear redis
		await redis.del('1001_65_experiment');
		// clear relate collection
		await report_model.collection.deleteMany();
		await element_model.deleteMany();
		await profileModel.collection.deleteMany();
		// create mock data
		await mock_page_element.seed2();
		await mock_report.seed();
		await mock_profile.seed();
	});

	it('should correct process', function(done){
		profileModel.findOne({
      _id: "5b48b6f89adb4f0cb2e2ed46"
    }, function(error, profile) {
    	let base_var =  base_variable.initializeArchive(profile,false);
    	experiment_signal.prepareSignal(base_var,[65],function(err,data){
    	 	let temp = JSON.parse(data.experimentList[0]);
    		temp.pValue.should.equal(0.994);
    		done();
    	});

    });
	});
});

