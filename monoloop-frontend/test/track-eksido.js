var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');

var mock_page_element3 = require('../mock-data/page-element-simple');

chai.use(chaiHttp);

describe("track no-condition test with url option 0", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('2580_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_page_element3.seed2();
  });

  it("should return content", function(done){
    chai.request(app)
    .get('/tracks?cid=2580&gmid=&mid=&skey=&vid=&url=https://eksido.com/demo/boozt/landingpage/')
    .end(function(err, res){
      res.status.should.equal(200);
      let ret_obj = JSON.parse(helper.getResponceJson(res.text));
      ret_obj.success.should.equal(true);
      ret_obj.should.have.property('eval');
      ret_obj.should.have.property('debug');
      ret_obj.should.have.property('data');
      ret_obj.data.length.should.equal(1);
      ret_obj.data[0].content.should.equal('Only content');
      ret_obj.data[0].uid.should.equal('901');
      done();
    });
  });
});

describe("track no-condition test with url option 1", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('2580_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_page_element3.seed3();
  });

  it("should return content", function(done){
    chai.request(app)
    .get('/tracks?cid=2580&gmid=&mid=&skey=&vid=&url=https://eksido.com/demo/boozt/landingpage/?utm_source=demo&utm_medium=banner&utm_campaign=demo&utm_term=demo&utm_content=boozthttps://eksido.com/demo/boozt/landingpage/?utm_source=demo&utm_medium=banner&utm_campaign=demo&utm_term=demo&utm_content=boozt')
    .end(function(err, res){
      res.status.should.equal(200);
      let ret_obj = JSON.parse(helper.getResponceJson(res.text));
      ret_obj.success.should.equal(true);
      ret_obj.should.have.property('eval');
      ret_obj.should.have.property('debug');
      ret_obj.should.have.property('data');
      ret_obj.data.length.should.equal(1);
      ret_obj.data[0].content.should.equal('Only content');
      ret_obj.data[0].uid.should.equal('901');
      done();
    });
  });


});

describe("track test with url option 1 and utm term condition", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('2580_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_page_element3.seed4();
  });

  it("should return content", function(done){
  	let url = encodeURIComponent('https://eksido.com/demo/boozt/landingpage/?utm_source=demo&utm_medium=banner&utm_campaign=demo&utm_term=demo&utm_content=boozthttps://eksido.com/demo/boozt/landingpage/?utm_source=demo&utm_medium=banner&utm_campaign=demo&utm_term=demo&utm_content=boozt');
    chai.request(app)
    .get('/tracks?cid=2580&gmid=&mid=&skey=&vid=&url='+url)
    .end(function(err, res){
      res.status.should.equal(200);
      let ret_obj = JSON.parse(helper.getResponceJson(res.text));
      ret_obj.success.should.equal(true);
      ret_obj.should.have.property('eval');
      ret_obj.should.have.property('debug');
      ret_obj.should.have.property('data');
      ret_obj.data.length.should.equal(1);
      ret_obj.data[0].content.should.equal('Only content');
      ret_obj.data[0].uid.should.equal('901');
      done();
    });
  });


});
