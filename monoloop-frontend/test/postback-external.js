var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');

var mock_profile = require('../mock-data/profile1');
var _ = require('underscore');
chai.use(chaiHttp);


let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';


var crypto = require('crypto');
var cryptSalt = '130293+109k21jl1c31c9312i3c910202';
var cid = '1001';
var hash = crypto.createHash('sha512').update('1001' + cryptSalt).digest('hex');

describe("Postback external unit test", function() {

  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });


  it("should add external to global profile",async function(){
  	const res = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&external=[{"k":"email","v":"nutjaa@msn.com"},{"k":"old-key","v":"123456"}]')
  	res.status.should.equal(200);

  	let profile = await profileModel.findOne({_id: gmid});
  	profile.externalKeys.length.should.equal(2);
  	profile.externalKeys[0].k.should.equal('email');
  	profile.externalKeys[0].v.should.equal('nutjaa@msn.com');

  });


  it("should update exists external profile",async function(){
  	const res = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&external=[{"k":"email","v":"nutjaa@msn.com"},{"k":"old-key","v":"123456"}]')
  	res.status.should.equal(200);
  	const res2 = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&external=[{"k":"email","v":"nutjaa@msn2.com"},{"k":"old-key","v":"123456"}]')
  	res2.status.should.equal(200);

  	let profile = await profileModel.findOne({_id: gmid});
  	profile.externalKeys.length.should.equal(2);

  	profile.externalKeys[0].k.should.equal('email');
  	profile.externalKeys[0].v.should.equal('nutjaa@msn2.com');
  });


  it("Should work with profile all api",async function(){
    const res = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&external=[{"k":"email","v":"nutjaa@msn.com"},{"k":"old-key","v":"123456"}]')
    res.status.should.equal(200);

    let res2 = await chai.request(app).get('/profiles?externalKey=email&externalValue=nutjaa@msn.com').auth(cid,hash);
    res2.status.should.equal(200);
    res2.body.success.should.equal(true);
    res2.body.profiles.length.should.equal(1);

    res2 = await chai.request(app).get('/profiles?externalKey=email&externalValue=nutjaa@msnx.com').auth(cid,hash);
    res2.status.should.equal(200);
    res2.body.success.should.equal(true);
    res2.body.profiles.length.should.equal(0);
  });

});