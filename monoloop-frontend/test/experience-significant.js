var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');
var reportModel = require('../app/model/ReportingArchiveAggregated');

var mock_profile = require('../mock-data/profile1');
var mock_page_element = require('../mock-data/page-element-significant');
var caching_experiment = require('../app/utils/caching/_experiment');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("No significant - with experiment group", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed();
  });

  it("#000",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].content.should.equal('pass1');
  });
});

describe("No significant - with control group", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed2();
  });

  it("#001",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].abg.should.equal(true);
  });
});

describe("Significant 0 - with experiment group", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed3();
  });

  it("#002",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].content.should.equal('pass1');
  });
});

describe("Significant 0 - with control group", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed4();
  });

  it("#003",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].abg.should.equal(true);
  });
});


describe("Significant 2 - with experiment group", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed5();
  });

  it("#004",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    console.log(res.text);
    ret_obj.data[0].abg.should.equal(true);
  });
});

describe("Significant 2 - with control group", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed6();
  });

  it("#005",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].abg.should.equal(true);
  });
});

describe("Significant 1 - with experiment group - winner is experiment", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed7();
  });

  it("#006",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].content.should.equal('pass1');
    //let pvalue = caching_experiment.calculatePValue(1000,125,1000,90);
    //let pvalue = caching_experiment.calculatePValue(1000,90,1000,125);
  });
});


describe("Significant 1 - with control group - winner is experiment", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed8();
  });

  it("#007",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].content.should.equal('pass1');
  });
});


describe("Significant 1 - with experiment group - winner is control", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed9();
  });

  it("#008",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].abg.should.equal(true);
  });
});

describe("Significant 1 - with control group - winner is control", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_profile.seed();
    await mock_page_element.seed10();
  });

  it("#009",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data[0].abg.should.equal(true);
  });
});


describe("Test code for @affan", function(){
  it("same pvalue",function(){
    let pvalue = caching_experiment.calculatePValue(1000,125,1000,90);
    console.log(`pvalue is ${pvalue} winner is experience group`);

    pvalue = caching_experiment.calculatePValue(1000,90,1000,125);
    console.log(`pvalue is ${pvalue} winner is control group`);
  });
});