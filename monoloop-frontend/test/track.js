var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');

var mock_profile = require('../mock-data/profile1');
var mock_page_element = require('../mock-data/page-element-default-experience');
var mock_page_element2 = require('../mock-data/page-element-with-false-audience');
var mock_page_element3 = require('../mock-data/page-element-simple');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("track common test", function(){
  it("should not pass with no cid ", function(done){
    chai.request(app)
    .get('/tracks?url=nutjaa.win')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      res.text.should.equal('MONOloop.MonoloopData = {"error":"No CID provided!"};');
      done();
    });
  });

  it("should not pass if wrong skey",function(done){
    server(app)
    .get('/tracks?url=nutjaa.win&cid=1001&gmid=5bx48432c4234a6269fe4a54f&mid=5b48432c4234a6269fe4a550&skey=48310fc165d2776acd56de9a5ffdde95373ef0e10ef45a1508f3673e7083e7a85a5f419cd421cece70b1df16b26352a3e67f2015d2820cd9a410ac8a79585d67&debug=1')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      res.text.should.equal('MONOloop.MonoloopData = {"error":"GMID and skey does not match - tracks"};');
      done();
    });
  });
});

describe("track no-condition test", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element3.seed();
  });

  it("should return content", function(done){
    chai.request(app)
    .get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://local-demo1wp.com/')
    .end(function(err, res){
      res.status.should.equal(200);
      let ret_obj = JSON.parse(helper.getResponceJson(res.text));
      ret_obj.success.should.equal(true);
      ret_obj.should.have.property('eval');
      ret_obj.should.have.property('debug');
      ret_obj.should.have.property('data');
      ret_obj.data.length.should.equal(2);
      ret_obj.data[0].content.should.equal('Only content');
      ret_obj.data[0].uid.should.equal('1001');

      ret_obj.data[1].content.should.equal('Only content2');
      ret_obj.data[1].uid.should.equal('1002');
      done();
    });
  });
});

describe("track with experience unit test", function() {
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seed();
  });
  it("track with control group difference",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');

    res.status.should.equal(200);
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    ret_obj.data.length.should.equal(2);
    ret_obj.data[0].content.should.equal('pass1');
    ret_obj.data[1].abg.should.equal(true);

    profile = await profileModel.findOne({ _id: gmid });
    profile.profiles[0].controlgroups[67].should.exist;
    profile.profiles[0].experiments[65].should.exist;

  });

  it("track with no vid #no-vid-001",async function(){
    let res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let profile = await profileModel.findOne({ _id: gmid });
    console.log(profile.profiles[0].visits.length.should.equal(2));

    res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    profile = await profileModel.findOne({ _id: gmid });
    console.log(profile.profiles[0].visits.length.should.equal(2));
  });
});


describe("Not found path test", function(){
  it("track with not found path", function(done){
    chai.request(app)
    .get('/tracks?cid=1001')
    .end(function(err, res){
      res.status.should.equal(200);
      let ret_obj = JSON.parse(helper.getResponceJson(res.text));
      ret_obj.error.should.equal('No url provided!');
      done();
    });
  });

  it("track with not found path2", function(done){
    chai.request(app)
    .get('/xxx')
    .end(function(err, res){
      res.status.should.equal(200);
      let ret_obj = JSON.parse(helper.getResponceJson(res.text));
      ret_obj.error.should.equal('Not found');
      done();
    });
  });
});