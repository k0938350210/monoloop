var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');

var mock_profile = require('../mock-data/profile1');
var _ = require('underscore');
chai.use(chaiHttp);


let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("Postback content unit test", function() {

  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("should not error for invalid json",async function(){
    const res = await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&content_ids=xxx')
    res.status.should.equal(200);
  });
});