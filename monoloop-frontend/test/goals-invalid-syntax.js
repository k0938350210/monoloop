var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');
var reportModel = require('../app/model/ReportingArchiveAggregated');

var mock_goal = require('../mock-data/goal');
var mock_profile = require('../mock-data/profile1');

chai.use(chaiHttp);

describe("track goal test", function(){

	beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await mock_profile.seed();
    await mock_goal.seedInvalidSyntax();
  });

  it("should not crash", async function(){
    const res = await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001');
    let keysObject = helper.getResponseKeyObject(res.text);


  });

});