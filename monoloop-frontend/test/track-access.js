var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');

chai.use(chaiHttp);

describe("Tracks - access test - should use public access middleware", function() {
  it("should not pass with no query ", function(done){
    chai.request(app)
    .get('/tracks/?url=nutjaa.win')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      res.text.should.equal('MONOloop.MonoloopData = {"error":"No CID provided!"};');
      done();
    });
  });

  it("should check match key",function(done){
    server(app)
    .post('/tracks?url=nutjaa.win&cid=3033&gmid=5bx48432c4234a6269fe4a54f&mid=5b48432c4234a6269fe4a550&skey=48310fc165d2776acd56de9a5ffdde95373ef0e10ef45a1508f3673e7083e7a85a5f419cd421cece70b1df16b26352a3e67f2015d2820cd9a410ac8a79585d67&debug=1')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      res.text.should.equal("MONOloop.MonoloopData = {\"error\":\"No Auth\"};");
      done();
    });
  });
});