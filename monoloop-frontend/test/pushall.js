var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');

var mock_profile = require('../mock-data/profile1');
chai.use(chaiHttp);

describe("test pushall", function(){

  beforeEach('Setup mock data',async function() {
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });
  /*
  it("should correct process pushall", function(done){
    profileModel.findOne({
      _id: '5b48b6f89adb4f0cb2e2ed46',
      'profiles._id': '5b48b6f89adb4f0cb2e2ed47'
    }, function(error, profile) {
      let command = {};
      command.$pushAll = {};
      command.$pushAll['profiles.$.goals'] = ['data1','data2'];
      let options = {
        upsert: true
      }
      profileModel.updateOne({_id: '5b48b6f89adb4f0cb2e2ed46','profiles._id': '5b48b6f89adb4f0cb2e2ed47'}, command, options,
        function(err, updated) {
          profileModel.findOne({
            _id: '5b48b6f89adb4f0cb2e2ed46'
          }, function(error, profile) {
            profile.profiles[0].goals.length.should.equal(2);
            profile.profiles[0].goals[0].should.equal('data1');
            profile.profiles[0].goals[1].should.equal('data2');
            done();
          });

        }
      );
    });
  });
  */
  it("should correct process pushall2", function(done){
    profileModel.findOne({
      _id: '5b48b6f89adb4f0cb2e2ed46',
      'profiles._id': '5b48b6f89adb4f0cb2e2ed47'
    }, function(error, profile) {
      let command = {};
      command.$push = {};
      command.$push['profiles.$.goals'] = {};
      command.$push['profiles.$.goals'].$each = [{
        uid: 1,
        tstamp: 100,
        p: 1,
        OCR: false
      },{
        uid: 2,
        tstamp: 100,
        p: 1,
        OCR: false
      }];
      let options = {
        upsert: true
      }
      profileModel.updateOne({_id: '5b48b6f89adb4f0cb2e2ed46','profiles._id': '5b48b6f89adb4f0cb2e2ed47'}, command, options,
        function(err, updated) {
          profileModel.findOne({
            _id: '5b48b6f89adb4f0cb2e2ed46'
          }, function(error, profile) {
            profile.profiles[0].goals.length.should.equal(2);
            profile.profiles[0].goals[0].uid.should.equal(1);
            profile.profiles[0].goals[1].uid.should.equal(2);
            done();
          });

        }
      );
    });
  });
});