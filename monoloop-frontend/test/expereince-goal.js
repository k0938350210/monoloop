var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');
var reportModel = require('../app/model/ReportingArchiveAggregated');

var mock_profile = require('../mock-data/profile1');
var mock_page_element = require('../mock-data/page-element-default-experience');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("experience with goal", function(){
	beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seed();
  });

  it("should pass #001",async function(){
		await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
		await chai.request(app).get('/test-archive?gmid='+gmid);

		await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/?goal-reach=true');
		await chai.request(app).get('/test-archive?gmid='+gmid);

		let report = await reportModel.findOne({cid: 1001});
		report.experiments[65].direct.should.equal(1);

 		console.log('--------------------------');

		await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/?goal-reach=true');
		await chai.request(app).get('/test-archive?gmid='+gmid);

		report = await reportModel.findOne({cid: 1001});
		report.experiments[65].direct.should.equal(1);

  });
});
