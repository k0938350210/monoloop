var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var mock_profile = require('../mock-data/profile1');

var Url = require('url');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("Profile unit test", function() {

  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    mock_profile.seed();
  });

  it('Should insert referer', function(done) {
    let refurl = encodeURIComponent('https://nut.win?utm_source=google');
    let url = encodeURIComponent('https://nutjab.win');
    chai.request(app)
    .get('/tracks/?url='+url+'&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+'&urlref='+refurl)
    .end(function(err, res){
      res.status.should.equal(200);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        profile.VisitReferer.should.equal('https://nut.win?utm_source=google');
        profile.utm_source.should.equal('google');

        return done();
      });

    });
  });

  it('Should correct blank utm_source', function(done) {
    let refurl = encodeURIComponent('https://nut.win');
    let url = encodeURIComponent('https://nutjab.win');
    chai.request(app)
    .get('/tracks/?url='+url+'&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+'&urlref='+refurl)
    .end(function(err, res){
      res.status.should.equal(200);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        profile.VisitReferer.should.equal('https://nut.win');
        profile.utm_source.should.equal('');
        profile.utm_content.should.equal('');
        profile.utm_term.should.equal('');
        profile.utm_medium.should.equal('');
        profile.utm_campaign.should.equal('');
        return done();
      });

    });
  });


  it('Should collect utm from url', function(done) {
    let refurl = encodeURIComponent('https://nut.win');
    let url = encodeURIComponent('https://nutjab.win?utm_source=google');
    chai.request(app)
    .get('/tracks/?url='+url+'&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+'&urlref='+refurl)
    .end(function(err, res){
      res.status.should.equal(200);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        profile.utm_source.should.equal('google');
        return done();
      });

    });
  });

  it('Should collect utm term from url', function(done) {
    let refurl = encodeURIComponent('https://nut.win');
    let url = encodeURIComponent('https://eksido.com/demo/boozt/landingpage/?utm_source=demo&utm_medium=banner&utm_campaign=demo&utm_term=demo&utm_content=boozt');
    chai.request(app)
    .get('/tracks/?url='+url+'&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+'&urlref='+refurl)
    .end(function(err, res){
      res.status.should.equal(200);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        profile.utm_term.should.equal('demo');
        return done();
      });

    });
  });

  it('Should collect utm content from url', function(done) {
    let refurl = encodeURIComponent('https://nut.win');
    let url = encodeURIComponent('https://eksido.com/demo/boozt/landingpage/?utm_source=demo&utm_medium=banner&utm_campaign=demo&utm_term=demo&utm_content=boozt');
    chai.request(app)
    .get('/tracks/?url='+url+'&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+'&urlref='+refurl)
    .end(function(err, res){
      res.status.should.equal(200);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        profile.utm_content.should.equal('boozt');
        return done();
      });

    });
  });

  it('Should collect utm_medium from url', function(done) {
    let refurl = encodeURIComponent('https://nut.win');
    let url = encodeURIComponent('https://eksido.com/demo/boozt/landingpage/?utm_source=demo&utm_medium=banner&utm_campaign=demo&utm_term=demo&utm_content=boozt');
    chai.request(app)
    .get('/tracks/?url='+url+'&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+'&urlref='+refurl)
    .end(function(err, res){
      res.status.should.equal(200);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        profile.utm_medium.should.equal('banner');
        return done();
      });

    });
  });

  it('Should collect utm_campaign from url', function(done) {
    let refurl = encodeURIComponent('https://nut.win');
    let url = encodeURIComponent('https://eksido.com/demo/boozt/landingpage/?utm_source=demo&utm_medium=banner&utm_campaign=demo&utm_term=demo&utm_content=boozt');
    chai.request(app)
    .get('/tracks/?url='+url+'&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+'&urlref='+refurl)
    .end(function(err, res){
      res.status.should.equal(200);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        profile.utm_campaign.should.equal('demo');
        return done();
      });

    });
  });


});