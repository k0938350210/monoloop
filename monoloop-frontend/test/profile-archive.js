var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var reportModel = require('../app/model/ReportingArchiveAggregated');

chai.use(chaiHttp);

describe("profiole archive test", function(){
	beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await reportModel.collection.deleteMany();
  });

  it('should archive simple page view',async function(){
  	const res = await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001');
    let keysObject = helper.getResponseKeyObject(res.text);

   	await chai.request(app).get('/test-archive?gmid='+keysObject.gmid);
    let report = await reportModel.findOne({cid: 1001});
   	report.cid.should.equal(1001);
 		report.totalPageViews.should.equal(1);
 		report.totalPages.should.equal(1);
 		report.totalVisitors.should.equal(1);
 		report.totalVisits.should.equal(1);
  });
});