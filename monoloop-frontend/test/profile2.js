var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var mock_profile = require('../mock-data/profile1');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("Profile unit test", function() {

  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("Should alway pass", function(done){
    let i = 1;
    i.should.equal(1);
    done();
  });

  it("Should success create new profile", function(done) {
    chai.request(app)
    .get('/tracks/?url=nutjaa.win&cid=1001')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      let responce = helper.getResponceJson(res.text);
      let keysObject = {};
      let keysObjectOne = {};
      responce = JSON.parse(responce);
      expect(responce).to.not.have.property('error');
      expect(responce).to.have.property('success');
      expect(responce).to.have.property('eval');
      if (responce && responce.eval !== void 0) {
        responce = responce.eval.split(';');
        responce.forEach(function(obj) {
          obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
          obj = obj.replace(/\"/g, '');
          obj = obj.split(",");
          keysObject[obj[0]] = obj[1];
          keysObjectOne[obj[0]] = obj[1];
        });
        profileModel.findOne({
          _id: keysObject.gmid
        }, function(error, profile) {

          expect(profile).to.have.property('customerID');
          expect(profile).to.have.property('OCR');
          expect(profile).to.have.property('recently_used_device');
          expect(profile).to.have.property('ngmid');
          expect(profile).to.have.property('pgmid');
          expect(profile).to.have.property('profiles');
          profile.profiles.should.be.a('Array');
          expect(profile.profiles).to.have.lengthOf(1);
          let first_profile = profile.profiles[0];
          first_profile.should.have.property('device_info');
          first_profile.should.have.property('visit_server_date');
          first_profile.should.have.property('current');
          let current = first_profile.current;
          current.should.have.property('views');
          first_profile.visits.length.should.equal(1);
          let first_visit = first_profile.visits[0];
          first_visit.should.have.property('clicks');
          first_visit.clicks.should.equal(1);
          done();

        });
      }else{
        done();
      }
    });
  });

  it('Shoud add visit in existing profile', function(done) {
    chai.request(app)
    .get('/tracks/?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid=')
    .end(function(err, res){
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      let responce = helper.getResponceJson(res.text);
      let keysObject = {};
      let keysObjectOne = {};
      responce = JSON.parse(responce);
      expect(responce).to.not.have.property('error');
      expect(responce).to.have.property('success');
      expect(responce).to.have.property('eval');
      if (responce && responce.eval !== void 0) {
          responce = responce.eval.split(';');
          responce.forEach(function(obj) {
            obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
            obj = obj.replace(/\"/g, '');
            obj = obj.split(",");
            keysObject[obj[0]] = obj[1];
          });
          profileModel.findOne({
            _id: keysObject.gmid
          }, function(error, profile) {
            expect(profile).to.have.property('customerID');
            expect(profile).to.have.property('OCR');
            expect(profile).to.have.property('recently_used_device');
            expect(profile).to.have.property('ngmid');
            expect(profile).to.have.property('pgmid');
            expect(profile).to.have.property('profiles');
            profile.profiles.should.be.a('Array');
            expect(profile.profiles).to.have.lengthOf(1);
            let first_profile = profile.profiles[0];
            first_profile.should.have.property('device_info');
            first_profile.should.have.property('visit_server_date');
            first_profile.should.have.property('current');
            let current = first_profile.current;
            current.should.have.property('views');
            expect(profile.profiles[0].visits).to.have.lengthOf(2);
            return done();
          });
        } else {
          return done();
        }
    });
  });

  it('Add page view in existing profile', function(done) {
    chai.request(app)
    .get('/tracks/?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .end(function(err, res) {
      let responce;
      let keysObject = {};
      let keysObjectOne = {};
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      responce = helper.getResponceJson(res.text);
      responce = JSON.parse(responce);
      expect(responce).to.not.have.property('error');
      expect(responce).to.have.property('success');
      expect(responce).to.have.property('eval');
      if (responce && responce.eval !== void 0) {
        responce = responce.eval.split(';');
        responce.forEach(function(obj) {
          obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
          obj = obj.replace(/\"/g, '');
          obj = obj.split(",");
          keysObject[obj[0]] = obj[1];
        });
        profileModel.findOne({
          _id: keysObject.gmid
        }, function(error, profile) {
          expect(profile).to.have.property('customerID');
          expect(profile).to.have.property('OCR');
          expect(profile).to.have.property('recently_used_device');
          expect(profile).to.have.property('ngmid');
          expect(profile).to.have.property('pgmid');
          expect(profile).to.have.property('profiles');
          profile.profiles.should.be.a('Array');
          expect(profile.profiles).to.have.lengthOf(1);
          let first_profile = profile.profiles[0];
          first_profile.should.have.property('device_info');
          first_profile.should.have.property('visit_server_date');
          first_profile.should.have.property('current');
          let current = first_profile.current;
          current.should.have.property('views');
          expect(profile.profiles[0].visits).to.have.lengthOf(1);
          profile.profiles[0].visits[0].clicks.should.equal(2);
          expect(profile.profiles[0].current.views).to.have.lengthOf(2);
          return done();
        });
      } else {
        return done();
      }
    });
  });


  it('Shoud create Sub profile', function(done) {
    chai.request(app)
    .get('/tracks/?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+'&skey='+'&vid=')
    .end(function(err, res) {
      let responce;
      let keysObject = {};
      let keysObjectOne = {};
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      responce = helper.getResponceJson(res.text);
      responce = JSON.parse(responce);
      expect(responce).to.not.have.property('error');
      expect(responce).to.have.property('success');
      expect(responce).to.have.property('eval');
      if (responce && responce.eval !== void 0) {
        responce = responce.eval.split(';');
        responce.forEach(function(obj) {
          obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
          obj = obj.replace(/\"/g, '');
          obj = obj.split(",");
          keysObject[obj[0]] = obj[1];
        });
        profileModel.findOne({
          _id: keysObject.gmid
        }, function(error, profile) {
          expect(profile).to.have.property('customerID');
          expect(profile).to.have.property('OCR');
          expect(profile).to.have.property('recently_used_device');
          expect(profile).to.have.property('ngmid');
          expect(profile).to.have.property('pgmid');
          expect(profile).to.have.property('profiles');
          profile.profiles.should.be.a('Array');
          expect(profile.profiles).to.have.lengthOf(2);
          let second_profile = profile.profiles[1];
          second_profile.should.have.property('device_info');
          second_profile.should.have.property('visit_server_date');
          second_profile.should.have.property('current');
          let current = second_profile.current;
          current.should.have.property('views');
          second_profile.visits[0].should.have.property('clicks');

          return done();
        });
      } else {
        return done();
      }
    });
  });

  it('Add visit in existing profile (newly inserted)', function(done) {
    chai.request(app)
    .get('/tracks/?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid=')
    .end(function(err, res) {
      var responce;
      let keysObject = {};
      let keysObjectOne = {};
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      responce = helper.getResponceJson(res.text);
      responce = JSON.parse(responce);
      expect(responce).to.not.have.property('error');
      expect(responce).to.have.property('success');
      expect(responce).to.have.property('eval');
      if (responce && responce.eval !== void 0) {
        responce = responce.eval.split(';');
        responce.forEach(function(obj) {
          obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
          obj = obj.replace(/\"/g, '');
          obj = obj.split(",");
          keysObject[obj[0]] = obj[1];
        });
        profileModel.findOne({
          _id: keysObject.gmid
        }, function(error, profile) {
          expect(profile).to.have.property('customerID');
          expect(profile).to.have.property('OCR');
          expect(profile).to.have.property('recently_used_device');
          expect(profile).to.have.property('ngmid');
          expect(profile).to.have.property('pgmid');
          expect(profile).to.have.property('profiles');
          profile.profiles.should.be.a('Array');
          expect(profile.profiles).to.have.lengthOf(1);
          let first_profile = profile.profiles[0];
          first_profile.should.have.property('device_info');
          first_profile.should.have.property('visit_server_date');
          first_profile.should.have.property('current');
          let current = first_profile.current;
          current.should.have.property('views');
          expect(profile.profiles[0].visits).to.have.lengthOf(2);
          return done();
        });
      } else {
        return done();
      }
    });
  });

  it('should crash with wrong mid', function(done){
    chai.request(app)
    .get('/tracks/?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid=1'+mid+'&skey='+skey+'&vid='+vid)
    .end(function(err, res) {
      res.status.should.equal(502);
      console.log(res);
      done();
    });
  });
});