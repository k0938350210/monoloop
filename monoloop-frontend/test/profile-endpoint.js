var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var mock_profile = require('../mock-data/profile1');

chai.use(chaiHttp);

var crypto = require('crypto');

var cryptSalt = '130293+109k21jl1c31c9312i3c910202';
var cid = '1001';
var hash = crypto.createHash('sha512').update('1001' + cryptSalt).digest('hex');

describe("profiles endpoint", function(){
  beforeEach('Setup mock data',async function() {
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("should require auth token",async function(){
    const res = await chai.request(app).get('/profiles');
    res.status.should.equal(401);
    res.body.status.should.equal('error');
  });

  it("should return listing", async function(){
    const res = await chai.request(app).get('/profiles').auth(cid,hash);
    res.status.should.equal(200);
    res.body.success.should.equal(true);
    res.body.profiles.length.should.equal(2);
  });

  it("should return listing with unique id", async function(){
    await chai.request(app).get('/tracks?cid=1001&url=non&uniqueID=nut');
    const res = await chai.request(app).get('/profiles').auth(cid,hash);
    res.status.should.equal(200);
    res.body.success.should.equal(true);
    res.body.profiles.length.should.equal(3);

    const res2 = await chai.request(app).get('/profiles?uniqueID=nut').auth(cid,hash);
    res2.status.should.equal(200);
    res2.body.success.should.equal(true);
    res2.body.profiles.length.should.equal(1);
  });

  it("shound single profile", async function(){
    const res = await chai.request(app).get('/profiles/5b48b6f89adb4f0cb2e2ed46').auth(cid,hash);
    res.status.should.equal(200);
    res.body.success.should.equal(true);
    res.body.profiles[0]._id.should.equal('5b48b6f89adb4f0cb2e2ed46');
  });

  it("should single profile by uniqueID", async function(){
    await chai.request(app).get('/tracks?cid=1001&url=non&uniqueID=nut');
    const res = await chai.request(app).get('/profiles/nut').auth(cid,hash);

    res.status.should.equal(200);
    res.body.success.should.equal(true);
    res.body.profiles[0].uniqueID.should.equal('nut');
    should.not.exist(res.body.profiles[0].inactive_webhook_ids);
    should.exist(res.body.profiles[0].uniqueID);
  });

  it("uniqueID should not across account #001", async function(){
    await chai.request(app).get('/tracks?cid=1002&url=non&uniqueID=nut');

    const res = await chai.request(app).get('/profiles/nut').auth(cid,hash);

    res.status.should.equal(200);
    res.body.success.should.equal(true);
    res.body.profiles.length.should.equal(0);
  });
});