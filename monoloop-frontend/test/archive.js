var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

chai.use(chaiHttp);

var profileModel = require('../app/model/profile');
var reportModel = require('../app/model/ReportingArchiveAggregated');
var mock_profile = require('../mock-data/profile1');

let gmid = '5b48b6f89adb4f0cb2e2ed46';

describe('Archive test controller checl', function() {
	beforeEach('Setup mock data',async function() {
    await profileModel.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await mock_profile.seed();
  });


	it('Should access test archive endpoint',async function(){

		const res = await chai.request(app).get('/test-archive?gmid='+gmid);
		res.status.should.equal(200);

		let report = await reportModel.findOne({cid: 1001});
 		report.cid.should.equal(1001);
 		report.totalPageViews.should.equal(1);
 		report.totalPages.should.equal(1);
 		report.totalVisitors.should.equal(1);
 		report.totalVisits.should.equal(1);
	});
});