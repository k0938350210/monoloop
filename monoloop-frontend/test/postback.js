var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');

var mock_profile = require('../mock-data/profile1');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("Postback basoc unit test", function() {

  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it('post back categories ', function(done) {
    chai.request(app)
    .get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cats=[{"g":4870,"n":"data1","d":0,"v":null,"c":null,"t":0,"a":0},{"g":4870,"n":"uncategorized","d":0,"v":null,"c":null,"t":0,"a":0},{"g":4871,"n":"data1","d":0,"v":null,"c":null,"t":0,"a":0},{"g":4871,"n":"xxx","d":0,"v":null,"c":null,"t":0,"a":0}]')
    .end(function(err, res){
      var responce;
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      responce = helper.getResponceJson(res.text);
      responce = JSON.parse(responce);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        expect(profile).to.have.property('customerID');
        expect(profile).to.have.property('OCR');
        expect(profile).to.have.property('recently_used_device');
        expect(profile).to.have.property('ngmid');
        expect(profile).to.have.property('pgmid');
        expect(profile).to.have.property('profiles');
        profile.profiles.should.be.a('Array');
        expect(profile.profiles).to.have.lengthOf(1);
        let first_profile = profile.profiles[0];
        first_profile.should.have.property('device_info');
        first_profile.should.have.property('visit_server_date');
        first_profile.should.have.property('current');
        let current = first_profile.current;
        current.should.have.property('views');

        first_profile.visits[0].should.have.property('categories');
        Object.keys(first_profile.visits[0].categories).should.have.length(4);
        return done();
      });
    });
  });

  it('post back Basket ', function(done) {
    chai.request(app)
    .get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&cart={"ts":1486543177329,"basketObj":[{"n":"LELE DEmo1","s":"xxx111","p":"590.00","q":"1","c":"฿","id":"1486543138636"}]}')
    .end(function(err, res){
      var responce;
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      responce = helper.getResponceJson(res.text);
      responce = JSON.parse(responce);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        expect(profile).to.have.property('customerID');
        expect(profile).to.have.property('OCR');
        expect(profile).to.have.property('recently_used_device');
        expect(profile).to.have.property('ngmid');
        expect(profile).to.have.property('pgmid');
        expect(profile).to.have.property('profiles');
        profile.profiles.should.be.a('Array');
        expect(profile.profiles).to.have.lengthOf(1);
        let first_profile = profile.profiles[0];
        first_profile.should.have.property('device_info');
        first_profile.should.have.property('visit_server_date');
        first_profile.should.have.property('current');
        let current = first_profile.current;
        current.should.have.property('views');

        first_profile.visits[0].should.have.property('carts');
        return done();
      });
    });
  });

  it('post back Purchases ', function(done) {
    chai.request(app)
    .get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&pc=[{"id":"1486543138636","p":true}]')
    .end(function(err, res){
      var responce;
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      responce = helper.getResponceJson(res.text);
      responce = JSON.parse(responce);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        expect(profile).to.have.property('customerID');
        expect(profile).to.have.property('OCR');
        expect(profile).to.have.property('recently_used_device');
        expect(profile).to.have.property('ngmid');
        expect(profile).to.have.property('pgmid');
        expect(profile).to.have.property('profiles');
        profile.profiles.should.be.a('Array');
        expect(profile.profiles).to.have.lengthOf(1);
        let first_profile = profile.profiles[0];
        first_profile.should.have.property('device_info');
        first_profile.should.have.property('visit_server_date');
        first_profile.should.have.property('current');
        let current = first_profile.current;
        current.should.have.property('views');

        first_profile.visits[0].should.have.property('pv');
        return done();
      });
    });
  });

  it('post back Search ', function(done) {
    chai.request(app)
    .get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&st=["data","data 2"]')
    .end(function(err, res){
      var responce;
      res.status.should.equal(200);
      expect(err).to.be.null;
      res.text.should.be.a('String');
      responce = helper.getResponceJson(res.text);
      responce = JSON.parse(responce);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        expect(profile).to.have.property('customerID');
        expect(profile).to.have.property('OCR');
        expect(profile).to.have.property('recently_used_device');
        expect(profile).to.have.property('ngmid');
        expect(profile).to.have.property('pgmid');
        expect(profile).to.have.property('profiles');
        profile.profiles.should.be.a('Array');
        expect(profile.profiles).to.have.lengthOf(1);
        let first_profile = profile.profiles[0];
        first_profile.should.have.property('device_info');
        first_profile.should.have.property('visit_server_date');
        first_profile.should.have.property('current');
        let current = first_profile.current;
        current.should.have.property('views');

        first_profile.visits[0].should.have.property('searches');
        return done();
      });
    });
  });


  it('post back uniqueID not exists',async function(){
    await chai.request(app).get('/postback?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&uniqueID=xxxx');
    let profile = await profileModel.findOne({_id: gmid});
    expect(profile).to.have.property('customerID');
  });
});