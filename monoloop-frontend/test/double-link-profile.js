var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

chai.use(chaiHttp);

var profileModel = require('../app/model/profile');

describe('Double profile unit test', function() {
	beforeEach('Setup mock data',async function() {
    await profileModel.collection.deleteMany();
  });

  it('Should success create link profile', async function(){
  	let res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1002');
  	let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    let responce = ret_obj.eval.split(';');
    let keysObject = {};
    let keysObjectOne = {};

    responce.forEach(function(obj) {
      obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
      obj = obj.replace(/\"/g, '');
      obj = obj.split(",");
      keysObject[obj[0]] = obj[1];
    });

    let gmid1 = keysObject.gmid;

 		let res2 = await chai.request(app).get('/test-archive?gmid='+gmid1);
		res2.status.should.equal(200);

		res = await chai.request(app).get('/tracks/?url=nutjaa2.win&cid=1002');
  	ret_obj = JSON.parse(helper.getResponceJson(res.text));
    responce = ret_obj.eval.split(';');
    keysObject = {};
    keysObjectOne = {};

    responce.forEach(function(obj) {
      obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
      obj = obj.replace(/\"/g, '');
      obj = obj.split(",");
      keysObject[obj[0]] = obj[1];
    });

    let gmid2 = keysObject.gmid;

 		res2 = await chai.request(app).get('/test-archive?gmid='+gmid2);
		res2.status.should.equal(200);

		let profile1 = await profileModel.findOne({_id: gmid1});
		let profile2 = await profileModel.findOne({_id: gmid2});

		profile1.ngmid.should.equal(profile2.id);
		profile2.pgmid.should.equal(profile1.id);

		res = await chai.request(app).get('/tracks/?url=nutjaa3.win&cid=1002');
  	ret_obj = JSON.parse(helper.getResponceJson(res.text));
    responce = ret_obj.eval.split(';');
    keysObject = {};
    keysObjectOne = {};

    responce.forEach(function(obj) {
      obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
      obj = obj.replace(/\"/g, '');
      obj = obj.split(",");
      keysObject[obj[0]] = obj[1];
    });

    let gmid3 = keysObject.gmid;

 		res2 = await chai.request(app).get('/test-archive?gmid='+gmid3);
		res2.status.should.equal(200);

		profile2 = await profileModel.findOne({_id: gmid2});
		let profile3 = await profileModel.findOne({_id: gmid3});

		profile2.ngmid.should.equal(profile3.id);
		profile3.pgmid.should.equal(profile2.id);
		expect(profile3.ngmid).to.be.null;
  });

});