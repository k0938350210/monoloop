var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var webhook_model = require('../app/model/webhook');
var mock_webhook = require('../mock-data/webhook');
var mock_profile = require('../mock-data/profile1');

var inactive_service = require('../app/services/profile/inactive');
//require('mongoose').set('debug', true);


describe("Inactive webhook list test", function() {
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await webhook_model.collection.deleteMany();
    await mock_webhook.seedInactive();
  });

  it("should return 2 inactive webhook", async function(){
    let webhooks = await inactive_service.getInactiveWebhooks();
    webhooks.length.should.equal(2);
  });
});


describe("Profile in inactive webhook condition", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await webhook_model.collection.deleteMany();
    await profileModel.collection.deleteMany();
    await mock_webhook.seedInactive();
    await mock_profile.seed();
  });


  it("should return inactive profiles", async function(){
    let webhooks = await inactive_service.getInactiveWebhooks();

    let profiles = await inactive_service.getInactiveProfiles(webhooks);

    profiles.length.should.equal(0);
  });
})


describe("Profile in inactive webhook condition-2", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('webhook-inactive-profile-list');
    await webhook_model.collection.deleteMany();
    await profileModel.collection.deleteMany();
    await mock_webhook.seedInactive();
    await mock_profile.seedWithUniqueID();
  });


  it("should return inactive profiles", async function(){
    let webhooks = await inactive_service.getInactiveWebhooks();
    let profiles = await inactive_service.getInactiveProfiles(webhooks);

    profiles.length.should.equal(1);
  });

  it("should update profile inactive flag #032", async function(){
    let webhooks = await inactive_service.getInactiveWebhooks();
    let profiles = await inactive_service.getInactiveProfiles(webhooks);
    await inactive_service.setInactiveFlagToProfiles(webhooks, profiles);

    let len = await redis.llen('webhook-inactive-profile-list');
    len.should.equal(2);

    let profile = await profileModel.findOne({_id: "5b48b6f89adb4f0cb2e2ed46"});
    profile.inactive_webhook_ids.length.should.equal(2);

    profiles = await inactive_service.getInactiveProfiles(webhooks);
    profiles.length.should.equal(0);
    await inactive_service.setInactiveFlagToProfiles(webhooks, profiles);

    len = await redis.llen('webhook-inactive-profile-list');
    len.should.equal(2);
  });
}),

describe("Profile in inactive webhook condition 3", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await webhook_model.collection.deleteMany();
    await profileModel.collection.deleteMany();
    await mock_webhook.seedInactive();
    await mock_profile.seedWithExternalKeys();
  });


  it("should return inactive profiles", async function(){
    let webhooks = await inactive_service.getInactiveWebhooks();

    let profiles = await inactive_service.getInactiveProfiles(webhooks);

    profiles.length.should.equal(1);
  });
})