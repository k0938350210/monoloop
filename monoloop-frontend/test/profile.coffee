chai = require('chai')
chaiHttp = require('chai-http')
data = require './json'
server = data.base_url
console.log server
should = chai.should()
expect = chai.expect
chai.use chaiHttp
profileModel = require '../app/model/profile'
helper = require './helper'

mongoose = require './../mongoose'
redis = require './../redis-conn'
redisSentinal = require './../redis-sentinel-conn'


# contain value of gmid, mid, skey and vid of current response
keysObject = {}
# contain value of gmid, mid, skey and vid of current response
keysObjectOne = {}

waitTime = data.waitTime
describe 'System Status',->
  before (done) ->
    chai.request(server).get("/").end (err, res) ->
      if err.status isnt undefined  and err.status is 404
        return done()
      else if err
        return done( throw new Error('Server is not running'))
    return
  beforeEach (done)->
    setTimeout ()->
      return done()
    , waitTime
    return
  it 'Mongodb server is running', (done)->
    db = mongoose.connection
    expect(db.readyState).to.deep.equal 1
    return done()
    ###db.on 'connecting', (err)->
      console.log "Mongodb status "+mongoose.connection.readyState
      expect(mongoose.connection._readyState).to.deep.equal 1
      return done()
    db.on 'connected', ()->
      expect(mongoose.connection.readyState).to.deep.equal 1
      return done()
      
    db.on 'error', (err)->
      expect(mongoose.connection.readyState).to.deep.equal 1
      return done()  ### 
    return
  it 'Redis is running', (done)->
    expect(redis.status).to.deep.equal 'ready'
    return done()
  it 'Redis-sentinal is running', (done)->
    expect(redisSentinal.status).to.deep.equal 'ready'
    return done()
  return

describe 'Monoloop Track Profiles Apis', ->
  before (done) ->
    chai.request(server).get("/").end (err, res) ->
      if err.status isnt undefined  and err.status is 404
        return done()
      else if err
        
        return done( throw new Error('Server is not running'))
    return
  beforeEach (done)->
    setTimeout ()->
      return done()
    , waitTime
    return
  it 'Create profile', (done)->
  
    createProfileUrl = data.url
    createProfileUrl = createProfileUrl.replace("gmid={|}","gmid=")
    createProfileUrl = createProfileUrl.replace("skey={|}","skey=")
    createProfileUrl = createProfileUrl.replace("mid={|}","mid=")
    createProfileUrl = createProfileUrl.replace("vid={|}","vid=")
    chai.request(server).get(createProfileUrl).end (err, res) ->

      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          keysObjectOne[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(1)
          expect(profile).to.have.deep.property('profiles.0.device_info')
          expect(profile).to.have.deep.property('profiles.0.visit_server_date')
          expect(profile).to.have.deep.property('profiles.0.current')
          expect(profile).to.have.deep.property('profiles.0.current.views')
          expect(profile).to.have.deep.property('profiles.0.visits.0.clicks')
          return done()
        return
      else
        return done()
    return
  .timeout(3000)
  it 'Add visit in existing profile', (done)->
    addvisitUrl = data.url
    addvisitUrl = addvisitUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    addvisitUrl = addvisitUrl.replace("skey={|}","skey="+keysObject.skey)
    addvisitUrl = addvisitUrl.replace("mid={|}","mid="+keysObject.mid)
    addvisitUrl = addvisitUrl.replace("vid={|}","vid=")
    #console.log (addvisit)
    chai.request(server).get(addvisitUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(1)
          expect(profile).to.have.deep.property('profiles.0.device_info')
          expect(profile).to.have.deep.property('profiles.0.visit_server_date')
          expect(profile).to.have.deep.property('profiles.0.current')
          expect(profile).to.have.deep.property('profiles.0.current.views')
          expect(profile.profiles[0].visits).to.have.lengthOf(2)
          return done()
        return
      else
        return done()
    return

  it 'Add page view in existing profile', (done)->
    addPageViewURl = data.url
    addPageViewURl = addPageViewURl.replace("gmid={|}","gmid="+keysObject.gmid)
    addPageViewURl = addPageViewURl.replace("skey={|}","skey="+keysObject.skey)
    addPageViewURl = addPageViewURl.replace("mid={|}","mid="+keysObject.mid)
    addPageViewURl = addPageViewURl.replace("vid={|}","vid="+keysObject.vid)
    #console.log (addvisit)
    chai.request(server).get(addPageViewURl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(1)
          expect(profile).to.have.deep.property('profiles.0.device_info')
          expect(profile).to.have.deep.property('profiles.0.visit_server_date')
          expect(profile).to.have.deep.property('profiles.0.current')
          expect(profile).to.have.deep.property('profiles.0.current.views')
          expect(profile.profiles[0].visits).to.have.lengthOf(2)
          profile.profiles[0].visits[1].clicks.should.equal 2
          expect(profile.profiles[0].current.views).to.have.lengthOf 2
          return done()
        return
      else
        return done()
    return
  it 'Create Sub profile', (done)->

    createProfileUrl = data.url
    createProfileUrl = createProfileUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    createProfileUrl = createProfileUrl.replace("skey={|}","skey=")
    createProfileUrl = createProfileUrl.replace("mid={|}","mid=")
    createProfileUrl = createProfileUrl.replace("vid={|}","vid=")
    chai.request(server).get(createProfileUrl).end (err, res) ->

      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(2)
          expect(profile).to.have.deep.property('profiles.1.device_info')
          expect(profile).to.have.deep.property('profiles.1.visit_server_date')
          expect(profile).to.have.deep.property('profiles.1.current')
          expect(profile).to.have.deep.property('profiles.1.current.views')
          expect(profile).to.have.deep.property('profiles.1.visits.0.clicks')
          return done()
        return
      else
        return done()
    return

  it 'Add visit in existing profile (newly inserted)', (done)->
    addvisitUrl = data.url
    addvisitUrl = addvisitUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    addvisitUrl = addvisitUrl.replace("skey={|}","skey="+keysObject.skey)
    addvisitUrl = addvisitUrl.replace("mid={|}","mid="+keysObject.mid)
    addvisitUrl = addvisitUrl.replace("vid={|}","vid=")
    #console.log (addvisit)
    chai.request(server).get(addvisitUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(2)
          expect(profile).to.have.deep.property('profiles.1.device_info')
          expect(profile).to.have.deep.property('profiles.1.visit_server_date')
          expect(profile).to.have.deep.property('profiles.1.current')
          expect(profile).to.have.deep.property('profiles.1.current.views')
          expect(profile.profiles[1].visits).to.have.lengthOf(2)
          return done()
        return
      else
        return done()
    return

  it 'Add page view in existing profile (newly inserted)', (done)->
    addPageViewURl = data.url
    addPageViewURl = addPageViewURl.replace("gmid={|}","gmid="+keysObject.gmid)
    addPageViewURl = addPageViewURl.replace("skey={|}","skey="+keysObject.skey)
    addPageViewURl = addPageViewURl.replace("mid={|}","mid="+keysObject.mid)
    addPageViewURl = addPageViewURl.replace("vid={|}","vid="+keysObject.vid)
    #console.log (addvisit)
    chai.request(server).get(addPageViewURl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(2)
          expect(profile).to.have.deep.property('profiles.1.device_info')
          expect(profile).to.have.deep.property('profiles.1.visit_server_date')
          expect(profile).to.have.deep.property('profiles.1.current')
          expect(profile).to.have.deep.property('profiles.1.current.views')
          expect(profile.profiles[1].visits).to.have.lengthOf(2)
          profile.profiles[1].visits[1].clicks.should.equal 2
          expect(profile.profiles[1].current.views).to.have.lengthOf 2
          return done()
        return
      else
        return done()
    return

  it 'OCR changes in goals, expirement,segments w.r.t OCR setting', (done)->
    profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
      expect(profile).to.have.property('customerID')
      expect(profile).to.have.property('OCR')
      expect(profile).to.have.property('recently_used_device')
      expect(profile).to.have.property('cachedProperties')
      expect(profile).to.have.property('ngmid')
      expect(profile).to.have.property('pgmid')
      expect(profile).to.have.property('profiles')
      profile.profiles.should.be.a 'Array'
      profile.profiles.forEach (Profile,key)->
        for k of Profile.experiments
          if Profile.experiments[k].aggregated isnt undefined
            expect(Profile.experiments[k]).to.have.property("OCR")
        for k of Profile.controgroups
          if Profile.experiments[k].aggregated isnt undefined
            expect(Profile.experiments[k]).to.have.property("OCR")
        if Profile.goals isnt undefined and Profile.goals.length > 0
          Profile.goals.forEach (goal)->
            expect(goal).to.have.property("OCR")
            return
        if Profile.visits isnt undefined and Profile.visits.length > 0
          Profile.visits.forEach (visit)->
            if visit.experiments isnt undefined
              for k of visit.experiments
                expect(visit.experiments[k]).to.have.property("OCR")
                
            if visit.segments isnt undefined and visit.aggregatedSegments isnt undefined 
              for k of visit.aggregatedSegments
                expect(visit.segments).to.have.deep.property(visit.aggregatedSegments[k]+"_OCR")
                return
        if key is (profile.profiles.length-1)
          return done()
    return
  return

describe 'PostBack Profiles Apis', ->
  before (done) ->
    chai.request(server).get("/").end (err, res) ->
      if err.status isnt undefined  and err.status is 404
        return done()
      else if err
        return done( throw new Error('Server is not running'))
    return
  beforeEach (done)->
    setTimeout ()->
      return done()
    , waitTime
    return
  it 'post back categories ', (done)->
    postBackCatsUrl = data.postBackCatsUrl
    postBackCatsUrl = postBackCatsUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    postBackCatsUrl = postBackCatsUrl.replace("skey={|}","skey="+keysObject.skey)
    postBackCatsUrl = postBackCatsUrl.replace("mid={|}","mid="+keysObject.mid)
    postBackCatsUrl = postBackCatsUrl.replace("vid={|}","vid="+keysObject.vid)
    #console.log (addvisit)
    chai.request(server).get(postBackCatsUrl).set({}).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.have.deep.property('Category')
      expect(responce.Category).to.have.deep.property('status')
      expect(responce.Category.status).to.be.true

      profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
        expect(profile).to.have.property('customerID')
        expect(profile).to.have.property('OCR')
        expect(profile).to.have.property('recently_used_device')
        expect(profile).to.have.property('cachedProperties')
        expect(profile).to.have.property('ngmid')
        expect(profile).to.have.property('pgmid')
        expect(profile).to.have.property('profiles')
        profile.profiles.should.be.a 'Array'
        expect(profile.profiles).to.have.lengthOf(2)
        expect(profile).to.have.deep.property('profiles.1.device_info')
        expect(profile).to.have.deep.property('profiles.1.visit_server_date')
        expect(profile).to.have.deep.property('profiles.1.current')
        expect(profile).to.have.deep.property('profiles.1.current.views')
        expect(profile).to.have.deep.property('profiles.1.visits.1.categories')
        return done()
      return
    return
  it 'post back Basket ', (done)->
    postBackBasketUrl = data.postBackBasketUrl
    postBackBasketUrl = postBackBasketUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    postBackBasketUrl = postBackBasketUrl.replace("skey={|}","skey="+keysObject.skey)
    postBackBasketUrl = postBackBasketUrl.replace("mid={|}","mid="+keysObject.mid)
    postBackBasketUrl = postBackBasketUrl.replace("vid={|}","vid="+keysObject.vid)
    chai.request(server).get(postBackBasketUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.have.deep.property('Category')
      expect(responce.purchaseCart).to.have.deep.property('status')
      expect(responce.purchaseCart.status).to.be.true

      profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
        expect(profile).to.have.property('customerID')
        expect(profile).to.have.property('OCR')
        expect(profile).to.have.property('recently_used_device')
        expect(profile).to.have.property('cachedProperties')
        expect(profile).to.have.property('ngmid')
        expect(profile).to.have.property('pgmid')
        expect(profile).to.have.property('profiles')
        profile.profiles.should.be.a 'Array'
        expect(profile.profiles).to.have.lengthOf(2)
        expect(profile).to.have.deep.property('profiles.1.device_info')
        expect(profile).to.have.deep.property('profiles.1.visit_server_date')
        expect(profile).to.have.deep.property('profiles.1.current')
        expect(profile).to.have.deep.property('profiles.1.current.views')
        expect(profile).to.have.deep.property('profiles.1.visits.1.categories')
        expect(profile).to.have.deep.property("profiles.1.visits.1.carts")
        expect()
        return done()
      return
    return
  it 'post back Purchases ', (done)->
    postBackPurchaseUrl = data.postBackPurchaseUrl
    postBackPurchaseUrl = postBackPurchaseUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    postBackPurchaseUrl = postBackPurchaseUrl.replace("skey={|}","skey="+keysObject.skey)
    postBackPurchaseUrl = postBackPurchaseUrl.replace("mid={|}","mid="+keysObject.mid)
    postBackPurchaseUrl = postBackPurchaseUrl.replace("vid={|}","vid="+keysObject.vid)
    chai.request(server).get(postBackPurchaseUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.have.deep.property('Category')
      expect(responce.Category).to.have.deep.property('status')
      expect(responce.Category.status).to.be.true

      profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
        expect(profile).to.have.property('customerID')
        expect(profile).to.have.property('OCR')
        expect(profile).to.have.property('recently_used_device')
        expect(profile).to.have.property('cachedProperties')
        expect(profile).to.have.property('ngmid')
        expect(profile).to.have.property('pgmid')
        expect(profile).to.have.property('profiles')
        profile.profiles.should.be.a 'Array'
        expect(profile.profiles).to.have.lengthOf(2)
        expect(profile).to.have.deep.property('profiles.1.device_info')
        expect(profile).to.have.deep.property('profiles.1.visit_server_date')
        expect(profile).to.have.deep.property('profiles.1.current')
        expect(profile).to.have.deep.property('profiles.1.current.views')
        expect(profile).to.have.deep.property('profiles.1.visits.1.categories')
        expect(profile).to.have.deep.property('profiles.1.visits.1.pv')
        return done()
      return
    return
  it 'post back Search ', (done)->
    postBackSearchUrl = data.postBackSearchUrl
    postBackSearchUrl = postBackSearchUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    postBackSearchUrl = postBackSearchUrl.replace("skey={|}","skey="+keysObject.skey)
    postBackSearchUrl = postBackSearchUrl.replace("mid={|}","mid="+keysObject.mid)
    postBackSearchUrl = postBackSearchUrl.replace("vid={|}","vid="+keysObject.vid)
    chai.request(server).get(postBackSearchUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.have.deep.property('Searches')
      expect(responce.Searches).to.have.deep.property('status')
      expect(responce.Searches.status).to.be.true

      profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
        expect(profile).to.have.property('customerID')
        expect(profile).to.have.property('OCR')
        expect(profile).to.have.property('recently_used_device')
        expect(profile).to.have.property('cachedProperties')
        expect(profile).to.have.property('ngmid')
        expect(profile).to.have.property('pgmid')
        expect(profile).to.have.property('profiles')
        profile.profiles.should.be.a 'Array'
        expect(profile.profiles).to.have.lengthOf(2)
        expect(profile).to.have.deep.property('profiles.1.device_info')
        expect(profile).to.have.deep.property('profiles.1.visit_server_date')
        expect(profile).to.have.deep.property('profiles.1.current')
        expect(profile).to.have.deep.property('profiles.1.current.views')
        expect(profile).to.have.deep.property('profiles.1.visits.1.categories')
        expect(profile).to.have.deep.property('profiles.1.visits.1.searches')
        expect(profile.profiles[1].visits[1].searches).to.have.length.above(0)
        return done()
      return
    return
  return

###describe 'Profiles Privacy Apis', ->
  before (done) ->
    chai.request(server).get("/").end (err, res) ->
      if err.status isnt undefined  and err.status is 404
        return done()
      else if err
        return done( throw new Error('Server is not running'))
    return
  beforeEach (done)->
    setTimeout ()->
      return done()
    , waitTime
    return
  it.skip 'Update privacy', (done)->
    updatePrivacyUrl = data.updatePrivacyUrl
    updatePrivacyUrl = updatePrivacyUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    updatePrivacyUrl = updatePrivacyUrl.replace("skey={|}","skey="+keysObject.skey)
    updatePrivacyUrl = updatePrivacyUrl.replace("mid={|}","mid="+keysObject.mid)
    updatePrivacyUrl = updatePrivacyUrl.replace("vid={|}","vid="+keysObject.vid)
    #console.log (addvisit)
    chai.request(server).get(updatePrivacyUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce)
      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce.success).to.deep.equal true

      profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
        expect(profile.customerID).to.not.be.undefined
        expect(profile.OCR).to.not.be.undefined
        expect(profile.recently_used_device).to.not.be.undefined
        expect(profile.cachedProperties).to.not.be.undefined
        expect(profile.ngmid).to.not.be.undefined
        expect(profile.pgmid).to.not.be.undefined
        expect(profile).to.have.property('profiles')
        profile.profiles.should.be.a 'Array'
        expect(profile.profiles).to.have.lengthOf(2)
        expect(profile).to.have.deep.property('profiles.1.device_info')
        expect(profile).to.have.deep.property('profiles.1.visit_server_date')
        expect(profile).to.have.deep.property('profiles.1.current')
        expect(profile).to.have.deep.property('profiles.1.current.views')
        expect(profile.profiles[1].visits).to.have.lengthOf(2)
        profile.profiles[1].visits[1].clicks.should.equal 2
        #expect(profile.profiles[1].current.views).to.have.lengthOf 2
        expect(profile).to.have.property('privacy')
        expect(profile).to.have.deep.property('privacy.personalization')
        expect(profile.privacy.personalization).to.deep.equal '0'
        return done()
      return
    return
  it.skip 'Empty Profile', (done)->
    emptyProfileUrl = data.emptyProfileUrl
    emptyProfileUrl = emptyProfileUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    emptyProfileUrl = emptyProfileUrl.replace("skey={|}","skey="+keysObject.skey)
    emptyProfileUrl = emptyProfileUrl.replace("mid={|}","mid="+keysObject.mid)
    emptyProfileUrl = emptyProfileUrl.replace("vid={|}","vid="+keysObject.vid)
    #console.log (addvisit)
    chai.request(server).get(emptyProfileUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce)
      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce.success).to.deep.equal true

      profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
        expect(profile.customerID).to.not.be.undefined
        expect(profile.OCR).to.not.be.undefined
        expect(profile.recently_used_device).to.be.undefined
        expect(profile.cachedProperties).to.be.undefined
        expect(profile.ngmid).to.be.undefined
        expect(profile.pgmid).to.be.undefined
        expect(profile).to.have.property('profiles')
        expect(profile).to.have.property('privacy')
        return done()
      return
    return
  it.skip 'Re insert profile', (done)->
    reInsertProfileUrl = data.url
    reInsertProfileUrl = reInsertProfileUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    reInsertProfileUrl = reInsertProfileUrl.replace("skey={|}","skey="+keysObject.skey)
    reInsertProfileUrl = reInsertProfileUrl.replace("mid={|}","mid="+keysObject.mid)
    reInsertProfileUrl = reInsertProfileUrl.replace("vid={|}","vid=")
    #console.log (addvisit)
    chai.request(server).get(reInsertProfileUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(2)
          expect(profile).to.have.deep.property('profiles.1.device_info')
          expect(profile).to.have.deep.property('profiles.1.visit_server_date')
          expect(profile).to.have.deep.property('profiles.1.current')
          expect(profile).to.have.deep.property('profiles.1.current.views')
          expect(profile.profiles[1].visits).to.have.lengthOf(1)
          return done()
        return
      else
        return done()
    return
  it.skip 'Empty Profile to test new profile insertion After empty-Profile', (done)->
    emptyProfileUrl = data.emptyProfileUrl
    emptyProfileUrl = emptyProfileUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    emptyProfileUrl = emptyProfileUrl.replace("skey={|}","skey="+keysObject.skey)
    emptyProfileUrl = emptyProfileUrl.replace("mid={|}","mid="+keysObject.mid)
    emptyProfileUrl = emptyProfileUrl.replace("vid={|}","vid="+keysObject.vid)
    #console.log (addvisit)
    chai.request(server).get(emptyProfileUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce)
      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce.success).to.deep.equal true
      return done()
    return
  it.skip 'Scenario: After Empty profile Create new Sub profile', (done)->

    createProfileUrl = data.url
    createProfileUrl = createProfileUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    createProfileUrl = createProfileUrl.replace("skey={|}","skey=")
    createProfileUrl = createProfileUrl.replace("mid={|}","mid=")
    createProfileUrl = createProfileUrl.replace("vid={|}","vid=")
    chai.request(server).get(createProfileUrl).end (err, res) ->

      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(3)
          expect(profile).to.have.deep.property('profiles.2.device_info')
          expect(profile).to.have.deep.property('profiles.2.visit_server_date')
          expect(profile).to.have.deep.property('profiles.2.current')
          expect(profile).to.have.deep.property('profiles.2.current.views')
          expect(profile).to.have.deep.property('profiles.2.visits.0.clicks')
          return done()
        return
      else
        return done()
    return
  it.skip 'Empty Profile to test next Scenario', (done)->
    emptyProfileUrl = data.emptyProfileUrl
    emptyProfileUrl = emptyProfileUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    emptyProfileUrl = emptyProfileUrl.replace("skey={|}","skey="+keysObject.skey)
    emptyProfileUrl = emptyProfileUrl.replace("mid={|}","mid="+keysObject.mid)
    emptyProfileUrl = emptyProfileUrl.replace("vid={|}","vid="+keysObject.vid)
    #console.log (addvisit)
    chai.request(server).get(emptyProfileUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce)
      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce.success).to.deep.equal true
      return done()
    return
  it.skip 'Scenario: After Empty profile using MID3 Reinsert profile using MID1', (done)->
    reInsertProfileUrl = data.url
    reInsertProfileUrl = reInsertProfileUrl.replace("gmid={|}","gmid="+keysObjectOne.gmid)
    reInsertProfileUrl = reInsertProfileUrl.replace("skey={|}","skey="+keysObjectOne.skey)
    reInsertProfileUrl = reInsertProfileUrl.replace("mid={|}","mid="+keysObjectOne.mid)
    reInsertProfileUrl = reInsertProfileUrl.replace("vid={|}","vid=")
    #console.log (addvisit)
    chai.request(server).get(reInsertProfileUrl).end (err, res) ->
      res.status.should.equal 200
      expect(err).to.be.null
      res.text.should.be.a 'String'
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce);

      expect(responce).to.not.have.property('error')
      expect(responce).to.have.property('success')
      expect(responce).to.have.property('eval')
      if responce and responce.eval isnt undefined
        responce= responce.eval.split(';')
        responce.forEach (obj)->
          obj = obj.substring(obj.lastIndexOf("(")+1,obj.lastIndexOf(")"))
          obj = obj.replace(/\"/g,'')
          obj = obj.split(",")
          keysObject[obj[0]] = obj[1]
          return
        profileModel.findOne {_id:keysObject.gmid},(error, profile) ->
          expect(profile).to.have.property('customerID')
          expect(profile).to.have.property('OCR')
          expect(profile).to.have.property('recently_used_device')
          expect(profile).to.have.property('cachedProperties')
          expect(profile).to.have.property('ngmid')
          expect(profile).to.have.property('pgmid')
          expect(profile).to.have.property('profiles')
          profile.profiles.should.be.a 'Array'
          expect(profile.profiles).to.have.lengthOf(3)
          expect(profile).to.have.deep.property('profiles.1.device_info')
          expect(profile).to.have.deep.property('profiles.1.visit_server_date')
          expect(profile).to.have.deep.property('profiles.1.current')
          expect(profile).to.have.deep.property('profiles.1.current.views')
          return done()
        return
      else
        return done()
    return###
describe 'Api Testing',->
  before (done) ->
    chai.request(server).get("/").end (err, res) ->
      if err.status isnt undefined  and err.status is 404
        return done()
      else if err
        return done( throw new Error('Server is not running'))
    return
  beforeEach (done)->
    setTimeout ()->
      return done()
    , waitTime
    return
  it 'invalidate key', (done)->
    invalidateKeyUrl = data.invalidateKeyUrl
    chai.request(server).get(invalidateKeyUrl).end (err, res) ->
      expect(err).to.be.null
      if res.text isnt undefined and res.text is ""
        res.status.should.be.equal 204
        return done()
      else
        responce = helper.getResponceJson(res.text)
        responce = JSON.parse(responce)
        expect(responce).to.have.property('info')
        return done()
    return
  it 'Set Information', (done)->
    header = helper.generateHeader(data.customerID)
    setInformationUrl = data.setInformationUrl
    setInformationUrl = setInformationUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    setInformationUrl = setInformationUrl.replace("mid={|}","mid="+keysObject.mid)
    chai.request(server).get(setInformationUrl).set({authorization:header}).end (err, res) ->
      expect(err).to.be.null
      res.status.should.be.equal 200
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce)
      expect(responce.Status).to.be.deep.equal(data.infoID+" successfully updated")
      return done()
    return 
  it 'Get Information', (done)->
    getInformationUrl = data.getInformationUrl
    header = helper.generateHeader(data.customerID)
    getInformationUrl = getInformationUrl.replace("gmid={|}","gmid="+keysObject.gmid)
    getInformationUrl = getInformationUrl.replace("mid={|}","mid="+keysObject.mid)
    chai.request(server).get(getInformationUrl).set({authorization:header}).end (err, res) ->
      expect(err).to.be.null
      res.status.should.be.equal 200
      responce = helper.getResponceJson(res.text)
      responce = JSON.parse(responce)
      expect(responce).to.have.property('Status')
      if responce.Status isnt undefined 
        if responce.Status is 'True'
          expect(responce.Status).to.be.equal('True')
        else
          expect(responce.Status).to.be.equal 'False'
      else 
        return done(new Error('Not true'))
      return done()
    return 
  return
