var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');
var reportModel = require('../app/model/ReportingArchiveAggregated');
var accountModel = require('../app/model/account');

var mock_profile = require('../mock-data/profile1');
var mock_page_element = require('../mock-data/page-element-default-experience');
var mock_account = require('../mock-data/account');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("Experience in test", function(){
	beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_account');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await accountModel.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleExperience();
    await mock_account.seedWithAllPlugins(1001);
  });

  it("should experience event invoke",async function(){
  	const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);

  	response.events.length.should.equal(1);
    response.events[0].type.should.equal('experience-in');
  });
});

describe("Experience in test with control group", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_account');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await accountModel.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleControlgroup();
    await mock_account.seedWithAllPlugins(1001);
  });

  it("should pass",async function(){
    const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);

  	response.events.length.should.equal(1);
    response.events[0].type.should.equal('experience-in');
  });
});

describe("Experience event with disable plugins", function(){
	beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_account');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await accountModel.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleExperience();
    await mock_account.seedWithDisabledPlugins(1001);
  });

  it("should experience event invoke",async function(){
  	const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);
  	response.events.length.should.equal(0);
  });
});

describe("Experience event with disable plugins", function(){
	beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_account');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();
    await accountModel.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seedSingleExperience();
    await mock_account.seedWithDisabledExperienceOptions(1001);
  });

  it("should experience event invoke",async function(){
  	const res = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://mon-experiences-1001/');
    res.status.should.equal(200);
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);
  	response.events.length.should.equal(0);
  });
});
