var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');

var mock_profile = require('../mock-data/profile1');
var mock_page_element = require('../mock-data/page-element-default-experience');
var mock_page_element2 = require('../mock-data/page-element-with-false-audience');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("External basic unit test", function() {
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element.seed();
  });

  it("should not pass with no cid ", function(done){
    chai.request(app)
    .get('/external?url=nutjaa.win')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      res.text.should.equal('MONOloop.MonoloopData = {"error":"No Auth"};');
      done();
    });
  });

  it("should not pass if wrong skey",function(done){
    server(app)
    .get('/external?url=nutjaa.win&cid=1001&gmid=5bx48432c4234a6269fe4a54f&mid=5b48432c4234a6269fe4a550&skey=48310fc165d2776acd56de9a5ffdde95373ef0e10ef45a1508f3673e7083e7a85a5f419cd421cece70b1df16b26352a3e67f2015d2820cd9a410ac8a79585d67&debug=1')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      res.text.should.equal("MONOloop.MonoloopData = {\"error\":\"No Auth\"};");
      done();
    });
  });
  /*
  it('external with empty payload', function(done) {
    chai.request(app)
    .post('/external?cid=1001')
    .end(function(err, res){
      res.status.should.equal(200);
      done();
    });
  });
  */
  it('external with empty experience', function(done){
    chai.request(app)
    .post('/external?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .send({
      content: [{
        experiment_ids: null,
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}"
      }]
    })
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      res.body.data[0].condition.should.equal(true);
      done();
    });
  });

  it('external with empty experience with false', function(done){
    chai.request(app)
    .post('/external?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .send({
      content: [{
        experiment_ids: null,
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount == '10' ) ){|}"
      }]
    })
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      res.body.data[0].condition.should.equal(false);
      done();
    });
  });

  it('external with empty experience 2', function(done){
    chai.request(app)
    .post('/external?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .send({
      content: [{
        experiment_ids: null,
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}"
      },{
        experiment_ids: null,
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount > '100' ) ){|}"
      }]
    })
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      res.body.data[0].condition.should.equal(true);
      res.body.data[1].condition.should.equal(false);
      done();
    });
  });

  it('external with experience with 100% experiment group', function(done){
    chai.request(app)
    .post('/external?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .send({
      content: [{
        experiment_ids: [65],
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      },{
        experiment_ids: [65],
        uid: 2,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      }]
    })
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      res.body.data[0].condition.should.equal(true);
      done();
    });
  });

  it('external with experience with 0% experiment group', function(done){
    chai.request(app)
    .post('/external?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .send({
      content: [{
        experiment_ids: [67],
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      },{
        experiment_ids: [67],
        uid: 2,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      }]
    })
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      res.body.data[0].condition.should.equal(false);
      done();
    });
  });

  it('external with experience with both experiment group', function(done){
    chai.request(app)
    .post('/external?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .send({
      content: [{
        experiment_ids: [65],
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      },{
        experiment_ids: [67],
        uid: 2,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      }]
    })
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      res.body.data[0].condition.should.equal(true);
      res.body.data[1].condition.should.equal(false);
      done();
    });
  });
});

describe("External in audience unit test", function() {
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_page_element2.seed();
  });

  it("Should no process via not pass audience", function(done){
    chai.request(app)
    .post('/external?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .send({
      content: [{
        experiment_ids: [65],
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      },{
        experiment_ids: [65],
        uid: 2,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      }]
    })
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      res.body.data[0].condition.should.equal(false);
      res.body.data[1].condition.should.equal(false);
      done();
    });
  });

  it("1 pass audience / 1 fail audience", function(done){
    chai.request(app)
    .post('/external?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
    .send({
      content: [{
        experiment_ids: [65],
        uid: 1,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      },{
        experiment_ids: [67],
        uid: 2,
        condition: "if ( ( MonoloopProfile.VisitCount < '5' ) ){|}"
      }]
    })
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      res.body.data[0].condition.should.equal(false);
      res.body.data[1].condition.should.equal(true);
      done();
    });
  });
});