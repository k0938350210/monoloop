var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var accountModel = require('../app/model/account');
var profileModel = require('../app/model/profile');
var element_model = require('../app/model/element');
var reportModel = require('../app/model/ReportingArchiveAggregated');

var mock_goal = require('../mock-data/goal');
var mock_profile = require('../mock-data/profile1');
var mock_account = require('../mock-data/account');

chai.use(chaiHttp);

describe("track goal test", function(){

  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await accountModel.collection.deleteMany();
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_account.seedWithAllPlugins(1001);
    await mock_profile.seed();
    await mock_goal.seed();
  });


  it("should return access goal #001", function(done){
    chai.request(app)
    .get('/tracks?url=http://local-demo1wp.com/&cid=1001')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      let responce = helper.getResponceJson(res.text);
      let keysObject = {};
      let keysObjectOne = {};
      responce = JSON.parse(responce);
      expect(responce).to.not.have.property('error');
      expect(responce).to.have.property('success');
      expect(responce).to.have.property('eval');
      if (responce && responce.eval !== void 0) {
        responce = responce.eval.split(';');
        responce.forEach(function(obj) {
          obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
          obj = obj.replace(/\"/g, '');
          obj = obj.split(",");
          keysObject[obj[0]] = obj[1];
          keysObjectOne[obj[0]] = obj[1];
        });
        profileModel.findOne({
          _id: keysObject.gmid
        }, function(error, profile) {
          profile.profiles[0].goals.length.should.equal(2);
          let goal1 = profile.profiles[0].goals[0];
          goal1.uid.should.equal(1);
          goal1.p.should.equal(10);
          goal1.OCR.should.equal(false);

          let goal2 = profile.profiles[0].goals[1];
          goal2.uid.should.equal(2);
          goal2.p.should.equal(20);
          goal2.OCR.should.equal(false);

          done();
        });

      }
    });
  });

  it("should return access goal with archive #002", async function(){
    const res = await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001');
    let keysObject = helper.getResponseKeyObject(res.text);

    const res2 = await chai.request(app).get('/test-archive?gmid='+keysObject.gmid);
    let report = await reportModel.findOne({cid: 1001});
    report.goals['1'].VisitsBeforeGoal.should.equal(1);
    report.goals['1'].amount.should.equal(1);
    report.goals['1'].VisitsBeforeGoal.should.equal(1);
    report.goals['2'].pagesBeforeGoal.should.equal(1);
    report.goals['2'].amount.should.equal(1);
    report.goals['2'].pagesBeforeGoal.should.equal(1);
  });

  it("should return access goal with archive #003", async function(){
    let res = await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001');
    let key = helper.getResponseKeyObject(res.text);

    await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001&gmid='+key.gmid+'&mid='+key.mid+'&skey='+key.skey+'&vid='+key.vid);
    await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001&gmid='+key.gmid+'&mid='+key.mid+'&skey='+key.skey+'&vid='+key.vid);
    await chai.request(app).get('/test-archive?gmid='+key.gmid);

    await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001&gmid='+key.gmid+'&mid='+key.mid+'&skey='+key.skey+'&vid=');
    await chai.request(app).get('/test-archive?gmid='+key.gmid);

    await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001&gmid='+key.gmid+'&mid='+key.mid+'&skey='+key.skey+'&vid=');
    await chai.request(app).get('/test-archive?gmid='+key.gmid);

    await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001&gmid='+key.gmid+'&mid='+key.mid+'&skey='+key.skey+'&vid=');
    await chai.request(app).get('/test-archive?gmid='+key.gmid);

    res = await chai.request(app).get('/profiles/'+key.gmid).set('Authorization',helper.generateHeader(1001));
    ret_obj = JSON.parse(res.text);

    let profile = ret_obj.profiles[0];
    let goals = profile.goals;
    goals.length.should.equal(2);

    expect(goals[0]).to.have.key('uid','tstamp');

    let profile1 = profile.profiles[0];
    goals = profile1.goals;
    goals.length.should.equal(2);
    expect(goals[0]).to.have.key('uid','tstamp');

  });
});



describe("track goal with fail condition test", function(){

  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await mock_profile.seed();
    await mock_goal.seed2();
  });


  it("should return access goal ", function(done){
    chai.request(app)
    .get('/tracks?url=http://local-demo1wp.com/&cid=1001')
    .end(function(err, res){
      if(err) done(err);
      res.status.should.equal(200);
      let responce = helper.getResponceJson(res.text);
      let keysObject = {};
      let keysObjectOne = {};
      responce = JSON.parse(responce);
      expect(responce).to.not.have.property('error');
      expect(responce).to.have.property('success');
      expect(responce).to.have.property('eval');
      if (responce && responce.eval !== void 0) {
        responce = responce.eval.split(';');
        responce.forEach(function(obj) {
          obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
          obj = obj.replace(/\"/g, '');
          obj = obj.split(",");
          keysObject[obj[0]] = obj[1];
          keysObjectOne[obj[0]] = obj[1];
        });
        profileModel.findOne({
          _id: keysObject.gmid
        }, function(error, profile) {
          profile.profiles[0].goals.length.should.equal(1);
          let goal1 = profile.profiles[0].goals[0];
          goal1.uid.should.equal(1);
          goal1.p.should.equal(10);
          goal1.OCR.should.equal(false);

          done();
        });

      }
    });
  });
});

describe("Goal with invoke event", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_account');
    await accountModel.collection.deleteMany();
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_account.seedWithAllPlugins(1001);
    await mock_profile.seed();
    await mock_goal.seed();
  });

  it('should return as invoke event #e001',async function(){
    let res = await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001');
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);

    response.events.length.should.equal(2);
    response.events[0].type.should.equal('goal');
  })
});

describe("Goal with disable plugins", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_account');
    await accountModel.collection.deleteMany();
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_account.seedWithDisabledPlugins(1001);
    await mock_profile.seed();
    await mock_goal.seed();
  });

  it('should not return as invoke event #e011',async function(){
    let res = await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001');
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);

    response.events.length.should.equal(0);
  });
});

describe("Goal with disable goal plugins", function(){
  beforeEach('Setup mock data',async function() {
    await redis.del('1001_pageelements');
    await redis.del('1001_account');
    await accountModel.collection.deleteMany();
    await profileModel.collection.deleteMany();
    await element_model.collection.deleteMany();
    await reportModel.collection.deleteMany();

    await mock_account.seedWithDisabledGoalOptions(1001);
    await mock_profile.seed();
    await mock_goal.seed();
  });

  it('should not return as invoke event #e021',async function(){
    let res = await chai.request(app).get('/tracks?url=http://local-demo1wp.com/&cid=1001');
    let response = helper.getResponceJson(res.text);
    response = JSON.parse(response);

    response.events.length.should.equal(0);
  });
});