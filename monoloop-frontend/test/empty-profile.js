var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var mock_profile = require('../mock-data/profile1');
var profileModel = require('../app/model/profile');

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

chai.use(chaiHttp);
//emptyProfileUrl = "/emptyProfile/?cid=" + customerID + "&gmid={|} &mid={|} &skey={|} &personalization=1 &vid={|}";

describe('Empty profile Api', function() {
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it('Empty Profile flow', function(done) {
    chai.request(app)
    .get('/emptyProfile?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&personalization=1')
    .end(function(err, res){
      res.status.should.equal(200);
      res.body.success.should.equal(true);
      profileModel.findOne({
        _id: gmid
      }, function(error, profile) {
        expect(profile.customerID).to.not.be.undefined;
        expect(profile.OCR).to.not.be.undefined;
        expect(profile.recently_used_device).to.be.undefined;
        expect(profile.cachedProperties).to.be.undefined;
        expect(profile.ngmid).to.be.undefined;
        expect(profile.pgmid).to.be.undefined;
        expect(profile).to.have.property('profiles');
        expect(profile).to.have.property('privacy');
        profile.profiles[0].visits.length.should.equal(0);
        //Call track

        chai.request(app)
        .get('/tracks/?url=nutjaa.win&cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid)
        .end(function(err, res){
          if(err) done(err);
          res.status.should.equal(200);
          profileModel.findOne({
            _id: gmid
          }, function(error, profile) {
            profile.profiles[0].visits.length.should.equal(1);
            done();
          });
        });
      });
    });
  });
});