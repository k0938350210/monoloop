var server = require('supertest');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = require('chai').should();
var app = require('../app');
var helper = require('./helper');
var redis = require('./../redis-conn');
var expect = chai.expect;

var profileModel = require('../app/model/profile');
var mock_profile = require('../mock-data/profile1');

chai.use(chaiHttp);

let gmid = '5b48b6f89adb4f0cb2e2ed46';
let mid = '5b48b6f89adb4f0cb2e2ed47';
let skey = 'c5ffbe812a76dd553381e0f16522638d39a5e9a54288f38656a5e0da96c449c771c00b89996556426125fb71e17524766dc8480860d40ab88aba2aec6d22ee56';
let vid = '5b503315f40a1b4f8562cdd0';

describe("Unique-profile on first user", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("Should add uid", function(done){
    chai.request(app)
    .get('/tracks/?url=nutjaa.win&cid=1001&uniqueID=nut')
    .end(async function(err, res){
      res.status.should.equal(200);
      let ret_obj = JSON.parse(helper.getResponceJson(res.text));
      let responce = ret_obj.eval.split(';');
      let keysObject = {};
      let keysObjectOne = {};

      responce.forEach(function(obj) {
        obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
        obj = obj.replace(/\"/g, '');
        obj = obj.split(",");
        keysObject[obj[0]] = obj[1];
      });

      let profile = await profileModel.findOne({ _id: keysObject.gmid });
      profile.uniqueID.should.equal('nut');
      done();
    });
  });
});

describe("Unique-profile on first user and sign out", function(){
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("Test user signout", async function(){
    const res = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001&uniqueID=nut');
    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    let responce = ret_obj.eval.split(';');
    let keysObject = {};
    let keysObjectOne = {};

    responce.forEach(function(obj) {
      obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
      obj = obj.replace(/\"/g, '');
      obj = obj.split(",");
      keysObject[obj[0]] = obj[1];
    });

    let profile_count = await profileModel.find({ uniqueID: 'nut' }).countDocuments();
    profile_count.should.equal(1);

    const res2 = await chai.request(app).get('/tracks?cid=1001&gmid='+keysObject.gmid+'&mid='+keysObject.mid+'&skey='+keysObject.skey+'&uniqueID=nut&url=http://nutjaa.win/');
    let profile = await profileModel.findOne({_id: keysObject.gmid});
    profile.profiles[0].visits[0].clicks.should.equal(2);

    const res3 = await chai.request(app).get('/tracks?cid=1001&gmid='+keysObject.gmid+'&mid='+keysObject.mid+'&skey='+keysObject.skey+'&uniqueID=&url=http://nutjaa.win/');
    profile_count = await profileModel.findOne({uniqueID: 'nut'}).countDocuments();
    profile_count.should.equal(1);
  });
});

describe("Unique-profile double first time", function() {
  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("Test first time profile", async function(){
    const res1 = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001&uniqueID=nut');
    const res2 = await chai.request(app).get('/tracks/?url=nutjaa.win&cid=1001&uniqueID=nut');
    //console.log(res1.text);
    //console.log(res2.text);
    profile_count = await profileModel.findOne({uniqueID: 'nut'}).countDocuments();
    profile_count.should.equal(1);
  });
});

describe("Unique-profile unit test", function() {

  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("Test should not create double profile with same uniqueID", async function(){
    const res = await chai.request(app).get('/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&uniqueID=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=&fla=1&java=1&pdf=1');

    let ret_obj = JSON.parse(helper.getResponceJson(res.text));
    let responce = ret_obj.eval.split(';');
    let keysObject = {};
    let keysObjectOne = {};

    responce.forEach(function(obj) {
      obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
      obj = obj.replace(/\"/g, '');
      obj = obj.split(",");
      keysObject[obj[0]] = obj[1];
    });

    const res2 = await chai.request(app).get('/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&uniqueID=1001&rand=0.7312085209992335&abg=false&uid=&gmid='+keysObject.gmid+'&mid='+keysObject.mid+'&omid=&skey='+keysObject.skey+'&vid='+keysObject.vid+'&fla=1&java=1&pdf=1');
    //console.log(res2.text);

    const res3 = await chai.request(app).get('/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&uniqueID=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=&fla=1&java=1&pdf=1');
    //console.log(res3.text);
    ret_obj = JSON.parse(helper.getResponceJson(res2.text));
    responce = ret_obj.eval.split(';');
    keysObject = {};
    keysObjectOne = {};

    responce.forEach(function(obj) {
      obj = obj.substring(obj.lastIndexOf("(") + 1, obj.lastIndexOf(")"));
      obj = obj.replace(/\"/g, '');
      obj = obj.split(",");
      keysObject[obj[0]] = obj[1];
    });

    const res4 = await chai.request(app).get('/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&uniqueID=1001&rand=0.7312085209992335&abg=false&uid=&gmid='+keysObject.gmid+'&mid='+keysObject.mid+'&omid=&skey='+keysObject.skey+'&vid='+keysObject.vid+'&fla=1&java=1&pdf=1');

    let profile_count = await profileModel.findOne({uniqueID: '1001'}).countDocuments();
    profile_count.should.equal(1);
  });

});


describe("Unique-profile on exists profile", function() {

  beforeEach('Setup mock data',async function() {
    redis.del('1001_pageelements');
    await profileModel.collection.deleteMany();
    await mock_profile.seed();
  });

  it("Test assign uniqueID to exists profile", async function(){
    const res1 = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&url=http://local-demo1wp.com/');
    const res2 = await chai.request(app).get('/tracks?cid=1001&gmid='+gmid+'&mid='+mid+'&skey='+skey+'&vid='+vid+'&uniqueID=nut&url=http://local-demo1wp.com/');
    const profile_count = await profileModel.findOne({ uniqueID: 'nut' }).countDocuments();
    profile_count.should.equal(1);
  });

});