(function() {
  /*
    *@author: an.pham
   */
  var ZPush, zmq;

  zmq = require("zmq");

  ZPush = class ZPush {
    init(properties) {
      var addr, port, providerURL;
      if (!this.sock) {
        this.sock = zmq.socket("push");
        addr = properties.getProperty("configurations.default.push.addr");
        port = properties.getProperty("configurations.default.push.port");
        providerURL = addr + ":" + port;
        this.sock.connect("tcp://" + providerURL);
        return console.log("Socket connected to " + providerURL);
      }
    }

    send(message) {
      if (this.sock) {
        return this.sock.send(message);
      } else {
        throw "Push socket is not ready to use !!!";
      }
    }

  };

  module.exports = new ZPush();

}).call(this);
