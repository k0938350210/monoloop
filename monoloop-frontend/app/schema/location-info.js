let mongoose = require(__dirname + '/../../mongoose');
let Schema = mongoose.Schema;

let loc_info = new Schema({
  loc_timezone: {
    type: String, // !!! Timezone is used in some functions .Had been decided to add this field in scheme
    default: ''
  },
  loc_browser_lang: {
    type: String,
    default: ''
  },
  loc_city: {
    type: String,
    default: ''
  },
  loc_country: {
    type: String,
    default: ''
  },
  loc_ip: {
    type: String,
    default: ''
  },
  loc_lat: {
    type: String,
    default: ''
  },
  loc_long: {
    type: String,
    default: ''
  },
  loc_continent: {
    type: String,
    default: ''
  },
  loc_region: {
    type: String,
    default: ''
  }
},{ _id : false });

module.exports = loc_info;