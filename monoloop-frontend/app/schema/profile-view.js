let mongoose = require(__dirname + '/../../mongoose');
let Schema = mongoose.Schema;

let view_schema = new Schema({
  url: String,
  extRef: String,
  localTime: Number,
  timeStamp: Number,
  //meta:[Meta]
  aggregated: Boolean
});

module.exports = view_schema;