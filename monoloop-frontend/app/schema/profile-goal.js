let mongoose = require(__dirname + '/../../mongoose');
let Schema = mongoose.Schema;

let goal_schema = new Schema({
  tstamp: Number,
  uid: Number,
  p: Number,
  OCR: Boolean
},{
	_id : false,
  toJSON: {
    transform: function (doc, ret) {
      delete ret.OCR;
      delete ret.p;
    }
  }
});

module.exports = goal_schema;