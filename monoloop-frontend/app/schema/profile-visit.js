let mongoose = require(__dirname + '/../../mongoose');
let Schema = mongoose.Schema;

let loc_info_schema = require('./location-info');

let visit_schema = new Schema({
  visitID: Schema.ObjectId,
  aggregated: Boolean,
  loc_info: {
    type: mongoose.Schema.Types.Object,
    ref: 'loc_info_schema'
  },
  achivedGoal: Number,
  clicks: Number,
  pv: Number,
  entrypage: String,
  exitpage: String,
  feedback: Number,
  firstclick: Number,
  lastclick: Number,
  outlink: String,
  points: Number,
  referer: String,
  referers: [String],
  downloads: [String],
  searches: [String],
  carts: Schema.Types.Mixed,
  meta: Schema.Types.Mixed,
  categories: Schema.Types.Mixed,
  // this field will remove
  //content:[Number],
  experiments: Schema.Types.Mixed,
  content: Schema.Types.Mixed,
  segments: Schema.Types.Mixed,
  segments_InOut: Schema.Types.Mixed,
  aggregatedSegments: Schema.Types.Mixed
},{
  toJSON: {
    transform: function (doc, ret) {
      delete ret.segments_InOut;
      delete ret.aggregatedSegments;
      delete ret.categories;
      delete ret.segments;
      delete ret.aggregated;
    }
  }
});

module.exports = visit_schema;