let mongoose = require(__dirname + '/../../mongoose');
let Schema = mongoose.Schema;
let _ = require('underscore');
//Local
let goal_schema = require('./profile-goal');
let view_schema = require('../schema/profile-view');
let visit_schema = require('../schema/profile-visit');
let device_info = require('../schema/device-info');

sub_profile = new Schema({
  profileID: Schema.ObjectId,
  lastVisitKey: String,
  lastVisitKeyTracker: String,
  bot: Schema.Types.Mixed,
  visit_server_date: Number,
  visitor_localtime: Number,
  synced: Boolean,
  aggregated: Boolean,
  cphsynced: Boolean,
  uncheckedprofile: Boolean,
  device_info: {
    type: mongoose.Schema.Types.Object,
    ref: 'device_info'
  },
  goals: [goal_schema],
  current: {
    views: [view_schema]
  },
  controlgroups: Schema.Types.Mixed,
  experiments: Schema.Types.Mixed,
  visits: [visit_schema]
},{
  toJSON: {
    transform: function (doc, ret) {
      delete ret.visit_server_date;
      delete ret.visitor_localtime;
      delete ret.profileID;
      delete ret.lastVisitKey;
      delete ret.lastVisitKeyTracker;
      delete ret.aggregated;
      delete ret.cphsynced;
      delete ret.uncheckedprofile;
      delete ret.synced;
      delete ret.experiments;
    }
  }
});

/*
// Deprecate: we only use virtual property in gmid level
sub_profile.methods.profileExperiences = function(experiments){
  let exps = [];
  _(experiments).each(function(experiment, key){
    exps.push({
      uid: key,
      tstamp: experiment.timestamp
    });
  });

  return exps;
};
*/

module.exports = sub_profile;