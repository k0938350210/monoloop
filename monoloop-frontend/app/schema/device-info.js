let mongoose = require(__dirname + '/../../mongoose');
let Schema = mongoose.Schema;

let device_info = new Schema({
  config_browser_name: {
    type: String,
    default: ''
  },
  config_browser_version: {
    type: String,
    default: ''
  },
  config_browser_language: {
    type: String,
    default: ''
  },
  config_cookie: {
    type: Number,
    default: 1
  },
  config_director: {
    type: String,
    default: '0'
  },
  config_flash: {
    type: String,
    default: '0'
  },
  config_gears: {
    type: String,
    default: '0'
  },
  config_java: {
    type: String,
    default: '0'
  },
  config_os: {
    type: String,
    default: ''
  },
  config_pdf: {
    type: String,
    default: '0'
  },
  config_quicktime: {
    type: String,
    default: '0'
  },
  config_realplayer: {
    type: String,
    default: '0'
  },
  config_resolution: {
    type: String,
    default: ''
  },
  config_silverlight: {
    type: String,
    default: '0'
  },
  config_windowsmedia: {
    type: String,
    default: '0'
  }
},{ _id : false });

module.exports = device_info;