(function() {
  'use strict';
  var calculatePValue, clientRedis, experimentCtrl, getFromDB, initiateCacheActivites, logger, processPValue, redis;

  redis = require('./_parentCache');

  logger = redis.initiateLogger();

  experimentCtrl = require('./../../controller/experiment');

  clientRedis = redis.initiateCache();

  const debug = require('debug')('ml:archive');

  module.exports = {
    initiateState: function(cid, experimentID, cb) {
      var state;
      state = redis.getServerStatus();
      if (!state) {
        return getFromDB(cid, experimentID, cb);
      } else {
        return initiateCacheActivites(cid, experimentID, cb);
      }
    },
    calculatePValue: function(visitors, direct, cg_visitors, cg_direct) {
      return calculatePValue(visitors, direct, cg_visitors, cg_direct);
    },
    checkRedisExpiry: function(cid, experimentID, expObj, cb) {
      return clientRedis.hget(cid + '_' + experimentID + '_experiment', 'experiments', function(err, replies) {
        if (err) {
          logger.printError("redis get error");
          return cb("redis get error");
        } else {
          if (!replies) {
            return clientRedis.hset(cid + '_' + experimentID + '_experiment', 'experiments', JSON.stringify(expObj), function(er, replies) {
              if (er) {
                logger.printError("error redis set");
                return cb("error redis set");
              } else {
                clientRedis.expire(cid + '_' + experimentID + '_experiment', 1);
                return cb("redis set successful");
              }
            });
          } else {
            return cb("redis get successful");
          }
        }
      });
    }
  };

  initiateCacheActivites = function(cid, experimentID, cb) {
    debug('redis key:');
    debug(cid + '_' + experimentID + '_experiment');
    return clientRedis.hget(cid + '_' + experimentID + '_experiment', 'experiments', function(err, replies) {
      if (replies) {
        logger.print('EXPERIMENTS FOUND IN Redis');
        return cb(null, replies);
      } else {
        logger.print("going to get from db");
        return getFromDB(cid, experimentID, cb);
      }
    });
  };

  //   getFromDB cid, experimentID, (error, experiments)->
  //     if error
  //       cb null, null
  //     else
  //         logger.print 'EXPERIMENTS FOUND IN DB :::'

  //         if experiments
  //             processPValue cid, experimentID, updatedAtTimestamp, experiments, cb
  //         else
  //             cb null, null
  processPValue = function(cid, experimentID, updatedAtTimestamp, experiments, cb) {
    debug('here2');
    var cg_direct, cg_visitors, direct, expObj, exp_list, i, len, pValue, visitors;
    visitors = 0;
    direct = 0;
    cg_visitors = 0;
    cg_direct = 0;
    for (i = 0, len = experiments.length; i < len; i++) {
      exp_list = experiments[i];
      if (exp_list.experiments[experimentID] !== void 0) {
        if (exp_list.experiments[experimentID].visitors) {
          visitors += exp_list.experiments[experimentID].visitors;
        }
        if (exp_list.experiments[experimentID].direct) {
          direct += exp_list.experiments[experimentID].direct;
        }
        if (exp_list.experiments[experimentID].cg_visitors) {
          cg_visitors += exp_list.experiments[experimentID].cg_visitors;
        }
        if (exp_list.experiments[experimentID].cg_direct) {
          cg_direct += exp_list.experiments[experimentID].cg_direct;
        }
      }
    }
    debug('here');
    if (visitors === 0 || cg_visitors === 0) {
      pValue = 0;
    } else {
      pValue = calculatePValue(visitors, direct, cg_visitors, cg_direct);
    }
    expObj = {
      experimentID: experimentID,
      customerID: cid,
      visitors: visitors,
      direct: direct,
      cg_visitors: cg_visitors,
      cg_direct: cg_direct,
      pValue: pValue
    };
    //updated_at: updatedAtTimestamp
    experimentCtrl.updatePValue(expObj, updatedAtTimestamp, function(e, res) {
      if (e) {
        return logger.printError('error in pvalue update');
      } else {
        return debug('success in pvalue update');
      }
    });
    return clientRedis.hset(cid + '_' + experimentID + '_experiment', 'experiments', JSON.stringify(expObj), function(er, replies) {
      if (er) {
        return cb(null, JSON.stringify(expObj));
      } else {
        clientRedis.expire(cid + '_' + experimentID + '_experiment', 900);
        return cb(null, JSON.stringify(expObj));
      }
    });
  };

  getFromDB = function(cid, experimentID, cb) {
    return experimentCtrl.getExperimentLastUpdatedTimestamp(experimentID, cid, function(err, date) {
      var dt, query;
      debug("updatedAtTimestamp:");
      debug("expID: " + experimentID + " - date: " + date);
      if (err) {
        return cb(err, null);
      } else {
        if (date && date !== '') {
          dt = new Date(date);
          query = {};
          query['experiments'] = {
            '$exists': true
          };
          query['cid'] = parseInt(cid);
          query['experiments.' + experimentID] = {
            '$exists': true
          };
          query['datestamp'] = {
            '$gt': dt.getTime() / 1000
          };
          return experimentCtrl.getExperimentFromReportingAggregated(query, function(e, res) {
            if (e) {
              logger.printError("error in getExperimentFromReportingAggregated",e);
              return cb(e, null);
            } else {
              debug('EXPERIMENTS FOUND IN DB');
              if (res) {
                return processPValue(cid, experimentID, date, res, cb);
              } else {
                return cb(null, null);
              }
            }
          });
        } else {
          return cb(null, null);
        }
      }
    });
  };

  calculatePValue = function(visitors, direct, cg_visitors, cg_direct) {
    var a, d1, d2, d3, d4, d5, d6, diff_c, diff_v, e, pValue, standard_error, zValue;
    pValue = 0;
    try {
      diff_v = direct / visitors;
      diff_c = cg_direct / cg_visitors;
      standard_error = Math.sqrt((diff_v * (1 - diff_v) / visitors) + (diff_c * (1 - diff_c) / cg_visitors));
      if (!standard_error || standard_error === (0/0) || standard_error === 0) {
        return pValue = 0;
      } else {
        zValue = (diff_v - diff_c) / standard_error;
        d1 = 0.0498673470;
        d2 = 0.0211410061;
        d3 = 0.0032776263;
        d4 = 0.0000380036;
        d5 = 0.0000488906;
        d6 = 0.0000053830;
        a = Math.abs(zValue);
        pValue = 1.0 + a * (d1 + a * (d2 + a * (d3 + a * (d4 + a * (d5 + a * d6)))));
        pValue *= pValue;
        pValue *= pValue;
        pValue *= pValue;
        pValue *= pValue;
        pValue = 1.0 / (pValue + pValue);
        if (zValue >= 0) {
          pValue = 1 - pValue;
        }
        /*
        if (pValue > 0.5) {
          pValue = 1 - pValue;
        }
        */
        return Math.round(pValue * 1000) / 1000; //pValue.toFixed(3)
      }
    } catch (error) {
      e = error;
      console.error(e);
      return 0;
    }
  };

}).call(this);
