// Generated by CoffeeScript 2.0.2
(function() {
  'use strict';
  var getFromDB, initiateCacheActivites, logger, redis;

  redis = require('./_parentCache');

  logger = redis.initiateLogger();

  module.exports = {
    initiateState: function(req, cb) {
      var state;
      state = redis.getServerStatus();
      if (!state) {
        return getFromDB(req, cb);
      } else {
        return initiateCacheActivites(req, cb);
      }
    },
    _setMeta: function(req, cb) {
      var state;
      state = redis.getServerStatus();
      if (state) {
        return clientRedis.set(req.urlhash, req.docs, cb);
      } else {
        return cb({
          err: 'no meta set'
        }, null);
      }
    }
  };

  initiateCacheActivites = function(req, cb) {
    var clientRedis;
    return clientRedis = redis.initiateCache();
  };

  getFromDB = function(req, cb) {};

}).call(this);
