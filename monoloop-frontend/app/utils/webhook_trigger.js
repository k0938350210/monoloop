(function() {
  (function() {
    var http, performTriggerRequest;
    http = require('http');
    performTriggerRequest = void 0;
    const config = require('../../config').get('webhook');
    const config_s3 = require('../../config').get('s3');
    const debug = require('debug')('ml:webhook');
    const webhook = require('./../model/webhook');
    const mongoose = require('mongoose');

    let logger = require('./../utils/logger');

    let AWS = require('aws-sdk');
    AWS.config.update({
      accessKeyId: config_s3.access_key,
      secretAccessKey: config_s3.secret_key
    });

    let sqs = new AWS.SQS({region: config_s3.sqs.region});


    performTriggerFromCmd = function(cid, gmid, mid, cmd, callback){
      let or = [];
      let out_ids = [];
      let exp_ids = [];
      for(let k in cmd){
        // Check for segment
        if(k.startsWith('segments.')){
          let segment = k.split('.');
          if(segment[2] == 'count'){
            or.push({
              event_type: 'Audience',
              event_id: segment[1],
              direction: 'In'
            });
          }
          if(segment[2] == 'to'){
            if(out_ids.indexOf(segment[1]) === -1){
              out_ids.push(segment[1]);

              or.push({
                event_type: 'Audience',
                event_id: segment[1],
                direction: 'Out'
              });
            }
          }
        }

        if(k.startsWith('goals.')){
          let goals = k.split('.');
          if(goals[2] == 'amount'){
            or.push({
              event_type: 'Goal',
              event_id: goals[1]
            });
          }
        }

        if(k.startsWith('experiments.')){
          let experiments = k.split('.');
          if(exp_ids.indexOf(experiments[1]) === -1){
            exp_ids.push(experiments[1]);

            or.push({
              event_type: 'Experiment',
              event_id: experiments[1]
            });
          }
        }
      }

      if(or.length === 0){
        return callback(null, null);
      }

      let query = {
        cid: cid,
        status: 'active',
        deleted: { $ne: 1},
        $or: or
      };

      webhook.find(query,'_id',function (err, webhooks) {
        if(err){
          logger.printError('Error on webhook query',query);
          return callback(err,null);
        }

        if(webhooks.length == 0){
          // return nothing as nothing to process
          return callback(null, null);
        }

        let webhook_ids = [];
        for(let i = 0;i < webhooks.length; i++){
          webhook_ids.push(webhooks[i]._id);
        }

        //send to sqs
        let params = {
          MessageGroupId: 'webhook',
          MessageDeduplicationId: mongoose.Types.ObjectId().toString(),
          MessageAttributes: {
            "gmid": {
              DataType: "String",
              StringValue: gmid.toString()
            },
            "mid": {
              DataType: "String",
              StringValue: mid.toString()
            }
          },
          MessageBody: JSON.stringify(webhook_ids),
          QueueUrl: config_s3.sqs.webhook_queue
        };

        sqs.sendMessage(params, function(err, data) {
          if (err) {
            logger.printError('Error on sqs send message',err);
            callback(err,null);
          } else {
            debug("Success", data.MessageId);
            callback(null,null);
          }
        });
      });
    };

    var performTriggerInactiveProfile = function(gmid, mid, webhook_id, callback){
      let webhook_ids = [];
      webhook_ids.push(webhook_id);

      //send to sqs
      let params = {
        MessageGroupId: 'webhook',
        MessageDeduplicationId: mongoose.Types.ObjectId().toString(),
        MessageAttributes: {
          "gmid": {
            DataType: "String",
            StringValue: gmid.toString()
          },
          "mid": {
            DataType: "String",
            StringValue: mid.toString()
          }
        },
        MessageBody: JSON.stringify(webhook_ids),
        QueueUrl: config_s3.sqs.webhook_queue
      };

      sqs.sendMessage(params, function(err, data) {
        if (err) {
          logger.printError('Error on sqs send message',err);
          callback(err,null);
        } else {
          debug("Success", data.MessageId);
          callback(null,null);
        }
      });
    };

    performTriggerRequest = function(cid, gmid, mid, id, eventName, eventType, profile) {
      var e, endpoint, headers, host, options, req;
      try {
        if (id === void 0) {
          id = "0";
        }
        if (eventName === void 0) {
          eventName = "";
        }
        if (gmid === void 0) {
          gmid = "";
        }
        if (mid === void 0) {
          mid = "";
        }
        host = config.host;
        endpoint = '/webhook/trigger?cid=' + cid + '&id=' + id + '&gmid=' + gmid + '&mid=' + mid + '&event_type=' + eventType + '&event_name=' + eventName + '&profile=' + profile;
        headers = {
          'Content-Type': 'application/json'
        };
        //'authorization': authToken
        options = {
          host: host,
          path: endpoint,
          method: 'GET',
          headers: headers,
          port: config.port
        };

        req = http.request(options, function(res) {
          var responseString;
          responseString = '';
          res.on('data', function(data) {
            debug("data: " + data);
            responseString += data;
          });
          res.on('end', function() {
            debug(responseString);
          });
        });
        req.on('error', function(e) {
          // General error, i.e.
          //  - ECONNRESET - server closed the socket unexpectedly
          //  - ECONNREFUSED - server did not listen
          //  - HPE_INVALID_VERSION
          //  - HPE_INVALID_STATUS
          //  - ... (other HPE_* codes) - server returned garbage
          console.error("performTriggerRequest error:");
          console.error(e);
        });
        req.on('timeout', function() {
          // Timeout happend. Server received request, but not handled it
          // (i.e. doesn't send any response or it took to long).
          // You don't know what happend.
          // It will emit 'error' message as well (with ECONNRESET code).
          console.error('performTriggerRequest timeout');
          req.abort();
        });
        req.end();
      } catch (error) {
        e = error;
        console.error('Error in performTriggerRequest -> ' + e);
        req.end();
      }
    };
    module.exports = {
      performTriggerRequest: performTriggerRequest,
      performTriggerFromCmd: performTriggerFromCmd,
      performTriggerInactiveProfile: performTriggerInactiveProfile
    };
  }).call(this);

}).call(this);


//# sourceMappingURL=webhook_trigger.js.map
//# sourceURL=coffeescript