(function() {
  var assetCache, e, experimentCache, metaCache, ocrCache, pageElementsCache, segmentsCache;

  segmentsCache = require('./caching/_segment');

  pageElementsCache = require('./caching/_pageElements');

  ocrCache = require('./caching/_ocr');

  //avgCache = require './caching/_avg'
  assetCache = require('./caching/_asset');

  metaCache = require('./caching/_meta');

  experimentCache = require('./caching/_experiment');

  module.exports = (function() {
    try {
      return {
        setMetaCache: function(req, cb) {
          return metaCache._setMeta(req, cb);
        },
        setAssetCache: function(req, cb) {
          return assetCache._setAsset(req, cb);
        },
        invokeCaching: function(req, cb) {
          switch (req.typeOfKey) {
            case 'ocr':
              return ocrCache.initiateState(req, cb);
            case 'pageelements':
              return pageElementsCache.initiateState(req, cb);
            case 'segments':
              return segmentsCache.initiateState(req, cb);
            // when 'avg'
            //   avgCache.initiateState(req,cb)
            case 'assets':
              return assetCache.initiateState(req, cb);
          }
        },
        invokeExperimentCaching: function(cid, experimentID, cb) {
          var res;
          res = experimentCache.initiateState(cid, experimentID, cb);
          // console.log "invokeExperimentCaching result:"
          // console.log res
          return res;
        }
      };
    } catch (error) {
      e = error;
      logger.printError("Caching Layer Experienced an Exception", e);
      return cb(e, null);
    }
  })();

}).call(this);


//# sourceMappingURL=cachingLayer.js.map
//# sourceURL=coffeescript