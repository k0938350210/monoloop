(function() {
  // Element logic

  // Copyright (c) 2009-2019
  // License: see project's LICENSE file
  // Date: 17.07.12
  var ProfileModel, async, controller, crc32, crypto, ejs, elementCtrl, elementModel, logger, profileCtrl, urlM, util;

  elementModel = require('./../model/element');

  elementCtrl = require('./elements');

  profileCtrl = require('./profiles');

  ProfileModel = require('./../model/profile');

  ejs = require('ejs');

  urlM = require('url');

  crypto = require('crypto');

  async = require('async');

  crc32 = require('buffer-crc32');

  util = require('./../utils/helper');

  logger = require('./../utils/logger');

  let invoke_event = require('../services/invoke-event');
  let invoke_experience = require('../services/invoke/experience');

  controller = module.exports = {
    buildContent(parameters){
      let MonoloopAssets = parameters.assets;
      let MonoloopProfile = parameters.profile;
      let elements = parameters.elements;
      let vid = parameters.vid;
      let experienceGroups = parameters.experience.experience;
      if (!MonoloopAssets) {
        MonoloopAssets = [];
      }
      let data = [];
      let goals = [];
      let signals = [];
      let debug = {};
      let goalsOCR = {};
      let content_out = '';

      // Add to signal ? ### need recheck;
      for(let i = 0; i < experienceGroups.controlGroups.length; i++){
        let exp_id = experienceGroups.controlGroups[i];
        let exp = invoke_experience.findExperience(elements, exp_id);
        if (exp.pValue === undefined) {
          signals.push(exp.experimentID);
        }
      }
      for(let i = 0; i < experienceGroups.experiments.length; i++){
        let exp_id = experienceGroups.experiments[i];
        let exp = invoke_experience.findExperience(elements, exp_id);
        if (exp.pValue === undefined) {
          signals.push(exp.experimentID);
        }
      }

      if(elements && elements.length){
        baseFunctions = util.getElementsBasicFunctions();
        Segments = [];
        currentProfile = MonoloopProfile.getRecentDeviceLVPorfile();
        for(let i = 0; i < elements.length; i++){
          let Element = elements[i];
          if (Element.content && Element.content.length) {
            for(let j = 0; j < Element.content.length; j++){
              let content = Element.content[j];
              let exp = null;
              if(content.experimentID){
                exp = invoke_experience.findExperience(elements, content.experimentID);
              }
              if(exp){
                //Process as experiment content

                //Ignor if hidden
                if(exp.hidden){
                  continue;
                }
                let inCG = false;
                let inExp = false;

                if (experienceGroups.controlGroups.indexOf(exp.experimentID) !== -1) {
                  inCG = true;
                } else if(experienceGroups.experiments.indexOf(exp.experimentID) !== -1) {
                  inExp = true;
                }

                if (content.processed === void 0 && !content.segments) {
                  try {
                    if (MonoloopProfile.lastVisit.experiments !== void 0 && MonoloopProfile.lastVisit.experiments[exp.experimentID] !== void 0) {
                      if (MonoloopProfile.OCR !== void 0) {
                        MonoloopProfile.lastVisit.experiments[exp.experimentID].OCR = MonoloopProfile.OCR;
                      } else {
                        MonoloopProfile.lastVisit.experiments[exp.experimentID].OCR = false;
                      }
                    }
                    content.processed = true;
                    content_out = ejs.render(baseFunctions + content.code, {
                      MonoloopProfile: MonoloopProfile,
                      Element,
                      goals,
                      Segments,
                      MonoloopAssets
                    });
                  } catch (error1) {
                    err = error1;
                    logger.printError("elements->buildContent(ContentID" + content.uid + " / ProfileID:" + currentProfile.id + ")", err.message);
                    content_out = '';
                  }

                  if (content_out.replace(/^\s+|\s+$/g, "") !== '') {
                    if (MonoloopProfile.lastVisit.experiments  && MonoloopProfile.lastVisit.experiments[exp.experimentID]) {
                      MonoloopProfile.lastVisit.experiments[exp.experimentID].isPartOfExperiment = true;
                      MonoloopProfile.lastVisit.experiments[exp.experimentID].views++;
                      MonoloopProfile.lastVisit.experiments[exp.experimentID].aggregated = false;
                    } else {
                      MonoloopProfile.lastVisit.experiments[exp.experimentID] = {
                        goal: exp.goalID,
                        isPartOfExperiment: true,
                        views: 1
                      };
                    }
                    // Return content depend on experiement;
                    if (exp.pValue < 0.05 || (1 - exp.pValue) < 0.05) {
                      if (exp.significantAction === 0) {
                        // The significantAction is what Monoloop should do when experience has a significant
                        // result (pvalue < 0.05). When significantAction is set to 0 - the experiment should
                        // continue to run when result is significant. So basically we do not need to ever
                        // check pvalue here.
                        if (inExp) {
                          out = {
                            name: content.name,
                            uid: content.uid,
                            mappedTo: content.mappedToacheived,
                            placementType: content.placeacheivedmentType,
                            order: content.orderacheived,
                            displayType: content.displayacheivedType,
                            addJS: content.addJS,
                            pageID: Element.pageID,
                            urlOption: null,
                            incWWW: null,
                            fullURL: Element.fullURL,
                            additionalJS: Element.additionalJS,
                            content: content_out,
                            experiment: "experiment content got served"
                          };
                        } else {
                          out = {
                            abg: true,
                            name: content.name,
                            uid: content.uid,
                            experiment: "no content is served"
                          };
                        }
                      } else if (exp.significantAction === 1) {
                        // check who is the winner and serve that (original/experience)
                        if (exp.pValue > 0.5) {
                          out = {
                            name: content.name,
                            uid: content.uid,
                            mappedTo: content.mappedToacheived,
                            placementType: content.placeacheivedmentType,
                            order: content.orderacheived,
                            displayType: content.displayacheivedType,
                            addJS: content.addJS,
                            pageID: Element.pageID,
                            urlOption: null,
                            incWWW: null,
                            fullURL: Element.fullURL,
                            additionalJS: Element.additionalJS,
                            content: content_out,
                            experiment: "experiment content got served"
                          };
                        } else {
                          out = {
                            abg: true,
                            name: content.name,
                            uid: content.uid,
                            experiment: "in control group, no content is served"
                          };
                        }
                      } else if (exp.significantAction === 2) {
                        out = {
                          abg: true,
                          name: content.name,
                          uid: content.uid,
                          experiment: "no content is served"
                        };
                      }
                    } else {
                      if (inExp) {
                        out = {
                          name: content.name,
                          uid: content.uid,
                          mappedTo: content.mappedToacheived,
                          placementType: content.placeacheivedmentType,
                          order: content.orderacheived,
                          displayType: content.displayacheivedType,
                          addJS: content.addJS,
                          pageID: Element.pageID,
                          urlOption: null,
                          incWWW: null,
                          fullURL: Element.fullURL,
                          additionalJS: Element.additionalJS,
                          content: content_out,
                          experiment: "experiment content got served"
                        };
                      } else {
                        out = {
                          abg: true,
                          name: content.name,
                          uid: content.uid,
                          experiment: "no content is served"
                        };
                      }
                    }
                    if (out !== void 0) {
                      data.push(out);
                    }
                  }
                }
              }else{
                //Process as normal content
                content.processed = true;
                try {
                  let segment_code = '';
                  if(content.segments){
                    for (se in content.segments) {
                      segment_code += content.segments[se];
                    }
                  }
                  content_out = ejs.render(baseFunctions + segment_code + content.code, {
                    MonoloopProfile: MonoloopProfile,
                    Element,
                    goals,
                    Segments,
                    MonoloopAssets
                  });
                } catch (error1) {
                  err = error1;
                  logger.printError("elements->buildContent(ContentID" + content.uid + " / ProfileID:" + currentProfile.id + ")", err.message);
                  content_out = '';
                }
                if (content_out.replace(/^\s+|\s+$/g, "") !== '') {
                  //Check if its controlgroup
                  out = {
                    name: content.name,
                    uid: content.uid,
                    mappedTo: content.mappedTo,
                    placementType: content.placementType,
                    order: content.order,
                    displayType: content.displayType,
                    addJS: content.addJS,
                    pageID: Element.pageID,
                    urlOption: null,
                    incWWW: null,
                    fullURL: Element.fullURL,
                    additionalJS: Element.additionalJS,
                    content: content_out
                  };
                  data.push(out);
                }
              }
            }
          }
        }

      }

      invoke_event.processGoals(goals);
      debug.controlGroups = experienceGroups.controlGroups;
      debug.experimentGroups = experienceGroups.experiments;
      return {data, goals, signals, debug, goalsOCR};
    },

    buildContent2: function(parameters) {

      var Element, MonoloopAssets, MonoloopProfile, Segments, baseFunctions, content, content_out, currentExp, currentProfile, data, debug, elements, err, exp, expHidden, experienceGroups, goals, goalsOCR, i, inCG, inExp, j, k, l, len, len1, len2, len3, out, pValue, ref, ref1, ref2, se, segment_served, signals, vid;
      elements = parameters.elements;
      MonoloopProfile = parameters.profile;
      MonoloopAssets = parameters.assets;
      vid = parameters.vid;
      experienceGroups = parameters.experience.experience;
      if (!MonoloopAssets) {
        MonoloopAssets = [];
      }
      data = []; //parameters.experience.data
      goals = [];
      signals = [];
      debug = {};
      goalsOCR = {};
      if (elements !== void 0 && elements.length) {
        baseFunctions = util.getElementsBasicFunctions();
        Segments = [];
        currentProfile = MonoloopProfile.getRecentDeviceLVPorfile();
        for (i = 0, len = elements.length; i < len; i++) {
          Element = elements[i];
          if (Element.Experiment) {
            ref = Element.Experiment;
            for (j = 0, len1 = ref.length; j < len1; j++) {
              exp = ref[j];
              expHidden = false;
              if (exp.hidden !== void 0) {
                if (exp.hidden === 1) {
                  expHidden = true;
                }
              }
              if (!expHidden) {
                pValue = exp.pValue;
                if (pValue === void 0) {
                  signals.push(exp.experimentID);
                }
              }
            }
          }
          if (Element.content !== void 0) {
            ref1 = Element.content;
            for (k = 0, len2 = ref1.length; k < len2; k++) {
              content = ref1[k];
              //Check OCR is defined and set to related uid for Goal OCR configuration
              if (content.goal !== void 0 && content.goal === true && MonoloopProfile.OCR !== void 0) {
                goalsOCR[content.uid] = MonoloopProfile.OCR;
              }
              inCG = false;
              inExp = false;
              currentExp = void 0;
              if (Element.Experiment !== void 0) {
                ref2 = Element.Experiment;
                for (l = 0, len3 = ref2.length; l < len3; l++) {
                  exp = ref2[l];
                  if (exp !== void 0 && content.experimentID !== void 0 && exp.experimentID === content.experimentID) {
                    currentExp = exp;
                  }
                }
              }
              if (currentExp !== void 0) {
                expHidden = false;
                if (currentExp.hidden !== void 0) {
                  if (currentExp.hidden === 1) {
                    expHidden = true;
                  }
                }
                if (!expHidden) {
                  if (content.processed === void 0 && !content.segments) {
                    try {
                      if (MonoloopProfile.lastVisit.experiments !== void 0 && MonoloopProfile.lastVisit.experiments[currentExp.experimentID] !== void 0) {
                        if (MonoloopProfile.OCR !== void 0) {
                          MonoloopProfile.lastVisit.experiments[currentExp.experimentID].OCR = MonoloopProfile.OCR;
                        } else {
                          MonoloopProfile.lastVisit.experiments[currentExp.experimentID].OCR = false;
                        }
                      }
                      if (experienceGroups.controlGroups.indexOf(currentExp.experimentID) !== -1) {
                        inCG = true;
                      } else {
                        if (experienceGroups.experiments.indexOf(currentExp.experimentID) !== -1) {
                          inExp = true;
                        }
                      }
                      content.processed = true;
                      content_out = ejs.render(baseFunctions + content.code, {
                        MonoloopProfile: MonoloopProfile,
                        Element,
                        goals,
                        Segments,
                        MonoloopAssets
                      });
                    } catch (error1) {
                      err = error1;
                      logger.printError("elements->buildContent(ContentID" + content.uid + " / ProfileID:" + currentProfile.id + ")", err.message);
                      content_out = '';
                    }
                    if (content_out.replace(/^\s+|\s+$/g, "") !== '') {
                      if (MonoloopProfile.lastVisit.experiments !== void 0) {
                        if (MonoloopProfile.lastVisit.experiments[currentExp.experimentID] !== void 0) {
                          MonoloopProfile.lastVisit.experiments[currentExp.experimentID].isPartOfExperiment = true;
                          MonoloopProfile.lastVisit.experiments[currentExp.experimentID].views++;
                          MonoloopProfile.lastVisit.experiments[currentExp.experimentID].aggregated = false;
                        } else {
                          MonoloopProfile.lastVisit.experiments[currentExp.experimentID] = {
                            goal: currentExp.goalID,
                            isPartOfExperiment: true,
                            views: 1
                          };
                        }
                      } else {
                        MonoloopProfile.lastVisit.experiments = [];
                        MonoloopProfile.lastVisit.experiments[currentExp.experimentID] = {
                          goal: currentExp.goalID,
                          isPartOfExperiment: true,
                          views: 1
                        };
                      }
                      if (pValue < 0.05 || (1 - pValue) < 0.05) {
                        if (currentExp.significantAction === 0) {
                          // The significantAction is what Monoloop should do when experience has a significant
                          // result (pvalue < 0.05). When significantAction is set to 0 - the experiment should
                          // continue to run when result is significant. So basically we do not need to ever
                          // check pvalue here.
                          if (inExp) {
                            out = {
                              name: content.name,
                              uid: content.uid,
                              mappedTo: content.mappedToacheived,
                              placementType: content.placeacheivedmentType,
                              order: content.orderacheived,
                              displayType: content.displayacheivedType,
                              addJS: content.addJS,
                              pageID: Element.pageID,
                              urlOption: null,
                              incWWW: null,
                              fullURL: Element.fullURL,
                              additionalJS: Element.additionalJS,
                              content: content_out,
                              experiment: "experiment content got served"
                            };
                          } else {
                            out = {
                              abg: true,
                              name: content.name,
                              uid: content.uid,
                              experiment: "no content is served"
                            };
                          }
                        } else if (currentExp.significantAction === 1) {
                          // check who is the winner and serve that (original/experience)
                          if (pValue > 0.5) {
                            out = {
                              name: content.name,
                              uid: content.uid,
                              mappedTo: content.mappedToacheived,
                              placementType: content.placeacheivedmentType,
                              order: content.orderacheived,
                              displayType: content.displayacheivedType,
                              addJS: content.addJS,
                              pageID: Element.pageID,
                              urlOption: null,
                              incWWW: null,
                              fullURL: Element.fullURL,
                              additionalJS: Element.additionalJS,
                              content: content_out,
                              experiment: "experiment content got served"
                            };
                          } else {
                            out = {
                              abg: true,
                              name: content.name,
                              uid: content.uid,
                              experiment: "in control group, no content is served"
                            };
                          }
                        } else if (currentExp.significantAction === 2) {
                          out = {
                            abg: true,
                            name: content.name,
                            uid: content.uid,
                            experiment: "no content is served"
                          };
                        }
                      } else {
                        if (inExp) {
                          out = {
                            name: content.name,
                            uid: content.uid,
                            mappedTo: content.mappedToacheived,
                            placementType: content.placeacheivedmentType,
                            order: content.orderacheived,
                            displayType: content.displayacheivedType,
                            addJS: content.addJS,
                            pageID: Element.pageID,
                            urlOption: null,
                            incWWW: null,
                            fullURL: Element.fullURL,
                            additionalJS: Element.additionalJS,
                            content: content_out,
                            experiment: "experiment content got served"
                          };
                        } else {
                          out = {
                            abg: true,
                            name: content.name,
                            uid: content.uid,
                            experiment: "no content is served"
                          };
                        }
                      }
                      if (out !== void 0) {
                        data.push(out);
                      }
                    }
                  }
                }
              }
              segment_served = false;
              if (content.segments !== void 0 && content.segments !== null && (content.segments.length > 1 || content.segments.length === void 0)) {
                for (se in content.segments) {
                  try {
                    content_out = ejs.render(baseFunctions + content.segments[se] + content.code, {
                      MonoloopProfile: MonoloopProfile,
                      Element,
                      goals,
                      Segments,
                      MonoloopAssets
                    });
                  } catch (error1) {
                    err = error1;
                    logger.printError("elements->buildContent(ContentID" + content.uid + " / ProfileID:" + currentProfile.id + ")", err.message);
                    content_out = '';
                  }
                  if (content_out.replace(/^\s+|\s+$/g, "") !== '') {
                    segment_served = true;
                    //Check if its controlgroup
                    out = {
                      name: content.name,
                      uid: content.uid,
                      mappedTo: content.mappedTo,
                      placementType: content.placementType,
                      order: content.order,
                      displayType: content.displayType,
                      addJS: content.addJS,
                      pageID: Element.pageID,
                      urlOption: null,
                      incWWW: null,
                      fullURL: Element.fullURL,
                      additionalJS: Element.additionalJS,
                      content: content_out
                    };
                    data.push(out);
                  } else {
                    segment_served = false;
                  }
                }
              } else {
                if (segment_served === false && content.processed === void 0) {
                  content.processed = true;
                  try {
                    content_out = ejs.render(baseFunctions + content.code, {
                      MonoloopProfile: MonoloopProfile,
                      Element,
                      goals,
                      Segments,
                      MonoloopAssets
                    });
                  } catch (error1) {
                    err = error1;
                    logger.printError("elements->buildContent(ContentID" + content.uid + " / ProfileID:" + currentProfile.id + ")", err.message);
                    content_out = '';
                  }
                  if (content_out.replace(/^\s+|\s+$/g, "") !== '') {
                    //Check if its controlgroup
                    out = {
                      name: content.name,
                      uid: content.uid,
                      mappedTo: content.mappedTo,
                      placementType: content.placementType,
                      order: content.order,
                      displayType: content.displayType,
                      addJS: content.addJS,
                      pageID: Element.pageID,
                      urlOption: null,
                      incWWW: null,
                      fullURL: Element.fullURL,
                      additionalJS: Element.additionalJS,
                      content: content_out
                    };
                    data.push(out);
                  }
                }
              }
            }
          }
        }
      }

      invoke_event.processGoals(goals);

      debug.controlGroups = experienceGroups.controlGroups;
      debug.experimentGroups = experienceGroups.experiments;
      return {data, goals, signals, debug, goalsOCR};
    },
    get: function(request, response, callback = false) {
      var _ids, doNotCheck, hasToLoadProfile, isWWW, query, suburl, tmp, url, url_q, urlhashes, urls;
      query = {};
      doNotCheck = false;
      hasToLoadProfile = false;
      if (callback) {
        this.callback = callback;
      }
      if (callback && request.query.pages && request.query.pages.match(/^[0-9a-fA-F]{24}$/)) {
        query = {};
        _ids = request.query.pages.split(',');
        query['_id'] = {
          '$in': _ids
        };
        query['cid'] = parseInt(request.query.cid);
        query['hidden'] = 0;
        query['deleted'] = 0;
        query['content'] = {
          '$exists': true
        };
        doNotCheck = true;
      } else if (callback && request.query.url) {
        //Get URL
        url = request.query.url;
        //Check if www is in url
        isWWW = false;
        if (request.query.url.indexOf('://www.') > -1) {
          isWWW = true;
          //Remove www.
          url = url.replace('://www.', '://');
        }
        url_q = urlM.parse(url);
        //find all urlhashes down to domain to support StartsWith:
        suburl = url_q.path;
        urlhashes = [];
        urls = [];
        while (suburl) {
          tmp = url_q.protocol + '//' + url_q.host + suburl;
          urls.push(tmp);
          urlhashes.push((crypto.createHash('md5').update(tmp).digest('hex')).substr(0, 16) + crc32.unsigned(tmp).toString(16));
          suburl = suburl.substring(0, suburl.length - 1);
        }
        urlhashes.push(crypto.createHash('md5').update('default').digest('hex').substr(0, 16) + crc32.unsigned('default').toString(16));
        urls.push('default');
        urlhashes.push((crypto.createHash('md5').update(url_q.protocol + '//' + url_q.host).digest('hex')).substr(0, 16) + crc32.unsigned(url_q.protocol + '//' + url_q.host).toString(16));
        urls.push(url_q.protocol + '//' + url_q.host);
        //TODO: finish querybuild, build multiple hashes to be sure "StartsWith" works too. Same with exactQS, inWWW and exWWW
        query = {};
        query['cid'] = parseInt(request.query.cid);
        query['hidden'] = 0;
        query['deleted'] = 0;
        query['urlhash'] = {
          '$in': urlhashes
        };
        query['content'] = {
          '$exists': true
        };
        if (isWWW) {
          query['inWWW'] = true;
        } else {
          query['exWWW'] = true;
        }
      } else if (request.params !== void 0 && request.params.param1 === 'elements' && request.params.param2) {
        //Request for single element request with id
        query = {
          id: request.params.param2
        };
      } else if (request.params !== void 0 && request.params.param3 === 'elements' && request.params.param4) {
        query = {
          id: request.params.param4
        };
      }
      return elementModel.find(query).lean().exec(function(error, elements) {
        var MonoloopProfile, checkElement, element, i, len, out;
        //Define function to check elements:
        checkElement = function(el) {
          //Let them trough if they have been found by ID
          if (doNotCheck) {
            return true;
          }
          //Check for req.exp
          if (el.regExp && el.regExp !== '') {
            if (url.match(new RegExp(el.regExp))) {
              return true;
            } else {
              return false;
            }
          }
          //Check for www
          if (isWWW) {
            if (!el.inWWW) {
              return false;
            }
          } else {
            if (!el.exWWW) {
              return false;
            }
          }
          //Check for StartsWith
          if (!el.startsWith) {
            if (url !== el.fullURL) {
              return false;
            }
          }
          return true;
        };
        // Error
        if (error) {
          if (callback) {
            callback(error);
          } else {
            response.json({
              success: false,
              error: error
            });
          }
          return;
        }
        // Return element objects...
        if (callback) {
          out = [];
// Filter elements depending on original url, StartsWith and IncludeWWW
          for (i = 0, len = elements.length; i < len; i++) {
            element = elements[i];
            if (checkElement(element)) {
              out.push(element);
            }
          }
          return callback(null, out);
        } else if (hasToLoadProfile) {
          return profileCtrl.get(request, response, {
            '_id': request.params.param2
          }, function(profile) {
            var MonoloopProfile;
            if (profile && profile[0] instanceof ProfileModel) {
              MonoloopProfile = profile[0];
            } else {
              MonoloopProfile = new ProfileModel;
            }
            return elementCtrl.buildContent(elements, MonoloopProfile, null, null, function(err, out) {
              return response.json({
                success: true,
                done: true,
                elements: out.data
              });
            });
          });
        } else {
          MonoloopProfile = new ProfileModel;
          return elementCtrl.buildContent(elements, MonoloopProfile, null, null, function(err, out) {
            return response.json({
              success: true,
              done: true,
              elements: out.data
            });
          });
        }
      });
    }
  };

}).call(this);
