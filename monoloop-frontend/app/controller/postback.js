(function() {
  var AssetModel, AssetQueue, ProfileModel, async, bcrc32, cachedLayer, checkType, controller, crc, cryptSalt, hardCodedConfigs, logger, profileCtrl, publisher, sha512, debug;

  AssetModel = require('./../model/asset');

  AssetQueue = require('./../model/asset_queue');

  async = require('async');

  sha512 = require('crypto');

  bcrc32 = require('buffer-crc32');

  crc = require('crc');

  cryptSalt = '130293+109k21jl1c31c9312i3c910202';

  profileCtrl = require('./profiles');

  ProfileModel = require('./../model/profile');

  cachedLayer = require('./../utils/cachingLayer');

  logger = require('./../utils/logger');

  publisher = require('./../../redis-conn');

  const json_helper = require('../utils/json');
  debug = require('debug')('ml:postback');

  controller = module.exports = {
    parse: function(request, response) {
      var command, crc32, error, out, querytoTriggerUnique, updateProfileData;
      command = {
        $inc: {},
        $set: {},
        $addToSet: {}
      };
      crc32 = this.crc32;
      updateProfileData = this.updateProfileData;
      querytoTriggerUnique = false;
      try {
        return async.auto({
          getProfileData: [
            function(callback) {
              debug('POSTBACK GMID:' + request.query.gmid);
              debug('POSTBACK CID:' + request.query.cid);
              if (request.query.gmid) {
                return ProfileModel.find({
                  _id: request.query.gmid
                }, null,{
                  limit: 1
                }).exec(function(error, profiles) {
                  var query;
                  if (error) {
                    debug("error:");
                    debug(error);
                    return callback(error,null);
                  } else {
                    if (profiles && profiles[0]) {
                      if (request.query.uniqueID && !profiles[0].uniqueID) {
                        query = {
                          uniqueID: request.query.uniqueID,
                          customerID: request.query.cid
                        };
                        return ProfileModel.findOne(query, null, null, function(err, result) {
                          if (result) {
                            result.customerID = profiles[0].customerID;
                            result.pgmid = profiles[0].pgmid;
                            result.ngmid = profiles[0].ngmid;
                            result.cv = profiles[0].cv;
                            result.co = profiles[0].co;
                            result.form = profiles[0].form;
                            result.OCR = profiles[0].OCR;
                            result.privacy = profiles[0].privacy;
                            //result.cachedProperties = profiles[0].cachedProperties
                            result.profiles.push(profiles[0].profiles[0]);
                            result.recently_used_device = profiles[0].recently_used_device;
                            result.save();
                            //re-assing values
                            request.query.gmid = result._id;
                            request.query.mid = result.recently_used_device;
                            querytoTriggerUnique = true;
                            return callback(null, result);
                          }else{
                            return callback(null, profiles[0]);
                          }
                        });
                      } else {
                        return callback(null, profiles[0]);
                      }
                    } else {
                      error = {
                        "error": "No Profile found. Cannot Proceed"
                      };
                      return callback(error, null);
                    }
                  }
                });
              } else {
                return callback(null, null);
              }
            }
          ],
          Assets: [
            'getProfileData', function(results, callback) {
              var changedAsset, cid, error, req, requestObj, url, urlhash;
              debug('ASSETS TASK IS PROCESSING');
              cid = parseInt(request.query.cid, 10);
              if (request.query.as && request.query.url && request.query.as.length > 2) {
                url = request.query.url.replace('://www.', '://');
                urlhash = (sha512.createHash('md5').update(url).digest('hex')).substr(0, 16) + bcrc32.unsigned(url).toString(16);
                try {
                  changedAsset = JSON.parse(request.query.as);
                } catch (error1) {
                  error = error1;
                  debug('JSON PARSE ERROR WITH AS PARAMETER VALUE');
                  logger.printLineBreaker();
                  debug(request.query.as, false);
                  logger.printLineBreaker();
                  changedAsset = [];
                }
                requestObj = {
                  assets: changedAsset,
                  url: url,
                  urlhash: urlhash,
                  mid: request.query.mid,
                  gmid: request.query.gmid
                };
                req = {
                  request: requestObj,
                  typeOfKey: 'assets'
                };
                return cachedLayer.invokeCaching(req, function(err, results) {
                  return callback(null, results);
                });
              } else {
                return callback(null,{
                  status: true,
                  assets: [{
                    'refID': false
                  }]
                });
              }
            }
          ],
          Downloads: [
            'getProfileData',function(results,callback) {
              var c_p_idx,c_v_idx,dl,error,nmb,profile;
              if (request.query.dl && request.query.dl.length > 2) {
                debug('DOWNLOAD TASK IS PROCESSING');
                //Do the downloadsaving
                nmb = 0;
                dl = json_helper.tryParseJSON(request.query.dl);
                if(dl === false){
                  return callback(null,{
                    status: true,
                    msg: 'invalid json'
                  });
                }
                if (dl.length > 0) {
                  nmb = dl.length;
                  profile = results.getProfileData;
                  c_p_idx = profile.getIndex();
                  c_v_idx = profile.getLastVisitIndex();
                  command.$addToSet['profiles.' + c_p_idx + '.visits.' + c_v_idx + '.downloads'] = {
                    $each: dl
                  };
                }
                return callback(null,{
                  status: true,
                  msg: 'Saved ' + nmb + ' downloads'
                });
              } else {
                return callback(null,{
                  status: true,
                  msg: 'No download data to save'
                });
              }
            }
          ],
          Searches: [
            'getProfileData',function(results,callback) {
              var c_p_idx,c_v_idx,error,nmb,profile,st;
              if (request.query.st && request.query.st.length > 2) {
                debug('SEARCHES TASK IS PROCESSING');
                //Do the searchsaving
                nmb = 0;
                st = json_helper.tryParseJSON(request.query.st);
                if(st === false){
                  return callback(null,{
                    status: true,
                    msg: 'invalid json'
                  });
                }
                if (st.length > 0) {
                  nmb = st.length;
                  profile = results.getProfileData;
                  c_p_idx = profile.getIndex();
                  c_v_idx = profile.getLastVisitIndex();
                  command.$addToSet['profiles.' + c_p_idx + '.visits.' + c_v_idx + '.searches'] = {
                    $each: st
                  };
                }
                return callback(null,{
                  status: true,
                  msg: 'Saved ' + nmb + ' searches'
                });
              } else {
                return callback(null,{
                  status: true,
                  msg: 'No search data to save'
                });
              }
            }
          ],
          cart: [
            'getProfileData',function(results,callback) {
              var cart;
              if (request.query.cart) {
                debug('CART TASK IS PROCESSING');
                cart = json_helper.tryParseJSON(request.query.cart);
                if(cart === false){
                  return callback(null,{
                    status: true,
                    msg: 'invalid json'
                  });
                }
                if (cart.basketObj !== '[]' && cart.basketObj.length !== void 0 && cart.basketObj.length > 0) {
                  cart.basketObj.forEach(function(basket) {
                    var basketId,c_p_idx,c_v_idx,crc_n,profile;
                    crc_n = crc32(basket.n);
                    basketId = basket.id;
                    delete basket["id"];
                    profile = results.getProfileData;
                    c_p_idx = profile.getIndex();
                    c_v_idx = profile.getLastVisitIndex();
                    return command.$set['profiles.' + c_p_idx + '.visits.' + c_v_idx + '.carts.' + basketId + '.' + crc_n] = basket;
                  });
                  return callback(null,{
                    status: true,
                    msg: 'Cart information updated'
                  });
                } else {
                  return callback(null,{
                    status: true,
                    msg: 'No Cart information'
                  });
                }
              } else {
                return callback(null,{
                  status: true,
                  msg: 'No Cart information'
                });
              }
            }
          ],
          purchaseCart: [
            'getProfileData',function(results,callback) {
              var c_p_idx,c_v_idx,error,i,item,k,len,pc,profile,profile_indexs,purchased,pv,ref,ref1,v,visits;
              if (request.query.pc) {
                pv = 0.0;
                purchased = json_helper.tryParseJSON(request.query.pc);
                if(purchased === false){
                  return callback(null,{
                    status: true,
                    msg: 'invalid json'
                  });
                }
                profile_indexs = results.getProfileData;
                c_p_idx = profile_indexs.getIndex();
                c_v_idx = profile_indexs.getLastVisitIndex();
                if (purchased.length > 0) {
                  results1 = [];
                  for (i = 0, len = purchased.length; i < len; i++) {
                    pc = purchased[i];
                    if (pc.id !== void 0) {
                      command.$set['profiles.' + c_p_idx + '.visits.' + c_v_idx + '.carts.' + pc.id + '.p'] = true;
                      profile = results.getProfileData.profiles[0];
                      visits = results.getProfileData.getVisitByIndex(c_v_idx);
                      if (visits) {
                        ref = visits.carts;
                        for (k in ref) {
                          v = ref[k];
                          if (k.toString() === pc.id) {
                            ref1 = visits.carts[k];
                            for (item in ref1) {
                              v = ref1[item];
                              if (visits.carts[k][item].p && visits.carts[k][item].q) {
                                pv += parseFloat(visits.carts[k][item].p) * parseInt(visits.carts[k][item].q);
                              }
                            }
                          }
                        }
                        command.$inc['profiles.' + c_p_idx + '.visits.' + c_v_idx + '.pv'] = pv;
                      }
                    }
                  }
                  return callback(null,{
                    status: true,
                    msg: 'Purchase Cart information updated'
                  });
                } else {
                  return callback(null,null);
                }
              } else {
                return callback(null,{
                  status: true,
                  msg: 'No purchase info updated'
                });
              }
            }
          ],
          form: [
            'getProfileData',function(results,callback) {
              var currFormKey,currFormVal,currForms,currProfile,error,firstPostFormVal,form,formd,formdObj,formg,forms,i,j,l,len,len1,len2,ref,ref1,value;
              if (request.query.form) {
                debug('FORM TASK IS PROCESSING');
                forms = json_helper.tryParseJSON(request.query.form);
                if(forms === false){
                  return callback(null,{
                    status: true,
                    msg: 'invalid json'
                  });
                }
                currProfile = results.getProfileData;
                if (currProfile && currProfile !== void 0) {
                  if (currProfile.profiles[0]) {
                    currForms = currProfile.form;
                    formdObj = {};
                    formg = {};
                    for (i = 0, len = forms.length; i < len; i++) {
                      form = forms[i];
                      if (form.g && form.d !== void 0) {
                        ref = form.d;
                        for (j = 0, len1 = ref.length; j < len1; j++) {
                          formd = ref[j];
                          //get first it use for all type but 'push'
                          if (formd.n instanceof Array) {
                            firstPostFormVal = formd.n[0];
                          } else {
                            firstPostFormVal = formd.n;
                          }
                          currFormVal = currForms[form.g];
                          if (currFormVal !== void 0) {
                            currFormVal = currFormVal[formd.g];
                            currFormKey = form.g.toString() + '.' + formd.g.toString();
                            if (formd.s === hardCodedConfigs().saveType['push']) {
                              if (!(currFormVal instanceof Array)) {
                                currFormVal = [currFormVal];
                              }
                              if (!(formd.n instanceof Array)) {
                                formd.n = [formd.n];
                              }
                              ref1 = formd.n;
                              for (l = 0, len2 = ref1.length; l < len2; l++) {
                                value = ref1[l];
                                if (checkType(formd.t,value)) {
                                  if (formd.t === hardCodedConfigs().dataType['date']) {
                                    value = new Date(value);
                                  }
                                  currFormVal.push(value);
                                }
                              }
                              command.$set['form.' + currFormKey] = currFormVal;
                            } else {
                              if (checkType(formd.t,firstPostFormVal)) {
                                // override
                                if (formd.s === hardCodedConfigs().saveType['override']) {
                                  if (formd.t === hardCodedConfigs().dataType['date']) {
                                    firstPostFormVal = new Date(firstPostFormVal);
                                  }
                                  command.$set['form.' + currFormKey] = firstPostFormVal;
                                // add
                                } else if (formd.s === hardCodedConfigs().saveType['add']) {
                                  if (formd.t === hardCodedConfigs().dataType['number']) {
                                    command.$inc['form.' + currFormKey] = firstPostFormVal;
                                  }
                                  if (formd.t === hardCodedConfigs().dataType['boolean']) {
                                    command.$set['form.' + currFormKey] = currFormVal || firstPostFormVal;
                                  }
                                // subtract
                                } else if (formd.s === hardCodedConfigs().saveType['subtract']) {
                                  if (formd.t === hardCodedConfigs().dataType['number']) {
                                    command.$inc['form.' + currFormKey + '.1'] = firstPostFormVal * (-1);
                                  }
                                }
                              }
                            }
                            command.$set['form.' + form.g.toString() + '.updated_at'] = Math.round(new Date().getTime() / 1000);
                          } else {
                            //add new to DB if FORM not exist
                            if (checkType(formd.t,firstPostFormVal)) {
                              if (formd.t === hardCodedConfigs().dataType['date']) {
                                firstPostFormVal = new Date(firstPostFormVal);
                              }
                              formdObj[formd.g.toString()] = firstPostFormVal;
                              formdObj['created_at'] = Math.round(new Date().getTime() / 1000);
                              command.$set['form.' + form.g.toString()] = formdObj;
                            }
                          }
                        }
                      }
                    }
                    return callback(null,{
                      status: true,
                      msg: 'Form Saved'
                    });
                  } else {
                    return callback(null,null);
                  }
                } else {
                  return callback(null,{
                    status: true,
                    msg: 'No Form data to save'
                  });
                }
              } else {
                return callback(null,{
                  status: true,
                  msg: 'No Form data to save'
                });
              }
            }
          ],
          externalKeys: [
            'getProfileData', function(results, callback){
              if(request.query.external){
                let currProfile = results.getProfileData;
                let externalKeys = currProfile.externalKeys;

                if(command.$push == undefined){
                  command.$push = {};
                }

                let data = json_helper.tryParseJSON(request.query.external);
                for(let i = 0; i < data.length; i++){
                  let obj = data[i];
                  if(obj.k && obj.v){
                    //get current index;
                    let match_index = false;
                    for(let j = 0; j < externalKeys.length; j++){
                      if(externalKeys[j].k == obj.k){
                        match_index = j;
                      }
                    }

                    if(match_index === false){
                      if(command.$push['externalKeys'] === undefined){
                        command.$push['externalKeys'] = {};
                        command.$push['externalKeys'].$each = [];
                      }

                      command.$push['externalKeys'].$each.push({
                        k: obj.k,
                        v: obj.v
                      });
                    }else{
                      command.$set['externalKeys.'+match_index+'.v'] = obj.v;
                      // found exists external key data
                    }
                  }
                }
                return callback(null,null);
              }else{
                return callback(null,{
                  status: true,
                  msg: 'No External keys data to save'
                });
              }
            }
          ],
          CustomVars: [
            'getProfileData',function(results,callback) {
              var currCVs,currCvKey,currCvVal,currProfile,cv,cvd,cvdObj,cvg,cvs,error,firstPostCvVal,i,j,l,len,len1,len2,ref,ref1,value;
              if (request.query.custom) {
                debug('CUSTOMVARS TASK IS PROCESSING');
                cvs = json_helper.tryParseJSON(request.query.custom);
                if(cvs === false){
                  return callback(null,{
                    status: true,
                    msg: 'invalid json'
                  });
                }
                currProfile = results.getProfileData;
                if (currProfile && currProfile !== void 0) {
                  if (currProfile.profiles[0]) {
                    currCVs = currProfile.cv;
                    cvdObj = {};
                    cvg = {};
                    for (i = 0, len = cvs.length; i < len; i++) {
                      cv = cvs[i];
                      if (cv.g && cv.d !== void 0) {
                        ref = cv.d;
                        for (j = 0, len1 = ref.length; j < len1; j++) {
                          cvd = ref[j];
                          //get first it use for all type but 'push'
                          if (cvd.n instanceof Array) {
                            firstPostCvVal = cvd.n[0];
                          } else {
                            firstPostCvVal = cvd.n;
                          }
                          currCvVal = currCVs[cv.g];
                          if (currCvVal !== void 0) {
                            currCvVal = currCvVal[cvd.g];
                            currCvKey = cv.g.toString() + '.' + cvd.g.toString();
                            if (cvd.s === hardCodedConfigs().saveType['push']) {
                              if (!(currCvVal instanceof Array)) {
                                currCvVal = [currCvVal];
                              }
                              if (!(cvd.n instanceof Array)) {
                                cvd.n = [cvd.n];
                              }
                              ref1 = cvd.n;
                              for (l = 0, len2 = ref1.length; l < len2; l++) {
                                value = ref1[l];
                                if (checkType(cvd.t,value)) {
                                  if (cvd.t === hardCodedConfigs().dataType['date']) {
                                    value = new Date(value);
                                  }
                                  currCvVal.push(value);
                                }
                              }
                              command.$set['cv.' + currCvKey] = currCvVal;
                            } else {
                              if (checkType(cvd.t,firstPostCvVal)) {
                                // override
                                if (cvd.s === hardCodedConfigs().saveType['override']) {
                                  if (cvd.t === hardCodedConfigs().dataType['date']) {
                                    firstPostCvVal = new Date(firstPostCvVal);
                                  }
                                  command.$set['cv.' + currCvKey] = firstPostCvVal;
                                // add
                                } else if (cvd.s === hardCodedConfigs().saveType['add']) {
                                  if (cvd.t === hardCodedConfigs().dataType['number']) {
                                    command.$inc['cv.' + currCvKey] = firstPostCvVal;
                                  }
                                  if (cvd.t === hardCodedConfigs().dataType['boolean']) {
                                    command.$set['cv.' + currCvKey] = currCvVal || firstPostCvVal;
                                  }
                                // subtract
                                } else if (cvd.s === hardCodedConfigs().saveType['subtract']) {
                                  if (cvd.t === hardCodedConfigs().dataType['number']) {
                                    command.$inc['cv.' + currCvKey + '.1'] = firstPostCvVal * (-1);
                                  }
                                }
                              }
                            }
                            command.$set['cv.' + cv.g.toString() + '.updated_at'] = Math.round(new Date().getTime() / 1000);
                          } else {
                            //add new to DB if CV not exist
                            if (checkType(cvd.t,firstPostCvVal)) {
                              if (cvd.t === hardCodedConfigs().dataType['date']) {
                                firstPostCvVal = new Date(firstPostCvVal);
                              }
                              cvdObj[cvd.g.toString()] = firstPostCvVal;
                              cvdObj['created_at'] = Math.round(new Date().getTime() / 1000);
                              command.$set['cv.' + cv.g.toString()] = cvdObj;
                            }
                          }
                        }
                      }
                    }
                    return callback(null,{
                      status: true,
                      msg: 'Custom Vars Saved'
                    });
                  } else {
                    return callback(null,null);
                  }
                } else {
                  return callback(null,{
                    status: true,
                    msg: 'No Custom Vars data to save'
                  });
                }
              } else {
                return callback(null,{
                  status: true,
                  msg: 'No Custom Vars data to save'
                });
              }
            }
          ],
          Category: [ 'getProfileData','Assets',function(results,callback) {
            var asset,c_p_idx,c_v_idx,categories,category,error,i,j,key,len,len1,objAddToSet,objInc,objSet,profile,ref,strKey,tmp;
              if (request.query.cats && request.query.cats.length > 2) {
                debug('CATGORY TASK IS PROCESSING');
                categories = json_helper.tryParseJSON(request.query.cats);
                if(categories === false){
                  return callback(null,{
                    status: true,
                    msg: 'invalid json'
                  });
                }
                profile = results.getProfileData;
                c_p_idx = profile.getIndex();
                c_v_idx = profile.getLastVisitIndex();

                objSet = {};
                objInc = {};
                objAddToSet = {};
                for (i = 0, len = categories.length; i < len; i++) {
                  category = categories[i];
                  if (category.g && category.n) {
                    key = category.g + '_' + crc32(category.n);
                    strKey = 'profiles.' + c_p_idx + '.visits.' + c_v_idx + '.categories.' + key.toString();
                    if ((profile.lastVisit.categories != null) && (profile.lastVisit.categories[key.toString()] != null)) {
                      command.$set[strKey + '.aggregated'] = {
                        aggregated: false,
                        previousDuration: profile.lastVisit.categories[key].d != null ? profile.lastVisit.categories[key].d : 0,
                        previousCount: profile.lastVisit.categories[key].c ? profile.lastVisit.categories[key].c : 0
                      };
                    } else {
                      command.$set[strKey + '.aggregated'] = {
                        aggregated: false,
                        previousDuration: 0,
                        previousCount: 0
                      };
                    }
                    if (category.d && category.d > 0) {
                      //This is a duration update
                      command.$inc[strKey + '.d'] = category.d;
                    } else {
                      //This is a new category hit
                      command.$inc[strKey + '.c'] = 1;
                      if (results.Assets && results.Assets.assets && category.a && category.a > 0) {
                        tmp = [];
                        ref = results.Assets.assets;
                        for (j = 0, len1 = ref.length; j < len1; j++) {
                          asset = ref[j];
                          if (asset.refID && asset.assetID && asset.assetID === category.a) {
                            tmp.push(asset.refID);
                          }
                        }
                        if (tmp.length > 0) {
                          command.$addToSet[strKey + '.a'] = {
                            $each: tmp
                          };
                        }
                      }
                    }
                    //Always update name and value
                    command.$set[strKey + '.n'] = category.n;
                    command.$set[strKey + '.v'] = category.v;
                    command.$set[strKey + '.g'] = category.g;
                    command.$set[strKey + '.t'] = category.t;
                    command.$set[strKey + '.id'] = category.id;
                  }
                }
                return callback(null,{
                  status: true,
                  msg: 'Category Saved',
                  signal: true
                });
              } else {
                return callback(null,{
                  status: true,
                  msg: 'No Category data to save',
                  signal: false
                });
              }
            }
          ],
          Content: [
            'getProfileData',function(results,callback) {
              var c_p_idx,c_v_idx,contentIDs,currProfile,error;
              if (request.query.content_ids && request.query.content_ids.length > 2) {
                debug('CONTENT TASK IS PROCESSING');
                contentIDs = json_helper.tryParseJSON(request.query.content_ids);
                if(contentIDs === false){
                  return callback(null,{
                    status: true,
                    msg: 'invalid json'
                  });
                }
                currProfile = results.getProfileData;
                c_p_idx = currProfile.getIndex();
                c_v_idx = currProfile.getLastVisitIndex();
                if (contentIDs.length > 0) {
                  command.$addToSet['profiles.' + c_p_idx + '.visits.' + c_v_idx + '.content'] = {
                    $each: contentIDs
                  };
                  return callback(null,{
                    status: true,
                    msg: 'Content Saved',
                    signal: true
                  });
                } else {
                  return callback(null,{
                    status: true,
                    msg: 'No content data to save',
                    signal: false
                  });
                }
              } else {
                return callback(null,{
                  status: true,
                  msg: 'No content data to save',
                  signal: false
                });
              }
            }
          ],
          saveToMongo: ['Downloads', 'CustomVars', 'form', 'Category', 'Content', 'Assets', 'Searches', 'cart', 'purchaseCart',
            function(results, callback) {
              var query;
              debug('SAVETOMONGO TASK IS PROCESSING');
              //Save data to mongo:
              Object.keys(command).forEach(function(cmdKey, value) {
                if (Object.keys(command[cmdKey]).length < 1) {
                  return delete command[cmdKey];
                }
              });
              if (querytoTriggerUnique) {
                query = {
                  '_id': request.query.gmid,
                  'uniqueID': request.query.uniqueID,
                  'profiles._id': request.query.mid
                };
                return ProfileModel.updateOne(query, command, {}, function(err, updated){
                  if (err) {
                    return callback(err, null);
                  } else {
                    return callback(null,{
                      status: true,
                      msg: 'successfully saved/updated'
                    });
                  }
                });
              } else {
                if (request.query.gmid && request.query.mid){
                  query = {
                    '_id': request.query.gmid,
                    'profiles._id': request.query.mid
                  };
                  return ProfileModel.updateOne(query, command, {}, function(err, updated){
                    if (err) {
                      return callback(err, null);
                    } else {
                      return callback(null,{
                        status: true,
                        msg: 'successfully saved/updated'
                      });
                    }
                  });
                }else{
                  return callback(null, null);
                }
              }
            }
          ]
        }, function(err, results) {
          var gmid, mid, out, trackerSignal, transport, v;
          if (err) {
            debug("err:");
            debug(err);
            response.writeHead(200, {
              'Content-type': 'text/javascript'
            });
            return response.end('MONOloop.postbackError=' + JSON.stringify(err));
          } else {
            debug('Success postback process');
            debug(results);
            trackerSignal = false;
            gmid = request.query.gmid;
            mid = request.query.mid;
            for (v in results) {
              if (results[v] && results[v].signal) {
                trackerSignal = true;
              }
            }
            transport = {
              gmid: gmid,
              mid: mid,
              signals: {
                "tracker": "enabled"
              }
            };
            if (trackerSignal) {
              publisher.rpush('profilelist', JSON.stringify(transport));
            }
            command = null;
            //logger.print 'OUTPUT TASK IS PROCESSING'
            delete results['getProfileData'];
            out = {
              eval: 'localStorage.setItem("gmid","' + gmid + '");localStorage.setItem("mid","' + mid + '");localStorage.setItem("skey","' + sha512.createHash('sha512').update(gmid + cryptSalt).digest('hex') + '");'
            };
            response.writeHead(200, {
              'Content-type': 'text/javascript'
            });
            return response.end('MONOloop.postbackResult=' + JSON.stringify(out));
          }
        });
      } catch (error1) {
        error = error1;
        debug(new Date() + ": postback->parse() - " + error);
        out = 'MONOloop.postbackResult={"error":"Unknown Error"};';
        response.writeHead(200, {
          'Content-type': 'text/javascript'
        });
        return response.end(out);
      }
    },
    crc32: function(sMessage) {
      var aCRC32Table, bytC, bytT, h, i, iCRC, len, lngA;
      aCRC32Table = new Array(0x0, 0x77073096, 0xEE0E612C, 0x990951BA, 0x76DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3, 0xEDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988, 0x9B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91, 0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7, 0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172, 0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B, 0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59, 0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924, 0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190, 0x1DB7106, 0x98D220BC, 0xEFD5102A, 0x71B18589, 0x6B6B51F, 0x9FBFE4A5, 0xE8B8D433, 0x7807C9A2, 0xF00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x86D3D2D, 0x91646C97, 0xE6635C01, 0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E, 0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65, 0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0, 0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F, 0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6, 0x3B6E20C, 0x74B1D29A, 0xEAD54739, 0x9DD277AF, 0x4DB2615, 0x73DC1683, 0xE3630B12, 0x94643B84, 0xD6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0xA00AE27, 0x7D079EB1, 0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC, 0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5, 0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B, 0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236, 0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D, 0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x26D930A, 0x9C0906A9, 0xEB0E363F, 0x72076785, 0x5005713, 0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0xCB61B38, 0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0xBDBDF21, 0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777, 0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45, 0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2, 0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9, 0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94, 0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D);
      iCRC = 0xFFFFFFFF;
      for (i = 0, len = sMessage.length; i < len; i++) {
        h = sMessage[i];
        bytC = h;
        bytT = (iCRC & 0xFF) ^ bytC;
        lngA = iCRC >> 8;
        iCRC = lngA ^ aCRC32Table[bytT];
      }
      return iCRC ^ 0xFFFFFFFF;
    },
    generateDataHash: function(data) {
      var el, i, k, len, str, tmp, v;
      tmp = [];
      for (k in data) {
        v = data[k];
        tmp.push({
          id: k,
          val: v
        });
      }
      tmp.sort(function(a, b) {
        if (a.id >= b.id) {
          return 1;
        } else {
          return -1;
        }
      });
      str = '';
      for (i = 0, len = tmp.length; i < len; i++) {
        el = tmp[i];
        str += el.val;
      }
      return (sha512.createHash('md5').update(str).digest('hex')).substr(0, 16) + bcrc32.unsigned(str).toString(16);
    },
    updateProfileData: function(query, command) {
      return ProfileModel.updateOne(query, command, {}, function(err, updated) {
        if (err) {
          return {
            update: 0,
            msg: err
          };
        }
        return {
          update: updated
        };
      });
    }
  };

  //Helper Functions for custom vars and forms
  hardCodedConfigs = function() {
    return {
      saveType: {
        'override': 1,
        'add': 2,
        'subtract': 3,
        'push': 4
      },
      dataType: {
        'number': 1,
        'string': 2,
        'boolean': 3,
        'date': 4 //Date is a string
      }
    };
  };

  checkType = function(data1, data2) {
    var tempDate;
    tempDate = new Date(data2);
    if (data1 === hardCodedConfigs().dataType['number'] && typeof data2 === 'number') {
      return true;
    } else if (data1 === hardCodedConfigs().dataType['string'] && typeof data2 === 'string') {
      if (!isNaN(new Date(data2) && !isNaN(parseInt(data2.split('-')[0])))) {
        if (parseInt(data2.split('-')[0]) === tempDate.getFullYear()) {
          return false;
        }
      }
      return true;
    } else if (data1 === hardCodedConfigs().dataType['boolean'] && typeof data2 === 'boolean') {
      return true;
    } else if (data1 === hardCodedConfigs().dataType['date'] && !isNaN(tempDate) && typeof data2 === 'string') {
      if (!isNaN(parseInt(data2.split('-')[0]))) {
        if (parseInt(data2.split('-')[0]) === tempDate.getFullYear()) {
          return true;
        }
      }
      return false;
    } else {
      return false;
    }
  };

}).call(this);


//# sourceMappingURL=postback.js.map
//# sourceURL=coffeescript