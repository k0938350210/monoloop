(function() {
  // Profile logic

  // Copyright (c) 2009-2012 [Vyacheslav Voronchuk](mailto:voronchuk@gmail.com), [Web management software](http://wm-software.com)
  // License: see project's LICENSE file
  // Date: 17.07.12
  var ProfileModel, async, consumer, controller, util, debug;

  ProfileModel = require('./../model/profile');

  //AvgModel = require './../model/averages'
  async = require('async');

  util = require('./../utils/helper');

  consumer = require('../../lib/doubleLinkedProfiles');

  debug = require('debug')('ml:profile');

  controller = module.exports = {
    get: function(request, response, q = null, callback = false) {
      var options, query;
      // Check for single record - otherwise load all profiles (based on limit and offset)
      query = {
        '_id': request.query.gmid
      };
      options = {
        limit: 50
      };
      if (q) {
        query = q;
      } else if (request.params.param2) {
        query['_id'] = request.params.param2;
      }
      //Checl for limit and skip
      if (request.query.limit) {
        options['limit'] = request.query.limit;
      }
      if (request.query.offset) {
        options['skip'] = request.query.offset;
      }
      // Get all profile records
      debug(query);

      ProfileModel.find(query, null, options, function(error, profiles) {
        // Error
        if (error) {
          if (callback) {
            return callback(error);
          } else {
            return response.json({
              success: false,
              error: error
            });
          }
        } else {
          // Return profile objectload
          if (callback) {
            return callback(null, profiles);
          } else {
            return response.json({
              success: true,
              profiles: profiles
            });
          }
        }
      });
    },
    getSingle: function(request, response) {
      var options, query;
      // Check for single record - otherwise load all profiles (based on limit and offset)
      query = {
        customerID: request.username
      };
      if (request.params.param1) {
        query['_id'] = request.params.param1;
      }
      options = {};
      //if request.query.updated then query = {updated:true}
      options['limit'] = 1;
      debug(query);
      return ProfileModel.find(query, null, options).exec(function(error, profiles) {
        // Error
        if (error) {
          response.json({
            success: false,
            error: error
          });
        } else {
          return response.json({
            success: true,
            profiles: profiles
          });
        }
      });
    },
    getSingleByID: function(request, response) {
      var options, query;
      query = {};
      options = {};
      if (request.params && request.params.param) {
        if (request.params.param.match(/^[0-9a-fA-F]{24}$/)) {
          query['_id'] = request.params.param;
          query['customerID'] = request.username;
        } else {
          query['uniqueID'] = request.params.param;
          query['customerID'] = request.username;
        }
      }
      options['limit'] = 1;
      debug(query);
      return ProfileModel.find(query, null, options).exec(function(error, profiles) {
        // Error
        if (error) {
          response.json({
            success: false,
            error: error
          });
        } else {
          return response.json({
            success: true,
            profiles: profiles
          });
        }
      });
    },
    getAll: function(request, response) {
      var options, query, sort;
      query = {
        customerID: request.username
      };
      if (request.query.uid) {
        query["_id"] = request.query.uid;
      }
      options = {
        limit: 50,
        skip: 0
      };
      sort = {};
      if (request.query.limit) {
        if (Number(request.query.limit) > 500 || Number(request.query.limit) <= 0) {
          response.json({
            success: false,
            error: 'limit should be 1 to 500'
          });
          return;
        } else if (!isNaN(request.query.limit)) {
          options['limit'] = request.query.limit ? Number(request.query.limit) : 50;
        } else {
          return response.json({
            success: false,
            error: 'limit should be a number value'
          });
        }
      }
      if (request.query.offset) {
        if (!isNaN(request.query.offset) && Number(request.query.offset) >= 0) {
          options['skip'] = request.query.offset ? Number(request.query.offset) : 0;
        } else {
          return response.json({
            'success': false,
            error: 'offset should be a positive number value'
          });
        }
      }
      //sort option
      if (request.query.sort) {
        if (request.query.sort.toLowerCase() === 'asc') {
          sort = {
            _id: 1
          };
        } else if (request.query.sort.toLowerCase() === 'desc') {
          sort = {
            _id: -1
          };
        }
      }
      //options['skip'] = if request.query.offset then request.query.offset else 0

      // Get all profile records
      if (request.query.debug) {
        debug(query, request.query);
      }
      if (request.query.debug) {
        debug(options);
      }
      // To  check uniqueID
      if ((request.query.externalKey != null) && request.query.externalValue !== "") {
        query = {
          customerID: request.username,
          externalKeys: {
            '$elemMatch':{
              k: request.query.externalKey,
              v: request.query.externalValue
            }
          }
        };
        options = {};
        options['limit'] = 1;
        options['skip'] = 0;
      } else if ((request.query.uniqueID != null) ) {
        query["$and"] = [
          {
            uniqueID: request.query.uniqueID
          }
        ];
      }
      return ProfileModel.find(query, null, options).sort(sort).exec(function(error, profiles) {
        // Error
        if (error) {
          console.error(error);
          response.status(500).send({
            success: false,
            error: "There is an error. Please contact",
            link: "https://www.monoloop.com/contact/"
          });
        } else {
          return response.json({
            success: true,
            profiles: profiles
          });
        }
      });
    },
    put: function(request, response, data = false, callback = false) {
      var options, query, updatedData;
      // @todo: some validation here
      options = {};
      query = {};
      // Update profile model
      updatedData = {}; // @todo: add profile changes here
      if (data) {
        //Call from batch
        updatedData = data.profile;
        options = data.options;
        query = data.query;
      } else {
        //Call from web service
        query = {
          _id: request.params.param2
        };
      }
      return ProfileModel.updateOne(query, updatedData, options, function(error, updated) {
        // Error
        if (error) {
          if (callback) {
            console.error(error);
            callback(error);
          } else {
            response.json({
              success: false,
              error: error
            });
          }
          return;
        }
        // Return profile object
        if (callback) {
          return callback(null, updated);
        } else {
          return response.json({
            success: true,
            profiles: updated
          });
        }
      });
    },
    post: function(request, response, ProfileData = null) {
      var profileModel;
      // @todo: some validation here

      // Create new profile model
      profileModel = new ProfileModel({
        // @todo: put model data here
        customerID: ProfileData ? ProfileData.cid : request.body.CustomerID,
        config_browser_name: request.body.BrowserName,
        config_browser_version: request.body.BrowserVersion,
        config_cookie: 1,
        config_director: request.body.DirectorEnabled,
        config_flash: request.body.FlashEnabled,
        config_gears: request.body.GearsEnabled,
        config_java: request.body.JavaEnabled,
        config_os: request.body.OS,
        config_pdf: request.body.PDFEnabled,
        config_quicktime: request.body.QuickTimeEnabled,
        config_realplayer: request.body.RealPlayerEnabled,
        config_resolution: request.body.ScreenResolution,
        config_silverlight: request.body.SilverlightEnabled,
        config_windowsmedia: request.body.WindowsMediaPlayerEnabled,
        location_browser_lang: request.body.BrowserLanguage,
        location_city: request.body.City,
        location_country: request.body.Country,
        location_ip: request.body.IP,
        location_lat: request.body.Latitude,
        location_long: request.body.Longtitude,
        visit_server_date: Date.now,
        visitor_localtime: request.body.LocalTime
      });
      // Save model
      return profileModel.save(function(error, profile) {
        // Error
        if (error) {
          response.json({
            success: false,
            error: error
          });
        } else {
          // Return profile object
          return response.json({
            success: true,
            profile: profile
          });
        }
      });
    },
    delete: function(request, response) {
      if (request.param && request.param.param2) {
        return ProfileModel.findByIdAndRemove(request.params.param2, function(error, success) {
          if (error) {
            response.json({
              success: false,
              error: error
            });
          } else if (success) {
            // Return profile object
            return response.json({
              success: true,
              success: success
            });
          } else {
            return response.json({
              success: true
            });
          }
        });
      } else {
        return response.json({
          success: false,
          error: "Invalid Profile"
        });
      }
    },
    emptyProfile: function(request, response) {
      var  query;
      query = request.query.gmid;
      return ProfileModel.findById(query, '-__v', function(error, profile) {
        // Error
        if (error) {
          response.json({
            success: false,
            error: error
          });
        } else if (profile) {
          return async.auto({
            relocatePointers: [
              function(callback) {
                return consumer.update(profile,
              function(err,
              result) {
                  return callback(err,
              result);
                });
              }
            ],
            removeData: [
              'relocatePointers',
              function(results,
              callback) {
                var currentMid,
              dataToKeep,
              i,
              k3,
              key,
              key2,
              len,
              mid,
              p,
              p1,
              ref,
              ref1,
              ref2,
              singleProfile;
                dataToKeep = ['_id',
              'privacy',
              'customerID',
              'OCR'];
                mid = "";
                currentMid = "";
                ref = profile.toObject();
                for (key in ref) {
                  p = ref[key];
                  if (key === 'recently_used_device') {
                    currentMid = p;
                  }
                  if (key === 'profiles') {
                    ref1 = profile[key];
                    for (key2 = i = 0, len = ref1.length; i < len; key2 = ++i) {
                      singleProfile = ref1[key2];
                      ref2 = singleProfile.toObject();
                      for (k3 in ref2) {
                        p1 = ref2[k3];
                        if (k3 === '_id') {
                          mid = p1;
                        }
                        if (dataToKeep.indexOf(k3) === -1) {
                          profile[key][key2][k3] = void 0;
                        }
                      }
                    }
                  } else {
                    if (dataToKeep.indexOf(key) === -1) {
                      profile[key] = void 0;
                    }
                  }
                }
                profile.cv = void 0;
                profile.co = void 0;
                profile.form = void 0;
                if (profile.privacy === void 0) {
                  profile.privacy = {
                    'personalization': 0,
                    'email': null,
                    'retargeting': null,
                    'lastViewed': new Date().getTime() / 1000,
                    'lastUpdated': new Date().getTime() / 1000
                  };
                }
                if (currentMid === null || void 0) {
                  currentMid = mid;
                }
                profile.privacy.lastProfileDelete = currentMid;
                profile.markModified('privacy');
                return profile.save(function(err,
              success) {
                  return callback(err,
              success);
                });
              }
            ]
          }, function(err, result) {
            if (err) {
              console.error(err);
              return response.json({
                success: false,
                error: err
              });
            } else {
              return response.json({
                success: true
              });
            }
          });
        } else {
          return response.json({
            success: false,
            message: 'No data found'
          });
        }
      });
    },
    updatePrivacy: function(request, response) {
      var command, personalization, query;
      personalization = request.query.personalization || 0;
      query = {
        _id: request.query.gmid,
        customerID: request.query.cid
      };
      command = {
        '$set': {
          'privacy': {
            'personalization': parseInt(personalization),
            'email': null,
            'retargeting': null,
            'lastViewed': new Date().getTime() / 1000,
            'lastUpdated': new Date().getTime() / 1000
          }
        }
      };
      return ProfileModel.updateOne(query, command, function(error, profile) {
        // Error
        if (error) {
          console.error(error);
          return response.json({
            success: false,
            error: error
          });
        } else if (profile) {
          return response.json({
            success: true,
            privacyConfiguration: command.$set.privacy
          });
        } else {
          return response.json({
            success: false,
            message: 'No data found to Update'
          });
        }
      });
    }
  };

}).call(this);


//# sourceMappingURL=profiles.js.map
//# sourceURL=coffeescript