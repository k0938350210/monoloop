
const baseArchive = require('../../workers/modules/baseController');
const baseVariable = require('../../workers/modules/baseVariables');
const wrapper = require('../utils/wrapper');
const async = require('async');

module.exports = {
  process:async function(req, res){
    let gmid = req.query.gmid;
    let onlyTracker = false;
    const { error, data } = await wrapper(baseArchive.initiateArchive(gmid));
    let gprofile = data;
    let asyncTasks = [];
    let signals = req.query.signals;

    gprofile.profiles.forEach(function(profile) {
      if (gprofile.recently_used_device === profile._id.toString() && profile.visits) {
        let baseVar = baseVariable.initializeArchive(gprofile, onlyTracker);
        if (!baseVar.onlyTracker) {
          if (!baseVar.onlyTracker) {
            //calculate calculatePValue
            asyncTasks.push(function(callback) {
              baseArchive.experimentSignal(baseVar, signals, callback);
            });
            //sync profile
            asyncTasks.push(function(callback) {
              baseArchive.initiateSyncProfile(baseVar, callback);
            });
            //calculate segments
            asyncTasks.push(function(callback) {
              baseArchive.processSegments(baseVar, callback);
            });
            //Bot calculation
            asyncTasks.push(function(callback) {
              baseArchive.detechbot(baseVar, callback);
            });

            async.parallel(asyncTasks, function(err, results) {
              baseVar.asyncResultFetch(results);

              if (!baseVar.onlyTracker) {
                baseArchive.profileGenericParamsInc(baseVar);
                baseArchive.calculateSegments(baseVar);
                baseArchive.calculateGoals(baseVar);
                baseArchive.calculateExperiments(baseVar);
              } else if (baseVar.onlyTracker) {
                baseArchive.trackersArchive(baseVar);
              }

              baseArchive.profileArchiveUpdator(baseVar, function(error, updated) {
                res.json({success:true,updated: updated});
              });

            });
          }
        }
      }
    });

  }
};