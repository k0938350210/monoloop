const mongoose = require('../../mongoose');
const redis = require('../../redis-conn');
const sentinel = require('../../redis-sentinel-conn');

const async = require('async');
const json = require('../utils/json');

module.exports = {
  check: function(request, response){
    async.parallel([ function(callback) {
      if(mongoose.connection.readyState === 1){
        callback(null,null);
      }else{
        callback('mongo',null);
      }
    }, function(callback) {
      if(redis.status === 'ready'){
        callback(null,null);
      }else{
        callback('redis',null);
      }
    }, function(callback) {
      if(sentinel.status === 'ready'){
        callback(null,null);
      }else{
        callback('sentinel',null);
      }
    } ], function done(err, results) {
        if (err) {
          response.status(502);
          return response.json({
            err: err
          });
        }
        return response.json({
          everything: "is ok"
        });
    });
  },

  test: function(request, response){
    async.parallel([ function(callback) {
      callback(null,null);
    }, function(callback) {
      let test = json.tryParseJSON('{"x":123}');
      if(test === false){
        return callback('invalid json',null);
      }
      callback(null,null);
    }, function(callback) {
      callback(null,null);
    } ], function done(err, results) {
      if (err) {
        response.status(502);
        return response.json({
          err: err
        });
      }
      return response.json({
        everything: "is ok"
      });
    });
  }
}