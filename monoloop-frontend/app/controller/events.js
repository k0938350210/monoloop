(function() {
  // Profile logic

  // Copyright (c) 2009-2012 [Vyacheslav Voronchuk](mailto:voronchuk@gmail.com), [Web management software](http://wm-software.com)
  // License: see project's LICENSE file 
  // Date: 17.07.12
  var ProfileModel, controller;

  ProfileModel = require('./../model/profile');

  controller = module.exports = {
    // Profile model

    // Read profiles
    get: function(request, response, q = false, callback = false) {
      var query;
      // Check for single record - otherwise load all profiles (based on limit and offset)
      query = '';
      if (q) {
        query = q;
      } else if (request.params.param2) {
        query = request.params.param2;
      }
      
      // Get all profile records
      return ProfileModel.findById(query, '', null, function(error, profiles) {
        // Error
        if (error) {
          if (callback) {
            callback(profiles);
          } else {
            response.json({
              success: false,
              error: error
            });
          }
          return;
        }
        // Return profile object
        if (callback) {
          return callback(profiles);
        } else {
          return response.json({
            success: true,
            profile: {
              id: profiles.id,
              events: profiles.events
            }
          });
        }
      });
    },
    
    // Update single profile
    post: function(request, response, callback = false) {
      var err, errorMsg, eventData, options, query, updatedData;
      //init vars
      updatedData = false;
      query = false;
      options = {};
      errorMsg = 'Unknown error';
      // @todo: some validation here
      if (request.params.param2) {
        query = {
          _id: request.params.param2
        };
      }
      if (request.body.event) {
        try {
          
          //Check data
          eventData = JSON.parse(request.body.event);
          if (eventData.eventId && eventData.data) {
            updatedData = {
              $addToSet: {
                events: eventData
              }
            };
          }
        } catch (error1) {
          err = error1;
          errorMsg = 'Malformed JSON event data';
        }
      }
      console.log(updatedData);
      if (updatedData && query) {
        return ProfileModel.updateOne(query, updatedData, options, function(error, updated) {
          // Error
          if (error) {
            if (callback) {
              callback(error);
            } else {
              response.json({
                success: false,
                error: error
              });
            }
            return;
          }
          // Return profile object
          if (callback) {
            return callback(updated);
          } else {
            return response.json({
              success: true,
              eventsUpdated: updated
            });
          }
        });
      } else {
        return response.json({
          success: false,
          error: errorMsg,
          eventsUpdated: 0
        });
      }
    },
    
    // Create single profile
    put: function(request, response, ProfileData = null) {},
    // @todo: some validation here
    /*
     * Create new profile model
    profileModel = new @profile
     * @todo: put model data here
    	customerID: if ProfileData then ProfileData.cid else request.body.CustomerID
    	config_browser_name: request.body.BrowserName
    	config_browser_version: request.body.BrowserVersion
    	config_cookie: 1
    	config_director: request.body.DirectorEnabled
    	config_flash: request.body.FlashEnabled
    	config_gears: request.body.GearsEnabled
    	config_java: request.body.JavaEnabled
    	config_os: request.body.OS
    	config_pdf: request.body.PDFEnabled
    	config_quicktime: request.body.QuickTimeEnabled
    	config_realplayer: request.body.RealPlayerEnabled
    	config_resolution: request.body.ScreenResolution
    	config_silverlight: request.body.SilverlightEnabled
    	config_windowsmedia: request.body.WindowsMediaPlayerEnabled
    	location_browser_lang: request.body.BrowserLanguage
    	location_city: request.body.City
    	location_country: request.body.Country
    	location_ip: request.body.IP
    	location_lat: request.body.Latitude
    	location_long: request.body.Longtitude
    	visit_server_date: Date.now
    	visitor_localtime: request.body.LocalTime

     * Save model
    profileModel.save (error, profile) ->
     * Error
    	if error 
    		response.json 
    			success: false
    			error: error
    		return

     * Return profile object
    	response.json 
    		success: true
    		profile: profile
     */

    // Remove single profile
    delete: function(request, response) {
      // Get profile model
      return ProfileModel.findById(request.params.param2, function(error, profile) {
        // Error
        if (error) {
          response.json({
            success: false,
            error: error
          });
          return;
        }
        
        // Mark to remove
        profile.remove();
        
        // Apply changes
        return profile.save(function(error) {
          // Error
          if (error) {
            response.json({
              success: false,
              error: error
            });
            return;
          }
          // Return profile object
          return response.json({
            success: true
          });
        });
      });
    }
  };

}).call(this);


//# sourceMappingURL=events.js.map
//# sourceURL=coffeescript