(function() {
  // Logic to parse batched requests

  // Copyright (c) 2009-2012 [Vyacheslav Voronchuk](mailto:voronchuk@gmail.com), [Web management software](http://wm-software.com)
  // License: see project's LICENSE file
  // Date: 10.10.17
  "use strict";
  var ProfileModel, assetCtrl, async, cachedLayer, controller, cryptSalt, ejs, elementCtrl, getHighPriorityExperiment, helper, logger, ocrConfig, profileCtrl, redis, sha512, util, debug, publisher;

  ProfileModel = require('./../model/profile');

  sha512 = require('crypto');

  async = require('async');

  elementCtrl = require('./elements');

  profileCtrl = require('./profiles');

  assetCtrl = require('./assets');

  util = require('util');

  cachedLayer = require('./../utils/cachingLayer');

  redis = require('redis');

  helper = require('./../utils/helper');

  logger = require('./../utils/logger');

  publisher = require('./../../redis-conn');

  ocrConfig = require('./../model/ocrConfig');

  let account_service = require('../services/account');

  let invoke_event = require('../services/invoke-event');

  let invoke_page_element = require('../services/invoke/page-element');

  let invoke_profile = require('../services/invoke/profile');
  let invoke_ocr = require('../services/invoke/ocr');
  let invoke_experience = require('../services/invoke/experience');

  cryptSalt = '130293+109k21jl1c31c9312i3c910202';

  ejs = require('ejs');

  debug = require('debug')('ml:invoke');
  /*
  var kue = require('kue')
  , queue = kue.createQueue();
  */
  controller = module.exports = {
    parse: function(request, response) {
      invoke_event.resetEvents();

      return async.auto({
        'loadAccountData':[
          function(callback){
            account_service.loadAccountData(callback, request);
          }
        ],
        loadPageElements: [
          function(callback){
            invoke_page_element.loadPageElements(callback, request, response);
          }
        ],
        detectProfileStatus: [
          function(callback) {
            invoke_profile.detectProfileStatus(callback, request, response);
          }
        ],
        getOCRStatus: [
          function(callback) {
            invoke_ocr.getOCRStatus(callback, request);
          }
        ],
        loadProfile: [
          'loadPageElements',
          'detectProfileStatus',
          'getOCRStatus',
          function(results, callback) {
            invoke_profile.loadProfile(results, callback, request);
          }
        ],
        assignExperienceGroup: [
          'loadPageElements',
          'loadProfile',
          function(results,
          callback) {
            var Element,
          MonoloopAssets,
          MonoloopProfile,
          Segments,
          baseFunctions,
          content,
          content_out,
          controlGroups,
          currentProfile,
          data,
          date1_ms,
          date2_ms,
          difference_ms,
          dts,
          e,
          elements,
          err,
          exp,
          expHidden,
          experience,
          experimentDays,
          experimentGroups,
          exps,
          firstview,
          goals,
          idxInCG,
          idxInEx,
          i,
          j,
          k,
          len,
          len1,
          len2,
          one_day,
          r,
          ref,
          se,
          segment_served,
          ts,
          vid,
          vidcheck;
            try {
              experience = {
                controlGroups: [],
                experiments: []
              };
              data = [];
              goals = [];
              elements = results.loadPageElements;
              MonoloopProfile = results.loadProfile.MonoloopProfile;
              baseFunctions = helper.getElementsBasicFunctions();
              currentProfile = MonoloopProfile.getRecentDeviceLVPorfile();
              if(currentProfile === undefined){
                return callback("Current profile not exists", null);
              }
              currentProfile.controlgroups = currentProfile.controlgroups || {};
              currentProfile.experiments = currentProfile.experiments || {};
              MonoloopAssets = results.loadProfile.Asset;
              dts = new Date;
              ts = new Date(dts.getFullYear(), dts.getMonth(), dts.getDate(), dts.getHours(), 0, 0).getTime() / 1000;
              segment_served = false;
              Segments = [];

              let pass_experiences = invoke_experience.isPassSegment(elements,{
                MonoloopProfile: MonoloopProfile
              });
              debug('Pass experiences');

              if(pass_experiences.length === 0){
                return callback(null,{
                  experience: experience,
                  data: data
                });
              }

              if(elements && elements.length){
                currentProfile = results.loadProfile.MonoloopProfile.getRecentDeviceLVPorfile();
                currentProfile.controlgroups = currentProfile.controlgroups || {};
                currentProfile.experiments = currentProfile.experiments || {};
                controlGroups = Object.keys(currentProfile.controlgroups);
                experimentGroups = Object.keys(currentProfile.experiments);
                results.loadProfile.MonoloopProfile.lastVisit.experiments = results.loadProfile.MonoloopProfile.lastVisit.experiments || {};
                vid = request.query.vid;

                for(let i = 0; i < elements.length; i++){
                  let element = elements[i];
                  if(element.Experiment && element.Experiment.length){
                    for(let j = 0; j < element.Experiment.length; j++){
                      let exp = element.Experiment[j];
                      debug(exp);

                      let exp_id = exp.experimentID;
                      if(exp.hidden === 1){
                        continue;
                      }
                      if(experience.controlGroups.indexOf(exp_id) !== -1 || experience.experiments.indexOf(exp_id) !== -1){
                        continue;
                      }

                      debug('control group');
                      debug(controlGroups);

                      debug('experiment group');
                      debug(experimentGroups);

                      // Randon new value
                      r = Math.round(Math.random() * 100);
                      one_day = 1000 * 60 * 60 * 24;
                      date1_ms = new Date(exp.created_at).getTime();
                      date2_ms = new Date().getTime();
                      difference_ms = date2_ms - date1_ms;
                      experimentDays = Math.round(difference_ms / one_day);


                      if (controlGroups.indexOf(exp.experimentID.toString()) !== -1 && experimentGroups.indexOf(exp.experimentID.toString()) !== -1) {
                        // Found in both control group & experiment group
                        if ((currentProfile.controlgroups[exp.experimentID].timestamp != null) && (currentProfile.experiments[exp.experimentID].timestamp != null)) {
                          if (currentProfile.controlgroups[exp.experimentID].timestamp > currentProfile.experiments[exp.experimentID].timestamp) {
                            experience.controlGroups.push(exp.experimentID);
                            delete currentProfile.experiments[exp.experimentID];
                          } else {
                            experience.experiments.push(exp.experimentID);
                            delete currentProfile.controlgroups[exp.experimentID];
                          }
                        }
                      }else if (controlGroups.indexOf(exp.experimentID.toString()) !== -1) {
                        // Found in controlgroup
                        // IF timeout reset
                        if (experimentDays > exp.controlGroupDays) {
                          delete currentProfile.controlgroups[exp.experimentID];
                        }else{
                          experience.controlGroups.push(exp.experimentID);
                        }
                      }else if (experimentGroups.indexOf(exp.experimentID.toString()) !== -1) {
                        // Found in experiment group
                        experience.experiments.push(exp.experimentID);
                      }

                      if (controlGroups.indexOf(exp.experimentID.toString()) === -1 && experimentGroups.indexOf(exp.experimentID.toString()) === -1) {
                        // If not found on both group assign new one.
                        debug('not found on both group');

                        if (r < exp.controlGroupSize){
                          experience.controlGroups.push(exp.experimentID);
                        }else{
                          experience.experiments.push(exp.experimentID);
                        }
                      }

                      if(experience.controlGroups.indexOf(exp.experimentID) !== -1){
                        invoke_experience.addVisitorToProfileControlGroup(currentProfile, exp.experimentID, vid);
                      }else{

                        // Set up experience data on visit level
                        if (results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] === void 0) {
                          invoke_experience.addVisitorToProfileExpGroup(currentProfile, exp.experimentID, vid);
                          results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] = {
                            goal: exp.goalID,
                            isPartOfExperiment: false,
                            views: 0
                          };
                        }
                      }
                    }
                  }
                }
              }
            } catch (error) {
              e = error;
              logger.printError("batch->assignExperienceGroup: ",e);
              return callback(e,null);
            }
            return callback(null,{
              experience: experience,
              data: data
            });
          }
        ],
        buildContent: [
          'loadAccountData', 'loadPageElements', 'loadProfile', 'assignExperienceGroup', function(results,callback) {
            invoke_page_element.buildContent(callback, results, request);
          }
        ],
        'invokeSegment': [
          'loadProfile','loadAccountData',function(results, callback){
            invoke_event.processSegments(results.loadProfile.MonoloopProfile);
            callback(null,null);
          }
        ],
        'invokeExperience': [
          'loadProfile','loadAccountData',function(results, callback){
            invoke_event.processExperience(results.loadProfile.MonoloopProfile);
            callback(null,null);
          }
        ],
        //callback null, {data:data.data,goals:data.goals}
        updateProfile: [
          'loadProfile',
          'buildContent',
          'getOCRStatus',
          'invokeSegment',
          function(results,
          callback) {
            invoke_profile.updateProfile(results,callback,request);
          }
        ]
      }, function(err, results) {
        var gmid, mid, out, transport, vid;
        if (err) {
          out = {
            success: false,
            message: 'Error occured',
            err: err
          };
          response.writeHead(502, {
            'Content-type': 'text/javascript; charset=utf-8'
          });
          return response.end('MONOloop.MonoloopData=' + JSON.stringify(out));
        } else {
          if (request.query.mid && request.query.gmid) {
            gmid = request.query.gmid;
            mid = request.query.mid;
          } else {
            gmid = results.loadProfile.MonoloopProfile._id;
            mid = request.query.profileID;
          }
          if (request.query.vid && request.query.gmid && request.query.mid) {
            vid = request.query.vid;
          } else {
            vid = results.loadProfile.VisitID;
          }
          transport = {
            gmid: gmid,
            mid: mid,
            signals: results.buildContent.signals
          };

          publisher.rpush('profilelist', JSON.stringify(transport));

          out = {
            success: true,
            eval: 'localStorage.setItem("gmid","' + gmid + '"); localStorage.setItem("mid","' + mid + '"); localStorage.setItem("skey","' + sha512.createHash('sha512').update(gmid + cryptSalt).digest('hex') + '");',
            data: results.buildContent.data,
            events: invoke_event.events,
            debug: results.buildContent.debug
          };
          response.writeHead(200, {
            'Content-type': 'text/javascript; charset=utf-8'
          });
          return response.end('MONOloop.MonoloopData=' + JSON.stringify(out));
        }
      });
    }
  };

  getHighPriorityExperiment = function(exps, contents) {
    var exp, expArray, filteredExps, getMappedTo, highPriority, i, j, key, len, len1, mappedTo, mappedToKeys, temp, value;
    filteredExps = [];
    mappedToKeys = {}; // {"mappedTo1":[ex_obj1, exp_obj2], "mappedTo2":[exp_obj3]}
    highPriority = 10;
    getMappedTo = function(exp, conts) {
      var content, i, len, mappedTo;
      mappedTo = "";
      if (conts && conts.length > 0) {
        for (i = 0, len = conts.length; i < len; i++) {
          content = conts[i];
          if (content && content.experimentID && content.experimentID === exp.experimentID) {
            if (content.mappedTo) {
              mappedTo = content.mappedTo;
            }
            break;
          }
        }
      }
      return mappedTo;
    };
    for (i = 0, len = exps.length; i < len; i++) {
      exp = exps[i];
      mappedTo = getMappedTo(exp, contents);
      if (mappedTo === "") {
        filteredExps.push(exp);
      } else {
        if (mappedToKeys[mappedTo]) {
          mappedToKeys[mappedTo].push(exp);
        } else {
          mappedToKeys[mappedTo] = [exp];
        }
      }
    }
    for (key in mappedToKeys) {
      value = mappedToKeys[key];
      expArray = mappedToKeys[key];
      if (expArray.length === 1) { // dont check for priorities
        filteredExps.push(expArray[0]);
      } else {
        for (j = 0, len1 = expArray.length; j < len1; j++) {
          exp = expArray[j];
          if (exp.priority && exp.priority < highPriority) {
            highPriority = exp.priority;
            temp = exp;
          }
        }
        if (temp) {
          filteredExps.push(temp);
        }
      }
    }
    if (!filteredExps || filteredExps.length === 0) {
      return exps;
    } else {
      return filteredExps;
    }
  };


}).call(this);