(function() {
  "use strict";
  var ProfileModel, assetCtrl, async, cachedLayer, controller, cryptSalt, ejs, elementCtrl, evaluateAudienceCondition, evaluateExperiments, helper, logger, ocrConfig, profileCtrl, publisher, redis, sha512, util, debug;

  ProfileModel = require('./../model/profile');

  sha512 = require('crypto');

  async = require('async');

  elementCtrl = require('./elements');

  profileCtrl = require('./profiles');

  assetCtrl = require('./assets');

  util = require('util');

  cachedLayer = require('./../utils/cachingLayer');

  redis = require('redis');

  helper = require('./../utils/helper');

  logger = require('./../utils/logger');

  publisher = require('./../../redis-conn');

  ocrConfig = require('./../model/ocrConfig');

  cryptSalt = '130293+109k21jl1c31c9312i3c910202';

  ejs = require('ejs');

  debug = require('debug')('ml:external');

  controller = module.exports = {
    process: function(request, response) {
      return async.auto({
        loadPageElements: [
          function(callback) {
            var req;
            debug("inside loadPageElements");
            request.query.url = 'http://mon-experiences-' + request.query.cid + '/';
            req = {
              request: request.query,
              response: response,
              typeOfKey: 'pageelements'
            };
            return cachedLayer.invokeCaching(req,
          function(err,
          result) {
              if (err) {
                return callback(err,
          null);
              } else {
                return callback(null,
          result);
              }
            });
          }
        ],
        detectProfileStatus: [
          function(callback) {
            var decideProfile,
          query,
          resultSet;
            debug("inside detectProfileStatus");
            decideProfile = null;
            query = {};
            resultSet = {};
            resultSet['profileType'] = "newProfile";
            resultSet['result'] = [];
            //Set default values
            if (request.query.mid === void 0) {
              request.query.mid = '';
            }
            if (request.query.gmid === void 0) {
              request.query.gmid = '';
            }
            if (request.query.uniqueID === void 0) {
              request.query.uniqueID = '';
            }
            if (helper.isNull(request.query.gmid) && request.query.mid) {
              query = {
                _id: request.query.mid
              };
              decideProfile = "oldProfile";
            }
            if (request.query.uniqueID && helper.isNull(request.query.gmid)) {
              query = {
                uniqueID: request.query.uniqueID
              };
              decideProfile = "newProfilePush";
            }
            if (request.query.gmid && helper.isNull(request.query.mid)) {
              query = {
                _id: request.query.gmid
              };
            }
            if (request.query.uniqueID) {
              query.uniqueID = request.query.uniqueID;
            }
            decideProfile = "newProfilePush";
            if (helper.isNull(request.query.gmid) && helper.isNull(request.query.mid)) {
              decideProfile = "newProfile";
            }
            if (request.query.gmid && request.query.mid) {
              query = {
                _id: request.query.gmid
              };
            }
            decideProfile = "dontPush";
            if (Object.keys(query).length > 0) {
              return profileCtrl.get(request,
          response,
          query,
          function(err,
          result) {
                if (err) {
                  return callback(err);
                }
                if (result && result.length > 0) {
                  if (result[0].privacy && result[0].privacy.lastProfileDelete === request.query.mid) {
                    decideProfile = "reInsertProfile";
                  }
                  resultSet['profileType'] = decideProfile;
                  resultSet['result'] = result;
                  return callback(null,
          resultSet);
                } else {
                  request.query.vid = !helper.isNull(request.query.vid) ? "" : void 0;
                  request.query.mid = !helper.isNull(request.query.mid) ? "" : void 0;
                  request.query.gmid = !helper.isNull(request.query.gmid) ? "" : void 0;
                  request.query.skey = !helper.isNull(request.query.skey) ? "" : void 0;
                  return callback(null,
          resultSet);
                }
              });
            } else {
              return callback(null,
          resultSet);
            }
          }
        ],
        getOCRStatus: [
          function(callback) {
            var req;
            debug("inside getOCRStatus");
            req = {
              cid: request.query.cid,
              typeOfKey: 'ocr'
            };
            return cachedLayer.invokeCaching(req,
          function(err,
          result) {
              if (err) {
                return callback(err,
          null);
              } else {
                return callback(null,
          result);
              }
            });
          }
        ],
        loadProfile: [
          'loadPageElements',
          'detectProfileStatus',
          'getOCRStatus',
          function(results,
          callback) {
            var MonoloopProfile,
          ProfileLoaded,
          VisitID,
          connector,
          content,
          element,
          gmid,
          hasToLoadAssets,
          hasToLoadProfile,
          i,
          j,
          k,
          l,
          len,
          len1,
          len2,
          len3,
          profile,
          profileID,
          ref,
          ref1,
          ref2,
          ref3,
          resultSet;
            debug("inside loadProfile");
            ProfileLoaded = false;
            hasToLoadProfile = false;
            hasToLoadAssets = [];
            profileID = "";
            MonoloopProfile = new ProfileModel();
            if (results.loadPageElements) {
              ref = results.loadPageElements;
              for (i = 0, len = ref.length; i < len; i++) {
                element = ref[i];
                if (element.content !== void 0) {
                  ref1 = element.content;
                  for (j = 0, len1 = ref1.length; j < len1; j++) {
                    content = ref1[j];
                    if (!element.content) {
                      continue;
                    }
                    ref2 = content.connectors;
                    for (k = 0, len2 = ref2.length; k < len2; k++) {
                      connector = ref2[k];
                      if (content.connectors) {
                        if (connector === 'MonoloopProfile') {
                          hasToLoadProfile = true;
                        }
                      }
                    }
                    if (content.asset) {
                      hasToLoadAssets.push(content.asset);
                    }
                  }
                }
              }
            }
            //load or create profiles according to the request
            resultSet = results.detectProfileStatus;
            if (hasToLoadProfile) {
              gmid = false;
              ProfileLoaded = true;
            }
            if (resultSet.result.length > 0) {
              ref3 = resultSet.result;
              for (l = 0, len3 = ref3.length; l < len3; l++) {
                profile = ref3[l];
                if (profile instanceof ProfileModel && parseInt(profile.customerID,
          10 === parseInt(request.query.cid,
          10))) {
                  if (profile._id.toString() === request.query.gmid || profile.uniqueID === request.query.uniqueID) {
                    MonoloopProfile = profile;
                    MonoloopProfile.recently_used_device = request.query.mid;
                  }
                  if (results.getOCRStatus !== null || results.getOCRStatus !== void 0) {
                    MonoloopProfile.OCR = results.getOCRStatus;
                  }
                }
              }
            }
            if (resultSet.profileType === "newProfile" || resultSet.profileType === "newProfilePush" || resultSet.profileType === "reInsertProfile") {
              profileID = MonoloopProfile.addProfile(request);
            }
            if (results.getOCRStatus !== null || results.getOCRStatus !== void 0) {
              MonoloopProfile.OCR = results.getOCRStatus;
            }
            if (profileID && resultSet.profileType === "newProfile" || resultSet.profileType === "newProfilePush") {
              request.query.profileID = profileID;
              request.query.mid = '';
            }
            if (helper.isNull(request.query.vid) || resultSet.profileType === "reInsertProfile") {
              VisitID = MonoloopProfile.addVisit(request);
            } else {

            }
            if (request.query.vid && request.query.gmid && request.query.vid.match(/^[0-9a-fA-F]{24}$/) && request.query.gmid.match(/^[0-9a-fA-F]{24}$/)) {
              MonoloopProfile.addPageView(request);
              VisitID = request.query.vid;
            } else {
              VisitID = MonoloopProfile.addVisit(request);
            }
            return callback(null,
          {
              MonoloopProfile: MonoloopProfile,
              VisitID: VisitID,
              ProfileLoaded: ProfileLoaded,
              Asset: hasToLoadAssets,
              ProfileType: resultSet.profileType
            });
          }
        ],
        evaluateCondition: [
          'loadPageElements',
          'loadProfile',
          function(results,
          callback) {
            var MonoloopAssets,
          MonoloopProfile,
          Segments,
          baseFunctions,
          bodyContent,
          codeToEvaluate,
          content_out,
          currentProfile,
          data,
          e,
          elements,
          err,
          exp,
          goals,
          i,
          isConditionPassed,
          isControlGroup,
          item,
          item_out,
          j,
          len,
          len1,
          out,
          ref,
          result;
            debug("inside evaluateCondition");
            out = void 0;
            data = [];
            try {
              goals = [];
              elements = results.loadPageElements;
              MonoloopProfile = results.loadProfile.MonoloopProfile;
              baseFunctions = helper.getElementsBasicFunctions();
              currentProfile = MonoloopProfile.getRecentDeviceLVPorfile();
              MonoloopAssets = results.loadProfile.Asset;
              Segments = [];
              if (request.body && request.body !== '' && request.body.content) {
                bodyContent = request.body.content;
                for (i = 0, len = bodyContent.length; i < len; i++) {
                  item = bodyContent[i];
                  result = false;
                  item_out = void 0;
                  if (item.experiment_ids && item.experiment_ids.length > 0) {
                    ref = item.experiment_ids;
                    for (j = 0, len1 = ref.length; j < len1; j++) {
                      exp = ref[j];
                      isConditionPassed = evaluateAudienceCondition(request,
          results,
          exp);
                      if (isConditionPassed) {
                        isControlGroup = evaluateExperiments(request,
          results,
          exp);
                        if (!isControlGroup) {
                          try {
                            codeToEvaluate = '';
                            if (!item.condition.includes('<%')) {
                              codeToEvaluate = '<% ' + item.condition.replace('{|}',
          '{ %>Pass<% } %>').replace('{ | }',
          '{ %>Pass<% } %>');
                            } else {
                              codeToEvaluate = item.condition;
                            }
                            content_out = ejs.render(baseFunctions + codeToEvaluate,
          {
                              MonoloopProfile: MonoloopProfile,
                              goals,
                              Segments,
                              MonoloopAssets
                            });
                          } catch (error) {
                            err = error;
                            logger.printError("external->evaluateCondition(ContentID: " + item.uid + " / ProfileID: " + currentProfile.id + ")",
          err);
                            content_out = '';
                          }
                          if (content_out.replace(/^\s+|\s+$/g,
          "") !== '') {
                            result = true;
                          }
                        }
                      }
                    }
                    item_out = {
                      uid: item.uid,
                      condition: result
                    };
                    data.push(item_out);
                  } else {
                    try {
                      codeToEvaluate = '';
                      if (!item.condition.includes('<%')) {
                        codeToEvaluate = '<% ' + item.condition.replace('{|}',
          '{ %>Pass<% } %>').replace('{ | }',
          '{ %>Pass<% } %>');
                      } else {
                        codeToEvaluate = item.condition;
                      }
                      content_out = ejs.render(baseFunctions + codeToEvaluate,
          {
                        MonoloopProfile: MonoloopProfile,
                        goals,
                        Segments,
                        MonoloopAssets
                      });
                    } catch (error) {
                      err = error;
                      logger.printError("elements->buildContent(ContentID" + item.uid + " / ProfileID:" + currentProfile.id + ")",
          err);
                      content_out = '';
                    }
                    result = false;
                    if (content_out === 'Pass') {
                      result = true;
                    }
                    item_out = {
                      uid: item.uid,
                      condition: result
                    };
                    data.push(item_out);
                  }
                }
              }
              out = {
                success: true,
                data: data
              };
            } catch (error) {
              e = error;
              logger.printError("external->evaluateExperiments: ",
          e);
              out = {
                success: false,
                data: []
              };
            }
            return response.json(out);
          }
        ]
      });
    }
  };

  // getSucessAudienceExperience = (results, experiences) ->
  //     pass_experiences = []
  //     baseFunctions = helper.getElementsBasicFunctions()
  //     for experience_id in experiences
  //         for page_element in results.loadPageElements
  //             for content in page_element.content
  //                 if content.experimentID is experience_id and experience_id not in pass_experiences
  //                     # process audience condition
  //                     try
  //                         segment_code = '';
  //                         content = page_element.content[0]
  //                         if content.segments isnt undefined && content.segments isnt null && (content.segments.length > 1 || content.segments.length is undefined)
  //                             for se of content.segments
  //                                 segment_code += content.segments[se]

  //                         content_out = ejs.render(baseFunctions+segment_code+page_element.content[0].code, { MonoloopProfile: results.loadProfile.MonoloopProfile})
  //                     catch err
  //                         content_out = ''

  //                     if content_out.replace(/^\s+|\s+$/g, "") isnt ''
  //                         pass_experiences.push experience_id
  //     return pass_experiences
  evaluateAudienceCondition = function(request, results, expID) {
    var Element, MonoloopAssets, MonoloopProfile, Segments, baseFunctions, codeToEvaluate, content, content_out, currentProfile, e, elements, err, goals, i, isConditionPassed, j, len, len1, ref;
    isConditionPassed = false;
    goals = [];
    elements = results.loadPageElements;
    MonoloopProfile = results.loadProfile.MonoloopProfile;
    baseFunctions = helper.getElementsBasicFunctions();
    currentProfile = MonoloopProfile.getRecentDeviceLVPorfile();
    MonoloopAssets = results.loadProfile.Asset;
    Segments = [];
    try {
      if (elements !== void 0) {
        for (i = 0, len = elements.length; i < len; i++) {
          Element = elements[i];
          if (Element.content !== void 0) {
            ref = Element.content;
            for (j = 0, len1 = ref.length; j < len1; j++) {
              content = ref[j];
              if (content.experimentID !== void 0 && content.experimentID === expID) {
                try {
                  codeToEvaluate = '';
                  if (!content.code.includes('<%')) {
                    codeToEvaluate = '<% ' + content.code.replace('{|}', '{ %>Pass<% } %>').replace('{ | }', '{ %>Pass<% } %>');
                  } else {
                    codeToEvaluate = content.code;
                  }
                  content_out = ejs.render(baseFunctions + codeToEvaluate, {
                    MonoloopProfile: MonoloopProfile,
                    goals,
                    Segments,
                    MonoloopAssets
                  });
                } catch (error) {
                  err = error;
                  logger.printError("external->evaluateAudienceCondition(ContentID: " + uid + " / ProfileID: " + currentProfile.id + ")", err);
                  content_out = '';
                }
                if (content_out.replace(/^\s+|\s+$/g, "") !== '') {
                  isConditionPassed = true;
                }
                break;
              }
            }
          }
        }
      }
    } catch (error) {
      e = error;
      logger.printError("external->evaluateAudienceCondition: ", e);
    }
    return isConditionPassed;
  };

  evaluateExperiments = function(request, results, expID) {
    var controlGroups, currentProfile, e, experience, experimentGroups, experiments, firstview, isControlGroup, vid, vidcheck;
    isControlGroup = false;
    experience = {
      controlGroups: [],
      experiments: []
    };
    try {
      currentProfile = results.loadProfile.MonoloopProfile.getRecentDeviceLVPorfile();
      vid = request.query.vid;
      if (typeof vid === 'string') {
        vidcheck = vid.match(/^[0-9a-fA-F]{24}$/);
      } else {
        vidcheck = null;
      }
      firstview = currentProfile.current.views.length === 1;
      currentProfile.controlgroups = currentProfile.controlgroups || {};
      currentProfile.experiments = currentProfile.experiments || {};
      controlGroups = Object.keys(currentProfile.controlgroups);
      experimentGroups = Object.keys(currentProfile.experiments);
      results.loadProfile.MonoloopProfile.lastVisit.experiments = results.loadProfile.MonoloopProfile.lastVisit.experiments || {};
      experiments = results.loadPageElements.filter(function(Element) {
        var date1_ms, date2_ms, difference_ms, exp, expHidden, experimentDays, i, idxInCG, idxInEx, len, one_day, r, ref, results1;
        if (Element.Experiment) {
          ref = Element.Experiment;
          results1 = [];
          for (i = 0, len = ref.length; i < len; i++) {
            exp = ref[i];
            if (exp.experimentID === expID) {
              expHidden = false;
              if (exp.hidden !== void 0) {
                if (exp.hidden === 1) {
                  expHidden = true;
                }
              }
              if (!expHidden) {
                one_day = 1000 * 60 * 60 * 24;
                date1_ms = new Date(exp.created_at).getTime();
                date2_ms = new Date().getTime();
                difference_ms = date2_ms - date1_ms;
                experimentDays = Math.round(difference_ms / one_day);
                r = Math.round(Math.random() * 100);
                if (vid) {
                  if (controlGroups.indexOf(exp.experimentID.toString()) !== -1 && experimentGroups.indexOf(exp.experimentID.toString()) !== -1) {
                    if ((currentProfile.controlgroups[exp.experimentID].timestamp != null) && (currentProfile.experiments[exp.experimentID].timestamp != null)) {
                      if (currentProfile.controlgroups[exp.experimentID].timestamp > currentProfile.experiments[exp.experimentID].timestamp) {
                        experience.controlGroups.push(exp.experimentID);
                      } else {
                        experience.experiments.push(exp.experimentID);
                      }
                    }
                  } else if (controlGroups.indexOf(exp.experimentID.toString()) !== -1) {
                    experience.controlGroups.push(exp.experimentID);
                  } else if (experimentGroups.indexOf(exp.experimentID.toString()) !== -1) {
                    experience.experiments.push(exp.experimentID);
                  }
                  if (controlGroups.indexOf(exp.experimentID.toString()) === -1 && experimentGroups.indexOf(exp.experimentID.toString()) === -1) {
                    //a new experiment is a visit, calculate its occurance
                    if (r < exp.controlGroupSize && (experimentDays < exp.controlGroupDays)) {
                      experience.controlGroups.push(exp.experimentID);
                      if (currentProfile.controlgroups[exp.experimentID] != null) {
                        currentProfile.controlgroups[exp.experimentID].visitorCount++;
                        currentProfile.controlgroups[exp.experimentID].timestamp = new Date().getTime() / 1000;
                      } else {
                        currentProfile.controlgroups[exp.experimentID] = {
                          visitorCount: 1,
                          timestamp: new Date().getTime() / 1000
                        };
                      }
                    } else {
                      experience.experiments.push(exp.experimentID);
                      if (currentProfile.experiments[exp.experimentID] != null) {
                        currentProfile.experiments[exp.experimentID].visitorCount++;
                        currentProfile.experiments[exp.experimentID].timestamp = new Date().getTime() / 1000;
                      } else {
                        currentProfile.experiments[exp.experimentID] = {
                          visitorCount: 1,
                          timestamp: new Date().getTime() / 1000
                        };
                      }
                    }
                  }
                  if (results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] === void 0) {
                    results1.push(results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] = {
                      goal: exp.goalID,
                      isPartOfExperiment: false,
                      views: 0
                    });
                  } else {
                    results1.push(void 0);
                  }
                } else {
                  idxInCG = controlGroups.indexOf(exp.experimentID.toString());
                  idxInEx = experimentGroups.indexOf(exp.experimentID.toString());
                  if (experimentDays < exp.controlGroupDays) {
                    if (r < exp.controlGroupSize) {
                      experience.controlGroups.push(exp.experimentID);
                      if (currentProfile.controlgroups[exp.experimentID] === void 0) {
                        currentProfile.controlgroups[exp.experimentID] = {
                          visitorCount: 1,
                          timestamp: new Date().getTime() / 1000
                        };
                      } else {
                        currentProfile.controlgroups[exp.experimentID].timestamp = new Date().getTime() / 1000;
                      }
                      if (results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] === void 0) {
                        results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] = {
                          goal: exp.goalID,
                          isPartOfExperiment: false,
                          views: 0
                        };
                      }
                      if (idxInEx !== -1) {
                        results1.push(delete currentProfile.experiments[experimentGroups[idxInEx]]);
                      } else {
                        results1.push(void 0);
                      }
                    } else {
                      experience.experiments.push(exp.experimentID);
                      if (results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] === void 0) {
                        results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] = {
                          goal: exp.goalID,
                          isPartOfExperiment: false,
                          views: 0
                        };
                      }
                      if (currentProfile.experiments[exp.experimentID] === void 0) {
                        currentProfile.experiments[exp.experimentID] = {
                          visitorCount: 1,
                          timestamp: new Date().getTime() / 1000
                        };
                      } else {
                        currentProfile.experiments[exp.experimentID].timestamp = new Date().getTime() / 1000;
                      }
                      if (idxInCG !== -1) {
                        results1.push(delete currentProfile.controlgroups[controlGroups[idxInCG]]);
                      } else {
                        results1.push(void 0);
                      }
                    }
                  } else {
                    if (controlGroups.indexOf(exp.experimentID.toString()) !== -1 && experimentGroups.indexOf(exp.experimentID.toString()) !== -1) {
                      if ((currentProfile.controlgroups[exp.experimentID].timestamp != null) && (currentProfile.experiments[exp.experimentID].timestamp != null)) {
                        if (currentProfile.controlgroups[exp.experimentID].timestamp > currentProfile.experiments[exp.experimentID].timestamp) {
                          experience.controlGroups.push(exp.experimentID);
                        } else {
                          experience.experiments.push(exp.experimentID);
                        }
                      }
                    } else if (controlGroups.indexOf(exp.experimentID.toString()) !== -1) {
                      experience.controlGroups.push(exp.experimentID);
                    } else if (experimentGroups.indexOf(exp.experimentID.toString()) !== -1) {
                      experience.experiments.push(exp.experimentID);
                    } else {
                      experience.experiments.push(exp.experimentID);
                      currentProfile.experiments[exp.experimentID] = {
                        visitorCount: 1,
                        timestamp: new Date().getTime() / 1000
                      };
                    }
                    if (results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] === void 0) {
                      results1.push(results.loadProfile.MonoloopProfile.lastVisit.experiments[exp.experimentID] = {
                        goal: exp.goalID,
                        isPartOfExperiment: false,
                        views: 0
                      });
                    } else {
                      results1.push(void 0);
                    }
                  }
                }
              } else {
                results1.push(void 0);
              }
            } else {
              results1.push(void 0);
            }
          }
          return results1;
        }
      });
    } catch (error) {
      e = error;
      logger.printError("external->evaluateExperiments: ", e);
    }
    if (experience.controlGroups.length > 0) {
      isControlGroup = true;
    }
    return isControlGroup;
  };

}).call(this);
