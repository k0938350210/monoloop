(function() {
  // Code by: Affan Toor
  var ProfileModel, aggregatedModel, async, controller, elementCtrl, elementModel, logger, mongo, publisher, util;

  elementModel = require('./../model/element');

  elementCtrl = require('./elements');

  ProfileModel = require('./../model/profile');

  util = require('./../utils/helper');

  logger = require('./../utils/logger');

  aggregatedModel = require('./../model/ReportingArchiveAggregated');

  mongo = require('mongodb');

  async = require('async');

  publisher = require('../../redis-sentinel-conn');

  controller = module.exports = {
    getExperimentFromReportingAggregated: function(query, cb) {
      return aggregatedModel.find(query).lean().exec(function(err, result) {
        logger.print("getExperimentFromReportingAggregated");
        logger.print(result);
        if (err) {
          cb(err);
        }
        if (result && result.length > 0) {
          return cb(null, result); //[0].experiments
        } else {
          return cb(null, null);
        }
      });
    },
    getExperimentLastUpdatedTimestamp: function(expID, customerID, cb) {
      var query;
      if (expID !== void 0 && expID !== '') {
        query = {};
        query['cid'] = parseInt(customerID);
        //query['sort'] = '_id':-1
        query['Experiment'] = {
          '$exists': true
        };
        return elementModel.find(query).lean().exec(function(error, elements) {
          var elem, expObj, i, j, len, len1, ref, updatedDate;
          if (error) {
            cb(null, '');
          }
          if (elements && elements.length > 0) {
            for (i = 0, len = elements.length; i < len; i++) {
              elem = elements[i];
              updatedDate = '';
              ref = elem.Experiment;
              for (j = 0, len1 = ref.length; j < len1; j++) {
                expObj = ref[j];
                if (expID === expObj.experimentID) {
                  updatedDate = expObj.updated_at;
                  break;
                }
              }
              if (updatedDate !== '') {
                break;
              }
            }
            return cb(null, updatedDate);
          } else {
            return cb(null, '');
          }
        });
      } else {
        return cb(null, '');
      }
    },
    updatePValue: function(expObj, date, cb) {
      var asyncTasks, currentDate, findQuery, updateQuery;
      // returned object:
      // expObj = {
      //       experimentID: experimentID,
      //       customerID: cid,
      //       visitors: visitors,
      //       direct: direct,
      //       cg_visitors: cg_visitors,
      //       cg_direct: cg_direct,
      //       pValue: pValue
      // }
      asyncTasks = [];
      findQuery = {};
      updateQuery = {};
      currentDate = new Date();
      findQuery['Experiment'] = {
        '$exists': true
      };
      findQuery['cid'] = parseInt(expObj.customerID);
      findQuery['Experiment'] = {
        '$elemMatch': {
          'experimentID': expObj.experimentID //, 'updated_at': date}
        }
      };
      return elementModel.find(findQuery).lean().exec(function(error, elements) {
        var element, i, len;
        if (error) {
          cb(err, null);
        }
        if (elements && elements.length > 0) {
// getElementFns = elements.map((element) ->
//     (callback) ->
//     ref = element.Experiment
//     exp = undefined
//     i = undefined
//     index = undefined
//     len = undefined
//     #results = undefined
//     #results = []
//     i = 0
//     len = ref.length
//     while i < len
//         exp = ref[i]
//         if expObj.experimentID is exp.experimentID
//             # and exp.updated_at is date
//             findQuery = {}
//             updateQuery['$set'] =
//                 "Experiment.#{i}.pValue": expObj.pValue
//                 "Experiment.#{i}.updated_at": currentDate.toISOString()
//             findQuery['_id'] = element._id
//             elementModel.updateOne findQuery, updateQuery, (err, res) ->
//                 callback null, res
//                 return
//             break
//         #results.push index += 1
//         i++
//     return
// )
          for (i = 0, len = elements.length; i < len; i++) {
            element = elements[i];
            asyncTasks.push(function(cb) {
              var exp, index, j, len1, ref, results1;
              index = 0;
              ref = element.Experiment;
              results1 = [];
              for (j = 0, len1 = ref.length; j < len1; j++) {
                exp = ref[j];
                if (expObj.experimentID === exp.experimentID) {
                  findQuery = {};
                  updateQuery['$set'] = {
                    [`Experiment.${index}.pValue`]: expObj.pValue,
                    [`Experiment.${index}.updated_at`]: currentDate.toISOString()
                  };
                  findQuery['_id'] = element._id;
                  elementModel.updateOne(findQuery, updateQuery, function(err, res) {
                    console.log(err);
                    console.log(res);
                    return cb(null, res);
                  });
                  // if err
                  //     cb err, null
                  // else
                  //     cb null, res
                  break;
                }
                results1.push(index += 1);
              }
              return results1;
            });
          }
          if (asyncTasks) {
            return async.parallel(asyncTasks, function(err, results) {
              publisher.publish("invalidate", expObj.customerID + "_pageelements");
              return cb(null, 'done');
            });
          }
        } else {
          return cb;
        }
      });
    }
  };

}).call(this);
