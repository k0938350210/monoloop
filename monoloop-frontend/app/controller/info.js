(function() {
  var ProfileModel, _, checkType, hardCodedConfigs, publisher, redis;

  ProfileModel = require('./../model/profile');

  redis = require('redis');

  publisher = redis.createClient();

  _ = require('underscore');

  module.exports = {
    setInformation: function(request, response) {
      var payload;
      //if request.body && typeof request.body.MID == "string" && +request.body.infoType == 1 && typeof requst.body.infoID == "number" && (requst.body.infoValue.toLowerCase() == "true" || requst.body.infoValue.toLowerCase() == "false")
      if (request.body && request.query.infoType === '1') {
        console.log(Date() + ' : customerID: ' + request.username + ' setting goal: ' + request.query.infoID + 'with InfoID: ' + request.query.infoType + 'with GMID: ' + request.query.gmid);
        return ProfileModel.findOne({
          customerID: +request.username,
          _id: request.query.gmid
        }, function(error, profiles) {
          var Status, check, goal, goalUID, goals, i, key, len, midFound, payload, profilesProcessed;
          if (profiles) {
            if (request.query.mid) {
              profilesProcessed = 0;
              midFound = false;
              profiles.profiles.forEach(function(singleProfile, index) {
                var Status, check, goal, goalUID, goals, i, key, len;
                profilesProcessed++;
                console.log(singleProfile._id + "   :  " + request.query.mid);
                if (singleProfile._id.toString() === request.query.mid) {
                  midFound = true;
                  goals = singleProfile.goals || [];
                  check = 0;
                  for (key = i = 0, len = goals.length; i < len; key = ++i) {
                    goal = goals[key];
                    console.log(Date() + ' Goals already there: ' + goal.uid);
                    goalUID = '' + goal.uid;
                    Status = '';
                    if (goalUID === request.query.infoID) {
                      check = 1;
                      profiles.profiles[index].goals[key].tstamp = Math.round(new Date().getTime() / 1000);
                      if (profiles.OCR !== void 0) {
                        profiles.profiles[index].goals[key].OCR = profiles.OCR;
                      }
                      profiles.profiles[index].markModified("goals");
                      break;
                    }
                  }
                  if (check !== 1) {
                    profiles.profiles[index].goals.push({
                      'tstamp': Math.round(new Date().getTime() / 1000),
                      'uid': request.query.infoID,
                      'p': 0,
                      'OCR': profiles.OCR !== void 0 ? profiles.OCR : false
                    });
                  }
                  return profiles.save(function(err) {
                    var payload, queueJson;
                    if (err) {
                      console.log(Date() + ' : Error saving information for goal:' + request.query.infoID);
                      payload = JSON.stringify({
                        "Status": "Database error. Please consult Monoloop admin"
                      });
                      response.writeHead(500, {
                        "Content-Type": "application/json",
                        'Content-Length': payload.length
                      });
                      return response.end(payload);
                    } else {
                      console.log(Date() + ' ' + request.query.infoID + ' successfully updated');
                      payload = JSON.stringify({
                        "Status": request.query.infoID + " successfully updated"
                      });
                      queueJson = {
                        gmid: request.query.gmid,
                        cid: request.username
                      };
                      publisher.RPUSH('profilelist', JSON.stringify(queueJson));
                      response.writeHead(200, {
                        "Content-Type": "application/json",
                        'Content-Length': payload.length
                      });
                      return response.end(payload);
                    }
                  });
                }
              });
              if (profilesProcessed === profiles.profiles.length && midFound === false) {
                console.log(Date() + ' : Could not locate Profile with Provided GMID and MID');
                payload = JSON.stringify({
                  "Status": "Failed. Could not locate Specific Profile with Provided GMID and MID"
                });
                response.writeHead(200, {
                  "Content-Type": "application/json",
                  'Content-Length': payload.length
                });
                return response.end(payload);
              }
            } else {
              goals = profiles.profiles[0].goals || [];
              if (error) {
                console.log(Date() + ' : Profile do not exist');
                payload = JSON.stringify({
                  "Status": "Profile do not exist. Please consult Monoloop admin"
                });
                response.writeHead(500, {
                  "Content-Type": "application/json",
                  'Content-Length': payload.length
                });
                return response.end(payload);
              } else {
                check = 0;
                for (key = i = 0, len = goals.length; i < len; key = ++i) {
                  goal = goals[key];
                  console.log(Date() + ' Goals already there: ' + goal.uid);
                  goalUID = '' + goal.uid;
                  Status = '';
                  if (goalUID === request.query.infoID) {
                    check = 1;
                    profiles.profiles[0].goals[key].tstamp = Math.round(new Date().getTime() / 1000);
                    if (profiles.OCR !== void 0) {
                      profiles.profiles[0].goals[key].OCR = profiles.OCR;
                    }
                    profiles.profiles[0].markModified("goals");
                    break;
                  }
                }
                if (check !== 1) {
                  profiles.profiles[0].goals.push({
                    'tstamp': Math.round(new Date().getTime() / 1000),
                    'uid': request.query.infoID,
                    'p': 0,
                    'OCR': profiles.OCR !== void 0 ? profiles.OCR : false
                  });
                }
                return profiles.save(function(err) {
                  var queueJson;
                  if (err) {
                    console.log(Date() + ' : Error saving information for goal:' + request.query.infoID);
                    payload = JSON.stringify({
                      "Status": "Database error. Please consult Monoloop admin"
                    });
                    response.writeHead(500, {
                      "Content-Type": "application/json",
                      'Content-Length': payload.length
                    });
                    return response.end(payload);
                  } else {
                    console.log(Date() + ' ' + request.query.infoID + ' successfully updated');
                    payload = JSON.stringify({
                      "Status": request.query.infoID + " successfully updated"
                    });
                    queueJson = {
                      gmid: request.query.gmid,
                      cid: request.username
                    };
                    publisher.RPUSH('profilelist', JSON.stringify(queueJson));
                    response.writeHead(200, {
                      "Content-Type": "application/json",
                      'Content-Length': payload.length
                    });
                    return response.end(payload);
                  }
                });
              }
            }
          } else {
            console.log(Date() + ' : No Profile Exists');
            payload = JSON.stringify({
              "Status": "Failed. No Profile Exists"
            });
            response.writeHead(200, {
              "Content-Type": "application/json",
              'Content-Length': payload.length
            });
            return response.end(payload);
          }
        });
      } else if (request.body && request.query.infoType === '2') {
        return ProfileModel.findOne({
          customerID: +request.username,
          _id: request.query.gmid
        }, function(error, profiles) {
          var command, currCVs, currCvKey, currCvVal, currProfile, cv, cvd, cvdObj, cvg, e, firstPostCvVal, i, j, len, len1, payload, query, ref, ref1, value;
          try {
            if (profiles) {
              command = {
                $set: {},
                $inc: {}
              };
              try {
                cv = JSON.parse(request.query.infoID);
              } catch (error1) {
                e = error1;
                console.log(Date() + ' : Invalid JSON', e);
                payload = JSON.stringify({
                  "Status": "Invalid Format. Please send valid JSON"
                });
                response.writeHead(500, {
                  "Content-Type": "application/json",
                  'Content-Length': payload.length
                });
                response.end(payload);
                return;
              }
              currProfile = profiles;
              currCVs = currProfile.cv;
              cvdObj = {};
              cvg = {};
              //for cv of cv
              if (cv.g && cv.d !== void 0) {
                ref = cv.d;
                for (i = 0, len = ref.length; i < len; i++) {
                  cvd = ref[i];
                  //get first it use for all type but 'push'
                  if (cvd.n instanceof Array) {
                    firstPostCvVal = cvd.n[0];
                  } else {
                    firstPostCvVal = cvd.n;
                  }
                  currCvVal = currCVs[cv.g];
                  if (currCvVal !== void 0) {
                    currCvVal = currCvVal[cvd.g];
                    currCvKey = cv.g.toString() + '.' + cvd.g.toString();
                    if (cvd.s === hardCodedConfigs().saveType['push']) {
                      if (!(currCvVal instanceof Array)) {
                        currCvVal = [currCvVal];
                      }
                      if (!(cvd.n instanceof Array)) {
                        cvd.n = [cvd.n];
                      }
                      ref1 = cvd.n;
                      for (j = 0, len1 = ref1.length; j < len1; j++) {
                        value = ref1[j];
                        if (checkType(cvd.t, value)) {
                          if (cvd.t === hardCodedConfigs().dataType['date']) {
                            value = new Date(value);
                          }
                          currCvVal.push(value);
                        }
                      }
                      command.$set['cv.' + currCvKey] = currCvVal;
                    } else {
                      if (checkType(cvd.t, firstPostCvVal)) {
                        // override
                        if (cvd.s === hardCodedConfigs().saveType['override']) {
                          if (cvd.t === hardCodedConfigs().dataType['date']) {
                            firstPostCvVal = new Date(firstPostCvVal);
                          }
                          command.$set['cv.' + currCvKey] = firstPostCvVal;
                        // add
                        } else if (cvd.s === hardCodedConfigs().saveType['add']) {
                          if (cvd.t === hardCodedConfigs().dataType['number']) {
                            command.$inc['cv.' + currCvKey] = firstPostCvVal;
                          }
                          if (cvd.t === hardCodedConfigs().dataType['boolean']) {
                            command.$set['cv.' + currCvKey] = currCvVal || firstPostCvVal;
                          }
                        // subtract
                        } else if (cvd.s === hardCodedConfigs().saveType['subtract']) {
                          if (cvd.t === hardCodedConfigs().dataType['number']) {
                            command.$inc['cv.' + currCvKey + '.1'] = firstPostCvVal * (-1);
                          }
                        }
                      }
                    }
                    command.$set['cv.' + cv.g.toString() + '.updated_at'] = Math.round(new Date().getTime() / 1000);
                  } else {
                    //add new to DB if CV not exist
                    if (checkType(cvd.t, firstPostCvVal)) {
                      if (cvd.t === hardCodedConfigs().dataType['date']) {
                        firstPostCvVal = new Date(firstPostCvVal);
                      }
                      cvdObj[cvd.g.toString()] = firstPostCvVal;
                      cvdObj['created_at'] = Math.round(new Date().getTime() / 1000);
                      command.$set['cv.' + cv.g.toString()] = cvdObj;
                    }
                  }
                }
              }
              query = {
                customerID: request.username,
                '_id': request.query.gmid
              };
              Object.keys(command).forEach(function(cmdKey, value) {
                if (Object.keys(command[cmdKey]).length < 1) {
                  return delete command[cmdKey];
                }
              });
              return ProfileModel.updateOne(query, command, {}, function(err, updated) {
                var queueJson;
                if (err) {
                  console.log(Date() + ' : Error saving information for Custom Variables:' + request.query.infoID);
                  payload = JSON.stringify({
                    "Status": "Database error. Please consult Monoloop admin"
                  });
                  response.writeHead(500, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                } else {
                  console.log(Date() + ' CV.g:' + cv.g + ' successfully updated');
                  payload = JSON.stringify({
                    "Status ": "CV.g:" + cv.g + " successfully updated"
                  });
                  queueJson = {
                    gmid: request.query.gmid,
                    cid: request.username
                  };
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                }
              });
            } else {
              console.log(Date() + ' : No Profile Exists');
              payload = JSON.stringify({
                "Status": "false",
                "msg": "No Profile Exists"
              });
              response.writeHead(200, {
                "Content-Type": "application/json",
                'Content-Length': payload.length
              });
              response.end(payload);
            }
          } catch (error1) {
            e = error1;
            payload = JSON.stringify({
              "Status": "Failed",
              "error": "Unexpected error occured. Please check your data and try again"
            });
            response.writeHead(500, {
              "Content-Type": "application/json",
              'Content-Length': payload.length
            });
            response.end(payload);
          }
        });
      } else if (request.body && request.query.infoType === '3') {
        return ProfileModel.findOne({
          customerID: +request.username,
          _id: request.query.gmid
        }, function(error, profiles) {
          var alreadyExists, co_keys, customObj, diff, e, flag, key, keys, payload, pre_co, ref;
          try {
            if (profiles) {
              flag = true;
              profiles.co = profiles.co || {};
              try {
                customObj = JSON.parse(request.query.infoID);
              } catch (error1) {
                e = error1;
                console.log(Date() + ' : Invalid JSON');
                payload = JSON.stringify({
                  "Status": "Invalid Format. Please send valid JSON"
                });
                response.writeHead(500, {
                  "Content-Type": "application/json",
                  'Content-Length': payload.length
                });
                response.end(payload);
                return;
              }
              keys = Object.keys(customObj);
              co_keys = Object.keys(profiles.co);
              diff = _.difference(keys, co_keys);
              if (diff.length > 0) {
                diff.forEach(function(k) {
                  if (customObj[k] && !profiles.co[k]) {
                    return profiles.co[k] = customObj[k];
                  }
                });
              }
              alreadyExists = [];
              if (profiles.co !== {}) {
                ref = profiles.co;
                for (key in ref) {
                  pre_co = ref[key];
                  if (keys.indexOf(key) !== -1) {
                    alreadyExists.push(key);
                  }
                }
                alreadyExists.forEach(function(key) {
                  if (customObj[key]) {
                    if (customObj[key]) {
                      return Object.keys(customObj[key]).forEach(function(int_key) {
                        return profiles.co[key][int_key] = customObj[key][int_key];
                      });
                    } else {
                      return flag = false;
                    }
                  }
                });
              } else {
                profiles.co = customObj;
              }
              if (!flag || keys.length < 1) {
                console.log(Date() + ' : No Data to Update');
                payload = JSON.stringify({
                  "Status": "false",
                  "msg": "No Data to Update"
                });
                response.writeHead(200, {
                  "Content-Type": "application/json",
                  'Content-Length': payload.length
                });
                response.end(payload);
              } else {
                profiles.markModified('co');
                return profiles.save(function(err) {
                  var queueJson;
                  if (err) {
                    console.log(Date() + ' : Error saving information for Custom Objects:' + request.query.infoID);
                    payload = JSON.stringify({
                      "Status": "Database error. Please consult Monoloop admin"
                    });
                    response.writeHead(500, {
                      "Content-Type": "application/json",
                      'Content-Length': payload.length
                    });
                    return response.end(payload);
                  } else {
                    console.log(Date() + ' ' + keys + ' successfully updated');
                    payload = JSON.stringify({
                      "Status": keys + " successfully updated"
                    });
                    queueJson = {
                      gmid: request.query.gmid,
                      cid: request.username
                    };
                    response.writeHead(200, {
                      "Content-Type": "application/json",
                      'Content-Length': payload.length
                    });
                    return response.end(payload);
                  }
                });
              }
            } else {
              console.log(Date() + ' : No Profile Exists');
              payload = JSON.stringify({
                "Status": "false",
                "msg": "No Profile Exists"
              });
              response.writeHead(200, {
                "Content-Type": "application/json",
                'Content-Length': payload.length
              });
              response.end(payload);
            }
          } catch (error1) {
            e = error1;
            payload = JSON.stringify({
              "Status": "Failed",
              "error": "Unexpected error occured. Please check your data and try again"
            });
            response.writeHead(500, {
              "Content-Type": "application/json",
              'Content-Length': payload.length
            });
            response.end(payload);
          }
        });
      } else {
        console.log(Date() + ' : Bad request');
        payload = JSON.stringify({"Bad request": "Bad request"});
        response.writeHead(400, {
          "Content-Type": "application/json",
          'Content-Length': payload.length
        });
        return response.end(payload);
      }
    },
    getInformation: function(request, response) {
      var payload;
      //if request.body && typeof request.body.MID == "string" && +request.body.infoType == 1 && typeof requst.body.infoID == "number"
      if (request.body && request.query.infoType === '1') {
        console.log(Date() + ' : customerID: ' + request.username + ' fetching goal: ' + request.query.infoID + 'with InfoID: ' + request.query.infoType);
        return ProfileModel.findOne({
          customerID: +request.username,
          _id: request.query.gmid
        }, function(error, profiles) {
          var Status, goal, goalUID, goals, i, key, len, midFound, payload, profilesProcessed;
          if (profiles) {
            if (error) {
              console.log(Date() + ' : Profile do not exist');
              payload = JSON.stringify({
                "Status": "Profile do not exist. Please consult Monoloop admin"
              });
              response.writeHead(500, {
                "Content-Type": "application/json",
                'Content-Length': payload.length
              });
              response.end(payload);
            }
            if (request.query.mid) {
              profilesProcessed = 0;
              midFound = false;
              return profiles.profiles.forEach(function(singleProfile, index) {
                var Status, goal, goalUID, goals, i, key, len;
                profilesProcessed++;
                if (singleProfile._id.toString() === request.query.mid) {
                  goals = singleProfile.goals || [];
                  midFound = true;
                  for (key = i = 0, len = goals.length; i < len; key = ++i) {
                    goal = goals[key];
                    console.log(Date() + ' Goals already there: ' + goal.uid);
                    goalUID = '' + goal.uid;
                    Status = '';
                    if (goalUID === request.query.infoID) {
                      Status = 'achieved';
                      break;
                    }
                  }
                  if (Status === 'achieved') {
                    console.log(Date() + ' : Goal achieved');
                    payload = JSON.stringify({
                      "Status": "True"
                    });
                    response.writeHead(200, {
                      "Content-Type": "application/json",
                      'Content-Length': payload.length
                    });
                    return response.end(payload);
                  } else {
                    console.log(Date() + ' : Goal not achieved');
                    payload = JSON.stringify({
                      "Status": "False"
                    });
                    response.writeHead(200, {
                      "Content-Type": "application/json",
                      'Content-Length': payload.length
                    });
                    return response.end(payload);
                  }
                }
              });
            } else {
              goals = profiles.profiles[0].goals;
              if (goals) {
                console.log(Date() + ' : Iterating goals array to fetch goal information');
                for (key = i = 0, len = goals.length; i < len; key = ++i) {
                  goal = goals[key];
                  console.log(Date() + ' Goals already there: ' + goal.uid);
                  goalUID = '' + goal.uid;
                  Status = '';
                  if (goalUID === request.query.infoID) {
                    Status = 'achieved';
                    break;
                  }
                }
                if (Status === 'achieved') {
                  console.log(Date() + ' : Goal achieved');
                  payload = JSON.stringify({
                    "Status": "True"
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  return response.end(payload);
                } else {
                  console.log(Date() + ' : Goal not achieved');
                  payload = JSON.stringify({
                    "Status": "False"
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  return response.end(payload);
                }
              } else {
                console.log(Date() + ' : Provided information is not valid');
                payload = JSON.stringify({"Provided information is not valid": "Provided information is not valid"});
                response.writeHead(404, {
                  "Content-Type": "application/json",
                  'Content-Length': payload.length
                });
                return response.end(payload);
              }
            }
          } else {
            console.log(Date() + ' : No Profile Exists');
            payload = JSON.stringify({
              "Status": "Failed. No Profile Exists"
            });
            response.writeHead(200, {
              "Content-Type": "application/json",
              'Content-Length': payload.length
            });
            return response.end(payload);
          }
        });
      } else if (request.body && request.query.infoType === '2') {
        return ProfileModel.findOne({
          customerID: +request.username,
          _id: request.query.gmid
        }, function(error, profiles) {
          var _data, e, filled, keys, multipleKeys, objKey, payload;
          try {
            if (profiles) {
              if (error) {
                console.log(Date() + ' : Profile do not exist');
                payload = JSON.stringify({
                  "Status": "Profile do not exist. Please consult Monoloop admin"
                });
                response.writeHead(500, {
                  "Content-Type": "application/json",
                  'Content-Length': payload.length
                });
                response.end(payload);
                return;
              }
              if (request.query.infoID) {
                try {
                  multipleKeys = [];
                  objKey = JSON.parse(JSON.stringify(request.query.infoID));
                  if (typeof JSON.parse(objKey) === "object") {
                    multipleKeys = JSON.parse(objKey);
                  }
                } catch (error1) {
                  e = error1;
                  console.log(e);
                }
                if (profiles.cv) {
                  keys = Object.keys(profiles.cv);
                }
                filled = false;
                if (multipleKeys.length > 0) {
                  _data = {};
                  multipleKeys.forEach(function(mk) {
                    if (keys.indexOf(mk) !== -1) {
                      filled = true;
                      return _data[mk] = profiles.cv[mk];
                    }
                  });
                }
                if (_data && filled) {
                  payload = JSON.stringify({
                    "Status": "true",
                    "data": _data
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                  return;
                }
                if (keys.indexOf(request.query.infoID) !== -1) {
                  _data = profiles.cv[request.query.infoID];
                  payload = JSON.stringify({
                    "Status": "true",
                    "data": _data
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                } else {
                  payload = JSON.stringify({
                    "Status": "false",
                    "error": "No data exists against the requested key"
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                }
              } else {
                if (profiles.co) {
                  payload = JSON.stringify({
                    "Status": "true",
                    "data": profiles.co
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                }
              }
            }
          } catch (error1) {}
        });
      } else if (request.body && request.query.infoType === '3') {
        return ProfileModel.findOne({
          customerID: +request.username,
          _id: request.query.gmid
        }, function(error, profiles) {
          var _data, e, filled, keys, multipleKeys, objKey, payload;
          try {
            if (profiles) {
              if (error) {
                console.log(Date() + ' : Profile do not exist');
                payload = JSON.stringify({
                  "Status": "Profile do not exist. Please consult Monoloop admin"
                });
                response.writeHead(500, {
                  "Content-Type": "application/json",
                  'Content-Length': payload.length
                });
                response.end(payload);
                return;
              }
              if (request.query.infoID) {
                try {
                  multipleKeys = [];
                  objKey = JSON.parse(JSON.stringify(request.query.infoID));
                  if (typeof JSON.parse(objKey) === "object") {
                    multipleKeys = JSON.parse(objKey);
                  }
                } catch (error1) {
                  e = error1;
                  console.log(e);
                }
                if (profiles.co) {
                  keys = Object.keys(profiles.co);
                }
                filled = false;
                if (multipleKeys.length > 0) {
                  _data = {};
                  multipleKeys.forEach(function(mk) {
                    if (keys.indexOf(mk) !== -1) {
                      filled = true;
                      return _data[mk] = profiles.co[mk];
                    }
                  });
                }
                if (_data && filled) {
                  payload = JSON.stringify({
                    "Status": "true",
                    "data": _data
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                  return;
                }
                if (keys.indexOf(request.query.infoID) !== -1) {
                  _data = profiles.co[request.query.infoID];
                  payload = JSON.stringify({
                    "Status": "true",
                    "data": _data
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                } else {
                  payload = JSON.stringify({
                    "Status": "false",
                    "error": "No data exists against the requested key"
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                }
              } else {
                if (profiles.co) {
                  payload = JSON.stringify({
                    "Status": "true",
                    "data": profiles.co
                  });
                  response.writeHead(200, {
                    "Content-Type": "application/json",
                    'Content-Length': payload.length
                  });
                  response.end(payload);
                }
              }
            } else {
              console.log(Date() + ' : No Profile Exists');
              payload = JSON.stringify({
                "Status": "false",
                "msg": "No Profile Exists"
              });
              response.writeHead(200, {
                "Content-Type": "application/json",
                'Content-Length': payload.length
              });
              response.end(payload);
            }
          } catch (error1) {
            e = error1;
            payload = JSON.stringify({
              "Status": "Failed",
              "error": "Unexpected error occured. Please check your data and try again"
            });
            response.writeHead(500, {
              "Content-Type": "application/json",
              'Content-Length': payload.length
            });
            response.end(payload);
          }
        });
      } else {
        console.log(Date() + ' : Bad request');
        payload = JSON.stringify({"Bad request": "Bad request"});
        response.writeHead(400, {
          "Content-Type": "application/json",
          'Content-Length': payload.length
        });
        return response.end(payload);
      }
    }
  };

  //Helper Functions for custom vars and forms
  hardCodedConfigs = function() {
    return {
      saveType: {
        'override': 1,
        'add': 2,
        'subtract': 3,
        'push': 4
      },
      dataType: {
        'number': 1,
        'string': 2,
        'boolean': 3,
        'date': 4 //Date is a string
      }
    };
  };

  checkType = function(data1, data2) {
    var tempDate;
    tempDate = new Date(data2);
    if (data1 === hardCodedConfigs().dataType['number'] && typeof data2 === 'number') {
      return true;
    } else if (data1 === hardCodedConfigs().dataType['string'] && typeof data2 === 'string') {
      if (!isNaN(new Date(data2) && !isNaN(parseInt(data2.split('-')[0])))) {
        if (parseInt(data2.split('-')[0]) === tempDate.getFullYear()) {
          return false;
        }
      }
      return true;
    } else if (data1 === hardCodedConfigs().dataType['boolean'] && typeof data2 === 'boolean') {
      return true;
    } else if (data1 === hardCodedConfigs().dataType['date'] && !isNaN(tempDate) && typeof data2 === 'string') {
      if (!isNaN(parseInt(data2.split('-')[0]))) {
        if (parseInt(data2.split('-')[0]) === tempDate.getFullYear()) {
          return true;
        }
      }
      return false;
    } else {
      return false;
    }
  };

  /*
infoapi: (request,response) ->
  resdata={custom:{}}
  handle100 = () ->
if nextbatch<getids.length
  ProfileModel.find {customerID:request.username, _id:{$in:getids.slice(nextbatch,nextbatch+100)}}, "cv _id", {}, (error, profiles) ->
    write1 = (err) ->
      if err
        errors[nextinlist-1]=true
      if nextinlist<profiles.length
        localerr=false
        for cvkey,cvdata of request.body.setCustom[profiles[nextinlist]._id]
          if typeof(cvdata)=="string" || typeof(cvdata)=="boolean"
            profiles[nextinlist].cv[cvkey]=cvdata
          else if typeof(cvdata)=="number" && cvdata == (0 | cvdata) #Check that cvdata is valid 32-bit signed integer.
            profiles[nextinlist].cv[cvkey]=cvdata
          else if typeof(cvdata)=="object" && cvdata.date
            profiles[nextinlist].cv[cvkey]=new Date(cvdata.date)
            if inNaN(profiles[nextinlist].cv[cvkey])
              localerr=true
          else
            localerr=true
        if localerr
          errors[nextinlist]=true
          setTimeout(write1,1)
        else
          profiles[nextinlist].save(write1)
        nextinlist++
      else
        for profile,key in profiles
          if errors[key]
            resdata.custom[profile._id]=null
          else
            resdata.custom[profile._id]=profile.cv
        nextbatch+=100
        setTimeout(handle100,50)
    nextinlist=0
    errors={}
    if error
      #err
    else
      write1()
else
  #done - TODO: respond
  if request.body
getids=[]
request.body.setCustom = request.body.setCustom || {}
if typeof(request.body.setCustom) is "object"
  for profileid,data of request.body.setCustom
    getids.push(profileid)
setlimit=getids.length
if Array.isArray(request.body.getCustom)
  for profileid in request.body.getCustom
    if !request.body.setCustom[profileid]
      getids.push(profileid)
nextbatch=0
handle100()
  else
#err
 */

}).call(this);


//# sourceMappingURL=info.js.map
//# sourceURL=coffeescript