let redis = require('../utils/caching/_parentCache');
let account = require('../model/account');
let logger = redis.initiateLogger();

module.exports = {
  account: null,

  loadAccountData(callback, request){
    let me = this;
    this.load(request, function(err,account){
      me.setAccount(account);
      return callback(null,account);
    });
  },

  load(req, cb){
    const state = redis.getServerStatus();
    if (!state) {
      return getFromDB(req, function(err,result){
        cb(null,result)
      });
    } else {
      return initiateCacheActivites(req, function(err,result){
        if(err){
          return cb(err,null);
        }
        if(result === null){
          cb(null, null);
        }else{
          cb(null,account.hydrate(result));
        }

      });
    }
  },

  setAccount(account){
    this.account = account;
  }
};


let initiateCacheActivites = function(req, cb) {
    var clientRedis;
    clientRedis = redis.initiateCache();
    return clientRedis.get(req.query.cid + "_account", function(err, replies) {
      if(err){
        return cb(err,null);
      }
      if (replies !== null && replies !== 'null') {
        logger.print('account FOUND IN Redis');
        return cb(null, JSON.parse(replies));
      } else {
        return getFromDB(req, function(error, result) {
          if (error) {
            return cb(error);
          } else {
            logger.print('account FOUND IN DB');
            return clientRedis.set(req.query.cid + "_account", JSON.stringify(result), function() {
              clientRedis.expire(req.query.cid + "_account", 3600);
              return cb(null, result);
            });
          }
        });
      }
    });
  };

let getFromDB = function(req, cb) {
  return account.findOne({
    uid: req.query.cid
  }, null, {
    limit: 1
  }).lean().exec(cb);
};
