let account_service = require('./account');
const _ = require('underscore');

module.exports = {
  events: [],

  resetEvents(){
    this.events = [];
    account_service.setAccount(null);
  },

  processGoals(goals){
    let goal_ids = [];
    if(goals && goals.length === 0){
      return;
    }
    let account = account_service.account;
    if(account && account.plugins){
      let plugin = account.plugins[0];
      if(plugin && plugin.enabled && plugin.goal){
        for(let i = 0; i < goals.length; i++){
          if(! _.contains(goal_ids,goals[i].uid)){
            this.events.push({
              type: 'goal',
              uid: goals[i].uid,
              value: goals[i].p
            });
            goal_ids.push(goals[i].uid);
          }
        }
      }
    }
  },

  processSegments(profile){
    let visit = profile.lastVisit;
    if(visit.segments_InOut === undefined){
      return;
    }
    let me = this;

    let account = account_service.account;
    let segment_out_ids = [];
    let segment_in_ids = [];
    if(account && account.plugins){
      let plugin = account.plugins[0];
      if(plugin && plugin.enabled && plugin.audience){
        _.each(visit.segments_InOut,function(segment,key){
          let uid = key.replace('segment_','');
          uid = parseInt(uid);
          if(segment.Out){
            if(! _.contains(segment_out_ids,uid)){
              me.events.push({
                type: 'audience-out',
                uid: uid,
              });
              segment_out_ids.push(uid);
            }
          }else{
            if(! _.contains(segment_in_ids,uid)){
              me.events.push({
                type: 'audience-in',
                uid: uid,
              });
              segment_in_ids.push(uid);
            }
          }
        });
      }
    }
  },

  processExperience(profile){
    let account = account_service.account;
    let me = this;
    let experience_ids = [];
    if(account && account.plugins){
      let plugin = account.plugins[0];
      if(plugin && plugin.enabled && plugin.experience){
        let sub_profile = profile.getRecentDevicePorfile();
        if(sub_profile.experiments){
          _.each(sub_profile.experiments,function(experiment,key){
            let uid = parseInt(key);
            if(! _.contains(experience_ids,uid)){
              me.events.push({
                type: 'experience-in',
                uid: uid
              });
              experience_ids.push(uid);
            }
          });
        }
        if(sub_profile.controlgroups){
          _.each(sub_profile.controlgroups,function(experiment,key){
            let uid = parseInt(key);
            if(! _.contains(experience_ids,uid)){
              me.events.push({
                type: 'experience-in',
                uid: uid
              });
              experience_ids.push(uid);
            }
          });
        }
      }
    }
  }

}