let cachedLayer = require('../../utils/cachingLayer');
let elementCtrl = require('../../controller/elements');

module.exports = {
  loadPageElements(callback, req, res){
    let parmas = {
      request: req.query,
      response: res,
      typeOfKey: 'pageelements'
    };
    return cachedLayer.invokeCaching(parmas,
      function(err,result) {
      if (err) {
        return callback(err,null);
      } else {
        return callback(null,result);
      }
    });
  },

  buildContent(callback, results, request){

    var contentArray, contentIDs, data, i, id, len, params, splittedContent;
    params = {
      elements: results.loadPageElements,
      profile: results.loadProfile.MonoloopProfile,
      assets: results.loadAssets,
      vid: request.query.vid,
      experience: results.assignExperienceGroup
    };
    data = elementCtrl.buildContent(params);
    contentIDs = request.query.content_ids;
    contentArray = [];
    if (contentIDs !== void 0 && contentIDs !== "") {
      splittedContent = contentIDs.split(",");
      for (i = 0, len = splittedContent.length; i < len; i++) {
        id = splittedContent[i];
        contentArray.push(id);
      }
    }
    return callback(null,{
      data: data.data,
      goals: data.goals,
      signals: data.signals,
      debug: data.debug,
      goalsOCR: data.goalsOCR,
      contents: contentArray
    });
  }
}