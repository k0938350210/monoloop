let helper = require('../../utils/helper');
let ejs = require('ejs');
let debug = require('debug')('ml:invoke');

module.exports = {
	isPassSegment(elements, options){
		debug('Process is pass segment');
		let pass_experiences = [];
    let baseFunctions = helper.getElementsBasicFunctions();

    let MonoloopProfile = options.MonoloopProfile;

    for(let i = 0; i < elements.length; i++){
      let element = elements[i];
      if(element.Experiment && element.Experiment.length && element.content && element.content.length){
        for (let j = 0; j < element.content.length; j++) {
          let content = element.content[j];

          if(content.experimentID){
            if(pass_experiences.indexOf(content.experimentID) === -1){
              // Process segment compute
              let content_out = ejs.render(baseFunctions + content.code,{
                MonoloopProfile: MonoloopProfile
              });

              if (content_out.replace(/^\s+|\s+$/g,"") !== '') {
                pass_experiences.push(content.experimentID);
              }
            }
          }
        }
      }
    }
    /*
		if(element.Experiment && element.Experiment.length && element.content && element.content.length){
			console.log('Compute segment');
			for (let i = 0; i < element.content.length; i++) {
				let content = element.content[i];
				console.log(content);
			}
		}

		return pass;
    */

    return pass_experiences;
	},


  findExperience(elements, experimentID){
    for(let i = 0; i < elements.length; i++){
      let element = elements[i];
      if(element.Experiment && element.Experiment.length){
        for(let j = 0; j < element.Experiment.length; j++){
          if(element.Experiment[j].experimentID == experimentID){
            return element.Experiment[j];
          }
        }
      }
    }
  },

  addVisitorToProfileExpGroup(currentProfile, exp_id, vid){
    if(vid){
      if(currentProfile.experiments[exp_id]){
        currentProfile.experiments[exp_id].visitorCount++;
        currentProfile.experiments[exp_id].timestamp = new Date().getTime() / 1000
      }else{
        currentProfile.experiments[exp_id] = {
          visitorCount: 1,
          timestamp: new Date().getTime() / 1000
        };
      }
    }
  },

  addVisitorToProfileControlGroup(currentProfile, exp_id, vid){
    if(vid){
      if(currentProfile.controlgroups[exp_id]){
        currentProfile.controlgroups[exp_id].visitorCount++;
        currentProfile.controlgroups[exp_id].timestamp = new Date().getTime() / 1000
      }else{
        currentProfile.controlgroups[exp_id] = {
          visitorCount: 1,
          timestamp: new Date().getTime() / 1000
        };
      }
    }
  }
};