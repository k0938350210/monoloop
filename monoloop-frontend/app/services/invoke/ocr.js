let cachedLayer = require('../../utils/cachingLayer');

module.exports = {
  getOCRStatus(callback, request){
    let params = {
      cid: request.query.cid,
      typeOfKey: 'ocr'
    };
    return cachedLayer.invokeCaching(params, function(err, result) {
      if (err) {
        return callback(err, null);
      } else {
        return callback(null, result);
      }
    });
  }
}