let helper = require('../../utils/helper');
let profileCtrl = require('../../controller/profiles');
let ProfileModel = require('../../model/profile');

module.exports = {
  detectProfileStatus(callback, request, response){
    var decideProfile, query, resultSet;
    decideProfile = null;
    query = {};
    resultSet = {};
    resultSet['profileType'] = "newProfile";
    resultSet['result'] = [];
    //Set default values
    if (request.query.mid === void 0) {
      request.query.mid = '';
    }
    if (request.query.gmid === void 0) {
      request.query.gmid = '';
    }
    if (request.query.uniqueID === void 0) {
      request.query.uniqueID = '';
    }
    if (helper.isNull(request.query.gmid) && request.query.mid) {
      query = {
        _id: request.query.mid
      };
      decideProfile = "oldProfile";
    }
    if (request.query.gmid && helper.isNull(request.query.mid)) {
      query = {
        _id: request.query.gmid
      };
      decideProfile = "newProfilePush";
    }
    if (helper.isNull(request.query.gmid) && helper.isNull(request.query.mid)) {
      decideProfile = "newProfile";
    }
    if (request.query.gmid && request.query.mid) {
      query = {
        _id: request.query.gmid
      };
      decideProfile = "dontPush";
    }
    if (query._id) {
      query['$or'] = [
        {
          uniqueID: {
            '$exists': false
          }
        },
        {
          uniqueID: ''
        }
      ];
    }
    if (request.query.uniqueID) {
      query = {
        uniqueID: request.query.uniqueID
      };
    }
    if (Object.keys(query).length > 0) {
      return profileCtrl.get(request,response,query,function(err,result) {
        if (err) {
          return callback(err);
        }
        if (result && result.length > 0) {
          if (result[0].privacy && result[0].privacy.lastProfileDelete === request.query.mid) {
            decideProfile = "reInsertProfile";
          }
          resultSet['profileType'] = decideProfile;
          resultSet['result'] = result;
          return callback(null, resultSet);
        } else {
          request.query.vid = !helper.isNull(request.query.vid) ? "" : void 0;
          request.query.mid = !helper.isNull(request.query.mid) ? "" : void 0;
          request.query.gmid = !helper.isNull(request.query.gmid) ? "" : void 0;
          request.query.skey = !helper.isNull(request.query.skey) ? "" : void 0;
          return callback(null,resultSet);
        }
      });
    } else {
      return callback(null, resultSet);
    }
  },

  loadProfile(results, callback, request){
    var MonoloopProfile, ProfileLoaded, VisitID, connector, content, element, gmid, hasToLoadAssets, hasToLoadProfile,
      i, j, k, l, len, len1, len2, len3, profile, profileID, ref, ref1, ref2, ref3, resultSet;
    ProfileLoaded = false;
    hasToLoadProfile = false;
    hasToLoadAssets = [];
    profileID = "";
    MonoloopProfile = new ProfileModel();
    if (results.loadPageElements) {
      ref = results.loadPageElements;
      for (i = 0, len = ref.length; i < len; i++) {
        element = ref[i];
        if (element.content !== void 0) {
          ref1 = element.content;
          for (j = 0, len1 = ref1.length; j < len1; j++) {
            content = ref1[j];
            if (!element.content) {
              continue;
            }
            ref2 = content.connectors;
            for (k = 0, len2 = ref2.length; k < len2; k++) {
              connector = ref2[k];
              if (content.connectors) {
                if (connector === 'MonoloopProfile') {
                  hasToLoadProfile = true;
                }
              }
            }
            if (content.asset) {
              hasToLoadAssets.push(content.asset);
            }
          }
        }
      }
    }
            //load or create profiles according to the request
    resultSet = results.detectProfileStatus;
    if (hasToLoadProfile) {
      gmid = false;
      ProfileLoaded = true;
    }
    if (resultSet.result.length > 0) {
      ref3 = resultSet.result;
      for (l = 0, len3 = ref3.length; l < len3; l++) {
        profile = ref3[l];
        if (profile instanceof ProfileModel && parseInt(profile.customerID,10 === parseInt(request.query.cid,10))) {
          if (profile._id.toString() === request.query.gmid || profile.uniqueID === request.query.uniqueID) {
            MonoloopProfile = profile;
            MonoloopProfile.recently_used_device = request.query.mid;
            if (results.getOCRStatus !== null || results.getOCRStatus !== void 0) {
              MonoloopProfile.OCR = results.getOCRStatus;
            }
          }
        }
      }
    }
    if (resultSet.profileType === "newProfile" || resultSet.profileType === "newProfilePush" || resultSet.profileType === "reInsertProfile") {
      profileID = MonoloopProfile.addProfile(request);
      if (results.getOCRStatus !== null || results.getOCRStatus !== void 0) {
        MonoloopProfile.OCR = results.getOCRStatus;
      }
    }
    if (profileID && resultSet.profileType === "newProfile" || resultSet.profileType === "newProfilePush") {
      request.query.profileID = profileID;
      request.query.mid = '';
    }

    if(request.query.vid === undefined){
      let current_ts = new Date().valueOf();
      if(MonoloopProfile.lastVisit && ( current_ts /1000 - MonoloopProfile.lastVisit.lastclick < 1800)){
        request.query.vid = MonoloopProfile.lastVisit.id;
      }else{
        request.query.vid = '';
      }
    }

    if (helper.isNull(request.query.vid) || resultSet.profileType === "reInsertProfile") {
      VisitID = MonoloopProfile.addVisit(request);
    } else {
      if (request.query.vid && request.query.gmid && request.query.vid.match(/^[0-9a-fA-F]{24}$/) && request.query.gmid.match(/^[0-9a-fA-F]{24}$/)) {
        MonoloopProfile.addPageView(request);
        VisitID = request.query.vid;
      } else {
        VisitID = MonoloopProfile.addVisit(request);
      }
    }
    return callback(null,{
      MonoloopProfile: MonoloopProfile,
      VisitID: VisitID,
      ProfileLoaded: ProfileLoaded,
      Asset: hasToLoadAssets,
      ProfileType: resultSet.profileType
    });
  },

  updateProfile(results, callback, request){
    var Goal, Profile, command, current_profile_index, current_visit_index, days, exisitingGoals, firstView, gls, gmid, goal, goals, goalsOCR, i,
      index, isFirstPageView, isReached, j, k, l, latestView, len, len1, len2, len3, matched, options, profiletype, query, ref, ref1, ref2, sortGoals, vidcheck, visit;
    matched = 0;
    Profile = results.loadProfile.MonoloopProfile;
    profiletype = results.loadProfile.ProfileType;
    current_profile_index = Profile.getIndex();
    current_visit_index = Profile.getLastVisitIndex();
    goals = Profile.profiles[current_profile_index].goals || [];
    command = {};
    query = {};
    options = {};
    gls = [];
    sortGoals = [];
    exisitingGoals = [];
    gmid = '';
    goalsOCR = results.buildContent.goalsOCR;
    if (goals) {
      for (index = i = 0, len = goals.length; i < len; index = ++i) {
        goal = goals[index];
        ref = results.buildContent.goals;
        for (j = 0, len1 = ref.length; j < len1; j++) {
          Goal = ref[j];
          if (Goal.uid === goal.uid) {
            exisitingGoals.push(goal.uid);
            goals[index].tstamp = Math.round((new Date).getTime() / 1000);
            goals[index].OCR = Profile.OCR !== void 0 ? Profile.OCR : false;
            matched = 1;
          }
        }
      }
    }
    if (matched !== 1) {
      if (results.buildContent.goals) {
        isReached = results.buildContent.goals.length > 0;
        ref1 = results.buildContent.goals;
        for (k = 0, len2 = ref1.length; k < len2; k++) {
          goal = ref1[k];
          if (sortGoals.indexOf(goal.uid) === -1) {
            sortGoals.push(goal.uid);
            gls.push({
              'tstamp': Math.round(new Date().getTime() / 1000),
              'uid': goal.uid,
              'p': goal.p,
              'OCR': goalsOCR[goal.uid] !== void 0 ? goalsOCR[goal.uid] : false
            });
          }
        }
      } else {

      }
      if (results.buildContent.goals) {
        ref2 = results.buildContent.goals;
        for (l = 0, len3 = ref2.length; l < len3; l++) {
          goal = ref2[l];
          if (exisitingGoals.indexOf(goal.uid) === -1) {
            goals.push({
              'tstamp': Math.round(new Date().getTime() / 1000),
              'uid': goal.uid,
              'p': goal.p,
              'OCR': goalsOCR[goal.uid] !== void 0 ? goalsOCR[goal.uid] : false
            });
          }
        }
      }
    }
    days = 0;
    if (profiletype === "oldProfile") {
      gmid = Profile._id;
    } else {
      gmid = request.query.gmid;
    }
    if (!request.query.gmid || profiletype === "oldProfile" || profiletype === "reInsertProfile") {
      Profile.customerID = request.query.cid;
      Profile.recently_used_device = Profile.profiles[current_profile_index]._id;
      Profile.markModified('privacy');
      Profile.profiles[current_profile_index].visits[current_visit_index].content = results.buildContent.contents;
      if (profiletype !== "oldProfile") {
        Profile.profiles[current_profile_index].goals = gls;
      }
      if (request.query.uniqueID != null) {
        Profile.uniqueID = request.query.uniqueID;
      }
      return Profile.save(function(err, success) {
        if (err) {
          logger.printError('Batch Profile Save Error ', err);
          return callback(err);
        } else {
          return callback(null, {gmid});
        }
      });
    } else {
      command.$push = {};
      command.$set = {};
      query['_id'] = request.query.gmid;
      if (profiletype !== "newProfilePush") {
        query['profiles._id'] = request.query.mid;
      }
      vidcheck = request.query.vid;
      isFirstPageView = "";
      latestView = Profile.profiles[current_profile_index].current.views[Profile.profiles[current_profile_index].current.views.length - 1];
      firstView = Profile.profiles[current_profile_index].current.views[0];
      isFirstPageView = Profile.profiles[current_profile_index].current.views.length === 1;
      if (typeof vidcheck === 'string') {
        vidcheck = vidcheck.match(/^[0-9a-fA-F]{24}$/);
      } else {
        vidcheck = null;
      }
      if (!request.query.vid || !vidcheck) {
        if (profiletype === "newProfilePush") {
          command.$set = {
            'recently_used_device': Profile.profiles[current_profile_index]._id,
            'version': 2
          };
          Profile.profiles[current_profile_index].goals = gls;
          visit = Profile.profiles[current_profile_index].visits.id(results.loadProfile.VisitID);
          if (Profile.profiles[current_profile_index].current.views.length > 0) {
            visit['content'] = results.buildContent.contents;
          }
          command.$push = {
            'profiles': Profile.profiles[current_profile_index]
          };
        }
        //command.$push['profiles.$.visits'] = visit.toJSON()
        if (profiletype === "dontPush") {
          command.$set = {
            'recently_used_device': Profile.profiles[current_profile_index]._id,
            'version': 2,
            'profiles.$.device_info': Profile.getDeviceInfo(request)
          };
          visit = Profile.profiles[current_profile_index].visits.id(results.loadProfile.VisitID);
          if (Profile.profiles[current_profile_index].current.views.length > 0) {
            visit['firstclick'] = latestView.timeStamp;
            visit['lastclick'] = latestView.timeStamp;
            visit['content'] = results.buildContent.contents;
          }
          command.$push['profiles.$.visits'] = visit.toJSON();
          command.$set['profiles.$.current.views'] = [latestView];
          if (Object.keys(Profile.profiles[current_profile_index].experiments).length > 0) {
            command.$set['profiles.$.experiments'] = Profile.profiles[current_profile_index].experiments;
          }
          if (Object.keys(Profile.profiles[current_profile_index].controlgroups).length > 0) {
            command.$set['profiles.$.controlgroups'] = Profile.profiles[current_profile_index].controlgroups;
          }
          if (matched !== 1) {
            command.$push['profiles.$.goals'] = {};
            command.$push['profiles.$.goals'].$each = gls;
          } else {
            command.$set['profiles.$.goals'] = goals;
          }
          command.$set['profiles.$.visit_server_date'] = new Date().getTime() / 1000;
        }
        if (Profile.privacy !== void 0) {
          command.$set['privacy.lastProfileDelete'] = null;
        }
      } else {
        command.$inc = {};
        command.$set = {
          'recently_used_device': Profile.profiles[current_profile_index]._id
        };
        query['profiles.visits._id'] = request.query.vid;
        if (Object.keys(Profile.profiles[current_profile_index].experiments).length > 0) {
          command.$set['profiles.$.experiments'] = Profile.profiles[current_profile_index].experiments;
        }
        if (Object.keys(Profile.profiles[current_profile_index].controlgroups).length > 0) {
          command.$set['profiles.$.controlgroups'] = Profile.profiles[current_profile_index].controlgroups;
        }
        command.$set['profiles.$.visits.' + current_visit_index + '.exitpage'] = request.query.url;
        command.$set['profiles.$.visits.' + current_visit_index + '.experiments'] = results.loadProfile.MonoloopProfile.lastVisit.experiments;
        command.$inc['profiles.$.visits.' + current_visit_index + '.clicks'] = 1;
        command.$push['profiles.$.visits.' + current_visit_index + '.content'] = results.buildContent.contents;
        // save content in visit
        //command.$push['profiles.$.visits.'+current_visit_index+'.content'] = result.buildContent.contentIDs
        if (matched !== 1) {
          command.$push['profiles.$.goals'] = {};
          command.$push['profiles.$.goals'].$each = gls;
        } else {
          command.$set['profiles.$.goals'] = goals;
        }
        command.$set['profiles.$.visit_server_date'] = new Date().getTime() / 1000;
        if (firstView) {
          command.$set['profiles.$.visits.' + current_visit_index + '.firstclick'] = firstView.timeStamp;
        }
        if (latestView) {
          command.$set['profiles.$.visits.' + current_visit_index + '.lastclick'] = latestView.timeStamp;
          command.$push['profiles.$.current.views'] = latestView;
        }
      }
      options = {
        upsert: true
      };
      Object.keys(command).forEach(function(cmdKey, value) {
        if (Object.keys(command[cmdKey]).length < 1) {
          return delete command[cmdKey];
        }
      });
      if (command !== void 0 && command.$set !== void 0 && command.$set['OCR'] && (results.getOCRStatus !== null || results.getOCRStatus !== void 0)) {
        command.$set['OCR'] = results.getOCRStatus;
      }
      if (command !== void 0 && command.$set && command.$set['uniqueID'] !== void 0 && (request.query.uniqueID != null)) {
        command.$set["uniqueID"] = request.query.uniqueID;
      }
      if (command !== void 0) { //and command.count > 0
        return ProfileModel.updateOne(query, command, options, function(err, updated) {
          if (err) {
            logger.printError("batch->parse()->databaseUpdateCallback - ", err + "\n\nCommand:\n" + util.inspect(command,{
              showHidden: true,
              depth: null
            }) + "\n\nQuery:\n" + util.inspect(query,{
              showHidden: true,
              depth: null
            }) + "\n\n");
            return callback(err);
          } else {
            return callback(null,{updated});
          }
        });
      }
    }
  }
}