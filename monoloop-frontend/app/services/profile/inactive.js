const webhook_model = require('../../model/webhook');
const profile_model = require('../../model/profile');
const publisher = require('./../../../redis-conn');
const debug = require('debug')('ml:worker');

function getTimeSpan(event_name){
  let events = event_name.split(' ');
  if(events.length != 2){
    return 0;
  }
  let time_value = events[0];
  let time_unit = events[1];

  debug(time_value);
  debug(time_unit);

  let timespan = 0;
  if(time_unit == 'minutes'){
    timespan = parseInt(time_value) * 60;
  }else if(time_unit == 'hours'){
    timespan = parseInt(time_value) * 60 * 60;
  }else if(time_unit == 'days'){
    timespan = parseInt(time_value) * 24 * 60 * 60;
  }
  return timespan;
}

module.exports = {

  getInactiveProfiles: async function(webhooks){

    let or = [];
    for(let i = 0; i < webhooks.length; i++){
      let webhook = webhooks[i];

      let timespan = getTimeSpan(webhook.event_name);

      or.push({
        customerID: webhook.cid,
        'profiles.visit_server_date': {
          '$lt': Date.now() / 1000 - timespan
        },
        'inactive_webhook_ids':{
          $nin: [ webhook._id.toString() ]
        }
      });

      debug(or);
    }

    let query = {
      '$and': [
        {
          '$or': [{
            'uniqueID': { '$exists': true ,'$ne': ""}
          },{
            'externalKeys': { '$exists': true ,'$ne': []}
          }]
        },{
          '$or': or
        }
      ]
    };

    //get match profiles
    let profiles = await profile_model.find(query).limit(1000);

    return profiles;

  },

  getInactiveWebhooks: async function(){
    let webhooks = await webhook_model.find({status: 'active',event_type: 'Inactive'});
    return webhooks;
  },

  setInactiveFlagToProfiles: async function(webhooks, profiles){
    for(let i = 0; i < profiles.length; i++){
      let profile = profiles[i];
      debug(profile);
      let visit_server_date = 0;
      for(let j = 0; j < profile.profiles.length; j++){
        if(visit_server_date < profile.profiles[j].visit_server_date){
          visit_server_date = profile.profiles[j].visit_server_date;
        }
      }
      debug("process profile " + profile.id);
      debug("visit_server_date : " + visit_server_date);

      //process webhook
      for(let j = 0; j < webhooks.length; j++){
        let webhook = webhooks[j];
        //need match cid
        if(webhook.cid != profile.customerID){
          continue;
        }
        //need not flag
        if(profile.inactive_webhook_ids !== undefined && profile.inactive_webhook_ids.indexOf(webhook.id) != -1){
          continue;
        }
        //make sure it not in timespan
        let timespan = getTimeSpan(webhook.event_name);
        if(visit_server_date >  Date.now() / 1000 - timespan){
          continue;
        }

        debug('match webhook ' + webhook.id + ' with profile ' + profile.id);
        let _mprofile = profile.getRecentDevicePorfile();

        await profile_model.updateOne({
          _id: profile.id
        },{ $push: { inactive_webhook_ids: webhook.id } });

        await publisher.rpush('webhook-inactive-profile-list', JSON.stringify({
          gmid: profile.id,
          mid: _mprofile.id,
          webhook_id: webhook.id
        }));
      }
    }
  }

}