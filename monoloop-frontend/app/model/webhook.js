const mongoose = require( './../../mongoose');

const  Schema = mongoose.Schema;

const Webhook = new Schema({
  _id: Schema.ObjectId,
  cid: Number,
  event_type: String,
  direction: String,
  event_name: String,
  event_id: String,
  callback_url: String,
  payload_type: String,
  status: String
});


// Model object
module.exports = mongoose.model('Webhook', Webhook, 'webhooks');