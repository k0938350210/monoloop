(function() {
  // Profile model

  // Copyright (c) 2009-2012 [Vyacheslav Voronchuk](mailto:voronchuk@gmail.com), [Web management software](http://wm-software.com)
  // License: see project's LICENSE file
  // Date: 17.07.12
  // Modules and variables
  var Profile, Schema, Url, Views, Visit, _, average, moment, mongoose, parse_str, parse_url, positiveList, rootObj, search_strings, strnatcmp, uaParser, urldecode, util, debug, geoip, countries;

  _ = require('underscore');

  moment = require('moment');

  Url = require('url');

  uaParser = require('ua-parser');

  //Avg = (require './../model/averages')
  util = require('./../utils/helper');

  rootObj = {};

  average = {};

  search_strings = ['q', 'search'];

  mongoose = require(__dirname + '/../../mongoose');

  Schema = mongoose.Schema;
  debug = require('debug')('ml:profile');
  if(process.title != 'archive' && process.title != 'webhook'){
    geoip = require('geoip-lite');
    countries = require('countries-list').countries;
  }

  let visit_schema = require('../schema/profile-visit');
  let sub_profile_schema = require('../schema/sub-profile');

  // Field definition for Profile
  Profile = new Schema({
    customerID: {
      type: Number,
      reqired: true
    },
    recently_used_device: {
      type: String
    },
    pgmid: {
      type: String
    },
    ngmid: {
      type: String
    },
    cv: {
      type: Schema.Types.Mixed,
      default: {}
    },
    co: {
      type: Schema.Types.Mixed,
      default: {}
    },
    form: {
      type: Schema.Types.Mixed,
      default: {}
    },
    OCR: Boolean,
    uniqueID: {
      type: String,
      default: ''
    },
    externalKeys: {
      type: Schema.Types.Mixed,
      default: []
    },
    inactive_webhook_ids: {
      type: Schema.Types.Mixed,
      default: []
    },
    privacy: {
      type: mongoose.Schema.Types.Object
    },
    //cachedProperties: {type: mongoose.Schema.Types.Object, ref: 'CachedProperties'}
    version: Number,
    profiles: [sub_profile_schema]
  },{
    toObject: {
      transform: function (doc, ret) {
      }
    },
    toJSON: {
      transform: function (doc, ret) {
        delete ret.OCR;
        delete ret.__v;
        delete ret.cachedProperties;
        delete ret.inactive_webhook_ids;
        ret.total_visits = doc.VisitCount;
        ret.total_page_views = doc.PageViewCount;
        ret.last_visit_ts = doc.lastVisit.lastclick;
        ret.categories = doc.summarySortCategories(2);
        ret.products = doc.summarySortCategories(1);
        ret.audiences = doc.activeAudiences();
        ret.goals = doc.allGoals();
        ret.experiences = doc.allExperiences();
        ret.controlgroups = doc.allControlGroups();
        ret.searches = doc.allSearches();
      }
    }
  });

  positiveList = {
    'customerID': {
      allow: true,
      alias: 'cid'
    },
    '_id': {
      allow: true,
      alias: 'MonoloopID'
    },
    'FirstVisit': true,
    'LastEntryPage': true,
    'LastExitPage': true,
    'VisitCount': true,
    'PageViewCount': true,
    'PageViewCountGoals': true,
    'DaysSinceLastVisit': true,
    'LastViewedCategory': true,
    'ViewedCategory': true,
    'isTopCategory': true,
    'isLowCategory': true,
    'isGrowthCategory': true,
    'isDecreaseCategory': true,
    'lastViewedScore': true,
    'engagementTrend': true,
    'lastBasketValue': true,
    'lastBasketNoOfItems': true,
    'daysSinceLastBasket': true,
    'totalViewedCategoryValueInDays': true,
    'totalPurchaseValueInDays': true,
    'lifetimePurchaseValue': true,
    'totalPurchases': true,
    'avgPurchaseValue': true,
    'daysSinceLastPurchase': true,
    'purchaseRatio': true,
    'downloadedFile': true,
    'daysSinceLastDownload': true,
    'searchedFor': true,
    'AverageTimeOnPage': true,
    'EntryPage': true,
    'VisitDuration': true,
    'CurrentPageViewCount': true,
    'VisitPoints': true,
    'SocialMediaVisitor': true,
    'SearchVisitor': true,
    'DirectVisitor': true,
    'VisitSearchString': true,
    'VisitReferer': true,
    'CurrentURL': true,
    'currentIsTopCategory': true,
    'currentIsLowCategory': true,
    'currentLastViewedScore': true,
    'currentBasketValue': true,
    'currentBasketAvgValue': true,
    'currentBasketNoOfItems': true,
    'currentHasPurchaced': true,
    'currentDownloadedFile': true,
    'currentSearchedFor': true,
    'TimeZone': true,
    'Country': true,
    'City': true,
    'Longitude': true,
    'Latitude': true,
    'isWithinArea': true,
    'EngagementScore': true,
    'ClickDepthIndex': true,
    'RecencyIndex': true,
    'DurationIndex': true,
    'BrandIndex': true,
    'CategoryIndex': true,
    'GoalReached': true,
    'OS': true,
    'Resolution': true,
    'Browser': true,
    'BrowserVersion': true,
    'BrowserLanguage': true,
    'Javascript': true,
    'Flash': true,
    'Quicktime': true,
    'Wma': true,
    'Silverlight': true,
    'PDF': true,
    'Realplayer': true,
    'Gears': true,
    'ViewedCategories': true,
    'TopCategories': true,
    'PageviewsQA': true,
    'VisitsQA': true,
    //'CategoriesQA':true
    //'UniqueCategoriesQA':true
    //'ProductsQA':true
    //'UniqueProductsQA':true
    'TimeOnPageQA': true,
    'VisitDurationQA': true,
    //'UniqueProductPrCateoryQA':true
    //'UniqueCateoryRecencyQA':true
    'UniqueCateoryRecencyCurrentQA': true,
    //'UniqueProductRecencyQA':true
    'UniqueProductRecencyCurrentQA': true,
    //'ProductRecencyQA':true
    //'CateoryRecencyQA':true
    'CateoryRecencyCurrentQA': true,
    'utm_source': true,
    'utm_medium': true,
    'utm_content': true,
    'utm_campaign': true,
    'utm_term': true
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method getPublic
   */
  Profile.methods.getPublic = function() {
    var f, out, v;
    out = {};
    for (f in positiveList) {
      v = positiveList[f];
      if (v || v.allow) {
        if (v.alias) {
          out[v.alias] = this[f];
        } else {
          out[f] = this[f];
        }
      }
    }
    return out;
  };

  /**
   * Description :
   * @method getRoot
   * @deprecated Not in use
   */
  Profile.methods.getRoot = function() {};

  /**
   * Description : Get new GMID
   * @method getNewGmid
   * @return {ObjectId} return mongoose object ID
   */
  Profile.methods.getNewGmid = function() {
    return new mongoose.Types.ObjectId;
  };

  Profile.pre('init', async function() {
    var profile, that;
    that = this;
    profile = this.getRecentDevicePorfile();
    if (util.isDefined(profile) && !(util.isNull(profile))) {
      if (_.isArray(profile.visits)) {
        //Sort by lastclick - ascending
        return (await that.profile.visits.sort(function(a, b) {
          return strnatcmp(a.lastclick, b.lastclick);
        }));
      }
    }
  });

  //next()
  /**
   * Description : Remove last visit from current profile
   * @method removeLastVisit
   */
  Profile.methods.removeLastVisit = function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (_.isArray(profile.visits && profile.visits.length > 0)) {
        return profile.visits.pop();
      }
    }
  };

  /**
   * Virtual property contain last visit detail in current profile
   *
   * @property lastVisit
   * @type Object
   */
  Profile.virtual('lastVisit').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile && profile.visits.length) {
        profile.visits.sort(function(a, b) {
          if (a._id > b._id) {
            return 1;
          } else if (b._id > a._id) {
            return -1;
          } else {
            return 0;
          }
        });
        return profile.visits[profile.visits.length - 1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain total unique pages count in current views
   *
   * @property uniquePages
   * @type Number
   */
  Profile.virtual('uniquePages').get(function() {
    var profile, totalPages, uniqueURL;
    totalPages = 0;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      uniqueURL = [];
      profile.current.views.forEach(function(view, key) {
        if (uniqueURL.indexOf(view.url) === -1) {
          uniqueURL.push(view.url);
          if (!view.aggregated || view.aggregated !== true) {
            profile.current.views[key].aggregated = true;
            return totalPages++;
          }
        } else {
          if (!view.aggregated || view.aggregated !== true) {
            return profile.current.views[key].aggregated = true;
          }
        }
      });
    }
    return totalPages;
  });

  /**
   * Virtual property contain 2nd last visit info
   *
   * @property previousVisit
   * @type Object
   */
  Profile.virtual('previousVisit').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits.length > 1) {
        profile.visits.sort(function(a, b) {
          if (a._id > b._id) {
            return 1;
          } else if (b._id > a._id) {
            return -1;
          } else {
            return 0;
          }
        });
        return profile.visits[profile.visits.length - 2];
      } else {
        return null;
      }
    }
  });

  //Aliases:
  /**
   * Virtual property contain longitude info
   *
   * @property Longitude
   * @type String
   */
  Profile.virtual('Longitude').get(function() {
    if (this.lastVisit && this.lastVisit.loc_info && this.lastVisit.loc_info.loc_long) {
      return this.lastVisit.loc_info.loc_long;
    }
  });

  /**
   * Virtual property contain Latitude info
   *
   * @property Latitude
   * @type String
   */
  Profile.virtual('Latitude').get(function() {
    if (this.lastVisit && this.lastVisit.loc_info && this.lastVisit.loc_info.loc_lat) {
      return this.lastVisit.loc_info.loc_lat;
    }
  });

  /**
   * Virtual property contain country info
   *
   * @property Country
   * @type String
   */
  Profile.virtual('Country').get(function() {
    if (this.lastVisit && this.lastVisit.loc_info && this.lastVisit.loc_info.loc_country) {
      return this.lastVisit.loc_info.loc_country.toLowerCase();
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain city info
   *
   * @property City
   * @type String
   */
  Profile.virtual('City').get(function() {
    if (this.lastVisit && this.lastVisit.loc_info && this.lastVisit.loc_info.loc_city) {
      return this.lastVisit.loc_info.loc_city;
    }
  });

  /**
   * Virtual property contain browser language info
   *
   * @property BrowserLanguage
   * @type String
   */
  Profile.virtual('BrowserLanguage').get(function() {
    if (this.lastVisit && this.lastVisit.loc_info && this.lastVisit.loc_info.loc_browser_lang) {
      return this.lastVisit.loc_info.loc_browser_lang;
    }
  });

  /**
   * Virtual property contain time zone info
   *
   * @property TimeZone
   * @type String
   */
  Profile.virtual('TimeZone').get(function() {
    if (this.lastVisit && this.lastVisit.loc_info && this.lastVisit.loc_info.loc_timezone) {
      return this.lastVisit.loc_info.loc_timezone;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain window media info
   *
   * @property Wma
   * @type String
   */
  Profile.virtual('Wma').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_windowsmedia;
    }
  });

  /**
   * Virtual property contain silverlight info
   *
   * @property Silverlight
   * @type String
   */
  Profile.virtual('Silverlight').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_silverlight;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain resolution info
   *
   * @property Resolution
   * @type String
   */
  Profile.virtual('Resolution').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_resolution;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain real player info
   *
   * @property Realplayer
   * @type String
   */
  Profile.virtual('Realplayer').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_realplayer;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain quick time info
   *
   * @property Quicktime
   * @type String
   */
  Profile.virtual('Quicktime').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_quicktime;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain PDF info
   *
   * @property PDF
   * @type String
   */
  Profile.virtual('PDF').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_pdf;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain operating system info
   *
   * @property OS
   * @type String
   */
  Profile.virtual('OS').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_os;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain Javascript info
   *
   * @property Javascript
   * @type String
   */
  Profile.virtual('Javascript').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_java;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain gears info
   *
   * @property Gears
   * @type String
   */
  Profile.virtual('Gears').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_gears;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain flash info
   *
   * @property Flash
   * @type String
   */
  Profile.virtual('Flash').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_flash;
    } else {
      return null;
    }
  });

  /**
   * Virtual property contain browser version info
   *
   * @property BrowserVersion
   * @type String
   */
  Profile.virtual('BrowserVersion').get(function() {
    var profile, v;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      v = profile.device_info.config_browser_version;
      v = v.split('.');
      return v[0];
    }
  });

  /**
   * Virtual property contain browser info
   *
   * @property Browser
   * @type String
   */
  Profile.virtual('Browser').get(function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      return profile.device_info.config_browser_name;
    }
  });

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method getProfileByFields
   * @param fields
   * @deprecated Not in use. Found only in test
   */
  Profile.methods.getProfileByFields = function(fields) {
    var field, l, len1, profile;
    profile = {
      'id': this.id
    };
    fields = ['lastVisit'];
    if (fields) {
//Loop fields and call methods to get
      for (l = 0, len1 = fields.length; l < len1; l++) {
        field = fields[l];
        profile.h = 'g';
        profile[field] = this[field]();
      }
    } else {
      profile = this;
    }
    return profile;
  };

  //Compare single value of custom variable properties
  /**
   * Description : Compare single value of custom variables (CV) properties
   * @method compareSingleCV
   * @param cvValue {Number|String|Boolean} cvValue
   * @param operator {String} Operator which is used to compare value
   * @param value {Number|String|Boolean} Other value which is used in comparison
   * @return {Boolean} return true false on base of comparison
   */
  Profile.methods.compareSingleCV = function(cvValue, operator, value) {
    var currentType, dataInDbType, regExp;
    currentType = typeof value;
    dataInDbType = typeof cvValue;
    if (currentType !== dataInDbType) {
      return false;
    }
    if (currentType === 'string') {
      if (operator === '==') {
        return value === cvValue;
      } else if (operator === '!=') {
        return value !== cvValue;
      } else if (operator === 'ml_contains') {
        return cvValue.indexOf(value) !== -1;
      } else if (operator === 'ml_dose_not_contains') {
        return cvValue.indexOf(value) === -1;
      } else if (operator === 'ml_starts_with') {
        return cvValue.indexOf(value) === 0;
      } else if (operator === 'ml_ends_with') {
        regExp = new RegExp('.*' + value + '$');
        return regExp.test(cvValue);
      }
    } else if (currentType === 'number') {
      if (operator === '==') {
        return value === cvValue;
      } else if (operator === '!=') {
        return value !== cvValue;
      } else if (operator === '<') {
        return cvValue < value;
      } else if (operator === '>') {
        return cvValue > value;
      } else if (operator === '<=') {
        return cvValue <= value;
      } else if (operator === '>=') {
        return cvValue >= value;
      }
    } else if (currentType === 'boolean') {
      if (operator === '==') {
        return value === cvValue;
      } else if (operator === '!=') {
        return value !== cvValue;
      }
    }
    return false;
  };

  //Compare custom vars properties
  /**
   * Description : Compare custom variables properties. If cv are array then its call one by one to compare single cv function
   * and decide result on base of that result.
   * @method compareCV
   * @param cv_id {Array|Number|String|Boolean} custom variables id
   * @param operator {String} Operator which is used to compare value
   * @param value {Number|String|Boolean} Other value which is used in comparison
   * @return {Boolean} return true/false on base of comparison
   */
  Profile.methods.compareCV = function(cv_id, operator, value) {
    var cvValue, cvs, err, l, len1;
    try {
      cvs = this.cv[parseInt(cv_id)];
      if (cvs !== void 0) {
        if (cvs instanceof Array) {
          for (l = 0, len1 = cvs.length; l < len1; l++) {
            cvValue = cvs[l];
            if (this.compareSingleCV(cvValue, operator, value) === true) {
              return true;
            }
          }
          return false;
        } else {
          return this.compareSingleCV(cvs, operator, value);
        }
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->compareCV(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  //Date properties
  /**
   * Description : To calculate
   * <li> Time difference from current date to previous specific date.</li>
   * <li> Call to function convertTU to calculate time before today.</li>
   * @method timeBeforeToday
   * @param theCV {Object} contain date value/Array which is/are used as lower limit (To date).
   * @param tu {String} in which unit time need to convert like years, seconds,days,months etc
   * @deprecated Not in use. Found only in test
   * @return {Object|Number}
   */
  Profile.methods.timeBeforeToday = function(theCV, tu) {
    var err, l, len1, val, values;
    try {
      //return integer
      values = this.cv[parseInt(theCV)];
      if (values !== void 0) {
        if (values instanceof Array) {
          for (l = 0, len1 = values.length; l < len1; l++) {
            val = values[l];
            arr.push(this.convertTU(new Date() - new Date(val), tu));
          }
          return arr;
        } else {
          return this.convertTU(new Date() - new Date(values), tu);
        }
      }
    } catch (error) {
      err = error;
      return null;
    }
  };

  /**
   * Description : To calculate
   * <li> Time difference from current date to next specific date.</li>
   * <li> Call to function convertTU to calculate time after today.</li>
   * @method timeAfterToday
   * @param theCV {Object} contain date value/Array which is/are used as upper limit (From date).
   * @param tu {String} in which unit time need to convert like years, seconds,days,months etc
   * @deprecated Not in use. Found only in test
   * @return {Object|Number}
   */
  Profile.methods.timeAfterToday = function(theCV, tu) {
    var err, l, len1, val, values;
    try {
      //return integers
      values = this.cv[parseInt(theCV)];
      if (values !== void 0) {
        if (values instanceof Array) {
          for (l = 0, len1 = values.length; l < len1; l++) {
            val = values[l];
            arr.push(this.convertTU(new Date(val) - new Date(), tu));
          }
          return arr;
        } else {
          return this.convertTU(new Date(values) - new Date(), tu);
        }
      }
    } catch (error) {
      err = error;
      return null;
    }
  };

  /**
   * Description : To calculate
   * <li> Time difference from specific date to previous specific date.</li>
   * <li> Call to function convertTU to calculate time before date.</li>
   * @method timeBeforeDate
   * @param theCV {Object} contain date value/Array which is/are used as lower limit (To date).
   * @param theDate {Object} From .
   * @param tu {String} in which unit time need to convert like years, seconds,days,months etc
   * @deprecated Not in use. Found only in test
   * @return {Object|Number}
   */
  Profile.methods.timeBeforeDate = function(theCV, theDate, tu) {
    var err, l, len1, val, values;
    try {
      //return integer
      values = this.cv[parseInt(theCV)];
      if (values !== void 0) {
        if (tu instanceof Array) {
          for (l = 0, len1 = values.length; l < len1; l++) {
            val = values[l];
            arr.push(this.convertTU(new Date(theDate) - new Date(val), tu));
          }
          return arr;
        } else {
          return this.convertTU(new Date(theDate) - new Date(values), tu);
        }
      }
    } catch (error) {
      err = error;
      return null;
    }
  };

  /**
   * Description : To calculate
   * <li> Time difference from specific date to next specific date.</li>
   * <li> Call to function convertTU to calculate time after date.</li>
   * @method timeAfterDate
   * @param theCV {Object} contain date value/Array which is/are used as lower limit (From date).
   * @param theDate {Object} To date.
   * @param tu {String} in which unit time need to convert like years, seconds,days,months etc
   * @deprecated Not in use. Found only in test
   * @return {Object|Number}
   */
  Profile.methods.timeAfterDate = function(theCV, theDate, tu) {
    var err, l, len1, val, values;
    try {
      //return integer
      values = this.cv[parseInt(theCV)];
      if (values !== void 0) {
        if (values instanceof Array) {
          for (l = 0, len1 = values.length; l < len1; l++) {
            val = values[l];
            arr.push(this.convertTU(new Date(val) - new Date(theDate), tu));
          }
          return arr;
        } else {
          return this.convertTU(new Date(values) - new Date(theDate), tu);
        }
      }
    } catch (error) {
      err = error;
      return null;
    }
  };

  /**
   * Description : Convert timestamp to time unit(minuets,seconds, hours, days, months, years).
   * If there is not time unit pass to function then by default it ll convert timestamp to days.
   * @method convertTU
   * @param ms {timestamp} time stamp
   * @param tu {String} Unit as string
   * @return {String} converted time stamp
   */
  Profile.methods.convertTU = function(ms, tu) {
    switch (tu) {
      case 'Seconds':
        return moment.duration(ms).asSeconds();
      case 'Minutes':
        return moment.duration(ms).asMinutes();
      case 'Hours':
        return moment.duration(ms).asHours();
      case 'Days':
        return moment.duration(ms).asDays();
      case 'Months':
        return moment.duration(ms).asMonths();
      case 'Years':
        return moment.duration(ms).asYears();
      default:
        return moment.duration(ms).asDays();
    }
  };

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property avg
   */
  Profile.virtual('avg').get(function() {
    return average;
  }).set(function(v) {
    average = v;
  });

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method PageviewsQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  Profile.methods.PageviewsQA = function(condition) {
    var params, res;
    params = condition.split('||');
    if (params[0] === 'avg') {
      res = this.PageviewsAverageHighLow;
    } else {
      res = this.PageviewsInQuintile;
    }
    return res === parseInt(params[1], 10);
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method VisitsQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  Profile.methods.VisitsQA = function(condition) {
    var params, res;
    params = condition.split('||');
    if (params[0] === 'avg') {
      res = this.VisitsAverageHighLow;
    } else {
      res = this.VisitsInQuintile;
    }
    return res === parseInt(params[1], 10);
  };

  Profile.methods.getVisitById = function(vid) {
    var l, len1, profile, ref1, visits;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits && typeof profile.visits !== void 0) {
        ref1 = profile.visits;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          visits = ref1[l];
          if (visits._id.toString() === vid) {
            return visits;
          }
        }
      }
    }
    return null;
  };

  Profile.methods.getVisitByIndex = function(index) {
    var l, len1, profile, ref1, visits;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits && typeof profile.visits !== void 0) {
        if(profile.visits[index]){
          return profile.visits[index];
        }
      }
    }
    return null;
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method CategoriesQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  //Profile.methods.CategoriesQA = (condition) ->
  //  params = condition.split '||'
  //  if params[0] is 'avg'
  //    res = @.CategoriesAverageHighLow
  //  else
  //    res = @.CategoriesInQuintile
  //  return res is parseInt(params[1], 10)
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method UniqueCategoriesQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  /*Profile.methods.UniqueCategoriesQA = (condition) ->
  params = condition.split '||'
  if params[0] is 'avg'
    res = @.UniqueCategoriesAverageHighLow
  else
    res = @.UniqueCategoriesInQuintile
  return res is parseInt(params[1], 10)*/
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method ProductsQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  //Profile.methods.ProductsQA = (condition) ->
  //  params = condition.split '||'
  //  if params[0] is 'avg'
  //    res = @.ProductsAverageHighLow
  //  else
  //    res = @.ProductsInQuintile
  //  return res is parseInt(params[1], 10)
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method UniqueProductsQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  //Profile.methods.UniqueProductsQA = (condition) ->
  //  params = condition.split '||'
  //  if params[0] is 'avg'
  //    res = @.UniqueProductsAverageHighLow
  //  else
  //    res = @.UniqueProductsInQuintile
  //  return res is parseInt(params[1], 10)
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method TimeOnPageQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  Profile.methods.TimeOnPageQA = function(condition) {
    var params, res;
    params = condition.split('||');
    if (params[0] === 'avg') {
      res = this.TimeOnPageAverageHighLow;
    } else {
      res = this.TimeOnPageInQuintile;
    }
    return res === parseInt(params[1], 10);
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method VisitDurationQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  Profile.methods.VisitDurationQA = function(condition) {
    var params, res;
    params = condition.split('||');
    if (params[0] === 'avg') {
      res = this.VisitDurationAverageHighLow;
    } else {
      res = this.VisitDurationInQuintile;
    }
    return res === parseInt(params[1], 10);
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method UniqueProductPrCateoryQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  //Profile.methods.UniqueProductPrCateoryQA = (condition) ->
  //  params = condition.split '||'
  //  if params[0] is 'avg'
  //    res = @.UniqueProductPrCateoryAverageHighLow
  //  else
  //    res = @.UniqueProductPrCateoryInQuintile
  //  return res is parseInt(params[1], 10)
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method UniqueCateoryRecencyQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  //Profile.methods.UniqueCateoryRecencyQA = (condition) ->
  //  params = condition.split '||'
  //  if params[0] is 'avg'
  //    res = @.UniqueCateoryRecencyAverageHighLow
  //  else
  //    res = @.UniqueCateoryRecencyInQuintile
  //  return res is parseInt(params[1], 10)
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method UniqueCateoryRecencyCurrentQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  Profile.methods.UniqueCateoryRecencyCurrentQA = function(condition) {
    var params, res;
    params = condition.split('||');
    if (params[0] === 'avg') {
      res = this.UniqueCateoryRecencyCurrentAverageHighLow;
    } else {
      res = this.UniqueCateoryRecencyCurrentInQuintile;
    }
    return res === parseInt(params[1], 10);
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method UniqueProductRecencyQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  //Profile.methods.UniqueProductRecencyQA = (condition) ->
  //  params = condition.split '||'
  //  if params[0] is 'avg'
  //    res = @.UniqueProductRecencyAverageHighLow
  //  else
  //    res = @.UniqueProductRecencyInQuintile
  //  return res is parseInt(params[1], 10)
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method UniqueProductRecencyCurrentQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  Profile.methods.UniqueProductRecencyCurrentQA = function(condition) {
    var params, res;
    params = condition.split('||');
    if (params[0] === 'avg') {
      res = this.UniqueProductRecencyCurrentAverageHighLow;
    } else {
      res = this.UniqueProductRecencyCurrentInQuintile;
    }
    return res === parseInt(params[1], 10);
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method ProductRecencyQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  /*Profile.methods.ProductRecencyQA = (condition) ->
  params = condition.split '||'
  if params[0] is 'avg'
    res = @.ProductRecencyAverageHighLow
  else
    res = @.ProductRecencyInQuintile
  return res is parseInt(params[1], 10)*/
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method CateoryRecencyQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  //Profile.methods.CateoryRecencyQA = (condition) ->
  //  params = condition.split '||'
  //  if params[0] is 'avg'
  //    res = @.CateoryRecencyAverageHighLow
  //  else
  //    res = @.CateoryRecencyInQuintile
  //  return res is parseInt(params[1], 10)
  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method CateoryRecencyCurrentQA
   * @param condition
   * @deprecated Not in use. Found in test
   */
  Profile.methods.CateoryRecencyCurrentQA = function(condition) {
    var params, res;
    params = condition.split('||');
    if (params[0] === 'avg') {
      res = this.CateoryRecencyCurrentAverageHighLow;
    } else {
      res = this.CateoryRecencyCurrentInQuintile;
    }
    return res === parseInt(params[1], 10);
  };

  //Quintile and average virtual:
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property PageviewsInQuintile
   */
  Profile.virtual('PageviewsInQuintile').get(function() {
    var i, l, len1, q, ref1;
    i = 1;
    if (this.avg && (this.avg.Pageviews != null)) {
      ref1 = this.avg.Pageviews.quintile;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        q = ref1[l];
        if (this.CurrentPageViewCount > q) {
          i++;
        } else {
          return i;
        }
      }
    }
    return i;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property PageviewsAverageHighLow
   */
  Profile.virtual('PageviewsAverageHighLow').get(function() {
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.Pageviews != null)) {
      if (this.CurrentPageViewCount > this.avg.Pageviews.avg) {
        return 1;
      } else if (this.CurrentPageViewCount < this.avg.Pageviews.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  /*if @avg.PageviewsCurrent?
  if @CurrentPageViewCount > @avg.PageviewsCurrent.avg
    return 1
  else if @CurrentPageViewCount < @avg.PageviewsCurrent.avg
    return -1
  else
    return 0
  return null*/
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property VisitsInQuintile
   */
  Profile.virtual('VisitsInQuintile').get(function() {
    var i, l, len1, q, ref1;
    //Return quintile number
    i = 1;
    if (this.avg && (this.avg.Visits != null)) {
      ref1 = this.avg.Visits.quintile;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        q = ref1[l];
        if (this.VisitCount > q) {
          i++;
        }
      }
    }
    return i;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property VisitsAverageHighLow
   */
  Profile.virtual('VisitsAverageHighLow').get(function() {
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.Visits != null)) {
      if (this.VisitCount > this.avg.Visits.avg) {
        return 1;
      } else if (this.VisitCount < this.avg.Visits.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CategoriesInQuintile
   */
  //Profile.virtual('CategoriesInQuintile').get ->
  //  i = 1
  //  if @avg and @avg.CategoriesInVisit?
  //    for q in @avg.CategoriesInVisit.quintile
  //      if @CategoryCountCurrent > q
  //        i++
  //  return i
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CategoriesAverageHighLow
   */
  //Profile.virtual('CategoriesAverageHighLow').get ->
  //  #Return "high=1", "low=-1", "equals=0" or null
  //  if @avg and @avg.CategoriesInVisit?
  //    if @CategoryCountCurrent > @avg.CategoriesInVisit.avg
  //      return 1
  //    else if @CategoryCountCurrent < @avg.CategoriesInVisit.avg
  //      return -1
  //    else
  //      return 0
  //  return null
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueCategoriesInQuintile
   */
  //Profile.virtual('UniqueCategoriesInQuintile').get ->
  //  #Return quintile number
  //  i = 1
  //  if @avg and @avg.UniqueCategoriesInVisit?

  //    for q in @avg.UniqueCategoriesInVisit.quintile
  //      if @UniqueCategoryCountCurrent > q
  //        i++

  //  return i
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueCategoriesAverageHighLow
   */
  Profile.virtual('UniqueCategoriesAverageHighLow').get(function() {
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.UniqueCategoriesInVisit != null)) {
      if (this.UniqueCategoryCountCurrent > this.avg.UniqueCategoriesInVisit.avg) {
        return 1;
      } else if (this.UniqueCategoryCountCurrent < this.avg.UniqueCategoriesInVisit.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  Profile.virtual('TimeOnPageInQuintile').get(function() {
    var i, l, len1, q, ref1;
    //Return quintile number
    i = 1;
    if (this.avg && (this.avg.TimeOnPageInVisit != null)) {
      if (this.avg.TimeOnPageInVisit.quintile) {
        ref1 = this.avg.TimeOnPageInVisit.quintile;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          q = ref1[l];
          if (this.AverageTimeOnPage > q) {
            i++;
          }
        }
      }
    }
    return i;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property TimeOnPageAverageHighLow
   */
  Profile.virtual('TimeOnPageAverageHighLow').get(function() {
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.TimeOnPageInVisit != null)) {
      if (this.AverageTimeOnPage > this.avg.TimeOnPageInVisit.avg) {
        return 1;
      } else if (this.AverageTimeOnPage < this.avg.TimeOnPageInVisit.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property VisitDurationInQuintile
   */
  Profile.virtual('VisitDurationInQuintile').get(function() {
    var i, l, len1, q, ref1;
    //Return quintile number
    i = 1;
    if (this.avg && (this.avg.LenghtOfVisit != null)) {
      ref1 = this.avg.LenghtOfVisit.quintile;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        q = ref1[l];
        if (this.VisitDuration > q) {
          i++;
        }
      }
    }
    return i;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property VisitDurationAverageHighLow
   */
  Profile.virtual('VisitDurationAverageHighLow').get(function() {
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.LenghtOfVisit != null)) {
      if (this.VisitDuration > this.avg.LenghtOfVisit.avg) {
        return 1;
      } else if (this.VisitDuration < this.avg.LenghtOfVisit.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });


  Profile.virtual('UniqueCateoryRecencyCurrentInQuintile').get(function() {
    var i, l, len1, q, ref1;
    //Return quintile number
    i = 1;
    if (this.avg && (this.avg.UniqueCategoryRecencyCurrent != null)) {
      ref1 = this.avg.UniqueCategoryRecencyCurrent.quintile;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        q = ref1[l];
        if (this.CategoryRecency(2, true, true) > q) {
          i++;
        }
      }
    }
    return i;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueCateoryRecencyCurrentAverageHighLow
   */
  Profile.virtual('UniqueCateoryRecencyCurrentAverageHighLow').get(function() {
    var recency;
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.UniqueCategoryRecencyCurrent != null)) {
      recency = this.CategoryRecency(2, true, true);
      if (recency > this.avg.UniqueCategoryRecencyCurrent.avg) {
        return 1;
      } else if (recency < this.avg.UniqueCategoryRecencyCurrent.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueProductRecencyInQuintile
   */
  //Profile.virtual('UniqueProductRecencyInQuintile').get ->
  //  #Return quintile number
  //  i = 1
  //  if @avg and @avg.UniqueProductRecency?
  //    for q in @avg.UniqueProductRecency.quintile
  //      if @CategoryRecency(1,true,false) > q
  //        i++
  //  return i
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueProductRecencyAverageHighLow
   */
  //Profile.virtual('UniqueProductRecencyAverageHighLow').get ->
  //  #Return "high=1", "low=-1", "equals=0" or null
  //  if @avg and @avg.UniqueProductRecency?
  //    recency = @CategoryRecency(1,true,false)
  //    if recency > @avg.UniqueProductRecency.avg
  //      return 1
  //    else if recency < @avg.UniqueProductRecency.avg
  //      return -1
  //    else
  //      return 0
  //  return null
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueProductRecencyCurrentInQuintile
   */
  Profile.virtual('UniqueProductRecencyCurrentInQuintile').get(function() {
    var i, l, len1, q, ref1;
    //Return quintile number
    i = 1;
    if (this.avg && (this.avg.UniqueProductRecencyCurrent != null)) {
      ref1 = this.avg.UniqueProductRecencyCurrent.quintile;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        q = ref1[l];
        if (this.CategoryRecency(1, true, true) > q) {
          i++;
        }
      }
    }
    return i;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueProductRecencyCurrentAverageHighLow
   */
  Profile.virtual('UniqueProductRecencyCurrentAverageHighLow').get(function() {
    var recency;
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.UniqueProductRecencyCurrent != null)) {
      recency = this.CategoryRecency(1, true, true);
      if (recency > this.avg.UniqueProductRecencyCurrent.avg) {
        return 1;
      } else if (recency < this.avg.UniqueProductRecencyCurrent.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property ProductRecencyInQuintile
   */
  //Profile.virtual('ProductRecencyInQuintile').get ->
  //  #Return quintile number
  //  i = 1
  //  if @avg and @avg.ProductRecency?
  //    for q in @avg.ProductRecency.quintile
  //      if @CategoryRecency(1,false,false) > q
  //        i++
  //  return i
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property ProductRecencyAverageHighLow
   */
  Profile.virtual('ProductRecencyAverageHighLow').get(function() {
    var recency;
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.ProductRecency != null)) {
      recency = this.CategoryRecency(1, false, false);
      if (recency > this.avg.ProductRecency.avg) {
        return 1;
      } else if (recency < this.avg.ProductRecency.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property ProductRecencyCurrentInQuintile
   */
  Profile.virtual('ProductRecencyCurrentInQuintile').get(function() {
    var i, l, len1, q, ref1;
    //Return quintile number
    i = 1;
    if (this.avg && (this.avg.ProductRecencyCurrent != null)) {
      ref1 = this.avg.ProductRecencyCurrent.quintile;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        q = ref1[l];
        if (this.CategoryRecency(1, false, true) > q) {
          i++;
        }
      }
    }
    return i;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property ProductRecencyCurrentAverageHighLow
   */
  Profile.virtual('ProductRecencyCurrentAverageHighLow').get(function() {
    var recency;
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.ProductRecencyCurrent != null)) {
      recency = this.CategoryRecency(1, false, true);
      if (recency > this.avg.ProductRecencyCurrent.avg) {
        return 1;
      } else if (recency < this.avg.ProductRecencyCurrent.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CateoryRecencyInQuintile
   */
  //Profile.virtual('CateoryRecencyInQuintile').get ->
  //  #Return quintile number
  //  i = 1
  //  if @avg and @avg.CategoryRecency?
  //    for q in @avg.CategoryRecency.quintile
  //      if @CategoryRecency(2,false,false) > q
  //        i++
  //  return i
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CateoryRecencyAverageHighLow
   */
  //Profile.virtual('CateoryRecencyAverageHighLow').get ->
  //  #Return "high=1", "low=-1", "equals=0" or null
  //  if @avg and @avg.CategoryRecency?
  //    recency = @CategoryRecency(2,false,false)
  //    if recency > @avg.CategoryRecency.avg
  //      return 1
  //    else if recency < @avg.CategoryRecency.avg
  //      return -1
  //    else
  //      return 0
  //  return null
  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CateoryRecencyCurrentInQuintile
   */
  Profile.virtual('CateoryRecencyCurrentInQuintile').get(function() {
    var i, l, len1, q, ref1;
    //Return quintile number
    i = 1;
    if (this.avg && (this.avg.CategoryRecencyCurrent != null)) {
      ref1 = this.avg.CategoryRecencyCurrent.quintile;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        q = ref1[l];
        if (this.CategoryRecency(2, false, true) > q) {
          i++;
        }
      }
    }
    return i;
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CateoryRecencyCurrentAverageHighLow
   */
  Profile.virtual('CateoryRecencyCurrentAverageHighLow').get(function() {
    var recency;
    //Return "high=1", "low=-1", "equals=0" or null
    if (this.avg && (this.avg.CategoryRecencyCurrent != null)) {
      recency = this.CategoryRecency(2, false, true);
      if (recency > this.avg.CategoryRecencyCurrent.avg) {
        return 1;
      } else if (recency < this.avg.CategoryRecencyCurrent.avg) {
        return -1;
      } else {
        return 0;
      }
    }
    return null;
  });

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method CategoryRecency
   * @param type
   * @param uinque {boolean}
   * @param current
   * @deprecated Not in use. Found in test
   */
  Profile.methods.CategoryRecency = function(type = 2, unique = false, current = false) {
    var err, k, l, latestItems, latestTimeStamp, len1, profile, ref1, ref2, ref3, v, visit;
    try {
      latestItems = [];
      latestTimeStamp = 0;
      if (!current) {
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          if (profile.visits && profile.visits.length > 0) {
            ref1 = profile.visits;
            for (l = 0, len1 = ref1.length; l < len1; l++) {
              visit = ref1[l];
              ref2 = visit.categories;
              for (k in ref2) {
                v = ref2[k];
                if (parseInt(v.t) === type) {
                  if (latestItems[k]) {
                    if (visit.lastclick > latestItems[k]) {
                      latestItems[k] = visit.lastclick;
                    }
                  } else {
                    latestItems[k] = visit.lastclick;
                  }
                  latestTimeStamp = visit.lastclick;
                }
              }
            }
          }
        }
      } else {
        if ((this.lastVisit != null) && (this.lastVisit.categories != null)) {
          ref3 = this.lastVisit.categories;
          for (k in ref3) {
            v = ref3[k];
            if (parseInt(v.t) === type) {
              if (latestItems[k]) {
                if (this.lastVisit.lastclick > latestItems[k]) {
                  latestItems[k] = this.lastVisit.lastclick;
                }
              } else {
                latestItems[k] = this.lastVisit.lastclick;
              }
              latestTimeStamp = this.lastVisit.lastclick;
            }
          }
        }
      }
      if (unique) {
        if (latestItems.length === 0) {
          return null;
        } else {
          return Math.round(new Date().getTime() / 1000) - Math.round(latestItems[latestItems.length - 1]);
        }
      } else {
        return Math.round(new Date().getTime() / 1000) - Math.round(latestTimeStamp);
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CategoryRecency(ID:" + this.id + ") - " + err);
      return null;
    }
  };

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CategoryCountCurrent
   */
  Profile.virtual('CategoryCountCurrent').get(function() {
    var c, err, k, ref1, v;
    try {
      c = 0;
      if ((this.lastVisit != null) && (this.lastVisit.categories != null)) {
        ref1 = this.lastVisit.categories;
        for (k in ref1) {
          v = ref1[k];
          if (parseInt(v.t) === 2) {
            c += v.c;
          }
        }
      }
      return c;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CategoryCountCurrent(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueCategoryCountCurrent
   */
  Profile.virtual('UniqueCategoryCountCurrent').get(function() {
    var arrCategory, c, err, k, ref1, v;
    try {
      c = 0;
      arrCategory = [];
      if ((this.lastVisit != null) && (this.lastVisit.categories != null)) {
        ref1 = this.lastVisit.categories;
        for (k in ref1) {
          v = ref1[k];
          if (parseInt(v.t) === 2 && arrCategory.indexOf(v.g) === -1) {
            arrCategory.push(v.g);
            c++;
          }
        }
      }
      return c;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->UniqueCategoryCountCurrent(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property ProductCountCurrent
   */
  Profile.virtual('ProductCountCurrent').get(function() {
    var c, err, k, ref1, v;
    try {
      c = 0;
      if ((this.lastVisit != null) && (this.lastVisit.categories != null)) {
        ref1 = this.lastVisit.categories;
        for (k in ref1) {
          v = ref1[k];
          if (v.t === 1) {
            c += v.c;
          }
        }
      }
      return c;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->ProductCountCurrent(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueProductCountCurrent
   */
  Profile.virtual('UniqueProductCountCurrent').get(function() {
    var arrProduct, c, err, k, ref1, v;
    try {
      c = 0;
      arrProduct = [];
      if ((this.lastVisit != null) && (this.lastVisit.categories != null)) {
        ref1 = this.lastVisit.categories;
        for (k in ref1) {
          v = ref1[k];
          if (v.t === 1 && arrProduct.indexOf(v.g) === -1) {
            arrProduct.push(v.g);
            c++;
          }
        }
      }
      return c;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->UniqueProductCountCurrent(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property UniqueProductPrCateory
   */
  Profile.virtual('UniqueProductPrCateory').get(function() {
    var arrCategory, arrProduct, c, e, k, l, len1, p, profile, ref1, ref2, v, visit;
    try {
      c = 0;
      p = 0;
      arrProduct = [];
      arrCategory = [];
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (profile.visits && profile.visits.length > 0) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            ref2 = visit.categories;
            for (k in ref2) {
              v = ref2[k];
              if (parseInt(v.t) === 2 && arrCategory.indexOf(v.g) === -1) {
                arrCategory.push(v.g);
                c++;
              } else if (parseInt(v.t) === 1 && arrProduct.indexOf(v.g) === -1) {
                arrProduct.push(v.g);
                p++;
              }
            }
          }
        }
      }
      if (c === 0) {
        return 0;
      }
      return p / c;
    } catch (error) {
      e = error;
      console.error(new Date() + ": Model->Profile->UniqueProductPrCateory(ID:" + this.id + ") - " + e);
      return 0;
    }
  });

  /**
   * Description : Form request add page view to current profile.update lastclik time, existpage, url , urlref and add new object(view)
   * to current object.
   * @method addPageView
   * @param request {Object} request object
   */
  Profile.methods.addPageView = function(request) {
    var err, key, l, len1, now, profile, ref1, ref_qs, results, that, url_qs, view, visit;
    try {
      now = new Date().getTime() / 1000;
      that = this;
      ref1 = this.profiles;
      results = [];
      for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
        profile = ref1[key];
        if (profile._id.toString() === that.recently_used_device) {
          visit = profile.visits.id(request.query.vid);
          if (visit) {
            visit.exitpage = request.query.url;
            visit.lastclick = now;
            visit.clicks++;
          }
          //Check if referer is different from url domain:
          if (request.query.urlref) {
            url_qs = Url.parse(request.query.url);
            ref_qs = Url.parse(request.query.urlref);
            if (url_qs.host !== ref_qs.host) {
              visit.referers.push(request.query.urlref);
            }
          }
          //Build "views"
          view = {
            url: request.query.url,
            extRef: request.query.urlref,
            localTime: request.query.ts,
            timeStamp: now
          };
          //As discussed with Kresten timeStamp for a page view will always be frontend server time stamp
          //meta: []
          view.timeStamp = now;
          //Add data to "CurrentVisit"
          results.push(profile.current.views.push(view));
        } else {
          results.push(void 0);
        }
      }
      return results;
    } catch (error) {
      err = error;
      return console.error(new Date() + ": Model->Profile->addPageView(ID:" + this.id + " / request.mid:" + request.query.mid + "/ request.vid:" + request.query.vid + ") - " + err);
    }
  };

  /**
   * Description : Migrate oldProfile to new profile structure
   * @method migrateToNewStructure
   * @param request {object}
   * @param oldProfile {boolean}
   * @deprecated Not in use. Found in test
   */
  Profile.methods.migrateToNewStructure = function(request, oldProfile) {
    var err, headers, now, params, profile, profileID, ua;
    params = request.query;
    headers = request.headers;
    if (headers['user-agent']) {
      ua = uaParser.parse(headers['user-agent']);
    }
    now = new Date().getTime() / 1000;
    profileID = new mongoose.Types.ObjectId;
    oldProfile = oldProfile[0].toObject();
    this.aggregated = null;
    this.recently_used_device = profileID;
    this.version = 2;
    //@.cachedProperties = oldProfile.cachedProperties
    profile = sub_profile_schema;
    profile._id = profileID;
    profile.device_info = {};
    //First update all root preferences:
    profile.device_info.config_resolution = params.res; // marc // added resolution
    profile.device_info.config_cookie = rootObj['cookie'] = 1;
    profile.device_info.config_flash = params.fla;
    profile.device_info.config_java = params.java;
    profile.device_info.config_gears = params.gears;
    profile.device_info.config_silverlight = params.ag;
    profile.device_info.config_director = params.dir;
    profile.device_info.config_windowsmedia = params.wma;
    profile.device_info.config_realplayer = params.realp;
    profile.device_info.config_quicktime = params.qt;
    profile.device_info.config_pdf = params.pdf;
    profile.experiments = oldProfile.experiments;
    profile.controlgroups = oldProfile.controlgroups;
    profile.goals = oldProfile.goals;
    try {
      profile.device_info.config_os = ua.os;
      profile.device_info.config_browser_name = ua.family;
      profile.device_info.config_browser_version = ua.major + '.' + ua.minor + '.' + ua.patch;
    } catch (error) {
      err = error;
      console.error(new Date() + ": model->profile->addProfile(ID:" + this.id + ") - " + err);
    }
    profile.visit_server_date = now;
    this.profiles.push(profile);
    this.addVisitMigration(request, oldProfile.visits);
    return {profileID};
  };

  /**
   * Description : Add new profile. Also update some properties base of adding new profile or reinserting profile.
   * If adding new profile then also update device_info properties
   * @method addProfile
   * @param request {Object} request object
   */
  Profile.methods.addProfile = function(request) {
    var _profileIndex, alreadyAdded, err, headers, index, l, len1, now, p, params, profile, profileID, ref1, ua;
    params = request.query;
    headers = request.headers;
    if (headers['user-agent']) {
      ua = uaParser.parse(headers['user-agent']);
    }
    now = new Date().getTime() / 1000;
    profileID = new mongoose.Types.ObjectId;
    this.aggregated = null;
    this.recently_used_device = profileID;
    this.version = 2;
    alreadyAdded = false;
    _profileIndex = null;
    ref1 = this.profiles;
    for (index = l = 0, len1 = ref1.length; l < len1; index = ++l) {
      p = ref1[index];
      if (p !== void 0 && p._id.toString() === params.mid) {
        //@.profiles.splice index
        _profileIndex = index;
        if (this.privacy) {
          if (this.privacy.lastProfileDelete) {
            this.privacy.lastProfileDelete = null;
          }
        }
        profile = sub_profile_schema;
        profile._id = mongoose.Types.ObjectId(params.mid);
        profile.device_info = {};
        //First update all root preferences:
        profile.device_info.config_resolution = params.res; // marc // added resolution
        profile.device_info.config_cookie = rootObj['cookie'] = 1;
        profile.device_info.config_flash = params.fla;
        profile.device_info.config_java = params.java;
        profile.device_info.config_gears = params.gears;
        profile.device_info.config_silverlight = params.ag;
        profile.device_info.config_director = params.dir;
        profile.device_info.config_windowsmedia = params.wma;
        profile.device_info.config_realplayer = params.realp;
        profile.device_info.config_quicktime = params.qt;
        profile.device_info.config_pdf = params.pdf;
        profile.experiments = {};
        profile.controlgroups = {};
        try {
          profile.device_info.config_os = ua.os;
          profile.device_info.config_browser_name = ua.family;
          profile.device_info.config_browser_version = ua.major + '.' + ua.minor + '.' + ua.patch;
        } catch (error) {
          err = error;
          console.error(new Date() + ": model->profile->addProfile(ID:" + this.id + ") - " + err);
        }
        profile.visit_server_date = now;
        this.recently_used_device = mongoose.Types.ObjectId(params.mid);
        alreadyAdded = true;
      }
    }
    if (!alreadyAdded) {
      profile = sub_profile_schema;
      profile._id = profileID;
      profile.device_info = {};
      //First update all root preferences:
      profile.device_info.config_resolution = params.res; // marc // added resolution
      profile.device_info.config_cookie = rootObj['cookie'] = 1;
      profile.device_info.config_flash = params.fla;
      profile.device_info.config_java = params.java;
      profile.device_info.config_gears = params.gears;
      profile.device_info.config_silverlight = params.ag;
      profile.device_info.config_director = params.dir;
      profile.device_info.config_windowsmedia = params.wma;
      profile.device_info.config_realplayer = params.realp;
      profile.device_info.config_quicktime = params.qt;
      profile.device_info.config_pdf = params.pdf;
      profile.experiments = {};
      profile.controlgroups = {};
      try {
        profile.device_info.config_os = ua.os;
        profile.device_info.config_browser_name = ua.family;
        profile.device_info.config_browser_version = ua.major + '.' + ua.minor + '.' + ua.patch;
      } catch (error) {
        err = error;
        console.error(new Date() + ": model->profile->addProfile(ID:" + this.id + ") - " + err);
      }
      profile.visit_server_date = now;
    }
    if (_profileIndex !== null) {
      this.profiles.splice(_profileIndex, 1, profile);
    } else {
      this.profiles.push(profile);
    }
    return profile._id;
  };

  /**
   * Description : Get device info from request header and params
   * @method getDeviceInfo
   * @param request {Object} request object
   * @return {Object} device_info object
   */
  Profile.methods.getDeviceInfo = function(request) {
    var err, headers, params, ua;
    params = request.query;
    headers = request.headers;
    if (headers['user-agent']) {
      ua = uaParser.parse(headers['user-agent']);
    }
    device_info = {};
    //First update all root preferences:
    device_info.config_resolution = params.res; // marc // added resolution
    device_info.config_cookie = rootObj['cookie'] = 1;
    device_info.config_flash = params.fla;
    device_info.config_java = params.java;
    device_info.config_gears = params.gears;
    device_info.config_silverlight = params.ag;
    device_info.config_director = params.dir;
    device_info.config_windowsmedia = params.wma;
    device_info.config_realplayer = params.realp;
    device_info.config_quicktime = params.qt;
    device_info.config_pdf = params.pdf;
    try {
      device_info.config_os = ua.os;
      device_info.config_browser_name = ua.family;
      device_info.config_browser_version = ua.major + '.' + ua.minor + '.' + ua.patch;
    } catch (error) {
      err = error;
      console.error(new Date() + ": model->profile-getDeviceInfo(ID:" + this.id + ") - " + err);
    }
    return device_info;
  };

  Profile.methods.insertUIDAndMigrate = function(request, callback) {
    var that;
    if (request.query.uniqueID) {
      that = this;
      return this.db.model('profiles').find({
        uniqueID: request.query.uniqueID,
        customerID: request.query.cid
      }, function(err, result) {
        if (result && result[0]) {
          debug(that.customerID);
          result[0].customerID = that.customerID;
          result[0].pgmid = that.pgmid;
          result[0].ngmid = that.ngmid;
          result[0].cv = that.cv;
          result[0].co = that.co;
          result[0].form = that.form;
          result[0].OCR = that.OCR;
          result[0].privacy = that.privacy;
          //result[0].cachedProperties = that.cachedProperties
          result[0].profiles.push(that.profiles[0]);
          result[0].recently_used_device = that.recently_used_device;
          return callback(null, result);
        } else {
          return callback(null, null);
        }
      });
    } else {
      return callback(null, null);
    }
  };

  /**
   * Description : Migrate old profile visit structure to new profile visit structure.
   * @method addVisitMigration
   * @param request {object}
   * @param visit {object}
   * @deprecated Used in profile migration from old to new structure
   */
  Profile.methods.addVisitMigration = function(request, visits) {
    var _index, headers, now, params, that, ua;
    params = request.query;
    headers = request.headers;
    if (headers['user-agent']) {
      ua = uaParser.parse(headers['user-agent']);
    }
    now = new Date().getTime() / 1000;
    that = this;
    _index = this.getIndex();
    return visits.map(function(oldVisit) {
      var visit;
      visit = visit_schema;
      visit.loc_info = {};
      //Visit:
      visit._id = new mongoose.Types.ObjectId;
      visit.clicks = oldVisit.clicks;
      visit.entrypage = oldVisit.entrypage;
      visit.exitpage = oldVisit.exitpage;
      visit.firstclick = oldVisit.firstclick;
      visit.lastclick = oldVisit.lastclick;
      visit.referers = oldVisit.referers;
      visit.referer = oldVisit.referer;
      visit.achivedGoal = oldVisit.achivedGoal;
      visit.pv = oldVisit.pv;
      visit.feedback = oldVisit.feedback;
      visit.outlink = oldVisit.outlink;
      visit.points = oldVisit.points;
      visit.downloads = oldVisit.downloads;
      visit.searches = oldVisit.searches;
      visit.carts = oldVisit.carts;
      visit.meta = oldVisit.meta;
      visit.categories = oldVisit.categories;
      visit.experiments = oldVisit.experiments;
      visit.content = oldVisit.content;
      visit.segments = oldVisit.segments;
      visit.segments_InOut = oldVisit.segments_InOut;
      return that.profiles[_index].visits.push(visit);
    });
  };

  /**
   * Description : Add new visit to current profile.
   * <li> Update loc_info in visit form request header </li>
   * <li>Push current view in current views array</li>
   * @method addVisit
   * @param request {Object} request object
   */
  Profile.methods.addVisit = function(request) {
    var _index, err, headers, key, l, len1, now, params, profile, ref1, ref_qs, that, ua, url_qs, view, visit, visitID;
    try {
      params = request.query;
      headers = request.headers;
      if (headers['user-agent']) {
        ua = uaParser.parse(headers['user-agent']);
      }
      now = new Date().getTime() / 1000;
      that = this;
      _index = this.getIndex();
      visitID = new mongoose.Types.ObjectId;
      ref1 = this.profiles;
      for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
        profile = ref1[key];
        if (profile._id.toString() === that.recently_used_device) {
          visit = visit_schema;
          visit.loc_info = {};
          //Visit:
          visit._id = visitID;

          let ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
          ip = ip.split(',')[0];
          //ip = '49.49.140.134';
          debug(ip);
          var geo = geoip.lookup(ip);

          if(geo !== null){
            if (geo.country in countries) {
              geo.continent = countries[geo.country].continent;
            }
            debug(geo);

            if (ip) {
              visit.loc_info.loc_ip = ip;
            }

            if (geo.ll.length == 2) {
              visit.loc_info.loc_lat = geo.ll[0];
            }
            if (geo.ll.length == 2) {
              visit.loc_info.loc_long = geo.ll[1];
            }

            if (geo.country) {
              visit.loc_info.loc_country = geo.country;
            }
            if (geo.city) {
              visit.loc_info.loc_city = geo.city;
            }
            if (geo.continent) {
              visit.loc_info.loc_continent = geo.continent;
            }
            if (geo.region) {
              visit.loc_info.loc_region = geo.region;
            }
          }

          visit.clicks = 1;
          visit.entrypage = params.url;
          visit.exitpage = params.url;
          if (that.profiles[_index].current.views.length === 0) {
            visit.firstclick = now;
          }
          visit.lastclick = visit.firstclick;
          visit.referers = "";
          visit.referer = "";
          //Check if referer is different from url domain:
          if (params.urlref) {
            url_qs = Url.parse(params.url);
            ref_qs = Url.parse(params.urlref);
            if (url_qs.host !== ref_qs.host) {
              visit.referers = params.urlref;
              visit.referer = params.urlref;
            }
          }
          view = {
            url: request.query.url,
            extRef: request.query.urlref,
            localTime: request.query.ts,
            timeStamp: now
          };
          //meta: []
          view.timeStamp = now;
          that.profiles[_index].visits.push(visit);
          that.profiles[_index].current.views.push(view);
        }
      }
      return visitID;
    } catch (error) {
      err = error;
      return console.error(new Date() + ": Model->Profile->addVisit(ID:" + this.id + ") - " + err);
    }
  };

  /**
   * Description : Using recently used profile get last visit index.
   * @method getLastVisitIndex
   * @return {number|Null} if profile does not exist its return null.
   */
  Profile.methods.getLastVisitIndex = function() {
    var profile;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits.length) {
        return profile.visits.length - 1;
      } else {
        return 0;
      }
    } else {
      return null;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method compare_visit
   * @param a
   * @param b
   * @deprecated Not in use. Found in test
   */
  Profile.methods.compare_visit = function(a, b) {
    return strnatcmp(b.tstamp, a.tstamp);
  };

  /**
   * Description : Check specific goal reached or not.
   * @method GoalReached
   * @param goal_id {Number} Id of goal
   * return {boolean} if goal reached return true else return false.
   */
  Profile.methods.GoalReached = function(goal_id) {
    var err, goal, l, len1, profile, ref1;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (profile.goals.length > 0) {
          ref1 = profile.goals;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            goal = ref1[l];
            if (parseInt(goal_id) === parseInt(goal['uid'])) {
              return true;
            }
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->GoalReached(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method EngagementScore
   * @param days
   * @param visits
   * @param brands
   * @param minutes
   * @param pageviews
   * @param date
   * @param allScores
   * @deprecated Not in use. Found in test and archive controller. Currently archive controller is not in use
   */
  Profile.methods.EngagementScore = function(days, visits, brands, minutes, pageviews, date = null, allScores = false) {
    var ES, err, key, out, sum, value;
    try {
      if (allScores) {
        sum = 0;
        out = {
          SubscriptionIndex: this.SubscriptionIndex(days, date) * 100,
          LoyaltyIndex: this.LoyaltyIndex(visits, days, date) * 100,
          BrandIndex: this.BrandIndex(brands, date),
          DurationIndex: this.DurationIndex(minutes, date),
          RecencyIndex: this.RecencyIndex(pageviews, days, date),
          ClickDepthIndex: this.ClickDepthIndex(pageviews, date),
          FeedbackIndex: this.getFeedbackIndex(),
          InteractionIndex: this.getInteractionIndex()
        };
        for (key in out) {
          value = out[key];
          sum += value;
        }
        out.EngagementIndex = parseFloat((sum / 8).toFixed(2));
        return out;
      }
      ES = parseFloat(((this.SubscriptionIndex(days, date) * 100 + this.LoyaltyIndex(visits, days, date) * 100 + this.BrandIndex(brands, date) + this.DurationIndex(minutes, date) + this.RecencyIndex(pageviews, days, date) + this.ClickDepthIndex(pageviews, date) + this.getFeedbackIndex() + this.getInteractionIndex()) / 8).toFixed(2));
      return ES;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->EngagementScore(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : Calculate total time and total revisit after specific experiment.
   * @method totalRevisitsExperiments
   * @param experimentID {Number} Id of experiment.
   * return revisits {Object} contain revisit info, time info.
   */
  Profile.methods.totalRevisitsExperiments = function(experimentID) {
    var date, distance, distanceBV, experimentTrigger, index, indexOccurence, l, lastClick, lastVisitKey, lastVisitlastClickHour, len1, prevVisitFirstClickHour, previousVisit, profile, ref1, revisits, visit;
    revisits = {};
    experimentTrigger = false;
    revisits.rV = 0;
    revisits.revistTTOS = 0;
    revisits.revistTOS = 0;
    profile = this.getRecentDevicePorfile();
    indexOccurence = [];
    if (!(util.isNull(profile))) {
      lastVisitKey = profile.lastVisitKey;
      if (profile.visits && typeof profile.visits !== void 0) {
        distance = profile.visits.length;
        if (profile.visits.length >= 1) {
          ref1 = profile.visits;
          for (index = l = 0, len1 = ref1.length; l < len1; index = ++l) {
            visit = ref1[index];
            distance = distance - 1;
            if (distance === 0 && experimentTrigger) {
              if (lastVisitKey.toString() === visit._id.toString()) {
                if (profile.current.views.length > 1) {
                  revisits.revistTTOS = profile.current.views[profile.current.views.length - 1].timeStamp - profile.current.views[profile.current.views.length - 2].timeStamp;
                }
              }
            }
            //for key, exp of visit.experiments
            if ((visit.experiments != null) && (visit.experiments[experimentID] != null) && visit.experiments[experimentID].isPartOfExperiment === true) {
              if (visit._id !== lastVisitKey && distance !== 0) {
                experimentTrigger = true;
                indexOccurence.push(index);
              }
            }
          }
        }
        if (experimentTrigger) {
          revisits.rV = 1;
        }
        if (indexOccurence.length > 0) {
          distanceBV = (profile.visits.length - 1) - indexOccurence[0];
          if (distanceBV > 1 && experimentTrigger && profile.lastVisitKey !== this.lastVisit._id.toString()) {
            date = new Date(this.previousVisit.lastclick * 1000);
            prevVisitFirstClickHour = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours()).getTime() / 1000;
            lastClick = new Date(this.lastVisit.lastclick * 1000);
            lastVisitlastClickHour = new Date(lastClick.getFullYear(), lastClick.getMonth(), lastClick.getDate(), lastClick.getHours()).getTime() / 1000;
            if (lastVisitlastClickHour === prevVisitFirstClickHour) {
              previousVisit = (new Date(this.previousVisit.lastclick * 1000).getTime() - new Date(this.previousVisit.firstclick * 1000).getTime()) / 1000;
              //if use float value it some time calculate negative value which is very small
              previousVisit = parseInt(previousVisit) * (-1);
              revisits.revistTOS = previousVisit;
            }
          } else {
            revisits.revistTOS = revisits.revistTTOS;
          }
        }
      }
    }
    return revisits;
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method CategoryIndex
   * @param days
   * @param trackerID
   * @param category
   * @param date
   */
  Profile.methods.CategoryIndex = function(days, trackerID, category, date = null) {
    var CategoryIndex, catClickCounter, catTimeCounter, catkey, catval, count, err, l, len1, profile, ref1, ref2, ref3, ref4, ref5, totalClickCounter, totalTimeCounter, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      count = 0;
      totalClickCounter = 0;
      totalTimeCounter = 0;
      catClickCounter = 0;
      catTimeCounter = 0;
      if (!(util.isNull(profile))) { //and profile.length > 0
        //profile = profile[0].toJSON()
        visits = profile.visits;
//category = "#{trackerID}_#{category}" # crc32 function has been removed
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          //Check if we need to filter out some visits due to $date != null
          if (date && visit.firstclick > date) {
            continue; // Don`t sure if "continue" works at coffee
          }
          if (visit.firstclick > (Math.floor(new Date().getTime() / 1000) - (days * 24 * 60 * 60))) {
            //valid visit:
            if (visit.categories) {
              ref1 = visit.categories;
              //categories exsits:
              //echo 'ID: '.$this->LastClick;
              //print_r($visit['categories']);
              //die($this->LastClick);
              for (catkey in ref1) {
                catval = ref1[catkey];
                //Inc total clicks and duration:
                totalClickCounter += (ref2 = catval.c) != null ? ref2 : 0;
                totalTimeCounter += (ref3 = catval['d']) != null ? ref3 : 0;
                //Check if current equals selected category and add counter for category:
                if (category === catkey) {
                  catClickCounter += (ref4 = catval['c']) != null ? ref4 : 0;
                  catTimeCounter += (ref5 = catval['d']) != null ? ref5 : 0;
                }
              }
            }
          }
        }
      }
      CategoryIndex = (totalClickCounter > 0 && totalTimeCounter > 0) ? (((catClickCounter / totalClickCounter) * 100) + ((catTimeCounter / totalTimeCounter) * 100)) / 2 : 0;
      return CategoryIndex;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CategoryIndex(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : All code inside this function is in comment.
   * @method SubscriptionIndex
   * @param days
   * @param date
   * @deprecated Not in use.
   */
  Profile.methods.SubscriptionIndex = function(days, date = null) {
    //SubscriptionIndex = if @Monetary.lenght > 0 then 1 else 0 # !!! Some undefined in database scheme variable
    return 0; //SubscriptionIndex
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method LoyaltyIndex
   * @param min_visits
   * @param days
   * @param date
   */
  Profile.methods.LoyaltyIndex = function(min_visits, days, date = null) {
    var LT, count, err, l, len1, loyaltycounter, profile, visit, visits;
    try {
      //create temp visit array:
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        count = 0;
        loyaltycounter = 0;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (date && visit.firstclick > date) {
            //Check if we need to filter out some visits due to $date != null
            continue;
          }
          if (visit.firstclick > (Math.floor(new Date().getTime() / 1000) - (days * 24 * 60 * 60))) {
            loyaltycounter++;
          }
          count++;
        }
        LT = loyaltycounter >= min_visits ? 1 : 0;
        return LT;
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->LoyaltyIndex(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method BrandIndex
   * @param brands
   * @param date
   */
  Profile.methods.BrandIndex = function(brands = '', date = null) {
    var BrandIndex, arr_brands, brandcounter, count, err, l, len1, profile, visit, visits;
    try {
      //create temp visit array:
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        count = 0;
        arr_brands = brands.split(',');
        brandcounter = 0;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (date && visit.firstclick > date) {
            continue;
          }
          if (visit.referer === '' || this.isBrandReferer(visit.referer, arr_brands)) {
            brandcounter++;
          }
          count++;
        }
        BrandIndex = count > 0 ? parseFloat((brandcounter / count).toFixed(2)) : 0;
        return BrandIndex * 100;
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->BrandIndex(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method DurationIndex
   * @param minutes
   * @param date
   */
  Profile.methods.DurationIndex = function(minutes, date = null) {
    var DurationIndex, count, duration_over, err, l, len1, profile, visit, visits;
    try {
      //create temp visit array:
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        duration_over = 0;
        count = 0;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (date && visit.firstclick > date) {
            //Check if we need to filter out some visits due to $date != null
            continue;
          }
          if ((visit.lastclick - visit.firstclick / 60) > minutes) {
            duration_over++;
          }
          count++;
        }
        DurationIndex = count > 0 ? parseFloat((duration_over / count).toFixed(2)) : 0;
        return DurationIndex * 100;
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->DurationIndex(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method RecencyIndex
   * @param pageviews
   * @param days
   * @param date
   */
  Profile.methods.RecencyIndex = function(pageviews, days, date = null) {
    var RecencyIndex, count, err, l, len1, profile, visit, visitCounter, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        visitCounter = 0;
        count = 0;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (date && visit.firstclick > date) {
            //Check if we need to filter out some visits due to $date != null
            continue;
          }
          if (visit.clicks > pageviews && visit.firstclick > (Math.floor(new Date().getTime() / 1000) - (days * 24 * 60 * 60))) {
            visitCounter++;
          }
          count++;
        }
        RecencyIndex = count > 0 ? parseFloat((visitCounter / count).toFixed(2)) : 0;
      }
      return RecencyIndex * 100;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->RecencyIndex(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method ClickDepthIndex
   * @param pageviews
   * @param date
   */
  Profile.methods.ClickDepthIndex = function(pageviews, date = null) {
    var ClickDepthIndex, count, err, l, len1, profile, visit, visitCounter, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        visitCounter = 0;
        count = 0;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (date && visit.firstclick > date) {
            continue;
          }
          if (visit.clicks > pageviews) {
            visitCounter++;
          }
          count++;
        }
        ClickDepthIndex = count > 0 ? parseFloat((visitCounter / count).toFixed(2)) : 0;
      }
      return ClickDepthIndex * 100;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->ClickDepthIndex(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method currentIsTopCategory
   * @param trackerID
   * @param categories
   */
  Profile.methods.currentIsTopCategory = function(trackerID, categories) {
    var err, v_categories, visit;
    try {
      visit = this.lastVisit;
      if (visit && (visit.categories != null) && _.isArray(visit.categories)) {
        //sort categories by (time+clicks)/2
        v_categories = this.sortCategories(visit.categories);
        if ((v_categories[0].n != null) && _.indexOf(categories, v_categories[0].n)) {
          return true;
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->currentIsTopCategory(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : Profile Method to check category is/are top or not.
   * <li>If OCR is enable then check against all used devices</li>
   * <li>If OCR is disable then check against recently used device</li>
   * @method isTopCategory
   * @param {Number} trackerID
   * @param cats
   * @return {Boolean} true/false
   */
  Profile.methods.isTopCategory = function(trackerID, cats) {
    var categories, err, k, l, len1, len2, len3, n, o, profile, ref1, ref2, ref3, temp, total_categories, v, visit, visits;
    try {
      categories = cats.split('||');
      total_categories = [];
      temp = [];
      if (this.OCR) {
        ref1 = this.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          profile = ref1[l];
          visits = profile.visits;
          for (n = 0, len2 = visits.length; n < len2; n++) {
            visit = visits[n];
            if (visit.categories) {
              ref2 = visit.categories;
              for (k in ref2) {
                v = ref2[k];
                if (v.g && v.g === parseInt(trackerID)) {
                  if (temp[k]) {
                    temp[k].c += v.c;
                    temp[k].d += v.d;
                    if (v.n !== '') {
                      temp[k].n = v.n;
                    }
                    if (v.v !== '') {
                      temp[k].v = v.v;
                    }
                    if (v.g > 0) {
                      temp[k].g = v.g;
                    }
                  } else {
                    temp[k] = v;
                  }
                }
              }
            }
          }
        }
        total_categories = this.sortCategories(temp);
        if (total_categories.length > 0 && (total_categories[0].n != null) && _.indexOf(categories, total_categories[0].n) >= 0) {
          return true;
        } else {

        }
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          visits = profile.visits;
          for (o = 0, len3 = visits.length; o < len3; o++) {
            visit = visits[o];
            if (visit.categories) {
              ref3 = visit.categories;
              for (k in ref3) {
                v = ref3[k];
                if (v.g && v.g === parseInt(trackerID)) {
                  if (temp[k]) {
                    temp[k].c += v.c;
                    temp[k].d += v.d;
                    if (v.n !== '') {
                      temp[k].n = v.n;
                    }
                    if (v.v !== '') {
                      temp[k].v = v.v;
                    }
                    if (v.g > 0) {
                      temp[k].g = v.g;
                    }
                  } else {
                    temp[k] = v;
                  }
                  total_categories = this.sortCategories(temp);
                  if (total_categories.length > 0 && (total_categories[0].n != null) && _.indexOf(categories, total_categories[0].n) >= 0) {
                    return true;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->isTopCategory(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description :  Profile Method to get categories/products in sorted order
   * <li>Check against all used devices</li>
   * <li>Return items matched with the provided type</li>
   * <li>Code by: Affan | Date: 16/02/18</li>
   * @method getSortedCategories
   * @param {Number} type
   * @return Categories/Products list in sorted order. In case of error returns false
   */
  Profile.methods.getSortedCategories = function() {
    var cats, err, k, l, len1, len2, n, prods, profile, ref1, ref2, tc, v, visit, visits;
    try {
      tc = [];
      prods = [];
      cats = [];
      ref1 = this.profiles;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        profile = ref1[l];
        visits = profile.visits;
        for (n = 0, len2 = visits.length; n < len2; n++) {
          visit = visits[n];
          if (visit.categories) {
            ref2 = visit.categories;
            for (k in ref2) {
              v = ref2[k];
              if (v.t && v.t === 1) {
                if (cats[k]) {
                  cats[k].c += v.c;
                  cats[k].d += v.d;
                  if (v.n !== '') {
                    cats[k].n = v.n;
                  }
                  if (v.v !== '') {
                    cats[k].v = v.v;
                  }
                  if (v.g > 0) {
                    cats[k].g = v.g;
                  }
                } else {
                  cats[k] = v;
                }
              } else if (v.t && v.t === 2) {
                if (prods[k]) {
                  prods[k].c += v.c;
                  prods[k].d += v.d;
                  if (v.n !== '') {
                    prods[k].n = v.n;
                  }
                  if (v.v !== '') {
                    prods[k].v = v.v;
                  }
                  if (v.g > 0) {
                    prods[k].g = v.g;
                  }
                } else {
                  prods[k] = v;
                }
              }
            }
          }
        }
      }
      tc.push(this.sortCategoriesWithSortOrder(cats));
      tc.push(this.sortCategoriesWithSortOrder(prods));
      return tc;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->getSortedCategories(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description :  Profile Method to get top categories.
   * <li>If OCR is enable then check against all used devices</li>
   * <li>If OCR is disable then check against recently used device</li>
   * @method topCategories
   * @param {Number} x
   * @param {Number} trackerID
   * @return {String} Categories list as string slitted by '||'. If there is not category then return false
   */
  Profile.methods.topCategories = function(x, trackerID) {
    var err, k, l, len1, len2, len3, n, o, out, profile, ref1, ref2, ref3, temp, total_categories, v, visit, visits;
    try {
      total_categories = [];
      temp = [];
      if (this.OCR) {
        out = [];
        ref1 = this.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          profile = ref1[l];
          visits = profile.visits;
          for (n = 0, len2 = visits.length; n < len2; n++) {
            visit = visits[n];
            if (visit.categories) {
              ref2 = visit.categories;
              for (k in ref2) {
                v = ref2[k];
                if (v.g && v.g === parseInt(trackerID)) {
                  if (temp[k]) {
                    temp[k].c += v.c;
                    temp[k].d += v.d;
                    if (v.n !== '') {
                      temp[k].n = v.n;
                    }
                    if (v.v !== '') {
                      temp[k].v = v.v;
                    }
                    if (v.g > 0) {
                      temp[k].g = v.g;
                    }
                  } else {
                    temp[k] = v;
                  }
                  total_categories = this.sortCategories(temp);
                  total_categories = total_categories.slice(0, parseInt(x));
                  for (k in total_categories) {
                    v = total_categories[k];
                    if (v.n) {
                      out.push(v.n);
                    }
                  }
                }
              }
            }
          }
        }
        return out.join('||');
      } else {
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          visits = profile.visits;
          for (o = 0, len3 = visits.length; o < len3; o++) {
            visit = visits[o];
            if (visit.categories) {
              ref3 = visit.categories;
              for (k in ref3) {
                v = ref3[k];
                if (v.g && v.g === parseInt(trackerID)) {
                  if (temp[k]) {
                    temp[k].c += v.c;
                    temp[k].d += v.d;
                    if (v.n !== '') {
                      temp[k].n = v.n;
                    }
                    if (v.v !== '') {
                      temp[k].v = v.v;
                    }
                    if (v.g > 0) {
                      temp[k].g = v.g;
                    }
                  } else {
                    temp[k] = v;
                  }
                  total_categories = this.sortCategories(temp);
                  out = [];
                  total_categories = total_categories.slice(0, parseInt(x));
                  for (k in total_categories) {
                    v = total_categories[k];
                    if (v.n) {
                      out.push(v.n);
                    }
                  }
                  return out.join('||');
                }
              }
            }
          }
        }
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->isTopCategories(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  Profile.methods.isDecreaseCategory = function(trackerID, value) {
    var cat, err, key, l, len1, profile, ref1, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (visit.categories) {
            ref1 = visit.categories;
            for (key in ref1) {
              cat = ref1[key];
              if (cat.g === parseInt(trackerID) && cat.v === value) {
                return true;
              }
            }
          }
        }
        return false;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->isDecreaseCategory(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  Profile.methods.isGrowthCategory = function(trackerID, value) {
    var cat, err, key, l, len1, profile, ref1, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (visit.categories) {
            ref1 = visit.categories;
            for (key in ref1) {
              cat = ref1[key];
              if (cat.g === parseInt(trackerID) && cat.v === value) {
                return true;
              }
            }
          }
        }
        return false;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->isGrowthCategory(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  Profile.methods.ViewedCategories = function(trackerID, value, days = 0) {
    var cat, diff, err, key, l, len1, profile, ref1, temp, visit, visits;
    try {
      //default days is 0
      if (days === void 0 || days === null || days === 'undefined') {
        days = 0;
      }
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (visit.categories) {
            ref1 = visit.categories;
            for (key in ref1) {
              cat = ref1[key];
              //if category value is null or undefined then check name
              temp = false;
              if ((cat.v === void 0 || cat.v === null) && cat.n === value) {
                temp = true;
              }
              if (cat.g === parseInt(trackerID) && (cat.v === value || temp)) {
                diff = Math.floor(new Date().getTime() / 1000) - profile.visit_server_date;
                if (parseInt(Math.floor(diff / 86400)) === parseInt(days)) {
                  return true;
                }
              }
            }
          }
        }
        return false;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->ViewedCategories(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method topAssets
   * @param x
   * @param trackerID
   * @param type
   * @deprecated Not in use. Found in test
   */
  Profile.methods.topAssets = function(x, trackerID = false, type = false) {
    var cat, err, index, l, len1, len2, n, out, profile, tc, total_categories, visit, visits;
    try {
      out = [];
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        total_categories = [];
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (visit.categories) {
            _.extend(total_categories, visit.categories);
          }
        }
        tc = this.sortCategories(total_categories);
        for (index = n = 0, len2 = tc.length; n < len2; index = ++n) {
          cat = tc[index];
          if (type) {
            if (cat.a && cat.t && cat.t === type) {
              out.push(cat.a[0]);
            }
          } else if (trackerID) {
            if (cat.a && cat.g && cat.g === trackerID) {
              out.push(cat.a[0]);
            }
          }
        }
      }
      return out;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->topAssets(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : Profile Method to check low category.
   * <li>If OCR is enable then check against all used devices</li>
   * <li>If OCR is disable then check against recently used device</li>
   * @method isLowCategory
   * @param {Number}trackerID
   * @param categories
   * @return {Boolean} true/false
   */
  Profile.methods.isLowCategory = function(trackerID, categories) {
    var err, l, len1, len2, len3, n, o, profile, ref1, that, total_categories, visit, visits;
    try {
      that = this;
      if (that.OCR) {
        total_categories = [];
        ref1 = that.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          profile = ref1[l];
          visits = profile.visits;
          for (n = 0, len2 = visits.length; n < len2; n++) {
            visit = visits[n];
            if (_.isArray(visit.categories)) {
              _.extend(total_categories, visit.categories);
            }
          }
        }
        total_categories = that.sortCategories(total_categories);
        if (total_categories && (total_categories[0].n != null) && _.indexOf(categories, total_categories[0].n)) {
          return true;
        }
      } else {
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          visits = profile.visits;
          total_categories = [];
          for (o = 0, len3 = visits.length; o < len3; o++) {
            visit = visits[o];
            if (_.isArray(visit.categories)) {
              _.extend(total_categories, visit.categories);
            }
          }
          total_categories = that.sortCategories(total_categories);
          if (total_categories && (total_categories[0].n != null) && _.indexOf(categories, total_categories[0].n)) {
            return true;
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->isLowCategory(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method currentIsLowCategory
   * @param trackerID
   * @param categories
   * @deprecated Not in use. Found in test
   */
  Profile.methods.currentIsLowCategory = function(trackerID, categories) {
    var err, v_categories, visit;
    try {
      visit = this.lastVisit;
      if (visit && (visit.categories != null) && _.isArray(visit.categories)) {
        //sort categories by (time+clicks)/2
        /*v_categories = visit.categories
          v_categories.sort (a, b) ->
        t_a = (a.d + a.c) / 2
        t_b = (b.d + b.c) / 2
        return 0 if t_a == t_b
        return if (t_a < t_b) then -1 else 1     #Return in descending order!*/
        v_categories = this.sortCategories(visit.categories);
        if ((v_categories[0].n != null) && _.indexOf(categories, v_categories[0].n)) {
          return true;
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->currentIsLowCategory(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method sortCategories
   * @param categories
   */
  Profile.methods.sortCategories = function(categories) {
    var e, k, sumC, sumD, tc, v;
    tc = [];
    try {
      sumC = 0;
      sumD = 0;
      for (k in categories) {
        v = categories[k];
        sumC += parseInt(v.c);
        sumD += parseInt(v.d);
        tc.push(v);
      }
      //Because number / 0 = Infinity. So we need to check it the
      if (sumC === 0) {
        sumC = 1;
      }
      if (sumD === 0) {
        sumD = 1;
      }
      tc.sort(function(a, b) {
        var t_a, t_b;
        t_a = (a.d / sumD + a.c / sumC) / 2;
        t_b = (b.d / sumD + b.c / sumC) / 2;
        if (t_a === t_b) {
          return 0;
        }
        if (t_a < t_b) {
          return 1;
        } else {
          return -1; //Return in descending order!
        }
      });
      return tc;
    } catch (error) {
      e = error;
      console.error(new Date() + ": Model->Profile->sortScore(ID:" + this.id + ") - " + err);
      return tc;
    }
  };

  Profile.methods.allCategories = function(t){
    let all_categories = {};
    for(let i = 0; i < this.profiles.length; i++){
      let profile = this.profiles[i];
      for(let j = 0; j < profile.visits.length; j++){
        let visit = profile.visits[j];
        if(visit.categories){
          for(let key in visit.categories){
            let category = visit.categories[key];
            if(t == category.t){
              if(all_categories[key] === undefined){
                all_categories[key] = category;
              }else{
                all_categories[key].d += category.d;
                all_categories[key].c += category.c;
              }
            }
          }
        }
      }
    }
    let sum = {};

    for(k in all_categories){
      if(sum[all_categories[k].g] === undefined){
        sum[all_categories[k].g] = {
          c: 0,
          d: 0
        };
      }
      sum[all_categories[k].g].c += all_categories[k].c;
      sum[all_categories[k].g].d += all_categories[k].d;
    }

    for(k in all_categories){
      let g = all_categories[k].g
      if(sum[g].c === 0 || sum[g].d === 0){
        all_categories[k].s = 0;
        continue;
      }
      all_categories[k].s = (all_categories[k].d / sum[g].d + all_categories[k].c / sum[g].c) / 2;
    }

    return all_categories;
  };

  Profile.methods.summarySortCategories = function(t){
    let all_categories = this.allCategories(t);
    let sort_categories = [];
    for(k in all_categories){
      sort_categories.push({
        g: all_categories[k].g,
        n: all_categories[k].n,
        d: all_categories[k].d,
        c: all_categories[k].c,
        s: all_categories[k].s
      })
    }

    sort_categories.sort(function(a, b) {
      if (a.s === b.s) {
        return 0;
      }
      if (a.s < b.s) {
        return 1;
      } else {
        return -1; //Return in descending order!
      }
    });
    return sort_categories;
  };

  Profile.methods.activeAudiences = function(){
    let profile = this.getRecentDevicePorfile();
    let last_visit = profile.visits[profile.visits.length-1];
    let res = [];
    for(key in last_visit.segments){
      if(key.indexOf('OCR') !== -1){
        continue;
      }
      res.push({
        uid: key.replace('segment_',''),
        tstamp: last_visit.segments[key]
      });
    }
    return res;
  };

  Profile.methods.allGoals = function(){
    let goals = {};

    for(let i = 0; i < this.profiles.length; i++){
      let profile = this.profiles[i];

      for(let j = 0; j < profile.goals.length; j++){
        let goal = profile.goals[j];

        if(goals[goal.uid] === undefined){
          goals[goal.uid] = {
            uid: goal.uid,
            tstamp: goal.tstamp
          }
        }else{
          if(goals[goal.uid].tstamp < goal.tstamp){
            goals[goal.uid].tstamp = goal.tstamp;
          }
        }
      }
    }

    let arr = [];
    _(goals).each(function(goal, key){
      arr.push(goal);
    });

    return arr;
  }

  Profile.methods.allExperiences = function(){
    let exps = {};

    for(let i = 0; i < this.profiles.length; i++){
      let profile = this.profiles[i];

      if(_.isObject(profile.experiments)){
        _(profile.experiments).each(function(experiment, key){
          if(exps[key] === undefined){
            exps[key] = {
              uid: key,
              tstamp: experiment.timestamp
            }
          }else{
            if(exps[key].tstamp < experiment.timestamp){
              exps[key].tstamp = experiment.timestamp;
            }
          }
        });
      }

    }

    let arr = [];
    _(exps).each(function(exp, key){
      arr.push(exp);
    });

    return arr;
  }

  Profile.methods.allControlGroups = function(){
    let cgs = {};

    for(let i = 0; i < this.profiles.length; i++){
      let profile = this.profiles[i];

      if(_.isObject(profile.controlgroups)){
        _(profile.controlgroups).each(function(control_group, key){
          if(cgs[key] === undefined){
            cgs[key] = {
              uid: key,
              tstamp: control_group.timestamp
            }
          }else{
            if(cgs[key].tstamp < control_group.timestamp){
              cgs[key].tstamp = control_group.timestamp;
            }
          }
        });
      }

    }

    let arr = [];
    _(cgs).each(function(cg, key){
      arr.push(cg);
    });

    return arr;
  }

  Profile.methods.allSearches = function(){
    let ss = {};
    for(let i = 0; i < this.profiles.length; i++){
      let profile = this.profiles[i];
      for(let j = 0; j < profile.visits.length; j++){
        let visit = profile.visits[j];
        if(visit.searches){
          for(let k = 0; k < visit.searches.length; k++){
            let search = visit.searches[k];
            if(ss[search] === undefined){
              ss[search] = {
                tstamp: visit.firstclick,
                term: search
              }
            }else{
              ss[search].tstamp = visit.firstclick;
            }
          }
        }
      }
    }
    let arr = [];
    _(ss).each(function(s, key){
      arr.push(s);
    });

    return arr;
  }



  /**
   * Description :  Profile Method to sort categories/products and return the categories with sort order
   * <li>Sorts on basis of predefined formula</li>
   * <li>Code by: Affan | Date: 16/02/18</li>
   * @method sortCategoriesWithSortOrder
   * @param {Object Array} categories
   * @return Categories/Products list in sorted order.
   */
  Profile.methods.sortCategoriesWithSortOrder = function(categories) {
    var e, k, l, len1, sumC, sumD, tc, v;
    tc = [];
    try {
      sumC = 0;
      sumD = 0;
      for (k in categories) {
        v = categories[k];
        sumC += parseInt(v.c != null ? v.c : 1);
        sumD += parseInt(v.d != null ? v.d : 0);
        tc.push({
          g: v.g,
          n: v.n,
          id: v.id
        });
      }
      //Because number / 0 = Infinity. So we need to check it the
      if (sumC === 0) {
        sumC = 1;
      }
      if (sumD === 0) {
        sumD = 1;
      }
      for (l = 0, len1 = tc.length; l < len1; l++) {
        v = tc[l];
        v.s = ((v.d != null ? v.d : 0) / sumD + (v.c != null ? v.c : 1) / sumC) / 2;
      }
      tc.sort(function(a, b) {
        if (a.s === b.s) {
          return 0;
        }
        if (a.s < b.s) {
          return 1;
        } else {
          return -1; //Return in descending order!
        }
      });
      return tc;
    } catch (error) {
      e = error;
      console.error(new Date() + ": Model->Profile->sortScore(ID:" + this.id + ") - " + err);
      return tc;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method currentlastViewedScore
   * @param trackerID
   * @param categories
   * @deprecated Not in use. Found in test
   */
  Profile.methods.currentlastViewedScore = function(trackerID, categories) {
    var err, lastCat, visit;
    try {
      visit = this.lastVisit;
      if (_.isArray(visit.categories)) {
        //categories exsits:
        lastCat = visit.categories[visit.categories.lenght - 1];
        if ((lastCat.n != null) && _.indexOf(categories, lastCat.n)) {
          return true;
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->currentlastViewedScore(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method lastViewedScore
   * @param trackerID
   * @param categories
   * @deprecated Not in use. Found in test
   */
  Profile.methods.lastViewedScore = function(trackerID, categories) {
    var currentIndex, err, key, l, lastCat, lastCategory, lastVisit, latestProfileVisit, len1, profile, profileID, profileIndex, ref1, visit;
    try {
      if (this.OCR) {
        visit = this.previousVisit;
        if (visit.categories) {
          lastCategory = visit.categories[Object.keys(visit.categories)[Object.keys(visit.categories).length - 1]];
          if ((lastCategory.n != null) && _.indexOf(categories, lastCategory.n)) {
            return true;
          }
        } else {
          latestProfileVisit = 0;
          profileIndex = null;
          profileID = null;
          currentIndex = this.getRecentDevicePorfile() && this.getRecentDevicePorfile()._id;
          ref1 = this.profiles;
          for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
            profile = ref1[key];
            if (currentIndex !== profile._id) {
              if (profile.visit_server_date > latestProfileVisit) {
                latestProfileVisit = profile.visit_server_date;
                profileIndex = key;
                profileID = profile._id;
              }
            }
          }
          if (profileID) {
            lastVisit = this.profiles[profileIndex].visits[this.profiles[profileIndex].visits.length - 1];
            lastCat = lastVisit.categories[lastVisit.categories.length - 1];
            if ((lastCat.n != null) && _.indexOf(categories, lastCat.n)) {
              return true;
            }
          }
        }
      } else {
        visit = this.lastVisit;
        if (visit.categories) {
          lastCategory = visit.categories[Object.keys(visit.categories)[Object.keys(visit.categories).length - 1]];
          if ((lastCategory.n != null) && _.indexOf(categories, lastCategory.n)) {
            return true;
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->lastViewedScore(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method currentDownloadedFile
   * @param filename {String}
   * @deprecated Not in use. Found in test
   */
  Profile.methods.currentDownloadedFile = function(filenames) {
    var download, err, l, len1, ref1, visit;
    try {
      visit = this.lastVisit;
      if (visit && visit.downloads) {
        if (_.isArray(visit.downloads)) {
          ref1 = visit.downloads;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            download = ref1[l];
            if (_.indexOf(filenames, download)) {
              return true;
            }
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->currentDownloadedFile(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method downloadedFile
   * @param filename {String}
   * @deprecated Not in use. Found in test
   */
  Profile.methods.downloadedFile = function(filenames) {
    var download, err, l, len1, len2, n, profile, total_downloads, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        total_downloads = [];
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (visit.downloads && _.isArray(visit.downloads)) {
            total_downloads = _.extend(total_downloads, visit.downloads);
          }
        }
        for (n = 0, len2 = total_downloads.length; n < len2; n++) {
          download = total_downloads[n];
          if (_.indexOf(filenames, download)) {
            return true;
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->downloadedFile(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : Virtual property contain count of days since last download.
   * <li>If OCR is enable then count days since last download against all used devices</li>
   * <li>If OCR is disable then count days since last download against recently used device</li>
   * @property DaysSinceLastDownload
   * @type {Number}
   */
  Profile.virtual('DaysSinceLastDownload').get(function() {
    var currentIndex, diff, err, key, l, latestProfileVisit, len1, len2, len3, n, o, profile, ref1, ref2, ref3, visit, visits;
    try {
      if (this.OCR) {
        latestProfileVisit = 0;
        currentIndex = this.getRecentDevicePorfile() && this.getRecentDevicePorfile()._id;
        ref1 = this.profiles;
        for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
          profile = ref1[key];
          if (!(util.isNull(profile.visits))) {
            ref2 = profile.visits;
            for (n = 0, len2 = ref2.length; n < len2; n++) {
              visit = ref2[n];
              if ((visit.downloads != null) && visit.downloads.length > 0) {
                if (visit.lastclick > latestProfileVisit) {
                  latestProfileVisit = visit.lastclick;
                }
              }
            }
          }
        }
        if (latestProfileVisit > 0) {
          diff = Math.floor(new Date().getTime() / 1000) - latestProfileVisit;
          return parseInt(Math.floor(diff / 86400));
        } else {
          return 0;
        }
      } else {
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          if (_.isArray(profile.visits)) {
            visits = profile.visits.reverse();
            ref3 = profile.visits;
            for (o = 0, len3 = ref3.length; o < len3; o++) {
              visit = ref3[o];
              if ((visit.downloads != null) && visit.downloads.length > 0) {
                diff = Math.floor(new Date().getTime() / 1000) - visit.lastclick;
                return parseInt(Math.floor(diff / 86400));
              }
            }
          }
        }
        return 0;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->DaysSinceLastDownload(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method currentSearchedFor
   * @param searches
   * @deprecated Not in use. Found in test
   */
  Profile.methods.currentSearchedFor = function(searches) {
    var err, l, len1, len2, n, profile, search, total_searches, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        total_searches = [];
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          if (_.isArray(visit.searches)) {
            _.extend(total_searches, visit.searches);
          }
        }
        for (n = 0, len2 = total_searches.length; n < len2; n++) {
          search = total_searches[n];
          if (_.indexOf(searches, search)) {
            return true;
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->currentSearchedFor(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method searchedFor
   * @param searches
   * @deprecated Not in use. Found in test
   */
  Profile.methods.searchedFor = function(searches) {
    var err, l, len1, ref1, search, visit;
    try {
      visit = this.lastVisit;
      if (visit && _.isArray(visit.searches)) {
        ref1 = visit.searches;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          search = ref1[l];
          if (_.indexOf(search, searches)) {
            return true;
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->searchedFor(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method isWithinArea
   * @param lng1
   * @param lat1
   * @param radius
   * @param miles
   * @deprecated Not in use. Found in test
   */
  Profile.methods.isWithinArea = function(lng1, lat1, radius, miles = false) {
    var M_PI, a, c, distance, dlat, dlng, err, km, lat2, lng2, pi80, r;
    try {
      if (!(util.isNull(this.lastVisit))) {
        //Get visitor position:
        lat2 = this.lastVisit.loc_info.loc_lat;
        lng2 = this.lastVisit.loc_info.loc_long;
        //Calc the distance from given long and lat to user location:
        // M_PI = 3.141592654
        M_PI = 3.141592654;
        pi80 = M_PI / 180;
        lat1 *= pi80;
        lng1 *= pi80;
        lat2 *= pi80;
        lng2 *= pi80;
        r = 6372.797; // mean radius of Earth in km
        dlat = lat2 - lat1;
        dlng = lng2 - lng1;
        a = Math.sin(dlat / 2) * Math.sin(dlat / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlng / 2) * Math.sin(dlng / 2);
        c = 2 * Math.tan(Math.sqrt(a), Math.sqrt(1 - a));
        km = r * c;
        distance = miles ? km * 0.621371192 : km;
        if (distance < radius) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->isWithinArea(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property LastBasketValue
   */
  Profile.virtual('LastBasketValue').get(function() {
    var cart, err, l, len1, len2, n, profile, ref1, ref2, ref3, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (_.isArray(profile.visits)) {
          visits = profile.visits.reverse();
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            if (visit && visit.carts && _.isArray(visit.carts)) {
              ref2 = visit.carts;
              for (n = 0, len2 = ref2.length; n < len2; n++) {
                cart = ref2[n];
                if ((cart.pur == null) || ((cart.pur != null) && !cart.pur)) {
                  return (ref3 = cart.p) != null ? ref3 : 0;
                }
              }
            }
          }
        }
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->LastBasketValue(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property LastBasketNoOfItems
   */
  Profile.virtual('LastBasketNoOfItems').get(function() {
    var cart, err, l, len1, len2, n, profile, ref1, ref2, ref3, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (_.isArray(profile.visits)) {
          visits = profile.visits.reverse();
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            if (visit && visit.carts && _.isArray(visit.carts)) {
              ref2 = visit.carts;
              for (n = 0, len2 = ref2.length; n < len2; n++) {
                cart = ref2[n];
                if ((cart.pur == null) || ((cart.pur != null) && !cart.pur)) {
                  return (ref3 = cart.q) != null ? ref3 : 0;
                }
              }
            }
          }
        }
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->LastBasketNoOfItems(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span>virtual property</span>
   * @property DaysSinceLastBasket
   */
  Profile.virtual('DaysSinceLastBasket').get(function() {
    var cart, diff, err, key, l, latestProfileBasket, len1, len2, len3, n, o, profile, ref1, ref2, ref3, visit, visits;
    try {
      if (this.OCR) {
        latestProfileBasket = 0;
        ref1 = this.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          profile = ref1[l];
          if (_.isArray(profile.visits)) {
            visits = profile.visits.reverse();
            for (n = 0, len2 = visits.length; n < len2; n++) {
              visit = visits[n];
              if (visit && visit.carts !== void 0 && _.isObject(visit.carts)) {
                ref2 = visit.carts;
                for (key in ref2) {
                  cart = ref2[key];
                  if (cart && (cart.p === void 0 || cart.p === false)) {
                    if (visit.lastclick > latestProfileBasket) {
                      latestProfileBasket = visit.lastclick;
                    }
                  }
                }
              }
            }
          }
        }
        if (latestProfileBasket > 0) {
          diff = Math.floor(new Date().getTime() / 1000) - latestProfileBasket;
          return parseInt(Math.floor(diff / 86400));
        } else {
          return 0;
        }
      } else {
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          if (_.isArray(profile.visits)) {
            visits = profile.visits.reverse();
            for (o = 0, len3 = visits.length; o < len3; o++) {
              visit = visits[o];
              if (visit && visit.carts !== void 0 && _.isObject(visit.carts)) {
                ref3 = visit.carts;
                for (key in ref3) {
                  cart = ref3[key];
                  if (cart && cart.p === void 0 || cart.p === false) {
                    diff = Math.floor(new Date().getTime() / 1000) - visit.lastclick;
                    return parseInt(Math.floor(diff / 86400));
                  }
                }
              }
            }
          }
        }
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->DaysSinceLastBasket(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method totalPurchaseValueInDays
   * @param days
   * @deprecated Not in use. Found in test
   */
  Profile.methods.totalPurchaseValueInDays = function(days = 0) {
    var cart, err, l, len1, len2, n, profile, ref1, ref2, totalValue, visit;
    try {
      totalValue = 0;
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (_.isArray(profile.visits)) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            if (days === 0 || visit.lastclick > (Math.floor(new Date().getTime() / 1000) - (days * 86400)) && _.isArray(visit.carts)) {
              ref2 = visit.carts;
              for (n = 0, len2 = ref2.length; n < len2; n++) {
                cart = ref2[n];
                if ((cart.pur == null) || ((cart.pur != null) && !cart.pur)) {
                  totalValue += cart.p;
                }
              }
            }
          }
        }
      }
      return totalValue;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->totalPurchaseValueInDays(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method ViewedCategory
   * @param days
   * @param trackerID
   * @param category
   * @deprecated Not in use. Found in test
   */
  Profile.methods.ViewedCategory = function(days, trackerID, category) {
    var catkey, catval, err, l, len1, profile, ref1, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        visits = profile.visits;
        for (l = 0, len1 = visits.length; l < len1; l++) {
          visit = visits[l];
          //Check if we need to filter out some visits due to $date != null
          if (visit.firstclick > (Math.floor(new Date().getTime() / 1000) - (days * 24 * 60 * 60))) {
            //valid visit:
            if (visit.categories) {
              ref1 = visit.categories;
              //categories exsits:
              //echo 'ID: '.$this->LastClick;
              //print_r($visit['categories']);
              //die($this->LastClick);
              for (catkey in ref1) {
                catval = ref1[catkey];
                //Check if current equals selected category and add counter for category:
                if (category === catval.n) {
                  return true;
                }
              }
            }
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->ViewedCategory(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method totalViewedCategoryValueInDays
   * @param days
   * @deprecated Not in use. Found in test
   */
  Profile.methods.totalViewedCategoryValueInDays = function(days = 0) {
    var cart, err, l, len1, len2, n, profile, ref1, ref2, totalValue, visit;
    try {
      totalValue = 0;
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (_.isArray(profile.visits)) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            if (days === 0 || visit.lastclick > (Math.floor(new Date().getTime() / 1000) - (days * 86400)) && visit && visit.carts && _.isArray(visit.carts)) {
              ref2 = visit.carts;
              for (n = 0, len2 = ref2.length; n < len2; n++) {
                cart = ref2[n];
                if ((cart.pur == null) || ((cart.pur != null) && !cart.pur)) {
                  totalValue += cart.v;
                }
              }
            }
          }
        }
      }
      return totalValue;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->totalViewedCategoryValueInDays(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : Total unique visitors
   * @method TotalUniqueVisitors
   * @return totalVisitors {Number} count of total unique visitors.
   */
  Profile.methods.TotalUniqueVisitors = function() {
    var l, lastclick, len1, profile, ref1, totalVisitors, v;
    totalVisitors = 0;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      if (profile.visits.length === 1) {
        if (!profile.visits[0].aggregated || profile.visits[0].aggregated !== true) {
          totalVisitors = 1;
        }
        return totalVisitors;
      }
      ref1 = profile.visits;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        v = ref1[l];
        if (!v.aggregated || v.aggregated !== true) {
          if (lastclick > 0) {
            if (this.DaysBetween(v.lastclick, lastclick) > 1) {
              totalVisitors++;
            }
          }
        }
        lastclick = v.lastclick;
      }
    }
    return totalVisitors;
  };

  /**
   * Description : Total visitors in current profile.
   * @method TotalVisitors
   * @return totalVisitors {Number} count of total visitors.
   */
  Profile.methods.TotalVisitors = function() {
    var l, lastclick, len1, profile, ref1, totalVisitors, v;
    totalVisitors = 1;
    profile = this.getRecentDevicePorfile();
    if (profile) {
      if (profile.visits.length === 1) {
        totalVisitors = 1;
        return totalVisitors;
      }
      ref1 = profile.visits;
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        v = ref1[l];
        if (lastclick > 0) {
          if (this.DaysBetween(v.lastclick, lastclick) > 1) {
            totalVisitors++;
          }
        }
      }
      lastclick = v.lastclick;
    }
    return totalVisitors;
  };

  /**
   * Description : Total duration of all visit in current profile.
   * @method TotalDurationProfile
   * @return totalDurationProfile {Number} duration.
   */
  Profile.methods.TotalDurationProfile = function() {
    var l, len1, profile, ref1, totalDurationProfile, visit;
    totalDurationProfile = 0;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits && typeof profile.visits !== void 0) {
        if (profile.visits.length >= 1) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            totalDurationProfile += (new Date(visit.lastclick * 1000).getTime() - new Date(visit.firstclick * 1000).getTime()) / 1000;
          }
        }
      }
    }
    return totalDurationProfile;
  };

  /**
   * Description : Total page views of all visit in current profile.
   * @method TotalPageViews
   * @return TotalPageView {Number} total page view count.
   */
  Profile.methods.TotalPageViews = function() {
    var l, len1, profile, ref1, totalPageView, visit;
    totalPageView = 0;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits && typeof profile.visits !== void 0) {
        if (profile.visits.length >= 1) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            totalPageView += visit.clicks;
          }
        }
      }
    }
    return totalPageView;
  };

  /**
   * Description : Total categories of all visit in current profile.
   * @method totalCategoriesVisit
   * @return  {Number} total categories count.
   */
  Profile.methods.totalCategoriesVisit = function() {
    var cat, l, len1, profile, ref1, totalCategoriesVisit;
    totalCategoriesVisit = 0;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits && typeof profile.visits !== void 0) {
        if (profile.visits.length >= 1) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            cat = ref1[l];
            if (typeof cat.categories !== void 0 && typeof cat.categories === "object") {
              totalCategoriesVisit += Object.keys(cat.categories).length;
            }
          }
        }
      }
    }
    return totalCategoriesVisit;
  };

  /**
   * Description : Total page refer in current profile.
   * @method totalPageReferer
   * @return  {Number} total page refers count.
   */
  Profile.methods.totalPageReferer = function() {
    var l, len1, profile, ref, ref1, totalPageReferer;
    totalPageReferer = 0;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits && typeof profile.visits !== void 0) {
        if (profile.visits.length >= 1) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            ref = ref1[l];
            if (typeof ref.referers !== void 0 && ref.referers[ref.referers.length - 1] !== '' && ref.referers[ref.referers.length - 1] !== null) {
              totalPageReferer += ref.referers.length;
            }
          }
        }
      }
    }
    return totalPageReferer;
  };

  /**
   * Description : Total point in current visit
   * @method totalPointsCurrentVisit
   * @return {Number} Total point in current visit
   */
  Profile.methods.totalPointsCurrentVisit = function() {
    var l, len1, point, profile, ref1, totalPointsCurrentVisit;
    totalPointsCurrentVisit = 0;
    profile = this.getRecentDevicePorfile();
    if (!(util.isNull(profile))) {
      if (profile.visits && typeof profile.visits !== void 0) {
        if (profile.visits.length >= 1) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            point = ref1[l];
            if (point.points !== void 0) {
              point.points = parseInt(point.points, 10);
              if (point.points !== (0/0)) {
                totalPointsCurrentVisit += point.points;
              }
            }
          }
        }
      }
    }
    return totalPointsCurrentVisit;
  };

  Profile.methods.LastViewedCategory = function(trackerID, value) {
    var err, lastCategory;
    try {
      if (this.lastVisit !== null) {
        if (this.lastVisit.categories) {
          lastCategory = this.lastVisit.categories[Object.keys(this.lastVisit.categories)[Object.keys(this.lastVisit.categories).length - 1]];
          if (lastCategory.g === parseInt(trackerID) && lastCategory.v === value) {
            return true;
          }
          return false;
        }
      } else {
        return false;
      }
    } catch (error) {
      err = error;
      return console.error(new Date() + ": Model->Profile->LastViewedCategory(ID:" + this.id + ") - " + err);
    }
  };

  /**
   * Description : In last visit count of visit categories.
   * @method totalLastViewedCategory
   * @return  totalLastViewedCategory {Number}.
   */
  Profile.methods.totalLastViewedCategory = function() {
    var totalLastViewedCategory;
    totalLastViewedCategory = 0;
    if (this.lastVisit !== null) {
      if (this.lastVisit.categories !== void 0 && this.lastVisit.categories !== null) {
        totalLastViewedCategory = Object.keys(this.lastVisit.categories).length;
      }
    }
    return totalLastViewedCategory;
  };

  /**
   * Description : Calculate number of days between two times
   * @method DaysBetween
   * @param time1 {timestamp}
   * @param time2 {timestamp}
   * @return {Number} Count days between two timestamps
   */
  Profile.methods.DaysBetween = function(time1, time2) {
    var ONE_DAY, date1_ms, date2_ms, difference_ms;
    // The number of milliseconds in one day
    ONE_DAY = 1000 * 60 * 60 * 24;
    // Convert both dates to milliseconds
    date1_ms = time1 * 1000;
    date2_ms = time2 * 1000;
    // Calculate the difference in milliseconds
    difference_ms = Math.abs(date1_ms - date2_ms);
    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY);
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method groupProfiles
   * @param cb {function}
   * @deprecated Not in use. Found in test
   */
  Profile.methods.groupProfiles = function(cb) {
    return cb([
      {
        Q1: {}
      }
    ]);
  };

  /**
   * Description :  Virtual property contain count of total purchase.
   * <li>If OCR is enable then count against all used devices</li>
   * <li>If OCR is disable then count against recently used device</li>
   * @property TotalPurchases
   * @type {number}
   */
  Profile.virtual('TotalPurchases').get(function() {
    var err, l, len1, len2, len3, n, o, profile, ref1, ref2, ref3, total, visit;
    try {
      total = 0;
      if (this.OCR) {
        ref1 = this.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          profile = ref1[l];
          if (_.isArray(profile.visits)) {
            ref2 = profile.visits;
            for (n = 0, len2 = ref2.length; n < len2; n++) {
              visit = ref2[n];
              if (visit.pv) {
                total++;
              }
            }
          }
        }
      } else {
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          if (_.isArray(profile.visits)) {
            ref3 = profile.visits;
            for (o = 0, len3 = ref3.length; o < len3; o++) {
              visit = ref3[o];
              /*if visit and visit.carts and _.isArray visit.carts
                  for cart in visit.carts
              total++ if cart.pur?*/
              if (visit.pv) {
                total++;
              }
            }
          }
        }
      }
      return total;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->TotalPurchases(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description :  Virtual property contain average purchase value.
   * <li>If OCR is enable then count against all used devices</li>
   * <li>If OCR is disable then count against recently used device</li>
   * @property AvgPurchaseValue
   * @type {Number}
   */
  Profile.virtual('AvgPurchaseValue').get(function() {
    var cart, err, key, l, len1, len2, len3, len4, n, o, profile, ref1, ref2, ref3, ref4, ref5, ref6, s, singleCart, singlekey, totalBuys, totalValue, visit;
    try {
      totalValue = 0;
      totalBuys = 0;
      if (this.OCR) {
        ref1 = this.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          profile = ref1[l];
          if (_.isArray(profile.visits)) {
            ref2 = profile.visits;
            for (n = 0, len2 = ref2.length; n < len2; n++) {
              visit = ref2[n];
              if (visit && visit.carts && _.isArray(visit.carts)) {
                ref3 = visit.carts;
                for (key in ref3) {
                  cart = ref3[key];
                  for (singlekey in cart) {
                    singleCart = cart[singlekey];
                    if (singleCart.p) {
                      totalBuys++;
                    }
                  }
                }
              }
              if (visit.pv) {
                totalValue += visit.pv;
              }
            }
          }
        }
      } else {
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          if (_.isArray(profile.visits)) {
            ref4 = profile.visits;
            for (o = 0, len3 = ref4.length; o < len3; o++) {
              visit = ref4[o];
              ref5 = profile.visits;
              for (s = 0, len4 = ref5.length; s < len4; s++) {
                visit = ref5[s];
                if (visit && visit.carts && _.isArray(visit.carts)) {
                  ref6 = visit.carts;
                  for (key in ref6) {
                    cart = ref6[key];
                    for (singlekey in cart) {
                      singleCart = cart[singlekey];
                      if (singleCart.p) {
                        totalBuys++;
                      }
                    }
                  }
                }
                if (visit.pv) {
                  totalValue += visit.pv;
                }
              }
            }
          }
        }
      }
      /*if visit and visit.carts and _.isArray visit.carts
            for cart in visit.carts
              if cart.p?
      totalBuys++;
      totalValue += cart.p*/
      if (totalBuys > 0) {
        return totalValue / totalBuys;
      } else {
        return 0;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->AvgPurchaseValue(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description :  Virtual property contain count of days since last purchase.
   * <li>If OCR is enable then count against all used devices</li>
   * <li>If OCR is disable then count against recently used device</li>
   * @property DaysSinceLastPurchase
   * @type {Number}
   */
  Profile.virtual('DaysSinceLastPurchase').get(function() {
    var cart, diff, err, key, l, latestProfilePurchase, len1, len2, len3, n, o, profile, ref1, ref2, ref3, visit, visits;
    try {
      if (this.OCR) {
        latestProfilePurchase = 0;
        ref1 = this.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          profile = ref1[l];
          if (_.isArray(profile.visits)) {
            visits = profile.visits.reverse();
            for (n = 0, len2 = visits.length; n < len2; n++) {
              visit = visits[n];
              if (visit && visit.carts !== void 0 && _.isObject(visit.carts)) {
                ref2 = visit.carts;
                for (key in ref2) {
                  cart = ref2[key];
                  if ((cart.p != null) && cart.p === true) {
                    if (visit.lastclick > latestProfilePurchase) {
                      latestProfilePurchase = visit.lastclick;
                    }
                  }
                }
              }
            }
          }
        }
        if (latestProfilePurchase > 0) {
          diff = Math.floor(new Date().getTime() / 1000) - latestProfilePurchase;
          return parseInt(Math.floor(diff / 86400));
        } else {
          return 0;
        }
      } else {
        profile = this.getRecentDevicePorfile();
        if (!(util.isNull(profile))) {
          if (_.isArray(profile.visits)) {
            visits = profile.visits.reverse();
            for (o = 0, len3 = visits.length; o < len3; o++) {
              visit = visits[o];
              if (visit && visit.carts !== void 0 && _.isObject(visit.carts)) {
                ref3 = visit.carts;
                for (key in ref3) {
                  cart = ref3[key];
                  if ((cart.p != null) && cart.p === true) {
                    diff = Math.floor(new Date().getTime() / 1000) - visit.lastclick;
                    return parseInt(Math.floor(diff / 86400));
                  }
                }
              }
            }
          }
        }
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->DaysSinceLastPurchase(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property PurchaseRatio
   */
  Profile.virtual('PurchaseRatio').get(function() {
    var cart, err, l, len1, len2, n, profile, ref1, ref2, total, visit;
    try {
      total = 0;
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (_.isArray(profile.visits)) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            if (visit && visit.carts && _.isArray(visit.carts)) {
              ref2 = visit.cart;
              for (n = 0, len2 = ref2.length; n < len2; n++) {
                cart = ref2[n];
                if (cart.pur) {
                  total++;
                }
              }
            }
          }
        }
      }
      if (total > 0) {
        return total / profile.visits.lenght * 100;
      } else {
        return 0;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->PurchaseRatio(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CurrentHasPurchaced
   */
  Profile.virtual('CurrentHasPurchaced').get(function() {
    var cart, err, l, len1, ref1, visit;
    try {
      visit = this.LastVisit;
      if (visit && visit.carts && _.isArray(visit.carts)) {
        ref1 = visit.carts;
        //carts exsits:
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          cart = ref1[l];
          if (cart.pur) {
            return true;
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CurrentHasPurchaced(ID:" + this.id + ") - " + err);
      return false;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CurrentBasketNoOfItems
   */
  Profile.virtual('CurrentBasketNoOfItems').get(function() {
    var cart, err, l, len1, ref1, totalValue, visit;
    try {
      visit = this.LastVisit;
      totalValue = 0;
      if (visit && visit.carts && _.isArray(visit.carts)) {
        ref1 = visit.carts;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          cart = ref1[l];
          //carts exsits:
          totalValue += cart.p * cart.q;
        }
      }
      return totalValue;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CurrentBasketNoOfItems(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CurrentBasketAvgValue
   */
  Profile.virtual('CurrentBasketAvgValue').get(function() {
    var avgValue, cart, err, l, len1, ref1, totalItems, totalValue, visit;
    try {
      avgValue = 0;
      visit = this.LastVisit;
      totalValue = 0;
      totalItems = 0;
      if (visit && visit.carts && _.isArray(visit.carts)) {
        ref1 = visit.carts;
        //carts exsits:
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          cart = ref1[l];
          totalValue += cart.p * cart.q;
          totalItems += cart.q;
        }
        avgValue = totalValue / totalItems;
      }
      return avgValue;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CurrentBasketAvgValue(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property CurrentBasketValue
   */
  Profile.virtual('CurrentBasketValue').get(function() {
    var cart, err, l, len1, ref1, totalValue, visit;
    try {
      visit = this.LastVisit;
      totalValue = 0;
      if (visit && visit.carts && _.isArray(visit.carts)) {
        ref1 = visit.carts;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          cart = ref1[l];
          //carts exsits:
          totalValue += cart.p * cart.q;
        }
      }
      return totalValue;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CurrentBasketValue(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Virtual property page view count info
   * <li>If OCR is enable then count page views against all used devices</li>
   * <li>If OCR is disable then count page views against recently used device</li>
   * @property PageViewCount
   * @type Number
   */
  Profile.virtual('PageViewCount').get(function() {
    var err, indProfile, l, len1, len2, len3, n, o, out, profile, ref1, ref2, ref3, viewCount, visit;
    try {
      //out = 1
      if (this.OCR) {
        viewCount = 0;
        ref1 = this.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          indProfile = ref1[l];
          if (_.isArray(indProfile.visits)) {
            ref2 = indProfile.visits;
            for (n = 0, len2 = ref2.length; n < len2; n++) {
              visit = ref2[n];
              viewCount += visit.clicks;
            }
          }
        }
        return viewCount;
      } else {
        out = 0;
        profile = this.getRecentDevicePorfile();
        if (profile) {
          if (_.isArray(profile.visits)) {
            ref3 = profile.visits;
            for (o = 0, len3 = ref3.length; o < len3; o++) {
              visit = ref3[o];
              out += visit.clicks;
            }
          }
        }
        return out;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->PageViewCount(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Virtual property page view count goals info
   *
   * @property PageViewCountGoals
   * @type Number
   */
  Profile.virtual('PageViewCountGoals').get(function() {
    var err, out, profile, visit;
    try {
      //out = 1
      out = 0;
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          if (profile.visits.length > 0) {
            visit = profile.visits[profile.visits.length - 1];
            out += visit.clicks;
          }
        }
      }
      return out;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->PageViewCountGoals(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Virtual property current page view count info in current visit of current profile
   *
   * @property CurrentPageViewCount
   * @type Number
   */
  Profile.virtual('CurrentPageViewCount').get(function() {
    var err, profile, visit;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          if (profile.visits.length > 0) {
            visit = profile.visits[profile.visits.length - 1];
            //return visit.clicks+1
            return visit.clicks;
          }
        }
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CurrentPageViewCount(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  Profile.methods.lastProfileByVisit = function() {
    var l, len1, profile, ref1, results;
    if (this.OCR) {
      ref1 = this.profiles;
      results = [];
      for (l = 0, len1 = ref1.length; l < len1; l++) {
        profile = ref1[l];
        if (profile.visit_server_date) {
          debug('test');
          results.push(void 0);
        } else {
          results.push(void 0);
        }
      }
      return results;
    }
  };

  /**
   * Virtual property contain days since last visit.
   * <li>If OCR is enable then count days since last visit against all used devices</li>
   * <li>If OCR is disable then count days since last visit against recently used device</li>
   * @property DaysSinceLastVisit
   * @type {Number}
   */
  Profile.virtual('DaysSinceLastVisit').get(function() {
    var currentIndex, err, key, l, latestProfileVisit, len1, profile, ref1;
    try {
      if (this.OCR) {
        latestProfileVisit = 0;
        currentIndex = this.getRecentDevicePorfile() && this.getRecentDevicePorfile()._id;
        ref1 = this.profiles;
        for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
          profile = ref1[key];
          if (currentIndex !== profile._id) {
            if (profile.visit_server_date > latestProfileVisit) {
              latestProfileVisit = profile.visit_server_date;
            }
          } else if (currentIndex === profile._id && (profile.visits != null) && profile.visits.length > 1) {
            if (profile.visits[profile.visits.length - 2].lastclick > latestProfileVisit) {
              latestProfileVisit = profile.visits[profile.visits.length - 2].lastclick;
            }
          }
        }
        if (latestProfileVisit > 0) {
          return Math.floor((Math.floor(new Date().getTime() / 1000) - latestProfileVisit) / 86400);
        } else {
          return 0;
        }
      } else {
        //Work on previous visit:
        if (this.previousVisit) {
          return Math.floor((Math.floor(new Date().getTime() / 1000) - this.previousVisit.lastclick) / 86400);
        }
        return 0;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->DaysSinceLastVisit(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Virtual property visit referrer in last visit
   *
   * @property VisitReferer
   * @type {String}
   */
  Profile.virtual('VisitReferer').get(function() {
    var err;
    try {
      if (this.lastVisit) {
        return this.lastVisit.referer;
      }
      return '';
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->VisitReferer(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property utm_content in visit referer
   *
   * @property utm_content
   * @type {String}
   * Added by: Affan Toor
   */
  Profile.virtual('utm_content').get(function() {
    var err, l, len1, str, url_parts, utmParams;
    try {
      let ret = '';
      if (this.VisitReferer) {
        url_parts = Url.parse(this.VisitReferer);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_content')) {
                ret = str.replace('utm_content=', '');
              }
            }
          }
        }
      }
      if (this.CurrentURL) {
        url_parts = Url.parse(this.CurrentURL);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_content')) {
                ret = str.replace('utm_content=', '');
              }
            }
          }
        }
      }
      return ret;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->utm_content(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property utm_source in visit referer
   *
   * @property utm_source
   * @type {String}
   * Added by: Affan Toor
   */
  Profile.virtual('utm_source').get(function() {
    var err, l, len1, str, url_parts, utmParams;
    let ret = '';
    try {
      if (this.VisitReferer) {
        url_parts = Url.parse(this.VisitReferer);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_source')) {
                ret = str.replace('utm_source=', '');
              }
            }
          }
        }
      }
      if(this.CurrentURL){
        url_parts = Url.parse(this.CurrentURL);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_source')) {
                ret = str.replace('utm_source=', '');
              }
            }
          }
        }
      }

      return ret;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->utm_source(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property utm_term in visit referer
   *
   * @property utm_term
   * @type {String}
   * Added by: Affan Toor
   */
  Profile.virtual('utm_term').get(function() {
    var err, l, len1, str, url_parts, utmParams;
    let ret = '';
    try {
      if (this.VisitReferer) {
        url_parts = Url.parse(this.VisitReferer);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_term')) {
                ret = str.replace('utm_term=', '');
              }
            }
          }
        }
      }
      if (this.CurrentURL) {
        url_parts = Url.parse(this.CurrentURL);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_term')) {
                ret = str.replace('utm_term=', '');
              }
            }
          }
        }
      }
      return ret;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->utm_term(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property utm_medium in visit referer
   *
   * @property utm_medium
   * @type {String}
   * Added by: Affan Toor
   */
  Profile.virtual('utm_medium').get(function() {
    var err, l, len1, str, url_parts, utmParams;
    try {
      let ret = '';
      if (this.VisitReferer) {
        url_parts = Url.parse(this.VisitReferer);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_medium')) {
                ret = str.replace('utm_medium=', '');
              }
            }
          }
        }
      }
      if (this.CurrentURL) {
        url_parts = Url.parse(this.CurrentURL);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_medium')) {
                ret = str.replace('utm_medium=', '');
              }
            }
          }
        }
      }
      return ret;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->utm_medium(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property utm_campaign in visit referer
   *
   * @property utm_campaign
   * @type {String}
   * Added by: Affan Toor
   */
  Profile.virtual('utm_campaign').get(function() {
    var err, l, len1, str, url_parts, utmParams;
    try {
      let ret = '';
      if (this.VisitReferer) {
        url_parts = Url.parse(this.VisitReferer);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_campaign')) {
                ret = str.replace('utm_campaign=', '');
              }
            }
          }
        }
      }
      if (this.CurrentURL) {
        url_parts = Url.parse(this.CurrentURL);
        if (url_parts.query !== void 0 && url_parts.query !== null) {
          utmParams = url_parts.query.split('&');
          if (utmParams && utmParams.length > 0) {
            for (l = 0, len1 = utmParams.length; l < len1; l++) {
              str = utmParams[l];
              if (str.includes('utm_campaign')) {
                ret = str.replace('utm_campaign=', '');
              }
            }
          }
        }
      }

      return ret;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->utm_campaign(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property average time on page in last visit of current profile
   *
   * @property AverageTimeOnPage
   * @type Number
   */
  Profile.virtual('AverageTimeOnPage').get(function() {
    var err, profile, visit;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          visit = profile.visits[profile.visits.length - 1];
          if (visit && visit.clicks > 1) {
            return Math.round((visit.lastclick - visit.firstclick) / (visit.clicks - 1));
          }
        }
      }
      return 0;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->AverageTimeOnPage(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Virtual property visit count
   * # <li>If OCR is enable then count visit against all used devices</li>
   * <li>If OCR is disable then count visit against recently used device</li>
   * @property VisitCount
   * @type Number
   */
  Profile.virtual('VisitCount').get(function() {
    var allVisits, err, indProfile, l, len1, profile, ref1;
    try {
      if (this.OCR) {
        allVisits = 0;
        ref1 = this.profiles;
        for (l = 0, len1 = ref1.length; l < len1; l++) {
          indProfile = ref1[l];
          if (indProfile.visits && indProfile.visits.length > 0) {
            allVisits += indProfile.visits.length;
          }
        }
        return allVisits;
      } else {
        profile = this.getRecentDevicePorfile();
        if (profile) {
          if (profile.visits && profile.visits.length > 0) {
            return profile.visits.length;
          }
        } else {
          return 0;
        }
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->VisitCount(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : Return how many visit not aggregated yet in current profile
   * @method VisitCountAggregated
   * @return  {Number}.
   */
  Profile.methods.VisitCountAggregated = function() {
    var err, l, len1, profile, ref1, v, visitCounts;
    try {
      visitCounts = 0;
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (profile.visits && profile.visits.length > 0) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            v = ref1[l];
            if (!v.aggregated || v.aggregated !== true) {
              visitCounts++;
            }
          }
        }
        return visitCounts;
      } else {
        return 0;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->VisitCountAggregated(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Virtual property to get last exit page
   * <li>If OCR is enable then check last exit page against all used devices</li>
   * <li>If OCR is disable then check last exit page against recently used device</li>
   * @property LastExitPage
   * @type String
   */
  Profile.virtual('LastExitPage').get(function() {
    var currentIndex, err, key, l, latestProfileVisit, len1, profile, profileID, profileIndex, ref1;
    try {
      if (this.OCR) {
        if (this.lastVisit.exitpage) {
          return this.lastVisit.exitpage;
        } else {
          latestProfileVisit = 0;
          profileIndex = null;
          profileID = null;
          currentIndex = this.getRecentDevicePorfile() && this.getRecentDevicePorfile()._id;
          ref1 = this.profiles;
          for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
            profile = ref1[key];
            if (currentIndex !== profile._id) {
              if (profile.visit_server_date > latestProfileVisit) {
                latestProfileVisit = profile.visit_server_date;
                profileIndex = key;
                profileID = profile._id;
              }
            }
          }
          if (profileID) {
            return this.profiles[profileIndex].visits[this.profiles[profileIndex].visits.length - 1].exitpage;
          } else {
            return '';
          }
        }
      }
      if (this.lastVisit) {
        return this.lastVisit.exitpage;
      }
      return '';
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->LastExitPage(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property to get last entry page
   * <li>If OCR is enable then check last entry page against all used devices</li>
   * <li>If OCR is disable then check last entry page against recently used device</li>
   * @property LastEntryPage
   * @type String
   */
  Profile.virtual('LastEntryPage').get(function() {
    var currentIndex, err, key, l, latestProfileVisit, len1, profile, profileID, profileIndex, ref1;
    try {
      if (this.OCR) {
        if (this.lastVisit) {
          return this.lastVisit.entrypage;
        } else {
          latestProfileVisit = 0;
          profileIndex = null;
          profileID = null;
          currentIndex = this.getRecentDevicePorfile() && this.getRecentDevicePorfile()._id;
          ref1 = this.profiles;
          for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
            profile = ref1[key];
            if (currentIndex !== profile._id) {
              if (profile.visit_server_date > latestProfileVisit) {
                latestProfileVisit = profile.visit_server_date;
                profileIndex = key;
                profileID = profile._id;
              }
            }
          }
          if (profileID) {
            return this.profiles[profileIndex].visits[this.profiles[profileIndex].visits.length - 1].entrypage;
          } else {
            return '';
          }
        }
      } else {
        if (this.lastVisit) {
          return this.lastVisit.entrypage;
        } else {
          return '';
        }
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->LastEntryPage(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property current URL of last visit of current profile
   *
   * @property CurrentURL
   * @type Number
   */
  Profile.virtual('CurrentURL').get(function() {
    var err;
    try {
      if (this.lastVisit) {
        return this.lastVisit.exitpage;
      } else {
        return '';
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->CurrentURL(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Virtual property total points of current profile
   *
   * @property TotalPoints
   * @type Number
   */
  Profile.virtual('TotalPoints').get(function() {
    var TotalPoints, err, l, len1, profile, ref1, visit;
    try {
      TotalPoints = 0;
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (_.isArray(profile.visits)) {
          ref1 = profile.visits;
          for (l = 0, len1 = ref1.length; l < len1; l++) {
            visit = ref1[l];
            if (visit.points > 0) {
              TotalPoints += visit.points;
            }
          }
        }
      }
      return TotalPoints;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->TotalPoints(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property VisitSearchString
   */
  Profile.virtual('VisitSearchString').get(function() {
    var err, l, len1, parts_query, parts_url, profile, query, ref1, ref2, search_string, visit, visits;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        visits = profile.visits;
        if (visits && visits.length) {
          visit = visits[visits.length - 1];
          if ((visit != null) && (visit.referer != null)) {
            parts_url = Url.parse(visit.referer);
            query = (ref1 = parts_url.query) != null ? ref1 : (ref2 = parts_url.fragment) != null ? ref2 : '';
            if (!query) {
              return '';
            }
            parts_query = {};
            parse_str(query, parts_query);
            search_strings = ['search', 'searchq', 'q'];
            for (l = 0, len1 = search_strings.length; l < len1; l++) {
              search_string = search_strings[l];
              if (parts_query[search_string] != null) {
                return parts_query[search_string];
              }
            }
          }
        }
      }
      return '';
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->VisitSearchString(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method isBrandReferer
   * @param ref
   * @param brand
   * @deprecated Not in use. Found in test and  brandindex method
   */
  Profile.methods.isBrandReferer = function(ref, brands) {
    var brand, err, l, len1, parts_query, parts_url, query, ref1, ref2;
    try {
      if (ref) {
        parts_url = Url.parse(ref);
        query = (ref1 = parts_url.query) != null ? ref1 : (ref2 = parts_url.fragment) != null ? ref2 : '';
        if (!query) {
          return '';
        }
        parts_query = {};
        parse_str(query, parts_query);
        for (l = 0, len1 = brands.length; l < len1; l++) {
          brand = brands[l];
          if (_.indexOf(parts_query, brand)) {
            return true;
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->isBrandReferer(ID:" + this.id + ") - " + err);
      return false;
    }
  };

  /**
   * Virtual property direct visitor
   *
   * @property DirectVisitor
   * @type Number
   */
  Profile.virtual('DirectVisitor').get(function() {
    var err, profile, visit;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          visit = profile.visits[profile.visits.length - 1];
          if (visit.referer === '') {
            return true;
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->DirectVisitor(ID:" + this.id + ") - " + err);
      return false;
    }
  });

  /**
   * Virtual property check visit is first visit or not
   *
   * @property FirstVisit
   * @type Boolean
   */
  Profile.virtual('FirstVisit').get(function() {
    var err, profile;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          if (profile.visits.length > 1) {
            return false;
          }
        }
      }
      return true;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->FirstVisit(ID:" + this.id + ") - " + err);
      return true;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property SearchVisitor
   */
  Profile.virtual('SearchVisitor').get(function() {
    var err, l, len1, parts, profile, ref, ref1, searh_referers, visit;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          visit = profile.visits[profile.visits.length - 1];
          ref = visit.referer;
          if (ref && ref.isString && ref !== '') {
            parts = Url.parse(ref);
            ref = parts.host;
            ref1 = this.searh_referers;
            for (l = 0, len1 = ref1.length; l < len1; l++) {
              searh_referers = ref1[l];
              searh_referers = new RegExp(`${searh_referers}`, "i");
              if (searh_referers.exec(ref)) {
                return true;
              }
            }
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->SearchVisitor(ID:" + this.id + ") - " + err);
      return false;
    }
  });

  /**
   * Description : <span style="color:red">virtual property</span>
   * @property SocialMediaVisitor
   */
  Profile.virtual('SocialMediaVisitor').get(function() {
    var err, l, len1, parts, profile, ref, social_ref, visit;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          visit = profile.visits[profile.visits.length - 1];
          ref = visit.referer;
          if (ref && ref.isString && ref !== '') {
            parts = Url.parse(ref);
            ref = parts.host;
            for (l = 0, len1 = social_referers.length; l < len1; l++) {
              social_ref = social_referers[l];
              social_ref = new RegExp(`${social_ref}`, "i");
              if (social_ref.exec(ref)) {
                return true;
              }
            }
          }
        }
      }
      return false;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->SocialMediaVisitor(ID:" + this.id + ") - " + err);
      return false;
    }
  });

  /**
   * Virtual property total points in last visit
   *
   * @property VisitPoints
   * @type Number
   */
  Profile.virtual('VisitPoints').get(function() {
    var err, profile, visit;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          visit = profile.visits[profile.visits.length - 1];
          return visit.points;
        }
      } else {
        return 0;
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->VisitPoints(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Virtual property total duration in last visit
   *
   * @property VisitDuration
   * @type Number
   */
  Profile.virtual('VisitDuration').get(function() {
    var err, profile, visit;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          visit = profile.visits[profile.visits.length - 1];
          return (visit.lastclick - visit.firstclick) + 1;
        }
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->VisitDuration(ID:" + this.id + ") - " + err);
      return 0;
    }
  });

  /**
   * Virtual property entry page in last visit
   *
   * @property EntryPage
   * @type Number
   */
  Profile.virtual('EntryPage').get(function() {
    var err, profile, visit;
    try {
      profile = this.getRecentDevicePorfile();
      if (profile) {
        if (_.isArray(profile.visits)) {
          visit = profile.visits[profile.visits.length - 1];
          return visit.entrypage;
        }
      }
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->EntryPage(ID:" + this.id + ") - " + err);
      return '';
    }
  });

  Profile.virtual('Events').get(function() {
    return '';
  });

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method getFeedbackIndex
   * @deprecated Not in use.
   */
  Profile.methods.getFeedbackIndex = function() {
    var FeedbackIndex, err, l, len1, profile, ref1, visit, visitCounter;
    try {
      FeedbackIndex = 0;
      profile = this.getRecentDevicePorfile();
      if (!(util.isNull(profile))) {
        if (_.isArray(profile.visits)) {
          if (profile.visits.length > 0) {
            visitCounter = 0;
            ref1 = profile.visits;
            for (l = 0, len1 = ref1.length; l < len1; l++) {
              visit = ref1[l];
              if (visit.feedback === 1) {
                visitCounter++;
              }
            }
            FeedbackIndex = parseFloat((visitCounter / profile.visits.length).toFixed(2));
          }
        }
      }
      return FeedbackIndex;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->getFeedbackIndex(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : <span style="color:red">Profile Method</span>
   * @method getInteractionIndex
   * @deprecated Not in use.
   */
  Profile.methods.getInteractionIndex = function() {
    var InteractionIndex, err;
    try {
      InteractionIndex = 0;
      return InteractionIndex;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->getInteractionIndex(ID:" + this.id + ") - " + err);
      return 0;
    }
  };

  /**
   * Description : Get profile index
   * @method getIndex
   * @return  {String}.
   */
  Profile.methods.getIndex = function(umid) {
    var key, l, len1, mid, profile, ref1;
    mid = umid || this.recently_used_device;
    if (util.isDefined(this.profiles)) {
      ref1 = this.profiles;
      for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
        profile = ref1[key];
        profile = profile.toJSON();
        if (profile._id.toString() === mid) {
          return key;
        }
      }
    } else {
      return null;
    }
  };

  /**
   * Description : list of languages as key value pair object
   * @method getLanguageList
   * @return  {Object}.
   */
  Profile.methods.getLanguageList = function() {
    var array;
    return array = {
      'Afar': 'aa',
      'Abkhazian': 'ab',
      'Afrikaans': 'af',
      'Amharic': 'am',
      'Arabic': 'ar',
      'Assamese': 'as',
      'Aymara': 'ay',
      'Azerbaijani': 'az',
      'Bashkir': 'ba',
      'Byelorussian': 'be',
      'Bulgarian': 'bg',
      'Bihari': 'bh',
      'Bislama': 'bi',
      'Bengali': 'bn',
      'Tibetan': 'bo',
      'Breton': 'br',
      'Catalan': 'ca',
      'Corsican': 'co',
      'Czech': 'cs',
      'Welsh': 'cy',
      'Danish': 'da',
      'German': 'de',
      'Bhutani': 'dz',
      'Greek': 'el',
      'English': 'en',
      'Esperanto': 'eo',
      'Spanish': 'es',
      'Estonian': 'et',
      'Basque': 'eu',
      'Persian': 'fa',
      'Finnish': 'fi',
      'Fiji': 'fj',
      'Faroese': 'fo',
      'French': 'fr',
      'Frisian': 'fy',
      'Irish': 'ga',
      'Scots': 'gd',
      'Galician': 'gl',
      'Guarani': 'gn',
      'Gujarati': 'gu',
      'Hausa': 'ha',
      'Hebrew': 'he',
      'Hindi': 'hi',
      'Croatian': 'hr',
      'Hungarian': 'hu',
      'Armenian': 'hy',
      'Interlingua': 'ia',
      'Indonesian': 'id',
      'Interlingue': 'ie',
      'Inupiak': 'ik',
      'Icelandic': 'is',
      'Italian': 'it',
      'Inuktitut': 'iu',
      'Japanese': 'ja',
      'Javanese': 'jw',
      'Georgian': 'ka',
      'Kazakh': 'kk',
      'Greenlandic': 'kl',
      'Cambodian': 'km',
      'Kannada': 'kn',
      'Korean': 'ko',
      'Kashmiri': 'ks',
      'Kurdish': 'ku',
      'Kirghiz': 'ky',
      'Latin': 'la',
      'Lingala': 'ln',
      'Laothian': 'lo',
      'Lithuanian': 'lt',
      'Latvian': 'lv',
      'Malagasy': 'mg',
      'Maori': 'mi',
      'Macedonian': 'mk',
      'Malayalam': 'ml',
      'Mongolian': 'mn',
      'Moldavian': 'mo',
      'Marathi': 'mr',
      'Malay': 'ms',
      'Maltese': 'mt',
      'Burmese': 'my',
      'Nauru': 'na',
      'Nepali': 'ne',
      'Dutch': 'nl',
      'Norwegian': 'no',
      'Occitan': 'oc',
      '(Afan)': 'om',
      'Oriya': 'or',
      'Punjabi': 'pa',
      'Polish': 'pl',
      'Pashto': 'ps',
      'Portuguese': 'pt',
      'Quechua': 'qu',
      'Rhaeto-Romance': 'rm',
      'Kirundi': 'rn',
      'Romanian': 'ro',
      'Russian': 'ru',
      'Kinyarwanda': 'rw',
      'Sanskrit': 'sa',
      'Sindhi': 'sd',
      'Sangho': 'sg',
      'Serbo-Croatian': 'sh',
      'Sinhalese': 'si',
      'Slovak': 'sk',
      'Slovenian': 'sl',
      'Samoan': 'sm',
      'Shona': 'sn',
      'Somali': 'so',
      'Albanian': 'sq',
      'Serbian': 'sr',
      'Siswati': 'ss',
      'Sesotho': 'st',
      'Sundanese': 'su',
      'Swedish': 'sv',
      'Swahili': 'sw',
      'Tamil': 'ta',
      'Telugu': 'te',
      'Tajik': 'tg',
      'Thai': 'th',
      'Tigrinya': 'ti',
      'Turkmen': 'tk',
      'Tagalog': 'tl',
      'Setswana': 'tn',
      'Tonga': 'to',
      'Turkish': 'tr',
      'Tsonga': 'ts',
      'Tatar': 'tt',
      'Twi': 'tw',
      'Uighur': 'ug',
      'Ukrainian': 'uk',
      'Urdu': 'ur',
      'Uzbek': 'uz',
      'Vietnamese': 'vi',
      'Volapuk': 'vo',
      'Wolof': 'wo',
      'Xhosa': 'xh',
      'Yiddish': 'yi',
      'Yoruba': 'yo',
      'Zhuang': 'za',
      'Chinese': 'zh',
      'Zulu': 'zu'
    };
  };

  /**
   * Description : List of browsers
   * @method getBrowserList
   * @return  {Object}.
   */
  Profile.methods.getBrowserList = function() {
    var array;
    return array = {
      'Internet Explorer': 'MSIE',
      'Mozilla Firefox': 'Firefox',
      'Google Chrome': 'Chrome',
      'Apple Safari': 'Safari',
      'Opera': 'Opera',
      'Netscape': 'Netscape'
    };
  };

  /**
   * Description : List of countries
   * @method getContriesList
   * @return  {Object}.
   */
  Profile.methods.getContriesList = function() {
    var array;
    return array = {
      'Afghanistan': 'af',
      'Aland Islands': 'ax',
      'Albania': 'al',
      'Algeria': 'dz',
      'American Samoa': 'as',
      'Andorra': 'ad',
      'Angola': 'ao',
      'Anguilla': 'ai',
      'Antarctica': 'aq',
      'Antigua And Barbuda': 'ag',
      'Argentina': 'ar',
      'Armenia': 'am',
      'Aruba': 'aw',
      'Australia': 'au',
      'Austria': 'at',
      'Azerbaijan': 'az',
      'Bahamas': 'bs',
      'Bahrain': 'bh',
      'Bangladesh': 'bd',
      'Barbados': 'bb',
      'Belarus': 'by',
      'Belgium': 'be',
      'Belize': 'bz',
      'Benin': 'bj',
      'Bermuda': 'bm',
      'Bhutan': 'bt',
      'Bolivia, Plurinational State Of': 'bo',
      'Bonaire, Saint Eustatius And Saba': 'bq',
      'Bosnia And Herzegovina': 'ba',
      'Botswana': 'bw',
      'Bouvet Island': 'bv',
      'Brazil': 'br',
      'British Indian Ocean Territory': 'io',
      'Brunei Darussalam': 'bn',
      'Bulgaria': 'bg',
      'Burkina Faso': 'bf',
      'Burundi': 'bi',
      'Cambodia': 'kh',
      'Cameroon': 'cm',
      'Canada': 'ca',
      'Cape Verde': 'cv',
      'Cayman Islands': 'ky',
      'Central African Republic': 'cf',
      'Chad': 'td',
      'Chile': 'cl',
      'China': 'cn',
      'Christmas Island': 'cx',
      'Cocos (Keeling) Islands': 'cc',
      'Colombia': 'co',
      'Comoros': 'km',
      'Congo': 'cg',
      'Congo, The Democratic Republic Of The': 'cd',
      'Cook Islands': 'ck',
      'Costa Rica': 'cr',
      'Cote D\'ivoire': 'ci',
      'Croatia': 'hr',
      'Cuba': 'cu',
      'Curacao': 'cw',
      'Cyprus': 'cy',
      'Czech Republic': 'cz',
      'Denmark': 'dk',
      'Djibouti': 'dj',
      'Dominica': 'dm',
      'Dominican Republic': 'do',
      'Ecuador': 'ec',
      'Egypt': 'eg',
      'El Salvador': 'sv',
      'Equatorial Guinea': 'gq',
      'Eritrea': 'er',
      'Estonia': 'ee',
      'Ethiopia': 'et',
      'Falkland Islands (Malvinas)': 'fk',
      'Faroe Islands': 'fo',
      'Fiji': 'fj',
      'Finland': 'fi',
      'France': 'fr',
      'French Guiana': 'gf',
      'French Polynesia': 'pf',
      'French Southern Territories': 'tf',
      'Gabon': 'ga',
      'Gambia': 'gm',
      'Georgia': 'ge',
      'Germany': 'de',
      'Ghana': 'gh',
      'Gibraltar': 'gi',
      'Greece': 'gr',
      'Greenland': 'gl',
      'Grenada': 'gd',
      'Guadeloupe': 'gp',
      'Guam': 'gu',
      'Guatemala': 'gt',
      'Guernsey': 'gg',
      'Guinea': 'gn',
      'Guinea-Bissau': 'gw',
      'Guyana': 'gy',
      'Haiti': 'ht',
      'Heard Island And Mcdonald Islands': 'hm',
      'Holy See (Vatican City State)': 'va',
      'Honduras': 'hn',
      'Hong Kong': 'hk',
      'Hungary': 'hu',
      'Iceland': 'is',
      'India': 'in',
      'Indonesia': 'id',
      'Iran, Islamic Republic Of': 'ir',
      'Iraq': 'iq',
      'Ireland': 'ie',
      'Isle Of Man': 'im',
      'Israel': 'il',
      'Italy': 'it',
      'Jamaica': 'jm',
      'Japan': 'jp',
      'Jersey': 'je',
      'Jordan': 'jo',
      'Kazakhstan': 'kz',
      'Kenya': 'ke',
      'Kiribati': 'ki',
      'Korea, Democratic People\'s Republic Of': 'kp',
      'Korea, Republic Of': 'kr',
      'Kuwait': 'kw',
      'Kyrgyzstan': 'kg',
      'Lao People\'s Democratic Republic': 'la',
      'Latvia': 'lv',
      'Lebanon': 'lb',
      'Lesotho': 'ls',
      'Liberia': 'lr',
      'Libyan Arab Jamahiriya': 'ly',
      'Liechtenstein': 'li',
      'Lithuania': 'lt',
      'Luxembourg': 'lu',
      'Macao': 'mo',
      'Macedonia, The Former Yugoslav Republic Of': 'mk',
      'Madagascar': 'mg',
      'Malawi': 'mw',
      'Malaysia': 'my',
      'Maldives': 'mv',
      'Mali': 'ml',
      'Malta': 'mt',
      'Marshall Islands': 'mh',
      'Martinique': 'mq',
      'Mauritania': 'mr',
      'Mauritius': 'mu',
      'Mayotte': 'yt',
      'Mexico': 'mx',
      'Micronesia, Federated States Of': 'fm',
      'Moldova, Republic Of': 'md',
      'Monaco': 'mc',
      'Mongolia': 'mn',
      'Montenegro': 'me',
      'Montserrat': 'ms',
      'Morocco': 'ma',
      'Mozambique': 'mz',
      'Myanmar': 'mm',
      'Namibia': 'na',
      'Nauru': 'nr',
      'Nepal': 'np',
      'Netherlands': 'nl',
      'New Caledonia': 'nc',
      'New Zealand': 'nz',
      'Nicaragua': 'ni',
      'Niger': 'ne',
      'Nigeria': 'ng',
      'Niue': 'nu',
      'Norfolk Island': 'nf',
      'Northern Mariana Islands': 'mp',
      'Norway': 'no',
      'Oman': 'om',
      'Pakistan': 'pk',
      'Palau': 'pw',
      'Palestinian Territory, Occupied': 'ps',
      'Panama': 'pa',
      'Papua New Guinea': 'pg',
      'Paraguay': 'py',
      'Peru': 'pe',
      'Philippines': 'ph',
      'Pitcairn': 'pn',
      'Poland': 'pl',
      'Portugal': 'pt',
      'Puerto Rico': 'pr',
      'Qatar': 'qa',
      'Reunion': 're',
      'Romania': 'ro',
      'Russian Federation': 'ru',
      'Rwanda': 'rw',
      'Saint Barthelemy': 'bl',
      'Saint Helena, Ascension And Tristan Da Cunha': 'sh',
      'Saint Kitts And Nevis': 'kn',
      'Saint Lucia': 'lc',
      'Saint Martin (French Part)': 'mf',
      'Saint Pierre And Miquelon': 'pm',
      'Saint Vincent And The Grenadines': 'vc',
      'Samoa': 'ws',
      'San Marino': 'sm',
      'Sao Tome And Principe': 'st',
      'Saudi Arabia': 'sa',
      'Senegal': 'sn',
      'Serbia': 'rs',
      'Seychelles': 'sc',
      'Sierra Leone': 'sl',
      'Singapore': 'sg',
      'Sint Maarten (Dutch Part)': 'sx',
      'Slovakia': 'sk',
      'Slovenia': 'si',
      'Solomon Islands': 'sb',
      'Somalia': 'so',
      'South Africa': 'za',
      'South Georgia And The South Sandwich Islands': 'gs',
      'Spain': 'es',
      'Sri Lanka': 'lk',
      'Sudan': 'sd',
      'Suriname': 'sr',
      'Svalbard And Jan Mayen': 'sj',
      'Swaziland': 'sz',
      'Sweden': 'se',
      'Switzerland': 'ch',
      'Syrian Arab Republic': 'sy',
      'Taiwan, Province Of China': 'tw',
      'Tajikistan': 'tj',
      'Tanzania, United Republic Of': 'tz',
      'Thailand': 'th',
      'Timor-Leste': 'tl',
      'Togo': 'tg',
      'Tokelau': 'tk',
      'Tonga': 'to',
      'Trinidad And Tobago': 'tt',
      'Tunisia': 'tn',
      'Turkey': 'tr',
      'Turkmenistan': 'tm',
      'Turks And Caicos Islands': 'tc',
      'Tuvalu': 'tv',
      'Uganda': 'ug',
      'Ukraine': 'ua',
      'United Arab Emirates': 'ae',
      'United Kingdom': 'gb',
      'United States': 'us',
      'United States Minor Outlying Islands': 'um',
      'Uruguay': 'uy',
      'Uzbekistan': 'uz',
      'Vanuatu': 'vu',
      'Vatican City State': 'va',
      'Venezuela, Bolivarian Republic Of': 've',
      'Viet Nam': 'vn',
      'Virgin Islands, British': 'vg',
      'Virgin Islands, U.S.': 'vi',
      'Wallis And Futuna': 'wf',
      'Western Sahara': 'eh',
      'Yemen': 'ye',
      'Zambia': 'zm',
      'Zimbabwe': 'zw'
    };
  };

  /**
   * Description : List of continent
   * @method getContinentList
   * @return  {Object}.
   */
  Profile.methods.getContinentList = function() {
    var array;
    return array = {
      'Africa': 'AF',
      'Antarctica': 'AN',
      'Asia': 'AS',
      'Europe': 'EU',
      'Oceania': 'OC',
      'North America': 'NA',
      'South America': 'SA'
    };
  };

  /**
   * Description : List of operating systems.
   * @method getOSList
   * @return  {Object}.
   */
  Profile.methods.getOSList = function() {
    var array;
    return array = {
      'Windows 3.11': 'Windows 3.11',
      'Windows 95': 'Windows 95',
      'Windows 98': 'Windows 98',
      'Windows 2000': 'Windows 2000',
      'Windows XP': 'Windows XP',
      'Windows Server 2003': 'Windows Server 2003',
      'Windows Vista': 'Windows Vista',
      'Windows 7': 'Windows 7',
      'Windows NT 4.0': 'Windows NT 4.0',
      'Windows ME': 'Windows ME',
      'Open BSD': 'Open BSD',
      'Sun OS': ' Sun OS',
      'Linux': 'Linux',
      'Mac OS': 'Mac OS',
      'QNX': 'QNX',
      'BeOS': 'BeOS',
      'OS/2': 'OS/2',
      'Search Bot': 'Search Bot'
    };
  };

  /**
   * Description : List of time zones.
   * @method getTimeZoneList
   * @return  {Object}.
   */
  Profile.methods.getTimeZoneList = function() {
    var array;
    return array = {
      "(GMT -12:00) Eniwetok, Kwajalein": '-12.0',
      "(GMT -11:00) Midway Island, Samoa": '-11.0',
      "(GMT -10:00) Hawaii": '-10.0',
      "(GMT -9:00) Alaska": '-9.0',
      "(GMT -8:00) Pacific Time (US & Canada)": '-8.0',
      "(GMT -7:00) Mountain Time (US & Canada)": '-7.0',
      "(GMT -6:00) Central Time (US & Canada), Mexico City": '-6.0',
      "(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima": '-5.0',
      "(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz": '-4.0',
      "(GMT -3:30) Newfoundland": '-3.5',
      "(GMT -3:00) Brazil, Buenos Aires, Georgetown": '-3.0',
      "(GMT -2:00) Mid-Atlantic": '-2.0',
      "(GMT -1:00 hour) Azores, Cape Verde Islands": '-1.0',
      "(GMT) Western Europe Time, London, Lisbon, Casablanca": '0.0',
      "(GMT +1:00) Brussels, Copenhagen, Madrid, Paris": '1.0',
      "(GMT +2:00) Kaliningrad, South Africa": '2.0',
      "(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg": '3.0',
      "(GMT +3:30) Tehran": '3.5',
      "(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi": '4.0',
      "(GMT +4:30) Kabul": '4.5',
      "(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent": '5.0',
      "(GMT +5:30) Bombay, Calcutta, Madras, New Delhi": '5.5',
      "(GMT +5:45) Kathmandu": '5.75',
      "(GMT +6:00) Almaty, Dhaka, Colombo": '6.0',
      "(GMT +7:00) Bangkok, Hanoi, Jakarta": '7.0',
      "(GMT +8:00) Beijing, Perth, Singapore, Hong Kong": '8.0',
      "(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk": '9.0',
      "(GMT +9:30) Adelaide, Darwin": '9.5',
      "(GMT +10:00) Eastern Australia, Guam, Vladivostok": '10.0',
      "(GMT +11:00) Magadan, Solomon Islands, New Caledonia": '11.0',
      "(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka": '12.0'
    };
  };

  /**
   * Description : Recently used device full profile.
   * @method getRecentDevicePorfile
   * @return profile {Object}.
   */
  Profile.methods.getRecentDevicePorfile = function() {
    var key, l, len1, mid, profile, ref1;
    mid = this.recently_used_device;
    if (util.isDefined(this.profiles)) {
      ref1 = this.profiles;
      for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
        profile = ref1[key];
        if (profile._id.toString() === mid) {
          return profile;
        }
      }
    } else {
      return null;
    }
  };

  //LV - Last Visit
  /**
   * Description : If recently used device last visit in not null then return profile.
   * @method getRecentDeviceLVPorfile
   * @return profile {Object|Null}.
   */
  Profile.methods.getRecentDeviceLVPorfile = function() {
    var key, l, len1, mid, profile, ref1, that;
    that = this;
    mid = this.recently_used_device;
    if (util.isDefined(this.profiles)) {
      ref1 = this.profiles;
      for (key = l = 0, len1 = ref1.length; l < len1; key = ++l) {
        profile = ref1[key];
        if (profile._id.toString() === mid && that.lastVisit !== null) {
          return profile;
        }
      }
    } else {
      return null;
    }
  };

  parse_url = function(str, component) {
    var err, i, key, m, mode, parser, uri;
    try {
      key = ['source', 'scheme', 'authority', 'userInfo', 'user', 'pass', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'fragment'];
      mode = 'strict';
      parser = {
        php: /^(?:([^:\/?#]+):)?(?:\/\/()(?:(?:()(?:([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?()(?:(()(?:(?:[^?#\/]*\/)*)()(?:[^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/\/?)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
      };
      m = parser[mode].exec(str);
      uri = {};
      i = 14;
      while (i--) {
        (m[i] ? uri[key[i]] = m[i] : void 0);
      }
      return uri[component.replace('PHP_URL_', '').toLowerCase()](component ? delete uri.source : void 0);
      return uri;
    } catch (error) {
      err = error;
      console.error(new Date() + ": Model->Profile->parse_url(ID:" + this.id + ") - " + err);
      return null;
    }
  };

  urldecode = function(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
  };

  parse_str = function(str, array) {
    var array2, bracket, chr, evalStr, fixStr, glue1, glue2, i, j, key, keys, l, n, o, ref1, ref2, ref3, ref4, results, s, that, tmp, value;
    glue1 = '=';
    glue2 = '&';
    array2 = String(str).replace(/^&?([\s\S]*?)&?$/, '$1').split(glue2);
    that = this;
    fixStr = function(str) {
      return urldecode(str).replace(/([\\"'])/g, '\\$1').replace(/\n/g, '\\n').replace(/\r/g, '\\r');
    };
    if (!array) {
      array = this.window;
    }
    results = [];
    for (i = l = 0, ref1 = array2.length - 1; (0 <= ref1 ? l <= ref1 : l >= ref1); i = 0 <= ref1 ? ++l : --l) {
      tmp = array2[i].split(glue1);
      if (tmp.length < 2) {
        tmp = [tmp, ''];
      }
      key = fixStr(tmp[0]);
      value = fixStr(tmp[1]);
      while (key.charAt(0) === ' ') {
        (key = key.substr(1));
      }
      if (key.indexOf(0) !== -1) {
        key = key.substr(0, key.indexOf(0));
      }
      if (key && key.charAt(0) !== '[') {
        keys = [];
        bracket = 0;
        for (j = n = 0, ref2 = key.length - 1; (0 <= ref2 ? n <= ref2 : n >= ref2); j = 0 <= ref2 ? ++n : --n) {
          if (key.charAt(j) === '[' && !bracket) {
            bracket = j + 1;
          } else if (key.charAt(j) === ']') {
            if (bracket) {
              if (!keys.length) {
                keys.push(key.substr(0, bracket - 1));
              }
              keys.push(key.substr(bracket, j - bracket));
              bracket = 0;
              if (key.charAt(j + 1) !== '[') {
                break;
              }
            }
          }
        }
        if (!keys.length) {
          keys = [key];
        }
        for (j = o = 0, ref3 = keys[0].length - 1; (0 <= ref3 ? o <= ref3 : o >= ref3); j = 0 <= ref3 ? ++o : --o) {
          chr = keys[0].charAt(j);
          if (chr === ' ' || chr === '.' || chr === '[') {
            keys[0] = keys[0].substr(0, j) + '_' + keys[0].substr(j + 1);
          }
          if (chr === '[') {
            break;
          }
        }
        evalStr = 'array';
        for (j = s = 0, ref4 = keys.length - 1; (0 <= ref4 ? s <= ref4 : s >= ref4); j = 0 <= ref4 ? ++s : --s) {
          key = keys[j];
          if ((key !== '' && key !== ' ') || j === 0) {
            key = "'" + key + "'";
          } else {
            key = eval(evalStr + '.push([]);') - 1;
          }
          evalStr += '[' + key + ']';
          if (j !== keys.length - 1 && eval('typeof ' + evalStr) === 'undefined') {
            eval(evalStr + ' = [];');
          }
        }
        evalStr += " = '" + value + "';\n";
        results.push(eval(evalStr));
      } else {
        results.push(void 0);
      }
    }
    return results;
  };

  strnatcmp = function(f_string1, f_string2, f_version) {
    var __strnatcmp_split, array1, array2, i, l, len, r, ref1, result, text;
    i = 0;
    if (f_version === void 0) {
      f_version = false;
    }
    __strnatcmp_split = function(f_string) {
      var buffer, chr, f_stringl, l, ref1, result, text;
      result = [];
      buffer = '';
      chr = '';
      i = 0;
      f_stringl = 0;
      text = true;
      f_stringl = f_string.length;
      for (i = l = 0, ref1 = f_stringl - 1; (0 <= ref1 ? l <= ref1 : l >= ref1); i = 0 <= ref1 ? ++l : --l) {
        chr = f_string.substring(i, i + 1);
        if (chr.match(/\d/)) {
          if (text) {
            if (buffer.length > 0) {
              result[result.length] = buffer;
              buffer = '';
            }
            text = false;
          }
          buffer += chr;
        } else if (text === false && chr === '.' && i < (f_string.length - 1) && f_string.substring(i + 1, i + 2).match(/\d/)) {
          result[result.length] = buffer;
          buffer = '';
        } else {
          if (text === false) {
            if (buffer.length > 0) {
              result[result.length] = parseInt(buffer, 10);
              buffer = '';
            }
            text = true;
          }
          buffer += chr;
        }
      }
      if (buffer.length > 0) {
        if (text) {
          result[result.length] = buffer;
        } else {
          result[result.length] = parseInt(buffer, 10);
        }
      }
      return result;
    };
    array1 = __strnatcmp_split(f_string1 + '');
    array2 = __strnatcmp_split(f_string2 + '');
    len = array1.length;
    text = true;
    result = -1;
    r = 0;
    if (len > array2.length) {
      len = array2.length;
      result = 1;
    }
    for (i = l = 0, ref1 = len - 1; (0 <= ref1 ? l <= ref1 : l >= ref1); i = 0 <= ref1 ? ++l : --l) {
      if (isNaN(array1[i])) {
        if (isNaN(array2[i])) {
          text = true;
          if ((r = this.strcmp(array1[i], array2[i])) !== 0) {
            return r;
          }
        } else if (text) {
          return 1;
        } else {
          return -1;
        }
      } else if (isNaN(array2[i])) {
        if (text) {
          return -1;
        } else {
          return 1;
        }
      } else {
        if (text || f_version) {
          if ((r = array1[i] - array2[i]) !== 0) {
            return r;
          }
        } else {
          if ((r = this.strcmp(array1[i].toString(), array2[i].toString())) !== 0) {
            return r;
          }
        }
        text = false;
      }
    }
    return result;
  };

  module.exports = mongoose.model('profiles', Profile);

}).call(this);


//# sourceMappingURL=profile.js.map
//# sourceURL=coffeescript