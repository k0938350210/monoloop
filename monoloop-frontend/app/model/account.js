const mongoose = require( './../../mongoose');
var config = require('../../config').get('mongodb');

var db = mongoose.createConnection(config.url+config.db_ui+config.options_url);

const  Schema = mongoose.Schema;

const account_plugin_schema = new Schema({
	_id: Schema.ObjectId,
	plugin_id: String,
	enabled: Boolean,
	audience: Number,
	experience: Number,
	goal: Number
});

const account_schema = new Schema({
  _id: Schema.ObjectId,
  uid: Number,
  plugins: [account_plugin_schema]
});




// Model object
module.exports = db.model('Account', account_schema, 'accounts');