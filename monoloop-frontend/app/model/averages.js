(function() {
  // # Customer Averages model
// #
// # Copyright (c) 2009-2012 [Sune Brodersen](mailto:sune@monoloop.com)
// # License: see project's LICENSE file
// # Date: 21.04.13

// # Modules and variables
// mongoose = require __dirname + '/../../mongoose'
// Schema = mongoose.Schema

// AvgElement = new Schema
// 	avg:Number
// 	quintile:[Number]

// # Field definition
// Averages = new Schema
// 	_id: Schema.ObjectId
// 	cid : Number
// 	Pageviews:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	Visits:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	CategoriesInVisit:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	UniqueCategoriesInVisit:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	ProductsInVisit:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	UniqueProductsInVisit:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	TimeOnPageInVisit:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	LenghtOfVisit:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	UniqueProductsPerCategoryInVisit:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	UniqueCategoryRecency:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	UniqueProductRecency:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	ProductRecency:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	CategoryRecency:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	PriceViewedPerCategory:
// 		type:Schema.Types.Mixed
// 	NumOfCustomVars:
// 		type:Number
// 	UniqueCategoryRecencyCurrent:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	UniqueProductRecencyCurrent:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	ProductRecencyCurrent:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement
// 	CategoryRecencyCurrent:
// 		type:Schema.Types.Mixed
// 		ref:AvgElement

// # Model object
// module.exports = mongoose.model 'Averages', Averages


}).call(this);


//# sourceMappingURL=averages.js.map
//# sourceURL=coffeescript