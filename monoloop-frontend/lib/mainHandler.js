(function() {
  var Crypto, _, mainHandler, method;

  _ = require('underscore');

  Crypto = require('crypto');

  mainHandler = function(query) {
    this._query = query;
    this.error = null;
    this.statusCode = null;
    this.cryptSalt = '130293+109k21jl1c31c9312i3c910202';
  };

  method = mainHandler.prototype;

  method.getCryptSalt = function() {
    return this.cryptSalt;
  };

  method.setError = function(error) {
    return this.error = error;
  };

  method.getError = function() {
    return this.error;
  };

  method.setStatusCode = function(statusCode) {
    return this.statusCode = statusCode;
  };

  method.getStatusCode = function() {
    return this.statusCode;
  };

  method.getQuery = function() {
    return this._query;
  };

  method.basicAPI = function(api) {
    var vid;
    api = api || 'tracks';
    if (!this.isDefined(this._query.cid)) {
      this.setError("No CID provided!");
      this.setStatusCode(200);
      return false;
    }
    if (api === 'postback') {
      if (this.profileParams() !== 3) {
        this.setError("Missing GMID, MID or skey. Cannot proceed");
        return false;
      }
      if (!this.postBackhashCheck()) {
        this.setError("GMID and skey does not match - " + api);
        return false;
      }
    } else {
      if (!this.hashCheck()) {
        this.setError("GMID and skey does not match - " + api);
        return false;
      }
      if (!this.isDefined(this._query.url)) {
        this.setError("No url provided!");
        return false;
      }
      if (!this.trackServerSide()) {
        this.setError("TrackServerSide not Provided!");
        return false;
      }
    }
    vid = this.isDefined(this._query.vid) && _.isObject(this._query.vid) ? this._query.vid[this._query.vid.length - 1] : '';
    if (!(!vid || vid.match(/^[0-9a-fA-F]{24}$/))) {
      this.setError("Invalid VID");
      return false;
    }
    return true;
  };

  method.emptyProfileAPI = function(api) {
    if (!this.isDefined(this._query.cid)) {
      this.setError("No CID provided!");
      this.setStatusCode(200);
      return false;
    }
    if (this.profileParams() !== 3) {
      this.setError("Missing GMID, MID or skey. Cannot proceed");
      return false;
    }
    if (!this.hashCheck()) {
      this.setError("GMID and skey does not match - " + api);
      return false;
    }
    return true;
  };

  method.setInformationAPI = function(api) {
    var missingValues, params, requestParams;
    requestParams = Object.keys(this._query);
    params = ['gmid', 'infoID', 'infoType', 'infoValue'];
    missingValues = _.difference(params, requestParams);
    if (missingValues.length > 0) {
      this.setError("Missing " + missingValues + " Cannot proceed");
      return false;
    }
    if (!this.setInformationParams()) {
      this.setError("Some of the parameters have Null or Undefined Values. Cannot proceed");
      return false;
    }
    return true;
  };

  method.getInformationAPI = function(api) {
    var missingValues, params, requestParams;
    requestParams = Object.keys(this._query);
    params = ['gmid', 'infoID', 'infoType'];
    missingValues = _.difference(params, requestParams);
    if (missingValues.length > 0) {
      this.setError("Missing " + missingValues + " Cannot proceed");
      return false;
    }
    if (!this.getInformationParams()) {
      this.setError("Some of the parameters have Null or Undefined Values. Cannot proceed");
      return false;
    }
    return true;
  };

  method.trackAPISanity = function() {
    this.basicAPI();
    return this.callResponse();
  };

  method.postbackAPISanity = function() {
    this.basicAPI('postback');
    return this.callResponse();
  };

  method.emptyProfileAPISanity = function() {
    this.emptyProfileAPI('emptyProfile');
    return this.callResponse();
  };

  method.setInformationAPISanity = function() {
    this.setInformationAPI('setInformation');
    return this.callResponse();
  };

  method.getInformationAPISanity = function() {
    this.getInformationAPI('getInformation');
    return this.callResponse();
  };

  method.hashcheckAPISanity = function() {
    while (true) {
      if (!this.isDefined(this._query.cid)) {
        this.setError("No CID provided!");
        this.setStatusCode(200);
        break;
      }
      if (!this.hashCheck()) {
        this.setError("GMID and skey does not match - hashcheck");
        break;
      }
      break;
    }
    return this.callResponse();
  };

  method.callResponse = function() {
    if (this.getError()) {
      return JSON.stringify({
        status: false,
        error: this.getError(),
        statusCode: this.getStatusCode()
      });
    } else {
      return JSON.stringify({
        status: true,
        error: null,
        statusCode: null
      });
    }
  };

  method.hashCheck = function() {
    var param;
    param = this.profileParams();
    if (((param === 0 || param === 3) && this.createHashAndCompare() === true) || (!this.isDefined(this._query.gmid) || this._query.gmid === '') || (!this.isDefined(this._query.mid) || this._query.mid === '')) {
      return true;
    } else {
      return false;
    }
  };

  method.postBackhashCheck = function() {
    var param;
    param = this.profileParams();
    if (((param === 0 || param === 3) && this.createHashAndCompare() === true) || (!this.isDefined(this._query.gmid) || this._query.gmid === '') || (!this.isDefined(this._query.mid) || this._query.mid === '')) {
      return true;
    } else {
      return false;
    }
  };

  method.profileParams = function() {
    var arr, paramCheck;
    arr = [this._query.gmid, this._query.mid, this._query.skey];
    paramCheck = _.without(arr, void 0, '');
    return paramCheck.length;
  };

  method.setInformationParams = function() {
    var arr, paramCheck;
    arr = [this._query.gmid, this._query.infoType, this._query.infoID, this._query.infoValue];
    paramCheck = _.without(arr, void 0, '');
    if (paramCheck.length !== arr.length) {
      return false;
    } else {
      return true;
    }
  };

  method.getInformationParams = function() {
    var arr, paramCheck;
    if (this._query.infoType === "3") {
      arr = [this._query.gmid, this._query.infoType];
    } else {
      arr = [this._query.gmid, this._query.infoType, this._query.infoID];
    }
    paramCheck = _.without(arr, void 0, '');
    if (paramCheck.length !== arr.length) {
      return false;
    } else {
      return true;
    }
  };

  method.createHashAndCompare = function() {
    if (this.isDefined(this._query.gmid)) {
      if (Crypto.createHash('sha512').update(this._query.gmid + this.getCryptSalt()).digest('hex') === this._query.skey) {
        return true;
      }
    }
    return false;
  };

  method.trackServerSide = function() {
    if (this.isDefined(this._query.url) && _.indexOf(this._query.url, 'trackServerSide') === -1) {
      return true;
    } else {
      return false;
    }
  };

  method.isDefined = function(value) {
    if (!_.isUndefined(value)) {
      return true;
    } else {
      return false;
    }
  };

  module.exports = mainHandler;

}).call(this);
