profileModel = require '../app/model/profile'
async = require 'async'

profileLoader = (request, response, avg, elements)->
  @_profileLoaded = false
  @_MonoloopProfile = new ProfileModel
  @hasToLoadProfile = false
  @hasToLoadAssets = []
  @elemensts = elements
  @avg = avg
  @_request = request
  @_response = response
  @_profileType = ["newProfile", "newProfilePush", "existingProfile"]
  profileID = ""

method = profileLoader.prototype

method.processElements = ->
  async.forEach @elements, ((element, callback) ->
    async.forEach element.content, ((content, callback) ->
      if content.asset
        @hasToLoadAssets.push content.asset
        @hasToLoadProfile = true
        async.forEach content.connectors, ((connector, callback) ->
          if connector is 'MonoloopProfile'
            @hasToLoadProfile = true
            callback()
        ), (err) ->
      callback()
    ), (err) ->
      callback()
  ), (err) ->
    console.log @hasToLoadAssets
    console.log @hasToLoadProfile


method.decideProfileType = (request,response, callback)->
  query = {}
  resultSet = {}
  resultSet['profileType'] = "newProfile"
  resultSet['result'] = []

  if request.query.gmid and helper.isNull(request.query.mid)
    query =
      _id: request.query.gmid
    decideProfile = "newProfilePush"

  if helper.isNull(request.query.gmid) and helper.isNull(request.query.mid)
    decideProfile = "newProfile"

  if request.query.gmid and request.query.mid
    query =
      _id: request.query.gmid
    decideProfile = "dontPush"

  if Object.keys(query).length > 0
    profileCtrl.get request, response, query, (result) ->
      if result.length > 0
        resultSet['profileType'] = decideProfile
        resultSet['result'] = result
        callback null,resultSet
      else
        callback null,resultSet
  else
    callback null,resultSet

module.exports = profileLoader
