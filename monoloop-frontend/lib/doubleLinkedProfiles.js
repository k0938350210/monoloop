(function() {
  var _, async, consumer, doubleLinkedList, method, profileModel;

  consumer = require('./../redis-sentinel-conn');

  profileModel = require('../app/model/profile');

  async = require('async');

  _ = require('underscore');

  method = {};

  method.add = function(cid, profile, cb) {
    var command, gmid, query, that;
    that = this;
    gmid = profile._id;
    command = {};
    query = [];
    return async.auto({
      getLgmid: [
        function(callback) {
          return that.readList(cid,
        function(err,
        lgmid) {
            return callback(err,
        lgmid);
          });
        }
      ],
      pushRedis: [
        'getLgmid',
        function(results,
        callback) {
          return that.createList(cid,
        gmid.toString(),
        function(err,
        success) {
            return callback(err,
        success);
          });
        }
      ],
      relocatePointers: [
        'getLgmid',
        'pushRedis',
        function(results,
        callback) {
          var lgmid,
        ngmid,
        pgmid;
          lgmid = results.getLgmid || null;
          pgmid = profile.pgmid || null;
          ngmid = profile.ngmid || null;
          gmid = gmid.toString();
          if (!lgmid || lgmid === null || lgmid === void 0) {
            //console.log "when lgmid is empty"
            command = {
              _id: gmid
            };
            command.$set = {
              pgmid: null,
              ngmid: null
            };
            query.push(command);
          }
          if (!pgmid && !ngmid) {
            //console.log 'not pgmid and not ngmid'
            if (lgmid !== null && lgmid !== gmid) {
              command = {
                _id: lgmid
              };
              command.$set = {
                ngmid: gmid
              };
              query.push(command);
              command = {
                _id: gmid
              };
              command.$set = {
                pgmid: lgmid,
                ngmid: null
              };
              query.push(command);
            }
          }
          if (pgmid && !ngmid) {
            //console.log 'pgmid and not ngmid'
            if (lgmid !== null && lgmid !== gmid) {
              command = {
                _id: lgmid
              };
              command.$set = {
                ngmid: gmid
              };
              query.push(command);
              command = {
                _id: gmid
              };
              command.$set = {
                pgmid: lgmid,
                ngmid: null
              };
              query.push(command);
            }
          }
          if (pgmid && ngmid) {
            //console.log 'pgmid and ngmid'
            if (lgmid !== null && lgmid !== gmid) {
              command = {
                _id: profile.pgmid
              };
              command.$set = {
                ngmid: profile.ngmid
              };
              query.push(command);
              command = {
                _id: profile.ngmid
              };
              command.$set = {
                pgmid: profile.pgmid
              };
              query.push(command);
              command = {
                _id: lgmid //lgmid = profile.ngmid
              };
              command.$set = {
                ngmid: gmid,
                pgmid: profile.pgmid
              };
              query.push(command);
              command = {
                _id: gmid
              };
              command.$set = {
                pgmid: lgmid,
                ngmid: null
              };
              query.push(command);
            }
          }
          if (!pgmid && ngmid) {
            //console.log 'not pgmid and ngmid'
            if (lgmid !== null && lgmid !== gmid) {
              command = {
                _id: profile.ngmid
              };
              command.$set = {
                pgmid: null
              };
              query.push(command);
              command = {
                _id: lgmid
              };
              command.$set = {
                ngmid: gmid
              };
              query.push(command);
              command = {
                _id: gmid
              };
              command.$set = {
                pgmid: lgmid,
                ngmid: null
              };
              query.push(command);
            }
          }
          return callback(null,
        query);
        }
      ]
    }, function(err, results) {
      var result;
      if (err) {
        return cb(err, null);
      } else {
        result = results.relocatePointers;
        if (!_.isEmpty(result)) {
          return async.each(result, (function(result, callback) {
            if (result._id) {
              return profileModel.findOneAndUpdate({
                _id: result._id
              }, {
                $set: result.$set
              }, function(err, updateResult) {
                if (err) {
                  return callback(err);
                } else {
                  return callback(null, null);
                }
              });
            } else {
              return callback(null, null);
            }
          }), function(err) {
            return cb(err, result);
          });
        } else {
          return cb(null, result);
        }
      }
    });
  };

  method.readList = function(cid, cb) {
    var status;
    status = consumer.status;
    if (status === 'ready') {
      /*consumer.lrange cid.toString(), 0, -1, (err, listdata)->
      console.log listdata*/
      return consumer.lpop(cid.toString(), function(err, result) {
        if (err) {
          return cb(err);
        } else if (result === null || result === void 0) {
          return cb(null, null);
        } else {
          return cb(null, result);
        }
      });
    } else {
      return profileModel.find({
        customerID: cid
      }).sort({
        _id: -1
      }).skip(1).limit(1).exec(function(err, result) {
        var profile;
        if (err) {
          return cb(err);
        } else if (result !== void 0 && result.length > 0) {
          profile = result[0];
          return cb(null, profile._id);
        } else {
          return cb(null, null);
        }
      });
    }
  };

  method.createList = function(cid, gmid, cb) {
    var status;
    status = consumer.status;
    if (status === 'ready') {
      return consumer.lpush(cid, gmid, function(err, success) {
        if (err) {
          return cb(err, null);
        } else {
          return cb(null, success);
        }
      });
    } else {
      return cb(null, null);
    }
  };

  //update ngmid and pgmid on emptyprofile also update data into redis
  method.update = function(profile, cb) {
    var cid, gmid, ngmid, pgmid, that;
    that = this;
    cid = profile.customerID;
    gmid = profile._id;
    pgmid = profile.pgmid || null;
    ngmid = profile.ngmid || null;
    return async.auto({
      relocatePointers: [
        function(callback) {
          if (ngmid === null && pgmid === null) {
            return callback(null,
        true);
          } else if (ngmid === null && pgmid !== null) {
            return profileModel.updateOne({
              _id: pgmid
            },
        {
              $set: {
                ngmid: null
              }
            },
        function(err,
        update) {
              return callback(err,
        update);
            });
          } else if (ngmid !== null && pgmid === null) {
            return profileModel.updateOne({
              _id: ngmid
            },
        {
              $set: {
                pgmid: null
              }
            },
        function(err,
        update) {
              return callback(err,
        update);
            });
          } else if (ngmid !== null && pgmid !== null) {
            return profileModel.updateOne({
              _id: pgmid
            },
        {
              $set: {
                ngmid: ngmid
              }
            },
        function(err,
        update) {
              if (err) {
                return callback(err,
        null);
              } else {
                return profileModel.updateOne({
                  _id: ngmid
                },
        {
                  $set: {
                    pgmid: pgmid
                  }
                },
        function(err,
        update) {
                  return callback(err,
        update);
                });
              }
            });
          }
        }
      ],
      updateRedis: [
        'relocatePointers',
        function(results,
        callback) {
          var status;
          status = consumer.status;
          //this is last inserted profile
          if (status === 'ready') {
            return consumer.lpop(cid.toString(),
        function(err,
        result) {
              if (err) {
                return callback(err);
              } else if (result === null || result === void 0) {
                return callback(null,
        null);
              } else {
                if (result === gmid.toString()) {
                  //profileModel.find({customerID: cid, _id :{$ne:gmid},cachedProperties:{$exists:true} }).sort({_id: -1}).limit(1).exec (err, result) ->
                  return profileModel.find({
                    customerID: cid,
                    _id: {
                      $ne: gmid
                    }
                  }).sort({
                    _id: -1
                  }).limit(1).exec(function(err,
        result) {
                    if (err) {
                      return callback(err);
                    } else if (result !== void 0 && result.length > 0) {
                      return that.createList(cid.toString(),
        result[0]._id.toString(),
        function(err,
        success) {
                        return callback(err,
        success);
                      });
                    } else {
                      return callback(null,
        null);
                    }
                  });
                } else {
                  return that.createList(cid,
        result,
        function(err,
        success) {
                    return callback(err,
        success);
                  });
                }
              }
            });
          } else {
            return callback(null,
        null);
          }
        }
      ]
    }, function(err, result) {
      return cb(err, result);
    });
  };

  module.exports = method;

}).call(this);


//# sourceMappingURL=doubleLinkedProfiles.js.map
//# sourceURL=coffeescript