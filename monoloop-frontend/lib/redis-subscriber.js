let Redis = require('ioredis');

let config = require('../config').get('redisSsentinel');
let publisher = new Redis({
  sentinels: config.sentinels,
  name: config.name
});

let local_redis = new Redis();

let debug = require('debug')('redis');

module.exports = {
  subscribeInvalidate(){
    let me = this;
    publisher.on("message", function(channel, message) {
      if(channel === 'invalidate'){
        me.invalidate(message);
      }
    });
    publisher.subscribe("invalidate",function(err, count){
      if(err){
        debug('Cannot subscribe to invalidate channel');
      }else{
        debug('Success to subscribe to invalidate channel');
      }
    });
  },

  invalidate(message){

    let key, keySplit;
    key = message;

    if (key && typeof key === "string") {
      keySplit = key.split("_");
      key = keySplit[0] + "_" + keySplit[1];
      return local_redis.del(key, function(err, status) {
        if (!err) {
          if (status === 1) {
            debug(new Date() + ' : ' + key + ' has been deleted from redis');
          } else {
            debug(new Date() + ' : ' + 'No key found in redis!');
          }
        } else {
          debug(new Date() + ' : ' + 'Could not perform delete operation!');
        }
      });
    } else {
      debug(new Date() + ' : ' + 'Seems correct parameter is not provided!');
    }

  }
};