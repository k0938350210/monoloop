_ = require 'underscore'
Crypto = require 'crypto'

mainHandler = (query)->
  @_query = query
  @error = null
  @statusCode = null
  @cryptSalt = '130293+109k21jl1c31c9312i3c910202'
  return

method = mainHandler.prototype

method.getCryptSalt = ->
  return @cryptSalt

method.setError = (error)->
  @error = error

method.getError = ()->
  return @error

method.setStatusCode = (statusCode)->
  @statusCode = statusCode

method.getStatusCode = ()->
  return @statusCode

method.getQuery = ->
  @_query

method.basicAPI = (api)->
  api = api || 'tracks'

  if not @isDefined(@_query.cid) then @setError("No CID provided!"); @setStatusCode(200); return false
  if api is 'postback'
    if @profileParams() isnt 3
      @setError("Missing GMID, MID or skey. Cannot proceed"); return false
    if not @postBackhashCheck() then @setError("GMID and skey does not match - "+api); return false
  else
    if not @hashCheck() then @setError("GMID and skey does not match - "+api); return false
    if not @isDefined(@_query.url) then @setError("No url provided!"); return false
    if not @trackServerSide() then @setError("TrackServerSide not Provided!"); return false
  vid = if @isDefined(@_query.vid) and _.isObject(@_query.vid) then  @_query.vid[@_query.vid.length-1] else ''
  if not (not vid or vid.match(/^[0-9a-fA-F]{24}$/)) then @setError("Invalid VID"); return false
  return true

method.emptyProfileAPI = (api)->
  if not @isDefined(@_query.cid) then @setError("No CID provided!"); @setStatusCode(200); return false
  if @profileParams() isnt 3
      @setError("Missing GMID, MID or skey. Cannot proceed"); return false
  if not @hashCheck() then @setError("GMID and skey does not match - "+api); return false
  return true

method.setInformationAPI = (api)->
  requestParams = Object.keys @_query
  params = ['gmid', 'infoID','infoType', 'infoValue']
  missingValues = _.difference(params,requestParams )
  if missingValues.length > 0
    @setError("Missing "+missingValues+" Cannot proceed"); return false
  if !@setInformationParams()
    @setError("Some of the parameters have Null or Undefined Values. Cannot proceed"); return false
  return true

method.getInformationAPI = (api)->
  requestParams = Object.keys @_query
  params = ['gmid', 'infoID','infoType']
  missingValues = _.difference(params,requestParams )
  if missingValues.length > 0
    @setError("Missing "+missingValues+" Cannot proceed"); return false
  if !@getInformationParams()
    @setError("Some of the parameters have Null or Undefined Values. Cannot proceed"); return false
  return true



method.trackAPISanity = ->
  @basicAPI()
  @callResponse()

method.postbackAPISanity = ->
  @basicAPI('postback')
  @callResponse()

method.emptyProfileAPISanity = ->
  @emptyProfileAPI('emptyProfile')
  @callResponse()

method.setInformationAPISanity = ->
  @setInformationAPI('setInformation')
  @callResponse()

method.getInformationAPISanity = ->
  @getInformationAPI('getInformation')
  @callResponse()


method.hashcheckAPISanity = ->
  while(true)
    if not @isDefined(@_query.cid) then @setError("No CID provided!"); @setStatusCode(200); break
    if not @hashCheck() then @setError("GMID and skey does not match - hashcheck"); break
    break;

  @callResponse()

method.callResponse = ->
  return if @getError() then JSON.stringify({status: false, error: @getError(), statusCode: @getStatusCode()}) else JSON.stringify({status: true, error: null, statusCode: null})

method.hashCheck = ->
  param = @profileParams()
  return if (((param is 0 || param is 3)  and @createHashAndCompare() is true) || (not @isDefined(@_query.gmid) || @_query.gmid is '') || (not @isDefined(@_query.mid) || @_query.mid is '')) then true else false

method.postBackhashCheck = ->
  param = @profileParams()
  return if ((((param is 0 || param is 3)  and @createHashAndCompare() is true)) || (not @isDefined(@_query.gmid) || @_query.gmid is '') || (not @isDefined(@_query.mid) || @_query.mid is '')) then true else false

method.profileParams = ->
  arr = [@_query.gmid,@_query.mid, @_query.skey]
  paramCheck = _.without(arr, undefined, '')
  return paramCheck.length


method.setInformationParams = ->
  arr = [@_query.gmid,@_query.infoType, @_query.infoID, @_query.infoValue]
  paramCheck = _.without(arr, undefined, '')
  if paramCheck.length isnt arr.length
    return false
  else
    return true

method.getInformationParams = ->
  if @_query.infoType is "3"
    arr = [@_query.gmid,@_query.infoType]
  else
    arr = [@_query.gmid,@_query.infoType, @_query.infoID]
  paramCheck = _.without(arr, undefined, '')
  if paramCheck.length isnt arr.length
    return false
  else
    return true


method.createHashAndCompare = ->
  if @isDefined(@_query.gmid)
    if Crypto.createHash('sha512').update(@_query.gmid+@getCryptSalt()).digest('hex') is @_query.skey
      return true
  return false

method.trackServerSide = ->
  if @isDefined(@_query.url) and _.indexOf(@_query.url,'trackServerSide') is -1
    return true
  else
    return false

method.isDefined = (value)->
  return if not _.isUndefined(value) then true else false

module.exports = mainHandler
