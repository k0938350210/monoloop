consumer = require('./../redis-sentinel-conn')
profileModel = require('../app/model/profile')
async = require 'async'
_ =require('underscore')
doubleLinkedList = ()->

method = doubleLinkedList.prototype
method.add = (cid, profile, cb)->
  that = @
  gmid = profile._id
  command={}
  query=[]
  async.auto
    getLgmid:[(callback)->
      that.readList cid, (err, lgmid) ->
        callback err,lgmid
    ]
    pushRedis:['getLgmid',(results,callback)->
      that.createList cid,gmid.toString(),(err,success)->
        callback err, success
    ]
    relocatePointers: ['getLgmid','pushRedis',(results,callback)->
      lgmid = results.getLgmid || null
      pgmid = profile.pgmid || null
      ngmid = profile.ngmid || null

      gmid = gmid.toString()
      if !lgmid or lgmid is null or lgmid is undefined
        #console.log "when lgmid is empty"
        command={_id:gmid}
        command.$set={
          pgmid: null
          ngmid: null
        }
        query.push(command)

      if !pgmid and !ngmid
        #console.log 'not pgmid and not ngmid'
        if lgmid isnt null and lgmid isnt gmid
          command = {_id:lgmid}
          command.$set = {ngmid: gmid}
          query.push(command)
          command = {_id:gmid}
          command.$set= {pgmid: lgmid,ngmid:null}
          query.push(command)

      if pgmid and !ngmid
        #console.log 'pgmid and not ngmid'
        if lgmid isnt null and lgmid isnt gmid
          command = {_id:lgmid}
          command.$set = {ngmid: gmid}
          query.push(command)
          command = {_id:gmid}
          command.$set = {pgmid: lgmid,ngmid:null}
          query.push(command)

      if pgmid and ngmid
        #console.log 'pgmid and ngmid'
        if lgmid isnt null and lgmid isnt gmid
          command = {_id:profile.pgmid}
          command.$set = {ngmid: profile.ngmid}
          query.push(command)
          command = {_id: profile.ngmid}
          command.$set = {pgmid: profile.pgmid}
          query.push(command)
          command = {_id:lgmid}  #lgmid = profile.ngmid
          command.$set = {ngmid: gmid, pgmid: profile.pgmid}
          query.push(command)
          command = {_id:gmid}
          command.$set = {pgmid: lgmid, ngmid: null}
          query.push(command)

      if !pgmid and ngmid
        #console.log 'not pgmid and ngmid'
        if lgmid isnt null and lgmid isnt gmid
          command = {_id:profile.ngmid}
          command.$set = {pgmid: null}
          query.push(command)
          command = {_id: lgmid}
          command.$set = { ngmid: gmid}
          query.push(command)
          command = {_id:gmid}
          command.$set = {pgmid: lgmid, ngmid: null}
          query.push(command)

      callback null,query
    ], (err,results)->
      if err
        cb err,null
      else
        result= results.relocatePointers
        if not _.isEmpty(result)
          async.each result, ((result,callback)->
            if result._id
              profileModel.findOneAndUpdate {_id: result._id}, {$set:result.$set}, (err, updateResult)->
                if err
                  callback err
                else
                  callback null,null
            else
              callback null,null

          ),(err)->
            cb err,result
        else
          cb null,result


method.readList = (cid, cb)->
  status = consumer.status
  if status is 'ready'
    ###consumer.lrange cid.toString(), 0, -1, (err, listdata)->
      console.log listdata###
    consumer.lpop cid.toString(), (err, result)->
      if err
        cb err
      else if result is null or result is undefined
        return cb null, null
      else
        return cb null, result
  else
    profileModel.find({customerID: cid}).sort({_id: -1}).skip(1).limit(1).exec (err, result) ->
      if err
        cb err
      else if result isnt undefined and result.length > 0
        profile = result[0]
        return cb null, profile._id
      else
        return cb null, null

method.createList = (cid, gmid, cb)->
  status = consumer.status
  if status is 'ready'
    consumer.lpush cid, gmid, (err, success)->
      if err
        cb err, null
      else
        cb null, success
  else
    cb null,null

#update ngmid and pgmid on emptyprofile also update data into redis
method.update = (profile,cb)->
  that = @
  cid = profile.customerID
  gmid = profile._id
  pgmid = profile.pgmid || null
  ngmid = profile.ngmid || null
  async.auto
    relocatePointers:[(callback)->
      if ngmid is null and pgmid is null
        callback null, true
      else if ngmid is null and pgmid isnt null
        profileModel.updateOne({_id:pgmid},{ $set: { ngmid: null }},(err,update)->
          callback err,update
        )
      else if ngmid isnt null and pgmid is null
        profileModel.updateOne {_id:ngmid} , {$set:pgmid:null},(err,update)->
          callback err,update
      else if ngmid isnt null and pgmid isnt null
        profileModel.updateOne {_id:pgmid} , {$set:ngmid:ngmid},(err,update)->
          if err
            callback err,null
          else
            profileModel.updateOne {_id:ngmid} , {$set:pgmid:pgmid},(err,update)->
              callback err,update
    ],
    updateRedis:['relocatePointers',(results,callback)->
      status = consumer.status
      #this is last inserted profile
      if status is 'ready'
        consumer.lpop cid.toString(), (err, result)->
          if err
            callback err
          else if result is null or result is undefined
            callback null, null
          else
            if result is gmid.toString()
              #profileModel.find({customerID: cid, _id :{$ne:gmid},cachedProperties:{$exists:true} }).sort({_id: -1}).limit(1).exec (err, result) ->
              profileModel.find({customerID: cid, _id :{$ne:gmid}}).sort({_id: -1}).limit(1).exec (err, result) ->
                if err
                  callback err
                else if result isnt undefined and result.length > 0
                  that.createList cid.toString(), result[0]._id.toString(),(err,success)->
                    callback err, success
                else
                  callback null , null
            else
              that.createList cid,result,(err,success)->
                callback err, success
      else
        callback null ,null
    ],
    (err,result)->
      cb(err,result)
module.exports = doubleLinkedList

