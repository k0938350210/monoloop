responseHandler = (response)->
  @response = response
  return

responseHandler.prototype.buildResponse = (status, msg, err)->
  status = status || ''
  msg = msg || ''
  err = err || ''
  if status
    @response.writeHead status, { 'Content-Type': 'text/javascript'}
  if err
    @response.end 'MONOloop.MonoloopData = {"error":"'+err+'"};'
  else if msg
    @response.end('MONOloop.MonoloopData=' + JSON.stringify(msg));

module.exports = responseHandler