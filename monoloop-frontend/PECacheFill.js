// Generated by CoffeeScript 1.3.3
(function() {
  var cryptSalt, doc1, doc2, docs, memcached, url, urlhash;

  url = 'http://www.webside.com/';

  cryptSalt = '130293+109k21jl1c31c9312i3c910202';

  urlhash = 'PE.8122f07b3bb1a4a7-50e912c5';

  doc1 = {
    _id: "50af94ae621ad61308c0fe2f",
    cid: "1",
    urlhash: "7a996fcf3996e899b8b884af",
    fullURL: "http://www.webside.com:8080/",
    pageID: "153",
    hidden: "0",
    exactQS: "",
    inWWW: true,
    exWWW: false,
    additionalJS: "",
    content: [
      {
        name: "Sune CV Test",
        connectors: ["MonoloopProfile"],
        uid: "4817",
        mappedTo: "HTML BODY",
        placementType: "1",
        order: "1",
        displayType: "1",
        addJS: "",
        code: "Just some content with <p>HTML</p>"
      }
    ]
  };

  doc2 = {
    _id: "50af94cc621ad61308c0fe30",
    cid: "1",
    urlhash: "7a996fcf3996e899b8b884af",
    fullURL: "http://www.webside.com:8080/",
    pageID: "153",
    hidden: "0",
    exactQS: "",
    inWWW: true,
    exWWW: false,
    additionalJS: "",
    content: [
      {
        name: "Sune CV Test",
        connectors: ["MonoloopProfile"],
        uid: "4817",
        mappedTo: "HTML BODY",
        placementType: "1",
        order: "1",
        displayType: "1",
        addJS: "",
        code: "Just some content with <p>HTML</p><% if (MonoloopProfile.PageViewCount > 30) { %> Conditional content <% } %>"
      }
    ]
  };

  docs = [doc1, doc2];

  console.log(docs);

  console.log(url);

  console.log(urlhash);

  memcached = require('./memcached');

  memcached.set(urlhash, docs, 10000, function(error, result) {
    memcached.end();
    if (error) {
      console.log(error);
    } else {
      console.log(result);
      console.log('saved to memcached');
    }
    return memcached.get(urlhash, function(error, result) {
      console.log(result);
      return console.log(error);
    });
  });

}).call(this);
