#!/bin/bash

cat << EOF > /etc/sentinel.conf
protected-mode no
port 26379
sentinel monitor mymaster redis 6379 2
sentinel down-after-milliseconds mymaster 5000
sentinel failover-timeout mymaster 60000
sentinel parallel-syncs mymaster 1
EOF

cat << EOF > /etc/sentinel1.conf
protected-mode no
port 26380
sentinel monitor mymaster redis 6379 2
sentinel down-after-milliseconds mymaster 5000
sentinel failover-timeout mymaster 60000
sentinel parallel-syncs mymaster 1
EOF

cat << EOF > /etc/sentinel2.conf
protected-mode no
port 26381
sentinel monitor mymaster redis 6379 2
sentinel down-after-milliseconds mymaster 5000
sentinel failover-timeout mymaster 60000
sentinel parallel-syncs mymaster 1
EOF

redis-server --daemonize yes
redis-server /etc/sentinel.conf --sentinel --daemonize yes
redis-server /etc/sentinel1.conf --sentinel --daemonize yes
redis-server /etc/sentinel2.conf --sentinel --daemonize yes
#mongod --fork --logpath=/var/lib/mongodb/mongo.log

cd /fe
nginx && pm2-runtime ecosystem.config.js --env testing

