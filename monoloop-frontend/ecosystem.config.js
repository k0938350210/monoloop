module.exports = {
  apps : [
    {
      name: "core", // use realtime, api endpoint ( postback, profile, ...)
      script: "./app.js",
      watch: true,
      env_development: {
        "NODE_ENV": "development"
      },
      env_production: {
        "NODE_ENV": "production"
      },
      env_staging: {
        "NODE_ENV": "staging"
      },
      env_testing: {
        "NODE_ENV": "testing"
      }
    },{
      name: "worker", // archive, webhook send ( send to webhook service )
      script: "./workers/worker.js",
      watch: true,
      env_development: {
        "NODE_ENV": "development"
      },
      env_production: {
        "NODE_ENV": "production"
      },
      env_staging: {
        "NODE_ENV": "staging"
      },
      env_testing: {
        "NODE_ENV": "testing"
      }
    },{
      name: "webhook", // aws webhook process
      script: "./workers/webhook.js",
      watch: true,
      env_development: {
        "NODE_ENV": "development"
      },
      env_production: {
        "NODE_ENV": "production"
      },
      env_staging: {
        "NODE_ENV": "staging"
      },
      env_testing: {
        "NODE_ENV": "testing"
      }
    },{
      name: "healthcheck", // health check process for restart core servie
      script: "./workers/healthcheck.js",
      watch: false,
      env_development: {
        "NODE_ENV": "development"
      },
      env_production: {
        "NODE_ENV": "production"
      },
      env_staging: {
        "NODE_ENV": "staging"
      },
      env_testing: {
        "NODE_ENV": "testing"
      }
    },{
      name: "geoip-update", // cron job for update geoip file. schedule 1 hour
      watch: false,
      script: "./node_modules/geoip-lite/scripts/updatedb.js",
      cron_restart: "0 * * * *",
      autorestart: false,
      env_development: {
        "NODE_ENV": "development"
      },
      env_production: {
        "NODE_ENV": "production"
      },
      env_staging: {
        "NODE_ENV": "staging"
      },
      env_testing: {
        "NODE_ENV": "testing"
      }
    }
  ],
  deploy: {
    'staging': {
      "key"  : "~/.ssh/m_staging.pem", // path to the public key to authenticate
      "user" : "ec2-user",              // user used to authenticate
      "host" : "54.247.72.248",      // where to connect
      "ref"  : "origin/master",
      "ssh_options": "ForwardAgent=yes",
      "repo" : "git@monoloop.git.beanstalkapp.com:/monoloop/monoloop-frontend.git",
      "path" : "/var/www/fe",
      "post-deploy" : "npm install;pm2 startOrRestart /var/www/fe/ecosystem.config.js --env staging"
    },
    'production':{
      "key"  : "~/.ssh/m_dedicated_test.pem", // path to the public key to authenticate
      "user" : "ec2-user",              // user used to authenticate
      "host" : "52.48.191.187",      // where to connect
      "ref"  : "origin/master",
      "ssh_options": "ForwardAgent=yes",
      "repo" : "git@monoloop.git.beanstalkapp.com:/monoloop/monoloop-frontend.git",
      "path" : "/fe",
      "post-deploy" : "npm install;pm2 startOrRestart /fe/ecosystem.config.js --env staging"
    }
  }
}
