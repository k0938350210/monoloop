(function() {
  // Connection manager for MongoDb

  // Copyright (c) 2009-2012 [Vyacheslav Voronchuk](mailto:voronchuk@gmail.com), [Web management software](http://wm-software.com)
  // Licensed to [Desk.no Ltd](http://www.desk.no)

  // Connection settings
  var config, mongoose, debug;

  mongoose = require('mongoose');

  config = require('./config').get('mongodb');

  debug = require('debug')('ml:mongo');

  mongoose.connect(`${config.url}${config.db}${config.options_url}`,config.options, function(err) {
    if (err) {
      return debug("Mongoose connection Error : " + err);
    } else {
      return debug("mongodb connected");
    }
  });

  mongoose.Promise = global.Promise;

  module.exports = mongoose;

}).call(this);


//# sourceMappingURL=mongoose.js.map
//# sourceURL=coffeescript