var element_model = require('./../app/model/element');
const mongoose = require('mongoose');

module.exports = {
  async seed(){
    console.log('seed');
    // 100% experience group / difference experience
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : "",
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      },{
        "controlGroupSize" : 0,
        "controlGroupDays" : 30.0,
        "experimentID" : 67,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : "",
        "goalID" : 217,
        "segmentID" : 23,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4589")
      }],
      "content" : [ {
        "uid" : null,
        "mappedTo" : "",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount > '5000' ) ){ %>pass<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      },{
        "uid" : null,
        "mappedTo" : "",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 67,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5000' ) ){ %>pass<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4590")
      }]
    })).save();
  }
}