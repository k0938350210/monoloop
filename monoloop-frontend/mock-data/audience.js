var segment_model = require('./../app/model/segment');
const mongoose = require('mongoose');

module.exports = {
  async seed(){

  	await (new segment_model({
  		_id: new mongoose.Types.ObjectId('5cd2b0c3ee4e831245014985'),
      name: 'test-001',
      uid: 1001,
      cid: 1001,
      hidden: 0,
      deleted: 0,
      condition: "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}"
    })).save();

    await (new segment_model({
    	_id: new mongoose.Types.ObjectId('5cd2b1682f17d51449e8dc07'),
      name: 'test-002',
      uid: 1002,
      cid: 1001,
      hidden: 0,
      deleted: 0,
      condition: "if ( ( MonoloopProfile.PageViewCount < '10' ) ){|}"
    })).save();

    await (new segment_model({
    	_id: new mongoose.Types.ObjectId('5cd2b1682f17d51449e8dc08'),
      name: 'test-003',
      uid: 1003,
      cid: 1001,
      hidden: 0,
      deleted: 0,
      condition: "if ( ( MonoloopProfile.PageViewCount > '10' ) ){|}"
    })).save();

  }
}