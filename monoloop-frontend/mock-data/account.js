var account_model = require('./../app/model/account');
const mongoose = require('mongoose');

module.exports = {
  async seedWithAllPlugins(uid){
    await (new account_model({
      _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
      uid: uid,
      plugins: [{
        _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
        enabled: true,
        audience: 1,
        experience: 1,
        goal: 1
      }]
    })).save();
  },
  async seedWithDisabledPlugins(uid){
    await (new account_model({
      _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
      uid: uid,
      plugins: [{
        _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
        enabled: false,
        audience: 1,
        experience: 1,
        goal: 1
      }]
    })).save();
  },
  async seedWithDisabledGoalOptions(uid){
    await (new account_model({
      _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
      uid: uid,
      plugins: [{
        _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
        enabled: true,
        audience: 1,
        experience: 1,
        goal: 0
      }]
    })).save();
  },
  async seedWithDisabledSegmentOptions(uid){
    await (new account_model({
      _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
      uid: uid,
      plugins: [{
        _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
        enabled: true,
        audience: 0,
        experience: 1,
        goal: 1
      }]
    })).save();
  },
  async seedWithDisabledExperienceOptions(uid){
    await (new account_model({
      _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
      uid: uid,
      plugins: [{
        _id: new mongoose.Types.ObjectId('5c66ad50242336ba148b4567'),
        enabled: true,
        audience: 1,
        experience: 0,
        goal: 1
      }]
    })).save();
  }
}