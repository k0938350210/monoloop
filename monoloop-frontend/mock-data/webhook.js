var webhook_model = require('./../app/model/webhook');
const mongoose = require('mongoose');

module.exports = {
  async seedInactive(){
    await (new webhook_model({
      "_id" : new mongoose.Types.ObjectId("5d3a837d24233605158b4568"),
      "cid" : 1001,
      "event_name": "50 hours",
      "event_id": 0,
      "payload_type": "Full",
      "callback_url" : "https://enhs0rknea0d.x.pipedream.net",
      "status" : "active",
      "event_type" : "Inactive"
    })).save();

    await (new webhook_model({
      "_id" : new mongoose.Types.ObjectId("5d3a84152423369a208b4567"),
      "cid" : 1001,
      "event_name": "30 minutes",
      "event_id": 0,
      "payload_type": "Lean",
      "callback_url" : "https://enhs0rknea0d.x.pipedream.net",
      "status" : "active",
      "event_type" : "Inactive"
    })).save();
  }
}