var element_model = require('./../app/model/element');
const mongoose = require('mongoose');

module.exports = {
  async seed(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c6a4c24242336e1668b4567"),
      "urlhash" : "45f460c38b83ff9fad73ad24",
      "fullURL" : "http://local-demo1wp.com/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "content" : [ {
        "uid" : 1001,
        "goal" : true,
        "p" : 10,
        "connectors" : [],
        "segments" : null,
        "code" : " <%  goals.push({ 'uid' : 1,'p' : 10 }); %>",
        "_id" : new mongoose.Types.ObjectId("5c6a512b242336be678b456f")
      },{
        "uid" : 1002,
      	"goal" : true,
      	"p" : 20,
        "code" : " <% if ( ( MonoloopProfile.VisitCount < '10' ) ){  goals.push({ 'uid' : 2,'p' : 20 });} %>",
        "connectors" : [
          "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c6a512d242336e1668b456e")
      }]
    })).save();
  },

  async seed2(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c6a4c24242336e1668b4567"),
      "urlhash" : "45f460c38b83ff9fad73ad24",
      "fullURL" : "http://local-demo1wp.com/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "content" : [ {
        "uid" : 1001,
        "goal" : true,
        "p" : 10,
        "connectors" : [],
        "segments" : null,
        "code" : " <%  goals.push({ 'uid' : 1,'p' : 10 }); %>",
        "_id" : new mongoose.Types.ObjectId("5c6a512b242336be678b456f")
      },{
        "uid" : 1002,
        "goal" : true,
        "p" : 20,
        "code" : " <% if ( ( MonoloopProfile.VisitCount > '10' ) ){  goals.push({ 'uid' : 2,'p' : 20 });} %>",
        "connectors" : [
          "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c6a512d242336e1668b456e")
      }]
    })).save();
  },

  async seedInvalidSyntax(){
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c6a4c24242336e1668b4567"),
      "urlhash" : "45f460c38b83ff9fad73ad24",
      "fullURL" : "http://local-demo1wp.com/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "content" : [{
        "uid" : 1002,
        "goal" : true,
        "p" : 20,
        "code" : " <% if ( ( MonoloopProfile.cv[6871][1] !== 'empty' ) ){  goals.push({ 'uid' : 2,'p' : 20 });} %>",
        "connectors" : [
          "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c6a512d242336e1668b456e")
      }]
    })).save();
  }

}