var element_model = require('./../app/model/element');
const mongoose = require('mongoose');

module.exports = {
  async seed(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "45f460c38b83ff9fad73ad24",
      "fullURL" : "http://local-demo1wp.com/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "content" : [ {
        "uid" : 1001,
        "mappedTo" : "body",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "code" : "Only content",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      },{
        "uid" : 1002,
        "mappedTo" : "body",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "code" : "Only content2",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4590")
      }]
    })).save();
  },

  async seed2(){
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5d5bd2ad4dfa09b0418b4567"),
      "urlhash" : "df530f0ab466a4153f4b2095",
      "fullURL" : "https://eksido.com/demo/boozt/landingpage/",
      "cid" : 2580,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "content" : [ {
        "uid" : 901,
        "name": "",
        "mappedTo" : "DIV.row.single-page-content P:eq(0)",
        "placementType" : "5",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "code" : "Only content",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5d5bd5304dfa09e5458b456a")
      }]
    })).save();
  },

  async seed3(){
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5d5bd2ad4dfa09b0418b4567"),
      "urlhash" : "df530f0ab466a4153f4b2095",
      "fullURL" : "https://eksido.com/demo/boozt/landingpage/",
      "cid" : 2580,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 2,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "startsWith": true,
      "content" : [ {
        "uid" : 901,
        "name": "",
        "mappedTo" : "DIV.row.single-page-content P:eq(0)",
        "placementType" : "5",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "code" : "Only content",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5d5bd5304dfa09e5458b456a")
      }]
    })).save();
  },

  async seed4(){
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5d5bd2ad4dfa09b0418b4567"),
      "urlhash" : "df530f0ab466a4153f4b2095",
      "fullURL" : "https://eksido.com/demo/boozt/landingpage/",
      "cid" : 2580,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 2,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "startsWith": true,
      "content" : [ {
        "uid" : 901,
        "name": "",
        "mappedTo" : "DIV.row.single-page-content P:eq(0)",
        "placementType" : "5",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "code" : "<% if ( MonoloopProfile.utm_campaign === 'demo' ){ %>Only content<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5d5bd5304dfa09e5458b456a")
      }]
    })).save();
  }
}