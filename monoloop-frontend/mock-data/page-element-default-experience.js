var element_model = require('./../app/model/element');
const mongoose = require('mongoose');

module.exports = {
  async seed(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      },{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 67,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 2,
        "goalID" : 217,
        "segmentID" : 23,
        "updated_at": new Date(),
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4589")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      },{
        "uid" : 2,
        "mappedTo" : "#A002",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 67,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass2<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4590")
      }]
    })).save();

    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c861207242336ef2e8b456e"),
      "urlhash" : "48660e91ebe5483f36758f46",
      "fullURL" : "http://mon-experiences-1001/?goal-reach=true",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "content" : [ {
        "goal" : true,
        "p" : 0,
        "code" : " <% if ( ( MonoloopProfile.VisitCount < '200' ) ){  goals.push({ 'uid' : 216,'p' : 0 });} %>",
        "uid" : 216,
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c861207242336ef2e8b4572")
      }]
    })).save();
  },

  async seed2(){
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5af30464242336253c8b4579"),
      "urlhash" : "45f460c38b83ff9fad73ad24",
      "fullURL" : "http://local-demo1wp.com/?t111",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : "",
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date('2017-01-01'),
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5af30464242336253c8b4579")
      }],
      "content" : []
    })).save();
  },

  async seedSingleExperience(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seedSingleExperienceWithFalseSegment(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount > '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seedSingleControlgroup(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seedSingleControlgroupWithFalseSegment(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount > '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seedSingleExperienceAndNormalContent(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      },{
        "uid" : 2,
        "mappedTo" : "#A002",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass2<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seedSingleControlgroupAndNormalContent(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      },{
        "uid" : 2,
        "mappedTo" : "#A002",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass2<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },
}