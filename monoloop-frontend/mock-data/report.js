var report_model = require('./../app/model/ReportingArchiveAggregated');
const mongoose = require('mongoose');

module.exports = {
  async seed(){
    await (new report_model({
      "_id" : new mongoose.Types.ObjectId("5c3b4560242336802c8b45e6"),
      "datestamp" : 1547362800,
      "cid" : 1001,
      "totalVisits" : 208,
      "totalPages" : 504,
      "experiments": {
        "65": {
          "direct" : 72,
          "visitors" : 362,
          "cg_direct" : 40,
          "cg_visitors" : 120
        }
      }
    })).save();

    await (new report_model({
      "_id" : new mongoose.Types.ObjectId("5c3b4560242336802c8b45e7"),
      "datestamp" : 1547366400,
      "cid" : 1001,
      "totalVisits" : 216,
      "totalPages" : 570,
      "experiments": {
        "65": {
          "direct" : 198,
          "visitors" : 311,
          "cg_direct" : 45,
          "cg_visitors" : 150
        }
      }
    })).save();
  }
}