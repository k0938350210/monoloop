var experiment_model = require('./../app/model/experiments');
const mongoose = require('mongoose');

module.exports = {
  async seed(){
    await (new experiment_model({
      "_id" : new mongoose.Types.ObjectId("5bdffa752423366575b0dc5b"),
      "name" : "Exp 001",
      "updated_at" : new Date('2018-11-05')
    })).save();
  }
}