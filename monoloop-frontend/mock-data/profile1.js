var ProfileModel = require('./../app/model/profile');
const mongoose = require('mongoose');

module.exports = {

  async seed(){
    await (new ProfileModel({
      "_id" : new mongoose.Types.ObjectId("5b48b6f89adb4f0cb2e2ed46"),
      "uniqueID" : "",
      "profiles" : [
        {
          "goals" : [],
          "current" : {
            "views" : [
              {
                "_id" : new mongoose.Types.ObjectId("5b503315f40a1b4f8562cdd1"),
                "timeStamp" : 1531982613.77,
                "extRef" : "",
                "url" : "http://local-demo1wp.com/"
              }
            ]
          },
          "visits" : [
            {
              "referers" : [
                ""
              ],
              "downloads" : [],
              "searches" : [],
              "referer" : "",
              "lastclick" : 1531982613.77,
              "firstclick" : 1531982613.77,
              "exitpage" : "http://local-demo1wp.com/",
              "entrypage" : "http://local-demo1wp.com/",
              "clicks" : 1,
              "_id" : new mongoose.Types.ObjectId("5b503315f40a1b4f8562cdd0"),
              "content" : []
            }
          ],
          "visit_server_date" : 1531982613.769,
          "device_info" : {
            "config_resolution" : "1920x1080",
            "config_cookie" : 1,
            "config_flash" : "0",
            "config_java" : "0",
            "config_gears" : "0",
            "config_silverlight" : "0",
            "config_director" : "0",
            "config_windowsmedia" : "0",
            "config_realplayer" : "0",
            "config_quicktime" : "0",
            "config_pdf" : "1",
            "config_os" : {
              "family" : "Linux",
              "major" : null,
              "minor" : null,
              "patch" : null,
              "patchMinor" : null
            },
            "config_browser_name" : "Chrome",
            "config_browser_version" : "67.0.3396"
          },
          "_id" : new mongoose.Types.ObjectId("5b48b6f89adb4f0cb2e2ed47")
        }
      ],
      "recently_used_device" : "5b48b6f89adb4f0cb2e2ed47",
      "version" : 2,
      "OCR" : false,
      "customerID" : 1001,
      "__v" : 0
    })).save();

    await (new ProfileModel({
      "_id" : new mongoose.Types.ObjectId("5ba9eabd68eaae4032efee52"),
      "uniqueID" : "",
      "profiles" : [
        {
          "goals" : [],
          "current" : {
            "views" : [
              {
                "_id" : new mongoose.Types.ObjectId("5ba9eabd68eaae4032efee55"),
                "timeStamp" : 1531982613.77,
                "extRef" : "",
                "url" : "http://local-demo1wp.com/"
              }
            ]
          },
          "visits" : [
            {
              "referers" : [
                ""
              ],
              "downloads" : [],
              "searches" : [],
              "referer" : "",
              "lastclick" : 1531982613.77,
              "firstclick" : 1531982613.77,
              "exitpage" : "http://local-demo1wp.com/",
              "entrypage" : "http://local-demo1wp.com/",
              "clicks" : 1,
              "_id" : new mongoose.Types.ObjectId("5ba9eabd68eaae4032efee54"),
              "content" : []
            }
          ],
          "visit_server_date" : 1531982613.769,
          "device_info" : {
            "config_resolution" : "1920x1080",
            "config_cookie" : 1,
            "config_flash" : "0",
            "config_java" : "0",
            "config_gears" : "0",
            "config_silverlight" : "0",
            "config_director" : "0",
            "config_windowsmedia" : "0",
            "config_realplayer" : "0",
            "config_quicktime" : "0",
            "config_pdf" : "1",
            "config_os" : {
              "family" : "Linux",
              "major" : null,
              "minor" : null,
              "patch" : null,
              "patchMinor" : null
            },
            "config_browser_name" : "Chrome",
            "config_browser_version" : "67.0.3396"
          },
          "_id" : new mongoose.Types.ObjectId("5ba9eabd68eaae4032efee53")
        }
      ],
      "recently_used_device" : "5ba9eabd68eaae4032efee53",
      "version" : 2,
      "OCR" : false,
      "customerID" : 1001,
      "__v" : 0
    })).save();
  },


  async seedWithUniqueID(){
    await (new ProfileModel({
      "_id" : new mongoose.Types.ObjectId("5b48b6f89adb4f0cb2e2ed46"),
      "uniqueID" : "nutjaa@msn.com",
      "profiles" : [
        {
          "goals" : [],
          "current" : {
            "views" : [
              {
                "_id" : new mongoose.Types.ObjectId("5b503315f40a1b4f8562cdd1"),
                "timeStamp" : 1531982613.77,
                "extRef" : "",
                "url" : "http://local-demo1wp.com/"
              }
            ]
          },
          "visits" : [
            {
              "referers" : [
                ""
              ],
              "downloads" : [],
              "searches" : [],
              "referer" : "",
              "lastclick" : 1531982613.77,
              "firstclick" : 1531982613.77,
              "exitpage" : "http://local-demo1wp.com/",
              "entrypage" : "http://local-demo1wp.com/",
              "clicks" : 1,
              "_id" : new mongoose.Types.ObjectId("5b503315f40a1b4f8562cdd0"),
              "content" : []
            }
          ],
          "visit_server_date" : 1531982613.769,
          "device_info" : {
            "config_resolution" : "1920x1080",
            "config_cookie" : 1,
            "config_flash" : "0",
            "config_java" : "0",
            "config_gears" : "0",
            "config_silverlight" : "0",
            "config_director" : "0",
            "config_windowsmedia" : "0",
            "config_realplayer" : "0",
            "config_quicktime" : "0",
            "config_pdf" : "1",
            "config_os" : {
              "family" : "Linux",
              "major" : null,
              "minor" : null,
              "patch" : null,
              "patchMinor" : null
            },
            "config_browser_name" : "Chrome",
            "config_browser_version" : "67.0.3396"
          },
          "_id" : new mongoose.Types.ObjectId("5b48b6f89adb4f0cb2e2ed47")
        }
      ],
      "recently_used_device" : "5b48b6f89adb4f0cb2e2ed47",
      "version" : 2,
      "OCR" : false,
      "customerID" : 1001,
      "__v" : 0
    })).save();
  },

  async seedWithExternalKeys(){
    await (new ProfileModel({
      "_id" : new mongoose.Types.ObjectId("5b48b6f89adb4f0cb2e2ed46"),
      "uniqueID" : "",
      "externalKeys" : [
        {
          "k" : "email",
          "v" : "nutjaa@msn.com"
        }
      ],
      "profiles" : [
        {
          "goals" : [],
          "current" : {
            "views" : [
              {
                "_id" : new mongoose.Types.ObjectId("5b503315f40a1b4f8562cdd1"),
                "timeStamp" : 1531982613.77,
                "extRef" : "",
                "url" : "http://local-demo1wp.com/"
              }
            ]
          },
          "visits" : [
            {
              "referers" : [
                ""
              ],
              "downloads" : [],
              "searches" : [],
              "referer" : "",
              "lastclick" : 1531982613.77,
              "firstclick" : 1531982613.77,
              "exitpage" : "http://local-demo1wp.com/",
              "entrypage" : "http://local-demo1wp.com/",
              "clicks" : 1,
              "_id" : new mongoose.Types.ObjectId("5b503315f40a1b4f8562cdd0"),
              "content" : []
            }
          ],
          "visit_server_date" : 1531982613.769,
          "device_info" : {
            "config_resolution" : "1920x1080",
            "config_cookie" : 1,
            "config_flash" : "0",
            "config_java" : "0",
            "config_gears" : "0",
            "config_silverlight" : "0",
            "config_director" : "0",
            "config_windowsmedia" : "0",
            "config_realplayer" : "0",
            "config_quicktime" : "0",
            "config_pdf" : "1",
            "config_os" : {
              "family" : "Linux",
              "major" : null,
              "minor" : null,
              "patch" : null,
              "patchMinor" : null
            },
            "config_browser_name" : "Chrome",
            "config_browser_version" : "67.0.3396"
          },
          "_id" : new mongoose.Types.ObjectId("5b48b6f89adb4f0cb2e2ed47")
        }
      ],
      "recently_used_device" : "5b48b6f89adb4f0cb2e2ed47",
      "version" : 2,
      "OCR" : false,
      "customerID" : 1001,
      "__v" : 0
    })).save();
  }
};