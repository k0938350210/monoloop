var element_model = require('./../app/model/element');
var report_model = require('./../app/model/ReportingArchiveAggregated');
const mongoose = require('mongoose');

module.exports = {
  async seed(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "pValue": 0.5,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seed2(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "pValue": 0.5,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seed3(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "pValue": 0.01,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seed4(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 0,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "pValue": 0.01,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seed5(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 2,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "pValue": 0.006,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seed6(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 2,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date,
        "created_at": new Date,
        "pValue": 0.01,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();
  },

  async seed7(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 1,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date('2017-01-01'),
        "created_at": new Date,
        "pValue": 0.994,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();

    await (new report_model({
      "_id" : new mongoose.Types.ObjectId("5c3b4560242336802c8b45e6"),
      "datestamp" : 1547362800,
      "cid" : 1001,
      "totalVisits" : 208,
      "totalPages" : 504,
      "experiments": {
        "65": {
          "direct" : 125,
          "visitors" : 1000,
          "cg_direct" : 90,
          "cg_visitors" : 1000
        }
      }
    })).save();
  },

  async seed8(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 1,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date('2017-01-01'),
        "created_at": new Date,
        "pValue": 0.994,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();

    await (new report_model({
      "_id" : new mongoose.Types.ObjectId("5c3b4560242336802c8b45e6"),
      "datestamp" : 1547362800,
      "cid" : 1001,
      "totalVisits" : 208,
      "totalPages" : 504,
      "experiments": {
        "65": {
          "direct" : 125,
          "visitors" : 1000,
          "cg_direct" : 90,
          "cg_visitors" : 1000
        }
      }
    })).save();
  },

  async seed9(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 0.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 1,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date('2017-01-01'),
        "created_at": new Date,
        "pValue": 0.006,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();

    await (new report_model({
      "_id" : new mongoose.Types.ObjectId("5c3b4560242336802c8b45e6"),
      "datestamp" : 1547362800,
      "cid" : 1001,
      "totalVisits" : 208,
      "totalPages" : 504,
      "experiments": {
        "65": {
          "direct" : 90,
          "visitors" : 1000,
          "cg_direct" : 125,
          "cg_visitors" : 1000
        }
      }
    })).save();
  },


  async seed10(){
    // 100% pass audience / difference control group
    await (new element_model({
      "_id" : new mongoose.Types.ObjectId("5c131eb92423364d293c9871"),
      "urlhash" : "c4bc37a2ee216eeef2fe89d6",
      "fullURL" : "http://mon-experiences-1001/",
      "cid" : 1001,
      "hidden" : 0,
      "deleted" : 0,
      "regExp" : null,
      "exactQS" : "",
      "urlOption" : 0,
      "inWWW" : true,
      "exWWW" : true,
      "inHTTP_HTTPS" : true,
      "Experiment" : [{
        "controlGroupSize" : 100.0,
        "controlGroupDays" : 30.0,
        "experimentID" : 65,
        "significantAction" : 1,
        "hidden" : 0,
        "priority" : 1,
        "goalID" : 216,
        "segmentID" : 22,
        "updated_at": new Date('2017-01-01'),
        "created_at": new Date,
        "pValue": 0.006,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b4588")
      }],
      "content" : [ {
        "uid" : 1,
        "mappedTo" : "#A001",
        "placementType" : "4",
        "order" : 1,
        "displayType" : "1",
        "addJS" : "",
        "startDate" : null,
        "endDate" : null,
        "content_type" : "",
        "experimentID" : 65,
        "code" : "<% if ( ( MonoloopProfile.VisitCount < '5' ) ){ %>pass1<% } %>",
        "connectors" : [
            "MonoloopProfile"
        ],
        "segments" : null,
        "_id" : new mongoose.Types.ObjectId("5c132a302423368f128b458f")
      }]
    })).save();

    await (new report_model({
      "_id" : new mongoose.Types.ObjectId("5c3b4560242336802c8b45e6"),
      "datestamp" : 1547362800,
      "cid" : 1001,
      "totalVisits" : 208,
      "totalPages" : 504,
      "experiments": {
        "65": {
          "direct" : 90,
          "visitors" : 1000,
          "cg_direct" : 125,
          "cg_visitors" : 1000
        }
      }
    })).save();
  },
}