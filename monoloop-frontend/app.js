(function() {
  var app, bodyParser, cluster, cookieParser, error, errorHandler, express, i, methodOverride, numCPUs, os, routes, server, toobusy, debug;

  express = require('express');

  routes = require('./routes');

  cluster = require('cluster');

  os = require('os');

  numCPUs = os.cpus().length;

  require("http").globalAgent.maxSockets = 2e308;

  bodyParser = require('body-parser');

  methodOverride = require('method-override');

  cookieParser = require('cookie-parser');

  errorHandler = require('errorhandler');

  toobusy = require('toobusy-js');

  toobusy.maxLag(90);

  debug = require('debug')('ml:core');

  const config = require('./config').get('fe');

  const logger = require('./app/utils/logger');

  if (cluster.isMaster && process.env.NODE_ENV !== 'test') {
    i = 0;
    while (i < numCPUs) {
      cluster.fork();
      ++i;
    }
  } else {
    // Create express server
    app = module.exports = express();
    // Server config
    app.use(bodyParser.json());
    app.use(methodOverride());
    app.use(cookieParser());
    app.use(express.static('public'));
    app.use(function(req, res, next) {
      if (toobusy()) {
        logger.printError("toobusy","");
        return res.status(503).send("I'm busy right now, sorry.");
      } else {
        return next();
      }
    });
    if ('development' === app.get('env')) {
      app.use(errorHandler());
    }
    if ('production' === app.get('env')) {
      app.use(errorHandler());
    }
    try {
      // Initialize routes
      routes(app);
    } catch (error1) {
      error = error1;
      console.error(error);
      return;
    }

    let port = (config && config.port)? config.port : 9000;
    // Start server
    server = app.listen(port, function() {
      var host, port;
      host = server.address().address;
      port = server.address().port;
      return debug('Express server listening at http://%s:%s', host, port);
    });
  }

}).call(this);
