process.title = 'archive';




//register invalidate server
const redisSubscriber = require('../lib/redis-subscriber');
redisSubscriber.subscribeInvalidate();
//process archive
require('./archive');
//process sqs send
require('./webhook-send');

