(function() {
  'use strict';

  module.exports = {
    calculateGoals: function(baseVar) {
      var _gprofile, date, goal_ts, i, id, len, obj, profile, ref, resultData;
      profile = baseVar.getActiveSubProfile();
      _gprofile = baseVar.getGlobalProfile();
      resultData = baseVar.getResultSet();
      if (profile.goals && profile.goals.length > 0) {
        ref = profile.goals;
        for (id = i = 0, len = ref.length; i < len; id = ++i) {
          obj = ref[id];
          baseVar.expGoals.push(obj.uid);
          if (baseVar.$incrementVisit) {
            profile.goals[id].aggregated = false;
          }
          date = new Date(obj.tstamp * 1000);
          //goal_ts = new Date(date.getFullYear(),date.getDate(),date.getDay(),0,0,0,0)
          goal_ts = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours()).getTime() / 1000;
          if (goal_ts <= baseVar.ts) {
            if (!obj.aggregated || obj.aggregated !== true) {
              baseVar.goalCounter = 1;
              profile.goals[id].aggregated = true;
            } else {
              baseVar.goalCounter = 0;
            }
            if (!isNaN(parseInt(id)) && obj !== null && obj.uid !== void 0) {
              if (isNaN(resultData['$inc']['goals.' + obj.uid + '.amount'])) {
                resultData['$inc']['goals.' + obj.uid + '.amount'] = baseVar.goalCounter;
              }
              if (baseVar.goalCounter === 1) {
                // initiate webhook trigger request
                if (isNaN(resultData['$inc']['goals.' + obj.uid + '.VisitsBeforeGoal'])) {
                  resultData['$inc']['goals.' + obj.uid + '.VisitsBeforeGoal'] = profile.visits.length;
                }
                if (isNaN(resultData['$inc']['goals.' + obj.uid + '.pagesBeforeGoal'])) {
                  resultData['$inc']['goals.' + obj.uid + '.pagesBeforeGoal'] = _gprofile.PageViewCountGoals;
                }
              }
            }
          }
        }
        return baseVar.updateResultSet(resultData);
      }
    }
  };

}).call(this);


//# sourceMappingURL=goals.js.map
//# sourceURL=coffeescript