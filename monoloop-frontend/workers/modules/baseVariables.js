(function() {
  'use strict';
  var initializeArchive, method;

  method = {};

  method.initializeArchive = function(_gprofile, tracker) {
    var dts;
    this._gprofile = _gprofile;
    this._mprofile = _gprofile.getRecentDevicePorfile();
    this.resultData = [];
    this.cid = _gprofile.customerID;
    dts = new Date;
    this.ts = new Date(dts.getFullYear(), dts.getMonth(), dts.getDate(), dts.getHours(), 0, 0).getTime() / 1000;
    this.resultData[this.cid] = {};
    this.resultData[this.cid][this.ts] = {
      '$inc': {
        'totalLastViewedCategory': 0,
        'totalDuration': 0,
        'totalUniqueVisitorProfiles': 0,
        'recencyTime': 0,
        'recencyVisits': 0,
        'recencyDuration': 0,
        'recencyUniqueCategory': 0,
        'recencyCategory': 0,
        'recencyUniqueCategoryCurrent': 0,
        'recencyCategoryCurrent': 0,
        'recencyUniqueProduct': 0,
        'recencyProduct': 0,
        'recencyUniqueProductCurrent': 0,
        'recencyProductCurrent': 0,
        'totalCategoryViews': 0,
        'totalUniqueCategories': 0,
        'totalProductViews': 0,
        'totalUniqueProducts': 0,
        'totalDaysSinceLastVisit': 0,
        'totalPageReferer': 0,
        'totalVisitors': 0,
        'totalCategoriesVisit': 0,
        'totalPointsCurrentVisit': 0,
        'recencyPages': 0,
        'totalVisits': 0,
        'totalPageViews': 0,
        'totalPages': 0
      },
      '$set': {}
    };
    this.totalVisitsProfile = 0;
    this.lastVisitProfile = this._gprofile.lastVisit;
    this.totalDurationProfile = 0;
    this.totalDaysSinceLastVisitProfile = 0;
    this.totalPagesProfile = 0;
    this.totalPageViews = 0;
    this.c_LastViewedCategory = 0;
    this.c_UniqueCategoryRecencyVisit = 0;
    this.c_UniqueProductsCategoryVisit = 0;
    this.c_ProductVisit = 0;
    this.c_UniqueProducts = 0;
    this.c_ProductRecencyVisit = 0;
    this.c_UniqueProductRecencyVisit = 0;
    this.c_UniqueCategories = 0;
    this.c_CategoryRecencyVisit = 0;
    this.lastVisit = this._gprofile.lastVisit;
    this.prevVisit = this._gprofile.previousVisit;
    this.profileLastVisit = _gprofile.lastVisit._id.toString();
    //goals
    this.excludeGoals = 0;
    this.expGoals = [];
    this.goalCounter = 0;
    this.convertedGoals = this._mprofile.goals ? this._mprofile.goals.length : 0;
    //Increment Variables
    this.$incrementVisit = 0;
    this.$incrementPageViews = 1;
    this.$inctotalCategoriesVisit = 0;
    this.$inctotalLastViewedCategory = 0;
    this.$incExperiments = 0;
    this.$incSegment = 0;
    this.$incrementPageReferer = 0;
    this.bounced = 0;
    this.$incTotalUniqueVisitors = 0;
    this.$incTotalDuration = 0;
    this.$incRecencyVisit = 0;
    this.$incRececnyPages = 1;
    this.onlyTracker = tracker;
    this.IncrementDecider();
    this.botInfo = null;
    this.revisedSeg = {};
    this.Segments_InOut = {};
    this.aggregate_inOut = {};
    // For category/product tracker
    this.$setAggregatedStatus = {};

    return this;
  };
  //@$scategories = if @_gprofile.cachedProperties && @_gprofile.cachedProperties.scategories then @_gprofile.cachedProperties.scategories else []
  //@$sproducts = if @_gprofile.cachedProperties && @_gprofile.cachedProperties.sproducts then @_gprofile.cachedProperties.sproducts else []


  method.getResultSet = function() {
    return this.resultData[this.cid][this.ts];
  };

  method.updateResultSet = function(data) {
    return this.resultData[this.cid][this.ts] = data;
  };

  method.getGlobalProfile = function() {
    return this._gprofile;
  };

  method.getActiveSubProfile = function() {
    return this._mprofile;
  };

  method.IncrementDecider = function() {
    if (this._mprofile.lastVisitKey) { //exists?
      /*
      if @onlyTracker
        if !@_mprofile.lastVisitKeyTracker or @_mprofile.lastVisitKeyTracker isnt @_gprofile.lastVisit._id.toString()
          @$incRecencyVisit = 1
          @$incRececnyPages = @lastVisit.clicks
          if @_gprofile.lastVisit.categories isnt undefined and typeof @_gprofile.lastVisit.categories is "object"
            @$inctotalCategoriesVisit = Object.keys(@_gprofile.lastVisit.categories).length
          @$inctotalLastViewedCategory = @_gprofile.totalLastViewedCategory()
      else */
      if (this._mprofile.lastVisitKey !== this._gprofile.lastVisit._id.toString()) {
        this.$incrementVisit = 1;
        this.$incrementPageViews = this.lastVisit.clicks;
        if (typeof this._gprofile.lastVisit.referers !== void 0 && this._gprofile.lastVisit.referers[this._gprofile.lastVisit.referers.length - 1] !== '' && this._gprofile.lastVisit.referers[this._gprofile.lastVisit.referers.length - 1] !== null) {
          this.$incrementPageReferer = 1;
        }
        this.$incTotalUniqueVisitors = this._gprofile.TotalUniqueVisitors();
        return this.totalDaysSinceLastVisitProfile = this._gprofile.DaysSinceLastVisit;
      }
    } else {
      if (typeof this._gprofile.lastVisit.referers !== void 0 && this._gprofile.lastVisit.referers[this._gprofile.lastVisit.referers.length - 1] !== '' && this._gprofile.lastVisit.referers[this._gprofile.lastVisit.referers.length - 1] !== null) {
        this.$incrementPageReferer = 1;
      }
      this.$incrementVisit = 1;
      this.$inctotalCategoriesVisit = 1;
      this.$inctotalCategoriesVisit = this._gprofile.totalCategoriesVisit();
      this.$inctotalLastViewedCategory = this._gprofile.totalLastViewedCategory();
      this.$incTotalUniqueVisitors = this._gprofile.TotalUniqueVisitors();
      return this.totalDaysSinceLastVisitProfile = this._gprofile.DaysSinceLastVisit;
    }
  };

  method.asyncResultFetch = function(results) {
    if(results[3] && results[3].bot){
      this.botInfo = results[3].bot;
    }

    if(results[2]){
      if(results[2].revisedSeg){
        this.revisedSeg = results[2].revisedSeg;
      }

      if(results[2].Segments_InOut){
        this.Segments_InOut = results[2].Segments_InOut;
      }

      if(results[2].aggregate_inOut){
        this.aggregate_inOut = results[2].aggregate_inOut;
      }
    }
  };
  module.exports = method;
}).call(this);


//# sourceMappingURL=baseVariables.js.map
//# sourceURL=coffeescript