// Generated by CoffeeScript 2.2.4
(function() {
  'use strict';
  var baseController, botMod, expSignalMod, goalsMod, logger, method, profileMod, segmentMod, syncMod, trackerMod, varMod;

  profileMod = require('./profile');

  segmentMod = require('./segment');

  expSignalMod = require('./signals/experiment');

  varMod = require('./baseVariables');

  syncMod = require('./syncProfile');

  botMod = require('./bot');

  trackerMod = require('./trackers');

  goalsMod = require('./goals');

  method = {};

  method.initiateArchive = function(id, cb) {
    return profileMod.getProfileDB(id, cb);
  };

  method.profileGenericParamsInc = function(baseVar) {
    var e;
    try {
      return profileMod.genericParametersArchive(baseVar);
    } catch (error) {
      e = error;
      return console.error("Profile Generic Params " + e);
    }
  };

  method.profileArchiveUpdator = function(baseVar, cb) {
    return profileMod.profileArchiveUpdator(baseVar, cb);
  };

  method.initiateSyncProfile = function(baseVar, cb) {
    return syncMod.syncProfle(baseVar, cb);
  };

  method.processSegments = function(baseVar, cb) {
    segmentMod.initializeSegments(baseVar);
    return segmentMod.processSegments(cb);
  };

  method.calculateSegments = function(baseVar) {
    return segmentMod.SegmentsArchiving(baseVar);
  };

  method.experimentSignal = function(baseVar, signals, cb) {
    return expSignalMod.prepareSignal(baseVar, signals, cb);
  };

  method.calculateGoals = function(baseVar) {
    var e;
    try {
      return goalsMod.calculateGoals(baseVar);
    } catch (error) {
      e = error;
      return console.error("Error in calculate Goals: " + e);
    }
  };

  method.calculateExperiments = function(baseVar) {
    var e;
    try {
      return expSignalMod.calculateExperiments(baseVar);
    } catch (error) {
      e = error;
      return console.error("Error in calculate Experiments: " + e);
    }
  };

  method.detechbot = function(baseVar, cb) {
    return botMod.detectBot(baseVar, cb);
  };

  method.trackersArchive = function(baseVar) {
    var trackers;
    trackers = new trackerMod(baseVar);
    return trackers.calculationForTrackers();
  };

  module.exports = method;

}).call(this);

//# sourceMappingURL=baseController.js.map
