(function() {
  'use strict';
  var Avg, async, initializeProfile, profileModel, publisher;

  profileModel = require('./../../app/model/profile');

  Avg = require('./../../app/model/ReportingArchiveAggregated');

  async = require('async');

  const debug = require('debug')('ml:archive');

  publisher = require('./../../redis-conn');

  initializeProfile = function() {};

  var profileModule = {
    baseVar: null,

    getProfileDB: function(id, cb) {
      return profileModel.findById(id).exec(cb);
    },
    genericParametersArchive: function(baseVar) {
      var $incRecencyDuration, currentview, resultData, tempDuration;
      resultData = baseVar.getResultSet();
      resultData['$inc']['totalVisits'] = baseVar.$incrementVisit;
      resultData['$inc']['totalPageViews'] = baseVar.$incrementPageViews;
      resultData['$inc']['totalPageReferer'] = baseVar.$incrementPageReferer;
      resultData['$inc']['totalPages'] = baseVar._gprofile.uniquePages;
      resultData['$inc']['totalPointsCurrentVisit'] = baseVar._gprofile.totalPointsCurrentVisit();
      resultData['$inc']['totalDaysSinceLastVisit'] = baseVar.totalDaysSinceLastVisitProfile; //(new Date).getDate() - new Date(baseVar.lastVisit.lastclick * 1000).getDate()
      resultData['$inc']['totalVisitors'] = baseVar._gprofile.TotalUniqueVisitors();
      if (baseVar._mprofile.current.views.length > 1) {
        currentview = baseVar._mprofile.current.views[baseVar._mprofile.current.views.length - 1].timeStamp - baseVar._mprofile.current.views[baseVar._mprofile.current.views.length - 2].timeStamp;
        resultData['$inc']['totalDuration'] = currentview;
        baseVar.$incTotalDuration = currentview;
        $incRecencyDuration = currentview;
      } else {
        tempDuration = (new Date(baseVar.lastVisit.lastclick * 1000).getTime() - new Date(baseVar.lastVisit.firstclick * 1000).getTime()) / 1000;
        resultData['$inc']['totalDuration'] = tempDuration;
        baseVar.$incTotalDuration = tempDuration;
        $incRecencyDuration = tempDuration;
      }
      if (baseVar.prevVisit) {
        resultData['$inc']['recencyVisits'] = baseVar.$incRecencyVisit;
        resultData['$inc']['recencyTime'] = baseVar.lastVisit.firstclick - baseVar.prevVisit.lastclick;
        resultData['$inc']['recencyPages'] = baseVar.$incRececnyPages;
        resultData['$inc']['recencyDuration'] = $incRecencyDuration; //baseVar.lastVisit.lastclick - (baseVar.lastVisit.firstclick)
      }
      return baseVar.updateResultSet(resultData);
    },

    webhook: function(callback){
      let baseVar = profileModule.baseVar;
      let ref1 = baseVar.resultData;
      let batch_cmd = null;
      let cid = null;
      let _gprofile = baseVar.getGlobalProfile();
      let profile = baseVar.getActiveSubProfile();
      let k,v,k2,should_process,cmd;



      for (k in ref1) {
        cid = parseInt(k);
        v = ref1[k];
        for (k2 in v) {
          if(v[k2]['$inc'] !== undefined){
            batch_cmd = v[k2]['$inc'];
            break;
          }
        }
      }

      if(batch_cmd === null){
        return callback(null, null);
      }

      should_process = false;
      for(cmd in batch_cmd){
        if(cmd.startsWith('segments.') !== -1 || cmd.startsWith('goals.') !== -1 || cmd.startsWith('experiments.') !== -1){
          should_process = true;
          break;
        }
      }

      if(!should_process){
        return callback(null, null);
      }
      //return callback(null, null);
      //webhook_trigger.performTriggerFromCmd(cid,_gprofile._id, profile._id, batch_cmd, callback);
      var transport = {
        cid: cid,
        gmid: _gprofile._id,
        mid: profile._id,
        batch_cmd: batch_cmd
      };

      publisher.rpush('webhooklist', JSON.stringify(transport),function(err,reply){
        callback(null, null);
      });
    },

    updateProfile: function(callback) {
      var _gprofile, command, k, lastVisitIndex, profile, query, ref, ref1, v;
      let baseVar = profileModule.baseVar;
      _gprofile = baseVar.getGlobalProfile();
      profile = baseVar.getActiveSubProfile();
      query = {
        _id: _gprofile._id,
        'profiles._id': profile._id,
        'profiles.visits._id': baseVar.lastVisit._id
      };
      if (baseVar.lastVisitProfile !== null) {
        //if (new Date).getDate() - new Date(baseVar.lastVisitProfile.lastclick * 1000).getDate() > 0
        //baseVar.totalDaysSinceLastVisitProfile = (new Date).getDate() - new Date(baseVar.lastVisitProfile.lastclick * 1000).getDate()
        baseVar.totalPagesProfile = baseVar.lastVisitProfile.clicks;
      }
      // if onlyTracker is true then only update those properties which are related to tracker(postBack APi)
      if ((baseVar.onlyTracker != null) && baseVar.onlyTracker === true) {
        //command = $set:
        //'cachedProperties.totalCategoriesVisit': _gprofile.totalCategoriesVisit()
        //'cachedProperties.totalUniqueCategories': baseVar.c_UniqueCategories
        //'cachedProperties.totalProductVisit': baseVar.c_ProductVisit
        //'cachedProperties.totalUniqueProducts': baseVar.c_UniqueProducts
        // 'cachedProperties.totalUniqueCategoryRecencyVisit': baseVar.c_UniqueCategoryRecencyVisit
        // 'cachedProperties.totalUniqueProductRecencyVisit': baseVar.c_UniqueProductRecencyVisit
        // 'cachedProperties.totalUniqueProductsCategoryVisit': baseVar.c_UniqueProductsCategoryVisit
        // 'cachedProperties.totalProductRecencyVisit': baseVar.c_ProductRecencyVisit
        // 'cachedProperties.totalCategoryRecencyVisit': baseVar.c_CategoryRecencyVisit
        // 'cachedProperties.totalLastViewedCategory': _gprofile.totalLastViewedCategory()
        command = {
          $set: {}
        };
        command.$set['profiles.' + _gprofile.getIndex() + '.lastVisitKeyTracker'] = baseVar.profileLastVisit;
        //command.$inc = {}
        // command.$inc['cachedProperties.totalCategoriesVisit'] = baseVar.$inctotalCategoriesVisit
        // command.$inc['cachedProperties.totalUniqueCategories'] = baseVar.c_UniqueCategories
        // command.$inc['cachedProperties.totalProductVisit'] = baseVar.c_ProductVisit
        // command.$inc['cachedProperties.totalUniqueProducts'] = baseVar.c_UniqueProducts
        // set aggregated true
        command.$set['profiles.' + _gprofile.getIndex() + '.visits.' + _gprofile.getLastVisitIndex() + '.categories'] = baseVar.$setAggregatedStatus;
      } else {
        // command.$set['cachedProperties.scategories'] = baseVar.$scategories
        // command.$set['cachedProperties.sproducts'] = baseVar.$sproducts
        command = {
          $set: {
            'profiles.$.bot': baseVar.botInfo,
            'profiles.$.aggregator_id': null,
            'profiles.$.aggregated': true,
            'profiles.$.goals': profile.goals,
            'profiles.$.current': profile.current
          }
        };
        //'cachedProperties.totalPageViews': _gprofile.TotalPageViews()
        //'cachedProperties.totalVisits': _gprofile.VisitCount
        //'cachedProperties.totalDuration': _gprofile.TotalDurationProfile()
        //'cachedProperties.totalUniqueVisitorProfiles': _gprofile.TotalVisitors()
        //'cachedProperties.totalPageReferer': _gprofile.totalPageReferer()
        // 'cachedProperties.totalDaysSinceLastVisit': baseVar.totalDaysSinceLastVisitProfile
        // 'cachedProperties.totalPointsCurrentVisit': _gprofile.totalPointsCurrentVisit()
        lastVisitIndex = _gprofile.getLastVisitIndex();
        command.$set['profiles.' + _gprofile.getIndex() + '.lastVisitKey'] = baseVar.profileLastVisit;
        command.$set['profiles.' + _gprofile.getIndex() + '.visits.' + lastVisitIndex + '.experiments'] = baseVar.lastVisit.experiments;
        command.$set['profiles.' + _gprofile.getIndex() + '.visits.' + lastVisitIndex + '.aggregated'] = true;
        command.$set['profiles.' + _gprofile.getIndex() + '.visits.' + lastVisitIndex + '.aggregatedSegments'] = baseVar.lastVisit.aggregatedSegments;
        ref = baseVar.revisedSeg;
        //increment cachedProperties
        //command.$inc = {}
        // command.$inc['cachedProperties.totalPageViews'] = baseVar.$incrementPageViews
        // command.$inc['cachedProperties.totalVisits'] = baseVar.$incrementVisit
        // command.$inc['cachedProperties.totalPageReferer'] = baseVar.$incrementPageReferer
        // command.$inc['cachedProperties.totalDuration'] = baseVar.$incTotalDuration
        //command.$inc['cachedProperties.totalCategoriesVisit'] = baseVar.$inctotalCategoriesVisit
        // command.$inc['cachedProperties.totalUniqueVisitorProfiles'] = baseVar.$incTotalUniqueVisitors
        for (k in ref) {
          v = ref[k];
          command.$set[k] = v;
        }
        ref1 = baseVar.Segments_InOut;
        for (k in ref1) {
          v = ref1[k];
          command.$set[k] = v;
        }
      }
      return profileModel.updateOne(query, command, {
        upsert: true
      },function(err, res){
        callback(err, res);
      });
    },

    updateReport: function(callback) {
      var cid, command, k, k2, query, ref, ref1, resultData, results, tstamp, v, v2;
      let baseVar = profileModule.baseVar;
      resultData = baseVar.getResultSet();
      ref = resultData['$inc'];
      for (k in ref) {
        v = ref[k];
        if (isNaN(v)) {
          delete resultData['$inc'][k];
        }
      }
      let obj = baseVar.resultData[baseVar.cid];
      for(var i in obj){ tstamp = i; v2 =  obj[i]; break; }

      tstamp = parseInt(tstamp);

      query = {
        datestamp: tstamp,
        cid: baseVar.cid
      };
      command = v2;
      command['$set']['datestamp'] = tstamp;
      command['$set']['cid'] = baseVar.cid;
      Avg.updateOne(query, command, {
        upsert: true
      },function(err, res){
        callback(err, res);
      });
    },

    profileArchiveUpdator: function(baseVar, cb) {
      this.baseVar = baseVar;
      async.parallel({
        webhook: this.webhook,
        updateProfile: this.updateProfile,
        updateReport: this.updateReport
      }, cb);
    }
  };

  module.exports =  profileModule;

}).call(this);


//# sourceMappingURL=profile.js.map
//# sourceURL=coffeescript