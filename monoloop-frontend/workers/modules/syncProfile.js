// Generated by CoffeeScript 2.0.2
(function() {
  var consumer;

  consumer = require('./../../lib/doubleLinkedProfiles');

  module.exports = {
    syncProfle: function(baseVar, cb) {
      var _gprofile, cid, list, profile;
      profile = baseVar.getActiveSubProfile();
      _gprofile = baseVar.getGlobalProfile();
      if (!profile.lastVisitKey || (profile.lastVisitKey !== _gprofile.lastVisit._id.toString())) {
        cid = _gprofile.customerID;
        return consumer.add(cid, _gprofile, cb);
      } else {
        return cb();
      }
    }
  };

}).call(this);
