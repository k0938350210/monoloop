(function() {
  'use strict';
  var initializeTrackers, logger, method;

  logger = require('./../../app/utils/logger');

  initializeTrackers = function(baseVar) {
    this._gprofile = baseVar.getGlobalProfile();
    this._mprofile = baseVar.getActiveSubProfile();
    this.resultData = baseVar.getResultSet();
    this.baseVar = baseVar;
    this.$inctotalCategoriesVisit = 0;
    this.$inctotalLastViewedCategory = 0;
    this.$inctotalCategoriesViews = 0;
    this.$incUniqueCategoriesVisit = 0;
    this.$inctotalProductViews = 0;
    this.$incUniqueProductViews = 0;
    this.$incUniqueProductVisit = 0;
    this.$incDuration = 0;
  };

  method = initializeTrackers.prototype;

  method.calculationForTrackers = function() {
    var Oldscore, _gprofile, i, id, j, len, lists, obj, ref, ref1, score, that;
    _gprofile = this.baseVar.getGlobalProfile();
    that = this;
    ref = ['brand', 'clickdepth', 'duration', 'engagement', 'recency'];
    //that.resultData['$inc']['totalCategoriesVisit'] = that.baseVar.$inctotalCategoriesVisit
    //that.resultData['$inc']['totalLastViewedCategory'] = that.baseVar.$inctotalLastViewedCategory

    //Add score indexes
    for (j = 0, len = ref.length; j < len; j++) {
      i = ref[j];
      that.resultData['$inc']['scores.' + i + '.levels.1'] = 0;
      that.resultData['$inc']['scores.' + i + '.levels.2'] = 0;
      that.resultData['$inc']['scores.' + i + '.levels.3'] = 0;
      that.resultData['$inc']['scores.' + i + '.levels.4'] = 0;
      that.resultData['$inc']['scores.' + i + '.levels.5'] = 0;
      that.resultData['$inc']['scores.' + i + '.trends.dec'] = 0;
      that.resultData['$inc']['scores.' + i + '.trends.inc'] = 0;
      that.resultData['$inc']['scores.' + i + '.trends.quo'] = 0;
      that.resultData['$inc']['scores.' + i + '.totalScore'] = 0;
    }
    //Caclulate categories/products properties
    if ((that.baseVar.lastVisit != null) && (that.baseVar.lastVisit.categories != null)) {
      ref1 = that.baseVar.lastVisit.categories;
      for (id in ref1) {
        obj = ref1[id];
        // if aggregated is false its mean it is new/updated category or product
        if ((obj.aggregated != null) && obj.aggregated.aggregated === false) {
          //Mark aggregated to true
          obj.aggregated.aggregated = true;
          that.baseVar.$setAggregatedStatus[id.toString()] = obj;
          //it is category
          if (obj.t === 2) {
            that.$inctotalCategoriesViews += obj.c - obj.aggregated.previousCount;
            that.$inctotalLastViewedCategory += obj.c - obj.aggregated.previousCount;
            //It is new category
            if (obj.c === 1 && obj.d === void 0) {
              that.$incUniqueCategoriesVisit += 1;
              that.$inctotalCategoriesVisit += 1;
            }
          // it is product
          } else if (obj.t === 1) {
            that.$inctotalProductViews += obj.c - obj.aggregated.previousCount;
            //it is new product
            if (obj.c === 1 && obj.d === void 0) {
              that.$incUniqueProductVisit += 1;
            }
          }
          if (obj.d !== void 0 && obj.d !== null) {
            that.$incDuration = obj.d - obj.aggregated.previousDuration;
          }
          score = _gprofile.CategoryIndex(365, obj.g, id.toString());
          Oldscore = 0;
          if (that.baseVar.prevVisit) {
            Oldscore = _gprofile.CategoryIndex(365, obj.g, id.toString(), that.baseVar.prevVisit.lastclick);
          }
          if (!that.resultData['$inc']['categories.' + id + '.amount']) {
            that.resultData['$inc']['categories.' + id + '.amount'] = 0;
            that.resultData['$set']['categories.' + id + '.name'] = obj.n;
            that.resultData['$inc']['categories.' + id + '.inc'] = 0;
            that.resultData['$inc']['categories.' + id + '.dec'] = 0;
            that.resultData['$inc']['categories.' + id + '.quo'] = 0;
            that.resultData['$inc']['categories.' + id + '.duration'] = 0;
            that.resultData['$inc']['categories.' + id + '.clicks'] = 0;
            that.resultData['$set']['categories.' + id + '.type'] = obj.t;
          }
          that.resultData['$inc']['categories.' + id + '.amount']++;
          that.resultData['$inc']['categories.' + id + '.duration'] += that.$incDuration; //if obj.d then obj.d else 0
          that.resultData['$inc']['categories.' + id + '.clicks'] += obj.c - obj.aggregated.previousCount; //if obj.c then obj.c else 0
          if (Oldscore < score) {
            that.resultData['$inc']['categories.' + id + '.inc']++;
          }
          if (Oldscore > score) {
            that.resultData['$inc']['categories.' + id + '.dec']++;
          }
          if (Oldscore === score) {
            that.resultData['$inc']['categories.' + id + '.quo']++;
          }
        }
      }
      // Update data
      that.resultData['$inc']['totalCategoriesVisit'] = that.baseVar.$inctotalCategoriesVisit = that.$inctotalCategoriesVisit;
      that.resultData['$inc']['totalLastViewedCategory'] = that.baseVar.$inctotalLastViewedCategory = that.$inctotalLastViewedCategory;
      that.resultData['$inc']['totalCategoryViews'] = that.$inctotalCategoriesViews;
      that.resultData['$inc']['totalUniqueCategories'] = that.baseVar.c_UniqueCategories = that.$incUniqueCategoriesVisit;
      that.resultData['$inc']['totalProductViews'] = that.baseVar.c_ProductVisit = that.$inctotalProductViews;
      that.resultData['$inc']['totalUniqueProducts'] = that.baseVar.c_UniqueProducts = that.$incUniqueProductVisit;
    }
    // Set sorted categories and products in cached properties [by:Affan]
    if (_gprofile.OCR !== void 0 && _gprofile.OCR === true && _gprofile.uniqueID !== void 0 && _gprofile.uniqueID !== '') {
      lists = _gprofile.getSortedCategories();
    }
    //that.baseVar.$scategories = if lists.length > 0 then lists[0] else []
    //that.baseVar.$sproducts = if lists.length > 1 then lists[1] else []
    /*
    #Sort all catagories to be able to get recency on products/categories full history:
    visits = @_mprofile.visits
    try
      if visits and visits.length >= 1
        #remove latest visit
        latestCategories = {}
        latestCategoryTS = 0
        latestProducts = {}
        latestProductTS = 0
        #visits.pop()
        for visit in visits
          if visit.categories
            for k, v of visit.categories
              if v.t and parseInt(v.t) is 1
                if latestProducts[k]
                  if visit.lastclick > latestProducts[k]
                    latestProducts[k] = visit.lastclick
                else
                  latestProducts[k] = visit.lastclick
                latestProductTS = visit.lastclick

              if v.t and parseInt(v.t) is 2
                if latestCategories[k]
                  if visit.lastclick > latestCategories[k]
                    latestCategories[k] = visit.lastclick
                else
                  latestCategories[k] = visit.lastclick
                latestCategoryTS = visit.lastclick

    catch e
      logger.printError 'Sort all catagories to be able to get recency on products/categories full history ', e

    try
     * begin add:
     * recencyCategoryCurrent
     * recencyUniqueCategoryCurrent
     * recencyProductCurrent
     * recencyUniqueProductCurrent

      if latestCategories
        categoriesLen = Object.keys(latestCategories).length
        categoriesKeys = Object.keys(latestCategories)
     * recencyCategoryCurrent
        Object.keys(latestCategories).forEach (c)->
          that.resultData['$inc']['recencyCategoryCurrent'] += that.baseVar.lastVisit.lastclick - latestCategories[c]
          that.baseVar.c_LastViewedCategory += that.baseVar.lastVisit.lastclick - latestCategories[c]

        #recencyUniqueCategoryCurrent
        if categoriesKeys and  latestCategories[categoriesKeys[categoriesLen - 1]] and latestCategories[categoriesKeys[categoriesLen - 1]] > 0
          that.resultData['$inc']['recencyUniqueCategoryCurrent'] += (that.baseVar.lastVisit.lastclick - latestCategories[categoriesKeys[categoriesLen - 1]])
          that.baseVar.c_UniqueCategoryRecencyVisit += (that.baseVar.lastVisit.lastclick - latestCategories[categoriesKeys[categoriesLen - 1]])

      if latestProducts and Object.keys(latestProducts).length
        productsLen = Object.keys(latestProducts).length
        productsKeys = Object.keys(latestProducts)
     * recencyProductCurrent
        Object.keys(latestProducts).forEach (p)->
          that.resultData['$inc']['recencyProductCurrent'] += that.baseVar.lastVisit.lastclick - latestProducts[p]

          #recencyUniqueProductCurrent
          if productsLen and latestProducts[productsKeys[productsLen - 1]] and latestProducts[productsKeys[productsLen - 1]] > 0
            that.resultData['$inc']['recencyUniqueProductCurrent'] += (that.baseVar.lastVisit.lastclick - latestProducts[productsKeys[productsLen - 1]])
            that.baseVar.c_UniqueProductsCategoryVisit += (that.baseVar.lastVisit.lastclick - latestProducts[productsKeys[productsLen - 1]])
     * end add

          for id,obj of that.baseVar.lastVisit.categories
            #Add totals depending if its category or products
            if obj.t and parseInt(obj.t) is 1 #its a product
              that.resultData['$inc']['totalProductViews'] += obj.c
              that.baseVar.c_ProductVisit = that.resultData['$inc']['totalProductViews']
              that.resultData['$inc']['totalUniqueProducts']++
              that.baseVar.c_UniqueProducts = that.resultData['$inc']['totalUniqueProducts']

              #Recency product
              if latestProducts and latestProductTS > 0
                that.resultData['$inc']['recencyProduct'] += (that.baseVar.lastVisit.lastclick - latestProductTS)
                that.baseVar.c_ProductRecencyVisit += (that.baseVar.lastVisit.lastclick - latestProductTS)

              #Recency unique product
              if latestProducts and latestProducts[id] and latestProducts[id] > 0
                that.resultData['$inc']['recencyUniqueProduct'] += (that.baseVar.lastVisit.lastclick - latestProducts[id])
                that.baseVar.c_UniqueProductRecencyVisit += (that.baseVar.lastVisit.lastclick - latestProducts[id])

            if obj.t and parseInt(obj.t) is 2  #its a category
              that.resultData['$inc']['totalCategoryViews'] += obj.c
              that.resultData['$inc']['totalUniqueCategories']++
              that.baseVar.c_UniqueCategories = that.resultData['$inc']['totalUniqueCategories']
                #Recency product
                if latestCategories and latestCategoryTS > 0
                  that.resultData['$inc']['recencyCategory'] += (that.baseVr.lastVisit.lastclick - latestCategoryTS)
                  that.baseVar.c_CategoryRecencyVisit += (that.baseVar.lastVisit.lastclick - latestCategoryTS)

              #Recency product
              if latestCategories and latestCategoryTS > 0
                that.resultData['$inc']['recencyCategory'] += (that.baseVar.lastVisit.lastclick - latestCategoryTS)
                that.baseVar.c_CategoryRecencyVisit += (that.baseVar.lastVisit.lastclick - latestCategoryTS)

              #Recency unique product
              if latestCategories and latestCategories[id] and latestCategories[id] > 0
                that.resultData['$inc']['recencyUniqueCategory'] += (that.baseVar.lastVisit.lastclick - latestCategories[id])

            score = _gprofile.CategoryIndex 365, obj.g, obj.n
            Oldscore = 0
            if that.baseVar.prevVisit
              Oldscore = _gprofile.CategoryIndex 365, obj.g, obj.n, that.baseVar.prevVisit.lastclick
          if not that.resultData['$inc']['categories.'+id+'.amount']
            that.resultData['$inc']['categories.'+id+'.amount']=0
            that.resultData['$set']['categories.'+id+'.name']=obj.n
            that.resultData['$inc']['categories.'+id+'.inc']=0
            that.resultData['$inc']['categories.'+id+'.dec']=0
            that.resultData['$inc']['categories.'+id+'.quo']=0
            that.resultData['$inc']['categories.'+id+'.duration']=0
            that.resultData['$inc']['categories.'+id+'.clicks']=0
            that.resultData['$set']['categories.'+id+'.type'] = obj.t

          that.resultData['$inc']['categories.'+id+'.amount']++
          that.resultData['$inc']['categories.'+id+'.duration'] += if obj.d then obj.d else 0
          that.resultData['$inc']['categories.'+id+'.clicks'] += if obj.c then obj.c else 0

          if Oldscore < score then that.resultData['$inc']['categories.'+id+'.inc']++
          if Oldscore > score then that.resultData['$inc']['categories.'+id+'.dec']++
          if Oldscore == score then that.resultData['$inc']['categories.'+id+'.quo']++
    catch e
      logger.printError 'Add totals depending if its category or products ' , e
     */
    return this.baseVar.updateResultSet(this.resultData);
  };

  module.exports = initializeTrackers;

}).call(this);


//# sourceMappingURL=trackers.js.map
//# sourceURL=coffeescript