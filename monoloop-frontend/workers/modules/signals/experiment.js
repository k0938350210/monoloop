(function() {
  'use strict';
  var async, cachedLayer, config, elementModel, experimentCtrl, initiateRequest, lupus, request;

  request = require('request');

  config = require('./../../../config').get('api');

  cachedLayer = require('./../../../app/utils/cachingLayer');

  elementModel = require('./../../../app/model/element');

  lupus = require('lupus');

  experimentCtrl = require('./../../../app/controller/experiment');

  async = require('async');

  let logger = require('./../../../app/utils/logger');

  const debug = require('debug')('ml:archive');

  module.exports = {
    prepareSignal: function(baseVar, signals, cb) {
      var _gprofile, asyncTasks, j, len, pSignals, signal;
      pSignals = signals;
      asyncTasks = [];
      if (pSignals && pSignals.length > 0) {
        _gprofile = baseVar.getGlobalProfile();
        for (j = 0, len = pSignals.length; j < len; j++) {
          signal = pSignals[j];
          asyncTasks.push(function(cb) {
            return cachedLayer.invokeExperimentCaching(_gprofile.customerID, signal, function(er, result) {
              if (er) {
                logger.printError(er);
                return cb(er,null);
              }
              return cb(null, result);
            });
          });
        }
        if (asyncTasks) {
          return async.parallel(asyncTasks, function(err, results) {
            return cb(null, {
              experimentList: results
            });
          });
        } else {
          return cb(null, null);
        }
      } else {
        return cb(null, {
          experimentList: []
        });
      }
    },
    calculateExperiments: function(baseVar) {
      var _gprofile, ctrlgKeys, expKeys, lastVisitExperiments, profile, resultData;
      _gprofile = baseVar.getGlobalProfile();
      //add experiments
      expKeys = [];
      ctrlgKeys = [];
      profile = baseVar.getActiveSubProfile();
      profile.experiments = profile.experiments || {};
      profile.controlgroups = profile.controlgroups || {};

      if (typeof profile.experiments === "object") {
        expKeys = Object.keys(profile.experiments);
      }
      if (typeof profile.controlgroups === "object") {
        ctrlgKeys = Object.keys(profile.controlgroups);
      }
      if (baseVar.lastVisit && baseVar.lastVisit.experiments) {
        if (typeof baseVar.lastVisit.experiments === "object") {
          lastVisitExperiments = baseVar.lastVisit.experiments;
          resultData = baseVar.getResultSet();
          return Object.keys(lastVisitExperiments).forEach(function(i) {
            var $incGExperiments, currentview, date, indexEKey, lastClick, lastVisitlastClickHour, prevVisitFirstClickHour, previousVisit, revisits, visitCurrentView;
            currentview = 0;
            let is_new_visit = false;
            if (expKeys.indexOf(i) !== -1) {
              indexEKey = 'experiment';
            }
            if (ctrlgKeys.indexOf(i) !== -1) {
              indexEKey = 'controlGroup';
            }
            revisits = baseVar._gprofile.totalRevisitsExperiments(i);
            if (baseVar.$incrementVisit) {
              resultData['$inc']['experimentMetrics.' + i + '.' + indexEKey + '.totalRevisits'] = revisits.rV;
            }
            if (revisits.rV) {
              resultData['$inc']['experimentMetrics.' + i + '.' + indexEKey + '.totalRevisitPageViews'] = 1;
            }
            if (revisits.rV) {
              resultData['$inc']['experimentMetrics.' + i + '.' + indexEKey + '.totalRevisitTotalTimeOnSite'] = revisits.revistTTOS;
            }
            if (revisits.rV) {
              resultData['$inc']['experimentMetrics.' + i + '.' + indexEKey + '.totalRevisitTimeOnSite'] = revisits.revistTOS;
            }
            if (lastVisitExperiments[i].isPartOfExperiment === true) {
              // initiate webhook trigger request

              if (!lastVisitExperiments[i].aggregated || lastVisitExperiments[i].aggregated !== true) {
                if( lastVisitExperiments[i].views === 1){
                  is_new_visit = true;
                }
                baseVar.$incExperiments = 1;
                lastVisitExperiments[i].aggregated = true;
                if (baseVar.lastVisit.clicks >= 2) {
                  baseVar.bounced = 0;
                } else {
                  baseVar.bounced = 1;
                }
              } else {
                baseVar.$incExperiments = 0;
                if (baseVar.lastVisit.clicks === 2) {
                  baseVar.bounced = -1;
                }
              }
              if (profile.goals) {
                if (baseVar.expGoals.indexOf(lastVisitExperiments[i].goal) !== -1) {
                  if (!lastVisitExperiments[i].goalAggregated || lastVisitExperiments[i].goalAggregated !== true) {
                    baseVar.$incGExperiments = 1;
                    lastVisitExperiments[i].goalAggregated = true;
                  } else {
                    baseVar.$incGExperiments = 0;
                  }
                  baseVar.excludeGoals++;
                  resultData['$set']['goals.' + lastVisitExperiments[i].goal + '.experimentID'] = i;
                  if (expKeys.indexOf(i) !== -1) {
                    if (!resultData['$inc']['experiments.' + i + '.direct']) {
                      resultData['$inc']['experiments.' + i + '.direct'] = baseVar.$incGExperiments;
                    }
                    if (baseVar.$incGExperiments === 1 && baseVar.convertedGoals >= baseVar.excludeGoals) {
                      resultData['$inc']['experiments.' + i + '.assisted'] = baseVar.convertedGoals - baseVar.excludeGoals;
                    }
                  }
                  if (ctrlgKeys.indexOf(i) !== -1) {
                    if (baseVar.$incGExperiments === 1 && baseVar.convertedGoals >= baseVar.excludeGoals) {
                      resultData['$inc']['experiments.' + i + '.cg_assisted'] = baseVar.convertedGoals - baseVar.excludeGoals;
                      resultData['$inc']['experiments.' + i + '.cg_a_views'] = lastVisitExperiments[i].views;
                    }
                    if (!resultData['$inc']['experiments.' + i + '.cg_direct']) {
                      resultData['$inc']['experiments.' + i + '.cg_direct'] = baseVar.$incGExperiments;
                    }
                  }
                } else {
                  lastVisitExperiments[i].goalAggregated = false;
                }
              }
              currentview = 0;
              visitCurrentView = 0;
              if (baseVar.$incExperiments) {
                visitCurrentView = (new Date(baseVar.lastVisit.lastclick * 1000).getTime() - new Date(baseVar.lastVisit.firstclick * 1000).getTime()) / 1000;
                currentview = (new Date(baseVar.lastVisit.lastclick * 1000).getTime() - new Date(baseVar.lastVisit.firstclick * 1000).getTime()) / 1000;
              } else {
                if (profile.current.views.length > 1) {
                  visitCurrentView = profile.current.views[profile.current.views.length - 1].timeStamp - profile.current.views[profile.current.views.length - 2].timeStamp;
                }
              }
              /*if baseVar.lastVisit.clicks is 1
              baseVar.bounced = 1
                          if baseVar.lastVisit.clicks is 2
              baseVar.bounced = -1*/
              if (profile.visits.length === 1) {
                if (!baseVar.$incExperiments && profile.current.views.length > 1) {
                  currentview = profile.current.views[profile.current.views.length - 1].timeStamp - profile.current.views[profile.current.views.length - 2].timeStamp;
                }
              } else {
                if (baseVar.$incExperiments) {
                  if (baseVar.lastVisit.clicks === 1) {
                    if (baseVar.prevVisit.experiments[i].isPartOfExperiment !== false) {
                      //check previous visit time is also in current hour
                      date = new Date(baseVar.prevVisit.lastclick * 1000);
                      prevVisitFirstClickHour = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours()).getTime() / 1000;
                      lastClick = new Date(baseVar.lastVisit.lastclick * 1000);
                      lastVisitlastClickHour = new Date(lastClick.getFullYear(), lastClick.getMonth(), lastClick.getDate(), lastClick.getHours()).getTime() / 1000;
                      if (lastVisitlastClickHour === prevVisitFirstClickHour) {
                        previousVisit = (new Date(baseVar.prevVisit.lastclick * 1000).getTime() - new Date(baseVar.prevVisit.firstclick * 1000).getTime()) / 1000;
                        //if use float value it some time calculate negative value which is very small
                        currentview = parseInt(previousVisit) * (-1);
                      }
                    }
                  } else {
                    currentview = profile.current.views[profile.current.views.length - 1].timeStamp - profile.current.views[profile.current.views.length - 2].timeStamp;
                  }
                } else {
                  currentview = profile.current.views[profile.current.views.length - 1].timeStamp - profile.current.views[profile.current.views.length - 2].timeStamp;
                }
              }
              if (expKeys.indexOf(i) !== -1) {
                if(is_new_visit){
                  resultData['$inc']['experimentMetrics.' + i + '.experiment.totalVisits'] = 1;
                  resultData['$inc']['experiments.' + i + '.a_visitors'] = 1;
                  resultData['$inc']['experiments.' + i + '.visitors'] = 1;
                }

                resultData['$inc']['experimentMetrics.' + i + '.experiment.totalPageViews'] = baseVar.$incExperiments;
                resultData['$inc']['experimentMetrics.' + i + '.experiment.totalTimeOnSite'] = visitCurrentView;
                resultData['$inc']['experimentMetrics.' + i + '.experiment.TimeOnSite'] = currentview;
                resultData['$inc']['experimentMetrics.' + i + '.experiment.totalBouncedVisits'] = baseVar.bounced;
                resultData['$inc']['experiments.' + i + '.views'] = baseVar.$incExperiments;
                resultData['$inc']['experiments.' + i + '.a_views'] = baseVar.$incExperiments;
              }
              if (ctrlgKeys.indexOf(i) !== -1) {
                if(is_new_visit){
                  resultData['$inc']['experimentMetrics.' + i + '.controlGroup.totalVisits'] = 1;
                  resultData['$inc']['experiments.' + i + '.cg_visitors'] = 1;
                  resultData['$inc']['experiments.' + i + '.cg_a_visitors'] = 1;
                }
                resultData['$inc']['experimentMetrics.' + i + '.controlGroup.totalPageViews'] = baseVar.$incExperiments;
                resultData['$inc']['experimentMetrics.' + i + '.controlGroup.TimeOnSite'] = currentview;
                resultData['$inc']['experimentMetrics.' + i + '.controlGroup.totalTimeOnSite'] = visitCurrentView;
                resultData['$inc']['experimentMetrics.' + i + '.controlGroup.totalBouncedVisits'] = baseVar.bounced;
                resultData['$inc']['experiments.' + i + '.cg_views'] = baseVar.$incExperiments;
              }
            }
            return baseVar.updateResultSet(resultData);
          });
        }
      }
    }
  };

  initiateRequest = function(_gprofile, varSignals) {
    var headerType;
    headerType = 'application/json';
    return request.post({
      headers: {
        'content-type': headerType,
        'token': config.token
      },
      url: config.base_url + config.paths.pValue,
      body: {
        cid: _gprofile.customerID,
        eid: varSignals
      },
      json: true
    });
  };

}).call(this);


//# sourceMappingURL=experiment.js.map
//# sourceURL=coffeescript