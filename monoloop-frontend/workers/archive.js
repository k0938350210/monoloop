
const Redis = require('ioredis');
const archive_redis = new Redis();
const Avg = require('../app/model/ReportingArchiveAggregated');
const async = require('async');
const debug = require('debug')('ml:archive');
let logger = require('./../app/utils/logger');
let baseArchive = require('./modules/baseController');
let baseVariable = require('./modules/baseVariables');

let baseVar = {};

var archive_queue = function(){
  return archive_redis.blpop('profilelist', 0, function(err, data) {
    var gmid = null;
    var signals = null;
    if (data !== void 0) {
      onlyTracker = false;
      data[1] = JSON.parse(data[1]);
      gmid = data[1].gmid;
      debug('Process profile: '+gmid);
      signals = data[1].signals;
      asyncTasks = [];
      if (signals && signals.tracker === "enabled") {
        onlyTracker = true;
      }

      return baseArchive.initiateArchive(gmid, function(error, _gprofile) {

        if (error) {
          logger.printError('Error occurred while profile fetching cannot proceed', error);
          return archive_queue();
        }

        if(_gprofile !== null){
          _gprofile.profiles.forEach(function(profile) {
            if (_gprofile.recently_used_device === profile._id.toString() && profile.visits) {
              baseVar = baseVariable.initializeArchive(_gprofile, onlyTracker);
              if (!baseVar.onlyTracker) {
                //calculate calculatePValue
                asyncTasks.push(function(callback) {
                  return baseArchive.experimentSignal(baseVar, signals, callback);
                });
                //sync profile
                asyncTasks.push(function(callback) {
                  return baseArchive.initiateSyncProfile(baseVar, callback);
                });
                //calculate segments
                asyncTasks.push(function(callback) {
                  return baseArchive.processSegments(baseVar, callback);
                });
                //Bot calculation
                return asyncTasks.push(function(callback) {
                  return baseArchive.detechbot(baseVar, callback);
                });

              }
            }
          });
        }else{
          logger.printError('Profile not exists : '+gmid, '');
          return archive_queue();
        }

        return async.parallel(asyncTasks, function(err, results) {
          if(err){
            logger.printError('Error on async.parallel', err);
            return archive_queue();
          }
          baseVar.asyncResultFetch(results);

          if (_gprofile !== null && _gprofile.customerID !== void 0) {
            if (!baseVar.onlyTracker) {
              baseArchive.profileGenericParamsInc(baseVar);
              baseArchive.calculateSegments(baseVar);
              baseArchive.calculateGoals(baseVar);
              baseArchive.calculateExperiments(baseVar);
            } else if (baseVar.onlyTracker) {
              baseArchive.trackersArchive(baseVar);
            }
            //Used in profileArchiveUpdator to Monoloop and postback api related properties separately
            return baseArchive.profileArchiveUpdator(baseVar, function(error, updated) {
              if (error) {
                logger.printError("Error Occurred in Final Update", error);
              } else {
                debug("Archiving Cycle Completed");
              }
              setTimeout(function() {
                debug('End profile: '+gmid);
                return archive_queue();
              }, 100);
            });
          } else {
            return archive_queue();
          }
        });
      });
    }
  });
}

archive_queue();