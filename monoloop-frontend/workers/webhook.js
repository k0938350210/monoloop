process.title = 'webhook';

const async = require('async');
const config_s3 = require('../config').get('s3');
const debug = require('debug')('ml:webhook');

let logger = require('../app/utils/logger');

let AWS = require('aws-sdk');
AWS.config.update({
  accessKeyId: config_s3.access_key,
  secretAccessKey: config_s3.secret_key
});

const webhook = require('../app/model/webhook');
const profile = require('../app/model/profile');

let sqs = new AWS.SQS({region: config_s3.sqs.region});

var params = {
  MaxNumberOfMessages: 1,
  MessageAttributeNames: [
    "gmid", "mid"
  ],
  QueueUrl: config_s3.sqs.webhook_queue,
  VisibilityTimeout: 600,
  WaitTimeSeconds: 0
};

let request = require('requestretry');

async.forever((function(next) {

  sqs.receiveMessage(params, function(err, data) {
    if (err) {
      logger.printError("Receive Error", err);
      next();
    } else if (data.Messages) {
      let message = data.Messages[0];
      let gmid = message.MessageAttributes.gmid.StringValue;
      let mid = message.MessageAttributes.mid.StringValue;
      let webhook_ids = JSON.parse(message.Body);
      debug(gmid);
      debug(webhook_ids);

      var deleteParams = {
        QueueUrl: config_s3.sqs.webhook_queue,
        ReceiptHandle: data.Messages[0].ReceiptHandle
      };

      sqs.deleteMessage(deleteParams, function(err, data) {
        if (err) {
          logger.printError("Delete sqs error", err);
          return next();
        } else {
          debug("Message Deleted", data);
        }
        // Find profile by gmid
        profile.findById(gmid,function(error, profile_obj){
          if(err){
            logger.printError("Profile fetch error", err);
            return next();
          }
          // Find webhook from sqs message
          webhook.find({
            _id: { '$in': webhook_ids}
          },function (err, webhooks) {

            if(err){
              logger.printError("Webhook fetch error", err);
              return next();
            }

            if(webhooks.length == 0){
              return next();
            }

            // Async request retry
            async.map(webhooks, function(webhook_obj, callback) {
              debug('request',webhook_obj.callback_url);

              request({
                url: webhook_obj.callback_url,
                json: true,
                method: 'POST',
                body:{
                  gmid: gmid,
                  mid: mid,
                  webhook: webhook_obj,
                  profile: profile_obj
                },
                // The below parameters are specific to request-retry
                maxAttempts: 5,   // (default) try 5 times
                retryDelay: 2000,  // (default) wait for 5s before trying again
                retryStrategy: request.RetryStrategies.HTTPOrNetworkError // (default) retry on 5xx or network errors
              }, function(err, response, body){
                // this callback will only be called when the request succeeded or after maxAttempts or on error
                callback(null, null);
              });


            }, function(err, results) {
              if(err){
                logger.printError("Request error", err);
              }
              return next();
            });
          });
        });
      });


    }else{
      debug('No queue');
      setTimeout(function() {
        next();
      }, 5000);
    }
  });

}), function() {
    debug('Exiting Process');
    return process.exit(1);
});