var request = require('request');
var total_fail = 0;
var pm2 = require('pm2');
let logger = require('./../app/utils/logger');
const config = require('../config').get('fe');

var checkHealth = function(){
  let port = (config && config.port)? config.port : 9000;

  var options = {
    url: 'http://127.0.0.1:'+port+'/healthcheck',
    method: 'GET',
    json:true
  }

  request(options, function(error, response, body){
    if(response !== undefined && response.statusCode === 200){
      total_fail = 0;
      setTimeout(function() {
        return checkHealth();
      }, 5000);
    }else{
      total_fail++;
      logger.printError('Fail '+ total_fail,'');
      if(total_fail == 3){
        total_fail = 0;
        pm2.restart('core',function(err,res){
          if(err){
            logger.printError(err,'');
          }
          logger.printError(err,'');
          setTimeout(function() {
            return checkHealth();
          }, 5000);
        });

      }else{
        setTimeout(function() {
          return checkHealth();
        }, 5000);
      }
    }
  });
};

checkHealth();