const webhook_model = require('../app/model/webhook');
const profile_model = require('../app/model/profile');

var inactive_service = require('../app/services/profile/inactive');

const async = require('async');
const debug = require('debug')('ml:worker');

var inactive_profiles = async function(){
  // get all inactive webhook
  let webhooks = await inactive_service.getInactiveWebhooks();

  //get match profiles
  let profiles = await inactive_service.getInactiveProfiles(webhooks);

  //debug(profiles);
  debug(webhooks);
  await inactive_service.setInactiveFlagToProfiles(webhooks, profiles);


  setTimeout(function() {
    return inactive_profiles();
  }, 1000);
};

inactive_profiles();