const Redis = require('ioredis');
const webhook_redis = new Redis();
const webhook_redis2 = new Redis();
const async = require('async');
const debug = require('debug')('ml:webhook');
//local
let logger = require('./../app/utils/logger');
const webhook_trigger = require('./../app/utils/webhook_trigger');

var webhook_queue = function(){
  return webhook_redis.blpop('webhooklist', 0, function(err, data) {
    data[1] = JSON.parse(data[1]);
    webhook_trigger.performTriggerFromCmd(data[1].cid,data[1].gmid, data[1].mid, data[1].batch_cmd, function(err,data){
      setTimeout(function() {
        return webhook_queue();
      }, 200);
    });
  });
};

webhook_queue();

var inactive_profile_webhook_queue = function(){
	debug('inactive profile webhook');

	return webhook_redis2.blpop('webhook-inactive-profile-list', 0, function(err, data) {
    data[1] = JSON.parse(data[1]);
    debug(data[1]);
    webhook_trigger.performTriggerInactiveProfile(data[1].gmid, data[1].mid, data[1].webhook_id, function(err,data){
      setTimeout(function() {
        return inactive_profile_webhook_queue();
      }, 200);
    });
  });
};

inactive_profile_webhook_queue();