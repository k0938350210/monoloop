
let profile_model = require('../app/model/profile');
let crypto = require('crypto');
let fe_config = require('../config').get('fe');
const fetch = require('node-fetch');
//let total_profile = await profile_model.collection.count();

(async () => {
  let total_profile = await profile_model.countDocuments();
  console.log('Total record: '+total_profile);

  let cur = 0;


  while(cur < 100000){
  	console.log('Process ' + cur + ' of 100000');

  	let profiles = await profile_model.find({},'customerID',{skip: cur}).sort({_id: -1}).limit(100);

  	var cryptSalt = '130293+109k21jl1c31c9312i3c910202';


  	for(let i = 0; i < profiles.length; i++){
  		let profile = profiles[i];
  		//console.log(profile);
  		let cid = profile.customerID;
  		let pass = crypto.createHash('sha512').update(cid + cryptSalt).digest('hex');
  		let url = fe_config.url+'/profiles/'+profile._id;

  		let res = await fetch(url,{
  			method: 'get',
  			headers: {
  				'Content-Type': 'application/json',
  				'Authorization': 'Basic ' + Buffer.from(cid + ':' + pass).toString('base64')
  			}
  		}).then(res => res.json());
  		if(res.success !== true){
  			console.error('Error ' + profile._id);

  			if(res.profiles[0]._id !== profile._id){
  				console.error('How that suppose to happen');
  			}
  		}
  	}

  	cur += 100;
  }

  process.exit();
})();
