<?php

class GoalDeleteWithPageTest extends TestCase{
  public function testDeleteGoalWithPage(){
    //api/content/web/folder?ajax=true
    $response = $this->call('GET','/api/content/web?ajax=true');
    $content = json_decode($response->getContent(),true);
    $folder_id = $content[0]['id'];

    $response = $this->call('POST','/api/content/web/folder?ajax=true',[
      'action' => 'files',
      'source' => $folder_id
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);
    #start content count
    $step1_totalpages = count($content['data']);

    ############ CREATE PAGE VARIATION

    #1.create new page element with variation
    $response = $this->call('POST','/api/page-element/content-create',[
      'condition' => '',
      'content' => 'new content',
      'contentType' => '',
      'experiment_id' => null,
      'fullURL' => 'http://local-demo1wo.com/product/demo4',
      'page_element_id' => null,
      'source' => 'default',
      'type' => 'variation',
      'variation_name' => 'Variation [1]',
      'xpath' => 'DIV#product-97'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $response = $this->call('POST','/component/page-list-publish/store',[
      'currentFolder' => $folder_id,
      'experiment_id  ' => null ,
      'fullURLWebPageList' => 'http://local-demo1wo.com/product/demo4' ,
      'includeHttpHttpsWebPageElement' => 0 ,
      'includeWWWWebPageElement' => 0 ,
      'isExistingPageELementID' => false,
      'page_element_id' => $content['pageElement']['_id'] ,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageList' => '' ,
      'source' => 'default' ,
      'urlOptionWebPageElement' => 'exactMatch'
    ]);
    $this->assertEquals(200, $response->status());

    #2. After create variation page listing record should plus one.
    $response = $this->call('POST','/api/content/web/folder?ajax=true',[
      'action' => 'files',
      'source' => $folder_id
    ]);
    $content = json_decode($response->getContent(),true);
    $step2_totalpages = count($content['data']);
    $this->assertEquals(1,$step2_totalpages - $step1_totalpages);

    ############# CRETE GOAL

    #3. Create new goal with page
    $response = $this->call('POST','/component/page-list-publish/store',[
      'currentFolder' => $folder_id,
      'experiment_id  ' => null ,
      'fullURLWebPageList' => 'http://local-demo1wo.com/product/demo4' ,
      'includeHttpHttpsWebPageElement' => 0 ,
      'includeWWWWebPageElement' => 0 ,
      'page_element_id' => null,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageList' => '' ,
      'source' => 'goal' ,
      'urlOptionWebPageElement' => 'exactMatch'
    ]);
    $content = json_decode($response->getContent(),true);
    $response = $this->call('POST','/api/goals/create',[
      'ajax' => true,
      'conditionGoal' => "if ( ( MonoloopProfile.VisitCount == '12' ) ){|}",
      'nameGoal' => 'name',
      'page_element_id' => $content['content']['_id'],
      'pointsGoal' => 0,
      'source' => $folder_id,
      'typeGoal' => 'page_related_with_condition'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);
    $goal_id = $content['content']['goal']['_id'];
    #4. After goal create page listing number sould still same
    $response = $this->call('POST','/api/content/web/folder?ajax=true',[
      'action' => 'files',
      'source' => $folder_id
    ]);
    $content = json_decode($response->getContent(),true);
    $step4_totalpages = count($content['data']);
    $this->assertEquals($step2_totalpages,$step4_totalpages);

    ############ DELETE GOAL

    #5 Delete goal
    $response = $this->call('POST','/api/goals/delete',[
      '_id' => 'goal__' . $goal_id
    ]);

    #6 Delete goal should not effect to page listing record count
    $response = $this->call('POST','/api/content/web/folder?ajax=true',[
      'action' => 'files',
      'source' => $folder_id
    ]);
    $content = json_decode($response->getContent(),true);
    $step6_totalpages = count($content['data']);
    $this->assertEquals($step6_totalpages,$step4_totalpages);
  }
}