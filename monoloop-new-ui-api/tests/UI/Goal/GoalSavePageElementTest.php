<?php

use App\Services\Experiment\PvalueCalculator;
use App\PageElement;
use App\User;

class GoalSavePageElementTest extends TestCase {
	public function dataConditionRelateGoal001(){
		return [
			'ajax' => true,
			'conditionGoal' => "if ( ( MonoloopProfile.PageViewCount == '1' ) ){|}",
			'nameGoal' => 'Test00123',
			'pointsGoal' => 1,
			'typeGoal' => 'condition_related',
		];
	}

	public function testGoalConditionRelate(){
		$user = User::first() ;
    $response = $this->actingAs($user)->call('POST', '/api/goals/create' ,$this->dataConditionRelateGoal001());

    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);
    $goal = $content['content']['goal'];

    $this->assertEquals(1,$goal['hidden']);
    $this->assertEquals(1,$goal['point']);
    $this->assertEquals('condition_related',$goal['type']);
    $this->assertEquals("if ( ( MonoloopProfile.PageViewCount == '1' ) ){|}",$goal['condition']);

    $page_element_id = $goal['page_element_id'] ;

    $page_element = PageElement::find($page_element_id);
    $goal_id = $goal['_id'] ;

    # AS we still hidden so no content yet .
    $this->assertTrue(!($page_element->content->count()));

    # Active
    $response = $this->actingAs($user)->call('POST', '/api/goals/update-status' ,[
    	'_id' =>   $goal_id ,
    	'action' => 'update_hidden' ,
    	'ajax' => true ,
    	'hidden' => 0
    ]);


    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

 		$page_element = PageElement::find($page_element_id);

 		$content = $page_element->content;
 		$this->assertEquals($content[0]['uid'],$goal['uid']);

 		# InActive ;
 		$response = $this->actingAs($user)->call('POST', '/api/goals/update-status' ,[
    	'_id' =>   $goal_id ,
    	'action' => 'update_hidden' ,
    	'ajax' => true ,
    	'hidden' => 1
    ]);

    $page_element = PageElement::find($page_element_id);
    $goal_id = $goal['_id'] ;

    $this->assertTrue(!($page_element->content->count()));

	}

	public function testGoalPageCondition(){

    $user = User::first() ;

  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://someurl.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);

  	$this->assertEquals(200, $response->status());
  	$content1 = json_decode($response->getContent(),true) ;

 		$page_element_id = $content1['content']['_id'] ;

 		$response = $this->actingAs($user)->call('POST','/api/goals/create',[
  		'conditionGoal' => "if ( ( MonoloopProfile.PageViewCount == '1' ) ){ | }" ,
  		'nameGoal' => 'New UI 02' ,
  		'page_element_id' => $page_element_id ,
  		'pointsGoal' => 1 ,
  		'typeGoal' => 'page_related_with_condition'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content3 = json_decode($response->getContent(),true) ;

  	$goal = $content3['content']['goal'];
  	$goal_id = $goal['_id'] ;

    $this->assertEquals(1,$goal['hidden']);
    $this->assertEquals(1,$goal['point']);
    $this->assertEquals('page_related_with_condition',$goal['type']);
    $this->assertEquals("if ( ( MonoloopProfile.PageViewCount == '1' ) ){ | }",$goal['condition']);

    $page_element = PageElement::find($page_element_id);
    $this->assertTrue(!($page_element->content->count()));

    # Active
    $response = $this->actingAs($user)->call('POST', '/api/goals/update-status' ,[
    	'_id' =>   $goal_id ,
    	'action' => 'update_hidden' ,
    	'ajax' => true ,
    	'hidden' => 0
    ]);


    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

 		$page_element = PageElement::find($page_element_id);

 		$content = $page_element->content;
 		$this->assertEquals($content[0]['uid'],$goal['uid']);
	}

}