<?php

use App\User;

class GoalListingTest extends TestCase{
	public function dataConditionRelateGoal001(){
		return [
			'ajax' => true,
			'conditionGoal' => "if ( ( MonoloopProfile.PageViewCount == '1' ) ){|}",
			'nameGoal' => 'Test00123',
			'pointsGoal' => 1,
			'typeGoal' => 'condition_related',
		];
	}

	public function testListingWithoutFolderId(){
		$user = User::first() ;
    $response = $this->actingAs($user)->call('POST', '/api/goals/create' ,$this->dataConditionRelateGoal001());

    $user = User::first() ;
    $response = $this->actingAs($user)->call('POST', '/api/goals/create' ,$this->dataConditionRelateGoal001());

    $response = $this->actingAs($user)->call('GET', '/api/goals/folder?ajax=true');
    $content = json_decode($response->getContent(),true);

    $this->assertEquals(count($content['data']),2);
	}
}