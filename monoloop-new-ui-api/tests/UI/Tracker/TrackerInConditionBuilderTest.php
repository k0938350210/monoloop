<?php
use App\User;
use App\Tracker;

class TrackerInConditionBuilderTest extends TestCase {
  public function testCBListingWithNoTracker(){
    $user = User::first() ;
    $response = $this->actingAs($user)->call('GET', '/api/profile-properties');
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $this->assertTrue(!$this->findGroup($content,'Trackers'));
    $this->assertTrue(!isset($content['Forms']));
  }

  public function testCBListingWithDeleteTracker(){
    $user = User::first();
    $response = $this->actingAs($user)->call('POST', '/api/trackers/create',['tracker' => [
      'name' => 'xx123',
      'type' => 'custom',
      'folder_id' => 'folder__580a7ec724233696188b4572',
      'id' => '',
      'url_config' => [
        'url' => 'http://local-demo1wp.com/',
        'remark' => '',
        'reg_ex' => '',
        'inc_www' => '0',
        'inc_http_https' => '0',
        'url_option' => 'exactMatch',
      ],
      'tracker_events' => [
        [
          '_id' => 'event-6e7b1a7a-b4b1-7abd-4834-d8abe4279148',
          'name' => '',
          'type' => 'page-render',
          'selector' => '',
          'custom' => '',
        ]
      ],
      'tracker_fields' => [
        [
          '_id' => 'field-a9e5d855-2ccb-0d19-211c-e56de3f81654',
          'name' => 'WWW',
          'field_type' => 'string',
          'operator' => 'push',
          'type' => 'x-path',
          'selector' => 'DIV.header-widget-region DIV.col-full DIV#search-3 SPAN.gamma.widget-title',
          'meta' => '',
          'cookie' => '',
          'js' => '',
          'custom' => "return 'val'",
          'filter' => 'none',
          'filter_custom' => 'return result',
        ]
      ]
    ],
    'ajax' => true]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $response = $this->actingAs($user)->call('POST', '/api/trackers/delete',[
      '_id' => 'tracker__'.$content['content']['id'],
      'ajax' => true,
    ]);

    $this->assertEquals(200,$response->getStatusCode());

    $response = $this->actingAs($user)->call('GET', '/api/profile-properties');
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $this->assertTrue(!$this->findGroup($content,'Trackers'));
  }

  public function testCBListingWithCustomerTracker(){
    // Create customer tracker
    $user = User::first();
    $response = $this->actingAs($user)->call('POST', '/api/trackers/create',['tracker' => [
      'name' => 'xx123',
      'type' => 'custom',
      'folder_id' => 'folder__580a7ec724233696188b4572',
      'id' => '',
      'url_config' => [
        'url' => 'http://local-demo1wp.com/',
        'remark' => '',
        'reg_ex' => '',
        'inc_www' => '0',
        'inc_http_https' => '0',
        'url_option' => 'exactMatch',
      ],
      'tracker_events' => [
        [
          '_id' => 'event-6e7b1a7a-b4b1-7abd-4834-d8abe4279148',
          'name' => '',
          'type' => 'page-render',
          'selector' => '',
          'custom' => '',
        ]
      ],
      'tracker_fields' => [
        [
          '_id' => 'field-a9e5d855-2ccb-0d19-211c-e56de3f81654',
          'name' => 'WWW',
          'field_type' => 'string',
          'operator' => 'push',
          'type' => 'x-path',
          'selector' => 'DIV.header-widget-region DIV.col-full DIV#search-3 SPAN.gamma.widget-title',
          'meta' => '',
          'cookie' => '',
          'js' => '',
          'custom' => "return 'val'",
          'filter' => 'none',
          'filter_custom' => 'return result',
        ]
      ]
    ],
    'ajax' => true]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $response = $this->actingAs($user)->call('POST', '/api/trackers/update-status',[
      '_id' => 'tracker__'.$content['content']['id'],
      'action' => 'update_hidden',
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    $response = $this->actingAs($user)->call('GET', '/api/profile-properties');
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $this->assertTrue($this->findGroup($content,'Trackers'));
    $this->assertEquals(1,$this->CountGroupItem($content,'Trackers'));
  }

  public function testCBListingWithFormTracker(){
    $user = User::first();
    $response = $this->actingAs($user)->call('POST', '/api/trackers/create',['tracker' => [
      'name' => 'xxx',
      'type' => 'form',
      'folder_id' => 'folder__580a7ec724233696188b4572',
      'id' => '',
      'url_config' => [
        'url' => 'http://local-demo1wp.com/',
        'remark' => '',
        'reg_ex' => '',
        'inc_www' => 0,
        'inc_http_https' => 0,
        'url_option' => 'exactMatch',
      ],
      'tracker_events' => [
        [
          '_id' => 'event-14134deb-b95c-adc5-47ec-1e7daaf72c13',
          'name' => '',
          'type' => 'page-render',
          'selector' => '',
          'custom' => ''
        ]
      ],
      'tracker_fields' => [
        [
          '_id' => 'field-e66d87a8-838e-f52d-31ee-556e12c34165',
          'name' => 'Select form',
          'field_type' => 'string',
          'operator' => 'push',
          'type' => 'x-path',
          'selector' => 'DIV.col-full    DIV.entry-content DIV.row DIV.col-xs-12 DIV.simple-conversion  FORM#ml-signup-form.frm-show-form',
          'meta' => '',
          'cookie' => '',
          'js' => '',
          'custom' => '',
          'filter' => 'none',
          'filter_custom' => '',
        ],
        [
          '_id' => 'field-a135fd40-1885-2e9f-4889-2254aad6e83c',
          'name' => 'firstname',
          'field_type' => 'string',
          'operator' => 'override',
          'type' => 'custom',
          'selector' => 'IV.col-full    DIV.entry-content DIV.row DIV.col-xs-12 DIV.simple-conversion  FORM#ml-signup-form DIV.frm_form_fields FIELDSET DIV.frm_form_field.form-field.frm_required_field.frm_none_container INPUT:eq(0)',
          'meta' => '',
          'cookie' => '',
          'js' => '',
          'custom' => "return MONOloop.val(MONOloop.$('DIV.col-full    DIV.entry-content DIV.row DIV.col-xs-12 DIV.simple-conversion  FORM#ml-signup-form DIV.frm_form_fields FIELDSET DIV.frm_form_field.form-field.frm_required_field.frm_none_container INPUT:eq(0)')); ",
          'filter' => '',
          'filter_custom' => '',
        ]
      ]
    ],
      'ajax' => true
    ]);

    $this->assertEquals(200,$response->getStatusCode());

    $response = $this->actingAs($user)->call('GET', '/api/profile-properties');
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);


    $this->assertTrue($this->findGroup($content,'Forms'));
    $this->assertEquals(1,$this->CountGroupItem($content,'Forms'));
  }

  public function testCBListingWithEmptyField(){
    $user = User::first();
    $response = $this->actingAs($user)->call('POST', '/api/trackers/create',['tracker' => [
      'name' => 'xx123',
      'type' => 'custom',
      'folder_id' => 'folder__580a7ec724233696188b4572',
      'id' => '',
      'url_config' => [
        'url' => 'http://local-demo1wp.com/',
        'remark' => '',
        'reg_ex' => '',
        'inc_www' => '0',
        'inc_http_https' => '0',
        'url_option' => 'exactMatch',
      ],
      'tracker_events' => null,
      'tracker_fields' => null,
    ],
    'ajax' => true]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);
    $this->assertEquals(200,$response->getStatusCode());

    $response = $this->actingAs($user)->call('GET', '/api/profile-properties');
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $this->assertTrue(!$this->findGroup($content,'Trackers'));
  }

  private function findGroup($content,$group){
    foreach($content as $rec){
      if($group === $rec['Group'])
        return true;
    }
    return false;
  }

  private function CountGroupItem($content,$group){
    $count = 0;
    foreach($content as $rec){
      if($group === $rec['Group']){
        $count++;
      }
    }
    return $count;
  }

}