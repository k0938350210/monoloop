<?php
use App\User;
use App\Tracker;

class TrackerListingTest extends TestCase {
  public function testListing(){
    $user = User::first();
    $response = $this->actingAs($user)->call('POST', '/api/trackers/create',['tracker' => [
      'name' => 'ME1',
      'type' => 'custom',
      'folder_id' => 'folder__580a7ec724233696188b4572',
      'id' => '',
      'url_config' => [
        'url' => 'http://local-demo1wp.com/',
        'remark' => '',
        'reg_ex' => '',
        'inc_www' => '0',
        'inc_http_https' => '0',
        'url_option' => 'exactMatch',
      ]
    ]]);

    $response = $this->actingAs($user)->call('POST', '/api/trackers/create',['tracker' => [
      'name' => 'ME2',
      'type' => 'custom',
      'folder_id' => 'folder__580a7ec724233696188b4572',
      'id' => '',
      'url_config' => [
        'url' => 'http://local-demo1wp2.com/',
        'remark' => '',
        'reg_ex' => '',
        'inc_www' => '0',
        'inc_http_https' => '0',
        'url_option' => 'exactMatch',
      ]
    ]]);

    $content = json_decode($response->getContent(),true);
    $tracker = Tracker::find($content['content']['id']);
    $tracker->cid = 100;
    $tracker->save();

    $response = $this->actingAs($user)->call('GET', '/api/trackers/folder?ajax=true');
    $content = json_decode($response->getContent(),true);
    $this->assertEquals(1,count($content['data']));
    $this->assertEquals('ME1',$content['data'][0]['value']);
  }
}