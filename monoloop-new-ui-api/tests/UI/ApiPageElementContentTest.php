<?php

use App\Placement;
use App\Content;
use App\PageElement;
use App\User;
use App\AccountDomain;


class ApiPageElementContentTest extends TestCase {
	public function testContentSave(){
    $user = User::first() ;
    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}" ,
  		'content' => 'my content' ,
  		'contentType' => '' ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => '' ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
  	]);
  	$content = json_decode($response->getContent(),true) ;

  	###Expect pageElement.content.code should wrap by condition
  	$this->assertEquals($content['pageElement']['content'][0]['code'] ,"<% if ( ( MonoloopProfile.VisitCount == '1' ) ){ %>my content<% } %>");
 	}

  public function testPlacementTypeDefault(){
    /*

    type: variation
    variation_name: Variation [1]
    condition:
    fullURL: http://demo-mon.nutjaa.win/local-test-010.html?demo
    page_element_id:
    experiment_id:
    xpath: DIV.col-sm H1
    content: Monoloop content will add below
    contentType:
    source: default

    */

    $user = User::first() ;
    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'type' => "variation",
      'variation_name' => 'Variation [1]',
      'condition' => '',
      'fullURL' => 'http://demo-mon.nutjaa.win/local-test-010.html?demo',
      'page_element_id' => null,
      'experiment_id' => null,
      'xpath' => 'DIV.col-sm H1',
      'content' => 'Monoloop content will add below' ,
      'contentType' => null ,
      'source' => 'default'
    ]);



    $this->assertEquals(200, $response->status());
    $content = json_decode($response->getContent(),true);
    $this->assertEquals(5,$content['pageElement']['content'][0]['placementType']);

    $page_element_id = $content['pageElement']['_id'];
    $page_element = PageElement::find($page_element_id);

    $this->assertEquals(5,$page_element->content->first()->placementType);

    // Api For listing
    $response = $this->actingAs($user)->call('GET','/api/page-element?url='.urlencode('http://demo-mon.nutjaa.win/local-test-010.html?demo').'&source=default&mode=edit&page_element_id='.$page_element_id);
    $this->assertEquals(200, $response->status());
    $content = json_decode($response->getContent(),true);
    $this->assertEquals(0, $content['is_domain_registered']);

    $domain = new AccountDomain();
    $domain->domain = 'demo-mon.nutjaa.win';
    $domain->privacy_option = 'auto';
    $domain->url_privacy = 'demo-mon.nutjaa.win';
    $user->account->domains()->save($domain);
    $response = $this->actingAs($user)->call('GET','/api/page-element?url='.urlencode('http://demo-mon.nutjaa.win/local-test-010.html?demo').'&source=default&mode=edit&page_element_id='.$page_element_id);
    $this->assertEquals(200, $response->status());
    $content = json_decode($response->getContent(),true);
    $this->assertEquals(1, $content['is_domain_registered']);


    //Api for content listing
    $response = $this->actingAs($user)->call('GET','/api/component/content-list?fullURL='.urlencode('http://demo-mon.nutjaa.win/local-test-010.html?demo').'&source=default&mode=edit&page_element_id='.$page_element_id.'&xpath=DIV.col-sm H1&xpath1=DIV.col-sm H1');
    $content = json_decode($response->getContent(),true);
    $this->assertEquals(200, $response->status());
    $this->assertEquals('DIV.col-sm H1', $content['xpath']);

    $this->assertEquals(1,count($content['variations']));
  }

  public function testContentSaveWithWWWHttpsDefault(){

    $user = User::first() ;
    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}" ,
      'content' => 'my content' ,
      'contentType' => '' ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => '' ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(true,$content['pageElement']['inWWW']);
    $this->assertEquals(true,$content['pageElement']['exWWW']);
    $this->assertEquals(true,$content['pageElement']['inHTTP_HTTPS']);
  }
}