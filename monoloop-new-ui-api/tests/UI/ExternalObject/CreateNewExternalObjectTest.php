<?php
use App\Account;
use App\Tracker;

class CreateNewExternalObjectTest extends TestCase {
  public function testCreateSimpleField(){
    $server = $this->getServerRequest();
    $cid = Account::first()->uid;
    $response = $this->call('POST', '/'.$cid.'/external-objects',[
      'tracker' => [
        'name' => 'test',
        'tracker_fields' => [[
          'name' => 'field1'
        ]]
      ]
    ], [], [], $server);

    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $this->assertEquals($content['tracker']['name'],'test');
    $this->assertEquals($content['tracker']['tracker_fields'][0]['name'], 'field1');
    $this->assertEquals($content['tracker']['tracker_fields'][0]['field_type'], 'string');
    $this->assertEquals($content['tracker']['tracker_fields'][0]['operator'], 'override');
  }

  public function testCannotDuplicateName(){
    $server = $this->getServerRequest();
    $cid = Account::first()->uid;
    $response = $this->call('POST', '/'.$cid.'/external-objects',[
      'tracker' => [
        'name' => 'test',
        'tracker_fields' => [[
          'name' => 'field1'
        ]]
      ]
    ], [], [], $server);

    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $this->assertEquals($content['tracker']['name'],'test');
    $this->assertEquals($content['tracker']['tracker_fields'][0]['name'], 'field1');

    $response = $this->call('POST', '/'.$cid.'/external-objects',[
      'tracker' => [
        'name' => 'test',
        'tracker_fields' => [[
          'name' => 'field1'
        ]]
      ]
    ], [], [], $server);

    $content = json_decode($response->getContent(),true);
    $this->assertEquals($content['status'],'422');
  }
}