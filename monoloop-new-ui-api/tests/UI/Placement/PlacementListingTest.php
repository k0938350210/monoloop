<?php

use App\PageElement;
use App\User;

class PlacementListingTest extends TestCase {
	public function testListing(){
		$user = User::first();
		$response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}",
  		'content' => 'my content',
  		'contentType' => '',
  		'experiment_id' => null,
  		'fullURL' => 'http://url_test001.com',
  		'page_element_id' => '',
  		'source' => 'default',
  		'type' => 'variation',
  		'variation_name' => 'my001',
  		'xpath' => 'BODY'
  	]);
    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}",
  		'content' => 'my content',
  		'contentType' => '',
  		'experiment_id' => null,
  		'fullURL' => 'http://url_test002.com',
  		'page_element_id' => '',
  		'source' => 'default',
  		'type' => 'variation',
  		'variation_name' => 'my001',
  		'xpath' => 'BODY'
  	]);
    $content = json_decode($response->getContent(),true);
 		$placement = PageElement::find($content['pageElement']['_id']);
 		$placement->cid = 100;
 		$placement->save();


 		$response = $this->actingAs($user)->call('GET','/api/content/web/folder?ajax=true');
 		$content = json_decode($response->getContent(),true);

    $this->assertEquals(count($content['data']),1);
    $this->assertEquals($content['data'][0]['value'],'http://url_test001.com');
	}
}