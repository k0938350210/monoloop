<?php

use App\User;
use App\UrlConfig;
use App\Services\ConditionBuilder\Syntax ;

class TestBenchPanelTest extends TestCase {
  public function testBasic(){
    $syntax = new Syntax() ;
    $groups =  $syntax->getTestBebchPanelObject("if ( ( MonoloopProfile.VisitCount == '13' ) ){|}") ;
    #print_r($groups);
    $this->assertTrue(array_key_exists('All Visits',$groups)) ;
    $this->assertEquals('Total Visits',$groups['All Visits']['MonoloopProfile_VisitCount']['text']);
    $this->assertEquals('VisitCount',$groups['All Visits']['MonoloopProfile_VisitCount']['id']);
    $this->assertEquals('integer',$groups['All Visits']['MonoloopProfile_VisitCount']['type']);
  }

  public function testBasic2(){
    $syntax = new Syntax() ;
    $groups =  $syntax->getTestBebchPanelObject("if ( ( ( MonoloopProfile.VisitCount == '' ) && ( ( MonoloopProfile.FirstVisit === true ) ) ) ){|}") ;
    #print_r($groups);
    $this->assertTrue(array_key_exists('All Visits',$groups)) ;
    $this->assertTrue(array_key_exists('Current Visit',$groups)) ;
  }

  public function testFunctionA(){
    $syntax = new Syntax();
    $groups = $syntax->getTestBebchPanelObject("if ( ( MonoloopProfile.isWithinArea('11', '11', '10', 'true') === true ) ){|}");
    $this->assertTrue(isset($groups['Location']['MonoloopProfile_isWithinArea_11_11_10_true'])) ;
    $this->assertEquals('function',$groups['Location']['MonoloopProfile_isWithinArea_11_11_10_true']['type']);
    $this->assertEquals('bool',$groups['Location']['MonoloopProfile_isWithinArea_11_11_10_true']['ReturnValue']);
  }
}
