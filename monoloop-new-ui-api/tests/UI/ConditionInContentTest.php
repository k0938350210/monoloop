<?php

use App\Services\MonoloopCondition;




class ConditionInContentTest extends TestCase {
  public function testBasic(){
    $conditionObj = new MonoloopCondition() ;
    $content = '<div style="display:inline;color:green;" isconditional="true" condition="if ( ( MonoloopProfile.VisitCount == \'33\' ) ){ | }" title="if ( ( MonoloopProfile.VisitCount == \'33\' ) ){ | }">data</div>' ;
    $content = $conditionObj->processContentInCondition($content) ;

    $this->assertEquals($content,'<%  if ( ( MonoloopProfile.VisitCount == \'33\' ) ){  %>  data  <% }  %>');
  }

  public function testBasicInside(){
    $conditionObj = new MonoloopCondition() ;
    $content = '<span class="header  ">Test <div style="display:inline;color:green;" isconditional="true" condition=" if (   ( MonoloopProfile.VisitSearchString == \'xxx\' )  ){|}">Marketplace</div> xxxxxx <div style="display:inline;color:green;" isconditional="true" condition=" if (   ( MonoloopProfile.VisitSearchString == \'yyy\' )  ){|}">Marketplace2</div>xxxxx</span>' ;
    $content = $conditionObj->processContentInCondition($content) ;
    $this->assertEquals($content,"<span class=\"header  \">Test   <%   if (   ( MonoloopProfile.VisitSearchString == 'xxx' )  ){   %>  Marketplace  <% }  %>   xxxxxx   <%   if (   ( MonoloopProfile.VisitSearchString == 'yyy' )  ){   %>  Marketplace2  <% }  %>  xxxxx</span>");
  }

  public function testMultiLevel(){
    $conditionObj = new MonoloopCondition() ;
    $content = '<span class="header  ">Test <div style="display:inline;color:green;" isconditional="true" condition=" if (   ( MonoloopProfile.VisitSearchString == \'xxx\' )  ){|}">aaaa <div style="display:inline;color:green;" isconditional="true" condition=" if (   ( MonoloopProfile.VisitSearchString == \'yyy\' )  ){|}">Marketplace2</div> bbbb</div> xxxxxx xxxxx</span>' ;
    $content = $conditionObj->processContentInCondition($content) ;
    $this->assertEquals($content,"<span class=\"header  \">Test   <%   if (   ( MonoloopProfile.VisitSearchString == 'xxx' )  ){   %>  aaaa   <%   if (   ( MonoloopProfile.VisitSearchString == 'yyy' )  ){   %>  Marketplace2  <% }  %>   bbbb  <% }  %>   xxxxxx xxxxx</span>");
  }

  #Nut 2016-02-16: deprecate we used another format.
  /*
  public function testBasic(){
    $conditionObj = new MonoloopCondition() ;
    $content = '<span class="header  ">Test <mlt style="color:blue;" condition=" if (   ( MonoloopProfile.VisitSearchString == \'xxx\' )  ){|}">Marketplace</mlt> xxxxxx <mlt condition=" if (   ( MonoloopProfile.VisitSearchString == \'yyy\' )  ){|}">Marketplace2</mlt>xxxxx</span>' ;
    $content = $conditionObj->processContentInCondition($content) ;
    $this->assertEquals($content,"<span class=\"header  \">Test   <%   if (   ( MonoloopProfile.VisitSearchString == 'xxx' )  ){   %>  Marketplace  <% }  %>   xxxxxx   <%   if (   ( MonoloopProfile.VisitSearchString == 'yyy' )  ){   %>  Marketplace2  <% }  %>  xxxxx</span>");

  }

  public function testMultiLevel(){
    $conditionObj = new MonoloopCondition() ;
    $content = '<span class="header  ">Test <mlt style="color:blue;" condition=" if (   ( MonoloopProfile.VisitSearchString == \'xxx\' )  ){|}">aaaa <mlt condition=" if (   ( MonoloopProfile.VisitSearchString == \'yyy\' )  ){|}">Marketplace2</mlt> bbbb</mlt> xxxxxx xxxxx</span>' ;
    $content = $conditionObj->processContentInCondition($content) ;
    $this->assertEquals($content,"<span class=\"header  \">Test   <%   if (   ( MonoloopProfile.VisitSearchString == 'xxx' )  ){   %>  aaaa   <%   if (   ( MonoloopProfile.VisitSearchString == 'yyy' )  ){   %>  Marketplace2  <% }  %>   bbbb  <% }  %>   xxxxxx xxxxx</span>");
  }
  */
}
