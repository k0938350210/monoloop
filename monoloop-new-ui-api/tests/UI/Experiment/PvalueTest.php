<?php

use App\Services\Experiment\PvalueCalculator ;

class PvalueTest extends TestCase {
	public function testPvalue(){
		$data = PvalueCalculator::p(700,150,600,100) ;
		$this->assertEquals($data,0.014)  ;
	}

}