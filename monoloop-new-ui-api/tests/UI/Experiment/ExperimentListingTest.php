<?php

use App\PageElement;
use App\User;
use App\Experiment;

class ExperimentListingTest extends TestCase {
  public function testListing(){
    $user = User::first();

    $response = $this->actingAs($user)->call('POST','api/experiments/create', [
      'nameExperiment' => 'test001',
      'descExperiment' => '',
      'segmentExperiment' => null,
      'goalExperiment' => null,
      'cgSizeExperiment' => 50,
      'cgDayExperiment' => 30,
      'significantActionExperiment' => 0,
      'page_element_ids' => null,
      'replicateGoal' => true,
      'replicateSegment' => true
    ]);

    $response = $this->actingAs($user)->call('POST','api/experiments/create', [
      'nameExperiment' => 'test002',
      'descExperiment' => '',
      'segmentExperiment' => null,
      'goalExperiment' => null,
      'cgSizeExperiment' => 50,
      'cgDayExperiment' => 30,
      'significantActionExperiment' => 0,
      'page_element_ids' => null,
      'replicateGoal' => true,
      'replicateSegment' => true
    ]);

    $content = json_decode($response->getContent(),true);

    $experiment = Experiment::find($content['content']['experiment']['_id']);
    $experiment->cid = 100;
    $experiment->save();

    $response = $this->actingAs($user)->call('GET','api/experiments/folder');
    $content = json_decode($response->getContent(),true);

    $this->assertEquals(2,count($content['data']));
    $this->assertEquals('test001',$content['data'][1]['value']);
  }
}