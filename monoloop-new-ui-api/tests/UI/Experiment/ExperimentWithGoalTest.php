<?php

use App\PageElement;
use App\User;

class ExperimentWithGoalTest extends TestCase {
	public function testGoalGenerateFromExperiment(){



		$user = User::first();
		// Create page
  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://someurl.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);

  	$this->assertEquals(200, $response->status());
  	$content1 = json_decode($response->getContent(),true);

  	$page_element_id = $content1['content']['_id'] ;

 		$response = $this->actingAs($user)->call('POST','/api/goals/create',[
  		'conditionGoal' => "",
  		'nameGoal' => 'base goal for experiment',
  		'page_element_id' => $page_element_id,
  		'pointsGoal' => 1,
  		'typeGoal' => 'page_related'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content3 = json_decode($response->getContent(),true);

  	$response = $this->actingAs($user)->call('POST', '/api/goals/update-status' ,[
    	'_id' =>   $content3['content']['goal']['_id'],
    	'action' => 'update_hidden',
    	'ajax' => true,
    	'hidden' => 0
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);
    $goal_uid = $content['content']['goal']['uid'];
 		$goal_id = $content['content']['goal']['_id'];

 		// Create segment;

 		$response = $this->actingAs($user)->call('POST','/api/segments/create', [
 			'nameSegment' => 'extra user',
 			'descSegment' => '',
 			'conditionSegment' => 'if ( ( MonoloopProfile.utm_medium === \'xxxx\' ) ){|}',
 		]);

 		$this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $segment_id = $content['content']['segment']['_id'];

    $total = PageElement::where('content.uid',$goal_uid)->count();
    $this->assertEquals(1,$total);


    // Create experiment;

    $response = $this->actingAs($user)->call('POST','api/experiments/create', [
    	'nameExperiment' => 'test',
    	'descExperiment' => '',
    	'segmentExperiment' => $segment_id,
    	'goalExperiment' => $goal_id,
    	'cgSizeExperiment' => 50,
    	'cgDayExperiment' => 30,
    	'significantActionExperiment' => 0,
    	'page_element_ids' => null,
    	'replicateGoal' => true,
    	'replicateSegment' => true
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);


    $experiment_id = $content['content']['experiment']['_id'];
    $new_segment_id = $content['content']['experiment']['segment_id'];
    $new_goal_id = $content['content']['experiment']['goal_id'];

    $this->assertTrue($new_segment_id !== $segment_id);
    $this->assertTrue($new_goal_id !== $goal_id);

    $response = $this->actingAs($user)->call('POST', '/api/experiments/update-status' ,[
    	'_id' =>   'experiment__'.$experiment_id ,
    	'action' => 'update_hidden',
    	'ajax' => true,
    	'hidden' => 0
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

 		$page_elements = PageElement::where('content.uid',$goal_uid)->where('fullURL','http://someurl.com/')->get();

 		$this->assertEquals(1,count($page_elements));

 		//print_r($page_elements->toArray());
	}
}