<?php
use App\User;
use App\Account;
use App\AccountUser;
use App\Services\Hasher;

use Carbon\Carbon;

class AccountUserValidTest extends TestCase {
  public function testInvalidAccess(){
    $account = Account::first();

    $user = factory(App\User::class)->make();
    $user->active_account = new MongoId($account->_id);
    $user->username = 'test_user2';
    $hasher = new Hasher();
    $user->pass = $hasher->getHashedPassword('test_password');
    $user->save();

    $accountUser = factory(App\AccountUser::class)->make();
    $accountUser->account_id = (string)$account->_id;
    $accountUser->user_id = (string)$user->_id;
    $accountUser->is_monoloop_admin = 1;
    $accountUser->role = 'admin';
    $accountUser->save();

    /// Should pass as normal
    $response = $this->actingAs($user)->call('GET','/api/profile?ajax=true');
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    /// Should fail
    $accountUser->hidden = true;
    $accountUser->save();

    $response = $this->actingAs($user)->call('GET','/api/profile?ajax=true');
    $this->assertEquals(401,$response->getStatusCode());

    $user = User::find($user->id);
    $this->assertEquals(null,$user->active_account);
  }

  public function testInvalidAccessShouldChangeToValidAccount(){
    $account = Account::first();

    $user = factory(App\User::class)->make();
    $user->active_account = new MongoId($account->_id);
    $user->username = 'test_user2';
    $hasher = new Hasher();
    $user->pass = $hasher->getHashedPassword('test_password');
    $user->save();

    $accountUser = factory(App\AccountUser::class)->make();
    $accountUser->account_id = (string)$account->_id;
    $accountUser->user_id = (string)$user->_id;
    $accountUser->is_monoloop_admin = 1;
    $accountUser->hidden = true;
    $accountUser->role = 'admin';
    $accountUser->save();

    $account2 = factory(App\Account::class)->make();
    $account2->uid = Account::max('uid') + 1 ;
    $account2->save();

    $accountUser = factory(App\AccountUser::class)->make();
    $accountUser->account_id = (string)$account2->_id;
    $accountUser->user_id = (string)$user->_id;
    $accountUser->is_monoloop_admin = 1;
    $accountUser->role = 'admin';
    $accountUser->save();

    $response = $this->actingAs($user)->call('GET','/api/profile?ajax=true');
    $this->assertEquals(200,$response->getStatusCode());

    $user = User::find($user->id);
    $this->assertEquals($account2->id,(string)$user->active_account);
  }

  public function testNoActiveAccountReturn401(){
    $account = Account::first();

    $user = factory(App\User::class)->make();
    $user->active_account = null;
    $user->username = 'test_user2';
    $hasher = new Hasher();
    $user->pass = $hasher->getHashedPassword('test_password');
    $user->save();

    $accountUser = factory(App\AccountUser::class)->make();
    $accountUser->account_id = (string)$account->_id;
    $accountUser->user_id = (string)$user->_id;
    $accountUser->is_monoloop_admin = 1;
    $accountUser->hidden = true;
    $accountUser->role = 'admin';
    $accountUser->save();

    $response = $this->actingAs($user)->call('GET','/api/profile?ajax=true');
    $this->assertEquals(401,$response->getStatusCode());
  }

  public function testNoActiveAccount2Return200(){
    $account = Account::first();

    $user = factory(App\User::class)->make();
    $user->active_account = null;
    $user->username = 'test_user2';
    $hasher = new Hasher();
    $user->pass = $hasher->getHashedPassword('test_password');
    $user->save();

    $accountUser = factory(App\AccountUser::class)->make();
    $accountUser->account_id = (string)$account->_id;
    $accountUser->user_id = (string)$user->_id;
    $accountUser->is_monoloop_admin = 1;
    $accountUser->hidden = 0;
    $accountUser->role = 'admin';
    $accountUser->save();

    $response = $this->actingAs($user)->call('GET','/api/profile?ajax=true');
    $this->assertEquals(200,$response->getStatusCode());
  }


  public function testValidExpiryDate(){
    $account = Account::first();

    $user = factory(App\User::class)->make();
    $user->username = 'test_user2';
    $user->active_account = new MongoId($account->_id);
    $hasher = new Hasher();
    $user->pass = $hasher->getHashedPassword('test_password');
    $user->save();

    $account_user = factory(App\AccountUser::class)->make();
    $account_user->account_id = (string)$account->_id;
    $account_user->user_id = (string)$user->_id;
    $account_user->is_monoloop_admin = 1;
    $account_user->hidden = false;
    $account_user->role = 'admin';
    $account_user->support_type = 'fix-until';
    $account_user->support_enddate = Carbon::now()->addDay();
    $account_user->save();

    $response = $this->actingAs($user)->call('GET','/api/profile?ajax=true');
    $this->assertEquals(200,$response->getStatusCode());
  }

  public function testInValidExpiryDate(){
    $account = Account::first();

    $user = factory(App\User::class)->make();
    $user->username = 'test_user2';
    $user->active_account = new MongoId($account->_id);
    $hasher = new Hasher();
    $user->pass = $hasher->getHashedPassword('test_password');
    $user->save();

    $account_user = factory(App\AccountUser::class)->make();
    $account_user->account_id = (string)$account->_id;
    $account_user->user_id = (string)$user->_id;
    $account_user->is_monoloop_admin = 1;
    $account_user->hidden = false;
    $account_user->role = 'admin';
    $account_user->support_type = 'fix-until';
    $account_user->support_enddate = Carbon::now()->subDay();
    $account_user->save();

    $response = $this->actingAs($user)->call('GET','/api/profile?ajax=true');
    $this->assertEquals(401,$response->getStatusCode());
  }

  public function testMustAutoDeActiveIfExpire(){
    $account = Account::first();

    $user = factory(App\User::class)->make();
    $user->username = 'test_user2';
    $user->active_account = new MongoId($account->_id);
    $hasher = new Hasher();
    $user->pass = $hasher->getHashedPassword('test_password');
    $user->save();

    $account_user = factory(App\AccountUser::class)->make();
    $account_user->account_id = (string)$account->_id;
    $account_user->user_id = (string)$user->_id;
    $account_user->is_monoloop_admin = 1;
    $account_user->hidden = false;
    $account_user->role = 'admin';
    $account_user->support_type = 'fix-until';
    $account_user->support_enddate = Carbon::now()->subDay();
    $account_user->save();

    $response = $this->actingAs($user)->call('GET','/api/profile?ajax=true');
    $this->assertEquals(401,$response->getStatusCode());

    $account_user = AccountUser::find($account_user->id);
    $this->assertEquals(1,$account_user->hidden);
  }
}