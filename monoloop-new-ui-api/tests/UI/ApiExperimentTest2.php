<?php

use App\User;
use App\Account;
use App\Experiment ;
use App\PageElement ;


class ApiExperimentTest2 extends TestCase {
  public function setUp()
  {
    parent::setUp();
  }

  public function testContentCreate(){
  	#Create 2 experiment should generate 2 pageElement record .
  	$response = $this->call('POST','/api/page-element/content-create',[
  		'condition' => "if ( ( MonoloopProfile.VisitCount == '12' ) ){ | }" ,
  		'content' => 'Content' ,
  		'contentType' => '' ,
  		'experiment_id' => 'undefined' ,
  		'fullURL' => 'http://test.com/A001' ,
  		'page_element_id' => '' ,
  		'source' => 'experiment' ,
  		'type' => 'variation' ,
  		'variation_name' => 'v1' ,
  		'xpath' => '#my-path'
  	]);
  	$this->assertEquals(200, $response->status());
  	$response = $this->call('POST','/api/page-element/content-create',[
  		'condition' => "if ( ( MonoloopProfile.VisitCount == '12' ) ){ | }" ,
  		'content' => 'Content' ,
  		'contentType' => '' ,
  		'experiment_id' => 'undefined' ,
  		'fullURL' => 'http://test.com/A001' ,
  		'page_element_id' => '' ,
  		'source' => 'experiment' ,
  		'type' => 'variation' ,
  		'variation_name' => 'v1' ,
  		'xpath' => '#my-path'
  	]);
  	$content = json_decode($response->getContent(),true) ;
  	$count =PageElement::where('urlhash','7cb84b8553d53f038d19c406')->count();
  	$this->assertEquals(1,$count);
  }
 }
