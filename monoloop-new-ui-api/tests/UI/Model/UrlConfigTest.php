<?php

use App\User;
use App\UrlConfig;

class UrlConfigTest extends TestCase {

  public function testA(){
    $urlConfig = new UrlConfig() ;
    $urlConfig->url = 'http://monoloop.com' ;
    $this->assertEquals($urlConfig->cleanUrl,'http://monoloop.com/');
  }

  public function testB(){
    $urlConfig = new UrlConfig() ;
    $urlConfig->url = 'https://monoloop.com' ;
    $this->assertEquals($urlConfig->cleanUrl,'https://monoloop.com/');
  }

  public function testC(){
    $urlConfig = new UrlConfig() ;
    $urlConfig->url = 'http://www.monoloop.com' ;
    $this->assertEquals($urlConfig->cleanUrl,'http://monoloop.com/');
  }

  public function testD(){
    $urlConfig = new UrlConfig() ;
    $urlConfig->url = 'http://www.monoloop.com?para1=a' ;
    $this->assertEquals($urlConfig->cleanUrl,'http://monoloop.com?para1=a');
  }

  public function testE(){
    $urlConfig = new UrlConfig() ;
    $urlConfig->url = 'HTTP://www.monoloop.com?para1=a&para=b' ;
    $this->assertEquals($urlConfig->cleanUrl,'http://monoloop.com?para1=a&para=b');
  }
}
