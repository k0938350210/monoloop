<?php

use App\User;
use App\Account;

class DomainTest extends TestCase {

	public function testFirstDomain()
	{
    $account = factory(App\Account::class)->create();
    $domain = factory(App\AccountDomain::class)->make();
    $domainName = $domain->domain ;
    $account->domains()->save($domain);
    $this->assertEquals($domainName,$account->firstDomain);
	}

	public function testDomainListingApi(){
		$user = User::first() ;
    $response = $this->actingAs($user)->call('GET', '/api/domain?ajax=true');
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);
    $this->assertEquals(3,count($content['topics']));
	}

}
