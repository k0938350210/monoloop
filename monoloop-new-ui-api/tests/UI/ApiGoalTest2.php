<?php

use App\User;
use App\Account;
use App\Goal;
use App\Folder ;
use App\PageElement ;



class ApiGoalTest2 extends TestCase {
  public function setUp()
  {
    parent::setUp();
  }

  public function testCreateGoalFlow(){
  	#/component/page-list-publish/store

  	$response = $this->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://someurl.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content1 = json_decode($response->getContent(),true) ;

  	$response = $this->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://someurl.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content2 = json_decode($response->getContent(),true) ;

  	$this->assertEquals($content1['content']['_id'],$content2['content']['_id']);
  	$page_element_id = $content2['content']['_id'];

  	#/api/goals/create
  	$response = $this->call('POST','/api/goals/create',[
  		'conditionGoal' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){ | }" ,
  		'nameGoal' => 'New UI 02' ,
  		'page_element_id' => $page_element_id ,
  		'pointsGoal' => 1 ,
  		'typeGoal' => 'page_related'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content3 = json_decode($response->getContent(),true) ;

    #create with success goal
  	$this->assertTrue(array_key_exists('_id',$content3['content']['goal'] ));
  	$content_uid = $content3['content']['goal']['uid'] ;
  	$goal_id = $content3['content']['goal']['_id'] ;
  	#This content uid must exists in pageElements.content.uid

    #goal must hidden page element as default;
    $goal = Goal::find($goal_id);
    $this->assertEquals(1,$goal->hidden);

    #goal as hidden should not generate pageelement record
  	$pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
  	$this->assertTrue(!(bool)$pageElement);

    #goal update status
    $response = $this->call('POST','/api/goals/update-status',[
      '_id' => 'goal__' . $goal_id ,
      'action' => 'update_hidden',
      'hidden' => 0
    ]);
    $content4 = json_decode($response->getContent(),true) ;
    #goal status should visible
    $goal = Goal::find($goal_id);
    $this->assertEquals(0,$goal->hidden);

    #goal as visible should not generate pageelement record
    $pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
    $this->assertTrue((bool)$pageElement);

  	#/api/goals/delete
  	$response = $this->call('POST','/api/goals/delete',[
  		'_id' => 'goal__' . $goal_id
  	]);

  	$this->assertEquals(200, $response->status());
  	$pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
  	$this->assertTrue(!(bool)$pageElement);
  }

  public function testUpdateStatusFlow(){

  	$response = $this->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://someurl.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content1 = json_decode($response->getContent(),true) ;
  	$page_element_id = $content1['content']['_id'];

  	#/api/goals/create
  	$response = $this->call('POST','/api/goals/create',[
  		'conditionGoal' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){ | }" ,
  		'nameGoal' => 'New UI 02' ,
  		'page_element_id' => $page_element_id ,
  		'pointsGoal' => 1 ,
  		'typeGoal' => 'page_related'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content3 = json_decode($response->getContent(),true) ;
  	$content_uid = $content3['content']['goal']['uid'] ;
  	$goal_id = $content3['content']['goal']['_id'] ;

  	$pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
  	$this->assertTrue(!(bool)$pageElement);

  	#/api/goals/update-status
  	$response = $this->call('POST','/api/goals/update-status',[
  		'_id' => 'goal__' . $goal_id ,
  		'hidden' => 0
  	]);

  	$this->assertEquals(200, $response->status());
  	$pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
  	$this->assertTrue((bool)$pageElement);
  }

  public function testConditionRelatedGoal(){
    $response = $this->call('POST','/api/goals/create',[
      'conditionGoal' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){ | }" ,
      'nameGoal' => 'New UI 01' ,
      'pointsGoal' => 1 ,
      'typeGoal' => 'condition_related'
    ]);
    $this->assertEquals(200, $response->status());
    $content3 = json_decode($response->getContent(),true) ;
    $content_uid = $content3['content']['goal']['uid'] ;

    #update to visible
    $response = $this->call('POST','/api/goals/update-status',[
      '_id' => 'goal__' . $content3['content']['goal']['_id'] ,
      'hidden' => 0
    ]);

    $pageElement = PageElement::where('content.uid',$content_uid)->first() ;

    $this->assertTrue(!is_null($pageElement));
  }

 }