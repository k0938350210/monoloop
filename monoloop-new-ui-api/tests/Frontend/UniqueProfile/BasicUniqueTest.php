<?php

use App\User;
use App\Account;
use App\Profile;
use App\Services\Frontend\BatchResponse ;

class BasicUniqueTest extends TestCase {
  public function testBasicWithSignout(){
    $cid = Account::first()->uid;

    // View page with unquieID 1001
    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&uniqueID=1001&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=&fla=1&java=1&pdf=1',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    // Revisit same page with same uniqueID and gmid
    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uniqueID=1001&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    $profile = Profile::where('uniqueID','1001')->first();
    $this->assertTrue(!is_null($profile));

    // Signout with blank unquieID and same gmid
    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uniqueID=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    // Search profile from uniqueID 1001 it gon now.
    $profile = Profile::where('uniqueID','1001')->first();
    $this->assertTrue(!is_null($profile));
  }

  public function testShouldNotCreateDoubleProfileWithSameUniqueID(){
    $cid = Account::first()->uid;

    // New visit
    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&uniqueID=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=&fla=1&java=1&pdf=1',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    // Same visit and include uniqueID
    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uniqueID=1001&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
    $batchResponse = new BatchResponse($data);


    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&uniqueID=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=&fla=1&java=1&pdf=1',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uniqueID=1001&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    $this->assertEquals(1,Profile::where('uniqueID','1001')->count());
  }
}
