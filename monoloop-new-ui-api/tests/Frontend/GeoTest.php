<?php

use App\User;
use App\Account;
use App\Profile;
use App\Services\Frontend\BatchResponse ;

class GeoTest extends TestCase {

	public function testBasic(){
		$user = User::first() ;
		$this->clearCache('profilelist') ;


   	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test002.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

   	$batchResponse = new BatchResponse($data);
   	$profile = Profile::find($batchResponse->gmid) ;
   	$visit = $profile['profiles'][0]['visits'][0] ;

   	$this->assertTrue(array_key_exists('loc_info', $visit ));
   	$this->assertEquals($visit['loc_info']['loc_continent'],'AS') ;
 		$this->assertEquals($visit['loc_info']['loc_city'],'Bangkok') ;
		$this->assertEquals($visit['loc_info']['loc_country'],'TH') ;
		$this->assertEquals($visit['loc_info']['loc_lat'],'13.7500') ;
		$this->assertEquals($visit['loc_info']['loc_ip'],'171.96.222.235') ;
		$this->assertEquals($visit['loc_info']['loc_long'],'100.4667') ;
	}


	public function testCityCondition(){
		$data = $this->createSimplePage() ;
    $user = User::first() ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
      'condition' => "" ,
      'content' => '<%= MonoloopProfile.City %>' ,
      'contentType' => '' ,
      'content_id' => $data['content_id'] ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => $data['page_element_id'] ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);

    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status',[
      '_id' => 'webpagelist__' . $data['page_element_id']  ,
      'hidden' => 0
    ]);

    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    $batchResponse = new BatchResponse($data);


    $this->assertEquals( $batchResponse->data[0]['content'] ,'Bangkok');
	}

	public function testCountryCondition(){
		$data = $this->createSimplePage() ;
    $user = User::first() ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
      'condition' => "" ,
      'content' => '<%= MonoloopProfile.Country %>' ,
      'contentType' => '' ,
      'content_id' => $data['content_id'] ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => $data['page_element_id'] ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);

    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status',[
      '_id' => 'webpagelist__' . $data['page_element_id']  ,
      'hidden' => 0
    ]);

    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);


    $this->assertEquals( $batchResponse->data[0]['content'] ,'th');
	}

  public function testWithinArea(){
    $data = $this->createSimplePage() ;
    $user = User::first() ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
      'condition' => "if ( ( MonoloopProfile.isWithinArea('100.4667', '13.75', '10', '') === true ) ){ | }" ,
      'content' => 'content' ,
      'contentType' => '' ,
      'content_id' => $data['content_id'] ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => $data['page_element_id'] ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);

    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status',[
      '_id' => 'webpagelist__' . $data['page_element_id']  ,
      'hidden' => 0
    ]);


    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);


    $this->assertEquals( $batchResponse->data[0]['content'] ,'content');
  }

	private function createSimplePage(){
    $user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => "" ,
      'content' => 'my content' ,
      'contentType' => '' ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => '' ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);
    $content = json_decode($response->getContent(),true) ;
    $content_id = $content['content']['_id'] ;
    $page_element_id = $content['pageElement']['_id'];

    $response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
      'experiment_id' => null ,
      'fullURLWebPageList' => 'http://url_test001.com'  ,
      'includeHttpHttpsWebPageElement' => 0 ,
      'includeWWWWebPageElement' => 0 ,
      'page_element_id' => $page_element_id ,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageLust' => '' ,
      'urlOptionWebPageElement' => 'exactMatch'
    ]);

    return ['content_id' => $content_id , 'page_element_id' => $page_element_id] ;
  }
}