<?php

use App\PageElement ;
use App\User ;
use App\Services\Frontend\BatchResponse ;
use App\ReportingArchiveAggregated ;


class GoalAmountTest extends TestCase {

	public function dataConditionRelateGoal001(){
		return [
			'ajax' => true ,
			'conditionGoal' => "if ( ( MonoloopProfile.PageViewCount == '1' ) ){|}" ,
			'nameGoal' => 'Test00123' ,
			'pointsGoal' => 1 ,
			'typeGoal' => 'condition_related'
		];
	}



	public function testConditionRelate(){
    $user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;
    $response = $this->actingAs($user)->call('POST', '/api/goals/create' ,$this->dataConditionRelateGoal001());

    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $goal = $content['content']['goal'];


    $page_element_id = $goal['page_element_id'] ;

    $page_element = PageElement::find($page_element_id);
    $goal_id = $goal['_id'] ;
    $goal_uid = $goal['uid'] ;

    $response = $this->actingAs($user)->call('POST', '/api/goals/update-status' ,[
    	'_id' =>   $goal_id ,
    	'action' => 'update_hidden' ,
    	'ajax' => true ,
    	'hidden' => 0
    ]);

    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com?data1&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    $batchResponse = new BatchResponse($data);

    sleep(2);

    $report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
    $this->assertEquals(1,$report->goals[$goal_uid]['amount']) ;

    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		$batchResponse = new BatchResponse($data);

 		sleep(2);

    $report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
    $this->assertEquals(1,$report->goals[$goal_uid]['amount']) ;

	}

}