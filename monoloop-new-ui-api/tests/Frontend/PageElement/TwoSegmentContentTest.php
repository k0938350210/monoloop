<?php

use App\PageElement;
use App\User;

use App\Services\Frontend\BatchResponse ;

class TwoSegmentContentTest extends TestCase {
  public function testFlow(){
    $response = $this->call('GET','/api/content/web?ajax=true');
    $content = json_decode($response->getContent(),true);
    $folder_id = $content[0]['id'];

    //create segment01
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( MonoloopProfile.VisitCount > \'0\' ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test1',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment1 = json_decode($response->getContent(),true);
    //enable segment01
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment1['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    //create segment02
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( MonoloopProfile.VisitCount > \'0\' ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test2',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment2 = json_decode($response->getContent(),true);
    //enable segment02
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment2['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    //check segment result
    $response  = $this->call('POST','/api/segments/folder?ajax=true',[
      'action' => 'files',
      'source' => $folder_id
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $segments = json_decode($response->getContent(),true);
    $this->assertEquals(3,count($segments['data']));
    //create content in pageelement
    $response = $this->call('POST','/api/page-element/content-create',[
      'condition' => 'if ( (  ( Segments.segment_'.$segment1['content']['segment']['uid'].'() === true )  ||  (  ( Segments.segment_'.$segment2['content']['segment']['uid'].'() === true )  )  ) ){|}',
      'content' => 'new content',
      'contentType' => '',
      'experiment_id' => '',
      'fullURL' => 'http://local-demo1wp.com/product/demo3/',
      'page_element_id' => '',
      'source' => 'default',
      'type' => 'variation',
      'variation_name' => 'my name is data',
      'xpath' => 'DIV#product-91 P:eq(1)'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);
    //save page element
    $response = $this->call('POST','/component/page-list-publish/store',[
      'currentFolder' => $folder_id,
      'experiment_id' => '',
      'fullURLWebPageList' => 'http://local-demo1wp.com/product/demo3/',
      'includeHttpHttpsWebPageElement' => 0,
      'includeWWWWebPageElement' => 0,
      'isExistingPageELementID' => false,
      'page_element_id' => $content['pageElement']['_id'],
      'regularExpressionWebPageElement' => '',
      'remarkWebPageList' => '',
      'source' => 'default',
      'urlOptionWebPageElement' => 'exactMatch'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $this->call('POST','/api/content/web/update-status?ajax=true',[
      '_id' => 'webpagelist__'.$content['pageElement']['_id'],
      'action' => 'update_hidden',
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    $page_element = PageElement::find($content['pageElement']['_id']);
    $this->assertEquals(2,count($page_element->content[0]['segments']));


    //fontend visit;

    $user = User::first() ;
    $this->clearCache('profilelist') ;
    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);
    $this->assertEquals(1,count($batchResponse->data));
  }
}