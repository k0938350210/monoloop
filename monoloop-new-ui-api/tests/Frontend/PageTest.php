<?php

use App\User;
use App\Account;
use App\Profile;
use App\PageElement ;
use App\Content ;
use App\Services\Frontend\BatchResponse ;

class PageTest extends TestCase {

	public function testBasicPage(){


		$user = User::first() ;
		$this->clearCache($user->account->uid . '_pageelements') ;


		$response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => '' ,
  		'content' => 'my content' ,
  		'contentType' => '' ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => '' ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
  	]);
		$this->assertEquals(200,$response->getStatusCode());
  	$content = json_decode($response->getContent(),true) ;
  	$page_element_id = $content['pageElement']['_id'];


  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id' => null ,
  		'fullURLWebPageList' => 'http://url_test001.com'  ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => $page_element_id ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageLust' => '' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
  	$this->assertEquals(200,$response->getStatusCode());
  	$content = json_decode($response->getContent(),true) ;

    #need active
    $page_element_id = $content['content']['_id'] ;

    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status?ajax=true',[
      '_id' => $page_element_id ,
      'action' => 'update_hidden' ,
      'hidden' => 0
    ]);


 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
 		$this->assertEquals(count($batchResponse->data),1);
 		$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');


 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
 		$this->assertEquals(count($batchResponse->data),0);
  }

  public function testStartWithPage(){
  	$user = User::first() ;
  	$this->clearCache($user->account->uid . '_pageelements') ;


		$response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => '' ,
  		'content' => 'my content' ,
  		'contentType' => '' ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => '' ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
  	]);
		$this->assertEquals(200,$response->getStatusCode());
  	$content = json_decode($response->getContent(),true) ;
  	$page_element_id = $content['pageElement']['_id'];


  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id' => null ,
  		'fullURLWebPageList' => 'http://url_test001.com'  ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
      'isExistingPageELementID' => true ,
  		'page_element_id' => $page_element_id ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageLust' => '' ,
      'source' => 'default' ,
  		'urlOptionWebPageElement' => 'startsWith'
  	]);
  	$this->assertEquals(200,$response->getStatusCode());
  	$content = json_decode($response->getContent(),true) ;

    $page_element_id = $content['content']['_id'] ;

    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status?ajax=true',[
      '_id' => $page_element_id ,
      'action' => 'update_hidden' ,
      'hidden' => 0
    ]);


  	$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
 		$this->assertEquals(count($batchResponse->data),1);
 		$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');

 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com/moreurl?d&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
 		$this->assertEquals(count($batchResponse->data),1);
 		$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');
  }
  /*
  public function testAllowParams(){
    $user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;


    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => '' ,
      'content' => 'my content' ,
      'contentType' => '' ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => '' ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $page_element_id = $content['pageElement']['_id'];


    $response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
      'experiment_id' => null ,
      'fullURLWebPageList' => 'http://url_test001.com'  ,
      'includeHttpHttpsWebPageElement' => true ,
      'includeWWWWebPageElement' => 0 ,
      'isExistingPageELementID' => true ,
      'page_element_id' => $page_element_id ,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageLust' => '' ,
      'source' => 'default' ,
      'urlOptionWebPageElement' => 'allowParams'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $url = urlencode('http://url_test001.com/?data=1') ;

    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url='.$url.'&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    $batchResponse = new BatchResponse($data);
    $this->assertEquals(count($batchResponse->data),1);
    $this->assertEquals( $batchResponse->data[0]['content'] ,'my content');
  }
  */

  public function testHttps(){
    $user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;


    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => '' ,
      'content' => 'my content' ,
      'contentType' => '' ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => '' ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $page_element_id = $content['pageElement']['_id'];


    $response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
      'experiment_id' => null ,
      'fullURLWebPageList' => 'http://url_test001.com'  ,
      'includeHttpHttpsWebPageElement' => true ,
      'includeWWWWebPageElement' => 0 ,
      'isExistingPageELementID' => true ,
      'page_element_id' => $page_element_id ,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageLust' => '' ,
      'source' => 'default' ,
      'urlOptionWebPageElement' => 'exactMatch'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $page_element_id = $content['content']['_id'] ;

    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status?ajax=true',[
      '_id' => $page_element_id ,
      'action' => 'update_hidden' ,
      'hidden' => 0
    ]);

    $url = urlencode('http://url_test001.com/') ;

    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url='.$url.'&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    $batchResponse = new BatchResponse($data);
    $this->assertEquals(count($batchResponse->data),1);
    $this->assertEquals( $batchResponse->data[0]['content'] ,'my content');

    $url = urlencode('https://url_test001.com/') ;

    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url='.$url.'&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    $batchResponse = new BatchResponse($data);
    $this->assertEquals(count($batchResponse->data),1);
    $this->assertEquals( $batchResponse->data[0]['content'] ,'my content');
  }

  public function testUpdateStatus(){

    $user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;


    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => '' ,
      'content' => 'my content' ,
      'contentType' => '' ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => '' ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $page_element_id = $content['pageElement']['_id'];


    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status',[
      '_id' => 'webpagelist__' . $page_element_id  ,
      'action' => 'update_hidden' ,
      'hidden' => '0'
    ]);

    $url = urlencode('http://url_test001.com/') ;

    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url='.$url.'&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    $batchResponse = new BatchResponse($data);
    $this->assertEquals(count($batchResponse->data),1);
    $this->assertEquals( $batchResponse->data[0]['content'] ,'my content');
  }

  public function testBasicCondition(){
  	$user = User::first() ;
  	$this->clearCache($user->account->uid . '_pageelements') ;

  	#### VISIT COUNT TEST #####

  	$response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){ | }" ,
  		'content' => 'my content' ,
  		'contentType' => '' ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => '' ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
  	]);
  	$content = json_decode($response->getContent(),true) ;
  	$content_id = $content['content']['_id'] ;
  	$page_element_id = $content['pageElement']['_id'];



  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id' => null ,
  		'fullURLWebPageList' => 'http://url_test001.com'  ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => $page_element_id ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageLust' => '' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);

    $content = json_decode($response->getContent(),true) ;
    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status?ajax=true',[
      '_id' => $page_element_id ,
      'action' => 'update_hidden' ,
      'hidden' => 0
    ]);

  	$pageElement = PageElement::find($page_element_id);
  	$this->assertEquals($pageElement->content()->first()->code,"<% if ( ( MonoloopProfile.VisitCount == '1' ) ){ %>my content<% } %>");


  	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
  	$batchResponse = new BatchResponse($data);
  	$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');

  	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
  	$batchResponse = new BatchResponse($data);
 		$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');

  	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());
  	$batchResponse = new BatchResponse($data);
 		$this->assertEquals(count($batchResponse->data),0);


 		#### FIRST VISIT ####
 		$response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
 			'condition' => "if ( ( MonoloopProfile.FirstVisit === true ) ){ | }" ,
  		'content' => 'my content' ,
  		'contentType' => '' ,
  		'content_id' => $content_id ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => $page_element_id ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
 		]);
 		$this->clearCache($user->account->uid . '_pageelements') ;

 		for($i = 0 ; $i < 5 ; $i ++ ){
		 	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
	  	$batchResponse = new BatchResponse($data);
	  	$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');
	  }

	  $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
  	$batchResponse = new BatchResponse($data);
  	$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');

  	### TEST VISIT COUNTT ###

  	$response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
 			'condition' => "" ,
  		'content' => '<%= MonoloopProfile.CurrentPageViewCount %>' ,
  		'contentType' => '' ,
  		'content_id' => $content_id ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => $page_element_id ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
 		]);


 		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
 		$batchResponse = new BatchResponse($data);
  	$this->clearCache($user->account->uid . '_pageelements') ;
  	$content = json_decode($response->getContent(),true) ;
  	for($i = 0 ; $i < 20 ; $i ++ ){
		 	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=' . $batchResponse->vid,false,$this->commonContext());
		 	$batchResponse = new BatchResponse($data);

	  	$this->assertEquals( (int)$batchResponse->data[0]['content'] ,$i+2  );
	  }

  	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=' ,false,$this->commonContext());
	 	$batchResponse = new BatchResponse($data);

  	$this->assertEquals( (int)$batchResponse->data[0]['content'] , 1 );

  	#### DIRECT VISITOR ####
  	$response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
 			'condition' => "if ( ( MonoloopProfile.DirectVisitor === true ) ){ | }" ,
  		'content' => 'my content' ,
  		'contentType' => '' ,
  		'content_id' => $content_id ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => $page_element_id ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
 		]);
 		$this->clearCache($user->account->uid . '_pageelements') ;
 		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
  	$batchResponse = new BatchResponse($data);
  	$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');

 		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
  	$batchResponse = new BatchResponse($data);
  	$this->assertEquals(count($batchResponse->data),0);

  	### VISIT REFERER ###
  	$response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
 			'condition' => "if ( ( ml_contains(MonoloopProfile.VisitReferer, 'google' ) ) ){ | }" ,
  		'content' => 'my content' ,
  		'contentType' => '' ,
  		'content_id' => $content_id ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => $page_element_id ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
 		]);
 		$this->clearCache($user->account->uid . '_pageelements') ;
 		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
  	$batchResponse = new BatchResponse($data);
  	$this->assertEquals( $batchResponse->data[0]['content'] ,'my content');

  }

  public function testConditionEntryPage(){
    $data = $this->createSimplePage() ;
    $user = User::first() ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
      'condition' => "" ,
      'content' => '<%= MonoloopProfile.EntryPage %>' ,
      'contentType' => '' ,
      'content_id' => $data['content_id'] ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => $data['page_element_id'] ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);

    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);
    $this->assertEquals( $batchResponse->data[0]['content'] ,'http://url_test001.com/');

  }

  public function testConditionVisitDuration(){
    $data = $this->createSimplePage() ;
    $user = User::first() ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
      'condition' => "" ,
      'content' => '<%= MonoloopProfile.VisitDuration %>' ,
      'contentType' => '' ,
      'content_id' => $data['content_id'] ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => $data['page_element_id'] ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);

    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);
    sleep(1);
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=' . $batchResponse->vid,false,$this->commonContext());

    $this->assertTrue( $batchResponse->data[0]['content'] < 3);
  }

  public function testConditionCurrentURL(){
    $data = $this->createSimplePage() ;
    $user = User::first() ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
      'condition' => "" ,
      'content' => '<%= MonoloopProfile.CurrentURL %>' ,
      'contentType' => '' ,
      'content_id' => $data['content_id'] ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => $data['page_element_id'] ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);

    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    $this->assertEquals( $batchResponse->data[0]['content'] ,'http://url_test001.com/');
  }

  public function testConditionLastEntryPage(){
    $data = $this->createSimplePage() ;
    $user = User::first() ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
      'condition' => "" ,
      'content' => '<%= MonoloopProfile.LastEntryPage %>' ,
      'contentType' => '' ,
      'content_id' => $data['content_id'] ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => $data['page_element_id'] ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);

    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test002.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=' . $batchResponse->vid,false,$this->commonContext());
    $batchResponse = new BatchResponse($data);


    $this->assertEquals( $batchResponse->data[0]['content'] ,'http://url_test002.com/');
  }

  public function testConditionLastExitPage(){
    $data = $this->createSimplePage() ;
    $user = User::first() ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-update',[
      'condition' => "" ,
      'content' => '<%= MonoloopProfile.LastExitPage %>' ,
      'contentType' => '' ,
      'content_id' => $data['content_id'] ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => $data['page_element_id'] ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);

    $this->clearCache($user->account->uid . '_pageelements') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);


    $this->assertEquals( $batchResponse->data[0]['content'] ,'http://url_test001.com/');
  }

  public function testDeleteContent(){
    $user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;


    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => '' ,
      'content' => 'my content' ,
      'contentType' => '' ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => '' ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $page_element_id = $content['pageElement']['_id'];


    $response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
      'experiment_id' => null ,
      'fullURLWebPageList' => 'http://url_test001.com'  ,
      'includeHttpHttpsWebPageElement' => 0 ,
      'includeWWWWebPageElement' => 0 ,
      'page_element_id' => $page_element_id ,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageLust' => '' ,
      'urlOptionWebPageElement' => 'exactMatch'
    ]);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $content_uid = $content['content']['content'][0]['uid'] ;

    $content = Content::byUid($content_uid)->first() ;

    $response = $this->actingAs($user)->call('POST','/api/component/content-list/delete',[
      'content_id' => $content->id ,
      'deleted' => 1
    ]);

    $content = Content::byUid($content_uid)->first() ;

    $data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    $batchResponse = new BatchResponse($data);
    $this->assertEquals(count($batchResponse->data),0);
  }

  private function createSimplePage(){
    $user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => "" ,
      'content' => 'my content' ,
      'contentType' => '' ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com' ,
      'page_element_id' => '' ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);
    $content = json_decode($response->getContent(),true) ;
    $content_id = $content['content']['_id'] ;
    $page_element_id = $content['pageElement']['_id'];

    $response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
      'experiment_id' => null ,
      'fullURLWebPageList' => 'http://url_test001.com'  ,
      'includeHttpHttpsWebPageElement' => 0 ,
      'includeWWWWebPageElement' => 0 ,
      'page_element_id' => $page_element_id ,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageLust' => '' ,
      'urlOptionWebPageElement' => 'exactMatch'
    ]);

    $content = json_decode($response->getContent(),true) ;

    $response = $this->actingAs($user)->call('POST','/api/content/web/update-status?ajax=true',[
      '_id' => $page_element_id ,
      'action' => 'update_hidden' ,
      'hidden' => 0
    ]);

    return ['content_id' => $content_id , 'page_element_id' => $page_element_id] ;
  }



}