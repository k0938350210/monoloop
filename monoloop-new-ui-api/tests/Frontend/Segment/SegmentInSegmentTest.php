<?php

use App\PageElement;
use App\User;

use App\Services\Frontend\BatchResponse ;

class SegmentInSegmentTest extends TestCase{
	public function testFlow(){
		$response = $this->call('GET','/api/content/web?ajax=true');
    $content = json_decode($response->getContent(),true);
    $folder_id = $content[0]['id'];

    //create segment01
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( MonoloopProfile.VisitCount > \'0\' ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test1',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment1 = json_decode($response->getContent(),true);
    //enable segment01
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment1['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    //create segment02
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( Segments.segment_'.$segment1['content']['segment']['uid'].'() === true ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test2',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment2 = json_decode($response->getContent(),true);
    //enable segment02
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment2['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    //hidden first one
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment1['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    $user = User::first() ;
    $this->clearCache('profilelist') ;
    $this->clearCache($user->account->uid . '_pageelements') ;
    $this->clearCache($user->account->uid . '_segments') ;
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

	}
}