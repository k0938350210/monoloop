<?php

use App\PageElement;
use App\User;
use App\Profile;
use App\ReportingArchiveAggregated;
use App\Services\Frontend\BatchResponse;

class SegmentFlowTest extends TestCase{
	public function testFlow1(){
		$response = $this->call('GET','/api/content/web?ajax=true');
    $content = json_decode($response->getContent(),true);
    $folder_id = $content[0]['id'];

    //create segment01
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( MonoloopProfile.PageViewCount == \'1\' ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test1',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment1 = json_decode($response->getContent(),true);
    //enable segment01
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment1['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    //create segment02
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( MonoloopProfile.PageViewCount == \'2\' ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test2',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment2 = json_decode($response->getContent(),true);
    //enable segment02
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment2['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    //create segment03
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( MonoloopProfile.PageViewCount == \'3\' ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test3',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment3 = json_decode($response->getContent(),true);
    //enable segment03
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment3['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    //create segment04
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( MonoloopProfile.PageViewCount == \'4\' ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test4',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment4 = json_decode($response->getContent(),true);
    //enable segment03
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment4['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    $user = User::first() ;
    $this->clearCache('profilelist') ;
    $this->clearCache($user->account->uid . '_pageelements') ;
    $this->clearCache($user->account->uid . '_segments') ;

    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    sleep(1);
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());


    sleep(1);
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());

    sleep(1);
    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
    sleep(1);
    $profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];

 		$this->assertEquals(4,count($p['visits'][0]['segments_InOut']));

 		$report = ReportingArchiveAggregated::first();
 		$this->assertEquals(4,count($report['segments']));
 		foreach($report['segments'] as $segment){
 			$this->assertEquals(1,$segment['count']);
 		}
	}

	public function testFlow2(){
		$response = $this->call('GET','/api/content/web?ajax=true');
    $content = json_decode($response->getContent(),true);
    $folder_id = $content[0]['id'];

    //create segment01
    $response = $this->call('POST','/api/segments/create',[
      'ajax' => true,
      'conditionSegment' => 'if ( ( MonoloopProfile.PageViewCount >= \'1\' ) ){|}',
      'descSegment' => '',
      'nameSegment' => 'test1',
      'source' => $folder_id
    ]);

    $this->assertEquals(200,$response->getStatusCode());
    $segment1 = json_decode($response->getContent(),true);
    //enable segment01
    $response = $this->call('POST','/api/segments/update-status',[
      '_id' => 'segment__' . $segment1['content']['segment']['_id'],
      'action' => 'update_hidden',
      'ajax' => true,
      'hidden' => 0
    ]);
    $this->assertEquals(200,$response->getStatusCode());

    $user = User::first() ;
    $this->clearCache('profilelist') ;
    $this->clearCache($user->account->uid . '_pageelements') ;
    $this->clearCache($user->account->uid . '_segments') ;

    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    $batchResponse = new BatchResponse($data);

    sleep(1);


    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());

    sleep(1);

    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://local-demo1wp.com/product/demo3/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    sleep(1);

 		$report = ReportingArchiveAggregated::first();
 		$this->assertEquals(2,$report['segments'][2]['count']);
	}
}