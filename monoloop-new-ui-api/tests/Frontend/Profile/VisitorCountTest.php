<?php

use App\Services\Frontend\BatchResponse ;
use App\ReportingArchiveAggregated ;

use App\PageElement ;
use App\User ;

class VisitorCountTest extends TestCase {
	public function testCompareWithReport(){
		$user = User::first() ;
		$this->clearCache($user->account->uid . '_pageelements') ;


		$response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => '' ,
  		'content' => '<%= MonoloopProfile.TotalVisitors() %>' ,
  		'contentType' => '' ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => '' ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
  	]);
		$this->assertEquals(200,$response->getStatusCode());
  	$content = json_decode($response->getContent(),true) ;
  	$page_element_id = $content['pageElement']['_id'];


  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id' => null ,
  		'fullURLWebPageList' => 'http://url_test001.com'  ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => $page_element_id ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageLust' => '' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
  	$this->assertEquals(200,$response->getStatusCode());
  	$content = json_decode($response->getContent(),true) ;

 		$page_element_id = $content['content']['_id'] ;

 		$response = $this->actingAs($user)->call('POST','/api/content/web/update-status?ajax=true',[
 			'_id' => $page_element_id ,
 			'action' => 'update_hidden' ,
 			'hidden' => 0
 		]);

 		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
	 	$this->assertEquals($batchResponse->data[0]['content'] ,'1');

 		sleep(2);


 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
 		sleep(2);

 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
 		sleep(2);

 		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
    $this->assertEquals($batchResponse->data[0]['content'],$report->totalVisitors) ;
	}

}