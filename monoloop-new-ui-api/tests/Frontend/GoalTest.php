<?php

use App\User;
use App\Account;
use App\Profile;
use App\Services\Frontend\BatchResponse ;

class GoalTest extends TestCase {
	public function testGoalDirectMatch(){

		$user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;


  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://url_test001.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
    $this->assertEquals(200,$response->getStatusCode());
  	$content1 = json_decode($response->getContent(),true) ;

  	$page_element_id = $content1['content']['_id'];

  	$response = $this->actingAs($user)->call('POST','/api/goals/create',[
  		'conditionGoal' => "" ,
  		'nameGoal' => 'Test goal with no condition' ,
  		'page_element_id' => $page_element_id ,
  		'pointsGoal' => 1 ,
  		'typeGoal' => 'page_related'
  	]);

  	$content2 = json_decode($response->getContent(),true) ;
  	$goal_uid = $content2['content']['goal']['uid'];

    #active goal ;
    $response = $this->actingAs($user)->call('POST', '/api/goals/update-status' ,[
      '_id' =>   $content2['content']['goal']['_id'] ,
      'action' => 'update_hidden' ,
      'ajax' => true ,
      'hidden' => 0
    ]);

  	#visit
  	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);

 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$this->assertEquals(1,count($p['goals']));
 		$this->assertEquals(3, $p['goals'][0]['uid']);


 		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		$batchResponse = new BatchResponse($data);

 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$this->assertEquals(1,count($p['goals']));

 		#visit difference url
 		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/test&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		$batchResponse = new BatchResponse($data);

 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$this->assertEquals(1,count($p['goals']));
	}

	public function testGoalStartWidth(){
		$user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;

		$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://url_test001.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'startsWith'
  	]);

  	$content1 = json_decode($response->getContent(),true) ;
  	$page_element_id = $content1['content']['_id'];

  	$response = $this->actingAs($user)->call('POST','/api/goals/create',[
  		'conditionGoal' => "" ,
  		'nameGoal' => 'Test goal with no condition' ,
  		'page_element_id' => $page_element_id ,
  		'pointsGoal' => 1 ,
  		'typeGoal' => 'page_related'
  	]);



  	$content2 = json_decode($response->getContent(),true) ;
  	$goal_uid = $content2['content']['goal']['uid'];

    #active goal ;
    $response = $this->actingAs($user)->call('POST', '/api/goals/update-status' ,[
      '_id' =>   $content2['content']['goal']['_id'] ,
      'action' => 'update_hidden' ,
      'ajax' => true ,
      'hidden' => 0
    ]);

  	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/?whatisit&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);

 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$this->assertEquals(1,count($p['goals']));
 		$this->assertEquals(3, $p['goals'][0]['uid']);

 		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test0010.com/test&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);

 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$this->assertEquals(1,count($p['goals']));
	}

  public function testGoalReachCondition(){
    $user = User::first() ;
    $this->clearCache($user->account->uid . '_pageelements') ;

    $response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
      'experiment_id  ' => null ,
      'fullURLWebPageList' => 'http://url_test001.com' ,
      'includeHttpHttpsWebPageElement' => 0 ,
      'includeWWWWebPageElement' => 0 ,
      'page_element_id' => null ,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageList' => '' ,
      'source' => 'goal' ,
      'urlOptionWebPageElement' => 'startsWith'
    ]);

    $content1 = json_decode($response->getContent(),true) ;
    $page_element_id = $content1['content']['_id'];

    $response = $this->actingAs($user)->call('POST','/api/goals/create',[
      'conditionGoal' => "" ,
      'nameGoal' => 'Test goal with no condition' ,
      'page_element_id' => $page_element_id ,
      'pointsGoal' => 1 ,
      'typeGoal' => 'page_related'
    ]);

    $content2 = json_decode($response->getContent(),true) ;
    $goal_uid = $content2['content']['goal']['uid'];

    #active goal ;
    $response = $this->actingAs($user)->call('POST', '/api/goals/update-status' ,[
      '_id' =>   $content2['content']['goal']['_id'] ,
      'action' => 'update_hidden' ,
      'ajax' => true ,
      'hidden' => 0
    ]);

    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => "if ( ( MonoloopProfile.GoalReached('$goal_uid') === true ) ){ | }" ,
      'content' => 'my content' ,
      'contentType' => '' ,
      'experiment_id' => null ,
      'fullURL' => 'http://url_test001.com?data1' ,
      'page_element_id' => '' ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'my001' ,
      'xpath' => 'BODY'
    ]);
    $content = json_decode($response->getContent(),true) ;
    $content_id = $content['content']['_id'] ;
    $page_element_id = $content['pageElement']['_id'];

    $response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
      'experiment_id' => null ,
      'fullURLWebPageList' => 'http://url_test001.com?data1'  ,
      'includeHttpHttpsWebPageElement' => 0 ,
      'includeWWWWebPageElement' => 0 ,
      'page_element_id' => $page_element_id ,
      'regularExpressionWebPageElement' => '' ,
      'remarkWebPageLust' => '' ,
      'urlOptionWebPageElement' => 'exactMatch'
    ]);

    #active goal ;
    $response = $this->actingAs($user)->call('POST', '/api/content/web/update-status' ,[
      '_id' =>   $page_element_id ,
      'action' => 'update_hidden' ,
      'ajax' => true ,
      'hidden' => 0
    ]);



    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com?data1&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

    $batchResponse = new BatchResponse($data);
    $this->assertEquals( count( $batchResponse->data )  , 0 );

    $data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com?data1&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());

    $batchResponse = new BatchResponse($data);
    $this->assertEquals( $batchResponse->data[0]['content'] ,'my content');
  }
}