<?php

use App\User;
use App\Account;
use App\Profile;
use App\Services\Frontend\BatchResponse ;

class PrivacyEmptyProfileTest extends TestCase {

	public function testEmptyFirst(){
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test002.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

		$batchResponse = new BatchResponse($data);

		//This shoulf mpt error ;

		$data = file_get_contents('http://localhost:9000/emptyProfile/?cid='.$user->account->uid.'&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid ,false,$this->commonContext());

	}

	public function testEmptyWithUpdateFirst(){
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test002.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

		$batchResponse = new BatchResponse($data);

		$data = file_get_contents('http://localhost:9000/updatePrivacy/?cid='.$user->account->uid.'&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid . '&personalization=1',false,$this->commonContext());

		$data = file_get_contents('http://localhost:9000/emptyProfile/?cid='.$user->account->uid.'&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid ,false,$this->commonContext());


		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
		$this->assertEquals($profile->privacy['lastProfileDelete'],$batchResponse->mid);
		$this->assertEquals(isset($p['visits']),false);
	}


}