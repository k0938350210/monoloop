<?php

use App\User;
use App\Account;
use App\Profile;
use App\Services\Frontend\BatchResponse ;
use App\ReportingArchiveAggregated ;
use Carbon\Carbon ;

class ReportArchiveTest extends TestCase {

	public function testBasic1(){
		$this->clearCache('profilelist') ;
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);

 		for($i = 0 ; $i < 3 ; $i++){
	 		sleep(2);
	 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		}
 		sleep(2);
 		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
 		$this->assertEquals($report->cid ,$user->account->uid );
 		$this->assertEquals($report->totalPageViews ,4 );
 		$this->assertEquals($report->totalVisits ,1 );
 		$this->assertEquals($report->totalVisitors ,1 );
 		$this->assertEquals($report->totalPageReferer ,0 );

	}

	public function testBasic2(){
		$this->clearCache('profilelist') ;
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);

 		for($i = 0 ; $i < 3 ; $i++){
	 		sleep(2);
	 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		}

 		sleep(2);

 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=' ,false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);

 		for($i = 0 ; $i < 3 ; $i++){
	 		sleep(2);
	 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		}

 		sleep(2);

 		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();





 		$this->assertEquals($report->cid ,$user->account->uid );
 		$this->assertEquals($report->totalPageViews ,8 );
 		$this->assertEquals($report->totalVisits ,2 );
 		$this->assertEquals($report->totalVisitors ,1 );
 		$this->assertEquals($report->totalPageReferer ,0 );

	}

	public function testBasicPageView1(){
		$this->clearCache('profilelist') ;
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);

 		for($i = 0 ; $i < 3 ; $i++){
	 		sleep(2);
	 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		}

 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=' ,false,$this->commonContext());

 		sleep(2);

 		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
 		$this->assertEquals($report->cid ,$user->account->uid );
 		$this->assertEquals($report->totalPageViews ,5 );
	}

	public function testBasicPageReferer(){
		$this->clearCache('profilelist') ;
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		sleep(2);

		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
		$this->assertEquals($report->totalPageReferer ,1);

	}

	public function testNoPageReferer(){
		$this->clearCache('profilelist') ;
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		sleep(2);

		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
		$this->assertEquals($report->totalPageReferer ,0);
	}

	public function testSecondVisit(){
		$this->clearCache('profilelist') ;
		$user = User::first() ;
		$this->clearCache($user->account->uid.'_segments') ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		sleep(2);
		$batchResponse = new BatchResponse($data);
		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());
		sleep(2);
		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
		//$this->assertEquals($report->totalPages ,2 );
 		$this->assertEquals($report->totalPageViews ,2 );
 		$this->assertEquals($report->totalVisits ,2 );
 		$this->assertEquals($report->totalVisitors ,1 );
 		$this->assertEquals($report->totalPageReferer ,2 );
	}

	public function testNewProfileVisit(){
		$this->clearCache('profilelist') ;
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		sleep(2);

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		sleep(2);

		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
		$this->assertEquals($report->totalPages ,2 );
 		$this->assertEquals($report->totalPageViews ,2 );
 		$this->assertEquals($report->totalVisits ,2 );
 		$this->assertEquals($report->totalVisitors ,2 );
 		$this->assertEquals($report->totalPageReferer ,0 );

	}

	public function testNewProfileVisit2(){

		$this->clearCache('profilelist') ;
		$user = User::first() ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		sleep(2);

		$batchResponse = new BatchResponse($data);
		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=www.google.com&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
		sleep(2);

		$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		sleep(2);

		$report =  ReportingArchiveAggregated::where('cid',$user->account->uid)->first();
		$this->assertEquals($report->totalPages ,3 );
 		$this->assertEquals($report->totalPageViews ,3 );
 		$this->assertEquals($report->totalVisits ,2 );
 		$this->assertEquals($report->totalVisitors ,2 );
 		$this->assertEquals($report->totalPageReferer ,0 );

	}


}