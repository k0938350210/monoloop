<?php

use App\User;
use App\Account;
use App\Plugin;


class ApiAccountUserTest extends TestCase {


  public function setUp()
  {
    parent::setUp();
    #add more account user ;
    $account = Account::first() ;
    $user = factory(App\User::class)->make();
    $user->active_account = new MongoId($account->_id) ;
    $user->save() ;

    $accountUser = factory(App\AccountUser::class)->make();
    $accountUser->account_id = (string)$account->_id ;
    $accountUser->user_id = (string)$user->_id ;
    $accountUser->is_monoloop_admin = 0 ;
    $accountUser->_id = new MongoId("5625ff0efaba0db822c72a74");
    $accountUser->save() ;

  }

  public function testListing(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/users' , [] , [] , [] , $server);
    $content = json_decode($response->getContent(),true) ;

    $this->assertEquals(200,$response->getStatusCode());
    $this->assertEquals($content['totalCount'],2);
    $this->assertTrue(array_key_exists('topics',$content));
    $this->assertTrue(array_key_exists('totalCount',$content));

    $user = $content['topics'][0] ;
    $this->assertTrue(array_key_exists('_id',$user));
    $this->assertTrue(array_key_exists('account_id',$user));
    $this->assertTrue(array_key_exists('user_id',$user));
    $this->assertTrue(array_key_exists('email',$user));
    $this->assertTrue(array_key_exists('company',$user));
    $this->assertTrue(array_key_exists('name',$user));
    $this->assertTrue(array_key_exists('deleted',$user));
    $this->assertTrue(array_key_exists('hidden',$user));
    $this->assertTrue(array_key_exists('is_monoloop_admin',$user));
    $this->assertTrue(array_key_exists('is_monoloop_support',$user));
    $this->assertTrue(array_key_exists('dashboard_config',$user));
    $this->assertTrue(array_key_exists('support_type',$user));
    $this->assertTrue(array_key_exists('support_enddate',$user));
    $this->assertTrue(array_key_exists('rights',$user));
    $this->assertTrue(array_key_exists('can_resend_invitation',$user));
  }

  public function testCreate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/users' , ['company' => 'monoloop' , 'email' => 'demo@monoloop.com' , 'is_monoloop_admin' => 1 , 'firstname' => 'demo' , 'lastname' => 'user' , 'confirmEmail' => 'demo@monoloop.com' ] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('accountUser',$content));
    $this->assertTrue(array_key_exists('email',$content));
    $this->assertEquals($content['success'],true);

    $user = $content['accountUser'] ;
    $this->assertEquals($user['email'],'demo@monoloop.com');
    $this->assertEquals($user['company'],'demo user');
    $this->assertEquals($user['name'],'demo user');
    $this->assertEquals($user['is_monoloop_admin'],1);
    $this->assertEquals($user['deleted'],0);
    $this->assertEquals($user['hidden'],0);
    $this->assertEquals($user['can_resend_invitation'],true);
  }

  public function testUpdate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('PUT', '/api/v1/users/' . "5625ff0efaba0db822c72a74"  , [ 'company' => 'monoloop1' , 'email' => 'demo@monoloop.com1' , 'is_monoloop_admin' => 0 , 'name' => 'demo user1' , 'rulesData' => '[]'  , 'confirmEmail' => 'demo@monoloop.com1' , 'firstname' => 'demo' , 'lastname' => 'user1' ] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());

    $content = json_decode($response->getContent(),true) ;

    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('accountUser',$content));
    $this->assertTrue(array_key_exists('email',$content));
    $this->assertEquals($content['success'],true);

    $user = $content['accountUser'];
    $this->assertEquals($user['email'],'demo@monoloop.com1');
    $this->assertEquals($user['company'],'demo user1');
    $this->assertEquals($user['name'],'demo user1');
    $this->assertEquals($user['is_monoloop_admin'],0);
    $this->assertEquals($user['deleted'],0);
    $this->assertEquals($user['hidden'],0);
  }

  public function testDelete(){
    $server = $this->getServerRequest() ;
    $response = $this->call('DELETE', '/api/v1/users/' . "5625ff0efaba0db822c72a74"  , [] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());

    $content = json_decode($response->getContent(),true) ;

    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('accountUser',$content));
    $this->assertTrue(array_key_exists('email',$content));
    $user = $content['accountUser'] ;
    $this->assertEquals($user['deleted'],true);
  }

  public function testUpdateStatus(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/users/' . "5625ff0efaba0db822c72a74/status-update"  , ['hidden' => 1] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $this->assertEquals($content['success'],true);
    $user = $content['accountUser'] ;
    $this->assertEquals($user['hidden'],1);

    $response = $this->call('POST', '/api/v1/users/' . "5625ff0efaba0db822c72a74/status-update"  , ['hidden' => 0] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $this->assertEquals($content['success'],true);
    $user = $content['accountUser'] ;
    $this->assertEquals($user['hidden'],0);
  }
}
