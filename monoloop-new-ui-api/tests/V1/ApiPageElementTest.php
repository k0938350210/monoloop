<?php

use App\Placement;
use App\Content ;

#deprecated 


class ApiPageElementTest extends TestCase {
  public function testListing(){
    Placement::truncate() ;
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' ] , [] , [] , $this->getServerRequest());
    $content = json_decode($response->getContent(),true) ;

    $response = $this->call('GET', '/api/v1/pages/' . $content['uid'] . '/elements' , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content2 = json_decode($response->getContent(),true) ;
    $this->assertEquals('success',$content2['status']);
    foreach(['status','totalCount','next','prev','relations'] as $key){
      $this->assertTrue(array_key_exists($key,$content2)) ;
    }
    $this->assertEquals(0,$content2['totalCount']);

    #get first element ;
    $defaultContent =  Content::first() ;

    //------ add new elements ;
    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , ['eid' => $defaultContent->uid , 'position' => 1 , 'xpath' => 'xpath'  ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content3 = json_decode($response->getContent(),true) ;
    $this->assertEquals('created',$content3['status']);



    $response = $this->call('GET', '/api/v1/pages/' . $content['uid'] . '/elements' , [] , [] , [] , $this->getServerRequest());
    $content4 = json_decode($response->getContent(),true) ;
    $this->assertEquals(1,$content4['totalCount']);

    //------ add fail element ;
    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , [ 'position' => 1 , 'xpath' => 'xpath' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , [ 'position' => 1 , 'eid' => $defaultContent->uid ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , [ 'xpath' => 'xpath', 'eid' => $defaultContent->uid ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , ['eid' => $defaultContent->uid , 'position' => 1 , 'xpath' => 'xpath' , 'order' => 'string'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , ['eid' => $defaultContent->uid , 'position' => 1 , 'xpath' => 'xpath' , 'active' => 'string'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , ['eid' => $defaultContent->uid , 'position' => 1 , 'xpath' => 'xpath' , 'displayType' => 'string'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , ['eid' => $defaultContent->uid , 'position' => 1 , 'xpath' => 'xpath' , 'order' => 'string'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

  }

  public function testUpdate(){
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' ] , [] , [] , $this->getServerRequest());
    $content = json_decode($response->getContent(),true) ;

    #get first element ;
    $defaultContent =  Content::first() ;
    //------ add new elements ;
    $response = $this->call('POST', '/api/v1/pages/'.$content['uid'] . '/elements' , ['eid' => $defaultContent->uid , 'position' => 1 , 'xpath' => 'xpath'  ] , [] , [] , $this->getServerRequest());
    $content2 = json_decode($response->getContent(),true) ;
    $response = $this->call('PUT', '/api/v1/pages/'.$content['uid'] . '/elements/' . $content2['uid'] , ['eid' => $defaultContent->uid , 'position' => 2, 'xpath' => 'xpath_new' , 'order' => 2 , 'displayType' => 2 , 'active' => true  ] , [] , [] , $this->getServerRequest());

    $this->assertEquals(201,$response->getStatusCode());

    //get element ;
    $response = $this->call('GET', '/api/v1/pages/'.$content['uid'] . '/elements/' . $content2['uid'] , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content3 = json_decode($response->getContent(),true) ;
    $this->assertEquals( $content2['uid'],$content3['uid']);
    $this->assertEquals( 'xpath_new',$content3['xpath']);
    $this->assertEquals( true ,$content3['active']);
    $this->assertEquals( 2 ,$content3['position']);
  }

  public function testDelete(){
    $defaultContent =  Content::first() ;
    $placement = Placement::orderBy('created_at', 'desc')->first();
    $response = $this->call('POST', '/api/v1/pages/'.$placement->uid  . '/elements' , ['eid' => $defaultContent->uid , 'position' => 1 , 'xpath' => 'xpath'  ] , [] , [] , $this->getServerRequest());
    $response = $this->call('GET', '/api/v1/pages/'.$placement->uid . '/elements' , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals( 2 ,$content['totalCount']);

    $uids = [] ;
    foreach($content['relations'] as $relation ){
      $uids[] = $relation['uid'] ;
    }

    $delete_uid = $uids[0] ;
    $response = $this->call('DELETE', '/api/v1/pages/'.$placement->uid . '/elements/' . $delete_uid , [] , [] , [] , $this->getServerRequest() ) ;
    $content2 = json_decode($response->getContent(),true) ;
    $this->assertEquals(200,$response->getStatusCode());
    $this->assertEquals( 'success' ,$content['status']);

    $response = $this->call('GET', '/api/v1/pages/'.$placement->uid . '/elements/' . $delete_uid , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('GET', '/api/v1/pages/'.$placement->uid . '/elements' , [] , [] , [] , $this->getServerRequest());
    $content4 = json_decode($response->getContent(),true) ;
    $this->assertEquals( 1 ,$content4['totalCount']);

    $uids2 = [] ;
    foreach($content4['relations'] as $relation ){
      $uids2[] = $relation['uid'] ;
    }
    $this->assertTrue(!in_array($delete_uid , $uids2));

    #cannot redelete
    $response = $this->call('DELETE', '/api/v1/pages/'.$placement->uid . '/elements/' . $delete_uid , [] , [] , [] , $this->getServerRequest() ) ;
    $this->assertEquals(400,$response->getStatusCode());
  }
}
