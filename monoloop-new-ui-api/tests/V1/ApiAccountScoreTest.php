<?php

use App\User;
use App\Account;


class ApiAccountScoreTest extends TestCase {
  public function testRequired(){
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('POST', '/api/v1/account/scores/update' , [] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/scores/update' , ['clickdepth' => '12'] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/scores/update' , ['duration' => '13'] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testInvalidClickDepth(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/account/scores/update' , ['clickdepth' => 'testesttest','duration' => 1.15 , 'latency' => 1 , 'recency' => 14] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testInvalidDuration(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/account/scores/update' , ['clickdepth' => 1,'duration' => -1.15 , 'latency' => 1 , 'recency' => 14] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/scores/update' , ['clickdepth' => 1,'duration' => 'test' , 'latency' => 1 , 'recency' => 14] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testInvalidLatency(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/account/scores/update' , ['clickdepth' => 1,'duration' => -1.15 , 'latency' => 'test' , 'recency' => 14] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testInvalidRecency(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/account/scores/update' , ['clickdepth' => 1,'duration' => -1.15 , 'latency' => 1 , 'recency' => 'test'] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testSuccessUpdate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/account/scores/update' , ['clickdepth' => 1,'duration' => 1 , 'latency' => 1 , 'recency' => 2] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('msg',$content));
    $this->assertEquals(true,$content['success']);

    $account = Account::first() ;
    $this->assertEquals(1,$account->scores['clickdepth']);
    $this->assertEquals(1,$account->scores['latency']);
    $this->assertEquals(2,$account->scores['recency']);
    $this->assertEquals(1,$account->scores['duration']); 
  }
}
