<?php

use App\User;
use App\Account;


class ApiAccountInvocationTest extends TestCase {
  public function testRequired(){
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('POST', '/api/v1/account/invocation' , [] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation' , ['anti_flicker' => true ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation' , ['content_delivery' => 1 ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation' , ['timeout' => 1 ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());


    $response = $this->call('POST', '/api/v1/account/invocation' , ['anti_flicker' => true , 'content_delivery' => 1  ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation' , ['anti_flicker' => true , 'timeout' => 1  ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation' , ['content_delivery' => true , 'timeout' => 1  ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

  }

  public function testIncorrectContentDelivery(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/account/invocation' , ['anti_flicker' => true , 'content_delivery' => -1  , 'timeout' => 1 ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation' , ['anti_flicker' => true , 'content_delivery' => 5  , 'timeout' => 1 ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testIncorrectTimeout(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/account/invocation' , ['anti_flicker' => true , 'content_delivery' => 0  , 'timeout' => -1 ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation' , ['anti_flicker' => true , 'content_delivery' => 0  , 'timeout' => 'string' ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testSuccessUpdate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/account/invocation' , ['anti_flicker' => true , 'content_delivery' => 1  , 'timeout' => 100 ] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('msg',$content));
    $this->assertTrue(array_key_exists('invocation',$content));

    $account = Account::first() ;
    $this->assertEquals(true,$account->invocation['anti_flicker']);
    $this->assertEquals(1,$account->invocation['content_delivery']);
    $this->assertEquals(100,$account->invocation['timeout']);
  }

}
