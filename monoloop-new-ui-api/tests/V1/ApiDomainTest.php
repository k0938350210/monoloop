<?php

use App\User;
use App\Account;
use App\Plugin;
use App\Services\Invocation\DomainValidator;


class ApiDomainTest extends TestCase {

  public function setUp()
  {
    parent::setUp();

  }

  public function testListing(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/domains' , [] , [] , [] , $server);
    $account = Account::first() ;
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals($content['totalCount'],count($account->domains));
    $this->assertTrue(array_key_exists('topics',$content));
    $this->assertTrue(array_key_exists('totalCount',$content));

    $domain = $content['topics'][0] ;
    $this->assertTrue(array_key_exists('domain',$domain));
    $this->assertTrue(array_key_exists('url_privacy',$domain));
    $this->assertTrue(array_key_exists('privacy_option',$domain));
    $this->assertTrue(array_key_exists('status',$domain));
  }

  public function testCreate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/domains' , ['domain' => 'monoloop1.com' , 'privacy_option' => 'manual' , 'url_privacy' => 'http://www.monoloop.com/privacy'] , [] , [] , $server);

    $account = Account::first() ;
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(4,count($account->domains));
    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('domain',$content));
    $domain = $content['domain'];
    $this->assertTrue(array_key_exists('_id',$domain));
    $this->assertTrue(array_key_exists('placement_id',$domain));
    $this->assertEquals('monoloop1.com', $domain['domain'] );
    $this->assertEquals('manual', $domain['privacy_option'] );
    //$this->assertEquals('http://www.monoloop.com/privacy', $domain['url_privacy'] );
    ### auto create placement_id ;

  }

  public function testUpdate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('PUT', '/api/v1/domains/' . 'monoloop.com' , ['domain' => 'monoloop1.com' , 'privacy_option' => 'auto' , 'url_privacy' => 'http://www.monoloop.com/privacy1'] , [] , [] , $server);
    $account = Account::first() ;
    $this->assertEquals(200,$response->getStatusCode());

    $account = Account::first() ;
    $this->assertEquals(3,count($account->domains));
    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('domain',$content));
    $domain = $content['domain'];
    $this->assertEquals('monoloop1.com', $domain['domain'] );
    $this->assertEquals('auto', $domain['privacy_option'] );
    $this->assertEquals('http://www.monoloop.com/privacy1', $domain['url_privacy'] );
  }

  public function testRequireDomain(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/domains' , ['privacy_option' => 'auto' , 'url_privacy' => 'http://www.monoloop.com/privacy1'] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }



  public function testUpdateNotExists(){
    $server = $this->getServerRequest() ;
    $response = $this->call('PUT', '/api/v1/domains/' . 'monoloopxxx.com' , ['domain' => 'monoloop1.com' , 'privacy_option' => 'auto' , 'url_privacy' => 'http://www.monoloop.com/privacy1'] , [] , [] , $server);
    $this->assertEquals(404,$response->getStatusCode());
  }

  public function testDelete(){
    $server = $this->getServerRequest() ;
    $account = Account::first() ;
    $domain = $account->domains[0] ;
    $response = $this->call('DELETE', '/api/v1/domains/' . $domain->_id , [] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $account = Account::first() ;
    $this->assertEquals(true, $content['success'] );
    $this->assertEquals(2, count( $account->domains) );
  }

  public function testDeleteNotExists(){
    $server = $this->getServerRequest() ;
    $id = new MongoId() ;
    $response = $this->call('DELETE', '/api/v1/domains/' .$id , [] , [] , [] , $server);
    $this->assertEquals(404,$response->getStatusCode());
  }

  public function testTestDomain(){
    /*
    $status = $domainValidator->checkInvocation('monoloop.com') ;
    $this->assertEquals(1,$status);
    */
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/domains/test-domain/monoloop.com' , [] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(2, $content['domain']['status'] );
  }
}
