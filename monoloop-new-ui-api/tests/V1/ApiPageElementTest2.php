<?php

use App\Placement;
use App\Content ;
use App\PageElement ;

#deprecated


class ApiPageElementTest2 extends TestCase {
  public function testUrlOptionSave(){
    Placement::truncate() ;
    $response = $this->call('POST', '/api/v1/page-element/save' , [
      'content' => [
        ['code' => 'My Code' , 'contentType' => null , 'mappedTo' => 'DIV#data001' , 'type' => 'default']
      ],
      'fullURL' => 'http://monoloop.aecstar.com/stag/index2.html' ,
      'tAdditionalJSWebPageElement' => '' ,
      'tIncludeHttpHttpsWebPageElement' => 0 ,
      'tIncludeWWWWebPageElement' => 0 ,
      'tRegularExpressionWebPageElement' => '' ,
      'tRemarkWebPageList' => '' ,
      'tUrlOptionWebPageElement' => 'exactMatch' ,
      'tfullURLWebPageElement' => 'http://monoloop.aecstar.com/stag/index2.html'
    ] , [] , [] , $this->getServerRequest());

    $pageElement = PageElement::where('fullURL','http://monoloop.aecstar.com/stag/index2.html')->first() ;

    $this->assertEquals(201,$response->getStatusCode());
    $this->assertEquals($pageElement->urlOption,0);
  }

  public function testUrlOption2Save(){
    Placement::truncate() ;
    $response = $this->call('POST', '/api/v1/page-element/save' , [
      'content' => [
        ['code' => 'My Code' , 'contentType' => null , 'mappedTo' => 'DIV#data001' , 'type' => 'default']
      ],
      'fullURL' => 'http://monoloop.aecstar.com/stag/index2.html' ,
      'tAdditionalJSWebPageElement' => '' ,
      'tIncludeHttpHttpsWebPageElement' => 0 ,
      'tIncludeWWWWebPageElement' => 0 ,
      'tRegularExpressionWebPageElement' => '' ,
      'tRemarkWebPageList' => '' ,
      'tUrlOptionWebPageElement' => 'allowParams' ,
      'tfullURLWebPageElement' => 'http://monoloop.aecstar.com/stag/index2.html'
    ] , [] , [] , $this->getServerRequest());

    $pageElement = PageElement::where('fullURL','http://monoloop.aecstar.com/stag/index2.html')->first() ;

    $this->assertEquals(201,$response->getStatusCode());
    $this->assertEquals($pageElement->urlOption,1);
  }

  public function testCodeSave(){
    Placement::truncate() ;
    $response = $this->call('POST', '/api/v1/page-element/save' , [
      'content' => [
        ['code' => '&nbsp;<div style="display:inline;color:green;" condition="if ( ( MonoloopProfile.VisitCount == \'12\'
) ){ | }">data<br></div>' , 'contentType' => null , 'mappedTo' => 'DIV#data001' , 'type' => 'default']
      ],
      'fullURL' => 'http://monoloop.aecstar.com/stag/index2.html' ,
      'tAdditionalJSWebPageElement' => '' ,
      'tIncludeHttpHttpsWebPageElement' => 0 ,
      'tIncludeWWWWebPageElement' => 0 ,
      'tRegularExpressionWebPageElement' => '' ,
      'tRemarkWebPageList' => '' ,
      'tUrlOptionWebPageElement' => 'exactMatch' ,
      'tfullURLWebPageElement' => 'http://monoloop.aecstar.com/stag/index2.html'
    ] , [] , [] , $this->getServerRequest());

    $pageElement = PageElement::where('fullURL','http://monoloop.aecstar.com/stag/index2.html')->first() ;

    $this->assertEquals(201,$response->getStatusCode());
    $this->assertEquals($pageElement->content[0]->code ,'&nbsp;<% if ( ( MonoloopProfile.VisitCount == \'12\'
) ){ %>data<br><%}%>');
  }

  public function testCode2Save(){
    Placement::truncate() ;
    $response = $this->call('POST', '/api/v1/page-element/save' , [
      'content' => [
        ['code' => 'Before <div style="display:inline;color:green;" condition="if ( ( MonoloopProfile.VisitCount == \'12\'
) ){ | }">data<br></div> After' , 'contentType' => null , 'mappedTo' => 'DIV#data001' , 'type' => 'default']
      ],
      'fullURL' => 'http://monoloop.aecstar.com/stag/index2.html' ,
      'tAdditionalJSWebPageElement' => '' ,
      'tIncludeHttpHttpsWebPageElement' => 0 ,
      'tIncludeWWWWebPageElement' => 0 ,
      'tRegularExpressionWebPageElement' => '' ,
      'tRemarkWebPageList' => '' ,
      'tUrlOptionWebPageElement' => 'exactMatch' ,
      'tfullURLWebPageElement' => 'http://monoloop.aecstar.com/stag/index2.html'
    ] , [] , [] , $this->getServerRequest());

    $pageElement = PageElement::where('fullURL','http://monoloop.aecstar.com/stag/index2.html')->first() ;

    $this->assertEquals(201,$response->getStatusCode());
    $this->assertEquals($pageElement->content[0]->code ,'Before <% if ( ( MonoloopProfile.VisitCount == \'12\'
) ){ %>data<br><%}%> After');
  }


  public function testCode3Save(){
    Placement::truncate() ;
    $response = $this->call('POST', '/api/v1/page-element/save' , [
      'content' => [
        ['code' => 'Before <div style="display:inline;color:green;" condition="if ( ( MonoloopProfile.VisitCount == \'12\'
) ){ | }">data <div style="display:inline;color:green;" condition="if ( ( MonoloopProfile.VisitCount == \'12\'
) ){ | }">inside</div> data2<br></div> After' , 'contentType' => null , 'mappedTo' => 'DIV#data001' , 'type' => 'default']
      ],
      'fullURL' => 'http://monoloop.aecstar.com/stag/index2.html' ,
      'tAdditionalJSWebPageElement' => '' ,
      'tIncludeHttpHttpsWebPageElement' => 0 ,
      'tIncludeWWWWebPageElement' => 0 ,
      'tRegularExpressionWebPageElement' => '' ,
      'tRemarkWebPageList' => '' ,
      'tUrlOptionWebPageElement' => 'exactMatch' ,
      'tfullURLWebPageElement' => 'http://monoloop.aecstar.com/stag/index2.html'
    ] , [] , [] , $this->getServerRequest());

    $pageElement = PageElement::where('fullURL','http://monoloop.aecstar.com/stag/index2.html')->first() ;

    $this->assertEquals(201,$response->getStatusCode());
    $this->assertEquals($pageElement->content[0]->code ,'Before <% if ( ( MonoloopProfile.VisitCount == \'12\'
) ){ %>data <% if ( ( MonoloopProfile.VisitCount == \'12\'
) ){ %>inside<%}%> data2<br><%}%> After');
  }
}
