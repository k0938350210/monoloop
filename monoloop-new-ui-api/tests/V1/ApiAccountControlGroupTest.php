<?php

use App\User;
use App\Account;


class ApiAccountControlGroupTest extends TestCase {
  public function testUpdateIncorrectSize(){
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('POST', '/api/v1/account/controlgroup' , ['size' => -1] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/controlgroup' , ['size' => 'None'] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/controlgroup' , ['size' => 500] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testUpdateIncorrectDays(){
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('POST', '/api/v1/account/controlgroup' , ['days' => -1] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testRequired(){
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('POST', '/api/v1/account/controlgroup' , [] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testSuccessUpdate(){
    $server = $this->getServerRequest() ;
		$response = $this->call('POST', '/api/v1/account/controlgroup' , ['size' => 50 , 'days' => 10 , 'enable' => true] , [] , [] , $server);
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(200,$response->getStatusCode());
    $account = Account::first() ;
    $this->assertEquals(50,$account->control_group['size']);
    $this->assertEquals(10,$account->control_group['days']);
    $this->assertEquals(true,$account->control_group['enabled']);
  }

  public function testSuccessUpdate2(){
    $server = $this->getServerRequest() ;
		$response = $this->call('POST', '/api/v1/account/controlgroup' , ['size' => 50 , 'days' => 0 , 'enable' => false] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $account = Account::first() ;
    $this->assertEquals(50,$account->control_group['size']);
    $this->assertEquals(0,$account->control_group['days']);
    $this->assertEquals(false,$account->control_group['enable']);
  }
}
