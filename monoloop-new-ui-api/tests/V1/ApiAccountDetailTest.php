<?php

use App\User;
use App\Account;


class ApiAccountDetailTest extends TestCase {
  public function testGetDetail(){
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('GET', '/api/v1/account' , [] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('_id',$content));
    $this->assertTrue(array_key_exists('cid',$content));
    $this->assertTrue(array_key_exists('uid',$content));
    $this->assertTrue(array_key_exists('updated_at',$content));
    $this->assertTrue(array_key_exists('created_at',$content));
    $this->assertTrue(array_key_exists('created_at',$content));
    $this->assertTrue(array_key_exists('deleted',$content));
    $this->assertTrue(array_key_exists('hidden',$content));
    $this->assertTrue(array_key_exists('name',$content));
    $this->assertTrue(array_key_exists('company',$content));
    $this->assertTrue(array_key_exists('contact_name',$content));
    $this->assertTrue(array_key_exists('contact_email',$content));
    $this->assertTrue(array_key_exists('phone',$content));
    $this->assertTrue(array_key_exists('invoice_reseller',$content));
    $this->assertTrue(array_key_exists('apihost',$content));
    $this->assertTrue(array_key_exists('apitoken',$content));
    $this->assertTrue(array_key_exists('control_group',$content));
    $this->assertTrue(array_key_exists('invocation',$content));
    $this->assertTrue(array_key_exists('scores',$content));
    $invocation = $content['invocation'] ;
    $this->assertTrue(array_key_exists('content_delivery',$invocation));
    $this->assertTrue(array_key_exists('anti_flicker',$invocation));
    $this->assertTrue(array_key_exists('timeout',$invocation));
    $this->assertTrue(array_key_exists('pre',$invocation));
    $this->assertTrue(array_key_exists('post',$invocation));
    $score = $content['scores'] ;
    $this->assertTrue(array_key_exists('clickdepth',$score));
    $this->assertTrue(array_key_exists('duration',$score));
    $this->assertTrue(array_key_exists('recency',$score));
    $this->assertTrue(array_key_exists('brands',$score));
    $control_group = $content['control_group'] ;
    $this->assertTrue(array_key_exists('enable',$control_group));
    $this->assertTrue(array_key_exists('size',$control_group));
    $this->assertTrue(array_key_exists('days',$control_group));
    return ;
  }
}
