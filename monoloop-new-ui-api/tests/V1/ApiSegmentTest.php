<?php

use App\User;
use App\Account;
use App\Segment;
use App\Folder ;

/***************** DEPRECATE ********************/

class ApiSegmentTest extends TestCase {


  public function setUp()
  {
    parent::setUp();
  }
  /*
  public function testList(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/segments' ,[] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    #print_r($content);
    //first element must be root & folder type
    $this->assertEquals(1,count($content));
    $this->assertEquals('root',$content[0]['value']);
    $this->assertEquals('folder',$content[0]['type']);
  }
  */
  public function testCreate(){

    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/create' , ['conditionSegment' => "if ( ( MonoloopProfile.VisitDuration == '12' ) ){}" , 'descSegment' => 'description' , 'nameSegment' => 'name' , 'source' => 'folder__' . Folder::first()->_id ] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('msg',$content));
    $this->assertTrue(array_key_exists('source',$content));
    $this->assertTrue(array_key_exists('content',$content));
    $content = $content['content'] ;
    $this->assertTrue(array_key_exists('segment',$content));
    $segment = $content['segment'] ;

    $this->assertTrue(array_key_exists('name',$segment));
    $this->assertTrue(array_key_exists('description',$segment));
    $this->assertTrue(array_key_exists('condition',$segment));
    //$this->assertTrue(array_key_exists('condition_json',$segment));
    $this->assertTrue(array_key_exists('account_id',$segment));
    $this->assertTrue(array_key_exists('deleted',$segment));
    $this->assertTrue(array_key_exists('hidden',$segment));
    $this->assertTrue(array_key_exists('_id',$segment));

    $this->assertEquals('name',$segment['name']);
    $this->assertEquals('description',$segment['description']);
    $this->assertEquals("if ( ( MonoloopProfile.VisitDuration == '12' ) ){}",$segment['condition']);
    $this->assertEquals($segment['deleted'],0);
    $this->assertEquals($segment['hidden'],1);
    $this->assertEquals($segment['uid'],2);
  }

  public function testInsertBlankName(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/create' , ['conditionSegment' => "if ( ( MonoloopProfile.VisitDuration == '12' ) ){}" , 'condition_json' => '{"rules":[{"key":"VisitDuration","Label":"Length of visit","Group":"Current Visit","Type":"integer","qtipCfg"
:{"text":"The number of seconds of this visit."},"rules":[],"linkingLogicalOperator":"","uid":"732cad95a0ba4192a2480acdaa0bb40f"
,"hideConditionLogicalOperators":true,"binaryConditions":[{"name":"=="},{"name":"!="},{"name":"<"},{"name"
:">"},{"name":"<="},{"name":">="}],"binaryConditionsDefault":"==","value":"12","styles":{"and":{"line"
:{"top":149,"left":62,"height":30},"icon":{"top":149,"left":48}},"or":{"line":{"lineUpper":{"top":21
,"left":-8,"width":8},"lineMiddle":{"top":21,"left":-8,"height":179},"lineLower":{"top":200,"left":-8
,"width":8}},"icon":{"left":-19}}}}]}' , 'descSegment' => 'description' , 'source' => 'folder__' . Folder::first()->_id ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testInvalidFolderId(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/create' , ['conditionSegment' => "if ( ( MonoloopProfile.VisitDuration == '12' ) ){}" , 'condition_json' => '{"rules":[{"key":"VisitDuration","Label":"Length of visit","Group":"Current Visit","Type":"integer","qtipCfg"
:{"text":"The number of seconds of this visit."},"rules":[],"linkingLogicalOperator":"","uid":"732cad95a0ba4192a2480acdaa0bb40f"
,"hideConditionLogicalOperators":true,"binaryConditions":[{"name":"=="},{"name":"!="},{"name":"<"},{"name"
:">"},{"name":"<="},{"name":">="}],"binaryConditionsDefault":"==","value":"12","styles":{"and":{"line"
:{"top":149,"left":62,"height":30},"icon":{"top":149,"left":48}},"or":{"line":{"lineUpper":{"top":21
,"left":-8,"width":8},"lineMiddle":{"top":21,"left":-8,"height":179},"lineLower":{"top":200,"left":-8
,"width":8}},"icon":{"left":-19}}}}]}' , 'descSegment' => 'description' , 'source' => 'folder__124' . Folder::first()->_id ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testInsertBlankCondition(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/create' , [ 'condition_json' => '{"rules":[{"key":"VisitDuration","Label":"Length of visit","Group":"Current Visit","Type":"integer","qtipCfg"
:{"text":"The number of seconds of this visit."},"rules":[],"linkingLogicalOperator":"","uid":"732cad95a0ba4192a2480acdaa0bb40f"
,"hideConditionLogicalOperators":true,"binaryConditions":[{"name":"=="},{"name":"!="},{"name":"<"},{"name"
:">"},{"name":"<="},{"name":">="}],"binaryConditionsDefault":"==","value":"12","styles":{"and":{"line"
:{"top":149,"left":62,"height":30},"icon":{"top":149,"left":48}},"or":{"line":{"lineUpper":{"top":21
,"left":-8,"width":8},"lineMiddle":{"top":21,"left":-8,"height":179},"lineLower":{"top":200,"left":-8
,"width":8}},"icon":{"left":-19}}}}]}' , 'descSegment' => 'description' , 'source' => 'folder__124' . Folder::first()->_id ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testUpdateSuccess(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/update' , ['_id' => '562dcdeffaba0d811a8b4573' ,  'conditionSegment' => "if ( ( MonoloopProfile.VisitDuration == '555' ) ){}" , 'condition_json' => '{"rules":[{"key":"VisitDuration","Label":"Length of visit","Group":"Current Visit","Type":"integer","qtipCfg"
:{"text":"The number of seconds of this visit."},"rules":[],"linkingLogicalOperator":"","uid":"732cad95a0ba4192a2480acdaa0bb40f"
,"hideConditionLogicalOperators":true,"binaryConditions":[{"name":"=="},{"name":"!="},{"name":"<"},{"name"
:">"},{"name":"<="},{"name":">="}],"binaryConditionsDefault":"==","value":"12","styles":{"and":{"line"
:{"top":149,"left":62,"height":30},"icon":{"top":149,"left":48}},"or":{"line":{"lineUpper":{"top":21
,"left":-8,"width":8},"lineMiddle":{"top":21,"left":-8,"height":179},"lineLower":{"top":200,"left":-8
,"width":8}},"icon":{"left":-19}}}}]}' , 'descSegment' => 'description1' , 'nameSegment' => 'name1' , 'source' => 'folder__' . Folder::first()->_id ] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $content = $content['content'] ;
    $segment = $content['segment'] ;
    $this->assertEquals('name1',$segment['name']);
    $this->assertEquals('description1',$segment['description']);
    $this->assertEquals("if ( ( MonoloopProfile.VisitDuration == '555' ) ){}",$segment['condition']);
    $this->assertEquals('562dcdeffaba0d811a8b4573',$segment['_id']);
  }

  public function testUpdateIncorrectId(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/update' , ['_id' => '562dcd1ffaba0d811a8b4573' ,  'conditionSegment' => "if ( ( MonoloopProfile.VisitDuration == '555' ) ){}" , 'condition_json' => '{"rules":[{"key":"VisitDuration","Label":"Length of visit","Group":"Current Visit","Type":"integer","qtipCfg"
:{"text":"The number of seconds of this visit."},"rules":[],"linkingLogicalOperator":"","uid":"732cad95a0ba4192a2480acdaa0bb40f"
,"hideConditionLogicalOperators":true,"binaryConditions":[{"name":"=="},{"name":"!="},{"name":"<"},{"name"
:">"},{"name":"<="},{"name":">="}],"binaryConditionsDefault":"==","value":"12","styles":{"and":{"line"
:{"top":149,"left":62,"height":30},"icon":{"top":149,"left":48}},"or":{"line":{"lineUpper":{"top":21
,"left":-8,"width":8},"lineMiddle":{"top":21,"left":-8,"height":179},"lineLower":{"top":200,"left":-8
,"width":8}},"icon":{"left":-19}}}}]}' , 'descSegment' => 'description1' , 'nameSegment' => 'name1' , 'source' => 'folder__' . Folder::first()->_id ] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testShow(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/segments/show?_id=segment__' . Segment::first()->_id , [] , [] , [] , $server ) ;
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('_id',$content));
    $this->assertTrue(array_key_exists('name',$content));
    $this->assertTrue(array_key_exists('description',$content));
    $this->assertTrue(array_key_exists('condition',$content));
    $this->assertTrue(array_key_exists('condition_json',$content));
    $this->assertTrue(array_key_exists('account_id',$content));
    $this->assertTrue(array_key_exists('deleted',$content));
    $this->assertTrue(array_key_exists('hidden',$content));
    $this->assertTrue(array_key_exists('folder_id',$content));
    $this->assertTrue(array_key_exists('updated_at',$content));
    $this->assertTrue(array_key_exists('created_at',$content));
  }

  public function testUpdateStatus(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/update-status' , ['_id' => '562dcdeffaba0d811a8b4573' , 'hidden' => 1] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(1,$content['content']['segment']['hidden']);

    $response = $this->call('POST', '/api/v1/segments/update-status' , ['_id' => '562dcdeffaba0d811a8b4573' , 'hidden' => 0] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(0,$content['content']['segment']['hidden']);
  }

  public function testUpdateStatusWithWrongId(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/update-status' , ['_id' => 'a62dcdeffaba0d811a8b4573' , 'hidden' => 1] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testUpdateStatusWithRequireId(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/update-status' , [ 'hidden' => 1] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testUpdateStatusWithRequireHidden(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/update-status' , [ '_id' => '562dcdeffaba0d811a8b4573'] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testDelete(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/delete' , [ '_id' => '562dcdeffaba0d811a8b4573'] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(1,$content['content']['segment']['deleted']);
  }

  public function testDeleteWithIncorrectId(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/segments/delete' , [ '_id' => '562dcdefxfaba0d811a8b4573'] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }
}
