<?php

class ApiConditionBuilderTest extends TestCase {
  public function testParse(){

    $response = $this->call('POST', '/api/v1/condition-builder/parse' , [
      'c' => "if ( ( MonoloopProfile.VisitCount == '13' ) ){ | }"
    ] , [] , [] , $this->getServerRequest());


    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ; 
  }
}
