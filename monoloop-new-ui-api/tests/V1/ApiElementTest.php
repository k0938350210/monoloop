<?php

use App\Placement;
use App\Content ;
use App\Folder;

class ApiElementTest extends TestCase {
  public function testListing(){
    Placement::truncate() ;

    $response = $this->call('GET', '/api/v1/elements' , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    foreach(['status','totalCount','next','prev','content'] as $key){
      $this->assertTrue(array_key_exists($key,$content)) ;
    }
    $this->assertEquals(1,$content['totalCount']);
    $this->assertEquals('success',$content['status']);
  }

  public function testCreate(){
    $response = $this->call('POST', '/api/v1/elements' , ['name' => 'new content' , 'body' => ['content' => 'my_content' ],'tmplType' => 'html'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals('created',$content['status']);

    $content = Content::byUid($content['uid'])->first() ;
    $rootFolder = Folder::root($content->account_id)->first();
    $this->assertEquals($rootFolder->_id,$content->folder_id);

    #GET
    $response = $this->call('GET', '/api/v1/elements/' . $content['uid']  , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content2 = json_decode($response->getContent(),true) ;
    foreach(['uid','href','active','name','body','updatedAt','createdAt','tmplType'] as $key){
      $this->assertTrue(array_key_exists($key,$content2)) ;
    }
    $this->assertEquals($content['uid'],$content2['uid']);
    $this->assertEquals(false,$content2['active']);
    $this->assertEquals('html',$content2['tmplType']);
    $this->assertEquals('new content',$content2['name']);

    #$content = Content::byUid(2)->first() ;
    #print_r($content->config->toArray()) ; die() ;
    $this->assertEquals( 'my_content' ,$content2['body']['content']);

    #create with incorrect
    $response = $this->call('POST', '/api/v1/elements' , ['name' => 'new content'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/elements' , ['tmplType' => 'html'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/elements' , ['name' => 'new content' , 'body' => ['content' => 'my_content' ],'tmplType' => 'not_exists'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());
  }

  public function testUpdate(){
    $response = $this->call('POST', '/api/v1/elements' , ['name' => 'new content' , 'body' => ['content' => 'my_content' ],'tmplType' => 'html'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $response = $this->call('PUT', '/api/v1/elements/' . $content['uid'] , ['name' => 'new content2' , 'body' => ['content' => 'my_content' ],'tmplType' => 'html' , 'active' => true] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content3 = json_decode($response->getContent(),true) ;
    $this->assertEquals('updated',$content3['status']);

    $response = $this->call('GET', '/api/v1/elements/' . $content['uid']  , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content2 = json_decode($response->getContent(),true) ;
    $this->assertEquals('new content2',$content2['name']);
    $this->assertEquals(true,$content2['active']);

    $response = $this->call('PUT', '/api/v1/elements/' . $content['uid'] , [  'body' => ['content' => 'my_content' ],'tmplType' => 'html' , 'active' => true] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('PUT', '/api/v1/elements/' . $content['uid'] , [ 'name' => 'new content2' ,  'body' => ['content' => 'my_content' ]  , 'active' => true] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('PUT', '/api/v1/elements/' . $content['uid'] , [ 'name' => 'new content2' ,  'body' => ['content' => 'my_content' ],'tmplType' => '----' , 'active' => true] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());
  }


  public function testDelete(){
    $response = $this->call('POST', '/api/v1/elements' , ['name' => 'new content' , 'body' => ['content' => 'my_content' ],'tmplType' => 'html'] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $response = $this->call('DELETE', '/api/v1/elements/' . $content['uid'] , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());

    $response = $this->call('GET', '/api/v1/elements/' . $content['uid']  , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $content = Content::byUid($content['uid'])->first() ;
    $this->assertEquals(1,$content->deleted );
  }
}
