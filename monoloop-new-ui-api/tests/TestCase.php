<?php

use App\Account;
use App\AccountUser;
use App\User;
use App\Placement ;
use App\Content ;
use App\ContentTemplate ;
use App\Folder ;
use App\Plugin ;
use App\Segment ;
use App\Goal;
use App\Tracker;
use App\Experiment;
use App\CustomField;
use App\PageElement;
use App\Job;
use App\Profile;
use App\ReportingArchiveAggregated ;

use App\Services\Hasher;
use App\Services\PageElement\ContentGenerator ;

class TestCase extends Illuminate\Foundation\Testing\TestCase {

  /**
   * Creates the application.
   *
   * @return \Illuminate\Foundation\Application
   */
  public function createApplication()
  {
    putenv('DB_DEFAULT=mongodb_testing');

    $app = require __DIR__.'/../bootstrap/app.php';

    $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

    $this->baseUrl = 'http://' . env('BASE_URL') ;

    return $app;
  }

  public function setUp()
  {
    parent::setUp();

    Account::truncate() ;
    User::truncate() ;
    AccountUser::truncate() ;
    ContentTemplate::truncate() ;
    Content::truncate() ;
    Folder::truncate() ;
    Plugin::truncate() ;
    Segment::truncate() ;
    Goal::truncate() ;
    Experiment::truncate();
    CustomField::truncate();
    PageElement::truncate();
    Job::truncate();
    Profile::truncate();
    ReportingArchiveAggregated::truncate();
    Tracker::truncate();

    factory(App\Plugin::class)->create();

    $account = factory(App\Account::class)->make();
    $account->_id = new MongoId("560ba220faba0d302c8b4592") ;
    $account->uid = Account::max('uid') + 1 ;
    $account->save();


    $account->plugins()->create(['enabled' => true , 'plugin_id' => new MongoId( Plugin::first()->_id ) ]);
    $account->domains()->create(['domain' => 'monoloop.com','url_privacy' => '','privacy_option' => 'auto','status' => 0]) ;
    $account->domains()->create(['domain' => 'url_test001.com','url_privacy' => '','privacy_option' => 'auto','status' => 0]) ;
    $account->domains()->create(['domain' => 'url_test002.com','url_privacy' => '','privacy_option' => 'auto','status' => 0]) ;

    #create root folder ;
    $folder = new folder() ;
    $folder->_id = new MongoId("561cc3d5faba0dec298b4e2b");
    $folder->cid = $account->uid;
    $folder->uid = -1 ;
    $folder->account_id = new MongoId($account->_id) ;
    $folder->deleted = 0 ;
    $folder->hidden = 0 ;
    $folder->title = 'root' ;
    $folder->pid = 0 ;
    $folder->parent_id = null ;
    $folder->save() ;




    $user = factory(App\User::class)->make();
    $user->active_account = new MongoId($account->_id) ;
    $user->save() ;

    $accountUser = factory(App\AccountUser::class)->make();
    $accountUser->account_id = (string)$account->_id;
    $accountUser->user_id = (string)$user->_id;
    $accountUser->is_monoloop_admin = 1;
    $accountUser->role = 'admin';
    $accountUser->save() ;

    #create test login user ;
    $user = factory(App\User::class)->make();
    $user->active_account = new MongoId($account->_id);
    $user->username = 'test_user';
    $hasher = new Hasher();
    $user->pass = $hasher->getHashedPassword('test_password');
    $user->save();

    $this->actingAs($user);

    $accountUser = factory(App\AccountUser::class)->make();
    $accountUser->account_id = (string)$account->_id ;
    $accountUser->user_id = (string)$user->_id ;
    $accountUser->is_monoloop_admin = 1 ;
    $accountUser->save() ;

    #create default element & tmltype
    if(ContentTemplate::where('uid',0)->count() == 0){
      #create defautl template
      $contentTemplate = new ContentTemplate(['uid' => 0 , 'name' => 'html' , 'description' => 'general html' , 'title' => 'html' , 'ds_uid' => 0 , 'account_id' => null,'data_structure'=>[['label'=>'html','el'=>'content','type'=>'text']]]) ;
      $contentTemplate->save() ;
      #create default element
      $element = new Content() ;
      $element->_id = new MongoId("560e47b6faba0df92b8b4589");
      $element->uid = ContentGenerator::newUid() ;
      $element->hidden = 0 ;
      $element->deleted = 0 ;
      $element->name = 'default element' ;
      $element->condition = '' ;
      $element->account_id =new MongoId( $account->_id )  ;
      $element->content_template_id = new MongoId($contentTemplate->_id);
      $element->type = 'complex' ;
      $element->config = ['content' => 'none']  ;
      $element->save() ;
    }

    #create default segment ;
    $segment = new Segment(['condition' => "if ( ( MonoloopProfile.VisitDuration == '12' ) ){|}" , 'condition_json' => '{"rules":[{"key":"VisitDuration","Label":"Length of visit","Group":"Current Visit","Type":"integer","qtipCfg"
:{"text":"The number of seconds of this visit."},"rules":[],"linkingLogicalOperator":"","uid":"732cad95a0ba4192a2480acdaa0bb40f"
,"hideConditionLogicalOperators":true,"binaryConditions":[{"name":"=="},{"name":"!="},{"name":"<"},{"name"
:">"},{"name":"<="},{"name":">="}],"binaryConditionsDefault":"==","value":"12","styles":{"and":{"line"
:{"top":149,"left":62,"height":30},"icon":{"top":149,"left":48}},"or":{"line":{"lineUpper":{"top":21
,"left":-8,"width":8},"lineMiddle":{"top":21,"left":-8,"height":179},"lineLower":{"top":200,"left":-8
,"width":8}},"icon":{"left":-19}}}}]}' , 'name' => 'name' ,  'description' => 'description' , 'hidden' =>  0 , 'deleted' => 0 ] ) ;
    $segment->account_id =new MongoId( $account->_id )  ;
    $segment->folder_id = new MongoId( $folder->_id ) ;
    $segment->uid = 1 ;
    $segment->cid = (int)$account->uid ;
    $segment->_id = new MongoId("562dcdeffaba0d811a8b4573");
    $segment->save() ;

    #create default goal
    $goal = new Goal(['condition' => "if ( ( MonoloopProfile.VisitDuration == '12' ) ){|}" , 'condition_json' => '{"rules":[{"key":"VisitDuration","Label":"Length of visit","Group":"Current Visit","Type":"integer","qtipCfg"
:{"text":"The number of seconds of this visit."},"rules":[],"linkingLogicalOperator":"","uid":"732cad95a0ba4192a2480acdaa0bb40f"
,"hideConditionLogicalOperators":true,"binaryConditions":[{"name":"=="},{"name":"!="},{"name":"<"},{"name"
:">"},{"name":"<="},{"name":">="}],"binaryConditionsDefault":"==","value":"12","styles":{"and":{"line"
:{"top":149,"left":62,"height":30},"icon":{"top":149,"left":48}},"or":{"line":{"lineUpper":{"top":21
,"left":-8,"width":8},"lineMiddle":{"top":21,"left":-8,"height":179},"lineLower":{"top":200,"left":-8
,"width":8}},"icon":{"left":-19}}}}]}' , 'name' => 'name' ,  'point' => 2 , 'type' => 'page_related' , 'hidden' =>  0 , 'deleted' => 0
] ) ;
    $goal->account_id =new MongoId( $account->_id )  ;
    $goal->folder_id = new MongoId( $folder->_id ) ;
    $goal->_id = new MongoId("561cc3d3faba0dec298b4827");
    $goal->uid = ContentGenerator::newUid() ;
    $goal->save() ;

    $goal->urlConfig()->create(array('url' => 'http://monoloop.com','reg_ex'=>'','inc_www'=>0,'inc_http_https'=>0 ,'url_option'=>0));

    #create default experiment
    $experiment = new Experiment();
    $experiment->experimentID = 1 ;
    $experiment->account_id =new MongoId( $account->_id )  ;
    $experiment->cid = (int)$account->uid;
    $experiment->segment_id = new MongoId( $segment->_id ) ;
    $experiment->goal_id = new MongoId( $goal->_id ) ;
    $experiment->name = 'name' ;
    $experiment->description = 'description' ;
    $experiment->folder_id =  new MongoId( $folder->_id ) ;
    $experiment->cg_size = 100 ;
    $experiment->cg_day = 1 ;
    $experiment->ordering = 1 ;
    $experiment->significant_action = 1 ;
    $experiment->hidden = 0 ;
    $experiment->deleted = 0 ;
    $experiment->_id = new MongoId("561cc3d5faba0dec298b4e33") ;
    $experiment->save() ;

    $customField = factory(App\CustomField::class)->make();
    $customField->account_id =new MongoId( $account->_id )  ;
    $customField->uid = 1 ;
    $customField->save();

    $this->clearCache($user->account->uid . '_segments') ;
  }

  public function getServerRequest(){
    $hasher = new Hasher() ;
    $account = Account::first() ;
    $server = [
      'PHP_AUTH_USER' => $account->uid ,
      'PHP_AUTH_PW' => $hasher->generateSign($account->uid)
    ];
    return $server;
  }

  public function tearDown()
  {
    $refl = new ReflectionObject($this);
    foreach ($refl->getProperties() as $prop) {
      if (!$prop->isStatic() && 0 !== strpos($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
        $prop->setAccessible(true);
        $prop->setValue($this, null);
      }
    }
  }

  public function clearCache($key){
    $client = new \Predis\Client();
    $client->del($key);
  }

  public function commonContext(){
    $opts = array(
      'http'=>array(
        'method'=>"GET",
        'header'=>"x-real-ip: 171.96.222.235\r\n" .
                  "x-longitude: 100.4667\r\n" .
                  "x-latitude: 13.7500\r\n" .
                  "x-continent: AS\r\n" .
                  "x-city: Bangkok\r\n" .
                  "x-country-code: TH\r\n"
      )
    );

    $context = stream_context_create($opts);
    return $context ;
  }



}
