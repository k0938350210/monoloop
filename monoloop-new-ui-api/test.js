{
    "type": "Program",
    "body": [
        {
            "type": "ExpressionStatement",
            "expression": {
                "type": "CallExpression",
                "callee": {
                    "type": "Identifier",
                    "name": "IF"
                },
                "arguments": [
                    {
                        "type": "LogicalExpression",
                        "operator": "&&",
                        "left": {
                            "type": "LogicalExpression",
                            "operator": "&&",
                            "left": {
                                "type": "BinaryExpression",
                                "operator": ">",
                                "left": {
                                    "type": "MemberExpression",
                                    "computed": false,
                                    "object": {
                                        "type": "Identifier",
                                        "name": "MonoloopProfile"
                                    },
                                    "property": {
                                        "type": "Identifier",
                                        "name": "PageViewCount"
                                    }
                                },
                                "right": {
                                    "type": "Literal",
                                    "value": "",
                                    "raw": "''"
                                }
                            },
                            "right": {
                                "type": "BinaryExpression",
                                "operator": "===",
                                "left": {
                                    "type": "MemberExpression",
                                    "computed": false,
                                    "object": {
                                        "type": "Identifier",
                                        "name": "MonoloopProfile"
                                    },
                                    "property": {
                                        "type": "Identifier",
                                        "name": "LastExitPage"
                                    }
                                },
                                "right": {
                                    "type": "Literal",
                                    "value": "",
                                    "raw": "''"
                                }
                            }
                        },
                        "right": {
                            "type": "BinaryExpression",
                            "operator": ">",
                            "left": {
                                "type": "MemberExpression",
                                "computed": false,
                                "object": {
                                    "type": "Identifier",
                                    "name": "MonoloopProfile"
                                },
                                "property": {
                                    "type": "Identifier",
                                    "name": "CurrentPageViewCount"
                                }
                            },
                            "right": {
                                "type": "Literal",
                                "value": "",
                                "raw": "''"
                            }
                        }
                    }
                ]
            }
        }
    ]
}
