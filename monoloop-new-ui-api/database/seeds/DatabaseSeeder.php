<?php

use Illuminate\Database\Seeder;
use Jenssegers\Mongodb\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		// $this->call('GuidelinesSeeder');
	}

}
