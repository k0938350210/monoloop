var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */
/*
elixir(function(mix) {
    mix.less('app.less');
});
*/
elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.sass('monoloop.scss' , 'public/css/monoloop.css');
    mix.sass('extjs3.scss' , 'public/css/extjs3.css');
});

//admin

var bowerDir = './bower_components/';

elixir(function(mix) {
    mix.copy(bowerDir + 'font-awesome/fonts', 'public/admin/fonts');
    mix.copy(bowerDir + 'bootstrap-sass/assets/fonts', 'public/admin/fonts');
    mix.sass('admin.scss' , 'public/admin/css/admin.css');
    mix.scripts([
            'jquery/dist/jquery.min.js',
            'bootstrap-sass/assets/javascripts/bootstrap.min.js',
            'PACE/pace.min.js',
            'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'
          ], 'public/admin/js/core.js', bowerDir);
    mix.scripts([
      'font-awesome/css/font-awesome.min.css',
      '../public/admin/css/admin.css',
      'bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css'
    ],'public/admin/css/core.css',bowerDir);


});

elixir(function(mix) {
    mix.version([
									'public/css/app.css',
									'public/css/monoloop.css',
									'public/css/webix.css',
									'public/webix/webix.css',
									'public/webix/filemanager/filemanager.css',
									'public/webui_popover/jquery.webui-popover.css',
									'public/js/tooltips.js',
									'public/js/guidelines.js',
									'public/webix/webix.js',
									'public/js/webix/layout/webix.js',
									'public/webui_popover/jquery.webui-popover.js',
									'public/webix/filemanager/filemanager.js',
									'public/js/jstz.min.js',
									'public/js/jquery.js',
									'public/js/jquery.backstretch.min.js',
									'public/admin/js/core.js',
									'public/js/validaion.js',
                  'public/webix/fonts/fontawesome-webfont.svg',
                  'public/webix/fonts/fontawesome-webfont.eot',
                  'public/webix/fonts/fontawesome-webfont.ttf',
                  'public/webix/fonts/fontawesome-webfont.woff',
                  'public/webix/fonts/PTS-bold.eot',
                  'public/webix/fonts/PTS-bold.ttf',
                  'public/webix/fonts/PTS-bold.woff',
                  'public/webix/fonts/PTS-webfont.eot',
                  'public/webix/fonts/PTS-webfont.ttf',
                  'public/webix/fonts/PTS-webfont.woff',
                  'public/ckeditor/ckeditor.js',
                  'public/js/webix/component/placement_window.js',
                  'public/conditionbuilder/css/bootstrap.min.css',
                  'public/conditionbuilder/css/styles.css',
                  'public/conditionbuilder/js/jquery.js',
                  'public/conditionbuilder/js/bootstrap.min.js',
                  'public/conditionbuilder/js/jqueryui.js',
                  'public/conditionbuilder/js/angular1218.js',
                  'public/conditionbuilder/js/angular-dragndrop.js',
                  'public/conditionbuilder/js/esprima.js',
                  'public/conditionbuilder/js/jsep.min.js',
                  'public/conditionbuilder/js/jquery.numeric.min.js',
                  'public/conditionbuilder/js/bootstrap-typeahead.js',
                  'public/conditionbuilder/js/init.js',
                  'public/js/underscore.js'
								]);
});
