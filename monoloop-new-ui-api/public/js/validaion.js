function validateLoginForm(){
  var flag=0;
  var username = $('#login-box').find('input#username').val();
  var passowrd = $('#login-box').find('input#password1').val();
  console.log(passowrd,username);
  $('#login-box').find('span#username').empty();
  $('#login-box').find('span#password').empty();
  $('#login-box').find('span#username').html("<div class='alert alert-danger'></div>");
  var a=$('#login-box').find('span#username').find("div.alert-danger");
    if (username == null || username == "") {
         a.append("Username must be filled out<br>");
          flag=1;
      }

      if (passowrd == null || passowrd == "") {
          a.append("Password must be filled out.<br>");
          flag=1;
      }
      if(flag){
        flag=0;
        return false;
      }else{
        $('#login-box').find('span#username').empty();
        return true;
      }
}
function validateResetForm(){
  var flag=0;
  var email = $('#login-box').find('input#email').val();

  $('#login-box').find('span#email').empty();
  $('#login-box').find('span#email').html("<div class='alert alert-danger'></div>");
  var a=$('#login-box').find('span#email').find("div.alert-danger");

  if (email == null || email == "" ) {
      a.append("Email must be filled out <br>");
      flag=1;
  }

  if(email&&!validateEmail(email)){
    a.append("Email is not valid <br>");
    flag=1;
  }
  if(flag){
    flag=0;
    return false;
  }else{
      $('#login-box').find('span#email').empty();
    return true;
  }

}
function validateSignupForm(){
  var flag=0;
  var username = $('#login-box').find('input#firstname').val();
  var lastname = $('#login-box').find('input#lastname').val();
  var email = $('#login-box').find('input#email').val();
  var cemail = $('#login-box').find('input#confirmemail').val();
  var password = $('#login-box').find('input#password').val();
  var cpassword = $('#login-box').find('input#confirmpassword').val();
  console.log(lastname,cemail,email);
  $('#login-box').find('span#username').empty();
  $('#login-box').find('span#lastname').empty();
  $('#login-box').find('span#email').empty();
  $('#login-box').find('span#cemail').empty();
  $('#login-box').find('span#password').empty();
  $('#login-box').find('span#cpassword').empty();
  $('#login-box').find('span#username').html("<div class='alert alert-danger'></div>");
  var a=$('#login-box').find('span#username').find("div.alert-danger");
      if (username == null || username == "") {
          a.append("First name must be filled out <br>");
          flag=1;
      }
      if (lastname == null || lastname == "") {
          a.append("Last name must be filled out. <br>");
          flag=1;
      }
      if (email == null || email == "" ) {
          a.append("Email must be filled out <br>");
          flag=1;
      }
      if (email == null || email == "" ) {
          a.append("Confirm email must be filled out <br>");
          flag=1;
      }
      if(email&&!validateEmail(email)){
        a.append("Email is not valid <br>");
        flag=1;
      }
      if(cemail&&!validateEmail(cemail)){
        a.append("Email is not valid <br>");
        flag=1;
      }
      if(email && cemail){
        email=validateEmail(email);
        cemail=validateEmail(cemail);

      if(email != cemail){
        a.append("Email Mismatched <br>");
        flag=1;
      }
    }



      if (password == null || password == "" ) {
          a.append("Password must be filled out <br>");
          flag=1;
      }

      if(password && cpassword){
          if(password != cpassword){
            a.append("Password Mismatched<br>");
            flag=1;
          }
    }

    if(flag){
      flag=0;
      return false;
    }else{
      $('#login-box').find('span#username').empty();
      return true;
    }
}
function validateEmail(sEmail) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
      if (filter.test(sEmail)) {
        return true;
      }else {
        return false;
    }
}
