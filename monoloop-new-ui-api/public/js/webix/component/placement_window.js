var parent_accessible = true ;
try{
  parent.document;
}catch(e){
  parent_accessible = false ;
}

if(parent_accessible){
  parent.placementWindowIframe = window;
}

var page = null ;
if(parent_accessible){
  page = parent.page;
}
var scriptNotExistsMsg = '<p style="padding: 0 10px;">This page does not have the Monoloop script in the page. This is required for Monoloop to work.<br /> Please copy the code from Account/Snippet section and place in the header section of your site template and try again.</p>';
var domainNotRegisteredMsg = '<p style="padding: 0 10px;">This domain is not registered. Please register this domain to edit this page.</p>';
var canWeConfirm = false;
var enableEditmode = true;
var enableTestMode = true ;
var iFrameConditionBuilderWin;
var IframeImageUploaderWin;
var customerSiteUrlLink = "";
var currentPageElement = {added: false, data: {}};
if(page === 'goal' || page === 'tracker'){
  enableEditmode = false;
  enableTestMode = false;
}
// console.log("page, enableEditmode => ",page, enableEditmode);
var isPWchanged = false;
var basicPlacementWindowFormValues = {};
var returnUrlConfig = {};
returnUrlConfig.url = "";
returnUrlConfig.url_option = 0;
returnUrlConfig.inc_www = 1;
returnUrlConfig.inc_http_https = 1;
returnUrlConfig.reg_ex = "";
returnUrlConfig.remark = "";
if(parent_accessible){
  parent.PWIFrameWin = window;
}

var testbench = {
isFirstTime : true ,
 fieldArray : {},
 mongoDetail: []
};

var defaultStepsPlacementWindowGoal = ['pw', 'edit_record','testbench', 'publish'];
var activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];

var currentStepPWGoal = 'pw';

var isPlacementWindowGeneratedGoal = false,
    isEditModeGeneratedGoal = false,
    isTestBenchGeneratedGoal = false,
    isPublishGeneratedGoal = false;

var pageExistingData = {
  variations: [],
  inlineContent: [],
  pageElement: false,
  originalUrl: "",
  reset: function(){
    this.variations = [];
    this.inlineContent = [];
    this.pageElement = false;
    this.originalUrl = "";
  }
};

var allFormDataPlacementWindowGoal = {};
allFormDataPlacementWindowGoal.data = {};


var urlHistoryPWGoal = [];
var indexUrlHistoyPWGoal = 0;
var currentUrlHosturyPWGoal = 0;
var traversingHistoryPWGoal = false;
var editPWUrl = '', testPWUrl = '', publishPWUrl = '';
var postMessageConfig = {
  stage: 0, // 0 init , 1 comfirm , 2 re-confirm , 3 - proxy
  url: ''
};

var fullscreen = {
  getHeight: function(){
    var height;
    if(activeStepPlacementWindowGoal === 'edit_record'){
      height = screen.innerHeight - 170;
    } else {
      height = screen.innerHeight - 220;
    }
    return height;
  },
  updateHeight: function(){
    $('.fullscreenMode .pw_divs').css('height', fullscreen.getHeight() + 'px');
  },
  full: function(){
    parent.postMessage(JSON.stringify({
          t: "pw-fullscreen-full"
        }), '*');


    $("#containerId").addClass('fullscreenMode');

    fullscreen.updateHeight();

    $$("fullscreen").hide();
    $$("fullscreen_exit").show();
  },
  exit: function(){

    parent.postMessage(JSON.stringify({
          t: "pw-fullscreen-exit"
        }), '*');

    $("#containerId").removeClass('fullscreenMode');

    $$("fullscreen").show();
    $$("fullscreen_exit").hide();
  }
}

//open domain registration popup
function registerDomain(){
  $("#innerContent").hide();
  $.getScript('/js/webix/page/account/domains.js', function() {
    openDomainPopup('new', undefined, undefined, false);
    $('#domain').val($('.webix_el_box>input').val());
    $('#url_privacy').val($('.webix_el_box>input').val());
  });
}

function xFrameCSPHeaderCheck(callback){
  var processResponse = function(response){
    var iFrameEmbeddEnabled = true;
    var xFrameHeader = response['X-Frame-Options'];
    if(xFrameHeader != undefined){
      if(Array.isArray(xFrameHeader)){
        iFrameEmbeddEnabled = !(xFrameHeader.indexOf('SAMEORIGIN') > -1);
      }else{
        iFrameEmbeddEnabled = !(xFrameHeader == 'SAMEORIGIN');
      }
    }

    if(iFrameEmbeddEnabled)
      callback();
    else{
      var isFirefox = typeof InstallTrigger !== 'undefined';
      var isChrome = !!window.chrome && !!window.chrome.webstore;

      var message = '';
      message +=  ' Your site seems to have X-Frame-Options or Content Security Policies (CSP) enabled. This setting block us from loading our user interface on top of your site. To solve this problem permanently, please check our documentation (link).';

      if(isChrome || isFirefox){
         message += '<br />';
         message += '  To Temporarily solve this now, please install this <a href=\"';
         if(isChrome){
           message +=  'https://goo.gl/SYSmvN';
         }else if(isFirefox){
           message += 'https://goo.gl/PMpMBr';
         }
         message += '\" target=\"_blank\">plugin</a> for your browser.';
         message += ' to enable this UI.';
      }
      webix.modalbox({
          title: 'Your site is blocking our user interface',
          buttons: ['OK'],
          text: message,
          width: '600px',
          callback: function(result){
          }
      });
    }
  }
  var value = $$('customerSiteUrlLink').getValue();
  value = refineUrl(value);
  if(is_valid_url(value)){
      value = refineUrl(value);
      var url = monolop_api_base_url + '/api/requester';
      $.post(
        url
        , { url: value}
        , function(response){
            if(response.success){processResponse(response.data);}
          }
      );
  }
}
// webix.ready(function() {
var placement_window_toolbar = webix.ui({
  container: "placement_window", //corresponds to the ID of the div block
  height: getPWHeight(160),
  css: "placementMenu",
  // width: 1242,
  rows: [{
    view: "toolbar",
    paddingY: 1,
    height: 56,
    id: "webixPlacementWindowWizardHeader",
    hidden: false,
    css: "webixWizardHeader placement_windowMenu pw_steps",
    elements: [
    //   {
    //   view: "button",
    //   id: "browseBtnPlacementWindowGoal",
    //   type: "iconButton",
    //   icon: "search",
    //   label: "Browse",
    //   css: 'pw_wizardBtn pw active',
    //   width: 150,
    //   click: function() {
    //     activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
    //     showViewPlacementWindowGoal(true, false, false, false, null, null, null);
    //     fullscreen.updateHeight();
    //   }
    // },
    {
      css: "searchBtnGroup",
      hidden : ! parent_accessible ,
      height: 40,
      cols: [
        {
          view: "text",
          id: 'customerSiteUrlLink',
          css: "browseSearchText",
          hidden : ! parent_accessible ,
          value: url,
          height: 40,
          // readonly: true,
          //width: 364,
          click: function(){

            stayOrLeavePage('textbox');


          }
        }, {
          view: "button",
          css: "browseSearchGo goBtn",
          hidden : ! parent_accessible ,
          label: "GO",
          id: "goBtnClick",
          height: 40,
          //width: 38,
          click: function() {
            xFrameCSPHeaderCheck(function(){
              stayOrLeavePage();
            });
          }
        }
      ]
    },
    {
      view: "button",
      id: "editRecordBtnPlacementWindowGoal",
      type: "iconButton",
      icon: "edit",
      label: "Edit",
      hidden: (enableEditmode === false) ? true : false,
      css: 'pw_wizardBtn edit_record',
      //width: 150,
      disabled: true,
      click: function() {
        $('#customerSiteUrlEditRecordSection').show();
        guidelines.removeHangedTooltips();

        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[1];
        showViewPlacementWindowGoal(false, true, false, false, null, null, null);
        fullscreen.updateHeight();
      }
    }, {
      view: "button",
      id: "testbenchRecordBtnPlacementWindowGoal",
      type: "iconButton",
      icon: "laptop",
      label: "Test",
      hidden: (enableTestMode === false) ? true : false,
      css: 'pw_wizardBtn testbench',
      //width: 150,
      disabled: true,
      click: function() {
        guidelines.removeHangedTooltips();
        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[2];
        showViewPlacementWindowGoal(false, false, true, false, null, null, null);
        fullscreen.updateHeight();
      }
    }, {
      view: "button",
      id: "publishBtnPlacementWindowGoal",
      type: "iconButton",
      icon: "laptop",
      label: (page == 'goal' || page == 'tracker') ? "Select" : "Save",
      hidden: false,
      css: 'pw_wizardBtn publish',
      //width: 150,
      disabled: true,
      click: function() {
        guidelines.removeHangedTooltips();
        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[3];
        if(page === 'tracker'){
          showTrackerConfig();
        }else{
          showViewPlacementWindowGoal(false, false, false, true, null, null, null);
        }

        fullscreen.updateHeight();
      }
    },

    {
      css : 'logout-link inside' ,
      id: "logoutLinkId",
      view : 'template' ,
      margin:0,
      hidden: (page === 'web') ? false : true,
      template:function(obj){
        return '<a href="javascript:void(0);" onclick="parent.storeSessionState();" title="Logout"><strong><i class="fa fa-power-off"></i></strong></a>' ;
      }
    },

    {
      view:"button",
      type: "image",
      image: monolop_base_url + "/images/fullscreen.png",
      //width: 46,
      css: "fullscreen screenicon",
      id: "fullscreen",
      click: fullscreen.full
    }, {
      view:"button",
      type: "image",
      image: monolop_base_url + "/images/fullscreen_exit.png",
      css: "fullscreen_exit screenicon",
      //width: 46,
      id: "fullscreen_exit",
      hidden: true,
      click: fullscreen.exit
    }
    // , {
    //   view:"icon",
    //   css: "pw_guideline_icon",
    //   icon:"fa-comment-o",
    //   tooltip: "Start guidelines for PW",
    //   width: 46,
    //   height: 46,
    //   click: function(){
    //     if(guidelines.open === false){
    //       guidelines.startGuideline('placementWindow', undefined, 'tip');
    //     }
    //   }
    // }
  ]
  },
  {
    css: 'pw_divs',
    template: "<div id='customerSiteUrlPlacementWindowGoalSection'></div><div id='customerSiteUrlEditRecordSection'></div><div id='customerSiteTestBenchSection'></div><div id='customerSitePublishSection'></div>"
  }]
});

// add guidelines icon into placement window header
var pwHeader = $('[view_id=webixPlacementWindowWizardHeader] div.webix_scroll_cont');

var stGd = document.createElement('i');
stGd.id = 'stGd';
stGd.title = 'Show Guides';
stGd.className = "webix_icon fa-comment-o";
stGd.style.position = 'relative';
stGd.style.marginTop = '1.4%';
stGd.style.marginLeft = '15%';
if(page == 'experiment' || page == 'goal'){
  stGd.style.left = '13%';
}
else{
  stGd.style.left = '8%';
}
stGd.style.fontSize = '1.2em';


var stGdToggle = document.createElement('i');
stGdToggle.id = 'toggleGuides';
stGdToggle.className = "webix_icon fa-toggle-on";
stGdToggle.style.position = 'relative';
stGdToggle.style.marginTop = '1.4%';
stGdToggle.style.marginLeft = '6.7%';
if(page == 'web')
  stGdToggle.style.marginLeft = '-7%';
if(page == 'experiment' || page == 'goal'){
  stGdToggle.style.right = '0%';
}else{
  stGdToggle.style.left = '10.5%';
}
stGdToggle.style.cursor = 'pointer';
stGdToggle.style.fontSize = '1.2em';
stGdToggle.title = 'Hide Guides';

stGdToggle.onclick = function(){
  if($('#toggleGuides').hasClass('fa-toggle-off')){
    $('#toggleGuides').removeClass('fa-toggle-off');
    $('#toggleGuides').addClass('fa-toggle-on');
    $('#toggleGuides').attr('title', 'Hide Guides');
    // Start guides
    if(guidelines.open === false) {
      switch(activeStepPlacementWindowGoal){
        case "edit_record":{
          parent.placementWindowIframe.postMessage(JSON.stringify({t: 'start-guideline', pageId: 'placementWindowEdit', url: postMessageConfig.url}), monolop_base_url);
          break;
        }
        case "testbench": {
          guidelines.page.pageId = 'placementWindowTest';
          guidelines.startGuideline('placementWindowTest', {placement: 'right'}, 'tip');
          break;
        }
        case "publish": {
          parent.placementWindowIframe.postMessage(JSON.stringify({t: 'start-guideline', pageId: 'placementWindowPublish', url: (monolop_base_url + '/component/page-list-publish?')}), monolop_base_url);
          break;
        }
        case "pw":
        default: {
          guidelines.page.pageId = 'placementWindow';
          guidelines.startGuideline('placementWindow', undefined, 'tip');
        }
      }
    }
  }
};

pwHeader.append(stGd, stGdToggle);



function initializeGuidesAndTipsOnPlacementWindow(mode){
    var _placementWindowMode = mode || '';
    // tooltips
    guidelines.page.pageId = ('placementWindow' + _placementWindowMode);
    // guidelines
    var _guideLinesURl = monolop_api_base_url+'/api/guidelines?page_id=' + guidelines.page.pageId;
    webix.ajax().get(_guideLinesURl, { }, function(text, xml, xhr){
      var r = JSON.parse(text);

      var cg = undefined;
      if(r.currentGuide){
        cg = JSON.parse(r.currentGuide);
        guidelines.startedBefore = true;
      }
      guidelines[guidelines.page.pageId] = r.guidelines;
      if(cg){
        if(cg.currentGuide){

          guidelines.page.currentGuide = cg.currentGuide;
        } else {
          guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
        }
      } else {
        guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
      }
      if(guidelines.page.currentGuide){
        // guidelines.generate(guidelines.page.currentGuide);
        if(guidelines.startedBefore === false){
          setTimeout(function(){guidelines.generate(guidelines.page.currentGuide);},2000);
        }else{
          if($('#toggleGuides').hasClass('fa-toggle-on')){
            $('#toggleGuides').removeClass('fa-toggle-on');
            $('#toggleGuides').addClass('fa-toggle-off');
            $('#toggleGuides').attr('title', 'Show Guides');
          }
        }
      }else{
        if($('#toggleGuides').hasClass('fa-toggle-on')){
          $('#toggleGuides').removeClass('fa-toggle-on');
          $('#toggleGuides').addClass('fa-toggle-off');
          $('#toggleGuides').attr('title', 'Show Guides');
        }
      }
      guidelines.startedBefore = false;
    });
}

initializeGuidesAndTipsOnPlacementWindow();
// guidelines.startGuideline('placementWindow', undefined, 'tip');
// submit form when enter key pressed

$('input').keypress(function(e) {
  if (e.which == 13) {
    // refine a valid URL
    if(is_valid_url($$('customerSiteUrlLink').getValue())){
      $$('customerSiteUrlLink').setValue(refineUrl($$('customerSiteUrlLink').getValue()));
      xFrameCSPHeaderCheck(function(){
        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
        showViewPlacementWindowGoal(true, false, false, false, null, null, null);
        openPlacementWindowGoalSection();
      });
    }
    return false; //<---- Add this line
  }
});
$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
       guidelines.saveInSession();
    }
});

function stayOrLeavePage(clickedOn){
  if(activeStepPlacementWindowGoal !== 'pw'){
    if(isPWchanged){
      if(basicPlacementWindowFormValues.source === 'default'){
        webix.confirm({
            title:"",
            width: 450,
            css: "leaveStayConfirm",
            ok:"Stay on page",
            cancel:"Leave the page",
            text:"Changes on page will be gone if you don't save them first. By leaving the page, new page wizard will be started.",
            callback: function(result){
              if(result === false){

                basicPlacementWindowFormValues.formData = {};

                currentPageElement.added = false;
                currentPageElement.data = {};

                pageExistingData.reset();

                activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
                showViewPlacementWindowGoal(true, false, false, false, null, null, null);

                if(isPlacementWindowGeneratedGoal === false){
                  openPlacementWindowGoalSection();
                }
              }
            }
        });
      } else {
        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
        showViewPlacementWindowGoal(true, false, false, false, null, null, null);
      }

    } else {
      activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
      showViewPlacementWindowGoal(true, false, false, false, null, null, null);
    }
  } else {
    activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
    showViewPlacementWindowGoal(true, false, false, false, null, null, null);
    if(clickedOn !== 'textbox'){
      openPlacementWindowGoalSection();
    } else {
      if(isPlacementWindowGeneratedGoal === false){
        openPlacementWindowGoalSection();
      }
    }
  }
}
//temp for test
var url = '';
var postMessageListener = function(event) {
  var data = JSON.parse(event.data);
  switch (data.t) {
    case "customization-occoured-in--user-site":
      if(data.isPWchanged){
        isPWchanged = true;
      }
      break;
    case "upload-image-required-message":
      webix.message({ type:"error", text:"Please upload an image first." });
      break;
    case "page-confirm-form-changed":
      if(data.formChanged){
        isPWchanged = true;
      }
      break;
    case "start-guideline":
      var pageId = data.pageId;
      var fetchGuidelinesUrl = '/api/guidelines?page_id='+pageId;
      $.get( fetchGuidelinesUrl, function( r ) {
        // var r = JSON.parse(r);


        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
        guidelines.removeHangedTooltips();
        iframe_edit.contentWindow.postMessage({
          t: "start-guideline-in-site",
          pageId: pageId,
          data: r
        }, data.url);


      });
      break;
    case "posted-from-monoloopui":
      if(new_url != 1){
        var c = readCookie('customerSiteUrlLink');
        if(c){
          $$("customerSiteUrlLink").setValue(c);
          eraseCookie('customerSiteUrlLink');
        }
      }

      xFrameCSPHeaderCheck(function(){
        basicPlacementWindowFormValues = data.data;

        var proceedWithPW = function(){
          allFormDataPlacementWindowGoal.data.url = $$("customerSiteUrlLink").getValue();
          // console.log("basicPlacementWindowFormValues.type",basicPlacementWindowFormValues.type);
          if(basicPlacementWindowFormValues.type !== undefined && basicPlacementWindowFormValues.type === 'publish'){
            //console.log('n-1');
            openPublishGoalSection();
          // } else if( ['web', 'experiment'].indexOf(basicPlacementWindowFormValues.page) !== -1 && basicPlacementWindowFormValues.mode === 'edit'){
          } else if(basicPlacementWindowFormValues.type !== undefined && basicPlacementWindowFormValues.type === 'edit_record'){
            //console.log('n-2');
            openEditRecordPlacementWindowGoalSection();
          } else if(basicPlacementWindowFormValues.type !== undefined && basicPlacementWindowFormValues.type === 'testbench'){
            //console.log('n-3');
            openTestRecordPlacementWindowGoalSection();
          } else {
            //console.log('n-4');
            openPlacementWindowGoalSection();
          }
        };

        if(basicPlacementWindowFormValues.source == 'experiment' && basicPlacementWindowFormValues.source.experiment){
          if(basicPlacementWindowFormValues.experiment.tempSegment){
            var segmentURL = monolop_api_base_url+ '/api/segments/show';
            webix.ajax().get(segmentURL, {
              _id: '__' + basicPlacementWindowFormValues.experiment.tempSegment
            }, function(rawResponse){
              basicPlacementWindowFormValues.experiment._segment = JSON.parse(rawResponse);
              proceedWithPW();
            });
          }else{ proceedWithPW();}
        }else{ proceedWithPW();}
      });

      break;
    case "page-published":
      currentPageElement.added = true;
      currentPageElement.data = data.pageElement;
      basicPlacementWindowFormValues.formData = data.pageElement;
      basicPlacementWindowFormValues.fullURL = data.pageElement.originalUrl;
      if(basicPlacementWindowFormValues.source === 'experiment'){
        basicPlacementWindowFormValues.experiment = data.experiment;
      }
      parent.postMessage(JSON.stringify({t: 'set-current-experiment', created: true, source: basicPlacementWindowFormValues.source, experiment: basicPlacementWindowFormValues.experiment}), '*');
      parent.postMessage(JSON.stringify({t: 'page-published' + '-' + page, source: data.source, pageElement: data.pageElement, temp: data.temp, message: data.message}), '*');
      break;
    case "set-current-pageElement":
        currentPageElement.added = true;
        currentPageElement.data = data.data.pageElement;
        basicPlacementWindowFormValues.formData = data.data.pageElement;
        basicPlacementWindowFormValues.fullURL = data.data.pageElement.originalUrl;
        if(basicPlacementWindowFormValues.source === 'experiment'){
          basicPlacementWindowFormValues.experiment = data.data.experiment;
          parent.postMessage(JSON.stringify({t: 'set-current-experiment', created: true, source: basicPlacementWindowFormValues.source, experiment: basicPlacementWindowFormValues.experiment}), monolop_base_url);
        }
        break;
    case 'confirm':
        //set url in text box
      $$("customerSiteUrlLink").setValue(data.url);
      if(parent_accessible){
        checkClientProtocal();
      }
      allFormDataPlacementWindowGoal.data.url = data.url;
      $$("customerSiteUrlLink").setValue(data.displayURL);
      if (!traversingHistoryPWGoal) {
        addnewHistoryUrlGoal(data.url);
      }
      traversingHistoryPWGoal = false;
      //set found invocation
      postMessageConfig.stage = 1;
      postMessageConfig.url = data.url;
      //send message for load editor-bundle
      if(currentStepPWGoal === 'pw'){
        var iframe = $('iframe')[0];
        var time = new Date().getTime();
        iframe.contentWindow.postMessage(JSON.stringify({
          t: "loadbundle",
          domain: monolop_base_url + '/',
          url: monolop_base_url + '/js/invocation/editor-bundle.js?' + time
        }), postMessageConfig.url);
      }else if(currentStepPWGoal == 'testbench'){
        $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().contentWindow.postMessage(JSON.stringify({
          t: "loadbundle",
          domain: monolop_base_url + '/',
          url: monolop_base_url + '/js/invocation/editor-bundle.js?' + time
        }), postMessageConfig.url);
      } else if(currentStepPWGoal == 'edit_record'){
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
          iframe_edit.contentWindow.postMessage(JSON.stringify({
            t: "loadbundle",
            domain: monolop_base_url + '/',
            url: monolop_base_url + '/js/invocation/editor-bundle.js?' + time
          }), postMessageConfig.url);
      }

      break;
    case 'loadbundle-success':
      // console.log('pw-loadbundle-success');
      if(currentStepPWGoal === 'pw'){
        var iframe = $('iframe')[0];
        iframe.contentWindow.postMessage(JSON.stringify({
          t: "init-browse"
        }), postMessageConfig.url);
      }else if(currentStepPWGoal == 'testbench'){

        $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().contentWindow.postMessage(JSON.stringify({
          t: "init-testmode"
        }), postMessageConfig.url);
      }else {
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
        iframe_edit.contentWindow.postMessage(JSON.stringify({
          t: "init-selectmode"
        }), postMessageConfig.url);
      }

      break;
    case 'set-stored-values':
      $$("customerSiteUrlLink").setValue(data.data.fullURL);
      allFormDataPlacementWindowGoal.data = data;
      basicPlacementWindowFormValues = data.data;
      if (data.url !== '') {
        openPlacementWindowGoalSection();
      }
      break;
    case 'open-condition-builder':
      openConditionBuilder(data.condition, data.type);
      break;
    case 'open-image-uploader':
      openImageUploader(data.data);
      break;
    case 'set-new-inline-variations':
      postInlineContentVariations();
      break;
    case "set-current-select-element-on-page":
      if(parent_accessible){
        if(parent.webPageListConfig){
          parent.webPageListConfig.placementWindow.edit_record = data.data;
        }
        if(parent.experimentListConfig){
          parent.experimentListConfig.placementWindow.edit_record = data.data;
        }
      }
      break;
    case "save-guides-in-session":
      guidelines.page.pageId = data.data.pageId;
      guidelines.page.actionID = data.data.actionID;
      guidelines.page.currentGuide = data.data.currentGuide;
      guidelines.saveInSession();
      break;
    case "save-changed-content-while-editing":
      $.ajax({
         url: monolop_api_base_url+"/api/page-element/content-update",
         type: "POST",
         data: data.data,
         beforeSend: function(xhr){
           xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
           xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
         },
       })
         .done(function( response ) {
           if (response.status === 'success') {
             webix.message("Variation has been updated!");
           } else {
             webix.alert("An error occoured, please refreh page and try again");
           }
         });
      break;
    case "load-uploaded-image-ckeditor":
      alert(data.t);
      break;
    case "add-element-content-variation":
      break;
    case "update-element-content-variation":
      break;
    case "edit-content-element-variation":
      break;
    case "get-page-data":
      var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
      var iframe_edit = $('iframe')[index];
      var userStateData = null ;
      if(parent_accessible){
        userStateData = parent.userStateData ;
      }
      iframe_edit.contentWindow.postMessage({
        t: "set-changed-content",
        originalUrl: pageExistingData.originalUrl,
        inlineContent: pageExistingData.inlineContent,
        pageElement: pageExistingData.pageElement,
        experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
        source: basicPlacementWindowFormValues.source,
        page: page,
        variations: pageExistingData.variations,
        userStateData: userStateData
      }, allFormDataPlacementWindowGoal.data.url);
      break;
    case "send-webix-message":
      webix.message(data.message);
      break;
    case 'test-allassetloadcomplete' :
      // Process testbench fields
      if(testbench.isFirstTime === false){
        processTestBenchData() ;
        return ;
      }
      $.ajax({
         url: monolop_api_base_url+"/api/page-element/testbench",
         type: "GET",
         data: { url: $$("customerSiteUrlLink").getValue() },
         beforeSend: function( xhr ) {
           xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
           xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
         }
       })
       .done(function( response ) {
         if( response.success == true){
           testbench.mongoDetail = response.mongoDetail ;
           generateTestBenchPanel(response.testbench) ;
           processTestBenchData() ;
         }
       });
      break;

    /*---- from monoloop thrid party extension ---*/
    case 'remove-wordpress-admin-bar' :
      setTimeout(function(){
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
        iframe_edit.contentWindow.postMessage({
          t: "remove-wordpress-admin-bar" ,
          data:{}
        }, postMessageConfig.url);
      }, 2000);
   case "resize-toolbar":
      webix.toNode("placement_window").style.width = data.data.width+"px";
      placement_window_toolbar.adjust();
    break;
  case "set-add-goal-url":
    $('div[view_id="customerSiteUrlLink"] input').val(data.data.fullURL);
    break;
  }
};
// insert postmessageListener ;
if (window.addEventListener) {
  addEventListener("message", postMessageListener, false);
} else {
  attachEvent("onmessage", postMessageListener);
}

function highLightCurrentPlacementWindowGoal(current) {
  $(".pw_wizardBtn").removeClass('active');
  switch (current) {
    case "pw":
      $(".pw_wizardBtn.pw").addClass('active');
      break;
    case "edit_record":
      $(".pw_wizardBtn.edit_record").addClass('active');
      break;
    case "testbench":
      $(".pw_wizardBtn.testbench").addClass('active');
      break;
    case "publish":
      $(".pw_wizardBtn.publish").addClass('active');
      break;
    default:
      break;
  }
}

function showViewPlacementWindowGoal(placementWindowGoal, editRecordGoal, testBench, publish, modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal) {
  if (isPlacementWindowGeneratedGoal) {
    if (placementWindowGoal) {
      highLightCurrentPlacementWindowGoal("pw");
      $('#customerSiteUrlPlacementWindowGoalSection').show();
      // $$("webixPlacementWindowSearchHeader").show();
    } else {
      $('#customerSiteUrlPlacementWindowGoalSection').hide();
      // $$("webixPlacementWindowSearchHeader").hide();
    }
  }
  if (isEditModeGeneratedGoal) {
    if (editRecordGoal) {
      if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
        highLightCurrentPlacementWindowGoal("edit_record");
        openEditRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
        $$("webixWizardEditRecordGoalId").show();
        initializeGuidesAndTipsOnPlacementWindow('Edit');
        // $$("webixPlacementWindowSearchHeader").hide();
      } else {
        webix.message("Enter the URL and press go.");
      }
      // $$("webixPlacementWindowSearchHeader").hide();
    } else {
      $("#customerSiteUrlEditRecordSection").hide();
    }
  } else {
    if (editRecordGoal) {
      if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
        // $$("webixPlacementWindowSearchHeader").hide();
        openEditRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
        // start placementWindowEdit Guidelines
        //guidelines.startGuideline('placementWindowEdit', undefined, 'tip');
        initializeGuidesAndTipsOnPlacementWindow('Edit');
      } else {
        webix.message("Enter the URL and press go.");
      }
    }
  }

  if (isTestBenchGeneratedGoal) {
    if (testBench) {
      if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
        highLightCurrentPlacementWindowGoal("testbench");
        openTestRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
        $$("webixWizardEditRecordGoalId").show();
        // $$("webixPlacementWindowSearchHeader").hide();
        initializeGuidesAndTipsOnPlacementWindow('Test');
      } else {
        webix.message("Enter the URL and press go.");
      }

      // $$("webixPlacementWindowSearchHeader").hide();
    } else {
      $("#customerSiteTestBenchSection").html("");
    }
  } else {
    if (testBench) {
      if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
        // $$("webixPlacementWindowSearchHeader").hide();
        openTestRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
        // start placementWindowTest Guidelines
        // guidelines.startGuideline('placementWindowTest', undefined, 'tip');
        initializeGuidesAndTipsOnPlacementWindow('Test');
      } else {
        webix.message("Enter the URL and press go.");
      }
    }
  }

  if (isPublishGeneratedGoal) {
    if (publish) {
      if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
        highLightCurrentPlacementWindowGoal("publish");
        openPublishGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
        $$("webixWizardPublishGoalId").show();
        initializeGuidesAndTipsOnPlacementWindow('Publish');
        // $$("webixPlacementWindowSearchHeader").hide();
        // start placementWindowTest Guidelines
        // guidelines.startGuideline('placementWindowPublish', undefined, 'tip');
      } else {
        webix.message("Enter the URL and press go.");
      }
      // $$("webixPlacementWindowSearchHeader").hide();
    } else {
      $("#customerSitePublishSection").html("");
    }
  } else {
    if (publish) {
      if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
        // $$("webixPlacementWindowSearchHeader").hide();
        openPublishGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
        initializeGuidesAndTipsOnPlacementWindow('Publish');
      } else {
        webix.message("Enter the URL and press go.");
      }
    }
  }

}

function showViewPlacementWindowTestbench(){
  if(isPlacementWindowGeneratedGoal === false){
    webix.message("Enter the URL and press go.");
    return ;
  }
  highLightCurrentPlacementWindowGoal("testbench");
  $('#customerSiteUrlPlacementWindowGoalSection').hide();
  // $$("webixPlacementWindowSearchHeader").hide();

  openTestRecordPlacementWindowGoalSection();

  testbench.isFirstTime = true ;
}
function getPWHeight(defH){
  defH = defH || 230;
  var h = 990;
  if(h < window.innerHeight){
    h = window.innerHeight;
  }
  // h = h - 49 - 20 - 65 - 55;
  h = h - defH;
  return h;
}

function resizeIframe() {
   $("#main div").css('width', '100%');
    var $iframes = $("iframe");
    $iframes.each(function() {
        var iframe = this;
        $(iframe).load(function() {
            iframe.height = iframe.contentWindow.document.body.scrollHeight + 35;
        });
    });
}


function openPlacementWindowGoalSection() {
  //console.log('n-openPlacementWindowGoalSection');
  if(parent_accessible){
    checkClientProtocalForHttps();
  }
  fullscreen.updateHeight();
  currentStepPWGoal = 'pw';
  highLightCurrentPlacementWindowGoal("pw");
  $("#customerSiteUrlPlacementWindowGoalSection").html('');
  // $$("webixPlacementWindowSearchHeader").show();

  isPlacementWindowGeneratedGoal = true;

  var linkPlacementWindow = $$("customerSiteUrlLink").getValue();
  customerSiteUrlLink = linkPlacementWindow;
  linkPlacementWindow  = linkPlacementWindow.trim();
  linkPlacementWindow = refineUrl(linkPlacementWindow);
  // if(linkPlacementWindow.indexOf('?') === -1){
  //   linkPlacementWindow += '?';
  // } else {
  //   linkPlacementWindow += '&';
  // }
  // linkPlacementWindow += 'ver=' + (new Date()).getTime() + Math.floor(Math.random() * 1000000);

  if (is_valid_url(linkPlacementWindow)) {
    $('#customerSiteUrlEditRecordSection').html('');
    webix.ui({
      container: "customerSiteUrlPlacementWindowGoalSection",
      id: "webixWizardPlacementWindowGoalId",
      height: getPWHeight(0),
      //height: window.innerHeight,
      //height: 1990,
      css: "customerSiteUrlPlacementWindowGoalSection",
      rows: [{
        view: "iframe",
        id: "PWIframePlacementWindowGoal",
        src: linkPlacementWindow,
        css: "pw_iframe",
        borderless: true,
        on: {
          onAfterLoad: function() {
            $('#customerSiteUrlEditRecordSection').html('');
            var view = this;
            //resizeIframe();
            //console.log(this);
            customerSiteUrlLink = $$("customerSiteUrlLink").getValue();
            parent.postMessage(JSON.stringify({
              t: "set-active-step-sessional",
              step: "pw",
              url: customerSiteUrlLink,
              data: allFormDataPlacementWindowGoal.data
            }), "*");

            if (postMessageConfig.stage == 1) {
              //found comfirm
              canWeConfirm = true;
              //change to re-confirm
              postMessageConfig.stage = 2;
              enableEditActions();
            } else {
              //delay 1 second
              setTimeout(function() {
                canWeConfirm = false;
                disableEditActions();
                if (postMessageConfig.stage != 1) {
                  //seem we not found invocation
                  //change to proxy mode ;
                  // webix.alert(scriptNotExistsMsg);
                  // $('iframe')[0].src =  monolop_base_url + '/placement-proxy/preview?l=' +  urlencode($('iframe')[0].src);
                  postMessageConfig.stage = 3;

                } else {
                }
              }, 1000);
              if (!traversingHistoryPWGoal) {
                addnewHistoryUrlGoal(customerSiteUrlLink);
              }
              traversingHistoryPWGoal = false;
              allFormDataPlacementWindowGoal.data.url = customerSiteUrlLink;

            }

            //edit record section
            if(enableEditmode){
              webix.ui({
                container: "customerSiteUrlEditRecordSection",
                css: "customerSiteUrlEditRecordSection",
                id: "webixWizardEditRecordGoalId",
                // height: screen.height,
                height: getPWHeight(0),
                borderless: true,
                rows: [{
                  view: "iframe",
                  id: "PWIframePlacementWindowEditModeGoal",
                  src: $$("customerSiteUrlLink").getValue(),
                  css: "pw_iframe PWIframePlacementWindowEditModeGoal",
                  borderless: true,
                  on: {
                    onAfterLoad: function() {
                      $('#customerSiteUrlEditRecordSection').hide();
                    }
                  }
                }]
              });
            }
          }
        }
      }]
    });

  } else {
    webix.message("Please enter the valid URL to browse.");
  }
  // $$("browseBtnPlacementWindowGoal").enable();

}
function enableEditActions(){
  $$("editRecordBtnPlacementWindowGoal").enable();
  $$("testbenchRecordBtnPlacementWindowGoal").enable();
  $$("publishBtnPlacementWindowGoal").enable();

  // mantis: 3624
  if(page == 'goal'){
    $('div[view_id=publishBtnPlacementWindowGoal]').css('background-color', 'green');
  }
}
function disableEditActions(){
  $$("editRecordBtnPlacementWindowGoal").enable();
  $$("testbenchRecordBtnPlacementWindowGoal").disable();
  $$("publishBtnPlacementWindowGoal").disable();
}
function postInlineContentVariations(){
  var urlParams = '';
  // checking if opening up in experiments or not
  if(basicPlacementWindowFormValues.source !== undefined){
    urlParams += 'source=' + basicPlacementWindowFormValues.source + '&';
  }
  if(basicPlacementWindowFormValues.pw !== undefined){
    if(basicPlacementWindowFormValues.pw.mode !== undefined){
      urlParams += 'mode=' + basicPlacementWindowFormValues.pw.mode + '&';
    }
  }
  if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null){
    if(basicPlacementWindowFormValues.formData._id){
      urlParams += 'page_element_id=' + basicPlacementWindowFormValues.formData._id;
    }
  }

  // $.get( monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams, );
  $.ajax({
    type: 'GET',
    url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams,
    beforeSend: function(xhr){
      xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
      xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
    },
    success: function( response ) {
        // response = JSON.parse(response);

        var inlineContent = {},
            pageElement = false,
            variations = [],
            originalUrl= ""  ;
        if(response.status == 200){

          if(response.found === true){
            pageExistingData.inlineContent = inlineContent = response.inlineContent;
            pageExistingData.pageElement = pageElement = response.pageElement;
            pageExistingData.variations = variations = response.variations;

            pageExistingData.originalUrl = originalUrl = response.originalUrl;
          }else{
            pageExistingData = {
              variations: [],
              inlineContent: [],
              pageElement: false,
              originalUrl: "",
            }
          }
        }
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
        var userStateData = null ;
        if(parent_accessible){
          userStateData = parent.userStateData ;
        }
        iframe_edit.contentWindow.postMessage({
          t: "set-changed-content",
          originalUrl: originalUrl,
          source: basicPlacementWindowFormValues.source,
          inlineContent: inlineContent,
          pageElement: pageElement,
          variations: variations,
          experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
          currentFolder: basicPlacementWindowFormValues.currentFolder,
          page: page,
          updateFab: false,
          userStateData: userStateData
        }, allFormDataPlacementWindowGoal.data.url);
      }
  });
}
function openEditRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal) {
  //console.log('n-openEditRecordPlacementWindowGoalSection');
  if(parent_accessible){
    checkClientProtocalForHttps();
  }
  fullscreen.updateHeight();
  var urlParams = '';
  // checking if opening up in experiments or not
  if(basicPlacementWindowFormValues.source !== undefined){
    urlParams += 'source=' + basicPlacementWindowFormValues.source + '&';
  }
  if(basicPlacementWindowFormValues.pw !== undefined){
    if(basicPlacementWindowFormValues.pw.mode !== undefined){
      urlParams += 'mode=' + basicPlacementWindowFormValues.pw.mode + '&';
    }
  }
  if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null){
    if(basicPlacementWindowFormValues.formData._id){
      urlParams += 'page_element_id=' + basicPlacementWindowFormValues.formData._id;
    }
  }

  currentStepPWGoal = 'edit_record';
  highLightCurrentPlacementWindowGoal("edit_record");
  // comment line below due to mantis#4103
  // $("#customerSiteUrlEditRecordSection").html("");

  // $$("webixPlacementWindowSearchHeader").hide();
  isEditModeGeneratedGoal = true;
  $.ajax({
    type: 'GET',
    url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams,
    beforeSend: function(xhr){
      xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
      xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
    },
    success: function( response ) {
        // response = JSON.parse(response);
        // console.log(response);
        var inlineContent = {},
            pageElement = false,
            variations = [],
            is_domain_registered = response.is_domain_registered,
            originalUrl= ""  ;
        if(response.status == 200){

          if(response.found === true){
            pageExistingData.inlineContent = inlineContent = response.inlineContent;
            pageExistingData.pageElement = pageElement = response.pageElement;
            pageExistingData.variations = variations = response.variations;

            pageExistingData.originalUrl = originalUrl = response.originalUrl;
          }else{
            pageExistingData = {
              variations: [],
              inlineContent: [],
              pageElement: false,
              originalUrl: "",
            }
          }
        }
        var iframeURL = allFormDataPlacementWindowGoal.data.url;
        if(parent_accessible){
          if(parent.isSessional && parent.userStateData){
            if(parent.userStateData.data.url){
              iframeURL = parent.userStateData.data.url;
            }
          }
        }
        iframeURL  = iframeURL.trim();
        iframeURL = refineUrl(iframeURL);

        if($("#customerSiteUrlEditRecordSection").is(':empty')){

          webix.ui({
            container: "customerSiteUrlEditRecordSection",
            css: "customerSiteUrlEditRecordSection",
            id: "webixWizardEditRecordGoalId",
            // height: screen.height,
            height: getPWHeight(0),
            borderless: true,
            rows: [{
              view: "iframe",
              id: "PWIframePlacementWindowEditModeGoal",
              src: iframeURL,
              css: "pw_iframe PWIframePlacementWindowEditModeGoal",
              borderless: true,
              on: {
                onAfterLoad: function() {
                  if(is_domain_registered === true){
                    var view = this;
                    if (postMessageConfig.stage == 1 || canWeConfirm == true) {
                      //found comfirm
                      enableEditActions();
                      //change to re-confirm
                      postMessageConfig.stage = 2;

                      parent.postMessage(JSON.stringify({
                        t: "set-active-step-sessional",
                        step: "edit_record",
                        url: iframeURL
                      }), "*");

                      var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                      var iframe_edit = $('iframe')[index];
                      var userStateData = null ;
                      if(parent_accessible){
                        userStateData = parent.userStateData ;
                      }
                      iframe_edit.contentWindow.postMessage({
                        t: "set-changed-content",
                        originalUrl: originalUrl,
                        source: basicPlacementWindowFormValues.source,
                        experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
                        inlineContent: inlineContent,
                        pageElement: pageElement,
                        page: page,
                        currentFolder: basicPlacementWindowFormValues.currentFolder,
                        variations: variations,
                        userStateData: userStateData
                      }, allFormDataPlacementWindowGoal.data.url);



                      var pageId = 'placementWindowEdit';
                      var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+pageId;

                      $.ajax({
                        type: 'GET',
                        url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams,
                        beforeSend: function(xhr){
                          xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
                          xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
                        },
                        success: function(r){
                          // r = JSON.parse(r);
                          var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                          var iframe_edit = $('iframe')[index];
                          setTimeout(function(){
                            iframe_edit.contentWindow.postMessage({
                              t: "start-guideline-in-site",
                              pageId: pageId,
                              data: r
                            }, postMessageConfig.url);
                          }, 2000);
                        }
                      });

                    } else {
                      //delay 1 second
                      webix.alert(scriptNotExistsMsg);
                      postMessageConfig.stage = 3;

                      if (!traversingHistoryPWGoal) {
                        addnewHistoryUrlGoal($$("customerSiteUrlLink").getValue());
                      }
                      traversingHistoryPWGoal = false;
                      allFormDataPlacementWindowGoal.data.url = $$("customerSiteUrlLink").getValue();
                      // console.log("iframe_edit", iframe_edit);
                      // iframe_edit.contentWindow.CKEDITOR.tools.callFunction(data.CKEditorFuncNum, data.url, data.msg);
                    }
                  } else {
                    webix.confirm({
                        title: "Confirmation", // the text of the box header
                        ok: "Register Now",
                        cancel: "Cancel",
                        text: domainNotRegisteredMsg,
                        callback: function(result) {
                            if (result) {
                                registerDomain();
                            }
                        }
                    });
                  }



                }
              }
            }]
          });

        }else{
          if(is_domain_registered === true){
            var view = this;
            if (postMessageConfig.stage == 1 || canWeConfirm == true) {

              var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
              var iframe_edit = $('iframe')[index];
              var time = new Date().getTime();

              iframe_edit.contentWindow.postMessage(JSON.stringify({
                t: "loadbundle",
                domain: monolop_base_url + '/',
                url: monolop_base_url + '/js/invocation/editor-bundle.js?' + time
              }), postMessageConfig.url);
              //found comfirm
              enableEditActions();
              //change to re-confirm
              postMessageConfig.stage = 2;

              parent.postMessage(JSON.stringify({
                t: "set-active-step-sessional",
                step: "edit_record",
                url: iframeURL
              }), "*");

              var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
              var iframe_edit = $('iframe')[index];
              var userStateData = null ;
              if(parent_accessible){
                userStateData = parent.userStateData ;
              }
              iframe_edit.contentWindow.postMessage({
                t: "set-changed-content",
                originalUrl: originalUrl,
                source: basicPlacementWindowFormValues.source,
                experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
                inlineContent: inlineContent,
                pageElement: pageElement,
                page: page,
                currentFolder: basicPlacementWindowFormValues.currentFolder,
                variations: variations,
                userStateData: userStateData
              }, allFormDataPlacementWindowGoal.data.url);





              var pageId = 'placementWindowEdit';
              var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+pageId;

              $.ajax({
                type: 'GET',
                url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams,
                beforeSend: function(xhr){
                  xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
                  xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
                },
                success: function(r){
                  // r = JSON.parse(r);
                  var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                  var iframe_edit = $('iframe')[index];
                  setTimeout(function(){
                    iframe_edit.contentWindow.postMessage({
                      t: "start-guideline-in-site",
                      pageId: pageId,
                      data: r
                    }, postMessageConfig.url);
                  }, 2000);
                }
              });

            } else {
              //delay 1 second
              webix.alert(scriptNotExistsMsg);
              postMessageConfig.stage = 3;

              if (!traversingHistoryPWGoal) {
                addnewHistoryUrlGoal($$("customerSiteUrlLink").getValue());
              }
              traversingHistoryPWGoal = false;
              allFormDataPlacementWindowGoal.data.url = $$("customerSiteUrlLink").getValue();
              // console.log("iframe_edit", iframe_edit);
              // iframe_edit.contentWindow.CKEDITOR.tools.callFunction(data.CKEditorFuncNum, data.url, data.msg);
            }
          } else {
            webix.confirm({
                title: "Confirmation", // the text of the box header
                ok: "Register Now",
                cancel: "Cancel",
                text: domainNotRegisteredMsg,
                callback: function(result) {
                    if (result) {
                        registerDomain();
                    }
                }
            });
          }

        }

      }
  });
  $$("editRecordBtnPlacementWindowGoal").enable();
}


openTestRecordPlacementWindowGoalSection = function(){
  //console.log('n-openTestRecordtWindowGoalSection');
  if(parent_accessible){
    checkClientProtocalForHttps();
  }
  window.testbench.isFirstTime = true ;
  fullscreen.updateHeight();
  currentStepPWGoal = 'testbench';
  highLightCurrentPlacementWindowGoal("testbench");
  isTestBenchGeneratedGoal = true;
  $("#customerSiteTestBenchSection").html("");
  //$("testbench-panel").html("");

  var iframeURL = allFormDataPlacementWindowGoal.data.url;
  if(parent_accessible){
    if(parent.isSessional && parent.userStateData){
      if(parent.userStateData.data.url){
        iframeURL = parent.userStateData.data.url;
      }
    }
  }
  webix.ui({
    container: "customerSiteTestBenchSection",
    css: "customerSiteUrlEditRecordSection",
    id: "webixWizardTestRecordGoalId",
    // height: screen.height,
    height: getPWHeight(0),
    borderless: true,
    cols: [{
      type : 'head' ,
      width : 300 ,
      rows :[
        {
          id: 'testbench-panel',
          padding:10,
          multi:true,
          view:"accordion",
          height : getPWHeight(0) - 120 ,
          css:'placement-testbench-panel',
          rows: []
        },{
          header : 'Static Link',
          body : {
            padding : 10 ,
            rows :[{
              view : 'text' ,
              id : 'static-link-textbox'
            },{
              view : 'label' ,
              label : '<button id="testbench-preview-clipboard-btn" data-clipboard-target="#static-link-textbox-inner" class="btn" data-clipboard-target="#static-link-textbox-inner">Copy to Clipboard</button>'
            }]
          }
        }
      ]
    },{ view:"resizer" },{
      view: "iframe",
      id: "PWIframePlacementWindowTestbenchModeGoal",
      src: iframeURL,
      css: "pw_iframe PWIframePlacementWindowEditModeTestbenchl",
      borderless: true,
      on: {
        onAfterLoad: function() {
          parent.postMessage(JSON.stringify({
            t: "set-active-step-sessional",
            step: "testbench",
            url: iframeURL
          }), "*");

          var view = this;
          if (postMessageConfig.stage == 1 || canWeConfirm == true) {
            postMessageConfig.stage = 2;
          } else {
            //delay 1 second
            setTimeout(function() {
              if (postMessageConfig.stage != 1) {
                //console.debug($('iframe')[0]);
                //console.debug(monolop_base_url + '/placement-proxy/preview?l=' +  urlencode($('iframe')[0].src));
                $('iframe')[0].src =  monolop_base_url + '/placement-proxy/preview?l=' +  urlencode($('iframe')[0].src);
                postMessageConfig.stage = 3;
              }
            }, 1000);
            if (!traversingHistoryPWGoal) {
              addnewHistoryUrlGoal($$("customerSiteUrlLink").getValue());
            }
            traversingHistoryPWGoal = false;
            allFormDataPlacementWindowGoal.data.url = $$("customerSiteUrlLink").getValue();
          }
          enableEditActions();
        }
      }
    }]
  });

  $('[view_id="static-link-textbox"] input').attr('id','static-link-textbox-inner') ;
  new Clipboard('#testbench-preview-clipboard-btn');
};

generateTestBenchPanel = function(testbench){
  var panel = $$('testbench-panel')  ;
  for (var group in testbench) {
    var groupVar = camelize(group);
    panel.addView({
      view: 'accordionitem',
      header: group ,
      body : {
        id : groupVar ,
        padding : 10 ,
        rows : []
      }
    });

    for(var field in testbench[group]){
      testbenchAddField(groupVar , testbench[group][field]) ;
    }
  }

  window.testbench.isFirstTime = false ;

};

testbenchAddField = function(groupId , fieldObj){
  if( fieldObj.type == 'integer'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    $$(groupId).addView({
      view : 'text' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      validate:webix.rules.isNumber ,
      on : {
        onChange : function(code , e){
          updateTestbench();
        }
      }
    });
  }else if( fieldObj.type == 'string'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    $$(groupId).addView({
      view : 'text' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      on : {
        onChange : function(code , e){
          updateTestbench();
        }
      }
    });
  }else if( fieldObj.type == 'bool'){
    $$(groupId).addView({
      view : "checkbox" ,
      label : '<strong>' + fieldObj.text + '</strong>' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      checkValue : true ,
      uncheckValue : false ,
      on:{
				onChange: function(){
					updateTestbench();
				}
			}
    });
  }else if( fieldObj.type == 'date'){
    $$(groupId).addView({
      view : "datepicker" ,
      label : '<strong>' + fieldObj.text + '</strong>' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      timepicker:false,
      on:{
				onChange: function(){
					updateTestbench();
				}
			}
    });
  }else if( fieldObj.type == 'single'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    var options = [] ;
    for(var i = 0 ; i < fieldObj.values.length ; i++){
      options.push({id:fieldObj.values[i].value , value:fieldObj.values[i].label});
    }

    $$(groupId).addView({
      view : 'richselect' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      options: options ,
      on : {
        onChange : function(code , e){
          updateTestbench();
        }
      }
    });
  }else if( fieldObj.type == 'single remote'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    $$(groupId).addView({
      view : 'combo' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      options: fieldObj.values + '?select=true' ,
      on : {
        onChange : function(code , e){
          updateTestbench();
        }
      }
    });
  }else if( fieldObj.type == 'function'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    if( fieldObj.values[0].Label != undefined){
     $$(groupId).addView({
       view : 'label' ,
       label : '<u>Set : ' +   fieldObj.set + '</u>'
     }) ;
    }

    for( var i = 0 ; i < fieldObj.values.length ; i++){
      if(  fieldObj.values[i].value_label != undefined ) {
        $$(groupId).addView({
          view : 'label' ,
          label : fieldObj.values[i].Label + ' is ' + fieldObj.values[i].value_label
        }) ;
      }else{
        $$(groupId).addView({
          view : 'label' ,
          label : fieldObj.values[i].Label + ' is ' + fieldObj.values[i].value
        }) ;
      }
    }
    if( fieldObj.ReturnValue == 'string'){
      $$(groupId).addView({
        view : 'text' ,
        id : fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set,
        on : {
          onChange : function(code , e){
            updateTestbench();
          }
        }
      });
    }else if( fieldObj.ReturnValue == 'integer'){
      $$(groupId).addView({
        view : 'text' ,
        id : fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set,
        validate:webix.rules.isNumber ,
        on : {
          onChange : function(code , e){
            updateTestbench();
          }
        }
      });
    }else{
      $$(groupId).addView({
        view : "checkbox" ,
        id : fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set,
        checkValue : true ,
        uncheckValue : false ,
        value : fieldObj.value,
        on:{
    			onChange: function(){
    				updateTestbench();
    			}
    		}
      });
    }
  }

  if(fieldObj.type == 'function'){
    testbench.fieldArray[fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set] = {
        connector : fieldObj.connector ,
        field : fieldObj.id ,
        value : '' ,
        type  : fieldObj.type ,
        ReturnValue : fieldObj.ReturnValue ,
        id    : fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set
    }  ;
    testbench.fieldArray[fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set]['params'] = {}
    for( var i = 0 ; i < fieldObj.values.length ; i++){
      testbench.fieldArray[fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set]['params']['param' + (i + 1 ) ] = fieldObj.values[i].value ;
   }
  }else{
    testbench.fieldArray[fieldObj.connector + '_' +  fieldObj.id] = {
      connector : fieldObj.connector ,
      field : fieldObj.id ,
      value : '' ,
      type  : fieldObj.type ,
      id    : fieldObj.connector + '_' +  fieldObj.id
    }  ;
  }
}

updateTestbench = function(){
  $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().src = $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().src ;
}

getStaticLink = function(){

  var static_link = '' ;
  var fieldList = testbench.fieldArray ;
  static_link = static_link + '?l=' + $$('customerSiteUrlLink').getValue();
  for (var key in fieldList) {
    if( $$(fieldList[key].id) == undefined)
      continue ;
    var keyname = key.replace("[", "_");
    keyname = keyname.replace("]", "");
    static_link =  static_link + '&data[' + keyname + ']' + '[main]=' + fieldList[key].connector + '|' +   fieldList[key].field + '|' + fieldList[key].type + '|' ;
    if( fieldList[key].ReturnValue != undefined){
      static_link =  static_link + fieldList[key].ReturnValue  + '|' ;
    }else{
      static_link =  static_link   + '|' ;
    }


    if(  fieldList[key].type == 'date'){
      var val = Ext.getCmp(fieldList[key].id).getValue() ;
      static_link =  static_link + val.format('d/m/Y') ;
    }else{
      static_link =  static_link + $$(fieldList[key].id).getValue() ;
    }
    if(  fieldList[key].type == 'function'){
      for (var key2 in fieldList[key].params) {
        static_link =  static_link + '&data[' + key + ']' + '[params][' + key2 + ']=' + fieldList[key]['params'][key2] ;
      }
    }
  }

  $$('static-link-textbox').setValue( monolop_base_url + '/component/testbench-preview' + static_link ) ;
  //$('#static-link-data').val($$('static-link-textbox').getValue());
}

testbenchValue = function(){
  var data = {} ;
  var fieldList = testbench.fieldArray ;
  for (var key in fieldList) {
    if( data[key] == undefined){
      data[key] = {} ;
    }
    data[key]['connector']= fieldList[key].connector ;
    data[key]['field'] = fieldList[key].field ;
    data[key]['type']  = fieldList[key].type ;
    if( $$(fieldList[key].id) == undefined)
      continue ;

    data[key]['value'] = $$(fieldList[key].id).getValue() ;

    if(  fieldList[key].type == 'function'){
      data[key]['ReturnValue'] =  fieldList[key].ReturnValue ;
      for (var key2 in fieldList[key].params) {
        if( data[key]['params'] == undefined){
          data[key]['params'] = {} ;
        }
          data[key]['params'][key2]  = fieldList[key]['params'][key2] ;
      }
    }
    /*
    if(  fieldList[key].type == 'date'){
        var val = Ext.getCmp(fieldList[key].id).getValue() ;
        data[key]['value'] = val.format('d/m/Y') ;
        //console.debug(val.format('d/m/Y')) ;
    }else{
        data[key]['value'] = Ext.getCmp(fieldList[key].id).getValue() ;
    }
    if(  fieldList[key].type == 'function'){
      data[key]['ReturnValue'] =  fieldList[key].ReturnValue ;
        for (var key2 in fieldList[key].params) {
          if( data[key]['params'] == undefined){
            data[key]['params'] = {} ;
          }
            data[key]['params'][key2]  = fieldList[key]['params'][key2] ;
        }
    }
    */

  }
  getStaticLink();
  return data ;
}

function processTestBenchData(){
  var data = testbenchValue() ;

  $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().contentWindow.postMessage(JSON.stringify({
    t: "test-processTestbench2" ,
    testData : data ,
    mongoDetail : testbench.mongoDetail ,
    loadDefault : true
  }), postMessageConfig.url);
}


function openPublishGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal) {
  //console.log('n-openPublicGolaSection');
  if(parent_accessible){
    checkClientProtocalForHttps();
  }

  fullscreen.updateHeight();
  currentStepPWGoal = 'publish';
  highLightCurrentPlacementWindowGoal('publish');
  $("#customerSitePublishSection").html('');

  isPublishGeneratedGoal = true;
  var isPageElementIdAdded = false;
  var publishURL = monolop_base_url + '/component/page-list-publish?';
  publishPWUrl = publishURL;

  if(basicPlacementWindowFormValues !== undefined){
    if(basicPlacementWindowFormValues.source !== undefined){
      publishURL += ('source='+basicPlacementWindowFormValues.source + '&');
    }
    if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null){
      if(basicPlacementWindowFormValues.formData._id !== undefined){
        publishURL += ('page_element_id='+basicPlacementWindowFormValues.formData._id + '&');
        isPageElementIdAdded = true;
      }
    }
    if(basicPlacementWindowFormValues.experiment !== undefined && basicPlacementWindowFormValues.experiment != null){
      publishURL += ('exp='+basicPlacementWindowFormValues.experiment._id + '&');
    }
  }

  if(isPageElementIdAdded === false){
    if(currentPageElement.added){
      if(currentPageElement.data._id !== undefined){
        publishURL += ('page_element_id='+currentPageElement.data._id + '&');
      }
    }
  }
  var browseUrl = $$("customerSiteUrlLink").getValue().trim();
  if(browseUrl.length > 0){
    publishURL += ('fullURL='+browseUrl + '&');
  }

  // alert(publishURL);

  webix.ui({
    container: "customerSitePublishSection",
    id: "webixWizardPublishGoalId",
    css: "webixWizardPublishGoalId",
    height: 1024,
    rows: [{
      view: "iframe",
      id: "PWIframePublishGoal",
      src: publishURL,
      css: "pw_iframe",
      borderless: true,
      on: {
        onAfterLoad: function() {

          parent.postMessage(JSON.stringify({
            t: "set-active-step-sessional",
            step: "publish",
            url: browseUrl
          }), "*");

          var view = this, currentFolder = '';
          customerSiteUrlLink = $$("customerSiteUrlLink").getValue();

          if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null){
            if(basicPlacementWindowFormValues.formData._id !==undefined){
              returnUrlConfig.url = customerSiteUrlLink;
              returnUrlConfig.url_option = basicPlacementWindowFormValues.formData.urlOption;
              returnUrlConfig.inc_www = basicPlacementWindowFormValues.formData.inWWW;
              returnUrlConfig.inc_http_https = basicPlacementWindowFormValues.formData.inHTTP_HTTPS;
              returnUrlConfig.reg_ex = basicPlacementWindowFormValues.formData.regExp;
              returnUrlConfig.remark = basicPlacementWindowFormValues.formData.remark;
            }
          }
          if(basicPlacementWindowFormValues.currentFolder !== undefined){
            currentFolder = basicPlacementWindowFormValues.currentFolder;
          }
          var page_element_id = "";
          var experiment_id = "";
          if(parent_accessible){
            if(parent.userStateData){
              if(parent.userStateData.data.publishData){
                customerSiteUrlLink = parent.userStateData.data.publishData.fullURLWebPageList;
                returnUrlConfig.url = parent.userStateData.data.publishData.fullURLWebPageList;
                returnUrlConfig.url_option = parent.userStateData.data.publishData.urlOptionWebPageElement;
                returnUrlConfig.inc_www = parent.userStateData.data.publishData.includeWWWWebPageElement;
                returnUrlConfig.inc_http_https = parent.userStateData.data.publishData.includeHttpHttpsWebPageElement;
                returnUrlConfig.reg_ex = parent.userStateData.data.publishData.regularExpressionWebPageElement;
                returnUrlConfig.remark = parent.userStateData.data.publishData.remarkWebPageList;
                currentFolder = parent.userStateData.data.publishData.currentFolder;
                page_element_id  = parent.userStateData.data.publishData.page_element_id;
                experiment_id  = parent.userStateData.data.publishData.experiment_id;
              }
            }
          }
          // commnet lines below for mantis#4103
          // var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
          // var iframe_edit = $('iframe')[index];
          $$('PWIframePublishGoal').getIframe().contentWindow.postMessage(JSON.stringify({
            t: "set-publish-page-options",
            fullURL: customerSiteUrlLink,
            currentFolder: currentFolder,
            experiment_id: experiment_id,
            page_element_id: page_element_id,
            mode: basicPlacementWindowFormValues.mode,
            returnUrlConfig: returnUrlConfig,
            temp: basicPlacementWindowFormValues.temp,
          }), publishURL);


          enableEditActions();
        }
      }
    }]
  });

  $$("publishBtnPlacementWindowGoal").enable();
}

function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
    return index == 0 ? match.toLowerCase() : match.toUpperCase();
  });
}

function urlencode(str) {
  return escape(str).replace(/\+/g, '%2B').replace(/%20/g, '+').replace(/\*/g, '%2A').replace(/\//g, '%2F').replace(/@/g, '%40');
};

function refineUrl(url) {
  url = url || "";
  if(url.length < 1){
    return url;
  }
  var prefix = ['http://', 'https://'];
  if (url.substr(0, prefix[1].length) === prefix[1] || url.substr(0, prefix[0].length) === prefix[0]) {
    return url;
  }
  url = prefix[0] + url;
  return url;
}

function is_valid_url(url) {
  // validate URL here
  if(url.length < 1){
    return false;
  }
  return true;
}

function addnewHistoryUrlGoal(url) {
  if ((currentUrlHosturyPWGoal + 1) < indexUrlHistoyPWGoal) {
    urlHistoryPWGoal.splice(currentUrlHosturyPWGoal + 1);
    indexUrlHistoyPWGoal = currentUrlHosturyPWGoal;
  }
  if (urlHistoryPWGoal.length > 0) {
    indexUrlHistoyPWGoal++;
    currentUrlHosturyPWGoal = indexUrlHistoyPWGoal;
  }
  urlHistoryPWGoal.push(url);
}

function backwardPWGoal() {
  if (indexUrlHistoyPWGoal === 0 || currentUrlHosturyPWGoal === 0) {
    return;
  }
  traversingHistoryPWGoal = true;
  currentUrlHosturyPWGoal = currentUrlHosturyPWGoal - 1;
  $$("customerSiteUrlLink").setValue(urlHistoryPWGoal[currentUrlHosturyPWGoal]);
  // console.log(urlHistoryPWGoal, currentUrlHosturyPWGoal);
  openPlacementWindowGoalSection();
}

function forwardPWGoal() {
  if (indexUrlHistoyPWGoal === 0 || currentUrlHosturyPWGoal === urlHistoryPWGoal.length + 1) {
    return;
  }
  if (currentUrlHosturyPWGoal >= indexUrlHistoyPWGoal) {
    return;
  }
  traversingHistoryPWGoal = true;
  currentUrlHosturyPWGoal = currentUrlHosturyPWGoal + 1;
  $$("customerSiteUrlLink").setValue(urlHistoryPWGoal[currentUrlHosturyPWGoal]);
  // console.log(urlHistoryPWGoal, currentUrlHosturyPWGoal);
  openPlacementWindowGoalSection();
}

function setFileManagerSelectUrl(url){
  /*
  var index = url.indexOf('/photos');
  url = url.substring(index+7);
  url = s3_public_url  + 'assets' + url ;
  url = url.replace(activeAccount['_id'],activeAccount['uid']);
  */

  var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
  var iUrl = $$("customerSiteUrlLink").getValue();
  var iFrame = $("iframe[src*='"+ iUrl +"']")[index];


  if(iFrame){
    iFrame.contentWindow.postMessage({
        t: "image-is-uploaded", data: {
          status : 'server' ,
          url : url
        }
      },
      $$("customerSiteUrlLink").getValue()
    );
  }
  $$('upload_an_image1').close();

}

function openImageUploader(data){
  //window.open('/filemanager?type=image', 'FileManager', 'width=900,height=600');
  var url = monolop_base_url + '/media?nav=1';
  webix.ui({
    view: "window",
    id: "upload_an_image1",
    modal: true,
    scroll: true,
    position: "center",
    height: 650,
    width: 900,
    headHeight : 0,
    body: {
      rows: [
        {
          view:"iframe",
          id:"frame-body",
          src: url,
          on: {
            onAfterLoad: function() {
            }
          }
        },{
          cols: [{}, {}, {}, {}, {
              view: "button",
              "type": "danger",
              value: "Cancel",
              width: 138,
              click: "$$('upload_an_image1').close();"
          }]
        }
      ]
    }
  }).show();
  // return ;
  var url = monolop_base_url + '/component/image-uploader';
  webix.ui({
    view: "window",
    id: "upload_an_image",
    modal: true,
    scroll: true,
    position: "center",
    height: 400,
    width: 500,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "Upload an image"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('upload_an_image').close();"
      }]
    },
    body: {
      rows: [
        {
          view:"iframe",
          id:"frame-body",
          src: url,
          on: {
            onAfterLoad: function() {
              var iframe = $('iframe')[2];
              // iframe.contentWindow.postMessage(condition, monolop_base_url + url);
            }
          }
      },
        {
          cols: [{}, {}, {}, {}, {
              view: "button",
              "type": "danger",
              value: "Cancel",
              width: 138,
              click: "$$('upload_an_image').close();"
            }, {
              view: "button",
              value: "Ok",
              width: 138,
              click: function(evt){
                var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                var iUrl = $$("customerSiteUrlLink").getValue();
                var iFrame = $("iframe[src*='"+ iUrl +"']")[index];
                if(IframeImageUploaderWin.uploadedFile){
                  if(IframeImageUploaderWin.uploadedFile.status === 'server'){
                  //   for (var i = 0; i < iFrame.length; i++) {
                  if(iFrame){
                      iFrame.contentWindow.postMessage({
                          t: "image-is-uploaded", data: IframeImageUploaderWin.uploadedFile
                        },
                        $$("customerSiteUrlLink").getValue()
                      );
                   }
                    $$('upload_an_image').close();
                  } else {
                    // iFrame.contentWindow.postMessage({
                    //     t: "remove-iframe-event"
                    //   },
                    //   $$("customerSiteUrlLink").getValue()
                    // );
                    webix.message({ type:"error", text:"Please upload an image first." });
                  }
                } else {
                  webix.message({ type:"error", text:"Please upload an image first." });
                }


              }
            }

          ]
        }
      ]
    }
  }).show();
}

function getChildComponentDimensionAsPerParent(parentWindow){
  var parentWidth = $(parentWindow).width();
  var parentHeight = $(parentWindow).height();

  var _width = parentWidth - (parentWidth * (5/100));
  var _height = parentHeight - (parentHeight * (30/100));

  return {width: _width, height: _height};
}

function openConditionBuilder(condition, type) {
  var dimensions = this.getChildComponentDimensionAsPerParent(window);

  condition === undefined && (condition = "if( ) {|}");
  var url  = '/api/profile-properties-template';

  // pass query_string flag[inherit_condition=true] to disable pointer event if condition is forced to be inherited in contents
  if(basicPlacementWindowFormValues.source == 'experiment'){
    if(basicPlacementWindowFormValues.experiment){
      if(basicPlacementWindowFormValues.experiment._segment){
        condition = basicPlacementWindowFormValues.experiment._segment.condition;
        url += '?inherit_condition=true';
      }
    }
  }


   webix.ui({
     view: "window",
     id: "edit_condition_bulder",
     modal: true,
     scroll: true,
     position: "top",
     height: /*700*/ dimensions.height,
     width: /*1200*/ dimensions.width,
     move: true,
     resize: true,
     head: {
       view: "toolbar",
       margin: -4,
       cols: [{
         view: "label",
         label: "Condition Builder"
       }, {
         view: "icon",
         icon: "times-circle",
         css: "alter",
         click: "$$('edit_condition_bulder').close();"
       }]
     },
     body: {
       rows: [
         {
           view:"iframe",
           id:"frame-body",
           src: url,
           on: {
             onAfterLoad: function() {
               var index = isPlacementWindowGeneratedGoal === false ? 1 : 2;
               var iframe = $('iframe')[index];
               iframe.contentWindow.postMessage(condition, monolop_base_url + url);
             }
           }
       },
         {
           cols: [{}, {}, {}, {}, {
               view: "button",
               "type": "danger",
               value: "Cancel",
               width: 138,
               click: "$$('edit_condition_bulder').close();"
             }, {
               view: "button",
               value: "Ok",
               css: "orangeBtn",
               width: 138,
               click: function(evt){
                 var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                 var iFrame = $("iframe")[index];
                 iFrame.contentWindow.postMessage({t: "condition-selected", type: type, returnCondition: iFrameConditionBuilderWin.returnCondition},"*");

                 $$('edit_condition_bulder').close();
               }
             }
           ]
         }
       ]
     }
   }).show();
 }
 function closeConditionBuilder(){
   $$('edit_condition_bulder').close();
   webix.message("Segment conditions are updated.");
 }
 function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}
$(window).bind('beforeunload', function(){
		createCookie("customerSiteUrlLink", $$("customerSiteUrlLink").getValue(), 1);
	});


function checkClientProtocal(){
  var parent_url = parent.document.location.href ;
  var current_url = $$("customerSiteUrlLink").getValue() ;
  var parent_scheme = isHttps(parent_url);
  var current_scheme = isHttps(current_url);
  if(parent_scheme !== current_scheme){
    if(parent_scheme === true){
      //convert to http
      parent.document.location.href = parent.document.location.href.replace('https','http');
    }else{
      //convert tp https
      parent.document.location.href = parent.document.location.href.replace('http','https');
    }
  }
}

function checkClientProtocalForHttps(){
  var parent_url = parent.document.location.href ;
  var current_url = $$("customerSiteUrlLink").getValue() ;
  var parent_scheme = isHttps(parent_url);
  var current_scheme = isHttps(current_url);
  if(parent_scheme !== current_scheme){
    if(parent_scheme === true){
      //convert tp https
      parent.document.location.href = parent.document.location.href.replace('https','http');
    }
  }
}

function isHttps(url){
  if(url.indexOf('https') == -1)
    return false ;
  return true ;
}

/******
Tracker add-on
******/

function showTrackerConfig(){
  //console.log($$('customerSiteUrlLink').getValue());
  parent.postMessage(JSON.stringify({
    t: 'to-config-step',
    url: $$('customerSiteUrlLink').getValue()
  }),'*');
}


// window.onresize = function(){
//   console.log("test");
//   setTimeout(function(){
//     window.location.reload();
//   });
// }

// window.addEventListener('resize', function(){
//   alert('hi');
// }, true);
