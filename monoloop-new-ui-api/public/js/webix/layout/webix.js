
var config = {};
config.dashboard = {link: '/', headerName: 'Dashboard', script: '/js/webix/page/dashboard.js', pageId: 'dashboard'};
config.dashboard.experiment = {link: '/dashboard/experiment', headerName: 'Experience', script: '/js/webix/admin/reports/experiment.js', pageId: 'dashboardExperiment'};
config.dashboard.experimentdetail = {link: '/dashboard/experiment/detail', headerName: '', script: '/js/webix/admin/reports/experiment.js', pageId: 'dashboardExperimentdetail'};

config.dashboard.goals = {link: '/dashboard/goals', headerName: 'Goal', script: '/js/webix/admin/reports/goals.js', pageId: 'dashboardGoals'};
config.dashboard.goalsdetail = {link: '/dashboard/goals/detail', headerName: '', script: '/js/webix/admin/reports/goals.js', pageId: 'dashboardGoalsdetail'};
config.dashboard.segmentdetail = {link: '/reports/segment/detail', headerName: '', script: '/js/webix/admin/reports/segment.js', pageId: 'dashboardSegmentDetail'};

config.segment = {link: '/segment', headerName: 'Audiences', script: '/js/webix/page/content/segment.js', pageId: 'segment'};
config.addsegment = {link: '/segment/add', headerName: 'Audiences', script: '/js/webix/page/content/segment.js', pageId: 'addsegment'};
config.editsegment = {link: '/segment/edit', headerName: 'Audiences', script: '/js/webix/page/content/segment.js', pageId: 'editsegment'};



config.experiment = {link: '/experiment', headerName: 'Experiences', script: '/js/webix/page/content/experiment.js', pageId: 'experiment'};
config.addexperiment = {link: '/experiment/add', headerName: 'Experiences', script: '/js/webix/page/content/experiment.js', pageId: 'addexperiment'};
config.editexperiment = {link: '/experiment/edit', headerName: 'Experiences', script: '/js/webix/page/content/experiment.js', pageId: 'editexperiment'};

config.goals = {link: '/goals', headerName: 'Goals', script: '/js/webix/page/content/goals.js', pageId: 'goals'};
config.addgoals = {link: '/goals/add', headerName: 'Goals', script: '/js/webix/page/content/goals.js', pageId: 'addgoals'};
config.editgoals = {link: '/goals/edit', headerName: 'Goals', script: '/js/webix/page/content/goals.js', pageId: 'editgoals'};

config.contentWeb = {link: '/content/web', headerName: 'Page List', script: '/js/webix/page/content/web.js', pageId: 'contentWeb'};
config.addcontentWeb = {link: '/content/web/add', headerName: 'Page List', script: '/js/webix/page/content/web.js', pageId: 'addcontentWeb'};
config.editcontentWeb = {link: '/content/web/edit', headerName: 'Page List', script: '/js/webix/page/content/web.js', pageId: 'editcontentWeb'};

config.contentWebContent = {link: '/content/web-content', headerName: 'Content List', script: '/js/webix/page/content/webContent.js', pageId: 'contentWebContent'};
config.contentBluePrints = {link: '/content/blueprints', headerName: 'Blueprints', script: '/js/webix/page/content/blueprints.js', pageId: 'contentBluePrints'};
config.trackers = {link: '/trackers', headerName: 'Trackers', script: '/js/webix/page/trackers.js', pageId: 'trackers'};

config.tracker = {link: '/tracker', headerName: 'Trackers', script: '/js/webix/page/content/tracker.js', pageId: 'trackers'};
config.addTracker = {link: '/tracker/add', headerName: 'Trackers', script: '/js/webix/page/content/tracker.js', pageId: 'addtracker'};
config.editTracker = {link: '/tracker/edit', headerName: 'Trackers', script: '/js/webix/page/content/tracker.js', pageId: 'edittracker'};

config.media = {link: '/media', headerName: 'Media', script: '/js/webix/page/media.js', pageId: 'media'};
config.accountProfile = {link: '/account/profile', headerName: 'My Profile', script: '/js/webix/page/account/account.js', pageId: 'accountProfile'};
config.controlGroup = {link: '/account/control-group', headerName: 'Control Group', script: '/js/webix/page/account/controlGroup.js', pageId: 'controlGroup'};
config.codenScript = {link: '/account/code-and-scripts', headerName: 'Snippet', script: '/js/webix/page/account/codenScript.js', pageId: 'codenScript'};
config.users = {link: '/account/users', headerName: 'Users', script: '/js/webix/page/account/users.js', pageId: 'users'};
config.clientAccounts = {link: '/account/client-accounts', headerName: 'Client Accounts', script: '/js/webix/page/account/clientAccounts.js', pageId: 'clientAccounts'};
config.accountApiToken = {link: '/account/api-token', headerName: 'Api Token', script: '/js/webix/page/account/apiToken.js', pageId: 'accountApiToken'};
config.accountPlugin = {link: '/account/plugins', headerName: 'Plugins', script: '/js/webix/page/account/plugins.js', pageId: 'accountPlugin'};
config.accountScore = {link: '/account/scores', headerName: 'Scores', script: '/js/webix/page/account/scores.js', pageId: 'accountScore'};
config.accountDomain = {link: '/account/domains', headerName: 'Domains', script: '/js/webix/page/account/domains.js', pageId: 'accountDomain'};
config.accountLog = {link: '/account/logs', headerName: 'Logs', script: '/js/webix/page/account/logs.js', pageId: 'accountLog'};
config.accountApplication = {link: '/account/applications', headerName: 'Applications', script: '/js/webix/page/account/applications.js', pageId: 'accountApplication'};
config.installationManual = {link: '/account/installation-manual', headerName: 'Documentation', script: '/js/webix/page/account/installation_manual.js', pageId: 'installationManual'};

config.styles = {};
config.styles.defaultFileManagerHeight  = 655;

var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

config.styles.defaultHeight = {
  h: 990,
  mainHeader: 55,
  heading: 49,
  getHeight: function(defH){
    defH = defH || this.h;
    var h = this.h;
    if(h < window.innerHeight){
      h = window.innerHeight;
    }
    return h;
  },
  getFileManagerHeight: function(defH){
    defH = defH || this.h;
    return (config.styles.defaultHeight.getHeight(defH) - this.heading - 20);
  },
  setInnerContentHeight: function(defH){
    defH = defH || this.h;
    var h = config.styles.defaultHeight.getHeight(defH) - this.heading - 10;
    $(".innerContent").css("minHeight", h + 'px');
  },
  getMainContainerHeight: function(defH){
    defH = defH || this.h;
    var upperHeight = this.mainHeader + this.heading;
  },
  updateHeights: function(defH){
    defH = defH || this.h;
    $(".main_cont, section#main, #mainContainer, .innerContent").css("minHeight", config.styles.defaultHeight.getHeight(defH) + 'px');

  },
};

// source: http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        // vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var manageLayouts = {
  default: function(options){
    var
      sidebarWidth = $('.sidebar').width(),
      bodyWidth = $('body').width();

    $('body').removeClass('pwLayout');
    $$("logoutLinkId").show();
    $$("accountName1").show();
    $("#main").css({'position': 'relative', 'top': '0', 'left': '0'});
    $("#header").css('width', bodyWidth + 'px');

  },
  pw: function(options){
    var
      sidebarWidth = $('.sidebar').width(),
      bodyWidth = $('body').width();

    $('body').addClass('pwLayout');
    $$("logoutLinkId").hide();
    $$("accountName1").hide();
    $("#main").css({'position': 'fixed', 'top': '0', 'left': sidebarWidth + 'px'});
    $("#header").css('width', sidebarWidth + 'px');

  }
}

function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
};

function generatePageContent(options) {
  var params = getQueryParams(document.location + '');
  if($('#floatingContainer').length > 0){
    $('#floatingContainer').remove();
  }
  tooltips.startedBefore = false;
  guidelines.startedBefore = false;
  webix.CustomScroll.init();

  //clean up data before switch ;
  if($('#datepicker-dropdown').length){
    $('#datepicker-dropdown').remove();
  }



  $.getScript(options.script,
    function() {
      $(".webui-popover.pop").remove();

      var mainContainer = document.getElementById("mainContainer");

      mainContainer.innerHTML = '';

      webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers,formData){
        if(!formData){
          headers["Content-type"]= ajaxHeaders['Content-Type'];
        }
        headers["ML-Token"]= ajaxHeaders['ML-Token'];
      });


      var header = document.createElement('div');
      header.id = 'viewHeader';
      header.style.height = config.styles.defaultHeight.heading + 'px';
      header.className = 'webix_view title viewHeader';

      var header0 = document.createElement('div');
      header0.className = 'webix_template';

      var header1 = document.createElement('div');
      header1.className = 'header';
      header1.innerHTML = options.headerName;
      header1.style.marginLeft = '15px';

      if(options.link === '/dashboard/experiment/detail'){
        var back = document.createElement('span');
        back.title = "Back to experiments";
        back.className = "webix_icon fa-arrow-left ";
        back.style.cursor = 'pointer';
        back.style.position = 'absolute';
        back.style.left = '1%';
        back.style.top = '2.3%';
        back.onclick = function(){
          this.style.pointerEvents = 'none';
          window.location = '/experiment' + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURIComponent(params.url)) : '');
        };
      } else if (options.link === '/reports/segment/detail'){
        var back = document.createElement('span');
        back.title = "Back to segments";
        back.className = "webix_icon fa-arrow-left ";
        back.style.cursor = 'pointer';
        back.style.position = 'absolute';
        back.style.left = '1%';
        back.style.top = '2.3%';
        back.onclick = function(){
          this.style.pointerEvents = 'none';
          window.location = '/segment' + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURIComponent(params.url)) : '');
        };
      }else if (options.link === '/dashboard/goals/detail'){
        var back = document.createElement('span');
        back.title = "Back to goals";
        back.className = "webix_icon fa-arrow-left ";
        back.style.cursor = 'pointer';
        back.style.position = 'absolute';
        back.style.left = '1%';
        back.style.top = '2.3%';
        back.onclick = function(){
          this.style.pointerEvents = 'none';
          window.location = '/goals' + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURIComponent(params.url)) : '');
        };
      }

      /*var stGd = document.createElement('i');
      stGd.title = 'Show Guides';
      stGd.className = "webix_icon fa-comment-o";
      stGd.style.position = 'absolute';
      stGd.style.top = '3.3%';
      stGd.style.right = '3.3%';

      // stGd.onclick = function(){
      //
      //   if(guidelines.open === false) {
      //     this.style.pointerEvents = 'none';
      //     guidelines.startGuideline(undefined, {_this: this});
      //   }
      // };

      var stGdToggle = document.createElement('i');
      stGdToggle.id = 'toggleGuides';
      stGdToggle.className = "webix_icon fa-toggle-on";
      stGdToggle.style.position = 'absolute';
      stGdToggle.style.top = '3.3%';
      stGdToggle.style.right = '1%';
      stGdToggle.style.cursor = 'pointer';
      stGdToggle.title = 'Hide Guides';

      stGdToggle.onclick = function(){
        if($('#toggleGuides').hasClass('fa-toggle-off')){
          $('#toggleGuides').removeClass('fa-toggle-off');
          $('#toggleGuides').addClass('fa-toggle-on');
          $('#toggleGuides').attr('title', 'Hide Guides');
          // Start guides
          if(guidelines.open === false) {
            this.style.pointerEvents = 'none';
            guidelines.startGuideline(undefined, {_this: this});
          }
        }
      };*/


      var stTip = document.createElement('i');
      stTip.title = 'Show Tips';
      stTip.className = "webix_icon fa-lightbulb-o";
      stTip.style.position = 'absolute';
      stTip.style.top = '2.3%';
      stTip.style.right = '3%';

      // stTip.onclick = function(){
      //   this.style.pointerEvents = 'none';
      //   tooltips.startTooltip(undefined, {_this: this});
      // };

      var stTipToggle = document.createElement('i');
      stTipToggle.id = 'toggleTips';
      stTipToggle.className = "webix_icon fa-toggle-on";
      stTipToggle.style.position = 'absolute';
      stTipToggle.style.top = '2.3%';
      stTipToggle.style.right = '1%';
      stTipToggle.style.cursor = 'pointer';
      stTipToggle.title = 'Hide Tips';

      stTipToggle.onclick = function(){
        // this.style.pointerEvents = 'none';
        if($('#toggleTips').hasClass('fa-toggle-off')){
          $('#toggleTips').removeClass('fa-toggle-off');
          $('#toggleTips').addClass('fa-toggle-on');
          $('#toggleTips').attr('title', 'Hide Tips');
          // Start Tips
          if(tooltips.page.currentTip !== undefined){
            tooltips.startTooltip(undefined, {_this: this});
          }else{
            guidelines.startGuideline(undefined, {_this: this});
          }
        }
      };


      header0.appendChild(header1);

      // header0.appendChild(stGd);
      // header0.appendChild(stGdToggle);

      header0.appendChild(stTip);
      header0.appendChild(stTipToggle);
      if(options.link === '/dashboard/experiment/detail' || options.link === '/reports/segment/detail' || options.link === '/dashboard/goals/detail'){
        header0.appendChild(back);
      }
      header.appendChild(header0);


      mainContainer.appendChild(header);

      var innerContent = document.createElement('div');
      innerContent.className = 'innerContent';
      innerContent.id = 'innerContent';
      mainContainer.appendChild(innerContent);

      config.styles.defaultHeight.setInnerContentHeight();
      manageLayouts.default();
      if($('body').hasClass('hidenav')){
        $("#main").css('width','100%');
      }else{
        $("#main").css('width', $('body').width() - $('.sidebar').width() - 1 + 'px');
      }

      // A generic responsive check which will resize the main 'section' element according to the browser window width
      $(window).on('resize', function(){
        $("#main").css('width', $('body').width() - $('.sidebar').width() - 1 + 'px');
        $("#viewHeader").css('width', $('body').width() - $('.sidebar').width() - 10 + 'px');
        $("#header").css('width', $('body').width() + 'px');
      });



      var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?tooltip=1&page_id='+options.pageId;

      webix.ajax().get(fetchGuidelinesUrl, { }, function(text, xml, xhr){
        var r = JSON.parse(text);

        var ctip = undefined;

        if(r.currentTip){
          ctip = JSON.parse(r.currentTip);
          tooltips.startedBefore = true;
        }


        tooltips[options.pageId] = r.tooltips;
        tooltips.page.pageId = options.pageId;

        if(ctip){
          if(ctip.currentTip){
            tooltips.page.currentTip = ctip.currentTip;
          } else {
            tooltips.page.currentTip = tooltips.first(tooltips[options.pageId]);
          }
        } else {
          tooltips.page.currentTip = tooltips.first(tooltips[options.pageId]);
        }

        var cg = undefined;
        if(r.currentGuide){
          cg = JSON.parse(r.currentGuide);
          guidelines.startedBefore = true;
        }

        guidelines[options.pageId] = r.guidelines;
        guidelines.page.pageId = options.pageId;
        guidelines.page.actionID = 'default';

        if(cg){
          if(cg.currentGuide){
            guidelines.page.currentGuide = cg.currentGuide;
          } else {
            guidelines.page.currentGuide = guidelines.first(guidelines[options.pageId]);
          }
        } else {
          guidelines.page.currentGuide = guidelines.first(guidelines[options.pageId]);
        }



        switch (options.link) {
          case config.dashboard.link:
            openLayout();
            break;
          case config.dashboard.experiment.link:
            openLayoutDashboardExperiment();
            break;
          case config.dashboard.experimentdetail.link:
            config.currentElementID = 'webixDashboardElementExperiment';
            openLayoutDashboardExperimentDetail();
            break;
          case config.dashboard.goals.link:
            openLayoutDashboardGoals();
            break;
          case config.dashboard.goalsdetail.link:
            config.currentElementID = 'webixDashboardElementGoal';
            openLayoutDashboardGoalsDetail();
            break;
          case config.dashboard.segmentdetail.link:
            config.currentElementID = 'webixDashboardElementSegment';
            openLayoutDashboardSegmentDetail(options.from_timestamp);
            break;
          case config.segment.link:
            config.currentElementID = 'webixFilemanagerSegment';
            openLayoutSegment();
            break;
          case config.addsegment.link:
            config.currentElementID = 'webixWizardHeaderMenuSegment';
            openWizardSegment('add', undefined, undefined, 'layout');
            break;
          case config.editsegment.link:
            config.currentElementID = 'webixWizardHeaderMenuSegment';
            updateSegment('segment__' + options.segment_id, 'layout');
            break;
          case config.tracker.link:
            config.currentElementID = 'webixFilemanagerTracker';
            openLayoutTracker();
            break;
          case config.addTracker.link:
            config.currentElementID = 'webixWizardHeaderMenuTracker';
            openWizardTracker('add');
            break;
          case config.editTracker.link:
            config.currentElementID = 'webixWizardHeaderMenuTracker';
            openLayoutTracker('tracker__' + options.segment_id, 'layout');
            break;
          case config.experiment.link:
            config.currentElementID = 'webixFilemanagerExperiment';
            openLayoutExperiment();
            break;
          case config.addexperiment.link:
            config.currentElementID = 'webixWizardHeaderMenuExperiment';
            experimentListConfig.openWizard('add', undefined, undefined, 'layout');
            break;
          case config.editexperiment.link:
            config.currentElementID = 'webixFilemanagerExperiment';
            experimentListConfig.updateFile('experiment__' + options.experiment_id, 'layout');
            break;
          case config.goals.link:
            config.currentElementID = 'webixFilemanagerGoal';
            openLayoutGoal();
            break;
          case config.addgoals.link:
            config.currentElementID = 'webixWizardHeaderMenuGoal';
            openWizardGoal('add', undefined, undefined, 'layout');
            break;
          case config.editgoals.link:
            config.currentElementID = 'webixWizardHeaderMenuGoal';
            updateGoal('goals__' + options.goal_id, 'layout');
            break;
          case config.contentWeb.link:
            config.currentElementID = 'webixFilemanagerWebPageList';
            openLayoutWeb();
            break;
          case config.addcontentWeb.link:
            config.currentElementID = 'webixWizardHeaderMenuWebPageList';
            webPageListConfig.openWizard('add', undefined, 'layout');
            break;
          case config.editcontentWeb.link:
            config.currentElementID = 'webixWizardHeaderMenuWebPageList';
            webPageListConfig.updateFile('web__' + options.page_id, undefined, 'layout');
            break;
          case config.contentWebContent.link:
            openLayoutWebContent();
            break;
          case config.contentBluePrints.link:
            openLayout();
            break;
          case config.trackers.link:
            openLayoutTracker();
          break;
            case config.media.link:
            config.currentElementID = 'webixFilemanagerMedia';
            openLayoutMedia();
          break;
          case config.accountProfile.link:
            config.currentElementID = 'profileForm';
            openLayout();
            break;
          case config.controlGroup.link:
            openLayout();
            break;
          case config.codenScript.link:
            config.currentElementID = 'webixCodeScriptAccordion';
            openLayout();
            break;
          case config.users.link:
            config.currentElementID = 'parentLayout';
            openLayout();
            break;
          case config.clientAccounts.link:
            openLayout();
            break;
          case config.accountApiToken.link:
            config.currentElementID = 'apiTokenForm';
            openLayout();
            break;
          case config.accountPlugin.link:
            openLayout();
            break;
          case config.accountScore.link:
            openLayout();
            break;
          case config.accountDomain.link:
            config.currentElementID = 'webixDomainsElement';
            openLayout();
            break;
          case config.accountLog.link:
            config.currentElementID = 'logDatatable';
            openLayout();
            break;
          case config.accountApplication.link:
            openLayout();
            break;
          case config.installationManual.link:
            config.currentElementID = 'frame-body';
            openLayout();
            break;
          default:
            openLayout();
          break;

        } // end switch


      });


  }, function(error){
    console.log(error);
  });
}


function getPageContent(link) {
  var options = {};
  isSessional = false;
  userStateData = '';
  switch (link) {
    case config.dashboard.link:
      window.history.pushState(config.dashboard.headerName, config.dashboard.headerName, config.dashboard.link);
      options.link = config.dashboard.link;
      options.headerName = config.dashboard.headerName;
      options.script = config.dashboard.script;
      options.pageId = config.dashboard.pageId;
      break;
    case config.tracker.link:
      window.history.pushState(config.tracker.headerName, config.tracker.headerName, config.tracker.link);
      options.link = config.tracker.link;
      options.headerName = config.tracker.headerName;
      options.script = config.tracker.script;
      options.pageId = config.tracker.pageId;
      break;
    case config.segment.link:
      window.history.pushState(config.segment.headerName, config.segment.headerName, config.segment.link);
      options.link = config.segment.link;
      options.headerName = config.segment.headerName;
      options.script = config.segment.script;
      options.pageId = config.segment.pageId;
      break;
    case config.addsegment.link:
      window.history.pushState(config.addsegment.headerName, config.addsegment.headerName, config.addsegment.link);
      options.link = config.addsegment.link;
      options.headerName = config.addsegment.headerName;
      options.script = config.addsegment.script;
      options.pageId = config.addsegment.pageId;
      break;
    case config.experiment.link:
      window.history.pushState(config.experiment.headerName, config.experiment.headerName, config.experiment.link);
      options.link = config.experiment.link;
      options.headerName = config.experiment.headerName;
      options.script = config.experiment.script;
      options.pageId = config.experiment.pageId;
      break;
    case config.goals.link:
      window.history.pushState(config.goals.headerName, config.goals.headerName, config.goals.link);
      options.link = config.goals.link;
      options.headerName = config.goals.headerName;
      options.script = config.goals.script;
      options.pageId = config.goals.pageId;
      break;
    case config.contentWeb.link:
      window.history.pushState(config.contentWeb.headerName, config.contentWeb.headerName, config.contentWeb.link);
      options.link = config.contentWeb.link;
      options.headerName = config.contentWeb.headerName;
      options.script = config.contentWeb.script;
      options.pageId = config.contentWeb.pageId;
      break;
    case config.contentWebContent.link:
      window.history.pushState(config.contentWebContent.headerName, config.contentWebContent.headerName, config.contentWebContent.link);
      options.link = config.contentWebContent.link;
      options.headerName = config.contentWebContent.headerName;
      options.script = config.contentWebContent.script;
      options.pageId = config.contentWebContent.pageId;
      break;
    case config.contentBluePrints.link:
      window.history.pushState(config.contentBluePrints.headerName, config.contentBluePrints.headerName, config.contentBluePrints.link);
      options.link = config.contentBluePrints.link;
      options.headerName = config.contentBluePrints.headerName;
      options.script = config.contentBluePrints.script;
      options.pageId = config.contentBluePrints.pageId;
      break;
    case config.trackers.link:
      window.history.pushState(config.trackers.headerName, config.trackers.headerName, config.trackers.link);
      options.link = config.trackers.link;
      options.headerName = config.trackers.headerName;
      options.script = config.trackers.script;
      options.pageId = config.trackers.pageId;
      break;
    case config.media.link:
      window.history.pushState(config.media.headerName, config.media.headerName, config.media.link);
      options.link = config.media.link;
      options.headerName = config.media.headerName;
      options.script = config.media.script;
      options.pageId = config.media.pageId;
      break;
    case config.accountProfile.link:
      window.history.pushState(config.accountProfile.headerName, config.accountProfile.headerName, config.accountProfile.link);
      options.link = config.accountProfile.link;
      options.headerName = config.accountProfile.headerName;
      options.script = config.accountProfile.script;
      options.pageId = config.accountProfile.pageId;
      break;
    case config.controlGroup.link:
      window.history.pushState(config.controlGroup.headerName, config.controlGroup.headerName, config.controlGroup.link);
      options.link = config.controlGroup.link;
      options.headerName = config.controlGroup.headerName;
      options.script = config.controlGroup.script;
      options.pageId = config.controlGroup.pageId;
      break;
    case config.codenScript.link:
      window.history.pushState(config.codenScript.headerName, config.codenScript.headerName, config.codenScript.link);
      options.link = config.codenScript.link;
      options.headerName = config.codenScript.headerName;
      options.script = config.codenScript.script;
      options.pageId = config.codenScript.pageId;
      break;
    case config.users.link:
      window.history.pushState(config.users.headerName, config.users.headerName, config.users.link);
      options.link = config.users.link;
      options.headerName = config.users.headerName;
      options.script = config.users.script;
      options.pageId = config.users.pageId;
      break;
    case config.clientAccounts.link:
      window.history.pushState(config.clientAccounts.headerName, config.clientAccounts.headerName, config.clientAccounts.link);
      options.link = config.clientAccounts.link;
      options.headerName = config.clientAccounts.headerName;
      options.script = config.clientAccounts.script;
      options.pageId = config.clientAccounts.pageId;
      break;
    case config.accountApiToken.link:
      window.history.pushState(config.accountApiToken.headerName, config.accountApiToken.headerName, config.accountApiToken.link);
      options.link = config.accountApiToken.link;
      options.headerName = config.accountApiToken.headerName;
      options.script = config.accountApiToken.script;
      options.pageId = config.accountApiToken.pageId;

      break;
    case config.accountPlugin.link:
      window.history.pushState(config.accountPlugin.headerName, config.accountPlugin.headerName, config.accountPlugin.link);
      options.link = config.accountPlugin.link;
      options.headerName = config.accountPlugin.headerName;
      options.script = config.accountPlugin.script;
      options.pageId = config.accountPlugin.pageId;

      break;
    case config.accountScore.link:
      window.history.pushState(config.accountScore.headerName, config.accountScore.headerName, config.accountScore.link);
      options.link = config.accountScore.link;
      options.headerName = config.accountScore.headerName;
      options.script = config.accountScore.script;
      options.pageId = config.accountScore.pageId;

      break;
    case config.accountDomain.link:
      window.history.pushState(config.accountDomain.headerName, config.accountDomain.headerName, config.accountDomain.link);
      options.link = config.accountDomain.link;
      options.headerName = config.accountDomain.headerName;
      options.script = config.accountDomain.script;
      options.pageId = config.accountDomain.pageId;
      break;
    case config.accountLog.link:
      window.history.pushState(config.accountLog.headerName, config.accountLog.headerName, config.accountLog.link);
      options.link = config.accountLog.link;
      options.headerName = config.accountLog.headerName;
      options.script = config.accountLog.script;
      options.pageId = config.accountLog.pageId;
      break;
    case config.accountApplication.link:
      window.history.pushState(config.accountApplication.headerName, config.accountApplication.headerName, config.accountApplication.link);
      options.link = config.accountApplication.link;
      options.headerName = config.accountApplication.headerName;
      options.script = config.accountApplication.script;
      options.pageId = config.accountApplication.pageId;
      break;
    case config.installationManual.link:
      window.history.pushState(config.installationManual.headerName, config.installationManual.headerName, config.installationManual.link);
      options.link = config.installationManual.link;
      options.headerName = config.installationManual.headerName;
      options.script = config.installationManual.script;
      options.pageId = config.installationManual.pageId;
      break;
    default:
      webix.send(link, null, "GET");
    break;

  }

  generatePageContent(options);
}


function redirect(e) {
  if (e === 'accountName') {
    webix.send(accountNameUrl, null, "GET");
  } else if (e === 'logout') {
    storeSessionState();
    // webix.send(logoutUrl, null, "GET");
  }
}

var accountsMenuOptions = [];

if(activeAccount.account_type === 'agency'){
  accountsMenuOptions =[{
    id: "profile",
    value: "Profile",
    icon: "fa fa-angle-right",
    details: "Profile",
    href: "/account/profile"
  }
  , {
    id: "codesScripts",
    value: "Snippet",
    icon: "fa fa-angle-right",
    details: "Snippet",
    href: "/account/code-and-scripts"
  }, {
    id: "aPIToken",
    value: "API Token",
    icon: "fa fa-angle-right",
    details: "API Token",
    href: "/account/api-token"
  }, {
    id: "plugins",
    value: "Plugins",
    icon: "fa fa-angle-right",
    details: "Plugins",
    href: "/account/plugins"
  }
  , {
    id: "users",
    value: "Users",
    icon: "fa fa-angle-right",
    details: "Users",
    href: "/account/users"
  }, {
    id: "clientAccounts",
    value: "Client Accounts",
    icon: "fa fa-angle-right",
    details: "Client Accounts",
    href: "/account/client-accounts"
  },
   {
    id: "domains",
    value: "Domains",
    icon: "fa fa-angle-right",
    details: "Domains",
    href: "/account/domains"
  },
  {
    id: "installationmanual",
    value: "Documentation",
    icon: "fa fa-angle-right",
    details: "Documentation",
    href: "/account/installation-manual"
  },
  {
    id: "logs",
    value: "Logs",
    icon: "fa fa-angle-right",
    details: "Logs",
    href: "/account/logs"
  },
  {
    id: "systemStatus",
    value: "Status",
    icon: "fa fa-angle-right",
    details: "Status",
    href: "javascript:void(0)"
  }];
} else {
  accountsMenuOptions = [{
    id: "profile",
    value: "Profile",
    icon: "fa fa-angle-right",
    details: "Profile",
    href: "/account/profile"
  }
  , {
    id: "codesScripts",
    value: "Snippet",
    icon: "fa fa-angle-right",
    details: "Snippet",
    href: "/account/code-and-scripts"
  }, {
    id: "aPIToken",
    value: "API Token",
    icon: "fa fa-angle-right",
    details: "API Token",
    href: "/account/api-token"
  }, {
    id: "plugins",
    value: "Plugins",
    icon: "fa fa-angle-right",
    details: "Plugins",
    href: "/account/plugins"
  }
  , {
    id: "users",
    value: "Users",
    icon: "fa fa-angle-right",
    details: "Users",
    href: "/account/users"
  }, {
    id: "clientAccounts",
    value: "Client Accounts",
    icon: "fa fa-angle-right",
    details: "Client Accounts",
    href: "/account/client-accounts"
  },
   {
    id: "domains",
    value: "Domains",
    icon: "fa fa-angle-right",
    details: "Domains",
    href: "/account/domains"
  },
  {
    id: "installationmanual",
    value: "Documentation",
    icon: "fa fa-angle-right",
    details: "Documentation",
    href: "/account/installation-manual"
  },
  {
    id: "logs",
    value: "Logs",
    icon: "fa fa-angle-right",
    details: "Logs",
    href: "/account/logs"
  },
  {
    id: "systemStatus",
    value: "Status",
    icon: "fa fa-angle-right",
    details: "Status",
    href: "javascript:void(0)"
  }
  ];
}

webix.ready(function() {

  //show menu icons only after 5 seconds
  if(config.currentElementID == 'webixDashboardElementExperiment'){
    setTimeout(function(){
      showSmallMenu(config.currentElementID);
    }, 9000);
  }else {
    setTimeout(function(){
      showSmallMenu(config.currentElementID);
    }, 5000);
  }


  $("#nav").mouseenter(function(){
      clearTimeout($(this).data('timeoutId'));
      showFullMenu(config.currentElementID);
  }).mouseleave(function(){
      var someElement = $(this),
          timeoutId = setTimeout(function(){
              showSmallMenu(config.currentElementID);
          }, 650);
      //set the timeoutId, allowing us to clear this trigger if the mouse comes back over
      someElement.data('timeoutId', timeoutId);
  });

  // top site header

  config.styles.defaultHeight.updateHeights();

  webix.ui({
    container: "header",
    view: "toolbar",
    id: "myHeader",
    width: 'auto',
    height: config.styles.defaultHeight.mainHeader,
    padding:0,
    cols: [{
      style: 'float:left;',
      template: "#title#",
      css: 'header-logo',
      width: 160,
      height: 40,
      data: {
        title: "Image One",
        src: "/images/logo_new.png"
      },
      template: function(obj) {
        // obj is a data record object
        return '<a href="/"><img src="' + obj.src + '"/></a>'
      }
    },{
      css : 'logout-link' ,
      id: "logoutLinkId",
      view : 'template' ,
      margin:0,
      template:function(obj){
        return '<a href="javascript:void(0);" onclick="storeSessionState();"><strong><i class="fa fa-power-off"></i></strong></a>' ;
      }
    }, {
      view: "button",
      id: "accountName1",
      type: "icon",
      icon: "angle-down",
      label: '<b>User: </b>' + accountName,
      width: 300,
      popup: "my_pop",
      borderless:true,
    } , {
      view: "button",
      id: "accountName2",
      type: "icon",
      icon: "angle-down",
      label: '<b>Account: </b>' + activeAccount.company,
      width: 220,
      popup: "accountSelectorsPop",
      borderless:true
    } ]
  });


  if(isChrome()){}
  else if(navigator.userAgent.indexOf("Firefox") != -1){}
  else{
    webix.alert({
      title: "Close",
      text: "This interface has been designed for modern browsers and only thoroughly tested with Chrome and Firefox. Your browser may work too - but it has not been tested and we do not support it.",
      type:"alert-error"
    });
  }






  // if (!webix.env.touch && webix.ui.scrollSize)
            // webix.CustomScroll.init();
  // accountName popup

  // webix.ui({
  // 	view:"popup",
  // 	id:"my_popaa",
  //   head:"Submenu",
  // 	width:300,
  // 	body:{
  // 		view:"list",
  // 		data:[ {id:"1", name:"Zoo", location: "New York"},
  // 				{id:"2", name:"Coffeebar", location:"Salt Lake City"},
  // 				{id:"3", name:"Teeparty", location:"Alabama"}
  // 		],
  // 		datatype:"json",
  // 		template:"#name# - #location#",
  // 		autoheight:true,
  // 		select:true
  // 	}
  // });

  var accSelz = [];
  if(accountSelectors.length > 0){
    for (var i = 0; i < accountSelectors.length; i++) {
      accSelz.push({
          id: "accSel__" + accountSelectors[i].id,
          icon: "user",
          value: "Activate: " + accountSelectors[i].name
      });
    }

  }

  // account name & details menu
  webix.ui({
    view: "popup",
    id: "accountSelectorsPop",
    head: "Submenu",
    width: 300,
    body: {
      view: "list",
      data: accSelz,
      on: {
        onItemClick: function(id) {
          var t = this.getItem(id);

          var accountId = id.split('__')[1];
          window.location = monolop_base_url + '/api/account/users/activate-new-account?accountId='+accountId;
          // webix.ajax().get("/api/account/users/activate-new-account?accountId="+accountId, function(res, data, XmlHttpRequest){
          //   res = JSON.parse(res);
          //   if(res.status === 'success'){
          //     window.location = document.URL;
          //     console.log("hello");
          //   } else {
          //     webix.message("An error occoured");
          //   }
          // });
        }
      },
      autoheight: true
    }
  }).hide();

  // username  & logout link
  webix.ui({
    view: "popup",
    id: "my_pop",
    head: "Submenu",
    width: 300,
    body: {
      view: "list",
      data: [{
        id: "accountName",
        icon: "user",
        value: "Profile"
      }, {
        id: "logout",
        icon: "cog",
        value: "Logout"
      }, ],
      on: {
        onItemClick: function(id) {
          if(id === "accountName"){
            var t = this.getItem(id);
            getPageContent('/account/profile');
          } else {
            redirect(id);
          }
        }
      },
      autoheight: true
    }
  }).hide();


  // left site menu bar

  webix.ui({
    container: "nav",
    view: "tree",
    id: "appmenu",
    subMenuPos: "right",
    scroll: false,
    layout: "y",
    height: config.styles.defaultHeight.getHeight(),
    autoheight:true,
    css: "menu",
    activeTitle: !0,
    select: !0,
    tooltip: {
      template: function(e) {
        return e.$count ? "" : e.details;
      }
    },
    template: function(obj, common) {
      return common.icon(obj, common) + '<span class="' + obj.icon + '" style="padding: 5px;"></span>' + obj.value;
    },
    on: {
      onBeforeSelect: function(e) {
        return this.getItem(e).$count ? !1 : void 0;
      },
      onAfterSelect: function(e) {
        $('#wrapper .sidebar').css({position: 'relative'});
        $('.header-logo').css('opacity', '1');
        var t = this.getItem(e);
        if(t.id == "systemStatus"){
            window.open('http://stats.monoloop.com/s3qonfsb33ph', '_blank');
        }else{
          getPageContent(t.href);
        }
      },
      onAfterOpen:function(id){
        var next = $("div[webix_tm_id='"+id+"']" ).next() ;
        $("div[webix_tm_id='"+id+"']" ).parent().addClass('expand') ;
        next.hide() ;
        next.slideDown();

        if(activeAccount.account_type === 'agency'){
          $("div.webix_tree_branch_2").has('div[webix_tm_id="clientAccounts"]').show();
        } else {
          $("div.webix_tree_branch_2").has('div[webix_tm_id="clientAccounts"]').hide();
        }

      },
      onBeforeClose:function(id){
        var item = this.getItem(id);

        var me = this;
        var next = $("div[webix_tm_id='"+id+"']" ).next() ;
        next.slideUp(function(){
          item.open = false ;
          $("div[webix_tm_id='"+id+"']" ).parent().removeClass('expand') ;
          me.data.callEvent("onStoreUpdated",[id, 0, "branch"]);
          me.callEvent("onAfterClose",[id]);
        });
        return false ;
      },
    },
    data: [{
        id: "experiment",
        value: "Experiences",
        icon: "icon icon-beaker",
        details: "Experience",
        href: "/experiment"
      }, {
        id: "segment",
        value: "Audiences",
        icon: "icon icon-group",
        details: "Audience",
        href: "/segment"
      },{
        id: "content",
        icon: "icon icon-list-ul",
        open: 0,
        id: "web",
        value: "Pages",
        details: "Web",
        href: "/content/web"
      }, {
        id: "goals",
        value: "Goals",
        icon: "icon icon-flag-checkered",
        details: "Goals",
        href: "/goals"
      },{
        id: "tracker",
        value: "Tracker",
        icon: "fa fa-crosshairs",
        details: "Tracker",
        href: "/tracker"
      },{
        id: "media",
        value: "Media",
        icon: "fa fa-book",
        details: "Media",
        href: "/media"
      },{
        id: "account",
        icon: "fa fa-cogs",
        open: 0,
        value: "Settings",
        data: accountsMenuOptions
      }
    ],
    type: {
      subsign: true,
      height: 50
    }
  });
});


var fab = {
  initActionButtons: function(fabOptions){
    if(MONOloop.jq('#floatingContainer').length > 0){
      MONOloop.jq('#floatingContainer').remove();
    }
    var body = MONOloop.jq('body');

    var floatingContainer = document.createElement('div');
    floatingContainer.id = 'floatingContainer';
    floatingContainer.className = 'floatingContainer ml';
    if(fabOptions.type){
      floatingContainer.className += ' ' + fabOptions.type;
    }


    var elementOption = fabOptions.actionButtonTypes[fabOptions.actionButtonTypes.type],
        buttons = elementOption.options;

    for (var i = 0; i < buttons.length; i++) {
      var subActionButton = document.createElement('div');
      subActionButton.className = "subActionButton " + buttons[i].className;
      subActionButton.onclick = buttons[i].callback;

      var floatingText = document.createElement('p');
      floatingText.className = "floatingText";

      var floatingTextBG = document.createElement('span');
      floatingTextBG.className = "floatingTextBG";
      floatingTextBG.textContent = buttons[i].label;

      floatingText.appendChild(floatingTextBG);

      subActionButton.appendChild(floatingText);

      floatingContainer.appendChild(subActionButton);
    }

    var actionButton = document.createElement('div');
    actionButton.className = "actionButton";

    var floatingText = document.createElement('p');
    floatingText.className = "floatingText";

    var floatingTextBG = document.createElement('span');
    floatingTextBG.className = "floatingTextBG";

    floatingTextBG.textContent = "View Options";

    fabOptions.primary.primaryBtnLabelCallback(actionButton,floatingTextBG);

    floatingText.appendChild(floatingTextBG);

    actionButton.appendChild(floatingText);

    floatingContainer.appendChild(actionButton);

    body.append(floatingContainer);

    fab.triggerActionButtonEvents(fabOptions);

  },
  triggerActionButtonEvents: function(fabOptions){
    MONOloop.jq('.floatingContainer').hover(function(){
      MONOloop.jq('.subActionButton').addClass('display'); // comment
    }, function(){
      MONOloop.jq('.subActionButton').removeClass('display'); // comment
      MONOloop.jq('.actionButton').removeClass('open'); // comment
    });
    MONOloop.jq('.subActionButton').hover(function(){
      MONOloop.jq(this).find('.floatingText').addClass('show');
    }, function(){
      MONOloop.jq(this).find('.floatingText').removeClass('show');
    });

    MONOloop.jq('.actionButton').hover(function(){
      MONOloop.jq(this).addClass('open');
      MONOloop.jq(this).find('.floatingText').addClass('show');
      MONOloop.jq('.subActionButton').addClass('display');
    }, function(){
      MONOloop.jq(this).find('.floatingText').removeClass('show');
    });
    fabOptions.actionButtonTypes.setType();
  },
};

function validateEmail(email){
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function getBaseUrl(){
  pathArray = location.href.split( '/' );
  protocol = pathArray[0];
  host = pathArray[2];
  url = protocol + '//' + host;
  return url;
}

function storeSessionState(){

  var page = window.location.href,
      pathname = window.location.pathname;

  var state = {
    page: page,
    data: {pathname: pathname}
  };

  switch (pathname) {
    case "/segment/add":
      state.section = 'segment';
      state.data.type = 'add';
      switch (activeStepSegment) {
        case 'basic':
          state.data.step = 'basic';
          state.data.basicData = $$("basicFormSegment").getValues();
          break;
        case 'conditions':
          state.data.step = 'conditions';
          state.data.basicData = $$("basicFormSegment").getValues();
          state.data.conditionsData = iFrameWin.returnCondition.logical;
          break;
        case 'confirm':
          state.data.step = 'confirm';
          state.data.basicData = $$("basicFormSegment").getValues();
          state.data.conditionsData = iFrameWin.returnCondition.logical;
          state.data.confirmData = $$("confirmFormSegment").getValues();
          break;
        default:
          break;

      }
      break;
    case "/segment/edit":
      state.section = 'segment';
      state.data.type = 'edit';
      switch (activeStepSegment) {
        case 'basic':
          state.data.step = 'basic';
          state.data.basicData = $$("basicFormSegment").getValues();
          break;
        case 'conditions':
          state.data.step = 'conditions';
          state.data.basicData = $$("basicFormSegment").getValues();
          state.data.conditionsData = iFrameWin.returnCondition.logical;
          break;
        case 'confirm':
          state.data.step = 'confirm';
          state.data.basicData = $$("basicFormSegment").getValues();
          state.data.conditionsData = iFrameWin.returnCondition.logical;
          state.data.confirmData = $$("confirmFormSegment").getValues();
          break;
        default:
          break;

      }
      break;
    case "/experiment/add":
      state.section = 'experiment';
      state.data.type = 'add';
      switch (experimentListConfig.activeStep) {
        case 'segment':
          state.data.step = 'segment';
          state.data.basicData = $$("segmentFormExperiment").getValues();
          break;
        case 'placement':
          state.data.step = 'placement';
          state.data.basicData = $$("segmentFormExperiment").getValues();
          state.data.experiment_id = experimentListConfig.experiment._id;

          state.data._step = experimentListConfig.currentStepInfo.step;
          state.data.url = experimentListConfig.currentStepInfo.url;
          switch (experimentListConfig.currentStepInfo.step) {
            case 'pw':

              break;
            case "edit_record":
              state.data.editRecordData = experimentListConfig.placementWindow.edit_record;
              break;
            case "testbench":

              break;
            case "publish":
              state.data.publishData = experimentListConfig.placementWindow.publish.$$("publishFormID").getValues();

              break;
            default:
              break;
          }

          break;
        case 'confirm':
          state.data.step = 'confirm';
          state.data.basicData = $$("segmentFormExperiment").getValues();
          state.data.experiment_id = experimentListConfig.experiment._id;
          state.data.confirmData = $$("confirmFormExperiment").getValues();
          break;
        default:
          break;

      }
      break;
    case "/experiment/edit":
      state.section = 'experiment';
      state.data.type = 'edit';
      switch (experimentListConfig.activeStep) {
        case 'segment':
          state.data.step = 'segment';
          state.data.basicData = $$("segmentFormExperiment").getValues();
          break;
        case 'placement':
          state.data.step = 'placement';
          state.data.basicData = $$("segmentFormExperiment").getValues();
          state.data.experiment_id = experimentListConfig.experiment._id;
          break;
        case 'confirm':
          state.data.step = 'confirm';
          state.data.basicData = $$("segmentFormExperiment").getValues();
          state.data.experiment_id = experimentListConfig.experiment._id;
          state.data.confirmData = $$("confirmFormExperiment").getValues();
          break;
        default:
          break;

      }
      break;
    case "/tracker/edit":
      state.section = 'tracker';
      state.data.type = 'edit';
      break;
    case "/goals/add":
      state.section = 'goals';
      state.data.type = 'add';
      switch (activeStepGoal) {
        case 'basic':
          state.data.step = 'basic';
          state.data.basicData = $$("basicFormGoal").getValues();
          break;
        case 'placement':
          state.data.step = 'placement';
          state.data.basicData = $$("basicFormGoal").getValues();
          if(PWIFrameWin){
            state.data.placementData = PWIFrameWin.allFormDataPlacementWindowGoal.data;
          } else {
            if(userStateData && userStateData.data.placementData){
              state.data.placementData  = userStateData.data.placementData;
            }
          }
          state.data.page_element = currentPageElementEditGoal;
          break;
        case 'conditions':
          state.data.step = 'conditions';
          state.data.basicData = $$("basicFormGoal").getValues();
          if(PWIFrameWin){
            state.data.placementData = PWIFrameWin.allFormDataPlacementWindowGoal.data;
          } else {
            if(userStateData && userStateData.data.placementData){
              state.data.placementData  = userStateData.data.placementData;
            }
          }
          state.data.page_element = currentPageElementEditGoal;
          state.data.conditionsData = iFrameWin.returnCondition.logical;
          break;
        case 'confirm':
          state.data.step = 'confirm';
          state.data.basicData = $$("basicFormGoal").getValues();
          if(PWIFrameWin){
            state.data.placementData = PWIFrameWin.allFormDataPlacementWindowGoal.data;
          } else {
            if(userStateData && userStateData.data.placementData){
              state.data.placementData  = userStateData.data.placementData;
            }
          }
          state.data.conditionsData = iFrameWin.returnCondition.logical;
          state.data.page_element = currentPageElementEditGoal;
          state.data.confirmData = $$("confirmFormGoal").getValues();
          break;
        default:
          break;

      }
      break;
    case "/goals/edit":
      state.section = 'goals';
      state.data.type = 'edit';
      switch (activeStepGoal) {
        case 'basic':
          state.data.step = 'basic';
          state.data.basicData = $$("basicFormGoal").getValues();
          break;
        case 'placement':
          state.data.step = 'placement';
          state.data.basicData = $$("basicFormGoal").getValues();
          if(PWIFrameWin){
            state.data.placementData = PWIFrameWin.allFormDataPlacementWindowGoal.data;
          } else {
            if(userStateData && userStateData.data.placementData){
              state.data.placementData  = userStateData.data.placementData;
            }
          }
          state.data.page_element = currentPageElementEditGoal;
          break;
        case 'conditions':
          state.data.step = 'conditions';
          state.data.basicData = $$("basicFormGoal").getValues();
          if(PWIFrameWin){
            state.data.placementData = PWIFrameWin.allFormDataPlacementWindowGoal.data;
          } else {
            if(userStateData && userStateData.data.placementData){
              state.data.placementData  = userStateData.data.placementData;
            }
          }
          state.data.page_element = currentPageElementEditGoal;
          state.data.conditionsData = iFrameWin.returnCondition.logical;
          break;
        case 'confirm':
          state.data.step = 'confirm';
          state.data.basicData = $$("basicFormGoal").getValues();
          if(PWIFrameWin){
            state.data.placementData = PWIFrameWin.allFormDataPlacementWindowGoal.data;
          } else {
            if(userStateData && userStateData.data.placementData){
              state.data.placementData  = userStateData.data.placementData;
            }
          }
          state.data.conditionsData = iFrameWin.returnCondition.logical;
          state.data.page_element = currentPageElementEditGoal;
          state.data.confirmData = $$("confirmFormGoal").getValues();
          break;
        default:
          break;

      }
      break;
    case "/content/web/add":
      state.section = 'web';
      state.data.type = 'add';
      state.data.step = webPageListConfig.currentStepInfo.step;
      state.data.url = webPageListConfig.currentStepInfo.url;
      switch (webPageListConfig.currentStepInfo.step) {
        case 'pw':

          break;
        case "edit_record":
          state.data.editRecordData = webPageListConfig.placementWindow.edit_record;
          break;
        case "testbench":

          break;
        case "publish":
          state.data.publishData = webPageListConfig.placementWindow.publish.$$("publishFormID").getValues();

          break;
        default:
          break;
      }
      break;
    case "/content/web/edit":
      state.section = 'web';
      state.data.type = 'edit';
      state.data.step = webPageListConfig.currentStepInfo.step;
      state.data.url = webPageListConfig.currentStepInfo.url;
      switch (webPageListConfig.currentStepInfo.step) {
        case 'pw':

          break;
        case "edit_record":
          state.data.editRecordData = webPageListConfig.placementWindow.edit_record;
          break;
        case "testbench":

          break;
        case "publish":
          state.data.publishData = webPageListConfig.placementWindow.publish.$$("publishFormID").getValues();

          break;
        default:
          break;

      }

      break;
    case "/account/profile":
      state.section = 'account-profile';
      state.data.basicData = $$("profileForm").getValues();
      break;
    case "/account/control-group":
      state.section = 'account-control-group';
      state.data.basicData = $$("controlGroup").getValues();
      break;
    case "/account/code-and-scripts":
      state.section = 'account-code-and-scripts';
      state.data.invocation = $$("invocationForm").getValues();
      state.data.advancedScript = $$("advancedScriptForm").getValues();
      break;
    case "/account/api-token":
      state.section = 'account-api-token';
      state.data.basicData = $$("apiTokenForm").getValues();
      break;
    case "/account/scores":
      state.section = 'account-scores';
      state.data.basicData = $$("scoresForm").getValues();
      break;
    default:
      break;

  }
  webix.ajax().post(monolop_api_base_url+"/api/store-session-state", {
    state: JSON.stringify(state)
  }, {
    error: function(text, data, XmlHttpRequest) {
      alert("error");
    },
    success: function(text, data, XmlHttpRequest) {
      var response = JSON.parse(text);
      if (response.success == true) {
        var _logoutUrl = monolop_base_url + '/auth/logout';
        webix.send(_logoutUrl, null, "GET");
      }
    }
  });
}

function startGuidelinePopovers(options){

  guidelines.page.pageId = options.page;

  guidelines.startedBefore = false;

  var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+guidelines.page.pageId;

  webix.ajax().get(fetchGuidelinesUrl, { }, function(text, xml, xhr){
    var r = JSON.parse(text);

    var cg = undefined;
    if(r.currentGuide){
      cg = JSON.parse(r.currentGuide);
      guidelines.startedBefore = true;
    }

    guidelines[guidelines.page.pageId] = r.guidelines;

    if(cg){
      if(cg.currentGuide){

        guidelines.page.currentGuide = cg.currentGuide;
      } else {
        guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
      }
    } else {
      guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
    }
    if(guidelines.page.currentGuide){
      // guidelines.generate(guidelines.page.currentGuide);
      if(guidelines.startedBefore === false){
        setTimeout(function(){guidelines.generate(guidelines.page.currentGuide);},2000);
      }
    }
    guidelines.startedBefore = false;
  });
}


function initIntialTTandGuides(tip_timeout, g_timeout, options){

  tip_timeout = tip_timeout || 500;
  g_timeout = g_timeout || 500;  //marc changed from 2000 to 500

  // marc added
  if(tooltips.startedBefore === false && guidelines.startedBefore === false){
    if(tooltips.page.currentTip != undefined){
      setTimeout(function(){tooltips.generate(tooltips.page.currentTip);}, tip_timeout);
    }else if(guidelines.page.currentGuide){
      setTimeout(function(){guidelines.generate(guidelines.page.currentGuide, options);}, g_timeout);
    }
  }else{
    if($('#toggleTips').hasClass('fa-toggle-on')){
      $('#toggleTips').removeClass('fa-toggle-on');
      $('#toggleTips').addClass('fa-toggle-off');
      $('#toggleTips').attr('title', 'Show Tips');
    }
  }

  /* marc commented out
  if(tooltips.startedBefore === false){
    setTimeout(function(){tooltips.generate(tooltips.page.currentTip);}, tip_timeout);
  }else{
    // update toggle button
    if($('#toggleTips').hasClass('fa-toggle-on')){
      $('#toggleTips').removeClass('fa-toggle-on');
      $('#toggleTips').addClass('fa-toggle-off');
      $('#toggleTips').attr('title', 'Show Tips');
    }
  }

  var toggleOffGuides =  false;
  if(tooltips.page.currentTip === undefined){
    if(guidelines.startedBefore === false){
      setTimeout(function(){guidelines.generate(guidelines.page.currentGuide, options);}, g_timeout);
    }else{
      toggleOffGuides = true;
    }
  }else{
    toggleOffGuides = true;
  }

  if(toggleOffGuides){
    // update toggle button
    if($('#toggleGuides').hasClass('fa-toggle-on')){
      $('#toggleGuides').removeClass('fa-toggle-on');
      $('#toggleGuides').addClass('fa-toggle-off');
      $('#toggleGuides').attr('title', 'Show Guides');
    }
  }
  */

}


function setTimezoneCookie(){
  var cookie_name = 'time_zone';
  var timezone = jstz.determine();
  var date,days=2,expires = "";
  if (days)
  {
    date = new Date( );
    date.setTime( date.getTime( )+( days*24*60*60*1000));
    expires = "; expires="+date.toGMTString( );
  }
  else  expires = "";
  document.cookie = cookie_name+"="+timezone.name()+expires+"; path=/";
}

function startGuidelinesInWizard(options){
  if(options.src === 'layout'){

    if(guidelines.page.currentGuide){
      if(guidelines.startedBefore === false){
        guidelines.generate(guidelines.page.currentGuide);
      }
    }
    guidelines.startedBefore = false;
  } else {
    startGuidelinePopovers({m: options.mode, page: options.mode === 'edit' ? options.edit : options.add});

  }
}

$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`

       if(tooltips.open === true){
         tooltips.saveInSession('yes','guidelines');
         if(guidelines.startedBefore === false){
           setTimeout(function(){guidelines.generate(guidelines.page.currentGuide);},1000);
         }
       } else {
          guidelines.saveInSession();
       }


    }
});

function checkFolderStatus(id, fileManager) {
  webix.ajax()
  .post(monolop_api_base_url+ "/api/folders/status"
  , { folder_id: id } // data
  , {
        error: function(text, data, XmlHttpRequest) {
          webix.message({type: "error", text: "Server error."});
        },
        success: function(text, data, XmlHttpRequest) {
          var response = JSON.parse(text);
          if(response.success === true)
              fileManager.deleteFile(id);
          else{
            webix.confirm({
              text: response.msg,
              ok: 'Delete folder!',
              cancel: 'Cancel',
              callback: function(result){
                if(result)
                    fileManager.deleteFile(id);
              }
            });
          }
    }
  });
}

window.addEventListener('popstate', function(e){
  getPageContent(window.location.pathname);
});

// add event listeners at each http ajax request
(function() {
    var origOpen = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function() {
        // console.log('request started!');
        this.addEventListener('load', function() {
            // console.log('request completed!');
            if(this.status === 401){
              window.location = '/';
            }

        });
        origOpen.apply(this, arguments);
    };


  setTimezoneCookie();
})();

function isChrome() {
  var isChromium = window.chrome,
    winNav = window.navigator,
    vendorName = winNav.vendor,
    isOpera = winNav.userAgent.indexOf("OPR") > -1,
    isIEedge = winNav.userAgent.indexOf("Edge") > -1,
    isIOSChrome = winNav.userAgent.match("CriOS");

  if(isIOSChrome){
    return true;
  } else if(isChromium !== null && isChromium !== undefined && vendorName === "Google Inc." && isOpera == false && isIEedge == false) {
    return true;
  } else {
    return false;
  }
}

function showSmallMenu(elemId){
  // if(elemId != null && elemId != 'webixWizardHeaderMenuWebPageList'){
  if(elemId != null){
    $('#wrapper .sidebar').animate({width: '40px'});
    $('#nav .menu').animate({width: '32px'}, function(){
      $("#main").css({'width': $('body').width() - $('.sidebar').width() - 1 + 'px'});
      $$(elemId).resize();
      // $("#mainContainer").css('position', 'absolute');
      $('#wrapper .sidebar').css({position: 'absolute', 'z-index':'107'});
      $("#main").css('left','40px');
    });
    $('#nav .menu .webix_tree_close, #nav .menu .webix_tree_open, .menu .webix_tree_item .fa-angle-right').animate({opacity: '0'});
    $('#nav .menu  .webix_tree_branch_1.expand').css({'border-right': '0px solid #0F9FD5'});
    if(elemId == 'parentLayout'){
      setTimeout(function(){
        $$('AccountUsersDatatable').define('width', $("#webixClientAccountGrid").width());
        $$('AccountUsersDatatable').resize();
      }, 1000);
    }
    if(elemId == "profileForm"){
      setTimeout(function(){
        $$('profileForm').define('width', $("#innerContent").width());
        $$('profileForm').resize();
      }, 1000);
    }

    if(elemId == "apiTokenForm"){
      setTimeout(function(){
        $$('apiTokenForm').define('width', $("#webixapiTokenForm").width());
        $$('apiTokenForm').resize();
      }, 1000);
    }

    if(elemId == 'webixDomainsElement'){
      setTimeout(function(){
        $$('domainsDatatable').define('width', $("#webixDomainsElement").width());
        $$('domainsDatatable').resize();
      }, 1000);
    }

    if(elemId == 'webixCodeScriptAccordion'){
      setTimeout(function(){
        var width = ($('#webixCodeScriptAccordion').width()) / 2;
        $$('columnOne').define('width', width);
        $$('columnOne').resize();
        $$('invocationForm').define('width', width);
        $$('invocationForm').resize();

        var labelTemp = $("div[view_id=scriptLabel] div:first-child");
        labelTemp.css('line-height', '18px');
      }, 1000);
    }

    if(elemId == 'webixWizardHeaderMenuWebPageList'){
      $('.header-logo').css('opacity', '0');
      setTimeout(function(){
        $$('PWStepWebPageList').define('width', $("#innerContent").width());
        $$('PWStepWebPageList').resize();
      }, 1000);

    }

  }
}

function showFullMenu(elemId){
  // if(elemId != null && elemId != 'webixWizardHeaderMenuWebPageList'){
  if(elemId != null){

    // $("#main").css({'width': $('body').width() - 183 + 'px'});
    // $$(elemId).resize();
    //
    // if(elemId == 'parentLayout'){
    //   $$('AccountUsersDatatable').define('width', $("#webixClientAccountGrid").width());
    //   $$('AccountUsersDatatable').resize();
    // }
    // if(elemId == "profileForm"){
    //   $$('profileForm').define('width', $("#innerContent").width());
    //   $$('profileForm').resize();
    // }
    //
    // if(elemId == "apiTokenForm"){
    //   $$('apiTokenForm').define('width', $("#webixapiTokenForm").width());
    //   $$('apiTokenForm').resize();
    // }
    //
    // if(elemId == 'webixDomainsElement'){
    //   $$('domainsDatatable').define('width', $("#webixDomainsElement").width());
    //   $$('domainsDatatable').resize();
    // }
    //
    // if(elemId == 'webixCodeScriptAccordion'){
    //   var width = ($('#webixCodeScriptAccordion').width()) / 2;
    //   $$('columnOne').define('width', width);
    //   $$('columnOne').resize();
    //   $$('invocationForm').define('width', width);
    //   $$('invocationForm').resize();
    //
    //   $$('pre_invocation').define('width', width); // marc
    //   $$('pre_invocation').resize();               // marc
		// 	$$('post_invocation').define('width', width);// marc
    //   $$('post_invocation').resize();              // marc
    //
    //   var labelTemp = $("div[view_id=scriptLabel] div:first-child");
    //   labelTemp.css('line-height', '18px');
    // }

    $('#nav .menu .webix_tree_close, #nav .menu .webix_tree_open, .menu .webix_tree_item .fa-angle-right').animate({opacity: '1'});
    $('#wrapper .sidebar').animate({width: '182px'});
    $('#nav .menu').animate({width: '180px'});
    $('#nav .menu  .webix_tree_branch_1.expand').css({'border-right': '5px solid #0F9FD5'});
  }
  // else if (elemId == 'webixWizardHeaderMenuWebPageList') {
  //   $("#main").css({'width': $('body').width() - 183 + 'px', 'left': '182px'});
  //   $('#nav .menu .webix_tree_close, #nav .menu .webix_tree_open, .menu .webix_tree_item .fa-angle-right').animate({opacity: '1'});
  //   $('#wrapper .sidebar').animate({width: '182px'});
  //   $('#nav .menu').animate({width: '180px'});
  //   $('#nav .menu  .webix_tree_branch_1.expand').css({'border-right': '5px solid #0F9FD5'});
  // }
}

// function resizeIframe() {
//     var $iframes = $("iframe");
//     $iframes.each(function() {
//         var iframe = this;
//         $(iframe).load(function() {
//             iframe.height = iframe.contentWindow.document.body.scrollHeight + 35;
//         });
//     });
// }


// window.onresize = function(){
//   // if($('div[view_id="placementStepExperiment"]').length > 0)
//   // {
//   //   // resizeIframe();
//   //   setTimeout(function(){
//   //     //window.location.reload();
//   //     $("#main").css('width', $('body').width() - $('.sidebar').width() - 1 + 'px');
//   //     $("#main div").css('width', '100%');
//   //     //$("iframe").css({"display": "block", "border": "none", "height": "100vh", "width": "100vw"});
//   //     //$("#main div").css('height', '100%');
//   //   });
//   // }
// }
