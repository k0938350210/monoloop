var folderFileManagerSegment; // file manager variable
var msg; // default variable for user messages

var stateSegment;
var isSessional = false;

var defaultStepsSegment;
var activeStepSegment;
var isBasicGeneratedSegment, isConditionsGeneratedSegment, isConfirmGeneratedSegment;
var conditionBuilderUrlSegment = '/component/profile-properties-template';
var iFrameWin;
var tempConditionSegment = {};

var allFormDataSegment = {};
// 14OCT
var localStorageDataSegment = {};
localStorageDataSegment.active = false;
localStorageDataSegment.basic = {};
localStorageDataSegment.basic.isSaved = false;
localStorageDataSegment.basic.Data = {};
localStorageDataSegment.conditions = {};
localStorageDataSegment.conditions.isSaved = false;
localStorageDataSegment.conditions.Data = {};
localStorageDataSegment.confirm = {};
localStorageDataSegment.confirm.isSaved = false;
localStorageDataSegment.confirm.Data = {};

var extendedFab = {
  actionButtonTypes: {
    type: "create",
    setType: function(t){
      t = t || "create";
      extendedFab.actionButtonTypes.type = t;
    },
    fabOptions: false,
    create: {
      options: [
        // {
        // 	label: "This is to test",
        // 	className: "segmentBuilder inline",
        //   callback: function(){
        //     webix.alert("This is to test......");
        //   }
        // }
      ]
    },
  },
  fabOptions: false,
  primary: {
    primaryBtnLabelCallback: function(actionButton, floatingTextBG){
      actionButton.className += " createSegment inline";
      floatingTextBG.textContent = "Create audience";
      actionButton.onclick = function(){
          config.currentElementID = 'webixWizardHeaderMenuSegment';
          guidelines.removeHangedTooltips();
          openWizardSegment('add');
      };
    },
  }


};

function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
};

function openLayoutSegment() {
  var params = getQueryParams(document.location + '');
  var f = window.location.hash;
  window.history.pushState(config.segment.headerName, config.segment.headerName, config.segment.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);

  $("#innerContent").html("");

  var formElement = document.createElement('div');
  formElement.id = 'fileManagerParentIdSegment';
  formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
  document.getElementById("innerContent").appendChild(formElement);

  // webix.ready(function() {

  folderFileManagerSegment = webix.ui({
    container: "fileManagerParentIdSegment",
    view: "filemanager",
    url: monolop_api_base_url+ "/api/segments?ajax=true" + (params.url ? ('&url=' + params.url) : ''), // loading data from the URL
    id: "webixFilemanagerSegment",
    // height: 500,
    filterMode: {
      showSubItems: true,
      openParents: true
    },
    mode: "table", // specify modeSegment selected by default
    modes: ["files", "table"], // all available modes including a new modeSegment
    // save and handle all the menu actionsSegment from here,
    // disable editing on double-click

    handlers: {
      "upload": monolop_api_base_url+ "/api/segments/create",
      // "download": "data/saving.php",
      // "copy": "data/saving.php",
      "move": monolop_api_base_url+ "/api/folders/move",
      "remove": monolop_api_base_url+ "/api/folders/delete",
      "rename": monolop_api_base_url+ "/api/folders/update",
      "create": monolop_api_base_url+ "/api/folders/create",
      "files": monolop_api_base_url+ "/api/segments/folder?ajax=true" + (params.url ? ('&url=' + params.url) : ''),
      // "branch": "../common/data_branch.php",
    },
    structure: {

    },
    on: {
      "onViewInit": function(name, config) {
        if (name == "table" || name == "files") {
          // disable multi-selection for "table" and "files" views
          config.select = true;

          if (name == "table") {
            // disable editing on double-click
            config.editaction = false;
            // an array with columns configuration
            var columns = config.columns;
            //  disabling columns date, type, size
            columns.splice(1, 3);

            // configuration of a new column description
            // var descriptionColumnSegment = {
            //   id: "descriptionColumnSegment",
            //   header: "Description",
            //   fillspace: 3,
            //   template: function(obj, common) {
            //     return obj.description || ""; // "description" property of files
            //   }
            // };

            // configuration of a new column date
            // var dateColumnSegment = {
            //   id: "dateColumnSegment",
            //   header: "Date",
            //   fillspace: 2,
            //   sort: function(a, b){
            //     var _a = new Date(a.date), _b = new Date(b.date);
            //     return (_a > _b ? 1 : (_b > _a ? -1: 0));
            //   },
            //   template: function(obj, common) {
            //     return obj.date || ""; // "description" property of files
            //   }
            // };

            // configuration of a new column actions
            // var reportColumnSegment = {
            //   id: "reportColumnSegment",
            //   header: "Report",
            //   // fillspace: 1,
            //   width: 70,
            //   template: function(obj, common) {
            //     var url  = '/reports/segment/detail/'+obj.segmentid + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURI(params.url)) : '');
            //     return '<a  title="Audience report" onclick="window.location = \'' + url + '\';" class="webix_list_item viewReport" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-bar-chart"></span></a>';
            //   }
            // };

            // configuration of a new column actionsSegment
            // var actionsColumnSegment = {
            //   id: "actionsColumnSegment",
            //   header: "Actions",
            //   css: "actionsCol",
            //   fillspace: 1,
            //   template: function(obj, common) {
            //
            //     return '<a title="Edit audience" onclick="updateSegment(\'' + obj.id + '\');" class="updateSegment webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" title="delete" onclick="deleteSegment(\'' + obj.id + '\');" class="deleteSegment webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
            //   }
            // };

            // changes related to Mantis # 3974
            var currentColumnSegment = {
              id: "currentColumnSegment",
              header: "Current",
              fillspace: 2,
              template: function(obj, common) {
                return obj.current || 0; // "description" property of files
              }
            };

            var previousColumnSegment = {
              id: "previousColumnSegment",
              header: "Previous",
              fillspace: 2,
              template: function(obj, common) {
                return obj.previous || 0; // "description" property of files
              }
            };

            var changeColumnSegment = {
              id: "changeColumnSegment",
              header: "Change (%)",
              fillspace: 2,
              template: function(obj, common) {
                return obj.change || 0; // "description" property of files
              }
            };

            // configuration of a new column actionsSegment
            var actionsColumnSegment = {
              id: "actionsColumnSegment",
              header: "Actions",
              css: "actionsCol",
              fillspace: 2,
              template: function(obj, common) {
                var url  = '/reports/segment/detail/'+obj.segmentid + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURI(params.url)) : '');
                return '<a title="Edit audience" onclick="updateSegment(\'' + obj.id + '\');" class="updateSegment webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" title="delete" onclick="deleteSegment(\'' + obj.id + '\');" class="deleteSegment webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>'+ ' | ' + '<a webix_l_id="remove" title="Audience report" onclick="window.location = \'' + url + '\';" class="webix_list_item viewReport" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-bar-chart"></span></a>';
              }
            };

            // configuration of a new column status
            var statusColumnSegment = {
              id: "statusColumnSegment",
              header: "Status",
              css: "statusCol",
              fillspace: 1,
              sort: function(a, b){
                var _a = Number(a.hidden), _b = Number(b.hidden);
                return (_a > _b ? 1 : (_b > _a ? -1: 0));
              },
              template: function(obj, common) {
                var obj_ = {
                  action: "update_hidden",
                  hidden: "1"
                };
                var paramsSegment = {
                  _id: obj.id,
                  action: "update_hidden",
                  node: this
                };
                if (obj.hidden == 0) {
                  paramsSegment.hidden = 1;
                  return '<a title="Deactivate audience" id="updateHiddenSegment' + obj.id + '"  onclick=\'updateHiddenSegment( ' + JSON.stringify(paramsSegment) + ');\' class="webix_list_item updateSegmentStatus status status0" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actionsSegment" property of files ;
                }
                paramsSegment.hidden = 0;
                return '<a title="Activate audience" id="updateHiddenSegment' + obj.id + '"  onclick=\'updateHiddenSegment(' + JSON.stringify(paramsSegment) + ');\' class="webix_list_item updateSegmentStatus status status1" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
              }
            };

            // insert columns
            webix.toArray(columns).insertAt(currentColumnSegment, 1);
            webix.toArray(columns).insertAt(previousColumnSegment, 2);
            webix.toArray(columns).insertAt(changeColumnSegment, 3);
            webix.toArray(columns).insertAt(actionsColumnSegment, 4);
            webix.toArray(columns).insertAt(statusColumnSegment, 5);
          }
        }
      }
    }
  });

  /*******************************Menu Customization******************************/
  // only FILES mode should be available
  $$('webixFilemanagerSegment').$$('modes').hide();

  // updating options from menu
  var actionsSegment = $$("webixFilemanagerSegment").getMenu();
  actionsSegment.clearAll();
  var newDataSegment = [

    {
      id: "create",
      method: "createFolder",
      icon: "folder-o",
      value: webix.i18n.filemanager.create // "Create Folder"
    }, {
      id: "deleteFile",
      method: "deleteFile",
      icon: "times",
      value: webix.i18n.filemanager.remove // "Delete"
    }, {
      id: "edit",
      method: "editFile",
      icon: "edit",
      value: webix.i18n.filemanager.rename // "Rename"
    }
  ];
  actionsSegment.parse(newDataSegment);
  // add new option for the menu to add new segment
  actionsSegment.add({
    id: "createSegment",
    icon: "file",
    value: "Create Audience"
  });

  /*******************************Segment Add**********************************/

  actionsSegment.attachEvent("onItemClick", function(id) {
    this.hide();
    // check if the action is CreateSegment
    if (id == "createSegment") {
      config.currentElementID = 'webixWizardHeaderMenuSegment';
      openWizardSegment('add');
    }
  });

  actionsSegment.getItem("create").batch = "item, root";
  actionsSegment.getItem("deleteFile").batch = "item, root";
  actionsSegment.getItem("edit").batch = "item, root";
  actionsSegment.getItem("createSegment").batch = "item, root";

  /******************************Segment Add End*******************************/


  /*****************************Menu Customization End*************************/


  /*******************************Custom Events********************************/

  /********************************Segment Edit********************************/

  $$("webixFilemanagerSegment").$$("table").attachEvent("onBeforeSelect", function(id, p){
    var rowName = id.row,
        columnName = id.column;
    var item = this.getItem(rowName);
    if (item.type !== "folder"){
      if(['reportColumnSegment', 'actionsColumnSegment', 'statusColumnSegment'].indexOf(columnName) === -1){
      //  updateSegment(item.id);
      }
    } else {

    }
    //note that 'id' for datatable is an object with several attributes
  });
  $$("webixFilemanagerSegment").attachEvent("onBeforeDeleteFile",function(id){
      // Fix: mantis # 3945
      if (id.indexOf('segment__') === -1) {
				var item = this.getItem(id);
				if(item.$parent === 0){
					webix.message({type: "error", text: "Root folder can not be deleted."})
					return false;
				}else{
					checkFolderStatus(id, folderFileManagerSegment);
					return false;
				}
			}
  });
  // before editing file
  $$("webixFilemanagerSegment").attachEvent("onBeforeEditFile", function(id) {
    var srcTypeId = id.split('__');


    if (srcTypeId[0] == 'folder') {
      return true;
    }

    updateSegment(id);

    return false;
  });

  /*****************************Segment edit End*******************************/

  // reload grid after folder creation
  $$("webixFilemanagerSegment").attachEvent("onAfterCreateFolder", function(id) {
    // refreshManagerSegment();
    setTimeout(function(){ openLayoutSegment(); }, 500);
    return false;
  });


  // it will be triggered before deletion of file
  $$("webixFilemanagerSegment").attachEvent("onBeforeDeleteFile", function(ids) {
    // deleteSegment(ids);
    if (ids.indexOf('segment__') === -1) {
      return true
    }

    deleteSegment(ids);
    return false;
  });

  // it will be triggered before dragging the folder/segment
  $$("webixFilemanagerSegment").attachEvent("onBeforeDrag", function(context, ev) {
    return true;
  });

  $$("webixFilemanagerSegment").attachEvent("onBeforeDrop", function(context, ev) {
    if (context.start.indexOf('segment__') === -1) {
      return true
    }
    webix.ajax().post(monolop_api_base_url+ "/api/segments/change-folder", {
      _id: context.start,
      target: context.target,
      ajax: true,
    }, {
      error: function(text, data, XmlHttpRequest) {
        alert("error");
      },
      success: function(text, data, XmlHttpRequest) {
        var response = JSON.parse(text);
        if (response.success == true) {}
      }
    });
    return true;
  });
  // it will be triggered after dropping the folder to the destination
  $$("webixFilemanagerSegment").attachEvent("onAfterDrop", function(context, ev) {
    webix.message("Segment has been moved to new folder.");
    return false;
  });

  $$("webixFilemanagerSegment").$$("files").attachEvent("onBeforeRender", filterFilesSegment);
  $$("webixFilemanagerSegment").$$("table").attachEvent("onBeforeRender", filterFilesSegment);

  var _exe = false;
  $$("webixFilemanagerSegment").attachEvent("onAfterLoad", function(context, ev) {
    if(_exe === false){
      _exe = true;
      initIntialTTandGuides();
    }
  });


  // fix : mantis # 3946
  $$("webixFilemanagerSegment").attachEvent("onSuccessResponse", function(req, res) {
  	if(res.success && (req.source === "newFolder" || req.source.indexOf("folder") != -1)){
  		switch(req.action){
  			case "rename":
  				webix.message("Folder renamed");
  				break;
  			case "create":
  				webix.message("Folder Created");
  				break;
  			case "remove":
  				webix.message("Folder deleted");
  				break;
  		}
  	}
  });
  /*******************************Custom Events End****************************/
  // });

  fab.initActionButtons({
    type: "segmentList",
    actionButtonTypes: extendedFab.actionButtonTypes,
    primary: extendedFab.primary,
  });

  $(window).on('resize', function(){
    $$('webixFilemanagerSegment').define('width', $('body').width() - $('.sidebar').width() - 1);
    $$('webixFilemanagerSegment').resize();
  });

}
// filter segments only
function filterFilesSegment(data) {
  data.blockEvent();
  data.filter(function(item) {
    return item.type != "folder"
  });
  data.unblockEvent();
}

/**********************************Functions***********************************/
function updateSegment(id, src) {
  config.currentElementID = 'webixWizardHeaderMenuSegment';
  var url = monolop_api_base_url+ '/api/segments/show';

  // retreiving form data and setting into the form
  var formDataSegment;
  webix.ajax().get(url, {
    _id: id,
    ajax: true
  }, function(text, xml, xhr) {
    //response
    formDataSegment = JSON.parse(text);
    openWizardSegment('edit', formDataSegment, undefined, src);
  });
}

function deleteSegment(id) {
  webix.confirm({
    text: "Do you want to delete?",
    ok: "Yes",
    cancel: "No",
    callback: function(result) {
      if (result) {
        webix.ajax().post(monolop_api_base_url+ "/api/segments/delete", {
          _id: id,
          ajax: true,
        }, {
          error: function(text, data, XmlHttpRequest) {
            alert("error");
          },
          success: function(text, data, XmlHttpRequest) {
            var response = JSON.parse(text);
            if (response.success == true) {
              folderFileManagerSegment.deleteFile(id);
            }
          }
        });
      }
    }
  });

}

function updateHiddenSegment(paramsSegment) {
  // var paramsSegment = {source: id, action: action, hidden: hidden};
  if (paramsSegment.hidden == 1) {
    var message = webix.message("deactivating...");
  } else if (paramsSegment.hidden == 0) {
    var message = webix.message("activating...");
  }
  paramsSegment.ajax = true;
  webix.ajax().post(monolop_api_base_url+ "/api/segments/update-status", paramsSegment, {
    error: function(text, data, XmlHttpRequest) {
      alert("error");
    },
    success: function(text, data, XmlHttpRequest) {
      var response = JSON.parse(text);
      if (response.success == true && paramsSegment.action == "update_hidden") {
        var element = document.getElementById("updateHiddenSegment" + paramsSegment._id)
        if (paramsSegment.hidden == 0) {
          element.innerHTML = "active";
          element.style.color = "green";
          element.onclick = function() {
            paramsSegment.hidden = 1;
            updateHiddenSegment(paramsSegment);
          };
          webix.message("Segment has been activated.");
        } else if (paramsSegment.hidden == 1) {
          element.innerHTML = "inactive";
          element.style.color = "red";
          element.onclick = function() {
            paramsSegment.hidden = 0;
            updateHiddenSegment(paramsSegment);
          };
          webix.message("Segment has been deactivated.");
        }
        refreshManagerSegment();
        webix.message.hide(message);
      }
    }
  });
}

function nextSegment(modeSegment, formDataSegment, referenceOptionsSegment) {

  var indexSegment = stepsSegment.indexOf(activeStepSegment);

  if (indexSegment === stepsSegment.length - 1) {
    indexSegment = indexSegment;
  } else {
    indexSegment = indexSegment + 1;
  }

  var nextStepSegment = stepsSegment[indexSegment];

  var isValidSegment = false;

  switch (activeStepSegment) {
    case "basic":
      var form = $$("basicFormSegment");
      if (form.validate()) {
        isValidSegment = true;
      }
      break;
    default:
      isValidSegment = true;
      break;
  }
  if (isValidSegment) {
    switch (nextStepSegment) {
      case "basic":
        activeStepSegment = stepsSegment[0];
        break;
      case "conditions":
        activeStepSegment = stepsSegment[1];
        $$("basicStepSegment").hide();

        if (!isConditionsGeneratedSegment) {
          openConditionsSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment);
        } else {
          $$("wizardConditionsSegment").enable();
          showViewSegment(false, true, false, modeSegment, formDataSegment, referenceOptionsSegment);
        }

        break;
      case "confirm":
        activeStepSegment = stepsSegment[2];

        $('#wizardConditionSectionSegment').hide();

        if (!isConfirmGeneratedSegment) {
          openConfirmSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment);
        } else {
          $$("wizardConfirmSegment").enable();
          showViewSegment(false, false, true, modeSegment, formDataSegment, referenceOptionsSegment);
        }
        break;
      default:
        break;
    }
  }
}

function prevSegment(modeSegment, formDataSegment, referenceOptionsSegment) {

  var indexSegment = stepsSegment.indexOf(activeStepSegment);

  if (indexSegment === 0) {
    indexSegment = indexSegment;
  } else {
    indexSegment = indexSegment - 1;
  }

  var prevStep = stepsSegment[indexSegment];
  switch (prevStep) {
    case "basic":
      activeStepSegment = stepsSegment[0];
      showViewSegment(true, false, false, modeSegment, formDataSegment, referenceOptionsSegment);
      break;
    case "conditions":
      activeStepSegment = stepsSegment[1];
      showViewSegment(false, true, false, modeSegment, formDataSegment, referenceOptionsSegment);
      break;
    case "confirm":
      activeStepSegment = stepsSegment[2];
      showViewSegment(false, false, true, modeSegment, formDataSegment, referenceOptionsSegment);
      break;
    default:
      break;
  }
}

function highLightCurrentSegment(current) {
  $(".wizardStepBtn .webix_badge").removeClass('active');
  switch (current) {
    case "basic":
      $(".wizardStepBtn.Basic .webix_badge").addClass('active');
      break;
    case "conditions":
      $(".wizardStepBtn.Conditions .webix_badge").addClass('active');
      break;
    case "confirm":
      $(".wizardStepBtn.Confirm .webix_badge").addClass('active');
      break;
    default:
      break;
  }
}

function showViewSegment(basic, condition, confirm, modeSegment, formDataSegment, referenceOptionsSegment) {
  if (isBasicGeneratedSegment) {
    if (basic) {
      highLightCurrentSegment("basic");
      $$("basicStepSegment").show();
    } else {
      $$("basicStepSegment").hide();
    }
  }
  if (isConditionsGeneratedSegment) {
    if (condition) {
      highLightCurrentSegment("conditions");
      $('#wizardConditionSectionSegment').show();
    } else {
      $('#wizardConditionSectionSegment').hide();
    }
  }
  if (isConfirmGeneratedSegment) {

    if (confirm) {
      highLightCurrentSegment("confirm");
      openConfirmSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment);
    } else {
        $$("confirmStepSegment").hide();
    }
  }
}

function openWizardSegment(modeSegment, formDataSegment, referenceOptionsSegment, src) {
  console.log('Test');
  if($('#floatingContainer').length > 0){
    $('#floatingContainer').remove();
  }


  var f = window.location.hash;
  // Fix: Mantis # 3996
  if(referenceOptionsSegment == null){
    if(modeSegment === 'add'){
      window.history.pushState(config.addsegment.headerName, config.addsegment.headerName, config.addsegment.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);
    } else {
      window.history.pushState(config.editsegment.headerName, config.editsegment.headerName, config.editsegment.link + '?id=' + formDataSegment._id + ($('body').hasClass('hidenav') ? '&nav=1' : '') + f);
    }
  }

  defaultStepsSegment = ["basic", "conditions", "confirm"];
  activeStepSegment = defaultStepsSegment[0];
  isBasicGeneratedSegment = false;
  isConditionsGeneratedSegment = false;
  isConfirmGeneratedSegment = false;

  if(userStateData){
    isSessional = userStateData.section === 'segment';
    activeStepSegment = userStateData.data.step;
  }

  var containerId = "innerContent";

  if(referenceOptionsSegment !== undefined){
    containerId = referenceOptionsSegment.container;
  }

  $("#"+containerId).html("");


  var wizardSegment = document.createElement('div');
  wizardSegment.id = 'webixWizardParentIdSegment';
  wizardSegment.style.minHeight = '680px';
  document.getElementById(containerId).appendChild(wizardSegment);


  stepsSegment = defaultStepsSegment;

  var basicButtonSegment = {
    view: "button",
    label: "Name",
    css: "wizardStepBtn Basic",
    id: "wizardBasicBtnSegment",
    width: 170,
    badge: 1,
    click: function() {
      activeStepSegment = stepsSegment[0];
      showViewSegment(true, false, false, modeSegment, formDataSegment, referenceOptionsSegment);
    }
  };

  var conditionsButtonSegment = {
    view: "button",
    css: "wizardStepBtn Conditions",
    id: "wizardConditionsSegment",
    label: "Conditions",
    width: 170,
    badge: 2,
    disabled: true,
    click: function() {
      activeStepSegment = stepsSegment[1];
      showViewSegment(false, true, false, modeSegment, formDataSegment, referenceOptionsSegment);
    }
  };
  var isConfirmDisabledSegment = modeSegment == "add" ? true : false;
  var consfirmButtonSegment = {
    view: "button",
    css: "wizardStepBtn Confirm Save",
    id: "wizardConfirmSegment",
    label: "Save",
    width: 170,
    badge: 3,
    disabled: isConfirmDisabledSegment,
    click: function() {
      guidelines.startSubGuideline('save');
      activeStepSegment = stepsSegment[2];
      showViewSegment(false, false, true, modeSegment, formDataSegment, referenceOptionsSegment);
    }
  };

  webix.ui({
    container: "webixWizardParentIdSegment",
    height: 680,
    id: "webixWizardHeaderMenuSegment",
    rows: [{
      view: "toolbar",
      paddingY: 1,
      height: 50,
      id: "webixWizardHeaderMenuToolbarSegment",
      hidden: false,
      css: "webixWizardHeader",
      elements: [{
          gravity: 1
        }, basicButtonSegment, conditionsButtonSegment, consfirmButtonSegment, {
          gravity: 1
        }

      ]
    }, {
      template: "<div id='wizardbasicSectionSegment'></div><div id='wizardConditionSectionSegment'></div><div id='wizardConfirmSectionSegment'></div>"
    }]
  });

  openBasicSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment);
  if (modeSegment === 'edit') {

  }
  startGuidelinesInWizard({
    src: src,
    mode: modeSegment,
    add: 'addsegment',
    edit: 'editsegment',
  });
}

function openBasicSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment) {
  highLightCurrentSegment("basic");

  isBasicGeneratedSegment = true;

  var _form = [{
    view: "layout",
    height: 520,
    rows: [{
      view: "text",
      id: "tnameSegment",
      label: 'Name',
      name: "tnameSegment",
      // invalidMessage: "Name can not be empty"
    }, {
      view: "textarea",
      id: "tdescSegment",
      label: 'Description',
      name: "tdescSegment",
      height: 200,
      // invalidMessage: "Description can not be empty"
    }]
  }, {
    cols: [{}, {}, {
        view: "button",
        "type": "danger",
        value: "Back",
        width: 138,
        disabled: true,
      }, {
        view: "button",
        value: "Next",
        css: "orangeBtn",
        width: 138,
        click: function() {
          nextSegment(modeSegment, formDataSegment, referenceOptionsSegment);
        }
      }, {}, {},

    ]
  }];
  var __formRules = {
    "tnameSegment": webix.rules.isNotEmpty,
    // "tdescSegment": webix.rules.isNotEmpty,
  };

  webix.ui({
    container: "wizardbasicSectionSegment",
    id: "basicStepSegment",
    rows: [{
      cols: [{}, {
        view: "form",
        id: "basicFormSegment",
        complexData: true,
        elements: _form,
        rules: __formRules,
        width: 600,
        borderless: true,
        margin: 3,
        elementsConfig: {
          labelPosition: "top",
          labelWidth: 140,
          bottomPadding: 18
        },
      }, {}, ]
    }]
  });


  if (modeSegment === 'edit') {
    $$("basicFormSegment").setValues({
      tnameSegment: formDataSegment.name,
      tdescSegment: formDataSegment.description,
      tid: formDataSegment._id,
    });

    if(activeStepSegment !== defaultStepsSegment[0]){
      $$("basicStepSegment").hide();
    }

    //
    if(userStateData && isSessional){
      $$("basicFormSegment").setValues(userStateData.data.basicData);
      if(['basic'].indexOf(userStateData.data.step) === -1){
        showViewSegment(false, true, false, modeSegment, formDataSegment, referenceOptionsSegment);
        highLightCurrentSegment("conditions")
      }
    }
    //

    openConditionsSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment);

  } else {
    if(userStateData && isSessional){
      $$("basicFormSegment").setValues(userStateData.data.basicData);
      if(['basic'].indexOf(userStateData.data.step) === -1){
        // iFrameWin.returnCondition.logical = userStateData.data.conditionsData;
        showViewSegment(false, true, false, modeSegment, formDataSegment, referenceOptionsSegment);
        openConditionsSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment);
      }
    }
  }

  $$("wizardBasicBtnSegment").enable();
}

function openConditionsSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment) {
  modeSegment === 'add' && highLightCurrentSegment("conditions");

  isConditionsGeneratedSegment = true;

  webix.ui({
    container: "wizardConditionSectionSegment",
    id: "conditionsStepSegment",
    rows: [{
      view: "layout",
      height: 540,
      rows: [{
        view: "iframe",
        id: "conditionBuilderIframeSegment",
        src: conditionBuilderUrlSegment,
      }]
    }, {
      cols: [{}, {}, {}, {
        view: "button",
        "type": "danger",
        value: "Back",
        width: 138,
        click: function() {
          prevSegment(modeSegment, formDataSegment, referenceOptionsSegment);
        }
      }, {
        view: "button",
        value: "Next",
        css: "orangeBtn",
        width: 138,
        click: function() {
          nextSegment(modeSegment, formDataSegment, referenceOptionsSegment);
        }
      }, {}, {}, {}]
    }]

  });

  $$("conditionBuilderIframeSegment").attachEvent("onAfterLoad", function(sid) {
    if (isConditionsGeneratedSegment) {}
    tempConditionSegment = iFrameWin.returnCondition.logical;
    var conditionBuilderIframeSegment = $("iframe[src*='component/profile-properties-template']");

    if (modeSegment === 'edit') {
      tempConditionSegment = formDataSegment.condition;
    }
    if(isSessional && userStateData){
      tempConditionSegment = userStateData.data.conditionsData;
      if(['confirm'].indexOf(userStateData.data.step) !== -1){
        tempConditionSegment = userStateData.data.confirmData.conditionSegment;
      }
    }
    conditionBuilderIframeSegment.get(0).contentWindow.postMessage(tempConditionSegment, "*");
  });
  if (modeSegment === 'edit') {
    if(activeStepSegment !== defaultStepsSegment[1]){
      $("#wizardConditionSectionSegment").hide();
    }

    //
    if(userStateData && isSessional){
      if(['basic', 'conditions'].indexOf(userStateData.data.step) === -1){
        showViewSegment(false, false, true, modeSegment, formDataSegment, referenceOptionsSegment);
        highLightCurrentSegment("confirm")
      }
    }
    //

    openConfirmSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment);
  } else {
    if(userStateData && isSessional){
      if(['basic', 'conditions'].indexOf(userStateData.data.step) === -1){
        openConfirmSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment);
        showViewSegment(false, false, true, modeSegment, formDataSegment, referenceOptionsSegment);
      }
    }
  }

  $$("wizardConditionsSegment").enable();
}

function openConfirmSectionSegment(modeSegment, formDataSegment, referenceOptionsSegment) {

  $("#wizardConfirmSectionSegment").html("");
  modeSegment === 'add' && highLightCurrentSegment("confirm");

  isConfirmGeneratedSegment = true;
  allFormDataSegment.basic = $$("basicFormSegment").getValues();
  if (iFrameWin !== undefined) {
    if(iFrameWin.returnCondition.logical === 'if (){|}'){
      allFormDataSegment.condition = '';
    } else {
      allFormDataSegment.condition = iFrameWin.returnCondition.logical;
    }
  }

  if(userStateData && isSessional){
    if(['confirm'].indexOf(userStateData.data.step) !== -1){
      allFormDataSegment.condition = userStateData.data.confirmData.conditionSegment;
    }
  }

  var __form = [{
    view: "layout",


    rows: [{
      view: "text",
      id: "nameSegment",
      label: 'Name',
      name: "nameSegment",
      readonly: true,
      value: allFormDataSegment.basic.tnameSegment
      // invalidMessage: "Name can not be empty"
    }, {
      view: "textarea",
      id: "descSegment",
      label: 'Description',
      name: "descSegment",
      height: 120,
      readonly: true,
      value: allFormDataSegment.basic.tdescSegment
      // invalidMessage: "Description can not be empty"
    }, {
      view: "textarea",
      label: 'Condition',
      name: "conditionSegment",
      id: "conditionSegment",
      height: 200,
      readonly: true,
      value: allFormDataSegment.condition
    }]
  }, ];

  webix.ui({
    container: "wizardConfirmSectionSegment",
    id: "confirmStepSegment",
    rows: [{
      cols: [{}, {
        view: "form",
        id: "confirmFormSegment",
        height: 540,
        // scroll: true,
        complexData: true,
        elements: __form,
        // rules: {},
        width: 600,
        borderless: true,
        margin: 3,
        elementsConfig: {
          labelPosition: "top",
          labelWidth: 140,
          bottomPadding: 18
        },
      }, {}, ]
    }, {
      cols: [{}, {}, {
        view: "button",
        label: "Back",
        type: "danger",
        click: function() {
          prevSegment(modeSegment, formDataSegment, referenceOptionsSegment);
        },
        width: 138,
      }, {
        view: "button",
        label: "Confirm and save",
        css: "confirmSave orangeBtn",
        width: 138,
        click: function() {
          var btnCtrl = this;
          btnCtrl.disable();
          var url;
          url = monolop_api_base_url+ '/api/segments/create';
          if (modeSegment === 'edit') {
            url = monolop_api_base_url+ '/api/segments/update'
          }
          var form = $$("confirmFormSegment");
          // adding parent folder id
          var currentFolderSegment;

          if(referenceOptionsSegment !== undefined){
            currentFolderSegment = referenceOptionsSegment.currentFolder;
          } else {
            // currentFolderSegment = folderFileManagerSegment.getCurrentFolder();
            // console.log("folderFileManagerSegment.getCurrentFolder()", folderFileManagerSegment.getCurrentFolder());
            currentFolderSegment = folderFileManagerSegment ? folderFileManagerSegment.getCurrentFolder() : window.location.hash.substr(3);
          }

          if (modeSegment === 'edit') {
            form.setValues({
              source: currentFolderSegment,
              _id: allFormDataSegment.basic.tid,
            }, true);
          } else {
            form.setValues({
              source: currentFolderSegment,
            }, true);
          }
          var formValues = form.getValues();
          formValues.ajax = true;

          // check if requested from a wizard. Right now, we only have that in experiment section
          if(referenceOptionsSegment){
            if(referenceOptionsSegment.reference == 'experiment'){
              formValues.embedded = 1;
            }
          }

          webix.ajax().post(url, formValues, {
            error: function(text, data, XmlHttpRequest) {
              btnCtrl.enable();
              alert("error");
            },
            success: function(text, data, XmlHttpRequest) {
              btnCtrl.enable();
              var response = JSON.parse(text);
              if (response.success == true) {
                if(referenceOptionsSegment !== undefined){
                  referenceOptionsSegment.newSegment = response.content.segment;
                  config.currentElementID = 'webixWizardHeaderMenuExperiment';
                  retrurnBackToExperimentSegment(modeSegment, formDataSegment, referenceOptionsSegment);
                } else {
                  config.currentElementID = 'webixFilemanagerSegment';
                  openLayoutSegment();
                }
                webix.message(response.msg);
              }
            }
          });

          isSessional = false;
          userStateData = '';
        }
      }, {}, {}]
    }]
  });
  if (modeSegment === 'edit') {
    if(activeStepSegment !== defaultStepsSegment[2]){
      $$("confirmStepSegment").hide();
    }
  } else {
    guidelines.startSubGuideline('save');
    if(userStateData && isSessional){
      // $$("confirmFormSegment").setValues(userStateData.data.confirmData);
    }
  }

  // isSessional = false;
  // userStateData = '';
  $$("wizardConfirmSegment").enable();
}

function retrurnBackToExperimentSegment(modeSegment, formDataSegment, referenceOptionsSegment){
  $("#segmentsRefExperiment").remove();
  referenceOptionsSegment.isSegmentGenerated = true;
  experimentListConfig.replicateSegment = false;
  experimentListConfig.openSegment(referenceOptionsSegment.oldFormData[0], referenceOptionsSegment.oldFormData[1], referenceOptionsSegment);
  $("#innerContent").show();
}

function refreshManagerSegment(folder_id) {
  folder_id = folder_id || '';
  folderFileManagerSegment.clearAll();
  folderFileManagerSegment.load(monolop_api_base_url+ "/api/segments?ajax=true");
}
