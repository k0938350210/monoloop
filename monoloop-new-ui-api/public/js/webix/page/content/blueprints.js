var folderSegmentManager; // file manager variable
var msg; // default variable for user messages
function openLayout()
{
  var formElement = document.createElement('div');
  formElement.id = 'segmentFilemanager';
  formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
  document.getElementById("innerContent").appendChild(formElement);

  // webix.ready(function() {

    folderSegmentManager = webix.ui({
      container: "segmentFilemanager",
      view: "filemanager",
      url: monolop_api_base_url+"/api/blueprints", // loading data from the URL
      id: "folderSgments",
      filterMode:{
        showSubItems:false,
        openParents:false
      },
      mode: "table", // specify mode selected by default
      modes: ["files", "table", "custom"], // all available modes including a new mode
      // save and handle all the menu actions from here,
      // disable editing on double-click
      handlers: {
        "upload": monolop_api_base_url+"/api/blueprints/create",
        "download": monolop_api_base_url+"data/saving.php",
        "copy": monolop_api_base_url+"data/saving.php",
        "move": monolop_api_base_url+"/api/folders/move",
        "remove": monolop_api_base_url+"/api/folders/delete",
        "rename": monolop_api_base_url+"/api/folders/update",
        "create": monolop_api_base_url+"/api/folders/create"
      },
      structure: {
        // specify the view of the new mode
        "custom": {
          view: "list",
          template: function(obj, common) {
            return common.templateIcon(obj, common) + obj.value;
          },
          select: "multiselect",
          editable: false,
          editaction: false,
          editor: "text",
          editValue: "value",
          drag: true,
          navigation: false,
          tabFocus: false,
          onContext: {}
        }
      },
      on: {
        "onViewInit": function(name, config) {
          if (name == "table" || name == "files") {
            // disable multi-selection for "table" and "files" views
            config.select = true;

            if (name == "table") {
              // disable editing on double-click
              config.editaction = false;
              // an array with columns configuration
              var columns = config.columns;
              //  disabling columns date, type, size
              columns.splice(1, 3);

              // configuration of a new column description
              var descriptionColumn = {
                id: "descriptionColumn",
                header: "Description",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.description || ""; // "description" property of files
                }
              };
              // configuration of a new column date
              var dateColumn = {
                id: "dateColumn",
                header: "Date",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.date || ""; // "description" property of files
                }
              };
              // configuration of a new column actions
              var actionsColumn = {
                id: "actionsColumn",
                header: "Actions",
                fillspace: 1,
                template: function(obj, common) {

                  if(typeof(obj.condition) === undefined || obj.condition === null){
                    obj.condition = '';
                  }

                  var params = {
                    source: 'segment',
                    id: obj.id
                  };
                  return '<a webix_l_id="condition_builder"  title="Condition" onclick=\'openConditionalBuilder(' + JSON.stringify(params) + ');\' class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" title="delete" onclick="deleteFile(\'' + obj.id + '\');" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
                }
              };
              // configuration of a new column status
              var statusColumn = {
                id: "statusColumn",
                header: "Status",
                fillspace: 1,
                template: function(obj, common) {
                  var obj_ = {
                    action: "update_hidden",
                    hidden: "1"
                  };
                  var params = {
                    source: obj.id,
                    action: "update_hidden",
                    node: this
                  };
                  if (obj.hidden == 0) {
                    params.hidden = 1;
                    return '<a id="updateHidden_' + obj.id + '"  onclick=\'updateHidden( ' + JSON.stringify(params) + ');\' class="webix_list_item" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actions" property of files ;
                  }
                  params.hidden = 0;
                  return '<a id="updateHidden_' + obj.id + '"  onclick=\'updateHidden(' + JSON.stringify(params) + ');\' class="webix_list_item" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
                }
              };
              // insert columns
              webix.toArray(columns).insertAt(descriptionColumn, 1);
              webix.toArray(columns).insertAt(dateColumn, 2);
              webix.toArray(columns).insertAt(actionsColumn, 3);
              webix.toArray(columns).insertAt(statusColumn, 4);
            }

          }
        }
      }
    });

    /*******************************Menu Customization******************************/

    // updating options from menu
    var actions = $$("folderSgments").getMenu();
    actions.clearAll();
    var newData = [

      {
        id: "create",
        method: "createFolder",
        icon: "folder-o",
        value: webix.i18n.filemanager.create // "Create Folder"
      }, {
        id: "deleteFile",
        method: "deleteFile",
        icon: "times",
        value: webix.i18n.filemanager.remove // "Delete"
      }, {
        id: "edit",
        method: "editFile",
        icon: "edit",
        value: webix.i18n.filemanager.rename // "Rename"
      }
    ];
    actions.parse(newData);
    // add new option for the menu to add new segment
    actions.add({
      id: "createSegment",
      icon: "file",
      value: "Create Blueprint"
    });

    /*******************************Segment Add**********************************/

    actions.attachEvent("onItemClick", function(id) {
      // check if the action is CreateSegment
      if (id == "createSegment") {
        // setting url
        var url = monolop_api_base_url+'/api/blueprints/create';
        var pathIds = folderSegmentManager.getPath();
        var segmentForm = [{
          view: "text",
          id: "name",
          label: 'Name',
          name: "name",
          // invalidMessage: "Name can not be empty"
        }, {
          view: "textarea",
          id: "desc",
          label: 'Description',
          name: "desc",
          // invalidMessage: "Description can not be empty"
        }, {
          view: "button",
          id: "sub",
          name: "submit",
          value: "Save and Next",
          click: function() {
            var form = this.getParentView();
            // adding parent folder id
            form.setValues({
              source: folderSegmentManager.getCurrentFolder()
            }, true)
            if (form.validate()) {
              webix.ajax().post(url, form.getValues(), {
                error: function(text, data, XmlHttpRequest) {
                  alert("error");
                },
                success: function(text, data, XmlHttpRequest) {
                  var response = JSON.parse(text);
                  if (response.success == true) {
                    $$("add_new_segment").hide();

                    var params = {
                      source: response.source,
                      id: response.content.id,
                      condition: response.content.condition,
                    };
                    // folderSegmentManager.refresh();

                    msg = 'Segment is created and ';

                    refreshManager();
                    openConditionalBuilder(params);
                    // webix.alert(response.msg);
                  }
                }
              });

            }
          }
        }, {
          view: "button",
          id: "cancel",
          name: "submit",
          value: "cancel",
          click: function() {
            $$("add_new_segment").hide();
          }
        }];
        var segmentFormRules = {
          "name": webix.rules.isNotEmpty,
          "desc": webix.rules.isNotEmpty,
        };

        webix.ui({
          view: "window",
          id: "add_new_segment",
          modal: true,
          position: "center",
          height: 400,
          width: 400,
          head: {
            view: "toolbar",
            margin: -4,
            cols: [{
              view: "label",
              label: "New Segment"
            }, {
              view: "icon",
              icon: "times-circle",
              css: "alter",
              click: "$$('add_new_segment').close();"
            }]
          },
          body: {
            view: "form",
            complexData: true,
            elements: segmentForm,
            rules: segmentFormRules,
          }
        }).show();
      }
    });

    /******************************Segment Add End*******************************/


    /*****************************Menu Customization End*************************/


    /*******************************Custom Events********************************/

    /********************************Segment Edit********************************/

    // before editing file
    $$("folderSgments").attachEvent("onBeforeEditFile", function(id) {

      var srcTypeId = id.split('__');

      if (srcTypeId[0] == 'folder') {
        return true;
      }


      var url = monolop_api_base_url+'/api/blueprints/update';

      var segmentForm = [{
        view: "text",
        id: "name",
        label: 'Name',
        name: "name",
        // invalidMessage: "Name can not be empty"
      }, {
        view: "textarea",
        id: "desc",
        label: 'Description',
        name: "desc",
        // invalidMessage: "Description can not be empty"
      }, {
        view: "button",
        id: "sub",
        name: "submit",
        value: "Sumbit",
        click: function() {
          var form = this.getParentView();
          // adding parent folder id

          if (form.validate()) {
            webix.ajax().post(url, form.getValues(), {
              error: function(text, data, XmlHttpRequest) {
                alert("error");
              },
              success: function(text, data, XmlHttpRequest) {
                var response = JSON.parse(text);
                if (response.success == true) {
                  $$("edit_segment").hide();
                  webix.alert(response.msg);
                }
              }
            });

          }
        }
      }];
      var segmentFormRules = {
        "name": webix.rules.isNotEmpty,
        "desc": webix.rules.isNotEmpty,
      };
      // form
      webix.ui({
        view: "window",
        id: "edit_segment",
        modal: true,
        position: "center",
        height: 400,
        width: 400,
        head: {
          view: "toolbar",
          margin: -4,
          cols: [{
            view: "label",
            label: "Update Segment"
          }, {
            view: "icon",
            icon: "times-circle",
            css: "alter",
            click: "$$('edit_segment').close();"
          }]
        },
        body: {
          view: "form",
          id: "edit_segment_form",
          complexData: true,
          elements: segmentForm,
          rules: segmentFormRules,
        }
      }).show();

      // retreiving form data and setting into the form
      var formData;
      webix.ajax().get(monolop_api_base_url+"/api/blueprints/show", {
        source: id
      }, function(text, xml, xhr) {
        //response
        formData = JSON.parse(text);

        $$("edit_segment_form").setValues({
          name: formData.name,
          desc: formData.description,
          source: id
        }, true);
      });
      return false;
    });

    /*****************************Segment edit End*******************************/

    // reload grid after folder creation
    $$("folderSgments").attachEvent("onAfterCreateFolder", function(id) {
      // refreshManager();
      setTimeout(function(){ openLayout(); }, 500);
      return false;
    });


    // it will be triggered before deletion of file
    $$("folderSgments").attachEvent("onBeforeDeleteFile", function(ids) {
      deleteFile(ids);
      return false;
    });

    // it will be triggered before dragging the folder/segment
    $$("folderSgments").attachEvent("onBeforeDrag", function(context, ev) {
      msg = webix.message("copying...");
      return true;
    });

    // it will be triggered after dropping the folder to the destination
    $$("folderSgments").attachEvent("onAfterDrop", function(context, ev) {
      webix.message.hide(msg);
      return true;
    });

    $$("folderSgments").$$("files").attachEvent("onBeforeRender", filterFiles);
    $$("folderSgments").$$("table").attachEvent("onBeforeRender", filterFiles);

    /*******************************Custom Events End****************************/
  // });
}
// filter segments only
function filterFiles(data){
   data.blockEvent();
   data.filter(function(item){
    return item.type != "folder"
   });
   data.unblockEvent();
}

/**********************************Functions***********************************/
function updateFile(id) {

}

function openConditionalBuilder(params) {
  return;
  var paramsString = getAsUriParameters(params);

  var url  = '/component/profile-properties-template?' + paramsString;


  webix.ui({
    view: "window",
    id: "edit_condition_bulder",
    modal: true,
    scroll: true,
    position: "center",
    height: 700,
    width: 1200,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "Condition Builder"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('edit_condition_bulder').close();"
      }]
    },
    body: {
      view:"iframe", id:"frame-body", src: url
    }
  }).show();

}
function closeConditionBuilder(){
  $$('edit_condition_bulder').close();
  webix.alert("Segment conditions are updated.");
}
function deleteFile(id) {

  webix.confirm({
    text: "Do you want to delete?",
    ok: "Yes",
    cancel: "No",
    callback: function(result) {
      if (result) {

        webix.message("deleting...");
        folderSegmentManager.deleteFile(id);
        return true;
      }
    }
  });


}

function updateHidden(params) {
  // var params = {source: id, action: action, hidden: hidden};
  if (params.hidden == 1) {
    var message = webix.message("deactivating...");
  } else if (params.hidden == 0) {
    var message = webix.message("activating...");
  }

  webix.ajax().post(monolop_api_base_url+"/api/folders/update", params, {
    error: function(text, data, XmlHttpRequest) {
      alert("error");
    },
    success: function(text, data, XmlHttpRequest) {
      var response = JSON.parse(text);
      if (response.success == true && params.action == "update_hidden") {
        var element = document.getElementById("updateHidden_" + params.source)
        if (params.hidden == 0) {
          element.innerHTML = "active";
          element.style.color = "green";
          element.onclick = function() {
            params.hidden = 1;
            updateHidden(params);
          };
        } else if (params.hidden == 1) {
          element.innerHTML = "inactive";
          element.style.color = "red";
          element.onclick = function() {
            params.hidden = 0;
            updateHidden(params);
          };
        }
        webix.message.hide(message);
      }
    }
  });
}

function refreshManager() {
  folderSegmentManager.clearAll();
  folderSegmentManager.load(monolop_api_base_url+"/api/blueprints");
}
function getAsUriParameters(data) {
   var url = '';
   for (var prop in data) {
      url += encodeURIComponent(prop) + '=' +
          encodeURIComponent(data[prop]) + '&';
   }
   return url.substring(0, url.length - 1)
}
