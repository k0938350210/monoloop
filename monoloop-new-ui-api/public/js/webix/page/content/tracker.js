var folderFileManagerTracker; // file manager variable
var msg; // default variable for user messages

var stateTracker;
var isSessional = false;

var defaultStepsTracker;
var activeStepTracker;
var isBasicGeneratedTracker, isPageGeneratedTracker , isConfigGeneratedTracker , isConfirmGeneratedTracker;
var conditionBuilderUrlTracker = '/component/profile-properties-template';
var iFrameWin;
var page = 'tracker';
var tempConditionTracker = {};

var allFormDataTracker = {};
// 14OCT
var localStorageDataTracker = {};
localStorageDataTracker.active = false;
localStorageDataTracker.basic = {};
localStorageDataTracker.basic.isSaved = false;
localStorageDataTracker.basic.Data = {};
localStorageDataTracker.conditions = {};
localStorageDataTracker.conditions.isSaved = false;
localStorageDataTracker.conditions.Data = {};
localStorageDataTracker.confirm = {};
localStorageDataTracker.confirm.isSaved = false;
localStorageDataTracker.confirm.Data = {};

var tracker_obj = {
  id: null,
  name: '',
  type: '',
  folder_id : null ,
  url_config: {},
  tracker_events: [],
  tracker_fields: [],
  mode: function(){
    if(this.id){
      return 'edit';
    }
    return 'add';
  },
  reset: function(){
    this.id = null;
    this.name = '';
    this.type = 'category';
    this.tracker_events = [];
    this.tracker_fields = [];
    this.url_config = {
      url: default_domain,
      url_option: 'exactMatch',
      remark: '',
      inc_www: 0,
      inc_http_https: 0,
      reg_ex: ''
    };
    var currentFolderTracker = folderFileManagerTracker ? folderFileManagerTracker.getCurrentFolder() : window.location.hash.substr(3);
    this.folder_id = currentFolderTracker ;
  },
  getFormData: function(){
    var data = {
      'tracker[name]': this.name ,
      'tracker[type]': this.type ,
      'tracker[folder_id]': this.folder_id,
      'tracker[id]': this.id ,
      'tracker[url_config][url]': this.url_config.url,
      'tracker[url_config][remark]': this.url_config.remark,
      'tracker[url_config][reg_ex]': this.url_config.reg_ex,
      'tracker[url_config][inc_www]': this.url_config.inc_www,
      'tracker[url_config][inc_http_https]': this.url_config.inc_http_https,
      'tracker[url_config][url_option]': this.url_config.url_option
    };

    for(var i = 0 ; i < this.tracker_events.length ; i++){
      var event = this.tracker_events[i];
      data['tracker[tracker_events]['+i+'][_id]'] = event._id ;
      data['tracker[tracker_events]['+i+'][name]'] = event.name ;
      data['tracker[tracker_events]['+i+'][type]'] = event.type ;
      data['tracker[tracker_events]['+i+'][selector]'] = event.selector ;
      data['tracker[tracker_events]['+i+'][custom]'] = event.custom ;
    }

    for(var i = 0 ; i < this.tracker_fields.length ; i++){
      var field = this.tracker_fields[i];
      data['tracker[tracker_fields]['+i+'][_id]'] = field._id ;
      data['tracker[tracker_fields]['+i+'][name]'] = field.name ;
      data['tracker[tracker_fields]['+i+'][field_type]'] = field.field_type ;
      data['tracker[tracker_fields]['+i+'][operator]'] = field.operator ;
      data['tracker[tracker_fields]['+i+'][type]'] = field.type ;
      data['tracker[tracker_fields]['+i+'][selector]'] = field.selector ;
      data['tracker[tracker_fields]['+i+'][meta]'] = field.meta ;
      data['tracker[tracker_fields]['+i+'][cookie]'] = field.cookie ;
      data['tracker[tracker_fields]['+i+'][js]'] = field.js ;
      data['tracker[tracker_fields]['+i+'][custom]'] = field.custom ;
      data['tracker[tracker_fields]['+i+'][filter]'] = field.filter ;
      data['tracker[tracker_fields]['+i+'][filter_custom]'] = field.filter_custom ;
    }

    return data;
  },
  load: function(data){
    this.id = data._id ;
    this.type = data.type ;
    this.name = data.name ;
    this.folder_id = data.folder_id ;
    this.tracker_events = data.tracker_events ;
    if(this.tracker_events == undefined){
      this.tracker_events = [];
    }
    this.tracker_fields = data.tracker_fields ;
    if(this.tracker_fields == undefined){
      this.tracker_fields = [];
    }
    this.url_config = {
      url: data.url_config.url,
      url_option: data.url_config.url_option,
      remark: data.url_config.remark,
      reg_ex: data.url_config.reg_ex,
      inc_www: data.url_config.inc_www,
      inc_http_https: data.url_config.inc_http_https
    }
  },
  /*-------------- EVENTS-----------------------*/
  getEventFieldList: function(){
    var data = [] ;
    data.push({id: 'event-new',value: '+ Add new'});
    for(var i = 0 ; i < this.tracker_events.length ; i++){
      var event = this.tracker_events[i] ;
      var value = '' ;
      if(event.type === 'page-render'){
        value = 'Page render';
      }else if(event.type === 'click'){
        value = 'Click';
      }else if(event.type === 'custom'){
        value = 'Custom';
      }
      if(event.name !== ''){
        value = value + ' <small>( ' + event.name + ' )</small>' ;
      }

      data.push({id:event._id,value: value});
    }
    return data;
  },
  getEventById: function(id){
    for(var i = 0 ; i < this.tracker_events.length ; i++){
      var event = this.tracker_events[i] ;
      if(event._id === id){
        return event ;
      }
    }
    return false;
  },
  updateEvemt: function(id,name,type,selector,custom){
    var found = false ;
    for(var i = 0 ; i < this.tracker_events.length ; i++){
      var event = this.tracker_events[i] ;
      if(event._id === id){
        found = true ;
        this.tracker_events[i].name = name ;
        this.tracker_events[i].type = type ;
        this.tracker_events[i].selector = selector ;
        this.tracker_events[i].custom = custom ;
        webix.message('Success update event ' + name );
      }
    }
    if(found == false){
      this.tracker_events.push({_id:'event-'+this.guid(),name:name,type:type,selector:selector,custom:custom});
      webix.message('Success create new event ' + name );
    }
  },
  removeEvent: function(id){
    for(var i =  this.tracker_events.length - 1 ; i >= 0 ; i--){
      var event = this.tracker_events[i] ;
      if(event._id === id){
        webix.message('Success remove event ' + event.name );
        this.tracker_events.splice(i, 1);

      }
    }
  },
  /*----------- FIELDS --------------------*/
  getFieldFieldList: function(){
    if(this.tracker_fields.length == 0){
      if(this.type === 'category'){
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Category',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
      }else if(this.type === 'product'){
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Name',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });

        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Value',
          field_type: 'number',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
      }else if(this.type === 'search'){
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Search value',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
      }else if(this.type === 'form'){
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Select form',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
      }else if(this.type === 'basket'){
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Table',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Product row',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Product name',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Product sku',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Price',
          field_type: 'number',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Currency',
          field_type: 'string',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
        this.tracker_fields.push({
          _id: 'field-' + this.guid(),
          name: 'Quantity',
          field_type: 'number',
          operator: 'push',
          type: 'x-path',
          selector: '',
          meta: '',
          cookie: '',
          js: '',
          custom: '',
          filter: 'none',
          filter_custom: ''
        });
      }
    }
    var data = [] ;
    if(this.type === 'asset' || this.type === 'custom'){
      data.push({id: 'field-new',value: '+ Add new'});
    }
    for(var i = 0 ; i < this.tracker_fields.length ; i++){
      var field = this.tracker_fields[i];
      data.push({id:field._id,value: field.name});
    }
    return data ;
  },
  getFieldById: function(id){
    for(var i = 0 ; i < this.tracker_fields.length ; i++){
      var field = this.tracker_fields[i] ;
      if(field._id === id){
        return field ;
      }
    }
    return false;
  },
  getFieldIndexById: function(id){
    for(var i = 0 ; i < this.tracker_fields.length ; i++){
      var field = this.tracker_fields[i] ;
      if(field._id === id){
        return i ;
      }
    }
    return 0;
  },
  updateField: function(id,name,field_type,operator,type,selector,meta,cookie,js,custom,filter,filter_custom){
    var found = false ;
    for(var i = 0 ; i < this.tracker_fields.length ; i++){
      var field = this.tracker_fields[i] ;
      if(field._id === id){
        found = true ;
        this.tracker_fields[i].name = name ;
        this.tracker_fields[i].field_type = field_type ;
        this.tracker_fields[i].operator = operator ;
        this.tracker_fields[i].type = type ;
        this.tracker_fields[i].selector = selector ;
        this.tracker_fields[i].meta = meta ;
        this.tracker_fields[i].cookie = cookie ;
        this.tracker_fields[i].js = js ;
        this.tracker_fields[i].custom = custom ;
        this.tracker_fields[i].filter = filter ;
        this.tracker_fields[i].filter_custom = filter_custom ;
        webix.message('Success update field ' + name );
      }
    }
    if(found == false){
      this.tracker_fields.push({_id:'field-'+this.guid(),name:name,field_type:field_type,operator:operator,type:type,selector:selector,meta: meta,cookie: cookie,custom: custom,js: js,filter: filter,filter_custom:filter_custom});
      webix.message('Success create new event ' + name );
    }
  },
  removeField: function(id){
    for(var i =  this.tracker_fields.length - 1 ; i >= 0 ; i--){
      var field = this.tracker_fields[i] ;
      if(field._id === id){
        webix.message('Success remove field ' + field.name );
        this.tracker_fields.splice(i, 1);

      }
    }
  },
  /*-------------- helper -------------*/
  guid: function() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }
};


function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
};

var extendedFab = {
  actionButtonTypes: {
    type: "create",
    setType: function(t){
      t = t || "create";
      extendedFab.actionButtonTypes.type = t;
    },
    fabOptions: false,
    create: {
      options: [
        // {
        //  label: "This is to test",
        //  className: "segmentBuilder inline",
        //   callback: function(){
        //     webix.alert("This is to test......");
        //   }
        // }
      ]
    },
  },
  fabOptions: false,
  primary: {
    primaryBtnLabelCallback: function(actionButton, floatingTextBG){
      actionButton.className += " createtracker inline";
      floatingTextBG.textContent = "Create tracker";
      actionButton.onclick = function(){
        config.currentElementID = 'webixWizardHeaderMenuTracker';
        openWizardTracker('add');
      };
    },
  }
};

function openLayoutTracker() {
  var params = getQueryParams(document.location + '');
  var f = window.location.hash;
  window.history.pushState(config.tracker.headerName, config.tracker.headerName, config.tracker.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);

  $("#innerContent").html("");

  var formElement = document.createElement('div');
  formElement.id = 'fileManagerParentIdTracker';
  formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
  document.getElementById("innerContent").appendChild(formElement);

  // webix.ready(function() {

  folderFileManagerTracker = webix.ui({
    container: "fileManagerParentIdTracker",
    view: "filemanager",
    url: monolop_api_base_url+ "/api/trackers?ajax=true" + (params.url ? ('&url=' + params.url) : ''), // loading data from the URL
    id: "webixFilemanagerTracker",
    // height: 500,
    filterMode: {
      showSubItems: true,
      openParents: true
    },
    mode: "table", // specify modeTracker selected by default
    modes: ["files", "table"], // all available modes including a new modeTracker
    // save and handle all the menu actionsTracker from here,
    // disable editing on double-click

    handlers: {
      "upload": monolop_api_base_url+ "/api/trackers/create",
      "move": monolop_api_base_url+ "/api/folders/move",
      "remove": monolop_api_base_url+ "/api/folders/delete",
      "rename": monolop_api_base_url+ "/api/folders/update",
      "create": monolop_api_base_url+ "/api/folders/create",
      "files": monolop_api_base_url+ "/api/trackers/folder?ajax=true" + (params.url ? ('&url=' + params.url) : '')
    },
    structure: {

    },
    on: {
      "onViewInit": function(name, config) {
        if (name == "table" || name == "files") {
          // disable multi-selection for "table" and "files" views
          config.select = true;

          if (name == "table") {
            // disable editing on double-click
            config.editaction = false;
            // an array with columns configuration
            var columns = config.columns;
            //  disabling columns date, type, size
            columns.splice(1, 3);

            // configuration of a new column description
            var descriptionColumnTracker = {
              id: "descriptionColumnTracker",
              header: "Description",
              fillspace: 3,
              template: function(obj, common) {
                return obj.description || ""; // "description" property of files
              }
            };
            // configuration of a new column date
            var dateColumnTracker = {
              id: "dateColumnTracker",
              header: "Date",
              fillspace: 2,
              sort: function(a, b){
                var _a = new Date(a.date), _b = new Date(b.date);
                return (_a > _b ? 1 : (_b > _a ? -1: 0));
              },
              template: function(obj, common) {
                return obj.date || ""; // "description" property of files
              }
            };

            // configuration of a new column actions
            var reportColumnTracker = {
              id: "reportColumnTracker",
              header: "Report",
              // fillspace: 1,
              width: 70,
              template: function(obj, common) {
                var url  = '/reports/tracker/detail/'+obj.Trackerid + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURI(params.url)) : '');
                return '<a  title="Audience report" onclick="window.location = \'' + url + '\';" class="webix_list_item viewReport" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-bar-chart"></span></a>';
              }
            };

            // configuration of a new column actionsTracker
            var actionsColumnTracker = {
              id: "actionsColumnTracker",
              header: "Actions",
              css: "actionsCol",
              fillspace: 1,
              template: function(obj, common) {
                if(obj.system != 1 ){
                  return '<a title="Edit audience" onclick="updateTracker(\'' + obj.id + '\');" class="updateTracker webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" title="delete" onclick="deleteTracker(\'' + obj.id + '\');" class="deleteTracker webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
                }
              }
            };
            // configuration of a new column status
            var statusColumnTracker = {
              id: "statusColumnTracker",
              header: "Status",
              css: "statusCol",
              fillspace: 1,
              sort: function(a, b){
                var _a = Number(a.hidden), _b = Number(b.hidden);
                return (_a > _b ? 1 : (_b > _a ? -1: 0));
              },
              template: function(obj, common) {
                var obj_ = {
                  action: "update_hidden",
                  hidden: "1"
                };
                var paramsTracker = {
                  _id: obj.id,
                  action: "update_hidden",
                  node: this
                };
                if (obj.hidden == 0) {
                  paramsTracker.hidden = 1;
                  return '<a title="Deactive tracker" id="updateHiddenTracker' + obj.id + '"  onclick=\'updateHiddenTracker( ' + JSON.stringify(paramsTracker) + ');\' class="webix_list_item updateTrackerStatus status status0" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actionsTracker" property of files ;
                }
                paramsTracker.hidden = 0;
                return '<a title="Activate tracker" id="updateHiddenTracker' + obj.id + '"  onclick=\'updateHiddenTracker(' + JSON.stringify(paramsTracker) + ');\' class="webix_list_item updateTrackerStatus status status1" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
              }
            };
            var actionsColumnTracker = {
              id: "actionsColumnTracker",
              header: "Actions",
              fillspace: 1,
              template: function(obj, common) {
                var edit = '<a  title="Edit" onclick="updateTracker(\'' + obj.id + '\');" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' ;
                var del = '<a webix_l_id="remove" title="delete" onclick="deleteTracker(\'' + obj.id + '\');" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
                if(obj.system != 1){
                  return edit + '|' + del ;
                }
                return edit ;
              }
            };
            var typeColumnTracker = {
              id: "typeColumnTracker",
              header: "Type",
              fillspace: 2,
              template: function(obj, common) {
                return obj.tracker_type || ""; // "description" property of files
              }
            };
            // insert columns
            // webix.toArray(columns).insertAt(descriptionColumnTracker, 1);
            // webix.toArray(columns).insertAt(dateColumnTracker, 2);
            // webix.toArray(columns).insertAt(reportColumnTracker, 3);
            // webix.toArray(columns).insertAt(actionsColumnTracker, 4);
            webix.toArray(columns).insertAt(typeColumnTracker, 1);
            webix.toArray(columns).insertAt(actionsColumnTracker, 2);
            webix.toArray(columns).insertAt(statusColumnTracker, 3);

          }

        }
      }
    }
  });

  /*******************************Menu Customization******************************/
  // only FILES mode should be available
  $$('webixFilemanagerTracker').$$('modes').hide();

  // updating options from menu
  var actionsTracker = $$("webixFilemanagerTracker").getMenu();
  actionsTracker.clearAll();
  var newDataTracker = [

    {
      id: "create",
      method: "createFolder",
      icon: "folder-o",
      value: webix.i18n.filemanager.create // "Create Folder"
    }, {
      id: "deleteFile",
      method: "deleteFile",
      icon: "times",
      value: webix.i18n.filemanager.remove // "Delete"
    }, {
      id: "edit",
      method: "editFile",
      icon: "edit",
      value: webix.i18n.filemanager.rename // "Rename"
    }
  ];
  actionsTracker.parse(newDataTracker);
  // add new option for the menu to add new Tracker
  // actionsTracker.add({
  //   id: "createTracker",
  //   icon: "file",
  //   value: "Create Audience"
  // });

  /*******************************Tracker Add**********************************/

  actionsTracker.attachEvent("onItemClick", function(id) {
    this.hide();
    // check if the action is CreateTracker
    if (id == "createTracker") {
      openWizardTracker('add');
    }
  });

  actionsTracker.getItem("create").batch = "item, root";
  actionsTracker.getItem("deleteFile").batch = "item, root";
  actionsTracker.getItem("edit").batch = "item, root";
  // actionsTracker.getItem("createTracker").batch = "item, root";

  /******************************Tracker Add End*******************************/


  /*****************************Menu Customization End*************************/


  /*******************************Custom Events********************************/

  /********************************Tracker Edit********************************/

  $$("webixFilemanagerTracker").$$("table").attachEvent("onBeforeSelect", function(id, p){
    var rowName = id.row,
        columnName = id.column;
    var item = this.getItem(rowName);
    if (item.type !== "folder"){
      if(['reportColumnTracker', 'actionsColumnTracker', 'statusColumnTracker'].indexOf(columnName) === -1){
      //  updateTracker(item.id);
      }
    } else {

    }
    //note that 'id' for datatable is an object with several attributes
  });
  $$("webixFilemanagerTracker").attachEvent("onBeforeDeleteFile",function(id){
      // Fix: mantis # 3945
      if (id.indexOf('tracker__') === -1) {
        var item = this.getItem(id);
        if(item.$parent === 0){
          webix.message({type: "error", text: "Root folder can not be deleted."})
          return false;
        }else{
          checkFolderStatus(id, folderFileManagerTracker);
          return false;
        }
      }
  });
  // before editing file
  $$("webixFilemanagerTracker").attachEvent("onBeforeEditFile", function(id) {
    var srcTypeId = id.split('__');


    if (srcTypeId[0] == 'folder') {
      return true;
    }

    updateTracker(id);

    return false;
  });

  /*****************************Tracker edit End*******************************/

  // reload grid after folder creation
  $$("webixFilemanagerTracker").attachEvent("onAfterCreateFolder", function(id) {
    // refreshManagerTracker();
    setTimeout(function(){ openLayoutTracker(); }, 500);
    return false;
  });


  // it will be triggered before deletion of file
  $$("webixFilemanagerTracker").attachEvent("onBeforeDeleteFile", function(ids) {
    // deleteTracker(ids);
    if (ids.indexOf('tracker__') === -1) {
      return true
    }

    deleteTracker(ids);
    return false;
  });

  // it will be triggered before dragging the folder/Tracker
  $$("webixFilemanagerTracker").attachEvent("onBeforeDrag", function(context, ev) {
    return true;
  });

  $$("webixFilemanagerTracker").attachEvent("onBeforeDrop", function(context, ev) {
    if (context.start.indexOf('tracker__') === -1) {
      return true
    }
    webix.ajax().post(monolop_api_base_url+ "/api/trackers/change-folder", {
      _id: context.start,
      target: context.target,
      ajax: true,
    }, {
      error: function(text, data, XmlHttpRequest) {
        alert("error");
      },
      success: function(text, data, XmlHttpRequest) {
        var response = JSON.parse(text);
        if (response.success == true) {}
      }
    });
    return true;
  });
  // it will be triggered after dropping the folder to the destination
  $$("webixFilemanagerTracker").attachEvent("onAfterDrop", function(context, ev) {
    webix.message("Tracker has been moved to new folder.");
    return false;
  });

  $$("webixFilemanagerTracker").$$("files").attachEvent("onBeforeRender", filterFilesTracker);
  $$("webixFilemanagerTracker").$$("table").attachEvent("onBeforeRender", filterFilesTracker);

  var _exe = false;
  $$("webixFilemanagerTracker").attachEvent("onAfterLoad", function(context, ev) {
    if(_exe === false){
      _exe = true;
      initIntialTTandGuides();
    }
  });

  // fix: mantis # 3946
  $$("webixFilemanagerTracker").attachEvent("onSuccessResponse", function(req, res) {
  	if(res.success && (req.source === "newFolder" || req.source.indexOf("folder") != -1)){
  		switch(req.action){
  			case "rename":
  				webix.message("Folder renamed");
  				break;
  			case "create":
  				webix.message("Folder Created");
  				break;
  			case "remove":
  				webix.message("Folder deleted");
  				break;
  		}
  	}
  });
  /*******************************Custom Events End****************************/
  // });

  // fab.initActionButtons({
  //   type: "TrackerList",
  //   actionButtonTypes: extendedFab.actionButtonTypes,
  //   primary: extendedFab.primary,
  // });
  fab.initActionButtons({
    type: "trackerList",
    actionButtonTypes: extendedFab.actionButtonTypes,
    primary: extendedFab.primary,
  });

  $(window).on('resize', function(){
    $$('webixFilemanagerTracker').define('width', $('body').width() - $('.sidebar').width() - 1);
    $$('webixFilemanagerTracker').resize();
  });
}
// filter Trackers only
function filterFilesTracker(data) {
  data.blockEvent();
  data.filter(function(item) {
    return item.type != "folder"
  });
  data.unblockEvent();
}

/**********************************Functions***********************************/
function updateTracker(id, src) {
  config.currentElementID = 'webixWizardHeaderMenuTracker';
  var url = monolop_api_base_url+ '/api/trackers/show';

  // retreiving form data and setting into the form
  var formDataTracker;
  webix.ajax().get(url, {
    _id: id,
    ajax: true
  }, function(text, xml, xhr) {
    //response
    formDataTracker = JSON.parse(text);
    tracker_obj.load(formDataTracker);
    openWizardTracker('edit');
  });
}

function deleteTracker(id) {
  webix.confirm({
    text: "Do you want to delete?",
    ok: "Yes",
    cancel: "No",
    callback: function(result) {
      if (result) {
        webix.ajax().post(monolop_api_base_url+ "/api/trackers/delete", {
          _id: id,
          ajax: true,
        }, {
          error: function(text, data, XmlHttpRequest) {
            alert("error");
          },
          success: function(text, data, XmlHttpRequest) {
            var response = JSON.parse(text);
            if (response.success == true) {
              folderFileManagerTracker.deleteFile(id);
            }
          }
        });
      }
    }
  });

}

function updateHiddenTracker(paramsTracker) {
  // var paramsTracker = {source: id, action: action, hidden: hidden};
  if (paramsTracker.hidden == 1) {
    var message = webix.message("deactivating...");
  } else if (paramsTracker.hidden == 0) {
    var message = webix.message("activating...");
  }
  paramsTracker.ajax = true;
  webix.ajax().post(monolop_api_base_url+ "/api/trackers/update-status", paramsTracker, {
    error: function(text, data, XmlHttpRequest) {
      alert("error");
    },
    success: function(text, data, XmlHttpRequest) {
      var response = JSON.parse(text);
      if (response.success == true && paramsTracker.action == "update_hidden") {
        var element = document.getElementById("updateHiddenTracker" + paramsTracker._id)
        if (paramsTracker.hidden == 0) {
          element.innerHTML = "active";
          element.style.color = "green";
          element.onclick = function() {
            paramsTracker.hidden = 1;
            updateHiddenTracker(paramsTracker);
          };
          webix.message("Tracker has been activated.");
        } else if (paramsTracker.hidden == 1) {
          element.innerHTML = "inactive";
          element.style.color = "red";
          element.onclick = function() {
            paramsTracker.hidden = 0;
            updateHiddenTracker(paramsTracker);
          };
          webix.message("Tracker has been deactivated.");
        }
        refreshManagerTracker();
        webix.message.hide(message);
      }
    }
  });
}

/*------------- TRACKER FORM --------------*/

function nextTrackerStep1(){
  var indexTracker = 1 ;
  var nextStepTracker = stepsTracker[indexTracker];
  var isValidTracker = false;
  var form = $$("basicFormTracker");
  if (form.validate()) {
    isValidTracker = true;
  }

  if (isValidTracker) {
    activeStepTracker = stepsTracker[1];
    $$("basicStepTracker").hide();
    $$("wizardPageTracker").enable();
    showViewTracker(false, true, false , false);
  }
}

function nextTrackerStep2(){
  activeStepTracker = stepsTracker[2];
  $$("pageStepTracker").hide();
  $$("wizardConfigTracker").enable();
  showViewTracker(false, false, true , false);
}

function nextTrackerStep3(){
  activeStepTracker = stepsTracker[3];
  $$('configStepTracker').hide();
  $$("wizardConfirmTracker").enable();
  showViewTracker(false, false, false , true);
}



function nextTracker() {

  var indexTracker = stepsTracker.indexOf(activeStepTracker);

  if (indexTracker === stepsTracker.length - 1) {
    indexTracker = indexTracker;
  } else {
    indexTracker = indexTracker + 1;
  }

  var nextStepTracker = stepsTracker[indexTracker];

  var isValidTracker = false;

  switch (activeStepTracker) {
    case "basic":
      var form = $$("basicFormTracker");
      if (form.validate()) {
        isValidTracker = true;
      }
      break;
    default:
      isValidTracker = true;
      break;
  }
  if (isValidTracker) {
    switch (nextStepTracker) {
      case "basic":
        activeStepTracker = stepsTracker[0];
        break;
      case "page":
        activeStepTracker = stepsTracker[1];
        $$("basicStepTracker").hide();
        $$("wizardPageTracker").enable();
        showViewTracker(false, true, false , false);
        break;
      break;
      case "config":
        activeStepTracker = stepsTracker[2];
        $$("pageStepTracker").hide();
        $$("wizardConfigTracker").enable();
        showViewTracker(false, false, true , false);

        break;
      break;
      case "confirm":
        activeStepTracker = stepsTracker[3];
        $$('configStepTracker').hide();
        $$("wizardConfirmTracker").enable();
        showViewTracker(false, false, false , true);
        break;
      default:
        break;
    }
  }
}

function prevTracker() {

  var indexTracker = stepsTracker.indexOf(activeStepTracker);

  if (indexTracker === 0) {
    indexTracker = indexTracker;
  } else {
    indexTracker = indexTracker - 1;
  }

  var prevStep = stepsTracker[indexTracker];
  switch (prevStep) {
    case "basic":
      activeStepTracker = stepsTracker[0];
      showViewTracker(true, false , false , false);
      break;
    case "page":
      activeStepTracker = stepsTracker[1];
      showViewTracker(false, true , false , false);
      break;
    case "config":
      activeStepTracker = stepsTracker[2];
      showViewTracker(false, false , true , false);
      break;
    case "confirm":
      activeStepTracker = stepsTracker[3];
      showViewTracker(false , false , false , true);
      break;
    default:
      break;
  }
}

function highLightCurrentTracker(current) {
  $(".wizardStepBtn .webix_badge").removeClass('active');
  switch (current) {
    case "basic":
      $(".wizardStepBtn.Basic .webix_badge").addClass('active');
      break;
    case "page":
      $(".wizardStepBtn.Page .webix_badge").addClass('active');
      break;
    case "config":
      $(".wizardStepBtn.Event .webix_badge").addClass('active');
      break;
    case "confirm":
      $(".wizardStepBtn.Confirm .webix_badge").addClass('active');
      break;
    default:
      break;
  }
}

function showViewTracker(basic, page , config , confirm) {
  if (isBasicGeneratedTracker) {
    if (basic) {
      highLightCurrentTracker("basic");
      $$("basicStepTracker").show();
    } else {
      $$("basicStepTracker").hide();
    }
  }

  if (isPageGeneratedTracker) {
    if (page) {
      highLightCurrentTracker("page");
      $$("pageStepTracker").show();
    } else {
      $$("pageStepTracker").hide();
    }
  }

  if (isConfigGeneratedTracker) {
    if (config) {
      highLightCurrentTracker("config");
      $$("configStepTracker").show();
      $$("configStepTracker").config.initStep();
    } else {
      $$("configStepTracker").hide();
    }
  }

  if (isConfirmGeneratedTracker) {
    if (confirm) {
      highLightCurrentTracker("confirm");
      $$("confirmStepTracker").show();
    } else {
        $$("confirmStepTracker").hide();
    }
  }
}

function openWizardTracker(method) {
  if(method === 'add'){
    tracker_obj.reset();
  }

  if($('#floatingContainer').length > 0){
    $('#floatingContainer').remove();
  }


  var f = window.location.hash;
  if(tracker_obj.id === null){
    window.history.pushState(config.addTracker.headerName, config.addTracker.headerName, config.addTracker.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);
  } else {
    window.history.pushState(config.editTracker.headerName, config.editTracker.headerName, config.editTracker.link + '?id=' + tracker_obj.id + ($('body').hasClass('hidenav') ? '&nav=1' : '') + f);
  }

  defaultStepsTracker = ["basic", "page" , "config" , "confirm"];
  activeStepTracker = defaultStepsTracker[0];
  isBasicGeneratedTracker = false;
  isConditionsGeneratedTracker = false;
  isConfirmGeneratedTracker = false;

  if(userStateData){
    isSessional = userStateData.section === 'Tracker';
    activeStepTracker = userStateData.data.step;
  }

  var containerId = "innerContent";



  $("#"+containerId).html("");


  var wizardTracker = document.createElement('div');
  wizardTracker.id = 'webixWizardParentIdTracker';
  wizardTracker.style.minHeight = '680px';
  document.getElementById(containerId).appendChild(wizardTracker);
  var isConfirmDisabledTracker = tracker_obj.id == null ? true : false;

  stepsTracker = defaultStepsTracker;

  var basicButtonTracker = {
    view: "button",
    label: "Name",
    css: "wizardStepBtn Basic",
    id: "wizardBasicBtnTracker",
    width: 170,
    badge: 1,
    click: function() {
      activeStepTracker = stepsTracker[0];
      showViewTracker(true , false , false, false);
    }
  };

  var pageButtonTracker = {
    view: "button",
    css: "wizardStepBtn Page",
    id: "wizardPageTracker",
    label: "Page",
    width: 170,
    badge: 2,
    disabled: isConfirmDisabledTracker,
    click: function() {
      activeStepTracker = stepsTracker[1];
      showViewTracker(false, true , false , false);
    }
  };

  var eventAndFieldTracker = {
    view: "button",
    css: "wizardStepBtn Event",
    id: "wizardConfigTracker",
    label: "Events & Fields",
    width: 180,
    badge: 3,
    disabled: isConfirmDisabledTracker,
    click: function() {
      activeStepTracker = stepsTracker[2];
      showViewTracker(false, false , true , false);
    }
  };


  var consfirmButtonTracker = {
    view: "button",
    css: "wizardStepBtn Confirm Save",
    id: "wizardConfirmTracker",
    label: "Save",
    width: 170,
    badge: 4,
    disabled: isConfirmDisabledTracker,
    click: function() {
      guidelines.startSubGuideline('save');
      activeStepTracker = stepsTracker[3];
      showViewTracker(false, false, false , true);
    }
  };

  webix.ui({
    container: "webixWizardParentIdTracker",
    height: 680,
    id: "webixWizardHeaderMenuTracker",
    rows: [{
      view: "toolbar",
      paddingY: 1,
      height: 50,
      id: "webixWizardHeaderMenuToolbarTracker",
      hidden: false,
      css: "webixWizardHeader",
      elements: [{
          gravity: 1
        }, basicButtonTracker, pageButtonTracker , eventAndFieldTracker , consfirmButtonTracker, {
          gravity: 1
        }

      ]
    }, {
      template: "<div id='wizardbasicSectionTracker'></div><div id='wizardPageSectionTracker'></div><div id='wizardConfigSectionTracker'></div><div id='wizardConfirmSectionTracker'></div>"
    }]
  });

  openBasicSectionTracker();
  openPageSectionTracker();
  openConfigSectionTracker();
  openConfirmSectionTracker();
  if (tracker_obj.mode() === 'edit') {

  }
  startGuidelinesInWizard({
    src: '',
    mode: tracker_obj.mode(),
    add: 'addTracker',
    edit: 'editTracker',
  });
}

function openBasicSectionTracker() {
  highLightCurrentTracker("basic");
  isBasicGeneratedTracker = true;

  var _form = [{
    view: "layout",
    height: 520,
    rows: [{
      view: "text",
      id: "tnameTracker",
      label: 'Name',
      name: "name",
      value: tracker_obj.name ,
      on: {
        onChange: function(newv,oldv){
          tracker_obj.name = newv ;
        }
      }
      // invalidMessage: "Name can not be empty"
    }, {
      view: "richselect",
      id: "ttypeTracker",
      label: 'Type',
      name: "type",
      value: tracker_obj.type,
      options:[
        { "id": 'category', "value": "Category" },
        { "id": 'product', "value": "Product" },
        { "id": 'search', "value": "Search" },
        { "id": 'basket', "value": "Basket" },
        { "id": 'purchase', "value": "Purchase" },
        { "id": 'asset' , "value": "Asset" },
        { "id": 'form' , "value": "Form" },
        { "id": 'custom' , "value": "Custom" }
      ],
      on: {
        onChange: function(newv,oldv){
          tracker_obj.type = newv ;
        }
      }
      // invalidMessage: "Description can not be empty"
    }]
  }, {
    cols: [{}, {}, {
        view: "button",
        "type": "danger",
        value: "Back",
        width: 138,
        disabled: true,
      }, {
        view: "button",
        value: "Next",
        css: "orangeBtn",
        width: 138,
        click: function() {
          nextTrackerStep1();
        }
      }, {}, {},

    ]
  }];
  var __formRules = {
    "name": webix.rules.isNotEmpty,
    // "tdescTracker": webix.rules.isNotEmpty,
  };

  webix.ui({
    container: "wizardbasicSectionTracker",
    id: "basicStepTracker",
    rows: [{
      cols: [{}, {
        view: "form",
        id: "basicFormTracker",
        complexData: true,
        elements: _form,
        rules: __formRules,
        width: 600,
        borderless: true,
        margin: 3,
        elementsConfig: {
          labelPosition: "top",
          labelWidth: 140,
          bottomPadding: 18
        },
      }, {}, ]
    }]
  });


  if (tracker_obj.id) {


  } else {
    if(userStateData && isSessional){
    }
  }

}

function openPageSectionTracker(){
  isPageGeneratedTracker = true ;
  webix.ui({
    container: "wizardPageSectionTracker",
    hidden: true,
    id: "pageStepTracker",
    height: config.styles.defaultHeight.getFileManagerHeight() - 65,
    css: "wizardPWSectionWebPageList",
    rows: [{
        view: "layout",
        rows: [{
          view: "iframe",
          id: "placementWindowIframeContainerTracker",
          css: "placementWindowIframeContainerTracker",
          src: "/component/placement-window?page=tracker&source=tracker",
        }]
      },
    ]
  });

  $$("placementWindowIframeContainerTracker").attachEvent("onAfterLoad", function(sid) {
    var iframesPlacementWindowGoal = $("iframe[src*='component/placement-window']");

    iframesPlacementWindowGoal.get(0).contentWindow.postMessage(JSON.stringify({
      t: "set-stored-values",
      data: {
        fullURL: tracker_obj.url_config.url
      }
    }), getBaseUrl() + "/component/placement-window?page=tracker&source=tracker");
  });
}

// ###TRACKER CONFIG###
function openConfigSectionTracker(){
  isConfigGeneratedTracker = true;

  webix.ui({
    container: "wizardConfigSectionTracker",
    id: "configStepTracker",
    hidden:true,
    postmessage_url:'',
    selected_event_id: null,
    selected_field: null,
    //custom function
    refreshListing:function(){
      //reload event list
      $$('tracker-event-list').clearAll();
      $$('tracker-event-list').parse(tracker_obj.getEventFieldList());
      $$('tracker-event-list').enable();
      //reload field list
      $$('tracker-field-list').clearAll();
      $$('tracker-field-list').parse(tracker_obj.getFieldFieldList());
      $$('tracker-field-list').enable();

      $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
        t: "config-listing" ,
        tracker_obj: tracker_obj
      }), $$('configStepTracker').postmessage_url);
    },
    initStep: function(){
      $$('tracker-iframe-config').load(tracker_obj.url_config.url);
    },
    rows: [{
      view: "layout",
      height: 580,
      borderless:true,
      cols:[
      {
        type:"clean",
        rows:[
        {
          borderless:true,
          view:"tabbar", id:'tracker-config-tabbar', css:'tracker-config-tabbar' , multiview:true, options: [
            { value: 'List', id: 'tracker-config-main'},
            { value: 'Form', id: 'tracker-config-event-form'},
            { value: 'Empty', id: 'tracker-config-field-form'}
          ],
          on:{
            onChange: function(newv , oldv){
              if(newv === 'tracker-config-main'){
                $$('configStepTracker').config.refreshListing();
              }else if(newv === 'tracker-config-event-form'){
                var event  = $$('configStepTracker').selected_event ;
                if(event.id === 'event-new'){

                  $$('tracker-config-event-form-layout').config.header = webix.template('< Add new event');
                  $$('tracker-config-event-form-layout').refresh();

                  $$('event-name').setValue('');
                  $$('event-selector-text').setValue('');
                  $$('event-custom-text').setValue('');
                  $$('event-type').setValue('page-render');
                  $$('event-remove-link').hide();
                }else{
                  var event_data = tracker_obj.getEventById(event.id);
                  if(event === false){
                    this.setValue('tracker-config-main');
                    webix.message('Cannot found event');
                  }
                  $$('tracker-config-event-form-layout').config.header = webix.template('< Edit ' + event_data.name + ' event');
                  $$('tracker-config-event-form-layout').refresh();

                  $$('event-name').setValue(event_data.name);
                  $$('event-selector-text').setValue(event_data.selector);
                  $$('event-custom-text').setValue(event_data.custom);
                  $$('event-type').setValue(event_data.type);
                  $$('event-remove-link').show();

                  $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                    t: "event-display" ,
                    data: event_data
                  }), $$('configStepTracker').postmessage_url);
                }
              }else if(newv === 'tracker-config-field-form'){
                $$('field-button-save').enable();
                var field  = $$('configStepTracker').selected_field ;
                var field_data = {} ;
                if(field.id === 'field-new'){
                  $$('tracker-config-field-form-layout').config.header = webix.template('< Add new field');
                  $$('tracker-config-field-form-layout').refresh();

                  field_data.name = '' ;
                  field_data.field_type = 'string' ;
                  field_data.operator = 'push' ;
                  field_data.type = 'x-path';
                  field_data.selector = '' ;
                  field_data.meta = '';
                  field_data.cookie = '';
                  field_data.js = '';
                  field_data.custom = 'return \'val\';' ;
                  field_data.filter = 'none';
                  field_data.filter_custom = 'return result;' ;
                }else{
                  field_data = tracker_obj.getFieldById(field.id);
                  if(field_data === false){
                    this.setValue('tracker-config-main');
                    webix.message('Cannot found field');
                  }
                  $$('tracker-config-field-form-layout').config.header = webix.template('< Edit ' + field.value + ' field');
                  $$('tracker-config-field-form-layout').refresh();
                }



                $$('field-name').setValue(field_data.name);
                $$('field-type').setValue(field_data.field_type);
                $$('field-operator').setValue(field_data.operator);

                $$('field-selector').setValue(field_data.selector);
                $$('field-meta').config.first_val = field_data.meta ;
                $$('field-meta').setValue(field_data.meta);
                $$('field-cookie').config.first_val = field_data.cookie ;
                $$('field-cookie').setValue(field_data.cookie);
                $$('field-js').config.first_val = field_data.js ;
                //$$('field-js').select(field_data.js);
                $$('field-custom').setValue(field_data.custom);

                $$('field-filter').setValue(field_data.filter);
                $$('field-filter-custom').setValue(field_data.filter_custom);

                $$('field-name').show();
                $$('field-type').show();
                $$('field-operator').show();
                $$('field-method-type').show();
                $$('field-selector').show();
                $$('field-meta').show();
                $$('field-cookie').show();
                $$('field-js').show();
                $$('field-custom').show();
                $$('field-filter').show();
                $$('field-filter-label').show();
                $$('field-filter-custom').show();
                $$('field-example').show();
                $$('field-example-label').show();
                $$('field-remove-link').show();

                $$('field-method-type').setValue(field_data.type);
                //$$('tracker-config-field-form').config.setMethod( $$('field-method-type').getValue());
                $$('tracker-config-field-form').config.setFilter( $$('field-filter').getValue());

                if(tracker_obj.type === 'category' || tracker_obj.type === 'product' || tracker_obj.type === 'search' || tracker_obj.type === 'basket' || tracker_obj.type === 'form'){
                  $$('field-name').disable();
                  $$('field-type').hide();
                  $$('field-type-label').hide();
                  $$('field-operator').hide();
                  $$('field-operator-label').hide();
                }else if(tracker_obj.type === 'asset'){
                  $$('field-operator').hide();
                  $$('field-operator-label').hide();
                }

                if(tracker_obj.type == 'form'){
                  $$('field-method-type').disable();
                  $$('field-meta').hide();
                  $$('field-cookie').hide();
                  $$('field-js').hide();

                  var index = tracker_obj.getFieldIndexById(field.id);
                  if(index === 0){
                    $$('field-custom').hide();
                    $$('field-filter').hide();
                    $$('field-filter-label').hide();
                    $$('field-filter-custom').hide();
                    $$('field-example').hide();
                    $$('field-example-label').hide();
                    $$('field-remove-link').hide();

                  }else{
                    $$('field-name').enable();
                    $$('field-selector').hide();
                  }
                }

                //custom do nothing
                field_data.tracker_type = tracker_obj.type ;
                field_data.field_index =  tracker_obj.getFieldIndexById(field.id) ;

                $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                  t: "field-display" ,
                  data: field_data
                }), $$('configStepTracker').postmessage_url);
              }
            }
          }
        },
        {
          cells:[
            {
              id: 'tracker-config-main',
              rows:[{
              header:'Events',
              css:'tracker-event-list',
              height: 250,
              body:{
                rows :[{
                  id: 'tracker-event-list',
                  view : 'list',
                  disabled: true,
                  template:"#value# <span class='next-arrow'>></span>" ,
                  on: {
                    onItemClick:function(id,e,node){
                      var item = this.getItem(id);
                      $$('configStepTracker').selected_event = item  ;
                      $$('tracker-config-tabbar').setValue('tracker-config-event-form');
                    }
                  }
                }]
              }

            },{
              header:'Fields',
              css:'tracker-field-list',
              height: 317,
              body:{
                rows :[{
                  id: 'tracker-field-list',
                  view : 'list',
                  template:"#value#  <span class='next-arrow'>></span>" ,
                  disabled: true,
                  on: {
                    onItemClick:function(id,e,node){
                      var item = this.getItem(id);
                      $$('configStepTracker').selected_field = item  ;
                      $$('tracker-config-tabbar').setValue('tracker-config-field-form');
                    }
                  }
                }]
              }
            }
            ]
          },
          {
            id:"tracker-config-event-form",
            css: "tracker-form tracker-event-form",
            rows:[{
              id:'tracker-config-event-form-layout',
              getEventdata:function(){
                var event  = $$('configStepTracker').selected_event ;
                return {
                  _id: event.id,
                  name: $$('event-name').getValue(),
                  type: $$('event-type').getValue(),
                  selector: $$('event-selector-text').getValue(),
                  custom: $$('event-custom-text').getValue()
                };
              },
              header:'...',
              height: 579,
              body:{
                view: "form",
                id: 'event-form',
                margin: 0,
                elements :[{
                  view: 'label',
                  label: 'Type'
                },{
                  view: 'richselect',
                  id: 'event-type',
                  options:[
                  {value: 'Page render',id: 'page-render'},
                  {value: 'Click',id: 'click'},
                  {value: 'Custom',id: 'custom'}
                  ],
                  on:{
                    onChange: function(newv,oldv){
                      $$('event-custom-label').hide();
                      $$('event-custom-text').hide();
                      $$('event-selector-label').hide();
                      $$('event-selector-text').hide();
                      if(newv === 'click'){
                        $$('event-selector-label').show();
                        $$('event-selector-text').show();
                      }else if(newv === 'custom'){
                        $$('event-custom-label').show();
                        $$('event-custom-text').show();
                      }

                      if(newv === 'page-render' || newv === 'click'){
                        $$('event-name').hide();
                        $$('event-name-label').hide();
                      }else{
                        $$('event-name').show();
                        $$('event-name-label').show();
                      }

                      var data = $$('tracker-config-event-form-layout').config.getEventdata();
                      $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                        t: "event-display" ,
                        data: data
                      }), $$('configStepTracker').postmessage_url);
                    }
                  }
                },{
                  view: 'label',
                  label: 'Name',
                  hidden: true ,
                  id: 'event-name-label'
                },{
                  view: 'text',
                  id: 'event-name',
                  name: 'name',
                  invalidMessage: "Name can not be empty",
                  hidden: true
                },{
                  view: 'label',
                  label: 'Selector',
                  id: 'event-selector-label',
                  hidden: true
                },{
                  view: 'search',
                  id: 'event-selector-text',
                  hidden: true,
                  on:{
                    onChange:function(newv,oldv){
                      var data = $$('tracker-config-event-form-layout').config.getEventdata();
                      data.selector = newv ;
                      $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                        t: "event-display" ,
                        data: data
                      }), $$('configStepTracker').postmessage_url);
                    }
                  }
                },{
                  view: 'label',
                  label: 'Custom',
                  id: 'event-custom-label',
                  hidden: true
                },{
                  view: 'textarea',
                  id: 'event-custom-text',
                  value: 'return callback();',
                  hidden: true
                },{
                  view: 'label',
                  label: '&nbsp;'
                },{
                  cols: [
                    {
                      view: 'label',
                      id: 'event-remove-link',
                      hidden: true,
                      label: '<a href="javascript:;" onclick="$$(\'tracker-config-event-form\').config.removeEvent();">remove</a>'
                    },
                    {
                      align:"right",
                      view:'button',
                      css: 'orangeBtn',
                      value:'Save',
                      click:function(){
                        var event  = $$('configStepTracker').selected_event ;
                        tracker_obj.updateEvemt(event.id,$$('event-name').getValue(),$$('event-type').getValue(),$$('event-selector-text').getValue(),$$('event-custom-text').getValue());
                        $$('tracker-config-tabbar').setValue('tracker-config-main');
                      }
                    }
                  ]
                }]
              },
              onClick:{
                "webix_accordionitem_label": function(e, id, trg){
                  $$('tracker-config-tabbar').setValue('tracker-config-main');
                }
              }
            }],
            removeEvent: function(){
              webix.alert({
                title: "Are your sure?",
                text: "remove this event",
                ok: "Yes",
                cancel: "No",
                type: "confirm-warning",
                callback:function(res){
                  var event  = $$('configStepTracker').selected_event ;
                  if(res){
                    tracker_obj.removeEvent(event.id);
                    $$('tracker-config-tabbar').setValue('tracker-config-main');
                  }
                }
              });
            }
          },
          {
            id:"tracker-config-field-form",
            setMethod:function(method){
              var method = $$('field-method-type').getValue() ;
              $$('field-selector').hide();
              $$('field-meta').hide();
              $$('field-cookie').hide();
              $$('field-js').hide();
              $$('field-custom').hide();
              if(method === 'x-path'){
                $$('field-selector').show();
              }else if(method === 'meta'){
                $$('field-meta').show();
                $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                  t: "field-get-meta"
                }), $$('configStepTracker').postmessage_url);
              }else if(method === 'cookie'){
                $$('field-cookie').show();
                $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                  t: "field-get-cookie"
                }), $$('configStepTracker').postmessage_url);
              }else if(method === 'js'){
                $$('field-js').show();
                $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                  t: "field-get-js"
                }), $$('configStepTracker').postmessage_url);
              }else if(method === 'custom'){
                if($$('field-custom').getValue() == ''){
                  $$('field-custom').setValue('return \'\';');
                }
                $$('field-custom').show();
              }
            },
            setFilter: function(filter){
              $$('field-filter-custom').hide();
              if(filter === 'custom'){
                $$('field-filter-custom').show();
              }
            },
            css: "tracker-form tracker-field-form",
            rows:[{
              id:'tracker-config-field-form-layout',
              height: 579,
              getFielddata:function(){
                var field  = $$('configStepTracker').selected_field ;
                return {
                  _id: field.id,
                  name: $$('field-name').getValue(),
                  field_type: $$('field-type').getValue(),
                  operator: $$('field-operator').getValue(),
                  type: $$('field-method-type').getValue(),
                  selector: $$('field-selector').getValue(),
                  meta: $$('field-meta').getValue(),
                  cookie: $$('field-cookie').getValue(),
                  js: $$('field-js').getSelectedId(),
                  custom: $$('field-custom').getValue(),
                  filter: $$('field-filter').getValue(),
                  filter_custom: $$('field-filter-custom').getValue()
                };
              },
              commonFieldChange:function(){
                var field_data = $$('tracker-config-field-form-layout').config.getFielddata();
                field_data.tracker_type = tracker_obj.type ;
                field_data.field_index =  tracker_obj.getFieldIndexById(field_data._id) ;

                $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                  t: "field-display" ,
                  data: field_data
                }), $$('configStepTracker').postmessage_url);
              },
              header:'...',
              body:{
                view: "form",
                id: 'config-form',
                scroll: true,
                margin: 0,
                elements :[{
                  view: 'label',
                  label: 'Name'
                },{
                  view: 'text',
                  id: 'field-name',
                  name: 'name',
                  invalidMessage: "Name can not be empty"
                },{
                  view: 'label',
                  label: 'Field type',
                  id: 'field-type-label'
                },{
                  view: 'richselect',
                  id: 'field-type',
                  options:[
                    {value: 'Number',id: 'number'},
                    {value: 'String',id: 'string'},
                    {value: 'Boolean',id: 'boolean'}
                  ]
                },{
                  view: 'label',
                  label: 'Operator',
                  id: 'field-operator-label'
                },{
                  view: 'richselect',
                  id: 'field-operator',
                  options:[
                    {value: 'Override',id: 'override'},
                    {value: 'Add',id: 'add'},
                    {value: 'Substract',id: 'substract'},
                    {value: 'Push',id: 'push'}
                  ]
                },{
                  view: 'label',
                  label: 'Tracker method type',
                  id: 'field-method-type-label'
                },{
                  view: 'richselect',
                  id: 'field-method-type',
                  options:[
                    {value: 'X-Path',id: 'x-path'},
                    {value: 'Meta',id: 'meta'},
                    {value: 'Cookie',id: 'cookie'},
                    {value: 'Javacript',id: 'js'},
                    {value: 'Custom',id: 'custom'}
                  ],
                  on:{
                    onChange: function(newv,oldv){
                      $$('tracker-config-field-form').config.setMethod(newv);
                      $$('tracker-config-field-form-layout').config.commonFieldChange();
                    }
                  }
                },{
                  view: 'search',
                  id: 'field-selector',
                  on:{
                    onChange: function(newv,oldv){
                      $$('tracker-config-field-form-layout').config.commonFieldChange();
                    }
                  }
                },{
                  view: 'richselect',
                  id: 'field-meta',
                  options:[],
                  on:{
                    onChange: function(newv,oldv){
                      $$('tracker-config-field-form-layout').config.commonFieldChange();
                    }
                  }
                },{
                  view: 'richselect',
                  id: 'field-cookie',
                  options:[],
                  on:{
                    onChange: function(newv,oldv){
                      $$('tracker-config-field-form-layout').config.commonFieldChange();
                    }
                  }
                },{
                  view: 'tree',
                  id: 'field-js',
                  height: 200,
                  select: true,
                  scroll: true,
                  data:[],
                  on:{
                    onChange: function(newv,oldv){
                      //$$('tracker-config-field-form-layout').config.commonFieldChange();
                    },
                    onSelectChange: function (ids) {
                      $$('tracker-config-field-form-layout').config.commonFieldChange();
                    }
                  }
                },{
                  view: 'textarea',
                  id: 'field-custom',
                  value: 'return "category";',
                  on:{
                    onChange: function(newv,oldv){
                      $$('tracker-config-field-form-layout').config.commonFieldChange();
                    }
                  }
                },{
                  view: 'label',
                  label: 'Filter',
                  id: 'field-filter-label'
                },{
                  view: 'richselect',
                  id: 'field-filter',
                  options:[
                    {value: 'none',id: 'none'},
                    {value: 'Price filter',id: 'price'},
                    {value: 'Image filter',id: 'image'},
                    {value: 'Custom',id: 'custom'}
                  ],
                  on:{
                    onChange: function(newv,oldv){
                      $$('tracker-config-field-form').config.setFilter(newv);
                      $$('tracker-config-field-form-layout').config.commonFieldChange();
                    }
                  }
                },{
                  view: 'textarea',
                  id: 'field-filter-custom',
                  value: 'return result;',
                  on:{
                    onChange: function(newv,oldv){
                      $$('tracker-config-field-form-layout').config.commonFieldChange();
                    }
                  }
                },{
                  view: 'label',
                  label: 'Example',
                  id: 'field-example-label'
                },{
                  view: 'textarea',
                  id: 'field-example',
                  text: ''
                },{
                  cols:[
                  {
                    view: 'label',
                    label: '<a href="javascript:;" onclick="$$(\'tracker-config-field-form\').config.removeField()">remove</a>',
                    id: 'field-remove-link'
                  },{
                    align:"right",
                    view:'button',
                    css: 'orangeBtn',
                    id: 'field-button-save',
                    value:'Save',
                    click:function(){
                      var field  = $$('configStepTracker').selected_field ;
                      if($$('config-form').validate()){
                        var field_data = $$('tracker-config-field-form-layout').config.getFielddata();
                        var tracker_type = tracker_obj.type ;
                        var field_index = tracker_obj.getFieldIndexById(field_data._id);
                        $$('field-button-save').disable();
                        tracker_obj.updateField(field.id,$$('field-name').getValue(),$$('field-type').getValue(),$$('field-operator').getValue(),$$('field-method-type').getValue()
                        ,$$('field-selector').getValue(),$$('field-meta').getValue(),$$('field-cookie').getValue(),$$('field-js').getSelectedId(),$$('field-custom').getValue(),$$('field-filter').getValue(),$$('field-filter-custom').getValue());


                        if(tracker_type == 'form' && field_index == 0){

                          $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
                            t: "field-form-generate" ,
                            data: field_data
                          }), $$('configStepTracker').postmessage_url);
                        }else{
                          $$('tracker-config-tabbar').setValue('tracker-config-main');
                        }


                      }
                    }
                  }
                  ]
                }],
                rules: {
                  'name': webix.rules.isNotEmpty
                }

              },
              onClick:{
                "webix_accordionitem_label": function(e, id, trg){
                  $$('tracker-config-tabbar').setValue('tracker-config-main');
                }
              }
            }],
            removeField: function(){
              webix.alert({
                title: "Are your sure?",
                text: "remove this field",
                ok: "Yes",
                cancel: "No",
                type: "confirm-warning",
                callback:function(res){
                  var field  = $$('configStepTracker').selected_field ;
                  if(res){
                    tracker_obj.removeField(field.id);
                    $$('tracker-config-tabbar').setValue('tracker-config-main');
                  }
                }
              });
            }
          }
        ]
      }
      ],
        width: 300
      },
      {
        view:"resizer",
        id:"resizer"
      },
      {
        view:"iframe",
        id:"tracker-iframe-config",
        src: tracker_obj.url_config.url,
        on:{
          onAfterLoad:function(){
            //alert('loaded');
          }
        }
      }
      ]
    }, {
      cols: [{}, {}, {}, {
        view: "button",
        "type": "danger",
        value: "Back",
        width: 138,
        click: function() {
          prevTracker();
        }
      }, {
        view: "button",
        value: "Next",
        css: "orangeBtn",
        width: 138,
        click: function() {
          nextTrackerStep3();
        }
      }, {}, {}, {}]
    }]
  });

}


function openConfirmSectionTracker() {
  $("#wizardConfirmSectionTracker").html("");
  var formChanged = false ;

  isConfirmGeneratedTracker = true;
  allFormDataTracker.basic = $$("basicFormTracker").getValues();
  if (iFrameWin !== undefined) {
    if(iFrameWin.returnCondition.logical === 'if (){|}'){
      allFormDataTracker.condition = '';
    } else {
      allFormDataTracker.condition = iFrameWin.returnCondition.logical;
    }
  }

  if(userStateData && isSessional){
    if(['confirm'].indexOf(userStateData.data.step) !== -1){
      allFormDataTracker.condition = userStateData.data.confirmData.conditionTracker;
    }
  }
  var checkIfFormChanged = function(){
    if(formChanged == false){
      parent.postMessage(JSON.stringify({t: "page-confirm-form-changed", formChanged: true}), monolop_base_url);
      formChanged = true;
    }
  }
  var urlOptions = [
    {id: 'exactMatch', value: 'Exact Match'},
    {id: 'allowParams', value: 'Allow Parameters'},
    {id: 'startsWith', value: 'Starts With'},
  ];
  var getUrlOption = function(id){
      if(isNaN(parseInt(id))){
        return id;
      }
      id = id || 0;
      switch (id) {
        case 0: return "exactMatch"; break;
        case 1: return "allowParams"; break;
        case 2: return "startsWith"; break;
        default: return "exactMatch"; break;
      }
  };


  var __form = [{
      view: "layout",
      height: 1024,
      rows: [{
        view: "label",
        id: "fullURLWebPageList",
        // label: 'Name',
        name: "fullURLWebPageList",
        readonly: true,
      }, {
        view: "richselect",
        label: "URL Match Options",
        id: "urlOptionWebPageElement",
        name: "url_option",
        value: tracker_obj.url_config.url_option,
        options: urlOptions,
        labelAlign: 'left',
        on:{
          onChange: function(){
            checkIfFormChanged();
          }
        },
      },{
       view: "textarea",
       rows: 3,
       height: 200,
       value: tracker_obj.url_config.remark,
       id: "remarkWebPageList",
       label: 'Remark',
       name: "remark",
       on:{
         onChange: function(){
           checkIfFormChanged();
         }
       },
       // readonly: true,
       // value: webPageListConfig.allFormData.basic.tRemarkWebPageList
       // invalidMessage: "Description can not be empty"
     }, {
       view: "text",
       id: "regularExpressionWebPageElement",
       label: 'Regular Expression',
       value: tracker_obj.url_config.reg_ex,
       width:500,
       name: "reg_ex",
       on:{
         onChange: function(){
           checkIfFormChanged();
         }
       },
       // readonly: true,
       // value: webPageListConfig.allFormData.basic.tRegularExpressionWebPageElement
       // invalidMessage: "Name can not be empty"
     },{
       cols: [
         {
           view:"checkbox",
           id:"includeWWWWebPageElement",
           name:"inc_www",
           label:"Include WWW",
           labelPosition: "left",
           value: tracker_obj.url_config.inc_www,
           on:{
             onChange: function(){
               checkIfFormChanged();
             }
           },
           // readonly: true,
           // value: webPageListConfig.allFormData.basic.tIncludeWWWWebPageElement
         }, {
           view:"checkbox",
           id:"includeHttpHttpsWebPageElement",
           name:"inc_http_https",
           value: tracker_obj.url_config.inc_http_https,
           label:"Include Http / Https",
           labelPosition: "left",
           on:{
             onChange: function(){
               checkIfFormChanged();
             }
           },
           // readonly: true,
           // value: webPageListConfig.allFormData.basic.tIncludeHttpHttpsWebPageElement
         }
       ]
     }]
    }, ];

  webix.ui({
    container: "wizardConfirmSectionTracker",
    hidden: true,
    id: "confirmStepTracker",
    rows: [{
      cols: [{}, {
        view: "form",
        id: "confirmFormTracker",
        height: 540,
        // scroll: true,
        complexData: true,
        elements: __form,
        // rules: {},
        width: 600,
        borderless: true,
        margin: 3,
        elementsConfig: {
          labelPosition: "top",
          labelWidth: 140,
          bottomPadding: 18
        },
      }, {}, ]
    }, {
      cols: [{}, {}, {
        view: "button",
        label: "Back",
        type: "danger",
        click: function() {
          prevTracker();
        },
        width: 138,
      }, {
        view: "button",
        label: "Confirm and save",
        css: "confirmSave orangeBtn",
        width: 138,
        click: function() {
          var btnCtrl = this;
          btnCtrl.disable();
          var url;
          url = monolop_api_base_url+ '/api/trackers/create';
          if (tracker_obj.mode() === 'edit') {
            url = monolop_api_base_url+ '/api/trackers/update'
          }

          // adding parent folder id

          var form = $$("confirmFormTracker");
          var url_config = form.getValues();

          //console.log(formValues);
          var form_basic = $$("basicFormTracker");
          var basic_config = form_basic.getValues();

          tracker_obj.name = basic_config.name ;
          tracker_obj.type = basic_config.type ;

          tracker_obj.url_config.url_option = url_config.url_option ;
          tracker_obj.url_config.remark = url_config.remark ;
          tracker_obj.url_config.reg_ex = url_config.reg_ex ;
          tracker_obj.url_config.inc_www = url_config.inc_www ;
          tracker_obj.url_config.inc_http_https = url_config.inc_http_https ;

          var data = tracker_obj.getFormData();
          data['ajax'] = true ;

          webix.ajax().post(url,data,{
            error: function(text, data, XmlHttpRequest) {
              btnCtrl.enable();
              alert("error");
            },
            success: function(text, data, XmlHttpRequest) {
              btnCtrl.enable();
              var response = JSON.parse(text);
              if (response.success == true) {
                config.currentElementID = 'webixFilemanagerTracker';
                openLayoutTracker();
                webix.message(response.msg);
              }
            }
          });

          return  ;




          if (tracker_obj.mode() === 'edit') {
            form.setValues({
              source: currentFolderTracker,
              _id: allFormDataTracker.basic.tid,
            }, true);
          } else {
            form.setValues({
              source: currentFolderTracker,
            }, true);
          }
          var formValues = form.getValues();
          formValues.ajax = true;

          webix.ajax().post(url, formValues, {
            error: function(text, data, XmlHttpRequest) {
              btnCtrl.enable();
              alert("error");
            },
            success: function(text, data, XmlHttpRequest) {
              btnCtrl.enable();
              var response = JSON.parse(text);
              if (response.success == true) {
                openLayoutTracker();
                webix.message(response.msg);
              }
            }
          });

          isSessional = false;
          userStateData = '';
        }
      }, {}, {}]
    }]
  });

  // custom changes in publish section
  var _template = '<div id="collapsableContainer">';
  _template    += '<fieldset class=\"collapse\" style=\"border:none;">';
  _template    += '<legend style=\"width: 104%;margin-left: -2%;\">Advanced <i class=\"fa fa-chevron-circle-up\"></i></legend>';
  _template    += '<div class=\"content\" style=\"margin-left:-2%;display:none;width:90%;\" ></div>';
  _template    += '</fieldset>';

  var controlURLMatchOption = $('[view_id=urlOptionWebPageElement]');
  var _pivot = $('div[view_id=remarkWebPageList]');
  // get webix control container div
  var _container = _pivot.nextAll('div');
  // remove existing elements from DOM
  _container.remove();
  controlURLMatchOption.remove();

  // add _template into DOM at above position
  $(_template).insertAfter(_pivot);
  // add existing element as above div's children
  $('#collapsableContainer div.content').append(_container[1], _container[0], controlURLMatchOption);

  // attach toggle handler
  $('#collapsableContainer legend').bind('click', function(){
    $(this).parent().find('div.content').slideToggle();
    // change icon
    if($(this).find('i').hasClass('fa-chevron-circle-up')){
      $(this).find('i').removeClass('fa-chevron-circle-up');
      $(this).find('i').addClass('fa-chevron-circle-down');
    } else{
      $(this).find('i').removeClass('fa-chevron-circle-down');
      $(this).find('i').addClass('fa-chevron-circle-up');
    }
  });

  if (tracker_obj.mode() === 'edit') {
    if(activeStepTracker !== defaultStepsTracker[2]){
      $$("confirmStepTracker").hide();
    }
  } else {
    guidelines.startSubGuideline('save');
    if(userStateData && isSessional){
      // $$("confirmFormTracker").setValues(userStateData.data.confirmData);
    }
  }

}



function refreshManagerTracker(folder_id) {
  folder_id = folder_id || '';
  folderFileManagerTracker.clearAll();
  folderFileManagerTracker.load(monolop_api_base_url+ "/api/trackers?ajax=true");
}


var postMessageListener = function(event) {
  var data = JSON.parse(event.data);
  switch (data.t) {
    case "pw-fullscreen-full":
      $("#wizardPlacementSectionTracker").addClass("fullscreenModeMainCont");
      break;
    case "pw-fullscreen-exit":
      $("#wizardPlacementSectionTracker").removeClass("fullscreenModeMainCont");
      break;
    case "page-published-" + page:
      if (data.source === 'tracker') {
        // webix.message(data.message);
        currentPageElementEditGoal = data.pageElement;
        // console.log(data);
        nextTracker(data.temp.mode, data.temp.formData, data.temp.referenceOptions);
      }
      break;
    case 'to-config-step':
      tracker_obj.url_config.url = data.url ;
      nextTrackerStep2();
      break;
    /*--------- tracker config -----------*/
    case 'confirm':
      var time = new Date().getTime();
      $$('configStepTracker').postmessage_url = data.url;
      $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
        t: "loadbundle",
        domain: monolop_base_url + '/',
        url: monolop_base_url + '/js/invocation/editor-bundle.js?' + time
      }), $$('configStepTracker').postmessage_url);
      break;
    case 'loadbundle-success':
      $$('tracker-iframe-config').getIframe().contentWindow.postMessage(JSON.stringify({
        t: "init-tracker-config"
      }), $$('configStepTracker').postmessage_url);
      break;
    case 'load-tracker-config-success':
      $$('configStepTracker').config.refreshListing();
      break;
    /*-------- FIELD --------------*/
    case 'display-example':
      $$('field-example').setValue(data.result);
      break;
    case 'set-field-selector':
      $$('field-selector').setValue(data.selector);
      break;

    case 'set-field-meta':
      var v = $$('field-meta').getValue();
      var combo = $$('field-meta');
      combo.define("options", data.metas);
      combo.refresh();
      if(v == ''){
        $$('field-meta').setValue($$('field-meta').config.first_val);
      }else{
        $$('field-meta').setValue(v);
      }
      break;
    case 'set-field-cookie':
      var v = $$('field-cookie').getValue();
      var combo = $$('field-cookie');
      combo.define("options", data.cookies);
      combo.refresh();
      if(v == ''){
        $$('field-cookie').setValue($$('field-cookie').config.first_val);
      }else{
        $$('field-cookie').setValue(v);
      }
      break;
    case 'set-field-js':
      var v = $$('field-js').getSelectedId();
      var combo = $$('field-js');
      combo.parse(data.js);
      /*
      combo.define("options", data.js);
      combo.refresh();
      */
      if(v == ''){
        if($$('field-js').config.first_val){
          $$('field-js').select($$('field-js').config.first_val);
        }
      }else{
        $$('field-js').select(v);
      }

      break;
    case 'generate-form-field':
      for(var i =  tracker_obj.tracker_fields.length - 1 ; i >= 1 ; i--){
        tracker_obj.tracker_fields.splice(i, 1);
      }

      for(var i = 0 ; i < data.data.length ; i++){
        var field = data.data[i];
        tracker_obj.updateField('',field.name,field.type,field.operator,field.method_type
                        ,field.selector,field.meta,field.cookie,field.js,field.custom,field.filter,field.filter_custom);
      }
      $$('tracker-config-tabbar').setValue('tracker-config-main');

      break;
    case 'set-event-selector':
      $$('event-selector-text').setValue(data.selector);
      break;
    default:
      break;
  }
};
// insert postmessageListener ;
if (window.addEventListener) {
    addEventListener("message", postMessageListener, false);
} else {
    attachEvent("onmessage", postMessageListener);
}
