
var folderFileManagerGoal; // file manager variable

var defaultStepsGoal;
var activeStepGoal;
var goalTypeGoal;
var isSessional = false;
var isBasicGeneratedGoal, isPlacementGeneratedGoal, isPlacementEditRecordGeneratedGoal, isConditionsGeneratedGoal, isPointsGeneratedGoal, isConfirmGeneratedGoal;
var conditionBuilderUrlGoal = '/component/profile-properties-template';
var iFrameWin;
var tempConditionGoal = {};
var page = 'goal';
var PWIFrameWin;
var PWUrlOptions = {
    url: "monoloop.com",
    url_option: "exactMatch",
    inc_www: 0,
    inc_http_https: 0,
    reg_ex: ""
};
var currentPageElementEditGoal = false;
var extendedFab = {
    actionButtonTypes: {
        type: "create",
        setType: function(t) {
            t = t || "create";
            extendedFab.actionButtonTypes.type = t;
        },
        fabOptions: false,
        create: {
            options: [
                // {
                // 	label: "This is to test",
                // 	className: "segmentBuilder inline",
                //   callback: function(){
                //     webix.alert("This is to test......");
                //   }
                // }
            ]
        },
    },
    fabOptions: false,
    primary: {
        primaryBtnLabelCallback: function(actionButton, floatingTextBG) {
            actionButton.className += " createGoal inline";
            floatingTextBG.textContent = "Create goal";
            actionButton.onclick = function() {
                config.currentElementID = 'webixWizardHeaderMenuGoal';
                guidelines.removeHangedTooltips();
                openWizardGoal('add');
            };
        },
    }


};

var allFormDataGoal = {};

function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
};

function openLayoutGoal() {
    var params = getQueryParams(document.location + '');
    var f = window.location.hash;
    window.history.pushState(config.goals.headerName, config.goals.headerName, config.goals.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);

    $("#innerContent").html("");

    var formElement = document.createElement('div');
    formElement.id = 'fileManagerParentIdGoal';
    formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
    document.getElementById("innerContent").appendChild(formElement);

    // webix.ready(function() {
    folderFileManagerGoal = webix.ui({
        container: "fileManagerParentIdGoal",
        view: "filemanager",
        url: monolop_api_base_url + "/api/goals?ajax=true" + (params.url ? ('&url=' + params.url) : ''), // loading data from the URL
        id: "webixFilemanagerGoal",
        filterMode: {
            showSubItems: true,
            openParents: true
        },
        mode: "table", // specify modeGoal selected by default
        modes: ["files", "table", "custom"], // all available modes including a new modeGoal
        // save and handle all the menu actionsGoal from here,
        // disable editing on double-click
        handlers: {
            "upload": monolop_api_base_url + "/api/goals/create",
            // "download": "data/saving.php",
            // "copy": "data/saving.php",
            "move": monolop_api_base_url + "/api/folders/move",
            "remove": monolop_api_base_url + "/api/folders/delete",
            "rename": monolop_api_base_url + "/api/folders/update",
            "create": monolop_api_base_url + "/api/folders/create",
            "files": monolop_api_base_url + "/api/goals/folder?ajax=true" + (params.url ? ('&url=' + encodeURIComponent(params.url)) : ''),
        },
        structure: {},
        on: {
            "onViewInit": function(name, config) {
                if (name == "table" || name == "files") {
                    // disable multi-selection for "table" and "files" views
                    config.select = true;

                    if (name == "table") {
                        // disable editing on double-click
                        config.editaction = false;
                        // an array with columns configuration
                        var columns = config.columns;
                        //  disabling columns date, type, size
                        columns.splice(1, 3);

                        // configuration of a new column description
                        // var typeColumnGoal = {
                        //     id: "typeColumnGoal",
                        //     header: "Type",
                        //     fillspace: 1,
                        //     template: function(obj, common) {
                        //         return obj.goal_type || ""; // "description" property of files
                        //     }
                        // };

                        // configuration of a new column date
                        // var dateColumnGoal = {
                        //     id: "dateColumnGoal",
                        //     header: "Date",
                        //     fillspace: 2,
                        //     sort: function(a, b){
                        //       var _a = new Date(a.date), _b = new Date(b.date);
                        //       return (_a > _b ? 1 : (_b > _a ? -1: 0));
                        //     },
                        //     template: function(obj, common) {
                        //         return obj.date || ""; // "description" property of files
                        //     }
                        // };

                        // configuration of a new column actionsGoal
                        // var actionsColumnGoal = {
                        //     id: "actionsColumnGoal",
                        //     header: "Actions",
                        //     fillspace: 1,
                        //     template: function(obj, common) {
                        //
                        //         if (typeof(obj.condition) === undefined || obj.condition === null) {
                        //             obj.condition = '';
                        //         }
                        //
                        //         var paramsGoal = {
                        //             source: 'segment',
                        //             id: obj.id
                        //         };
                        //         return '<a   title="Edit goal" onclick="updateGoal(\'' + obj.id + '\');" class="webix_list_item updateGoal" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" title="delete" onclick="deleteGoal(\'' + obj.id + '\');" class="webix_list_item deleteGoal" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
                        //     }
                        // };

                        // configuration of a new column actions
                        // var reportColumnGoals = {
                        //     id: "reportColumnGoals",
                        //     header: "Report",
                        //     // fillspace: 1,
                        //     width: 70,
                        //     template: function(obj, common) {
                        //         // console.log(" obj.id", obj)
                        //         var url = '/reports/goals/detail/' + obj.id + ($('body').hasClass('hidenav') ? '?nav=1' : '')  + (params.url ? ('&url=' + params.url) : '');
                        //         return '<a  title="Goal report" onclick="window.location = \'' + url + '\';" class="webix_list_item viewReport" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-bar-chart"></span></a>';
                        //     }
                        // };

                        // configuration of a new column status
                        var statusColumnGoal = {
                            id: "statusColumnGoal",
                            header: "Status",
                            fillspace: 1,
                            sort: function(a, b){
                              var _a = Number(a.hidden), _b = Number(b.hidden);
                              return (_a > _b ? 1 : (_b > _a ? -1: 0));
                            },
                            template: function(obj, common) {
                                var obj_ = {
                                    action: "update_hidden",
                                    hidden: "1"
                                };
                                var paramsGoal = {
                                    _id: obj.id,
                                    action: "update_hidden",
                                    node: this
                                };
                                if (obj.hidden == 0) {
                                    paramsGoal.hidden = 1;
                                    return '<a id="updateHiddenGoal' + obj.id + '"  onclick=\'updateHiddenGoal( ' + JSON.stringify(paramsGoal) + ');\' class="webix_list_item updateGoalStatus status status0" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actionsGoal" property of files ;
                                }
                                paramsGoal.hidden = 0;
                                return '<a id="updateHiddenGoal' + obj.id + '"  onclick=\'updateHiddenGoal(' + JSON.stringify(paramsGoal) + ');\' class="webix_list_item updateGoalStatus  status status1" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
                            }
                        };

                        // changes related to Mantis # 3974
                        var conversionsColumnGoal = {
                            id: "conversionsColumnGoal",
                            header: "Conversions",
                            fillspace: 1,
                            template: function(obj, common) {
                                return obj.conversions || 0; // "description" property of files
                            }
                        };

	                       var conversionRateColumnGoal = {
                            id: "conversionRateColumnGoal",
                            header: "CR%",
                            fillspace: 1,
                            template: function(obj, common) {
                                return obj.conversion_rate ||0; // "description" property of files
                            }
                        };

	                       var actionsColumnGoal = {
                            id: "actionsColumnGoal",
                            header: "Actions",
                            fillspace: 1,
                            template: function(obj, common) {

                                if (typeof(obj.condition) === undefined || obj.condition === null) {
                                    obj.condition = '';
                                }

                                var paramsGoal = {
                                    source: 'segment',
                                    id: obj.id
                                };
                                var url = '/reports/goals/detail/' + obj.id + ($('body').hasClass('hidenav') ? '?nav=1' : '')  + (params.url ? ('&url=' + params.url) : '');
                                return '<a   title="Edit goal" onclick="updateGoal(\'' + obj.id + '\');" class="webix_list_item updateGoal" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" title="delete" onclick="deleteGoal(\'' + obj.id + '\');" class="webix_list_item deleteGoal" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>'+ ' | ' + '<a webix_l_id="remove" title="Goal report" onclick="window.location = \'' + url + '\';" class="webix_list_item viewReport" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-bar-chart"></span></a>';
                            }
                        };

                        // insert columns
                        // webix.toArray(columns).insertAt(typeColumnGoal, 1);
                        // webix.toArray(columns).insertAt(dateColumnGoal, 2);
                        // webix.toArray(columns).insertAt(reportColumnGoals, 3);
                        // webix.toArray(columns).insertAt(actionsColumnGoal, 4);
                        // webix.toArray(columns).insertAt(statusColumnGoal, 5);

                        // changes related to Mantis # 3974
                        webix.toArray(columns).insertAt(conversionsColumnGoal, 1);
                        webix.toArray(columns).insertAt(conversionRateColumnGoal, 2);
                        webix.toArray(columns).insertAt(actionsColumnGoal, 3);
                        webix.toArray(columns).insertAt(statusColumnGoal, 4);
                    }

                }
            }
        }
    });

    /*******************************Menu Customization******************************/
    // only FILES mode should be available
    $$('webixFilemanagerGoal').$$('modes').hide();

    // updating options from menu
    var actionsGoal = $$("webixFilemanagerGoal").getMenu();
    actionsGoal.clearAll();
    var newDataGoal = [

        {
            id: "create",
            method: "createFolder",
            icon: "folder-o",
            value: webix.i18n.filemanager.create // "Create Folder"
        }, {
            id: "deleteFile",
            method: "deleteFile",
            icon: "times",
            value: webix.i18n.filemanager.remove // "Delete"
        }, {
            id: "edit",
            method: "editFile",
            icon: "edit",
            value: webix.i18n.filemanager.rename // "Rename"
        }
    ];
    actionsGoal.parse(newDataGoal);
    // add new option for the menu to add new segment
    actionsGoal.add({
        id: "createGoal",
        icon: "file",
        value: "Create Goal"
    });

    /*******************************Goals Add**********************************/

    actionsGoal.attachEvent("onItemClick", function(id) {
        this.hide();
        // check if the action is CreateSegment

        if (id === 'deleteFile') {}
        if (id == "createGoal") {
            config.currentElementID = 'webixWizardHeaderMenuGoal';
            openWizardGoal('add');
        }
    });

    actionsGoal.getItem("create").batch = "item, root";
    actionsGoal.getItem("deleteFile").batch = "item, root";
    actionsGoal.getItem("edit").batch = "item, root";
    actionsGoal.getItem("createGoal").batch = "item, root";

    /******************************Segment Add End*******************************/


    /*****************************Menu Customization End*************************/


    /*******************************Custom Events********************************/

    /********************************Segment Edit********************************/

    $$("webixFilemanagerGoal").attachEvent("onBeforeDeleteFile", function(id) {
        // Fix: mantis # 3945
        if (id.indexOf('goal__') === -1) {
          var item = this.getItem(id);
          if(item.$parent === 0){
            webix.message({type: "error", text: "Root folder can not be deleted."})
            return false;
          }else{
            checkFolderStatus(id, folderFileManagerGoal);
            return false;
          }
        }
    });


    $$("webixFilemanagerGoal").$$("table").attachEvent("onBeforeSelect", function(id, p) {
        var rowName = id.row,
            columnName = id.column;
        var item = this.getItem(rowName);

        if (item.type !== "folder") {
            if (['reportColumnGoals', 'actionsColumnGoal', 'statusColumnGoal'].indexOf(columnName) === -1) {
                //  updateGoal(item.id);
            }
        }
        //note that 'id' for datatable is an object with several attributes
    });
    // before editing file
    $$("webixFilemanagerGoal").attachEvent("onBeforeEditFile", function(id) {

        var srcTypeId = id.split('__');

        if (srcTypeId[0] == 'folder') {
            return true;
        }

        updateGoal(id);

        return false;
    });

    /*****************************Segment edit End*******************************/

    // reload grid after folder creation
    $$("webixFilemanagerGoal").attachEvent("onAfterCreateFolder", function(id) {
        // refreshManagerGoal();
        openLayoutGoal();
        setTimeout(function() {
            openLayoutGoal();
        }, 500);
        return false;
    });


    // it will be triggered before deletion of file
    $$("webixFilemanagerGoal").attachEvent("onBeforeDeleteFile", function(ids) {
        // deleteGoal(ids);
        if (ids.indexOf('goal__') === -1) {
            return true
        }

        deleteGoal(ids);
        return false;
    });

    // it will be triggered before dragging the folder/segment
    $$("webixFilemanagerGoal").attachEvent("onBeforeDrag", function(context, ev) {
        return true;
    });
    $$("webixFilemanagerGoal").attachEvent("onBeforeDrop", function(context, ev) {
        if (context.start.indexOf('goal__') === -1) {
            return true
        }
        webix.ajax().post(monolop_api_base_url + "/api/goals/change-folder", {
            _id: context.start,
            target: context.target,
            ajax: true,
        }, {
            error: function(text, data, XmlHttpRequest) {
                alert("error");
            },
            success: function(text, data, XmlHttpRequest) {
                var response = JSON.parse(text);
                if (response.success == true) {}
            }
        });
        return true;
    });
    // it will be triggered after dropping the folder to the destination
    $$("webixFilemanagerGoal").attachEvent("onAfterDrop", function(context, ev) {
        webix.message("Goal has been moved to new folder.");
        return false;
    });

    $$("webixFilemanagerGoal").$$("files").attachEvent("onBeforeRender", filterFilesGoal);
    $$("webixFilemanagerGoal").$$("table").attachEvent("onBeforeRender", filterFilesGoal);

    var _exe = false;
    $$("webixFilemanagerGoal").attachEvent("onAfterLoad", function(context, ev) {
        if (_exe === false) {
            _exe = true;
            initIntialTTandGuides();
        }
    });

    // fix: mantis # 3946
    $$("webixFilemanagerGoal").attachEvent("onSuccessResponse", function(req, res) {
    	if(res.success && (req.source === "newFolder" || req.source.indexOf("folder") != -1)){
    		switch(req.action){
    			case "rename":
    				webix.message("Folder renamed");
    				break;
    			case "create":
    				webix.message("Folder Created");
    				break;
    			case "remove":
    				webix.message("Folder deleted");
    				break;
    		}
    	}
    });
    /*******************************Custom Events End****************************/
    // });

    fab.initActionButtons({
        type: "goalList",
        actionButtonTypes: extendedFab.actionButtonTypes,
        primary: extendedFab.primary,
    });

    $(window).on('resize', function(){
      $$('webixFilemanagerGoal').define('width', $('body').width() - $('.sidebar').width() - 1);
      $$('webixFilemanagerGoal').resize();
    });

}
// filter segments only
function filterFilesGoal(data) {
    data.blockEvent();
    data.filter(function(item) {
        return item.type != "folder"
    });
    data.unblockEvent();
}

/**********************************Functions***********************************/
function updateGoal(id, src) {
    var url = monolop_api_base_url + '/api/goals/show';

    // retreiving form data and setting into the form
    var formDataGoal;
    webix.ajax().get(url, {
        _id: id,
        ajax: true,
    }, function(text, xml, xhr) {
        //response
        formDataGoal = JSON.parse(text);
        openWizardGoal('edit', formDataGoal, undefined, src);
    });
}


function deleteGoal(id) {
    webix.confirm({
        text: "Do you want to delete?",
        ok: "Yes",
        cancel: "No",
        callback: function(result) {
            if (result) {
                webix.ajax().post(monolop_api_base_url + "/api/goals/delete", {
                    _id: id,
                    ajax: true,
                }, {
                    error: function(text, data, XmlHttpRequest) {
                        alert("error");
                    },
                    success: function(text, data, XmlHttpRequest) {
                        var response = JSON.parse(text);
                        if (response.success == true) {
                            folderFileManagerGoal.deleteFile(id);
                        }
                    }
                });
            }
        }
    });

}

function updateHiddenGoal(paramsGoal) {
    if (paramsGoal.hidden == 1) {
        var message = webix.message("deactivating...");
    } else if (paramsGoal.hidden == 0) {
        var message = webix.message("activating...");
    }
    paramsGoal.ajax = true;

    webix.ajax().post(monolop_api_base_url + "/api/goals/update-status", paramsGoal, {
        error: function(text, data, XmlHttpRequest) {
            alert("error");
        },
        success: function(text, data, XmlHttpRequest) {
            var response = JSON.parse(text);
            if (response.success == true && paramsGoal.action == "update_hidden") {
                var element = document.getElementById("updateHiddenGoal" + paramsGoal._id)
                if (paramsGoal.hidden == 0) {
                    element.innerHTML = "active";
                    element.style.color = "green";
                    element.onclick = function() {
                        paramsGoal.hidden = 1;
                        updateHiddenGoal(paramsGoal);
                    };
                    webix.message("Goal has been activated.");

                } else if (paramsGoal.hidden == 1) {
                    element.innerHTML = "inactive";
                    element.style.color = "red";
                    element.onclick = function() {
                        paramsGoal.hidden = 0;
                        updateHiddenGoal(paramsGoal);
                    };
                    webix.message("Goal has been deactivated.");
                }
                refreshManagerGoal();
                webix.message.hide(message);
            }
        }
    });
}

function nextGoal(modeGoal, formDataGoal, referenceOptionsGoal) {

    var indexGoal = stepsGoal.indexOf(activeStepGoal);

    if (indexGoal === stepsGoal.length - 1) {
        indexGoal = indexGoal;
    } else {
        indexGoal = indexGoal + 1;
    }

    var nextStepGoal = stepsGoal[indexGoal];

    var isValidGoal = false;

    switch (activeStepGoal) {
        case "basic":
            var form = $$("basicFormGoal");
            if (form.validate()) {
                isValidGoal = true;
            }
            break;
        case "placement_edit_record":

            var form = $$("placementEditRecordFormGoal");
            if (form.validate()) {
                isValidGoal = true;
            }
            break;
        default:
            isValidGoal = true;
            break;
    }

    if (isValidGoal) {
        switch (nextStepGoal) {
            case "basic":
                activeStepGoal = stepsGoal[0];
                break;
            case "placement":
                activeStepGoal = stepsGoal[1];
                $$("basicStepGoal").hide();
                if (!isPlacementGeneratedGoal) {
                    openPlacementSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                } else {
                    showViewGoal(false, true, false, false, modeGoal, formDataGoal, referenceOptionsGoal);
                }
                break;
            case "conditions":

                if (goalTypeGoal == "condition_related") {
                    activeStepGoal = stepsGoal[1];
                    $$("basicStepGoal").hide();
                } else {
                    activeStepGoal = stepsGoal[2];
                    $("#placementStepGoal").hide();
                }
                if (!isConditionsGeneratedGoal) {
                    openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                } else {
                    if (goalTypeGoal == "condition_related") {
                        $$("wizardConditionsGoal").enable();
                    } else {
                        $$("wizardConditionsPageGoal").enable();
                    }
                }
                showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);

                break;
            case "confirm":
                switch(goalTypeGoal){
                  case "page_related":{
                    activeStepGoal = stepsGoal[2];
                    break;
                  }
                  case "page_related_with_condition":{
                    activeStepGoal = stepsGoal[3];
                    break;
                  }
                  case "condition_related":{
                    activeStepGoal = stepsGoal[2];
                    break;
                  }
                  default:{
                    activeStepGoal = stepsGoal[3];
                    break;
                  }
                }

                // if (goalTypeGoal == "condition_related") {
                //     activeStepGoal = stepsGoal[2];
                // } else{
                //     activeStepGoal = stepsGoal[3];
                // }

                // Hide the previous step containers
                switch(goalTypeGoal){
                  case "condition_related":
                    $('#wizardConditionSectionGoal').hide();
                    break
                  case "page_related":
                    $('#wizardPlacementSectionGoal').hide();
                    break;
                  case "page_related_with_condition":
                    $('#wizardConditionSectionGoal').hide();
                    break;
                }
                // $('#wizardConditionSectionGoal').hide();

                if (!isConfirmGeneratedGoal) {
                  openConfirmSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                } else {
                    if (goalTypeGoal == "condition_related") {
                        $$("wizardConfirmBtnGoal").enable();
                    } else {
                        $$("wizardConfirmPageBtnGoal").enable();
                    }
                    showViewGoal(false, false, false, true, modeGoal, formDataGoal, referenceOptionsGoal);
                }
                break;
            default:
                break;
        }
    }
}

function prevGoal(modeGoal, formDataGoal, referenceOptionsGoal) {

    var indexGoal = stepsGoal.indexOf(activeStepGoal);

    if (indexGoal === 0) {
        indexGoal = indexGoal;
    } else {
        indexGoal = indexGoal - 1;
    }

    var prevStep = stepsGoal[indexGoal];
    switch (prevStep) {
        case "basic":
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[0];
            } else {
                activeStepGoal = stepsGoal[0];
            }
            showViewGoal(true, false, false, false, modeGoal, formDataGoal, referenceOptionsGoal);
            break;
        case "placement":
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[1];
            } else {
                activeStepGoal = stepsGoal[1];
            }
            showViewGoal(false, true, false, false, modeGoal, formDataGoal, referenceOptionsGoal);
            break;
            // case "placement_edit_record":
            //   if (goalTypeGoal == "condition_related") {
            //     activeStepGoal = stepsGoal[2];
            //   } else {
            //     activeStepGoal = stepsGoal[2];
            //   }
            //   showViewGoal(false, false, true, false, false, false, modeGoal, formDataGoal, referenceOptionsGoal);
            //   break;
        case "conditions":
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[1];
            } else {
                activeStepGoal = stepsGoal[2];
            }

            showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
            break;
        case "confirm":
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[2];
            } else {
                activeStepGoal = stepsGoal[3];
            }
            showViewGoal(false, false, false, true, modeGoal, formDataGoal, referenceOptionsGoal);
            break;
        default:
            break;
    }
}

function highLightCurrentGoal(current) {
    $(".wizardStepBtn .webix_badge").removeClass('active');
    switch (current) {
        case "basic":
            $(".wizardStepBtn.Basic .webix_badge").addClass('active');
            break;
        case "placement":
            $(".wizardStepBtn.Placement .webix_badge").addClass('active');
            break;
        case "placement_edit_record":
            $(".wizardStepBtn.PlacementEditRecord .webix_badge").addClass('active');
            break;
        case "conditions":
            $(".wizardStepBtn.Conditions .webix_badge").addClass('active');
            break;
        case "points":
            $(".wizardStepBtn.Points .webix_badge").addClass('active');
            break;
        case "confirm":
            $(".wizardStepBtn.Confirm .webix_badge").addClass('active');
            break;
        default:
            break;
    }
}

function showViewGoal(basic, placement, condition, confirm, modeGoal, formDataGoal, referenceOptionsGoal) {
    if (isBasicGeneratedGoal) {
        if (basic) {
            highLightCurrentGoal("basic");
            $$("basicStepGoal").show();
            showHeaderGoal();
        } else {
            $$("basicStepGoal").hide();
        }

    }
    if (placement) {
        if (!isPlacementGeneratedGoal) {
            openPlacementSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
        }
    }

    if (isPlacementGeneratedGoal) {
        if (placement) {
            highLightCurrentGoal("placement");
            $("#wizardPlacementSectionGoal").show();
            hideHeaderGoal();
        } else {
            $("#wizardPlacementSectionGoal").hide();
        }

    }
    // if(isPlacementEditRecordGeneratedGoal){
    //   if(placementEditRecord){
    //     highLightCurrentGoal("placement_edit_record");
    //     showHeaderGoal();
    //     // $$("placementEditRecordStepGoal").show();
    //     openPlacementEditRecordSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
    //   } else {
    //     $$("placementEditRecordStepGoal").hide();
    //   }
    // }

    if (isConditionsGeneratedGoal) {
        if (condition) {
            highLightCurrentGoal("conditions");
            showHeaderGoal();
            $('#wizardConditionSectionGoal').show();
            // $$("conditionsStepGoal").show();
        } else {
            $('#wizardConditionSectionGoal').hide();
            // $$("conditionsStepGoal").hide();
        }
    }

    // if (isPointsGeneratedGoal) {
    //   if (points) {
    //     highLightCurrentGoal("points");
    //     showHeaderGoal();
    //     $$("pointsStepGoal").show();
    //   } else {
    //     $$("pointsStepGoal").hide();
    //   }
    // }
    if (isConfirmGeneratedGoal) {
        if (confirm) {
            highLightCurrentGoal("confirm");
            showHeaderGoal();
            openConfirmSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            $$("confirmStepGoal").show();
        } else {
            $$("confirmStepGoal").hide();
        }
    }

    if(goalTypeGoal == 'page_related_with_condition' && activeStepGoal == 'conditions'){
      if($('div[view_id=wizardPlacementBtnGoal]').hasClass('webix_disabled_view')){
        $('div[view_id=wizardPlacementBtnGoal]').removeClass('webix_disabled_view');
        $('div[view_id=wizardPlacementBtnGoal] div.webix_disabled').remove();
      }
    }
}

function openWizardGoal(modeGoal, formDataGoal, referenceOptionsGoal, src) {
    if ($('#floatingContainer').length > 0) {
        $('#floatingContainer').remove();
    }


    var f = window.location.hash;
    // Fix: Mantis # 3996
    if(referenceOptionsGoal == null){
      if (modeGoal === 'add') {
          window.history.pushState(config.addgoals.headerName, config.addgoals.headerName, config.addgoals.link + f);
      } else {
          window.history.pushState(config.editgoals.headerName, config.editgoals.headerName, config.editgoals.link + '?id=' + formDataGoal._id + f);
      }
    }

    defaultStepsGoal = ["basic", "conditions", "confirm"];
    activeStepGoal = defaultStepsGoal[0];
    goalTypeGoal = "condition_related";
    isBasicGeneratedGoal = false;
    isPlacementGeneratedGoal = false;
    isPlacementEditRecordGeneratedGoal = false;
    isConditionsGeneratedGoal = false;
    isPointsGeneratedGoal = false;
    isConfirmGeneratedGoal = false;


    if (userStateData) {
        isSessional = userStateData.section === 'goals';
        activeStepGoal = userStateData.data.step;
    }

    var containerId = "innerContent";

    if (referenceOptionsGoal !== undefined) {
        containerId = referenceOptionsGoal.container;
    }

    $("#" + containerId).html("");

    var wizardGoal = document.createElement('div');
    wizardGoal.id = 'webixWizardParentIdGoal';

    document.getElementById(containerId).appendChild(wizardGoal);


    stepsGoal = defaultStepsGoal;

    var basicButtonGoal = {
        view: "button",
        label: "Name",
        css: "wizardStepBtn Basic",
        id: "wizardBasicBtnGoal",
        width: 170,
        badge: 1,
        click: function() {
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[0];
            } else {
                activeStepGoal = stepsGoal[0];
            }

            showViewGoal(true, false, false, false, modeGoal, formDataGoal);
        }
    };

    var placementWindowButtonGoal = {
        view: "button",
        css: "wizardStepBtn Placement Page",
        id: "wizardPlacementBtnGoal",
        label: "Page",
        width: 170,
        badge: 2,
        disabled: true,
        click: function() {
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[1];
            } else {
                activeStepGoal = stepsGoal[1];
            }
            showViewGoal(false, true, false, false, modeGoal, formDataGoal);
        }
    };

    // var placementWindowEditRecordButtonGoal = {
    //   view: "button",
    //   css: "wizardStepBtn PlacementEditRecord",
    //   id: "wizardPlacementEditRecordBtnGoal",
    //   label: "URL Options",
    //   width: 170,
    //   badge: 3,
    //   disabled: true,
    //   click: function() {
    //     if (goalTypeGoal == "condition_related") {
    //       activeStepGoal = stepsGoal[2];
    //     } else {
    //       activeStepGoal = stepsGoal[2];
    //     }
    //
    //     showViewGoal(false, false, true, false, false, false, modeGoal, formDataGoal);
    //   }
    // };

    var conditionsButtonGoal = {
        view: "button",
        css: "wizardStepBtn Conditions",
        id: "wizardConditionsGoal",
        label: "Conditions",
        width: 170,
        badge: 2,
        disabled: true,
        click: function() {
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[1];
            } else {
                activeStepGoal = stepsGoal[2];
            }

            showViewGoal(false, false, true, false, modeGoal, formDataGoal);
        }
    };

    var conditionsPageButtonGoal = {
        view: "button",
        css: "wizardStepBtn Conditions",
        id: "wizardConditionsPageGoal",
        label: "Conditions",
        width: 170,
        badge: 3,
        disabled: true,
        click: function() {
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[1];
            } else {
                activeStepGoal = stepsGoal[2];
            }

            showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
        }
    };

    var consfirmButtonGoal = {
        view: "button",
        css: "wizardStepBtn Confirm Save",
        id: "wizardConfirmBtnGoal",
        label: "Save",
        width: 170,
        badge: 3,
        disabled: true,
        click: function() {
            guidelines.startSubGuideline('save');
            if (goalTypeGoal == "condition_related") {
                activeStepGoal = stepsGoal[2];
            } else {
                activeStepGoal = stepsGoal[3];
            }

            showViewGoal(false, false, false, true, modeGoal, formDataGoal, referenceOptionsGoal);
        }
    };

    var confirmButtonForPageRelatedWithConditionGoal = {
        view: "button",
        css: "wizardStepBtn Confirm Save",
        id: "wizardConfirmPageBtnGoal",
        label: "Save",
        width: 170,
        badge: 4,
        disabled: true,
        click: function() {
            activeStepGoal = stepsGoal[3];
            showViewGoal(false, false, false, true, modeGoal, formDataGoal, referenceOptionsGoal);

        }
    };

    var confirmButtonForPageRelatedGoal = {
        view: "button",
        css: "wizardStepBtn Confirm Save",
        id: "wizardConfirmPageBtnGoal",
        label: "Save",
        width: 170,
        badge: 3,
        disabled: true,
        click: function() {
            activeStepGoal = stepsGoal[2];
            showViewGoal(false, false, false, true, modeGoal, formDataGoal, referenceOptionsGoal);

        }
    };

    webix.ui({
        container: "webixWizardParentIdGoal",
        height: 900,
        id: "webixWizardHeaderMenuGoal",
        rows: [{
            view: "toolbar",
            paddingY: 1,
            height: 50,
            id: "webixWizardConditionalHeaderMenuToolbarGoal",
            hidden: false,
            css: "webixWizardHeader",
            elements: [{
                    gravity: 1
                }, basicButtonGoal, conditionsButtonGoal, consfirmButtonGoal, {
                    gravity: 1
                }

            ]
        }, {
            view: "toolbar",
            paddingY: 1,
            height: 50,
            id: "webixWizardPageHeaderConditionalMenuToolbarGoal",
            hidden: true,
            css: "webixWizardHeader",
            elements: [{
                    gravity: 1
                }, basicButtonGoal, placementWindowButtonGoal, conditionsPageButtonGoal, confirmButtonForPageRelatedWithConditionGoal, {
                    gravity: 1
                }

            ]
        }, {
            view: "toolbar",
            paddingY: 1,
            height: 50,
            id: "webixWizardPageHeaderMenuToolbarGoal",
            hidden: true,
            css: "webixWizardHeader",
            elements: [{
                    gravity: 1
                }, basicButtonGoal, placementWindowButtonGoal, confirmButtonForPageRelatedGoal, {
                    gravity: 1
                }

            ]
        }, {
            template: "<div id='wizardbasicSectionGoal'></div><div id='wizardPlacementSectionGoal'></div><div id='wizardConditionSectionGoal'></div><div id='wizardConfirmSectionGoal'></div>"
        }]
    });

    openBasicSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);

    if (modeGoal === 'edit') {
        switch(formDataGoal.type){
          case "condition_related":{
            stepsGoal = ["basic", "conditions", "confirm"];
            goalTypeGoal = "condition_related";

            $$("webixWizardConditionalHeaderMenuToolbarGoal").show();

            $$("webixWizardPageHeaderMenuToolbarGoal").hide();
            $$("webixWizardPageHeaderConditionalMenuToolbarGoal").hide();

            break;
          }
          case "page_related":{
            stepsGoal = ["basic", "placement", "confirm"];
            goalTypeGoal = "page_related";

            $$("webixWizardPageHeaderMenuToolbarGoal").show();

            $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();
            $$("webixWizardPageHeaderConditionalMenuToolbarGoal").hide();

            $$("wizardPlacementBtnGoal").enable();
            break;
          }
          case "page_related_with_condition":{
            stepsGoal = ["basic", "placement", "conditions", "confirm"];
            goalTypeGoal = "page_related_with_condition";

            $$("webixWizardPageHeaderMenuToolbarGoal").hide();
            $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();

            $$("webixWizardPageHeaderConditionalMenuToolbarGoal").show();

            $$("wizardPlacementBtnGoal").enable();

            break;
          }
          default: {
            console.log('This is a default case where goal type is not specified');
            break;
          }
        }

        openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
        openConfirmSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);

        if (userStateData && isSessional) {
            switch (userStateData.data.step) {
                case 'basic':
                    showViewGoal(true, false, false, false, modeGoal, formDataGoal, referenceOptionsGoal);
                    break;
                case 'placement':
                    showViewGoal(false, true, false, false, modeGoal, formDataGoal, referenceOptionsGoal);
                    break;
                case 'conditions':
                    showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
                    break;
                case 'confirm':
                    showViewGoal(false, false, false, true, modeGoal, formDataGoal, referenceOptionsGoal);
                    break;
                default:
                    showViewGoal(true, false, false, false, modeGoal, formDataGoal, referenceOptionsGoal);
                    break;

            }
        } else {
            showViewGoal(true, false, false, false, modeGoal, formDataGoal, referenceOptionsGoal);
        }
    }
    startGuidelinesInWizard({
        src: src,
        mode: modeGoal,
        add: 'addgoals',
        edit: 'editgoals',
    });
}

function openBasicSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal) {

    highLightCurrentGoal("basic");

    if (modeGoal === 'add') {
        showHeaderGoal();
    }

    isBasicGeneratedGoal = true;

    var __typeGoal = [{
        id: "page_related",
        value: "Page related"
    }, {
        id: "page_related_with_condition",
        value: "Page related with condition"
    }, {
        id: "condition_related",
        value: "Condition related"
    }];
    var __formStepOneGoal = [{
        view: "layout",
        height: 520,
        rows: [{
            view: "text",
            id: "tnameGoal",
            label: 'Name',
            name: "tnameGoal",
            // invalidMessage: "Name can not be empty"
        }, {
            view: "richselect",
            label: "Type",
            id: "ttypeGoal",
            css: "goalType",
            name: "ttypeGoal",
            value: __typeGoal[2].id,
            options: __typeGoal,
            labelAlign: 'left',
        }]
    }, {
        cols: [{}, {}, {
                view: "button",
                "type": "danger",
                value: "Back",
                width: 138,
                disabled: true,
            }, {
                view: "button",
                value: "Next",
                css: "orangeBtn",
                width: 138,
                click: function() {
                    nextGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                }
            }, {}, {},

        ]
    }];
    var __formStepOneRules = {
        "tnameGoal": webix.rules.isNotEmpty,
        "ttypeGoal": webix.rules.isNotEmpty,
    };

    webix.ui({
        container: "wizardbasicSectionGoal",
        id: "basicStepGoal",
        rows: [{
            cols: [{}, {
                view: "form",
                id: "basicFormGoal",
                complexData: true,
                elements: __formStepOneGoal,
                rules: __formStepOneRules,
                width: 600,
                borderless: true,
                margin: 3,
                elementsConfig: {
                    labelPosition: "top",
                    labelWidth: 140,
                    bottomPadding: 18
                },
            }, {}, ]
        }]
    });


    if (modeGoal === 'edit') {
        $$("basicFormGoal").setValues({
            tnameGoal: formDataGoal.name,
            ttypeGoal: formDataGoal.type,
            tid: formDataGoal._id,
        });

        if (activeStepGoal !== defaultStepsGoal[0]) {
            $$("basicStepGoal").hide();
        }

        $$("ttypeGoal").disable();

        if (userStateData && isSessional) {
            $$("basicFormGoal").setValues(userStateData.data.basicData);

            goalTypeGoal = $$("ttypeGoal").getValue();

            if (goalTypeGoal == "page_related_with_condition") {
                // stepsGoal = defaultStepsGoal;
                // if (stepsGoal[1] !== "placement") {
                //     stepsGoal.splice(1, 0, "placement");
                // }
                stepsGoal = ["basic", "placement", "conditions", "confirm"];
                goalTypeGoal = "page_related_with_condition";

                $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();
                $$("webixWizardPageHeaderMenuToolbarGoal").hide();
                $$("webixWizardPageHeaderConditionalMenuToolbarGoal").show();
            } else if (goalTypeGoal == "page_related") {
                stepsGoal = ["basic", "placement", "confirm"];
                goalTypeGoal = "page_related";

                $$("webixWizardPageHeaderMenuToolbarGoal").show();

                $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();
                $$("webixWizardPageHeaderConditionalMenuToolbarGoal").hide();
            }else {
                stepsGoal = ["basic", "conditions", "confirm"];
                goalTypeGoal = "condition_related";
                $$("webixWizardConditionalHeaderMenuToolbarGoal").show();
                $$("webixWizardPageHeaderMenuToolbarGoal").hide();
                $$("webixWizardPageHeaderConditionalMenuToolbarGoal").hide();
            }

            if (['basic'].indexOf(userStateData.data.step) === -1) {
                if (goalTypeGoal == "page_related_with_condition") {
                    activeStepGoal = stepsGoal[1];
                    $$("wizardPlacementBtnGoal").enable();
                    if (['placement'].indexOf(userStateData.data.step) !== -1) {
                        showViewGoal(false, true, false, false, modeGoal, formDataGoal, referenceOptionsGoal);

                    } else {
                        if (['basic', 'placement'].indexOf(userStateData.data.step) === -1) {
                            showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
                            // openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                        }
                    }
                } else {
                    showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
                    // openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                }

            }
        }

    } else {
        if (userStateData && isSessional) {
            $$("basicFormGoal").setValues(userStateData.data.basicData);

            goalTypeGoal = $$("ttypeGoal").getValue();

            if (goalTypeGoal == "page_related_with_condition") {
                // stepsGoal = defaultStepsGoal;
                // if (stepsGoal[1] !== "placement") {
                //     stepsGoal.splice(1, 0, "placement");
                // }
                stepsGoal = ["basic", "placement", "conditions", "confirm"];
                goalTypeGoal = "page_related_with_condition";
                $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();
                $$("webixWizardPageHeaderMenuToolbarGoal").hide();
                $$("webixWizardPageHeaderConditionalMenuToolbarGoal").show();
            } else if (goalTypeGoal == "page_related") {
                stepsGoal = ["basic", "placement", "confirm"];
                goalTypeGoal = "page_related";
                $$("webixWizardPageHeaderMenuToolbarGoal").show();
                $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();
                $$("webixWizardPageHeaderConditionalMenuToolbarGoal").hide();
            } else {
                stepsGoal = ["basic", "conditions", "confirm"];
                goalTypeGoal = "condition_related";
                $$("webixWizardConditionalHeaderMenuToolbarGoal").show();
                $$("webixWizardPageHeaderMenuToolbarGoal").hide();
                $$("webixWizardPageHeaderConditionalMenuToolbarGoal").hide();
            }

            if (['basic'].indexOf(userStateData.data.step) === -1) {
                if (goalTypeGoal == "page_related_with_condition") {
                    activeStepGoal = stepsGoal[1];
                    $$("wizardPlacementBtnGoal").enable();
                    if (['placement'].indexOf(userStateData.data.step) !== -1) {
                        showViewGoal(false, true, false, false, modeGoal, formDataGoal, referenceOptionsGoal);

                    } else {
                        if (['basic', 'placement'].indexOf(userStateData.data.step) === -1) {
                            showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
                            openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                        }
                    }
                } else {
                    showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
                    openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                }

            }
        }
    }

    $$("ttypeGoal").attachEvent("onChange", function(newv, oldv) {
        iFrameWin = undefined;
        if (newv == "page_related_with_condition") {
            stepsGoal = ["basic", "placement", "conditions", "confirm"];
            goalTypeGoal = "page_related_with_condition";
            $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();
            $$("webixWizardPageHeaderMenuToolbarGoal").hide();
            $$("webixWizardPageHeaderConditionalMenuToolbarGoal").show();
        } else if (newv == "page_related") {
            stepsGoal = ["basic", "placement", "confirm"];
            goalTypeGoal = "page_related";
            $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();
            $$("webixWizardPageHeaderMenuToolbarGoal").show();
            $$("webixWizardPageHeaderConditionalMenuToolbarGoal").hide();
        }else {
            stepsGoal = ["basic", "conditions", "confirm"];
            goalTypeGoal = "condition_related";
            $$("webixWizardPageHeaderMenuToolbarGoal").hide();
            $$("webixWizardConditionalHeaderMenuToolbarGoal").show();
            $$("webixWizardPageHeaderConditionalMenuToolbarGoal").hide();
        }

    });

    $$("wizardBasicBtnGoal").enable();
}

function openPlacementSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal) {

    var defaultDomainUrl = '';
    modeGoal === 'add' && highLightCurrentGoal("placement");

    isPlacementGeneratedGoal = true;

    webix.ui({
        container: "wizardPlacementSectionGoal",
        id: "placementStepGoal",
        height: config.styles.defaultHeight.getFileManagerHeight() - 65,
        css: "wizardPWSectionWebPageList",
        rows: [{
                view: "layout",
                rows: [{
                    view: "iframe",
                    id: "placementWindowIframeContainerGoal",
                    css: "placementWindowIframeContainerGoal",
                    src: "/component/placement-window?page=goal&source=goal",
                }]
            },

        ]

    }).show();

    if(modeGoal == 'add'){
      webix.ajax().get('/data/domains/default', {ajax: true
      }, function(response, xml, xhr) {
        response = JSON.parse(response);
        defaultDomainUrl = response.defaultDomainUrl;
      });
      $$("placementWindowIframeContainerGoal").attachEvent("onAfterLoad", function(sid) {
        var iframesPlacementWindowGoal = $("iframe[src*='component/placement-window']");
        iframesPlacementWindowGoal.get(0).contentWindow.postMessage(JSON.stringify({
            t: "set-add-goal-url",
            data: {
                fullURL: defaultDomainUrl
            }
        }), getBaseUrl() + "/component/placement-window?page=goal&source=goal");
      });
    }

    if (modeGoal === 'edit') {

        // if (activeStepGoal !== defaultStepsGoal[1]) {
        // stepsGoal variable is updated per goal type
        if (activeStepGoal !== stepsGoal[1]) {
            $("#wizardPlacementSectionGoal").hide();
        }

        if (userStateData && isSessional) {
            $$("basicFormGoal").setValues(userStateData.data.basicData);

            if (['basic', 'placement'].indexOf(userStateData.data.step) === -1) {
                showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
                openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            }
        }

    } else {
        if (userStateData && isSessional) {
            $$("basicFormGoal").setValues(userStateData.data.basicData);

            if (['basic', 'placement'].indexOf(userStateData.data.step) === -1) {
                showViewGoal(false, false, true, false, modeGoal, formDataGoal, referenceOptionsGoal);
                openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            }
        }
    }

    $$("placementWindowIframeContainerGoal").attachEvent("onAfterLoad", function(sid) {
        var iframesPlacementWindowGoal = $("iframe[src*='component/placement-window']");
        if (modeGoal === 'edit') {
            // PWUrlOptions = formDataGoal.url_config;
        }
        // PWUrlOptions.t = "set-stored-values";
        var fullURL = '';
        var page_element = {};

        if (formDataGoal !== undefined) {
            fullURL = formDataGoal.page_element.originalUrl;
            page_element = formDataGoal.page_element;
        }
        var pw = {
            mode: modeGoal,
            data: false
        };
        var currentFolderGoal;

        if (referenceOptionsGoal !== undefined) {
            currentFolderGoal = referenceOptionsGoal.currentFolder;
        } else {
            currentFolderGoal = folderFileManagerGoal ? folderFileManagerGoal.getCurrentFolder() : '';
        }

        if (isSessional && userStateData) {
            if (userStateData.data.placementData) {
                fullURL = userStateData.data.placementData.url;
                pw = userStateData.data.placementData.data.pw;
                currentFolderGoal = userStateData.data.placementData.data.currentFolder;
                page_element = userStateData.data.placementData.data.formData;
                temp = userStateData.data.placementData.data.temp;
            }

        }

        iframesPlacementWindowGoal.get(0).contentWindow.postMessage(JSON.stringify({
            t: "set-stored-values",
            data: {
                fullURL: fullURL,
                pw: pw,
                source: 'goal',
                formData: page_element,
                currentFolder: currentFolderGoal,
                temp: !(isSessional && userStateData && userStateData.data.placementData) ? {
                    mode: modeGoal,
                    formData: formDataGoal,
                    referenceOptions: referenceOptionsGoal
                } : temp
            }
        }), getBaseUrl() + "/component/placement-window?page=goal&source=goal");
    });

    $$("wizardPlacementBtnGoal").enable();
    hideHeaderGoal();
}

function openPlacementEditRecordSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal) {
    isPlacementEditRecordGeneratedGoal = true;
    modeGoal === 'add' && highLightCurrentGoal("placement_edit_record");

    if (modeGoal === 'add') {
        showHeaderGoal();
    }

    var tempPlacementEditRecordData;
    if (modeGoal === "add") {
        tempPlacementEditRecordData = PWIFrameWin.allFormDataPlacementWindowGoal.data;
    } else {
        if (PWIFrameWin !== undefined) {
            tempPlacementEditRecordData = PWIFrameWin.allFormDataPlacementWindowGoal.data;
        } else {
            tempPlacementEditRecordData = formDataGoal.url_config;
        }
    }

    var __formGoal = [{
        view: "layout",
        height: 520,
        rows: [{
            view: "text",
            label: "URL",
            id: "urlPWGoal",
            name: "urlPWGoal",
            readonly: true,
            value: tempPlacementEditRecordData.url
        }, {
            view: "checkbox",
            label: 'Include www',
            name: "inc_wwwPWGoal",
            id: "inc_wwwPWGoal",
            value: tempPlacementEditRecordData.inc_www,
        }, {
            view: "checkbox",
            label: 'Include http https:',
            name: "inc_http_httpsPWGoal",
            id: "inc_http_httpsPWGoal",
            value: tempPlacementEditRecordData.inc_http_https,
        }, {
            view: "richselect",
            label: "URL Options",
            id: "url_optionPWGoal",
            name: "url_optionPWGoal",
            value: tempPlacementEditRecordData.url_option,
            options: [{
                id: 'exactMatch',
                value: 'Exact Match'
            }, {
                id: 'allowParams',
                value: 'Allow Parameters'
            }, {
                id: 'startsWith',
                value: 'Starts With'
            }, ],
            labelAlign: 'left',
        }]
    }, {
        cols: [{}, {}, {
            view: "button",
            "type": "danger",
            value: "Back",
            click: function() {
                prevGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            },
            width: 138,
        }, {
            view: "button",
            value: "Next",
            css: "orangeBtn",
            click: function() {
                nextGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            },
            width: 138,
        }, {}, {}]
    }];
    var __formRulesGoal = {
        "urlPWGoal": webix.rules.isNotEmpty,
    };
    webix.ui({
        container: "wizardPlacementEditRecordSectionGoal",
        id: "placementEditRecordStepGoal",
        rows: [{

        }, {
            cols: [{}, {
                view: "form",
                id: "placementEditRecordFormGoal",
                complexData: true,
                elements: __formGoal,
                rules: __formRulesGoal,
                width: 600,
                borderless: true,
                margin: 3,
                elementsConfig: {
                    labelPosition: "top",
                    labelWidth: 140,
                    bottomPadding: 18
                },
                animate: {
                    direction: "top"
                },
            }, {}, ]
        }]
    });
    if (modeGoal === 'edit') {
        // $$("placementEditRecordFormGoal").setValues({
        //   "urlPWGoal": formDataGoal.url_config.url,
        //   "urlPWGoal": formDataGoal.url_config.inc_http_https,
        //   "urlPWGoal": formDataGoal.url_config.inc_www,
        //   "urlPWGoal": formDataGoal.url_config.url_option,
        // });


        if (activeStepGoal !== defaultStepsGoal[2]) {
            $$("placementEditRecordStepGoal").hide();
        }

    }

    $$("inc_wwwPWGoal").attachEvent("onChange", function(sid) {
        PWIFrameWin.allFormDataPlacementWindowGoal.data.inc_www = $$("inc_wwwPWGoal").getValue();
    });
    $$("inc_http_httpsPWGoal").attachEvent("onChange", function(sid) {
        PWIFrameWin.allFormDataPlacementWindowGoal.data.inc_http_https = $$("inc_http_httpsPWGoal").getValue();
    });
    $$("url_optionPWGoal").attachEvent("onChange", function(sid) {
        PWIFrameWin.allFormDataPlacementWindowGoal.data.url_option = $$("url_optionPWGoal").getValue();
    });


    $$("wizardPlacementEditRecordBtnGoal").enable();
}

function openConditionsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal) {

    modeGoal === 'add' && highLightCurrentGoal("conditions");
    if (modeGoal === 'add') {
        showHeaderGoal();
    }
    isConditionsGeneratedGoal = true;

    webix.ui({
        container: "wizardConditionSectionGoal",
        id: "conditionsStepGoal",
        rows: [{
            view: "layout",
            height: 540,
            rows: [{
                view: "iframe",
                id: "conditionBuilderIframeGoal",
                src: conditionBuilderUrlGoal,
            }]
        }, {
            cols: [{}, {}, {}, {
                view: "button",
                "type": "danger",
                value: "Back",
                width: 138,
                click: function() {
                    prevGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                }
            }, {
                view: "button",
                value: "Next",
                css: "orangeBtn",
                width: 138,
                click: function() {
                    nextGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                }
            }, {}, {}, {}]
        }]

    });

    if (modeGoal === 'edit') {
        if (goalTypeGoal == "condition_related") {
            if (activeStepGoal !== defaultStepsGoal[1]) {
                $("#wizardConditionSectionGoal").hide();
            }
        } else {
            if (activeStepGoal !== defaultStepsGoal[3]) {
                $("#wizardConditionSectionGoal").hide();
            }
        }

        if (userStateData && isSessional) {
            if (['basic', 'placement', 'conditions'].indexOf(userStateData.data.step) === -1) {
                showViewGoal(false, false, false, true, modeGoal, formDataGoal, referenceOptionsGoal);
                // openConfirmSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            }
        }
    } else {
        if (userStateData && isSessional) {
            if (['basic', 'placement', 'conditions'].indexOf(userStateData.data.step) === -1) {
                showViewGoal(false, false, false, true, modeGoal, formDataGoal, referenceOptionsGoal);
                openConfirmSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            }
        }
    }

    $$("conditionBuilderIframeGoal").attachEvent("onAfterLoad", function(sid) {
        if (isConditionsGeneratedGoal) {}
        tempConditionGoal = iFrameWin.returnCondition.logical;
        // var conditionBuilderIframeGoal = $("iframe");
        var conditionBuilderIframeGoal = $("iframe[src*='component/profile-properties-template']");
        if (modeGoal === 'edit') {
            tempConditionGoal = formDataGoal.condition;
        }
        iFrameIndexGoal = 0;
        if (goalTypeGoal == "page_related_with_condition") {
            iFrameIndexGoal = 1;
        }
        if (modeGoal === 'edit') {
            iFrameIndexGoal = 0;
        }
        if (isSessional && userStateData) {
            if (userStateData.data.conditionsData) {
                tempConditionGoal = userStateData.data.conditionsData;
            }
            if (['confirm'].indexOf(userStateData.data.step) !== -1) {
                if (userStateData.data.confirmData.conditionGoal) {
                    tempConditionGoal = userStateData.data.confirmData.conditionGoal;
                }
            }
        }
        conditionBuilderIframeGoal.get(0).contentWindow.postMessage(tempConditionGoal, "*");
    });


    if (goalTypeGoal == "condition_related") {
        $$("wizardConditionsGoal").enable();
    } else {
        $$("wizardConditionsPageGoal").enable();
    }


}

function openPointsSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal) {
    isPointsGeneratedGoal = true;

    modeGoal === 'add' && highLightCurrentGoal("points");
    if (modeGoal === 'add') {
        showHeaderGoal();
    }
    var __formGoal = [{
        view: "layout",
        height: 520,
        rows: [{
            view: "text",
            id: "tpointsGoal",
            label: 'Points',
            name: "tpointsGoal",
            value: "0",
        }]
    }, {
        cols: [{}, {}, {
            view: "button",
            "type": "danger",
            value: "Back",
            click: function() {
                prevGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            },
            width: 138,
        }, {
            view: "button",
            value: "Next",
            css: "orangeBtn",
            click: function() {
                nextGoal(modeGoal, formDataGoal, referenceOptionsGoal);
            },
            width: 138,
        }, {}, {}]
    }];
    var __formRulesGoal = {
        "tpointsGoal": webix.rules.isNumber,
    };
    webix.ui({
        container: "wizardPointsSectionGoal",
        id: "pointsStepGoal",
        rows: [{

        }, {
            cols: [{}, {
                view: "form",
                id: "pointsFormGoal",
                complexData: true,
                elements: __formGoal,
                rules: __formRulesGoal,
                width: 600,
                borderless: true,
                margin: 3,
                elementsConfig: {
                    labelPosition: "top",
                    labelWidth: 140,
                    bottomPadding: 18
                },
                animate: {
                    direction: "top"
                },
            }, {}, ]
        }]
    });
    if (modeGoal === 'edit') {
        $$("pointsFormGoal").setValues({
            tpointsGoal: formDataGoal.point,
        });

        if (goalTypeGoal == "condition_related") {
            if (activeStepGoal !== defaultStepsGoal[2]) {
                $$("pointsStepGoal").hide();
            }
        } else {
            if (activeStepGoal !== defaultStepsGoal[4]) {
                $$("pointsStepGoal").hide();
            }
        }

    }
    if (goalTypeGoal == "condition_related") {
        $$("wizardPointsBtnGoal").enable();
    } else {
        $$("wizardPointsPageBtnGoal").enable();
    }
}

function openConfirmSectionGoal(modeGoal, formDataGoal, referenceOptionsGoal) {


    $("#wizardConfirmSectionGoal").html('');

    modeGoal === 'add' && highLightCurrentGoal("confirm");

    if (modeGoal === 'add') {
        showHeaderGoal();
    }

    isConfirmGeneratedGoal = true;
    var winHeightConfirmGoal = 540;
    allFormDataGoal.basic = $$("basicFormGoal").getValues();
    if (allFormDataGoal.basic.ttypeGoal == 'page_related_with_condition') {
        // winHeightConfirmGoal = 800;
        allFormDataGoal.placement = {};
        allFormDataGoal.placement.url = "";
        if (modeGoal === "add") {
            if (PWIFrameWin !== undefined) {
                allFormDataGoal.placement = PWIFrameWin.allFormDataPlacementWindowGoal.data;
            }
        } else {
            if (PWIFrameWin !== undefined) {
                allFormDataGoal.placement = PWIFrameWin.allFormDataPlacementWindowGoal.data;
            } else {
                allFormDataGoal.placement = formDataGoal.url_config;
            }
        }
        // console.log("allFormDataGoal.placement", allFormDataGoal.placement);
    }

    // console.log("iFrameWin", iFrameWin.returnCondition);
    if (iFrameWin !== undefined) {
        if (iFrameWin.returnCondition.logical === 'if (){|}') {
            allFormDataGoal.condition = '';
        } else {
            allFormDataGoal.condition = iFrameWin.returnCondition.logical;
        }
    }else{
      allFormDataGoal.condition = '';
    }
    allFormDataGoal.points = {};
    if (modeGoal !== 'edit') {
        allFormDataGoal.points.tpointsGoal = 0;
    } else {
        allFormDataGoal.points.tpointsGoal = formDataGoal.point;
    }

    var _form = [{
        view: "layout",
        rows: [{
            view: "text",
            id: "nameGoal",
            label: 'Name',
            name: "nameGoal",
            // disabled: true,
            value: allFormDataGoal.basic.tnameGoal
        }, {
            view: "text",
            id: "typeGoal",
            label: 'Type',
            name: "typeGoal",
            readonly: true,
            value: allFormDataGoal.basic.ttypeGoal
        }, {
            view: "textarea",
            label: 'Condition',
            name: "conditionGoal",
            id: "conditionGoal",
            height: 200,
            readonly: true,
            value: allFormDataGoal.condition
        }, {
            view: "text",
            id: "pointsGoal",
            label: 'Points',
            name: "pointsGoal",
            // disabled: true,
            value: ""+allFormDataGoal.points.tpointsGoal+""
        }]
    }, ];
    var __formRules = {
        "nameGoal": webix.rules.isNotEmpty,
        "typeGoal": webix.rules.isNotEmpty,
        "pointsGoal": webix.rules.isNumber,
    };
    webix.ui({
        container: "wizardConfirmSectionGoal",
        id: "confirmStepGoal",
        rows: [{
            cols: [{}, {
                view: "form",
                id: "confirmFormGoal",
                height: winHeightConfirmGoal,
                // scroll: true,
                complexData: true,
                elements: _form,
                rules: __formRules,
                width: 600,
                borderless: true,
                margin: 3,
                elementsConfig: {
                    labelPosition: "top",
                    labelWidth: 140,
                    bottomPadding: 18
                },
            }, {}, ]
        }, {
            cols: [{}, {}, {
                view: "button",
                label: "Back",
                type: "danger",
                click: function() {
                    prevGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                },
                width: 138,
            }, {
                view: "button",
                label: "Confirm and save",
                css: "confirmSave orangeBtn",
                width: 138,
                click: function() {
                    var btnCtrl = this;
                    btnCtrl.disable();
                    var url;
                    url = monolop_api_base_url + '/api/goals/create';
                    if (modeGoal === 'edit') {
                        url = monolop_api_base_url + '/api/goals/update'
                    }
                    var form = $$("confirmFormGoal");
                    // adding parent folder id
                    if (form.validate()) {
                        var currentFolderGoal;

                        if (referenceOptionsGoal !== undefined) {
                            currentFolderGoal = referenceOptionsGoal.currentFolder;
                        } else {
                            currentFolderGoal = folderFileManagerGoal ? folderFileManagerGoal.getCurrentFolder() : '';
                        }
                        if (modeGoal === 'edit') {
                            form.setValues({
                                source: currentFolderGoal,
                                _id: allFormDataGoal.basic.tid,
                            }, true);
                        } else {
                            form.setValues({
                                source: currentFolderGoal,
                            }, true);
                        }
                        var formValues = form.getValues();
                        formValues.ajax = true;

                        if(referenceOptionsGoal){
                          if(referenceOptionsGoal.reference == 'experiment'){
                            formValues.embedded = 1;
                          }
                        }

                        webix.ajax().post(url, formValues, {
                            error: function(text, data, XmlHttpRequest) {
                                btnCtrl.enable();
                                alert("error");
                            },
                            success: function(text, data, XmlHttpRequest) {
                                btnCtrl.enable();
                                var response = JSON.parse(text);
                                if (response.success == true) {
                                    if (referenceOptionsGoal !== undefined) {
                                        referenceOptionsGoal.newGoal = response.content.goal;
                                        config.currentElementID = 'webixWizardHeaderMenuExperiment';
                                        retrurnBackToExperimentGoal(modeGoal, formDataGoal, referenceOptionsGoal);
                                    } else {
                                        config.currentElementID = 'webixFilemanagerGoal';
                                        openLayoutGoal();
                                    }
                                    webix.message(response.msg);
                                }

                            }
                        });
                    }else{
                      btnCtrl.enable();
                      alert("Please fill all value to proceed");
                    }
                    isSessional = false;
                    userStateData = '';
                }
            }, {}, {}]
        }]
    });

    // forcefully set computed value
    // document.getElementById('pointsGoal').value = allFormDataGoal.points.tpointsGoal;
    $$('pointsGoal').setValue(""+allFormDataGoal.points.tpointsGoal+"");

    if (modeGoal === 'edit') {
        if (goalTypeGoal == "condition_related") {
            if (activeStepGoal !== defaultStepsGoal[3]) {
                $$("confirmStepGoal").hide();
            }
        } else {
            if (activeStepGoal !== defaultStepsGoal[5]) {
                $$("confirmStepGoal").hide();
            }
        }

        if (userStateData && isSessional) {
            if (userStateData.data.confirmData) {
                $$("confirmFormGoal").setValues(userStateData.data.confirmData);
            }
        }

    } else {
        guidelines.startSubGuideline('save');
        if (userStateData && isSessional) {
            if (userStateData.data.confirmData) {
                $$("confirmFormGoal").setValues(userStateData.data.confirmData);
            }
        }
    }
    if (goalTypeGoal == "condition_related") {
        $$("wizardConfirmBtnGoal").enable();
    } else {
        if (!currentPageElementEditGoal) {
            if (userStateData) {
                if (userStateData.data.page_element) {
                    currentPageElementEditGoal = userStateData.data.page_element;
                }
            }
        }
        $$("confirmFormGoal").setValues({
            nameGoal: $$("nameGoal").getValue(),
            typeGoal: $$("typeGoal").getValue(),
            conditionGoal: $$("conditionGoal").getValue(),
            pointsGoal: $$("pointsGoal").getValue(),
            page_element_id: currentPageElementEditGoal ? currentPageElementEditGoal._id : "",
            // placement_urlGoal: $$("placement_urlGoal").getValue(),
            // placement_url_optionGoal: allFormDataGoal.placement.url_option,
            // placement_inc_wwwGoal: allFormDataGoal.placement.inc_www,
            // placement_inc_http_httpsGoal: allFormDataGoal.placement.inc_http_https,
            // placement_reg_exGoal: allFormDataGoal.placement.reg_ex,
        });

        $$("wizardConfirmPageBtnGoal").enable();
    }

}

function showHeaderGoal() {
    $('div[view_id="webixWizardPageHeaderMenuToolbarGoal"]').show();
    $('div[view_id="webixWizardPageHeaderConditionalMenuToolbarGoal"]').show();
    $("#viewHeader").show();
}

function hideHeaderGoal() {
    $('div[view_id="webixWizardPageHeaderMenuToolbarGoal"]').hide();
    $('div[view_id="webixWizardPageHeaderConditionalMenuToolbarGoal"]').hide();
    $("#viewHeader").hide();
}

function retrurnBackToExperimentGoal(modeGoal, formDataGoal, referenceOptionsGoal) {
    $("#goalsRefExperiment").remove();
    referenceOptionsGoal.isGoalGenerated = true;
    experimentListConfig.replicateGoal = false;
    experimentListConfig.openSegment(referenceOptionsGoal.oldFormData[0], referenceOptionsGoal.oldFormData[1], referenceOptionsGoal);
    $("#innerContent").show();
}

function refreshManagerGoal() {
    folderFileManagerGoal.clearAll();
    folderFileManagerGoal.load(monolop_api_base_url + "/api/goals");
}


var postMessageListener = function(event) {
    var data = JSON.parse(event.data);
    switch (data.t) {
        case "pw-fullscreen-full":
            $("#wizardPlacementSectionGoal").addClass("fullscreenModeMainCont");
            break;
        case "pw-fullscreen-exit":
            $("#wizardPlacementSectionGoal").removeClass("fullscreenModeMainCont");
            break;
        case "page-published-" + page:
            if (data.source === 'goal') {
                // webix.message(data.message);
                currentPageElementEditGoal = data.pageElement;
                // console.log(data);
                nextGoal(data.temp.mode, data.temp.formData, data.temp.referenceOptions);
            }
            break;
        default:
            break;
    }
};
// insert postmessageListener ;
if (window.addEventListener) {
    addEventListener("message", postMessageListener, false);
} else {
    attachEvent("onmessage", postMessageListener);
}
