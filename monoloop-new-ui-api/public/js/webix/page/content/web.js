"use strict";
(function(global){
  var msg; // default variable for user messages
  var isSessional = false;
  var webPageListConfig = {
    folderManager: false,
    currentStepInfo: {},
    msg: "",
    page: 'web',
    placementWindow: {},
    apis: {
      show: monolop_api_base_url+"/api/content/web/show?ajax=true",
      list: monolop_api_base_url+"/api/content/web?ajax=true",
      create: monolop_api_base_url+"/api/content/web/create?ajax=true",
      update: monolop_api_base_url+"/api/content/web/update?ajax=true",
      update_status: monolop_api_base_url+"/api/content/web/update-status?ajax=true",
      change_folder: monolop_api_base_url+"/api/content/web/change-folder?ajax=true",
      delete: monolop_api_base_url+"/api/content/web/delete?ajax=true",
      files: monolop_api_base_url+"/api/content/web/folder?ajax=true",
    },
    urlOptions: [
      {id: 'exactMatch', value: 'Exact Match'},
      {id: 'allowParams', value: 'Allow Parameters'},
      {id: 'startsWith', value: 'Starts With'},
    ],
    getUrlOption: function(id){
        id = id || 0;
        switch (id) {
          case 0: return "exactMatch"; break;
          case 1: return "allowParams"; break;
          case 2: return "startsWith"; break;
          default: return "exactMatch"; break;
        }
    },
    formElement: {
      id: 'pageElementIdWebPageList',
    },
    allFormData: {
      fullURL: "",
      basic: false,
      pw: false,
      confirm: false,
    },
    placementIframeSrc: "/component/placement-window",
    defaultSteps: [],
    steps: [],
    activeStep: "",
    stepsGenerated: {
      basic: false,
      pw: false,
      confirm: false
    },
    containerId: "innerContent",

    openWizard: function (mode, formData, type, src) {
      if($('#floatingContainer').length > 0){
        $('#floatingContainer').remove();
      }

      var f = window.location.hash;
      if(mode === 'add'){
        window.history.pushState(config.addcontentWeb.headerName, config.addcontentWeb.headerName, config.addcontentWeb.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);
      } else {
        window.history.pushState(config.editcontentWeb.headerName, config.editcontentWeb.headerName, config.editcontentWeb.link + '?id=' + formData._id + ($('body').hasClass('hidenav') ? '&nav=1' : '') + f);
      }

      manageLayouts.pw();
      $("#viewHeader").hide();

      var containerId = webPageListConfig.containerId;

      $("#"+containerId).html("");

      var wizard = document.createElement('div');
      wizard.id = 'webixWizardParentIdWebPageList';
      // wizard.style.minHeight = '680px';
      document.getElementById(containerId).appendChild(wizard);

      if(userStateData){
        isSessional = userStateData.section === 'web';
      }

      webix.ui({
        container: "webixWizardParentIdWebPageList",
        height: config.styles.defaultHeight.getFileManagerHeight(),
        id: "webixWizardHeaderMenuWebPageList",
        css: "webixWizardHeaderMenuWebPageList",
        rows: [{
          template: "<div id='wizardPWSectionWebPageList'></div>"
        }]
      });
      webPageListConfig.openPlacementWindow(mode, formData, type);
      if (mode === 'edit') {

      }
      startGuidelinesInWizard({
        src: src,
        mode: mode,
        add: 'addcontentWeb',
        edit: 'editcontentWeb',
      });
    },
    openPlacementWindow: function (mode, formData, type) {
      var defaultDomainUrl;
      webix.ajax().get('/data/domains/default', {ajax:true
      }, function(response, xml, xhr) {
        //  response
        response = JSON.parse(response);
        defaultDomainUrl = response.defaultDomainUrl;

        if (mode === 'edit') {
          webPageListConfig.allFormData.basic = {};
          webPageListConfig.allFormData.basic.tid = formData._id;
          webPageListConfig.allFormData.fullURL = formData.originalUrl;
        } else {
          webPageListConfig.allFormData.fullURL = defaultDomainUrl;
        }
        if(userStateData){
          isSessional = userStateData.section === 'web';
        }
        if (isSessional && userStateData) {
          if(userStateData.data.url){
            webPageListConfig.allFormData.fullURL = userStateData.data.url;
          }
        }
        if(window.placement_url){
          webPageListConfig.allFormData.fullURL = window.placement_url;
        }


        var placementWindowUrl = monolop_base_url + webPageListConfig.placementIframeSrc + '?url=' + webPageListConfig.allFormData.fullURL + '&ver=' + (new Date()).getTime() + Math.floor(Math.random() * 1000000);

        if(window.placement_url){
          placementWindowUrl = placementWindowUrl + '&new_url=1' ;
        }

        webix.ui({
          container: "wizardPWSectionWebPageList",
          id: "PWStepWebPageList",
          height: config.styles.defaultHeight.getFileManagerHeight()-65,
          css: "wizardPWSectionWebPageList",
          rows: [{
            view: "layout",
            // height: 780,
            rows: [{
              view: "iframe",
              id: "PWIframeWebPageList",
              css: "PWIframeWebPageList",
              src: placementWindowUrl,
            }]
          }]

        });

        $$("PWIframeWebPageList").attachEvent("onAfterLoad", function(sid) {
          var PWIframeWebPageList = $("iframe[src*='component/placement-window']");

          var source = 'default';
          if (mode === 'edit') {
            if(formData.Experiment !== undefined){
              source = 'experiment';
            }
          }

          if(isSessional && userStateData){

            mode = userStateData.data.type ? userStateData.data.type : mode;
            type = userStateData.data.step ? userStateData.data.step : type;
            mode = userStateData.data.type ? userStateData.data.type : mode;
          }

          var pw = {};
          pw.mode = mode;
          PWIframeWebPageList.get(0).contentWindow.postMessage(JSON.stringify({t: "posted-from-monoloopui", data: {
              fullURL: webPageListConfig.allFormData.fullURL,
              source: source,
              formData: formData,
              pw: pw,
              type: type,
              page: page,
              mode: mode,
              currentFolder: webPageListConfig.folderManager ? webPageListConfig.folderManager.getCurrentFolder() :  window.location.hash.substr(3)
            }
          }), placementWindowUrl);
          // var width;
          // webix.event(window, "resize", function(){
          //   if($("#wizardPWSectionWebPageList").hasClass("fullscreenModeMainCont")){
          //     width = window.innerWidth-20;
          //   }else{
          //     width = window.innerWidth-197;
          //   }
          //   PWIframeWebPageList.get(0).contentWindow.postMessage(JSON.stringify({t: "resize-toolbar",data:{width:width}}),placementWindowUrl)
          //});
        });
      });
    },
    updateFile: function (id, type, src) {
      config.currentElementID = 'webixWizardHeaderMenuWebPageList';
      $('.header-logo').css('opacity', '0');
      // $("#main").css({'width': $('body').width() - 183 + 'px'});
      // $('#nav .menu .webix_tree_close, #nav .menu .webix_tree_open, .menu .webix_tree_item .fa-angle-right').animate({opacity: '1'});
      // $('#wrapper .sidebar').animate({width: '182px'});
      // $('#nav .menu').animate({width: '180px'});
      // $('#nav .menu  .webix_tree_branch_1.expand').css({'border-right': '5px solid #0F9FD5'});
      var url = webPageListConfig.apis.show;
      if(userStateData){
        isSessional = userStateData.section === 'web';
      }
      // retreiving form data and setting into the form
      var formData;
      webix.ajax().get(url, {
        _id: id
      }, function(response, xml, xhr) {
        //response
        formData = JSON.parse(response);
        setTimeout(function(){
          webPageListConfig.openWizard('edit', formData, type, src);
        }, 1000);
      });
    },
    deleteFile: function (id) {
      webix.confirm({
        text: "Do you want to delete?",
        ok: "Yes",
        cancel: "No",
        callback: function(result) {
          if (result) {
            webix.ajax().post(webPageListConfig.apis.delete, {
              _id: id
            }, {
              error: function(text, data, XmlHttpRequest) {
                alert("error");
              },
              success: function(text, data, XmlHttpRequest) {
                var response = JSON.parse(text);
                if (response.success == true) {
                  webPageListConfig.folderManager.deleteFile(id);
                }
              }
            });
          } else {
          }
        }
      });


    },
    updateHidden: function (params) {
      // var params = {source: id, action: action, hidden: hidden};


      if (params.hidden == 1) {
        var message = webix.message("deactivating...");
      } else if (params.hidden == 0) {
        var message = webix.message("activating...");
      }

      webix.ajax().post(webPageListConfig.apis.update_status, params, {
        error: function(text, data, XmlHttpRequest) {
          alert("error");
        },
        success: function(text, data, XmlHttpRequest) {
          var response = JSON.parse(text);
          if (response.success == true && params.action == "update_hidden") {
            var element = document.getElementById("updateHidden_" + params._id);
            if (response.pageElement.hidden == "0") {
              element.innerHTML = "active";
              element.style.color = "green";
              element.onclick = function() {
                params.hidden = 1;
                webPageListConfig.updateHidden(params);
              };
              webix.message("Page has been activated.");
            } else if (response.pageElement.hidden == "1") {
              element.innerHTML = "inactive";
              element.style.color = "red";
              element.onclick = function() {
                params.hidden = 0;
                webPageListConfig.updateHidden(params);
              };
              webix.message("Page has been deactivated.");
            }
            webPageListConfig.refreshManager();
            webix.message.hide(message);
          }
        }
      });
    },
    refreshManager: function () {
      webPageListConfig.folderManager.clearAll();
      webPageListConfig.folderManager.load(webPageListConfig.apis.list);
    },
    fab: {
      actionButtonTypes: {
    		type: "create",
    		setType: function(t){
    			t = t || "create";
    			webPageListConfig.fab.actionButtonTypes.type = t;
    		},
    		fabOptions: false,
    		create: {
    			options: [
            // {
    				// 	label: "This is to test",
    				// 	className: "segmentBuilder inline",
            //   callback: function(){
            //     webix.alert("This is to test......");
            //   }
  				  // }
    			]
    		},
    	},
      fabOptions: false,
      primary: {
        primaryBtnLabelCallback: function(actionButton, floatingTextBG){
          actionButton.className += " createPage inline";
          floatingTextBG.textContent = "Create page";
          actionButton.onclick = function(){
              config.currentElementID = 'webixWizardHeaderMenuWebPageList';
              $('.header-logo').css('opacity', '0');
              // $("#main").css({'width': $('body').width() - 183 + 'px'});
              // $('#nav .menu .webix_tree_close, #nav .menu .webix_tree_open, .menu .webix_tree_item .fa-angle-right').animate({opacity: '1'});
              // $('#wrapper .sidebar').animate({width: '182px'});
              // $('#nav .menu').animate({width: '180px'});
              // $('#nav .menu  .webix_tree_branch_1.expand').css({'border-right': '5px solid #0F9FD5'});
              guidelines.removeHangedTooltips();
              setTimeout(function(){
                webPageListConfig.openWizard('add');
              }, 1000);
          };
        },
      }
      // var PWIframeWebPageList = $("iframe[src*='component/placement-window']");
      //     PWIframeWebPageList.get(0).contentWindow.postMessage(JSON.stringify({t: "resize-toolbar",data:{width:window.innerWidth-20}}),'*')
      // var PWIframeWebPageList = $("iframe[src*='component/placement-window']");
      //     PWIframeWebPageList.get(0).contentWindow.postMessage(JSON.stringify({t: "resize-toolbar",data:{width:window.innerWidth-197}}),'*')


    },
    postMessageListener: function(event) {
      var data = JSON.parse(event.data);
      switch (data.t) {
        case "pw-fullscreen-full":
          $("#wizardPWSectionWebPageList").addClass("fullscreenModeMainCont");
          break;
        case "pw-fullscreen-exit":
          $("#wizardPWSectionWebPageList").removeClass("fullscreenModeMainCont");
          break;
        case "page-published-" + webPageListConfig.page:
          // if(data.source === 'default'){
            // webix.message(data.message);
            openLayoutWeb();
            // console.log("hello world!");
          // }
          break;
        case "set-active-step-sessional":
          webPageListConfig.currentStepInfo.step = data.step;
          webPageListConfig.currentStepInfo.url = data.url;
          break;
        default:
          break;
      }
    }
  };

  function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
      tokens,
      re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
      params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
  };

  function openLayoutWeb()
  {
    var params = getQueryParams(document.location + '');
    var f = window.location.hash;
    window.history.pushState(config.contentWeb.headerName, config.contentWeb.headerName, config.contentWeb.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);

    if(params.url){
      webPageListConfig.apis.list += '&url=' + params.url ;
      webPageListConfig.apis.files += '&url=' + params.url ;
    }

    $("#viewHeader").show();
    $("#innerContent").html("");

    var formElement = document.createElement('div');
    formElement.id = webPageListConfig.formElement.id;
    formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
    document.getElementById("innerContent").appendChild(formElement);

    // webix.ready(function() {

      webPageListConfig.folderManager = webix.ui({
        container: webPageListConfig.formElement.id,
        view: "filemanager",
        url: webPageListConfig.apis.list, // loading data from the URL
        id: "webixFilemanagerWebPageList",
        filterMode:{
          showSubItems:false,
          openParents:false
        },
        mode: "table", // specify mode selected by default
        modes: ["files", "table", "custom"], // all available modes including a new mode
        // save and handle all the menu actions from here,
        // disable editing on double-click,

        handlers: {
          "upload": webPageListConfig.apis.create,
          // "download": "data/saving.php",
          // "copy": "data/saving.php",
          "move": monolop_api_base_url+"/api/folders/move",
          "remove": monolop_api_base_url+"/api/folders/delete",
          "rename": monolop_api_base_url+"/api/folders/update",
          "create": monolop_api_base_url+"/api/folders/create",
          "files": webPageListConfig.apis.files,
        },
        structure: {
          // specify the view of the new mode
          "custom": {
            view: "list",
            template: function(obj, common) {
              return common.templateIcon(obj, common) + obj.value;
            },
            // select: "multiselect",
            editable: false,
            editaction: false,
            editor: "text",
            editValue: "value",
            drag: true,
            navigation: false,
            tabFocus: false,
            onContext: {}
          }
        },
        on: {
          "onViewInit": function(name, config) {
            if (name == "table" || name == "files") {
              // disable multi-selection for "table" and "files" views
              // config.select = true;
              if (name == "table") {
                // disable editing on double-click
                config.editaction = false;
                // an array with columns configuration
                var columns = config.columns;
                //  disabling columns date, type, size
                columns.splice(1, 3);
                // configuration of a new column inWWW
                var inWWWColumn = {
                  id: "inWWWColumn",
                  header: "WWW",
                  fillspace: 1,
                  template: function(obj, common){
                    return obj.inWWW ? 'Yes' : 'No';
                  },
                };

                var inHTTP_HTTPSColumn = {
                  id: "inHTTP_HTTPS",
                  header: "Https",
                  fillspace: 1,
                  template: function(obj,common){
                    return obj.inHTTP_HTTPS ? 'Yes' : 'No';
                  },
                };

                var urlOptionColumn = {
                  id: "urlOptionColumn",
                  header: "Url Option",
                  fillspace: 2,
                  template: function(obj, common){
                    var id = webPageListConfig.getUrlOption(obj.urlOption),
                        idValue = '';

                    webPageListConfig.urlOptions.forEach(function(element, index, array){
                      if(element.id === id){
                        idValue = element.value;
                        return;
                      }
                    });
                    return idValue;
                  },
                };
                var regExpColumn = {
                  id: "regExpColumn",
                  header: "Regex",
                  fillspace: 2,
                  template: function(obj, common){
                    return obj.regExp;
                  },
                };


                // configuration of a new column date
                var dateColumn = {
                  id: "dateColumn",
                  header: "Date",
                  fillspace: 2,
                  template: function(obj, common) {
                    return obj.date || ""; // "description" property of files
                  }
                };
                // configuration of a new column actions
                var actionsColumn = {
                  id: "actionsColumn",
                  header: "Actions",
                  fillspace: 1,
                  template: function(obj, common) {

                    if(typeof(obj.condition) === undefined || obj.condition === null){
                      obj.condition = '';
                    }

                    var params = {
                      source: 'segment',
                      id: obj.id
                    };
                    return '<a webix_l_id="update" styles="padding: 10px !important;"  title="Edit page" onclick="webPageListConfig.updateFile(\'' + obj.id + '\', \'publish\');" class="webix_list_item updatePage" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" title="delete" onclick="webPageListConfig.deleteFile(\'' + obj.id + '\');" class="webix_list_item deletePage" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
                  }
                };
                // configuration of a new column status
                var statusColumn = {
                  id: "statusColumn",
                  header: "Status",
                  fillspace: 1,
                  sort: function(a, b){
                    var _a = Number(a.hidden), _b = Number(b.hidden);
                    return (_a > _b ? 1 : (_b > _a ? -1: 0));
                  },
                  template: function(obj, common) {
                    var obj_ = {
                      action: "update_hidden",
                      hidden: "1"
                    };
                    var params = {
                      _id: obj.id,
                      action: "update_hidden",
                      node: this
                    };
                    if (obj.hidden == 0) {
                      params.hidden = 1;
                      return '<a id="updateHidden_' + obj.id + '"  onclick=\'webPageListConfig.updateHidden( ' + JSON.stringify(params) + ');\' class="webix_list_item updatePageStatus status status0" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actions" property of files ;
                    }
                    params.hidden = 0;
                    return '<a id="updateHidden_' + obj.id + '"  onclick=\'webPageListConfig.updateHidden(' + JSON.stringify(params) + ');\' class="webix_list_item updatePageStatus status status1" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
                  }
                };
                // insert columns
                webix.toArray(columns).insertAt(inWWWColumn, 1);
                webix.toArray(columns).insertAt(inHTTP_HTTPSColumn, 2);
                webix.toArray(columns).insertAt(urlOptionColumn, 3);
                webix.toArray(columns).insertAt(regExpColumn, 4);
                // webix.toArray(columns).insertAt(dateColumn, 2);
                webix.toArray(columns).insertAt(actionsColumn, 5);
                webix.toArray(columns).insertAt(statusColumn, 6);
              }

            }
          }
        }
      });

      /*******************************Menu Customization******************************/
      // only FILES mode should be available
      $$('webixFilemanagerWebPageList').$$('modes').hide();

      // updating options from menu
      var actions = $$("webixFilemanagerWebPageList").getMenu();
      actions.clearAll();
      var newData = [

        {
          id: "create",
          method: "createFolder",
          icon: "folder-o",
          value: webix.i18n.filemanager.create // "Create Folder"
        }, {
          id: "deleteFile",
          method: "deleteFile",
          icon: "times",
          value: webix.i18n.filemanager.remove // "Delete"
        }, {
          id: "edit",
          method: "editFile",
          icon: "edit",
          value: webix.i18n.filemanager.rename // "Rename"
        }
      ];
      actions.parse(newData);
      // add new option for the menu to add new segment
      actions.add({
        id: "createWebPageListElement",
        icon: "file",
        value: "Create Page"
      });

      /*******************************Segment Add**********************************/

      actions.attachEvent("onItemClick", function(id) {
        this.hide();
        // check if the action is CreateSegment
        if (id == "createWebPageListElement") {
          config.currentElementID = 'webixWizardHeaderMenuWebPageList';
          $('.header-logo').css('opacity', '0');
          // $("#main").css({'width': $('body').width() - 183 + 'px'});
          // $('#nav .menu .webix_tree_close, #nav .menu .webix_tree_open, .menu .webix_tree_item .fa-angle-right').animate({opacity: '1'});
          // $('#wrapper .sidebar').animate({width: '182px'});
          // $('#nav .menu').animate({width: '180px'});
          // $('#nav .menu  .webix_tree_branch_1.expand').css({'border-right': '5px solid #0F9FD5'});
          setTimeout(function(){
            webPageListConfig.openWizard('add');
          }, 1000);
        }
      });

      actions.getItem("create").batch = "item, root";
      actions.getItem("deleteFile").batch = "item, root";
      actions.getItem("edit").batch = "item, root";
      actions.getItem("createWebPageListElement").batch = "item, root";

      /******************************Segment Add End*******************************/


      /*****************************Menu Customization End*************************/


      /*******************************Custom Events********************************/

      /********************************Segment Edit********************************/
      $$("webixFilemanagerWebPageList").attachEvent("onBeforeDeleteFile",function(id){
          // Fix: mantis # 3945
          if (id.indexOf('webpagelist__') === -1) {
            var item = this.getItem(id);
            if(item.$parent === 0){
              webix.message({type: "error", text: "Root folder can not be deleted."})
              return false;
            }else{
              checkFolderStatus(id, webPageListConfig.folderManager);
              return false;
            }
          }
      });
      $$("webixFilemanagerWebPageList").$$("table").attachEvent("onBeforeSelect", function(id, p){
        var rowName = id.row,
            columnName = id.column;
        var item = this.getItem(rowName);

        if(p === undefined){
          return ;
        }

        if (item.type !== "folder"){
          if(['actionsColumn', 'statusColumn'].indexOf(columnName) === -1){
            webPageListConfig.updateFile(item.id, 'edit_record');
          }
        }
        //note that 'id' for datatable is an object with several attributes
      });

      // before editing file
      $$("webixFilemanagerWebPageList").attachEvent("onBeforeEditFile", function(id) {

        var srcTypeId = id.split('__');

        if (srcTypeId[0] == 'folder') {
          return true;
        }
        return false;
      });

      /*****************************Segment edit End*******************************/

      // reload grid after folder creation
      $$("webixFilemanagerWebPageList").attachEvent("onAfterCreateFolder", function(id) {
        // webPageListConfig.refreshManager();
        setTimeout(function(){ openLayoutWeb(); }, 500);
        return false;
      });


      // it will be triggered before deletion of file
      $$("webixFilemanagerWebPageList").attachEvent("onBeforeDeleteFile", function(ids) {
        if (ids.indexOf('webpagelist__') === -1) {
          return true
        }
        webPageListConfig.deleteFile(ids);
        return false;
      });
      $$("webixFilemanagerWebPageList").attachEvent("onBeforeDrop", function(context, ev) {
        if (context.start.indexOf('webpagelist__') === -1) {
          return true
        }
        webix.ajax().post(webPageListConfig.apis.change_folder, {
          _id: context.start,
          target: context.target
        }, {
          error: function(text, data, XmlHttpRequest) {
            alert("error");
          },
          success: function(text, data, XmlHttpRequest) {
            var response = JSON.parse(text);
            if (response.success == true) {}
          }
        });
        return true;
      });
      // it will be triggered before dragging the folder/segment
      $$("webixFilemanagerWebPageList").attachEvent("onBeforeDrag", function(context, ev) {
        msg = webix.message("copying...");
        return true;
      });

      // it will be triggered after dropping the folder to the destination
      $$("webixFilemanagerWebPageList").attachEvent("onAfterDrop", function(context, ev) {
        webix.message.hide(msg);
        return true;
      });

      $$("webixFilemanagerWebPageList").$$("files").attachEvent("onBeforeRender", filterFiles);
      $$("webixFilemanagerWebPageList").$$("table").attachEvent("onBeforeRender", filterFiles);

      var _exe = false;
      $$("webixFilemanagerWebPageList").attachEvent("onAfterLoad", function(context, ev) {
        if(_exe === false){
          _exe = true;
          initIntialTTandGuides();
        }
      });

      // fix: mantis # 3946
      $$("webixFilemanagerWebPageList").attachEvent("onSuccessResponse", function(req, res) {
      	if(res.success && (req.source === "newFolder" || req.source.indexOf("folder") != -1)){
      		switch(req.action){
      			case "rename":
      				webix.message("Folder renamed");
      				break;
      			case "create":
      				webix.message("Folder Created");
      				break;
      			case "remove":
      				webix.message("Folder deleted");
      				break;
      		}
      	}
      });

      /*******************************Custom Events End****************************/
    // });

    fab.initActionButtons({
      type: "pageList",
      actionButtonTypes: webPageListConfig.fab.actionButtonTypes,
      primary: webPageListConfig.fab.primary,
    });

    $(window).on('resize', function(){
      $$('webixFilemanagerWebPageList').define('width', $('body').width() - $('.sidebar').width() - 1);
      $$('webixFilemanagerWebPageList').resize();
    });
  }

  // insert postmessageListener ;
  if (window.addEventListener) {
    addEventListener("message", webPageListConfig.postMessageListener, false);
  } else {
    attachEvent("onmessage", webPageListConfig.postMessageListener);
  }
  // filter segments only
  function filterFiles(data){
     data.blockEvent();
     data.filter(function(item){
      return item.type != "folder"
     });
     data.unblockEvent();
  }


  function getAsUriParameters(data) {
     var url = '';
     for (var prop in data) {
        url += encodeURIComponent(prop) + '=' +
            encodeURIComponent(data[prop]) + '&';
     }
     return url.substring(0, url.length - 1)
  }

  global.openLayoutWeb = openLayoutWeb;
  global.webPageListConfig = webPageListConfig;
  global.page = webPageListConfig.page;
  global.isSessional = isSessional;
  global.placementWindowIframe = undefined;
})(window);
