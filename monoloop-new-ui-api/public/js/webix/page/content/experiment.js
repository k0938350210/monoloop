"use strict";
(function(global){


  var msg; // default variable for user messages
  var isSessional = false;

  // var _cms_page_url = $('body').attr('cms-page-url') !== undefined ? $('body').attr('cms-page-url') : '';
  // console.log('---', _cms_page_url);
  var experimentListConfig = {
    replicateGoal: true,
    replicateSegment:true,
    folderManager: false,
    msg: "",
    placementWindow: {},
    currentStepInfo: {},
    page: 'experiment',
    apis: {
      show: monolop_api_base_url + '/api/experiments/show',
      list: monolop_api_base_url + "/api/experiments?ajax=true",
      create: monolop_api_base_url + "/api/experiments/create",
      update: monolop_api_base_url + "/api/experiments/update",
      update_status: monolop_api_base_url + "/api/experiments/update-status",
      change_folder: monolop_api_base_url + "/api/experiments/change-folder",
      delete: monolop_api_base_url + "/api/experiments/delete",
      files: monolop_api_base_url + "/api/experiments/folder?ajax=true",
    },
    formElement: {
      id: 'pageElementIdExperimentList',
    },
    allFormData: {
      data: {},
      basic: {},
      segment: {},
      goal: {},
      pw: {},
      cg: {},
      confirm: {},
    },
    pw: {mode: 'add', data: {}},
    pageElementDocumentEdit: {},
    pageElements: [],
    experiment:  {},
    placementIframeSrc: "/component/placement-window",
    defaultSteps: [],
    steps: [],
    activeStep: "",
    stepsGenerated: {
      basic: false,
      segment: false,
      goal: false,
      pw: false,
      cg: false,
      confirm: false
    },
    containerId: "innerContent",
    showHeader: function (){
      $('div[view_id="webixWizardHeaderMenuToolbarExperiment"]').show();
      $("#viewHeader").show();
    },

    hideHeader: function(){
      $('div[view_id="webixWizardHeaderMenuToolbarExperiment"]').hide();
      $("#viewHeader").hide();
    },

    urlOptions: [
      {id: 'exactMatch', value: 'Exact Match'},
      {id: 'allowParams', value: 'Allow Parameters'},
      {id: 'startsWith', value: 'Starts With'},
    ],

    getUrlOption: function(id){
        id = id || 0;
        switch (id) {
          case 0: return "exactMatch"; break;
          case 1: return "allowParams"; break;
          case 2: return "startsWith"; break;
          default: return "exactMatch"; break;
        }
    },
    highLightCurrentStep: function (current) {
      $(".wizardStepBtn .webix_badge").removeClass('active');
      switch (current) {
        case "basic":
          $(".innerContent .wizardStepBtn.Basic .webix_badge").addClass('active');
          break;
        case "segment":
          $(".innerContent .wizardStepBtn.SegmentAndGoal .webix_badge").addClass('active');
          break;
        case "goal":
          $(".innerContent .wizardStepBtn.Goal .webix_badge").addClass('active');
          break;
        case "placement":
          $(".innerContent .wizardStepBtn.Placement .webix_badge").addClass('active');
          break;
        case "controlGroup":
          $(".innerContent .wizardStepBtn.controlGroup .webix_badge").addClass('active');
          break;
        case "confirm":
          $(".innerContent .wizardStepBtn.Confirm .webix_badge").addClass('active');
          break;
        default:
          break;
      }
    },
    next: function (mode, formData) {
      // alert(experimentListConfig.activeStep);
      var index = experimentListConfig.steps.indexOf(experimentListConfig.activeStep);

      if (index === experimentListConfig.steps.length - 1) {
        index = index;
      } else {
        index = index + 1;
      }

      var nextStep = experimentListConfig.steps[index];

      var isValid = false;

      switch (experimentListConfig.activeStep) {
        case "segment":
          var form = $$("segmentFormExperiment");
          if (form.validate()) {
            isValid = true;
          }
          break;
        default:
          isValid = true;
          break;
      }
      // alert(nextStep);
      if (isValid) {
        switch (nextStep) {
          case "segment":
            experimentListConfig.activeStep = experimentListConfig.steps[0];
            break;
          case "placement":
            experimentListConfig.activeStep = experimentListConfig.steps[1];
            $$("segmentStepExperiment").hide();
            if (!experimentListConfig.stepsGenerated.pw) {
              experimentListConfig.openExperimentPages(mode, formData);

            } else {
              $$("wizardPlacementBtnExperiment").enable();
              experimentListConfig.showView(false, true, false, mode, formData);
            }

            break;
          case "confirm":
            experimentListConfig.activeStep = experimentListConfig.steps[2];

            $('#wizardPlacementSectionIdExperiment').hide();

            if (!experimentListConfig.stepsGenerated.confirm) {
              experimentListConfig.openConfirm(mode, formData);
            } else {
              $$("wizardConfirmBtnExperiment").enable();
              experimentListConfig.showView(false, false, true, mode, formData);
            }
            break;
          default:
            break;
        }
      }
    },
    previous: function (mode, formData) {
      var index = experimentListConfig.steps.indexOf(experimentListConfig.activeStep);

      if (index === 0) {
        index = index;
      } else {
        index = index - 1;
      }

      var prevStep = experimentListConfig.steps[index];
      switch (prevStep) {
        case "segment":
          experimentListConfig.activeStep = experimentListConfig.steps[0];
          experimentListConfig.showView(true, false, false, mode, formData);
          break;
        case "placement":
          experimentListConfig.activeStep = experimentListConfig.steps[1];
          experimentListConfig.showView(false, true, false, mode, formData);
          break;
        case "confirm":
          experimentListConfig.activeStep = experimentListConfig.steps[2];
          experimentListConfig.showView(false, false, true, mode, formData);
          break;
        default:
          break;
      }
    },
    showView: function (segment, placement, confirm, mode, formData) {
      if (experimentListConfig.stepsGenerated.segment) {
        if (segment) {
          experimentListConfig.highLightCurrentStep("segment");
          $$('segmentStepExperiment').show();
          experimentListConfig.showHeader();
        } else {
          $$('segmentStepExperiment').hide();
        }
      }
      if (experimentListConfig.stepsGenerated.pw) {
        if (placement) {
          experimentListConfig.highLightCurrentStep("placement");
          $('#wizardPlacementSectionIdExperiment').show();
          experimentListConfig.openExperimentPages(mode, formData);
          experimentListConfig.showHeader();
        } else {
          $('#wizardPlacementSectionIdExperiment').hide();
        }
      }
      if (experimentListConfig.stepsGenerated.confirm) {
        if (confirm) {
          experimentListConfig.highLightCurrentStep("confirm");
          experimentListConfig.openConfirm(mode, formData);
          experimentListConfig.showHeader();
        } else {
          $$("confirmStepExperiment").hide();
        }
      }
    },
    openWizard: function (mode, formData, src) {
      // reinitialize the settings
      experimentListConfig.replicateGoal = true;
      experimentListConfig.replicateSegment = true;

      if($('#floatingContainer').length > 0){
        $('#floatingContainer').remove();
      }


      var f = window.location.hash;
      if(mode === 'add'){
        window.history.pushState(config.addexperiment.headerName, config.addexperiment.headerName, config.addexperiment.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);
      } else {
        window.history.pushState(config.editexperiment.headerName, config.editexperiment.headerName, config.editexperiment.link + '?id=' + formData._id +   ($('body').hasClass('hidenav') ? '&nav=1' : '') + f);
      }

      experimentListConfig.defaultSteps = ["segment", "placement", "confirm"];
      experimentListConfig.activeStep = experimentListConfig.defaultSteps[0];
      experimentListConfig.stepsGenerated.basic = false;
      experimentListConfig.stepsGenerated.segment = false;
      experimentListConfig.stepsGenerated.goal = false;
      experimentListConfig.stepsGenerated.pw = false;
      experimentListConfig.stepsGenerated.cg = false;
      experimentListConfig.stepsGenerated.confirm = false;

      if(userStateData){
        isSessional = userStateData.section === 'experiment';
        experimentListConfig.activeStep = userStateData.data.step;
      }

      $("#innerContent").html("");

      var wizard = document.createElement('div');
      wizard.id = 'webixWizardParentIdExperiment';
      // wizard.style.minHeight = '680px';
      document.getElementById("innerContent").appendChild(wizard);


      experimentListConfig.steps = experimentListConfig.defaultSteps;

      var segmentWindowButtonExperiment = {
        view: "button",
        css: "wizardStepBtn SegmentAndGoal",
        id: "wizardSegmentBtnExperiment",
        label: "Audience & Goal",
        width: 200,
        badge: 1,
        disabled: true,
        click: function() {
          experimentListConfig.activeStep = experimentListConfig.steps[0];
          experimentListConfig.showView(true, false, false, mode, formData);
        }
      };

      var placementWindowButtonExperiment = {
        view: "button",
        css: "wizardStepBtn Placement",
        id: "wizardPlacementBtnExperiment",
        label: "Experience",
        width: 170,
        badge: 2,
        disabled: true,
        click: function() {
          guidelines.startSubGuideline('experience');
          experimentListConfig.activeStep = experimentListConfig.steps[1];
          if(experimentListConfig.stepsGenerated.pw){
            experimentListConfig.showView(false, true, false, mode, formData);
          } else {
            experimentListConfig.showView(false, true, false, mode, formData);
            experimentListConfig.openExperimentPages(mode, formData);
          }

        }
      };

      var isConfirmDisabledExperiment = mode == "add" ? true : false;
      var consfirmButtonExperiment = {
        view: "button",
        css: "wizardStepBtn Confirm",
        id: "wizardConfirmBtnExperiment",
        label: "Confirm",
        width: 170,
        badge: 3,
        disabled: isConfirmDisabledExperiment,
        click: function() {
          guidelines.startSubGuideline('confirm');

          experimentListConfig.activeStep = experimentListConfig.steps[2];
          experimentListConfig.showView(false, false, true, mode, formData);
        }
      };

      webix.ui({
        container: "webixWizardParentIdExperiment",
        height: config.styles.defaultHeight.getFileManagerHeight(),
        id: "webixWizardHeaderMenuExperiment",
        rows: [{
          view: "toolbar",
          paddingY: 1,
          height: 50,
          id: "webixWizardHeaderMenuToolbarExperiment",
          hidden: false,
          css: "webixWizardHeader",
          elements: [{
              gravity: 1
            }, segmentWindowButtonExperiment, placementWindowButtonExperiment, consfirmButtonExperiment, {
              gravity: 1
            }

          ]
        }, {
          template: "<div id='wizardbasicSectionIdExperiment'></div><div id='wizardSegmentSectionIdExperiment'></div><div id='wizardGoalSectionIdExperiment'></div><div id='wizardPlacementSectionIdExperiment'></div><div id='wizardControlGroupSectionIdExperiment'></div><div id='wizardConfirmSectionIdExperiment'></div>"
        }]
      });

      experimentListConfig.openSegment(mode, formData);
      if (mode === 'edit') {

      }
      startGuidelinesInWizard({
        src: src,
        mode: mode,
        add: 'addexperiment',
        edit: 'editexperiment',
      });
    },
    openSegment: function (mode, formData, referenceOptions) {

      $("#wizardSegmentSectionIdExperiment").html("");

      global.page = experimentListConfig.page;

      experimentListConfig.stepsGenerated.segment = true;

      var goalURL = monolop_api_base_url + "/data/experiments/goal";
      var segmentURL = monolop_api_base_url + "/data/experiments/segment";

      webix.ajax().get(segmentURL, {ajax: true}, function(resSegment, xml, xhr) {
        webix.ajax().get(goalURL, {ajax: true}, function(resGoal, xml, xhr) {

          var prepareListItem = function(item){
            if(!item.description){
              item.description = "";
            }else{
              item.description = "-"+item.description;
            }
            var _html = '<div>';
            _html += '<span style=\"float:left;\">' + item.name + item.description.substr(0, 30) + '</span>';
            _html += '</div>';
            return _html;
          };
          //response
          var segments = [],
              createNewSegmentOption = [{
                'id': 'create_new_segment',
                'value': '<b><span style="color: #0F9FD5;">Create new</span></b>',
              }];
          resSegment = JSON.parse(resSegment);

          for (var i = 0; i < resSegment.segments.length; i++) {
            if(!resSegment.segments[i].description){
              resSegment.segments[i].description="";
            }
            segments.push({
              'id': resSegment.segments[i]._id,
              // 'value': '<b>' + resSegment.segments[i].name + '</b>' + ' - ' + resSegment.segments[i].description.substr(0, 30)
              'value': prepareListItem(resSegment.segments[i])
            });
          }
          experimentListConfig.allFormData.data.segment = segments;


          //response
          var goals = [],
              createNewGoalOption = [{
                'id': 'create_new_goal',
                'value': '<b><span style="color: #0F9FD5;">Create new</span></b>',
              }];
          resGoal = JSON.parse(resGoal);
          for (var i = 0; i < resGoal.goals.length; i++) {
            goals.push({
              'id': resGoal.goals[i]._id,
              // 'value': '<b>' + resGoal.goals[i].name + '</b>'
              'value': prepareListItem(resGoal.goals[i])
            });
          }
          experimentListConfig.allFormData.data.goal = goals;


          var _form = [{
            view: "layout",
            height: 520,
            rows: [{
                view: "text",
                id: "tnameExperiment",
                label: 'Name',
                name: "tnameExperiment",
                // readonly: true,
                  // invalidMessage: "Name can not be empty"
              }, {
                view: "richselect",
                label: "Please select the audience you wish to target:",
                id: "tsegmentExperiment",
                css: "selectSegment",
                name: "tsegmentExperiment",
                value: "",
                options: createNewSegmentOption.concat(segments),
                labelAlign: 'left',
            }, {
                view: "richselect",
                label: "Please select the goal you wish to target:",
                id: "tgoalExperiment",
                css: "selectGoal",
                name: "tgoalExperiment",
                value: "",
                options: createNewGoalOption.concat(goals),
                labelAlign: 'left',
            }]
          }, {
            cols: [{}, {}, {
                view: "button",
                value: "Next",
                css: "orangeBtn",
                width: 138,
                click: function() {
                  guidelines.close(null, "yes");
                  experimentListConfig.next(mode, formData);
                }
              }, {}, {},

            ]
          }];
          var __formRules = {
            "tnameExperiment": webix.rules.isNotEmpty,
          };

          webix.ui({
            container: "wizardSegmentSectionIdExperiment",
            id: "segmentStepExperiment",
            rows: [{
              cols: [{}, {
                view: "form",
                id: "segmentFormExperiment",
                complexData: true,
                elements: _form,
                rules: __formRules,
                width: 600,
                borderless: true,
                margin: 3,
                elementsConfig: {
                  labelPosition: "top",
                  labelWidth: 140,
                  bottomPadding: 18
                },
              }, {}, ]
            }]
          });

          var toggleEditButtons = function(){
            var selectedGoal = $$('tgoalExperiment').getValue();
            var selectedSegment = $$('tsegmentExperiment').getValue();
            console.log(selectedGoal, selectedSegment);
            if(selectedGoal != 'create_new_goal' && selectedGoal.length > 0){
              $('.edit-button.goal').removeClass('hidden');
            }else{
              $('.edit-button.goal').addClass('hidden');
            }

            if(selectedSegment != 'create_new_segment' && selectedSegment.length > 0){
              $('.edit-button.segment').removeClass('hidden');
            }else{
              $('.edit-button.segment').addClass('hidden');
            }
          };

          $$("tsegmentExperiment").attachEvent('onChange', function(id){
             toggleEditButtons();
             if(id === 'create_new_segment'){
               webix.confirm({
                 text: "Do you want to create?",
                 ok: "Yes",
                 cancel: "No",
                 callback: function(result) {
                   if (result) {
                     config.currentElementID = 'webixWizardHeaderMenuSegment';
                     experimentListConfig.createNewSegment(mode, formData);
                   }
                 }
               });
               $$("tsegmentExperiment").setValue('');
             } else {
               return true;
             }
          });

          $$("tgoalExperiment").attachEvent('onChange', function(id){
            toggleEditButtons();
             if(id === 'create_new_goal'){
               webix.confirm({
                 text: "Do you want to create?",
                 ok: "Yes",
                 cancel: "No",
                 callback: function(result) {
                   if (result) {
                     config.currentElementID = 'webixWizardHeaderMenuGoal';
                     experimentListConfig.createNewGoal(mode, formData);
                   }
                 }
               });
               $$("tgoalExperiment").setValue('');
             } else {

               return true;
             }
          });

          // add segment edit button
          var _segmentButton = '<a class="edit-button segment hidden" title="Edit audeince"><span class="webix_icon fa-edit"></span></a>' ;
          var _goalButton = '<a class="edit-button goal hidden" title="Edit goal"><span class="webix_icon fa-edit"></span></a>';

          $('div[view_id=segmentStepExperiment]').append(_segmentButton);
          $('div[view_id=segmentStepExperiment]').append(_goalButton);

          $('.edit-button.segment').on('click', function(e){
            var segmentID = $$('tsegmentExperiment').getValue();
            webix.confirm({
              text: "Do you want to edit this segment?",
              ok: "Yes",
              cancel: "No",
              callback: function(result) {
                if (result) {
                  config.currentElementID = 'webixWizardHeaderMenuSegment';
                  experimentListConfig.editSegment(segmentID, mode, formData);
                }
              }
            });
          });

          $('.edit-button.goal').on('click', function(e){
            var goalID = $$('tgoalExperiment').getValue();
            webix.confirm({
              text: "Do you want to edit this goal?",
              ok: "Yes",
              cancel: "No",
              callback: function(result) {
                if (result) {
                  config.currentElementID = 'webixWizardHeaderMenuGoal';
                  experimentListConfig.editGoal(goalID, mode, formData);
                }
              }
            });
          });

          if (experimentListConfig.activeStep === experimentListConfig.defaultSteps[0]) {
            experimentListConfig.highLightCurrentStep("segment");
          }
          if (mode === 'edit') {
            $$("segmentFormExperiment").setValues({
              tnameExperiment: formData.name,
              tsegmentExperiment: formData.segment_id,
              tgoalExperiment: formData.goal_id,
            });

            if (experimentListConfig.activeStep !== experimentListConfig.defaultSteps[0]) {
              $$("segmentStepExperiment").hide();
            }

            //
            if(userStateData && isSessional){
              $$("segmentFormExperiment").setValues(userStateData.data.basicData);
              if(['segment'].indexOf(userStateData.data.step) === -1){
                experimentListConfig.showView(false, true, false, mode, formData);
                experimentListConfig.highLightCurrentStep("placement");
              }
            }
            //

            experimentListConfig.openExperimentPages(mode, formData);

            // $$("wizardPlacementBtnExperiment").enable();
            // experimentListConfig.openConfirm(mode, formData);
          } else {
            if(userStateData && isSessional){
              $$("segmentFormExperiment").setValues(userStateData.data.basicData);
              if(['segment'].indexOf(userStateData.data.step) === -1){
                experimentListConfig.showView(false, true, false, mode, formData);
                experimentListConfig.openExperimentPages(mode, formData);
                experimentListConfig.highLightCurrentStep("placement");
              }
            }
          }

          // seeting up newly added segment while adding/editing experiment
          if (referenceOptions !== undefined) {

            if (referenceOptions.newSegment !== undefined) {
              $$("tnameExperiment").setValue(referenceOptions.oldFormData[2].tnameExperiment);
              $$("tsegmentExperiment").setValue(referenceOptions.newSegment._id);
              $$("tgoalExperiment").setValue(referenceOptions.oldFormData[2].tgoalExperiment);
            }
            if (referenceOptions.newGoal !== undefined) {
              $$("tnameExperiment").setValue(referenceOptions.oldFormData[2].tnameExperiment);
              $$("tsegmentExperiment").setValue(referenceOptions.oldFormData[2].tsegmentExperiment);
              $$("tgoalExperiment").setValue(referenceOptions.newGoal._id);
            }

            experimentListConfig.highLightCurrentStep("segment");
          }
        });
      });
      $$("wizardSegmentBtnExperiment").enable();
    },
    openExperimentPages: function(mode, formData){

      $("#wizardPlacementSectionIdExperiment").html('');
      var _id = '';
      if(formData != null){
        _id = formData._id;
      } else if(experimentListConfig.experiment != null) {
        _id = experimentListConfig.experiment._id;
      }
      if(!_id){
        if(userStateData && isSessional){
          if(userStateData.data.experiment_id){
            _id = userStateData.data.experiment_id;
            experimentListConfig.experiment._id = _id;
          }
        } else if(experimentListConfig.experiment != null) {
          _id = experimentListConfig.experiment._id;
        }
      }
      webix.ajax().get('/api/experiments/getPages', { _id: _id,ajax: true,
      }, function(response, xml, xhr) {
        //response
        experimentListConfig.pageElements = response = JSON.parse(response);
        var pageList = [];

        for (var i = 0; i < response.length; i++) {
          pageList.push({
            _id: response[i]._id,
            s_no: i+ 1,
            originalUrl: response[i].originalUrl,
            inWWW: response[i].inWWW,
            inHTTP_HTTPS: response[i].inHTTP_HTTPS,
            urlOption: experimentListConfig.getUrlOption(response[i].urlOption),
            statusName: (response[i].hidden == 0) ? 'inactive': 'active',
            status: response[i].hidden,
          });
        }

        experimentListConfig.stepsGenerated.pw = true;
        $("#wizardPlacementSectionIdExperiment").empty();
        webix.ui({
          container: "wizardPlacementSectionIdExperiment",
          id: "wizardPlacementSectionIdExperimentPagesList",
          height: 580,
          css: "wizardPlacementSectionIdExperimentPagesList",
          rows: [
          {
            cols: [
              {view: "button", css: "createExperimentBtn", value: "Create page", click: function(){
                experimentListConfig.openPW(mode, formData, 'add');
              }},
              {},{},{},{},{},{},{}
            ]
          },
          {
            view:"datatable", select:false, editable:false, id: "pagesDatatable",
            columns:[
              {id:"s_no", header:"#", width:50},
              {id:"originalUrl", header:["Page"], sort:"string", minWidth:'30%', fillspace: 2, editor:"text"},
              {id:"inWWW", header:["Include WWW"], sort:"string", minWidth:'10%', fillspace: 2, editor:"text"},
              {id:"inHTTP_HTTPS", header:["Include HTTP/HTTPS"], sort:"string", minWidth:'10%', fillspace: 2, editor:"text"},
              {id:"urlOption", header:["URL Option"], sort:"string", minWidth:'10%', fillspace: 2, editor:"text"},
              // {id:"status", header:["Status"], minWidth:'20%', fillspace: 1, template:"<span style='color:black;' class='status status#status#'>#statusName#</span>"},

              {id:"edit", header:"&nbsp;", width:35, template:"<span  style=' cursor:pointer;' class='webix_icon fa-pencil editPageFromExp'></span>"},
  		        {id:"delete", header:"&nbsp;", width:35, template:"<span  style='cursor:pointer;' class='webix_icon fa-trash-o deletePageFromExp'></span>"}

            ],
            data: pageList,
            onClick:{
              "deletePageFromExp":function(e,id,node){
                webix.confirm({
                  text:"Are you sure that you <br />want to delete?", ok:"Yes", cancel:"Cancel",
                  callback:function(res){
                    if(res){
                      var item = webix.$$("pagesDatatable").getItem(id);


                      webix.ajax().post("/api/content/web/delete", {_id: 'webpagelist__' + item._id, ajax: true,}, function(res, data, xhr){
                        res = JSON.parse(res);
                        if(res.success === true){
                          webix.$$("pagesDatatable").remove(id);
                          webix.message("Page removed!");
                        }
                      });


                    }
                  }
                });
              },
              "editPageFromExp": function(e,id,node){
                var item = webix.$$("pagesDatatable").getItem(id);
                for (var i = 0; i < experimentListConfig.pageElements.length; i++) {
                  if(experimentListConfig.pageElements[i]._id === item._id){
                    experimentListConfig.pageElementDocumentEdit = experimentListConfig.pageElements[i];
                    break;
                  }
                }
                experimentListConfig.openPW(mode, formData, 'edit');

              }
            },
            ready:function(){
                 if (!this.count()){ //if no data is available
                     webix.extend(this, webix.OverlayBox);
                     this.showOverlay("<div style='font-weight: 800;font-size: 16px;'>No page is added yet.</div>");
                 }
             },
          },{
            cols: [{}, {}, {}, {
              view: "button",
              "type": "danger",
              value: "Back",
              width: 138,
              click: function() {
                guidelines.close(null, "yes");
                experimentListConfig.previous(mode, formData);
              }
            }, {
              view: "button",
              value: "Next",
              css: "orangeBtn",
              width: 138,
              click: function() {
                guidelines.close(null, "yes");
                experimentListConfig.next(mode, formData);
              }
            }, {}, {}, {}]
          }]

        });

        if (experimentListConfig.activeStep === experimentListConfig.defaultSteps[1]) {
          experimentListConfig.highLightCurrentStep("placement");
        }
        if (mode === 'edit') {

          if (experimentListConfig.activeStep !== experimentListConfig.defaultSteps[1]) {
            $("#wizardPlacementSectionIdExperiment").hide();
          }
          //
          if(userStateData && isSessional){
            if(['segment', 'placement'].indexOf(userStateData.data.step) === -1){
              experimentListConfig.showView(false, false, true, mode, formData);
              experimentListConfig.highLightCurrentStep("confirm");
            }
          }
          //
          experimentListConfig.openConfirm(mode, formData);
        } else {
          guidelines.startSubGuideline('experience');
          if(userStateData && isSessional){
            if(['segment', 'placement'].indexOf(userStateData.data.step) === -1){
              experimentListConfig.showView(false, false, true, mode, formData);
              experimentListConfig.openConfirm(mode, formData);
            } else if (['placement'].indexOf(userStateData.data.step) !== -1) {
              experimentListConfig.openPW(mode, formData, 'add');
            }
          }
        }
        $$("wizardPlacementBtnExperiment").enable();

      });


    },
    openPW: function (mode, formData, pageMode) {
      experimentListConfig.hideHeader();

      $("#wizardPlacementSectionIdExperiment").html('');
      var defaultDomainUrl;

      webix.ajax().get('/data/domains/default', {ajax: true
      }, function(response, xml, xhr) {
        //response
        response = JSON.parse(response);
        defaultDomainUrl = response.defaultDomainUrl;
        if(defaultDomainUrl == ''){
          defaultDomainUrl = 'www.monoloop.com';
        }

        if (mode === 'edit') {
          // webPageListConfig.allFormData.basic = {};
          // webPageListConfig.allFormData.basic.tid = formData._id;
          // webPageListConfig.allFormData.fullURL = formData.originalUrl;
          if(experimentListConfig.pageElementDocumentEdit){
            experimentListConfig.allFormData.fullURL = experimentListConfig.pageElementDocumentEdit.originalUrl; // testing
          }
          if(experimentListConfig.allFormData.fullURL === undefined){
            experimentListConfig.allFormData.fullURL = defaultDomainUrl
          }
        } else {
          experimentListConfig.allFormData.fullURL = defaultDomainUrl;
          if(experimentListConfig.pageElementDocumentEdit){
            if(experimentListConfig.pageElementDocumentEdit.originalUrl){
              experimentListConfig.allFormData.fullURL = experimentListConfig.pageElementDocumentEdit.originalUrl; // testing
            }
          }
        }

        if (isSessional && userStateData) {
          if(userStateData.data.url){
            experimentListConfig.allFormData.fullURL = userStateData.data.url;
          }
        }

        experimentListConfig.stepsGenerated.pw = true;

        var placementWindowUrl = monolop_base_url + experimentListConfig.placementIframeSrc + '?source=experiment&url=' + experimentListConfig.allFormData.fullURL + '&ver=' + (new Date()).getTime() + Math.floor(Math.random() * 1000000);
        webix.ui({
          container: "wizardPlacementSectionIdExperiment",
          id: "placementStepExperiment",
          height: config.styles.defaultHeight.getFileManagerHeight()-65,
          css: "wizardPWSectionWebPageList",
          rows: [{
            view: "layout",
            // height: 780,
            rows: [{
              view: "iframe",
              id: "PWIframeExperimentList",
              css: "PWIframeWebPageList",
              src: placementWindowUrl,
            }]
          },
          //  {
          //   cols: [{}, {}, {}, {
          //     view: "button",
          //     "type": "danger",
          //     value: "Back",
          //     width: 138,
          //     click: function() {
          //       experimentListConfig.previous(mode, formData);
          //     }
          //   }, {
          //     view: "button",
          //     value: "Next",
          //     css: "orangeBtn",
          //     width: 138,
          //     click: function() {
          //       experimentListConfig.next(mode, formData);
          //     }
          //   }, {}, {}, {}]
          // }
        ]

        });

        $$("PWIframeExperimentList").attachEvent("onAfterLoad", function(sid) {
          if (experimentListConfig.stepsGenerated.pw) {}
          var PWIframeExperimentList = $("iframe[src*='component/placement-window']");
          experimentListConfig.pw.mode = mode;
          if (mode === 'edit') {
          }
          var type = 'pw';

          if(isSessional && userStateData){
            mode = userStateData.data.type ? userStateData.data.type : mode;
            type = userStateData.data._step ? userStateData.data._step : type;
            mode = userStateData.data.type ? userStateData.data.type : mode;
          }

          if($$('tsegmentExperiment').getValue()){
            if(experimentListConfig.experiment){
              experimentListConfig.experiment.tempSegment = $$('tsegmentExperiment').getValue();
            }else{
              experimentListConfig.experiment = {};
              experimentListConfig.experiment.tempSegment = $$('tsegmentExperiment').getValue();
            }
          }

          PWIframeExperimentList.get(0).contentWindow.postMessage(JSON.stringify({
            t: "posted-from-monoloopui",
            data: {
                    fullURL: experimentListConfig.allFormData.fullURL,
                    pw: experimentListConfig.pw,
                    source: 'experiment',
                    formData: experimentListConfig.pageElementDocumentEdit,
                    experiment: experimentListConfig.experiment,
                    page: experimentListConfig.page,
                    type: type,
                    mode: pageMode,
                    currentFolder: experimentListConfig.folderManager ? experimentListConfig.folderManager.getCurrentFolder() : '',
                    temp: {mode: mode, formData: formData}
                  }
              }), placementWindowUrl);

        });
        if (experimentListConfig.activeStep === experimentListConfig.defaultSteps[1]) {
          experimentListConfig.highLightCurrentStep("placement");
        }
        if (mode === 'edit') {

          if (experimentListConfig.activeStep !== experimentListConfig.defaultSteps[1]) {
            $("#wizardPlacementSectionIdExperiment").hide();
          }
          experimentListConfig.openConfirm(mode, formData);
        }
        $$("wizardPlacementBtnExperiment").enable();

      });
    },
    openCG: function (mode, formData) {
      mode === 'add' && experimentListConfig.highLightCurrentStep("controlGroup");

      experimentListConfig.stepsGenerated.cg = true;

      webix.ajax().get("/data/experiments/control-group", {ajax: true}, function(text, xml, xhr) {
        text = JSON.parse(text);
        var significantActions = [];

        for (var i = 0; i < text.significantActions.length; i++) {
          significantActions.push({
            'id': i,
            'value': '<b>' + text.significantActions[i] + '</b>'
          });
        }
        experimentListConfig.allFormData.data.cg = significantActions;
        var _form = [{
          view: "layout",
          height: 520,
          rows: [
            {
              view: "slider",
              type: "alt",
              label: "Size of the control group (%)",
              value: "50",
              id: "tcgSizeExperiment",
              name: "tcgSizeExperiment",
              title: webix.template("#value# %")
            }, {
              view: "text",
              id: "tcgDayExperiment",
              label: 'Days visitors stay in control group (days)',
              name: "tcgDayExperiment",
              attributes:{
                maxlength:3,
            },
              value: 30,
              // invalidMessage: "Name can not be empty"
            }, {
              view: "select",
              label: "Action when significant:",
              id: "tsignificantActionExperiment",
              name: "tsignificantActionExperiment",
              options: significantActions,
              labelAlign: 'left',
            }
          ]
        }, {
          cols: [{}, {}, {
              view: "button",
              "type": "danger",
              value: "Back",
              width: 138,
              click: function() {
                experimentListConfig.previous(mode, formData);
              }
            }, {
              view: "button",
              value: "Next",
              width: 138,
              click: function() {
                experimentListConfig.next(mode, formData);
              }
            }, {}, {},

          ]
        }];
        var __formRules = {
          "tcgSizeExperiment": webix.rules.isNumber,
          "tcgDayExperiment": webix.rules.isNumber,
          // "tsignificantActionExperiment": webix.rules.isNotEmpty,
        };

        webix.ui({
          container: "wizardControlGroupSectionIdExperiment",
          id: "controlGroupStepExperiment",
          rows: [{
            cols: [{}, {
              view: "form",
              id: "controlGroupFormExperiment",
              complexData: true,
              elements: _form,
              rules: __formRules,
              width: 600,
              borderless: true,
              margin: 3,
              elementsConfig: {
                labelPosition: "top",
                labelWidth: 140,
                bottomPadding: 18
              },
            }, {}, ]
          }]
        });

        if (mode === 'edit') {
          $$("controlGroupFormExperiment").setValues({
            tcgSizeExperiment: formData.cg_size,
            tcgDayExperiment: formData.cg_day,
            tsignificantActionExperiment: formData.significant_action,
          });

          if (experimentListConfig.activeStep !== experimentListConfig.defaultSteps[4]) {
            $$("controlGroupStepExperiment").hide();
          }

          experimentListConfig.openConfirm(mode, formData);
        }
      });
      $$("wizardControlGroupBtnExperiment").enable();
    },
    openConfirm: function (mode, formData) {

      // mode === 'add' && experimentListConfig.highLightCurrentStep("confirm");

      // $("#innerContent, #webixWizardParentIdExperiment").css('height', '1500px');

      // if(global.placementWindowIframe !== undefined){
      //   experimentListConfig.allFormData.pw.fullURL = global.placementWindowIframe.customerSiteUrlLink;
      // }
      var addedVariations = [];



      webix.ajax().get("/data/experiments/control-group", {ajax: true}, function(resCG, xml, xhr) {
        resCG = JSON.parse(resCG);
        var significantActions = [];

        for (var i = 0; i < resCG.significantActions.length; i++) {
          significantActions.push({
            'id': i,
            'value': '<b>' + resCG.significantActions[i] + '</b>'
          });
        }
        experimentListConfig.allFormData.data.cg = significantActions;


        experimentListConfig.stepsGenerated.confirm = true;

        // experimentListConfig.allFormData.pw.tIncludeWWWWebPageElement = 1;
        // experimentListConfig.allFormData.pw.tIncludeHttpHttpsWebPageElement = 0;
        // experimentListConfig.allFormData.pw.tUrlOptionWebPageElement = 'exactMatch';
        // experimentListConfig.allFormData.pw.tRegularExpressionWebPageElement = '';
        // experimentListConfig.allFormData.pw.tRemarkWebPageList = '';

        if(mode === 'edit'){
          experimentListConfig.allFormData.basic = {
                                                      tnameExperiment: formData.name,
                                                      tdescExperiment: formData.description,
                                                      tid: formData._id
                                                    };
          experimentListConfig.allFormData.cg = {
                                                      tcgSizeExperiment: formData.cg_size,
                                                      tcgDayExperiment: formData.cg_day,
                                                      tsignificantActionExperiment: formData.significant_action
                                                    };

        } else {
          experimentListConfig.allFormData.basic = {
                                                      tnameExperiment: '',
                                                      tdescExperiment: '',
                                                    };
          experimentListConfig.allFormData.cg = {
                                                      tcgSizeExperiment: "50",
                                                      tcgDayExperiment: 30,
                                                      tsignificantActionExperiment: 0
                                                    };
        }

        // experimentListConfig.allFormData.basic = $$("basicFormExperiment").getValues();
        experimentListConfig.allFormData.segment = $$("segmentFormExperiment").getValues();
        // experimentListConfig.allFormData.goal = $$("goalFormExperiment").getValues();
        // experimentListConfig.allFormData.pw = {};
        // experimentListConfig.allFormData.cg = $$("controlGroupFormExperiment").getValues();

        // if(experimentListConfig.pageElementDocumentEdit){
        //   experimentListConfig.allFormData.pw.tIncludeWWWWebPageElement = experimentListConfig.editPageFromExp.inWWW;
        //   experimentListConfig.allFormData.pw.tIncludeHttpHttpsWebPageElement = experimentListConfig.editPageFromExp.inHTTP_HTTPS;
        //   experimentListConfig.allFormData.pw.tUrlOptionWebPageElement = experimentListConfig.getUrlOption(experimentListConfig.editPageFromExp.urlOption);
        //   experimentListConfig.allFormData.pw.tRegularExpressionWebPageElement = experimentListConfig.editPageFromExp.regExp;
        //   experimentListConfig.allFormData.pw.tRemarkWebPageList = experimentListConfig.editPageFromExp.remark;
        // }


        var __form = [{
          view: "layout",
          rows: [{
              view: "text",
              id: "nameExperiment",
              label: 'Name',
              name: "nameExperiment",
              readonly: true,
              value: experimentListConfig.allFormData.segment.tnameExperiment
                // invalidMessage: "Name can not be empty"
            }, {
              view: "textarea",
              id: "descExperiment",
              label: 'Description',
              name: "descExperiment",
              height: 150,
              // readonly: true,
              value: experimentListConfig.allFormData.basic.tdescExperiment
                // invalidMessage: "Description can not be empty"
            }, {
              view: "richselect",
              label: "Selected audience",
              id: "segmentExperiment",
              name: "segmentExperiment",
              value: experimentListConfig.allFormData.segment.tsegmentExperiment,
              options: experimentListConfig.allFormData.data.segment,
              labelAlign: 'left',
              // readonly: true,
            }, {
              view: "richselect",
              label: "Selected goal",
              id: "goalExperiment",
              name: "goalExperiment",
              value: experimentListConfig.allFormData.segment.tgoalExperiment,
              options: experimentListConfig.allFormData.data.goal,
              labelAlign: 'left',
              // readonly: true,
            }, {
              view: "slider",
              type: "alt",
              label: "Size of the control group (%)",
              value: experimentListConfig.allFormData.cg.tcgSizeExperiment,
              id: "cgSizeExperiment",
              name: "cgSizeExperiment",
              // disabled: true,
              title: webix.template("#value# %")
            }, {
              view: "text",
              id: "cgDayExperiment",
              label: 'Days visitors stay in control group (days)',
              name: "cgDayExperiment",
              // readonly: true,
              value: experimentListConfig.allFormData.cg.tcgDayExperiment,
              // invalidMessage: "Name can not be empty"
            }, {
              view: "select",
              label: "Action when significant:",
              id: "significantActionExperiment",
              name: "significantActionExperiment",
              value: experimentListConfig.allFormData.cg.tsignificantActionExperiment,
              options: experimentListConfig.allFormData.data.cg,
              labelAlign: 'left',
              // readonly: true,
            }

          ]
        }, ];
        var __formRules = {
          "nameExperiment": webix.rules.isNotEmpty,
          "cgSizeExperiment": webix.rules.isNumber,
          "cgDayExperiment": webix.rules.isNumber,
          "significantActionExperiment": webix.rules.isNotEmpty,
        };

        webix.ui({
          container: "wizardConfirmSectionIdExperiment",
          id: "confirmStepExperiment",
          // height: 620,
          // scroll: true,
          rows: [{
            cols: [{}, {
              view: "form",
              id: "confirmFormExperiment",
              // height: 680,
              // scroll: true,
              complexData: true,
              elements: __form,
              rules: __formRules,
              width: 620,
              borderless: true,
              margin: 3,
              elementsConfig: {
                labelPosition: "top",
                labelWidth: 140,
                bottomPadding: 18
              },
            }, {}, ]
          }, {
            cols: [{}, {}, {
              view: "button",
              label: "Back",
              type: "danger",
              click: function() {
                experimentListConfig.previous(mode, formData);
              },
              width: 138,
            }, {
              view: "button",
              label: "Confirm and save",
              css: "orangeBtn",
              width: 138,
              click: function() {
                var btnCtrl = this;
                btnCtrl.disable();
                var form = $$("confirmFormExperiment");

                var pageElementIds = '';

                // if(global.placementWindowIframe.currentPageElement.added){
                //   pageElementId = global.placementWindowIframe.currentPageElement.data._id;
                // }
                // if(pageElementId === ''){
                //   pageElementId = experimentListConfig.editPageFromExp._id;
                // }

                for (var i = 0; i < experimentListConfig.pageElements.length; i++) {
                  pageElementIds += experimentListConfig.pageElements[i]._id;
                  if((i+1) !== experimentListConfig.pageElements.length){
                    pageElementIds += ',';
                  }
                }
                if(form.validate()){
                  var url;
                  url = experimentListConfig.apis.create;
                  if (mode === 'edit') {
                    url = experimentListConfig.apis.update;
                  }
                  // adding parent folder id
                  if (mode === 'edit') {
                    form.setValues({
                      source: experimentListConfig.folderManager ? experimentListConfig.folderManager.getCurrentFolder() : '',
                      _id: experimentListConfig.allFormData.basic.tid,
                      page_element_ids: pageElementIds,
                    }, true);
                  } else {
                    form.setValues({
                      source: experimentListConfig.folderManager ? experimentListConfig.folderManager.getCurrentFolder() : '',
                      page_element_ids: pageElementIds,
                    }, true);
                  }
                  var formValues = form.getValues();
                  formValues.ajax = true;

                  formValues.replicateGoal = experimentListConfig.replicateGoal;
                  formValues.replicateSegment = experimentListConfig.replicateSegment;

                  webix.ajax().post(url, formValues, {
                    error: function(text, data, XmlHttpRequest) {
                      btnCtrl.enable();
                      alert("error");
                    },
                    success: function(text, data, XmlHttpRequest) {
                      btnCtrl.enable();
                      var response = JSON.parse(text);
                      if (response.success == true) {
                        config.currentElementID = 'webixFilemanagerExperiment';
                        openLayoutExperiment();
                        webix.message(response.msg);
                      }
                    }
                  });
                } else {
                  btnCtrl.enable();
                  webix.message({type: 'error', text: "Fill all the required fields correctly."})
                }
                isSessional = false;
                userStateData = '';
              }
            }, {}, {}]
          }]
        });
        if (experimentListConfig.activeStep === experimentListConfig.defaultSteps[2]) {
          experimentListConfig.highLightCurrentStep("confirm");
        }
        if (mode === 'edit') {
          if (experimentListConfig.activeStep !== experimentListConfig.defaultSteps[2]) {
            $$("confirmStepExperiment").hide();
          }

          if(userStateData && isSessional){
            if(userStateData.data.confirmData){
              $$("confirmFormExperiment").setValues(userStateData.data.confirmData);
            }
          }
        } else {
          guidelines.startSubGuideline('confirm');
          if(userStateData && isSessional){
            if(userStateData.data.confirmData){
              $$("confirmFormExperiment").setValues(userStateData.data.confirmData);
            }
          }
        }
        $$("wizardConfirmBtnExperiment").enable();
        isSessional = false;
        userStateData = '';
      });


    },
    updateFile: function (id, src) {
      config.currentElementID = 'webixWizardHeaderMenuExperiment';
      var url = experimentListConfig.apis.show;

      // retreiving form data and setting into the form
      var formData;
      webix.ajax().get(url, {
        _id: id,
        ajax: true,
      }, function(text, xml, xhr) {
        //response
        text = JSON.parse(text);
        // experimentListConfig.pageElementDocumentEdit = text.pageElement;
        formData = text.experiment;
        experimentListConfig.experiment = formData;
        experimentListConfig.openWizard('edit', formData, src);
      });
    },
    deleteFile: function (id) {
      webix.confirm({
        text: "Do you want to delete?",
        ok: "Yes",
        cancel: "No",
        callback: function(result) {
          if (result) {
            webix.ajax().post(experimentListConfig.apis.delete, {
              _id: id,
              ajax: true
            }, {
              error: function(text, data, XmlHttpRequest) {
                alert("error");
              },
              success: function(text, data, XmlHttpRequest) {
                var response = JSON.parse(text);
                if (response.success == true) {
                  experimentListConfig.folderManager.deleteFile(id);
                }
              }
            });
          }
        }
      });

    },
    updateHidden: function (params) {
      // var params = {source: id, action: action, hidden: hidden};
      if (params.hidden == 1) {
        var message = webix.message("deactivating...");
      } else if (params.hidden == 0) {
        var message = webix.message("activating...");
      }
      params.ajax = true;

      webix.ajax().post(experimentListConfig.apis.update_status, params, {
        error: function(text, data, XmlHttpRequest) {
          alert("error");
        },
        success: function(text, data, XmlHttpRequest) {
          var response = JSON.parse(text);
          if (response.success == true && params.action == "update_hidden") {
            var element = document.getElementById("updateHidden_" + params._id)
            if (params.hidden == 0) {
              element.innerHTML = "active";
              element.style.color = "green";
              element.onclick = function() {
                params.hidden = 1;
                experimentListConfig.updateHidden(params);
              };
              webix.message("Experiment has been activated.");
            } else if (params.hidden == 1) {
              element.innerHTML = "inactive";
              element.style.color = "red";
              element.onclick = function() {
                params.hidden = 0;
                experimentListConfig.updateHidden(params);
              };
              webix.message("Experiment has been deactivated.");
            }
            experimentListConfig.refreshManager();
            webix.message.hide(message);
          }
        }
      });
    },
    refreshManager: function () {
      experimentListConfig.folderManager.clearAll();
      experimentListConfig.folderManager.load(experimentListConfig.apis.list);
    },
    fab: {
      actionButtonTypes: {
    		type: "create",
    		setType: function(t){
    			t = t || "create";
    			experimentListConfig.fab.actionButtonTypes.type = t;
    		},
    		fabOptions: false,
    		create: {
    			options: [
            // {
    				// 	label: "This is to test",
    				// 	className: "segmentBuilder inline",
            //   callback: function(){
            //     webix.alert("This is to test......");
            //   }
  				  // }
    			]
    		},
    	},
      fabOptions: false,
      primary: {
        primaryBtnLabelCallback: function(actionButton, floatingTextBG){
          actionButton.className += " createExperiment inline";
          floatingTextBG.textContent = "Create experience";
          actionButton.onclick = function(){
              config.currentElementID = 'webixWizardHeaderMenuExperiment';
              guidelines.removeHangedTooltips();
              experimentListConfig.openWizard('add');
          };
        },
      }


    },
    postMessageListener: function(event) {
      console.log('Check Event =>', event);
      var data = JSON.parse(event.data);
      switch (data.t) {
        case "pw-fullscreen-full":
          $("#wizardPlacementSectionIdExperiment").addClass("fullscreenModeMainCont");
          break;
        case "pw-fullscreen-exit":
          $("#wizardPlacementSectionIdExperiment").removeClass("fullscreenModeMainCont");
          break;
        case "set-current-experiment":
          if(data.created){
            experimentListConfig.experiment = data.experiment;
          }
          break;
        case "page-published-" + experimentListConfig.page:

          if(data.source === 'experiment'){
            isSessional = false;
            userStateData = '';
            // webix.message(data.message);
            experimentListConfig.pageElementDocumentEdit = {};
            // experimentListConfig.next(data.temp.mode, data.temp.formData);
            experimentListConfig.showView(false, true, false, data.temp.mode, data.temp.formData);
          }
          break;
        case "set-active-step-sessional":
          experimentListConfig.currentStepInfo.step = data.step;
          experimentListConfig.currentStepInfo.url = data.url;
          break;
        default:
          break;
      }
    },
    createNewSegment: function(mode, formData){
      $("#innerContent").hide();
      $.getScript(config.segment.script, function() {
        var mainContainer2 = document.getElementById("mainContainer");
        if ($("#segmentsRefExperiment").length > 0) {
          $("#segmentsRefExperiment").html("");
        }

        var innerContent2 = document.createElement('div');
        innerContent2.className = 'innerContent2';
        innerContent2.id = 'segmentsRefExperiment';

        mainContainer2.appendChild(innerContent2);

        openWizardSegment('add', undefined, {
          reference: 'experiment',
          container: 'segmentsRefExperiment',
          oldFormData: [mode, formData, $$("segmentFormExperiment").getValues()],
          currentFolder: experimentListConfig.folderManager ? experimentListConfig.folderManager.getCurrentFolder() : '',
        });
      });
    },
    createNewGoal: function(mode, formData){
      $("#innerContent").hide();
      $.getScript(config.goals.script, function() {
        var mainContainer2 = document.getElementById("mainContainer");
        if ($("#goalsRefExperiment").length > 0) {
          $("#goalsRefExperiment").html("");
        }

        var innerContent2Goal = document.createElement('div');
        innerContent2Goal.className = 'innerContent2';
        innerContent2Goal.id = 'goalsRefExperiment';

        mainContainer2.appendChild(innerContent2Goal);

        openWizardGoal('add', undefined, {
          reference: 'experiment',
          container: 'goalsRefExperiment',
          oldFormData: [mode, formData, $$("segmentFormExperiment").getValues()],
          currentFolder: experimentListConfig.folderManager ? experimentListConfig.folderManager.getCurrentFolder() : '',
        });
      });
    },
    editSegment: function(segmentID, mode, formData){
      $("#innerContent").hide();
      $.getScript(config.segment.script, function() {
        console.log('test');
        var mainContainer2 = document.getElementById("mainContainer");
        if ($("#segmentsRefExperiment").length > 0) {
          $("#segmentsRefExperiment").html("");
        }

        var innerContent2 = document.createElement('div');
        innerContent2.className = 'innerContent2';
        innerContent2.id = 'segmentsRefExperiment';

        mainContainer2.appendChild(innerContent2);

        // create duplicate
        var duplicateEndpoint = monolop_api_base_url + '/api/segment/' + segmentID + '/duplicate';
        webix.ajax().post(duplicateEndpoint, {ajax: true}, function(dupResponse, dupXml, dupXhr){
          var duplicateRecord = JSON.parse(dupResponse).obj;
          var url = monolop_api_base_url+ '/api/segments/show';

          // retreiving form data and setting into the form
          var formDataSegment;
          webix.ajax().get(url, {
            _id: '__' + duplicateRecord._id,
            ajax: true
          }, function(text, xml, xhr) {
            //response
            formDataSegment = JSON.parse(text);
            openWizardSegment('edit', formDataSegment, {
              reference: 'experiment',
              container: 'segmentsRefExperiment',
              oldFormData: [mode, formData, $$("segmentFormExperiment").getValues()],
              currentFolder: experimentListConfig.folderManager ? experimentListConfig.folderManager.getCurrentFolder() : '',
            });
          });
        });
      });
    },
    editGoal: function(goalID, mode, formData){
      $("#innerContent").hide();
      $.getScript(config.goals.script, function() {
        var mainContainer2 = document.getElementById("mainContainer");
        if ($("#segmentsRefExperiment").length > 0) {
          $("#segmentsRefExperiment").html("");
        }

        var innerContent2 = document.createElement('div');
        innerContent2.className = 'innerContent2';
        innerContent2.id = 'goalsRefExperiment';

        mainContainer2.appendChild(innerContent2);

        // create duplicate
        var duplicateEndpoint = monolop_api_base_url + '/api/goals/' + goalID + '/duplicate';
        webix.ajax().post(duplicateEndpoint, {ajax: true}, function(dupResponse, dupXml, dupXhr){
          var duplicateRecord = JSON.parse(dupResponse).obj;
          var url = monolop_api_base_url+ '/api/goals/show';

          // retreiving form data and setting into the form
          var formDataGoal;
          webix.ajax().get(url, {
            _id: '__' + duplicateRecord._id,
            ajax: true
          }, function(text, xml, xhr) {
            //response
            formDataGoal = JSON.parse(text);
            openWizardGoal('edit', formDataGoal, {
              reference: 'experiment',
              container: 'goalsRefExperiment',
              oldFormData: [mode, formData, $$("segmentFormExperiment").getValues()],
              currentFolder: experimentListConfig.folderManager ? experimentListConfig.folderManager.getCurrentFolder() : '',
            });
          });
        });
      });
    }
  };

  function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
      tokens,
      re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
      params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
  };

  function openLayoutExperiment() {
    //include url from params
    var params = getQueryParams(document.location + '');
    if(params.url){
      experimentListConfig.apis.list = experimentListConfig.apis.list + '&url=' + params.url ;
      experimentListConfig.apis.files = experimentListConfig.apis.files + '&url=' + params.url ;
    }

    var f = window.location.hash;
    window.history.pushState(config.experiment.headerName, config.experiment.headerName, config.experiment.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);

    experimentListConfig.pageElementDocumentEdit = {};
    experimentListConfig.experiment = {};
    $("#innerContent").html("");

    var formElement = document.createElement('div');
    formElement.id = experimentListConfig.formElement.id;
    formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
    document.getElementById("innerContent").appendChild(formElement);

    // webix.ready(function() {

    experimentListConfig.folderManager = webix.ui({
      container: experimentListConfig.formElement.id,
      view: "filemanager",
      url: experimentListConfig.apis.list, // loading data from the URL
      id: "webixFilemanagerExperiment",
      filterMode: {
        showSubItems: true,
        openParents: true
      },
      editable:true,
      editor:"text",
      editValue:"value",
      mode: "table", // specify mode selected by default
      // modes: ["files", "table"], // all available modes including a new mode
      modes: ["table"],
      // modes: ["table"],
      // save and handle all the menu actions from here,
      // disable editing on double-click
      handlers: {
        "upload": experimentListConfig.apis.create,
        // "download": "data/saving.php",
        // "copy": "data/saving.php",
        "move": "/api/folders/move",
        "remove": "/api/folders/delete",
        "rename": "/api/folders/update",
        "create": "/api/folders/create",
        "files": experimentListConfig.apis.files,
      },
      on: {
        "onViewInit": function(name, config) {
          if (name == "table" || name == "files") {

            // disable multi-selection for "table" and "files" views
            config.select = true;

            if (name == "table") {

              // disable editing on double-click
              config.editaction = false;

              // an array with columns configuration
              var columns = config.columns;
              //  disabling columns date, type, size
              columns.splice(1, 3);

              // configuration of a new column segment
              // var segmentColumnExperiment = {
              //   id: "segmentColumnExperiment",
              //   header: "Audience",
              //   fillspace: 2,
              //   template: function(obj, common) {
              //     return obj.segment || ""; // "description" property of files
              //   }
              // };

              // configuration of a new column goal
              // var goalColumnExperiment = {
              //   id: "goalColumnExperiment",
              //   header: "Goal",
              //   fillspace: 2,
              //   template: function(obj, common) {
              //     return obj.goal || ""; // "description" property of files
              //   }
              // };

              // configuration of a new column date
              // var dateColumnExperiment = {
              //   id: "dateColumnExperiment",
              //   header: "Date",
              //   sort:"string",
              //   fillspace: 2,
              //   sort: function(a, b){
              //     var _a = new Date(a.date), _b = new Date(b.date);
              //     return (_a > _b ? 1 : (_b > _a ? -1: 0));
              //   },
              //   template: function(obj, common) {
              //     return obj.date || ""; // "description" property of files
              //   }
              // };

              // configuration of a new column actions
              // var reportColumnExperiment = {
              //   id: "reportColumnExperiment",
              //   header: "Report",
              //   width: 70,
              //   template: function(obj, common) {
              //     var url  = '/reports/experiment/detail/' + obj.experimentID + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURIComponent(params.url)) : '');
              //     return '<a   onclick="window.location = \'' + url + '\';" title="Experience report" class="webix_list_item viewReport" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-bar-chart"></span></a>';
              //   }
              // };

              // configuration of a new column actions
              // var actionsColumnExperiment = {
              //   id: "actionsColumnExperiment",
              //   header: "Actions",
              //   width: 100,
              //   template: function(obj, common) {
              //
              //     return '<a  title="Edit experience" onclick="experimentListConfig.updateFile(\'' + obj.id + '\');" class="webix_list_item updateExperiment" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" onclick="experimentListConfig.deleteFile(\'' + obj.id + '\');" class="webix_list_item deleteExperiment" title="Delete experiment" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
              //   }
              // };

              // changes related to Mantis # 3974
              var visitorsColumnExperiment = {
                id: "visitorsColumnExperiment",
                header: "Visitors",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.visitors || 0; // "description" property of files
                }
              };



              var conversionsColumnExperiment = {
                id: "conversionsColumnExperiment",
                header: "Conversions",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.conversions || 0; // "description" property of files
                }
              };



              var conversionsRateColumnExperiment = {
                id: "conversionsRateColumnExperiment",
                header: "CR%",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.conversion_rate || 0; // "description" property of files
                }
              };



              var pvalueColumnExperiment = {
                id: "pvalueColumnExperiment",
                header: "P-Value",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.p_value || 0; // "description" property of files
                }
              };

              var changeColumnExperiment = {
                id: "changeColumnExperiment",
                header: "Change (%)",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.change || 0; // "description" property of files
                }
              };

              // configuration of a new column actions
              var actionsColumnExperiment = {
                id: "actionsColumnExperiment",
                header: "Actions",
                width: 150,
                template: function(obj, common) {
                  var url  = '/reports/experiment/detail/' + obj.experimentID + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURIComponent(params.url)) : '');
                  return '<a  title="Edit experience" onclick="experimentListConfig.updateFile(\'' + obj.id + '\');" class="webix_list_item updateExperiment" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" onclick="experimentListConfig.deleteFile(\'' + obj.id + '\');" class="webix_list_item deleteExperiment" title="Delete experiment" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>'+ ' | ' + '<a  onclick="window.location = \'' + url + '\';" class="webix_list_item viewReport" title="Experience report" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-bar-chart"></span></a>';
                }
              };

              // configuration of a new column status
              var statusColumnExperiment = {
                id: "statusColumnExperiment",
                header: "Status",
                sort: function(a, b){
                  var _a = Number(a.hidden), _b = Number(b.hidden);
                  return (_a > _b ? 1 : (_b > _a ? -1: 0));
                },
                width: 80,
                template: function(obj, common) {
                  var obj_ = {
                    action: "update_hidden",
                    hidden: "1"
                  };
                  var params = {
                    _id: obj.id,
                    action: "update_hidden",
                    node: this
                  };
                  if (obj.hidden == 0) {
                    params.hidden = 1;
                    return '<a id="updateHidden_' + obj.id + '" title="Activate/Deactivate experience"  onclick=\'experimentListConfig.updateHidden( ' + JSON.stringify(params) + ');\' class="webix_list_item updateExperimentStatus  status status0" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actions" property of files ;
                  }
                  params.hidden = 0;
                  return '<a id="updateHidden_' + obj.id + '" title="Activate/Deactivate experience"  onclick=\'experimentListConfig.updateHidden(' + JSON.stringify(params) + ');\' class="webix_list_item updateExperimentStatus status status1" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
                }
              };

              // insert columns
              // webix.toArray(columns).insertAt(segmentColumnExperiment, 1);
              // webix.toArray(columns).insertAt(goalColumnExperiment, 2);
              // webix.toArray(columns).insertAt(dateColumnExperiment, 3);
              // webix.toArray(columns).insertAt(reportColumnExperiment,4)
              // webix.toArray(columns).insertAt(actionsColumnExperiment, 5);
              // webix.toArray(columns).insertAt(statusColumnExperiment, 6);

              // changes related to Mantis # 3974
              webix.toArray(columns).insertAt(visitorsColumnExperiment, 1);
              webix.toArray(columns).insertAt(conversionsColumnExperiment, 2);
              webix.toArray(columns).insertAt(conversionsRateColumnExperiment, 3);
              webix.toArray(columns).insertAt(pvalueColumnExperiment,4);
              webix.toArray(columns).insertAt(changeColumnExperiment,5);
              webix.toArray(columns).insertAt(actionsColumnExperiment, 6);
              webix.toArray(columns).insertAt(statusColumnExperiment, 7);
            }
          }
        }
      }
    });

    /*******************************Menu Customization******************************/
    // only FILES mode should be available
    $$('webixFilemanagerExperiment').$$('modes').hide();

    // updating options from menu

    var actions = $$("webixFilemanagerExperiment").getMenu();
    actions.clearAll();
    var newData = [

      {
        id: "create",
        method: "createFolder",
        icon: "folder-o",
        value: webix.i18n.filemanager.create // "Create Folder"
      }, {
        id: "deleteFile",
        method: "deleteFile",
        icon: "times",
        value: webix.i18n.filemanager.remove // "Delete"
      }, {
        id: "edit",
        method: "editFile",
        icon: "edit",
        value: webix.i18n.filemanager.rename // "Rename"
      }
    ];
    actions.parse(newData);

    // add new option for the menu to add new segment
    actions.add({
      id: "createExperiment",
      icon: "file",
      value: "Create Experience"
    });

    /*******************************Segment Add**********************************/

    actions.attachEvent("onItemClick", function(id) {
      this.hide();
      // check if the action is createExperiment
      if (id == "createExperiment") {
        config.currentElementID = 'webixWizardHeaderMenuExperiment';
        experimentListConfig.openWizard('add');
      }
    });

    actions.getItem("create").batch = "item, root";
    actions.getItem("deleteFile").batch = "item, root";
    actions.getItem("edit").batch = "item, root";
    actions.getItem("createExperiment").batch = "item, root";

    $$("webixFilemanagerExperiment").$$("table").attachEvent("onBeforeSelect", function(id, p){
      var rowName = id.row,
          columnName = id.column;
      var item = this.getItem(rowName);

      if (item.type !== "folder"){
        if(['reportColumnExperiment', 'actionsColumnExperiment', 'statusColumnExperiment'].indexOf(columnName) === -1){
        //  experimentListConfig.updateFile(item.id);
        }
      }
      //note that 'id' for datatable is an object with several attributes
    });

    $$("webixFilemanagerExperiment").attachEvent("onBeforeDeleteFile",function(id){
        // Fix: mantis # 3945
        if (id.indexOf('experiment__') === -1) {
  				var item = this.getItem(id);
  				if(item.$parent === 0){
  					webix.message({type: "error", text: "Root folder can not be deleted."})
  					return false;
  				}else{
  					checkFolderStatus(id, experimentListConfig.folderManager);
  					return false;
  				}
  			}
    });

    // before editing file
    $$("webixFilemanagerExperiment").attachEvent("onBeforeEditFile", function(id) {

      var srcTypeId = id.split('__');

      if (srcTypeId[0] == 'folder') {
        return true;
      }

      experimentListConfig.updateFile(id);

      return false;
    });

    // reload grid after folder creation
    $$("webixFilemanagerExperiment").attachEvent("onAfterCreateFolder", function(id) {
      // experimentListConfig.refreshManager();
      setTimeout(function(){ openLayoutExperiment(); }, 500);
      return false;
    });

    // it will be triggered before deletion of file
    $$("webixFilemanagerExperiment").attachEvent("onBeforeDeleteFile", function(ids) {
      // experimentListConfig.deleteFile(ids);
      if (ids.indexOf('experiment__') === -1) {
        return true
      }

      experimentListConfig.deleteFile(ids);
      return false;
    });

    // it will be triggered before dragging the folder/segment
    $$("webixFilemanagerExperiment").attachEvent("onBeforeDrag", function(context, ev) {
      return true;
    });

    $$("webixFilemanagerExperiment").attachEvent("onBeforeDrop", function(context, ev) {
      if (context.start.indexOf('experiment__') === -1) {
        return true
      }
      webix.ajax().post("/api/experiments/change-folder", {
        _id: context.start,
        target: context.target,
        ajax: true
      }, {
        error: function(text, data, XmlHttpRequest) {
          alert("error");
        },
        success: function(text, data, XmlHttpRequest) {
          var response = JSON.parse(text);
          if (response.success == true) {}
        }
      });
      return true;
    });

    // it will be triggered after dropping the folder to the destination
    $$("webixFilemanagerExperiment").attachEvent("onAfterDrop", function(context, ev) {
      webix.message("Experiment has been moved to new folder.");
      return false;
    });

    // $$("webixFilemanagerExperiment").$$("files").attachEvent("onBeforeRender", filterFilesExperiment);
    $$("webixFilemanagerExperiment").$$("table").attachEvent("onBeforeRender", filterFilesExperiment);

    var _exe = false;
    $$("webixFilemanagerExperiment").attachEvent("onAfterLoad", function(context, ev) {
      if(_exe === false){
        _exe = true;
        initIntialTTandGuides();
      }
    });

    // fix: mantis # 3946
    $$("webixFilemanagerExperiment").attachEvent("onSuccessResponse", function(req, res) {
    	if(res.success && (req.source === "newFolder" || req.source.indexOf("folder") != -1)){
    		switch(req.action){
    			case "rename":
    				webix.message("Folder renamed");
    				break;
    			case "create":
    				webix.message("Folder Created");
    				break;
    			case "remove":
    				webix.message("Folder deleted");
    				break;
    		}
    	}
    });

    /*******************************Custom Events End****************************/
    // });
    fab.initActionButtons({
      type: "experimentList",
      actionButtonTypes: experimentListConfig.fab.actionButtonTypes,
      primary: experimentListConfig.fab.primary,
    });

    $(window).on('resize', function(){
      // console.log($$('webixFilemanagerExperiment'));
      $$(config.currentElementID).define('width', $('body').width() - $('.sidebar').width() - 10);
      $$(config.currentElementID).resize();
      if($$('wizardPlacementSectionIdExperimentPagesList')){
        $$('wizardPlacementSectionIdExperimentPagesList').define('width', $('body').width() - $('.sidebar').width() - 10);
        $$('wizardPlacementSectionIdExperimentPagesList').resize();
      }
      if($$('placementStepExperiment')){
        $$('placementStepExperiment').define('width', $('body').width() - $('.sidebar').width() - 10);
        $$('placementStepExperiment').resize();
      }

    });

  }


  global.openLayoutExperiment = openLayoutExperiment;
  global.experimentListConfig = experimentListConfig;
  global.page = experimentListConfig.page;
  global.placementWindowIframe = undefined;
})(window);
// insert postmessageListener ;
if (window.addEventListener) {
  addEventListener("message", experimentListConfig.postMessageListener, false);
} else {
  attachEvent("onmessage", experimentListConfig.postMessageListener);
}
// filter segments only
function filterFilesExperiment(data) {
  data.blockEvent();
  data.filter(function(item) {
    return item.type != "folder"
  });
  data.unblockEvent();
}
