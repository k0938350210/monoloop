var isSessional = false;

// ZeroClipboard.setDefaults({
//       moviePath: "/zeroclipboard1.2.3/ZeroClipboard.swf"
// });

var apiTokenForm = [
			{ view:"text", css: 'host', label:'Host', name:"host", tooltip: "Click to copy Host", click: function() {

				var copyTextarea = document.querySelector('.host input');
			  copyTextarea.select();

			  try {
			    var successful = document.execCommand('copy');
					webix.message("Host Copied");
			  } catch (err) {
			    alert('Oops, unable to copy');
			  }
			}},
			{ view:"text", css: 'account_id', label:'Account ID', name:"account_id"},
      { view:"textarea", css: 'api_token', label:'API Token', name:"api_token", id: "api_token", height:200},
      { view:"button", value: "Copy API Token", css: "copy_api_token orangeBtn", id: "copy_api_token", width: 150, click:function(){
				var copyTextarea = document.querySelector('.api_token textarea');
			  copyTextarea.select();

			  try {
			    var successful = document.execCommand('copy');
					webix.message("Copied.");
			  } catch (err) {
			    alert('Oops, unable to copy');
			  }
      }}

		];


function openLayout()
{

  if(userStateData){
    isSessional = userStateData.section === 'account-api-token';
  }

  var formElement = document.createElement('div');
  formElement.id = 'webixapiTokenForm';

  document.getElementById("innerContent").appendChild(formElement);

  webix.ui({
			view:"form", scroll:false,
      id: "apiTokenForm",
			container: "webixapiTokenForm",
			elements: apiTokenForm,
			width: $('body').width() - $('.sidebar').width(),
			margin:3,
			elementsConfig:{
				labelPosition: "top",
				labelWidth: 140,
				bottomPadding: 18
			}

		});

    $("div[view_id=api_token] textarea").attr('id', 'api_token_text_id');

		var copyBtn = $("div[view_id=copy_api_token] button");
		copyBtn.attr('id', 'api_token_text_btn_id');
    copyBtn.attr('class', 'copy-clipboard');
		copyBtn.attr('data-clipboard-target', '#api_token_text_id');
    copyBtn.attr('data-clipboard-action', 'copy');

    webix.ajax().get(monolop_api_base_url+"/api/account?ajax=true", function(res){

        res  = JSON.parse(res);

        $$("apiTokenForm").setValues({
          host: res.apihost,
          account_id: res.uid,
          api_token: res.apitoken
        }, true);

        if(isSessional && userStateData){
          if(userStateData.data.basicData){
            $$("apiTokenForm").setValues(userStateData.data.basicData);
          }
        }
        isSessional = false;
        userStateData = '';

        initIntialTTandGuides();

        copyBtn.attr('data-clipboard-text', res.apitoken);
        // new Clipboard('.copy-clipboard');
    });

		$(window).on('resize', function(){
      $$('apiTokenForm').define('width', $('body').width() - $('.sidebar').width());
      $$('apiTokenForm').resize();
    });
}
