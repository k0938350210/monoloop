var isSessional = false;
var controlGroup = [
			{ view:"checkbox", css: 'enable', label:'Enable control group', name:"enable"},
			{ view:"text", css: 'size', label:'Size (%) of the control group', name:"size", invalidMessage: "The size is not valid." },
      { view:"text", css: 'days', label:'No. of days in control group?', name:"days", invalidMessage: "No. of days are not valid."},
			{ view:"text", type:'hidden', name:"enable-ab"},
			{ view:"button", value: "Save", css: "save orangeBtn", width: 70, click:function(){
				var form = this.getParentView();
				if (form.validate()){

					if($$("enable").getValue()){
						$$("enable-ab").setValue("on");
					}
          webix.ajax().post(monolop_api_base_url+"/api/account/controllgroup?ajax=true", form.getValues(), function(res, data, xhr){
            res  = JSON.parse(res);
            if(res.success === true){
              webix.message("Changes saved successfully.");
            }
          });
				}
			}}
		];
function openLayout()
{
  var formElement = document.createElement('div');
  formElement.id = 'webixControlGroupElement';

	if(userStateData){
		isSessional = userStateData.section === 'account-control-group';
	}

  document.getElementById("innerContent").appendChild(formElement);

  webix.ui({
			view:"form", scroll:false,
      id: "controlGroup",
			container: "webixControlGroupElement",
			elements: controlGroup,
			width: 350,
			margin:3,
			rules:{
        "size": webix.rules.isNumber,
				"size": function(value){
					var s = parseInt(value);
					if(s >= 0 && s <= 100 ){
						return true;
					}
					$$("controlGroup").elements.size.define("invalidMessage", "The size is not valid.");
					return false;
				},
        "days": webix.rules.isNumber,
			},
			elementsConfig:{
				labelPosition: "top",
				labelWidth: 140,
				bottomPadding: 18
			}

		});

		webix.ajax().get(monolop_api_base_url+"/api/account?ajax=true", function(res){

				res  = JSON.parse(res);
				if(res.control_group){
					$$("controlGroup").setValues({
						enable: res.control_group.enabled,
						size: res.control_group.size,
						days: res.control_group.days
					}, true);
				}

				if(isSessional && userStateData){
					if(userStateData.data.basicData){
						$$("controlGroup").setValues(userStateData.data.basicData);
					}
				}
				isSessional = false;
				userStateData = '';

				initIntialTTandGuides();
		});

}
