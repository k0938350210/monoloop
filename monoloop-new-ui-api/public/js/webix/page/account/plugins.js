

function openLayout(){
  var formElement = document.createElement('div');
  formElement.id = 'webixpluginsElement';

  document.getElementById("innerContent").appendChild(formElement);

  webix.ajax().post(monolop_api_base_url+"/api/account/plugins?ajax=true", function(res, data, xhr){
      res  = JSON.parse(res);

      var pluginData = [];

      if(res.totalCount > 0) {
        for (var i = 0; i < res.topics.length; i++) {
          console.log(res.topics);
          pluginData.push({
            id: i+1,
            pluginId: res.topics[i]._id,
            pluginName: res.topics[i].plugin.name,
            statusName: (res.topics[i].enabled == false) ? 'deactivate': 'activate',
            status: (res.topics[i].enabled == false) ? 0: 1
          });
        }

        webix.ui({
          container: "webixpluginsElement",
          id: "pluginDatatable",
          view:"datatable", select:false, editable:false, height: 600,
          columns:[
            {id:"id", header:"#", width:50},
            {id:"pluginName", header:["Plugin name", {content:"textFilter"} ], sort:"string", minWidth:'70%', fillspace: 2, editor:"text"},
            {id:"statusName", header:["Status"], minWidth:'25%', minWidth:'25%', fillspace: 1, template:"<span style='color:black;' class='updateStatus status status#status#'>#statusName#</span>"},
          ],
          // pager:"pagerA",
          data: pluginData,
          onClick:{
            "updateStatus":function(e,id,node){
              console.log(id);
          //    webix.confirm({
            //    text:"Are you sure?", ok:"Yes", cancel:"Cancel",
            //    callback:function(res){
            //      if(res){
                    var item = webix.$$("pluginDatatable").getItem(id);


                    var enabled;

                    if(item.statusName === 'activate'){
                      enabled = 0;
                      item.status = 0;
                      item.statusName = "deactivate";
                    } else {
                      enabled = 1;
                      item.status = 1;
                      item.statusName = "activate";
                    }

                    webix.ajax().post(monolop_api_base_url+"/api/account/plugins/"+ item.pluginId + '?ajax=true', {enabled: enabled}, function(res, data, xhr){
                      res = JSON.parse(res);
                      if(res.success === true){
                        webix.$$("pluginDatatable").refresh(id);
                      }
                    });


                //  }
              //  }
            //  });
            }
          },
        });

      } else {
        webix.ui({
          container: "webixpluginsElement",
          view: "label",
          label: "No plugins has been added yet."
        });
      }

      initIntialTTandGuides();
  });





}
