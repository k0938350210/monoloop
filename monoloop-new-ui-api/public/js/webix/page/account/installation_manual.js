function openLayout(){
  webix.ready(function(){
    webix.ui({
      view:"iframe"
      , id:"frame-body",
      container: 'innerContent',
       src:"https://docs.google.com/document/d/1zhsLuI4oFno_uVO-DXv5T8lkITcBvkXaK5y2YnRitS8/pub?embedded=true"
    });

    $(window).on('resize', function(){  		
  		$$('frame-body').define('width', $('body').width() - $('.sidebar').width());
  		$$('frame-body').resize();
  	});
  });
}
