
var domainActions = ['Automaticly add notificationtab to page', 'Manually add link to privacy center'];
var accountTypes = [{id:'trail', value: 'trail' }, {id:'free', value: 'free' }];
var _form = [
			{ view: "text", label: 'First name', css: "ip_firstname", name: "firstname", invalidMessage: "First name can not be empty." },
			{ view: "text", label: 'Last name', css: "ip_lastname", name: "lastname", invalidMessage: "Last name can not be empty." },
			{ view: "text", label: 'Email', css: "ip_email", name: "email", invalidMessage: "Incorrect e-mail address" },
			{ view: "text", label: 'Confirm email', css: "ip_confirmEmail", name: "confirmEmail", invalidMessage: "Incorrect e-mail address" },
			{ view:"text", label:'Company', css: "ip_company", name:"company", invalidMessage: "Company name can not be empty."},
			{ view:"checkbox", label:'Invoice through agency', css: "ip_invoice_through_agency", value: 1, name:"invoice_through_agency", labelAlign: 'left'},
      {view:"richselect", label:"Account type", css: "ip_account_type", id: "account_type", name: "type", value: "free", options: accountTypes, labelAlign:'left', hidden: true},
		];


var _formRules = {
  "firstname": webix.rules.isNotEmpty,
	"lastname": webix.rules.isNotEmpty,
  "company": webix.rules.isNotEmpty,
	"email": function(value){
			value = value.trim();
			if(value === ''){
				 $$("_form").elements.email.define("invalidMessage", "Email can not be empty.");
				 return false;
			}
			if(!validateEmail(value)){
				 $$("_form").elements.email.define("invalidMessage", "Incorrect e-mail address.");
				 return false;
			}
			if(value !== $$("confirmEmail").getValue()){
				$$("_form").elements.email.define("invalidMessage", "Email does not match with confirm email.");
				return false
			}
		 return true;
	 },
	"confirmEmail": function(value){
		value = value.trim();
		if(value === ''){
			 $$("_form").elements.confirmEmail.define("invalidMessage", "Confirm email can not be empty.");
			 return false;
		}
		if(!validateEmail(value)){
			 $$("_form").elements.confirmEmail.define("invalidMessage", "Incorrect confirm e-mail address.");
			 return false;
		}
		if(value !== $$("email").getValue()){
			$$("_form").elements.confirmEmail.define("invalidMessage", "Confirm email does not match with email.");
			return false
		}
	 return true;
	},
};

var extendedFab = {
  actionButtonTypes: {
    type: "create",
    setType: function(t){
      t = t || "create";
      extendedFab.actionButtonTypes.type = t;
    },
    fabOptions: false,
    create: {
      options: [
        // {
        // 	label: "This is to test",
        // 	className: "segmentBuilder inline",
        //   callback: function(){
        //     webix.message("This is to test......");
        //   }
        // }
      ]
    },
  },
  fabOptions: false,
  primary: {
    primaryBtnLabelCallback: function(actionButton, floatingTextBG){
      actionButton.className += " createUserAccount inline";
      floatingTextBG.textContent = "Create account";
      actionButton.onclick = function(){
          openClientAccountPopup('new', undefined, undefined);
      };
    },
  }
};

function openClientAccountPopup(source, id, data){
  var form;

  var addButton = [
    { view:"button", value: "Save", id: "submitClientAccountForm", css: "orangeBtn saveBtn", align:"right", width: 150, click:function(){
        var form = this.getParentView();
        if (form.validate()){

          webix.ajax().post(monolop_api_base_url+"/api/client-account?ajax=true", form.getValues(), function(res, data, xhr){
            res  = JSON.parse(res);
            if(res.success === true){

							if(res.email === true){
								webix.ajax().post(monolop_api_base_url+"/api/client-account/invite/" + res.account._id + '?ajax=true', {}, function(ires, idata, ixhr){
									// ires  = JSON.parse(ires);
								});
							}

              $$('clientAccountDatatable').add({
                id: $$('clientAccountDatatable').getLastId() + 1,
								_id: res.account._id,
								company: res.account.company,
								contact_email: res.account.contact_email,
								contact_name: res.account.contact_name,
								type: (res.account.invoice_reseller == true) ? "Yes" : "No",
								invoice_through_agency: (res.account.invoice_reseller == true) ? 1 : 0,
								name: res.account.name,
								statusName: (res.account.hidden == 1) ? 'deactivate': 'activate',
								status: res.account.hidden,
              }, $$('clientAccountDatatable').getLastId() + 1);


              $$('createNewClientAccountPopup').close();
              webix.message("Client added successfully <br /> and invitation has been sent.");
            } else {
              webix.message({type:"error", text:res.msg});
            }
          });


        }
      }}
  ];
  var editButton = [
    { view:"button", value: "Save", id: "submitClientAccountForm", css: "orangeBtn saveBtn", align:"right", width: 150, click:function(){
        var form = this.getParentView();
        if (form.validate()){
          var item = webix.$$("clientAccountDatatable").getItem(id);
          webix.ajax().put(monolop_api_base_url+"/api/client-account/" + item._id + '?ajax=true', form.getValues(), function(res, data, xhr){
            res  = JSON.parse(res);
            if(res.success === true){

							if(res.email === true){
								webix.ajax().post(monolop_api_base_url+"/api/client-account/invite/" + res.account._id + '?ajax=true', {}, function(ires, idata, ixhr){
									// ires  = JSON.parse(ires);
								});
							}

              var item = webix.$$("clientAccountDatatable").getItem(id);

							item._id = res.account._id;
							item.company = res.account.company;
							item.contact_email = res.account.contact_email;
							item.contact_name = res.account.contact_name;
							item.type = (res.account.invoice_reseller == true) ? "Yes" : "No",
							item.account_type = (res.account.invoice_reseller == true) ? "" : res.account.type,
							item.invoice_through_agency = (res.account.invoice_reseller == true) ? 1 : 0;
							item.name = res.account.name;
							item.statusName = (res.account.hidden == 1) ? 'deactivate': 'activate';
							item.status = res.account.hidden;



                $$("clientAccountDatatable").refresh();

              $$('createNewClientAccountPopup').close();
              webix.message("Client account updated successfully.");
            } else {
              webix.message({type:"error", text:res.msg});
            }
          });


        }
      }}
  ];
  if(id === undefined) {
    form = _form.concat(addButton);
  } else {
    // when editing the existing domain
    form = _form.concat(editButton);
  }

  webix.ui({
    view: "window",
    id: "createNewClientAccountPopup",
    modal: true,
    position: "center",
		height: 600,
		width: 500,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: (id === undefined) ? "New Account" : "Edit Account"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('createNewClientAccountPopup').close();"
      }]
    },
    body: {
      view: "form",
      id: "_form",
			height: 500,
			width: 500,
      complexData: true,
      elements: form,
      rules: _formRules,
      margin:4,
      elementsConfig:{
        // labelPosition: "top",
        labelWidth: 180,
        bottomPadding: 18
      }
    }
  }).show();

	$$("invoice_through_agency").attachEvent('onChange', function(v){
		if(v === 1){
			$$("account_type").hide();
		} else {
			$$("account_type").show();
		}
	});

  if(id !== undefined){

		var names = data.name.split(' ');
		if(names[1] === undefined){
			names[1] = '';
		}
    $$("_form").setValues({
			_id: data._id,
			company: data.company,
			email: data.contact_email,
			confirmEmail: data.contact_email,
			firstname: names[0],
			lastname: names[1],
			invoice_through_agency: data.invoice_through_agency,
			type: data.account_type
    }, true);
  }


	if(id === undefined) {
    guidelines.startSubGuideline('newClientAccountPopup');
  } else {
    guidelines.startSubGuideline('editClientAccountPopup');
  }
}
function openLayout(){
  var formElement = document.createElement('div');
  formElement.id = 'webixclientAccountElement';

  document.getElementById("innerContent").appendChild(formElement);

 // Layout for domains
  webix.ui({
        container: "webixclientAccountElement",
        rows:[
                {template:"<div id='actionMenu'></div>", height: 50},
				        {template:"<div id='webixClientAccountGrid'></div>", height: 600},
            ]
  });
  // template for domain action menue
  webix.ui({
        container: "actionMenu",
        cols:[
              {view:"button", id: "createNewAccount", label:"Create Account", css: "orangeBtn", width: 150, click:function(){
                // open add new domain form
                  openClientAccountPopup('new', undefined, undefined);
              }},
            ]
  });

  webix.ajax().get(monolop_api_base_url+"/api/client-account?ajax=true", function(res, data, xhr){
      res  = JSON.parse(res);
      // console.log(res);
      var gridData = [];

      // if(res.totalCount > 0) {
        for (var i = 0; i < res.topics.length; i++) {
          gridData.push({
            id: i+1,
            _id: res.topics[i]._id,
            company: res.topics[i].company,
            contact_email: res.topics[i].contact_email,
						contact_name: res.topics[i].contact_name,
						type:  (res.topics[i].invoice_reseller == true) ? "Yes" : "No",
						account_type: (res.topics[i].invoice_reseller == true) ? "" : res.topics[i].type,
						invoice_through_agency: (res.topics[i].invoice_reseller == true) ? 1 : 0,
						name: res.topics[i].name,
						statusName: (res.topics[i].hidden == 1) ? 'deactivate': 'activate',
						status: res.topics[i].hidden,
          });
        }

        webix.ui({
          container: "webixClientAccountGrid",
          id: "clientAccountDatatable",
          view:"datatable", select:false, editable:false, height: 600,
          columns:[
            {id:"id", header:"#", width:50},
            {id:"company", header:["Company", {content:"textFilter"} ], sort:"string", minWidth:'20%', fillspace: 2, editor:"text"},
            {id:"name", header:["Account name", {content:"textFilter"} ], sort:"string", minWidth:'20%', fillspace: 2, editor:"text"},
						{id:"type", header:["Invoice through agency", {content:"textFilter"} ], sort:"string", minWidth:'20%', fillspace: 2, editor:"text"},
            {id:"contact_email", header:["Email", {content:"textFilter"} ], sort:"string", minWidth:'20%', fillspace: 2, editor:"text"},
						{id:"statusName", header:["Status"], minWidth:'20%', fillspace: 1, template:"<span style='color:black;' class='updateStatus status status#status#'>#statusName#</span>"},

            {id:"edit", header:"&nbsp;", width:35, template:"<span  style=' cursor:pointer;' class='webix_icon fa-pencil editUser'></span>"},
		        {id:"delete", header:"&nbsp;", width:35, template:"<span  style='cursor:pointer;' class='webix_icon fa-trash-o deleteUser'></span>"}

          ],
          // pager:"pagerA",
          data: gridData,
          onClick:{
						"updateStatus":function(e,id,node){
							// console.log(id);
						//	webix.confirm({
							//	text:"Are you sure?", ok:"Yes", cancel:"Cancel",
							//	callback:function(res){
								//	if(res){
										var item = webix.$$("clientAccountDatatable").getItem(id);
										var enabled;

										if(item.statusName === 'activate'){
											enabled = 1;
											item.status = 1;
											item.statusName = "deactivate";
										} else {
											enabled = 0;
											item.status = 0;
											item.statusName = "activate";
										}

										webix.ajax().post(monolop_api_base_url+"/api/client-account/status-update/"+ item._id, {hidden: enabled, ajax: true}, function(res, data, xhr){
											res = JSON.parse(res);
											if(res.success === true){
												webix.$$("clientAccountDatatable").refresh(id);
											}
										});


									//}
							//	}
						//	});
						},
            "deleteUser":function(e,id,node){
              webix.confirm({
                text:"Are you sure that you <br />want to delete?", ok:"Yes", cancel:"Cancel",
                callback:function(res){
                  if(res){
                    var item = webix.$$("clientAccountDatatable").getItem(id);


                    webix.ajax().del(monolop_api_base_url+"/api/client-account/" + item._id + '?ajax=true', {id: item._id}, function(res, data, xhr){
                      res = JSON.parse(res);
                      if(res.success === true){
                        webix.$$("clientAccountDatatable").remove(id);
                        webix.message("Selected account is deleted!");
                      }
                    });


                  }
                }
              });
            },
            "editUser": function(e,id,node){
              var item = webix.$$("clientAccountDatatable").getItem(id);
              openClientAccountPopup('edit', id, item);

            }
          },
					on:{
							onBeforeLoad:function(){
							},
							onAfterLoad:function(){
								initIntialTTandGuides();
							}
					},
        });

      // } else {
      //   webix.ui({
      //     container: "webixpluginsElement",
      //     view: "label",
      //     label: "No client account has been added yet."
      //   });
      // }

  });
	fab.initActionButtons({
		type: "clientAccountList",
		actionButtonTypes: extendedFab.actionButtonTypes,
		primary: extendedFab.primary,
	});
	// guidelines.generate(guidelines.page.currentGuide);
}
