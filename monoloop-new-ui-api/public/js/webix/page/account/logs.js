


function openLayout(){
  var formElement = document.createElement('div');
  var pagingElement = document.createElement('div');
  formElement.id = 'webixlogsElement';
  pagingElement.id = 'pageHere';

  document.getElementById("innerContent").appendChild(formElement);
  document.getElementById("innerContent").appendChild(pagingElement);

  webix.ready(function(){
        var gridData = webix.ui({
            container: "webixlogsElement",
            id: "logDatatable",
            view:"datatable", select:false, editable:false, height: 600,
            on:{
                   onBeforeLoad:function(){
                       this.showOverlay("Loading...");
                   },
                   onAfterLoad:function(){
                       this.hideOverlay();
                   }
               },
            columns:[
              {id:"created_at", header:["Date" ], sort:"string", minWidth:'20%', fillspace: 1, editor:"text", template:function(obj){
                var created_at = obj.created_at.split(" ");
                var time = moment(created_at[0]+'T'+created_at[1]+'Z') ;
                var offset = new Date().getTimezoneOffset();

                if(activeAccount.date_format){
                  activeAccount.date_format = activeAccount.date_format == 'D MMMM,YYYY' ? 'D MMMM, YYYY' : activeAccount.date_format;
                }
                if(activeAccount.timezone)
                  return time.tz(activeAccount.timezone).format(activeAccount.date_format + ' ' + activeAccount.time_format) ;
                else
                  return time.format('D MMMM, YYYY' + ' ' + 'HH:mm:ss')  ;
              }},
              {id:"msg", header:["Message" ], sort:"string", minWidth:'70%', fillspace: 3, editor:"text"},
              {id:"ip", header:["IP Address" ], sort:"string", minWidth:'10%', fillspace: 1, editor:"text"},
            ],
            // pager:"pagerA",
            // autowidth:true,
  				  type:{template:"{common.space()}"},
            pager:{
              id: 'logGridPager',
    					container:"pageHere",
              template:"{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
    					size:30,
    					group:10
    				},
            url: monolop_api_base_url+"/api/account/logs?ajax=true",
            onClick:{

            },
        });
  });
  initIntialTTandGuides(undefined, undefined, {placement: 'top'});
  $(window).on('resize', function(){
		$('webixlogsElement').css('width', ($('body').width() - $('.sidebar').width()) + 'px');

		$$('logDatatable').define('width', $('body').width() - $('.sidebar').width());
		$$('logDatatable').resize();

    $$('logGridPager').define('width', $('body').width() - $('.sidebar').width());
		$$('logGridPager').resize();
	});
}
