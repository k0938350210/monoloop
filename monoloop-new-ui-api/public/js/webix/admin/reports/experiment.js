"use strict";
(function(global) {
    var dashboardExperimentConfig = {
        folderManager: false,
        msg: "",
        currentStepInfo: {},
        apis: {
            getAll: monolop_api_base_url + "/api/reports/experiments/getAll"
        },
        formElement: {
            id: 'pageElementIdExperimentList',
        }
    };

    function timeDifference(current, previous) {

        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;

        var elapsed = current - previous;

        if (elapsed < msPerMinute) {
            return Math.round(elapsed / 1000) + ' seconds ago';
        } else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        } else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        } else if (elapsed < msPerMonth) {
            return 'approximately ' + Math.round(elapsed / msPerDay) + ' days ago';
        } else if (elapsed < msPerYear) {
            return 'approximately ' + Math.round(elapsed / msPerMonth) + ' months ago';
        } else {
            return Math.round(elapsed / msPerYear) + ' Years ago';
        }
    }

    function convertMS(ms) {
        var d, h, m, s;
        s = Math.floor(ms / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        return {
            d: d,
            h: h,
            m: m,
            s: s
        };
    }

    function openLayoutDashboardExperimentDetail() {

        var formElement = document.createElement('div');
        formElement.id = 'webixDashboardElement';
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "name_you_want");
        input.setAttribute("value", "value_you_want");
        document.getElementById("innerContent").appendChild(input);



        document.getElementById("innerContent").appendChild(formElement);
        // try {
        //     parent.location.hash = ''
        // } catch (e) {
        //     console.log('Report loaded in an iframe!');
        // }
        var href = location.href;
        var expid = href.substr(href.lastIndexOf('/') + 1);
        expid = expid.replace('#', '');
        var params = expid.indexOf('?') ;
        if(params !== -1){
          expid = expid.substr(0 , params );
        }
        webix.ready(function() {

            webix.ajax().get(monolop_api_base_url + '/api/reports/experiments/getExperiment/' + expid, function(response, xml, xhr) {
                response = JSON.parse(response);
                if (response.experiment && response.experiment._id) {

                    $("div.header:first").html(response.experiment.name);
                    $("div.header:first").attr('style', 'margin-left: 2%;font-size:24px;');

                    // calculate the experiment running time
                    var expDate = new Date(response.updated_at * 1000);
                    var expRunTime = convertMS(new Date().getTime() - expDate.getTime());

                    var goal = (response.experiment.goal_id == null) ? 0 : 1;

                    var variations = response.variations;
                    var locations = response.locations;

                    var getStaticBox = function() {
                        var staticBox = "";
                        staticBox += "<div class=\"ng-isolate-scope\">";
                        staticBox += "	<div class=\"ng-summary\">";
                        staticBox += "		<div class=\"ng-stats\">";
                        staticBox += "			<div class=\"ng-stat-group\">";
                        staticBox += "				<dl class=\"ng-date-scope\">";
                        staticBox += "					<dt class=\"ng-date-created\">";
                        staticBox += "						<h3 class=\"ng-date-summary\">";
                        staticBox += "							Runtime";
                        staticBox += "						<\/h3>";
                        staticBox += "					<\/dt>";
                        staticBox += "					<dd class=\"gn-scope-date\">";

                        staticBox += expRunTime.d > 0 ? ('<span class=\"exp-runtime number\">' + expRunTime.d + '</span>' + ' Day') : '';
                        staticBox += expRunTime.d > 1 ? 's' : '';
                        staticBox += expRunTime.d > 0 ? ', ' : '';
                        staticBox += expRunTime.h > 0 ? ('<span class=\"exp-runtime number\">' + expRunTime.h + '</span>' + ' Hour') : '';
                        staticBox += expRunTime.h > 1 ? 's' : '';
                        staticBox += expRunTime.h > 0 ? ', ' : '';
                        staticBox += expRunTime.m > 0 ? ('<span class=\"exp-runtime number\">' + expRunTime.m + '</span>' + ' Minute') : '';
                        staticBox += expRunTime.m > 1 ? 's' : '';
                        if (expRunTime.d == 0 && expRunTime.h == 0 && expRunTime.m == 0) {
                            staticBox += expRunTime.s > 0 ? ('<span class=\"exp-runtime number\">' + expRunTime.s + '</span>' + ' Second') : '';
                            staticBox += expRunTime.s > 1 ? 's' : '';
                        }

                        staticBox += "					<\/dd>";
                        staticBox += "				<\/dl>";
                        staticBox += "			<\/div>";
                        staticBox += "			<div class=\"ng-goals-main\">";
                        staticBox += "				<dl class=\"ng-goals-sub variations\">";
                        staticBox += "					<dt class=\"ng-goals-head\">";
                        staticBox += "						<h3 class=\"ng-goals-heading\">";
                        staticBox += "							Variations";
                        staticBox += "						<\/h3>";
                        staticBox += "					<\/dt>";
                        staticBox += "					<dd class=\"ng-goals-no\">";
                        staticBox += "						<h2 class=\"ng-goals-count\">";
                        staticBox += variations;
                        staticBox += "						<\/h2>";
                        staticBox += "					<\/dd>";
                        staticBox += "				<\/dl>";
                        staticBox += "				<dl class=\"ng-goals-sub locations\">";
                        staticBox += "					<dt class=\"ng-goals-head\">";
                        staticBox += "						<h3 class=\"ng-goals-heading\">";
                        staticBox += "							Locations";
                        staticBox += "						<\/h3>";
                        staticBox += "					<\/dt>";
                        staticBox += "					<dd class=\"ng-goals-no\">";
                        staticBox += "						<h2 class=\"ng-goals-count\">";
                        staticBox += locations;
                        staticBox += "						<\/h2>";
                        staticBox += "					<\/dd>";
                        staticBox += "				<\/dl>";
                        staticBox += "			<\/div>";
                        staticBox += "			<div class=\"ng-goals-main goals\">";
                        staticBox += "				<dl class=\"ng-goals-sub goals\">";
                        staticBox += "					<dt class=\"ng-goals-head\">";
                        staticBox += "						<h3 class=\"ng-goals-heading\">";
                        staticBox += "							Goals";
                        staticBox += "						<\/h3>";
                        staticBox += "					<\/dt>";
                        staticBox += "					<dd class=\"ng-goals-no\">";
                        staticBox += "						<h2 class=\"ng-goals-count\">";
                        staticBox += goal;
                        staticBox += "						<\/h2>";
                        staticBox += "					<\/dd>";
                        staticBox += "				<\/dl>";
                        staticBox += "			<\/div>";
                        staticBox += "<i class=\"webix_icon fa-clock-o\" style=\"z-index: -1\"></i>"
                        staticBox += "		<\/div>";
                        staticBox += "	<\/div>";
                        staticBox += "<\/div>";
                        return staticBox;
                    };

                    var getVisitorBox = function() {
                        var strVar = "<div class=\"chart\"><\/div>";
                        strVar += "<div class=\"DIV_1_ml \">";
                        strVar += "	<div class=\"DIV_2_ml\">";
                        strVar += "		<div class=\"DIV_3_ml\">";
                        strVar += "			<div class=\"DIV_4_ml\">";
                        strVar += "				<div class=\"DIV_10_ml\">";
                        strVar += "					 <span class=\"SPAN_11_ml\">" + (response.visitors + response.cg_visitors) + "<\/span>";
                        strVar += "					<h3 class=\"H3_12_ml\">";
                        strVar += "						Visitors";
                        strVar += "					<\/h3>";
                        strVar += "				<\/div>";
                        strVar += "";
                        strVar += "			<\/div>";
                        strVar += "		<\/div>";
                        strVar += "<i class=\"webix_icon fa-group\" style=\"z-index: 1;\"></i>";
                        strVar += "	<\/div>";
                        strVar += "<\/div>";
                        return strVar;
                    };

                    var getExperimentStateVisit = function() {
                        var strVar = "<div class=\"DIV_1_ml getExperimentStateVisit\">";
                        if (response.is_exp_running === 1) {
                            strVar += "	<div class=\"DIV_2_ml\">";
                        } else {
                            strVar += "	<div class=\"DIV_2_ml error\">";
                        }
                        strVar += "		<div class=\"DIV_3_ml\">";
                        strVar += "			<div class=\"DIV_4_ml\">";
                        if (response.is_exp_running === 1) {
                            strVar += "<dd class='expStatusLabelPosit'>Experiment is running correct.</dd>";
                        } else {
                            strVar += "<dd class='expStatusLabelNegat'>Script is not detected in experience domain.</dd>";
                        }

                        if (response.estimatedDays80PercentChance > 90) {
                            strVar += "<dd class='expStatusLabelPosit exceed90'>Estimated days for 80% chance of significant result is exceeding 90 days</dd>";
                        }
                        if (response.estimatedDays95PercentChance > 90) {
                            strVar += "<dd class='expStatusLabelPosit exceed90'>Estimated days for 95% chance of significant result is exceeding 90 days</dd>";
                        }

                        strVar += "			<\/div>";
                        strVar += "		<\/div>";
                        if (response.is_exp_running === 1) {
                            strVar += "<i class=\"webix_icon fa-check-square\" style=\"z-index: 1;\"></i>";
                        } else {
                            strVar += "<i class=\"webix_icon fa-exclamation-triangle\" style=\"z-index: 1;\"></i>";
                        }

                        strVar += "	<\/div>";
                        strVar += "<\/div>";
                        return strVar;
                    };

                    var renderSignificant = function(p) {
                        if (p == 0 || p == null) {
                            return '';
                        } else if (p > 0.05) {
                            return 'No';
                        } else if (p <= 0.05 && p > 0.01) {
                            return 'Yes';
                        } else if (p <= 0.01) {
                            return 'Absolute';
                        }
                    }

                    var significantResult = function() {
                        var strVar = "";
                        if (response.p_value.toFixed(2) < 0.05) {
                            strVar += "<div class=\"DIV_1_ml significantResult\">";
                        } else {
                            strVar += "<div class=\"DIV_1_ml significantResult insig\">";
                        }
                        strVar += "	<div class=\"DIV_2_ml\">";
                        strVar += "		<div class=\"DIV_3_ml\">";
                        strVar += "			<div class=\"DIV_4_ml\">";

                        if((response.estimatedDays80PercentChance == null || response.estimatedDays80PercentChance == 0) && (response.estimatedDays95PercentChance == null || response.estimatedDays95PercentChance == 0) ){
                          strVar += "				<dd class=\"estTdays\">No data found.</dd><br />";
                        }else{
                          // 80% chance field
                          if(response.estimatedDays80PercentChance == null || response.estimatedDays80PercentChance == 0){
                            strVar += "				<dd class=\"estTdays\">No data found.</dd><br />";
                          }else{
                            strVar += "				<dd class=\"estTdays\">80% confident in est <b> " + response.estimatedDays80PercentChance + "</b> days</dd><br />";
                          }

                          // 95% chance field
                          if(response.estimatedDays95PercentChance == null || response.estimatedDays95PercentChance == 0){
                            strVar += "				<dd class=\"estTdays\">No data found.</dd><br />";
                          }else{
                            strVar += "				<dd class=\"estTdays\">95% confident in est <b> " + response.estimatedDays95PercentChance + "</b> days</dd><br />";
                          }
                        }

                        strVar += "				<div style=\"width: 100%\" class=\"DIV_10_ml sig imp\">";
                        strVar += "					 <span style=\"font-weight: 900;font-size: 26px;\" class=\"SPAN_11_ml\">" + (response.improvement).toFixed(2) + "%<\/span>";
                        strVar += "					<h3 style=\"font-weight: 900;\" class=\"H3_12_ml\">";
                        strVar += "						Improvement";
                        strVar += "					<\/h3>";
                        strVar += "				<\/div>";
                        strVar += "				<div style=\"width: 100%\" class=\"DIV_10_ml sig\">";
                        // strVar += "					 <span style=\"font-weight: 900;font-size: 26px;\" class=\"SPAN_11_ml\">" + response.p_value.toFixed(2) + "<\/span>";
                        strVar += "					 <span style=\"font-weight: 900;font-size: 26px;\" class=\"SPAN_11_ml\">" + response.p_value.toFixed(3) + "<\/span>";
                        strVar += "					<h3 style=\"font-weight: 900;width: 100%\" class=\"H3_12_ml\">";
                        strVar += "						P-Value";
                        strVar += "					<\/h3>";
                        strVar += "				<\/div>";
                        strVar += "";
                        strVar += "			<\/div>";
                        strVar += "		<\/div>";
                        strVar += "	<\/div>";
                        strVar += "<\/div>";
                        return strVar;
                    };


                    webix.ajax().get(monolop_api_base_url + '/api/account/logs?ajax=true&type=App\\Experiment&id=' + response.experiment._id, function(response_logs, xml, xhr) {
                        var logs = JSON.parse(response_logs);

                        var timeline = "<div class=\"DIV_276\">";
                        timeline += "<div class=\"DIV_277\">";
                        timeline += "<div class=\"DIV_278\">";
                        timeline += "<h5 class=\"H5_279\">";
                        timeline += "Timeline";
                        timeline += "<\/h5>";
                        timeline += "<\/div>";
                        timeline += "<div class=\"DIV_280\">";
                        timeline += "							<ul class=\"UL_281\">";
                        if (logs.data.length > 0) {
                            for (var i = 0; i < logs.data.length; i++) {
                                timeline += "<li class=\"LI_282\">";
                                timeline += "<div class=\"DIV_283\">";
                                timeline += "<div class=\"DIV_284\">";
                                timeline += logs.data[i].created_at.slice(0, -3);
                                timeline += "<\/div> <span title=\"" + accountName + "\" class=\"STRONG_286\" style=\"text: wrap\">" + (accountName.length > 19 ? accountName.substr(0, 16) + '..' : accountName) + "<\/span>";
                                timeline += "<\/div>";
                                timeline += "<p class=\"P_287\">";
                                timeline += logs.data[i].msg;
                                timeline += "<\/p>";
                                timeline += "<\/li>";
                            }
                        } else {
                            timeline += "<li class=\"LI_300\">";
                            timeline += " You have no feeds yet.";
                            timeline += "<\/li>";
                        }


                        timeline += "<\/ul>";
                        timeline += "<\/div>";


                        var tabbar = {
                            view: "tabbar",
                            id: "tabbarView",
                            type: "bottom",
                            multiview: true,
                            css: "tabber_view",
                            options: [{
                                    css: "summary_tab",
                                    value: "<li id=\"LI_143\"><a href=\"Javascript:void(0)\" id=\"A_144\">Summary<\/a><\/li>",
                                    id: 'listView',
                                    css: 'custom summaryTab'
                                }, {
                                    css: "repart_tab",
                                    value: "<li class=\"detail_report\" id=\"LI_145\"><a href=\"Javascript:void(0)\" id=\"A_146\">Details<\/a><\/li>",
                                    id: 'aboutView',
                                    css: 'detailedReportTab'
                                },

                            ],
                            height: 40,
                            margin: 0
                        };

                        var formatted;
                        var data = {
                            css: "data_view",
                            cells: [{
                                    id: "listView",
                                    // height: 750,
                                    css: "listView",
                                    // width: 660,
                                    height: 883,
                                    type: "space",
                                    rows: [{
                                        css: "staticBOX widget",
                                        cols: [{
                                            css: "widget",
                                            height: 250,
                                            width: 300,
                                            template: getStaticBox()
                                        }, {
                                            css: "widget visitor group",
                                            height: 250,
                                            width: 300,
                                            template: getVisitorBox()
                                        }, {
                                            css: "widget significant group",
                                            height: 250,
                                            width: 300,
                                            template: significantResult()
                                        }]
                                    }, {
                                        css: "donutChart",
                                        cols: [{
                                            css: "cgBox",
                                            height: 300,
                                            width: 585,
                                            template: '<div id="cgPeiChartID"></div>',
                                        }, {
                                            css: "widget status group",
                                            height: 250,
                                            width: 315,
                                            template: getExperimentStateVisit()
                                        }]
                                    }]
                                }, {
                                    id: "formView",
                                    css: "formView",
                                    template: "Place for the form control"
                                }, {
                                    id: "aboutView",
                                    // width: 960,
                                    css: "aboutView",
                                    height: 883,
                                    template: ''
                                      + '<div id="mainContainerDashboard">'
                                      + '  <div id="dateRangeCRChartFilters"></div>'
                                      + '  <div id="currentSnapShotChartHeader"></div>'
                                      + '  <div id="dateRangeCRChart"></div>'
                                      + '  <div id="currentSnapShotChart"></div>'
                                      + '  <div id="dataRangeTabularform"></div>'
                                      + '  <div id="averageReportSection"></div>'
                                      + '</div>'
                                },


                            ]
                        };


                        webix.ui({
                            container: "webixDashboardElement",
                            id: "webixDashboardElementExperiment",
                            type: 'wide',
                            margin: 0,
                            // height: $("#nav").height(),
                            rows: [{
                                css: "tabbar_timline",
                                // height: 883,
                                autoheight: true,
                                cols: [{
                                    css: "tabbar",
                                    rows: [
                                        tabbar,
                                        data,

                                    ]
                                }, {
                                    css: "timeline",
                                    padding: 0,
                                    // height: $("#nav").height(),
                                    // scroll: true,
                                    template: timeline
                                }]
                            }, ]


                        });
                        var data = [{
                            label: "Control",
                            value: response.cg_visitors
                        }, {
                            label: "Experience",
                            value: response.visitors
                        }];

                        if (response.cg_visitors > 0 || response.visitors > 0) {
                            var x = d3.scale.linear()
                                .domain([0, d3.max(data, function(d) {
                                    return d.value;
                                })])
                                .range([0, 200]);
                            d3.select(".chart")
                                .selectAll("div")
                                .data(data)
                                .enter().append("div")
                                // .style("width", function(d) { return x(d.value) + "px"; })
                                .style("width", function(d) {
                                    return "93%;";
                                })
                                .style("height", "20px")
                                .text(function(d) {
                                    return d.label + ':' + d.value;
                                });
                        }



                        $(".changeStatus").on('click', function(e) {

                            $(".changeStatus").removeClass('active');

                            var value = $(this).attr('value');
                            var hidden;
                            if (value === 'running') {
                                hidden = 0
                            } else {
                                hidden = 1;
                            }

                            webix.ajax().post(monolop_api_base_url + "/api/experiments/update-status", {
                                _id: 'experiment__' + response.experiment._id,
                                ajax: true,
                                hidden: hidden,
                                action: 'update_hidden'
                            }, function(res, data, xhr) {
                                res = JSON.parse(res);
                                if (res.success === true) {
                                    webix.message(res.msg);
                                }
                            });

                            $(this).addClass('active');
                        });

                        $$("tabbarView").attachEvent("onChange", function(newv, oldv) {
                            $('#datepicker-dropdown').css('display', 'none');
                            if (newv !== 'aboutView') {
                                return false;
                            }
                            var width = $("#dateRangeCRChartFilters").width();

                            $("#dateRangeCRChartFilters").empty();
                            $("#currentSnapShotChartHeader").empty();

                            webix.ui({
                                container: "dateRangeCRChartFilters",
                                css: "dateRangeCRChartFilters",
                                height: 50,
                                type: 'wide',
                                width: width,
                                rows: [{
                                    cols: [{
                                        type: "space",
                                        width: 20
                                    }, {
                                        view: "select",
                                        css: 'dateRangeCRChartFiltersMenu',
                                        width: width / 2 - 40,
                                        value: 'cr',
                                        options: [{
                                            id: 'cr',
                                            value: "Conversion rate"
                                        }, {
                                            id: 'conversions',
                                            value: "Conversions"
                                        }, {
                                            id: 'visitors',
                                            value: "Visitors"
                                        }]
                                    }, {
                                        type: "space",
                                        width: 20
                                    }, {
                                        css: "dateRangeCRChartFiltersDRField",
                                        width: width / 2 - 20,
                                        template: '<div id="date-range-field"><span>Enable date range</span></div><div id="datepicker-calendar"></div>'
                                    }, {
                                        type: "space",
                                        width: 20
                                    }]
                                }]
                            });

                            webix.ui({
                                container: "currentSnapShotChartHeader",
                                css: "currentSnapShotChartHeader",
                                height: 52,
                                autoWidth: true,
                                // type: 'wide',
                                // width: $("#mainContainerDashboard").width()/2,
                                cols: [{
                                    template: "<p class='heading'>Conversion rate</p>"
                                }]
                            });

                            setTimeout(function() {
                                var to = new Date();
                                var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 6);
                                var experiment_updated_at = new Date(response.updated_at * 1000);

                                var calendarStart = from,
                                    calendarEnd = to;
                                if (experiment_updated_at.getTime() > from.getTime()) {
                                    calendarStart = experiment_updated_at;
                                }
                                $('#date-range-field').DateRangesWidget({
                                    inline: true,
                                    date: [calendarStart, calendarEnd],
                                    selectableDates: [experiment_updated_at, calendarEnd],
                                    calendars: 3,
                                    mode: 'tworanges',
                                    showOn: 'focus',
                                    onChange: function(dates, el) {
                                        dates = getDates();

                                        dates[0] = dates[0].replace(/,/g, "").replace(/ /g, "-");
                                        dates[1] = dates[1].replace(/,/g, "").replace(/ /g, "-");
                                        // update the range display

                                        var _date_from = dates[0];
                                        var _date_to = dates[1];

                                        generateDataRangeReportGraph(
                                            expid,
                                            _date_from,
                                            _date_to,
                                            'data_range',
                                            response.experiment
                                        );

                                        generateCurrentSnapshotGraph(expid, 'current_snapshot');

                                    }
                                });

                            }, 500);




                            generateDataRangeReportGraph(expid, '', '', 'data_range', response.experiment);
                            generateCurrentSnapshotGraph(expid, 'current_snapshot');
                        });

                        // generateCgPieChart(expid);
                        // initIntialTTandGuides();
                    });
                }


            });
        });

    }

    function generateCurrentSnapshotGraph(expid, graph_type) {


        $("#currentSnapShotChart").empty();


        // d3.json(monolop_api_base_url+"/api/reports/experiments/getCombinedCR?eID="+expid, function(error, data) {
        webix.ajax().get(monolop_api_base_url + "/api/reports/experiments/getCombinedCR?eID=" + expid, function(data) {
            data = JSON.parse(data);
            if (graph_type === 'current_snapshot') {
                var menu = d3.select(".dateRangeCRChartFilters select");
                var series = menu.property("value");
                var txt = series;
                switch (series) {
                    case 'cr':
                        txt = 'Conversion rate';
                        break;
                    case 'conversions':
                        txt = 'Conversions';
                        break;
                    case 'visitors':
                        txt = 'Visitors';
                        break;
                    default:
                        break;
                }
                if (series === 'cr') {
                    data = [data.cg.cr, data.experiment.cr];
                } else if (series === 'visitors') {
                    data = [data.cg.visitors, data.experiment.visitors];
                } else {
                    data = [data.cg.direct, data.experiment.direct];
                }
                var margin = {
                        top: 50,
                        right: 50,
                        bottom: 50,
                        left: 50
                    },
                    width = $("#mainContainerDashboard").width() / 2 - margin.left - margin.right,
                    height = 450 - margin.top - margin.bottom;

                var x = d3.scale.ordinal()
                    .rangeRoundBands([0, width], .1);

                var y = d3.scale.linear()
                    .range([height, 0]);

                var xAxis = d3.svg.axis()
                    .scale(x)
                    .orient("bottom");

                var yAxis = d3.svg.axis()
                    .scale(y)
                    .orient("left")
                    .ticks(10, "");

                $("#currentSnapShotChart").empty();

                var svg = d3.select("#currentSnapShotChart").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


                x.domain(data.map(function(d) {
                    return d.name;
                }));
                y.domain([0, d3.max(data, function(d) {
                    return d.value;
                })]);

                svg.append("g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + height + ")")
                    .call(xAxis);

                svg.append("g")
                    .attr("class", "y axis")
                    .call(yAxis)
                    .append("text")
                    .attr("text-anchor", "middle") // this makes it easy to centre the text as the transform is applied to the anchor
                    .attr('class', 'y-label-text')
                    .attr("transform", "translate(" + (-35) + "," + (height / 2) + ")rotate(-90)") // text is drawn off the screen top left, move down and out and rotate
                    .text(txt);

                svg.selectAll(".bar")
                    .data(data)
                    .enter().append("rect")
                    .attr("class", "bar")
                    .attr("x", function(d) {
                        return (x(d.name) + 25);
                    })
                    .attr("width", x.rangeBand() - 50)
                    .attr("y", function(d) {
                        return y(d.value);
                    })
                    .attr("height", function(d) {
                        return height - y(d.value);
                    });

                function type(d) {
                    d.value = +d.value;
                    return d;
                }
            }
        });
    }

    function generateDataRangeReportGraph(expid, from_date, to_date, graph_type, experiment) {

        from_date = from_date || '';
        to_date = to_date || '';

        if (to_date === '' || from_date === '') {
            var to = new Date();
            var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 28);
            from_date = from.getDate() + '-' + monthNames[from.getMonth()] + '-' + from.getFullYear();
            to_date = to.getDate() + '-' + monthNames[to.getMonth()] + '-' + to.getFullYear();
        }
        $("#dateRangeCRChart").empty();

        var _url = monolop_api_base_url + "/api/reports/experiments/getCR?eID=" + expid + '&from_date=' + from_date + '&to_date=' + to_date;
        // d3.json(_url, function(error, data) {
        webix.ajax().get(_url, function(data) {
            data = JSON.parse(data);
            if (graph_type === 'data_range') {

                var formatted;
                //set the margins
                var margin = {
                        top: 50,
                        right: 50,
                        bottom: 50,
                        left: 50
                    },
                    width = $("#mainContainerDashboard").width() / 2 - margin.left - margin.right,
                    height = 450 - margin.top - margin.bottom;

                //set dek and head to be as wide as SVG
                // d3.select('#dek')
                // .style('width', width+'px');
                // d3.select('#headline')
                // .style('width',width+'px');

                //write out your source text here
                // var sourcetext= "Source: XXXXX";

                // set the type of number here, n is a number with a comma, .2% will get you a percent, .2f will get you 2 decimal points
                var NumbType = d3.format(".2f");

                // color array
                var bluescale4 = ["#8BA9D0", "#6A90C1", "#066CA9", "#004B8C"];

                //color function pulls from array of colors stored in color.js
                var color = d3.scale.ordinal().range(bluescale4);

                //define the approx. number of x scale ticks
                var xscaleticks = 5;

                //defines a function to be used to append the title to the tooltip.  you can set how you want it to display here.
                var maketip = function(d) {
                    var tip = '<p class="tip3">' + d.name + '<p class="tip1">' + NumbType(d.value) + '</p> <p class="tip3">' + formatDate(d.date) + '</p>';
                    return tip;
                }

                //define your year format here, first for the x scale, then if the date is displayed in tooltips
                var parseDate = d3.time.format("%m/%d/%y").parse;
                var formatDate = d3.time.format("%b %d, '%y");

                $("#dateRangeCRChart").empty();

                //create an SVG
                var svg = d3.select("#dateRangeCRChart").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                //make a rectangle so there is something to click on
                svg.append("svg:rect")
                    .attr("width", width)
                    .attr("height", height)
                    .attr("class", "plot");

                //make a clip path for the graph
                var clip = svg.append("svg:clipPath")
                    .attr("id", "clip")
                    .append("svg:rect")
                    .attr("x", 0)
                    .attr("y", 0)
                    .attr("width", width)
                    .attr("height", height);

                // force data to update when menu is changed
                var menu = d3.select(".dateRangeCRChartFilters select")
                    .on("change", change);
                //suck in the data, store it in a value called formatted, run the redraw function
                // d3.csv("http://monoloop.laravel.new/data.csv", function(data) {
                //   formatted = data;
                //   console.log("formatted",formatted);
                //   redraw();
                // });

                formatted = data;
                redraw();

                d3.select(window)
                    .on("keydown", function() {
                        altKey = d3.event.altKey;
                    })
                    .on("keyup", function() {
                        altKey = false;
                    });
                var altKey;

                // set terms of transition that will take place
                // when a new economic indicator is chosen
                function change() {
                    var series = menu.property("value");
                    var txt = series;
                    switch (series) {
                        case 'cr':
                            txt = 'Conversion rate';
                            break;
                        case 'conversions':
                            txt = 'Conversions';
                            break;
                        case 'visitors':
                            txt = 'Visitors';
                            break;
                        default:
                            break;

                    }

                    $(".y-label-text:first").html(txt);
                    generateCurrentSnapshotGraph(expid, 'current_snapshot');
                    d3.transition()
                        .duration(altKey ? 7500 : 1500)
                        .each(redraw);


                }

                // all the meat goes in the redraw function
                function redraw() {

                    // create data nests based on economic indicator (series)
                    var nested = d3.nest()
                        .key(function(d) {
                            return d.type;
                        })
                        .map(formatted)

                    // get value from menu selection
                    // the option values are set in HTML and correspond
                    //to the [type] value we used to nest the data
                    var series = menu.property("value");
                    var txt = series === 'cr' ? 'Conversion rate' : series;

                    // only retrieve data from the selected series, using the nest we just created
                    var data = nested[series];

                    // for object constancy we will need to set "keys", one for each type of data (column name) exclude all others.
                    if (data)
                        color.domain(d3.keys(data[0]).filter(function(key) {
                            return (key !== "date" && key !== "type");
                        }));

                    var linedata = color.domain().map(function(name) {
                        return {
                            name: name,
                            values: data.map(function(d) {
                                return {
                                    name: name,
                                    date: parseDate(d.date),
                                    value: parseFloat(d[name], 10)
                                };
                            })
                        };
                    });


                    //make an empty variable to stash the last values into so i can sort the legend
                    var lastvalues = [];

                    //setup the x and y scales
                    var x = d3.time.scale()
                        .domain([
                            d3.min(linedata, function(c) {
                                return d3.min(c.values, function(v) {
                                    return v.date;
                                });
                            }),
                            d3.max(linedata, function(c) {
                                return d3.max(c.values, function(v) {
                                    return v.date;
                                });
                            })
                        ])
                        .range([0, width]);

                    var y = d3.scale.linear()
                        .domain([
                            d3.min(linedata, function(c) {
                                return d3.min(c.values, function(v) {
                                    return v.value;
                                });
                            }),
                            d3.max(linedata, function(c) {
                                return d3.max(c.values, function(v) {
                                    return v.value;
                                });
                            })
                        ])
                        .range([height, 0]);

                    //will draw the line
                    var line = d3.svg.line()
                        .x(function(d) {
                            return x(d.date);
                        })
                        .y(function(d) {
                            return y(d.value);
                        });

                    //define the zoom
                    var zoom = d3.behavior.zoom()
                        .x(x)
                        .y(y)
                        .scaleExtent([1, 8])
                        .on("zoom", zoomed);

                    //call the zoom on the SVG
                    svg.call(zoom);

                    //create and draw the x axis
                    var xAxis = d3.svg.axis()
                        .scale(x)
                        .orient("bottom")
                        .tickPadding(8)
                        .ticks(xscaleticks);

                    svg.append("svg:g")
                        .attr("class", "x axis");

                    //create and draw the y axis
                    var yAxis = d3.svg.axis()
                        .scale(y)
                        .orient("left")
                        .tickSize(0 - width)
                        .tickPadding(8);

                    svg.append("svg:g")
                        .attr("class", "y axis");

                    //bind the data
                    var thegraph = svg.selectAll(".thegraph")
                        .data(linedata)

                    //append a g tag for each line and set of tooltip circles and give it a unique ID based on the column name of the data
                    var thegraphEnter = thegraph.enter().append("g")
                        .attr("clip-path", "url(#clip)")
                        .attr("class", "thegraph")
                        .attr('id', function(d) {
                            return d.name + "-line";
                        })
                        .style("stroke-width", 2.5)
                        .on("mouseover", function(d) {
                            d3.select(this) //on mouseover of each line, give it a nice thick stroke
                                .style("stroke-width", '6px');

                            var selectthegraphs = $('.thegraph').not(this); //select all the rest of the lines, except the one you are hovering on and drop their opacity
                            d3.selectAll(selectthegraphs)
                                .style("opacity", 0.2);

                            var getname = document.getElementById(d.name); //use get element cause the ID names have spaces in them
                            var selectlegend = $('.legend').not(getname); //grab all the legend items that match the line you are on, except the one you are hovering on

                            d3.selectAll(selectlegend) // drop opacity on other legend names
                                .style("opacity", .2);

                            d3.select(getname)
                                .attr("class", "legend-select"); //change the class on the legend name that corresponds to hovered line to be bolder
                        })
                        .on("mouseout", function(d) { //undo everything on the mouseout
                            d3.select(this)
                                .style("stroke-width", '2.5px');

                            var selectthegraphs = $('.thegraph').not(this);
                            d3.selectAll(selectthegraphs)
                                .style("opacity", 1);

                            var getname = document.getElementById(d.name);
                            var getname2 = $('.legend[fakeclass="fakelegend"]')
                            var selectlegend = $('.legend').not(getname2).not(getname);

                            d3.selectAll(selectlegend)
                                .style("opacity", 1);

                            d3.select(getname)
                                .attr("class", "legend");
                        });

                    //actually append the line to the graph
                    thegraphEnter.append("path")
                        .attr("class", "line")
                        .style("stroke", function(d) {
                            return color(d.name);
                        })
                        .attr("d", function(d) {
                            return line(d.values[0]);
                        })
                        .transition()
                        .duration(2000)
                        .attrTween('d', function(d) {
                            var interpolate = d3.scale.quantile()
                                .domain([0, 1])
                                .range(d3.range(1, d.values.length + 1));
                            return function(t) {
                                return line(d.values.slice(0, interpolate(t)));
                            };
                        });

                    //then append some 'nearly' invisible circles at each data point
                    thegraph.selectAll("circle")
                        .data(function(d) {
                            return (d.values);
                        })
                        .enter()
                        .append("circle")
                        .attr("class", "tipcircle")
                        .attr("cx", function(d, i) {
                            return x(d.date)
                        })
                        .attr("cy", function(d, i) {
                            return y(d.value)
                        })
                        .attr("r", 12)
                        .style('opacity', 1e-6) //1e-6
                        .attr("title", maketip);

                    //append the legend
                    var legend = svg.selectAll('.legend')
                        .data(linedata);

                    var legendEnter = legend
                        .enter()
                        .append('g')
                        .attr('class', 'legend')
                        .attr('id', function(d) {
                            return d.name;
                        })
                        .on('click', function(d) { //onclick function to toggle off the lines
                            if ($(this).css("opacity") == 1) { //uses the opacity of the item clicked on to determine whether to turn the line on or off

                                var elemented = document.getElementById(this.id + "-line"); //grab the line that has the same ID as this point along w/ "-line"  use get element cause ID has spaces
                                d3.select(elemented)
                                    .transition()
                                    .duration(1000)
                                    .style("opacity", 0)
                                    .style("display", 'none');

                                d3.select(this)
                                    .attr('fakeclass', 'fakelegend')
                                    .transition()
                                    .duration(1000)
                                    .style("opacity", .2);
                            } else {

                                var elemented = document.getElementById(this.id + "-line");
                                d3.select(elemented)
                                    .style("display", "block")
                                    .transition()
                                    .duration(1000)
                                    .style("opacity", 1);

                                d3.select(this)
                                    .attr('fakeclass', 'legend')
                                    .transition()
                                    .duration(1000)
                                    .style("opacity", 1);
                            }
                        });

                    //create a scale to pass the legend items through
                    var legendscale = d3.scale.ordinal()
                        .domain(lastvalues)
                        .range([0, 30, 60, 90, 120, 150, 180, 210]);

                    //actually add the circles to the created legend container
                    // legendEnter.append('circle')
                    // .attr('cx', width +20)
                    // .attr('cy', function(d){return legendscale(d.values[d.values.length-1].value);})
                    // .attr('r', 7)
                    // .style('fill', function(d) {
                    //   return color(d.name);
                    // });

                    //add the legend text
                    // legendEnter.append('text')
                    // .attr('x', width+35)
                    // .attr('y', function(d){return legendscale(d.values[d.values.length-1].value);})
                    // .text(function(d){ return d.name; });

                    // set variable for updating visualization
                    var thegraphUpdate = d3.transition(thegraph);

                    // change values of path and then the circles to those of the new series
                    thegraphUpdate.select("path")
                        .attr("d", function(d, i) {

                            //must be a better place to put this, but this works for now
                            lastvalues[i] = d.values[d.values.length - 1].value;
                            lastvalues.sort(function(a, b) {
                                return b - a
                            });
                            legendscale.domain(lastvalues);

                            return line(d.values);
                        });

                    thegraphUpdate.selectAll("circle")
                        .attr("title", maketip)
                        .attr("cy", function(d, i) {
                            return y(d.value)
                        })
                        .attr("cx", function(d, i) {
                            return x(d.date)
                        });


                    // and now for legend items
                    var legendUpdate = d3.transition(legend);

                    legendUpdate.select("circle")
                        .attr('cy', function(d, i) {
                            return legendscale(d.values[d.values.length - 1].value);
                        });

                    legendUpdate.select("text")
                        .attr('y', function(d) {
                            return legendscale(d.values[d.values.length - 1].value);
                        });


                    // update the axes,
                    d3.transition(svg).select(".y.axis")
                        .call(yAxis);

                    d3.transition(svg).select(".x.axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    //make my tooltips work
                    $('circle').tipsy({
                        opacity: .9,
                        gravity: 'n',
                        html: true
                    });

                    try{
                      d3.transition(svg)
                        .append("text") // comment: there isn't any method append on this object.
                        .attr("text-anchor", "middle") // this makes it easy to centre the text as the transform is applied to the anchor
                        .attr('class', 'y-label-text')
                        .attr("transform", "translate(" + (-40) + "," + (height / 2) + ")rotate(-90)") // text is drawn off the screen top left, move down and out and rotate
                        .text(txt);
                      }catch(e1){}

                    try{
                      d3.transition(svg)
                          .append("text") // comment: there isn't any method append on this object.
                          .attr("text-anchor", "middle") // this makes it easy to centre the text as the transform is applied to the anchor
                          .attr("transform", "translate(" + (width / 2) + "," + (height + 50) + ")") // centre below axis
                          .text("Date range");
                    }catch(e2){}


                    //define the zoom function
                    function zoomed() {

                        svg.select(".x.axis").call(xAxis);
                        svg.select(".y.axis").call(yAxis);

                        svg.selectAll(".tipcircle")
                            .attr("cx", function(d, i) {
                                return x(d.date)
                            })
                            .attr("cy", function(d, i) {
                                return y(d.value)
                            });

                        svg.selectAll(".line")
                            .attr("class", "line")
                            .attr("d", function(d) {
                                return line(d.values)
                            });

                    }

                    //end of the redraw function
                }
                //
                // svg.append("svg:text")
                // .attr("text-anchor", "start")
                // .attr ("x", 0-margin.left)
                // .attr("y", height+margin.bottom-10)
                // .text (sourcetext)
                // .attr ("class","source");
            }
        });


        webix.ajax().get(monolop_api_base_url + '/api/reports/experiments/getExperimentOverview?eID=' + expid + '&from_date=' + from_date + '&to_date=' + to_date, function(response_exp_overview, xml, xhr) {
            var experiment_overview = JSON.parse(response_exp_overview);
            $("#dataRangeTabularform").empty();
            webix.ui({
                container: "dataRangeTabularform",
                view: "datatable",
                // hidden: true,
                width: $("#mainContainerDashboard").width() - 10,
                autoheight: true,
                css: "dataRangeTabularform",
                columns: [{
                    id: "name",
                    header: "",
                    width: ($("#mainContainerDashboard").width() / 6) - 1.7
                }, {
                    id: "visitors",
                    header: "Visitors",
                    width: ($("#mainContainerDashboard").width() / 6) - 1.7
                }, {
                    id: "conversions",
                    header: "Conversions",
                    width: ($("#mainContainerDashboard").width() / 6) - 1.7
                }, {
                    id: "cr",
                    header: "CR%",
                    width: ($("#mainContainerDashboard").width() / 6) - 1.7
                }, {
                    id: "p_value",
                    header: "P-Value",
                    width: ($("#mainContainerDashboard").width() / 6) - 1.7
                }, {
                    id: "improvement",
                    header: "Improvement",
                    width: ($("#mainContainerDashboard").width() / 6) - 1.7
                }],
                data: [{
                        name: "<span class='webix_icon row-icon fa-filter'></span> Experience",
                        visitors: experiment_overview.experiment.visitors.value,
                        conversions: experiment_overview.experiment.conversion.value,
                        cr: experiment_overview.experiment.cr.value.toFixed(2) + '%',
                        p_value: experiment_overview.experiment.p_value.value,
                        improvement: (experiment_overview.experiment.improvement.value.toFixed(2) > 0 ? '<span style="color: #669900;" title="Positive">' + experiment_overview.experiment.improvement.value.toFixed(2) + '%</span>' : '<span style="color: #cc0000;"  title="Negative">' + experiment_overview.experiment.improvement.value.toFixed(2) + '%</span>')
                    }, {
                        name: "<span class='webix_icon row-icon fa-cogs'></span> Control",
                        visitors: experiment_overview.cg.visitors.value,
                        conversions: experiment_overview.cg.conversion.value,
                        cr: experiment_overview.cg.cr.value.toFixed(2) + '%',
                        p_value: experiment_overview.cg.p_value.value,
                        improvement: experiment_overview.cg.improvement.value
                    },
                    // 	{  var:"<span class='webix_icon row-icon fa-copyright'></span>Control</font>", imp:1972, crr:511495, cv:9.2, ctb:2}
                ]
            });

            console.log('My dataset', experiment_overview.averages);

            webix.ui({
                container: "averageReportSection",
                view: "datatable",
                id: 'expAveragesDataTable',
                width: $("#mainContainerDashboard").width() - 10,
                autoheight: true,
                css: "dataRangeTabularform",
                columns: [{
                    id: "name",
                    header: "Property",
                    width: ($("#mainContainerDashboard").width() / 4)
                }, {
                    id: "experiment",
                    header: "Experiment",
                    width: ($("#mainContainerDashboard").width() / 4)
                }, {
                    id: "controlgroup",
                    header: "Control Group",
                    width: ($("#mainContainerDashboard").width() / 4)
                }, {
                    id: "improvement",
                    header: "Improvement (%)",
                    width: ($("#mainContainerDashboard").width() / 4)
                }],
                data: experiment_overview.averages
            });
        });
    }

    function generateCgPieChart(expid) {
        // d3.json(monolop_api_base_url+'/api/reports/experiments/getControlGroups/'+expid, function(data){
        webix.ajax().get(monolop_api_base_url + '/api/reports/experiments/getControlGroups/' + expid, function(data) {
            data = JSON.parse(data);
            /* ------- PIE SLICES -------*/

            if (data[0].value !== 0 || data[1].value !== 0) {
                var svg = d3.select("#cgPeiChartID")
                    .append("svg")
                    .append("g")

                svg.append("g")
                    .attr("class", "slices");
                svg.append("g")
                    .attr("class", "labels");
                svg.append("g")
                    .attr("class", "lines");

                var width = 500 + 40 + 20,
                    height = 300,
                    radius = (Math.min(width, height) / 2) - 60;

                var pie = d3.layout.pie()
                    .sort(null)
                    .value(function(d) {
                        return d.value;
                    });

                var arc = d3.svg.arc()
                    .outerRadius(radius * 0.8)
                    .innerRadius(radius * 0.4);

                var outerArc = d3.svg.arc()
                    .innerRadius(radius * 0.9)
                    .outerRadius(radius * 0.9);

                svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                var key = function(d) {
                    return d.data.label;
                };

                var color = d3.scale.ordinal()
                    .range(["#EE9000", "#78A300"]);

                function randomData() {
                    var labels = color.domain();
                    return labels.map(function(label) {
                        return {
                            label: label,
                            value: Math.random()
                        }
                    });
                }


                change(data);

                d3.select(".randomize")
                    .on("click", function() {
                        change(randomData());
                    });


                function change(data) {

                    /* ------- PIE SLICES -------*/
                    var slice = svg.select(".slices").selectAll("path.slice")
                        .data(pie(data), key);

                    slice.enter()
                        .insert("path")
                        .style("fill", function(d) {
                            return color(d.data.label);
                        })
                        .attr("class", "slice");

                    slice
                        .transition().duration(1000)
                        .attrTween("d", function(d) {
                            this._current = this._current || d;
                            var interpolate = d3.interpolate(this._current, d);
                            this._current = interpolate(0);
                            return function(t) {
                                return arc(interpolate(t));
                            };
                        })

                    slice.exit()
                        .remove();

                    /* ------- TEXT LABELS -------*/

                    var text = svg.select(".labels").selectAll("text")
                        .data(pie(data), key);

                    text.enter()
                        .append("text")
                        .attr("dy", ".35em")
                        .attr("font-size", "13px")
                        .text(function(d) {
                            return d.data.label + ": " + d.data.value.toFixed(2) + '% (Visitors: ' + Math.ceil(d.data.visitors) + ')';
                        });

                    function midAngle(d) {
                        return d.startAngle + (d.endAngle - d.startAngle) / 2;
                    }

                    text.transition().duration(1000)
                        .attrTween("transform", function(d) {
                            this._current = this._current || d;
                            var interpolate = d3.interpolate(this._current, d);
                            this._current = interpolate(0);
                            return function(t) {
                                var d2 = interpolate(t);
                                var pos = outerArc.centroid(d2);
                                pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                                return "translate(" + pos + ")";
                            };
                        })
                        .styleTween("text-anchor", function(d) {
                            this._current = this._current || d;
                            var interpolate = d3.interpolate(this._current, d);
                            this._current = interpolate(0);
                            return function(t) {
                                var d2 = interpolate(t);
                                return midAngle(d2) < Math.PI ? "start" : "end";
                            };
                        });

                    text.exit()
                        .remove();

                    /* ------- SLICE TO TEXT POLYLINES -------*/

                    var polyline = svg.select(".lines").selectAll("polyline")
                        .data(pie(data), key);

                    polyline.enter()
                        .append("polyline");

                    polyline.transition().duration(1000)
                        .attrTween("points", function(d) {
                            this._current = this._current || d;
                            var interpolate = d3.interpolate(this._current, d);
                            this._current = interpolate(0);
                            return function(t) {
                                var d2 = interpolate(t);
                                var pos = outerArc.centroid(d2);
                                pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                                return [arc.centroid(d2), outerArc.centroid(d2), pos];
                            };
                        });

                    polyline.exit()
                        .remove();
                };
            }
        });
    }

    function openLayoutDashboardExperiment() {
        var formElement = document.createElement('div');
        formElement.id = 'webixDashboardElement';

        document.getElementById("innerContent").appendChild(formElement);

        webix.ready(function() {
            webix.ajax().get(monolop_api_base_url + '/api/reports/experiments/getAll', function(response, xml, xhr) {
                var getTemplate = function() {
                    return "<div class='title'><a href='/reports/experiment/detail/#experimentID#'>#name#</a></div>"
                };

                webix.ui({
                    container: "webixDashboardElement",
                    type: "space",
                    rows: [{
                        cols: [{
                            view: "button",
                            value: "prev",
                            click: function() {
                                $$('pager1').select("prev");
                            }
                        }, {}, {
                            view: "button",
                            value: "next",
                            click: function() {
                                $$('pager1').select("next");
                            }
                        }]
                    }, {
                        view: "list",
                        data: response,
                        yCount: 10,
                        type: {
                            width: 500,
                            height: 65,
                            template: getTemplate()
                        },
                        pager: {
                            apiOnly: true,
                            id: "pager1",
                            size: 10,
                            animate: {
                                direction: "top"
                            }
                        }
                    }]
                });
            });
        });
        // initIntialTTandGuides();
    }

    global.openLayoutDashboardExperiment = openLayoutDashboardExperiment;
    global.openLayoutDashboardExperimentDetail = openLayoutDashboardExperimentDetail;
    global.experimentListConfig = dashboardExperimentConfig;
    global.page = experimentListConfig.page;
    global.placementWindowIframe = undefined;

})(window);


var showDetail = function() {

};
