"use strict";
(function(global) {
    var dashboardExperimentConfig = {
        folderManager: false,
        msg: "",
        currentStepInfo: {},

    };

    function openLayoutDashboardSegmentDetail(from_timestamp, to_timestamp) {
        var formElement = document.createElement('div');
        formElement.id = 'webixDashboardElement';
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "name_you_want");
        input.setAttribute("value", "value_you_want");
        document.getElementById("innerContent").appendChild(input);



        document.getElementById("innerContent").appendChild(formElement);
        // try {
        //     parent.location.hash = ''
        // } catch (e) {
        //     console.log('Report loaded in an iframe!');
        // }
        var href = location.href;
        var segmentid = href.substr(href.lastIndexOf('/') + 1);
        segmentid = segmentid.replace('#', '');
        var params = segmentid.indexOf('?');
        if (params !== -1) {
            segmentid = segmentid.substr(0, params);
        }
        webix.ready(function() {
            var dateFormat = 'DD-MMMM-YYYY';
            from_timestamp = from_timestamp || '';
            to_timestamp = to_timestamp || '';

            if (to_timestamp === '' || from_timestamp === '') {
                var to = new Date();
                var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 6);

                if(from_timestamp === '' || ((from_timestamp * 1000) < from.getTime())){
                    from_timestamp = moment(from.getTime()).format(dateFormat);
                }else {
                  from_timestamp = moment(from_timestamp * 1000).format(dateFormat);
                }
                to_timestamp = moment(to.getTime()).format(dateFormat);
            }

            webix.ajax().get(monolop_api_base_url + '/api/reports/segments/get/' + segmentid + '?from_date=' + from_timestamp + '&to_date=' + to_timestamp, function(response, xml, xhr) {
                response = JSON.parse(response);


                if (response.segment && response.segment._id) {

                    $("#viewHeader div.header:first").html(response.segment.name);
                    $("#viewHeader div.header:first").attr('style', 'margin-left: 2%;font-size:24px;');


                    webix.ajax().get(monolop_api_base_url + '/api/account/logs?ajax=true&type=App\\Segment&id=' + response.segment._id, function(response_logs, xml, xhr) {
                        var logs = JSON.parse(response_logs);

                        var timeline = "<div class=\"DIV_276\">";
                        timeline += "<div class=\"DIV_277\">";
                        timeline += "<div class=\"DIV_278\">";
                        timeline += "<h5 class=\"H5_279\">";
                        timeline += "Timeline";
                        timeline += "<\/h5>";
                        timeline += "<\/div>";
                        timeline += "<div class=\"DIV_280\">";
                        timeline += "							<ul class=\"UL_281\">";
                        if (logs.data.length > 0) {
                            for (var i = 0; i < logs.data.length; i++) {
                                timeline += "<li class=\"LI_282\">";
                                timeline += "<div class=\"DIV_283\">";
                                timeline += "<div class=\"DIV_284\">";
                                timeline += logs.data[i].created_at.slice(0, -3);
                                timeline += "<\/div> <span title=\"" + accountName + "\" class=\"STRONG_286\" style=\"text: wrap\">" + (accountName.length > 19 ? accountName.substr(0, 16) + '..' : accountName) + "<\/span>";
                                timeline += "<\/div>";
                                timeline += "<p class=\"P_287\">";
                                timeline += logs.data[i].msg;
                                timeline += "<\/p>";
                                timeline += "<\/li>";
                            }
                        } else {
                            timeline += "<li class=\"LI_300\">";
                            timeline += " You have no feeds yet.";
                            timeline += "<\/li>";
                        }


                        timeline += "<\/ul>";
                        timeline += "<\/div>";


                        var tabbar = {
                            view: "tabbar",
                            id: "tabbarView",
                            type: "bottom",
                            multiview: true,
                            css: "tabber_view",
                            options: [{
                                    css: "repart_tab",
                                    value: "<li class=\"detail_report\" id=\"LI_145\"><a href=\"javascript:void(0)\" id=\"A_146\">Summary<\/a><\/li>",
                                    id: 'aboutView',
                                    css: 'detailedReportTab'
                                }, {
                                    css: "summary_tab",
                                    value: "<li id=\"LI_143\"><a href=\"javascript:void(0)\" id=\"A_144\">Details<\/a><\/li>",
                                    id: 'listView',
                                    css: 'custom summaryTab'
                                },

                            ],
                            height: 40,
                            margin: 0
                        };

                        //console.log($('.datarangeFilter').width());
                        var formatted;
                        var data = {
                            css: "data_view",
                            cells: [{
                                    id: "aboutView",
                                    // width: 960,
                                    css: "aboutView segemnt",
                                    height: 700,
                                    cols: [{
                                            css: "chart1",
                                            rows: [{
                                                css: "currentSnapShotChartHeader",
                                                height: 52,
                                                // autoWidth: true,
                                                template: "<p class='heading'>Audience flow chart - " + response.segment.name + "</p>"
                                            }, {
                                                css: "datarangeFilter",
                                                height: 36,
                                                cols: [{}, {
                                                    css: "date-range-field-cl",
                                                    template: '<div id="date-range-field"><span>Enable date range</span></div><div id="datepicker-calendar"></div>'
                                                }, {}]
                                            }, {
                                                css: "chart1g",
                                                height: 598,
                                                template: '<div id="chart1ID"></div><div id="toolTip" class="tooltip" style="opacity:0; width:200px; height:90px; position:absolute;"><div id="header1" class="header3"></div><div class="header-rule"></div><div id="head" class="header h"></div><div class="header-rule"></div><div  id="header2" class="header2"></div><div  class="tooltipTail"></div></div>',
                                            }]
                                        }, {
                                            css: "chart2",
                                            height: 700,
                                            rows: [{
                                                css: "currentSnapShotChartHeader",
                                                height: 52,
                                                // autoWidth: true,
                                                template: "<p class='heading'>Audience bar chart - " + response.segment.name + "</p>"
                                            }, {
                                                css: "chart2g",
                                                height: 598 + 36,
                                                template: '<div id="chart2ID"></div>',
                                            }]
                                        }

                                    ]


                                }, {

                                }, {
                                    id: "listView",
                                    // height: 750,
                                    css: "listView",
                                    // width: 660,
                                    height: 550,
                                    template: '<div id="mainContainerDashboard2"><div id="dataRangeTabularform"></div></div>',
                                },

                            ]
                        };

                        $("#webixDashboardElement").empty();
                        webix.ui({
                            container: "webixDashboardElement",
                            id: "webixDashboardElementSegment",
                            type: 'wide',
                            margin: 0,
                            // height: $("#nav").height(),
                            rows: [{
                                css: "tabbar_timline",
                                // height: 883,
                                // autoheight: true,
                                cols: [{
                                    css: "tabbar",
                                    rows: [
                                        tabbar,
                                        data,

                                    ]
                                }, {
                                    css: "timeline segment",
                                    padding: 0,
                                    // height: $("#nav").height(),
                                    // scroll: true,
                                    template: timeline
                                }]
                            }, ]


                        });
                        //console.log($('.datarangeFilter').width());
                        // var from_dates = response.from_timestamp.split('-');
                        // var to_dates = response.to_timestamp.split('-');
                        // var _from = new Date(from_dates[0], from_dates[1] - 1, from_dates[2]);
                        // var _to = new Date(to_dates[0], to_dates[1] - 1, to_dates[2]);

                        var to = new Date();
                        var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 6);
                        var segment_updated_at = new Date(response.segment.updated_at);

                        var calendarStart = from,
                            calendarEnd = to;
                        if (segment_updated_at.getTime() > from.getTime()) {
                            calendarStart = segment_updated_at;
                        }

                        setTimeout(function() {

                            $('#date-range-field').DateRangesWidget({
                                inline: true,
                                date: [calendarStart, calendarEnd],
                                selectableDates: [segment_updated_at, calendarEnd],
                                calendars: 3,
                                mode: 'tworanges',
                                showOn: 'focus',
                                onChange: function(dates, el) {
                                    dates = getDates();
                                    if (!dates) {
                                        dates = [calendarStart, calendarEnd];
                                    }
                                    // dates = getDates();
                                    dates[0] = dates[0].replace(/,/g, "").replace(/ /g, "-");
                                    dates[1] = dates[1].replace(/,/g, "").replace(/ /g, "-");
                                    // update the range display

                                    var _date_from = dates[0];
                                    var _date_to = dates[1];

                                    $("#chart1ID").empty();
                                    $("#chart2ID").empty();
                                    openLayoutDashboardSegmentDetail(
                                        _date_from,
                                        _date_to
                                    );



                                }
                            });
                            $('#datepicker-calendar').hide();

                            $("#date-range-field").on('click', function() {
                                if ($('#datepicker-calendar').css('display') === 'none') {
                                    $('#datepicker-calendar').show();
                                } else {
                                    $('#datepicker-calendar').hide();
                                }

                            });
                        }, 500);

                        $$("tabbarView").attachEvent('onItemClick', function(id, i) {
                            $('#datepicker-dropdown').css('display', 'none');
                            if (i.target.innerText !== 'DETAILS') {
                                return false
                            }
                            setTimeout(function() {
                                segmentOverview(response.segment_cur_prev_dataset, response.segment);
                            }, 500);
                        });

                        generateSegmentBarChart(response.segment_cur_prev_dataset, 'bar_chart');
                        generateSegmentFlowChart(response.in_out_chart, response.selected, response.segment);


                        initIntialTTandGuides();
                    });

                }


            });
        });

    }

    function segmentOverview(segment_cur_prev_dataset, segmentt) {


        var arr = [];

        for (var i = 0; i < segment_cur_prev_dataset.length; i++) {
            arr.push({
                name: "<span class='webix_icon row-icon fa-filter'></span> " + segment_cur_prev_dataset[i].name,
                currently: segment_cur_prev_dataset[i].currently,
                previously: segment_cur_prev_dataset[i].previously,
                change: segment_cur_prev_dataset[i].change + '%'
            });
        }

        $("#dataRangeTabularform").empty();
        webix.ui({
            container: "dataRangeTabularform",
            view: "datatable",
            // hidden: true,
            width: $("#mainContainerDashboard2").width() - 10,
            autoheight: true,
            css: "dataRangeTabularform",
            columns: [{
                id: "name",
                header: "Name",
                width: ($("#mainContainerDashboard2").width() / 4) - 1.7
            }, {
                id: "currently",
                header: "Currently",
                width: ($("#mainContainerDashboard2").width() / 4) - 1.7
            }, {
                id: "previously",
                header: "Previously",
                width: ($("#mainContainerDashboard2").width() / 4) - 1.7
            }, {
                id: "change",
                header: "Change",
                width: ($("#mainContainerDashboard2").width() / 4) - 1.7
            }, ],
            data: arr
        });
    }

    function generateSegmentBarChart(segmentData, graph_type) {


        $("#chart2ID").empty();


        if (graph_type === 'bar_chart') {
            var series = 'cr';
            var data = [{
                name: "Currently",
                value: segmentData[0].currently
            }, {
                name: "Previously",
                value: segmentData[0].previously
            }];
            var margin = {
                    top: 50,
                    right: 50,
                    bottom: 50,
                    left: 50
                },
                width = $("#chart2ID").width() - margin.left - margin.right,
                height = 450 - margin.top - margin.bottom;


            var x = d3.scale.ordinal()
                .rangeRoundBands([0, width], .1);

            var y = d3.scale.linear()
                .range([height, 0]);

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom");

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left")
                .ticks(10, "");


            var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function(d) {
                    return "<div class='toolTip'><span class='label'>" + d.name + "</span><br /> <span class='value'>" + d.value + "</span></div>";
                })
            $("#chart2ID").empty();

            var svg = d3.select("#chart2ID").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


            svg.call(tip);

            x.domain(data.map(function(d) {
                return d.name;
            }));
            y.domain([0, d3.max(data, function(d) {
                return d.value;
            })]);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("text-anchor", "middle") // this makes it easy to centre the text as the transform is applied to the anchor
                .attr('class', 'y-label-text')
                .attr("transform", "translate(" + (-35) + "," + (height / 2) + ")rotate(-90)") // text is drawn off the screen top left, move down and out and rotate
                .text('Values');

            svg.selectAll(".bar")
                .data(data)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function(d) {
                    return (x(d.name) + 25);
                })
                .attr("width", x.rangeBand() - 50)
                .attr("y", function(d) {
                    return y(d.value);
                })
                .attr("height", function(d) {
                    return height - y(d.value);
                })
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);

            function type(d) {
                d.value = +d.value;
                return d;
            }
        }
    }

    function generateSegmentFlowChart(data, selected, segment) {
        var segmentSelected = selected;
        var segmentFlowData = data;
        var segmentSelectedDet = segment;

        segmentFlowData = $.map(segmentFlowData, function(value, index) {
            return [value];
        });

        // var p = me = this ;
        var maxWidth = $("#chart1ID").width();

        var outerRadius = (maxWidth / 2),
            innerRadius = outerRadius - 100,
            monthWidth = Math.max(400, (innerRadius * 2) - 250);


        var iText, iChords, eText, eChords;

        var angleRange = 320,
            baseYear = 2001,
            maxMonth = 1,
            maxYear = 12,
            monthOffset = (monthWidth) / (maxYear * 12 + maxMonth),
            countries,
            e_labels = [],
            e_chords = [],
            i_labels = [],
            i_chords = [],
            topCountryCount = 20,
            e_buf_indexByName = {},
            e_indexByName = {},
            e_nameByIndex = {},
            i_indexByName = {},
            i_nameByIndex = {},
            i_buf_indexByName = {},
            export_countries = [],
            import_countries = [],
            e_colorByName = {},
            i_colorByName = {},
            months = [],
            monthlyExports = [],
            monthlyImports = [],
            countriesGrouped,
            delay = 1200,
            refreshIntervalId,
            year = 0,
            month = -1,
            running = true,
            formatNumber = d3.format(",.0f"),
            formatCurrency = function(d) {
                return "-$" + formatNumber(d)
            },
            eTextUpdate,
            eChordUpdate,
            TextUpdate,
            iChordUpdate;

        var toolTip = d3.select(document.getElementById("toolTip"));
        var header = d3.select(document.getElementById("head"));
        var header1 = d3.select(document.getElementById("header1"));
        var header2 = d3.select(document.getElementById("header2"));

        var e_fill = d3.scale.ordinal().range(["#00AC6B", "#20815D", "#007046", "#35D699", "#60D6A9"]);
        var i_fill = d3.scale.ordinal().range(["#EF002A", "#B32D45", "#9B001C", "#F73E5F", "#F76F87"]);

        var monthsMap = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


        /*
        d3.select(document.getElementById("mainDiv"))
            .style("width",(outerRadius*2 + 200) + "px")
            .style("height",(outerRadius*2 + 200) + "px");

        */




        d3.select(document.getElementById("imgDiv"))
            .style("left", ((outerRadius - monthWidth / 2)) + "px");

        var svg = d3.select("#chart1ID")
            .style("width", (outerRadius * 2) + "px")
            .style("height", (outerRadius * 2) + "px")
            .append("svg")
            .attr("id", "svg")
            .style("width", (outerRadius * 2) + "px")
            .style("height", (outerRadius * 2) + "px");
        /*
        var svg = d3.select(document.getElementById("svgDiv"))
            .style("width", '100%')
            .style("height", '100%')
            .append("svg")
            .attr("id","svg")
            .style("width", '100%')
            .style("height", '100%');
        */
        // p.setHeight(outerRadius*2) ;

        var export_chord = d3.layout.arc_chord()
            .padding(.05)
            .sortSubgroups(d3.descending)
            .sortChords(d3.descending)
            .yOffsetFactor(-0.8);

        var import_chord = d3.layout.arc_chord()
            .padding(.05)
            .yOffsetFactor(0.7)
            .sortSubgroups(d3.descending)
            .sortChords(d3.descending);

        var arc = d3.svg.arc()
            .innerRadius(innerRadius)
            .outerRadius(innerRadius + 5);


        var dGroup = svg.append("g")
            .attr("class", "mainLabel")

        dGroup.append("text")
            .attr("class", "mainLabel")
            .attr("transform", "translate(" + (outerRadius - 20) + "," + (outerRadius + 30) + ")")
            .style("font-size", "0px");



        var gY = (outerRadius - (innerRadius * .8 / 2));

        var gradientGroup = svg.append("g")
            .attr("class", "gradient")
            .attr("transform", "translate(" + (outerRadius - 6) + "," + (gY) + ")");

        gradientGroup.append("rect")
            .attr("height", ((outerRadius + innerRadius * .7 / 2) - gY))
            .attr("width", 0)
            .style("fill", "url(#gradient1)");
        /*
        var mGroup=svg.append("g")
            .attr("class","months")
            .attr("transform", "translate(" + (outerRadius-monthWidth/2-20) + ","  + 40 + ")");
        */
        var eGroup = svg.append("g")
            .attr("class", "exports")
            .attr("transform", "translate(" + outerRadius + "," + (outerRadius) + ")");

        var iGroup = svg.append("g")
            .attr("class", "imports")
            .attr("transform", "translate(" + outerRadius + "," + (outerRadius) + ")");

        // from event.js

        function node_onMouseOver(d) {
            var t;

            if (typeof d.imports == "undefined") {
                t = "From: " + Number(d.exports);
            } else {
                t = "To: " + Number(d.imports);
            }
            toolTip.transition()
                .duration(200)
                .style("opacity", ".9");
            header1.text((d.index + 1) + ". " + d.label);
            header.text(t);
            //header2.text(t);
            toolTip.style("left", (d3.event.layerX + 15) + "px")
                .style("top", (d3.event.layerY - 75) + "px");
        }

        function node_onMouseOut(d) {

            toolTip.transition() // declare the transition properties to fade-out the div
                .duration(500) // it shall take 500ms
                .style("opacity", "0"); // and go all the way to an opacity of nil

        }
        /** Returns an event handler for fading a given chord group. */
        function fade(opacity) {

            return;

            return function(g, i) {
                svg.selectAll("path.chord")
                    .filter(function(d) {
                        //  return true;
                        return d.source.index != i && d.target.index != i;
                    })
                    .transition()
                    .style("opacity", opacity);
            };
        }
        // from data.js

        function initialize() {

            var count = maxYear * 12 + maxMonth;

            for (var i = 0; i < count; i++) {
                var o = {};
                o.index = i;
                o.month = monthsMap[i % 12];
                o.year = baseYear + Math.floor(i / 12);
                months.push(o);
            }

            for (var i = 0; i < topCountryCount; i++) {
                var l = {};
                l.index = i;
                l.label = "null";
                l.angle = 0;
                e_labels.push(l);

                var c = {}
                c.label = "null";
                c.source = {};
                c.target = {};
                e_chords.push(c);

                var l1 = {};
                l1.index = i;
                l1.label = "null";
                l1.angle = 0;
                i_labels.push(l1);

                var c1 = {}
                c1.label = "null";
                c1.source = {};
                c1.target = {};
                i_chords.push(c1);
            }

            createVerticalGradient('svg', 'gradient1', [{
                offset: '0%',
                'stop-color': '#00AC6B'
            }, {
                offset: '40%',
                'stop-color': '#FFFFFF',
                'stop-opacity': '0'
            }, {
                offset: '60%',
                'stop-color': '#FFFFFF',
                'stop-opacity': '0'
            }, {
                offset: '100%',
                'stop-color': '#9B001C'
            }]);



            gradientGroup.transition()
                .select("rect")
                .delay(delay * 1.5)
                .attr("width", 12);

            dGroup.transition()
                .selectAll("text")
                .delay(delay * 1.5)
                .style("font-size", "10px");
        }


        function fetchData() {

            var normalized = [];
            var csv = segmentFlowData;



            for (var i = 0; i < csv.length; i++) {
                var row = csv[i];

                var newRow = {};
                newRow.Year = '2001';
                newRow.Country = row.name;
                newRow.Month = '01';
                newRow.Imports = Number(row["I_1"]);
                newRow.Exports = Number(row["E_1"]);
                normalized.push(newRow);

            }

            topCountryCount = (csv.length > 20) ? 20 : csv.length;

            initialize();

            countriesGrouped = d3.nest()
                .key(function(d) {
                    return d.Year;
                })
                .key(function(d) {
                    return d.Month;
                })
                .entries(normalized);
            //console.debug(countriesGrouped);
            //Sum total deficit for each month
            var totalImport = 0;
            var totalExport = 0;
            for (var y = 0; y < countriesGrouped.length; y++) {
                var yearGroup = countriesGrouped[y];
                for (var m = 0; m < yearGroup.values.length; m++) {
                    var monthGroup = yearGroup.values[m];
                    for (var c = 0; c < monthGroup.values.length; c++) {
                        var country = monthGroup.values[c];
                        totalImport = Number(totalImport) + Number(country.Imports) * 10000000;
                        totalExport = Number(totalExport) + Number(country.Exports) * 10000000;
                    }
                    //    //console.log("totalExport=" + String(totalExport));
                    monthlyExports.push(totalExport);
                    monthlyImports.push(totalImport);
                }

            }

            //Start running
            update(0, 0);
            update(0, 0);
            //setInterval(run, delay);
            // run();


        }

        // from _buildChords.js
        function buildChords(y, m) {

            if (countriesGrouped[y] == undefined)
                return;


            countries = countriesGrouped[y].values[m].values;

            countries.sort(function(a, b) {
                //Descending Sort
                if (a.Exports > b.Exports) return -1;
                else if (a.Exports < b.Exports) return 1;
                else return 0;
            });
            export_countries = countries.slice(0, topCountryCount);

            countries.sort(function(a, b) {
                //Descending Sort
                if (a.Imports > b.Imports) return -1;
                else if (a.Imports < b.Imports) return 1;
                else return 0;
            });

            import_countries = countries.slice(0, topCountryCount);

            var import_matrix = [],
                export_matrix = [];

            e_buf_indexByName = e_indexByName;
            i_buf_indexByName = i_indexByName;

            e_indexByName = [];
            e_nameByIndex = [];
            i_indexByName = [];
            i_nameByIndex = [];
            var n = 0;

            // Compute a unique index for each package name
            var totalExports = 0;
            export_countries.forEach(function(d) {
                totalExports += Number(d.Exports);
                d = d.Country;
                if (!(d in e_indexByName)) {
                    e_nameByIndex[n] = d;
                    e_indexByName[d] = n++;
                }
            });

            export_countries.forEach(function(d) {
                var source = e_indexByName[d.Country],
                    row = export_matrix[source];
                if (!row) {
                    row = export_matrix[source] = [];
                    for (var i = -1; ++i < n;) row[i] = 0;
                }
                row[e_indexByName[d.Country]] = d.Exports;
            });

            // Compute a unique index for each country name.
            n = 0;
            var totalImports = 0;
            import_countries.forEach(function(d) {
                totalImports += Number(d.Imports);
                d = d.Country;
                if (!(d in i_indexByName)) {
                    i_nameByIndex[n] = d;
                    i_indexByName[d] = n++;
                }
            });

            import_countries.forEach(function(d) {
                var source = i_indexByName[d.Country],
                    row = import_matrix[source];
                if (!row) {
                    row = import_matrix[source] = [];
                    for (var i = -1; ++i < n;) row[i] = 0;
                }
                row[i_indexByName[d.Country]] = d.Imports;
            });

            var exportRange = angleRange * (totalExports / (totalExports + totalImports));
            var importRange = angleRange * (totalImports / (totalExports + totalImports));
            export_chord.startAngle(-(exportRange / 2))
                .endAngle((exportRange / 2));

            import_chord.startAngle(180 - (importRange / 2))
                .endAngle(180 + (importRange / 2));

            import_chord.matrix(import_matrix);
            export_chord.matrix(export_matrix);

            var tempLabels = [];
            var tempChords = [];

            for (var i = 0; i < e_labels.length; i++) {
                e_labels[i].label = 'null';
                e_chords[i].label = 'null';
            }
            //console.debug( 'yyy' + export_chord.groups().length );
            for (var i = 0; i < export_chord.groups().length; i++) {
                var d = {}
                var g = export_chord.groups()[i];
                var c = export_chord.chords()[i];
                d.index = i;
                d.angle = (g.startAngle + g.endAngle) / 2;
                d.label = e_nameByIndex[g.index];
                if (typeof c == 'undefined')
                    continue;
                d.exports = c.source.value;
                var bIndex = e_buf_indexByName[d.label];
                if (typeof bIndex != 'undefined') { //Country already exists so re-purpose node.
                    e_labels[bIndex].angle = d.angle;
                    e_labels[bIndex].label = d.label;
                    e_labels[bIndex].index = i;
                    e_labels[bIndex].exports = d.exports;

                    e_chords[bIndex].index = i;
                    e_chords[bIndex].label = d.label;
                    e_chords[bIndex].source = c.source;
                    e_chords[bIndex].target = c.target;
                    e_chords[bIndex].exports = d.exports;

                } else { //Country doesnt currently exist so save for later
                    tempLabels.push(d);
                    tempChords.push(c);
                }
            }

            //Now use up unused indexes
            //console.debug( 'zzzz' +  e_labels.length);

            for (var i = 0; i < e_labels.length; i++) {
                if (e_labels[i].label == "null") {

                    var o = tempLabels.pop();
                    if (typeof o == 'undefined')
                        continue;
                    e_labels[i].index = e_indexByName[o.label];
                    e_labels[i].label = o.label;
                    e_labels[i].angle = o.angle;
                    e_labels[i].exports = o.exports;

                    var c = tempChords.pop();
                    e_chords[i].label = o.label;
                    e_chords[i].index = i;
                    e_chords[i].source = c.source;
                    e_chords[i].target = c.target;
                    e_chords[i].exports = c.exports;

                }
            }

            //remove null label ;
            var i = e_labels.length;
            while (i--) {
                if (e_labels[i].label == "null") {
                    e_labels.splice(i, 1);
                    e_chords.splice(i, 1);
                }
            }


            tempLabels = [];
            tempChords = [];

            for (var i = 0; i < i_labels.length; i++) {
                i_labels[i].label = 'null';
                i_labels[i].angle = Math.PI;
                i_chords[i].label = 'null';
            }
            //console.debug(import_chord.groups().length);
            for (var i = 0; i < import_chord.groups().length; i++) {
                var d = {}
                var g = import_chord.groups()[i];
                var c = import_chord.chords()[i];
                d.index = i;
                d.angle = (g.startAngle + g.endAngle) / 2;
                d.label = i_nameByIndex[g.index];
                if (typeof c == 'undefined')
                    continue;
                d.imports = c.source.value;
                var bIndex = i_buf_indexByName[d.label];
                if (typeof bIndex != 'undefined') { //Country already exists so re-purpose node.
                    i_labels[bIndex].angle = d.angle;
                    i_labels[bIndex].label = d.label;
                    i_labels[bIndex].imports = d.imports;
                    i_labels[bIndex].index = i;

                    i_chords[bIndex].index = i;
                    i_chords[bIndex].label = d.label;
                    i_chords[bIndex].source = c.source;
                    i_chords[bIndex].target = c.target;
                    i_chords[bIndex].imports = d.imports;

                } else { //Country doesnt currently exist so save for later
                    tempLabels.push(d);
                    tempChords.push(c);
                }
            }

            //Now use up unused indexes
            for (var i = 0; i < i_labels.length; i++) {
                if (i_labels[i].label == "null") {
                    var o = tempLabels.pop();
                    if (typeof o == 'undefined')
                        continue;
                    i_labels[i].index = i_indexByName[o.label];
                    i_labels[i].label = o.label;
                    i_labels[i].angle = o.angle;
                    i_labels[i].imports = o.imports;

                    var c = tempChords.pop();
                    i_chords[i].label = o.label;
                    i_chords[i].index = i;
                    i_chords[i].source = c.source;
                    i_chords[i].target = c.target;
                    i_chords[i].imports = c.imports;

                }
            }

            //remove null label ;
            var i = i_labels.length;
            while (i--) {
                if (i_labels[i].label == "null") {
                    i_labels.splice(i, 1);
                    i_chords.splice(i, 1);
                }
            }

            function getFirstIndex(index, indexes) {
                for (var i = 0; i < topCountryCount; i++) {
                    var found = false;
                    for (var y = index; y < indexes.length; y++) {
                        if (i == indexes[y]) {
                            found = true;
                        }
                    }
                    if (found == false) {
                        return i;
                        //  break;
                    }
                }
                //      //console.log("no available indexes");
            }

            function getLabelIndex(name) {
                for (var i = 0; i < topCountryCount; i++) {
                    if (e_buffer[i].label == name) {
                        return i;
                        //   break;
                    }
                }
                return -1;
            }
        }

        function update(y, m) {
            //updateMonths(y,m);
            //console.debug('update');
            buildChords(y, m);

            eText = eGroup.selectAll("g.group")
                .data(e_labels, function(d) {
                    return d.label;
                });

            iText = iGroup.selectAll("g.group")
                .data(i_labels, function(d) {
                    return d.label;
                });

            eChords = eGroup.selectAll("g.chord")
                .data(e_chords, function(d) {
                    return d.label;
                });

            iChords = iGroup.selectAll("g.chord")
                .data(i_chords, function(d) {
                    return d.label;
                });
            var selectedRec = segmentSelectedDet;
            var td = selectedRec.name;
            // me.ownerCt.setTitle('Segment Flow chart - ' + td) ;
            var l = (String)(td).length;
            //if(l < 15)
            //    l = 15 ;
            l = 15;
            var fs = (12 + (l - 10) * 4);
            dGroup.transition()
                .select("text")
                .delay(delay)
                .text(td)
                .attr("transform", "translate(" + (outerRadius - (td.length * fs / 2) / 2) + "," + ((outerRadius * 2) / 2 + 5) + ")")
                .style("font-size", fs + "px");

            eText.enter()
                .append("g")
                .attr("class", "group")
                .append("text")
                .attr("class", "export")
                .attr("dy", ".35em")
                .attr("text-anchor", function(d) {
                    return d.angle > Math.PI ? "end" : null;
                })
                .attr("transform", function(d) {
                    return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" +
                        "translate(" + (innerRadius + 6) + ")" +
                        (d.angle > Math.PI ? "rotate(180)" : "");
                })
                .text(function(d) {
                    return (d.index + 1) + ". " + d.label;
                })
                .on("mouseover", function(d) {
                    node_onMouseOver(d);
                })
                .on("mouseout", function(d) {
                    node_onMouseOut(d);
                });

            eText.transition()
                .duration(delay - 10)
                .select("text")
                .attr("dy", ".35em")
                .attr("text-anchor", function(d) {
                    return d.angle > Math.PI ? "end" : null;
                })
                .attr("transform", function(d) {
                    return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" +
                        "translate(" + (innerRadius + 6) + ")" +
                        (d.angle > Math.PI ? "rotate(180)" : "");
                })
                .text(function(d) {
                    return (d.index + 1) + ". " + d.label;
                });

            eText.exit().remove();

            eChords.enter()
                .append("g")
                .attr("class", "chord")
                .append("path")
                .attr("class", "chord")
                .style("stroke", function(d) {
                    return d3.rgb(getExportColor(d.source.index)).darker();
                })
                .style("fill", function(d) {
                    return getExportColor(d.source.index);
                })
                .style("fill-opacity", function(d, i) {
                    return .85 * (topCountryCount - d.index) / topCountryCount
                })
                .attr("d", d3.svg.arc_chord().radius(innerRadius))
                .style("opacity", 0)
                .on("mouseover", function(d) {
                    node_onMouseOver(d);
                })
                .on("mouseout", function(d) {
                    node_onMouseOut(d);
                });


            eChords.transition()
                .select("path")
                .duration(delay)
                .attr("d", d3.svg.arc_chord().radius(innerRadius))
                .style("stroke", function(d) {
                    return d3.rgb(getExportColor(d.source.index)).darker();
                })
                .style("fill", function(d) {
                    return getExportColor(d.source.index);
                })
                .style("stroke-opacity", function(d, i) {
                    return Math.max(.85 * (topCountryCount - d.index) / topCountryCount, .2);
                })
                .style("fill-opacity", function(d, i) {
                    return .85 * (topCountryCount - d.index) / topCountryCount
                })
                .style("opacity", 1);


            eChords.exit().remove();

            iText.enter()
                .append("g")
                .attr("class", "group")
                .append("text")
                .attr("class", "import")
                .attr("dy", ".35em")
                .attr("text-anchor", function(d) {
                    return d.angle > Math.PI ? "end" : null;
                })
                .attr("transform", function(d) {
                    return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" +
                        "translate(" + (innerRadius + 6) + ")" +
                        (d.angle > Math.PI ? "rotate(180)" : "");
                })
                .text(function(d) {
                    return (d.index + 1) + ". " + d.label;
                })
                .on("mouseover", function(d) {
                    node_onMouseOver(d);
                })
                .on("mouseout", function(d) {
                    node_onMouseOut(d);
                });

            iText.transition()
                .select("text")
                .duration(delay - 10)
                .attr("dy", ".35em")
                .attr("text-anchor", function(d) {
                    return d.angle > Math.PI ? "end" : null;
                })
                .attr("transform", function(d) {
                    return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")" +
                        "translate(" + (innerRadius + 6) + ")" +
                        (d.angle > Math.PI ? "rotate(180)" : "");
                })
                .text(function(d) {
                    return (d.index + 1) + ". " + d.label;
                });

            iText.exit()
                .attr("class", "exit")
                .transition()
                .duration(delay)
                .attr("y", 0)
                .attr("fill-opacity", 1e-6)
                .remove();

            iChords.enter()
                .append("g")
                .attr("class", "chord")
                .append("path")
                .attr("class", "chord")
                .style("stroke", function(d) {
                    return d3.rgb(getImportColor(d.source.index)).darker();
                })
                .style("stroke-opacity", function(d, i) {
                    return Math.max(.85 * (topCountryCount - d.index) / topCountryCount, .2);
                })
                .style("fill", function(d) {
                    return getImportColor(d.source.index);
                })
                .style("fill-opacity", function(d, i) {
                    return .7 * (topCountryCount - d.index) / topCountryCount
                })
                .attr("d", d3.svg.arc_chord().radius(innerRadius))
                .on("mouseover", function(d) {
                    node_onMouseOver(d);
                })
                .on("mouseout", function(d) {
                    node_onMouseOut(d);
                });

            iChords.transition()
                .select("path")
                .duration(delay - 10)
                .attr("d", d3.svg.arc_chord().radius(innerRadius))
                .style("stroke", function(d) {
                    return d3.rgb(getImportColor(d.source.index)).darker();
                })
                .style("fill", function(d) {
                    return getImportColor(d.source.index);
                })
                .style("stroke-opacity", function(d, i) {
                    return Math.max(.85 * (topCountryCount - d.index) / topCountryCount, .2);
                })
                .style("fill-opacity", function(d, i) {
                    return .7 * (topCountryCount - d.index) / topCountryCount
                });


            iChords.exit().remove();

        }



        function getExportColor(i) {
            var country = e_nameByIndex[i];
            if (e_colorByName[country] == undefined) {
                e_colorByName[country] = e_fill(i);
            }

            return e_colorByName[country];
        }

        function getImportColor(i) {
            var country = i_nameByIndex[i];
            if (i_colorByName[country] == undefined) {
                i_colorByName[country] = i_fill(i);
            }

            return i_colorByName[country];
        }
        fetchData();
    }

    global.openLayoutDashboardSegmentDetail = openLayoutDashboardSegmentDetail;
    global.experimentListConfig = dashboardExperimentConfig;
    global.page = experimentListConfig.page;
    global.placementWindowIframe = undefined;

})(window);
