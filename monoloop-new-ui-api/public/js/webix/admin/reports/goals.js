"use strict";
(function(global) {
    var dashboardGoalsConfig = {
        folderManager: false,
        msg: "",
        currentStepInfo: {},
        apis: {
            getAll: monolop_api_base_url + "/api/reports/goals/getAll"
        },
        formElement: {
            id: 'pageElementIdGoalsList',
        }
    };

    function openLayoutDashboardGoalsDetail() {

        var formElement = document.createElement('div');
        formElement.id = 'webixDashboardElement';
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "name_you_want");
        input.setAttribute("value", "value_you_want");
        document.getElementById("innerContent").appendChild(input);



        document.getElementById("innerContent").appendChild(formElement);

        // try {
        //     parent.location.hash = ''
        // } catch (e) {
        //     console.log('Report loaded in an iframe!');
        // }

        var href = location.href;
        var goalid = href.substr(href.lastIndexOf('_') + 1);
        goalid = goalid.replace('#', '');
        var params = goalid.indexOf('?') ;
        if(params !== -1){
          goalid = goalid.substr(0 , params );
        }

        webix.ready(function() {

            webix.ajax().get(monolop_api_base_url + '/api/reports/goals/getGoals/' + goalid, function(response, xml, xhr) {
                //  response={"experiment":{"_id":"57b561eaa6ff028f2b8b4589","account_id":"565fe2330f7bb23c678b5ba1","cg_day":30,"cg_size":50,"created_at":"2016-08-18 07:21:14","deleted":0,"description":"","experimentID":222,"folder_id":"565fe2330f7bb23c678b5ba6","goal_id":"565fe2e80f7bb23c678ba2a1","hidden":0,"name":"ExpNew17","segment_id":"565fe2cb0f7bb23c678b9165","significant_action":"0","updated_at":"2016-08-18 07:33:52"},"pages":1,"variations":3,"locations":2,"is_exp_running":1,"cr":1171.9653052262,"cr_cg":740.18270944742,"improvement":650.9810635514,"p_value":0.278,"visitors":189,"cg_visitors":311,"estimatedDays80PercentChance":6,"estimatedDays95PercentChance":10};
                response = JSON.parse(response);
                if (response.goal && response.goal._id) {
                    var heading = response.goal.name;
                    if(response.goal.uid){
                      heading = heading + ' (infoID: ' + response.goal.uid + ')';
                    }
                    $("div.header:first").html(heading);
                    $("div.header:first").attr('style', 'margin-left: 2%;font-size:24px;');

                    webix.ajax().get(monolop_api_base_url + '/api/account/logs?ajax=true&type=App\\Goals&id=' + response.goal._id, function(response_logs, xml, xhr) {

                        var goals, amount = 0,
                            cr = 0,
                            vg = 0,
                            pvg = 0,
                            totalVisit = 0;
                        if(response.goal.result.length > 0){
                          for (var i = 0; i < response.goal.result.length; i++) {
                              goals = response.goal.result[i].goals[response.goal.uid];
                              amount = (goals.amount ? goals.amount : 0) + amount;
                              vg = (goals.VisitsBeforeGoal ? goals.VisitsBeforeGoal : 0) + vg;
                              pvg = pvg + (goals.pagesBeforeGoal ? goals.pagesBeforeGoal : 0);
                              totalVisit = totalVisit + (response.goal.result[i].totalVisits ? response.goal.result[i].totalVisits : 0);
                          }

                          cr = amount > 0 ? ((amount / totalVisit) * 100) : 0;
                          vg = vg > 0 ? (vg / totalVisit) : 0;
                          pvg = pvg > 0 ? (pvg / totalVisit) : 0;
                        }
                        var logs = JSON.parse(response_logs);


                        var timeline = "<div class=\"DIV_276\">";
                        timeline += "<div class=\"DIV_277\">";
                        timeline += "<div class=\"DIV_278\">";
                        timeline += "<h5 class=\"H5_279\">";
                        timeline += "Timeline";
                        timeline += "<\/h5>";
                        timeline += "<\/div>";
                        timeline += "<div class=\"DIV_280\">";
                        timeline += "							<ul class=\"UL_281\">";
                        if (logs.data.length > 0) {
                            for (var i = 0; i < logs.data.length; i++) {
                                timeline += "<li class=\"LI_282\">";
                                timeline += "<div class=\"DIV_283\">";
                                timeline += "<div class=\"DIV_284\">";
                                timeline += logs.data[i].created_at.slice(0, -3);
                                timeline += "<\/div> <span title=\"" + accountName + "\" class=\"STRONG_286\" style=\"text: wrap\">" + (accountName.length > 19 ? accountName.substr(0, 16) + '..' : accountName) + "<\/span>";
                                timeline += "<\/div>";
                                timeline += "<p class=\"P_287\">";
                                timeline += logs.data[i].msg;
                                timeline += "<\/p>";
                                timeline += "<\/li>";
                            }
                        } else {
                            timeline += "<li class=\"LI_300\">";
                            timeline += " You have no feeds yet.";
                            timeline += "<\/li>";
                        }


                        timeline += "<\/ul>";
                        timeline += "<\/div>";




                        var data = {
                            css: "data_view",
                            cells: [{
                                    id: "listView",
                                    // height: 750,
                                    css: "goalView",

                                }

                            ]
                        };


                        webix.ui({
                            container: "webixDashboardElement",
                            type: 'wide',
                            margin: 0,
                            // hidden:true,
                            // height: $("#nav").height(),
                            rows: [

                                {
                                    css: "tabbar_timline",
                                    // height: 883,
                                    autoheight: true,
                                    cols: [{
                                        css: "tabbar",
                                        rows: [
                                            data,

                                        ]
                                    }, {
                                        css: "timeline",
                                        padding: 0,
                                        // height: $("#nav").height(),
                                        // scroll: true,
                                        template: timeline
                                    }]
                                },
                            ]


                        });



                        var width = $('.goalView').width() - 25;

                        var formatted;

                        var data = {
                            css: "data_view",
                            cells: [{
                                    id: "listView",
                                    // height: 750,
                                    css: "goalView",
                                    // width: 660,
                                    height: 883,
                                    type: "space",
                                    rows: [{
                                            css: "Table",
                                            cols: [{
                                                id: 'dataTableMonoloop',
                                                css: "dataRangeTabularform",
                                                width: width,
                                                autoheight: true,

                                                view: "datatable",
                                                columns: [{
                                                    id: "goal_name",
                                                    header: "Goal Name",
                                                    width: width / 5 - 1
                                                }, {
                                                    id: "goals",
                                                    header: "Goals",
                                                    width: width / 5 - 1
                                                }, {
                                                    id: "cr",
                                                    header: "CR %",
                                                    width: width / 5 - 1
                                                }, {
                                                    id: "vg",
                                                    header: "Visits/Goal",
                                                    width: width / 5 - 1
                                                }, {
                                                    id: "pvg",
                                                    header: "Page Views/Goal",
                                                    width: width / 5 - 1
                                                }],
                                                data: [{
                                                    goal_name: "<span class='webix_icon row-icon fa-flag'></span> " + response.goal.name,
                                                    goals: amount,
                                                    cr: cr,
                                                    vg: vg > 0 ? (Math.round(vg * 100) / 100): vg,
                                                    pvg: pvg > 0 ? (Math.round(pvg * 100) / 100): pvg
                                                }]
                                            }]
                                        },

                                        {
                                            css: "Line Chart",
                                            cols: [{
                                                css: "goalView",
                                                // height: "auto",
                                                width: width,
                                                template: '<div id="goalLineChart"><div id="dataRange"></div><div id="chartArea"></div></div>',
                                            }]
                                        }

                                    ]
                                },

                            ]
                        };

                        $("#webixDashboardElement").empty();
                        webix.ui({
                            container: "webixDashboardElement",
                            id: "webixDashboardElementGoal",
                            type: 'wide',
                            margin: 0,
                            // height: $("#nav").height(),
                            rows: [

                                {
                                    css: "tabbar_timline",
                                    // height: 883,
                                    autoheight: true,
                                    cols: [{
                                        css: "tabbar",
                                        rows: [
                                            data,

                                        ]
                                    }, {
                                        css: "timeline",
                                        padding: 0,
                                        // height: $("#nav").height(),
                                        // scroll: true,
                                        template: timeline
                                    }]
                                },
                            ]


                        });



                        webix.ui({
                            container: "dataRange",
                            css: "dateRangeGoalChartFilters",
                            height: 50,
                            type: 'wide',
                            // width: $("div.data_view:first").height()/2,
                            rows: [{
                                cols: [{
                                    type: "space",
                                    width: 20
                                }, {
                                    css: "dateRangeGoalChartFiltersDRField",
                                    width: 250,
                                    template: '<div id="goal-date-range-field"><span>Enable date range</span></div><div id="datepicker-calendar"></div>'
                                }, {}]
                            }]
                        });
                        var flag = true;
                        var to = new Date();
                        var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 6);
                        var goal_updated_at = new Date(response.goal.updated_at);

                        var calendarStart = from, calendarEnd= to;
                        if(goal_updated_at.getTime() > from.getTime()){
                          calendarStart = goal_updated_at;
                        }
                        generateReportGraph(from.getDate() + ' ' + monthNames[from.getMonth()] + ', ' +
                            from.getFullYear(),
                            to.getDate() + ' ' + monthNames[to.getMonth()] + ', ' +
                            to.getFullYear(),
                            'data_range', response);
                        $('#dataRange').DateRangesWidget({
                            inline: true,
                            date: [calendarStart, calendarEnd],
                            selectableDates: [goal_updated_at, calendarEnd],
                            calendars: 3,
                            mode: 'tworanges',
                            showOn: 'focus',
                            onChange: function(dates, el) {
                                dates = getDates();
                                dates[0] = dates[0].replace(/,/g, "").replace(/ /g, "-");
                                dates[1] = dates[1].replace(/,/g, "").replace(/ /g, "-");
                                updateHeaderValues(dates);
                                $('#chartArea').empty();
                                generateReportGraph(dates[0],
                                    dates[1],
                                    'data_range', response);

                            }
                        });
                        initIntialTTandGuides();

                    });


                }


            });
        });

    }

    function updateHeaderValues(dates){
      var params = '';
      if(dates){
        params = '?';
        // params += 'start_date=' + (new Date(dates[0]).getTime()/1000);
        // params += '&end_date=' + (new Date(dates[1]).getTime()/1000);
        params += 'start_date=' + dates[0];
        params += '&end_date=' + dates[1];
      }
      console.log(dates), 'sadsadsa';
      var goalid = location.href.substr(location.href.lastIndexOf('_') + 1);
      var url = monolop_api_base_url + '/api/reports/goals/getGoals/' + goalid + '/table' + params;
      $$("dataTableMonoloop").clearAll();
      $$("dataTableMonoloop").load(url);
    };

    function generateReportGraph(from_date, to_date, graph_type, response) {
        from_date = from_date || '';
        to_date = to_date || '';
        var formatted;
        //set the margins
        //  var width=$('#webixDashboardElement').width()-400;
        var margin = {
                top: 50,
                right: 160,
                bottom: 80,
                left: 150
            },
            width = $('.goalView').width() - 300,
            height = 400 - margin.top - margin.bottom;


        // set the type of number here, n is a number with a comma, .2% will get you a percent, .2f will get you 2 decimal points
        var NumbType = d3.format(".2f");

        // color array
        var bluescale4 = ["#8BA9D0", "#6A90C1", "#066CA9", "#004B8C"];

        //color function pulls from array of colors stored in color.js
        var color = d3.scale.ordinal().range(bluescale4);

        //define the approx. number of x scale ticks
        var xscaleticks = 5;

        //defines a function to be used to append the title to the tooltip.  you can set how you want it to display here.
        var maketip = function(d) {
            var tip = '<p class="tip3">' + d.name + '<p class="tip1">' + NumbType(d.value) + '</p> <p class="tip3">' + formatDate(d.date) + '</p>';
            return tip;
        }

        //define your year format here, first for the x scale, then if the date is displayed in tooltips
        var parseDate = d3.time.format("%m/%d/%y").parse;
        var formatDate = d3.time.format("%b %d, '%y");

        //create an SVG


        var svg = d3.select("#chartArea").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //make a rectangle so there is something to click on
        svg.append("svg:rect")
            .attr("width", width)
            .attr("height", height)
            .attr("class", "plot");

        //make a clip path for the graph
        var clip = svg.append("svg:clipPath")
            .attr("id", "clip")
            .append("svg:rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", width)
            .attr("height", height);

        // force data to update when menu is changed
        var _url = monolop_api_base_url + '/api/reports/goals/getGoalsOverview?uid=' + response.goal.uid + '&from_date=' + from_date + '&to_date=' + to_date;
        webix.ajax().get(_url, function(response_goals_overview, xml, xhr) {
            //console.log(response_goals_overview);
            response_goals_overview = JSON.parse(response_goals_overview);
            var data = [],
                obj, date;
            //if (response_goals_overview.length > 0) {
                for (var i = 0; i < response_goals_overview.length; i++) {
                    date = new Date(response_goals_overview[i].datestamp * 1000);
                    date = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear().toString().substr(2, 2);
                    obj = {
                        "type": "Line",
                        "date": date,
                        "Conversions": response_goals_overview[i].goals[response.goal.uid].amount
                    };
                    data.push(obj);
                }

                if(data.length == 0){
                  // console.log('Filling dummy data', from_date, to_date);
                  data.push({ "type": "Line","date": moment(from_date).format('MM/d/YY'),"Conversions": 0});
                  data.push({ "type": "Line","date": moment(to_date).format('MM/d/YY'),"Conversions": 0});
                }

                // console.log(data);
                formatted = data;
                redraw();


                d3.select(window)
                    .on("keydown", function() {
                        altKey = d3.event.altKey;
                    })
                    .on("keyup", function() {
                        altKey = false;
                    });
                var altKey;

                // set terms of transition that will take place
                // when a new economic indicator is chosen
                function change() {
                    d3.transition()
                        .duration(altKey ? 7500 : 1500)
                        .each(redraw);
                }

                // all the meat goes in the redraw function
                function redraw() {

                    // create data nests based on economic indicator (series)
                    var nested = d3.nest()
                        .key(function(d) {
                            return d.type;
                        })
                        .map(formatted)

                    // get value from menu selection
                    // the option values are set in HTML and correspond
                    //to the [type] value we used to nest the data
                    var series = 'Line';
                    // only retrieve data from the selected series, using the nest we just created
                    var data = nested[series];
                    // for object constancy we will need to set "keys", one for each type of data (column name) exclude all others.


                    color.domain(d3.keys(data[0]).filter(function(key) {
                        return (key !== "date" && key !== "type");
                    }));

                    var linedata = color.domain().map(function(name) {
                        return {
                            name: name,
                            values: data.map(function(d) {
                                return {
                                    name: name,
                                    date: parseDate(d.date),
                                    value: parseFloat(d[name], 10)
                                };
                            })
                        };
                    });


                    //make an empty variable to stash the last values into so i can sort the legend
                    var lastvalues = [];

                    //setup the x and y scales
                    var x = d3.time.scale()
                        .domain([
                            d3.min(linedata, function(c) {
                                return d3.min(c.values, function(v) {
                                    return v.date;
                                });
                            }),
                            d3.max(linedata, function(c) {
                                return d3.max(c.values, function(v) {
                                    return v.date;
                                });
                            })
                        ])
                        .range([0, width]);

                    var y = d3.scale.linear()
                        .domain([
                            d3.min(linedata, function(c) {
                                return d3.min(c.values, function(v) {
                                    return v.value;
                                });
                            }),
                            d3.max(linedata, function(c) {
                                return d3.max(c.values, function(v) {
                                    return v.value;
                                });
                            })
                        ])
                        .range([height, 0]);

                    //will draw the line
                    var line = d3.svg.line()
                        .x(function(d) {
                            return x(d.date);
                        })
                        .y(function(d) {
                            return y(d.value);
                        });

                    //define the zoomf
                    var zoom = d3.behavior.zoom()
                        .x(x)
                        .y(y)
                        .scaleExtent([1, 100])
                        .on("zoom", zoomed);

                    //call the zoom on the SVG
                    svg.call(zoom);

                    //create and draw the x axis
                    var xAxis = d3.svg.axis()
                        .scale(x)
                        .orient("bottom")
                        .tickPadding(8)
                        .ticks(xscaleticks);

                    svg.append("svg:g")
                        .attr("class", "x axis");

                    //create and draw the y axis
                    var yAxis = d3.svg.axis()
                        .scale(y)
                        .orient("left")
                        .tickSize(0 - width)
                        .tickPadding(8);

                    svg.append("svg:g")
                        .attr("class", "y axis");

                    //bind the data
                    var thegraph = svg.selectAll(".thegraph")
                        .data(linedata)

                    //append a g tag for each line and set of tooltip circles and give it a unique ID based on the column name of the data
                    var thegraphEnter = thegraph.enter().append("g")
                        .attr("clip-path", "url(#clip)")
                        .attr("class", "thegraph")
                        .attr('id', function(d) {
                            return d.name + "-line";
                        })
                        .style("stroke-width", 2.5)
                        .on("mouseover", function(d) {
                            d3.select(this) //on mouseover of each line, give it a nice thick stroke
                                .style("stroke-width", '6px');

                            var selectthegraphs = $('.thegraph').not(this); //select all the rest of the lines, except the one you are hovering on and drop their opacity
                            d3.selectAll(selectthegraphs)
                                .style("opacity", 0.2);

                            var getname = document.getElementById(d.name); //use get element cause the ID names have spaces in them
                            var selectlegend = $('.legend').not(getname); //grab all the legend items that match the line you are on, except the one you are hovering on

                            d3.selectAll(selectlegend) // drop opacity on other legend names
                                .style("opacity", .2);

                            d3.select(getname)
                                .attr("class", "legend-select"); //change the class on the legend name that corresponds to hovered line to be bolder
                        })
                        .on("mouseout", function(d) { //undo everything on the mouseout
                            d3.select(this)
                                .style("stroke-width", '2.5px');

                            var selectthegraphs = $('.thegraph').not(this);
                            d3.selectAll(selectthegraphs)
                                .style("opacity", 1);

                            var getname = document.getElementById(d.name);
                            var getname2 = $('.legend[fakeclass="fakelegend"]')
                            var selectlegend = $('.legend').not(getname2).not(getname);

                            d3.selectAll(selectlegend)
                                .style("opacity", 1);

                            d3.select(getname)
                                .attr("class", "legend");
                        });

                    //actually append the line to the graph
                    thegraphEnter.append("path")
                        .attr("class", "line")
                        .style("stroke", function(d) {
                            return color(d.name);
                        })
                        .attr("d", function(d) {
                            return line(d.values[0]);
                        })
                        .transition()
                        .duration(2000)
                        .attrTween('d', function(d) {
                            var interpolate = d3.scale.quantile()
                                .domain([0, 1])
                                .range(d3.range(1, d.values.length + 1));
                            return function(t) {
                                return line(d.values.slice(0, interpolate(t)));
                            };
                        });

                    //then append some 'nearly' invisible circles at each data point
                    thegraph.selectAll("circle")
                        .data(function(d) {
                            return (d.values);
                        })
                        .enter()
                        .append("circle")
                        .attr("class", "tipcircle")
                        .attr("cx", function(d, i) {
                            return x(d.date)
                        })
                        .attr("cy", function(d, i) {
                            return y(d.value)
                        })
                        .attr("r", 12)
                        .style('opacity', 1e-6) //1e-6
                        .attr("title", maketip);

                    //append the legend
                    var legend = svg.selectAll('.legend')
                        .data(linedata);

                    var legendEnter = legend
                        .enter()
                        .append('g')
                        .attr('class', 'legend')
                        .attr('id', function(d) {
                            return d.name;
                        })
                        .on('click', function(d) { //onclick function to toggle off the lines
                            if ($(this).css("opacity") == 1) { //uses the opacity of the item clicked on to determine whether to turn the line on or off

                                var elemented = document.getElementById(this.id + "-line"); //grab the line that has the same ID as this point along w/ "-line"  use get element cause ID has spaces
                                d3.select(elemented)
                                    .transition()
                                    .duration(1000)
                                    .style("opacity", 0)
                                    .style("display", 'none');

                                d3.select(this)
                                    .attr('fakeclass', 'fakelegend')
                                    .transition()
                                    .duration(1000)
                                    .style("opacity", .2);
                            } else {

                                var elemented = document.getElementById(this.id + "-line");
                                d3.select(elemented)
                                    .style("display", "block")
                                    .transition()
                                    .duration(1000)
                                    .style("opacity", 1);

                                d3.select(this)
                                    .attr('fakeclass', 'legend')
                                    .transition()
                                    .duration(1000)
                                    .style("opacity", 1);
                            }
                        });

                    //create a scale to pass the legend items through
                    var legendscale = d3.scale.ordinal()
                        .domain(lastvalues)
                        .range([0, 30, 60, 90, 120, 150, 180, 210]);

                    //actually add the circles to the created legend container
                    legendEnter.append('circle')
                        .attr('cx', width + 20)
                        .attr('cy', function(d) {
                            return legendscale(d.values[d.values.length - 1].value);
                        })
                        .attr('r', 7)
                        .style('fill', function(d) {
                            return color(d.name);
                        });

                    //add the legend text
                    legendEnter.append('text')
                        .attr('x', width + 35)
                        .attr('y', function(d) {
                            return legendscale(d.values[d.values.length - 1].value);
                        })
                        .text(function(d) {
                            return d.name;
                        });

                    // set variable for updating visualization
                    var thegraphUpdate = d3.transition(thegraph);

                    // change values of path and then the circles to those of the new series
                    thegraphUpdate.select("path")
                        .attr("d", function(d, i) {

                            //must be a better place to put this, but this works for now
                            lastvalues[i] = d.values[d.values.length - 1].value;
                            lastvalues.sort(function(a, b) {
                                return b - a
                            });
                            legendscale.domain(lastvalues);

                            return line(d.values);
                        });

                    thegraphUpdate.selectAll("circle")
                        .attr("title", maketip)
                        .attr("cy", function(d, i) {
                            return y(d.value)
                        })
                        .attr("cx", function(d, i) {
                            return x(d.date)
                        });


                    // and now for legend items
                    var legendUpdate = d3.transition(legend);

                    legendUpdate.select("circle")
                        .attr('cy', function(d, i) {
                            return legendscale(d.values[d.values.length - 1].value);
                        });

                    legendUpdate.select("text").style('fill', 'white')
                        .attr('y', function(d) {
                            return legendscale(d.values[d.values.length - 1].value);
                        });


                    // update the axes,
                    d3.transition(svg).select(".y.axis")
                        .call(yAxis);

                    d3.transition(svg).select(".x.axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    d3.transition(svg).append("text")
                        .attr("text-anchor", "middle") // this makes it easy to centre the text as the transform is applied to the anchor
                        .attr("transform", "translate(" + (-40) + "," + (height / 2) + ")rotate(-90)") // text is drawn off the screen top left, move down and out and rotate
                        .style('fill', 'white')
                        .style("font-size", "20px")
                        .text('Conversions');

                    d3.transition(svg).append("text")
                        .attr("text-anchor", "middle") // this makes it easy to centre the text as the transform is applied to the anchor
                        .attr("transform", "translate(" + (width / 2) + "," + (height + 50) + ")") // centre below axis
                        .style('fill', 'white')
                        .style("font-size", "20px")
                        .text("Date");

                    //make my tooltips work
                    $('circle').tipsy({
                        opacity: .9,
                        gravity: 'n',
                        html: true
                    });


                    //define the zoom function
                    function zoomed() {

                        svg.select(".x.axis").call(xAxis);
                        svg.select(".y.axis").call(yAxis);

                        svg.selectAll(".tipcircle")
                            .attr("cx", function(d, i) {
                                return x(d.date)
                            })
                            .attr("cy", function(d, i) {
                                return y(d.value)
                            });

                        svg.selectAll(".line")
                            .attr("class", "line")
                            .attr("d", function(d) {
                                return line(d.values)
                            });
                    }
                    //end of the redraw function
                }
            //}

        });


    }


    global.openLayoutDashboardGoalsDetail = openLayoutDashboardGoalsDetail;
    global.experimentListConfig = dashboardGoalsConfig;
    global.page = experimentListConfig.page;
    global.placementWindowIframe = undefined;

})(window);


var showDetail = function() {

};
