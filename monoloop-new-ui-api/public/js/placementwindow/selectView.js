var monoloop_select_view = {
  lock : false ,
  tempUnlock : false ,
  records : {} ,
  actionsOnXapth: [],
  toolbarType: "default",
  page: "",
  currentTransaction: {action: 'add', data: {}},
  variationsBubbleCalled: false,
  currentPageElement: '',
  editorID: '',
  currentFolder: '',
  source: "default",
  selectorsHaveVariations: [],
  currentExperiment: undefined,
  originalUrl: "",
  changedElements: [],
  addConditionToChangedElements: function(condition){
    for (var i = 0; i < monoloop_select_view.changedElements.length; i++) {
      if(monoloop_select_view.changedElements[i].xpath === CKEDITOR.config.mappedTo){
        monoloop_select_view.changedElements[i].condition = condition;
      }
    }
  },
  storeChangedContent: function(options){
    var found  = false;
    for (var i = 0; i < monoloop_select_view.changedElements.length; i++) {
      if(monoloop_select_view.changedElements[i].content_id === options.content_id){
        monoloop_select_view.changedElements[i].content_id = options.content_id;
        if(options.content !== undefined){
          monoloop_select_view.changedElements[i].content = options.content;
        }
        if(options.condition !== undefined){
          monoloop_select_view.changedElements[i].condition = options.condition;
        }
        found = true;
        break;
      }
    }
    if(found === false){
      monoloop_select_view.changedElements.push(options);
    }
  },
  saveChangedContent: function(){
    for (var i = 0; i < monoloop_select_view.changedElements.length; i++) {
      var selector = monoloop_select_view.changedElements[i].xpath,
          element = monoloop_select_view.getSelectorByXPath(selector),
          content = element.html();
      monoloop_select_view.postToParent(JSON.stringify({
            t: "save-changed-content-while-editing" ,
              data : {
                content_id: monoloop_select_view.changedElements[i].content_id,
                content: content,
                condition: monoloop_select_view.changedElements[i].condition ? monoloop_select_view.changedElements[i].condition : ''
              }
      }), "*");
      monoloop_select_view.changedElements.splice(i,1);
    }
  },
  originalContents: [],
  storeOriginalContent: function(options){
    var found  = false;
    for (var i = 0; i < monoloop_select_view.originalContents.length; i++) {
      if(monoloop_select_view.originalContents[i].xpath === options.xpath){
        found = true;
      }
    }
    if(found === false){
      monoloop_select_view.originalContents.push(options);
    }
  },
  restoreOriginalContents: function(){
    for (var i = 0; i < monoloop_select_view.originalContents.length; i++) {
      var selector = monoloop_select_view.originalContents[i].xpath,
          element = monoloop_select_view.getSelectorByXPath(selector);
      element.html(monoloop_select_view.originalContents[i].content);
    }
    MONOloop.jq('.removedElement').remove();
  },
  isPWchangedPosted: false,
  selectedElementsOnPage: [],
  newExperimentBackUp: undefined,
  totalVariations: 0,
  toolbarGroups: function(type){
    switch (type) {
      case 'image':
        return [
          // { name: 'ePlugins'},
          { name: 'others', groups: [ 'others' ] },
          { name: 'uploader', groups: [ 'uploader' ] },
        ];
        break;
      default:
        return [
          { name: 'ePlugins'},{ name: 'insert'},
          { name: 'others', groups: [ 'others' ] },
          { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
          // { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
          { name: 'document', groups: [ 'document', 'doctools', 'mode' ] },
          { name: 'forms', groups: [ 'forms' ] },
          // '/',
          { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
          { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
          { name: 'links', groups: [ 'links' ] },
          { name: 'uploader', groups: [ 'uploader' ] },
          // '/',
          { name: 'styles', groups: [ 'styles' ] },
          { name: 'colors', groups: [ 'colors' ] },
          { name: 'tools', groups: [ 'tools' ] },
        ];
        break;
    }
  },
  pushActionsOnXapth: function(xpath){
    if(monoloop_select_view.actionsOnXapth.indexOf(xpath) === -1){
      monoloop_select_view.actionsOnXapth.push(xpath);
    }
  },
  clearSelection : function(){
    var me = this ;
        MONOloop.jq('.monoloopBorderFix').removeClass('monoloopBorderFix') ;
        MONOloop.jq('.monoloopBorder').removeClass('monoloopBorder') ;
        MONOloop.jq('#temp-tooltip').remove();
        me.removeAllBorder() ;
        me.lock = false ;
        //monoloop_select_view.disPlayBorder() ;
        //monoloop_select_view.lock = false  ;
  } ,
  removeAllBorder : function(){
    var me = this ;
    MONOloop.jq('#ml-borders').html('') ;

    MONOloop.jq('.ml-border').css('top' ,0) ;
        MONOloop.jq('.ml-border').css('left' ,0) ;
        MONOloop.jq('.ml-border').css('width' ,0) ;
        MONOloop.jq('.ml-border').css('height' ,0) ;
  } ,
  checkParent : function(selectorString){
    var me = this ;
    var parent = MONOloop.jq(selectorString).parent() ;
    var selector = me.getXPath(parent) ;
    if( MONOloop.jq.trim(selector) == '')
      return false ;
    return true ;
  } ,
  checkChild : function(selectorString){
    var me = this ;
    if( MONOloop.jq(selectorString).children().length ){
      return true ;
    }
    return false ;
  } ,
  selectFocusParent : function(selectorString){
    var me = this ;
        if( MONOloop.jq(selectorString).parent().length ){
            me.clearSelection() ;
            var parent = MONOloop.jq(selectorString).parent() ;
          var selector = me.getXPath(parent) ;
            //top.selectTab.changeSelectorTxt(  selector  ) ;
            monoloop_select_view.postToParent(JSON.stringify({
              t: "select-changeSelectorTxt" ,
        selector : selector
            }), "*");
          //parent.click() ;
        }else{
            alert('Not Found'); // Not found element
        }
    } ,
    processCustomXPath : function(selectorString){
      var me = this ;
      if( MONOloop.jq(selectorString).length ){
        MONOloop.jq(selectorString).addClass('monoloopBorderFix') ;
        me.disPlayAllBorder() ;
      }
    } ,
    selectFocusFirstChild : function(selectorString){
      var me = this ;
        if( MONOloop.jq(selectorString).children().length ){
            me.clearSelection() ;
            var firstChild = MONOloop.jq(selectorString).children();

            var selector = me.getXPath(firstChild) ;
            //top.selectTab.changeSelectorTxt(  selector  ) ;
            monoloop_select_view.postToParent(JSON.stringify({
              t: "select-changeSelectorTxt",
              selector : selector
            }), "*");
          //firstChild.click() ;
        }else{
            alert('Not Found'); // Not found element
        }
    } ,
    getActiveElement: function( document ){

       document = document || window.document;

       // Check if the active element is in the main web or iframe
       if( document.body === document.activeElement
          || document.activeElement.tagName == 'IFRAME' ){
           // Get iframes
           var iframes = document.getElementsByTagName('iframe');
           for(var i = 0; i<iframes.length; i++ ){
               // Recall
               var focused = getActiveElement( iframes[i].contentWindow.document );
               if( focused !== false ){
                   return focused; // The focused
               }
           }
       }

      else return document.activeElement;

       return false;
  },
  processImage : function(dom){
    var me = this ;
    var base = MONOloop.jq('head base').attr('href') ;

    var src = MONOloop.jq(dom).attr('src') ;
    var t1 = src.substr(0,7).toLowerCase();
    var t2 = src.substr(0,8).toLowerCase();
    if( t1 == 'http://' || t2 == 'https://')
      return ;
    var t3 = src.substr(0,1) ;
    if( t3 == '/'){
      var pathArray = base.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      MONOloop.jq(dom).attr('src' , protocol + '//' + host + src ) ;
    }else{
      MONOloop.jq(dom).attr('src' , base + '/' + src) ;
    }

  } ,
  //  Old version of getting selected content ;
  getSelectContent : function(){
    var me = this ;
    select = MONOloop.jq('.monoloopBorderFix:eq(0)') ;

      if( select.length > 0){
        if(  select[0].nodeName == 'IMG' ){
        me.processImage(select[0]) ;
      }else{
        MONOloop.jq('.monoloopBorderFix:eq(0) img').each(function(index){
          me.processImage(this) ;
        }) ;
      }
      }
        var data = MONOloop.jq('.monoloopBorderFix:eq(0)').outerHTML() ;
        data = data.replace('monoloopBorderFix' , '') ;
        data = data.replace('monoloopBorder' , '') ;
        return  data ;
    } ,
    // New version for returning multiple data ;
    getSelectedData : function(){
      var me = this ;
      var borderFixes  = MONOloop.jq('.monoloopBorderFix') ;
      var data = {} ;
      for( var i = 0 ; i < borderFixes.length ; i++){
         var el = borderFixes[i] ;
         var xPath = me.getXPath(el);
         var content = me.getContentByXPath(xPath);
         data['content[' + i + '][xpath]'] = xPath;
         data['content[' + i + '][content]'] = content;
      }
      monoloop_select_view.postToParent(JSON.stringify({
            t: "select-receiveIframeMultiSelectedData" ,
        data : data
      }), "*");
    } ,
    getContentByXPath : function(xpath){
      var me = this ;
      select = MONOloop.jq(xpath) ;
      if( select.length > 0){
        if(  select[0].nodeName == 'IMG' ){
        me.processImage(select[0]) ;
      }else{
        MONOloop.jq(xpath + ' img').each(function(index){
          me.processImage(this) ;
        });
      }
      }
      var data = MONOloop.jq(xpath).outerHTML() ;
        data = data.replace('monoloopBorderFix' , '') ;
        data = data.replace('monoloopBorder' , '') ;
        return  data ;
    } ,
    setContentByXPath : function(xpath, updatedHtml, contentType){
      var me = this;
      var selector = monoloop_select_view.getSelectorByXPath(xpath);
      switch (contentType) {
        case 'image':
          var newSelector;
          selector.replaceWith(updatedHtml);
          newSelector = monoloop_select_view.getSelectorByXPath(xpath);
          newSelector.load(function(){
            monoloop_select_view.bindElement("#" + newSelector.attr('id'));
          });
          break;
        default:
          selector.html(updatedHtml);
          break;
      }
    } ,
    getSelectorByXPath : function(xpath){
      var me = this ;
      selector = MONOloop.jq(xpath) ;
      return  selector ;
    } ,
    getXPath : function(el){
      var selector = MONOloop.jq(el).parents()
                .map(function(){
                    var id = MONOloop.jq(this).attr("id");
                    var returnPart = this.tagName ;
                    if( returnPart == 'HTML'  || returnPart == 'BODY')
                      return returnPart ;
                    if (id) {
                      id = id.replace(/\./g, '\\.');
                      id = id.replace(' ', '\\ ');
                        returnPart += "#"+ id;
                    }else{
                        var classNames = MONOloop.jq(this).attr("class");
                        if (MONOloop.jq.trim(classNames) ) {
                        returnPart += "." + MONOloop.jq.trim(classNames).replace(/\s+/gi, ".");
                        }
                    }
                    return  returnPart ;
                })
                .get().reverse().join(" ");
        var selectorTest = selector ;
        if (selector) {
          selector += " "+ MONOloop.jq(el)[0].nodeName;
        }
        var id = MONOloop.jq(el).attr("id");
        if (id) {
          if(id.indexOf("ml_custom_ckedit_id_") === -1){
            id = id.replace(/\./g, '\\.');
            id = id.replace(' ', '\\ ');
            selector += "#"+ id;
          }
        }
        var classNames = MONOloop.jq(el).attr("class");
        if (MONOloop.jq.trim(classNames)) {
          selector += "." + MONOloop.jq.trim(classNames).replace(/\s+/gi, ".");
          //.monoloopBorder.monoloopBorderFix
          selector = selector.replace(/.monoloopBorderFix/ , '') ;
          selector = selector.replace(/.monoloopBorder/ , '') ;
          var excludePattern = /.cke_editable|.cke_editable_inline|_inline|.cke_contents_ltr|.cke_show_borders|.cke_focus|.monoloop_remove/ig;
          selector = selector.replace(
            excludePattern,
            function replacer(match){
              return "";
            }
          );
        }
        var curElement = MONOloop.jq(selector);
        if( curElement.length > 1){
             for( var i = 0 ; i < curElement.length ; i++){
                if( el ==  curElement[i]){
                    selector += ':eq('+i+')' ;
                }
             }
        }

        return this.cleanUpXpath(selector) ;
    } ,


    cleanUpXpath:function(selector){

      var res = selector.split(" ");

      var index = 0 ;
      var temp = null ;
      var xpath1 = '' ;
      var masternode = MONOloop.jq(selector).get( 0 ) ;

      while(index < res.length){
        temp = res.slice(0) ;
        res.splice(index, 1);

        if(res.length <= 2){
          break ;
        }

        xpath1 = res.join(" ");

        var node1 = MONOloop.jq(xpath1).get( 0 ) ;

        if((node1.compareDocumentPosition(masternode) === 0) && (temp[index].indexOf('#') === -1 ) ){
          ;
        }else{
          index++ ;
          res = temp.slice(0) ;

        }

      }

      var last_id_index = -1 ;

      for(var i = res.length -1 ; i >= 0 ; i-- ){
        if(res[i].indexOf('#') !== -1 ){
          if( last_id_index === -1){
            last_id_index = i ;
          }else{
            res[i] = '' ;
          }
        }
      }



      return res.join(" ") ;
    },

    // For show popup menu position.
    getScrollXY : function () {
      var scrOfX = 0, scrOfY = 0;
      if( typeof( window.pageYOffset ) == 'number' ) {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
      } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
      } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
      }
      return [ scrOfX, scrOfY ];
    } ,
    addAndDisplayBorder : function(id ,  el){
        if( id != 'ml-border'){
            MONOloop.jq('#ml-borders').append('<div id="'+id+'-l" class="ml-border"></div><div id="'+id+'-r" class="ml-border"></div><div id="'+id+'-s" class="ml-border"></div><div id="'+id+'-u" class="ml-border"></div>');
        }

        var $el = MONOloop.jq(el) ;
        if($el.length > 0){
          var top = $el.offset().top ;
          var left = $el.offset().left ;
          var hight = $el.outerHeight();
          var width =  $el.outerWidth();
          var xpath = monoloop_select_view.getXPath(el);
          if(xpath.split(/#(.+)/)[1] != undefined){
            var parent = xpath.split('#');
            var child = '#'+(xpath.split(/#(.+)/)[1])
          }else if(xpath.indexOf('.') != -1){
            var parent = xpath.split('.');
            var child = '.'+(xpath.substring(xpath.indexOf('.')+1))
          }else{
            var parent = xpath.split('.');
            var child = '';
          }

          if(id == 'ml-border'){
             MONOloop.jq('body').append('<div class="tooltiptext" id="temp-tooltip"><span style="color:#FBADFF;">'+parent[0]+'</span><span style="color:skyblue;">'+child+'</span>  |  <span style="font-size: 11px !important;">'+width+' x '+hight+'</span><span class="arrow" id="tooltipArrow"></span></div>');
          }

          MONOloop.jq('#'+id+'-l').css('top' , top -3) ;
          MONOloop.jq('#'+id+'-l').css('left' ,  left - 3) ;
          MONOloop.jq('#'+id+'-l').css('width' , 3) ;
          MONOloop.jq('#'+id+'-l').css('height' , hight + 6) ;

          MONOloop.jq('#'+id+'-u').css('top' , top -3) ;
          MONOloop.jq('#'+id+'-u').css('left' ,  left ) ;
          MONOloop.jq('#'+id+'-u').css('width' , width) ;
          MONOloop.jq('#'+id+'-u').css('height' , 3) ;

          MONOloop.jq('#'+id+'-r').css('top' , top -3) ;
          MONOloop.jq('#'+id+'-r').css('left' ,  left + width) ;
          MONOloop.jq('#'+id+'-r').css('width' , 3) ;
          MONOloop.jq('#'+id+'-r').css('height' , hight + 6) ;

          MONOloop.jq('#'+id+'-s').css('top' , top + hight) ;
          MONOloop.jq('#'+id+'-s').css('left' ,  left ) ;
          MONOloop.jq('#'+id+'-s').css('width' , width) ;
          MONOloop.jq('#'+id+'-s').css('height' , 3) ;

          var tooltipWidth = MONOloop.jq('#temp-tooltip').outerWidth();
          var screenWidth = MONOloop.jq(window).width();
          if(id == 'ml-border'){
            if(top < 21){
              top = hight + 30;
              MONOloop.jq('#temp-tooltip').removeClass('tooltiptext').addClass('tooltiptext-bottom');
            }else{
              top = top -31;
            }
            if((left + tooltipWidth) > screenWidth){
              MONOloop.jq('#temp-tooltip').css({'top':top, 'left': (screenWidth-tooltipWidth)-2});
              MONOloop.jq('#tooltipArrow').css('left', left - ((screenWidth-tooltipWidth)-2));

            }else{
              MONOloop.jq('#temp-tooltip').css({'top':top, 'left': left}) ;
            }
          }
        }

      } ,
  disPlayAllBorder : function(){
    var me = this ;
    me.removeAllBorder() ;
    // Display border fixed.
    var borderFixes  = MONOloop.jq('.monoloopBorderFix') ;
    for( var i = 0 ; i < borderFixes.length ; i++){
      me.addAndDisplayBorder('ml-border-' + i , borderFixes[i]) ;
    }
  } ,
  displayCursorBorder : function(){
    var me = this ;
    var cursorBorder = MONOloop.jq('.monoloopBorder') ;
    me.addAndDisplayBorder('ml-border'  , cursorBorder[0]) ;
  } ,
  variationsGroupByXpath: function(data, fn){
    var groups = {};
    data.forEach( function( o ){
      var group = JSON.stringify( fn(o) );
      groups[group] = groups[group] || [];
      groups[group].push( o );
    });
    return Object.keys(groups).map( function( group ){
      return groups[group];
    })
  },
  getXPathId: function(xpath){
    return xpath.replace(/ /g, "_").replace(/#/g, "_").replace(/\./g,"_").replace(/\(/g,"_").replace(/\)/g,"_").replace(/:/g,"_");
  },
  createImageCounterBubble: function(data){
    var selector = data.mappedTo,
        totalImages,
        element;
    MONOloop.jq('.imageCounter').remove();
    element = monoloop_select_view.getSelectorByXPath(selector);
    if(MONOloop.jq(element).length !== 0){
      totalImages = element.find('img').length;
      if(totalImages > 0){
        var right = MONOloop.jq(element).offset().left + MONOloop.jq(element).outerWidth();
        var top = MONOloop.jq(element).offset().top ;
        var bubbleHtml = '<div class="speech-bubble red-bubble imageCounter ' + data.selectorId + '" style="position: absolute;top: ' + (top + 20) + 'px' + ';left: ' + (right - 100) + 'px' + ';" >' + totalImages + '</div>';
        MONOloop.jq('body').prepend(bubbleHtml);
      }
    }
  },
  createDeleteBubble: function(data){
    var selector = data.mappedTo,
        totalVariations,
        element,
        selectorId = selector.replace(/#/g, monoloop_select_view.hashFn);

    element = monoloop_select_view.getSelectorByXPath(selector);
    if(MONOloop.jq(element).length !== 0){
      var right = MONOloop.jq(element).offset().left + MONOloop.jq(element).outerWidth();
      var top = MONOloop.jq(element).offset().top ;
      var id = "hiddenBubble_" + data.selectorId;
      var bubbleHtml = '<div id="'+id+'" originalUrl="'+monoloop_select_view.currentPageElement.originalUrl+'" selector="'+selector+'"  class="speech-bubble removedElement ' + data.selectorId + '" style="position: absolute;top: ' + top + 'px' + ';left: ' + (right - 50) + 'px' + ';" >' + 'X' + '</div>';
      MONOloop.jq('body').prepend(bubbleHtml);

      if(monoloop_select_view.currentPageElement){
        var iframeUrl = MONOloop.domain + '/component/content-list?fullURL=' + monoloop_select_view.currentPageElement.originalUrl;
        iframeUrl += ('&xpath=' + selectorId);
        iframeUrl += ('&hash_fn=' + monoloop_select_view.hashFn);
        iframeUrl += ('&page_element_id=' + monoloop_select_view.currentPageElement._id);
        var _id = monoloop_select_view.getXPathId(selector);
        if(MONOloop.jq("#"+_id).length > 0){
          monoloop_select_view.openVariationPopover(iframeUrl, id, 'deleteBubble', {t: 'delete'});
        }
      }


    }
  },
  hashFn: "_mp_hash_function_",
  bubblePositions: [],
  showVariationBubbles: function(data, dataSet){
    monoloop_select_view.variationsBubbleCalled = true;
    var selector,
        totalVariations,
        element,
        iframeUrl,
        hashFn = monoloop_select_view.hashFn;


    data.forEach(function(node){
      iframeUrl = MONOloop.domain + '/component/content-list?fullURL=' + dataSet.originalUrl;
      selector = node[0].mappedTo;
      selectorId = selector.replace(/#/g, hashFn);
      totalVariations = node.length || 0;
      element = monoloop_select_view.getSelectorByXPath(selector);

      if(monoloop_select_view.selectorsHaveVariations.indexOf(selector) === -1){
        monoloop_select_view.selectorsHaveVariations.push(selector);
      }


      monoloop_select_view.storeOriginalContent({
        xpath: selector,
        content: element.html()
      });

      iframeUrl += ('&xpath=' + selectorId);
      iframeUrl += ('&hash_fn=' + hashFn);
      iframeUrl += ('&page_element_id=' + dataSet.pageElement._id);

      var contentSelectorId = monoloop_select_view.getXPathId(selector), iframeUrl = '';
      iframeUrl = MONOloop.domain + '/component/content-menu?fullURL=' + monoloop_select_view.originalUrl;
      iframeUrl += ('&xpath=' + contentSelectorId);
      iframeUrl += ('&hash_fn=' + monoloop_select_view.hashFn);

      var selectorId = selector.replace(/#/g, monoloop_select_view.hashFn);
      var element = monoloop_select_view.getSelectorByXPath(selector);
      iframeUrl += ('&xpath1=' + selectorId);

      if(monoloop_select_view.currentPageElement){
        iframeUrl += ('&page_element_id=' + monoloop_select_view.currentPageElement._id);
      }

      if(monoloop_select_view.source !== undefined){
        iframeUrl += ('&source=' + monoloop_select_view.source);
      }
      if(monoloop_select_view.currentExperiment !== undefined){
        iframeUrl += ('&exp=' + monoloop_select_view.currentExperiment._id);
      }
      if(monoloop_select_view.currentFolder){
        iframeUrl += ('&current_folder=' + monoloop_select_view.currentFolder);
      }
      if(monoloop_select_view.page){
        iframeUrl += ('&page=' + monoloop_select_view.page);
      }

      iframeUrl += ('&tVar=' + monoloop_select_view.totalVariations);

      if(monoloop_select_view.newExperimentBackUp){
        iframeUrl += ('&bk_seg=' + monoloop_select_view.newExperimentBackUp.segmentExperiment);
        iframeUrl += ('&bk_goal=' + monoloop_select_view.newExperimentBackUp.goalExperiment);
      }




      if(MONOloop.jq(element).length !== 0){
        var right = MONOloop.jq(element).offset().left + MONOloop.jq(element).outerWidth();
        var top = MONOloop.jq(element).offset().top ;

        if(monoloop_select_view.bubblePositions.length > 0){
          for (var i = 0; i < monoloop_select_view.bubblePositions.length; i++) {
            if(monoloop_select_view.bubblePositions[i].right == right && monoloop_select_view.bubblePositions[i].top == top){
              right = right - 50;
              top = top;
              break;
            }
          }
        }

        monoloop_select_view.bubblePositions.push({right: right, top: top});


        var _id = monoloop_select_view.getXPathId(selector);
        // console.log(right,top);
        var bubbleHtml = '<div id="'+_id+'" originalUrl="'+dataSet.originalUrl+'" selector="'+selector+'" class="speech-bubble variations-counter"  style="position: absolute;top: ' + top + 'px' + ';left: ' + (right - 25) + 'px' + ';" >' + totalVariations + '</div>';
        MONOloop.jq('body').prepend(bubbleHtml);
        monoloop_select_view.openVariationPopover(iframeUrl, _id, 'varCountBubble', {title: "Variation(s)",t: 'content_list'});
      } else {

      }


    });
  },
  // adjust the popover position so that it doesn't intersects with CKEditor's Toolbar
  adjustPopoverPosition: function(){
    var _offset = MONOloop.jq('.webui-popover').offset();
    var _position = monoloop_select_view.getPopoverPosition();
    var _offsetDelta = (70);
    switch(_position){
      // as the toolbar is always on TOP, so we have to give popover DIV a small margin to keep it above the CKEITOR's Toolbar
      case "top":{
        // MONOloop.jq('.webui-popover').offset({top: _offset.top - _offsetDelta, left: _offset.left});
        MONOloop.jq('.webui-popover').offset({top: _offset.top - (_offsetDelta + 60), left: _offset.left});
        break;
      }
      case "bottom":{
        MONOloop.jq('.webui-popover').offset({top: _offset.top + (_offsetDelta - 30), left: _offset.left});
        break;
      }
    }
  },
  getPopoverPosition: function(){
    // get the current popover instance
    var currentPopover = MONOloop.jq('.webui-popover');
    var popoverArrowOffset = MONOloop.jq('.webui-arrow', currentPopover).offset();
    var popoverInnerOffset = MONOloop.jq('.webui-popover-inner', currentPopover).offset();
    return ((popoverArrowOffset.top < popoverInnerOffset.top) ? 'bottom' : 'top');
  },
  openVariationPopover: function(url, id, type, options){
    var settings = {
                  trigger:'click',
                  title:options.title,
                  content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
                  // width:auto,
                  multi:false,
                  closeable:true,
                  style:'',
                  delay:300,
                  padding:true,
                  backdrop:false
              };
    var iframeSettings = {  width:360,
                           height:420,
                           closeable:true,
                           padding:false,
                           multi:false,
                           trigger: "manual",
                           placement: "vertical",
                           dismissible: false,
                           autoHide:false,
                           type:'iframe',
                           cache:false,
                           url: url,
                           offsetTop:20,

                           onHide: function($element) {
                             monoloop_select_view.restoreOriginalContents();
                             if(type === 'content'){
                               var exID = MONOloop.jq(id).attr('id');
                               if(!exID){
                                 MONOloop.jq(id).prop('id', monoloop_select_view.randomID());
                               }
                               var id_ = MONOloop.jq(id).attr('id');

                               MONOloop.jq("#"+id_).webuiPopover('destroy');

                               MONOloop.jq('.webui-popover').remove();

                               monoloop_select_view.bindElement('#' + MONOloop.jq(id).attr('id'));
                             }


                           },
                           onShow: function($element) {
                             monoloop_select_view.adjustPopoverPosition($element);
                           }
                           // url: 'http://webix.com/'
                         };
    if(type === 'content'){
      MONOloop.jq(id).webuiPopover(MONOloop.jq.extend({},settings,iframeSettings));
    } else {
      iframeSettings.trigger = 'click'; // click-enabled popover incase of bubbles
      MONOloop.jq('#'+id).webuiPopover(MONOloop.jq.extend({},settings,iframeSettings));
    }
  },
  FAbEventFns: {
    variation: function(t, options){
      // if(options.selector){
      //
      //  // attach popover with add variation
      //  var url = options.url;
      //
      //  var selectorId = options.selector.replace(/#/g, options.hashFn);
      //  var element = monoloop_select_view.getSelectorByXPath(options.selector);
      //
      //  url += ('&xpath=' + selectorId);
      //  url += ('&hash_fn=' + options.hashFn);
      //  if(options.currentPageElement){
      //    url += ('&page_element_id=' + options.currentPageElement._id);
      //  }
      //  if(options.source !== undefined){
      //    url += ('&source=' + options.source);
      //  }
      //  if(options.experiment !== undefined){
      //    url += ('&exp=' + options.experiment._id);
      //  }
      //  if(t === 'edit'){
      //    if(options.content_id){
      //      url += ('&content_id=' + options.content_id);
      //    }
      //  }
      //  monoloop_select_view.initPopover(
      //    url,
      //    // '.actionButton.updateVariation.inline',
      //    options.domAddr,
      //    {
      //      width:360,
      //      placement: 'auto', // auto will place it automatically
      //      height:250,
      //      closeable:true,
      //      multi:true,
      //      padding:false,
      //      cache:false,
      //      offsetTop: (options.placement !== 'toolbar' ? -125 : 0),
      //      type:'iframe',
      //      title: options.title,
      //      url: url,
      //      onShow: function($element){
      //        if(options.placement !== 'toolbar'){
      //          var clientHeight = $element.height();
      //          $element.find('.webui-arrow').css('top', (clientHeight - 25) + 'px');
      //        }
      //      }
      //    },
      //    options.placement
      //  );
      // }
    },
    experiment: function(t, options){
      // // console.log("experiment: options", options);
      // // attach popover with add variation
      // var url = options.url;
      //
      // var selectorId = options.selector.replace(/#/g, options.hashFn);
      // var element = monoloop_select_view.getSelectorByXPath(options.selector);
      //
      // url += ('&xpath=' + selectorId);
      // url += ('&hash_fn=' + options.hashFn);
      // if(options.currentPageElement){
      //  url += ('&page_element_id=' + options.currentPageElement._id);
      // }
      // if(options.source !== undefined){
      //  url += ('&source=' + options.source);
      // }
      // if(options.currentFolder){
      //  url += ('&current_folder=' + options.currentFolder);
      // }
      // if(options.page){
      //  url += ('&page=' + options.page);
      // }
      // if(t === 'edit'){
      //  if(options.content_id){
      //    url += ('&content_id=' + options.content_id);
      //  }
      // }
      // if(monoloop_select_view.newExperimentBackUp){
      //  url += ('&bk_seg=' + monoloop_select_view.newExperimentBackUp.segmentExperiment);
      //  url += ('&bk_goal=' + monoloop_select_view.newExperimentBackUp.goalExperiment);
      // }
      // monoloop_select_view.initPopover(
      //  url,
      //  // '.actionButton.updateVariation.inline',
      //  options.domAddr,
      //  {
      //    width:360,
      //    placement: 'auto', // auto will place it automatically
      //    height:420,
      //    offsetTop: (options.placement !== 'toolbar' ? -210 : 0),
      //    closeable:true,
      //    padding:false,
      //    multi:true,
      //    cache:false,
      //    type:'iframe',
      //    title: options.title,
      //    url: url,
      //    onShow: function($element){
      //      if(options.placement !== 'toolbar'){
      //        var clientHeight = $element.height();
      //        $element.find('.webui-arrow').css('top', (clientHeight - 25) + 'px');
      //      }
      //
      //    }
      //  },
      //  options.placement
      // );
    },
    segment: function(t, options){
      // console.log("experiment: options", options);
      // attach popover with add variation
      var url = options.url;


      if(options.source !== undefined){
        url += ('&source=' + options.source);
      }
      if(options.currentFolder){
        url += ('&current_folder=' + options.currentFolder);
      }
      // console.log("(options.placement !== 'toolbar' ? -210 : 0)",(options.placement !== 'toolbar' ? -210 : 0));
      monoloop_select_view.initPopover(
        url,
        // '.actionButton.updateVariation.inline',
        options.domAddr,
        {
          // width:1100,
          width: 360,
          placement: 'top-left', // auto will place it automatically
          // height:520,
          height: 420,
          // offsetTop: (options.placement !== 'toolbar' ? -210 : 0),
          closeable:true,
          padding:false,
          multi: true,
          cache:false,
          style: "position: fixed;",
          type:'iframe',
          title: options.title,
          url: url,
          onShow: function($element){
            // console.log("options.placement out", options.placement);
            // if(options.placement !== 'toolbar'){
            //  console.log("options.placement", options.placement);
            //  var clientHeight = $element.height();
            //  $element.find('.webui-arrow').css('top', (clientHeight - 25) + 'px');
            // }
          },
          onHide: function(){
            MONOloop.jq('.subActionButton.segmentBuilder.inline.var').hide();
          },
        },
        options.placement
      );
    },
    goal: function(t, options){
      var url = options.url;


      if(options.source !== undefined){
        url += ('&source=' + options.source);
      }
      if(options.currentPageElement){
        url += ('&page_element_id=' + options.currentPageElement._id);
      }
      if(options.currentFolder){
        url += ('&current_folder=' + options.currentFolder);
      }

      monoloop_select_view.initPopover(
        url,
        // '.actionButton.updateVariation.inline',
        options.domAddr,
        {
          width:360,
          placement: 'top-left', // auto will place it automatically
          height:420,
          // offsetTop: (options.placement !== 'toolbar' ? -210 : 0),
          closeable:true,
          padding:false,
          multi: true,
          cache:false,
          type:'iframe',
          title: options.title,
          url: url,
          onShow: function($element){
            // if(options.placement !== 'toolbar'){
            //  var clientHeight = $element.height();
            //  $element.find('.webui-arrow').css('top', (clientHeight - 25) + 'px');
            // }
          },
          onHide: function(){
            MONOloop.jq('.subActionButton.goalBuilder.inline.var').hide();
          }
        },
        options.placement
      );
    }
  },
  actionButtonTypes: {
    type: "pageLevel",
    setType: function(t){
      t = t || "pageLevel";
      monoloop_select_view.actionButtonTypes.type = t;
    },
    fabOptions: false,
    pageLevel: {
      options: [
        {
          label: "Add audience",
          className: "segmentBuilder inline",
        }, {
          label: "Add goal",
          className: "goalBuilder inline",
        },
      ]
    },
    addVariation: {
      options: [

         {
          label: "Add condition",
          className: "conditionBuilder inline",
        },{
          label: "Add asset",
          className: "assetBuilder inline",
        },{
          label: "Hide",
          className: "remove inline",
        }
        ,
        {
          label: "Add audience",
          className: "segmentBuilder inline var",
        }, {
          label: "Add goal",
          className: "goalBuilder inline var",
        }
        ,{
          label: "Add to experience",
          className: "experimentBuilder inline",
        }
      ]
    },
    updateVariation: {
      options: [
        {
          label: "Add audience",
          className: "segmentBuilder inline var",
        }, {
          label: "Add goal",
          className: "goalBuilder inline var",
        }, {
          label: "Add condition",
          className: "conditionBuilder inline",
        },{
          label: "Add asset",
          className: "assetBuilder inline",
        },{
          label: "Hide",
          className: "remove inline",
        },{
          label: "Add to experience",
          className: "experimentBuilder inline",
        }
      ]
    }
  },
  initActionButtons: function(fabOptions){
    monoloop_select_view.actionButtonTypes.fabOptions = fabOptions;

    if(MONOloop.jq('#floatingContainer').length > 0){
      MONOloop.jq('#floatingContainer').remove();
    }

    var body = MONOloop.jq('body');

    var floatingContainer = document.createElement('div');
    floatingContainer.id = 'floatingContainer';
    floatingContainer.className = 'floatingContainer';



    var elementOption = monoloop_select_view.actionButtonTypes[monoloop_select_view.actionButtonTypes.type],
        buttons = elementOption.options;

    for (var i = 0; i < buttons.length; i++) {
      var subActionButton = document.createElement('div');
      subActionButton.className = "subActionButton " + buttons[i].className;

      var floatingText = document.createElement('p');
      floatingText.className = "floatingText";

      var floatingTextBG = document.createElement('span');
      floatingTextBG.className = "floatingTextBG";
      floatingTextBG.textContent = buttons[i].label;

      floatingText.appendChild(floatingTextBG);

      subActionButton.appendChild(floatingText);

      floatingContainer.appendChild(subActionButton);

    }

    var actionButton = document.createElement('div');
    actionButton.className = "actionButton";

    var floatingText = document.createElement('p');
    floatingText.className = "floatingText";

    var floatingTextBG = document.createElement('span');
    floatingTextBG.className = "floatingTextBG";
    floatingTextBG.textContent = "View Options";

    switch (monoloop_select_view.actionButtonTypes.type) {
      case 'pageLevel':
        // actionButton.className += " addVariation inline";
        // floatingTextBG.textContent = "Add Variation";
        break;
      case 'addVariation':
        actionButton.className += " addVariation inline";
        floatingTextBG.textContent = "Add Variation";
        break;
      case 'updateVariation':
        actionButton.className += " updateVariation inline";
        floatingTextBG.textContent = "Update Variation";
        break;
      default:
        break;

    }

    // floatingContainer.style.top = top + 'px';

    floatingText.appendChild(floatingTextBG);

    actionButton.appendChild(floatingText);

    floatingContainer.appendChild(actionButton);

    body.append(floatingContainer);

    monoloop_select_view.triggerActionButtonEvents(fabOptions);

  },
  triggerActionButtonEvents: function(fabOptions){
    MONOloop.jq('.floatingContainer').hover(function(){
      MONOloop.jq('.subActionButton').addClass('display'); // comment
    }, function(){
      MONOloop.jq('.subActionButton').removeClass('display'); // comment
      MONOloop.jq('.actionButton').removeClass('open'); // comment
    });
    MONOloop.jq('.subActionButton').hover(function(){
      MONOloop.jq(this).find('.floatingText').addClass('show');
    }, function(){
      MONOloop.jq(this).find('.floatingText').removeClass('show');
    });

    MONOloop.jq('.actionButton').hover(function(){
      MONOloop.jq(this).addClass('open');
      MONOloop.jq(this).find('.floatingText').addClass('show');
      MONOloop.jq('.subActionButton').addClass('display');
    }, function(){
      MONOloop.jq(this).find('.floatingText').removeClass('show');
    });
    monoloop_select_view.attachOnlickEvents(fabOptions);
  },
  attachOnlickEvents: function(fabOptions){

    var fns = monoloop_select_view.FAbEventFns;


    var editor = CKEDITOR.instances[fabOptions.editorID];
    switch (monoloop_select_view.actionButtonTypes.type) {
      case 'pageLevel':
        // for segment
        fns.segment('add', {
          url: MONOloop.domain + '/component/content-add-segment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.segmentBuilder.inline',
          title: 'Add audience',
        });
        // for goal
        fns.goal('add', {
          url: MONOloop.domain + '/component/content-add-goal?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentPageElement: monoloop_select_view.currentPageElement,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.goalBuilder.inline',
          title: 'Add goal',
        });
        break;
      case 'addVariation':
        var url = MONOloop.domain + '/component/content-create?fullURL=' + monoloop_select_view.originalUrl + '&tVar=' + monoloop_select_view.totalVariations;
        fns.variation('add', {
          url: url,
          originalUrl: monoloop_select_view.originalUrl,
          hashFn: monoloop_select_view.hashFn,
          selector: fabOptions.selector,
          currentPageElement: monoloop_select_view.currentPageElement,
          currentFolder: monoloop_select_view.currentFolder,
          source: monoloop_select_view.source,
          page: monoloop_select_view.page,
          experiment: monoloop_select_view.currentExperiment,
          domAddr: '.actionButton.addVariation.inline',
          title: 'Add variation',
        });
        // for experiments
        fns.experiment('add', {
          url: MONOloop.domain + '/component/content-add-to-experiment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          hashFn: monoloop_select_view.hashFn,
          selector: fabOptions.selector,
          currentPageElement: monoloop_select_view.currentPageElement,
          source: monoloop_select_view.source,
          page: monoloop_select_view.page,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.experimentBuilder.inline',
          title: 'Experiences',
        });

        // for segment
        fns.segment('add', {
          url: MONOloop.domain + '/component/content-add-segment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentFolder: monoloop_select_view.currentFolder,
          page: monoloop_select_view.page,
          domAddr: '.subActionButton.segmentBuilder.inline',
          title: 'Add audience',
        });

        // for goal
        fns.goal('add', {
          url: MONOloop.domain + '/component/content-add-goal?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentPageElement: monoloop_select_view.currentPageElement,
          page: monoloop_select_view.page,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.goalBuilder.inline',
          title: 'Add goal',
        });

        MONOloop.jq('.subActionButton.conditionBuilder.inline').on('click', function(e){
          editor.execCommand('insertCb', 'variation');
        });
        MONOloop.jq('.subActionButton.assetBuilder.inline').on('click', function(e){
          editor.execCommand('insertAsset');
        });
        MONOloop.jq('.subActionButton.remove.inline').on('click', function(e){
          editor.execCommand('insertRemove');
          if(monoloop_select_view.selectedElementsOnPage.length > 0){
            for (var i = 0; i < monoloop_select_view.selectedElementsOnPage.length; i++) {
              var view = monoloop_select_view.getSelectorByXPath(monoloop_select_view.selectedElementsOnPage[i]);
              view.html('');
            }
          }
        });
        break;
      case 'updateVariation':

        var url = MONOloop.domain + '/component/content-update?fullURL=' + monoloop_select_view.originalUrl;
        fns.variation('edit', {
          url: url,
          originalUrl: monoloop_select_view.originalUrl,
          hashFn: monoloop_select_view.hashFn,
          selector: fabOptions.selector,
          currentPageElement: monoloop_select_view.currentPageElement,
          source: monoloop_select_view.source,
          currentFolder: monoloop_select_view.currentFolder,
          page: monoloop_select_view.page,
          content_id: fabOptions.content_id,
          domAddr: '.actionButton.updateVariation.inline',
          title: 'Update variation',
        });

        // for experiments
        fns.experiment('edit', {
          url: MONOloop.domain + '/component/content-add-to-experiment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          hashFn: monoloop_select_view.hashFn,
          selector: fabOptions.selector,
          currentPageElement: monoloop_select_view.currentPageElement,
          page: monoloop_select_view.page,
          source: monoloop_select_view.source,
          currentFolder: monoloop_select_view.currentFolder,
          content_id: fabOptions.content_id,
          domAddr: '.subActionButton.experimentBuilder.inline',
          title: 'Experiences',
        });

        // for segment
        fns.segment('edit', {
          url: MONOloop.domain + '/component/content-add-segment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          page: monoloop_select_view.page,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.segmentBuilder.inline',
          title: 'Add audience',
        });

        // for goal
        fns.goal('edit', {
          url: MONOloop.domain + '/component/content-add-goal?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentPageElement: monoloop_select_view.currentPageElement,
          page: monoloop_select_view.page,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.goalBuilder.inline',
          title: 'Add goal',
        });

        editor.title = fabOptions.content_condition;
        MONOloop.jq(editor.element.$).attr('title',fabOptions.content_condition);
        MONOloop.jq('.subActionButton.conditionBuilder.inline').on('click', function(e){
          editor.execCommand('insertCb', {
            type: 'variation',
            condition: fabOptions.content_condition
          });

          // monoloop_select_view.postToParent(JSON.stringify({
          //  t: 'open-condition-builder',
          //  condition: fabOptions.content_condition,
          //  type: 'variation'
          // }));
        });
        MONOloop.jq('.subActionButton.assetBuilder.inline').on('click', function(e){
          editor.execCommand('insertAsset');
        });
        MONOloop.jq('.subActionButton.remove.inline').on('click', function(e){
          editor.execCommand('insertRemove');
        });
        break;
      default:
        break;

    }
    MONOloop.jq('.subActionButton.segmentBuilder.inline.var').hide();
    MONOloop.jq('.subActionButton.goalBuilder.inline.var').hide();

    monoloop_select_view.actionButtonTypes.setType();
  },
  initPopover: function(url, id, iframeSettings, placement){

    if(placement !== 'toolbar'){
      MONOloop.jq(".webui-popover").remove();
    }

    var settings = {
                  trigger:'click',
                  title:'Variation(s)',
                  content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
                  // width:auto,
                  multi:false,
                  closeable:false,
                  style:'',
                  delay:300,
                  padding:true,
                  backdrop:false
              };
    // console.log("iframeSettings",iframeSettings);
    MONOloop.jq(id).webuiPopover(MONOloop.jq.extend({},settings,iframeSettings));

  },
  showPreplace : function( records ){
    var me = this ;
    me.records = records ;
        MONOloop.jq('#preplaced-panel').html('') ;
        data = {} ;
        for( i = 0 ; i < records.length ; i++ ){
            if( typeof data[records[i]['element_id']] == "undefined" )
                data[records[i]['element_id']] = 1 ;
            else
                data[records[i]['element_id']]++ ;
        }
        i = 0 ;
        for (var key in data) {
            if( MONOloop.jq(key).length == 0)
                continue ;
            var right = MONOloop.jq(key).offset().left + MONOloop.jq(key).outerWidth();
            var top = MONOloop.jq(key).offset().top ;
            MONOloop.jq('#preplaced-panel').append('<div class="speech-bubble" onclick="monoloop_select_view.bubbleClick(event , \''+key+'\')" id="preplace-'+i+'" onmouseover="monoloop_select_view.bubbleMouseOver(event , \''+key+'\')">'+data[key]+'</div>') ;
            MONOloop.jq('#preplace-'+i).addClass('speech-bubble') ;
            MONOloop.jq('#preplace-'+i).css('top' , top) ;
            MONOloop.jq('#preplace-'+i).css('left' ,  right - 25) ;
            i++ ;
        }
    } ,
    bubbleClick : function(e , selector){
        MONOloop.jq(selector).addClass('monoloopBorderFix') ;
        monoloop_select_view.postToParent(JSON.stringify({
          t: "select-bubbleClick"  ,
          selector : selector
        }), "*")
        agent = MONOloop.jq.browser;
      if(agent.msie) {
        window.event.cancelBubble = true;
      } else {
        e.stopPropagation();
      }
    } ,
    bubbleMouseOver : function(e , selector){
        if( monoloop_select_view.lock == false ){
            agent = MONOloop.jq.browser;
            if(agent.msie) {
            window.event.cancelBubble = true;
            } else {
              e.stopPropagation();
            }
            MONOloop.jq('*').removeClass('monoloopBorder') ;
            MONOloop.jq('#temp-tooltip').remove();
            MONOloop.jq(selector).addClass('monoloopBorder') ;
            monoloop_select_view.displayCursorBorder() ;
        }
    } ,
    bindElement : function(element, t){

      element = element || "";
      var appllyEventsTo;

      if(element){
        appllyEventsTo = element + ', ' + element + ' *'; // :not(em, strong, u, .cke, .cke *)
      } else {
        appllyEventsTo = '*'; // :not(em, strong, u, .cke, .cke *)
      }

      MONOloop.jq(appllyEventsTo).unbind("mouseenter");
      MONOloop.jq(appllyEventsTo).mouseenter(function(event) {
          if(['html', 'body'].indexOf(MONOloop.jq(this).prop('tagName').toLowerCase()) === -1){
            if( monoloop_select_view.lock == false ){
                event.stopPropagation();

                // if parent is in edit-mode, return FALSE
                if(monoloop_select_view.isParentAlreadyInEditMode(this))
                  return false;

                MONOloop.jq('*').removeClass('monoloopBorder') ;
                MONOloop.jq('#temp-tooltip').remove();
                MONOloop.jq(this).addClass('monoloopBorder') ;
                monoloop_select_view.displayCursorBorder() ;
            }
          }
          return false;
      });
      MONOloop.jq(appllyEventsTo).unbind("mouseleave");
      MONOloop.jq(appllyEventsTo).mouseleave(function(event) {
          if(['html', 'body'].indexOf(MONOloop.jq(this).prop('tagName').toLowerCase()) === -1){
            if( monoloop_select_view.lock == false ){
                event.stopPropagation();

                MONOloop.jq(this).removeClass('monoloopBorder') ;
                MONOloop.jq('#temp-tooltip').remove();
                monoloop_select_view.displayCursorBorder() ;
            }

          }
          return false ;
      });
      MONOloop.jq(appllyEventsTo).unbind("click");
      MONOloop.jq(appllyEventsTo).click(function(event) {
        if(event.target.className.startsWith('cke')){
          return false;
        }else{
          event.stopPropagation();

          // if parent is in edit-mode, return FALSE
          if(monoloop_select_view.isParentAlreadyInEditMode(this))
            return false;

          MONOloop.jq('.webui-popover').remove();



          if(['html', 'body'].indexOf(MONOloop.jq(this).prop('tagName').toLowerCase()) === -1){
            if( monoloop_select_view.lock == false /* || monoloop_select_view.tempUnlock == true || event.ctrlKey*/ ){
              event.preventDefault();

              var selector = monoloop_select_view.getXPath(this);

              if( event.ctrlKey ){
                if( MONOloop.jq(this).hasClass('monoloopBorderFix')){
                  MONOloop.jq(this).removeClass('monoloopBorderFix') ; // un select
                  if(monoloop_select_view.selectedElementsOnPage.indexOf(selector) !== -1){
                    monoloop_select_view.selectedElementsOnPage.splice(monoloop_select_view.selectedElementsOnPage.indexOf(selector), 1);
                  }
                }else{
                  MONOloop.jq(this).addClass('monoloopBorderFix') ; // select
                  if(monoloop_select_view.selectedElementsOnPage.indexOf(selector) === -1){
                    monoloop_select_view.selectedElementsOnPage.push(selector);
                  }
                }
              }else{
                monoloop_select_view.clearSelection();
                MONOloop.jq(this).addClass('monoloopBorderFix') ; // single click without ctrl
                if(monoloop_select_view.selectedElementsOnPage.indexOf(selector) === -1){
                  monoloop_select_view.selectedElementsOnPage.push(selector);
                }
              }
              /*
              monoloop_select_view.clearSelection() ;
              MONOloop.jq(this).addClass('monoloopBorderFix') ;
              */
              monoloop_select_view.disPlayAllBorder();
              // monoloop_select_view.selectFocusParent();
              if(! event.ctrlKey ){

                var closestCKE = MONOloop.jq(this).closest(".cke");
                // alert(closestCKE.length);
                if(closestCKE.length === 0 ){
                  var selector = monoloop_select_view.getXPath(this);
                  //top.pwMain.showMenu1(event.pageX , event.pageY , page[0] , page[1] , selector  ) ;
                  if(this.tagName !== 'BODY' && this.tagName !== 'HTML'){
                    var tFABOp = {},
                        content = '',
                        curAction = monoloop_select_view.currentTransaction.action,
                        curActionData = monoloop_select_view.currentTransaction.data,
                        defaultToolbarWidth = 235;

                    if(curAction === 'edit'){
                      tFABOp.type = 'updateVariation';
                    }else {
                      tFABOp.type = 'addVariation';
                    }
                    monoloop_select_view.initElement(selector, tFABOp); // here

                    curAction = monoloop_select_view.currentTransaction.action;
                    curActionData = monoloop_select_view.currentTransaction.data;

                    var editor = CKEDITOR.instances[monoloop_select_view.editorID];
                    if(curAction === 'edit'){
                      content += '<button class="ml_custom_toolbar_btn updateVariation">Update variation</button> ';
                      defaultToolbarWidth += 125;
                    }
                    content += '<button class="ml_custom_toolbar_btn addVariation">Add variation</button> ';
                    var havVar = 0;
                    if(monoloop_select_view.selectorsHaveVariations.indexOf(selector) !== -1){
                      content += '<button class="ml_custom_toolbar_btn experimentBuilder">Add to experience</button> ';
                      defaultToolbarWidth += 135;
                      havVar = 1;
                    }

                    content += '<button class="ml_custom_toolbar_btn remove">Hide</button> ';
                    if(curAction === 'edit'){
                      content += '<button class="ml_custom_toolbar_btn conditionBuilder">Update condition</button> ';
                      defaultToolbarWidth += 130;
                    }

                    var _this = this;

                    //  content menu ifram start
                    var contentSelectorId = monoloop_select_view.getXPathId(selector), iframeUrl = '';
                    iframeUrl = MONOloop.domain + '/component/content-menu?fullURL=' + monoloop_select_view.originalUrl;
                    iframeUrl += ('&xpath=' + contentSelectorId);
                    iframeUrl += ('&hash_fn=' + monoloop_select_view.hashFn);

                    var selectorId = selector.replace(/#/g, monoloop_select_view.hashFn);
                    var element = monoloop_select_view.getSelectorByXPath(selector);
                    iframeUrl += ('&xpath1=' + selectorId);

                    if(monoloop_select_view.currentPageElement){
                      iframeUrl += ('&page_element_id=' + monoloop_select_view.currentPageElement._id);
                    }

                    if(monoloop_select_view.source !== undefined){
                      iframeUrl += ('&source=' + monoloop_select_view.source);
                    }
                    if(monoloop_select_view.currentExperiment !== undefined){
                      iframeUrl += ('&exp=' + monoloop_select_view.currentExperiment._id);
                    }
                    if(monoloop_select_view.currentFolder){
                      iframeUrl += ('&current_folder=' + monoloop_select_view.currentFolder);
                    }
                    if(monoloop_select_view.page){
                      iframeUrl += ('&page=' + monoloop_select_view.page);
                    }

                    iframeUrl += ('&tVar=' + monoloop_select_view.totalVariations);

                    if(monoloop_select_view.newExperimentBackUp){
                      iframeUrl += ('&bk_seg=' + monoloop_select_view.newExperimentBackUp.segmentExperiment);
                      iframeUrl += ('&bk_goal=' + monoloop_select_view.newExperimentBackUp.goalExperiment);
                    }
                    if(curAction === 'edit'){
                      if(curActionData.content_id){
                        iframeUrl += ('&content_id=' + curActionData.content_id);
                      }
                    }

                    iframeUrl += ('&havVar=' + havVar);
                    iframeUrl += ('&tagName=' + this.tagName);

                    // MONOloop.jq(this).webuiPopover('destroy');
                    monoloop_select_view.openVariationPopover(iframeUrl, this, 'content', {title: "Editor", t: 'menu'});
                    MONOloop.jq(this).webuiPopover('show');
                    //content menu iframe end



                    // MONOloop.jq(this).webuiPopover({
                    //  content:content,
                    //  width:defaultToolbarWidth,
                    //  height:24,
                    //  multi: false,
                    //  cache:false,
                    //  onHide: function(a){
                    //    var exID = MONOloop.jq(_this).attr('id');
                    //    if(!exID){
                    //      MONOloop.jq(_this).prop('id', monoloop_select_view.randomID());
                    //    }
                    //    var id = MONOloop.jq(_this).attr('id');
                    //
                    //    MONOloop.jq("#"+id).webuiPopover('destroy');
                    //
                    //    monoloop_select_view.bindElement('#' + MONOloop.jq(_this).attr('id'));
                    //  },
                    //  placement: 'bottom-left'
                    // });
                    // MONOloop.jq(this).webuiPopover('show');
                    // var url = MONOloop.domain + '/component/content-create?fullURL=' + monoloop_select_view.originalUrl + '&tVar=' + monoloop_select_view.totalVariations;
                    // monoloop_select_view.FAbEventFns.variation('add', {
                    //  url: url,
                    //  originalUrl: monoloop_select_view.originalUrl,
                    //  hashFn: monoloop_select_view.hashFn,
                    //  selector: selector,
                    //  currentPageElement: monoloop_select_view.currentPageElement,
                    //  currentFolder: monoloop_select_view.currentFolder,
                    //  source: monoloop_select_view.source,
                    //  page: monoloop_select_view.page,
                    //  experiment: monoloop_select_view.currentExperiment,
                    //  domAddr: '.ml_custom_toolbar_btn.addVariation',
                    //  title: 'Add variation',
                    //  placement: 'toolbar',
                    // });
                    // if(curAction === 'edit'){
                    //  var url = MONOloop.domain + '/component/content-update?fullURL=' + monoloop_select_view.originalUrl;
                    //  monoloop_select_view.FAbEventFns.variation('edit', {
                    //    url: url,
                    //    originalUrl: monoloop_select_view.originalUrl,
                    //    hashFn: monoloop_select_view.hashFn,
                    //    selector: selector,
                    //    currentPageElement: monoloop_select_view.currentPageElement,
                    //    source: monoloop_select_view.source,
                    //    currentFolder: monoloop_select_view.currentFolder,
                    //    page: monoloop_select_view.page,
                    //    content_id: curActionData.content_id,
                    //    domAddr: '.ml_custom_toolbar_btn.updateVariation',
                    //    title: 'Update variation',
                    //    placement: 'toolbar',
                    //  });
                    // }
                    // if(monoloop_select_view.selectorsHaveVariations.indexOf(selector) !== -1){
                    //  // for experiments
                    //  monoloop_select_view.FAbEventFns.experiment('add', {
                    //    url: MONOloop.domain + '/component/content-add-to-experiment?fullURL=' + monoloop_select_view.originalUrl,
                    //    originalUrl: monoloop_select_view.originalUrl,
                    //    hashFn: monoloop_select_view.hashFn,
                    //    selector: selector,
                    //    currentPageElement: monoloop_select_view.currentPageElement,
                    //    source: monoloop_select_view.source,
                    //    page: monoloop_select_view.page,
                    //    currentFolder: monoloop_select_view.currentFolder,
                    //    domAddr: '.ml_custom_toolbar_btn.experimentBuilder',
                    //    title: 'Experiments',
                    //    placement: 'toolbar',
                    //  });
                    // }
                    // if(curAction === 'edit'){
                    //  editor.title = (monoloop_select_view.currentTransaction.data ? monoloop_select_view.currentTransaction.data.condition : '');
                    //  // MONOloop.jq(editor.element.$).attr('title',(monoloop_select_view.currentTransaction.data ? monoloop_select_view.currentTransaction.data.condition : ''));
                    //  MONOloop.jq('.ml_custom_toolbar_btn.conditionBuilder').on('click', function(e){
                    //    editor.execCommand('insertCb', {
                    //      type: 'variation',
                    //      condition: (monoloop_select_view.currentTransaction.data ? monoloop_select_view.currentTransaction.data.condition : '')
                    //    });
                    //  });
                    // }
                    //
                    //
                    // MONOloop.jq('.ml_custom_toolbar_btn.remove').on('click', function(e){
                    //  editor.execCommand('insertRemove');
                    // });

                    // monoloop_select_view.bindElement() ;


                    // monoloop_select_view.createImageCounterBubble({
                    //  mappedTo: selector,
                    //  selectorId: monoloop_select_view.getXPathId(selector)
                    // });


                  }
                }
              }
              //monoloop_select_view.lock = true ;
            }
            return false ;
        }
        }
      }) ;

  },
  initElement: function(selector, fabOptions, placement){
    CKEDITOR.config.lastMappedTo = CKEDITOR.config.mappedTo || "";
    fabOptions = fabOptions || {};
    if(fabOptions.type !== 'updateVariation'){
      monoloop_select_view.currentTransaction.action = 'add';
      monoloop_select_view.currentTransaction.data = {};
    } else {

    }

    CKEDITOR.config.mappedTo = selector;
    var element = monoloop_select_view.getSelectorByXPath(selector);
    //console.log(element);
    monoloop_select_view.storeOriginalContent({
      xpath: selector,
      content: element.html()
    });

    CKEDITOR.config.initData = {};
    CKEDITOR.config.initData.xpath = selector;

    monoloop_select_view.postToParent(JSON.stringify({
      t: "set-current-select-element-on-page",
      data: {
        xpath: selector,
        content: element.html(),
      }
    }));

    // this condition need to be revised as it overlaps with edit-variation-content flow
    // mantis # 3307
    // if(placement !== 'toolbar'){
    monoloop_select_view.bindCkeditoraTo(selector, fabOptions);
    // }

  },
  randomID: function(prefix){
    prefix = prefix || "ml_custom_ckedit_id";
    var d = new Date().getTime();
    var uuid = 'xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return prefix + '_'+uuid;
  },
  bindCkeditoraTo: function(selector, fabOptions){
    if(!selector) return false;
    fabOptions = fabOptions || {};

    var view = monoloop_select_view.getSelectorByXPath(selector),
        id,
        tagName = view.prop('tagName').toLowerCase(),
        toolbar = [];

    switch (tagName) {
      case 'img':
        for(var name in CKEDITOR.instances){  // marc
          monoloop_select_view.destroyCKE(CKEDITOR.instances[name].name);
        }
        return;
        //monoloop_select_view.toolbarType = "image";
        break;
      default:
        monoloop_select_view.toolbarType = "default";
        break;
    }
    if(!view.attr('contenteditable')){
      view.attr('contenteditable', true);
    }
    if(!view.attr('id')){
      id = monoloop_select_view.randomID();
      view.attr('id', id);
    } else {
      id = view.attr('id');
    }
    CKEDITOR.dtd.$editable.span = 1;
    CKEDITOR.dtd.p.div = 1;
    CKEDITOR.dtd.$editable.p = 1;
    CKEDITOR.dtd.$editable.strong = 1;
    CKEDITOR.dtd.$editable.ul = 1;
    CKEDITOR.dtd.$editable.li = 1;
    CKEDITOR.dtd.$editable.a = 1;
    CKEDITOR.dtd.$editable.b = 1;
    CKEDITOR.dtd.$editable.img = 1;
    CKEDITOR.dtd.$editable.input = 1;
    CKEDITOR.on( 'dialogDefinition', function( ev ){
      // Take the dialog name and its definition from the event data.
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;
      if ( dialogName == 'link'  || dialogName == 'image') {

        dialogDefinition.removeContents( 'upload' );
        dialogDefinition.removeContents('Upload');
        // dialogDefinition.removeContents( 'target' );
      }
    });
    var editor_ = CKEDITOR.instances[id];

    // Funciton depending on editor selection,  will set the state of our command.
    function RefreshState() {
        var editable = editor_.editable(),
            // Command that we want to control.
            command = editor_.getCommand( 'insertCb' ),
            range,
            commandState;

        if ( !editable ) {
            // It might be a case that editable is not yet ready.
            return;
        }

        // We assume only one range.
        range = editable.getDocument().getSelection().getRanges()[ 0 ];

        // The state we're about to set for the command.
        commandState = ( range && !range.collapsed ) ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED;

        command.setState( commandState );
    }

    if (editor_ === undefined) {
      editor_ = CKEDITOR.inline(view.get(0), {
            mappedTo: selector,
            on: {
                instanceReady: function( event ) {
                    this.focus();
                    for(var name in CKEDITOR.instances){
                      if(CKEDITOR.instances[name].name != id){
                        monoloop_select_view.destroyCKE(name);
                      }
                    }
                    // Also do state refresh on instanceReady.
                    RefreshState();
                },
                change: function( event ) {
                  // monoloop_select_view.pushChangedElement(event.editor, selector, view, tagName);
                  monoloop_select_view.pushActionsOnXapth(selector);

                  monoloop_select_view.makeSureIfAnyChangeOccoured();

                },
                blur: function( event ) {
                  var ae = MONOloop.jq(document.activeElement).get(0).tagName.toLowerCase(),
                      id;
                  if (ae != "iframe" && ae != "body" && ae != "button") {
                      monoloop_select_view.saveChangedContent();
                      monoloop_select_view.restoreOriginalContents();

                      id = this.element.$.id;
                      // monoloop_select_view.destroyCKE(id);
                      return false;
                  } else {
                    return false;
                  }
                },
                doubleclick: function(event){
                  return false;
                },
                // this doesn't work everytime you select content in editor
                // selectionChange: function(event){
                //  console.log('selection change event fired at ', event.editor.getSelection().getSelectedText());
                // }
            }
        });
    }

    // We'll use throttled function calls, because this event can be fired very, very frequently.
    var throttledFunction = CKEDITOR.tools.eventsBuffer( 250, RefreshState );

    // Now this is the event that detects all the selection changes.
    editor_.on( 'selectionCheck', throttledFunction.input );

    fabOptions.selector = selector;
    fabOptions.tagName = tagName;
    fabOptions.editorID = id;
    monoloop_select_view.editorID = id;
    // monoloop_select_view.actionButtonTypes.type = "addVariation";
    if(CKEDITOR.config.mappedTo !== CKEDITOR.config.lastMappedTo){
      monoloop_select_view.currentTransaction.action = 'add';
      monoloop_select_view.currentTransaction.data = {};
      if(fabOptions.type !== undefined){
        monoloop_select_view.actionButtonTypes.type = fabOptions.type;
      } else {
        monoloop_select_view.actionButtonTypes.type = 'addVariation';
      }
      // monoloop_select_view.initActionButtons(fabOptions);
    } else {
      if(fabOptions.refresh){
        // monoloop_select_view.initActionButtons(fabOptions);
      }
    }

    //
  },
  destroyCKE: function(id, createNew){
    createNew = createNew || false;
    setTimeout(function() {
      var editor_ = CKEDITOR.instances[id];
      if(editor_) CKEDITOR.instances[id].destroy(true);
      MONOloop.jq('#'+id).removeAttr('contenteditable');
      monoloop_select_view.bindElement('#' + id);
      if(createNew){
        monoloop_select_view.initElement(CKEDITOR.config.mappedTo);
      }
   },0);
  },
  ejs2Html: function(ejsCode){
    var output = ejsCode || "";
    if(output.indexOf('<% if') !== -1) {
      output = output.replace(/<% if/g,'&nbsp;<div style="display:inline;color:green;" condition="if').replace(/{ %>/g, '{|}">').replace(/<%}%>/g, '</div>');
    }
    return output;
  },
  init: function(){
    monoloop_select_view.bindElement();
  },
  resizePopover: function(options){
    if(MONOloop.jq('.webui-popover').length > 0){
      var iframes = MONOloop.jq('.webui-popover'),
          iframe = iframes[iframes.length - 1];
          iframe.height(900);
          // console.log("iframeiframe", iframe.height(900));
    }
  },
  postToParent: function(json_obj, d){
    d = d || MONOloop.domain;
    parent.postMessage(json_obj, d);
  },
  makeSureIfAnyChangeOccoured: function(){
    if(!monoloop_select_view.isPWchangedPosted){
      monoloop_select_view.postToParent(JSON.stringify({t: 'customization-occoured-in--user-site', isPWchanged: true}));
      monoloop_select_view.isPWchangedPosted = true;
    }
  },
  isParentAlreadyInEditMode: function(elem){
    var _enabledParent = MONOloop.jq(elem).parents('.monoloopBorderFix:eq(0)');
    var _selfEditable = MONOloop.jq(elem).hasClass('monoloopBorderFix');
    var _isSpeechBubble = MONOloop.jq(elem).hasClass('speech-bubble');
    var _result = (_enabledParent.length || _selfEditable) ? true : false;
    return _result;
  }

}

var __listener = function (e) {
       var data = e.data;
       switch (data.t) {
        case "resize-popover":
          monoloop_select_view.resizePopover(data.options);
          break;
         case "hide-iframe":
                     monoloop_select_view.postToParent(JSON.stringify({t: "send-webix-message", message:data.message }));
                     MONOloop.jq('.webui-popover').has('iframe').remove();


           break;
        case "upload-image-required-message":
          monoloop_select_view.postToParent(JSON.stringify({t: data.t}));
          break;
        case "remove-or-decrease-variation-bubble-from-list":
          monoloop_select_view.postToParent(JSON.stringify({t: 'set-new-inline-variations', data: {}}));
          if(data.remove_popup === true){
            var selectorId = monoloop_select_view.getXPathId(data.xpath);
            MONOloop.jq("#"+selectorId).remove();
            MONOloop.jq('.webui-popover').has('iframe').remove();
          }
          if(data.variationCount == 0){
            var selectorId = monoloop_select_view.getXPathId(data.xpath);
            MONOloop.jq("#"+selectorId).remove();
          }
          break;
        case "hide-an-element":
          var editor = CKEDITOR.instances[monoloop_select_view.editorID];
          editor.execCommand('insertRemove');
          break;
        case "set-changed-content":
          monoloop_select_view.currentPageElement = data.pageElement;
          monoloop_select_view.originalUrl = data.originalUrl;
          monoloop_select_view.page = data.page;
          monoloop_select_view.source = data.source;
          if(monoloop_select_view.source === 'experiment') {
            monoloop_select_view.currentExperiment = data.experiment;
          }
          monoloop_select_view.currentFolder = data.currentFolder;


          if(data.variations !== undefined && data.variations.length > 0){
            MONOloop.jq('.variations-counter').remove();
            var groupsVariations = monoloop_select_view.variationsGroupByXpath(data.variations, function(item){
              return [item.mappedTo];
            });
            monoloop_select_view.bubblePositions = [];
            monoloop_select_view.showVariationBubbles(groupsVariations, data);
          }

          monoloop_select_view.totalVariations = data.variations.length ? data.variations.length : 0;
          if(!data.userStateData){
              if(data.updateFab !== false){
                if(['experiment'].indexOf(monoloop_select_view.page) === -1){
                  monoloop_select_view.initActionButtons({});
                }
              } else {
              }
          } else {
            monoloop_select_view.initActionButtons({});
          }

          if(data.userStateData){
            if(data.userStateData.data.editRecordData){
              var element = monoloop_select_view.getSelectorByXPath(data.userStateData.data.editRecordData.xpath);
              element.html(data.userStateData.data.editRecordData.content);
              monoloop_select_view.actionButtonTypes.type = 'addVariation';
              monoloop_select_view.initElement(data.userStateData.data.editRecordData.xpath);
            }
          }
          break;
          case "start-guideline-in-site":

            var r = data.data;
            var cg = undefined;
            if(r.currentGuide){
              cg = JSON.parse(r.currentGuide);
              guidelines.startedBefore = true;
            }

            if(guidelines){
              guidelines[data.pageId] = r.guidelines;
              guidelines.page.pageId = data.pageId;
            }

            if(cg){
              if(cg.currentGuide){
                guidelines.page.currentGuide = cg.currentGuide;
              } else {
                guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
              }
            } else {
              guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
            }

            // console.log("guidelines.page",guidelines.page.currentGuide);
            if(guidelines.page.currentGuide && !MONOloop.jq(guidelines.page.currentGuide.location_id).attr('data-target')){
              if(!cg){
                setTimeout(function(){
                  guidelines.generate(guidelines.page.currentGuide, {});
                  MONOloop.jq(document).keyup(function(e) {
                       if (e.keyCode == 27) { // escape key maps to keycode `27`
                         guidelines.saveInSession('yes','iframe');

                      }
                  });
                }, 1000);
              }

            } else {
            }
            break;
          case "set-current-pageElement":
            // refresh page element and related items
            monoloop_select_view.currentPageElement = data.data.pageElement;

            monoloop_select_view.totalVariations++;

            // if(data.data.source && data.data.source === 'experiment'){
            //  monoloop_select_view.currentExperiment = data.data.experiment;
            //  monoloop_select_view.FAbEventFns.experiment('add', {
            //    url: MONOloop.domain + '/component/content-add-to-experiment?fullURL=' + monoloop_select_view.originalUrl,
            //    originalUrl: monoloop_select_view.originalUrl,
            //    hashFn: monoloop_select_view.hashFn,
            //    selector: monoloop_select_view.actionButtonTypes.fabOptions.selector,
            //    currentPageElement: monoloop_select_view.currentPageElement,
            //    source: monoloop_select_view.source,
            //    currentFolder: monoloop_select_view.currentFolder,
            //    domAddr: '.subActionButton.experimentBuilder.inline',
            //    title: 'Experiments',
            //  });
            // }

            monoloop_select_view.makeSureIfAnyChangeOccoured();
            // console.log("monoloop_select_view.actionButtonTypes.fabOptions", monoloop_select_view.actionButtonTypes.fabOptions);
            // if(data.variationCreated === true){
            //  monoloop_select_view.actionButtonTypes.type = 'addVariation';
            // }
            monoloop_select_view.postToParent(JSON.stringify({t: data.t, data: {content: data.data.content, pageElement: data.data.pageElement, source: data.data.source, experiment: monoloop_select_view.currentExperiment}}));
            monoloop_select_view.postToParent(JSON.stringify({t: 'set-new-inline-variations', data: {content: data.data.content, pageElement: data.data.pageElement, source: data.data.source, experiment: monoloop_select_view.currentExperiment}}));


            // window.removeEventListener(messageEvent, __listener, false);
            break;
          case "get-element-content-variation":
            if(CKEDITOR.config.initData){

              var data1 = data.data,
                  element,
                  tagName,
                  t = "add-element-content-variation";
              data1.xpath = CKEDITOR.config.initData.xpath;
              element = monoloop_select_view.getSelectorByXPath(data1.xpath);
              tagName = element.prop("tagName").toLowerCase();

              switch (tagName) {
                case 'img':
                  var html = element[0].outerHTML;

                  if(data1.hide){
                    if(data1.hide === true){
                      html = "<!-- monoloop remove -->&nbsp;";

                      var editor = CKEDITOR.instances[monoloop_select_view.editorID];
                      editor.execCommand('insertRemove');

                      monoloop_select_view.createDeleteBubble({
                        mappedTo: data1.xpath,
                        selectorId: monoloop_select_view.getXPathId(data1.xpath)
                      });

                    }
                  } else {
                    if(element.hasClass('monoloop_remove')){
                      html = "<!-- monoloop remove -->&nbsp;";
                    }
                  }

                  data1.content = html;
                  data1.contentType = 'image';
                  data1.imageLink = element.attr('src');
                  break;
                default:
                  if(data1.hide){
                    if(data1.hide === true){
                      data1.content = "<!-- monoloop remove -->&nbsp;";

                      var editor = CKEDITOR.instances[monoloop_select_view.editorID];
                      editor.execCommand('insertRemove');

                      monoloop_select_view.createDeleteBubble({
                        mappedTo: data1.xpath,
                        selectorId: monoloop_select_view.getXPathId(data1.xpath)
                      });

                    }
                  } else {
                    data1.content = element.html().trim();
                  }
                  data1.contentType = '';
                  break;
              }
              if(data1.content_id){
                t = "update-element-content-variation";
              }

              data1.source = monoloop_select_view.source;
              if(MONOloop.jq('.webui-popover').length > 0){
                var iframes = MONOloop.jq('.webui-popover iframe');

                for (var i = 0; i < iframes.length; i++) {
                    iframes[i].contentWindow.postMessage(JSON.stringify({t: t, data: data1}), iframes[i].src);
                }

              }
            } else {
              monoloop_select_view.postToParent(JSON.stringify({t: "send-webix-message", message: 'No element is selected to add variation.'}));
            }
            break;
          case "image-is-uploaded":
          var editor = CKEDITOR.instances[monoloop_select_view.editorID];
          var xapth = CKEDITOR.config.mappedTo,
              selector = monoloop_select_view.getSelectorByXPath(xapth),
              rootSelector = selector,
              id = selector.attr('id'),
              rootId = id,
              tagName = selector.prop("tagName").toLowerCase(),
              rootTagName = tagName,
              src = "";
              switch (tagName) {
                case 'img':
                  src = selector.attr('src');
                  break;
                default:
                  var selector = MONOloop.jq(editor.getSelection().getStartElement().$);
                  if(selector){
                    tagName = selector.prop('tagName').toLowerCase();
                    if(tagName === 'img'){
                      id = selector.attr('id');
                      if(!id){
                        selector.attr('id', monoloop_select_view.randomID('inline_img'));
                        id = selector.attr('id');
                        src = selector.attr('src');
                      }
                    }
                  }
                  break;

              }
             // window.removeEventListener(messageEvent, listener, false);
             if(data.data.status === 'server'){
               var srcset = '' ;
               var url = data.data.url ;
               var file_name = url.substr(url.lastIndexOf('/')+1);
               var file_working_dir = url.substring(0,url.lastIndexOf('/'));
               srcset = encodeURI(file_working_dir + '/600/' + file_name) + ' 600w,' + encodeURI(file_working_dir + '/300/' + file_name) + ' 300w,' + encodeURI(file_working_dir + '/130/' + file_name) + ' 130w'
               if(tagName === 'img'){
                 selector.attr('src', encodeURI(data.data.url) + '?' + Math.random());
                 selector.attr('srcset',srcset);
                 selector.load(function(){
                   // monoloop_select_view.pushChangedElement(editor, xapth, rootSelector, rootTagName);
                   monoloop_select_view.pushActionsOnXapth(xapth);
                 });

               } else {
                //  editor.focus();
                //  editor.fire( 'saveSnapshot' );
                 editor.insertHtml('<img  src="' + encodeURI(data.data.url) + '" srcset="'+srcset+'" />');
                //  editor.fire( 'saveSnapshot' );
               }
             }

             break;
          case "set-element-content-by-xpath":
              var selector = data.xpath,
              element = monoloop_select_view.getSelectorByXPath(selector);
              data.content = monoloop_select_view.ejs2Html(data.content);
              element.html(data.content);
            break;
          case "get-element-content-by-xpath":
              var selector = data.xpath,
              element = monoloop_select_view.getSelectorByXPath(selector);

              if(MONOloop.jq('.webui-popover').length > 0){
                var iframes = MONOloop.jq('.webui-popover iframe');

                for (var i = iframes.length - 1; i < iframes.length; i++) {
                    iframes[i].contentWindow.postMessage(
                      JSON.stringify({
                          t: 'load-variation-list'
                          , data: {
                            content: element.html()
                            , xpath: selector
                          }
                        }
                      )
                      , iframes[i].src
                    );
                }

              }
            break;
          case "init-FAB-from-edit-variation":

            MONOloop.jq(".webui-popover").has('iframe').remove();
            var selector = data.xpath,
                element = monoloop_select_view.getSelectorByXPath(selector);

            data.content = monoloop_select_view.ejs2Html(data.content);

            //console.log("data.content",data.content);

            if(data.content === '<!-- monoloop remove -->&nbsp;'){
              data.content = element.html() + '<!-- monoloop remove -->&nbsp;';
              monoloop_select_view.createDeleteBubble({
                mappedTo: selector,
                selectorId: monoloop_select_view.getXPathId(selector)
              });
            }

            element.html(data.content);
            monoloop_select_view.actionButtonTypes.setType('updateVariation');

            monoloop_select_view.clearSelection() ;
            element.addClass('monoloopBorderFix');
            monoloop_select_view.disPlayAllBorder() ;

            monoloop_select_view.storeChangedContent({
              content_id: data.content_id,
              condition: data.condition,
              content: data.content,
              xpath: data.xpath,
            });

            monoloop_select_view.currentTransaction.action = 'edit';
            monoloop_select_view.currentTransaction.data = data;
            monoloop_select_view.initElement(selector, {
              content_id: data.content_id,
              content_name: data.name,
              content_condition: data.condition,
              content_content: data.content,
              xpath: data.xpath,
              refresh: true,
              type: "updateVariation",
            }, 'toolbar');



            element.click();
            element.focus();
            monoloop_select_view.makeSureIfAnyChangeOccoured();


            break;
          case "condition-selected":
            var condition = data.returnCondition.logical;
            if(data.type !== undefined){
              if(data.type === "content-preview"){
                  if(MONOloop.jq('.webui-popover').length > 0){
                    var iframes = MONOloop.jq('.webui-popover iframe');

                    for (var i = 0; i < iframes.length; i++) {
                      iframes[i].contentWindow.postMessage(JSON.stringify({t: 'set-updated-condition', data: {condition: condition, type: data.type}}), iframes[i].src);
                    }
                  }
                }else{
                  if(MONOloop.jq('.webui-popover').length > 0){
                    var iframes = MONOloop.jq('.webui-popover iframe');

                    for (var i = 0; i < iframes.length; i++) {
                      iframes[i].contentWindow.postMessage(JSON.stringify({t: 'set-condition', data: {condition: condition, type: data.type}}), iframes[i].src);
                    }
                  }
                }
              if(data.type === 'variation'){
                if(monoloop_select_view.currentTransaction.action === 'edit'){
                  monoloop_select_view.postToParent(JSON.stringify({
                        t: "save-changed-content-while-editing" ,
                          data : {
                            content_id: monoloop_select_view.currentTransaction.data.content_id,
                            content: monoloop_select_view.currentTransaction.data.content,
                            condition: condition,
                          }
                  }), "*");
                  monoloop_select_view.currentTransaction.data.condition = condition;
                }
              }

              monoloop_select_view.makeSureIfAnyChangeOccoured();
            }
            break;
          case "page-level-segment-created":
            if(data.created){
              MONOloop.jq('.webui-popover').has('iframe').remove();
              if(monoloop_select_view.newExperimentBackUp){
                monoloop_select_view.newExperimentBackUp.segmentExperiment = data.segment_id;
                MONOloop.jq('.subActionButton.experimentBuilder.inline').click();
              }
              monoloop_select_view.makeSureIfAnyChangeOccoured();
            }
            break;
          case "page-level-goal-created":
            if(data.created){
              MONOloop.jq('.webui-popover').has('iframe').remove();
              if(monoloop_select_view.newExperimentBackUp){
                monoloop_select_view.newExperimentBackUp.goalExperiment = data.goal_id;
                MONOloop.jq('.subActionButton.experimentBuilder.inline').click();

              }
              monoloop_select_view.makeSureIfAnyChangeOccoured();
            }
            break;
          case "variation-added-to-experiments":
            if(data.created){
              monoloop_select_view.makeSureIfAnyChangeOccoured();
            }
            break;
          case "open-new-segment-from-inlin-exp":
            monoloop_select_view.newExperimentBackUp = data.data;
            // MONOloop.jq('.subActionButton.segmentBuilder.inline.var').show();
            // MONOloop.jq('.subActionButton.segmentBuilder.inline').click();
            MONOloop.jq('.ml_custom_toolbar_btn.segmentBuilder').click();
            break;
          case "open-new-goal-from-inlin-exp":
            monoloop_select_view.newExperimentBackUp = data.data;
            // MONOloop.jq('.subActionButton.goalBuilder.inline.var').show();
            // MONOloop.jq('.subActionButton.goalBuilder.inline').click();
            MONOloop.jq('.ml_custom_toolbar_btn.goalBuilder').click();
            break;
          case "get-new-experiment-backup":
            var newExp = false;
            if(monoloop_select_view.newExperimentBackUp){
              newExp = true;
            }
            if(MONOloop.jq('.webui-popover').length > 0){
              var iframes = MONOloop.jq('.webui-popover iframe');

              for (var i = 0; i < iframes.length; i++) {
                  iframes[i].contentWindow.postMessage(JSON.stringify({t: 'set-experiment-values-from-backup', newExp: newExp, data: monoloop_select_view.newExperimentBackUp}), "*");
              }

            }
            monoloop_select_view.newExperimentBackUp = undefined;
            break;
          case "open-condition-builder":
            monoloop_select_view.postToParent(JSON.stringify({
              t: 'open-condition-builder',
              condition: data.condition,
              type: data.type,
            }));
            break;
         case "remove-wordpress-admin-bar":
           if(MONOloop.jq('#wpadminbar').length)
             MONOloop.jq('#wpadminbar').remove();
           break;
         default:
         break;
       }
       // Do whatever you want to do with the data got from IFrame in Parent form.
};

// listen from the child
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];

var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
// Listen to message from child IFrame window
eventer(messageEvent, __listener, false);
MONOloop.jq(document).ready(function() {
  monoloop_select_view.init();

}) ;
