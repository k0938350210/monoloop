var monoloop_select_view = {
    mode : 1 ,

    searchTextbox_xpath : '' ,
    searchButton_xpath : '' ,

    initPosMessage : function(){
    	var me = this ;
    	var listener = function(event) {
			var data;
			try{
				data = JSON.parse(event.data);
			}catch (e) {
				return ;
			}
			switch (data.t) {
				case 'tracker-search-setDefaultData':
				    me.setDefaultData(data.dataObj) ;
				break ;
				case 'tracker-search-setMode' :
					me.setMode(data.myStep) ;
				break ;
				case 'tracker-search-checkNext' :
					var msg = me.checkNext() ;
				    parent.postMessage(JSON.stringify({
		              t: "tracker-search-checkNext_callBack" ,
					  msg : msg
		            }), '*');
				break ;
				case 'tracker-search-getDataObj' :
					var dObj = me.getDataObj() ;
					parent.postMessage(JSON.stringify({
		              t: "tracker-search-getDataObj_callBack" ,
					  dObj : dObj
		            }), '*');
				break ;
			}
		};

		if (window.addEventListener) {
	        return addEventListener("message", listener, false);
		} else {
	        attachEvent("onmessage", listener);
		}
    } ,

    showDefaultXpath : function(){
        var me = this ;
        MONOloop.jq('#ml-cart-table-select .l1').hide() ;
        MONOloop.jq('#ml-cart-table-select .l2').hide() ;

        if(  me.searchTextbox_xpath != '')
            me.showText('1' , me.searchTextbox_xpath) ;

        if(  me.searchButton_xpath != '')
            me.showText('2' , me.searchButton_xpath) ;
    } ,

    showText : function(textID , selector){
        var me = this ;

        if( MONOloop.jq(selector).length == 0)
            return ;

        var top = MONOloop.jq(selector).offset().top ;
        var left = MONOloop.jq(selector).offset().left ;
        MONOloop.jq('#ml-cart-table-select .l'+ textID).css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l'+ textID).css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l'+ textID).show() ;
    } ,

    setDefaultData : function(dataObj){
        var me = this ;
        if( dataObj == undefined){
            return ;
        }
        if( me.searchTextbox_xpath == ''){
            me.searchTextbox_xpath = dataObj.searchTextbox_xpath ;
        }
        if( me.searchButton_xpath == ''){
            me.searchButton_xpath = dataObj.searchButton_xpath ;
        }
    } ,

    setMode : function(m){
        m = parseInt(m) ;
        var me = this ;
        me.mode = m ;
        //console.debug(me.mode ) ;
        me.removeYellowBorder() ;
        //console.debug(me.mode ) ;
        if( me.mode == 2){
            me.genSearchBox() ;
            me.showDefaultXpath() ;
        }else if( me.mode == 3){
            me.genSearchButton() ;
        }
    } ,

    checkNext : function(){
       var me = this ;
        if( me.mode == 2){
            if( MONOloop.jq(me.searchTextbox_xpath).length == 0){
                return 'Please select search box' ;
            }
        }
        if( me.mode == 3){
            if( MONOloop.jq(me.searchButton_xpath).length == 0){
                return 'Please select search button' ;
            }
        }
        return '' ;
    } ,

    genSearchBox : function(){
        //console.debug('123') ;
        var me  = this ;
        var tables = MONOloop.jq('input:text') ;
        for( var i = 0 ; i < tables.length ; i++ ){
            var selector = me.getXpathByEL(tables[i] , true) ;
            MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.searchboxClick(event,\''+selector+'\');"' , '')) ;
            me.disPlayBorderByElement(tables[i] , '-'+i) ;
            //console.debug(tables[i]) ;
        }
    } ,

    genSearchButton : function(){
        var me  = this ;
        var tables = MONOloop.jq('input:button , input:submit , a , input:image , button') ;
        for( var i = 0 ; i < tables.length ; i++ ){
            var selector = me.getXpathByEL(tables[i] , true) ;
            MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.searchbuttonClick(event,\''+selector+'\');"' , '')) ;
            me.disPlayBorderByElement(tables[i] , '-'+i) ;
        }
    } ,

    getDataObj : function(){
        var data = {} ;
        var me = this ;
        data['searchTextbox_xpath'] = me.searchTextbox_xpath ;
        data['searchButton_xpath'] = me.searchButton_xpath ;
        return data ;
    } ,

    checkBubble : function(e){
        var agent = MONOloop.jq.browser;
    	if(agent.msie) {
    		window.event.cancelBubble = true;
    	} else {
    		e.stopPropagation();
    	}
    } ,

    searchboxClick : function( e , selector){
        var me = this ;
        me.checkBubble(e) ;
        // Add select tex t ;
        var top = MONOloop.jq(selector).offset().top ;
        var left = MONOloop.jq(selector).offset().left ;


        if(me.searchTextbox_xpath == selector){
            me.searchTextbox_xpath = '' ;
            MONOloop.jq('#ml-cart-table-select .l1').hide() ;
            return ;
        }

        MONOloop.jq('#ml-cart-table-select .l1').css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l1').css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l1').show() ;

        me.searchTextbox_xpath = selector ;
    } ,

    searchbuttonClick : function( e , selector ){
        var me = this ;
        me.checkBubble(e) ;
        // Add select tex t ;
        var top = MONOloop.jq(selector).offset().top ;
        var left = MONOloop.jq(selector).offset().left ;


        if(me.searchButton_xpath == selector){
            me.searchButton_xpath = '' ;
            MONOloop.jq('#ml-cart-table-select .l2').hide() ;
            return ;
        }

        MONOloop.jq('#ml-cart-table-select .l2').css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l2').css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l2').show() ;

        me.searchButton_xpath = selector ;
    } ,

    disPlayBorderByElement : function(el , subfix){
        var top = MONOloop.jq(el).offset().top ;
        var left = MONOloop.jq(el).offset().left ;
        var hight = MONOloop.jq(el).outerHeight();
        var width =  MONOloop.jq(el).outerWidth();

        //MONOloop.jq('#ml-border-l'+subfix).parent().css('width' , width) ;
        //MONOloop.jq('#ml-border-l'+subfix).parent().css('height' , hight) ;

        MONOloop.jq('#ml-border-l'+subfix).css('top' , top -3) ;
        MONOloop.jq('#ml-border-l'+subfix).css('left' ,  left - 3) ;
        MONOloop.jq('#ml-border-l'+subfix).css('width' , width ) ;
        MONOloop.jq('#ml-border-l'+subfix).css('height' , hight ) ;

    } ,

    getNewBorder : function(idSubFix , classSubFix , onClickString , onMouseOutString){
        return '<div class="ml-border-group" '+onClickString+' '+onMouseOutString+'><div id="ml-border-l'+idSubFix+'" class="ml-border'+classSubFix+'"></div></div>' ;
    } ,

    removeYellowBorder : function(){
        MONOloop.jq('#ml-cart-table-list').html('') ;
    } ,

    getXpathByEL : function(el , isIncludeEQ){
        var selector = MONOloop.jq(el).parents()
                .map(function(){
                    var id = MONOloop.jq(this).attr("id");
                    var returnPart = this.tagName ;
                    if (id){
                    	id = id.replace(/\./g, '\\.');
                        returnPart += "#"+ id;
                    }else{
                        var classNames = MONOloop.jq(this).attr("class");
                        if (classNames) {
                        //returnPart += "." + MONOloop.jq.trim(classNames).replace(/\s/gi, ".");
                        }
                    }
                    return  returnPart ;
                })
                .get().reverse().join(" ");
        var selectorTest = selector ;
        if (selector) {
          selector += " "+ MONOloop.jq(el)[0].nodeName;
        }

        var id = MONOloop.jq(this).attr("id");
        if (id) {
          id = id.replace(/\./g, '\\.');
          selector += "#"+ id;
        }

        var classNames = MONOloop.jq(this).attr("class");
        if (classNames) {
          selector += "." + MONOloop.jq.trim(classNames).replace(/\s/gi, ".");
          //.monoloopBorder.monoloopBorderFix
          selector = selector.replace(/.monoloopBorderFix/ , '') ;
          selector = selector.replace(/.monoloopBorder/ , '') ;
        }

        if( isIncludeEQ ){
            var curElement = MONOloop.jq(selector);
            if( curElement.length > 1){
                 for( var i = 0 ; i < curElement.length ; i++){
                    if( el == curElement[i])
                        selector += ':eq('+i+')' ;
                 }
            }
        }

        return selector ;
    }
} ;

MONOloop.jq(document).ready(function() {

    MONOloop.jq('a').live("click", function(event){
         if( monoloop_select_view.mode == 1 ){
    	    // Check if
    	    if (window['monoloopClientURL2'] != undefined) {
    	    	return previewView.processATag(this) ;
    	    }else{
         		return true ;
			}
         }else{
            return false ;
         }
    });

    MONOloop.jq('a').click(function(){
        if( monoloop_select_view.mode == 1 ){
    	    // Check if
        	if (window['monoloopClientURL2'] != undefined){
        		return previewView.processATag(this) ;
    	    }else{
         		return true ;
       		}
         }else{
            return false ;
         }
    }) ;

    MONOloop.jq('*').click(function(event) {
        if( monoloop_select_view.mode == 1){
            return true ;
        }

        return false ;
    }) ;
}) ;