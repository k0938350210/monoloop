


//MONOloopReady(function() {

MONOloop.callURL("//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js", function (){

	MONOloop.$ = jQuery
	describe('Post.js with jQuery overwrite', function() {

	 	it("Should monoloop ready", function() {
			expect(true).toEqual(true);
		}) ;

		describe("Basic jquery function test", function() {
			it("Should MONOloop.before", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.before(MONOloop.$('#test p'), 'T');
				expect(document.getElementById('test').innerHTML).toEqual('T<p>T</p>');
			}) ;

			it("Should MONOloop.after", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.after(MONOloop.$('#test p'), 'T');
				expect(document.getElementById('test').innerHTML).toEqual('<p>T</p>T');
			}) ;

			it("Should MONOloop.append", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.append(MONOloop.$('#test p'), 'X');
				expect(document.getElementById('test').innerHTML).toEqual('<p>TX</p>');
			}) ;

			it("Should MONOloop.prepend", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.prepend(MONOloop.$('#test p'), 'X');
				expect(document.getElementById('test').innerHTML).toEqual('<p>XT</p>');
			}) ;

			it("Should MONOloop.replaceWith", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.replaceWith(MONOloop.$('#test p'), 'replace');
				expect(document.getElementById('test').innerHTML).toEqual('replace');
			}) ;

			it("Should MONOloop.children", function() {
				document.getElementById('test').innerHTML = '<p>T</p><p>T2</p>' ;
				var l = MONOloop.children (MONOloop.$('#test')).length ;
				expect(l).toEqual(2);
			}) ;

			it("Should MONOloop.parent", function() {
				document.getElementById('test').innerHTML = '<div id="child">T</div>' ;
				var p = MONOloop.parent((MONOloop.$('#child'))) ;
				expect(p[0].getAttribute('id')).toEqual('test');
			}) ;

			describe("Basic Monoloop.val input[type='text']", function() {
			 	it("Should val from textbox", function() {
			 		document.getElementById('test').innerHTML = '<input id="test1" type="text" value="text" />' ;
			 		var val = MONOloop.val(MONOloop.$('#test1'));
			 		expect(val).toEqual('text');

			 		MONOloop.val(MONOloop.$('#test1'),'text1');
			 		var val = MONOloop.val(MONOloop.$('#test1'));
			 		expect(val).toEqual('text1');
		 		}) ;
		 	}) ;

		 	it("Should MONOloop.html", function(){
		 		document.getElementById('test').innerHTML = '<p>T</p>' ;
		 		var val = MONOloop.html(MONOloop.$('#test')) ;
		 		expect(val).toEqual('<p>T</p>');
	 		}) ;

	 		it("Should MONOloop.attr", function(){
	 			var val = MONOloop.attr(MONOloop.$('#test'), 'id');
	 			expect(val).toEqual('test');
 			}) ;

 			it("Should MONOloop.is", function(){
 				expect(MONOloop.is(MONOloop.$('#test'),'div')).toEqual(true);
			}) ;

			it("Should MONOloop.next", function(){
				document.getElementById('test').innerHTML = '<p id="a">12</p><p id="b">12</p>' ;
				var n = MONOloop.next( MONOloop.$('#a') ) ;
				expect(n[0].getAttribute('id')).toEqual('b') ;
			}) ;
		}) ;

	});

	MONOloop.$.getJSON("http://app.monoloop.com/tracking/profile_jsonp.php?callback=?", 
		{
            mid :   '53157cc526259d53250f240e' ,
            skey:   '81ee28edd61985b22d7cb31fcd481bcf1427dd2c008bb145468b9faf8432b9b5a31fc53f13fe085f527f98854ba7b7091f68418a273fd7061ca7c5e2d464a22c'
        },
        function(data) {
        	it("Ajax result test - privacy getprofile should return object " , function(){
    			expect(typeof data).toEqual('object');
			}) ;
    	}
	) ;

}) ;

//}) ;
