CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:

	config.toolbarGroups = monoloop_select_view.toolbarGroups(monoloop_select_view.toolbarType);

	config.allowedContent = true;

	config.codeSnippet_height = '220';

	config.extraAllowedContent = 'img(left,right)[!src,alt,width,height];';

	config.autoParagraph = false;

	config.title = false;

	config.codeSnippet_languages = {
     javascript: 'JavaScript'
	};

	config.removeButtons = 'Find,SelectAll,Form,Radio,TextField,Textarea,Button,Select,ImageButton,HiddenField,Checkbox,RemoveFormat,BidiLtr,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Replace,Save,NewPage,Preview,Print,PasteFromWord,PasteText,Templates,BidiRtl,spellchecker,Table,Image';

  // config.extraPlugins = 'popup,notification,lineutils,notificationaggregator,cb,asset,remove,uploader';
	// config.extraPlugins = 'popup,notification,lineutils,notificationaggregator,variation,cb,asset,remove,uploader';
	config.extraPlugins = 'cb,uploader,codesnippet';
	config.filebrowserUploadUrl = MONOloop.domain + 'api/upload-image';
	config.floatSpaceDockedOffsetY = 10;
	config.htmlEncodeOutput = false;
	config.entities = false;
	//config.skin = 'moono-dark';
	config.skin = 'moono-lisa';
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};


CKEDITOR.disableAutoInline = true;
