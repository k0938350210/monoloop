(function(jq182) {
	var jq182current_target;
	var jq182dropdown;

	// form elements
	var jq182datepicker;

	var jq182parameters;

	var jq182daterangePreset;
	var jq182parameter1;
	var jq182aggregation;
	var jq182aggregationWrap;

	var jq182enableComparison;
	var jq182comparisonPreset;
	var dates;
	var default_options = {

		aggregations : ['-', 'daily', 'weekly', 'monthly', 'yearly'],
        values : {}

	};

    var default_aggregation = 'daily';

	var db = {

		aggregations : {
			'-' : {
				title : "Inherit",
				presets : []
			},
			'hourly' : {
				title : "Hourly",
				presets : ['custom', 'lastdays']
			},
			'daily' : {
				title : "Daily",
				presets : ['custom', 'lastdays', 'yesterday', 'today']
			},
			'weekly' : {
				title : "Weekly",
				presets : ['custom', 'lastweeks']
			},
			'monthly' : {
				title : "Monthly",
				presets : ['custom', 'lastmonths']
			},
			'quarterly' : {
				title : "Quarterly",
				presets : ['custom', 'lastquarters']
			},
			'yearly' : {
				title : "Yearly",
				presets : ['custom', 'lastyears']
			},
			'whole' : {
				title : "Whole period",
				presets : ['custom', 'lastdays', 'lastweeks', 'lastmonths', 'lastquarters', 'lastyears']
			}
		},

		date_presets : {
			'custom' : {
				title: "Custom",
				dates: function() {return null;}
			},
			'today' : {
				title: "Today",
				dates: function() {
					var dates = [];
					dates[0] = ((new Date()).setHours(0,0,0,0)).valueOf();
					dates[1] = new Date(dates[0]).setHours(23,59,59,0).valueOf();
					return dates;
				}
			},
			'yesterday' : {
				title: "Yesterday",
				dates: function() {
					var dates = [];
					dates[0] = ((new Date()).setHours(0,0,0,0)).valueOf() - 24*3600*1000;
					dates[1] = new Date(dates[0]).setHours(23,59,59,0).valueOf();
					//console.log(dates);
					return dates;
				}
			},
			'lastdays' : {
				title: "Last Day(s)",
				parameters: true,
				defaults : {
					parameter1 : 7
				},
				dates: function() {
					var days = internal.getParameter1();
					var dates = [];

					var today = new Date();
					dates[0] = new Date(today).setDate(today.getDate() - days).valueOf();
					dates[1] = new Date(today);
					dates[1].setDate(today.getDate() - 1);
					dates[1].setHours(23,59,59,0).valueOf();

					return dates;
				}
			},
			'lastweeks' : {
				title: "Last Week(s)",
				parameters: true,
				defaults : {
					parameter1 : 2
				},
				dates: function() {
					var dates = [];
					var weeks = internal.getParameter1();

					var monday = internal.getMonday(new Date());
					monday.setDate(monday.getDate() - (7 * weeks));
					dates[0] = monday.valueOf();
					var sunday = new Date(monday);
					sunday.setDate(sunday.getDate()+6 + (7 * (weeks - 1)));
					sunday.setHours(23,59,59,0);
					dates[1] = sunday.valueOf();

					return dates;
				}
			},
			'lastmonths' : {
				title: "Last Month(s)",
				parameters: true,
				defaults : {
					parameter1 : 3
				},
				dates: function() {
					var months = internal.getParameter1();
					var dates = [];

					var lastOfMonth = new Date().setDate(0);
					var firstOfMonth = new Date(lastOfMonth);
					firstOfMonth.setDate(1);
					firstOfMonth.setMonth(firstOfMonth.getMonth() - months + 1);
					dates[0] = firstOfMonth.valueOf();
					dates[1] = lastOfMonth.valueOf();

					return dates;
				}
			},
			'lastquarters' : {
				title: "Last Quarters(s)",
				parameters: true,
				defaults : {
					parameter1 : 2
				},
				dates: function() {
					// TODO: fix -- works as months now
					var months = internal.getParameter1() * 3;
					var dates = [];

					var lastOfMonth = new Date().setDate(0);
					var firstOfMonth = new Date(lastOfMonth);
					firstOfMonth.setDate(1);
					firstOfMonth.setMonth(firstOfMonth.getMonth() - months + 1);
					dates[0] = firstOfMonth.valueOf();
					dates[1] = lastOfMonth.valueOf();

					return dates;
				}
			},
			'lastyears' : {
				title: "Last Year(s)",
				parameters: true,
				defaults : {
					parameter1 : 1
				},
				dates: function() {
					var years = internal.getParameter1();
					var dates = [];

					var lastOfYear = new Date();
					lastOfYear.setDate(0);
					lastOfYear.setMonth(-1);

					var firstOfYear = new Date(lastOfYear);
					firstOfYear.setDate(1);
					firstOfYear.setMonth(-12*(years - 1));
					dates[0] = firstOfYear.valueOf();
					dates[1] = lastOfYear.valueOf();

					return dates;
				}
			}
		},
		/* data structure to hold options passed from external call to DateRangesWidget object*/
		options: null
	};

	var methods = {

		init : function(options) {
			db.options = options;
			return this.each(function() {
				var jq182this = jq182(this);
				var data = jq182this.data('DateRangesWidget');
                jq182this.data('test', internal);

				// initialize data in dom element
				if (!data) {

					var effective_options = jq182.extend({}, default_options, options);

					jq182this.data('DateRangesWidget', {
						options : effective_options
					});

				}
				internal.createElements(jq182this);
				internal.updateDateField(jq182this);


			});
		}

		/*
		remove : function() {
			this.text('');
		},

		destroy : function() {
			return this.each(function() {
				var jq182this = jq182(this),
				data = jq182this.data('DateRangesWidget');

				// Namespacing FTW
				jq182(window).unbind('.DateRangesWidget');
				data.target.remove();
				jq182this.removeData('DateRangesWidget');
			})
		}
		*/
	};

	var internal = {

		refreshForm : function() {
			var lastSel = jq182datepicker.DatePickerGetLastSel();

			if (jq182('.comparison-preset', jq182dropdown).val() != 'custom') {
				lastSel = lastSel % 2;
				jq182datepicker.DatePickerSetLastSel(lastSel);
			}
			jq182('.dr', jq182dropdown).removeClass('active');
			jq182('.dr[lastSel=' + lastSel + ']', jq182dropdown).addClass('active');

			var dates = jq182datepicker.DatePickerGetDate()[0];
			////console.log('dates', dates);
			var newFrom = dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+
			dates[0].getFullYear();
			var newTo = dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+
			dates[1].getFullYear()

			var oldFrom = jq182('.dr1.from', jq182dropdown).val();
			var oldTo = jq182('.dr1.to', jq182dropdown).val();

			if (newFrom != oldFrom || newTo != oldTo) {
				jq182('.dr1.from', jq182dropdown).val(newFrom);
				jq182('.dr1.to', jq182dropdown).val(newTo);

			}

            jq182('.dr1.from_millis', jq182dropdown).val(dates[0].getTime());
            jq182('.dr1.to_millis', jq182dropdown).val(dates[1].getTime());

			if (dates[2]) {
				jq182('.dr2.from', jq182dropdown).val(dates[2].getDate() + '/' + (dates[2].getMonth()+1) + '/' + dates[2].getFullYear());
			}
			if (dates[3]) {
				jq182('.dr2.to', jq182dropdown).val(dates[3].getDate() + '/' + (dates[3].getMonth()+1) + '/' + dates[3].getFullYear());
			}
		},

		createElements : function(jq182target) {
			// modify div to act like a dropdown
			jq182target.html(
				'<div id="goal-date-range-field">'+
					'<span class="main"></span>'+
					//'<span class="comparison-divider"> Cmp to: </span>'+
					'<span class="comparison"></span>'+

				'</div>'
			);

			// only one dropdown exists even though multiple widgets may be on the page
			if (!jq182dropdown) {
				jq182dropdown = jq182(
				'<div id="datepicker-dropdown">'+
					'<div class="date-ranges-picker"></div>'+
					'<div class="date-ranges-form">'+

						'<div class="main-daterange">'+
							'<div>'+
								'Date Range:'+
								'<select class="daterange-preset">'+
								'</select>'+
								'<span class="parameters">'+
									'<input type="number" class="daterange-preset-parameter1" />'+
								'</span>'+
							'</div>'+
							'<div class="input-fields"><input type="text" class="dr dr1 from" lastSel="0" /> - <input type="text" class="dr dr1 to" lastSel="1" />'+
                            '<input type="hidden" class="dr dr1 from_millis" lastSel="2" /><input type="hidden" class="dr dr1 to_millis" lastSel="3" /></div>'+
						'</div>'+

							'<input type="checkbox" class="enable-comparison" hidden />'+
					 				'<div class="btn-group">'+
						'<button class="btn btn-mini" id="button-ok">Apply</button>&nbsp;&nbsp;&nbsp;'+
						'<button class="btn btn-mini" id="button-cancel">Cancel</button>'+
						'</div>'+
					'</div>'+
				'</div>');
				jq182dropdown.appendTo(jq182('body'));

				jq182aggregation = jq182('.aggregation', jq182dropdown);
				jq182aggregationWrap = jq182('.aggregation-wrap', jq182dropdown);

				jq182datepicker = jq182('.date-ranges-picker', jq182dropdown);

				jq182daterangePreset = jq182('.daterange-preset', jq182dropdown);
				jq182parameters = jq182('.parameters', jq182dropdown);
				jq182parameter1 = jq182('.daterange-preset-parameter1', jq182dropdown);

				jq182enableComparison = jq182('.enable-comparison', jq182dropdown);
				jq182comparisonPreset = jq182('.comparison-preset', jq182dropdown);


				var to = new Date();
				var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 28);
				// TODO: inherit options from DRW options
				jq182datepicker.DatePicker({
					mode: 'tworanges',
					starts: 1,
					calendars: 3,
					inline: true,
					date: db.options.date,
					selectableDates: db.options.selectableDates,
					onChange: function(dates, el, options) {

						if(dates){
							setDates(dates);
						}
						internal.setDaterangePreset('custom');
            ////console.log("onchange datepicker");
					}
				});

				/**
				 * Handle change of aggregation.
				 */
				jq182aggregation.change(function() {
					internal.populateDateRangePresets();
				});


				/**
				 * Handle change of datePreset
				 */
				jq182daterangePreset.change(function() {
					var date_preset = internal.getDaterangePreset();
					if (date_preset.parameters) {
						//////console.log(internal.getParameter1());
						if (!jq182.isNumeric(internal.getParameter1())) {
							internal.setParameter1(date_preset.defaults.parameter1);
						}
						jq182parameters.show();
					} else {
						jq182parameters.hide();
					}
					jq182('.dr1', jq182dropdown).prop('disabled', (jq182daterangePreset.val() == 'custom' ? false : true));

					internal.recalculateDaterange();
				});

				jq182parameter1.change(function() {
					var p1 = internal.getParameter1();
					//////console.log(p1);
					if (!jq182.isNumeric(p1) || p1 < 1)
						internal.setParameter1(1);
					internal.recalculateDaterange();
				});

				/**
				 * Handle enable/disable comparison.
				 */
				jq182enableComparison.change(function() {
					internal.setComparisonEnabled(jq182(this).is(':checked'));
				});

				/**
				 * Handle change of comparison preset.
				 */
				jq182comparisonPreset.change(function() {
					internal.recalculateComparison();
				});

				/**
				 * Handle clicking on date field.
				 */
				jq182('.dr', jq182dropdown).click(function() {
					// set active date field for datepicker
					jq182datepicker.DatePickerSetLastSel(jq182(this).attr('lastSel'));
					//internal.refreshForm(); // don't refresh
				});

				/**
				 * Handle clicking on OK button.
				 */
				jq182('#button-ok', jq182dropdown).click(function() {
					internal.retractDropdown(jq182current_target);
					internal.saveValues(jq182current_target);
					internal.updateDateField(jq182current_target);
					return false;
				});

				/**
				 * Handle clicking on OK button.
				 */
				jq182('#button-cancel', jq182dropdown).click(function() {
					//////console.log('cancel')
					var jq182this = jq182(this);
					internal.retractDropdown(jq182current_target);
					return false;
				});

			}

			/**
			 * Handle expand/retract of dropdown.
			 */
			jq182target.bind('click', function() {
				var jq182this = jq182(this);
				//////console.log(jq182this);
				//////console.log('clicked on ', jq182this);
				if (jq182this.hasClass('DRWClosed')) {
					internal.expandDropdown(jq182this);
				} else {
					internal.retractDropdown(jq182this);
				}
				return false;
			});

			jq182target.addClass('DRWInitialized');
			jq182target.addClass('DRWClosed');
		},

		recalculateDaterange : function() {
			var date_preset = internal.getDaterangePreset();

			var dates = jq182datepicker.DatePickerGetDate()[0];
			//////console.log('original dates', dates);

			// TODO: remove

			if (date_preset.dates == undefined) throw date_preset.title + " doesn't have dates()";

			var d = date_preset.dates();

			if (d != null) {
				dates[0] = d[0];
				dates[1] = d[1];
			}
			//////console.log('new dates', dates);
			jq182datepicker.DatePickerSetDate(dates);
			internal.recalculateComparison();
			/*
			jq182('.main-daterange input.dr', jq182dropdown).prop('disabled', (jq182this.val() == 'custom' ? false : true));

			jq182('.comparison-preset', jq182dropdown).change();
			internal.refreshForm(); // should do only one refresh call
			*/
		},

		recalculateComparison : function() {
			var dates = jq182datepicker.DatePickerGetDate()[0];

			if (dates.length >= 2) {
				var comparisonPreset = internal.getComparisonPreset();
				switch (comparisonPreset) {
					case 'previousperiod':
						var days = parseInt((dates[1]-dates[0])/(24*3600*1000));
						dates[2] = new Date(dates[0]).setDate(dates[0].getDate() - (days+1));
						dates[3] = new Date(dates[1]).setDate(dates[1].getDate() - (days+1));
						break;
					case 'previousyear':
						dates[2] = new Date(dates[0]).setFullYear(dates[0].getFullYear(dates[0]) - 1);
						dates[3] = new Date(dates[1]).setFullYear(dates[1].getFullYear(dates[1]) - 1);
						break;
				}
				jq182datepicker.DatePickerSetDate(dates);
				//////console.log('comp', jq182this.val());
				jq182('.comparison-daterange input.dr', jq182dropdown).prop('disabled', (comparisonPreset == 'custom' ? false : true));
				internal.refreshForm();
			}
		},

		populateAggregations : function(aggregations) {
			var jq182select = jq182('select.aggregation', jq182dropdown);

			jq182select.html('');
			jq182.each(aggregations, function(i, aggregation) {
				jq182select.append(jq182("<option/>", {
					value : aggregation,
					text : db.aggregations[aggregation].title
				}));
			});
			internal.populateDateRangePresets();
		},

		/**
		 * Loads values from target element's data to controls.
		 */
		loadValues : function(jq182target) {
			var values = jq182target.data('DateRangesWidget').options.values;
			// handle initial values
			jq182('.dr1.from', jq182dropdown).val(values.dr1from);
			jq182('.dr1.from', jq182dropdown).change();
			jq182('.dr1.to', jq182dropdown).val(values.dr1to);
			jq182('.dr1.to', jq182dropdown).change();
			jq182('.dr2.from', jq182dropdown).val(values.dr2from)
			jq182('.dr2.from', jq182dropdown).change();
			jq182('.dr2.to', jq182dropdown).val(values.dr2to)
			jq182('.dr2.to', jq182dropdown).change();

			jq182aggregation.val(values.aggregation);
			jq182aggregation.change();

			jq182daterangePreset.val(values.daterangePreset);
			jq182daterangePreset.change();

			jq182parameter1.val(values.parameter1);
			jq182parameter1.change();

			jq182enableComparison.prop('checked', values.comparisonEnabled);
			jq182enableComparison.change();

			jq182comparisonPreset.val(values.comparisonPreset);
			jq182comparisonPreset.change();
		},

		/**
		 * Stores values from controls to target element's data.
		 */
		saveValues : function(jq182target) {
			var data = jq182target.data('DateRangesWidget');
			var values = data.options.values;
			values.aggregation = internal.getAggregation();
			values.daterangePreset = internal.getDaterangePresetVal()
			values.parameter1 = internal.getParameter1();
			values.dr1from = jq182('.dr1.from', jq182dropdown).val()
			values.dr1to = jq182('.dr1.to', jq182dropdown).val()
            values.dr1from_millis = jq182('.dr1.from_millis', jq182dropdown).val()
            values.dr1to_millis = jq182('.dr1.to_millis', jq182dropdown).val()
setDates([values.dr1from,values.dr1to]);
			values.comparisonEnabled = internal.getComparisonEnabled();
			values.comparisonPreset = internal.getComparisonPreset();
			values.dr2from = jq182('.dr2.from', jq182dropdown).val()
			values.dr2to = jq182('.dr2.to', jq182dropdown).val()

            values.dr2from_millis = jq182('.dr2.from_millis', jq182dropdown).val()
            values.dr2to_millis = jq182('.dr2.to_millis', jq182dropdown).val()
			jq182target.data('DateRangesWidget', data);

            if(jq182target.data().DateRangesWidget.options.onChange)
                jq182target.data().DateRangesWidget.options.onChange(values);

		},


		/**
		 * Updates target div with data from target element's data
		 */
		updateDateField : function(jq182target) {
			var values = jq182target.data("DateRangesWidget").options.values;

			if (values.aggregation) {
				jq182('span.aggregation', jq182target).text(values.aggregation);
				jq182('span.aggregation', jq182target).hide();
			} else {
				jq182('span.aggregation', jq182target).hide();
			}


			if (values.dr1from && values.dr1to) {
				jq182('span.main', jq182target).text(values.dr1from + ' - ' + values.dr1to);


			} else if(values.daterangePreset) {
			          var dates = db.date_presets[values.daterangePreset].dates();
                jq182('span.main', jq182target).text(dates[0] + ' - ' + dates[1]);

            }
            else {
							// var to = new Date();
							// var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 28);
							var dates = jq182target.data("DateRangesWidget").options.date;
							var fromDate = moment(dates[0]).format('DD MMMM YYYY');
							var toDate = moment(dates[1]).format('DD MMMM YYYY');
							jq182('span.main', jq182target).text(fromDate + ' - ' + toDate);
							// jq182('span.main', jq182target).text(  from.getDate()+' '+monthNames[from.getMonth()]+', '+
							// 	from.getFullYear()  +' - '+
							// 	to.getDate()+' '+monthNames[to.getMonth()]+', '+
							// 	to.getFullYear());
							//	jq182('span.main', jq182target).text('Enable Data Range');
			}
			if (values.comparisonEnabled && values.dr2from && values.dr2to) {
				jq182('span.comparison', jq182target).text(values.dr2from + ' - ' + values.dr2to);
				jq182('span.comparison', jq182target).show();
				jq182('span.comparison-divider', jq182target).show();
			} else {
				jq182('span.comparison-divider', jq182target).hide();
				jq182('span.comparison', jq182target).hide();
			}


			return true;
		},

		getAggregation : function() {
			return jq182aggregation.val();
		},

		getDaterangePresetVal : function() {
			return jq182daterangePreset.val();
		},

		getDaterangePreset : function() {
			return db.date_presets[jq182daterangePreset.val()];
		},

		setDaterangePreset : function(value) {
			jq182daterangePreset.val(value);
			jq182daterangePreset.change();
		},

		getParameter1 : function() {
			return parseInt(jq182parameter1.val());
		},

		setParameter1 : function(value) {
			jq182parameter1.val(value);
		},

		setComparisonEnabled : function(enabled) {
			jq182datepicker.DatePickerSetMode(enabled ? 'tworanges' : 'range');
		},

		getComparisonEnabled : function() {
			return jq182enableComparison.prop('checked');
		},

		getComparisonPreset : function() {
			return jq182comparisonPreset.val();
		},

		populateDateRangePresets : function() {
			var aggregation = internal.getAggregation();
            if(!aggregation)
                aggregation = default_aggregation;
			var main_presets_keys =  db.aggregations[aggregation].presets;

            var jq182other_presets = jq182('<optgroup/>', {label : 'Other presets'})
			var valueBackup = jq182daterangePreset.val();

			jq182daterangePreset.html('');

			// add main presets
			jq182.each(main_presets_keys, function(i, main_preset_key) {
				var date_preset = db.date_presets[main_preset_key];
				if (date_preset == undefined) throw 'Invalid preset "' + main_preset_key + '".';
				jq182daterangePreset.append(jq182("<option/>", {
					value : main_preset_key,
					text : date_preset.title
				}));
			});

			// add other presets
			jq182.each(db.date_presets, function(preset_key, date_preset) {
				if (jq182.inArray(preset_key, main_presets_keys) == -1) {
					jq182other_presets.append(jq182("<option/>", {
						value : preset_key,
						text : date_preset.title
					}));
				}
			});
			jq182daterangePreset.append(jq182other_presets);

			jq182daterangePreset.val(valueBackup);
		},

		expandDropdown : function(jq182target) {
			var options = jq182target.data("DateRangesWidget").options;
			jq182current_target = jq182target;

			// init aggregations
			if (options.aggregations.length > 0) {
				internal.populateAggregations(options.aggregations);
				jq182aggregationWrap.show();
			} else {
				jq182aggregationWrap.hide();
			}

			internal.loadValues(jq182target);


			// retract all other dropdowns
			jq182('.DRWOpened').each(function() {
				internal.retractDropdown(jq182(this));
			});

			var leftDistance = jq182target.offset().left;
			var rightDistance = jq182(document).width() - jq182target.offset().left - jq182target.width();
			//////console.log(leftDistance, rightDistance);
			jq182dropdown.show();
			if (rightDistance > leftDistance) {
				//////console.log('aligning left')
				//////console.log(jq182target.offset().top, jq182target.height());
				// align left edges
				////console.log( jq182target.height());
				jq182dropdown.offset({
					left : jq182target.offset().left+25,
					top : jq182target.offset().top + jq182target.height() - 1+25
				});
				jq182dropdown.css('border-radius',  '0 5px 5px 5px')
			} else {
				//////console.log('aligning right')
				// align right edges
				var fix = parseInt(jq182dropdown.css('padding-left').replace('px', '')) +
					parseInt(jq182dropdown.css('padding-right').replace('px', '')) +
					parseInt(jq182dropdown.css('border-left-width').replace('px', '')) +
					parseInt(jq182dropdown.css('border-right-width').replace('px', ''))
				jq182dropdown.offset({
					left : jq182target.offset().left + jq182target.width() - jq182dropdown.width() - fix,
					top : jq182target.offset().top + jq182target.height() - 1
				});
				jq182dropdown.css('border-radius',  '5px 0 5px 5px')
			}


			// switch to up-arrow
			jq182('#goal-date-range-field a', jq182target).html('&#9650;');
			jq182('#goal-date-range-field', jq182target).css({borderBottomLeftRadius:0, borderBottomRightRadius:0});
			jq182('#goal-date-range-field a', jq182target).css({borderBottomRightRadius:0});
			jq182target.addClass('DRWOpened');
			jq182target.removeClass('DRWClosed');


			// refresh
			internal.recalculateDaterange();
		},

		retractDropdown : function(jq182target) {
			//////console.log('retract', jq182target);

			jq182dropdown.hide();
			jq182('#goal-date-range-field a', jq182target).html('&#9660;');
			jq182('#goal-date-range-field', jq182target).css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
			jq182('#goal-date-range-field a', jq182target).css({borderBottomRightRadius:5});
			jq182target.addClass('DRWClosed');
			jq182target.removeClass('DRWOpened');
		},

		getMonday : function(d) {
			d = new Date(d);
			var day = d.getDay();
			var diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
			return new Date(d.setDate(diff));
		}

	};

	jq182.fn.DateRangesWidget = function(method) {



		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply( this, arguments );
		} else {
			jq182.error('Method ' +  method + ' does not exist on jQuery.DateRangesWidget');
		}

	};
	setDates=function(dates){
		this.dates=dates;
	};
	getDates=function(){
		return this.dates;
	};

})( jQuery );
