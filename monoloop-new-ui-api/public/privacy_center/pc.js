window.ml_pc_var.timer = null ;

	function ml_hasClass(el, className) {
	  if (el.classList)
	    return el.classList.contains(className)
	  else
	    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
	}

	function ml_addClass(el, className) {
	  if (el.classList)
	    el.classList.add(className)
	  else if (!hasClass(el, className)) el.className += " " + className
	}

	function ml_removeClass(el, className) {
	  if (el.classList)
	    el.classList.remove(className)
	  else if (hasClass(el, className)) {
	    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
	    el.className=el.className.replace(reg, ' ')
	  }
	}

	function _ml_html_1(){

		var content = '' ;
		if(window.ml_pc_var.preview){
			content += '<div id="_ml_pc" class="_ml_widget _ml_ltr _ml_skin_dark _ml_position_right _ml_widget_open " style="bottom: 0px;">' ;
		}else{
			var c = '_ml_widget_hidden' ;
			if(window.ml_pc_var.pvc <= 1 ){
				c = '_ml_widget_open _ml_widget_firsttime' ;
			}
			content += '<div id="_ml_pc" class="_ml_widget _ml_ltr _ml_skin_dark _ml_position_right '+c+'" style="bottom: 0px;">' ;
		}
		content += '<a class="_ml_widget_open_close _ml_action_toggle_widget"><span class="_ml_widget_icon"></span></a>' ;
		content += '<div class="_ml_widget_hidden_handle _ml_action_toggle_widget"></div>';
		content += '<div id="_ml_pc_body">'
							+'<div class="_ml_widget_body">'
								+'<p class="_ml_widget_title _ml_action_open_widget"><span class="_ml_header_text">Privacy Center</span></p>'
									+'<div class="_ml_widget_state _ml_widget_state_open">'
										+'<ul><li class="_ml_menu_preferences active" onclick="ml_tab_switch(\'preferences\')"><a ><strong>Consent</strong></a></li><li>|</li><li class="_ml_menu_data" onclick="ml_tab_switch(\'data\')"><a ><strong>Profile</strong></a></li></ul>'
										+'<div style="clear: both;"></div>'
										+'<div class="_ml_intro"></div>'
									+'</div>'
									+'<div class="_ml_widget_footer"><p>Powered by <a href="http://monoloop.com" target="_blank"><img style="height: 27px;vertical-align:bottom;" src="//mlres.s3.amazonaws.com/images/ml-logo-small.png"></a></p><div class="_hj-f5b2a1eb-9b07_clear_both"></div>'
							+'</div>'
						+'</div>';
		content += '</div></div>' ;
		MONOloop.html(MONOloop.$('#_ml_pc_container'),content);

		_ml_html_2();
	}

	function _ml_callCssURL(url, cb) {
    var content;
    content = {};
    content = document.createElement('link');
    if (!cb) {
      cb = function() {};
    }
    content.type = 'text/css';
    content.rel = 'stylesheet';
    if(url.indexOf("http") !== 0)
    	content.href = ("https:" === document.location.protocol ? 'https://' + url : 'http://' + url);
 	  else
 	    content.href = url ;
    document.getElementsByTagName("head")[0].appendChild(content);
    if (content.readyState) {
      content.onreadystatechange = function() {
        if (content.readyState === "loaded") {
          content.onreadystatechange = null;
          return cb();
        } else if (content.readyState === "complete") {
          content.onreadystatechange = null;
          return cb();
        }
      };
    } else {
      content.onload = function() {
        return cb();
      };
    }
    return content;
  }

	function _ml_widgetclick(){
		clearTimeout(window.ml_pc_var.timer);
		var widgetEL = MONOloop.$('._ml_widget')[0] ;
		if(ml_hasClass(widgetEL,'_ml_widget_hidden')){
			ml_addClass(widgetEL,'_ml_widget_open');
			ml_removeClass(widgetEL,'_ml_widget_hidden');
			ml_removeClass(widgetEL,'_ml_widget_firsttime');
		}else{
			ml_addClass(widgetEL,'_ml_widget_hidden');
			ml_removeClass(widgetEL,'_ml_widget_open');
		}
	}

	function _ml_init(){
		MONOloop.click(MONOloop.$('._ml_action_toggle_widget'),function(){
			_ml_widgetclick() ;
		});

		if(window.ml_pc_var.preview){
			window.ml_pc_var.timer = setTimeout(function(){
				MONOloop.$('._ml_action_toggle_widget')[0].click() ;
			}, 3000);
		}else{
			if(window.ml_pc_var.pvc <= 1 ){
		 		window.ml_pc_var.timer = setTimeout(function(){
					MONOloop.$('._ml_action_toggle_widget')[0].click() ;
				}, 3000);
	 		}
		}
	}

	function ml_tab_switch(module){
		if(MONOloop.$('._ml_preferences').length === 0 && module === 'intro'){
			return ;
		}else if(MONOloop.$('._ml_preferences').length === 0){
			//Load additional asset
      _ml_html_2() ;
    	ml_tab_switch2(module) ;
		}else{
			ml_tab_switch2(module) ;
		}
	}

	function ml_tab_switch2(module){
		//remove active class ;
		var els = MONOloop.$('._ml_widget_state li') ;

		for(var i = 0 ; i < els.length ; i++){
			ml_removeClass(els[i],'active');
		}
		ml_removeClass(MONOloop.$('._ml_intro')[0],'active');
		ml_removeClass(MONOloop.$('._ml_preferences')[0],'active');
		ml_removeClass(MONOloop.$('._ml_data')[0],'active');

		if(module == 'preferences'){
			ml_addClass(MONOloop.$('._ml_preferences')[0],'active');
			ml_addClass(MONOloop.$('._ml_menu_preferences')[0],'active');
		}else if(module == 'data'){
			ml_addClass(MONOloop.$('._ml_data')[0],'active');
			ml_addClass(MONOloop.$('._ml_menu_data')[0],'active');
		}else if(module == 'intro'){
			ml_addClass(MONOloop.$('._ml_intro')[0],'active');
			ml_addClass(MONOloop.$('._ml_menu_intro')[0],'active');
		}
	}


// PC2


	function _ml_change_privacy_config(event){
		if(event.target.tagName != 'INPUT'){
			var elem = document.getElementById("ml_personalization");
			elem.checked=!elem.checked;
		}
		_ml_save_privacy_config();

	};

	function _ml_save_privacy_config(){
		var ml_personalization = localStorage.getItem('ml_personalization');

		if(MONOloop.val(MONOloop.$('input:checkbox[id="ml_personalization"]:checked'))){
    	ml_personalization = '1' ;
    }else{
    	ml_personalization = '0' ;
    }

    if( ml_personalization == null){
      ml_personalization = '1' ;
    }

    //cal frontend ;
    var gmid = localStorage.getItem('gmid');
		var skey = localStorage.getItem('skey');
		var mid = localStorage.getItem('mid');

		if(gmid === null || gmid === undefined || gmid === '' || gmid === 'null'){
			//gmid = '57304ecd2c1a5b9241600364' ;
			return;
		}

		MONOloop.callURL(ml_pc_var.frontend_url  + '/updatePrivacy/?gmid=' + gmid + '&mid=' + mid  + '&skey=' + skey + '&vid=' + vid + '&cid=' + ML_vars.cid + '&personalization=' + ml_personalization , function(){
			alert('Preferences have been updated');
	  });


    localStorage.setItem('ml_personalization',ml_personalization) ;

	};

	function _ml_delete_cookie(){

		if( confirm("You are about to delete your Monoloop cookie - by doing so this site will not be able to recognize you.\nNOTE: We will NOT be able to respect any Privacy Preferences you may have indicated under Preferences. If you return to this site again - you will be recorded as a new visitor and we will initiate default tracking.")){
      localStorage.setItem('monoloop_pc','');
      localStorage.setItem('gmid','');
      localStorage.setItem('mid','');
      localStorage.setItem('skey','');
      MONOloop.removeGmid() ;
      alert('Cookie has been deleted') ;
    } ;
	};

	function _ml_delete_profile(){
		if( confirm("You are about to delete your profile - by doing so this site will not be able to personalize its content to you. Are you sure you want to delete the profile?")){
			var gmid = localStorage.getItem('gmid');
			var skey = localStorage.getItem('skey');
			var mid = localStorage.getItem('mid');

			if(gmid === null || gmid === undefined || gmid === '' || gmid === 'null'){
				//gmid = '57304ecd2c1a5b9241600364' ;
				return;
			}

			var ml_personalization = localStorage.getItem('ml_personalization');

			if(MONOloop.val(MONOloop.$('input:checkbox[id="ml_personalization"]:checked'))){
	    	ml_personalization = '1' ;
	    }else{
	    	ml_personalization = '0' ;
	    }

	    if( ml_personalization == null){
	      ml_personalization = '1' ;
	    }


			MONOloop.callURL(ml_pc_var.frontend_url  + '/emptyProfile/?gmid=' + gmid + '&mid=' + mid  + '&skey=' + skey + '&cid=' + ML_vars.cid + '&personalization=' + ml_personalization , function() {
				alert('Profile has been deleted') ;
	  	});

		}
	};

	function _ml_show_profile(){
		var gmid = localStorage.getItem('gmid');
		var skey = localStorage.getItem('skey');
		ml_addClass(MONOloop.$('._ml_profile_dialog')[0],'active');
		if(gmid === null || gmid === undefined || gmid === '' || gmid === 'null'){
			//gmid = '5735b8bbd1d3138d3b81d174' ;
			_ml_generate_profile_nocontent() ;
			return ;
		}



		var xmlhttp=new XMLHttpRequest();
		xmlhttp.open("GET",  ml_pc_var.app_url + '/api/profiles/'+gmid + '?skey=' + skey);
		xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
        if(xmlhttp.status == 200){
          profile = JSON.parse(xmlhttp.responseText);
          _ml_generate_profile_content(profile) ;
        }else{
          _ml_generate_profile_nocontent() ;
        }
	    }
		}
		xmlhttp.send();
	};

	function _ml_download_profile(){
		var gmid = localStorage.getItem('gmid');
		var skey = localStorage.getItem('skey');
		if(gmid === null || gmid === undefined || gmid === '' || gmid === 'null'){
			return ;
		}



		var xmlhttp=new XMLHttpRequest();
		xmlhttp.open("GET",  ml_pc_var.app_url + '/api/profiles/'+gmid + '?skey=' + skey);
		xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
        if(xmlhttp.status == 200){
          var profile = JSON.parse(xmlhttp.responseText);
          _ml_download_json(profile);
        }else{
        }
	    }
		}
		xmlhttp.send();
	};

	function _ml_download_json(json_obj){
		var exportName = localStorage.getItem('gmid');
		var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(json_obj));
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href",     dataStr);
    downloadAnchorNode.setAttribute("download", exportName + ".json");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
	}

	function _ml_close_dialog(){
		ml_removeClass(MONOloop.$('._ml_profile_dialog')[0],'active');
	};


	function _ml_generate_profile_content(profile){
		profile = profile.profile ;
		var data = '<table  width="100%"><tr><td>Cross Device Profile:</td><td>'+profile._id+'</td></tr></table><br><strong>Profiles</strong><br>' ;
		for(var i = 0 ; i < profile.profiles.length ; i++){
			data = data + _ml_generate_profile_detail(profile.profiles[i]);
		}
		MONOloop.html(MONOloop.$('._ml_dialog_inner_content'),data);
	};

	function _ml_generate_profile_nocontent(){
		var data = '<table  width="100%"><tr><td><p align="center">No data found</p></td></tr></table>' ;
		MONOloop.html(MONOloop.$('._ml_dialog_inner_content'),data);
	};

	function _ml_generate_profile_detail(profile){
		console.debug(profile);
		var id = profile._id['$id'] ;
		var datat = '' ;
		if(profile.device_info){
			data = '<div class="_ml_profile_group" onclick="_ml_profile_detail_plus_click(\''+id+'\')"><div class="l">Device profile: '+id+'</div><div id="_ml_profile_icon_'+id+'" class="r plus" ></div><div class="_ml_clear"></div></div><div id="_ml_profile_detail_'+id+'" class="_ml_profile_detail"><table  width="100%"><tr><td>Browser :</td><td>'+profile.device_info.config_browser_name+' '+profile.device_info.config_browser_version+'</td></tr><tr><td>Os :</td><td>'+profile.device_info.config_os.family+'</td></tr><tr><td>Cookie :</td><td>'+profile.device_info.config_cookie+'</td></tr><tr><td>Visit server date :</td><td>'+_ml_timeConverter(profile.visit_server_date)+'</td></tr></table>' ;
		}else{
			data = '<div class="_ml_profile_group" onclick="_ml_profile_detail_plus_click(\''+id+'\')"><div class="l">Device profile: '+id+'</div><div class="_ml_clear"></div>'
		}

		if(profile.visits){
			for(var i = 0 ; i < profile.visits.length ; i++){
				data = data + _ml_generate_visit_detail(profile.visits[i]);
			}

		}

		return data + '</div>' ;
	};

	function _ml_generate_visit_detail(visit){
		var id = visit._id['$id'] ;
		return '<div class="_ml_visit_group" onclick="_ml_visit_detail_plus_click(\''+id+'\')"><div class="l">Visit : '+_ml_timeConverter(visit.firstclick)+'</div><div id="_ml_visit_icon_'+id+'" class="r plus" ></div><div class="_ml_clear"></div></div><div id="_ml_visit_detail_'+id+'" class="_ml_visit_detail"><table  width="100%"><tr><td>Entrypage :</td><td>'+visit.entrypage+'</td></tr><tr><td>Exitpage :</td><td>'+visit.exitpage+'</td></tr><tr><td>Referer :</td><td>'+visit.referer+'</td></tr><tr><td>Firstclick :</td><td>'+_ml_timeConverter(visit.firstclick)+'</td></tr><tr><td>Lastclick :</td><td>'+_ml_timeConverter(visit.lastclick)+'</td></tr><tr><td>Clicks :</td><td>'+visit.clicks+'</td></tr></table></div>'
	};

	function _ml_profile_detail_plus_click(profile_id){
		if(ml_hasClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'plus')){
			ml_removeClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'plus');
			ml_addClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'minus');
			ml_addClass(MONOloop.$('#_ml_profile_detail_'+profile_id)[0],'active');
		}else{
			ml_removeClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'minus');
			ml_addClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'plus');
			ml_removeClass(MONOloop.$('#_ml_profile_detail_'+profile_id)[0],'active');
		}
	};

	function _ml_visit_detail_plus_click(visit_id){
		if(ml_hasClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'plus')){
			ml_removeClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'plus');
			ml_addClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'minus');
			ml_addClass(MONOloop.$('#_ml_visit_detail_'+visit_id)[0],'active');
		}else{
			ml_removeClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'minus');
			ml_addClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'plus');
			ml_removeClass(MONOloop.$('#_ml_visit_detail_'+visit_id)[0],'active');
		}
	};


	function _ml_timeConverter(UNIX_timestamp){
	  var a = new Date(UNIX_timestamp * 1000);
	  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	  var year = a.getFullYear();
	  var month = months[a.getMonth()];
	  var date = a.getDate();
	  var hour = a.getHours();
	  var min = a.getMinutes();
	  var sec = a.getSeconds();
	  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	  return time;
	}

	function _ml_html_2(){
		MONOloop.html(MONOloop.$('#_ml_profile_modal'),'<div class="_ml_dialog_content"><a  title="Close" class="close" onclick="_ml_close_dialog();">X</a><strong>Profile Information</strong><hr/><div class="_ml_dialog_inner_content"></div><div class="_ml_profile_footer"><button class="_ml_btn_primary _ml_rounded_corners _ml_transition _ml_shadow" type="button" onclick="_ml_download_profile();">Download</button></div></div>');

		var content
						 ='<div class="_ml_info "></div>'
						 + '<div class="_ml_preferences active"><p><b>Please select how we track and handle your data.</b></p><br/><table style="padding-left: 50px;margin:0;"><tbody><tr onclick="_ml_change_privacy_config(event);"><td><input type="checkbox" value="1" name="ml_personalization" id="ml_personalization"></td><td>Personalization</td></tr></tbody></table></div>'
						 + '<div class="_ml_data"><p>&nbsp;</p><p align="center"><button class="_ml_btn_primary _ml_rounded_corners _ml_transition _ml_shadow" type="button" onclick="_ml_show_profile();">View profile</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="_ml_btn_primary _ml_rounded_corners _ml_transition _ml_shadow" type="button" onclick="_ml_download_profile();">Download</button></p><p>&nbsp;</p><br/><div class="_ml_pc_delete_by"><p>&nbsp;</p><p align="center"><button class="_ml_btn_danger _ml_rounded_corners _ml_transition _ml_shadow" type="button" onclick="_ml_delete_profile();">Delete profile</button></p></div></div>' ;

		MONOloop.append(MONOloop.$('._ml_widget_state'), content);

		if(localStorage.getItem('ml_personalization') == '1' || localStorage.getItem('ml_personalization') == ''){
			MONOloop.$('input:checkbox[id="ml_personalization"]')[0].checked = true;
		}

	}