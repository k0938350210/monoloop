var operators = [{name: 'AND' }, { name: 'OR' }];
var logicalOperatorSymbols = {AND: ' && ',OR: ' || ' };
var integerBinaryConditions = [{  name: '==' }, {   name: '!=' }, {  name: '<' }, {  name: '>'}, {   name: '<='}, {  name: '>='}];
var stringBinaryConditions = [{name: 'is'}, {name: 'is not'}, {name: 'contains'}, {name: 'does not contain'}, {name: 'starts with'}, {name: 'ends with'}];
var stringBinaryConditionsSymbols = {'is': '===','is not': '!==','contains': 'ml_contains','does not contain': 'ml_dose_not_contains','starts with': 'ml_starts_with','ends with': 'ml_ends_with'};
var labelKeyDefault = "MonoloopProfile";
// var profilePropertiesUrl = '/api/profile-properties';
// var profilePropertiesTemplateUrl = '/api/profile-properties-template';
var conditionbuilderLayout = '';
