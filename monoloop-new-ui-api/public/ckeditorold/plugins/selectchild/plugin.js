CKEDITOR.plugins.add( 'selectchild', {
    icons: 'selectchild',
    init: function( editor ) {
      editor.addCommand( 'insertSelectChild',
      {
        exec : function( editor )
        {
          alert('selectchild');
        }
      });

      editor.ui.addButton( 'selectchild',
      {
      	label: 'Select child',
      	command: 'insertSelectChild',
        toolbar: 'ePlugins,4',
      	icon: this.path + 'icons/selectchild.png'
      } );
    }
});
