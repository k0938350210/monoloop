CKEDITOR.plugins.add('variation',
{
	selected_variation : '' ,
	requires : ['richcombo'],
	findActiveVariation: function(xpath){
		var data = monoloop_select_view.queuedVariations;
		if(data.length > 0){
			var active;
			for(var i = 0; i< data.length; i++){
				if(data[i].mappedTo === xpath){
					return data[i];
				}
			}
		}
		return false;
	},
	init : function( editor )
	{
		var plugin = this ;
		var seperator = '___CONTENT_ID_SEPERATOR_MP___';
		this.selected_variation = '' ;
		//  array of variations to choose from that'll be inserted into the editor
		var strings = [];
		var c, cType, imageLink, element, tagName, initData, originalTxt;

		initData = CKEDITOR.config.initData;
		originalTxt = monoloop_select_view.mapOriginalText(initData.xpath);
		element = monoloop_select_view.getSelectorByXPath(initData.xpath);
		tagName = element.prop('tagName').toLowerCase();

		if(originalTxt){
			c = originalTxt.text.replace(/\n/g, "");
			cType = "";
			imageLink = "";
			if(tagName === 'img'){
				cType = originalTxt.contentType;
				imageLink = originalTxt.imageLink;
			}
			strings.push([
				c,
				"Original",
				"Original",
				"Original",
				"",
				cType,
				imageLink
			]);
		}

		// find active variation from the history
		var defaultVaitionLabel = "Variations";
		var activeVar = this.findActiveVariation(initData.xpath);
		if(activeVar){
			defaultVaitionLabel = activeVar.variation_title;
		}

		if(initData.variations.length > 0){

			for (var i = 0; i < initData.variations.length; i++) {
				c = "";
				cType = "";
				imageLink = "";
				if(initData.variations[i].content){
					if(initData.variations[i].content.config){
						if(initData.variations[i].content.config){
							if(initData.variations[i].content.config.content){
								c = initData.variations[i].content.config.content.replace(/\n/g, "");
							}
							if(initData.variations[i].content.config.content_type){
								cType = initData.variations[i].content.config.content_type;
							}
							if(initData.variations[i].content.config.image_link){
								imageLink = initData.variations[i].content.config.image_link;
							}
						}
					}
				}
				strings.push([
					c,
					initData.variations[i].content.name,
					initData.variations[i].content.name,
					initData.variations[i].content._id,
					initData.variations[i].content.condition,
					cType,
					imageLink
				]);
			}
		}


		// add the menu to the editor
		editor.ui.addRichCombo('variation',
		{
			label: 		defaultVaitionLabel,
			title: 		defaultVaitionLabel,
			voiceLabel: 'Variations',
			className: 	'cke_format',
      toolbar: 'ePlugins,0',
			multiSelect:false,
			panel:
			{
				css: [ editor.config.contentsCss, CKEDITOR.skin.getPath('editor') ],
				voiceLabel: editor.lang.panelVoiceLabel
			},

			init: function()
			{

				// this.startGroup( "Variations" );
				for (var i in strings)
				{
					this.add(
						strings[i][0] + seperator + strings[i][3] + seperator +  strings[i][1] + seperator +  strings[i][4] + seperator +  strings[i][5] + seperator +  strings[i][6],
						strings[i][1],
						strings[i][2]
					);
				}

			},

			onClick: function( value )
			{

				var valArray = value.split(seperator),
						newValue = valArray[0],
						content_id = valArray[1],
						variation_name = valArray[2],
						condition = valArray[3],
						content_type = valArray[4];
						image_link = valArray[5];

						selector = CKEDITOR.config.mappedTo,
						selectorId = monoloop_select_view.getXPathId(selector),
						element = monoloop_select_view.getSelectorByXPath(selector),
						id = element.attr("id");

				this.title = variation_name;
				this.label = variation_name;
				// this.title = variation_name;
				// this.voiceLabel = variation_name;

				plugin.selected_variation = variation_name ;

				editor.focus();
				editor.fire( 'saveSnapshot' );
				switch (content_type) {
					case 'image':
						MONOloop.jq("#"+id).attr('src', image_link + '?' + Math.random());
						if(newValue === "<!-- monoloop remove -->&nbsp;"  || newValue.indexOf('monoloop_remove') !== -1){
							MONOloop.jq("#"+id).addClass('monoloop_remove');
              // MONOloop.jq("#"+id).css("display", "none");
						} else {
							// MONOloop.jq("#"+id).removeClass('monoloop_remove');
              // MONOloop.jq("#"+id).css("display", "block");
						}
						break;
					default:
						var newValue = monoloop_select_view.ejs2Html(newValue);
						editor.setData(newValue);
						break;

				}
				editor.fire( 'saveSnapshot' );
				if(newValue === "<!-- monoloop remove -->&nbsp;" || newValue.indexOf('monoloop_remove') !== -1){
					monoloop_select_view.createDeleteBubble({mappedTo: selector, selectorId: selectorId});
				} else {
					MONOloop.jq('.removedElement.' + selectorId).remove();
				}

				this.queueVariation({
					mappedTo: CKEDITOR.config.mappedTo,
					variation_title: variation_name,
					content_id: content_id,
					condition: condition,
					content_type: content_type
				});

				parent.postMessage(JSON.stringify({t: "edit-content-element-variation", data: {content_id: content_id}}), MONOloop.domain);

			},

			queueVariation: function(vari){
				var data = monoloop_select_view.queuedVariations;

				if(data.length > 0){
					var found = false;
					for(var i = 0; i< data.length; i++){
						if(data[i].mappedTo === vari.mappedTo){
							found = true;
							if(vari.variation_title === "Original") {
								monoloop_select_view.queuedVariations.splice(i,1);
							} else {
								data[i] = vari;
							}
						}
					}
					if(!found){
							if(vari.variation_title !== "Original") monoloop_select_view.queuedVariations.push(vari);
					}
				} else {
					if(vari.variation_title !== "Original") monoloop_select_view.queuedVariations.push(vari);
				}
			}
		});
	}
});
