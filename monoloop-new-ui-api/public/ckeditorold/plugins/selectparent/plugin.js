CKEDITOR.plugins.add( 'selectparent', {
    icons: 'selectparent',
    init: function( editor ) {
      editor.addCommand( 'insertselectparent',
      {
        exec : function( editor )
        {
          alert('parent');
        }
      });

      editor.ui.addButton( 'selectparent',
      {
      	label: 'Select parent',
      	command: 'insertselectparent',
        toolbar: 'ePlugins,3',
      	icon: this.path + 'icons/selectparent.png'
      } );
    }
});
