CKEDITOR.plugins.add( 'remove', {
    icons: 'remove',
    init: function( editor ) {
      editor.addCommand( 'insertRemove',
      {
        exec : function( editor )
        {
          var xpath = CKEDITOR.config.mappedTo,
              selector = monoloop_select_view.getSelectorByXPath(xpath),
              id = selector.attr('id'),
              tagName = selector.prop("tagName").toLowerCase(),
              src = "";
          switch (tagName) {
            case 'img':
              selector.addClass('monoloop_remove');
              // selector.css("display", "none");
              // monoloop_select_view.pushChangedElement(editor, xpath, selector, tagName);
              monoloop_select_view.pushActionsOnXapth(xpath);
              break;
            default:
              editor.setData(editor.getData() + "<!-- monoloop remove -->&nbsp;");
              break;

          }

          monoloop_select_view.createDeleteBubble({
            mappedTo: CKEDITOR.config.mappedTo,
            selectorId: monoloop_select_view.getXPathId(CKEDITOR.config.mappedTo)
          });
        }
      });

      // editor.ui.addButton( 'Remove',
      // {
      // 	label: 'Remove',
      // 	command: 'insertRemove',
      //   toolbar: 'ePlugins,3',
      // 	icon: this.path + 'icons/remove.gif'
      // } );
    }
});
