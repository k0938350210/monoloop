<?php

return [
	'url' => env('MANTIS_URL','https://mantis.monoloop.com'),
	'username' => env('MANTIS_USER'),
	'password' => env('MANTIS_PASSWORD'),
	'project_id' => env('MANTIS_P_ID'),
	'filter_id' => env('MANTIS_FILTER_ID'),
	'bug_cate_id' => env('MANTIS_BUG_CATEGORY_ID'),
	'parent_issue_id' => env('MANTIS_MAIN_ISSUE'),
];
