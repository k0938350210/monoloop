<?php namespace App;

use App\TrackerBase ;

class Asset extends TrackerBase{

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }

  public function asset_details()
  {
    return $this->embedsMany(\App\AssetDetail::class);
  }

  public function assetDetails(){
    return $this->embedsMany(\App\UrlConfig::class,null,'asset_details','asset_details');
  }
}
