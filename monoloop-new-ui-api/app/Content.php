<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;

class Content extends Eloquent{

  protected $collection = 'contents';

  /*--- relation ----*/
  /*
   * The contents associated with specific folder
   *
   * @param folder Object
   * @rerurn array
   */
  public function getFolderItems($folder = NULL, $isRoot = false, $account_id = NULL){

    $branch = [];

    if($isRoot){
        $contents = Content::where('folder_id', new \MongoId($folder))
                            ->orwhere(function ($query) use ($folder) {
                                        $query->where('folder_id', '=', NULL);
                          } )->where(function ($query) {
                                   $query->where('deleted', 0)
                                         ->orwhere(function ($query) {
                                                     $query->where('deleted', '=', NULL);
                                       } );
                                 })->orderBy('created_at', 'desc')->get();

    } else {
        $contents = Content::where('folder_id', new \MongoId($folder))
                            ->where(function ($query) {
                                     $query->where('deleted', 0)
                                           ->orwhere(function ($query) {
                                                       $query->where('deleted', NULL);
                                         } );
                                   })->orderBy('created_at', 'desc')->get();
    }


      foreach ($contents as $key => $content) {
          $node = new \stdClass();
          $node->id = "webcontentlist__".$content->_id;
          $node->value = $content->name;
          $node->type = "file";
          // $node->description = "";
          // if(!empty($content->config)){
          //   $node->description = $content->config->content;
          // }


          $node->date = date('j F, Y', strtotime($content->created_at));
          if(isset($content->attributes['hidden'])){
            $hidden = $content->attributes['hidden'];
          } else {
            $hidden = 0;
          }
          $node->hidden = $hidden;
          $node->condition = "";
          array_push($branch, $node);
    }

    return $branch;
  }

  public function config()
  {
    return $this->embedsOne(\App\ContentConfig::class);
  }

  public function content_template()
  {
    return $this->belongsTo(\App\ContentTemplate::class,'content_template_id');
  }

  public function account(){
    return $this->belongsTo(\App\Account::class,'account_id');
  }

  /*--- attribute ----*/
  public function getCodeAttribute(){
    if($this->type == 'variation'){
      $condition = $this->condition ;
      $content = "";
      if(isset($this->config)){
        $content = $this->config->content ;
      }
      $condition = str_replace('{|}','{ | }', $condition);
      $conditionList = explode(' | ' , $condition) ;
      if(count($conditionList) == 2){
        return '<% ' . $conditionList[0] . ' %>' . $content . '<% ' . $conditionList[1] . ' %>' ;
      }
      return $content ;
    }
  }

	/*--- Scope ---*/

  public function scopeByAccount($query , $account_id){
    return $query->where('account_id', new MongoId($account_id));
  }

  public function scopeByUid($query,$uid){
    return $query->where('uid', (int)$uid);
  }

  public function scopeAvailable($query){
    return $query->where('deleted', 0)->whereIn('type',['text','complex']) ;
  }

  public function scopeFolderContent($query , $folder_id){
    return $query->where('folder_id', new \MongoId($folder_id));
  }
}
