<?php

namespace App\Handlers\Events;

use Auth;
use App\Events\Logger;
use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GenerateLogger
{
    /**
     * Create the event handler.
     */
    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    /**
     * Handle the event.
     *
     * @param Logger $event
     */
    public function handle(Logger $event  )
    {
        $this->log->account_id = new \MongoId(Auth::User()->account->_id);
        $this->log->user_id = new \MongoId(Auth::User()->_id);
        $this->log->cid = Auth::User()->account->uid;
        $this->log->ip = request()->ip();
        $this->log->logable_type = !empty($event->logableType) ? $event->logableType : null;
        $this->log->logable_id = $event->logableId;
        $exMsg = !empty($event->extenededMessage) ?  ' ['.$event->extenededMessage.']' : "";
        $this->log->msg = trim('['.$event->username.']'.' '.$event->message.' '.$exMsg);
        $this->log->save();
    }

}
