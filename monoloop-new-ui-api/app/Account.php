<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use App\Tracker;

class Account extends Eloquent{

  protected $dates = ['last_billing_date'];

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
		// if(!isset($this->domains)){
		// 	$this->domains = [];
		// }
	}

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'accounts';

	/**
	 * The account plugins
	 * @return void
	 */
	public function plugins()
  {
      return $this->embedsMany('\App\AccountPlugin');
  }

	/**
	 * The account domains
	 * @return void
	 */
	public function domains()
	{
	    return $this->embedsMany('\App\AccountDomain');
	}

	/**
	 * The account logs
	 * @return void
	 */
	public function logs()
	{
	    return $this->hasMany('\App\Log', 'account_id');
	}

  public function billing_historicals()
  {
      return $this->hasMany('\App\BillingHistorical', 'account_id');
  }

	protected $hidden = ['is_reseller','lock_account_type','auto_activate_after_trial','trialconvertion','s3_bucket','domains'];

	/*------- scope -----------*/
	public function scopeSearch($query,$s){
		if(trim($s) == ''){
			return $query ;
		}
		return $query->where(function ($query) use ($s) {
			$query->where('name', 'like', '%'.$s.'%') ;
			$query->orWhere('uid', (int)$s ) ;
		});
	}

  public function scopeChild($query , $id){
    return $query->where('parent_id', new MongoId($id) );
  }

  public function scopeByUid($query , $uid){
    return $query->where('uid', (int)$uid);
  }

  public function scopeByAvailable($query){
    return $query->where(function($query){
    	$query->where('deleted',0)->orWhere('deleted','exists',false) ;
    })->where(function($query){
    	$query->where('hidden',0)->orWhere('hidden','exists',false) ;
    });
  }

  /*------- attribute --------*/
  public function getIsGAEnabledAttribute(){
    foreach( $this->plugins  as $plugin ){
      if($plugin->plugin->code == 'ga')
        return $plugin->enabled ;
    }
    return false ;
  }

  public function getIsUnSafeAttribute(){
    if($this->invocation['content_delivery'] == 1){
      return true ;
    }
    return false ;
  }

  public function getFirstnameAttribute(){
    $names = explode(' ' , $this->company );
    if(isset($names[0]))
      return $names[0];
    return $this->company;
  }

  public function getLastnameAttribute(){
    $names = explode(' ' , $this->company);
    if(isset($names[1]))
      return $names[1];
    return $this->company;
  }

	public function getFirstDomainAttribute(){
    $domains = $this->domains ;
    $firstDomain = '' ;
    if(count($domains)>0){
      $firstDomain = $domains[0]->domain;
    }
    return $firstDomain;
  }

	public function getCustomerUniqueIdAttribute(){
		return 0 ;
	}

  public function getExternalKeyIdAttribute(){
    $tracker = Tracker::isSystem()->where('system_name', 'external-key')->first();
    if(!is_null($tracker)){
      return $tracker->uid;
    }
    return 0;
  }

  #custom function

  public function isWpPluginActivated($plugin){
    if(count($this->wp_plugins)>0){
      foreach($this->wp_plugins as $wp_plugin){
        if($wp_plugin['File'] == $plugin){
          return true ;
        }
      }
    }
    return false ;
  }

}
