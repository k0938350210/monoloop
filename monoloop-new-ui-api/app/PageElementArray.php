<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;


class PageElementArray extends Eloquent{ 
  protected $fillable = ["pageelementID"];
}
