<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Validator;
use DB;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		\Auth::extend('monoloop', function($app)
    {
        return new MonoloopUserProvider();
    });

    View::share('nav', request('nav'));
		View::share('cms_page_url', request('url'));

		Validator::extend('uniquemon', function($attribute, $value, $parameters, $validator) {
 			$is_exists = DB::collection($parameters[0])->where('name',$value)->where($parameters[3],'<>',$parameters[2])->where($parameters[4],(int)$parameters[5])->where($parameters[6],(int)$parameters[7])->exists();
      return !$is_exists;
    });
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
