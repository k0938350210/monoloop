<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use MongoId ; 

class TrackerNode extends Eloquent{
  protected $collection = 'trackers'; 
  
  /*--- Fixed Data ---*/
  public static $types = array(
    'tracker' => 'Tracker' , 
    'customvar' => 'Custom Var' , 
    'asset' => 'Asset'
  );
  /*--- Relation --- */
  
  public function tracker(){
    return $this->embedsOne(\App\Tracker::class); 
  }
  
  public function customvar(){
    return $this->embedsOne(\App\CustomVar::class); 
  }
  
  public function asset(){
    return $this->embedsOne(\App\Asset::class); 
  }
   
  public function urlConfig(){
    return $this->embedsOne(\App\UrlConfig::class,null,'url_config','url_config'); 
  }
  
  public function account(){ 
    return $this->belongsTo(\App\Account::class,'account_id'); 
  }
  
  /*--- Scope ---*/
  
  public function scopeByAccount($query , $account_id){
    return $query->where('account_id', '=', new MongoId($account_id));
  }
  
  public function scopeByAvailable($query){
    #need add deleted later ; 
    return $query->where('deleted', '=', 0 ); 
  }
  
  public function scopeByFolder($query , $folder_id){
    if($folder_id == 0 )
      return $query->where('folder_id', '=', null );
    return $query->where('folder_id', '=',new MongoId($folder_id));
  }
  
  public function scopeByType($query,$type){
    return $query->where('type','=',$type);
  }
  
  /*--- attribute ---*/ 
  public function getCanDeleteAttribute(){
    if($this->type != 'tracker')
      return true ; 
    
    return $this->tracker()->can_delete ; 
  }
  
  /*--------------------------*/
  public function toArray()
  {
    $array = parent::toArray();  
    $array['tracker'] = $this->tracker ; 
    $array['customvar'] = $this->customvar ; 
    $array['url_config'] = $this->urlConfig ; 
    return $array ; 
  }
}