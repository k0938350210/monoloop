<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Logger extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
     public function __construct($username = NULL,$message = NULL, $extenededMessage = NULL, $logableType = NULL,$logableId=NULL)
     {
         $this->username = $username;
         $this->message = $message;
         $this->extenededMessage  = $extenededMessage;
         $this->logableType  = $logableType;
         $this->logableId = $logableId;
     }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
