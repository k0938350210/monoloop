<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['pass','pwdreset_key','pwdreset_time', 'remember_token' , 'active_account' , 'accounts'];


	public function account()
  {
        return $this->belongsTo('\App\Account','active_account');
  }

	public function accounts()
  {
        // return $this->embedsMany('\App\UserAccount');
				return $this->hasMany('\App\AccountUser');
  }

	public function currentAccountConfig(){
		foreach($this->accounts  as $account){
			if($account->account_id == $this->active_account){
				return $account ;
			}
		}
		return null ;
	}

    #helper class
    public function account_for_selector(){
    	$ret = array() ;
    	foreach($this->accounts as $account){
    		// if($account->account_id == $this->active_account)
    		// 	continue ;
   			$ret[$account->account->contact_name] = array('id' => $account->account_id , 'name' => $account->account->company);
     	}
     	ksort($ret) ;
     	return $ret ;
    }

		/*------- scope ---------- */

		public function scopeSearch($query,$s){
			if(trim($s) == ''){
				return $query ;
			}
			return $query->where(function ($query) use ($s) {
				$query->where('username', 'like', '%'.$s.'%') ;
			});
		}

	  public function scopeByUsername($query, $username)
	  {
	    return $query->where('username', '=', $username);
	  }

		/*-------- attributes ----------*/

		public function getIsAdminAttribute(){
	    if($this->admin == null || $this->admin == false )
	      return false ;
	    return true ;
	  }

	  public function getUidAttribute(){
	  	return $this->account->uid ;
	  }

		public function getConfirmationURLAttribute(){
	    return config('app.ui_url').'/confirmation/'.$this->confirmation_id ;
	  }

		public function getResetURLAttribute(){
	    return config('app.ui_url').'/reset/'.$this->pwdreset_key ;
	  }

		public function getTotalAccountsAttribute(){
			return $this->accounts->count() ;
		}

    public function toArray()
    {
        $array = parent::toArray();
        $currentAccountConfig = $this->currentAccountConfig() ;
        $array['current_config'] = $currentAccountConfig;
        return $array;
    }
}
