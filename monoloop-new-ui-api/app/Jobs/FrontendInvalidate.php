<?php namespace App\Jobs;

use Artisan ;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class FrontendInvalidate extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $hash ;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($hash)
    {
        Log::info('invalidate log by umair '.$hash);
        $this->hash = $hash ;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $exitCode = Artisan::call('monoloop:invalidate', [
        'key' => $this->hash
      ]);
    }
}
