<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Artisan ;
use App\Account ;

class PrivacyDefault extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $account;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Account $account)
    {
        $this->account = $account ;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $exitCode = Artisan::call('monoloop:privacy_patch', [
        'cid' => $this->account->uid
      ]);
    }
}
