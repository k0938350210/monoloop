<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Artisan ;
use App\Account ;

class SugarLeadAndMailChimp extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $account;
    protected $source;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Account $account , $source)
    {
        $this->account = $account ;
        $this->source = $source ;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      #process mailchimp
      $exitCode = Artisan::call('monoloop:mailchimp', [
        'email' => $this->account->contact_email ,
        'fname' => $this->account->firstname ,
        'lname' => $this->account->lastname,
        'url' => $this->account->cms_site_url
      ]);
      #process sugralead
      $exitCode = Artisan::call('monoloop:sugarlead', [
        'cid' => $this->account->uid ,
        'source' => $this->source
      ]);
    }
}
