<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Artisan ;
use App\Experiment ;

class ExperimentAggregationReset extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $experiment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Experiment $experiment)
    {
        $this->experiment = $experiment ;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $exitCode = Artisan::call('monoloop:experiment-aggregation-reset', [
        'experiment_id' => $this->experiment->experimentID
      ]);
    }
}
