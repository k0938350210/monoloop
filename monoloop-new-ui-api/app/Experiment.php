<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use DB ;

// changes related to Mantis # 3974
use App\ExperimentHistory as History;
use App\ReportingArchiveAggregated;
use App\Services\Experiment\PvalueCalculator ;
use Carbon\Carbon;

class Experiment extends Eloquent {

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }

  /**
   * The database table used by the model.
   *
   * @var string
   */
    protected $collection = 'Experiments';

    public function account(){
      return $this->belongsTo(\App\Account::class,'account_id');
    }

    /*
     * get Experiment's segment
     *
     * @rerurn Object
     */

     public function segment()
     {
       return $this->belongsTo('\App\Segment','segment_id');
     }

     /*
      * get Experiment's goal
      *
      * @rerurn Object
      */

      public function goal()
      {
        return $this->belongsTo('\App\Goal','goal_id');
      }

    /*
     * The experiments associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public static function experiments($folder = NULL, $isRoot = false, $cid = NULL , $eids = []){

      $branch = [];

      if($isRoot){
        $experiments = self::where('deleted', '<>', 1)
                          ->where(function ($query) use ($folder, $cid) {
                            $query->where('folder_id', new \MongoId($folder))
                                  ->orWhere(function ($query) use ($folder, $cid) {
                                    $query->where('folder_id', null)
                                          ->where('cid', $cid);
                                  });

                        });
      } else {
        $experiments = self::byFolder($folder)->where('cid', $cid)->where('deleted', '<>', 1) ;
      }

      if(count($eids) > 0){
        $experiments->whereIn('experimentID',$eids);
      }


      $experiments = $experiments->get();

      $cur_to = Carbon::now();
      $cur_from = Carbon::now()->subWeek();

      $first_experiment = $experiments->first();
      $reports = [];
      if($first_experiment){
        $reports = ReportingArchiveAggregated::where('cid', '=', $first_experiment->cid)->where('datestamp','>',$cur_from->timestamp)->where('datestamp','<=',$cur_to->timestamp)->select('experiments')->get();
      }


        foreach ($experiments as $key => $experiment) {
        $node = new \stdClass();
        $node->hidden = $experiment->attributes['hidden'] ;
        // $node->deleted = $experiment->deleted;

        // changes rleated to Mantis # 3974
        $node->id = "experiment__".$experiment->_id;
        $node->value = $experiment->name;
        $node->type = "file";
        $node->experimentID = $experiment->experimentID;
        // Mantis #4101
        $node->priority = $experiment->priority;
        $visitors = 0;
        $conversions = 0;
        $cg_visitors = 0;
        $cg_direct = 0;

        foreach($reports as $report){
          if(isset($report->experiments[$experiment->experimentID])){
            $exp = $report->experiments[$experiment->experimentID];
            $visitors += isset($exp['visitors']) ? $exp['visitors'] : 0;
            $conversions += isset($exp['direct']) ? $exp['direct'] : 0;
            $cg_visitors += isset($exp['cg_visitors']) ? $exp['cg_visitors'] : 0;
            $cg_direct += isset($exp['cg_direct']) ?  $exp['cg_direct'] : 0;
            // $result['conversion_rate']+= ($result['visitors'] != 0) ?   $result['conversions'] / $result['visitors'] : 0;
          }
        }

        $conversion_rate = ($visitors != 0) ?   ($conversions / $visitors) * 100 : 0;
        // $cr =  $result['conversion_rate'] * 100;
        $cr = $conversion_rate;
        $cr_cg = ($cg_visitors != 0) ? ($cg_direct / $cg_visitors) * 100 : 0;
        $change = ($cr_cg != 0) ? (($cr - $cr_cg)/$cr_cg) * 100 : 0;
        $p_value = PvalueCalculator::p($visitors, $conversions, $cg_visitors, $cg_direct);

        // rounding up the final values
        $conversions = round($conversions, 2);
        $conversion_rate = round($conversion_rate, 2);
        $change = round($change, 2);

        $node->visitors = $visitors;
        $node->conversions = $conversions;
        $node->cg_visitors = $cg_visitors;
        $node->cg_direct = $cg_direct;
        $node->conversion_rate = $conversion_rate;
        $node->change = $change;
        $node->p_value = $p_value;


        array_push($branch, $node);
      }

      $res['data'] = $branch;
      $res['parent'] = 'folder__'.$folder;
      return $res;
    }

    public function scopeByAccount($query , $account_id){
      return $query->where('account_id', '=', new \MongoId($account_id));
    }

    public function scopeByCid($query, $cid){
      return $query->where('cid',(int)$cid);
    }

    public function scopeFolderExperiment($query , $folder_id){
      return $query->where('folder_id', new \MongoId($folder_id));
    }

    public function scopeByFolder($query,$folder){
      if(\MongoId::isValid($folder)){
        $query->where('folder_id', '=', new \MongoId($folder));
      }
      return $query;
    }

    public function unsetReportAggregated(){
      DB::collection('ReportingArchiveAggregated')->where('experiments.'.$this->experimentID,'exists',true)->where('cid',(int)$this->account->uid )->unset('experiments.' . $this->experimentID);
    }

    protected function getExperimentUpdatedTimestamp(){
      $timestamp = false;
      $timestamp = History::where('experimentID', (int) $this->experimentID)->max('timestamp');
      if(is_null($timestamp)){
         $experiment = self::where('experimentID',$this->experimentID)->first() ;
         $date = new Carbon($experiment['updated_at']);
         $timestamp  = $date->timestamp;
      }

      $prev = $timestamp - ($timestamp % 3600);
      return $prev;

    }
    // public function pageElementsArray(){
    //   return $this->embedsMany(\App\PageElementArray::class,'PageElements');
    // }
    // Changes related to Mantis # 3974
    protected function getReportingAttributes(){
      $result = array(
        'name' => $this->name, // experiment.name
        'visitors' => 0, // experiment.visitors
        'conversions' => 0, // experiment.dirtect
        'cg_visitors' => 0, // experiment.cg_visitors
        'cg_direct' => 0, // experiment.cg_direct
        'conversion_rate' => 0, // conversions / visitors
        'p_value' => 0,
        'change' => 0, // use reporting's improvement field
        'hidden' => $this->attributes['hidden']
      );
      $updated_timestamp = $this->getExperimentUpdatedTimestamp();
      $reports = ReportingArchiveAggregated::where('cid', '=', $this->attributes['cid'])
                        ->where('datestamp','>=',$updated_timestamp)
                        ->whereNotNull('experiments.'.$this->experimentID)
                        ->lists('experiments');
      foreach ($reports as $key=>$report) {
        if(is_array($report)){
            foreach($report as $key2 => $expRep){
              if($key2 == $this->experimentID){
                $result['visitors'] += (array_key_exists('visitors', $expRep)) ?  $expRep['visitors'] :  0;
                $result['conversions'] += (array_key_exists('direct', $expRep)) ? $expRep['direct'] :  0;
                $result['cg_visitors'] += (array_key_exists('cg_visitors', $expRep)) ? $expRep['cg_visitors'] : 0;
                $result['cg_direct'] += (array_key_exists('cg_direct', $expRep)) ?  $expRep['cg_direct'] : 0;
                // $result['conversion_rate']+= ($result['visitors'] != 0) ?   $result['conversions'] / $result['visitors'] : 0;
              }
            }

        }
      }
      $result['conversion_rate'] += ($result['visitors'] != 0) ?   ($result['conversions'] / $result['visitors']) * 100 : 0;
      // $cr =  $result['conversion_rate'] * 100;
      $cr = $result['conversion_rate'];
      $cr_cg = $result['cg_visitors'] != 0 ? ($result['cg_direct'] / $result['cg_visitors']) * 100 : 0 ;
      $result['change'] = $cr_cg != 0 ? (($cr - $cr_cg)/$cr_cg) * 100 : 0 ;
      $result['p_value'] = PvalueCalculator::p($result['visitors'], $result['conversions'], $result['cg_visitors'], $result['cg_direct']);

      // rounding up the final values
      $result['conversions'] = round($result['conversions'], 2);
      $result['conversion_rate'] = round($result['conversion_rate'], 2);
      $result['change'] = round($result['change'], 2);

      return $result;

    }
}
