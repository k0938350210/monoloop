<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use App\Services\ConditonBuilder\ConditionalContentReengineer;

// changes related to Mantis # 3974
use App\ReportingArchiveAggregated;
use Carbon\Carbon;

class Segment extends Eloquent {

  protected $fillable = ['name', 'description', 'condition' , 'condition_json' , 'hidden' , 'deleted'];

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }


  /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $collection = 'Segments';



    /*---- relation ----*/

    public function account(){
      return $this->belongsTo(\App\Account::class,'account_id');
    }
    /*
     * The Segments associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public static function segments($folder = NULL){

    	$branch = [];

      $cur_to = Carbon::now();
      $cur_from = Carbon::now()->subWeek();
      $pre_to = $cur_from;
      $pre_from = $cur_from->copy()->subWeek();
      /*
      echo $cur_to . "\n";
      echo $cur_from . "\n";
      echo $pre_to . "\n";
      echo $pre_from . "\n";
      */
      $segments = Segment::where('folder_id', '=', new \MongoId($folder))
                  ->where('deleted', 0)
                  ->where('embedded', '<>', 1)
                  ->get();


      $first_segment = $segments->first();
      if($first_segment){
        $report_curs = ReportingArchiveAggregated::where('cid', $first_segment->cid)->where('datestamp','>',$cur_from->timestamp)->where('datestamp','<=',$cur_to->timestamp)->lists('segments');
        $report_prevs = ReportingArchiveAggregated::where('cid', $first_segment->cid)->where('datestamp','>',$pre_from->timestamp)->where('datestamp','<=',$pre_to->timestamp)->lists('segments');
      }


      //$reports = ReportingArchiveAggregated::where('cid', '=', $this->attributes['cid'])



        foreach ($segments as $key => $segment) {
            $node = new \stdClass();
            // $node->id = "segment__".$segment->_id;
            // $node->value = $segment->name;
            // $node->type = "file";
            // $node->description = (strlen($segment->description) < 50 ? $segment->description : substr($segment->description, 0,47).'...');
            // $node->date = date('j F, Y', strtotime($segment->created_at));
            // $node->hidden = $segment->attributes['hidden'];
            // $node->condition = $segment->condition;
            // $node->segmentid = $segment->uid;

            // changes related to Mantis # 3974
            $node->id = "segment__".$segment->_id;
            $node->value = $segment->name;
            $node->type = "file";

            $node->hidden = $segment->attributes['hidden'];

            $node->segmentid = $segment->uid;
            /*
            $reportingCollection = $segment->getReportingAttributes();
            foreach ($reportingCollection as $key => $value) {
              $node->$key = $value;
            }
            */
            $prev = 0;
            $cur = 0;

            foreach($report_prevs as $report_prev){
              if(isset($report_prev[$segment->uid])){
                $prev += $report_prev[$segment->uid]['count'];
              }
            }

            foreach($report_curs as $report_cur){
              if(isset($report_cur[$segment->uid])){
                $cur += $report_cur[$segment->uid]['count'];
              }
            }

            $change = $prev != 0 ? ($cur - $prev)/$prev * 100 : 0;

            $node->current = round($cur, 2);
            $node->previous = round($prev, 2);
            $node->change = round($change, 2);

    		    array_push($branch, $node);
    	}

      $res['data'] = $branch;
      $res['parent'] = 'folder__'.$folder;

    	return $res;
    }

    public function getConditionJson(){

      $conditionStr = $this->condition;

      $engineerConditionBuilder = new ConditionalContentReengineer();

  		$conditionStr = $engineerConditionBuilder->prepareCondition($conditionStr) ;
  		$conditionStr = $engineerConditionBuilder->extendBraket($conditionStr) ;

  		$condition = $engineerConditionBuilder->splitAndOr($conditionStr) ;

      return $condition;
    }

    /*
     * The Segments associated with account_id
     *
     * @param $query
     * @param $account_id
     *
     * @rerurn array
     */
    public function scopeCompactList($query , $cid){
      return $query->where('cid', $cid)->where('deleted', '<>', 1 )->where('embedded', '<>', '1')->get();
    }

    public function scopeByAccount($query , $account_id){
      return $query->where('account_id', '=', new \MongoId($account_id));
    }

    public function scopeFolderSegment($query , $folder_id){
      return $query->where('folder_id', '=', new \MongoId($folder_id));
    }

    public function scopeByUid($query,$uid){
      return $query->where('uid',(int)$uid);
    }

    public function scopeByCid($query,$cid){
      return $query->where('cid',(int)$cid);
    }

    public function scopeByName($query,$name){
      return $query->where('name',$name);
    }

    public function scopeBySystem($query){
      return $query->where('system',1);
    }

    /*
    * Returns segments with embedded flag to TRUE
    */
    public function scopeEmbeddedOnly($query, $cid){
      return $query
      ->where('cid', $cid)
      ->where('deleted', '<>', 1 )
      ->where('embedded', 1)
      ->get();
    }

    public function scopeGetAll($query, $cid){
      return $query
      ->where('cid', '=', $cid)
      ->where('deleted', '<>', 1 )
      ->get();
    }

    // changes related to Mantis # 3974
    protected function getReportingAttributes(){
      $result = array(
        'current' => 0,
        'previous' => 0,
        'change' => 0
      );

      $segments = ReportingArchiveAggregated::where('cid', '=', $this->attributes['cid'])
                  ->whereNotNull('segments.'.$this->uid)
                  ->lists('segments');
      foreach($segments as $key => $seg){
          if(is_array($seg)){
              foreach($seg as $key2 => $s){
                if($key2 == $this->uid){
                  $result['current'] += isset($s['count']) ? $s['count'] : 0;
                }
                if(isset($s['from'])){
                  if(isset($s['from'][$this->uid])){
                    $result['previous'] += (int)$s['from'][$this->uid];
                  }
                }
              }
          }
      }
      $result['change'] = $result['previous'] != 0 ? ($result['current'] - $result['previous'])/$result['previous'] * 100 : 0;

      // rounding the final values
      $result['current'] = round($result['current'], 2);
      $result['previous'] = round($result['previous'], 2);
      $result['change'] = round($result['change'], 2);

      return $result;
    }
}
