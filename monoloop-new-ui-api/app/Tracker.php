<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Tracker extends Eloquent {

  /**
   * The database table used by the model.
   *
   * @var string
   */
    protected $collection = 'trackers';

    protected $hidden = ['deleted','account','account_id'];

    public function urlConfig(){
      return $this->embedsOne(\App\UrlConfig::class,null,'url_config','url_config');
    }

    /*
     * The trackers associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public static function trackers($folder = NULL, $isRoot = false, $cid = NULL){

      $branch = [];

      if($isRoot){
          $trackers = Tracker::where('deleted', 0)
            ->where(function ($query) use ($folder, $cid) {
                $query->byFolder($folder)
                ->orWhere(function($query) use ($folder, $cid){
                  $query->where('folder_id', NULL)
                    ->where('cid', '=', $cid);
                });
          })->get();
      }else{
        $trackers = Tracker::byFolder($folder)->where('deleted', 0)->where('cid', '=', $cid)->get();
      }


      foreach ($trackers as $key => $tracker) {
        $node = new \stdClass();
        $node->id = "tracker__".$tracker->_id;
        $node->value = $tracker->name;
        $node->type = "file";
        $node->tracker_type = $tracker->type;
        $node->date = date('j F, Y', strtotime($tracker->created_at));
        $node->hidden = $tracker->attributes['hidden'];
        $node->system = $tracker->system;
        $node->condition = $tracker->condition;
        array_push($branch, $node);
      }

      $res['data'] = $branch;
      $res['parent'] = 'folder__'.$folder;
      return $res;
    }

    /*---- relation ----*/

    public function account(){
      return $this->belongsTo(\App\Account::class,'account_id');
    }

    /*
     * The trckers associated with account_id
     *
     * @param $query
     * @param $account_id
     *
     * @rerurn array
     */
    public function scopeCompactList($query , $account_id){
      return $query->where('account_id', '=', new \MongoId($account_id))->where('deleted', '<>', 1 )->get();
    }

    public function scopeFolderTracker($query , $folder_id){
      return $query->where('folder_id', '=', new \MongoId($folder_id));
    }

    public function scopeByCid($query,$cid){
      return $query->where('cid',(int)$cid)->where('deleted', '<>', 1);
    }

    public function scopeByUid($query,$uid){
      return $query->where('uid',(int)$uid);
    }

    public function scopeByName($query,$name){
      return $query->where('name',$name);
    }

    public function scopeIsSystem($query){
      return $query->where('system',1);
    }


    public function scopeByActive($query){
      return $query->where('hidden',0)->where('deleted',0);
    }

    public function scopeByType($query,$type){
      if(! is_array($type)){
        return $query->where('type',$type);
      }
      return $query->whereIn('type',$type);
    }

    public function scopeByUniqueTracker($query){
      return $query->where('system',1);
    }

    public function scopeByFolder($query,$folder){
      if(\MongoId::isValid($folder)){
        $query->where('folder_id', '=', new \MongoId($folder));
      }
      return $query;
    }

    /*----- attribte --- */
    public function getMaxFieldUidAttribute(){
      $max = 0 ;
      if(is_array($this->tracker_fields)){
        foreach($this->tracker_fields as $tracker_field){
          if(isset($tracker_field['uid'])){
            if($tracker_field['uid'] > $max){
              $max = $tracker_field['uid'];
            }
          }
        }
      }
      return $max ;
    }

    /*---- CUSTOM function ---------*/
    public function getOperatorNumber($operator){
      switch ($operator) {
        case 'override':
          return 1 ;
        case 'add':
          return 2 ;
        case 'substract':
          return 3 ;
        case 'push':
          return 4 ;
        default:
          return 1 ;
      }
    }

    public function getFieldTypeNumber($field_type){
      switch ($field_type) {
        case 'number':
          return 1 ;
        case 'string':
          return 2 ;
        case 'boolean':
          return 3 ;
        default:
          return 1 ;
      }
    }

    public function getFieldByName($name){
      if(is_array($this->tracker_fields)){
        foreach($this->tracker_fields as $tracker_field){
          if($tracker_field['name'] == $name){
            return $tracker_field;
          }
        }
      }
      return null;
    }

}
