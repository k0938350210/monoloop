<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

  /**
   * The Artisan commands provided by your application.
   *
   * @var array
   */
  protected $commands = [
    'App\Console\Commands\Inspire',
    'App\Console\Commands\FrontendInvalidate',
    'App\Console\Commands\CbrCommand',
    'App\Console\Commands\CbrInvalidate',
    'App\Console\Commands\SubscribeMailChimp',
    'App\Console\Commands\SugarLead',
    'App\Console\Commands\ExperimentAggregationReset',
    'App\Console\Commands\ContentToCode' ,
    'App\Console\Commands\ContentSave',
    'App\Console\Commands\TestCommand',
    'App\Console\Commands\PrivacyPatchCommand',
    'App\Console\Commands\ReportGenRandomCommand',
    'App\Console\Commands\PrivacyCompressCommand',
    'App\Console\Commands\DbPatchMissingCidCommand',
    'App\Console\Commands\SendTestMailCommand',
    'App\Console\Commands\AccountAccTypeFreePatchCommand',
    'App\Console\Commands\BillingPatchResetCommand',
    'App\Console\Commands\BillingUpdateAccountPageViewCommand',
    'App\Console\Commands\DefaultAudienceCommand',
    'App\Console\Commands\DefaultTrackerCommand',
    'App\Console\Commands\TrackerPreconfigured' ,
    'App\Console\Commands\S3ResizingCommand' ,
    'App\Console\Commands\S3MigrateCommand',
    'App\Console\Commands\AutoPageUrlGoalCommand',
    'App\Console\Commands\PluginPatchCommand',
    'App\Console\Commands\TrackerDefaultCommand',
    'App\Console\Commands\ResetDefaultTrackerCommand',
    'App\Console\Commands\TrackerSaveCommand',
    'App\Console\Commands\TestMantisCommand',
    'App\Console\Commands\Migrate\AccountUserCleanUpCommand',
    'App\Console\Commands\Migrate\PageElementExperiencePatchCommand',
    'App\Console\Commands\AccountDefault\AccountFunnelDefault',
    'App\Console\Commands\Patch\MailChimpReupdateCommand',
    'App\Console\Commands\Patch\GeoIpUpdateCommand',
    'App\Console\Commands\Test\AuthTestCommand',
    'App\Console\Commands\Test\PValueCommand',
    'App\Console\Commands\Test\RedisPublishCommand',
    'App\Console\Commands\Test\ProfilesJsonCommand',
    'App\Console\Commands\Experience\UpdateDefaultExperiencePageElementCommand',
  ];

  /**
   * Define the application's command schedule.
   *
   * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
   * @return void
   */
  protected function schedule(Schedule $schedule)
  {
    $schedule->command('monoloop:cbr-invalidate')
         ->everyFiveMinutes();
    $schedule->command('monoloop:billing_update')
         ->everyTenMinutes();
  }

}
