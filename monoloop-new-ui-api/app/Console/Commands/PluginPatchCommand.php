<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;
use App\AccountPlugin;
use App\Plugin;
use App\PageElement;
use App\PageElementContent;
use View ;

class PluginPatchCommand extends Command
{
	protected $signature = 'monoloop:plugin_patch {cid}';
  protected $description = 'Patch Plugin';
  public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
  	$cid = $this->argument('cid');
    if($cid == 'all'){
      //Privacy need make sure that we update to all new user .
      $accounts = Account::where('cid' , '>=' , 2000)->get() ;
      foreach ($accounts as $account) {
        $this->processPlugin($account);
      }
    }else{
      $account = Account::byUid($cid)->first();
      if($account){
        $this->processPlugin($account);
      }else{
        $this->info('CID['.$cid.'] NOT exists');
      }
    }
 	}

 	public function processPlugin($account){
 		$plugins = Plugin::all();

 		foreach($plugins as $plugin){
 			$account_plugin = $account->plugins()->where('plugin_id',$plugin->id)->first();
 			if(is_null($account_plugin)){
 				$account_plugin = new AccountPlugin();
 				$account_plugin->plugin_id = $plugin->id ;
 				$account_plugin->enabled = false ;
 				$account->plugins()->save($account_plugin);
 			}
 		}
 	}
}