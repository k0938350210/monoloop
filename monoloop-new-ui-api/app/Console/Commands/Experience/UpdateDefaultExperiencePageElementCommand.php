<?php namespace App\Console\Commands\Experience;

use Illuminate\Console\Command;

use App\Account;
use App\Experiment;

use App\Services\Experiment\DefautlExperiencePageElement;

class UpdateDefaultExperiencePageElementCommand extends Command{
	protected $signature = 'monoloop:experience:default-pageelement';

	protected $description = 'Process default experience pagelement';

	public function handle(){
		$this->info('Start - Process dummy experience');

		$default_experience_pageelement = new DefautlExperiencePageElement();
		$count = Account::count();
		$this->info('Total record - '. $count);
		$page = ceil($count/20);
		$this->info('Total page - '.$page);
		for($i = 0; $i < $page; $i++){
			$accounts = Account::take(20)->skip($i*20)->orderBy('company','asc')->get();
			foreach($accounts as $account){
				if($account->uid == null){
					continue;
				}
				$this->info('Process account - '. $account->uid.' - '.$account->company);
				$default_experience_pageelement->generate($account->uid, true);
			}
		}
	}
}