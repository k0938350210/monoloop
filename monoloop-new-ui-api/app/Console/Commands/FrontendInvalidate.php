<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Frontend\Invalidator;

class FrontendInvalidate extends Command
{
  protected $signature = 'monoloop:invalidate {key}';

  protected $description = 'Invalidate frontend redis key';

  public function __construct()
  {
      parent::__construct();
  }

  public function handle()
  {
    $key = $this->argument('key');
    $this->info('Process - invalidate :' .$key );

    $invalidator = new Invalidator() ;
    $invalidator->removeCached($key , true);

    $this->info('Success - Process - invalidate :' .$key );
  }
}
