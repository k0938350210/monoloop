<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CbrBatch;

class CbrInvalidate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monoloop:cbr-invalidate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clound front invalidate with cbr_batches collection .';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->info('Process cbr invalidate');
      $invalidates = CbrBatch::all() ; 
      
      if(count($invalidates) == 0){
        $this->info('Nothing invalidate');
        return ;
      }
      
      $batches = array()  ; 
      foreach($invalidates as $invalidate){ 
        $batches[$invalidate->uid] = ['cbr' => '/' . $invalidate->uid .  '_cbr.js' , 'post' => '/' . $invalidate->uid .  '_post.js'];
        $invalidate->delete() ; 
      }
      $batch2 = [] ; 
      foreach($batches as $batch){
        $batch2[] = $batch['cbr'] ; 
        $batch2[] = $batch['post'] ; 
      } 
      
      $date = gmdate('D, d M Y G:i:s T');
      $sig = base64_encode(
          hash_hmac('sha1', $date, config('services.s3.secret'), true)
      );
      
      $s3_auth_string = config('services.s3.key').':' . $sig ;  
      $call_id = \Carbon\Carbon::now()->format('Ymdhis') ;  
      $xml = view('scripts.s3.post_invalidate')->with('s3_auth_string',$s3_auth_string)->with('date',$date)->with('batches',$batch2)->with('call_id',$call_id)->render() ; 
      
      
      $len = strlen($xml);
      $msg = "POST /2012-05-05/distribution/".config('services.s3.distribution_id')."/invalidation HTTP/1.0\r\n";
      $msg .= "Host: cloudfront.amazonaws.com\r\n";
      $msg .= "Date: {$date}\r\n";
      $msg .= "Content-Type: text/xml; charset=UTF-8\r\n";
      $msg .= "Authorization: AWS $s3_auth_string\r\n";
      $msg .= "Content-Length: {$len}\r\n\r\n";
      $msg .= $xml;

      
      $fp = fsockopen('ssl://cloudfront.amazonaws.com', 443,
        $errno, $errstr, 30
      );
      if (!$fp) {
          echo "Connection failed: {$errno} {$errstr}\n" ;   
      }else{
        fwrite($fp, $msg);
        $resp = '';
        while(! feof($fp)) {
          $resp .= fgets($fp, 1024);
        }
      } 
      fclose($fp);  
      
      $this->info('End process invalidate');
    }
}
