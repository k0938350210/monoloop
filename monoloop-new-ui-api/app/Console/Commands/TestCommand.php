<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Account\MailService;
use App\PageElement;
use App\AccountUser;
use Carbon\Carbon;

use Storage;
use Image;

class TestCommand extends Command {

  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'personal-test';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Display an inspiring quote';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->info('personal test');

    $page_element = PageElement::find('5c57e68624233624728b457b');
    $contents = $page_element->content->where('uid',237)->first();
    print_r($contents->toArray());
  }

}
