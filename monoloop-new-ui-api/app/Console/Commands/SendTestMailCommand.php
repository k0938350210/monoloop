<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User ;
use App\AccountUser ;
use App\Services\Account\MailService ;

class SendTestMailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monoloop:sendTestMail {type} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail for admin testing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $email = $this->argument('email');
      $type = $this->argument('type');

      $user = User::where('username',$email)->first() ;

      if(is_null($user)){
        $this->info('user not found');
        return ;
      }

      $account_user = AccountUser::where('account_id',(string)$user->active_account)->where('user_id',(string)$user->_id)->first() ;

      if(is_null($account_user)){
        $this->info('account user not found');
      }

      $mail_service = new MailService();
      $mail_service->prefix = 'v.2.0.7 ' ;

      # ----- Comfirm
      if($type == 'all' || $type == 'confirm'){
        $user->confirmation_id = new \MongoId();
        $user->save() ;
        $this->info('sending confirm to ' . $user->username ) ;
        $mail_service->sendComfirmationMail($account_user,$user) ;
      }



      # ---- Reset
      if($type == 'all' || $type == 'reset'){
        $user->pwdreset_key = (string)(new \MongoId()) ;
        $user->save() ;

        $this->info('sending reset link to ' . $user->username ) ;
        $mail_service->sendResetLink($email,$user) ;
      }

      # ----- invite
      if($type == 'all' || $type == 'invite'){
        $account_user->email = $email ;
        $account_user->save() ;

        $this->info('sending invitation to ' . $user->username ) ;
        $mail_service->sendInvitationMail($account_user) ;
      }
    }
}
