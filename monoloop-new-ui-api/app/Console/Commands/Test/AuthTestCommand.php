<?php  namespace App\Console\Commands\Test;

use Illuminate\Console\Command;
use App\Account;

class AuthTestCommand extends Command{
  protected $signature = 'monoloop:test-auth';

  public function handle()
  {
    //Hard code token for test only!
    $token = 'd9b119f4b0ba0a1834a9660b2af71e9_5810894b0f7bb22a428b4599_0.45083100';

    $apihost = config('app.monoloop')['apihost'];
    $url = $apihost.'/api/user/validate';

    $this->info('Process url '.$url);

    $headers = ['Content-Type: application/json', 'ML-Version: v1.0', 'ML-Token: '.$token];

    $fields_string = json_encode([]);

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count([]));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    //execute post
    $response = curl_exec($ch);
    $result = [];

    // Then, after your curl_exec call:
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $header_size);
    $result['body'] = substr($response, $header_size);

    //close connection
    curl_close($ch);

    print_r($result['body']);


  }

}