<?php  namespace App\Console\Commands\Test;

use Illuminate\Console\Command;
use App\Services\Hasher;

class ProfilesJsonCommand extends Command{
  protected $signature = 'monoloop:profile-json {cid} {total}';

  protected $description = 'Generate profile to json';

  public function handle(){
    $this->info('Profile to json');

    $cid =  $this->argument('cid');
    $total = $this->argument('total');

    $hasher = new Hasher();
    $token = $hasher->generateSign($cid);

    $json_data = [];
    $id = '5cee0db0fd423d3c24cfc51a';
    for($i = 0; $i < $total; $i++){
      $this->info($id);
      $ch = curl_init('invoke.monoloop.com/V1/profiles/'.$id);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_USERPWD, $cid . ":" . $token);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $return = curl_exec($ch);
      curl_close($ch);

      $return = json_decode($return,true);

      $json_data[] = $return['profiles'][0];
      $id = $return['profiles'][0]['pgmid'];
    }

    $json_string = json_encode($json_data, JSON_PRETTY_PRINT);

    file_put_contents('./storage/logs/profile-json.json', $json_string);
  }
}