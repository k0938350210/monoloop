<?php  namespace App\Console\Commands\Test;

use Illuminate\Console\Command;
use App\Services\Experiment\PvalueCalculator;

class PValueCommand extends Command{
  protected $signature = 'monoloop:p-value {vt} {vc} {ct} {cc}';

  protected $description = 'Compute for p-value';

  public function handle(){
  	$vt = $this->argument('vt');
  	$vc = $this->argument('vc');
  	$ct = $this->argument('ct');
  	$cc = $this->argument('cc');

  	$p_value = PvalueCalculator::p($vt,$vc,$ct,$cc);
  	$this->info('P-value is '.$p_value);
  }
}