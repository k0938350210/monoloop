<?php  namespace App\Console\Commands\Test;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class RedisPublishCommand extends Command{
  protected $signature = 'monoloop:test-redis-public {msg}';

  protected $description = 'Test redis publisher';

  public function handle(){
  	$msg = $this->argument('msg');
  	$this->info('Send publish msg : '.$msg);

		$client = \App\Services\Redis::getSentinel();
    $client->publish('invalidate', $msg);

    $client->set('test-fromnutjaa','test');
    print_r($client->get("test-fromnutjaa"));
  }
}