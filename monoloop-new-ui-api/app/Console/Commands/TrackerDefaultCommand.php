<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use App\Services\Account\DefaultTracker;

class TrackerDefaultCommand extends Command
{
	protected $signature = 'monoloop:tracker-default {cid}';

	protected $description = '';

	public function handle()
  {
  	$cid = $this->argument('cid');
		if($cid == 'all'){
      $this->processAll();
    }else{
      $this->processSingle($cid);
    }
	}

	private function processAll(){

    $accounts = Account::byAvailable()->get();
    $default_tracker = new DefaultTracker();

    foreach($accounts as $account){
    	$this->info('Update default tracker : ' . $account->uid );
    	$default_tracker->createDefault($account);
    }
  }

  private function processSingle($cid){
    $account = Account::byUid($cid)->first();
 		$this->info('Update default tracker : ' . $account->uid );
 		$default_tracker = new DefaultTracker();
 		$default_tracker->createDefault($account);
  }


}