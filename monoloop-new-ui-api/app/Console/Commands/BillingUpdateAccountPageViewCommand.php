<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;
use App\BillingHistorical ;
use App\ReportingArchiveAggregated ;
use Carbon\Carbon ;

class BillingUpdateAccountPageViewCommand extends Command
{
	protected $signature = 'monoloop:billing_update';

	public function handle()
  {
  	$accounts = Account::byAvailable()->get() ;
  	foreach($accounts as $account){
      $this->info('update billing - '.$account->uid);
  		$reports = ReportingArchiveAggregated::where('datestamp','>=',$account->last_billing_date->timestamp)->where('datestamp','<',$account->last_billing_date->addMonth()->timestamp)->where('cid',$account->uid)->get();
  	 	$total_pageviews = 0 ;
  	 	foreach($reports as $report){
  	 		$total_pageviews += $report->totalPages ;
  	 	}

  	 	$account->billing_pageviews = $total_pageviews ;
  	 	$account->save() ;

      if(Carbon::today() >= $account->last_billing_date->addMonth()){
        $this->info('add to billing historical - ' . $account->uid);

        $billingHistorical = new BillingHistorical() ;
        $billingHistorical->account_id = $account->id ;
        $billingHistorical->cid = (int)$account->uid ;
        $billingHistorical->billing_pageviews = $total_pageviews ;
        $billingHistorical->payment_type = 'pending' ;
        $billingHistorical->billing_date = $account->last_billing_date->addMonth()  ;
        $billingHistorical->save() ;

        $this->info('update new billing date');
        $account->last_billing_date = $account->last_billing_date->addMonth() ;
        $account->billing_pageviews = 0 ;
        $account->save();
      }
    }
  }

}