<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;
use App\Services\Account\DefaultAudience;

class DefaultAudienceCommand extends Command{

	protected $signature = 'monoloop:default-audience {cid}';

	protected $description = 'Create default audience by cid .';

	public function __construct()
  {
    parent::__construct();
  }

  public function handle()
	{
		$this->info('Start - Process default audience');
		$cid = $this->argument('cid');
		if($cid == 'all'){
      $this->processAll();
    }else{
      $this->processSingle($cid) ;
    }
    $this->info('End - Process default audience');
	}

	public function processSingle($cid){
		$account = Account::byUid($cid)->first();
		if(is_null($account)){
			$this->info('cid : ' . $cid . ' is not eixsts.');
			return;
		}
		$defaultAudience = new DefaultAudience();
		$defaultAudience->createDefault($account);

		$this->info('cid : ' . $cid );
	}

	public function processAll(){
		$accounts = Account::where('cid' , '>=' , 2000)->byAvailable()->get();
		foreach($accounts as $account){
      $this->processSingle($account->uid);
    }
	}


}