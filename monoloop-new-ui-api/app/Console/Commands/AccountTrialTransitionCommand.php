<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;

class AccountTrialTransitionCommand extends Command{
	/**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'monoloop:account_transition';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Transition from trial to free';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
   	$this->info('Start trial conversion process');

    return ;

    $accounts = Account::where('account_type','trial')->where('created_at','<',new \DateTime('-1 months'))->get() ;

    $this->info('Total process - ' . count($accounts) ) ;

    foreach ($accounts as $account) {
      $account->account_type = 'free' ;
      #Relate process for free account
      $account->save();
    }

   	$this->info('End trial conversion process');
  }
}