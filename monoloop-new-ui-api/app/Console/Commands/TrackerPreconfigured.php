<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Account ;
use App\Services\Trackers\PreconfiguredWpTrackers ;

class TrackerPreconfigured extends Command
{
	protected $signature = 'monoloop:tracker-wp-preconfigured {cid}';

	protected $description = '';

	public function handle()
  {
  	$cid = $this->argument('cid');
  	 if($cid == 'all'){
      $this->processAll();
    }else{
      $this->processSingle($cid) ;
    }
	}

	private function processAll(){
    // Make sure that process all cbr only for new customer.
    $accounts = Account::where('cid' , '>=' , 2000)->byAvailable()->where('source','WP Plugin')->get() ;
    $preconfigured_tracker = new PreconfiguredWpTrackers() ;
    foreach($accounts as $account){
    	$this->info('Update WP preconfigured tracker : ' . $account->uid ) ;
    	$preconfigured_tracker->updateDefault($account , []) ;
    }
  }

  private function processSingle($cid){
    $account = Account::byUid($cid)->first() ;
 		$this->info('Update WP preconfigured tracker : ' . $account->uid ) ;
 		$preconfigured_tracker = new PreconfiguredWpTrackers() ;
 		$preconfigured_tracker->updateDefault($account , []) ;
  }


}