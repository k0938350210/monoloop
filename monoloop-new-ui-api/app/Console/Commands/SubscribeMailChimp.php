<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SubscribeMailChimp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monoloop:mailchimp {email} {fname} {lname} {url?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscribe mail to monoloop lists.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $email = $this->argument('email');
      $fname = $this->argument('fname');
      $lname = $this->argument('lname');
      $url = $this->argument('url');
      $this->info('Process - mailchimp subscribe [' . $email . ']');
      $lists = explode(',',config('services.mailchimp.list_ids')) ;
      $api_key = config('services.mailchimp.api_key') ;
      $MailChimp = new \Drewm\MailChimp($api_key);
      foreach($lists as $list){
        $this->info('Subscribe - ' . $list );
        $result = $MailChimp->call('lists/subscribe', array(
          'id'                => $list,
          'email'             => array('email'=>$email),
          'merge_vars'        => array('FNAME'=>$fname, 'LNAME'=>$lname, 'URL'=>$url),
          'double_optin'      => false,
          'update_existing'   => true,
          'replace_interests' => false,
          'send_welcome'      => false,
        ));

      }


    }
}
