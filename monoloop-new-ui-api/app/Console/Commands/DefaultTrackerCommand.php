<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;
use App\Services\Account\DefaultTracker;

class DefaultTrackerCommand extends Command{

	protected $signature = 'monoloop:default-tracker {cid}';

	protected $description = 'Create default tracker by cid .';

	public function __construct()
  {
    parent::__construct();
  }

  public function handle()
	{
		$this->info('Start - Process default tracker');
		$cid = $this->argument('cid');
		if($cid == 'all'){
      $this->processAll();
    }else{
      $this->processSingle($cid) ;
    }
    $this->info('End - Process default tracker');
	}

	public function processSingle($cid){
		$account = Account::byUid($cid)->first() ;
		$default_tracker = new DefaultTracker();
		$default_tracker->createDefault($account);

		$this->info('cid : ' . $cid );
	}

	private function processAll(){
    // Make sure that process all cbr only for new customer.
    $accounts = Account::byAvailable()->get() ;
   	foreach($accounts as $account){
   		$this->processSingle($account->uid) ;
   	}
  }


}