<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

use Storage;
use Auth ;
use File ;
use Image ;
use App\Services\Media;

class S3ResizingCommand extends Command
{
	protected $signature = 's3:resizing {working_dir} {filename}';
	protected $description = 'resizing image file and upload to s3';

	public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
  	$working_dir = $this->argument('working_dir');
    $filename = $this->argument('filename');
  	$s3_file = config('filesystems.disks.s3.public_url') . $working_dir . '/' . $filename  ;



    if(!Storage::exists($working_dir . '/' . $filename)){
      //Not exists so do nothing;
      return ;
    }

  	$attachment = array(
      'thumb' => array('w' => 130 , 'h' => 130 ) ,
      'medium' => array('w' => 300 , 'h' => 300 ) ,
      'large' => array('w' => 600 , 'h' => 600 ) ,
    ) ;

    $media = new Media();


    foreach($attachment as $key => $attach){

      $image2 = Image::make($media->getReadablePath($s3_file))->widen($attach['w']);
      Storage::put(  $working_dir . '/' . $attach['w'] . '/' . $filename ,(string)$image2->encode(), 'public');

    }

    //also convert to webp
    // $info = pathinfo($filename);
    // $filename = $info['filename'] . '.webp';
		//
    // $image2 = Image::make($image->basePath());
    // $image2->save(tempnam(sys_get_temp_dir(), 'Tux2').'.webp');
    // Storage::put(  $working_dir .  '/webp/' . $filename ,file_get_contents($image2->basePath()), 'public');
  }
}
