<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\MantisWs ;
use App\PageElement;
use App\AccountUser;
use Carbon\Carbon;

use Storage;
use Image;

class TestMantisCommand extends Command {
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'mantis-test';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Display an inspiring quote';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $mantis = new MantisWs();
    print_r($mantis->getFilterOnProject());
  }
}