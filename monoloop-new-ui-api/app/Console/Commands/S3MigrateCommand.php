<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

use Storage;
use Auth ;
use File ;
use Image ;

class S3MigrateCommand extends Command
{
	protected $signature = 's3:migrate {cid}';
	protected $description = 'migrate image format (ex. thumbs , resizing )';

	public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
  	$cid = $this->argument('cid');
  	$all_files = Storage::allFiles('assets/'.$cid);

  	foreach($all_files as $all_file){
  		$this->info("Process - " . $all_file);
			$files = explode('/',$all_file);
			$filename = end($files);

			if(in_array('thumbs',$files) || in_array('130',$files) || in_array('300',$files) || in_array('600',$files) ){
				$this->info('ignore as preserve folder');
				continue ;
			}

			array_pop($files);
			$working_dir = implode('/',$files) ;


			$s3_file = config('filesystems.disks.s3.public_url') . $working_dir . '/' . $filename ;
			$storePath = $working_dir . '/thumbs/' . $filename ;

			$image = Image::make($s3_file);
    	$image->save(tempnam(sys_get_temp_dir(), 'Tux'));
      // create thumb image
      $image2 =  Image::make($image->basePath())->fit(200, 200)->save(tempnam(sys_get_temp_dir(), 'Tux2'));

      Storage::disk('s3')->put(
        $storePath,
        file_get_contents($image2->basePath())
      );
      Storage::disk('s3')->setVisibility($storePath, 'public');

 			$attachment = array(
	      'thumb' => array('w' => 130 , 'h' => 130 ) ,
	      'medium' => array('w' => 300 , 'h' => 300 ) ,
	      'large' => array('w' => 600 , 'h' => 600 ) ,
	    ) ;

	    foreach($attachment as $key => $attach){

	      $image2 = Image::make($image->basePath())->widen($attach['w']);
	      $image2->save(tempnam(sys_get_temp_dir(), 'Tux2'));
	      Storage::put(  $working_dir . '/' . $attach['w'] . '/' . $filename ,file_get_contents($image2->basePath()), 'public');

	    }
  	}
  }
}
