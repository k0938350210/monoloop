<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;
use App\Folder;
use App\PageElement ;
use App\Services\Goal\AutoPageUrlGoal ;

class AutoPageUrlGoalCommand extends Command
{
	protected $signature = 'monoloop:goal_url {cid}';

	public function handle()
  {
    $cid = $this->argument('cid');
    if($cid === 'all'){
    	$this->processAll();
    }else{
    	$this->processSingle($cid) ;
    }
  }

  public function processAll(){
  	$accounts = Account::byAvailable()->get() ;
  	foreach($accounts as $account){
     	$this->processSingle($account->uid);
    }
  }

  public function processSingle($cid){
  	$account = Account::byUid($cid)->first() ;
  	$folder = Folder::root($account->id)->first();
  	$autoPageUrlGoal = new AutoPageUrlGoal() ;
  	$placements = PageElement::byAccountCid($cid)->byAvailable()->where('fullURL','<>','default')->get();
		foreach ($placements as $placement) {
		 	$autoPageUrlGoal->url($account , $placement);
		}
  }
}