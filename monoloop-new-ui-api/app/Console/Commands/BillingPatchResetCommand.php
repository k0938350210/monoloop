<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;
use Carbon\Carbon ;

class BillingPatchResetCommand extends Command
{
	protected $signature = 'monoloop:billing_reset {cid}';

	public function handle()
  {
      $cid = $this->argument('cid');
      if($cid == 'all'){
        $this->processAll();
      }else{
        $this->processSingle($cid) ;
      }
  }

  public function processAll(){
  	$accounts = Account::byAvailable()->get() ;
  	foreach($accounts as $account){
     	$this->processSingle($account->uid);
    }
  }

  public function processSingle($cid){
  	$account = Account::byUid($cid)->first() ;

  	$this->info('Reset billing cid : ' .$cid );

  	if(! $account){
  		$this->error('Account not found');
  	}

  	if($account->AccType == 'Paid'){
  		$account->max_pageviews = config('services.billing.page_views.paid');
  	}else{
  		$account->max_pageviews = config('services.billing.page_views.free');
  	}

  	$account->billing_pageviews = 0  ;

  	$account->last_billing_date = $account->created_at->next()->previous();
  	$account->save() ;
  }

}