<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Tracker;

class TrackerSaveCommand extends Command
{
  protected $signature = 'tracker:save {uid}';

  protected $description = 'save tracker -- test observer';

  public function __construct()
  {
      parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      $uid = $this->argument('uid');
      $tracker = Tracker::byUid($uid)->first();
      if($tracker){
        $tracker->code = 'update';
        $tracker->save();
        $this->info($tracker->code);
        $this->info('Success update tracker : '.$uid);
      }else{
        $this->info('No tracker to save');
      }
  }
}
