<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;

class SugarLead extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monoloop:sugarlead {cid} {source}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create sugar lead with mononoloop account cid.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $lead_source = 'MonoloopAccountSignup' ;

      $cid = $this->argument('cid');
      $source = $this->argument('source');
      $this->info('Process - sugar lead [cid-' . $cid . ']');
      $account = Account::byUid($cid)->first() ;
      if(empty($account)){
        $this->error('Account not exists!');
        return;
      }

      $url  = config('services.sugar_crm.url');
      $client = new \nusoap_client($url,'wsdl','','','','');
      $user_auth = array(
    		'user_auth' => array(
        'user_name' => config('services.sugar_crm.user'),
        'password' => md5(config('services.sugar_crm.pass')),
        'version' => '0.1'),
        'application_name' => 'Talefod'
    		);

   		$result = $client->call('login',$user_auth);
      if($result['id'] == -1){
        $this->error('Invalid sugar crm user/password!');
        return;
      }

      /*
      $result_contact = $client->call('get_module_fields',['session' => $result['id'] , 'module_name' => 'Emails']);
       foreach($result_contact['module_fields'] as $field){
        $this->info($field['name']);
       }
      */

      $get_entry_params = array(
        'session' => $result['id'],
        'module_name' => 'Contacts',
        'select_fields' => array(
          'id'
        ),
        'query' => 'contacts.email1=\''.$account->contact_email.'\'',
        'offset' => '0',
        'max_results' => '1',
        'deleted' => '0'
     	);

		  //$result_contact = $client->call('get_entry_list',$get_entry_params);

      $set_entry_params = array(
        'session' => $result['id'],
        'module_name' => 'Contacts',
        'name_value_list'=>array(
          array('name'=>'first_name','value'=>$account->firstname),
          array('name'=>'last_name','value'=>$account->lastname),
          array('name'=>'email1','value'=>$account->contact_email),
          array('name'=>'lead_source','value'=>utf8_encode($lead_source )),
          array('name'=>'account_name','value'=>$account->company)
		    )
      );
			$result3 = $client->call('set_entry',$set_entry_params);
			$contact_id = $result3['id'];

      $get_entry_params = array(
        'session' => $result['id'],
        'module_name' => 'Accounts',
        'select_fields' => array(
				  'id'
				),
        'query' => 'accounts.name=\''.$account->company.'\'',
        'offset' => '0',
        'max_results' => '1',
        'deleted' => '0'
      );

		  $result2 = $client->call('get_entry_list',$get_entry_params);
      if (($result2['result_count']>0)) {
  			//Account exists
  			$account_id = $result2['entry_list'][0]['id'];
  		}
  		else {
  			//Account does not exist - we create it
  			$set_entry_params = array(
          'session' => $result['id'],
          'module_name' => 'Accounts',
          'name_value_list'=>array(
     	      array('name'=>'name','value'=>$account->company),
  					array('name'=>'website','value'=>'')
     	    )
		    );
 				$res_account = $client->call('set_entry',$set_entry_params);
  			$account_id = $res_account['id'];
  		}

      $set_relationship_params = array(
        'session' => $result['id'],
        'set_relationship_value' => array(
        'module1'=> 'Contacts',
        'module1_id'=> $contact_id,
        'module2' => 'Accounts',
        'module2_id'=> $account_id,
			)
      	);
		  $result4 = $client->call('set_relationship',$set_relationship_params);

      $data = file_get_contents('http://ip-api.com/json/'.$account->signup_ip);

      $country = '' ;
      $city = '' ;

      $data = json_decode($data,true);
      if($data['status'] == 'success'){
        $country = $data['country'] ;
        $city = $data['city'] ;
      }

      // Now create the Lead
		  $set_lead_params = array(
        'session' => $result['id'],
        'module_name' => 'Leads',
        'name_value_list'=>array(
          array('name'=>'first_name','value'=>$account->contact_name),
          #array('name'=>'last_name','value'=>$account->lastname),
         	array('name'=>'email1','value'=>$account->contact_email),
          array('name'=>'lead_source','value'=>utf8_encode($lead_source )),
          array('name'=>'lead_source_description','value'=>utf8_encode( $source )),
          array('name'=>'primary_address_city','value'=>utf8_encode($city)),
          array('name'=>'primary_address_country','value'=>utf8_encode( $country )),
				  //array('name'=>'title','value'=>$recordArray['title']),
				  array('name'=>'account_id','value'=>$account_id),
          array('name'=>'contact_id','value'=>$contact_id),
				  array('name'=>'website','value'=>'')
        )
     	);
		  $res_lead = $client->call('set_entry',$set_lead_params);
		  $lead_id = $res_lead['id'];

      $set_note_params = array(
	   		'session' => $result['id'],
        'module_name' => 'Notes',
        'name_value_list'=>array(
          array('name'=>'name','value'=>utf8_encode($lead_source )),
          array('name'=>'description','value'=>utf8_encode('Signed up for a Monoloop Account')),
  				array('name'=>'parent_type','value'=>'Accounts'),
          array('name'=>'parent_id','value'=>$account_id),
          array('name'=>'contact_id','value'=>$contact_id)
  			)
  		);
  		$res_note = $client->call('set_entry',$set_note_params);
  		$note_id = $res_note['id'];

      $this->info('Success - sugar lead [cid-' . $cid . ']');
    }
}
