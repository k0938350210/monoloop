<?php namespace App\Console\Commands\AccountDefault;

use Illuminate\Console\Command;
use App\Account;
use App\Services\Account\DefaultFunnel;

class AccountFunnelDefault extends Command{

  protected $signature = 'monoloop:default-funnel {cid}';

  protected $description = 'Create default funnel by cid .';

  public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
    $this->info('Start - Process default funnel');
    $cid = $this->argument('cid');
    if($cid == 'all'){
      $this->processAll();
    }else{
      $this->processSingle($cid) ;
    }
    $this->info('End - Process default funnel');
  }

  public function processSingle($cid){
    $account = Account::byUid($cid)->first();
    if(is_null($account)){
      $this->info('cid : ' . $cid . ' is not eixsts.');
      return;
    }

    $default_funnel = new DefaultFunnel();
    $default_funnel->createDefault($account);

    $this->info('cid : ' . $cid );
  }

  public function processAll(){
    $accounts = Account::where('cid' , '>=' , 2000)->byAvailable()->get();
    foreach($accounts as $account){
      $this->processSingle($account->uid);
    }
  }


}