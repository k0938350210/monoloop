<?php namespace App\Console\Commands\Migrate;

use Illuminate\Console\Command;
use App\Account;
use App\AccountUser;

class AccountUserCleanUpCommand extends Command{
  protected $signature = 'account-user:cleanup';

  protected $description = 'Clean up account user data';

  public function handle()
  {
    $this->info('Clean up data');

    $account_users = AccountUser::all();
    foreach($account_users as $account_user){
      $account_user->role = 'admin';
      $account_user->account_id = (string)$account_user->account_id;
      $account_user->user_id = (string)$account_user->user_id;

      if($account_user->cid && $account_user->account_id == ''){
        $account_user->account_id = (string)Account::where('uid',$account_user->cid)->first()->id;
      }

      $account_user->save();
      $this->info('success process : '. $account_user->email);
    }
  }
}