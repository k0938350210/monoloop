<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;
use App\UrlConfig;
use App\PageElement;
use App\PageElementContent;
use View ;

class PrivacyPatchCommand extends Command
{
  protected $signature = 'monoloop:privacy_patch {cid}';
  protected $description = 'Patch Privacy';
  public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
    $this->info('Start privacy center patching');
    $cid = $this->argument('cid');
    if($cid == 'all'){
      //Privacy need make sure that we update to all new user .
      $accounts = Account::where('cid' , '>=' , 2000)->get() ;
      foreach ($accounts as $account) {
        $this->processPrivacy($account);
      }
    }else{
      $account = Account::byUid($cid)->first();
      if($account){
        $this->processPrivacy($account);
      }else{
        $this->info('CID['.$cid.'] NOT exists');
      }
    }


    $this->info('Finish privacy center patching');
  }

  private function processPrivacy($account){
    $this->info('--- privacy account ' . $account->uid);
    if($account->AccType ==  'Paid'){

      $pageElement = PageElement::where('fullURL' , 'default')->where('cid',$account->uid)->first();
      if($pageElement){
        $pageElementContent = $pageElement->content()->where('name', 'privacy')->where('uid', NULL)->first();
        if($pageElementContent){
          if($account->AccType === 'Paid'){
            $pageElementContent->delete();
          }
        }
        $pageElement->save();
      }
      $this->info('Paid account make sure we remove privacy content');
      return ;
    }

    $view = View::make('page.content.elements.privacy-center', ['preview' => 0]);
    $privacy_code = $view->render();

    $pageElement = PageElement::where('fullURL' , 'default')->where('cid',$account->uid)->first();
    if(is_null($pageElement)){
      $pageElement = new PageElement();
    }

    $pageElement->cid = $account->uid;
    $pageElement->hidden = 0;
    $pageElement->deleted = 0;
    $pageElement->pageID = 0;
    $pageElement->fullURL = 'default';
    $pageElement->regExp = '';
    $pageElement->exactQS = '';
    $pageElement->startsWith = true;
    $pageElement->urlOption = 0;
    $pageElement->inWWW = true;
    $pageElement->exWWW = true;
    $pageElement->inHTTP_HTTPS = false;
    $pageElement->originalUrl = 'default';
    $pageElement->additionalJS = '';
    $pageElement->folder_id = 'none';
    $pageElement->remark = '';
    $pageElement->save();



    $pageElementContent = $pageElement->content()->where('name' , 'privacy')->where('uid' , NULL)->first();
    if(!$pageElementContent){
      $pageElementContent = new PageElementContent();
    }
    $pageElementContent->name = "privacy";
    $pageElementContent->connectors = [];

    $pageElementContent->uid = null;
    $pageElementContent->mappedTo = 'body';
    $pageElementContent->placementType = "3";
    $pageElementContent->order = 1;
    $pageElementContent->displayType = "1";
    $pageElementContent->addJS = "";
    $pageElementContent->startDate = null;
    $pageElementContent->endDate = null;
    $pageElementContent->content_type = '';
    $pageElementContent->code = $privacy_code;
    $pageElement->content()->save($pageElementContent);
  }
}