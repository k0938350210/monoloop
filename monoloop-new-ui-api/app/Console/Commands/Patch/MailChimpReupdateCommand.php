<?php  namespace App\Console\Commands\Patch;

use Illuminate\Console\Command;
use App\Account;

class MailChimpReupdateCommand extends Command{
  protected $signature = 'monoloop:mailchimp-reupdate';

  public function handle()
  {
    $lists = explode(',',config('services.mailchimp.list_ids')) ;
    $api_key = config('services.mailchimp.api_key') ;
    $MailChimp = new \Drewm\MailChimp($api_key);

    $accounts = Account::where('cid' , '>=' , 2000)->get() ;
    $this->info('Process reupdate mailchimp start - total ' . count($accounts));

    foreach($accounts as $account){
      $this->info('Process cid : ' . $account->uid);
      foreach($lists as $list){
        $this->info('Subscribe - ' . $list );
        $result = $MailChimp->call('lists/subscribe', array(
          'id'                => $list,
          'email'             => array('email'=>$account->contact_email),
          'merge_vars'        => array('FNAME'=>$account->firstname, 'LNAME'=>$account->lastname, 'URL'=>$account->cms_site_url),
          'double_optin'      => false,
          'update_existing'   => true,
          'replace_interests' => false,
          'send_welcome'      => false,
        ));
      }
    }


  }

}