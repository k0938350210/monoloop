<?php  namespace App\Console\Commands\Patch;

use Illuminate\Console\Command;
use App\City;
use App\Country;

use Storage;

class GeoIpUpdateCommand extends Command{
  protected $signature = 'monoloop:geoip-update';

  public function handle()
  {
    $this->info('Start geo ip update');
    //$this->info(Storage::get('app/city-en.csv'));
    $storage_path  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
    $this->info('Start process country');
    Country::truncate();
    $row = 0;
    if (($handle = fopen($storage_path."data/city-en.csv", "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        if($row === 1){
          continue;
        }
        if(!Country::where('name',$data['5'])->exists() && trim($data['5']) !== ''){
          $country = new Country();
          $country->code = strtolower($data[4]);
          $country->name = $data[5];
          $country->save();

          $this->info('Process : ' . $country->name);
        }
      }
      fclose($handle);
    }

    $this->info('Start process city');
    City::truncate();
    $row = 0;
    if (($handle = fopen($storage_path."data/city-en.csv", "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        if($row === 1){
          continue;
        }
        if(trim($data['5']) !== '' && trim($data['10']) !== '' && !City::where('name',$data[5].'/'.$data[10])->exists()){
          $city = new City();
          $city->name = $data[5].'/'.$data[10];
          $city->save();

          $this->info('Process : ' . $city->name);
        }
      }
      fclose($handle);
    }
  }
}