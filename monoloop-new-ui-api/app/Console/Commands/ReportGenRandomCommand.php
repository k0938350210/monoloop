<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;

use App\ReportingArchiveAggregated ;
use App\Account ;
use App\Goal ;
use App\Segment ;
use App\Experiment ;

class ReportGenRandomCommand extends Command
{

	/**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'dashboard:random {cid} {day}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'generate random data.';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
      parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
  	$cid = $this->argument('cid');
    $this->info('Process - dashboard random data cid : ' . $cid );
    $account = Account::byUid($cid)->first() ;
    if(empty($account)){
      $this->error('Account not exists!');
      return;
    }

    $day = $this->argument('day');

    $goallists = Goal::byAccount($account->id)->get() ;
    $segmentlists = Segment::byAccount($account->id)->get() ;
    $experimentlists = Experiment::byAccount($account->id)->get() ;



    #Generate random data.
  	for( $i = -5 ; $i < intval($day) ; $i++){
      for($j = 0 ; $j < 24 ; $j++ ){
    		$timestamp = gmmktime($j, 0, 0, date("m"), date("d") - $i , date("y"));
    		$report = ReportingArchiveAggregated::where('datestamp',$timestamp)->first() ;
    		if(is_null($report)){
    			$report = new ReportingArchiveAggregated();
    			$report->datestamp = $timestamp ;
    		}

    		$report->cid =  intval($cid) ;
    		$report->totalVisits = rand(200,300);
        $report->totalVisitors = rand(100,200);
    		$report->totalPages = rand(500,600);

    		$scores = [] ;
    		$scores['engagement'] = array('totalScore' => rand(10000,30000) , 'trends' => array('inc' => rand(1,20) , 'dec' => rand(1,20) , 'quo' => rand(1,20) ) , 'levels' => array('1' => rand(5,50) , '2' => rand(5,50) , '3' => rand(5,50) , '4' => rand(5,50) , '5' => rand(5,50))) ;

    		$scores['brand'] = array('totalScore' => rand(10000,30000) , 'trends' => array('inc' => rand(1,20) , 'dec' => rand(1,20) , 'quo' => rand(1,20) ) , 'levels' => array('1' => rand(5,50) , '2' => rand(5,50) , '3' => rand(5,50) , '4' => rand(5,50) , '5' => rand(5,50))) ;

    		$scores['recency'] = array('totalScore' => rand(10000,30000) , 'trends' => array('inc' => rand(1,20) , 'dec' => rand(1,20) , 'quo' => rand(1,20) ) , 'levels' => array('1' => rand(5,50) , '2' => rand(5,50) , '3' => rand(5,50) , '4' => rand(5,50) , '5' => rand(5,50))) ;

    		$scores['duration'] = array('totalScore' => rand(10000,30000) , 'trends' => array('inc' => rand(1,20) , 'dec' => rand(1,20) , 'quo' => rand(1,20) ) , 'levels' => array('1' => rand(5,50) , '2' => rand(5,50) , '3' => rand(5,50) , '4' => rand(5,50) , '5' => rand(5,50))) ;

    		$report->scores = $scores ;

    		if( $i > 28){
          $report->recencyPages = rand(100,200) ;
          $report->recencyVisits = rand(100,200) ;
          $report->recencyDuration = rand(100,200) ;
          $report->recencyTime = rand(100,200) ;

        }

    		$goals = [] ;
    		foreach ($goallists as $goal) {
    			$goals[$goal->uid]  = array('amount' => rand(0,22) , 'VisitsBeforeGoal' => rand(0,22)  , 'pagesBeforeGoal' => rand(0,25) ) ;
    		}
    		$report->goals = $goals ;

    		$segments = [] ;

    		foreach($segmentlists as $segment){
        	$segments[$segment->uid] = array('count' => rand(100,200) , 'from' => array()) ;

        	foreach($segmentlists as $segment2){
        		if( $segment2->uid != $segment->uid ){
        			$segments[$segment->uid]['from'][$segment2->uid] = rand(50,100) ;
        		}
       		}
        }
        $report->segments = $segments ;

        $experiments = [] ;
        foreach($experimentlists as $experiment){
        	$experiments[$experiment->experimentID] = [
        		'direct' => rand(50,200) ,
  					'visitors' => rand(300,400) ,
  					'views' => rand(300,400) ,

  					'cg_direct' => rand(50,200) ,
  					'cg_visitors' => rand(50,200) ,
  					'cg_views' => rand(50,200) ,

  					'assisted' => rand(50,200) ,
  					'a_visitors' => rand(300,400) ,
  					'a_views' => rand(300,400) ,

  					'cg_assisted' => rand(50,200) ,
  					'cg_a_visitors' => rand(300,400) ,
  					'cg_a_views' => rand(300,400)
        	];
        }
        $report->experiments = $experiments ;
    		$report->save() ;
      }
  	}

  	$this->info('Finish - dashboard random data cid : ' . $cid );

  }

}