<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Funnel extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'funnels';

	public static function funnels($folder = NULL, $isRoot = false, $account_id = NULL){

    $branch = [];

    if($isRoot){
        $funnels = Funnel::where('deleted', 0)
          ->where(function ($query) use ($folder, $account_id) {
              $query->where('folder_id', new \MongoId($folder))
              ->orWhere(function($query) use ($folder, $account_id){
                $query->where('folder_id', NULL)
                  ->where('account_id', $account_id);
              });
        })->get();
    }else{
      $funnels = Funnel::where('folder_id', new \MongoId($folder))->where('deleted', 0)->get();
    }


    foreach ($funnels as $key => $funnel) {
      $node = new \stdClass();
      $node->id = "funnel__".$funnel->_id;
      $node->value = $funnel->name;
      $node->type = "file";
      $node->description = $funnel->description;
      $node->date = date('j F, Y', strtotime($funnel->created_at));
      $node->hidden = $funnel->attributes['hidden'];
      array_push($branch, $node);
    }

    $res['data'] = $branch;
    $res['parent'] = 'folder__'.$folder;
    return $res;
  }

  public function scopeByCid($query,$cid){
    return $query->where('cid',(int)$cid);
  }

  public function scopeByName($query,$name){
    return $query->where('name',$name);
  }
}
