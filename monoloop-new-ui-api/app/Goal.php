<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use App\Services\MonoloopCondition;
use App\ReportingArchiveAggregated;
use Carbon\Carbon;

class Goal extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'goals';
    protected $fillable = ['name', 'point', 'type' , 'condition' , 'condition_json' , 'hidden' , 'deleted' ];
    /**
     * Options for goal types.
     *
     * @var array
     */
    public static $types = array('page_related', 'page_related_with_condition', 'condition_related');

    /**
     * Url Config
     */
    public function urlConfig(){
      return $this->embedsOne(\App\UrlConfig::class,null,'url_config','url_config');
    }

    public function pageElement(){
      return $this->belongsTo(\App\PageElement::class,'page_element_id');
    }

    public function account(){
      return $this->belongsTo(\App\Account::class,'account_id');
    }

    /*
     * The experiments associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public static function goals($folder = null, $isRoot = false, $cid = null, $pids= [])
    {
        $branch = [];
        if ($isRoot) {
            $goals = self::where('deleted', '<>', 1)
                                                ->where(function ($query) use ($folder, $cid) {
                                                    $query->byFolder($folder)
                                                          ->orWhere(function ($query) use ($folder, $cid) {
                                                            $query->where('folder_id', '=', null)
                                                                  ->where('cid', '=', $cid);
                                                          })->where('embedded', '<>', 1);
                                              });
        } else {
            $goals = self::byFolder($folder)
            ->where('deleted', '<>', 1)
            ->where('cid', '=', $cid)
            -> where('embedded', '<>', 1);
        }

        if(count($pids) > 0){
            $goals = $goals->whereIn('page_element_id', $pids);
        }
        $goals = $goals->get();

        $cur_to = Carbon::now();
        $cur_from = Carbon::now()->subWeek();

        $first_goal = $goals->first();
        $reports = [];
        if($first_goal){
          $reports = ReportingArchiveAggregated::where('cid', $first_goal->cid)->where('datestamp','>',$cur_from->timestamp)->where('datestamp','<=',$cur_to->timestamp)->select('goals','totalVisitors')->get();
        }

        foreach ($goals as $key => $goal) {
            $node = new \stdClass();
            // $node->id = 'goal__'.$goal->_id;
            // $node->value = $goal->name;
            // $node->type = 'file';
            // $node->goal_type = $goal->type;
            //
            // $node->url_config = $goal->urlConfig;
            // if(is_object($goal->urlConfig)){
            //   $goal->urlConfig->url_option = $goal->urlConfig->getOptionStringAttribute($goal->urlConfig->url_option);
            // }
            // $node->point = $goal->point;
            // $node->date = date('j F, Y', strtotime($goal->created_at));
            $node->hidden = $goal->attributes['hidden'] ;
            // $node->deleted = $goal->deleted;
            // $node->condition = $goal->condition;

            // changes related to Mantis # 3974
            $node->id = 'goal__'.$goal->_id;
            $node->value = $goal->name;
            $node->type = 'file';
            $node->url_config = $goal->urlConfig;
            if(is_object($goal->urlConfig)){
              $goal->urlConfig->url_option = $goal->urlConfig->getOptionStringAttribute($goal->urlConfig->url_option);
            }

            $conversions = 0;
            $conversion_rate = 0;
            $total_visitors = 0;
            foreach($reports as $report){
              if(isset($report->goals[$goal->uid])){
                $conversions += $report->goals[$goal->uid]['amount'];
                $total_visitors += $report->totalVisitors;
              }
            }

            $node->conversions = $conversions;
            $node->total_visitors = $total_visitors;
            $node->conversion_rate = round(($total_visitors != 0 ? ($conversions / $total_visitors) * 100 : 0) , 2);
            /*
            $reportingCollection = $goal->getReportingAttributes();
            foreach ($reportingCollection as $key => $value) {
              $node->$key = $value;
            }
            */

            array_push($branch, $node);
        }

        $res['data'] = $branch;
        $res['parent'] = 'folder__'.$folder;
        return $res;
    }

    /*---- Attribute ---*/
    public function getCodeAttribute(){
      if($this->condition != ''){
        $condition = new MonoloopCondition($this->condition) ;
        $condition->replacePipe(' goals.push({ \'uid\' : '.$this->uid.',\'p\' : '.$this->point.' });');
        return  ' <% ' . $condition . ' %>' ;
      }else{
        return ' <%  ' . 'goals.push({ \'uid\' : '.$this->uid.',\'p\' : '.$this->point.' });'. ' %>'  ;
      }
    }

    /*
     * The Goals associated with account_id
     *
     * @param $query
     * @param $account_id
     *
     * @rerurn array
     */
    public function scopeCompactList($query , $cid){
      return $query->where('cid', '=', $cid)->where('deleted', '<>', 1 )->where('embedded', '<>', '1')->get();
    }

    public function scopeByAccount($query , $account_id){
      return $query->where('account_id', new \MongoId($account_id));
    }

    public function scopeFolderGoal($query , $folder_id){
      return $query->where('folder_id', new \MongoId($folder_id));
    }

    public function scopeEmbeddedOnly($query, $cid){
      return $query
      ->where('cid', $cid)
      ->where('deleted', '<>', 1 )
      ->where('embedded', 1)
      ->get();
    }

    public function scopeByName($query,$name){
      return $query->where('name',$name);
    }

    public function scopeGetAll($query, $cid){
      return $query
      ->where('cid', $cid)
      ->where('deleted', '<>', 1 )
      ->get();
    }

    public function scopeByFolder($query,$folder){
      if(\MongoId::isValid($folder)){
        $query->where('folder_id', new \MongoId($folder));
      }
      return $query;
    }

    // changes related to Mantis # 3974
    protected function getReportingAttributes(){
      $report = array(
        'conversions' => 0,
        'conversion_rate' => 0,
        'total_visitors' => 0,
        'hidden' => $this->attributes['hidden']
      );
      $result = ReportingArchiveAggregated::where('cid', $this->cid)
                ->whereNotNull('goals.'.$this->uid)
                ->get();
      if(!empty($result)){
        foreach($result as $it){
          $_goals = $it->goals[$this->uid];
          if(!empty($_goals)){
            $report['conversions'] += (array_key_exists('amount', $_goals)) ?  $_goals['amount'] :  0;
            $report['total_visitors'] += (empty($it->totalVisitors) ? 0 : $it->totalVisitors);
          }
        }
      }
      $report['conversion_rate'] = round(($report['total_visitors'] != 0 ? ($report['conversions'] / $report['total_visitors']) * 100 : 0) , 2);
      return $report;
    }
}
