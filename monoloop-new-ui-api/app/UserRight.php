<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class UserRight extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'user_rights';


	/**
	 * the right category ID used by the user rights
	 * @return void
	 */
	public function category()
  {
    return $this->belongsTo('\App\UserRightCategory','user_right_category_id');
  }
}
