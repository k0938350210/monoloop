<?php namespace App\Services\Redis;


class CustomReplicationStrategy extends \Predis\Replication\ReplicationStrategy
{
  protected function getReadOnlyOperations()
  {
    return [];
  }
}
