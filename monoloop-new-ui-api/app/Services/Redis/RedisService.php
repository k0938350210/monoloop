<?php

namespace App\Services\Redis;

use Redis;
use Session;

/**
 *
 */
class RedisService
{
  const UserState = "UserState";
	const KeyValueSeperator = ":";

  function __construct()
  {
    $this->redis = \App\Services\Redis::getSentinel();
  }

  public static function getKey($id, $preText = ''){
    if(empty($preText)){
      $preText = self::UserState;
    }
    return $preText.self::KeyValueSeperator.$id;
  }

  function set($key, $value, $type = "default"){
    $this->redis->set($key, $value);
    return;
  }

  function get($key, $type = "default"){
    $value = $this->redis->get($key);
    return $value;
  }

  public static function intendedRedirect($id){

    $key = self::getKey($id);

    $redisService = new RedisService();

    $state = $redisService->get($key);
    $state = trim($state);

    if(empty($state)){
      if(RedisService::isSecure()){
        return redirect()->secure('/');
      }else{
        return redirect('/');
      }
    }

    Session::set('UserStateData', $state);
    $state = json_decode($state);

    $url = $state->page;
    $parsed_url = parse_url($url);

    // parse url and prepare url without schema
    // incase user has http URL saved in cache it should redirect to https
    $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
    $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
    $pass     = ($user || $pass) ? "$pass@" : '';
    $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
    $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';

    if(RedisService::isSecure()){
      $url = "https://"."$user$pass$host$port$path$query$fragment";
      return redirect()->secure($url);
    }else{
      $url = "http://"."$user$pass$host$port$path$query$fragment";
      return redirect($url);
    }
  }

  public static function isSecure(){
    $isSecure = false;
  	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on"){
  		$isSecure = true;
  	}else if( !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on'){
  		$isSecure = true;
  	}
  	return $isSecure;
  }
}
