<?php namespace App\Services;

class MantisWs{
  private $client;
  public function __construct(){
    $this->client = new \SoapClient(config('mantis.url').'/api/soap/mantisconnect.php?wsdl', array(  'soap_version' => SOAP_1_1,
      'trace' => true,
    ));
  }

  public function reportException($traces){
    if(!isset($traces[0])){
      return;
    }

    $main = $traces[0];
    $summary = 'bug-report ' . $main['file'] . '-' . $main['line'];

    $issues = $this->getIssuesByFilter();

    $found = false;
    if(is_array($issues)){
      foreach($issues as $issue){
        if($issue->summary == $summary){
          $found = true;
          break;
        }
      }
    }

    if($found){ return; }

    $description = 'type - ' . $main['type'] . "\n" . 'file - ' . $main['type'] . "\n" . 'line - ' . $main['line'] . "\n" . 'value - ' . $main['value'] . "\n";

    $issue_id = $this->createIssue(null,null,$summary,$description);

    if(!$issue_id){
      return;
    }

    $this->addNote($issue_id,str_replace('#','',$main['trace']));

    if(config('mantis.parent_issue_id')){
      $parent_id = config('mantis.parent_issue_id');
      $this->setRelationIssue($issue_id,$parent_id);
    }
  }

  public function getFilterOnProject($p_id = null){
    if(is_null($p_id)){
      $p_id = config('mantis.project_id');
    }
    try {
      $params = array(
        'username' => config('mantis.username'),
        'password' => config('mantis.password') ,
        'p_project_id' => $p_id
      );
      return $this->client->__soapCall( 'mc_filter_get', $params );
    } catch (SoapFault $e) {
      return false;
    }
  }

  public function getIssuesByFilter($p_id = null,$filter_id = null){
    if(is_null($p_id)){
      $p_id = config('mantis.project_id');
    }
    if(is_null($filter_id)){
      $filter_id = config('mantis.filter_id');
    }
    /* $p_project_id, $p_filter_id, $p_page_number, $p_per_page*/
    try {
      $params = array(
        'username' => config('mantis.username'),
        'password' => config('mantis.password') ,
        'p_project_id' => $p_id,
        'p_filter_id' => $filter_id,
        'p_page_number' => 0,
        'p_per_page' => 100,
      );
      return $this->client->__soapCall( 'mc_filter_get_issues', $params );
    } catch (SoapFault $e) {
      return false;
    }
  }

  public function createIssue($p_id = null,$category='Bug',$summary,$description){
    if(is_null($p_id)){
      $p_id = config('mantis.project_id');
    }

    try {
      $params = array(
        'username' => config('mantis.username'),
        'password' => config('mantis.password') ,
        'p_issue' => array(
          'project' => array(
            'id' => $p_id
          ),
          'category' => 'Bug',
          'summary' => $summary,
          'description' => $description,
        ),
      );
      return $this->client->__soapCall( 'mc_issue_add', $params );
    } catch (SoapFault $e) {
      return false;
    }
  }

  public function setRelationIssue($issue_id,$parent_issue_id){
    try {
      $params = array(
        'username' => config('mantis.username'),
        'password' => config('mantis.password') ,
        'p_issue_id' => $issue_id,
        'p_relationship' => array(
          'type' => array(
            'id' => 3
          ),
          'target_id' => $parent_issue_id,
        ),
      );
      return $this->client->__soapCall( 'mc_issue_relationship_add', $params );
    } catch (SoapFault $e) {
      return false;
    }
  }

  public function addNote($issue_id,$note){
    try {
      $params = array(
        'username' => config('mantis.username'),
        'password' => config('mantis.password'),
        'p_issue_id' => $issue_id,
        'p_note' => array(
          'text' => $note,
        ),
      );
      return $this->client->__soapCall( 'mc_issue_note_add', $params );
    } catch (SoapFault $e) {
      return false;
    }
  }

  public function getCategoriesProduct($p_id = null){
    if(is_null($p_id)){
      $p_id = config('mantis.project_id');
    }
    try {
      $params = array(
        'username' => config('mantis.username'),
        'password' => config('mantis.password') ,
        'p_project_id' => $p_id
      );
      return $this->client->__soapCall( 'mc_project_get_categories', $params );
    } catch (SoapFault $e) {
      return false;
    }
  }
}