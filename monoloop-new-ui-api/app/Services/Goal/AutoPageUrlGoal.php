<?php namespace App\Services\Goal ;

use App\Goal;
use App\Folder;
use App\Services\PageElement\ContentGenerator ;

class AutoPageUrlGoal{
	public function url($account , $page_element){
		$url = $page_element->fullURL ;
	 	$name = 'Viewed ' . $url ;
	 	$goal = Goal::byName($name)->where('page_element_id',new \MongoId($page_element->id))->first();
	 	if(!is_null($goal)){
	 		return ;
	 	}

	 	#echo 'Create new goal;' . $name . "\n" ;
	 	$account_id = new \MongoId($account->id);
	 	$goal = new Goal();
    $goal->name = $name;
    $goal->type = 'page_related';
    $goal->condition = null;
    $goal->point = 1;
    $goal->account_id = $account_id ;
		$goal->cid = $account->uid;
		$goal->uid = ContentGenerator::newUid() ;
		$goal->embedded =  0;
		$goal->page_element_id  = new \MongoId($page_element->id);

    $goal->deleted = 0;
    $goal->hidden = 0;

		$goal->folder_id = new \MongoId(Folder::rootFolders($account_id)->first()->_id);
    $goal->save();

 	}
}