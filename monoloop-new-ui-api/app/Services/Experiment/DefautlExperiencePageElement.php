<?php namespace App\Services\Experiment;

use App\Experiment;
use App\PageElement;
use App\PageElementExperiment;
use App\PageElementContent;
use App\UrlConfig;

class DefautlExperiencePageElement{
	public function generate($cid,$debug = false){
		$experiments = Experiment::byCid($cid)->where('hidden',0)->where('deleted',0)->get();
		if(count($experiments) == 0){
			return;
		}
		if($debug){
			echo 'Found experience '.count($experiments). "\n";
		}
		$page_element = PageElement::where('fullURL','http://mon-experiences-'.$cid.'/')->first();
		if(is_null($page_element)){
			$page_element = new PageElement();
		}

		$url_config = new UrlConfig();
		$url_config->reg_ex = null;
		$url_config->url = 'http://mon-experiences-'.$cid;
		$url_config->url_option = 0;
		$url_config->inc_www = true;
		$url_config->inc_http_https = true;
		$url_config->urlHash = $url_config->getUrlHashAttribute();
		$url_config->cleanUrl = $url_config->getCleanUrlAttribute();
		$url_config->hidden = 0;

		$page_element->setUrlConfig($url_config);
		$page_element->cid = (int)$cid;
		$page_element->Experiment = [];
		$page_element->content = [];
		$page_element->deleted = 0;
		$page_element->hidden = 0;
		$page_element->save();

		foreach($experiments as $experiment){
			$page_element_experiment = new PageElementExperiment();
			$page_element_experiment->controlGroupSize = $experiment->cg_size;
			$page_element_experiment->controlGroupDays = $experiment->cg_day;
			$page_element_experiment->experimentID = $experiment->experimentID;
			$page_element_experiment->significantAction = $experiment->significant_action ;
			// new structural change
			$page_element_experiment->hidden = $experiment->hidden ;
			// Mantis #4101
			$page_element_experiment->priority = $experiment->priority;
			// $pageElementExperiment->_id = $pageElementId;
			if($experiment->goal){
				$page_element_experiment->goalID = $experiment->goal->uid;
			}
			if($experiment->segment){
				$page_element_experiment->segmentID = $experiment->segment->uid;
			}
			$page_element->experiments()->save($page_element_experiment);
		}

		foreach($experiments as $experiment){

			if($experiment->segment){
				$content = new PageElementContent();
				$content->uid = null;
		    $content->mappedTo = '';
		    $content->placementType = "4";
		    $content->order = 1;
		    $content->displayType = "1";
		    $content->addJS = "";
		    $content->startDate = null;
		    $content->endDate = null;
		    $content->content_type = '';
		    $content->experimentID = $experiment->experimentID;
		    $content->code = '<% '. str_replace('{|}', '{ %>pass<% }', $experiment->segment->condition) . ' %>';
		    $page_element->content()->save($content);
			}
		}
	}
}