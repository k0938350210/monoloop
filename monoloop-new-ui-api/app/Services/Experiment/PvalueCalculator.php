<?php namespace App\Services\Experiment;

/***
	* Helper class for calculate experiment p-value
	*
**/

class PvalueCalculator{
	private static function NormalP($x){
  	$d1=0.0498673470;
		$d2=0.0211410061;
		$d3=0.0032776263;
		$d4=0.0000380036;
		$d5=0.0000488906;
		$d6=0.0000053830;
		$a=abs($x);
		$t=1.0+$a*($d1+$a*($d2+$a*($d3+$a*($d4+$a*($d5+$a*$d6)))));
		$t*=$t;$t*=$t;$t*=$t;$t*=$t;$t=1.0/($t+$t);
		if($x>=0)
			$t=1-$t;
		return $t;
  }

  public static function P($v_t,$v_c,$c_t,$c_c){
    if( $c_t == 0 || $v_t == 0 )
      return 0;
  	$c_p= $c_c/$c_t;
		$v_p= $v_c/$v_t;
		$std_error= sqrt(($c_p*(1-$c_p)/$c_t)+($v_p*(1-$v_p)/$v_t));
		if($std_error == 0)
			return 0 ;
		$z_value=($v_p-$c_p)/$std_error;
		#echo $z_value ; die() ;
		$p_value= self::NormalP($z_value);

		if($p_value>0.5)
			$p_value=1-$p_value;
		$p_value = number_format($p_value,3) ;
		echo $p_value;
		if($p_value == 0)
			return 0 ;
  	return $p_value ;
  }
}