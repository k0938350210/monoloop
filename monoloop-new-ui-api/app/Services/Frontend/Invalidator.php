<?php namespace App\Services\Frontend;

use Illuminate\Support\Facades\Redis;

class Invalidator{

  private $_debug;

  public function removeCached($hash, $debug = false){

  	$client = \App\Services\Redis::getSentinel();
    $client->publish('invalidate', $hash);

  }

}
