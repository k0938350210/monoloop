<?php namespace App\Services\Account;

use App\Account;
use App\Funnel;
use App\Segment;
use App\Folder;

class DefaultFunnel{
  public function createDefault(Account $account){
  	# Get folder id ;
    $rFid = Folder::rootFolders(new \MongoId($account->id))->first() ;
    if($rFid){
      $rFid =  new \MongoId($rFid->id);
    } else {
      $rFid = null;
    }

    $default_funnels = [];

    $life_cycle =
    [
      'name' => 'Life cycle',
      'description' => '',
      'items' => [],
    ];

    $temp = ['First time visitors','Previous','Known','Trialist','Customer','Reengage'];
    $segments = Segment::byCid($account->uid)->bySystem()->get();
    foreach($temp as $t){
	    $segment = $segments->where('name',$t)->first();
	    if($segment){
	    	$life_cycle['items'][] = ['type' => 'audience','value' => $segment->uid];
	    }else{
	    	return;
	    }
    }

    $default_funnels[] = $life_cycle;

    foreach($default_funnels as $funnel_data){
    	$funnel = Funnel::byCid($account->uid)->byName($funnel_data['name'])->first();
    	if(is_null($funnel)){
    		$funnel = new Funnel();
    		$funnel->name = $funnel_data['name'];
    	}
    	$funnel->description = $funnel_data['description'];
    	$funnel->folder_id = $rFid;
    	$funnel->cid = $account->uid;
    	$funnel->items = $funnel_data['items'];
    	$funnel->save();
    }
  }
}