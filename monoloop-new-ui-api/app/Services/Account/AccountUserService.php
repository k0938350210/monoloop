<?php namespace App\Services\Account; 

use MongoId;

use App\Account;
use App\AccountUser;
use App\User ;

class AccountUserService{ 
  private $data = [] ; 
  public function __construct(){
    #blank data ; 
    $this->data['account_id'] = null ; 
    $this->data['user_id'] = null ; 
    $this->data['email'] = '' ; 
    $this->data['name'] = '' ; 
    $this->data['company'] = '' ; 
    $this->data['deleted'] = 0 ; 
    $this->data['hidden'] = 0 ; 
    $this->data['is_monoloop_admin'] = 0 ; 
    $this->data['is_monoloop_support'] = 0 ; 
    $this->data['dashboard_config'] = null ; 
    $this->data['support_type'] = 0 ; 
    $this->data['support_enddate'] = null ; 
    $this->data['invitation'] = null ;   
  }
  
  public function createAdmin(Account $account){
    $data = $this->data ; 
     
    $data['account_id'] = $account->_id ;  
    $data['email'] = $account->contact_email ; 
    $data['name'] = $account->contact_name ; 
    $data['company'] = $account->company ;  
    $data['is_monoloop_admin'] = 1 ;     
    $data['invitation'] = new MongoId() ; 
    return $this->create($data) ; 
  }
  
  public function createMonoloopSupport(Account $account){
    $data = $this->data ; 
    
    $supportUser = User::byUsername('monoloop_cs')->first() ; 
    
    $data['account_id'] = $account->_id ;   
    $data['user_id'] = $supportUser->_id ; 
    $data['email'] = 'support@monoloop.com' ; 
    $data['company'] = 'monoloop' ; 
    $data['name'] = 'monoloop_cs' ;  
    $data['hidden'] = 1 ; 
    $data['is_monoloop_support'] = 1 ; 
    return $this->create($data) ; 
  }
  
  private function create($data){
    $accountUser = new AccountUser() ; 
    $accountUser->account_id = $data['account_id'] ; 
    $accountUser->user_id = $data['user_id'] ; 
    $accountUser->email = $data['email'] ; 
    $accountUser->name = $data['name'] ; 
    $accountUser->company = $data['company'] ; 
    $accountUser->deleted = $data['deleted'] ; 
    $accountUser->hidden = $data['hidden'] ; 
    $accountUser->is_monoloop_admin = $data['is_monoloop_admin'] ; 
    $accountUser->is_monoloop_support = $data['is_monoloop_support'] ; 
    $accountUser->dashboard_config = $data['dashboard_config'] ; 
    $accountUser->support_type = $data['support_type'] ; 
    $accountUser->support_enddate = $data['support_enddate'] ; 
    $accountUser->invitation = $data['invitation'] ; 
    $accountUser->save() ; 
    return $accountUser ; 
  }
}
	