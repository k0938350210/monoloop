<?php namespace App\Services\Account;

use App\Account ;
use App\Tracker;


class UniqeIdCbr{
  public $account;
  public $js;

  public function __construct(Account $account)
  {
    $this->account = $account;
    $this->renderJS();
  }

  public function renderJS(){
    $tracker = Tracker::byCid($this->account->uid)->byUniqueTracker()->byActive()->first();

    if(is_null($tracker)){
      $this->js = '';
    }else{
      $this->js = View('scripts.trackers.unique')->with('account',$this->account)->with('tracker',$tracker)->render() ;

    }
  }
}