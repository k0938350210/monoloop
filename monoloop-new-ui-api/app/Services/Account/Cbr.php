<?php namespace App\Services\Account;
/**
 * This class for generate cbr.js & post.js
 * */
use App\Account ;
use App\CbrBatch ;
use App\Services\Account\UniqeIdCbr ;
use App\Services\Account\AssetCbr ;
use App\Services\Account\TrackerCbr ;


use File ;
use Symfony\Component\Process\Process;

class Cbr{

  public $account;

  public function __construct(Account $account)
  {
    $this->account = $account;
  }

	public function process(){
		$this->generateCbr();
    $this->generatePost();
    $this->addToBatchQueue() ;
    //$this->processDomainHook() ;
	}

  private function generateCbr(){
    $unige_id = new UniqeIdCbr($this->account);
    $cbrContent = View('scripts.cbr')->with('account',$this->account)->with('cbr_additional','')->with('customvars_additional',$unige_id->js)->render() ;
    File::put(public_path('fileadmin/cbr/' . $this->account->uid . '_cbr.js') , $cbrContent ) ;

    if(config('app.debug') == false){
      #compress file
      $process = new Process('uglifyjs  ' . public_path('fileadmin/cbr/' . $this->account->uid . '_cbr.js') .' --compress -o ' . public_path('fileadmin/cbr/' . $this->account->uid . '_cbr.js'  ));
      $process->run();
    }
  }

  private function generatePost(){
    # should we move to event ?

    //$asset = new AssetCbr($this->account) ;
    $tracker = new TrackerCbr($this->account);
    $unige_id = new UniqeIdCbr($this->account);
    //return ;
    $postContent = View('scripts.post')->with('account',$this->account)->with('post_additional',  $tracker->js . ' ' . $unige_id->js)->render() ;
    File::put(public_path('fileadmin/cbr/' . $this->account->uid . '_post.js') , $postContent ) ;

    if(config('app.debug') == false){
      #compress file
      $process = new Process('uglifyjs  ' . public_path('fileadmin/cbr/' . $this->account->uid . '_post.js') .' --compress -o ' . public_path('fileadmin/cbr/' . $this->account->uid . '_post.js'  ));
      $process->run();
    }
  }

  private function addToBatchQueue(){
    $cbrBatch = new CbrBatch()  ;
    $cbrBatch->uid = $this->account->uid ;
    $cbrBatch->save() ;
  }

  private function processDomainHook(){
    foreach ($this->account->domains as $domain){
      $url = 'http://'. $domain->domain . '?monoloop_refetch_cbr=true' ;
      echo 'Domain - ' . $url . "\n";
      try{
        file_get_contents($url );
      }catch (\Exception $e) {

      }

    }
  }
}
