<?php namespace App\Services\Account;

use App\Segment;
use App\Folder;
use App\Account;


class DefaultAudience{

  public function createDefault(Account $account){

    $default_audiences = [
      [
        'name' => 'First time visitors',
        'condition' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}",
        'hidden' => 0,
      ],
      [
        'name' => 'Recent visitors' ,
        'condition' => "if ( ( MonoloopProfile.DaysSinceLastVisit <= '30' ) ){|}",
        'hidden' => 0,
      ],
      [
        'name' => 'Lapsed visitors' ,
        'condition' => "if ( ( MonoloopProfile.DaysSinceLastVisit >= '60' ) ){|}",
        'hidden' => 0,
      ],
      [
        'name' => 'Active visitors' ,
        'condition' => "if ( ( MonoloopProfile.VisitsQA('quintile||2') === true ) || ( MonoloopProfile.VisitsQA('quintile||1') === true ) ){|}",
        'hidden' => 0,
      ],
      [
        'name' => 'Organic Search Visitors' ,
        'condition' => "if ( ( MonoloopProfile.SearchVisitor === true ) ){|}",
        'hidden' => 0,
      ],
      [
        'name' => 'Adwords Visitor' ,
        'condition' => "if ( ( ml_contains(MonoloopProfile.VisitReferer, '?gclid=' ) ) ){|}",
        'hidden' => 0,
      ],
      [
        'name' => 'Previous Visitors' , // Total visits>1 AND Visitor NOT Known OR Customer
        'condition' => "if ( ( ( MonoloopProfile.uniqueID !== '' ) && (  MonoloopProfile.VisitCount > 1 ) ) ){|}",
        'hidden' => 0,
      ],
      [
        'name' => 'Known Visitors' ,
        'condition' => "if ( ( MonoloopProfile.uniqueID !== '' ) ) {|}",
        'hidden' => 1,
      ],
      [
        'name' => 'Trialist' ,
        'condition' => "if ( ( MonoloopProfile.totalPurchases == '1' ) ){|}",
        'hidden' => 1,
      ],
      [
        'name' => 'Customer',
        'condition' => "if ( ( MonoloopProfile.totalPurchases > '1' ) ){|}",
        'hidden' => 1,
      ],
      [
        'name' => 'Reengage',
        'condition' => "if ( ( MonoloopProfile.daysSinceLastPurchase <= '180' ) ){|}",
        'hidden' => 1,
      ]
    ];


    $rFid = Folder::rootFolders(new \MongoId($account->id))->first() ;
    if($rFid){
      $rFid =  new \MongoId($rFid->id);
    } else {
      $rFid = null;
    }

    foreach ($default_audiences as $default_audience) {
      if (Segment::byName($default_audience['name'])->byCid($account->uid)->bySystem()->exists()) {
        // For reupdate condition;
        $segment = Segment::byName($default_audience['name'])->byCid($account->uid)->bySystem()->first();
        $segment->condition = $default_audience['condition'];
        $segment->hidden = $default_audience['hidden'];
        $segment->save();
        continue;
      }

      $segment = new Segment();
      $segment->name = $default_audience['name'];
      $segment->description = '';
      $segment->condition = $default_audience['condition'];
      $segment->cid = $account->uid;
      $segment->account_id = new \MongoId($account->id);
      $segment->deleted = 0;
      $segment->hidden = $default_audience['hidden'];
      $segment->embedded = 0;
      $segment->system = 1;
      $segment->folder_id = $rFid;
      $segment->save();
    }

    // Need reupdate previous
    $segment = Segment::byName('Previous Visitors')->byCid($account->uid)->bySystem()->first();
    $customer_segment = Segment::byName('Customer')->byCid($account->uid)->bySystem()->first();
    $know_segment = Segment::byName('Known Visitors')->byCid($account->uid)->bySystem()->first();
    $segment->condition = "if ( ( ( MonoloopProfile.VisitCount > '1' ) && ( ( MonoloopProfile.segment_".$know_segment->uid."() === false ) || ( MonoloopProfile.segment_".$customer_segment->uid."() == true ) ) ) ){|}";
    $segment->save();

    //if ( ( ( MonoloopProfile.VisitCount == '1' ) && ( ( MonoloopProfile.LastExitPage === '' ) || ( MonoloopProfile.VisitCount == '' ) ) ) ){|}

  }

}
