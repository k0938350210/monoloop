<?php namespace App\Services\Account;

use App\Account ;
use App\Tracker;

class TrackerCbr{

  public $account;
  public $js;

  public function __construct(Account $account)
  {
    $this->account = $account;
    $this->renderJS() ;
  }

  public function renderJS(){
    $trackers = Tracker::byCid($this->account->uid)->byActive()->get() ;
    $this->js = View('scripts.post_tracker')->with('account',$this->account)->with('trackers',$trackers)->render() ;
  }
}