<?php namespace App\Services\Account;

use App\Account ;
use App\TrackerNode ;

class CustomVarCbr{

  public $account;
  public $js;

  public function __construct(Account $account)
  {
    $this->account = $account;
    $this->renderJS();
  }

  public function renderJS(){
    $customVars = TrackerNode::byType('customvar')->byAccount($this->account->_id)->where('hidden', 0)->get();
    $this->js = View('scripts.post_customvar')->with('account',$this->account)->with('customVars',$customVars)->render();
  }
}