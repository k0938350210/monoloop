<?php

namespace App\Services\Trackers;

use App\Folder;
use App\Tracker;
use View;
use App\UrlConfig;


class PreconfiguredWpTrackers{
	public function updateDefault($account){
		# get root folder ;
		$rFid = Folder::rootFolders(new \MongoId($account->id))->first() ;
		if($rFid){
			$rFid =  new \MongoId($rFid->id);
		} else {
			$rFid = null;
		}

		//WP Unique tracker
		//Search for unuique tracker
		$tracker = Tracker::where('system_name','uniqid')->byCid($account->uid)->isSystem()->first();
		$tracker->tracker_fields = [
		[
			'_id' => 'field-unique',
      'name' => 'uniqid',
      'field_type' => 'string',
      'operator' => 'override',
      'type' => 'custom',
      'selector' => '',
      'meta' => '',
      'cookie' => '',
      'js' => '',
      'custom' => 'return monoloop_tracker.user',
      'filter' => 'none',
      'filter_custom' => '',
      'uid' => 1,
		]];


		$url_config = $tracker->urlConfig;
		$url_config->url = $account->first_domain;
		$url_config->save();
		$tracker->hidden = 0;
		$tracker->save();

		//WP email field
		$tracker = Tracker::where('system_name','wpfields')->byCid($account->uid)->isSystem()->first();
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->name = 'Wordpress fields';
			$tracker->type = 'custom';
			$tracker->system_name = 'wpfields';
			$tracker->system = 1;
			$tracker->tracker_events = [[
        "_id" => "event-page-render",
        "name" => "",
        "type" => "page-render",
        "selector" => "",
        "custom" => ""
      ]];
      $tracker->cid = $account->uid;
      $tracker->account_id = new \MongoId($account->id);
      $tracker->deleted = 0;
      $tracker->folder_id = $rFid;
      $tracker->uid = Tracker::max('uid') + 1;
      $tracker->hidden = 1;
      $tracker->save();
		}

		$url_config = new UrlConfig();
    $url_config->url = $account->first_domain;
    $url_config->remark = '';
    $url_config->url_option = 'startsWith';
    $url_config->reg_ex = '';
    $url_config->inc_www = true;
    $url_config->inc_http_https = true;
    $tracker->urlConfig()->save($url_config);

		$tracker->tracker_fields = [
		[
			'_id' => 'field-email',
      'name' => 'email',
      'field_type' => 'string',
      'operator' => 'override',
      'type' => 'custom',
      'selector' => '',
      'meta' => '',
      'cookie' => '',
      'js' => '',
      'custom' => '
      	var email = MONOloop.trim(MONOloop.getUrlParameters(\'utm_content\', document.location.href,true));
      	if(MONOloop.isEmail(email)){
      		return email;
      	}
      	return monoloop_tracker.email;',
      'filter' => 'none',
      'filter_custom' => '',
      'uid' => 1,
		]];

		$tracker->hidden = 0;
		$tracker->save();





		return;
		# Just deprecate old one.

		# post categoris tracker
		$tracker = Tracker::byName('wp_post_category')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_post_category' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.content_categories', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();


		# post tag tracker
		$tracker = Tracker::byName('wp_tag_category')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_tag_category' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.content_tags', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();

		#search tracker
		$tracker = Tracker::byName('wp_search')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_search' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.search', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();

		$this->updateWooCommerge($account,$rFid);
		$this->updateWPCommerge($account,$rFid);
	}

	public function updateWooCommerge($account,$rFid){
		# check if this account active woo ecommerge plugin
		if(! $account->isWpPluginActivated('woocommerce/woocommerce.php')){
			return ;
		}
		# Woo product
		$tracker = Tracker::byName('wp_woo_product')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_woo_product' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.woo_product', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();

		# Woo basket
		$tracker = Tracker::byName('wp_woo_basket')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_woo_basket' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.woo_basket', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();

		# Woo product
		$tracker = Tracker::byName('wp_woo_purchase')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_woo_purchase' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.woo_purchase', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();
	}

	public function updateWPCommerge($account,$rFid){
		if(! $account->isWpPluginActivated('wp-e-commerce/wp-shopping-cart.php')){
			return ;
		}

		# Woo product
		$tracker = Tracker::byName('wp_commerce_product')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_commerce_product' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.wp_commerce_product', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();

		# Woo basket
		$tracker = Tracker::byName('wp_commerce_basket')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_commerce_basket' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.wp_commerce_basket', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();

		# Woo product
		$tracker = Tracker::byName('wp_commerce_purchase')->byCid($account->uid)->first() ;
		if(is_null($tracker)){
			$tracker = new Tracker() ;
			$tracker->account_id = $account->id ;
			$tracker->cid = (int)$account->uid ;
			$tracker->hidden = 0 ;
			$tracker->deleted = 0 ;
			$tracker->folder_id =$rFid ;
			$tracker->uid = Tracker::max('uid') + 1 ;
			$tracker->name = 'wp_commerce_purchase' ;
		}

		$view = View::make('scripts.trackers.preconfigured.wp.wp_commerce_purchase', ['tracker' => $tracker]);
		$tracker->code = $view->render() ;
		$tracker->type = 'auto' ;
		$tracker->save();
	}

}