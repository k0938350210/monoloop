<?php namespace App\Services\Trackers;

use App\Tracker ;
use View ;

class PostProcessCode{
  public function run(Tracker $tracker){
    if($tracker->hidden || $tracker->deleted){
      $tracker->code = '' ;
      return ;
    }

    if($tracker->type === 'category'){
      $category_id_field = $tracker->getFieldByName('Client Category ID');
      $name_field = $tracker->getFieldByName('Category');

      $view = View::make('scripts.trackers.category', ['tracker' => $tracker, 'id_field' => $category_id_field, 'name_field' => $name_field]);
      $tracker->code = $view->render() ;
    }elseif($tracker->type === 'asset'){
      $view = View::make('scripts.trackers.asset', ['tracker' => $tracker]);
      $tracker->code = $view->render() ;
    }
    elseif($tracker->type === 'product'){
      $id_field = $tracker->getFieldByName('Client Product ID');
      $name_field = $tracker->getFieldByName('Name');
      $value_field = $tracker->getFieldByName('Value');

      $view = View::make('scripts.trackers.product', ['tracker' => $tracker, 'id_field' => $id_field, 'name_field' => $name_field, 'value_field' => $value_field]);
      $tracker->code = $view->render() ;
    }elseif($tracker->type === 'custom'){

      $view = View::make('scripts.trackers.custom', ['tracker' => $tracker]);
      $tracker->code = $view->render() ;
    }elseif($tracker->type === 'basket'){
      $view = View::make('scripts.trackers.basket', ['tracker' => $tracker]);
      $tracker->code = $view->render() ;
    }
    elseif($tracker->type === 'search'){
      $view = View::make('scripts.trackers.search', ['tracker' => $tracker]);
      $tracker->code = $view->render() ;
    }
    elseif($tracker->type === 'form'){
      $view = View::make('scripts.trackers.form', ['tracker' => $tracker]);
      $tracker->code = $view->render() ;
    }
  }
}