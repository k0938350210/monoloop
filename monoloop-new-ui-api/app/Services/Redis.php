<?php namespace App\Services;

/**
 * Use to perform redis operations
 */
class Redis
{
  public static function getUserIdFromRedisToken($token){
    // $user_id = str_replace('"', '', $token);
    // $user_id = substr($token, strlen(substr($token, 0, 35)));
    // $user_id =  substr($user_id, 0, stripos($user_id, '\\'));

    $breakToken = explode('_', $token);
    $user_id = $breakToken[1];

    return $user_id;
  }

  public static function getSentinel(){
    $sentinels = config('database.redis.sentinel.hosts');
    $options   = ['replication' => 'sentinel', 'service' => config('database.redis.sentinel.name')];

    if(config('app.debug')){
      $options['aggregate'] = function () {
        return function ($sentinels, $options) {
          $replication = new \Predis\Connection\Aggregate\SentinelReplication(
              $options->service,
              $sentinels,
              $options->connections,
              new \App\Services\Redis\CustomReplicationStrategy()
          );
          return $replication;
        };
      };
    }

    $client = new \Predis\Client($sentinels, $options);
    return $client;
  }
}
