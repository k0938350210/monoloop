<?php namespace App\Services;

use Auth;
use Storage;
use Cache;
use Carbon\Carbon ;

class Media{
	private $ds = '/';

	public function basePath(){
		$uid = Auth::user()->uid ;
		return 'assets/'.$uid ;
	}

	public function getFiles($source){
		$files = [];
		if(Cache::has('S3_FILES_'.$source)){
			$files = Cache::get('S3_FILES_'.$source);
		}else{
			$files = Storage::files($source);
			$expiresAt = Carbon::now()->addWeek(1);
			Cache::put('S3_FILES_'.$source , $files,$expiresAt);
		}

		return $files ;
	}

	public function getFilesRecursive($source){
		$files = [];
		if(Cache::has('S3_ALL_FILES_'.$source)){
			$files = Cache::get('S3_ALL_FILES_'.$source);
		}else{
			$files = Storage::allFiles($source);
			$expiresAt = Carbon::now()->addWeek(1);
			Cache::put('S3_ALL_FILES_'.$source , $files,$expiresAt);
		}

		return $files ;
	}

	public function getAllDirectories($source){
		$all_directories = [] ;
		if(Cache::has('S3_'.$source)){
			$all_directories = Cache::get('S3_'.$source);
		}else{
			$all_directories = Storage::allDirectories($source);
			$expiresAt = Carbon::now()->addWeek(1);
			Cache::put('S3_'.$source , $all_directories,$expiresAt);
		}
		return $all_directories;
	}

	public function clearCacheDirectoties(){
		Cache::forget('S3_'.$this->basePath());
	}

	public function clearCacheFiles($target){
		Cache::forget('S3_FILES_'.$target);
		Cache::forget('S3_ALL_FILES_'.$target);
	}

	public function clearCacheFile($target){
		Cache::forget('S3_FILE_'.$target);
	}

	public function renameFile($source,$target){
		$file_name = $this->getFileName($source);
		$folder = $this->getFolderFromFile($source);

		if($folder.'/'.$file_name == $folder.'/'.$target){
			return;
		}

		$old_ext = substr($file_name, strrpos($file_name, '.')+1);
		$new_ext = substr($target, strrpos($target, '.')+1);

		if($old_ext != $new_ext){
			$target .= '.' . $old_ext ;
		}

		if(Storage::exists($folder.'/'.$file_name)){
			Storage::move($folder.'/'.$file_name, $folder.'/'.$target);
			//move thumb
			if(Storage::exists($folder. '/' . config('lfm.thumb_folder_name') . '/' . $file_name )){
				Storage::move($folder. '/' . config('lfm.thumb_folder_name') . '/' . $file_name , $folder. '/' . config('lfm.thumb_folder_name') . '/' . $target);
			}
			//move resize
			foreach(config('lfm.resize_folder') as $resize){
				if(Storage::exists($folder. '/' . $resize . '/' . $file_name )){
					Storage::move($folder. '/' . $resize . '/' . $file_name , $folder. '/' . $resize . '/' . $target);
				}
			}
		}
		$this->clearCacheFiles($folder);
		return ['url' => config('filesystems.disks.s3.public_url') .  $folder . '/' . $target , 'value' => $target ] ;
	}

	public function deleteFile($source){

		$file_name = $this->getFileName($source);
		$folder = $this->getFolderFromFile($source);

		if(Storage::exists($source)){
			//remove file
			Storage::delete($source);
			//remove thumb
			if(Storage::exists($folder. '/' . config('lfm.thumb_folder_name') . '/' . $file_name)){
				Storage::delete($folder. '/' . config('lfm.thumb_folder_name') . '/' . $file_name);
			}
			//remove resize
			foreach(config('lfm.resize_folder') as $resize){
				if(Storage::exists($folder. '/' . $resize . '/' . $file_name)){
					Storage::delete($folder. '/' . $resize . '/' . $file_name);
				}
			}
		}

		$this->clearCacheFiles($folder);
	}



  public function getFolderFromFile($source){
  	$arr = explode($this->ds,$source);
  	array_pop($arr);
		return implode($this->ds,$arr);
  }

  public function getFileName($source){
  	$arr_files = explode($this->ds,$source);
		return end($arr_files);
  }

  public function getReadablePath($path){
  	return str_replace(' ', '+',$path);
  }

  public function checkDeletePermission($file){
  	$basePath = $this->basePath() ;
  	if(strpos($file,$basePath) === false){
  		return 'permission deny' ;
  	}
  	if($file == $basePath || $basePath . '/' == $file){
  		return 'cannot delete root folder' ;
  	}

  	$files = $this->getFiles($file);
  	if(count($files) > 0){
  		return 'cannot delete not empty folder';
  	}

  	return true ;
  }

  public function checkRenamePermission($file){
  	$basePath = $this->basePath() ;
  	if(strpos($file,$basePath) === false){
  		return 'permission deny' ;
  	}
  	if($file == $basePath || $basePath . '/' == $file){
  		return 'cannot rename root folder' ;
  	}

  	return true ;
  }


  public function checkPermission($file){
  	$basePath = $this->basePath() ;
  	if(strpos($file,$basePath) === false){
  		return 'permission deny' ;
  	}
  	return true ;
  }
}