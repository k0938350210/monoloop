<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class GuidesTipsStatus extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'guides_tips_status';
}
