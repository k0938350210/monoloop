<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use Auth;
use Carbon\Carbon;
use MongoId;

class AccountUser extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'account_users';

    protected $hidden = ['invitation'];
    protected $dates = array('support_enddate');
  /*------- relation -------- */

  public function account()
  {
      return $this->belongsTo('\App\Account', 'account_id');
  }

  public function user()
  {
      return $this->belongsTo('\App\User', 'user_id');
  }

  /*--- Scope ---*/

  public function scopeInvitation($query, $invitation)
  {
      return $query->where('invitation', new MongoId($invitation));
  }

  public function scopeByAccountId($query , $account_id){
    return $query->where('account_id', (string)$account_id);
  }

  public function scopeByUserId($query , $user_id){
    return $query->where('user_id', (string)$user_id);
  }

  public function scopeByMonoloopAdmin($query,$is_monoloop_admin){
    return $query->where('is_monoloop_admin', $is_monoloop_admin);
  }

  public function scopeByEmail($query,$email){
    return $query->where('email', $email);
  }

  /*------- attributes --------*/

  public function getIsAccessibleAttribute()
  {
      $currentAccount = Auth::user()->account->_id;
      if ($this->account_id == $currentAccount) {
          return true;
      }

      return false;
  }

  public function getCanResendInvitationAttribute()
  {
    if ($this->invitation != '') {
      return true;
    }

    if ($this->user == null) {
      return true;
    }

    return false;
  }

  /**
   * The method can make two kind of invites: one for user and one for accounts,
   * linking different url for different kind of invites
   *
   * @return string
   */
  public function getInvitationURLAttribute()
  {
    if(!empty($this->invitationType) && $this->invitationType === "accountUser"){
      $url = config('app.ui_url').'/invitation/user/'.$this->account_id.'/'.$this->id;
    } else {
      $url = config('app.ui_url').'/invitation/account/'.$this->account_id.'/'.$this->id;
    }
    return $url;
  }

  public function isAccountValid(){
    if($this->attributes['hidden'] == 1 || $this->deleted == 1){
      return false;
    }
    if($this->support_type === 'fix-until' && Carbon::now() > $this->support_enddate){
      return false;
    }
    return true;
  }

  /*------- method ----------*/
  public function toArray()
  {
      $array = parent::toArray();
      $array['can_resend_invitation'] = $this->canResendInvitation;
      /*
      if (!empty($array['support_enddate']) && $array['support_enddate'] != '') {
        $array['support_enddate'] = Carbon\Carbon::parse($array['support_enddate'])->format('d/m/Y');
      }
      */
      if(is_object($this->user)){
        $array['user']['username'] = $this->user->username;
      }

      return $array;
  }
}
