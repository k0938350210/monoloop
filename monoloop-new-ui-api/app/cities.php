<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;

class City extends Eloquent{

  protected $collection = 'cities';

}
