<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Services\MantisWs;

class Handler extends ExceptionHandler {

  /**
   * A list of the exception types that should not be reported.
   *
   * @var array
   */
  protected $dontReport = [
    'Symfony\Component\HttpKernel\Exception\HttpException'
  ];

  /**
   * Report or log an exception.
   *
   * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
   *
   * @param  \Exception  $e
   * @return void
   */
  public function report(Exception $e)
  {
    if($this->shouldReport($e)){
      if(config('mantis.username')){
        $this->captureException($e);
      }
    }
    return parent::report($e);
  }

  /**
   * Render an exception into an HTTP response.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Exception  $e
   * @return \Illuminate\Http\Response
   */
  public function render($request, Exception $e)
  {
    if($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException)
    {
      return response()->json([
        'message' => 'Record not found',
      ], 404);
    }
    return parent::render($request, $e);
  }

  public function captureException($exception, $data = null, $logger = null, $vars = null)
  {
    $has_chained_exceptions = version_compare(PHP_VERSION, '5.3.0', '>=');
    $exc = $exception;
    $trace = array();
    do {
      /**'exception'
       * Exception::getTrace doesn't store the point at where the exception
       * was thrown, so we have to stuff it in ourselves. Ugh.
       */
      $trace[] = array(
        'file' => $exc->getFile(),
        'line' => $exc->getLine(),
        'value' => $exc->getMessage() ,
        'type' => get_class($exc),
        'trace' => $exc->getTraceAsString(),
      );

    } while ($has_chained_exceptions && $exc = $exc->getPrevious() );

    $mantis = new MantisWs();
    $mantis->reportException($trace);
  }

}
