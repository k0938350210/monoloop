<?php namespace App\Http\Requests\Experiment;

use App\Http\Requests\Request; 

class DeleteForm extends ExperimentBase
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_id' => 'required',
      ];
    }
}
