<?php namespace App\Http\Requests\CustomField;

use App\Http\Requests\Request;
use Auth ;
use App\CustomField ;
use MongoId ;
use Route;
use MongoException;

class DeleteRequest extends BaseRequest {

  public function authorize()
  {
    $current_params = Route::current()->parameters();
    return CustomField::uid($current_params['customvars'])->byAccount(Auth::user()->active_account)->exists();
  }

  public function rules(){
    return [];
  }

}
