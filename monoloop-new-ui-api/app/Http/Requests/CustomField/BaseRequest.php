<?php namespace App\Http\Requests\CustomField;

use App\Http\Requests\Request;
use Auth ;
use App\CustomField ;
use MongoId ;
use MongoException;

class BaseRequest extends Request {

  public function authorize()
  {
    $srcId = null ;
    try {
      $srcId = new MongoId($this->input('_id'));
    } catch (MongoException $ex) {
      return false ;
    }
    return CustomField::where('_id', $srcId )->byAccount(Auth::user()->active_account)->exists();
  }

  public function dataTypes(){
    return ['string','integer','date','boolean'];
  }
}
