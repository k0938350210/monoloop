<?php

namespace App\Http\Requests\Goal;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;
use App\Goal;
use Auth;
use MongoId;

class ShowRequest extends GoalBase
{  
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        '_id' => 'required'
      ];
    }

}
