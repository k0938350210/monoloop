<?php

namespace App\Http\Requests\Goal;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;
use App\Goal;
use MongoId ;
use Auth;

class StatusUpdateForm extends GoalBase
{ 

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_id' => 'required',
          'hidden' => 'required|boolean',
      ];
    }
}
