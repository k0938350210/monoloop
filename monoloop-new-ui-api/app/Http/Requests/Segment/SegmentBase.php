<?php namespace App\Http\Requests\Segment;

use App\Http\Requests\Request;
use Auth ;
use App\Segment ;
use MongoId ;
use MongoException;

class SegmentBase extends Request {

  public function authorize()
  {
    if (strpos($this->input('_id'), '__') !== false) {
      $srcTypeId = explode('__', $this->input('_id'));
      $srcId = $srcTypeId[1];
    } else {
      $srcId = $this->input('_id');
    }
    try {
      $srcId = new MongoId($srcId);
    } catch (MongoException $ex) {
      return false ;
    }
    return Segment::where('_id', $srcId )->byAccount(Auth::user()->active_account)->exists();
  }
}
