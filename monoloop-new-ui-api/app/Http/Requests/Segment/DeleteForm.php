<?php namespace App\Http\Requests\Segment;

use App\Http\Requests\Request;

class DeleteForm extends SegmentBase
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_id' => 'required',
      ];
    }

}
