<?php namespace App\Http\Requests\Segment;

class StatusUpdateForm extends SegmentBase
{ 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_id' => 'required',
          'hidden' => 'required',
      ];
    }

}
