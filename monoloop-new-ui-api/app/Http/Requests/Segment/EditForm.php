<?php namespace App\Http\Requests\Segment;

class EditForm extends SegmentBase
{ 

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_id' => 'required',
          'nameSegment' => 'required|max:255',
          // 'descSegment' => 'required',
      ];
    }

}
