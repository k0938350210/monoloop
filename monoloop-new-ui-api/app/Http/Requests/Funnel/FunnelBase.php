<?php namespace App\Http\Requests\Funnel;

use App\Http\Requests\Request;
use Auth;
use App\Funnel;
use MongoId;
use MongoException;

class FunnelBase extends Request{

  public function authorize()
  {
    if (strpos($this->input('_id'), '__') !== false) {
      $srcTypeId = explode('__', $this->input('_id'));
      $srcId = $srcTypeId[1];
    } else {
        $srcId = $this->input('_id');
    }
    try {
      $srcId = new MongoId($srcId);
    } catch (MongoException $ex) {
      return false ;
    }
    return Funnel::where('_id', $srcId )->byAccount(Auth::user()->active_account)->exists();
  }

}
