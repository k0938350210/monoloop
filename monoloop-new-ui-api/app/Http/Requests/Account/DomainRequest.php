<?php namespace App\Http\Requests\Account;

use App\Http\Requests\Request;

class DomainRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'domain' => 'required',
        // 'privacy_option' => 'required|in:auto,manual',
        // 'url_privacy' => 'required'
      ];
    }
}
