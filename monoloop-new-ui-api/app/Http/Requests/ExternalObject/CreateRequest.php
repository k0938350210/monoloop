<?php namespace App\Http\Requests\ExternalObject;

use Auth;
use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class CreateRequest extends Request
{
  public function authorize()
  {
    return true;
  }

  public function rules()
  {

    $rules = [
      'tracker.name' => ['required','max:191','uniquemon:trackers,name,NULL,_id,cid,'.$this->cid.',deleted,0'],
      'tracker.tracker_fields' => 'required|array',
    ];

    if(is_array($this->input('tracker.tracker_fields'))){
      foreach($this->input('tracker.tracker_fields') as $key => $val){
        $rules['tracker.tracker_fields.'.$key.'.name'] = 'required|max:191';
        $rules['tracker.tracker_fields.'.$key.'.field_type'] = 'in:string,number,boolean';
        $rules['tracker.tracker_fields.'.$key.'.operator'] = 'in:override,add,substract,push';
      }
    }

    return $rules;
  }

  public function response(array $errors)
  {
    $res['status'] = 422;
    $res['message'] = 'Unprocessable Request';
    $res['errors'] = $errors;

    return new JsonResponse($res);
  }
}