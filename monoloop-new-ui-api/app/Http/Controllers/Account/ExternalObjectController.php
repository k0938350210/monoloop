<?php namespace App\Http\Controllers\Account;

use Auth;
use App\Tracker;
use App\Folder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\ExternalObject\CreateRequest;
use App\Http\Requests\ExternalObject\UpdateRequest;

class ExternalObjectController extends Controller {
  public function index(Request $request, $cid){
    $trackers = Tracker::byCid($cid)->byType('external-object')->get();
    return response()->json(['trackers' => $trackers]);
  }

  public function show(Request $request, $cid, $id){
    $tracker = Tracker::byCid($cid)->byType('external-object')->findOrFail($id);
    return response()->json(['tracker' => $tracker]);
  }

  public function store(CreateRequest $request, $cid){
    $account_id = Auth::user()->active_account;
    $folder = Folder::rootFolders($account_id)->first();

    $tracker = new Tracker();
    $tracker->name = $request->input('tracker.name');
    $tracker->type = 'external-object';
    $tracker->account_id = $account_id;
    $tracker->cid = (int)Auth::user()->uid;
    $tracker->uid = Tracker::where('cid',$tracker->cid)->max('uid') + 1;
    $tracker->hidden = 0;
    $tracker->deleted = 0;
    $tracker->folder_id = new \MongoId($folder->id);

    $tracker_fields = $request->input('tracker.tracker_fields');
    $i =1;
    $fields = [];
    if($tracker_fields != null){
      foreach($tracker_fields as $tracker_field){
        $tracker_field['uid'] = $i;
        if(!isset($tracker_field['field_type'])){
          $tracker_field['field_type'] = 'string';
        }
        if(!isset($tracker_field['operator'])){
          $tracker_field['operator'] = 'override';
        }
        $fields[] = $tracker_field;
        $i++;
      }
    }
    $tracker->tracker_fields = $fields;
    $tracker->save();

    return response()->json(['tracker' => $tracker]);
  }

  public function update(UpdateRequest $request, $cid, $id){
    $tracker = Tracker::byCid($cid)->byType('external-object')->findOrFail($id);

    $tracker->name = $request->input('tracker.name');
    $tracker->hidden = $request->input('tracker.hidden');

    $tracker_fields = $request->input('tracker.tracker_fields');
    $i =1;
    $fields = [];
    if($tracker_fields != null){
      foreach($tracker_fields as $tracker_field){
        $tracker_field['uid'] = $i;
        if(!isset($tracker_field['field_type'])){
          $tracker_field['field_type'] = 'string';
        }
        if(!isset($tracker_field['operator'])){
          $tracker_field['operator'] = 'override';
        }
        $fields[] = $tracker_field;
        $i++;
      }
    }
    $tracker->tracker_fields = $fields;
    $tracker->save();

    return response()->json(['tracker' => $tracker]);
  }

  public function destroy(Request $request, $cid, $id){
    $tracker = Tracker::byCid($cid)->byType('external-object')->findOrFail($id);
    $tracker->hidden = 1;
    $tracker->deleted = 1;
    $tracker->save();
    return response()->json(['tracker' => $tracker]);
  }
}