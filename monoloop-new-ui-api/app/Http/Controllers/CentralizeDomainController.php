<?php namespace App\Http\Controllers;

use Auth ;
use Illuminate\Http\Request;
use MongoId ;
use Cookie ;

class CentralizeDomainController extends Controller {
  public function index(Request $request){
    $cid = $request->input('cid',0);
    $gmid = $request->cookie($cid . '_gmid');
    if($gmid == null){
      $gmid = (string)new MongoId();
      Cookie::queue(Cookie::make($cid . '_gmid', $gmid, 525600));
    }

    return view('centralize_domain')->with('gmid',$gmid) ; 
  }
}
