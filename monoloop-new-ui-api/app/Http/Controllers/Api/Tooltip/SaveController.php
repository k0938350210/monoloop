<?php namespace App\Http\Controllers\Api\Tooltip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ;

use App\Tooltip;
use App\GuidesTipsStatus;
use App\Services\Redis\RedisService;

class SaveController extends Controller {

	const UserTooltipState = "UserTooltipState";

	public function __construct()
	{
		$this->middleware('auth');
	}

  /*--- REST FULL ----*/

  public function index(Request $request){
		$response = array();
		$response['status'] = 200;
		$response['message'] = "";
		$response['guidelines'] = [] ;

		return response()->json($response);
  }

	public function save(Request $request){
		$response = array();
		$response['status'] = 200;
		$response['message'] = "";
		$response['guidelines'] = [] ;


		$state = $request->input('state', '');
		$pageId = $request->input('pageId', '');

		$account_cid = Auth::user()->account->uid;

		$GuidesTipsStatus = GuidesTipsStatus::where('cid', '=', $account_cid)->where('pageId', '=', $pageId)->get();
		if(sizeof($GuidesTipsStatus) > 0){
			$GuidesTipsStatus[0] ->istipsViewed = 1;
			$GuidesTipsStatus[0] -> save();
		}else {
			$GuidesTipsStatus = new GuidesTipsStatus();
			$GuidesTipsStatus ->pageId = $pageId;
			$GuidesTipsStatus ->istipsViewed = 1;
			$GuidesTipsStatus ->cid = $account_cid;
			$GuidesTipsStatus -> save();
		}

		$response['state'] = $state;

		$response['key'] = $key = RedisService::getKey(Auth::user()->_id, self::UserTooltipState).':'.$pageId;

		$redisService = new RedisService();
		$redisService->set($key, $state);

		return response()->json($response);
	}


		public function import(Request $request){
			$response = array();
			$response['status'] = 200;
			$response['message'] = "Done";

			Tooltip::truncate();
			// run laravel: composer dump-autoload -o to show this class to terminal

			$file = public_path()."/".'MergedUITips - Sheet1.csv';
			\Maatwebsite\Excel\Facades\Excel::load($file, function($reader) {
				// reader methods
				// Getting all results
				$results = $reader->get();

				// ->all() is a wrapper for ->get() and will work the same
				$results = $reader->all();

				foreach ($results as $key => $value) {
					// var_dump($value);die;
					$g = new Tooltip();
					$g->name = $value['name'];
					$g->description = $value['tip_desccription'];
					$g->page_id = $value['page_id'];
					$g->tooltip_no = $value['tooltip_no'];
					if(!$value['next_tip_link'] || $value['next_tip_link'] === 'NULL' || $value['next_tip_link'] === 'Null' || $value['next_tip_link'] === 'null'){
						$g->next = null;
					} else {
						$g->next = $value['next_tip_link'];
					}
					if(!$value['previous_tip_link'] || $value['previous_tip_link'] === 'NULL' || $value['previous_tip_link'] === 'Null' || $value['previous_tip_link'] === 'null'){
						$g->prev = null;
					} else {
						$g->prev = $value['previous_tip_link'];
					}

					$g->screenshot = $value['screenshot'];
					$g->deleted = 0;
					$g->hidden = 0;
					$g->save();


				}

			});

			return response()->json($response);
		}
}
