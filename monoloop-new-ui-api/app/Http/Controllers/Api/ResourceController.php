<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth ;
use Storage;


class ResourceController extends Controller {
	public function show(Request $request,$id){
		list($key,$value) = explode('-',$id);

		switch ($key) {
			case 'cbr':
				if(!file_exists(base_path('public/fileadmin/cbr/'.$value . '_cbr.js'))){
					return response()->json([
		        'message' => 'Record not found',
			    ], 404);
				}else{
					return file_get_contents(base_path('public/fileadmin/cbr/'.$value . '_cbr.js'));
				}
				break;
			case 'post':
				if(!file_exists(base_path('public/fileadmin/cbr/'.$value . '_post.js'))){
					return response()->json([
		        'message' => 'Record not found',
			    ], 404);
				}else{
					return file_get_contents(base_path('public/fileadmin/cbr/'.$value . '_post.js'));
				}
				break;

			default:
				break;
		}
	}

}