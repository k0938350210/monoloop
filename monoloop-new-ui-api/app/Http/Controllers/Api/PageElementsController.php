<?php namespace App\Http\Controllers\Api;

use Auth;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\PageElement ;

class PageElementsController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');

	}

	public function index(Request $request){
		$page_elements = PageElement::byAccountCid(Auth::user()->account->uid)->get() ;

		return response()->json($page_elements->toArray());
	}

}