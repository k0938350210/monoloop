<?php namespace App\Http\Controllers\Api\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ConditionBuilder\Syntax ;
use App\Goal;

use Auth;
use Validator;
use App\PageElement;

class TestBenchController extends Controller {
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function show(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['url' => 'required'] );
    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $url = $request->input('url');
    $page_element_id = $request->input('page_element_id');
    $selectedExperimentId = $request->input('selected_experiment_id', '');
    $ret['status'] = 200;
    $ret['success'] = true;
    $ret['msg'] = "";
    $ret['found'] = false;

    $mongoDetail = [];

    $pageElements = PageElement::where('originalUrl', $url);
    if($page_element_id){
      $pageElements = $pageElements->where('_id',  new \MongoId($page_element_id) );
    }
    $pageElements = $pageElements->where('deleted', 0)->where('cid', $active_account->uid)->get();
    $allCondition = null ;
    foreach($pageElements as $pageElement){
      foreach( $pageElement->content  as $content ){
        if($selectedExperimentId != '' && $selectedExperimentId != 'all'){
          if((int)$selectedExperimentId == $content->experimentID){
            $allCondition .= $content->extractCondition;
            $mongoDetail[] = $content->toArray();
          }
        }else{
          $allCondition .= $content->extractCondition;
          $mongoDetail[] = $content->toArray();
        }

      }
    }

    $syntax = new Syntax() ;
    $groups =  $syntax->getTestBebchPanelObject($allCondition) ;

    // add goal name instead of ID for test-bench details
    // only case: MonoloopProfile.GoalReached('Goal ID')
    // we are replacing goal uid with goal name for display in test bench side panel
    if(!empty($groups['Goals'])){
      // goal condition key :: gc_key

      $keys = array_keys($groups['Goals']);
      foreach($keys as $gc_key){
        $goalCondition = $groups['Goals'][$gc_key];
        if(!empty($goalCondition['values'])){
          $goalValue = $goalCondition['values'][0];
          $goalID = $goalValue['value'];
          $goal = Goal::where('uid', (int)$goalID)->first();
          if($goal){
            $groups['Goals'][$gc_key]['values'][0]['value'] = $goal->name;
          }
        }
      }
    }
    $ret['testbench'] = $groups;
    $ret['mongoDetail'] = $mongoDetail;
    return response()->json($ret , 200);
  }

  public function showFromCondition(Request $request){
    $condition = $request->input('condition');

    $syntax = new Syntax();
    $groups =  $syntax->getTestBebchPanelObject($condition);

    if(!empty($groups['Goals'])){
      // goal condition key :: gc_key

      $keys = array_keys($groups['Goals']);
      foreach($keys as $gc_key){
        $goalCondition = $groups['Goals'][$gc_key];
        if(!empty($goalCondition['values'])){
          $goalValue = $goalCondition['values'][0];
          $goalID = $goalValue['value'];
          $goal = Goal::where('uid', (int)$goalID)->first();
          if($goal){
            $groups['Goals'][$gc_key]['values'][0]['value'] = $goal->name;
          }
        }
      }
    }

    $ret['testbench'] = $groups;
    return response()->json($ret , 200);
  }
}
