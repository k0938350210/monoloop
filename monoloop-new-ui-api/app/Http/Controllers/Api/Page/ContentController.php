<?php namespace App\Http\Controllers\Api\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth ;
use Validator;
use MongoId ;
use MongoException;

use App\Placement;
use App\PageElement;
use App\PageElementContent;
use App\UrlConfig;
use App\Content;
use App\ContentConfig;
use App\Experiment;
use App\PageElementExperiment;


class ContentController extends Controller {

  public function __construct()
	{
		$this->middleware('auth');
	}

  private $postRules = [] ;

  // request post: returing page element contens according to the xpath and type of contents (inline, variations and all)

  public function index(Request $request){

    $ret['status'] = 'success';
    $ret['msg'] = "";

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['xpath' => 'required', 'fullURL' => 'required'] );
    if($validator->fails()){
      $ret['status'] = 'error';
      $ret['msg'] = $validator->errors()->all();
      return response()->json($ret, 400);
    }

    $ret['xpath'] = $request->input('xpath');
    $ret['fullURL'] = $request->input('fullURL');

    $ret['type'] = $request->input('type', 'all');

    // echo $active_account->uid;die;
    $ret['variations'] = [];
    $pageElements = PageElement::where('originalUrl',$ret['fullURL'])->where('deleted', '<>', 1)->where('cid', $active_account->uid)->get();

    if(count($pageElements) > 0){

      foreach ($pageElements as $pageElement) {
        $content = $pageElement->content->where('mappedTo', '=', $ret['xpath']);
        if($ret['type'] === 'variations'){
          $variations = $content->where('uid', '<>', null)->get();

          foreach ($variations as $value) {
            $value->content = Content::where('uid', $value->uid)->first();
            if($value->content){
              if((int)$value->content->deleted !== 1){
                $ret['variations'][] = $value;
              }
            }
          }
        } elseif($ret['type'] === 'inline') {
          $ret[$ret['type']] = $content->where('uid', null)->get();
        } else {
          $ret[$ret['type']] = $content->get();
        }
      }


    } else {
      $ret[$ret['type']] = [];
    }
    return response()->json($ret,200);

  }

  public function show(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['content_id' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $fullURL = $request->input('fullURL', '');
    $content_id = $request->input('content_id');

    try {
        $content_id_mongo = new \MongoId($content_id);
        $newContent = Content::find($content_id_mongo);
    } catch (MongoException $ex) {
        return response()->json(['status'=>'error','msg'=> 'Invalid input.'], 400);
    }
    if(!$newContent){
      return response()->json(['status'=>'error','msg'=> 'Variation not found.'], 400);
    }


    // $pageElement = PageElement::where('fullURL','=',$fullURL)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->first();


    return response()->json(['status'=>'success','content'=> $newContent], 200);
  }

  public function store(Request $request){

    $active_account = Auth::user()->account;


    // var_dump($active_account->_id);die;

    $validator = Validator::make($request->all(), ['fullURL' => 'required', 'variation_name' => 'required', 'xpath' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $variation_name = $request->input('variation_name');
    $type = $request->input('type', 'variation');
    $xpath = $request->input('xpath');
    $condition = $request->input('condition', "");
    $content = $request->input('content', "");
    $inlineContent = $request->input('inlineContent', "");
    $contentType = $request->input('contentType', "");
    $imageLink = $request->input('imageLink', "");

    $fullURL = $request->input('fullURL');
    $regExp = $request->input('tRegularExpressionWebPageElement', '');
    $inWWW = (int)$request->input('tIncludeWWWWebPageElement', 0);
    $inHTTP_HTTPS = (int) $request->input('tIncludeHttpHttpsWebPageElement', 0);
    $urlOption = $request->input('tUrlOptionWebPageElement', 0);
    $additionalJS = $request->input('tAdditionalJSWebPageElement', '');
    $remark = $request->input('tRemarkWebPageList', '');
    $selectedExperimentId = $request->input('selected_experiment_id', '');

    // $tFullUrl = $fullURL;
    // if(strpos($tFullUrl, 'http') === false){
    //   $tFullUrl = 'http://'.$tFullUrl;
    // }
    // $parse = parse_url($tFullUrl);
    //
    // $domain = $parse['host'];
    //
    // $domain = str_replace('www.','',$domain);
    //
    // $host_names = explode(".", $domain);
    // $bottom_host_name = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];
    //
    // $domainFound = $active_account->domains->where('domain', '=', $bottom_host_name)->first();
    //
    // if(!$domainFound){
    //   return response()->json(['status'=>'error','msg'=> "You're trying to add domain which is not registered in your account."], 200);
    // }

    // to check if it's an experiment's page
    $mode = $request->input('mode', 'default');
    $source = $request->input('source', 'default');
    $pageElementId = $request->input('page_element_id', '');
    $experiment_id = $request->input('experiment_id', '');
    if($pageElementId === 'undefined'){
      $pageElementId = '';
    }
    $newContent = new Content();
    $newContent->account_id = new \MongoId($active_account->_id);
    $newContent->hidden = 0;
    $newContent->deleted = 0;
    $newContent->name = $variation_name;
    $newContent->condition = $condition;
    $newContent->type = $type;
    if ($request->input('placement_type') != "") {
      $newContent->placement_type = $request->input('placement_type'); // Mantis #4556
    }

    $newUid = 1 ;
    $maxUid = Content::max('uid') ;
    if($maxUid){
      $newUid = $maxUid+1 ;
    }
    $newContent->uid = $newUid;

    if(strpos($content,"<!--{cke_protected}{C}%3C!%2D%2D%20monoloop%20remove%20%2D%2D%3E-->&nbsp;") !== false){
      $content = "<!-- monoloop remove -->&nbsp;";
    }

    // if($content === "<!--{cke_protected}{C}%3C!%2D%2D%20monoloop%20remove%20%2D%2D%3E-->&nbsp;"){
    //   $content = "<!-- monoloop remove -->&nbsp;";
    // }

    $config = ['content' => $content, 'content_type' => $contentType];


    // echo $pageElementId;die;

    $newContent->config()->content = $config['content'];
    $newContent->config()->content_type = $config['content_type'];
    if($contentType === 'image'){
      $config['image_link'] = $imageLink;
      $newContent->config()->image_link = $config['image_link'];
    }

    $newContent->save();

    $config = new ContentConfig($config);
    $config->setContent();

    $newContent->config()->save($config);

    // adding page element


    $urlConfig = new UrlConfig();
		$urlConfig->reg_ex = $regExp;
		$urlConfig->url = $fullURL;
		$urlConfig->url_option = $urlOption;
		$urlConfig->inc_www = $inWWW;
		$urlConfig->inc_http_https = $inHTTP_HTTPS;
		$urlConfig->urlHash = $urlConfig->getUrlHashAttribute();
		$urlConfig->cleanUrl = $urlConfig->getCleanUrlAttribute();

    $urlConfig->setUrlOptionFromString($urlConfig->url_option);

    $pe = new PageElement();
		$pe->setUrlConfig($urlConfig);

    // if($source === 'default'){
    //
    //
    //
    //   if(strlen($pageElementId) > 0){
    //     $pageElement = PageElement::find($pageElementId);
    //   } else {
    //
    //   }
    //
    // } else {
    //   switch ($source) {
    //     case 'experiment':
    //       $pageElement = PageElement::find($pageElementId);
    //       break;
    //     default:
    //       $pageElement = PageElement::find($pageElementId);
    //       break;
    //   }
    // }

    $found = false;
    $experimentFound = false;
    if($pageElementId){
      $pageElement = PageElement::find($pageElementId);
      if($pageElement){
        $found = true;
      }
    }else {
        $pageElement = PageElement::where('originalUrl','=',$fullURL)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->first();
        if($pageElement){
          $found = true;
        }
    }
    if($found === false){
      $pageElement = PageElement::where('originalUrl','=',$fullURL)
                                  ->where('deleted', '<>', 1)
                                  ->where('cid', '=', $active_account->uid)
                                  ->where('inWWW', '=', $pe->inWWW)
                                  ->where('inHTTP_HTTPS', '=', $pe->inHTTP_HTTPS)
                                  ->where('urlOption', '=', (int) $pe->urlOption)
                                  ->where('regExp', '=', $pe->regExp)
                                  ->first();
    }

    // var_dump($pageElement);die;
    if(!$pageElement){
      $pageElement = $pe;
			$pageElement->cid = $active_account->uid;
      $pageElement->hidden = 1;
      $pageElement->deleted = 0;
    } else {
			// $pageElement->setUrlConfig($urlConfig);
      // $pageElement->hidden = 1;
		}
    $pageElement->originalUrl = $fullURL;
    $pageElement->pageID = 0;
    $pageElement->additionalJS = $additionalJS;
    $pageElement->remark = $remark;
    $pageElement->save();

    $pageElementContent = new PageElementContent();

    $pageElementContent->name = "";
    $pageElementContent->connectors = [];

    // Changed
    if($source === 'experiment'){
      if($selectedExperimentId != '' && $selectedExperimentId != 'all'){
        $pageElementContent->experimentID = (int) $selectedExperimentId;
      }
      // $pageElementContent->experimentID = -2;
      $pageElementContent->exp_status = -2;
    }else {
      if($selectedExperimentId != '' && $selectedExperimentId != 'all'){
        $pageElementContent->experimentID = (int) $selectedExperimentId;
      }else {
        $pageElementContent->experimentID = -1;
      }
      $pageElementContent->exp_status = -1;
    }

    // changed

    $pageElementContent->uid = $newUid;
    $pageElementContent->mappedTo = $xpath;
    if($contentType === 'image'){
      $pageElementContent->placementType = "4";
    }else{
      // Mantis #4556
      // $pageElementContent->placementType = ($request->input('placement_type') > -1) ? $request->input('placement_type') : 0 ;
      if ($request->input('placement_type') == "" ) {
        $pageElementContent->placementType = "5";
      }else {
        $pageElementContent->placementType = $request->input('placement_type');
      }
      // $pageElementContent->placementType = "5";
      // $pageElementContent->placementType = $request->input('placement_type',5);
    }
    $pageElementContent->order = "1";
    $pageElementContent->displayType = "1";
    $pageElementContent->addJS = "";
    $pageElementContent->startDate = null;
    $pageElementContent->endDate = null;

    if($type === "variation"){
      $pageElementContent->code = $newContent->code ;
      if($inlineContent != null && $inlineContent != ""){
        $pageElementContent->inlineContent = $inlineContent;
      }
    }

    $pageElement->content()->save($pageElementContent);

    $experiment = false;

    if($source === 'experiment'){
      $experiment = Experiment::find($experiment_id);
      if(!$experiment){
        // $experiment = new Experiment();
    		// $experiment->name = 'no name';
    		// $experiment->description = '';
        // $experiment->cg_size = 0.00;
    		// $experiment->cg_day = 0;
    		// $experiment->significant_action = 0;
    		// $experiment->account_id = Auth::user()->active_account;
    		// $experiment->deleted = 1;
    		// $experiment->hidden = 1;
        // $experiment->pageElementId = $pageElement->_id;
    		// $experiment->folder_id = null;
    		// $experiment->experimentID = Experiment::max('experimentID') + 1;
    		// $experiment->save() ;
      }
      if($experiment){
        if($pageElement->Experiment){
					// $pageElementExperiment = $pageElement->experiment;
          // Mantis #4363
          if (sizeof($pageElement->Experiment) > 0) {
            foreach($pageElement->Experiment as $found_experiment)
  					 {
  						 if ($found_experiment['experimentID'] == $experiment->experimentID) {
  							$pageElementExperiment = $found_experiment;
                $experimentFound = true;
  						 }else {
               $pageElementExperiment = new PageElementExperiment();
               }
  						}
            // for ($i=0; $i <sizeof($pageElement->experiment) ; $i++) {
            //   if ($pageElement->experiment[$i]->experimentID == $experiment->experimentID) {
            //     $pageElementExperiment = $pageElement->experiment[$i];
            //   }else {
            //   $pageElementExperiment = new PageElementExperiment();
            //   }
            // }
          }
          else {
            $pageElementExperiment = new PageElementExperiment();
          }
				} else {
					$pageElementExperiment = new PageElementExperiment();
				}
        if(!$experimentFound){
          $pageElementExperiment->controlGroupSize = $experiment->cg_size;
  				$pageElementExperiment->controlGroupDays = $experiment->cg_day;
  				$pageElementExperiment->experimentID = $experiment->experimentID;
          $pageElementExperiment->significantAction = $experiment->significant_action ;
          // new structural change
          $pageElementExperiment->hidden = $experiment->hidden;
  				if($experiment->goal){
  					$pageElementExperiment->goalID = $experiment->goal->uid;
  				}
  				if($experiment->segment){
  					$pageElementExperiment->segmentID = $experiment->segment->uid;
  				}

  				$pageElement->experiments()->save($pageElementExperiment);
        }

      }
    }

    $newContent->pageID = $pageElement->pageID;
    $newContent->xpath = $xpath;
    $newContent->code = $newContent->code;
    $newContent->save();

    return response()->json(['status'=>'success','content'=> $newContent, 'pageElement' => $pageElement, '_source' => $source, 'experiment' => $experiment], 200);
  }

  public function update(Request $request){
    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['content_id' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $variation_name = $request->input('variation_name', false);
    $type = $request->input('type', 'variation');
    $condition = $request->input('condition', false);
    $content = $request->input('content', false);
    $content_id = $request->input('content_id');
    $xpath = trim($request->input('xPath'));

    try {
        $content_id_mongo = new \MongoId($content_id);
        $newContent = Content::find($content_id_mongo);
    } catch (MongoException $ex) {
        return response()->json(['status'=>'error','msg'=> 'Invalid input.'], 400);
    }
    if(!$newContent){
      return response()->json(['status'=>'error','msg'=> 'Variation not found.'], 400);
    }
    $oldXpath = $newContent->xpath;
    $newContent->name = $variation_name === false ? $newContent->name : $variation_name;
    $newContent->condition = $condition === false ? $newContent->condition : $condition;
    $newContent->condition = $condition;
    $newContent->xpath = $xpath;
    if ($request->input('placement_type') != "") {
      $newContent->placement_type = $request->input('placement_type');
    }



    $config = $newContent->config;

    if(strpos($content,"<!--{cke_protected}{C}%3C!%2D%2D%20monoloop%20remove%20%2D%2D%3E-->&nbsp;") !== false){
      $content = "<!-- monoloop remove -->&nbsp;";
    }

    if(strtolower($xpath) == 'body'){
      $newContent->placement_type = 3;
    }

    // if($content === "<!--{cke_protected}{C}%3C!%2D%2D%20monoloop%20remove%20%2D%2D%3E-->&nbsp;"){
    //   $content = "<!-- monoloop remove -->&nbsp;";
    // }
    $config->content = $content;

    $config->setContent();

    $newContent->config()->save($config);

    $newContent->save();

    return response()->json(['status'=>'success','content'=> $newContent], 200);
  }

  public function updateCondition(Request $request){
    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['content_id' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }
    $condition = $request->input('condition', false);
    $content_id = $request->input('content_id');


    try {
        $content_id_mongo = new \MongoId($content_id);
        $newContent = Content::find($content_id_mongo);
    } catch (MongoException $ex) {
        return response()->json(['status'=>'error','msg'=> 'Invalid input.'], 400);
    }
    if(!$newContent){
      return response()->json(['status'=>'error','msg'=> 'Variation not found.'], 400);
    }

    if($condition == 'if (){|}'){
      $condition = '' ;
    }

    $newContent->condition = $condition;


    $newContent->save();

    return response()->json(['status'=>'success','content'=> $newContent], 200);
  }

  public function updateExperience(Request $request){
    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['content_id' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }
    $updated_experiment_id = $request->input('updated_experiment_id', false);
    $content_id = $request->input('content_id');


    try {
        $content_id_mongo = new \MongoId($content_id);
        $newContent = Content::find($content_id_mongo);
    } catch (MongoException $ex) {
        return response()->json(['status'=>'error','msg'=> 'Invalid input.'], 400);
    }
    if(!$newContent){
      return response()->json(['status'=>'error','msg'=> 'Variation not found.'], 400);
    }
    $pageElements = PageElement::where('content.uid',$newContent->uid)->get();

    if(count($pageElements) > 0){

      foreach ($pageElements as $pageElement) {

        foreach ($pageElement->content as $content) {
          if($content->uid == $newContent->uid){
            $content->experimentID = (int) $updated_experiment_id;
            $content->save();
          }
        }


      }


    }

    return response()->json(['status'=>'success','msg'=> 'Experience Updated'], 302);
  }

  public function destroy(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['content_id' => 'required', 'fullURL' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $fullURL = $request->input('fullURL');
    $content_id = $request->input('content_id');


    try {
        $content_id_mongo = new \MongoId($content_id);
        $newContent = Content::find($content_id_mongo);
    } catch (MongoException $ex) {
        return response()->json(['status'=>'error','msg'=> 'Invalid input.'], 400);
    }
    if(!$newContent){
      return response()->json(['status'=>'error','msg'=> 'Variation not found.'], 400);
    }
    $newContent->deleted = 1;

    $newContent->save();

    // getting page element

    $pageElement = PageElement::where('fullURL','=',$fullURL)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->whereNull('Experiment')->first();


    return response()->json(['status'=>'success','content'=> $newContent, 'pageElement' => $pageElement], 200);
  }
}
