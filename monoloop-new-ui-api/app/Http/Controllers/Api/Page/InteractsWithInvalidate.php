<?php namespace  App\Http\Controllers\Api\Page;

use DB ;
use App\Services\Frontend\Invalidator ;
use Auth ;


trait InteractsWithInvalidate
{
  private function Invalidate(){
    $invalidator = new Invalidator() ;
    $invalidator->removeCached(  Auth::user()->account->uid  . '_pageelements_' ) ;
  }
}
