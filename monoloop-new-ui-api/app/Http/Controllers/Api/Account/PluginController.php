<?php namespace App\Http\Controllers\Api\Account;
use Auth;
use MongoId;
use Illuminate\Http\Request;
use App\Http\Requests\Account\PluginUpdateRequest;

use App\Events\Logger;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Common\InteractsWithCbr ;


class PluginController extends Controller {

  use InteractsWithCbr;
  /**
   * __construct performing pre actions before all the mentioned actions
   * @return JSON
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(){

    $plugins = Auth::user()->account->plugins;
    return response()->json( array('topics' => $plugins , 'totalCount' => count($plugins) ));
  }

  public function update(PluginUpdateRequest $request , $id ){
    $ret = ['success' => true , 'msg' => ''];
    $event = $request->input('event', false);

    $plugin = Auth::user()->account->plugins->where('_id', $id)->first();
    if ($event) {
      $plugin->enabled = (bool)$request->input('enabled');
      $plugin->experience = (int)$request->input('experience');
      $plugin->audience = (int)$request->input('audience');
      $plugin->goal = (int)$request->input('goal');
    }else {
      $plugin->enabled = (bool)$request->input('enabled');
    }

    $plugin->save();

    $this->generateCbr(Auth::user()->account);

    \Event::fire(new Logger(Auth::user()->username, "plugin updated", NULL, "App\AccountPlugin"));

    return response()->json($ret);
  }
}
