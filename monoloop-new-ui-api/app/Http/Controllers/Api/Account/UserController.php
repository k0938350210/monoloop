<?php namespace App\Http\Controllers\Api\Account;
use Auth;
use MongoId;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Services\ExtjsFilter;

class UserController extends Controller {

	/**
	 * __construct performing pre actions before all the mentioned actions
	 * @return JSON
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request){
		$query = User::where('accounts.account_id',new  MongoId('5523948bf4efec040b00002b'));
		return response()->json([
			'topics' => ExtjsFilter::filter($query,$request,['email'=>'accounts.email'])->get(),
			'totalCount' => $query->count()
		]);
	}

	public function activateNewAccount(Request $request){

		$accountId = $request->input('accountId', false);

		if(!$accountId){
			return redirect('/');
		}

		$user = Auth::user();

		$user->active_account = new \MongoId($accountId);

		$user->save();


		Auth::login($user);

		return redirect('/');
	}

}
