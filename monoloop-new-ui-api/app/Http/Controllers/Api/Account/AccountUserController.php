<?php

namespace App\Http\Controllers\Api\Account;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\Logger;
use App\Events\Invitation;
use App\Account;
use App\AccountUser;
use App\User;

use Carbon;

class AccountUserController extends Controller
{
    /**
     * __construct performing pre actions before all the mentioned actions.
     *
     * @return JSON
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

  #--- restful

    public function index(Request $request)
    {
      $users = AccountUser::where('account_id', Auth::user()->account->_id)->where('deleted', 0)->get();

      // return account user array as json with their total count
      return response()->json([
        'topics' => $users,
        'totalCount' => count($users),
      ]);
    }

    public function show(Request $request, $id)
    {
        $ret = ['success' => false];

        $accountUser = AccountUser::find($id);
        if (!empty($accountUser)) {
            return response()->json($ret, 404);
        }

        $ret['success'] = true;
        $ret['accountUser'] = $accountUser;

        return response()->json($ret, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
         'firstname' => 'required',
         'lastname' => 'required',
         'email' => 'required|email',
         'confirmEmail' => 'required|email|same:email',
       ]);


        $ret = ['success' => false];
        $accountUser = AccountUser::where('email', $request->input('email'))->where('account_id', Auth::user()->account->_id)->where('deleted', 0)->first();

        if (!empty($accountUser)) {
            $ret['msg'] = 'Email '.$request->input('email').' already exists';

            return response()->json($ret, 200);
        }

        $accountUser = new AccountUser();
        $accountUser->account_id = Auth::user()->account->_id;
        $accountUser->user_id = null;
        $accountUser->email = $request->input('email');
        $accountUser->company = $request->input('firstname').' '.$request->input('lastname');
        $accountUser->name = $request->input('firstname').' '.$request->input('lastname');
        $accountUser->deleted = 0;
        $accountUser->hidden = $request->input('hidden');
        $accountUser->role = $request->input('role');
        $accountUser->dashboard_config = [];
        $accountUser->support_type = $request->input('support_type');
        if (!empty($accountUser->support_type)) {
            if (!empty($request->input('support_enddate'))) {
                $accountUser->support_enddate = date("Y-m-d", strtotime($request->input('support_enddate')));
            } else {
                // $accountUser->support_enddate = "";
            }
        } else {
            $accountUser->is_monoloop_support = 0;
        }
        if ($accountUser->is_monoloop_admin == 0) {
            $accountUser->rights = [];
            $ruleDataInput = json_decode($request->input('rulesData'));

            if (!empty($ruleDataInput)) {
              $tempRights = [];
              foreach ($ruleDataInput as $rule) {
                if (strpos($rule, 'category_') === false && strpos($rule, 'root') === false) {
                  $tempRights[] = $rule;
                }
              }
              $accountUser->rights = $tempRights;
            }
        }

        $user = User::byUsername($accountUser->email)->first();
        if($user){
          $accountUser->user_id = $user->id;
        }


        $accountUser->save();

        $ret['success'] = true;
        $ret['accountUser'] = $accountUser;
        $ret['email'] = true;
        \Event::fire(new Logger(Auth::user()->username, 'account user created', $accountUser->name, "App\AccountUser"));

        return response()->json($ret, 200);
    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
       'firstname' => 'required',
       'lastname' => 'required',
       'email' => 'required|email',
       'confirmEmail' => 'required|email|same:email',
     ]);

        $ret = ['success' => false];
        $accountUser = AccountUser::find($id);
        if (empty($accountUser)) {
            $ret['msg'] = 'An error occoured, please refresh the page.';

            return response()->json($ret, 200);
        }

        $accountExisting = AccountUser::where('email', $request->input('email'))
                                      ->where('email', '<>', $accountUser->email)
                                      ->where('account_id', Auth::user()->account->_id)
                                      ->where('deleted',0)
                                      ->first();

        if (!empty($accountExisting)) {
            $ret['msg'] = 'User '.$request->input('name').' already exists';

            return response()->json($ret, 200);
        }

        $accountUser->email = $request->input('email');

        $accountUser->company = $request->input('firstname').' '.$request->input('lastname');
        $accountUser->name = $request->input('firstname').' '.$request->input('lastname');

        $accountUser->hidden = $request->input('hidden');
        $accountUser->role = $request->input('role');
        $accountUser->dashboard_config = [];
        $accountUser->support_type = $request->input('support_type');
        if (!empty($accountUser->support_type)) {
            if (!empty($request->input('support_enddate'))) {
            // var_dump($request->input('support_enddate'),date("Y-m-d", $request->input('support_enddate')));die;
                $accountUser->support_enddate = date("Y-m-d", strtotime($request->input('support_enddate')));
            } else {
                // $accountUser->support_enddate = "";
            }
        } else {
            $accountUser->is_monoloop_support = 0;
        }

        if ($accountUser->is_monoloop_admin == 0) {
            $accountUser->rights = [];

            $ruleDataInput = json_decode($request->input('rulesData'));

            if (!empty($ruleDataInput)) {
                $tempRights = [];
                foreach ($ruleDataInput as $rule) {
                    if (strpos($rule, 'category_') === false && strpos($rule, 'root') === false) {
                        $tempRights[] = $rule;
                    }
                }
                $accountUser->rights = $tempRights;
            }
        }
        $accountUser->save();

        $ret['success'] = true;
        $ret['accountUser'] = $accountUser;
        $ret['email'] = false;
        \Event::fire(new Logger(Auth::user()->username, 'account user updated', $accountUser->name, "App\AccountUser"));

        return response()->json($ret, 200);
    }

    public function destroy(Request $request, $id)
    {
        $ret = ['success' => false];
        $accountUser = AccountUser::find($id);
        if (empty($accountUser)) {
            $ret['msg'] = 'An error occoured, please refresh the page.';

            return response()->json($ret, 200);
        }

        $accountUser->deleted = 1;
        $accountUser->save();

        $ret['success'] = true;
        $ret['accountUser'] = $accountUser;
        $ret['email'] = false;
        \Event::fire(new Logger(Auth::user()->username, 'account user deleted', $accountUser->name, "App\AccountUser"));

        return response()->json($ret, 200);
    }

  /*--- custom function ---*/

  public function statusUpdate(Request $request, $id)
  {
      $ret = ['success' => false];
      $accountUser = AccountUser::find($id);
      if (empty($accountUser)) {
          $ret['msg'] = 'An error occoured, please refresh the page.';

          return response()->json($ret, 200);
      }

      $accountUser->hidden = $request->input('hidden');
      $accountUser->save();

      $ret['success'] = true;
      $ret['accountUser'] = $accountUser;
      $ret['email'] = false;
      \Event::fire(new Logger(Auth::user()->username, 'account user status updated', $accountUser->name, "App\AccountUser"));

      return response()->json($ret, 200);
  }

    public function invite(Request $request, $id)
    {
        $ret = ['success' => false];
        $accountUser = AccountUser::find($id);
        if (empty($accountUser)) {
            return response()->json($ret, 200);
        }
        $accountUser->invitationType = 'accountUser';
        $options = [
                    'user' => $accountUser,
                    'loginAccount' => Auth::user()->account,
                    'layout' => 'emails.invitation.account.user',
                  ];
        \Event::fire(new Invitation('account_user', $options));

        $ret['success'] = true ;
        echo json_encode($ret);
        // return response()->json($ret, 200);
    }
}
