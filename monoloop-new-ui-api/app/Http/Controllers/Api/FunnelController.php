<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Funnel;
use App\Folder;
use App\Http\Requests\Funnel as FORMS;

use Input;

class FunnelController extends Controller {

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request, $type = "tree")
  {
    $ret = [];
    $ret['funnels'] = [];

    $account_id = Auth::user()->active_account;

    $folder_id = Input::get('source', NULL);

    if(!empty($folder_id)){
      $folder = explode('__', $folder_id);
      if(is_array($folder) && count($folder) === 2){
        $folder_id = $folder[1];
      }
    }

    $f = Folder::find($folder_id);
    $isRoot = false;
    if($f){
      $isRoot = empty($f->parent_id) ? true : false;
    }

    switch ($type) {
      case 'compact-list':
        $ret['funnels'] = Funnel::CompactList($account_id);
        break;
      case 'folder':
        $ret['funnels'] = Funnel::funnels($folder_id, $isRoot, $account_id);
        break;
      default:
        $folder = new Folder();
        $ret['funnels'] = $folder->funnelsFolders($account_id);
        break;
    }
    return response()->json( $ret['funnels'] ) ;
  }

  public function show()
  {
    $ret = [];
    $srcTypeId = explode("__", Input::get('_id'));

    $ret['funnel'] = Funnel::find($srcTypeId[1]);
    return response()->json( $ret['funnel'] ) ;
  }

  public function create(Request $request){
    $ret = ['success' => true , 'msg' => 'Funnel created!'];

    $srcTypeId = explode("__", $request->input('folder_id'));
    $account_id = Auth::user()->active_account;

    $funnel = new Funnel();
    $funnel->name = $request->input('name');
    $funnel->description = $request->input('description');
    $funnel->items = $this->funnalArray($request->input('items'));
    $funnel->cid = (int)Auth::user()->uid ;
    $funnel->folder_id = new \MongoId($srcTypeId[1]);
    $funnel->save();

    $ret['source'] = 'funnel';
    $ret['content']['id'] = $funnel->id;

    return response()->json($ret);
  }

  public function update(Request $request){
    $ret = ['success' => true , 'msg' => 'Funnel updated!'];

    $id = $request->input('id');

    $funnel = Funnel::findOrFail($id);
    $funnel->name = $request->input('name');
    $funnel->description = $request->input('description');
    $funnel->items = $this->funnalArray($request->input('items',[]));

    $funnel->save();

    $ret['funnel'] = $funnel;
    return response()->json($ret);
  }

  private function funnalArray($values){
    $result_array = array();

    foreach ($values as $value) {
      $result_array[] =array(
        'type' => 'audience',
        'value' => (int)$value['value']
      );
    }
    return $result_array;
  }

  public function delete(Request $request)
  {
    $ret = ['success' => true , 'msg' => 'Funnel deleted!'];

    if (strpos($request->input('_id'), '__') !== false) {
      $srcTypeId = explode('__', $request->input('_id'));
      $srcId = $srcTypeId[1];
    } else {
      $srcId = $request->input('_id');
    }

    $funnel = Funnel::find($srcId);
    $funnel->deleted = 1;
    $funnel->save();

    $ret['source'] = 'funnel';
    $ret['content']['funnel'] = $funnel;
    return response()->json($ret);
  }

  public function changeFolder(FORMS\ChangeFolderForm $request)
  {
    $ret = ['success' => true , 'msg' => 'Funnel is moved to new folder.'];

    if (strpos($request->input('_id'), '__') !== false) {
      $srcTypeId = explode('__', $request->input('_id'));
      $srcId = $srcTypeId[1];
    } else {
      $srcId = $request->input('_id');
    }

    $target = explode('__', $request->input('target'));

    if($target){
      try{
        $funnel = Funnel::find($srcId);
        $funnel->folder_id = new \MongoId($target[1]);
        $funnel->save();

        $ret['source'] = 'funnel';
        $ret['content']['funnel'] = $funnel;
      }
      catch(\Exception $e){
        // ToDo: Report Error for Invalid MongoID
        $ret['success'] = false;
        $ret['msg'] = $e-> getMessage();
      }
    }


    return response()->json($ret);
  }
}