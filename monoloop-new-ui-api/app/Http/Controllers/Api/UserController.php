<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Hasher;
use App\Account;
use App\AccountUser;
use App\OCRConfigs;
use App\Events\Logger;
use App\Http\Requests\Account\ProfileRequest;
use App\Services\Account\MailService;
use App\Http\Controllers\Common\InteractsWithCbr;

class UserController extends Controller {
	use InteractsWithCbr;
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function profile(Request $request)
	{
		$ret = ['success' => false ];

    $account = Auth::user()->active_account;
    if(empty($account)){
      return response()->json($ret, 404);
    }
    $ret['success'] = true;
    $ret['user'] = Auth::user();
		$ret['account'] = Account::find($account);
		$ret['accountUser'] = AccountUser::where('account_id', '=', $account->{'$id'})->first();
    return response()->json($ret, 200);

	}

	public function profileUpdate(ProfileRequest $request){
		$ret = ['success' => true , 'msg' => 'Save profile success.'];

		$account = Auth::user()->active_account;

		$accountUser = AccountUser::where('account_id', '=', $account->{'$id'})->first();

		// $config = Auth::user()->currentAccountConfig() ;
		// var_dump($config);die;
		$accountUser->name = $request->input('name');
		$accountUser->email = $request->input('email');
		$accountUser->save();

		$accountModel = Account::find($account);
		$accountModel->contact_name = $request->input('name');
		$accountModel->contact_email = $request->input('email');
		$accountModel->account_type = $request->input('account_type');
		$accountModel->timezone = $request->input('timezone');
		$accountModel->date_format = $request->input('date_format');
		$accountModel->time_format = $request->input('time_format');
		$accountModel->omni_channel_ready = $accountModel->omni_channel_ready == 1 ? 1 : $request->input('omni_channel_ready');
		$accountModel->cross_domain_tracking = (int)$request->input('cross_domain_tracking');
		$accountModel->legal_entity = $request->input('legal_entity');
		$accountModel->save();

		$this->generateCbr($accountModel);

		/*
		$pass = $request->input('password') ;
		if($pass != ''){
			$user = Auth::user() ;
			$hasher = new Hasher() ;
			$user->pass = $hasher->getHashedPassword($pass) ;
			$user->Save()  ;
	 	}
	 	*/

		$ocrModel = OCRConfigs::where('cid', '=', $accountModel->cid)->first();
		if($ocrModel != null){
			$ocrModel->ocReady = (boolval($accountModel->omni_channel_ready) ? true : false);
			$ocrModel->save();
		}else{
			$ocrModel = new OCRConfigs();
			$ocrModel->cid = $accountModel->cid;
			$ocrModel->ocReady = (boolval($accountModel->omni_channel_ready) ? true : false);
			$ocrModel->save();
		}


		return response()->json($ret);
	}

	public function complyGdpr(Request $request){
		$ret = ['success' => true, 'msg' => 'Save profile gdpr.'];
		$account = Auth::user()->account;
		$account->comply_gdpr = true;
		$account->legal_entity = $request->input('legal_entity');
		$account->save();

		\Event::fire(new Logger(Auth::user()->username, 'Account comply gdpr', $account->company, "App\Account"));

		// Send eamail;
		$mailService = new MailService() ;
   	$mailService->sendMonoloopElectronicSignature($account, Auth::user(),$request->ip());

		return response()->json($ret);
	}

	public function updatePassword(Request $request){
		$ret = ['success' => true];
		$pass = $request->input('password') ;
		if($pass != ''){
			$user = Auth::user() ;
			$hasher = new Hasher() ;
			$user->pass = $hasher->getHashedPassword($pass) ;
			$user->Save()  ;
	 	}

		return response()->json($ret);
	}

}
