<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ;

use App\Services\Redis\RedisService;

class StoreSessionStateController extends Controller {



	public function __construct()
	{
		$this->middleware('auth');
	}

  /*--- REST FULL ----*/

  public function store(Request $request){
		$response = array();
		$response['success'] = true;
		$response['message'] = "";
		$response['content'] = [] ;

    $state = $request->input('state', '');

    $response['state'] = $state;

    $response['key'] = $key = RedisService::getKey(Auth::user()->_id);

    $redisService = new RedisService();
    $redisService->set($key, $state);

		return response()->json($response);
  }


}
