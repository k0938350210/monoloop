<?php namespace App\Http\Controllers\Api\Content ;

use DB ;
use App\Services\MonoloopCondition ;
use App\Services\Frontend\Invalidator ;
use App\Jobs\FrontendInvalidate;
use Illuminate\Foundation\Bus\DispatchesJobs ;
use App\PageElement;
use App\PageElementContent;
use App\Content;

trait InteractsWithPageElement
{
  private function updatePageElementContent($content){
    if(!$content->hidden){
      // $db = DB::connection('mongodb');
      $pageElement = PageElement::where('pageID', (int) $content->pageID)->first();
      //var_dump($pageElement);die;
      if($pageElement){
        $pageElementContent = new PageElementContent();
        $pageElementContent->name = "";
        $pageElementContent->connectors = [];


        $pageElementContent->uid = $content->uid;
        // Changed
       $pageElementContent->experimentID = $content->experimentID;
       $pageElementContent->exp_status = $content->exp_status;
       // changed
        $pageElementContent->mappedTo = $content->xpath;
        $pageElementContent->placementType = "5";
        $pageElementContent->order = "1";
        $pageElementContent->displayType = "1";
        $pageElementContent->addJS = "";
        $pageElementContent->startDate = null;
        $pageElementContent->endDate = null;
        $pageElementContent->code = $content->code;

        $pageElement->content()->save($pageElementContent);

        $this->dispatch(new FrontendInvalidate($pageElement->cid  . '_pageelements'));
      }
    }
  }
}
