<?php namespace App\Http\Controllers\Api\Tracker;
use Auth;
use MongoId;
use Carbon;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\InteractsWithCbr ;

use App\TrackerNode ;
use App\CustomVar ;
use App\CustomField ;
use App\UrlConfig ;

class CustomVarController extends Controller {
  use InteractsWithCbr ;

	public function __construct()
	{
		$this->middleware('auth');
	}

  /*---- REST FUL -----*/

  public function store(Request $request){
    $ret = ['success' => true , 'msg' => 'Custom var created!'] ;

    $tracker = new TrackerNode();

    $customField = CustomField::uid($request->input('custom_field_uid'))->first() ;
    if($customField == null){
      $ret['success'] = false ;
      $ret['msg'] = 'Custom fileld not exists' ;
      return response()->json($ret, 200);
    }

    $tracker->account_id = Auth::user()->active_account ;
    $tracker->type = 'customvar' ;
    $tracker->hidden = 1 ;
    $tracker->deleted = 0 ;
    $tracker->nid = TrackerNode::max('nid')+1;
    $tracker->folder_id = null ;
    $tracker->tracker = null ;
    $tracker->asset = null ;
    $tracker->name = $customField->name ;
    $tracker->save() ;

    $customvar = new CustomVar() ;
    $customvar->save_function = $request->input('save_function',1) ;
    $customvar->type_id = (int)$request->input('type_id',2) ;
    $customvar->type_data = $request->input('type_data') ;
    $customvar->custom_field_id = new MongoId( $customField->_id ) ;
    $customvar->filter_type_id = 0 ;
    $customvar->filter_data = '' ;

    $urlConfig = new UrlConfig() ;
    $urlConfig->url = $request->input('url_config.url') ;
    $urlConfig->url_option = (int)$request->input('url_config.url_option',0);
    $urlConfig->inc_www = (int)$request->input('url_config.inc_www',0);
    $urlConfig->inc_http_https = (int)$request->input('url_config.inc_http_https',0);
    $urlConfig->reg_ex = $request->input('url_config.reg_ex');

    $tracker->customvar()->save( $customvar ) ;
    $tracker->urlConfig()->save( $urlConfig ) ;

    $this->generateCbr($tracker->account) ;

    return response()->json($ret, 200);
  }

  public function update(Request $request , $id){
    $ret = ['success' => true , 'msg' => 'Custom var updated!'] ;

    $customField = CustomField::uid($request->input('custom_field_uid'))->first() ;
    if($customField == null){
      $ret['success'] = false ;
      $ret['msg'] = 'Custom fileld not exists' ;
      return response()->json($ret, 200);
    }
    $tracker = TrackerNode::find($id) ;

    #validation
    $tracker->name = $customField->name ;
    $tracker->hidden = 1 ;
    $customvar = $tracker->customvar ;
    $customvar->save_function = $request->input('save_function',1) ;
    $customvar->type_id = $request->input('type_id',2) ;
    $customvar->type_data = $request->input('type_data') ;
    $customvar->custom_field_id = new MongoId( $customField->_id ) ;
    $customvar->filter_type_id = 0 ;
    $customvar->filter_data = '' ;
    $customvar->save() ;
    $urlConfig = $tracker->urlConfig ;
    $urlConfig->url = $request->input('url_config.url') ;
    $urlConfig->url_option = $request->input('url_config.url_option',0);
    $urlConfig->inc_www = $request->input('url_config.inc_www',0);
    $urlConfig->inc_http_https = $request->input('url_config.inc_http_https',0);
    $urlConfig->reg_ex = $request->input('url_config.reg_ex');
    $urlConfig->save() ;

    $tracker->save() ;

    $this->generateCbr($tracker->account) ;

    return response()->json($ret, 200);
  }
}
