<?php namespace App\Http\Controllers\Api\Tracker;
use Auth;
use MongoId;
use Carbon;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\InteractsWithCbr ;

use App\TrackerNode ;
use App\Tracker ;
use App\UrlConfig ;

class TrackerController extends Controller {
  use InteractsWithCbr ;

  public function __construct()
	{
		$this->middleware('auth');
	}

  public function store(Request $request){

    $ret = ['success' => true , 'msg' => 'Tracker created!'] ;

    $trackerNode = new TrackerNode();

    $folder_id = $request->input('folder_id') ;
    if($folder_id == 0)
      $folder_id = null ;

    $trackerNode->account_id = Auth::user()->active_account ;
    $trackerNode->type = 'tracker' ;
    $trackerNode->hidden = 1 ;
    $trackerNode->deleted = 0 ;
    $trackerNode->nid = TrackerNode::max('nid')+1;
    $trackerNode->folder_id = $folder_id ;
    $trackerNode->customvar = null ;
    $trackerNode->asset = null ;
    $trackerNode->name = $request->input('name') ;
    $trackerNode->save() ;

    $tracker = new Tracker() ;
    $tracker->type_id = (int)$request->input('type_id',2) ;
    $tracker->type_data = $request->input('type_data') ;
    $tracker->type2_id = (int)$request->input('type2_id',2) ;
    $tracker->type2_data = $request->input('type2_data') ;
    $tracker->filter_type_id = (int)$request->input('filter_type_id') ;
    $tracker->filter_data = $request->input('filter_data') ;
    $tracker->filter2_type_id = (int)$request->input('filter2_type_id') ;
    $tracker->filter2_data = $request->input('filter2_data') ;
    $tracker->asset_id = new MongoId($request->input('asset_id')) ;
    $tracker->tracker_type = 'custom' ;
    $tracker->can_delete = 1 ;

    $urlConfig = new UrlConfig() ;
    $urlConfig->url = $request->input('url_config.url') ;
    $urlConfig->url_option = (int)$request->input('url_config.url_option',0);
    $urlConfig->inc_www = (int)$request->input('url_config.inc_www',0);
    $urlConfig->inc_http_https = (int)$request->input('url_config.inc_http_https',0);
    $urlConfig->reg_ex = $request->input('url_config.reg_ex');

    $trackerNode->tracker()->save( $tracker ) ;
    $trackerNode->urlConfig()->save( $urlConfig ) ;

    $this->generateCbr($trackerNode->account) ;

    return response()->json($ret, 200);
  }

  public function update(Request $request , $id){
    $ret = ['success' => true , 'msg' => 'Tracker updated!'] ;

    $trackerNode = TrackerNode::find($id) ;
    $trackerNode->name = $request->input('name') ;
    $trackerNode->hidden = 1 ;
    $asset_id = $request->input('asset_id') ;
    $tracker = $trackerNode->tracker ;
    if($tracker->tracker_type == 'custom'){
      $tracker->type_id = (int)$request->input('type_id',2) ;
      $tracker->type_data = $request->input('type_data') ;
      $tracker->type2_id = (int)$request->input('type2_id',2) ;
      $tracker->type2_data = $request->input('type2_data') ;
      $tracker->filter_type_id = (int)$request->input('filter_type_id') ;
      $tracker->filter_data = $request->input('filter_data') ;
      $tracker->filter2_type_id = (int)$request->input('filter2_type_id') ;
      $tracker->filter2_data = $request->input('filter2_data') ;
    }elseif($tracker->tracker_type == 'product'){
      $tracker->type_id = (int)$request->input('type_id',2) ;
      $tracker->type_data = $request->input('type_data') ;
      $tracker->type2_id = (int)$request->input('type2_id',2) ;
      $tracker->type2_data = $request->input('type3_data') ;
      $tracker->type3_id = (int)$request->input('type3_id',2) ;
      $tracker->type3_data = $request->input('type2_data') ;
      $tracker->filter_type_id = (int)$request->input('filter_type_id') ;
      $tracker->filter_data = $request->input('filter_data') ;
      $tracker->filter2_type_id = (int)$request->input('filter2_type_id') ;
      $tracker->filter2_data = $request->input('filter2_data') ;
      $tracker->filter3_type_id = (int)$request->input('filter3_type_id') ;
      $tracker->filter3_data = $request->input('filter3_data') ;
    }elseif($tracker->tracker_type == 'search'){
      $tracker->search_textbox_xpath = $request->input('search_textbox_xpath') ;
      $tracker->search_button_xpath = $request->input('search_button_xpath') ;
      $tracker->filter_type_id = (int)$request->input('filter_type_id') ;
      $tracker->filter_data = $request->input('filter_data') ;
    }elseif($tracker->tracker_type == 'download'){
      $tracker->type_id = (int)$request->input('type_id',2) ;
      $tracker->type_data = $request->input('type_data') ;
      $tracker->filter_type_id = (int)$request->input('filter_type_id') ;
      $tracker->filter_data = $request->input('filter_data') ;
    }elseif($tracker->tracker_type == 'purchase'){
      $tracker->type_id = (int)$request->input('type_id',2) ;
      $tracker->type_data = $request->input('type_data') ;
    }elseif($tracker->tracker_type == 'basket'){
      $tracker->type_id = (int)$request->input('type_id',2) ;
      $tracker->type_data = $request->input('type_data') ;
      $tracker->basket = $request->input('basket') ;
    }

    if(MongoId::isValid()){
      $tracker->asset_id = new MongoId($asset_id) ;
    }

    $tracker->save() ;

    $urlConfig = $trackerNode->urlConfig ;
    $urlConfig->url = $request->input('url_config.url') ;
    $urlConfig->url_option = $request->input('url_config.url_option',0);
    $urlConfig->inc_www = $request->input('url_config.inc_www',0);
    $urlConfig->inc_http_https = $request->input('url_config.inc_http_https',0);
    $urlConfig->reg_ex = $request->input('url_config.reg_ex');
    $urlConfig->save() ;

    $trackerNode->save();

    $this->generateCbr($trackerNode->account) ;

    return response()->json($ret, 200);
  }


}
