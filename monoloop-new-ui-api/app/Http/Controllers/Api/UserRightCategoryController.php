<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserRightCategory;

use Input;

class UserRightCategoryController extends Controller {

 	/*
 	 * Action is finding all rights categories and it's associated rights
 	 *
 	 * @return Json
 	 */
	public function index()
	{

		$rights = UserRightCategory::all();
    foreach ($rights as $right) {
      $right->rights;
    }

		return response()->json([
				'topics' => $rights,
				'totalCount' => count($rights),
		]);
	}

}
