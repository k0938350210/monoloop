<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tracker as FORMS;
use App\Services\Media;
use App\Tracker;
use App\Folder;

use Input;
use Storage;
use Cache;
use Image;
use Validator;

use Carbon\Carbon ;
use App\Jobs\S3ResizingImage ;

class MediaController extends Controller {
	private $ds = '/';

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request){
		$source = $request->input('source');
		$action = $request->input('action');
		$text = $request->input('text');

		if($action == 'search'){
			return $this->searchFiles($source,$text);
		}else{
			return $this->indexFiles($source);
		}
	}

	private function searchFiles($source,$text){
		$media = new media();
		$files = $media->getFilesRecursive($source);
		$branch = [];
		$thumb_folder_name = config('lfm.thumb_folder_name');
		$resize_folder_name = config('lfm.resize_folder') ;

		foreach($files as $file){
 			if(strpos($file,$text) === false){
				continue;
			}

			if(strpos($file,'/'. $thumb_folder_name .'/') !== false){
				continue;
			}

			$is_resize_folder = false ;
			foreach($resize_folder_name as $resize){
				if(strpos($file,'/'. $resize .'/') !== false){
					$is_resize_folder = true ;
				}
			}

			if($is_resize_folder){
				continue;
			}

			$row = [] ;
			$row['id'] = config('filesystems.disks.s3.public_url') . $file ;
			$arr_files = explode($this->ds,$file);
			$row['value'] = end($arr_files);
			$row['type'] = 'image';
			$file_details = [] ;
			if(Cache::has('S3_FILE_'.$file)){
				$file_details = Cache::get('S3_FILE_'.$file);
			}else{
				$file_details['size'] = Storage::size($file);
				$file_details['mdate'] = Storage::lastModified($file);
				$expiresAt = Carbon::now()->addWeek(1);
				Cache::put('S3_FILE_'.$file , $file_details,$expiresAt);
			}

			$row['date'] = $file_details['mdate'];
			$row['size'] = $file_details['size'] ;

			$base_path = $media->basePath();
			$row['location'] = str_replace($base_path  , '', $media->getFolderFromFile($file) );

			$branch[] = $row ;
		}
		return response()->json($branch) ;
	}

	private function indexFiles($source){
		$media = new media();
		$files = $media->getFiles($source) ;

		$branch = [];
		foreach($files as $file){
			$row = [] ;
			$row['id'] = config('filesystems.disks.s3.public_url') . $file ;
			$arr_files = explode($this->ds,$file);
			$row['value'] = end($arr_files);
			$row['type'] = 'image';
			$file_details = [] ;
			if(Cache::has('S3_FILE_'.$file)){
				$file_details = Cache::get('S3_FILE_'.$file);
			}else{
				$file_details['size'] = Storage::size($file);
				$file_details['mdate'] = Storage::lastModified($file);
				$expiresAt = Carbon::now()->addWeek(1);
				Cache::put('S3_FILE_'.$file , $file_details,$expiresAt);
			}

			$row['date'] = $file_details['mdate'];
			$row['size'] = $file_details['size'] ;

			$branch[] = $row ;
		}

    return response()->json(['data' => $branch , 'parent' => $source]) ;
	}

	public function store(Request $request){
		$validator = Validator::make($request->all(), [
			'upload' => 'max:10000',
		]);

		if ($validator->fails()) {
	    return response()->json([ 'status' => 'error'  ,'data' => $validator->messages()], 200);
		}

		$target = $request->input('target');
		$file = $request->file('upload');

		$new_filename = $file->getClientOriginalName();
    $old_id = $new_filename;
		$mimetype = $file->getClientMimeType();

    if(!preg_match("/image\//",$mimetype)) {
      return response()->json([ 'status' => 'error'  ,'data' => ['upload' => ['upload file should be an image']]], 200);
    }

    $new_file_path = tempnam( sys_get_temp_dir(), 'Tux');

    if($mimetype === 'image/webp'){
      $new_file_path = $new_file_path . '.jpg';
      $path_info = pathinfo($new_filename);
      $new_filename = $path_info['filename'] . '.jpg';
    }

		$image = Image::make($file->getRealPath())
        ->orientate();


    $storePath = $target . '/' . $new_filename;
    Storage::disk('s3')->put(
      $storePath,
      (string)$image->encode(),
      'public'
    );

    $this->makeThumb($image , $target , $new_filename);

    $this->dispatch(new S3ResizingImage($target,$new_filename));

 		$ret = [
 			'folder' => $target,
 			'value' => $new_filename,
      'name' => $new_filename,
 			'id' => config('filesystems.disks.s3.public_url') .  $target . '/' . $new_filename,
      'old_id' => $old_id,
 			'type' => 'image',
 			'status' => 'server'
 		];

 		$media = new Media();
 		$media->clearCacheFiles($target);

 		return response()->json($ret);
	}

	private function makeThumb( $image , $target , $new_filename)
  {
    $storePath = $target . '/' . config('lfm.thumb_folder_name') . '/' . $new_filename ;

    // create thumb image
    $image = $image->fit(200, 200);

    Storage::disk('s3')->put(
      $storePath,
      (string)$image->encode(),
      'public'
    );
  }

  public function resizeDetail(Request $request){
  	$s3_file = $request->input('source');

    $media = new Media() ;

  	$original_image  = Image::make( $media->getReadablePath($s3_file) );
    $original_width  = $original_image->width();
    $original_height = $original_image->height();
    $ratio = 1.0;
    $scaled = false;

    $width  = $original_width;
    $height = $original_height;

    /*
    if ($original_width > 600) {
        $ratio  = 600 / $original_width;
        $width  = $original_width  * $ratio;
        $height = $original_height * $ratio;
        $scaled = true;
    } else {
        $width  = $original_width;
        $height = $original_height;
    }

    if ($height > 400) {
        $ratio  = 400 / $original_height;
        $width  = $original_width  * $ratio;
        $height = $original_height * $ratio;
        $scaled = true;
    }
    */

    return response()->json([
    	'img' => $s3_file,
    	'height' => $height,
    	'width' => $width,
    	'original_height' => $original_height,
    	'original_width' => $original_width,
    	'scaled' => $scaled,
    	'ratio' => $ratio,
    ]) ;
  }

  public function resize(Request $request){
  	$image      =  $request->input('source');

  	$dataHeight =  $request->input('dataHeight');
    $dataWidth  =  $request->input('dataWidth');
    $image_path =  $image;

    $new_file_path = tempnam( sys_get_temp_dir(), 'Tux');

    $file = $this->getFileS3Path($image);
    $media = new Media();
    $file_name = $media->getFileName($file) ;
    $folder = $media->getFolderFromFile($file);
    // crop image
    Image::make($media->getReadablePath($image) )
        ->resize($dataWidth, $dataHeight)
        ->save($new_file_path);

    $storePath = $folder. '/' . $file_name ;
    Storage::disk('s3')->put(
      $storePath,
      file_get_contents($new_file_path)
    );
    Storage::disk('s3')->setVisibility($storePath, 'public');

    $storePath = $folder . '/' . config('lfm.thumb_folder_name') . '/' . $file_name ;

    // create thumb image
    Image::make($new_file_path)
        ->fit(200, 200)
        ->save($new_file_path);

    Storage::disk('s3')->put(
      $storePath,
      file_get_contents($new_file_path)
    );
    Storage::disk('s3')->setVisibility($storePath, 'public');

    $this->dispatch(new S3ResizingImage($folder,$file_name));

 		$ret = [
 			'folder' => $folder ,
 			'value' => $file_name ,
 			'id' => $folder . '/' . $file_name ,
 			'type' => 'image' ,
 			'status' => 'server'
 		];

 		$media = new Media();
 		$media->clearCacheFile($folder . '/' . $file_name );

 		return response()->json($ret) ;
  }

  public function crop(Request $request){
  	$image      =  $request->input('source');
    $dataX      =  $request->input('dataX');
    $dataY      =  $request->input('dataY');
    $dataHeight =  $request->input('dataHeight');
    $dataWidth  =  $request->input('dataWidth');
    $image_path =  $image;

    $new_file_path = tempnam( sys_get_temp_dir(), 'Tux');

    $file = $this->getFileS3Path($image);
    $media = new Media();
    $file_name = $media->getFileName($file) ;
    $folder = $media->getFolderFromFile($file);
    // crop image
    Image::make($media->getReadablePath($image))
        ->crop($dataWidth, $dataHeight, $dataX, $dataY)
        ->save($new_file_path);

    $storePath = $folder. '/' . $file_name ;
    Storage::disk('s3')->put(
      $storePath,
      file_get_contents($new_file_path)
    );
    Storage::disk('s3')->setVisibility($storePath, 'public');

    $storePath = $folder . '/' . config('lfm.thumb_folder_name') . '/' . $file_name ;

    // create thumb image
    Image::make($new_file_path)
        ->fit(200, 200)
        ->save($new_file_path);

    Storage::disk('s3')->put(
      $storePath,
      file_get_contents($new_file_path)
    );
    Storage::disk('s3')->setVisibility($storePath, 'public');

    $this->dispatch(new S3ResizingImage($folder,$file_name));

 		$ret = [
 			'folder' => $folder ,
 			'value' => $file_name ,
 			'id' => $folder . '/' . $file_name ,
 			'type' => 'image' ,
 			'status' => 'server'
 		];

 		$media = new Media();
 		$media->clearCacheFile($folder . '/' . $file_name );

 		return response()->json($ret) ;
  }

	public function folder(Request $request){
		$uid = Auth::user()->uid ;

		$base_path = 'assets/'.$uid ;


		$media = new Media();

		$all_directories = $media->getAllDirectories($base_path);

		foreach($all_directories as &$directory){
			$directory = str_replace($base_path . '/', '', $directory);
		}

		$view_directoties = [] ;
		$thumb_folder_name = config('lfm.thumb_folder_name');
		$resize_folder_name = config('lfm.resize_folder') ;

		$ret = [] ;

		$node = [];
		$node['id'] = $base_path ;
		$node['value'] = 'Files' ;
		$node['open'] = true;
		$node['type'] = "folder";
		$node['webix_files'] = 1;
		$node['date'] = null ;
		$node['data'] = [];



		$temp = [] ;

		foreach($all_directories as $directory){
			$arr_dir = explode($this->ds, $directory);
      $file_name = end($arr_dir);

      if ($file_name !== $thumb_folder_name && ! in_array($file_name,$resize_folder_name)) {
      	$view_directoties[] = $directory ;
      	$temp2 = &$temp ;
      	foreach($arr_dir as $dir){
      		if(!isset($temp2[$dir])){
      			$temp2[$dir] = [] ;
      		}
      		$temp2 = &$temp2[$dir];
      	}
      }
		}

		$node['data'] = $this->processSubFolder($base_path ,  $temp);

		$ret[] = $node ;

		return response()->json($ret);
	}

	private function processSubFolder($parent_path , $data){
		$ret = [] ;

		foreach($data as $key => $record){
			$node = [];
			$node['id'] = $parent_path . '/' . $key ;
			$node['value'] = $key;
			$node['open'] = false;
			$node['type'] = "folder";
			$node['webix_files'] = 1;
			$node['date'] = null ;
			$node['data'] = $this->processSubFolder($parent_path.'/'.$key , $record) ;
			$ret[] = $node ;
		}

		return $ret ;
	}

	private function humanFilesize($bytes, $decimals = 2)
  {
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$size[$factor];
  }

  public function folderCreate(Request $request){
  	$value = $request->input('value');
  	$target = $request->input('target');
    $i = 1 ;
    $new_v = $value ;
    while(Storage::exists($target.'/'.$new_v)){
      $new_v = $value . '(' . $i . ')' ;
      $i++ ;
    }
  	Storage::makeDirectory($target.'/'.$new_v);
  	$media = new Media();
  	$media->clearCacheDirectoties();
  	return response()->json([
  		'id' => $target.'/'.$new_v ,
  		'value' => $new_v
  	]);
  }

  public function folderUpdate(Request $request){
  	$source = $request->input('source');
  	$target = $request->input('target');

    $media = new Media();

    $file = $this->getFileS3Path($source);
    $permission = $media->checkRenamePermission($file) ;
    $error = [] ;
    if($permission !== true){
      $error[] = $source . ' ' . $permission ;
    }

    if( count($error)){
      return response()->json(["status" => "error" , 'errors' =>  $error ] , 422 );
    }

    #$source = $media->getReadablePath($source);

  	if($this->checkFile($source)){
  		$file = $this->getFileS3Path($source);

  		$data = $media->renameFile($file,$target);
  		return response()->json([
	  		'id' => $data['url'] ,
	  		'value' => $data['value']
	  	]);
  	}else{
  		$arr_sources = explode($this->ds,$source);
	  	$arr_sources[count($arr_sources)-1] = $target ;

	  	$source_loc = $source ;
	  	$target_loc = implode($this->ds , $arr_sources);

      if(Storage::exists($target_loc)){
        return response()->json(["status" => "error" , 'errors' =>  ['Cannot rename to exists loacation'] ] , 422 );
      }

	 		//Create new folder;
	 		Storage::makeDirectory($target_loc);
	 		$files = Storage::allFiles($source_loc);
	 		foreach($files as $file){
	 			$new_loc = str_replace($source_loc, $target_loc, $file);
	 			Storage::copy($file,$new_loc);
	 		}

	 		//Delete old folder
	 		Storage::deleteDirectory($source);

	  	$media->clearCacheDirectoties();
	  	return response()->json([
	  		'id' => $target_loc ,
	  		'value' => $target
	  	]);
  	}



  }

  public function folderDelete(Request $request){
  	$sources = $request->input('source');
  	$media = new Media();

    $sources = explode(',', $sources);
    $error = [] ;

    foreach($sources as $source){
      $file = $this->getFileS3Path($source);
      $permission = $media->checkDeletePermission($file) ;
      if($permission !== true){
        $error[] = $source . ' ' . $permission ;
      }
    }

    if( count($error)){
      return response()->json(["status" => "error" , 'errors' =>  $error ] , 422 );
    }


    $filesnames = [] ;

    foreach($sources as $source){
      #$source = $media->getReadablePath($source);
      $file = $this->getFileS3Path($source);
      $file_name = $media->getFileName($file) ;
      $folder = $media->getFolderFromFile($file);

      if($this->checkFile($source)){
        $media->deleteFile($file);
      }else{
        Storage::deleteDirectory($source);
        $media->clearCacheDirectoties();
      }

      $filesnames[] = $file_name ;
    }

  	return response()->json(["STATUS" => "OK" , 'names' => $filesnames ]);
  }

  public function move(Request $request){
    $source = $request->input('source');
    $target = $request->input('target');
    $sources = explode(',',$source);
    $error = [] ;

    foreach($sources as $source){
      if(!$this->checkFile($source)){
        $error[] = 'Cannot move folder' ;
        return response()->json(["status" => "error" , 'errors' =>  $error ] , 422 );
      }

    }


    //Process move ;
    $media = new Media();

    $permission = $media->checkPermission($target);
    if($permission !== true){
      $error[] = $target . ' ' . $permission ;
    }

    if( count($error)){
      return response()->json(["status" => "error" , 'errors' =>  $error ] , 422 );
    }

    $data = []  ;

    foreach($sources as $source){
      $file = $this->getFileS3Path($source);
      $file_name = $media->getFileName($file) ;
      $folder = $media->getFolderFromFile($file);
      $data[] = [
        'folder' => $target ,
        'value' => $file_name ,
        'id' => config('filesystems.disks.s3.public_url') .  $target . '/' . $file_name ,
        'type' => 'image' ,
        'status' => 'server'
      ] ;
      if($folder == $target){
        continue ;
      }

      if(Storage::exists($file)){
        //remove file
        if(Storage::exists($target . '/' . $file_name)){
          Storage::delete($target . '/' . $file_name);
        }

        Storage::move($folder . '/' . $file_name , $target . '/' . $file_name );
        //remove thumb
        if(Storage::exists( $target. '/' . config('lfm.thumb_folder_name')  . '/' . $file_name)){
          Storage::delete( $target. '/' . config('lfm.thumb_folder_name')  . '/' . $file_name);
        }

        Storage::move($folder. '/' . config('lfm.thumb_folder_name') . '/' . $file_name , $target. '/' . config('lfm.thumb_folder_name')  . '/' . $file_name);
        //remove resize
        foreach(config('lfm.resize_folder') as $resize){
          //remove thumb
          if(Storage::exists( $target.'/'. $resize . '/' . $file_name )){
            Storage::delete(  $target.'/'. $resize . '/' . $file_name );
          }
          Storage::move($folder. '/' . $resize . '/' . $file_name , $target.'/'. $resize . '/' . $file_name );
        }
      }
      $media->clearCacheFiles($folder);
    }
    $media->clearCacheFiles($target);
    $media->clearCacheFile( $target . '/' . $file_name);
    return response()->json( $data , 200 );
  }

  private function checkFile($source){
  	if(strpos($source,'https') === 0){
  		return true ;
  	}
  	return false ;
  }

	private function getFileS3Path($source){
  	$base = config('filesystems.disks.s3.public_url') ;
  	return str_replace($base,'',$source);
  }

}