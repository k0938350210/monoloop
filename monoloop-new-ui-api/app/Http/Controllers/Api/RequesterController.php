<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ;
use Storage;


class RequesterController extends Controller {
  public function __construct(){
	}

  public function index(Request $request){
    $result = array(
      'success' => true,
      'data' => []
    );;
		$url = $request->input('url');
    try{
      $result['success'] = true;
      $result['data'] = get_headers($url, 1);
    }
    catch(\Exception $e){
      $result['success'] = false;
      $result['data'] = $e->getMessage();      
    }
    return response()->json($result);
  }
}
