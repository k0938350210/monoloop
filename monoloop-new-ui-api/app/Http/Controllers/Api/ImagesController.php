<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth ;
use Storage;


class ImagesController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

  public function index(Request $request){

		$files = Storage::files('assets/'.Auth::user()->account->uid);
		return response()->json($files);
	}
	public function upload(Request $request){

			$response = [];
			$response['status'] = 'error';
			$response['url'] = '';
			$response['msg'] = '';

			// $response['status'] = 'server';
			// $response['url'] = 'https://s3-eu-west-1.amazonaws.com/mlres/assets/1024/couch.jpg';
			// $response['msg'] = 'couch.jpg successfully uploaded: \n- Size: 119.055 KB \n- Image Width x Height: 1100 x 1100';
			// return response()->json($response);

			// HERE SET THE PATH TO THE FOLDER WITH IMAGES ON YOUR SERVER (RELATIVE TO THE ROOT OF YOUR WEBSITE ON SERVER)
			$upload_dir = '/ckeditor/plugins/uploadimage/uploads/';
			// var_dump($upload_dir);die;
			// If 1 and filename exists, RENAME file, adding "_NR" to the end of filename (name_1.ext, name_2.ext, ..)
			// If 0, will OVERWRITE the existing file
			define('RENAME_F', 1);

			// HERE PERMISSIONS FOR IMAGE
			$imgsets = array(
			 'maxsize' => 5000,     // maximum file size, in KiloBytes (2 MB)
			 'maxwidth' => 2000,     // maximum allowed width, in pixels
			 'maxheight' => 2000,    // maximum allowed height, in pixels
			 'minwidth' => 5,      // minimum allowed width, in pixels
			 'minheight' => 5,     // minimum allowed height, in pixels
			 'type' => array('bmp', 'gif', 'jpg', 'jpeg', 'png'),  // allowed extensions
			);

			$re = '';
			if(isset($_FILES['upload']) && strlen($_FILES['upload']['name']) > 1) {
			  $upload_dir = trim($upload_dir, '/') .'/';
			  define('IMG_NAME', preg_replace('/\.(.+?)$/i', '', basename($_FILES['upload']['name'])));  //get filename without extension

			  // get protocol and host name to send the absolute image path to CKEditor
			  $protocol = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';
			  $site = $protocol. $_SERVER['SERVER_NAME'] .'/';
			  $sepext = explode('.', strtolower($_FILES['upload']['name']));
			  $type = end($sepext);    // gets extension
			  list($width, $height) = getimagesize($_FILES['upload']['tmp_name']);  // gets image width and height
			  $err = '';         // to store the errors

			  //set filename; if file exists, and RENAME_F is 1, set "img_name_I"
			  // $p = dir-path, $fn=filename to check, $ex=extension $i=index to rename
			  function setFName($p, $fn, $ex, $i){
			    if(RENAME_F ==1 && file_exists($p .$fn .$ex)) return setFName($p, IMG_NAME .'_'. ($i +1), $ex, ($i +1));
			    else return $fn .$ex;
			  }

			  $img_name = setFName($_SERVER['DOCUMENT_ROOT'] .'/'. $upload_dir, IMG_NAME, ".$type", 0);
			  $uploadpath = $_SERVER['DOCUMENT_ROOT'] .'/'. $upload_dir . $img_name;  // full file path

			  // Checks if the file has allowed type, size, width and height (for images)
			  if(!in_array($type, $imgsets['type'])) $err .= 'The file: '. $_FILES['upload']['name']. ' has not the allowed extension type.';
			  if($_FILES['upload']['size'] > $imgsets['maxsize']*1000) $err .= '\\n Maximum file size must be: '. $imgsets['maxsize']. ' KB.';
			  if(isset($width) && isset($height)) {
			    if($width > $imgsets['maxwidth'] || $height > $imgsets['maxheight']) $err .= '\\n Width x Height = '. $width .' x '. $height .' \\n The maximum Width x Height must be: '. $imgsets['maxwidth']. ' x '. $imgsets['maxheight'];
			    if($width < $imgsets['minwidth'] || $height < $imgsets['minheight']) $err .= '\\n Width x Height = '. $width .' x '. $height .'\\n The minimum Width x Height must be: '. $imgsets['minwidth']. ' x '. $imgsets['minheight'];
			  }

			  // If no errors, upload the image, else, output the errors
			  if($err == '') {
					$storePath = '' ;
					if(Auth::guest()){
						$storePath = 'assets/0/'.$request->file('upload')->getClientOriginalName() ;
					}else{
						$storePath = 'assets/'.Auth::user()->account->uid.'/'.$request->file('upload')->getClientOriginalName() ;
					}
					Storage::disk('s3')->put(
		        $storePath,
		        file_get_contents($request->file('upload')->getRealPath())
		      );
		      Storage::disk('s3')->setVisibility($storePath, 'public');
		      if(Storage::disk('s3')->exists($storePath)){
						// $CKEditorFuncNum = $_GET['CKEditorFuncNum'];
			      $url = config('filesystems.disks.s3.public_url').$storePath;
						$response['url'] = $url;
			      $msg = IMG_NAME .'.'. $type .' successfully uploaded: \\n- Size: '. number_format($_FILES['upload']['size']/1024, 3, '.', '') .' KB \\n- Image Width x Height: '. $width. ' x '. $height;
						$response['msg'] = $msg;
						$response['status'] = 'server';
			      // $re = "window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')";
					}else{
						// $re = 'alert("Unable to upload the file")';
						$response['msg'] = "Unable to upload the file";
						$response['status'] = 'error';
					}
			  }
			  else {
					$response['msg'] = $err;
					$response['status'] = 'error';
				}
			}
			// echo '<script>'. $re .';</script>';
			return response()->json($response);
  }
	public function destroy(Request $request){

		$filename = 'assets/'. Auth::user()->account->uid . '/001.jpg';
		try{
			Storage::delete($filename);
		}catch(\League\Flysystem\FileNotFoundException $e ){
			return response()->json('file not found',404);
		}
		return response()->json($filename .' deleted',200);
	}
}
