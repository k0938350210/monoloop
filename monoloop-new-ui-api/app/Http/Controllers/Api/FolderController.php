<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Segment;
use App\Content;
use App\Goal;
use App\Experiment;
use App\Tracker;
use App\Folder;
use App\PageElement;

class FolderController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{
		$ret = [];
		return response()->json($ret) ;
	}

	/*
 	 * Action is createing new folder
 	 *
 	 * @return Json
 	 */
	public function create(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder created!'] ;

		$account_id = Auth::user()->active_account;

		// creating new folder
		$folder = new Folder();
		$folder->account_id = $account_id;
		$folder->deleted = 0;
		$folder->hidden = 0;
		$folder->title = $request->input('value');
		$folder->account_id = $request->input('value');
		$folder->cid = Auth::user()->account->uid;
		$folder->parent_id = new \MongoId(explode("__", $request->input('$parent'))[1]);
		$folder->created_at = new \Datetime();
		$folder->updated_at = new \Datetime();
		$folder->save();

		return response()->json( $ret ) ;
	}

	/*
 	 * Action is updating folder / segment
 	 *
 	 * @return Json
 	 */
	public function update(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder updated!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		// performing action in update (folder / segment edit) or status update
		$ret['action'] = $request->input('action');

		// cases for folder and segment
		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);

				switch ($ret['action']) {
					case 'rename':
						$folder->title = $request->input('target');
						break;
					case 'update_hidden':
						$folder->hidden = $request->input('hidden');
						break;

					default:
						# code...
						break;
				}

				$folder->updated_at = new \Datetime();
				$folder->save();
				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				switch ($ret['action']) {
					case 'rename':
            $segment->name = $request->input('name');
					  $segment->description = $request->input('desc');
						break;
					case 'update_hidden':
						$segment->hidden = $request->input('hidden');
						break;

					default:
						# code...
						break;
				}
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}


		return response()->json( $ret ) ;
	}

	/*
 	 * Action is deleting folder / segment
 	 *
 	 * @return Json
 	 */
	public function delete(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder deleted!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);
				$folder->deleted = ($request->input('action') === 'remove') ? 1 : 0;
				$folder->updated_at = new \Datetime();
				$folder->save();
				if($folder->deleted == 1){
					$segments = Segment::folderSegment($folder->_id)->get();
					foreach ($segments as $segment) {
						$segment->deleted = 1;
						$segment->save();
					}
					$contents = Content::folderContent($folder->_id)->get();
					foreach ($contents as $content) {
						$content->deleted = 1;
						$content->save();
					}
					$experiments = Experiment::folderExperiment($folder->_id)->get();
					foreach ($experiments as $experiment) {
						$experiment->deleted = 1;
						$experiment->save();
					}
					$goals = Goal::folderGoal($folder->_id)->get();
					foreach ($goals as $goal) {
						$goal->deleted = 1;
						$goal->save();
					}
					$trackers = Tracker::folderTracker($folder->_id)->get();
					foreach ($trackers as $tracker) {
						$tracker->deleted = 1;
						$tracker->save();
					}
				}

				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				$segment->deleted = ($request->input('action') === 'remove') ? 1 : 0;
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}

		return response()->json( $ret ) ;
	}

	/*
 	 * Action is moving folder / segment to folder / segment
 	 *
 	 * @return Json
 	 */
	public function move(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder deleted!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		$targetTypeId = explode("__", $request->input('target'));



		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);
				// moving folder to targeted folder
				$folder->parent_id = new \MongoId($targetTypeId[1]);
				$folder->updated_at = new \Datetime();
				$folder->save();
				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				// moving segment to targeted folder
				$segment->folder_id = new \MongoId($targetTypeId[1]);
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}

		return response()->json( $ret ) ;
	}


	public function status(Request $request){
		$res = ['success' => true, 'data' => [], 'msg' => ''];

		$folder_id = $request->input('folder_id', '');
		if($folder_id === ''){
				$res['success'] = false;
				$res['msg'] = 'Parameter \'folder_id\' is missing.';
				return response()->json($res);
		}else
			$folder_id = explode('__', $folder_id)[1];

    // check experiments against folder_id
		$experiments = Experiment::where('folder_id', new \MongoId($folder_id))->get();
		if(count($experiments))
		    foreach($experiments as $experiment)
				    $res['data']['experiments'][] = $experiment;

		// check segments against folder_id
		$segments = Segment::where('folder_id', new \MongoId($folder_id))->get();
		if(count($segments))
		    foreach($segments as $segment)
				    $res['data']['segments'][] = $segment;

		// chec goals against folder_id
		$goals = Goal::where('folder_id', new \MongoId($folder_id))->get();
		if(count($goals))
		    foreach($goals as $goal)
				    $res['data']['goals'][] = $goal;

		// check page elements against folder_id
		$page_elements = PageElement::where('folder_id', new \MongoId($folder_id))->get();
		if(count($page_elements))
		    foreach($page_elements as $page_element)
				    $res['data']['page_elements'][] = $page_element;

		// check trackers against folder_id
		$trackers = Tracker::where('folder_id', new \MongoId($folder_id))->get();
		if(count($trackers))
		    foreach($trackers as $tracker)
				    $res['data']['trackers'][] = $tracker;

		if(count($res['data']) > 0){
			  $res['success'] = false;
				$res['msg'] = 'There is data related to selected folder, deleting this folder will restrict your access to that data. Do you want to proceed?';
		}

	  return response()->json($res);
	}
}
