<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ;

use App\City;

class CityController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

  /*--- REST FULL ----*/

  public function index(Request $request){
		$response = array();
		$response['status'] = 200;
		$response['message'] = "";
		$response['content'] = [] ;

		$query = $request->input('query', "");
		$select = $request->input('select', "");
		$query = $request->input('filter.value', $query);

		if($query !== ''){
			$response['content'] = City::where('name', 'like', '%'.$query.'%')->get();
		}

		if($select == true){
			$cities = $response['content'] ;
			$content = [] ;
			foreach ($cities as $city) {
				$content[] = $city->name ;
 			}
			$response['content'] = $content ;
		}

		return response()->json($response['content']);
  }
}
