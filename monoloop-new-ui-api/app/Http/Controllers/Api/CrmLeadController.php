<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CrmLeadController extends Controller {

	public function __construct()
	{
	}

	public function typo3(Request $request){
		$typo3_username = $request->input('username') ;
		$typo3_url = $request->input('url') ;

		$url  = config('services.sugar_crm.url');
    $client = new \nusoap_client($url,'wsdl','','','','');
    $user_auth = array(
  		'user_auth' => array(
      'user_name' => config('services.sugar_crm.user'),
      'password' => md5(config('services.sugar_crm.pass')),
      'version' => '0.1'),
      'application_name' => 'Talefod'
  		);

 		$result = $client->call('login',$user_auth);
 		/*
 		$set_entry_params = array(
	    'session' => $result['id'],
	    'module_name' => 'Contacts',
	    'name_value_list'=>array(
	      array('name'=>'first_name','value'=>$typo3_url),
	      array('name'=>'last_name','value'=>$typo3_username),
	      array('name'=>'email1','value'=>''),
	      array('name'=>'lead_source','value'=>utf8_encode('Monoloop TYPO3 pre signup')),
	      array('name'=>'account_name','value'=>'')
	    )
	  );
		$result3 = $client->call('set_entry',$set_entry_params);
		$contact_id = $result3['id'];
		*/

		$set_lead_params = array(
      'session' => $result['id'],
      'module_name' => 'Leads',
      'name_value_list'=>array(
      	array('name'=>'title','value'=>$typo3_url),
        array('name'=>'first_name','value'=>$typo3_username),
        array('name'=>'last_name','value'=>''),
       	array('name'=>'email1','value'=>''),
        array('name'=>'lead_source','value'=>utf8_encode('Monoloop TYPO3 pre signup')),
			  array('name'=>'website','value'=>'')
      )
   	);
	  $res_lead = $client->call('set_entry',$set_lead_params);


		return response()->json([
			'success' => true ,
			'username' => $request->input('username') ,
			'url' => $request->input('url')
		]);
	}
}