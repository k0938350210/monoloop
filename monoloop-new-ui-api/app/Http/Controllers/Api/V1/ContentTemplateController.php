<?php namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\V1\Common\ApiFilter;
use App\Http\Controllers\Api\V1\Common\ApiLink;

use Auth ;
use Validator;
use MongoId ;

use App\ContentTemplate ;

class ContentTemplateController  extends Controller {
  use ApiFilter ;
  use ApiLink ;

  private $rawRequest ;

  public function __construct()
	{
		$this->middleware('auth.api');
	}

  private function formatRequest(Request $request){
    $order = $request->input('order','desc') ;
    $limit = $request->input('limit',20) ;
    $sortBy = $request->input('sortBy','name');
    $offset = $request->input('offset',0) ;

    $request->merge(['order'=>$order,'limit'=>$limit,'sortBy'=>$sortBy,'offset'=>$offset]);
  }

  private function rowsFormat($rows  ){
    $ret = [];
    if(count($rows)>0){
      foreach($rows as $row){
        $ret[] = $this->rowFormat($row );
      }
    }
    return $ret ;
  }

  private function rowFormat($row  ){
    return [
      'uid' => (int)$row->uid ,
      'href' => '/tmplTypes/' . (int)$row->uid . '/'  ,
      'name' => $row->name
    ];
  }

  private function getProperties($template){
    $data_structure = $template->data_structure ;
    $ret = [] ;
    foreach ($data_structure as $key => $value) {
      $ret[$value['el']] = [
        'type' => $value['type']
      ] ;
    }
    return $ret ; 
  }

  public function index(Request $request){
    $this->rawRequest = $request->all() ;
    $this->formatRequest($request) ;

    $validator = Validator::make($request->all(), [
		  'limit' => 'digits_between:0,500' ,
      'sortBy' => 'in:name' ,
      'order' => 'in:asc,desc'
		]);

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $templates = ContentTemplate::search($request->input('filter')) ;

    $totalCount = $templates->count() ;
    $this->apifilter($request,$templates) ;
    $templates = $templates->get() ;

    return response()->json([
      'status' => 'success' ,
      'totalCount' => $totalCount ,
      'next' =>  $this->getNextURL($request, '/tmplTypes/') ,
      'prev' =>  $this->getPrevURL($request, '/tmplTypes/') ,
      'tmplTypes' => $this->rowsFormat($templates ) ,
    ],200);
  }

  public function show(Request $request,$id){
    $template = ContentTemplate::byUid($id)->first() ;
    if(empty($template) ){
      return response()->json(['status' => 'error' ,'href' => '/tmplTypes/' . (int)$id . '/'  ,'message' => 'Resource not found'],400);
    }
    return response()->json([
      'href' => '/tmplTypes/' . (int)$id . '/' ,
      'uid' => $id ,
      'name' => $template->name ,
      'properties' => $this->getProperties($template)
    ],200);
  }
}
