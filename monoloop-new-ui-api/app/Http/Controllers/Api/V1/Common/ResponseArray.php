<?php namespace App\Http\Controllers\Api\V1\Common;
use MongoId ;

trait ResponseArray{
  public function contentResponse($content){
    if(is_null($content))
      return ;
    return [
      'uid' => (int)$content->uid ,
      'href' =>  '/elements/' . (int)$content->uid . '/'  ,
      'updatedAt' => $content->updated_at->toIso8601String() ,
      'createdAt' => $content->created_at->toIso8601String() ,
      'active' => !(bool)$content->hidden ,
      'name' =>  $content->name ,
      'body' => $content->config ,
      'tmplType' => $content->content_template->name
    ];
  }

}
