<?php namespace App\Http\Controllers\Api\V1\Common;
use MongoId ;
trait ApiLink{
  public function getNextURL($request , $baseApiUrl){
    $next = $baseApiUrl . '?offset=' . ($request->input('offset') + $request->input('limit')) ;
		if( isset($this->rawRequest['order'])){
			$next .= '&order=' . $this->rawRequest['order'] ;
		}
    if( isset($this->rawRequest['folder'])){
			$next .= '&folder=' . $this->rawRequest['folder'] ;
		}
		if( isset($this->rawRequest['limit'])){
			$next .= '&limit=' . $this->rawRequest['limit'] ;
		}

		if( isset($this->rawRequest['filter'])){
			$next .= '&filter=' .$this->rawRequest['filter'] ;
		}
		if( isset($this->rawRequest['sortBy'])){
			$next .= '&sortBy=' . $this->rawRequest['sortBy'] ;
		}
    return $next ;
  }

  public function getPrevURL($request,$baseApiUrl){
    if( $request->input('offset') == 0)
			return null ;
		else{
			$offset = ($request->input('offset') - $request->input('limit')) ;
			if( $offset < 0 ){
				$offset = 0 ;
			}
			$prev = $baseApiUrl . '?offset=' . $offset ;
			if( isset($this->rawRequest['order'])){
				$prev .= '&order=' . $this->rawRequest['order'] ;
			}
			if( isset($this->rawRequest['folder'])){
				$prev .= '&folder=' . $this->rawRequest['folder'] ;
			}
			if( isset($this->rawRequest['limit'])){
				$prev .= '&limit=' . $this->rawRequest['limit'] ;
			}
		  if( isset($this->rawRequest['filter'])){
				$prev .= '&filter=' . $this->rawRequest['filter'] ;
			}
			if( isset($this->rawRequest['sortBy'])){
				$prev .= '&sortBy=' . $this->rawRequest['sortBy'] ;
			}
		}
    return $prev ;
  }
}
