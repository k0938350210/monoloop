<?php namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\V1\Common\ApiFilter;
use App\Http\Controllers\Api\V1\Common\ApiLink;

use Auth ;
use Validator;
use MongoId ;

use App\Placement;
use App\UrlConfig ;
use App\Folder;

class PageController extends Controller {
  use ApiFilter ;
  use ApiLink ;

  private $rawRequest ;
  private $postRules = [
    'url' => 'required',
    'incWWW' => 'boolean' ,
    'urlOption' => 'in:exactMatch,allowParams,startsWith' ,
    'active' => 'boolean'
  ] ;

  public function __construct()
	{
		$this->middleware('auth.api');
	}

  private function formatRequest(Request $request){
    $order = $request->input('order','desc') ;
    $limit = $request->input('limit',20) ;
    $sortBy = $request->input('sortBy','created_at');
    $offset = $request->input('offset',0) ;

    $sortByMapp = [
      'updatedAt' => 'updated_at' ,
      'createdAt' => 'created_at' ,
      'active' => 'hidden' ,
      'url' => 'url_config.url' ,
      'description' => 'remark' ,
    ];

    if(isset($sortByMapp[$sortBy])){
      $sortBy = $sortByMapp[$sortBy] ;
    }

    $request->merge(['order'=>$order,'limit'=>$limit,'sortBy'=>$sortBy,'offset'=>$offset]);
  }

  private function rowsFormat($rows){
    $ret = [];
    if(count($rows)>0){
      foreach($rows as $row){
        $ret[] = $this->rowFormat($row);
      }
    }
    return $ret ;
  }

  private function rowFormat($row){
    return [
      'uid' => (int)$row->uid ,
      'href' => '/pages/' . (int)$row->uid . '/'  ,
      'updatedAt' => $row->updated_at->toIso8601String() ,
      'createdAt' => $row->created_at->toIso8601String() ,
      'active' => !(bool)$row->hidden ,
      'description' => $row->remark ,
      'additionalJS' => $row->addition_js ,
      'url' => $row->urlConfig->url ,
      'regExp' => $row->urlConfig->reg_ex ,
      'incWWW' => (bool)$row->urlConfig->inc_www ,
      'urlOption' => $row->urlConfig->optionString ,
    ];
  }

  private function nextParams($request){
    return $this->getNextURL($request,'/page/');
  }

  private function prevParams($request){
    return $this->getPrevURL($request,'/page/');
  }

  public function index(Request $request){
    $this->rawRequest = $request->all() ;
    $this->formatRequest($request) ;
    #validate
    $validator = Validator::make($request->all(), [
		  'limit' => 'digits_between:0,500' ,
      'sortBy' => 'in:uid,updated_at,created_at,hidden,remark,url_config.url' ,
      'order' => 'in:asc,desc'
		]);

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $placements = Placement::byAccount(Auth::user()->active_account)->available() ;
    $totalCount = $placements->count();
    $this->apifilter($request,$placements);
    $placements = $placements->get();
    return response()->json([
      'totalCount' => $totalCount,
      'next' =>  $this->nextParams($request),
      'prev' =>  $this->prevParams($request),
      'pages' => $this->rowsFormat($placements),
    ],200);
  }

  public function show(Request $request , $id){
    $placement = Placement::byAccount(Auth::user()->active_account)->byUid($id)->available()->first();
    if(empty($placement)){
      return response()->json(['status' => 'error' ,'href' => '/pages/'.$id ,'message' => 'Resource not found'],400);
    }
    return response()->json($this->rowFormat($placement),200);
  }


  public function store(Request $request){
    $validator = Validator::make($request->all(), $this->postRules );
    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }
    $page = new Placement();
    $page->remark = $request->input('description');
    $page->addition_js = $request->input('additionalJS');
    $page->hidden = (int)!$request->input('active',0);
    $page->folder_id = null;
    $page->deleted = 0;
    $page->account_id = Auth::User()->active_account;
    $page->uid = Placement::max('uid')+1;
    $rootFolder = Folder::root(Auth::User()->active_account)->first();
    $page->folder_id = new MongoId($rootFolder->_id);
    $page->save();

    $urlConfig =  new UrlConfig();
    $urlConfig->url = $request->input('url');
    $urlConfig->reg_ex = $request->input('regExp');
    $urlConfig->inc_www = (int)$request->input('incWWW',1);
    $urlConfig->setUrlOptionFromString($request->input('urlOption','exactMatch'));
    $page->urlConfig()->save($urlConfig);

    return response()->json(['status' => 'created' , 'uid' => $page->uid , 'href' => '/pages/' . $page->uid . '/' ],201);
  }

  public function update(Request $request , $id){
    $validator = Validator::make($request->all(), $this->postRules );
    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $placement = Placement::byAccount(Auth::user()->active_account)->byUid($id)->available()->first() ;
    if(empty($placement)){
      return response()->json(['status' => 'error' ,'href' => '/pages/'.$id ,'message' => 'Resource not found'],400);
    }

    $placement->remark = $request->input('description');
    $placement->addition_js = $request->input('additionalJS');
    $placement->hidden = (int)!$request->input('active',1);
    $placement->save() ;

    $urlConfig = $placement->urlConfig ;
    $urlConfig->url = $request->input('url') ;
    $urlConfig->reg_ex = $request->input('regExp') ;
    $urlConfig->inc_www = (int)$request->input('incWWW',1) ;
    $urlConfig->setUrlOptionFromString($request->input('urlOption','exactMatch'));
    $urlConfig->save() ;

    return response()->json(['status' => 'updated' , 'uid' => $placement->uid , 'href' => '/pages/' . $placement->uid . '/' ],200);

  }

  public function destroy(Request $request , $id){
    $placement = Placement::byAccount(Auth::user()->active_account)->byUid($id)->first() ;
    if(empty($placement)){
      return response()->json(['status' => 'error' ,'href' => '/pages/'.$id ,'message' => 'Resource not found'],400);
    }
    $placement->hidden = 1 ;
    $placement->deleted = 1 ;
    $placement->save() ;

    return response()->json(['status' => 'success' ,  'message' => 'Page deleted'],200);
  }
}
