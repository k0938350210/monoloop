<?php namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\V1\Common\ApiFilter;
use App\Http\Controllers\Api\V1\Common\ApiLink;
use App\Http\Controllers\Api\V1\Common\ResponseArray;

use Auth ;
use Storage;
use Validator;
use MongoId ;
use App\Content ;
use App\COntentConfig;
use App\ContentTemplate ;
use App\Folder ;


class ContentController extends Controller {
  use ApiFilter ;
  use ApiLink ;
  use ResponseArray ;

  private $rawRequest ;

  public function __construct()
	{
		$this->middleware('auth.api');
	}

  private $postRules = [
    'active' => 'boolean',
    'tmplType' => 'required' ,
    'name' => 'required' ,
  ] ;

  private function formatRequest(Request $request){
    $order = $request->input('order','desc') ;
    $limit = $request->input('limit',20) ;
    $sortBy = $request->input('sortBy','created_at');
    $offset = $request->input('offset',0) ;

    $sortByMapp = [
      'updatedAt' => 'updated_at' ,
      'createdAt' => 'created_at' ,
      'active' => 'hidden' ,
    ];

    if(isset($sortByMapp[$sortBy])){
      $sortBy = $sortByMapp[$sortBy] ;
    }

    $request->merge(['order'=>$order,'limit'=>$limit,'sortBy'=>$sortBy,'offset'=>$offset]);
  }

  private function rowsFormat($rows ){
    $ret = [];
    if(count($rows)>0){
      foreach($rows as $row){
        $ret[] = $this->rowFormat($row );
      }
    }
    return $ret ;
  }

  private function rowFormat($row){
    return $this->contentResponse($row);
  }

  public function index(Request $request){
    $this->rawRequest = $request->all() ;
    $this->formatRequest($request) ;
    #validate
    $validator = Validator::make($request->all(), [
		  'limit' => 'digits_between:0,500' ,
      'sortBy' => 'in:uid,updated_at,created_at,hidden,name' ,
      'order' => 'in:asc,desc'
		]);

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $contents = Content::byAccount(Auth::user()->active_account)->available();
    $totalCount = $contents->count();
    $this->apifilter($request,$contents);
    $contents = $contents->get();
    return response()->json([
      'status' => 'success',
      'totalCount' => $totalCount,
      'next' =>  $this->getNextURL($request, '/elements/'),
      'prev' =>  $this->getPrevURL($request, '/elements/'),
      'content' => $this->rowsFormat($contents)
    ],200);
  }

  public function show(Request $request , $id ){
    $content = Content::byAccount(Auth::user()->active_account)->byUid($id)->available()->first();
    if(empty($content)){
      return response()->json(['status' => 'error' ,'href' => '/elements/'.$id  . '/'  ,'message' => 'Resource not found'],400);
    }
    return response()->json($this->rowFormat($content),200);
  }

  public function store(Request $request){
    $validator = Validator::make($request->all(), $this->postRules );
    if ($validator->fails()) {
      return response()->json(['status'=>'error', 'msg'=> $validator->errors()->all()], 400);
    }

    $contentTemplate = contentTemplate::byName($request->input('tmplType'))->first();
    if(empty($contentTemplate)){
      return response()->json(['status' => 'error', 'message' => 'tmplType not exists'],400);
    }
    $content = new Content();
    $content->account_id = Auth::user()->active_account;
    $content->hidden = (int)!$request->input('active',false);
    $content->deleted = 0;
    $content->condition = '';
    $content->folder_id = null;
    $content->type = 'complex';
    //$content->config = $request->input('body') ;
    $content->name = $request->input('name');
    $content->content_template_id = new MongoId( $contentTemplate->_id );
    $content->uid = Content::max('uid')+1;
    $rootFolder = Folder::root(Auth::User()->active_account)->first();
    $content->folder_id = new MongoId($rootFolder->_id);
    $content->save();

    $config = new ContentConfig($request->input('body'));
    $content->config()->save($config);


    return response()->json(['status' => 'created' , 'uid' => $content->uid , 'href' => '/elements/' . $content->uid . '/' ],201);
  }

  public function update(Request $request , $id){
    $validator = Validator::make($request->all(), $this->postRules );
    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $content = Content::byAccount(Auth::user()->active_account)->byUid($id)->available()->first();
    if(empty($content)){
      return response()->json(['status' => 'error' ,'href' => '/elements/'.$id  . '/'  ,'message' => 'Resource not found'],400);
    }

    $contentTemplate = contentTemplate::byName($request->input('tmplType'))->first() ;
    if(empty($contentTemplate)){
      return response()->json(['status' => 'error'   ,'message' => 'tmplType not exists'],400);
    }

    $content->hidden = (int)!$request->input('active',false) ;
    $content->type = 'complex' ;
    $content->config = $request->input('body') ;
    $content->name = $request->input('name') ;
    $content->content_template_id = new MongoId( $contentTemplate->_id ) ;
    $content->save() ;
    return response()->json(['status' => 'updated' , 'uid' => $content->uid , 'href' => '/elements/' . $content->uid . '/' ],200);
  }

  public function destroy(Request $request , $id ){
    $content = Content::byAccount(Auth::user()->active_account)->byUid($id)->available()->first() ;
    if(empty($content)){
      return response()->json(['status' => 'error' ,'href' => '/elements/'.$id  . '/'  ,'message' => 'Resource not found'],400);
    }

    $content->deleted = 1 ;
    $content->save() ;

    return response()->json(['status' => 'success' ,  'message' => 'Element deleted'],200);
  }

  public function imageUpload(Request $request){
    if ($request->hasFile('image')) {
      //echo $request->file('image')->getClientOriginalName() ; //getClientOriginalName
      //die() ;
      $storePath = 'assets/'.Auth::user()->account->uid.'/'.$request->file('image')->getClientOriginalName() ;
      Storage::disk('s3')->put(
        $storePath,
        file_get_contents($request->file('image')->getRealPath())
      );
      Storage::disk('s3')->setVisibility($storePath, 'public');
      if(Storage::disk('s3')->exists($storePath)){

        return response()->json(['status' => 'created' , 'path' => config('filesystems.disks.s3.public_url').$storePath],200) ;
      }else{
        return response()->json(['status' => 'error' ,'href' => '/elements/images'  ,'message' => 'S3 process fail'],500);
      }
      die() ;
    }else{
      return response()->json(['status' => 'error' ,'href' => '/elements/images'  ,'message' => 'Images not found'],400);
    }
  }
}
