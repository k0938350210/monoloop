<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tracker as FORMS;
use App\Tracker;
use App\Folder;
use App\UrlConfig;
use App\Services\Trackers\PostProcessCode;

use Input;

class TrackerController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

 	/*
 	 * Action is finding all Trackers and associated folders of logged in user
 	 *
 	 * @return Json
 	 */
	public function index(Request $request, $type = "tree")
	{
		$ret = [];
		$ret['trackers'] = [];

		$account_id = Auth::user()->active_account;
		$account_cid = Auth::user()->account->uid;
		$folder_id = Input::get('source', NULL);

		if(!empty($folder_id)){
			$folder = explode('__', $folder_id);
			if(is_array($folder) && count($folder) === 2){
				$folder_id = $folder[1];
			}
		}

		$f = Folder::find($folder_id);
		$isRoot = false;
		if($f){
			$isRoot = empty($f->parent_id) ? true : false;
		}

		switch ($type) {
			case 'compact-list':
				$ret['trackers'] = Tracker::CompactList($account_id);
				break;
			case 'folder':
				$ret['trackers'] = Tracker::trackers($folder_id, $isRoot, $account_cid);
				break;
			default:
				$folder = new Folder();
				$ret['trackers'] = $folder->trackersFolders($account_id);
				break;
		}
		return response()->json( $ret['trackers'] ) ;
	}

	/*
	* Action is a Tracker from id
	*
	* @return Json
	*/
	public function show()
	{
		$ret = [];
		$srcTypeId = explode("__", Input::get('_id'));

		$ret['tracker'] = Tracker::find($srcTypeId[1]);
		return response()->json( $ret['tracker'] ) ;
	}

	private function checkField($field){

		$find = ['name','field_type','operator','type','selector','custom','filter'];
		if (count($find) === count(array_filter(array_keys($field), function($key) use ($find) { return in_array($key, $find); })))
		{
		  return true ;
		} else {
		  return false;
		}
	}

	/*
	* Action is creating a new Tracker
	*
	* @return Json
	*/
	public function create(Request $request){
		$ret = ['success' => true , 'msg' => 'Tracker created!'];
		$srcTypeId = explode("__", $request->input('tracker.folder_id'));


		$account_id = Auth::user()->active_account;

		$tracker = new Tracker();
		$tracker->name = $request->input('tracker.name');
		$tracker->type = $request->input('tracker.type');
		$tracker->account_id = $account_id;
		$tracker->cid = (int)Auth::user()->uid;
		$tracker->hidden = 1;
		$tracker->deleted = 0;
		$tracker->uid = Tracker::where('cid',$tracker->cid)->max('uid') + 1;
		$tracker->folder_id = new \MongoId($srcTypeId[1]);
		$tracker->tracker_events = $request->input('tracker.tracker_events');
		$tracker_fields = $request->input('tracker.tracker_fields');
		$i =1;
		$fields = [];
		if($tracker_fields != null){
			foreach($tracker_fields as $tracker_field){
				$tracker_field['uid'] = $i;
				if($this->checkField($tracker_field)){
					$fields[] = $tracker_field;
				}
				$i++;
			}
		}
		$tracker->tracker_fields = $fields;
		$tracker->save();

		$url_config = new UrlConfig();
		$url_config->url = $request->input('tracker.url_config.url');
		$url_config->remark = $request->input('tracker.url_config.remark');
		$url_config->url_option = $request->input('tracker.url_config.url_option');
		$url_config->reg_ex = $request->input('tracker.url_config.reg_ex');
		$url_config->inc_www = $request->input('tracker.url_config.inc_www');
		$url_config->inc_http_https = $request->input('tracker.url_config.inc_http_https');

		$tracker->urlConfig()->save($url_config);

		$postPorcessCode = new PostProcessCode();
    $postPorcessCode->run($tracker);
    $tracker->save();

		$ret['source'] = 'tracker';
		$ret['content']['id'] = $tracker->id;

		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a Tracker
	*
	* @return Json
	*/
	public function update(Request $request){
		$ret = ['success' => true , 'msg' => 'Tracker updated!'] ;

 		$id = $request->input('tracker.id');

		$tracker = Tracker::findOrFail($id);
		$old_tracker_fields = $tracker->tracker_fields;
		$tracker->name = $request->input('tracker.name');
		$tracker->type = $request->input('tracker.type');
		$tracker->tracker_events = $request->input('tracker.tracker_events');
		$tracker_fields = $request->input('tracker.tracker_fields',[]);
		$max_uid = null ;
		$fields = [] ;
		if(is_array($tracker_fields)){
			foreach($tracker_fields as $tracker_field){
				if(is_array($old_tracker_fields)){
					foreach($old_tracker_fields as $old_tracker_field){
						if($old_tracker_field['_id'] === $tracker_field['_id']){
							if(isset($old_tracker_field['uid']))
								$tracker_field['uid'] = $old_tracker_field['uid'];
						}
					}
				}

				if(!isset($tracker_field['uid'])){
					if(is_null($max_uid)){
						$max_uid = $tracker->maxFieldUid + 1 ;
					}else{
						$max_uid = $max_uid + 1 ;
					}
					$tracker_field['uid'] = $max_uid ;
				}

				if($this->checkField($tracker_field)){
					$fields[] = $tracker_field;
				}
			}
		}

		$tracker->tracker_fields = $fields ;

		$url_config = $tracker->urlConfig ;
		$url_config->url = $request->input('tracker.url_config.url');
		$url_config->remark = $request->input('tracker.url_config.remark');
		$url_config->url_option = $request->input('tracker.url_config.url_option');
		$url_config->reg_ex = $request->input('tracker.url_config.reg_ex');
		$url_config->inc_www = $request->input('tracker.url_config.inc_www');
		$url_config->inc_http_https = $request->input('tracker.url_config.inc_http_https');

		$url_config->save();



		$postPorcessCode = new PostProcessCode();
    $postPorcessCode->run($tracker);

    $tracker->save();

		$ret['tracker'] = $tracker;
		return response()->json( $ret ) ;
	}

	/*
	* Toggle hidden value active/inactive
	*
	* @return Json
	*/
	public function updateStatus(FORMS\StatusUpdateForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Tracker activated!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$tracker = Tracker::find($srcId);
			$tracker->hidden = (int)$request->input('hidden');
			$tracker->save();

			if($tracker->hidden == "1"){
				$ret['msg'] = 'Tracker deactivated';
			}

			$ret['source'] = 'tracker';
			$ret['content']['tracker'] = $tracker;
			return response()->json($ret);
	}

	/*
	* change folder of Tracker
	*
	* @return Json
	*/
	public function changeFolder(FORMS\ChangeFolderForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Tracker is moved to new folder.'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$target = explode('__', $request->input('target'));

			$tracker = Tracker::find($srcId);
			if(\MongoId::isValid($target[1])){
				$tracker->folder_id = new \MongoId($target[1]);
				$tracker->save();
			}
			$ret['source'] = 'tracker';
			$ret['content']['tracker'] = $tracker;
			return response()->json($ret);
	}

	/*
	* Action is deleting a Tracker
	*
	* @return Json
	*/
	public function delete(FORMS\DeleteForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Tracker deleted!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$tracker = Tracker::find($srcId);
			$tracker->deleted = 1;
			$tracker->save();

			$ret['source'] = 'tracker';
			$ret['content']['tracker'] = $tracker;
			return response()->json($ret);
	}
}
