<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Services\Profile\Profile;
use Input;
use App\Segment;
use App\Goal;
use App\Folder;
use App\Account;

use View;
use Response;
use Auth;

class ProfileController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

    /*
   * Return the properties of required condition parameters
   *
   * @return json
   */

  public function index(Request $request)
  {
      $profile = new Profile();
      // $folder = new Folder();

      $account_id = Auth::user()->active_account;
      $cid = Account::where('_id',$account_id)->lists('uid');

      $properties = $profile->getProperties();

      $segmentsProperties = [];
      $segments = Segment::CompactList($cid[0]);

      if(!empty($segments)){
        foreach ($segments as $segment) {
          if($segment->hidden == 0 && $segment->embedded == 0){
            $key = 'segment_'.$segment->uid;
            $segmentsProperties[$key] = ['key' => $key, 'Label' => $segment->name, 'Group' => 'Audiences', 'Type' => 'bool', 'Default' => true];
          }
        }
      }

      $goals = Goal::CompactList($cid[0]);

      $goalsProperties = [];

      if(!empty($goals)){
        foreach ($goals as $goal) {
          // $goalsProperties[] = array('Label' => $goal->name, 'Value' => $goal->uid);
          if($goal->hidden == 0 && $goal->embedded == 0){
            $key = 'goal'.$goal->uid;
            $segmentsProperties[$key] = ['key' => $key, 'Label' => $goal->name, 'Group' => 'Goals', 'Type' => 'bool', 'Default' => true];
          }
        }
      }

      if(count($goalsProperties) < 1){
        $properties['GoalReached']['Values'] = [];
      } else {
        $properties['GoalReached']['Values'][0]['Values'] = $goalsProperties;
      }


      return response()->json($properties + $segmentsProperties + $goalsProperties);
  }

}
