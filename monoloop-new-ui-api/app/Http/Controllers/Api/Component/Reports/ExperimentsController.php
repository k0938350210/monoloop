<?php

namespace App\Http\Controllers\Api\Component\Reports;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Page\PageController;
use Input;

use App\Experiment;


class ExperimentsController extends PageController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->viewData['selected'] = array('Dashboard','Experiment');
        return view('admin.reports.experiment',$this->viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function experimentdetail(Request $request , $id){
        $this->viewData['selected'] = array('Dashboard','Experiment');

        // $experiment = Experiment::where('experimentID','=', (int)$id)->first();
        //
        //
        // $this->viewData['_experiment_id'] = $experiment->_id;
        // $this->viewData['_experiment_hidden'] = $experiment->hidden;



        return view('admin.reports.experiment-detail',$this->viewData);

    }
}
