<?php

namespace App\Http\Controllers\Api\Component\Reports\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Experiment;
use App\ReportingArchiveAggregated as Reports;
use App\Account as Account;
use App\PageElement;
use App\Content;
use App\ExperimentHistory;
use App\Goal;
use Carbon\Carbon;
use Input;

class GoalsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getGoals(Request $request, $id)
    {
        $account_id = Auth::user()->active_account;
        $id = new \MongoId($id);
        $response = [];

        $response['goal'] = Goal::where('account_id', '=', $account_id)
            ->where('_id', '=', $id)
            ->first();

        if (!$response['goal']) {
            $response['message'] = "No experiment found";
            return response()->json($response);
        }
        $cid = Account::where('_id',$account_id)->lists('uid');
        $result = Reports::where('cid', $cid[0])
            ->whereNotNull('goals.'.$response['goal']['uid'])
            ->get();
        $response['goal']['result']=$result;
        return response()->json($response);
    }

    public function getGoalsDataTable(Request $request, $id)
    {
        $account_id = Auth::user()->active_account;
        $id = new \MongoId($id);
        $response = [];

        if(Input::has('start_date')){
            $start_date = Input::get('start_date', 0);
        }
        if(Input::has('end_date')){
            $end_date = Input::get('end_date', 0);
        }

        $goal = Goal::where('account_id', '=', $account_id)
            ->where('_id', '=', $id)
            ->first();

        if (!$goal) {
            $response['message'] = "No data found";
            return response()->json($response);
        }

        $cid = Account::where('_id',$account_id)->lists('uid');
        if(!empty($start_date) && !empty($end_date)){
          $from_date = strtotime($start_date);
          $to_date = strtotime($end_date)+ 86400;
          $result = Reports::where('cid', $cid[0])
              ->whereNotNull('goals.'.$goal->uid)
              ->where('datestamp','>=',$from_date)
              ->where('datestamp','<',$to_date)
              ->get();
        }else{
          $timestamp = Carbon::now()->subWeeks(1)->timestamp;
          $result = Reports::where('cid', $cid[0])
              ->whereNotNull('goals.'.$goal->uid)
              ->where('datestamp','>=',$timestamp)
              ->get();
        }

        // var_dump($result);

        $amount = 0;
        $totalVisits = 0;
        $response = (object)[
          "goal_name" => $goal->name,
          "goals" => 0,
          "cr" => 0,
          "vg"=> 0,
          "pvg"=> 0
        ];

        $response->goals = 0;
        $response->vg = 0;;
        $response->pvg = 0;

        // compile result here
        if(!empty($result)){
          foreach($result as $it){
            $_goals = $it->goals[$goal->uid];
            if(!empty($_goals)){
              $response->goals += (empty($_goals['amount']) ? 0 : $_goals['amount']);
              $response->vg += (empty($_goals['VisitsBeforeGoal']) ? 0 : $_goals['VisitsBeforeGoal']);
              $response->pvg += (empty($_goals['pagesBeforeGoal']) ? 0 : $_goals['pagesBeforeGoal']);
              $totalVisits += (empty($it->totalVisitsnull) ? 0 : $it->totalVisits);
            }
          }

          if($totalVisits > 0){
            $response->cr = ($response->goals / $totalVisits) * 100;
            $response->vg = $response->vg / $totalVisits;
            $response->pvg = $response->pvg / $totalVisits;
          }
        }

        return response()->json(
          array($response)
        );
    }

    /*
    * Action is to get all experiments regardless of folders
    *
    * @return Json
    */

    public function getAllGoals(Request $request){
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        return response()->json(Goal::where('account_id', '=', $account_id)
            ->where('deleted', '<>', 1)
            ->where('hidden', '<>', 1)
            ->get());
    }

    /*public function getGoalsOverview(){
        $goalId = Input::get('uid');
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '') ;

        if(empty($from_date) && empty($to_date)){
          $timestamp = Carbon::now()->subWeeks(1)->timestamp;
          $goals = Reports::where('datestamp','>=',$timestamp)
                    ->where('cid', $cid[0])
                    ->whereNotNull('goals.'.$goalId)
                    ->get();
        } else {
          $from_date = strtotime($from_date);
          $to_date = strtotime($to_date)+ 86400;
          $goals = Reports::where('cid', $cid[0])
                            ->whereNotNull('goals.'.$goalId)
                            ->where('datestamp','>=',$from_date)
                            ->where('datestamp','<',$to_date)
                            ->get();
        }
        return response()->json($goals);
    }*/

    public function getGoalsOverview(){
      $goalId = Input::get('uid');
      $account_id = Auth::user()->active_account;
      $cid = Account::where('_id',$account_id)->lists('uid');
      $from_date = Input::get('from_date', '');
      $to_date = Input::get('to_date', '') ;

      if(empty($from_date) && empty($to_date)){
        $timestamp = Carbon::now()->subWeeks(1)->timestamp;
        $goals = Reports::where('datestamp','>=',$timestamp)
                  ->where('cid', $cid[0])
                  ->whereNotNull('goals.'.$goalId)
                  ->get(['datestamp', 'goals']);
      } else {
        $from_date = strtotime($from_date);
        $to_date = strtotime($to_date)+ 86400;
        $goals = Reports::where('cid', $cid[0])
                          ->whereNotNull('goals.'.$goalId)
                          ->where('datestamp','>=',$from_date)
                          ->where('datestamp','<',$to_date)
                          ->get(['datestamp', 'goals']);
      }

      $group = array();

      foreach ($goals as $goal){

       $temp = array();
       $date = date("Y-m-d",$goal["datestamp"]);

       if(isset($group[$date])){
           $amount = (isset($goal['goals'][$goalId]['amount'])) ? $group[$date]['goals'][$goalId]['amount'] + $goal['goals'][$goalId]['amount'] : $group[$date]['goals'][$goalId]['amount'];
           $pagesBeforeGoal = (isset($goal['goals'][$goalId]['pagesBeforeGoal'])) ? $group[$date]['goals'][$goalId]['pagesBeforeGoal'] + $goal['goals'][$goalId]['pagesBeforeGoal'] : $group[$date]['goals'][$goalId]['pagesBeforeGoal'];
           $VisitsBeforeGoal = (isset($goal['goals'][$goalId]['VisitsBeforeGoal'])) ? $group[$date]['goals'][$goalId]['VisitsBeforeGoal'] + $goal['goals'][$goalId]['VisitsBeforeGoal'] : $group[$date]['goals'][$goalId]['VisitsBeforeGoal'];
           $temp = array('amount' => $amount, 'pagesBeforeGoal' => $pagesBeforeGoal, 'VisitsBeforeGoal' => $VisitsBeforeGoal);
         }else{
           $temp = array('amount' => (isset($goal['goals'][$goalId]['amount'])) ? $goal['goals'][$goalId]['amount'] : 0, 'pagesBeforeGoal' => (isset($goal['goals'][$goalId]['pagesBeforeGoal'])) ? $goal['goals'][$goalId]['pagesBeforeGoal'] : 0 , 'VisitsBeforeGoal' => (isset($goal['goals'][$goalId]['VisitsBeforeGoal'])) ? $goal['goals'][$goalId]['VisitsBeforeGoal'] : 0);
         }

       $group[$date] = array('_id' => $goal['_id'], 'datestamp' => $goal["datestamp"], 'goals' => array($goalId => $temp ));
      }

      return response()->json(array_values($group));
    }
}
