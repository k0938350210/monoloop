<?php

namespace App\Http\Controllers\Api\Component\Reports\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Experiment;
use App\Segment;
use App\ReportingArchiveAggregated as Reports;
use App\Account as Account;
use App\PageElement;
use App\Content;
use App\ExperimentHistory;
use Carbon\Carbon;
use Input;

class SegmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getSegment_backup(Request $request, $id){
        $account_id = Auth::user()->active_account;

        $response = [];

        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');

        $response['segment'] = Segment::where('account_id', '=', $account_id)
            ->where('uid', (int)$id)
            ->first();

        if(!$response['segment']){
          $response['message'] = "No segment found";
          return response()->json($response);
        }

        if(empty($from_date) && empty($to_date)){
          $from_date = date('Y-m-d', strtotime("-30 days"));
          $to_date = date('Y-m-d');
        } else {
          $from_date = date('Y-m-d', strtotime($from_date));
          $to_date = date('Y-m-d', strtotime($to_date));
        }

        $from_date_unix = strtotime($from_date);
        $to_date_unix = strtotime($to_date) + 86400 ;

        $reports = Reports::whereNotNull('segments.'.$id)
                          ->where('datestamp','>=',$from_date_unix)
                          ->where('datestamp','<',$to_date_unix)
                          ->get();


        $segments = $reports->lists('segments');

        $response['segment_cur_prev_dataset'] = [];
        $node = ['segmentid'=> $response['segment']->uid, 'name'=> $response['segment']->name,'currently'=> 0, 'previously' => 0];

        $fromData = array() ;
        $toData = array() ;

        foreach($segments as $key => $seg){
            if(is_array($seg)){
                foreach($seg as $key2 => $s){

                  // calculate current value
                  if($key2 == $id){
                    $node['currently'] += isset($s['count']) ? $s['count'] : 0;
                  }



                  if(isset($s['from'])){
                    if(isset($s['from'][$id])){
                      $node['previously'] += (int)$s['from'][$id];
                    }
                  }

                  if(isset($s['from']) && count($s['from'])){
                    foreach($s['from']  as $k => $form){
                      if(! isset($fromData[$k]))
                        $fromData[$k] = 0 ;
                      $fromData[$k] += $form['count'] ;
                    }
                  }

                  if( isset($s['from'][$id]) ){
                    if( ! isset($toData[$k]))
                      if(isset($s['from'][$id])){
                        $toData[$k] = 0  ;
                        $toData[$k] += (int) $s['from'][$id];
                      }
                  }

                }
            }
        }

        if($node['previously'] != 0){
          $node['change'] = ($node['currently'] - $node['previously'])/$node['previously'] * 100;
        } else {
          $node['change'] = ($node['currently'] - $node['previously'])/1 * 100;
        }
        $node['change'] = number_format($node['change'],2);
        array_push($response['segment_cur_prev_dataset'], $node);
        $response['data'] = [];
        foreach($fromData as $k => $v){
          $response['data'][$k]['uid'] = $k ;
          $response['data'][$k]['I_1'] = $v ;
        }
        foreach($toData as $k => $v){
          $response['data'][$k]['uid'] = $k ;
          $response['data'][$k]['E_1'] = $v ;
        }
        foreach($response['data'] as $k => &$v){
          $uid = $v['uid'];

          $segment = Segment::where('uid','=', (int)$uid)->first();
          $response['data'][$k]['segment'] = $segment ;
          $response['data'][$k]['name'] = !empty($segment) ? $segment->name: '' ;
          if(!isset($v['E_1']))
              $v['E_1'] = 0 ;
          if(!isset($v['I_1']))
              $v['I_1'] = 0 ;
        }
        $response['selected'] = $id;

        $response['from_timestamp'] = $from_date;
        $response['to_timestamp'] = $to_date;

        return response()->json($response);
    }

    public function getSegment(Request $request, $id){
        $account_id = Auth::user()->active_account;

        $response = [];
        $inAndOut = [];

        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');

        $response['segment'] = Segment::where('account_id', '=', $account_id)
            ->where('uid', '=', (int)$id)
            ->first();

        if(!$response['segment']){
          $response['message'] = "No segment found";
          return response()->json($response);
        }

        if(empty($from_date) && empty($to_date)){
          $from_date = date('Y-m-d', strtotime("-30 days"));
          $to_date = date('Y-m-d');
        } else {
          $cur_from = Carbon::createFromTimestamp(strtotime($from_date));
          $cur_to = Carbon::createFromTimestamp(strtotime($to_date));

          $from_date = date('Y-m-d', strtotime($from_date));
          $to_date = date('Y-m-d', strtotime($to_date));
        }

        $_days_diff = $cur_from->diffInDays($cur_to) + 1;
        $prev_from = $cur_from->copy()->subDays((int) $_days_diff);
        $prev_to = $cur_to->copy()->subDays((int) $_days_diff);


        $from_date_unix = strtotime($from_date);
        $to_date_unix = strtotime($to_date) + 86400 ;

        $dates = array(
          'current_from' => $cur_from->timestamp,
          'current_from_' => $cur_from,
          'current_to' => $cur_to->timestamp,
          'current_to_' => $cur_to,
          'previous_from' => $prev_from->timestamp,
          'previous_from_' => $prev_from,
          'previous_to' => $prev_to->timestamp,
          'previous_to_' => $prev_to,
          'c'=> $from_date_unix,
          't'=> $to_date_unix
        );
        $response['d'] = $dates;



        $currentData = Reports::whereNotNull('segments.'.$id)
                          ->where('datestamp','>=', $dates['current_from'])
                          ->where('datestamp','<',($dates['current_to'] + 86400))
                          ->get();
        $previousData = Reports::whereNotNull('segments.'.$id)
                          ->where('datestamp','>=', $dates['previous_from'])
                          ->where('datestamp','<',($dates['previous_to'] + 86400))
                          ->get();


        $segments = $currentData->lists('segments');
        $previousSegments = $previousData->lists('segments');
        $response['segment_cur_prev_dataset'] = [];
        $node = ['segmentid'=> $response['segment']->uid, 'name'=> $response['segment']->name,'currently'=> 0, 'previously' => 0];

        $fromData = array() ;
        $toData = array() ;

        foreach($segments as $key => $seg){
            if(is_array($seg)){
                foreach($seg as $key2 => $s){

                  // calculate current value
                  if($key2 == $id){
                    $node['currently'] += isset($s['count']) ? $s['count'] : 0;

                    // calculate IN Segment
                    if(isset($s['from']) && count($s['from'])){
                      foreach($s['from']  as $k => $_from){
                        $_segment = Segment::where('uid','=', (int)$k)->first();
                        if($_segment->embedded === 0){ // include segments  which aren't related to any experiment
                          if(!isset($inAndOut[$k])){
                            $inAndOut[$k] = [];
                            $inAndOut[$k]['E_1'] = 0;
                            $inAndOut[$k]['I_1'] = 0;
                            $inAndOut[$k]['uid'] = $k;
                            $inAndOut[$k]['segment'] = $_segment;
                            $inAndOut[$k]['name'] = $inAndOut[$k]['segment']['name'];
                          }

                          $inAndOut[$k]['E_1'] += (int) $_from;
                          $inAndOut[$k]['I_1'] += 0;
                          $inAndOut[$k]['uid'] = $k;
                        }
                      }
                    }

                    // calculate OUT Segment
                    if(isset($s['to']) && count($s['to'])){
                      foreach($s['to']  as $k => $_to){
                        $_segment = Segment::where('uid','=', (int)$k)->first();
                        if($_segment->embedded === 0){
                          if(!isset($inAndOut[$k])){
                            $inAndOut[$k] = [];
                            $inAndOut[$k]['E_1'] = 0;
                            $inAndOut[$k]['I_1'] = 0;
                            $inAndOut[$k]['uid'] = $k;
                            $inAndOut[$k]['segment'] = $_segment;
                            $inAndOut[$k]['name'] = $inAndOut[$k]['segment']['name'];
                          }

                          $inAndOut[$k]['E_1'] += 0;
                          $inAndOut[$k]['I_1'] += (int) $_to;
                          $inAndOut[$k]['uid'] = $k;
                        }
                      }
                    }
                  }
                }
            }
        }

        foreach($previousSegments as $pkey => $pseg){
            if(is_array($pseg)){
                foreach($pseg as $pkey2 => $ps){
                  // calculate current value
                  if($pkey2 == $id){
                    $node['previously'] += isset($ps['count']) ? $ps['count'] : 0;
                  }
                }
            }
        }

        if($node['previously'] != 0){
          $node['change'] = ($node['currently'] - $node['previously'])/$node['previously'] * 100;
        } else {
          $node['change'] = ($node['currently'] - $node['previously'])/1 * 100;
        }
        $node['change'] = number_format($node['change'],2);
        array_push($response['segment_cur_prev_dataset'], $node);

        $response['in_out_chart'] = $inAndOut;
        return response()->json($response);
    }

}
