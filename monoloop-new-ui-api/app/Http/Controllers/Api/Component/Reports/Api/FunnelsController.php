<?php

namespace App\Http\Controllers\Api\Component\Reports\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ReportingArchiveAggregated as Reports;
use App\Account;
use App\Funnel;
use App\Segment;

use Auth;
use Carbon\Carbon;
use Input;


class FunnelsController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function getFunnels(Request $request, $id)
  {
    $id = new \MongoId($id);
    $response = [];
    $cid = Auth::user()->account->uid;
    $response['funnel'] = Funnel::where('cid', '=', $cid )->where('_id', '=', $id)->first();

    if (!$response['funnel']) {
      $response['message'] = "No funnel found";
      return response()->json($response);
    }

    //Process report
    if(Input::has('start_date')){
      $start_date = Input::get('start_date', 0);
    }
    if(Input::has('end_date')){
      $end_date = Input::get('end_date', 0);
    }

    $reports = null;

    $items = $response['funnel']->items;

    if(!empty($start_date) && !empty($end_date)){
      $from_date = strtotime($start_date);
      $to_date = strtotime($end_date)+ 86400;
      $reports = Reports::where('cid', $cid)
          ->where('datestamp','>=',$from_date)
          ->where('datestamp','<',$to_date);
    }else{
      $timestamp = Carbon::now()->subWeeks(1)->timestamp;
      $reports = Reports::where('cid', $cid)->where('datestamp','>=',$timestamp);
    }
    $reports = $reports->where(function ($query) use ($items) {
      if(is_array($items)){
        foreach($items as $item){
          $query->orWhere('segments.'.$item['value'],'exists',true);
        }
      }
      return $query;
    })->get();
    $response['data'] = [];
    for($i = 0 ; $i < count($items) ; $i++){
      $val = $items[$i]['value'];
      $segment = Segment::byUid($val)->first();
      if($i == 0){
        $total = 0;
        foreach($reports as $report){
          if(isset($report->segments[$val])){
            $total += $report->segments[$val]['count'];
          }
        }
        $response['data'][] = ['label' => $segment->name ,'value' => $total];
      }else{
        $last_val = $items[$i-1]['value'];
        $total = 0;
        foreach($reports as $report){
          if(isset($report->segments[$val]['from'][$last_val])){
            $total += $report->segments[$val]['from'][$last_val];
          }
        }
        $response['data'][] = ['label' => $segment->name ,'value' => $total];
      }
    }

    // Calculate %
    for($i = 1; $i < count($response['data']);$i++){
      $current_value = $response['data'][$i]['value'];
      $pre_value = $response['data'][$i-1]['value'];
      if($pre_value){
        $percent = $current_value/$pre_value * 100;
      }else{
        $percent = 0;
      }
      $response['data'][$i]['label'] .= '(' . number_format($percent,2) . '%)' ;
    }


    return response()->json($response);
  }
}