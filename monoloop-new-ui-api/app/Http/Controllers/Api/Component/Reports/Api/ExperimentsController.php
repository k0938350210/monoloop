<?php namespace App\Http\Controllers\Api\Component\Reports\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Experiment;
use App\ReportingArchiveAggregated as Reports;
use App\Account as Account;
use App\PageElement;
use App\Content;
use App\ExperimentHistory;
use App\Services\Invocation\DomainValidator;
use App\Services\Experiment\PvalueCalculator ;

use Carbon\Carbon;

use Input;

class ExperimentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function no_of_daily_visitors($reports, $experimentID){

    }

    private function noOfEstDays($existingCR, $expectedImprovement, $chance, $no_of_variations, $avg_no_of_daily_visitors){


      $existingCR = (float) number_format($existingCR,2);
      $expectedImprovement = (float)  number_format($expectedImprovement,2);
      $avg_no_of_daily_visitors = (int) round($avg_no_of_daily_visitors);

      if($existingCR != 0 && $expectedImprovement != 0){
        $total_visitors = $no_of_variations * ($chance * (pow(sqrt($existingCR * ((1- $existingCR) < 0 ? (-1* (1- $existingCR)) : (1- $existingCR)))/($existingCR * $expectedImprovement), 2)));
      } else {
        $total_visitors = $no_of_variations * ($chance * (pow(sqrt($existingCR * ((1- $existingCR) < 0 ? (-1* (1- $existingCR)) : (1- $existingCR))), 2)));
      }
      if($avg_no_of_daily_visitors != 0){
        $estdays = (int) round($total_visitors / $avg_no_of_daily_visitors);
      } else {
        $estdays = (int) round($total_visitors);
      }


      return $estdays;

    }

    private function getLastExUpdatedTimestamp($experimentID){
      $timestamp = false;

      $timestamp = ExperimentHistory::where('experimentID', (int) $experimentID)->max('timestamp');

      if(is_null($timestamp)){
         $experiment = Experiment::where('experimentID',(int)$experimentID)->first() ;
         $date = new Carbon($experiment->updated_at);
         $timestamp  = $date->timestamp ;
      }
      $prev = $timestamp - ($timestamp % 3600);
      return $prev;
    }

    public function getExperiment(Request $request, $id){
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        $response = [];



        $response['experiment'] = Experiment::where('account_id', $account_id)
            ->where('experimentID', '=', (int)$id)
            ->first();

        if(!$response['experiment']){
          $response['message'] = "No experience found";
          return response()->json($response);
        }
        $timestamp = $this->getLastExUpdatedTimestamp($id);

        $response['from_timestamp'] = date('Y-m-d',$timestamp);
        $response['to_timestamp'] = date('Y-m-d');
        $response['updated_at'] = $timestamp;

        if($timestamp){
          $reports = Reports::where('datestamp','>=',$timestamp)
                            ->where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$id)
                            ->get();
        } else {
          $reports = Reports::whereNotNull('experiments.'.$id)
                            ->where('cid', $cid[0])
                            ->get();
        }


        $response['pages'] = 0;
        $response['variations'] = 0;
        $response['locations'] = 0;
        #Mantis 4520 Fixed, no need for PageElementID we can access with experimentID
        $pages = PageElement::where('Experiment.experimentID', $response['experiment']->experimentID)
        ->where('deleted', 0)
        ->where('hidden', 0)
        ->get();

        $added = [];
        foreach ($pages as $page) {
          $response['pages']++;
          $contents = $page->content;
          foreach ($contents as $c) {
            $contents = Content::where('uid','=',$c['uid']);
            if(!empty($contents)){
              $content = $contents->first();
              if($content != null){
                if($content->deleted === 0 && $content->hidden === 0){
                  $response['variations']++;
                  if(!in_array($c['mappedTo'], $added)){
                    $response['locations']++;
                  }
                  $added[] = $c['mappedTo'];
                }
              }
            }
          }
          $response['is_exp_running'] = 0;
          $parse = parse_url($page->originalUrl);

          $parse['host'] = str_replace('http', '', $parse['host']);
          $parse['host'] = str_replace('://', '', $parse['host']);
          $parse['host'] = str_replace('www.','',$parse['host']);

          $domain = Auth::user()->account->domains
          ->where('domain',$parse['host'])
          ->first() ;
          if($domain){
            if(isset($domain->status)){
              if($domain->status == 2){
                $response['is_exp_running'] = 1;
              }
            }else{
              $domainValidator = new DomainValidator() ;

              $domain->status = $domainValidator->checkInvocation($domain->domain) ;
              $domain->laststatus = \Carbon::now()->format('Y-m-d H:i:s') ;
              $domain->save() ;

              if($domain->status == 2){
                $response['is_exp_running'] = 1;
              }
            }
          }
          $response['page_url'] = $parse['host'];
        }

        $response['cr'] = 0;
        $response['cr_cg'] = 0;
        $response['improvement'] = 0;
        $response['p_value'] = 0;
        $response['visitors'] = 0;
        $response['cg_visitors'] = 0;
        $response['direct'] = 0;
        $response['cg_direct'] = 0;
        $records = 0;
        if(count($reports) > 0){
          foreach ($reports as $report) {
            $experiments = isset($report['experiments']) ? $report['experiments'] : [];
            if(count($experiments) > 0){
              $experiment = isset($experiments[$id]) ? $experiments[$id] : null;
              if($experiment)
              {
                $visitors = !empty($experiment['visitors']) ? $experiment['visitors'] : 0;
                $cg_visitors = !empty($experiment['cg_visitors']) ? $experiment['cg_visitors'] : 0;

                $direct = !empty($experiment['direct']) ? $experiment['direct'] : 0;
                $cg_direct = !empty($experiment['cg_direct']) ? $experiment['cg_direct'] : 0;

                if($visitors > 0){
                  $response['visitors'] += $visitors;
                }

                if($cg_visitors > 0){
                  $response['cg_visitors'] += $cg_visitors;
                }

                if($direct > 0){
                  $response['direct'] += $direct;
                }

                if($cg_direct > 0){
                  $response['cg_direct'] += $cg_direct;
                }

                $records++;
              }
            }

          }
        }

        $p_value = PvalueCalculator::p($response['visitors'], $response['direct'], $response['cg_visitors'], $response['cg_direct']);
        // $response['p_value'] = floatval($p_value);
        $response['p_value'] = floatval($p_value);

        if($response['visitors'] != 0){
          $response['cr'] = ($response['direct'] / $response['visitors']) * 100;
        } else {
          $response['cr'] = ($response['direct'] / 1) * 100;
        }

        if($response['cg_visitors'] != 0){
          $response['cr_cg'] = ($response['cg_direct'] / $response['cg_visitors']) * 100;
        } else {
          $response['cr_cg'] = ($response['cg_direct'] / 1) * 100;
        }

        if($response['cr_cg'] != 0){
          $response['improvement'] = (($response['cr'] - $response['cr_cg'])/$response['cr_cg']) * 100;
          $controlCr = $response['cr_cg']/100;
        } else {
          $controlCr = 1/100;
          $response['improvement'] = (($response['cr'] - 0)/1);
        }

        if($response['cr'] != 0){
          $expCr = $response['cr']/100;
        }else{
          $expCr = 1/100;
        }

        if($controlCr == 0){
          $percent_change = ($expCr - $controlCr);
        } else {
          $percent_change = ($expCr - $controlCr)/$controlCr;
        }

        // var_dump($controlCr, $expCr, $percent_change);die;
        $avg_no_of_daily_visitors = $response['visitors'];
        if($records != 0){
          $avg_no_of_daily_visitors = $response['visitors'] / $records;
        }

        $response['estimatedDays80PercentChance'] = $this->noOfEstDays($controlCr, $percent_change,16, $response['variations'], $avg_no_of_daily_visitors);
        $response['estimatedDays95PercentChance'] = $this->noOfEstDays($controlCr, $percent_change, 26, $response['variations'], $avg_no_of_daily_visitors);

        return response()->json($response);
    }

    /*
    * Action is to get all experiments regardless of folders
    *
    * @return Json
    */

    public function getAllExperiments(Request $request){
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        return response()->json(Experiment::where('account_id', '=', $account_id)
            ->where('deleted', '<>', 1)
            ->where('hidden', '<>', 1)
            ->get());
    }

    public function getExperimentOverview(){
        $experimentID = Input::get('eID');
        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        // var_dump($from_date,$to_date, '<br />');
        if(empty($from_date) && empty($to_date)){
          $timestamp = $this->getLastExUpdatedTimestamp($experimentID);
          $result = Reports::where('datestamp','>=',$timestamp)
                            ->where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->get();
        } else {
          $from_date = strtotime($from_date);
          $to_date = strtotime($to_date)+ 86400;
          // echo date('m/d/Y', $from_date); echo '<br />';
          $result = Reports::where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->where('datestamp','>=',$from_date)
                            ->where('datestamp','<',$to_date)
                            ->get();
        }


        // $datestamp = $result->lists('datestamp');
        $experiments = $result->lists('experiments');
        $points = array();

        $points['experiment']['cr']['name'] = 'Conversation rate';
        $points['experiment']['cr']['value'] = 0;
        $points['experiment']['visitors']['name'] = 'Visitors';
        $points['experiment']['visitors']['value'] = 0;
        $points['experiment']['conversion']['name'] = 'Conversions';
        $points['experiment']['conversion']['value'] = 0;
        $points['experiment']['improvement']['name'] = 'Improvement';
        $points['experiment']['improvement']['value'] = 0;
        $points['experiment']['p_value']['name'] = 'P-Value';
        $points['experiment']['p_value']['value'] = 0;


        $points['cg']['cr']['name'] = 'Conversation rate';
        $points['cg']['cr']['value'] = 0;
        $points['cg']['visitors']['name'] = 'Visitors';
        $points['cg']['visitors']['value'] = 0;
        $points['cg']['conversion']['name'] = 'Conversions';
        $points['cg']['conversion']['value'] = 0;
        $points['cg']['improvement']['name'] = 'Improvement';
        $points['cg']['improvement']['value'] = '-';
        $points['cg']['p_value']['name'] = 'P-Value';
        $points['cg']['p_value']['value'] = '-';

        foreach($experiments as $key => $exp){
            if(is_array($exp)){
                foreach($exp as $key2 => $controlExp){
                        // var_dump($controlExp);die;
                    // if($key2 == $experimentID) {
                      // if (array_key_exists('cg_direct', $controlExp) && array_key_exists('cg_visitors', $controlExp)) {

                        if($key2 == $experimentID){
                          if(!isset($controlExp['cg_visitors'])) $controlExp['cg_visitors'] = 0;
                          if(!isset($controlExp['cg_direct'])) $controlExp['cg_direct'] = 0;
                          if(!isset($controlExp['visitors'])) $controlExp['visitors'] = 0;
                          if(!isset($controlExp['direct'])) $controlExp['direct'] = 0;

                          $points['experiment']['visitors']['value'] += ($controlExp['visitors'] != 0 ? $controlExp['visitors'] : 0);
                          $points['experiment']['conversion']['value'] += ($controlExp['direct'] != 0 ? $controlExp['direct'] : 0);

                          $points['cg']['visitors']['value'] += ($controlExp['cg_visitors'] != 0 ? $controlExp['cg_visitors'] : 0) ;
                          $points['cg']['conversion']['value'] += ($controlExp['cg_direct'] != 0 ? $controlExp['cg_direct'] : 0);
                        }

                        // if($cr_percent2 > 0){
                        //   $improvement = ($cr_percent - $cr_percent2)/$cr_percent2 * 100;
                        // } else {
                        //   $improvement = 0;
                        // }
                        // $points['cg']['improvement']['value'] += $improvement;
                        // $p_value = PvalueCalculator::p($controlExp['visitors'], $controlExp['direct'], $controlExp['cg_visitors'], $controlExp['cg_direct']);
                        // $points['cg']['p_value']['value'] += $p_value;


                      // }
                  //  }
                }
            }
        }

        $p_value = PvalueCalculator::p($points['experiment']['visitors']['value'], $points['experiment']['conversion']['value'], $points['cg']['visitors']['value'], $points['cg']['conversion']['value']);
        $points['experiment']['p_value']['value'] = $p_value;

        if($points['experiment']['visitors']['value'] != 0){
          $points['experiment']['cr']['value'] = ($points['experiment']['conversion']['value'] / $points['experiment']['visitors']['value']) * 100;
        } else {
          $points['experiment']['cr']['value'] = ($points['experiment']['conversion']['value'] / 1) * 100;
        }

        if($points['cg']['visitors']['value'] != 0){
          $points['cg']['cr']['value'] = ($points['cg']['conversion']['value'] / $points['cg']['visitors']['value']) * 100;
        } else {
          $points['cg']['cr']['value'] = ($points['cg']['conversion']['value'] / 1) * 100;
        }

        if($points['cg']['cr']['value'] != 0){
          $improvement = ($points['experiment']['cr']['value'] - $points['cg']['cr']['value'])/$points['cg']['cr']['value'] * 100;
        } else {
          $improvement = 0;
        }

        $points['experiment']['improvement']['value'] = $improvement;

        $points['averages'] = $this->getExperimentAverages($result->lists('experimentMetrics'), $experimentID);
        // $points['averaged'] = $result;

        echo json_encode($points);

    }

    public function getControlGroups(Request $request, $id){
        $experimentID = $id;
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');

        $timestamp = $this->getLastExUpdatedTimestamp($experimentID);

        $td = Reports::where('datestamp','>=',$timestamp)
                      // ->where('cid', $cid[0])
                      ->whereNotNull('experiments.'.$experimentID)
                      // ->get();
                      ->lists('experiments');

        $controlVisitors = 0;
        $visitors = 0;
        $controlGroup = array();
        foreach($td as $key => $exp){
            if(is_array($exp)){
                foreach($exp as $key2 => $controlExp){
                  //  if($key2 == $experimentID){
                        if (array_key_exists('visitors', $controlExp)) $visitors += $controlExp['visitors'];
                        if (array_key_exists('cg_visitors', $controlExp)) $controlVisitors += $controlExp['cg_visitors'];
                  //  }

                }

            }

        }
        if(($controlVisitors + $visitors) != 0){
          $cg = $controlVisitors / ($controlVisitors + $visitors) * 100;
        } else {
          $cg = $controlVisitors / 1 * 100;
        }
        if(($controlVisitors + $visitors) != 0){
          $exp = $visitors / ($controlVisitors + $visitors) * 100;
        } else {
          $exp = $visitors / 1 * 100;
        }

        $visitors_arr = array(['label'=> 'Control ', 'value'=> $cg, 'visitors' => $controlVisitors], ['label'=> 'Experience', 'value'=> $exp, 'visitors' => $visitors]);
        return response()->json($visitors_arr);

    }

    public function getCombinedCR(){
        $experimentID = Input::get('eID');
        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        // var_dump($from_date,$to_date, '<br />');
        if(empty($from_date) && empty($to_date)){
          $timestamp = $this->getLastExUpdatedTimestamp($experimentID);
          $result = Reports::where('datestamp','>=',$timestamp)
                            ->where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->get();
        } else {
          $from_date = strtotime($from_date);
          $to_date = strtotime($to_date)+ 86400;
          // echo date('m/d/Y', $from_date); echo '<br />';
          $result = Reports::where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->where('datestamp','>=',$from_date)
                            ->where('datestamp','<',$to_date)
                            ->get();
        }


        // $datestamp = $result->lists('datestamp');
        $experiments = $result->lists('experiments');
        $points = array();


        $points['experiment']['direct']['name'] = 'Conversions (Experience)';
        $points['experiment']['direct']['value'] = 0;
        $points['experiment']['visitors']['name'] = 'Visitors (Experience)';
        $points['experiment']['visitors']['value'] = 0;
        $points['experiment']['cr']['name'] = 'Experience';
        $points['experiment']['cr']['value'] = 0;


        $points['cg']['cr']['name'] = 'Control';
        $points['cg']['cr']['value'] = 0;
        $points['cg']['visitors']['name'] = 'Visitors (Control group)';
        $points['cg']['visitors']['value'] = 0;
        $points['cg']['direct']['name'] = 'Conversions (Control group)';
        $points['cg']['direct']['value'] = 0;


        foreach($experiments as $key => $exp){
            if(is_array($exp)){
                foreach($exp as $key2 => $controlExp){
                        // var_dump($controlExp);die;
                    // if($key2 == $experimentID) {
                      if (array_key_exists('cg_direct', $controlExp) && array_key_exists('cg_visitors', $controlExp)) {

                        $points['cg']['visitors']['value'] += $controlExp['cg_visitors'];
                        $points['cg']['direct']['value'] += $controlExp['cg_direct'];
                      }
                      if (array_key_exists('direct', $controlExp) && array_key_exists('visitors', $controlExp)) {

                        $points['experiment']['visitors']['value'] += $controlExp['visitors'];
                        $points['experiment']['direct']['value'] += $controlExp['direct'];

                      }
                  //  }
                }
            }
        }
        if($points['experiment']['visitors']['value'] != 0){
          $points['experiment']['cr']['value'] = ($points['experiment']['direct']['value'] / $points['experiment']['visitors']['value']) * 100;
        } else {
          $points['experiment']['cr']['value'] = ($points['experiment']['direct']['value'] / 1) * 100;
        }

        if($points['cg']['visitors']['value'] != 0){
          $points['cg']['cr']['value'] = ($points['cg']['direct']['value'] / $points['cg']['visitors']['value']) * 100;
        } else {
          $points['cg']['cr']['value'] = ($points['cg']['direct']['value'] / 1) * 100;
        }

        echo json_encode($points);

    }

    public function getCR(){
        $experimentID = Input::get('eID');
        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        // var_dump($from_date,$to_date, '<br />');
        if(empty($from_date) && empty($to_date)){
          $timestamp = $this->getLastExUpdatedTimestamp($experimentID);
          $result = Reports::where('datestamp','>=',$timestamp)
                            ->where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->get();
        } else {
          $from_date = strtotime($from_date);
          $to_date = strtotime($to_date)+ 86400;
          // echo date('m/d/Y', $from_date); echo '<br />';
          // echo date('m/d/Y', $to_date); echo '<br />';
          $result = Reports::where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->where('datestamp','>=',$from_date)
                            ->where('datestamp','<',$to_date)
                            ->get();
        }


        $datestamp = $result->lists('datestamp');
        $experiments = $result->lists('experiments');
        $points = array();
        foreach($experiments as $key => $exp){
            if(is_array($exp)){
                foreach($exp as $key2 => $controlExp){
                        // var_dump($controlExp);die;
                    // if($key2 == $experimentID) {
                      if (array_key_exists('visitors', $controlExp) && array_key_exists('cg_direct', $controlExp) && array_key_exists('cg_visitors', $controlExp)) {

                          $conversions = ['conversions' =>  (isset($controlExp['cg_direct']) ? $controlExp['cg_direct'] : 0) + (isset($controlExp['cg_direct']) ? $controlExp['cg_direct'] : 0), 'date' => date('m/d/y',$datestamp[$key]), 'type' => 'conversions'];
                          array_push($points, $conversions);

                          $visitors = ['visitors' =>  (isset($controlExp['visitors']) ? $controlExp['visitors'] : 0) + (isset($controlExp['cg_visitors']) ? $controlExp['cg_visitors'] : 0), 'date' => date('m/d/y',$datestamp[$key]), 'type' => 'visitors'];
                          array_push($points, $visitors);

                          // conversion rate =  (no_of_direct_visitors / no_of_control_group_visitors) * 100
                          $_direct_visitors = (isset($controlExp['cg_direct']) ? $controlExp['cg_direct'] : 0);
                          $_cg_visitors = (isset($controlExp['cg_visitors'])? $controlExp['cg_visitors'] : 1);
                          if($_cg_visitors == 0){
                            $_cg_visitors = 1; // division by zero not allowed in CR calculation
                          }
                          $CR = ['cr' => ( $_direct_visitors / $_cg_visitors) * 100, 'date' => date('m/d/y',$datestamp[$key]), 'type' => 'cr'];
                          array_push($points, $CR);
                      }
                  //  }
                }
            }
        }
        echo json_encode($points);

    }

    public function getExperimentAverages($dataset, $id){
      $avgs = array(
        'experiment' => array(
          "TimeOnSite" => 0,
          "totalBouncedVisits" => 0,
          "totalPageViews" => 0,
          "totalRevisitPageViews" => 0,
          "totalRevisitTimeOnSite" => 0,
          "totalRevisitTotalTimeOnSite" => 0,
          "totalRevisits" => 0,
          "totalTimeOnSite" => 0,
          "totalVisits" => 0
        ),
        'controlGroup' => array(
          "TimeOnSite" => 0,
          "totalBouncedVisits" => 0,
          "totalPageViews" => 0,
          "totalRevisitPageViews" => 0,
          "totalRevisitTimeOnSite" => 0,
          "totalRevisitTotalTimeOnSite" => 0,
          "totalRevisits" => 0,
          "totalTimeOnSite" => 0,
          "totalVisits" => 0
        ),
        'improvement' => array(
          "TimeOnSite" => 0,
          "totalBouncedVisits" => 0,
          "totalPageViews" => 0,
          "totalRevisitPageViews" => 0,
          "totalRevisitTimeOnSite" => 0,
          "totalRevisitTotalTimeOnSite" => 0,
          "totalRevisits" => 0,
          "totalTimeOnSite" => 0,
          "totalVisits" => 0
        )
      );
      $keys = [
        "TimeOnSite",
        "totalBouncedVisits",
        "totalPageViews",
        "totalRevisitPageViews",
        "totalRevisitTimeOnSite",
        "totalRevisitTotalTimeOnSite",
        "totalRevisits",
        "totalTimeOnSite",
        "totalVisits"
      ];

      if(!empty($dataset[0])){
        for($i=0; $i<sizeof($dataset); $i++){
          foreach($dataset[$i] as $key => $value){
            if($key == $id){
              $_keys = array_keys($avgs);
              foreach($_keys as $pk){
                foreach($keys as $ck){
                  switch($pk){
                    case 'improvement': {
                      if(!empty($value['experiment'][$ck]) && !empty($value['controlGroup'][$ck])){
                        $impValue = $value['experiment'][$ck] - $value['controlGroup'][$ck];
                        $avgs[$pk][$ck] = round(($value['controlGroup'][$ck] == null) ? 0: (($impValue/$value['controlGroup'][$ck]) * 100));
                      }
                      break;
                    }
                    default: {
                      if(!empty($value[$pk][$ck])){
                        $avgs[$pk][$ck] =  $avgs[$pk][$ck] + $value[$pk][$ck];
                      }

                      // if($avgs[$pk][$ck] != null){
                      //   $avgs[$pk][$ck] =  $avgs[$pk][$ck] + ($value[$pk][$ck] == null) ? 0: $value[$pk][$ck];
                      // }
                      break;
                    }
                  }
                }
              }
            }
          }
        }

      }
      $finalDS = array();
      $TDkeys = array_keys($avgs['improvement']);

      foreach($TDkeys as $td_key){
        $finalDS[] = array(
          'name' => $td_key,
          'experiment' => round($avgs['experiment'][$td_key]),
          'controlgroup' => round($avgs['controlGroup'][$td_key]),
          'improvement' => round($avgs['improvement'][$td_key]),
        );
      }

      return $finalDS;
    }
}
