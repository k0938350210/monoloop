<?php namespace App\Http\Controllers\Api\Segment ;

use DB ;
use App\Services\MonoloopCondition ;
use App\PageElement ;
use App\Services\Frontend\Invalidator ;
use App\Jobs\FrontendInvalidate;
use Illuminate\Foundation\Bus\DispatchesJobs ;
use Carbon\Carbon;
use App\Services\Experiment\PvalueCalculator ;

trait InteractsWithPageElement
{
  Use DispatchesJobs;

  private function UpdatePageElement($segment){
    $pagElement = new PageElement() ;
    $collection = $pagElement->getTable() ;
    $db = DB::getMongoDB();

    $conditionObj = new MonoloopCondition($segment->condition);
    $conditionObj->replacePipe( '  return true ;  ') ;
    $conditionstr = ' <%  Segments[\'segment_' . $segment->uid.'\']  = function(){ '.$conditionObj .'  return false ;};  %> ';

    $res = $db->$collection->update(['content.segments.segment_' . $segment->uid => ['$exists' => true ]],
                             ['$set' => [ 'content.$.segments.segment_' . $segment->uid  => $conditionstr ]] ,
                             ['multiple' => true]);
    return ;
  }

  private function updatePValueAndTimestamp($cid, $eIDs){
    $response = array();
    $db = DB::connection('mongodb');
    $actionPerformed = false;

    foreach($eIDs as $_obj){
      // get reporting data
      $query = ['experiments' => ['$exists' => true], ('experiments.'.$_obj['id']) => ['$exists' => true], 'cid' => (int)$cid, 'datestamp' => ['$gt' => $_obj['timestamp']]];
      $data = $db->collection('ReportingArchiveAggregated')->where($query)->get();

      // compile experiment statistics
      if(count($data) > 0){
        $actionPerformed = $actionPerformed || true;
        $response[$_obj['id']] = array();

        $response[$_obj['id']]['visitors'] = 0;
        $response[$_obj['id']]['direct'] = 0;
        $response[$_obj['id']]['cg_visitors'] = 0;
        $response[$_obj['id']]['cg_direct'] = 0;
        $response[$_obj['id']]['timestamp'] = $_obj['timestamp'];

        foreach($data as $aggObj){
          $expObj = $aggObj['experiments'][$_obj['id']];

          $response[$_obj['id']]['visitors'] += empty($expObj['visitors']) ? 0 : $expObj['visitors'];
          $response[$_obj['id']]['direct'] += empty($expObj['direct']) ? 0 : $expObj['direct'];
          $response[$_obj['id']]['cg_visitors'] += empty($expObj['cg_visitors']) ? 0 : $expObj['cg_visitors'];
          $response[$_obj['id']]['cg_direct'] += empty($expObj['cg_direct']) ? 0 : $expObj['cg_direct'];
        }

        // update against experiment (pvalue & timestamp)
        $_carbon_timestamp = Carbon::parse(Carbon::now()->format('Y-m-d H:00:00'))->timestamp;
        $response[$_obj['id']]['p_value'] = PvalueCalculator::P($response[$_obj['id']]['visitors'], $response[$_obj['id']]['direct'], $response[$_obj['id']]['cg_visitors'], $response[$_obj['id']]['cg_direct']);
        $updateQuery = array(
          'Experiment' => ['$exists' => true],
          'cid' => (int) $cid,
          'Experiment.experimentID' => (int)$_obj['id']
        );
        $values = array(
          'Experiment.timestamp' => $_carbon_timestamp,
          'Experiment.pvalue' => $response[$_obj['id']]['p_value']
        );
        $response[$_obj['id']]['db_update_response'] = $db->collection('PageElements')->where($updateQuery)->update($values, ['multiple' => true]);
      }else{
        $response[$_obj['id']] = array();
        $response[$_obj['id']]['response'] = 'No reporting data found against provided values';
        $response[$_obj['id']]['query'] = $query;
      }
    }

    if($actionPerformed){
      #invalidate frontend server
      $this->dispatch(new FrontendInvalidate($cid  . '_pageelements'));
    }

    return $response;
  }

  private function togglePageElements($segment){
    $pagElement = new PageElement() ;
    $collection = $pagElement->getTable() ;
    $db = DB::getMongoDB();

    $res = $db->$collection->update(['content.segments.segment_' . $segment->uid => ['$exists' => true ]],
                             ['$set' => ['hidden' => (int)$segment->hidden, 'deleted' => (int) $segment->deleted]] ,
                             ['multiple' => true]);
    return ;
  }
}
