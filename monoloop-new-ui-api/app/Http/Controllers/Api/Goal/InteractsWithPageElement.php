<?php namespace App\Http\Controllers\Api\Goal ;

use DB ;
use App\Services\MonoloopCondition ;
use App\PageElement ;
use App\PageElementContent ;
use MongoId ;

trait InteractsWithPageElement
{

  private function UpdatePageElement($goal){
    $pageElement =  $goal->pageElement  ;
    if(is_null($pageElement)){
      //create new page element ;
      $pageElement = new PageElement() ;
    }
    $pageElement->cid = $goal->account->uid ;
    $pageElement->setUrlConfig($goal->urlConfig) ;
    $pageElement->hidden = $goal->hidden ?: 1;
    $pageElement->save();
    $content = null ;
    if( $pageElement->content() ){
      $content = $pageElement->content()->first() ;
    }


    if(is_null($content)){
      $content = new PageElementContent();
    }
    $content->goal = true ;
    $content->p = (int)$goal->point ;
    #generate content ejs ;
    $content->code = $goal->code ;

    $pageElement->content()->save($content);

    if(is_null($goal->pageElement)){
      $goal->page_element_id = new MongoId($pageElement->_id) ;
      $goal->save();
    }
  }

  private function removaPageElement($goal){
    // $pageElement =  $goal->pageElement  ;
    // if(is_null($pageElement)){
    //   return ;
    // }
    // $pageElement->removed = 1 ;
    // $pageElement->removedTS = time() ;
    // $pageElement->save();
  }
}
