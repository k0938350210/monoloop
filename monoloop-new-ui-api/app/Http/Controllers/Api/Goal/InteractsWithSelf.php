<?php namespace App\Http\Controllers\Api\Goal ;

use DB ;
use App\PageElement ;
use MongoId ;

trait InteractsWithSelf
{
  private function generateClone($source){
    $clone = $source->replicate();
    $clone->name = "Copy of ".$clone->name;
    $clone->embedded = (int) 1;

    if(!empty($source->page_element_id)){
      $sourcePE = PageElement::where('_id', new \MongoId($source->page_element_id))->first();
      if(!empty($sourcePE)){
        $clonePE = $sourcePE->replicate();
        $clonePE->save();
        $clone->page_element_id = new \MongoId($clonePE->_id);
      }
    }

    $clone->save();
    return $clone;
  }
}
