<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\MantisWs;

class Beanstalk2MantisController extends Controller
{
  public function index(Request $request){
    $payload = $request->input('payload');

    if(is_null($payload)){
      return;
    }

    $mantis = new MantisWs();

    $name = $payload['pusher_name'];
    foreach($payload['commits'] as $commit){
      $ids = $this->parseHash($commit['message']);
      foreach($ids as $id){
        $note = '['. $name . '] ' . $commit['message'] . "\n" . $commit['url'];
        $mantis->addNote($id,$note);
      }
    }
  }

   private function parseHash($text){
    preg_match_all('/(^|[^a-z0-9_])#([a-z0-9_]+)/i', $text, $matchedHashtags);
    $hashtag = '';
    // For each hashtag, strip all characters but alpha numeric
    if (!empty($matchedHashtags[0])) {
      foreach($matchedHashtags[0] as $match) {
        $hashtag .= preg_replace("/[^a-z0-9]+/i", "", $match).',';
      }
    }
    //to remove last comma in a string
    $hashtag = rtrim($hashtag, ',');
    return explode(',', $hashtag) ;
  }

}