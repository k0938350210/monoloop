<?php namespace App\Http\Controllers\Api\Guideline;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ;

use App\Guideline;
use App\Tooltip;
use App\Services\Redis\RedisService;

class IndexController extends Controller {

	const UserGuidelineState = "UserGuidelineState";

	const UserTooltipState = "UserTooltipState";

	public function __construct()
	{
		$this->middleware('auth');
	}

  /*--- REST FULL ----*/

  public function index(Request $request){
		$response = array();
		$response['status'] = 200;
		$response['message'] = "";
		$response['guidelines'] = [] ;
		$response['tooltips'] = [] ;

		$response['pageId'] = $page_id = $request->input('page_id', "");
		$response['actionID'] = $action_id = $request->input('action_id', "default");

		if($page_id !== ''){
			$response['guidelines'] = Guideline::guidelinesByPageId($page_id);
			$response['tooltips'] = Tooltip::tooltipsByPageId($page_id);
		}

		$guideline_key = RedisService::getKey(Auth::user()->_id, self::UserGuidelineState).':'.$page_id.':'.$action_id;

		$tooltip_key = RedisService::getKey(Auth::user()->_id, self::UserTooltipState).':'.$page_id;

		$redisService = new RedisService();
		$response['currentGuide'] = $redisService->get($guideline_key);
		$response['currentTip'] = $redisService->get($tooltip_key);

		return response()->json($response);
  }
}
