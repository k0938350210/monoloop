<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Segment;
use App\Tracker;
use App\Folder;

use Input;

class BlueprintController extends Controller {
	/**
	 * __construct performing pre actions before all the mentioned actions.
	 *
	 * @return JSON
	 */
	public function __construct()
	{
			$this->middleware('auth');
	}

 	/*
 	 * Action is finding all segments and associated folders of logged in user
 	 *
 	 * @return Json
 	 */
	public function index()
	{
		$ret = [];

		$account_id = Auth::user()->active_account;
		$folder = new Folder();

		$ret['records'] = $folder->blueprintsFolders($account_id);
		return response()->json( $ret['records'] ) ;
	}

	/*
	* Action is a segment from id
	*
	* @return Json
	*/
	public function show(Request $request)
	{
		$ret = [];

		$srcTypeId = explode("__", Input::get('source'));

		$ret['segment'] = Segment::find($srcTypeId[1]);
		return response()->json( $ret['segment'] ) ;
	}

	/*
	* Action is creating a new segment
	*
	* @return Json
	*/
	public function create(Request $request){
		$ret = ['success' => true , 'msg' => 'Segment created!'] ;

		$srcTypeId = explode("__", $request->input('source'));

		$account_id = Auth::user()->active_account;

		$segment = new Segment();
		$segment->name = $request->input('name');
		$segment->description = $request->input('desc');
		$segment->account_id = $account_id;
		$segment->deleted = 0;
		$segment->hidden = 0;
		$segment->folder_id = new \MongoId($srcTypeId[1]);
		$segment->save() ;

		$ret['source'] = 'segment';
		$ret['content']['id'] = $segment->id;
		$ret['content']['condition'] = $segment->condition;

		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a segment
	*
	* @return Json
	*/
	public function update(Request $request){
		$ret = ['success' => true , 'msg' => 'Segment updated!'] ;

		if(strpos($request->input('source'), '__') !== false){
			$srcTypeId = explode("__", $request->input('source'));
			$srcId = $srcTypeId[1];
		} else {
			$srcId = $request->input('source');
		}


		$segment = Segment::find($srcId);
		$segment->name = !empty($request->input('name')) ? $request->input('name') : $segment->name;
		$segment->description = !empty($request->input('desc')) ? $request->input('desc') : $segment->description;
		$segment->condition = !empty($request->input('condition')) ? $request->input('condition') : $segment->condition;
		$segment->condition_json = !empty($request->input('condition_json')) ? $request->input('condition_json') : $segment->condition_json;
		$segment->save() ;

		$ret['segment'] = $segment;
		return response()->json( $ret ) ;
	}
}
