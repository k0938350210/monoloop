<?php namespace App\Http\Controllers\Api;

use App\Events\ExperimentHistoryLog;
use App\ExperimentHistory;
use App\Segment;
use Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExperimentHistory as History;
use App\Http\Requests\Experiment as FORMS;
use App\Experiment;
use App\Tracker;
use App\Folder;
use App\PageElement;
use App\PageElementContent;
use App\PageElementExperiment;
use App\UrlConfig;
use App\Goal;
use Illuminate\Support\Collection;
use App\Events\Logger;
use App\Events\Invitation;
use Input;
use App\Http\Controllers\Api\Segment\InteractsWithPageElement;

/* traits -> experiment */
use App\Http\Controllers\Api\Experiment\InteractsWithGoal;
use App\Http\Controllers\Api\Experiment\InteractsWithSegment;
use App\Http\Controllers\Api\Experiment\InteractsWithPageElement as ExperimentInteractsWithPageElement;
use App\Http\Controllers\Api\Goal\InteractsWithSelf;

class UpdateExperiment extends Controller {

	public function __construct()
	{
		$this->middleware('auth', ['except' => 'calculatePValue']);
	}


	public function index(Request $request, $type = "tree")
	{
		$ret = ['success' => true , 'msg' => 'Experience creatred!'];
    $pages = PageElement::whereNotNull('Experiment')->get();
    $expobj = [];
		for ($i=0; $i < sizeof($pages); $i++) {
			$expobj[$i] = (Object)$pages[$i]->{'Experiment'};
    }
		for ($i=0; $i <sizeof($expobj) ; $i++) {

			$collection = collect($pages[$i]);
			$getkeys = $collection->keys();
			for ($j=0; $j < sizeof($getkeys); $j++) {
				if ($getkeys[$j] == "Experiment") {
					$collection->forget($getkeys[$j]);
				}
				if ($getkeys[$j] == "experiment") {
					$collection->forget($getkeys[$j]);
				}
			}
			$pageElementExperiment = new PageElementExperiment();
			$collection_Exp = collect($expobj[$i]);
			foreach ($collection_Exp as $key => $value) {
				$pageElementExperiment->{$key} = $value;
			}
			$pageElement = PageElement::find($pages[$i]->_id);
			// delete that page element
			$pageElement->delete($pages[$i]->_id);

			$pageElement = new PageElement();
			foreach ($collection as $key => $value) {
				$pageElement->{$key} = $value;
			}
	    $pageElement->save();
			$pageElement->experiments()->save($pageElementExperiment);

		}

		return response()->json( $pages ) ;
	}
}
