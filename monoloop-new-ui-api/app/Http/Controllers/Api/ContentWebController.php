<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Segment;
use App\Tracker;
use App\Folder;
use App\PageElement;
use App\Account;
use App\UrlConfig;

use Auth ;
use Validator;
use MongoId ;
use Input;

class ContentWebController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

 	/*
 	 * Action is finding all page elements and associated folders of logged in user
 	 *
 	 * @return Json
 	 */
	public function index(Request $request, $type = "tree")
	{
		$ret = [];

		$account_id = Auth::user()->active_account;
		$account_cid = Auth::user()->account->uid;

		$folder_id = Input::get('source', NULL);
		$url = Input::get('url',null);

		if(!empty($folder_id)){
			$folder = explode('__', $folder_id);
			if(is_array($folder) && count($folder) === 2){
				$folder_id = $folder[1];
			}
		}

		$f = Folder::find($folder_id);
		$isRoot = false;
		if($f){
			$isRoot = empty($f->parent_id) ? true : false;
		}

		switch ($type) {
			case 'compact-list':
				break;
			case 'folder':
				// echo 'Test plan';
				// echo $url;
				$ret['records'] = PageElement::pages($folder_id, $isRoot, $account_cid, $url);
				break;
			default:
				$folder = new Folder();
				$ret['records'] = $folder->placementsFolders($account_cid);
				break;
		}


		return response()->json( $ret['records'] ) ;
	}

	/*
	* show page element
	*
	* @return Json
	*/
	public function show(Request $request)
	{
		$ret = [];

		$srcTypeId = explode("__", Input::get('_id'));

		$ret['pageElement'] = PageElement::find($srcTypeId[1]);
		return response()->json( $ret['pageElement'] ) ;
	}

	/*
	* create page element
	*
	* @return Json
	*/
	public function create(Request $request){
		$ret = ['status' => 'success' , 'msg' => 'Page created!'] ;

		$active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['fullURLWebPageList' => 'required'] );
		$srcTypeId = explode("__", $request->input('source'));

    if ($validator->fails() || !$srcTypeId) {
			$ret['status'] = 'error';
			$ret['msg'] = $validator->errors()->all();
      return response()->json($ret, 400);
    }

		$fullURL = $request->input('fullURLWebPageList');
    $regExp = $request->input('regularExpressionWebPageElement', '');
    $inWWW = (int)$request->input('includeWWWWebPageElement', 0);
    $inHTTP_HTTPS = (int) $request->input('includeHttpHttpsWebPageElement', 0);
    $urlOption = $request->input('urlOptionWebPageElement', '');
    $additionalJS = $request->input('additionalJSWebPageElement', '');
    $remark = $request->input('remarkWebPageList', '');


		$urlConfig = new UrlConfig();
		$urlConfig->reg_ex = $regExp;
		$urlConfig->url = $fullURL;
		$urlConfig->url_option = $urlOption;
		$urlConfig->inc_www = $inWWW;
		$urlConfig->inc_http_https = $inHTTP_HTTPS;
		$urlConfig->urlHash = $urlConfig->getUrlHashAttribute();
		$urlConfig->cleanUrl = $urlConfig->getCleanUrlAttribute();

		$urlConfig->setUrlOptionFromString($urlConfig->url_option);

		$pe = new PageElement();
		$pe->setUrlConfig($urlConfig);
		$pageElement = PageElement::where('fullURL','=',$pe->fullURL)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->first();

    if(!$pageElement){
      $pageElement = $pe;
			$pageElement->cid = $active_account->uid;
    } else {
			$pageElement->setUrlConfig($urlConfig);
		}
		$pageElement->originalUrl = $fullURL;
    $pageElement->pageID = 0;
    $pageElement->hidden = 0;
    $pageElement->deleted = 0;
    $pageElement->additionalJS = $additionalJS;
    $pageElement->remark = $remark;
    $pageElement->save();

		$ret['source'] = 'webpaglelist';
		$ret['content'] = $pageElement;

		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a segment
	*
	* @return Json
	*/
	public function update(Request $request){
		$ret = ['status' => 'success' , 'msg' => 'Page updated!'] ;

		$active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['fullURLWebPageList' => 'required'] );
		$srcId = $request->input('_id', false);

    if ($validator->fails() || !$srcId) {
			$ret['status'] = 'error';
			$ret['msg'] = $validator->errors()->all();
      return response()->json($ret, 400);
    }

		$fullURL = $request->input('fullURLWebPageList');
    $regExp = $request->input('regularExpressionWebPageElement', '');
    $inWWW = (int)$request->input('includeWWWWebPageElement', 0);
    $inHTTP_HTTPS = (int) $request->input('includeHttpHttpsWebPageElement', 0);
    $urlOption = $request->input('urlOptionWebPageElement', '');
    $additionalJS = $request->input('additionalJSWebPageElement', '');
    $remark = $request->input('remarkWebPageList', '');


		$urlConfig = new UrlConfig();
		$urlConfig->reg_ex = $regExp;
		$urlConfig->url = $fullURL;
		$urlConfig->url_option = $urlOption;
		$urlConfig->inc_www = $inWWW;
		$urlConfig->inc_http_https = $inHTTP_HTTPS;
		$urlConfig->urlHash = $urlConfig->getUrlHashAttribute();
		$urlConfig->cleanUrl = $urlConfig->getCleanUrlAttribute();

		$urlConfig->setUrlOptionFromString($urlConfig->url_option);

		$pe = new PageElement();
		$pe->setUrlConfig($urlConfig);

		$pageElement = PageElement::where('_id','<>', new \MongoId($srcId))->where('deleted', '<>', 1)->where('fullURL', '=', $pe->fullURL)->where('cid', '=', $active_account->uid)->first();

    if(!$pageElement){
      $pageElement = PageElement::find($srcId);
    }
		$pageElement->setUrlConfig($urlConfig);
    $pageElement->hidden = 0;
    $pageElement->deleted = $pageElement->deleted;
    $pageElement->additionalJS = $additionalJS;
    $pageElement->remark = $remark;
    $pageElement->save();

		$ret['source'] = 'webpaglelist';
		$ret['content'] = $pageElement;

		return response()->json( $ret ) ;
	}

	/*
	* Toggle hidden value active/inactive
	*
	* @return Json
	*/
	public function updateStatus(Request $request)
	{
			$ret = ['success' => true , 'msg' => 'Page activated!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$pageElement = PageElement::find($srcId);
			$pageElement->hidden = (int)$request->input('hidden');
			$pageElement->save();


			if($pageElement->hidden == "1"){
				$ret['msg'] = 'Page deactivated';
			}

			$ret['source'] = 'webpagelist';
			$ret['pageElement'] = $pageElement;
			return response()->json($ret);
	}
	/*
	* change folder of page element
	*
	* @return Json
	*/
	public function changeFolder(Request $request)
	{
			$ret = ['success' => true , 'msg' => 'Page is moved to new folder.'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$target = explode('__', $request->input('target'));

			if($target){
					try{
						$pageElement = PageElement::find($srcId);
						$pageElement->folder_id = new \MongoId($target[1]);
						$pageElement->save();

						$ret['source'] = 'webpagelist';
						$ret['content']['webpagelist'] = $pageElement;
					}
					catch(\Exception $e){
						// ToDo: Report invalid mongoId error
						$ret['success'] = false;
						$ret['msg'] = $e->getMessage();
					}
			}

			return response()->json($ret);
	}
	/*
	* Action is deleting a web page list element
	*
	* @return Json
	*/
	public function delete(Request $request)
	{
			$ret = ['success' => true , 'msg' => 'Page deleted!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$pageElement = PageElement::find($srcId);
			$pageElement->deleted = 1;
			$pageElement->save();


			$ret['source'] = 'webpagelist';
			$ret['pageElement'] = $pageElement;
			return response()->json($ret);
	}
}
