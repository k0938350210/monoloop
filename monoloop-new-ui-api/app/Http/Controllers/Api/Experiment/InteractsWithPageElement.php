<?php namespace App\Http\Controllers\Api\Experiment ;

use DB ;
use App\PageElement;
use Illuminate\Support\Facades\Log;

trait InteractsWithPageElement
{
  // update related page_elements
  private function updateRelatedPageElements($experiment){
    // if ($experiment->PageElements) {
    // for ($i=0; $i < sizeof($experiment->PageElements); $i++) {
    // $obj = (Object)$experiment->PageElements[$i];
    $pagesExists = PageElement::where('deleted', '<>', 1)->where('Experiment.experimentID',$experiment->experimentID)->get() ;
    // var_dump($pagesExists);die;
    if(!empty($pagesExists)){
      foreach($pagesExists as $pagesExist){
        foreach($pagesExist->experiments as $found_experiment)
        {
          if ($found_experiment->experimentID == $experiment->experimentID) {
            $found_experiment->hidden = $experiment->hidden;
            $found_experiment->save();
          }
        }
        if ($pagesExist->content) {
          foreach($pagesExist->content as $found_content)
          {
            if ($found_content->experimentID == $experiment->experimentID) {
              $found_content->exp_status = $experiment->hidden;
              $found_content->save();
            }
          }
        }
      }
    }
    // }








    // for ($i=0; $i < sizeof($experiment->PageElements); $i++) {
    //   $obj = (Object)$experiment->PageElements[$i];
    //   $pagesExists = DB::connection('mongodb')
    //   ->collection('PageElements')
    //   ->where('_id', $obj->pageelementID)->get();
    //   if(!empty($pagesExists)){
    //     $updateResponse = DB::connection('mongodb')
    //     ->collection('PageElements')
    //     ->where('_id', $obj->pageelementID)
    //     ->update(
    //       [
    //         'hidden' => (int)$experiment->hidden
    //         , 'delete' => (int) $experiment->deleted
    //       ]
    //       , ['multiple' => true]
    //     );
    //   }
    // }
    // }




  }
}
