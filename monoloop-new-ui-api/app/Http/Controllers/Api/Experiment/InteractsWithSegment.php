<?php namespace App\Http\Controllers\Api\Experiment ;

use App\Segment;

trait InteractsWithSegment
{
  private function updateRelatedSegments($experiment){
    // update related segment
    if(!empty($experiment->segment_id)){
      $_segment = Segment::where('_id', new \MongoId($experiment->segment_id))->first();
      $_segment->hidden = $experiment->hidden;
      $_segment->save();
    }
  }
}
