<?php namespace App\Http\Controllers\Api\Experiment ;

use App\Goal;
use App\PageElement;

trait InteractsWithGoal
{
  // update related goals
  private function updateRelatedGoals($experiment){
    if(!empty($experiment->goal_id)){
      $_goal = Goal::where('_id', new \MongoId($experiment->goal_id))->first();
      $_goal->hidden = $experiment->hidden;
      $_goal->save();
      if($_goal->page_element_id != null){
        $page_element_goal = PageElement::where('_id', new \MongoId($_goal->page_element_id))->first();
        if (!empty($page_element_goal)) {
          $page_element_goal->hidden = $_goal->hidden;
          $page_element_goal->save();
        }

      }
    }
  }
}
