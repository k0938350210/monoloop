<?php namespace App\Http\Controllers\Api;

use App\Events\ExperimentHistoryLog;
use App\ExperimentHistory;
use App\Segment;
use Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExperimentHistory as History;
use App\Http\Requests\Experiment as FORMS;
use App\Experiment;
use App\Tracker;
use App\Folder;
use App\PageElement;
use App\PageElementContent;
use App\PageElementExperiment;
use App\PageElementArray;
use App\UrlConfig;
use App\Goal;
use Illuminate\Support\Collection;
use App\Events\Logger;
use App\Events\Invitation;
use Input;
use App\Http\Controllers\Api\Segment\InteractsWithPageElement;

/* traits -> experiment */
use App\Http\Controllers\Api\Experiment\InteractsWithGoal;
use App\Http\Controllers\Api\Experiment\InteractsWithSegment;
use App\Http\Controllers\Api\Experiment\InteractsWithPageElement as ExperimentInteractsWithPageElement;
use App\Http\Controllers\Api\Goal\InteractsWithSelf;

class ExperimentController extends Controller {
	use InteractsWithPageElement;

	use InteractsWithGoal;
	use InteractsWithSegment;
	use ExperimentInteractsWithPageElement;
	use InteractsWithSelf;

	public function __construct()
	{
		$this->middleware('auth', ['except' => 'calculatePValue']);
	}

 	/*
 	 * Action is finding all experiments and associated folders of logged in user
 	 *
	 * @params $type
	 *
 	 * @return Json
 	 */

	public function index(Request $request, $type = "tree")
	{
		$ret = [];

		$folder_id = Input::get('source', NULL);
		$url = Input::get('url',null);

		if(!empty($folder_id)){
			$folder = explode('__', $folder_id);
			if(is_array($folder) && count($folder) === 2){
				$folder_id = $folder[1];
			}
		}

		$account_id = Auth::user()->active_account;
		$account_cid = Auth::user()->account->uid;

		$f = Folder::find($folder_id);
		$isRoot = false;
		if($f){
			$isRoot = empty($f->parent_id) ? true : false;
		}

		$ret['experiments'] = [];
		switch ($type) {
			case 'compact-list':
				break;
			case 'folder':
				$eids = [] ;
				if($url){

					$url_config = new UrlConfig() ;
					$url_config->url = $url ;

					$page_elements = PageElement::where('fullURL',$url_config->clean_url)->where('Experiment','exists',true)->byAccountCid($account_cid)->get();
					foreach ($page_elements as $page_element) {
						// Mantis #4363
						for ($i=0; $i < sizeof($page_element->experiment) ; $i++) {
							$eids[] = $page_element->experiment[$i]['experimentID'];
						}
						// Mantis #4363
						// $eids[] = $page_element->Experiment['experimentID'];
					}
				}
				if(!empty($url) && count($eids) == 0){
					$ret['experiments'] = []; // no data should be served in case of url not present in pageElements
				}else{
					$ret['experiments'] = Experiment::experiments($folder_id, $isRoot, $account_cid , $eids );
				}
				break;
			default:
				$folder = new Folder();
				$ret['experiments'] = $folder->experimentsFolders($account_cid);
				break;
		}
		return response()->json( $ret['experiments'] ) ;
	}

	/*
	* Action is a experiment from id
	*
	* @return Json
	*/

    public function getExperiment(Request $request, $id){
        $account_id = Auth::user()->active_account;
        return response()->json(Experiment::where('account_id', $account_id)
            ->where('experimentID', (int)$id)
            ->get());
    }
		public function getallExp(Request $request){
        $account_id = Auth::user()->active_account;
				$account_cid = Auth::user()->account->uid;
        return response()->json(Experiment::where('deleted', '<>', 1)
				->where('cid','=',$account_cid)->get());
    }

		public function getAccountCID(Request $request){
				$account_cid = Auth::user()->account->uid;
				return response()->json($account_cid);
		}
	public function show(FORMS\ShowRequest $request)
	{
		$ret = [];

		$srcTypeId = explode("__", $request->input('_id'));

		$ret['experiment'] = Experiment::find($srcTypeId[1]);
		// Mantis #4363
		// $ret['pageElement'] = PageElement::where('Experiment.experimentID', '=', $ret['experiment']->experimentID)->first();
		for ($i=0; $i < sizeof($ret['experiment']->PageElements); $i++) {
			// var_dump($ret['experiment']->PageElements[0]);die;
			$obj = (Object) $ret['experiment']->PageElements[$i];
			$ret['pageElement'] = PageElement::where('_id', $obj->pageelementID)->first();
		}

		//Mantis #4363
		return response()->json( $ret) ;
	}

    /*
    * Action is to get all experiments regardless of folders
    *
    * @return Json
    */

    public function getAllExperiments(Request $request){
        $account_id = Auth::user()->active_account;
        return response()->json(Experiment::where('account_id', '=', $account_id)
            ->where('deleted', '<>', 1)
            ->where('hidden', '<>', 1)
            ->get());
    }

    /*
    * Action is a to get pages form experiment
    *
    * @return Json
    */
	public function getPages(Request $request)
	{
		$ret = [];

		$srcTypeId = $request->input('_id', '');
        // echo $srcTypeId;
			$ret['experiment'] = Experiment::find($srcTypeId);

			if($ret['experiment']){
				//Mantis #4363
				for ($i=0; $i < sizeof($ret['experiment']->PageElements); $i++) {
					$obj = (Object) $ret['experiment']->PageElements[$i];
					$ret['pageElements'] = PageElement::where('_id', $obj->pageelementID)->where('deleted', '<>', 1)->get();
				}
			}else {
				$ret['pageElements'] = [];
			}
		return response()->json( $ret);
		// return response()->json( $ret['pageElements']) ;
	}

	/*
	* Action is creating a new experiment
	*
	* @return Json
	*/
    public function create(FORMS\AddForm $request){
		$ret = ['success' => true , 'msg' => 'Experience created!'] ;
		// die('done');
		$src = $request->input('source', false);
		if($src && strlen($src) > 0){
			$folder_id = explode("__", $request->input('source'));
		} else {
			$folder_id = null;
		}
		$account_id = Auth::user()->active_account;
		$active_account = Auth::user()->account;

		$segment = $request->input('segmentExperiment', false);
		$replicateSegment = $request->input('replicateSegment', false);

		$goal = $request->input('goalExperiment', false);
		$replicateGoal = $request->input('replicateGoal', false);

		$experiment = new Experiment();
		$experiment->name = $request->input('nameExperiment');
		$experiment->description = $request->input('descExperiment');

		if($segment){
			if($replicateSegment == 'true'){
				$source = Segment::where('_id', new \MongoId($segment))->first();
				$clone = $source->replicate();
				$clone->name = "Copy of ".$clone->name;
				$clone->embedded = (int) 1;
				$clone->save();

				$segment_id_mongo = new \MongoId($clone->_id);
			}else{
				$segment_id_mongo = new \MongoId($segment);
			}
			$experiment->segment_id = $segment_id_mongo;
		}
		if($goal){
			if($replicateGoal == 'true'){
				$source = Goal::where('_id', new \MongoId($goal))->first();
				$clone = $this->generateClone($source);
				$goal_id_mongo = new \MongoId($clone->_id);
			}else{
				$goal_id_mongo = new \MongoId($goal);
			}
			$experiment->goal_id = $goal_id_mongo;
		}
		$experiment->cg_size = (float) $request->input('cgSizeExperiment');
		$experiment->cg_day = (float) $request->input('cgDayExperiment');
		$experiment->significant_action = $request->input('significantActionExperiment');
		$experiment->account_id = $account_id;
		$experiment->deleted = 0;
		$experiment->hidden = 1;
		// Mantis #4101
		$experiment->priority = $request->input('priorityExperiment');
		$experiment->OCR = $active_account->omni_channel_ready;

		if(is_array($folder_id)){
			$experiment->folder_id = new \MongoId($folder_id[1]);
		} else {
			$experiment->folder_id = new \MongoId(Folder::rootFolders($account_id)->first()->_id);
		}

		$experiment->experimentID = Experiment::max('experimentID') + 1;
		$experiment->cid = Auth::user()->account->uid;

		$experiment->save() ;

		// Mantis #4363

		$pageElementIds = $request->input('page_element_ids', '');

		// Excluding because of no use of PageElementArray

		// if(strlen($pageElementIds) > 0){
		// 	$breakPageElementIds = explode(',', $pageElementIds);
		// 	if(count($breakPageElementIds) > 0){
		// 		for ($i = 0; $i < count($breakPageElementIds); $i++) {
		// 			$pageElementId = new PageElementArray();
		// 			$pageElementId->pageelementID = trim($breakPageElementIds[$i]);
		// 			$experiment->pageElementsArray()->save($pageElementId);
		// 		}
		// 	}
		// }
		// Mantis #4363
    $experimentId = new \MongoId($experiment->_id);
    \Event::fire(new Logger(Auth::user()->username, " created experience", $experiment->name , "App\Experiment",$experimentId));
    $this->setHistoryLogs($request,$goal,$segment,$experiment);




		// page element


		// $pageElementIds = $request->input('page_element_ids', '');
		if(strlen($pageElementIds) > 0){
			$breakPageElementIds = explode(',', $pageElementIds);
			if(count($breakPageElementIds) > 0){
				for ($i = 0; $i <  count($breakPageElementIds) ; $i++) {
					$pageElementId = trim($breakPageElementIds[$i]);
					$pageElement = PageElement::find($pageElementId);

					// new structural change in Content array
					// var_dump($pageElement);die;
					if ($pageElement->content) {
							foreach ($pageElement->content as $foundContent ) {
								if ($foundContent->experimentID == -2 & $foundContent->exp_status == -2) {
										$foundContent->experimentID = $experiment->experimentID;
										$foundContent->exp_status = $experiment->hidden;
										$foundContent->save();
								}
							}

					}



					if($pageElement->experiment){
						// Mantis #4363
						if (sizeof($pageElement->experiment) > 0) {
							foreach($pageElement->experiments as $found_experiment)
	             {
	               if ($found_experiment->experimentID == $experiment->experimentID) {
	                $pageElementExperiment = $found_experiment;
	               }else {
 								$pageElementExperiment = new PageElementExperiment();
 								}
	              }
							// for ($j=0; $j <sizeof($pageElement->experiment) ; $j++) {
							// 	$obj = (Object) $pageElement->experiment[$j];
							// 	if ($obj->experimentID == $experiment->experimentID) {
							// 		$pageElementExperiment = $pageElement->experiment[$j];
							// 	}

							// }
						}
						else {
							$pageElementExperiment = new PageElementExperiment();
						}
					} else {
						$pageElementExperiment = new PageElementExperiment();
					}
					// $pageElementExperiment = new PageElementExperiment();
					$pageElementExperiment->controlGroupSize = $experiment->cg_size;
					$pageElementExperiment->controlGroupDays = $experiment->cg_day;
					$pageElementExperiment->experimentID = $experiment->experimentID;
					$pageElementExperiment->significantAction = $experiment->significant_action ;
					// new structural change
					$pageElementExperiment->hidden = $experiment->hidden ;
					// Mantis #4101
					$pageElementExperiment->priority = $experiment->priority;
					// $pageElementExperiment->_id = $pageElementId;
					if($goal){
						$pageElementExperiment->goalID = $experiment->goal->uid;
					}
					if($segment){
						$pageElementExperiment->segmentID = $experiment->segment->uid;
					}
					$pageElement->experiments()->save($pageElementExperiment);
							\Event::fire(new Logger(Auth::user()->username, " created page_element experience", $experiment->name , "App\Experiment",$experimentId));
				}
			}
		}
		$newPageElement = (int) $request->input('newPageElement', 0);
		$pageElement = '';
		if($newPageElement === 1){
			$fullURL = $request->input('fullURLWebPageList');
			$regExp = $request->input('regularExpressionWebPageElement', '');
			$inWWW = (int)$request->input('includeWWWWebPageElement', 0);
			$inHTTP_HTTPS = (int) $request->input('includeHttpHttpsWebPageElement', 0);
			$urlOption = $request->input('urlOptionWebPageElement', '');
			$additionalJS = $request->input('tAdditionalJSWebPageElement', '');
			$remark = $request->input('remarkWebPageList', '');

			$urlConfig = new UrlConfig();
			$urlConfig->reg_ex = $regExp;
			$urlConfig->url = $fullURL;
			$urlConfig->url_option = $urlOption;
			$urlConfig->inc_www = $inWWW;
			$urlConfig->inc_http_https = $inHTTP_HTTPS;
			$urlConfig->urlHash = $urlConfig->getUrlHashAttribute();
			$urlConfig->cleanUrl = $urlConfig->getCleanUrlAttribute();

			$urlConfig->setUrlOptionFromString($urlConfig->url_option);

			$pageElement = new PageElement();

			$pageElement->cid = $active_account->uid;
			$pageElement->hidden = 1;
			$pageElement->deleted = 0;
			$pageElement->pageID = 0;

			$pageElement->setUrlConfig($urlConfig);
	    $pageElement->originalUrl = $fullURL;

	    $pageElement->additionalJS = $additionalJS;
	    $pageElement->remark = $remark;

	    $pageElement->save();
            \Event::fire(new Logger(Auth::user()->username, " created page_element", $experiment->name , "App\Experiment",$experimentId));
            $this->setHistoryLogs($request,$goal,$segment,$experiment);
			if($pageElement->experiment){
				// Mantis #4363
				if (sizeof($pageElement->experiment) > 0) {

					foreach($pageElement->experiments as $found_experiment)
					 {
						 if ($found_experiment->experimentID == $experiment->experimentID) {
							$pageElementExperiment = $found_experiment;
						 }else {
 						$pageElementExperiment = new PageElementExperiment();
 						}
						}
					// for ($i=0; $i <sizeof($pageElement->experiment) ; $i++) {
					// 	$obj = (Object) $pageElement->experiment[$i];
					// 	if ($obj->experimentID == $experiment->experimentID) {
					// 		$pageElementExperiment = $pageElement->experiment[$i];
					// 	}

					// }
				}
				else {
					$pageElementExperiment = new PageElementExperiment();
				}
			} else {
				$pageElementExperiment = new PageElementExperiment();
			}
			$pageElementExperiment->controlGroupSize = $experiment->cg_size;
			$pageElementExperiment->controlGroupDays = $experiment->cg_day;
			$pageElementExperiment->experimentID = $experiment->experimentID;
			// new structural change
			$pageElementExperiment->hidden = $experiment->hidden ;
			// Mantis #4101
			$pageElementExperiment->priority = $experiment->priority;
			if($goal){
				$pageElementExperiment->goalID = $experiment->goal->uid;
			}
			if($segment){
				$pageElementExperiment->segmentID = $experiment->segment->uid;
			}
            \Event::fire(new Logger(Auth::user()->username, " created page_element experience", $experiment->name , "App\Experiment",$experimentId));

			$pageElement->experiments()->save($pageElementExperiment);

		}

		if($goal){
			$this->linkExperimentWithGoal($experiment->goal->uid, $active_account, $experiment);
		}
        if($segment){
            $segment_id_mongo = new \MongoId($segment);
            $segment = Segment::find($segment_id_mongo);
            \Event::fire(new Logger(Auth::user()->username, " has added an audience to an experience ($experiment->name)", $segment->name , "App\Experiment",$experimentId));
        }

		$ret['source'] = 'experiment';
		$ret['content']['experiment'] = $experiment;
		$ret['content']['pageElement'] = $pageElement;
		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a experiment
	*
	* @return Json
	*/
	public function update(FORMS\EditForm $request){
		$ret = ['success' => true , 'msg' => 'Experience updated!'] ;

		$srcId = $request->input('_id');
		$segment = $request->input('segmentExperiment', false);
		$goal = $request->input('goalExperiment', false);

		$account_id = Auth::user()->active_account;
		$active_account = Auth::user()->account;

		$experiment = Experiment::find($srcId);
		$experiment->name = $request->input('nameExperiment');
		$experiment->description = $request->input('descExperiment');
		if($segment){
			$segment_id_mongo = new \MongoId($segment);
			$experiment->segment_id = $segment_id_mongo;
		}
		if($goal){
			$goal_id_mongo = new \MongoId($goal);
			$experiment->goal_id = $goal_id_mongo;
		}

		$experiment->cg_size = (float) $request->input('cgSizeExperiment');
		$experiment->cg_day = (float) $request->input('cgDayExperiment');
		$experiment->significant_action = (int) $request->input('significantActionExperiment');
		// Mantis #4101
		$experiment->priority = $request->input('priorityExperiment');
		$experiment->updated_at = \Carbon::now();
		$experiment->save() ;
        $experimentId = new \MongoId($experiment->_id);
        \Event::fire(new Logger(Auth::user()->username, " updated an experience", $experiment->name , "App\Experiment", $experimentId));
        $this->setHistoryLogs($request,$goal,$segment,$experiment);
				// Mantis #4363

				// Excluding because of no use of PageElementArray

			// 	$pageElementIds = $request->input('page_element_ids', '');
			// 	if(strlen($pageElementIds) > 0){
			// 		$breakPageElementIds = explode(',', $pageElementIds);
			// 		if(count($breakPageElementIds) > 0){
			// 			$pageElementId = trim($breakPageElementIds[count($breakPageElementIds) - 1]);
			// // this content is changed
			// 			for ($i=0; $i <sizeof($experiment->PageElements) ; $i++) {
			// 				$obj =(Object) $experiment->PageElements[$i];
			// 				if ($obj->pageelementID != $pageElementId) {
			// 					$pageElementId = new PageElementArray();
			// 					$pageElementId->pageelementID = trim($breakPageElementIds[count($breakPageElementIds) - 1]);
			// 					$experiment->pageElementsArray()->save($pageElementId);
			// 				}
			// 			}
			//
			// 		}
			// 	}
				// till here
        // page element

		$pageElementIds = $request->input('page_element_ids', '');
		if(strlen($pageElementIds) > 0){
			$breakPageElementIds = explode(',', $pageElementIds);
			if(count($breakPageElementIds) > 0){
				for ($i = 0; $i < count($breakPageElementIds); $i++) {
					$pageElementId = trim($breakPageElementIds[$i]);
					$pageElement = PageElement::find($pageElementId);

					// new structural change in Content array
					// var_dump($pageElement);die;
					if ($pageElement->content) {
							foreach ($pageElement->content as $foundContent ) {
								if ($foundContent->experimentID == -2 || $foundContent->exp_status == -2) {
										$foundContent->experimentID = $experiment->experimentID;
										$foundContent->exp_status = $experiment->hidden;
										$foundContent->save();
								}
							}

					}




					if($pageElement->Experiment){
						// Mantis #4363

						if (sizeof($pageElement->Experiment) > 0) {
							foreach ($pageElement->Experiments as $found_exp) {
								if ($found_exp->experimentID == $experiment->experimentID) {
									$pageElementExperiment = $found_exp;
								}else {
									$pageElementExperiment = new PageElementExperiment();
									}
							}



							// for ($i=0; $i <sizeof($pageElement->experiment) ; $i++) {
							// 	$obj = (Object) $pageElement->experiment[$i];
							// 	if ($obj->experimentID == $experiment->experimentID) {
							// 		$pageElementExperiment = $experiment;
							// 	}

							// }
						}
						else {
							$pageElementExperiment = new PageElementExperiment();
						}
					} else {
						$pageElementExperiment = new PageElementExperiment();
					}
					$pageElementExperiment->controlGroupSize = $experiment->cg_size;
					$pageElementExperiment->controlGroupDays = $experiment->cg_day;
					$pageElementExperiment->experimentID = $experiment->experimentID;
					$pageElementExperiment->significantAction = $experiment->significant_action ;
					// new structural change
					$pageElementExperiment->hidden = $experiment->hidden ;
					// Mantis #4101
					$pageElementExperiment->priority = $experiment->priority;
					if($goal){
						$pageElementExperiment->goalID = $experiment->goal->uid;
					}
					if($segment){
						$pageElementExperiment->segmentID = $experiment->segment->uid;
					}

					$pageElement->experiments()->save($pageElementExperiment);
                    \Event::fire(new Logger(Auth::user()->username, " updated page elements experience", $experiment->name , "App\Experiment",$experimentId));
                    $this->setHistoryLogs($request,$goal,$segment,$experiment);
                }
			}
		}

		if($goal){
			$this->linkExperimentWithGoal($experiment->goal->uid, $active_account, $experiment);
		}
        if($segment){
            $segment_id_mongo = new \MongoId($segment);
            $segment = Segment::find($segment_id_mongo);
            \Event::fire(new Logger(Auth::user()->username, " has updated an audience to an experience ($experiment->name)", $segment->name , "App\Experiment",$experimentId));
        }
		$ret['source'] = 'experiment';
		$ret['content']['experiment'] = $experiment;

		return response()->json( $ret ) ;
	}

	private function linkExperimentWithGoal($goldUid, $account, $experiment){
		$pageElements = PageElement::where('cid',(int)$account->uid)->where('deleted', '=', 0)->whereNotNull('originalUrl')->get();
    if($goldUid){
        $goal = Goal::where('uid','=', $goldUid)->first();
    }
    if(count($pageElements) > 0){
			foreach ($pageElements as $pageElement) {
				$contents = $pageElement->content->where('uid','=', $goldUid);
				if(count($contents) > 0){
					foreach ($contents as $content) {
						if(!isset($content->experiments)){
							$content->experiments = [];
								$content->experiments = array_merge($content->experiments,[$experiment->experimentID]);
						} else {
							if(!in_array($experiment->experimentID, $content->experiments)){
								// var_dump($content->experiments,$experimentID);die;
								// $content->experiments = (array) $content->experiments;
								$content->experiments = array_merge($content->experiments,[$experiment->experimentID]);
								// $content->experiments[] = $experimentID;
							}
						}
						// var_dump($content->experiments);die;
						$pageElement->content()->save($content);
          }
				}
			}
		}

    if($goal){
      $experimentId = new \MongoId($experiment->_id);
      \Event::fire(new Logger(Auth::user()->username, " has added a goal to an experience ($experiment->name)", $goal->name , "App\Experiment",$experimentId));
    }

	}

	/*
	* Toggle hidden value active/inactive
	*
	* @return Json
	*/
	public function updateStatus(FORMS\StatusUpdateForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Experience activated!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$experiment = Experiment::find($srcId);
			$experiment->hidden = (int) $request->input('hidden');
			$experiment->save();
						$experimentId = new \MongoId($experiment->_id);

			if($experiment->hidden == "1"){
				$ret['msg'] = 'Experiment deactivated';
								\Event::fire(new Logger(Auth::user()->username, " has deactivated experience ", $experiment->name , "App\Experiment",$experimentId));
								$this->setHistoryLogs($request,0,0,$experiment);

				}else{
						\Event::fire(new Logger(Auth::user()->username, " has activated experience ", $experiment->name , "App\Experiment",$experimentId));
						$this->setHistoryLogs($request,0,0,$experiment);
				}

			$this->updateRelatedGoals($experiment);
			$this->updateRelatedSegments($experiment);
			$this->updateRelatedPageElements($experiment);

			$ret['source'] = 'experiment';
			$ret['content']['experiment'] = $experiment;


			return response()->json($ret);
	}

	/*
	* change folder of experiment
	*
	* @return Json
	*/
	public function changeFolder(FORMS\ChangeFolderForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Experience is moved to new folder.'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$target = explode('__', $request->input('target'));

			if($target){
				try{
					$experiment = Experiment::find($srcId);
					$experiment->folder_id = new \MongoId($target[1]);
					$experiment->save();

					$ret['source'] = 'experiment';
					$ret['content']['experiment'] = $experiment;
				}
				catch(\Exception $e){
					// ToDo: Report Error for Invalid MongoID
					$ret['success'] = false;
					$ret['msg'] = $e-> getMessage();
				}
			}

			return response()->json($ret);
	}

	/*
	* Action is deleting a goal
	*
	* @return Json
	*/
	public function delete(FORMS\DeleteForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Experience deleted!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$experiment = Experiment::find($srcId);
			$experiment->deleted = 1;
			$experiment->save();
            $experimentId = new \MongoId($experiment->_id);
            \Event::fire(new Logger(Auth::user()->username, " has delete the experience ", $experiment->name , "App\Experiment",$experimentId));
             $this->setHistoryLogs($request,0,0,$experiment);

            $ret['source'] = 'experiment';
			$ret['content']['experiment'] = $experiment;


			return response()->json($ret);
	}


    public function setHistoryLogs($request,$goal=0,$segment=0,$experiment)
    {
        $exp=new ExperimentController();
        $response=$exp->getExperiment($request,$experiment->experimentID);
        $content=json_decode($response->getContent());
        $today = Carbon::today()->timestamp;
        $data= History::where('timestamp',">=", $today - (24*60*60))->get();

				$variations = 0;
				if(!empty($content->variations)){
					$variations = $content->variations;
				}
				$locations = 0;
				if(!empty($content->locatiosn)){
					$locations = $content->locations;
				}

        if(sizeof($data)>0){
            for($i=0;$i<sizeof($data);$i++){
                if($goal && $segment){
                    \Event::fire(new ExperimentHistoryLog($experiment->experimentID,1,1,$variations,$locations,1,$data[$i]->timestamp));
                }else if($segment){
                    \Event::fire(new ExperimentHistoryLog($experiment->experimentID,0,1,$variations,$locations,1,$data[$i]->timestamp));
                }
                else if($goal){
                    \Event::fire(new ExperimentHistoryLog($experiment->experimentID,1,0,$variations,$locations,1,$data[$i]->timestamp));
                }else {
                    \Event::fire(new ExperimentHistoryLog($experiment->experimentID,0,0,$variations,$locations,1,$data[$i]->timestamp));
                }
            }
        } else{

            if($goal && $segment){
                \Event::fire(new ExperimentHistoryLog($experiment->experimentID,1,1,$variations,$locations));
            }else if($segment){
                \Event::fire(new ExperimentHistoryLog($experiment->experimentID,0,1,$variations,$locations));
            }
            else if($goal){
                \Event::fire(new ExperimentHistoryLog($experiment->experimentID,1,0,$variations,$locations));
            }else {
                \Event::fire(new ExperimentHistoryLog($experiment->experimentID,0,0,$variations,$locations));
            }
        }

    }

		private function getLastExUpdatedTimestamp($experimentID){
      $timestamp = false;

      $timestamp = History::where('experimentID', '=', (int) $experimentID)->max('timestamp');

      if(is_null($timestamp)){
         $experiment = Experiment::where('experimentID',(int)$experimentID)->first() ;
         $date = new Carbon($experiment['updated_at']);
         $timestamp  = $date->timestamp ;
      }

      return $timestamp;
    }

		/*
		* Action is creating p-value
		*
		* @params experiment ID
		* @params $customer ID
		*
		* return JSON Response
		*/
		public 	function calculatePValue(Request $request){
			$response = '';

			$token = $request->header('token', '-');
			if($token == config('app.monoloop.static_key')){
				try
				{
					// get json data from body
					$requestData = $request->json()->all();

					$eIDs = array_map(function($value){
						return (array('id' => (int) $value, 'timestamp' => ($this->getLastExUpdatedTimestamp((int)$value))));
					}, explode(',', $requestData['eid'])); // experiment IDs
					$cid = $requestData['cid']; // customer ID

					// update pvalue & timestamp
				  $response = $this->updatePValueAndTimestamp($cid, $eIDs);
				}
				catch(\Exception $e){
						$response = $e->getMessage();
				}
			}	else{
				$response = 'Unauthorized request.';
			}

			return response()->json($response);
		}
}
