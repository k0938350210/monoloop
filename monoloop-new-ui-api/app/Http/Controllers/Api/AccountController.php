<?php

namespace App\Http\Controllers\Api;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Hasher;
use App\Services\Account\Cbr;
use App\Services\Account\Invocation;
use App\Events\Logger;

use App\Http\Requests\Account\ControlGroupRequest;
use App\Http\Requests\Account\InvocationPrepostRequest ;
use App\Http\Requests\Account\InvocationRequest ;
use App\Http\Requests\Account\ScoreRequest ;

use App\Http\Controllers\Common\InteractsWithCbr ;
use App\Services\Trackers\PreconfiguredWpTrackers ;


use App\Account;

class AccountController extends Controller
{
  use InteractsWithCbr ;
    /**
     * __construct performing pre actions before all the mentioned actions.
     *
     * @return JSON
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(Request $request)
    {

        $accountObj = Auth::user()->account;
        $account = $accountObj->toArray();

        $invocation = new Invocation($accountObj);
        $invocationStr = $invocation->getInvocation();

        $account['invocation']['str'] = $invocationStr;
        $account['invocation']['str2'] = htmlentities($account['invocation']['str']);
        $account['invocation']['str2'] = nl2br($account['invocation']['str2']);

        $account['apihost'] = $accountObj->uid.'invoke.monoloop.com/api/';
        $hasher = new Hasher();
        $account['apitoken'] = $hasher->generateSign($accountObj->uid);

		\Event::fire(new Logger(Auth::user()->username, "invocation updated", NULL, "App\Account"));

        return response()->json($account);
    }
    public function controllgroupUpdate(ControlGroupRequest $request)
    {

        $ret = ['success' => true , 'msg' => ''];
        $account = Auth::user()->account;
        $control_group = $account->control_group;
        $control_group['enabled'] = (bool) ($request->input('enable'));
        $control_group['size'] = intval($request->input('size'));
        $control_group['days'] = intval($request->input('days'));
        $account->control_group = $control_group;
        $account->save();

        $this->generateCbr($account) ;

				\Event::fire(new Logger(Auth::user()->username, "control group updated", NULL, "App\Account"));
        return $ret;
    }

    public function invocationUpdate(InvocationRequest $request)
    {
        $ret = ['success' => true , 'msg' => ''];

        $cid = intval($request->input('cid', 0));
        if($cid > 0){
          $account = Account::where('uid', '=', $cid)->first();
          if(!$account){
            $ret['msg'] = "Invalid cid.";
            return $ret;
          }
        } else {
          $account = Auth::user()->account;
        }

        $invocation = $account->invocation;
        $invocation['anti_flicker'] = (bool) ($request->input('anti_flicker'));
        $invocation['timeout'] = intval($request->input('timeout'));

        $account->invocation = $invocation;
        $account->save();

        $invocation = new Invocation($account);
        $invocationStr = $invocation->getInvocation();

        $ret['invocation']['str'] = $invocationStr;
        $ret['invocation']['str2'] = htmlentities($ret['invocation']['str']);
        $ret['invocation']['str2'] = nl2br($ret['invocation']['str2']);

        $this->generateCbr($account);

				\Event::fire(new Logger(Auth::user()->username, "invocation updated", NULL, "App\Account"));
        return $ret;
    }

    public function prepostUpdate(InvocationPrepostRequest $request)
    {
        $ret = ['success' => true , 'msg' => ''];

        $account = Auth::user()->account;
        $invocation = $account->invocation;
        $invocation['pre'] = $request->input('pre_invocation');
        $invocation['post'] = $request->input('post_invocation');
        $account->invocation = $invocation;
        $account->save();

        $this->generateCbr($account);

				\Event::fire(new Logger(Auth::user()->username, "script updated", NULL, "App\Account"));
        return $ret;
    }

    public function spaUpdate(Request $request){
        $ret = ['success' => true , 'msg' => ''];

        $account = Auth::user()->account;
        $invocation = $account->invocation;
        $invocation['postback_delay'] = intval($request->input('postback_delay'));
        $invocation['spa_pageview_option'] = $request->input('spa_pageview_option') ;
        $account->invocation = $invocation;
        $account->save();

        $this->generateCbr($account);

                \Event::fire(new Logger(Auth::user()->username, "spa script updated", NULL, "App\Account"));
        return $ret;
    }
    /**
     * The action to save account's score values.
     *
     * @param request posted params
     *
     * @return json
     */
    public function scoresUpdate(ScoreRequest $request)
    {
        $ret = ['success' => true , 'msg' => ''];

        $account = Auth::user()->account;
        $scores = $account->scores;
        $scores['clickdepth'] = intval($request->input('clickdepth'));
        $scores['duration'] = intval($request->input('duration'));
        $scores['recency'] = intval($request->input('recency'));
        $scores['latency'] = intval($request->input('latency'));
        // echo '<pre>'; print_r($scores);echo '</pre>';die;
        $account->scores = $scores;
        $account->save();

				\Event::fire(new Logger(Auth::user()->username, "scores updated", NULL, "App\Account"));
        return $ret;
    }

    public function wpPluginsUpdate(Request $request){
        $accountObj = Auth::user()->account;
        $accountObj->wp_plugins = $request->input('wp_plugins');
        $accountObj->save();

        $preconfiguredWpTrackers = new PreconfiguredWpTrackers() ;
        $preconfiguredWpTrackers->updateDefault($accountObj);

        return response()->json(['success' => true]);
    }
}
