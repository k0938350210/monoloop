<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Experiment;
use App\Segment;
use App\ReportingArchiveAggregated;
use App\Funnel;
use Carbon\Carbon;

class DashboardController extends Controller{
	public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request){

    $range = $request->input('range');

    if($range === 'today'){
      $cur_to = Carbon::now();
      $cur_from = Carbon::now()->subDay();
      $pre_to = $cur_from;
      $pre_from = $cur_from->copy()->subDay();
    }elseif($range === 'yesterday'){
      $cur_to = Carbon::now()->subDay();
      $cur_from = Carbon::now()->subDay(2);
      $pre_to = $cur_from;
      $pre_from = $cur_from->copy()->subDay();
    }elseif($range === 'last-30-days'){
      $cur_to = Carbon::now();
      $cur_from = Carbon::now()->subMonth();
      $pre_to = $cur_from;
      $pre_from = $cur_from->copy()->subMonth();
    }else{
      $cur_to = Carbon::now();
      $cur_from = Carbon::now()->subWeek();
      $pre_to = $cur_from;
      $pre_from = $cur_from->copy()->subWeek();
    }


    $cid = Auth::user()->account->uid;
    $reports = ReportingArchiveAggregated::where('cid', $cid)->where('datestamp','>',$cur_from->timestamp)->where('datestamp','<=',$cur_to->timestamp)->select('experiments','segments','totalVisits')->get();

  	$experiments = Experiment::where('deleted', '<>', 1)->where('cid', $cid)->get();
  	$experiment_data = [];
 		foreach ($experiments as $key => $experiment) {
 			$visitors = 0;
      $conversions = 0;
      $cg_visitors = 0;
      $cg_direct = 0;

      foreach($reports as $report){
        if(isset($report->experiments[$experiment->experimentID])){
          $exp = $report->experiments[$experiment->experimentID];
          $visitors += isset($exp['visitors']) ? $exp['visitors'] : 0;
          $conversions += isset($exp['direct']) ? $exp['direct'] : 0;
          $cg_visitors += isset($exp['cg_visitors']) ? $exp['cg_visitors'] : 0;
          $cg_direct += isset($exp['cg_direct']) ?  $exp['cg_direct'] : 0;
        }
      }

      $conversions = round($conversions, 2);
      $conversion_rate = ($visitors != 0) ?   ($conversions / $visitors) * 100 : 0;
      $conversion_rate = round($conversion_rate, 2);
      $experiment_data[] = [
      	'uid' => $experiment->experimentID,
      	'name' => $experiment->name,
      	'visitors' => $visitors,
      	'conversions' => $conversions,
      	'cr' => $conversion_rate,
      ];
 		}

 		$reports_prev = ReportingArchiveAggregated::where('cid', $cid)->where('datestamp','>',$pre_from->timestamp)->where('datestamp','<=',$pre_to->timestamp)->select('experiments','segments','totalVisits')->get();

 		$segments = Segment::where('deleted', '=', 0)->where('cid', $cid)->where('embedded', '<>', 1)->get();
 		$segment_data = [];
 		foreach ($segments as $key => $segment) {
 			$prev = 0;
      $cur = 0;

      foreach($reports_prev as $report_prev){
        if(isset($report_prev->segments[$segment->uid])){
          $prev += $report_prev->segments[$segment->uid]['count'];
        }
      }

      foreach($reports as $report_cur){
        if(isset($report_cur->segments[$segment->uid])){
          $cur += $report_cur->segments[$segment->uid]['count'];
        }
      }

      $change = $prev != 0 ? ($cur - $prev)/$prev * 100 : 0;

      $segment_data[] = [
      	'uid' => $segment->uid,
      	'name' => $segment->name,
      	'current' => round($cur, 2),
      	'previous' => round($prev, 2),
      	'change' => round($change, 2)
      ];
 		}


  	$categories = [
  		['name' => 'Category 001','score' => rand(1,10)],
  		['name' => 'Category 002','score' => rand(1,10)],
  		['name' => 'Category 003','score' => rand(1,10)],
  		['name' => 'Category 004','score' => rand(1,10)],
  		['name' => 'Category 005','score' => rand(1,10)],
  	];

    $total_visits = 0;
    $total_visits_prev = 0;
    foreach($reports as $report){
      $total_visits += $report->totalVisits;
    }
    foreach($reports_prev as $report){
      $total_visits_prev += $report->totalVisits;
    }

    $funnel = Funnel::byName('Life cycle')->byCid($cid)->first();
    $total = 0;
    $a_total = 0;
    $i_total = 0;
    if(!is_null($funnel)){
      if(count($funnel->items) == 6){
        $items = $funnel->items;
        $anonymous_list = [$items[0]['value'],$items[1]['value']];
        $identify_list = [$items[2]['value'],$items[3]['value'],$items[4]['value'],$items[5]['value']];



        foreach($reports as $report_cur){
          foreach($anonymous_list as $a_item){
            if(isset($report_cur->segments[$a_item])){
              $a_total += $report_cur->segments[$a_item]['count'];
            }
          }
          foreach($identify_list as $i_item){
            if(isset($report_cur->segments[$i_item])){
              $i_total += $report_cur->segments[$i_item]['count'];
            }
          }
        }

        $total = $a_total + $i_total;

      }
    }

  	$general = [
  		'new_users' => rand(1000,2000),
  		'new_users_p' => rand(1,100),
  		'removed_users' => rand(500,1000),
  		'removed_users_p' => rand(1,100),
  		'total_users' => rand(5000,10000),
  		'total_visits' => $total_visits,
  		'total_visits_p' => ($total_visits_prev)?round(($total_visits-$total_visits_prev)/$total_visits_prev,2):0,
      'life_cycle_a_p' => ($total)?round(($a_total)/$total*100,2):0,
      'life_cycle_i_p' => ($total)?round(($i_total)/$total*100,2):0,
      'life_cycle_data' => $this->getLifeCycleDataList($funnel,$reports,$reports_prev),
  	];
  	return [
  		'general' => $general,
  		'categories' => $categories,
  		'experiments' => $experiment_data,
  		'audiences' => $segment_data,
  	];
  }

  public function getLifeCycleDataList($funnel,$reports,$reports_prev){
    $data = [];
    if(is_null($funnel)){
      return $data;
    }
    for($i = 0 ; $i < count($funnel->items) -1 ;$i++){
      $c_segment = $funnel->items[$i]['value'];
      $n_segment = $funnel->items[$i+1]['value'];
      $segment_c = Segment::byUid($c_segment)->first();
      $segment_n = Segment::byUid($n_segment)->first();
      $name = '';
      if(!is_null($segment_c)){
        $name = $segment_c->name;
      }else{
        $name = $segment_c->uid;
      }
      $name .= ' - ';
      if(!is_null($segment_n)){
        $name .= $segment_n->name;
      }else{
        $name .= $segment_n->uid;
      }
      $sub_data = [];
      $sub_data['name'] = $name;
      $sub_data['visitors'] = 0;
      $from_visitor = 0;
      foreach($reports as $report){
        if(isset($report->segments[$n_segment]['from'][$c_segment])){
          $sub_data['visitors'] += $report->segments[$n_segment]['from'][$c_segment];
        }
        if(isset($report->segments[$c_segment]['count'])){
          $from_visitor += $report->segments[$c_segment]['count'];
        }
      }
      if($from_visitor > 0){
        $sub_data['conversions'] = $sub_data['visitors'];
        $sub_data['conversion_rate'] = number_format($sub_data['visitors'] / $from_visitor * 100,2);
      }else{
        $sub_data['conversions'] = 0;
        $sub_data['conversion_rate'] = 0;
      }
      $sub_data['visitors'] = $from_visitor;
      $data[] = $sub_data;
    }
    return $data;
  }
}