<?php namespace App\Http\Controllers\Page;

use Auth;
use Illuminate\Http\Request;

class DashboardController extends PageController {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->viewData['selected'] = array('Dashboard','Dashboard') ;
		return view('page.dashboard',$this->viewData);
	}

	public function funnel(){
		$this->viewData['selected'] = array('Dashboard','Funnel') ;
		return view('page.funnel',$this->viewData);
	}

    public function experiment(){
        $this->viewData['selected'] = array('Dashboard','Experiment');
        return view('page.experiment',$this->viewData);
    }

    public function experimentdetail(Request $request , $id){
        $this->viewData['selected'] = array('Dashboard','Experiment');
        return view('page.experiment-detail',$this->viewData);

    }

}
