<?php namespace App\Http\Controllers\Page;

use Auth;
use Validator ;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\UseMonoloopViewData ;

use App\AccountUser;
use App\User;
use MongoId ;

class ConfirmSignupController extends Controller {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
  use UseMonoloopViewData ;

	public function index(Request $request)
	{
     return view('page.confirmation.wait');
	}

  public function confirm(Request $request , $id){
    if(MongoId::isValid($id) == false){
      abort(404);
    }
    $user = User::where('confirmation_id',new MongoId($id))->first() ;
    if(empty($user)){
      $user = Auth::User();
      if(!empty($user)){
        $viewData = $this->getViewData() ;
        $viewData['user'] = Auth::user();

      }
      $viewData['selected'] = array('','') ;
      return view('page.confirmation.activated',$viewData);
    }
    if(empty($user)) {
      abort(404);
    }

    #automatic authen ;
    // Auth::login($user);

    $user->confirmation_id = null ;
    $user->save() ;

    $viewData = $this->getViewData() ;
    $viewData['selected'] = array('','') ;
    return view('page.confirmation.success',$viewData);
  }

}
