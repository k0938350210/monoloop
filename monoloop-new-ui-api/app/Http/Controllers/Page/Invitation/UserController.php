<?php namespace App\Http\Controllers\Page\Invitation;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AccountUser;

class UserController extends Controller {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
  private $viewData = array() ;

	public function index(Request $request , $id, $token)
	{
    $accountUser = AccountUser::where('account_id', $id)->first() ;
    if(is_null($accountUser)){
      abort(404);
    }
    $accountUser->account;
    $this->viewData['user'] = $accountUser ;
    $this->viewData['type'] = "accountUser" ;
		return view('page.invitation.user',$this->viewData);
	}


}
