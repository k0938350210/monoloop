<?php namespace App\Http\Controllers\Page;

use Auth;
use Session;
use Illuminate\Http\Request;


// test

use Input;

use Redis;


class ContentController extends PageController {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function web()
	{
		$this->viewData['selected'] = array('Content','Page list') ;
		return view('page.content.web',$this->viewData);
	}
	public function addWeb(Request $request)
	{
		$url = $request->input('url');
		$this->viewData['selected'] = array('Content','Page list') ;
		$this->viewData['url'] = $url ;
		return view('page.content.add-web',$this->viewData);
	}
	public function editWeb()
	{
		$this->viewData['selected'] = array('Content','Page list') ;
		$this->viewData['page_id'] = Input::get('id', '');
		return view('page.content.edit-web',$this->viewData);
	}
	/**
	 * Show the web content to user.
	 *
	 * @return Response
	 */
	public function webcontent()
	{
		$this->viewData['selected'] = array('Content','Content list') ;
		return view('page.content.webcontent',$this->viewData);
	}

	public function bluepronts(){
		$this->viewData['selected'] = array('Content','Blueprints') ;
		return view('page.content.bluepronts',$this->viewData);
	}

	public function segment(){
		$this->viewData['selected'] = array('Segment','') ;
		return view('page.content.segment',$this->viewData);
	}
	public function addSegment(){
		$this->viewData['selected'] = array('Segment','') ;
		return view('page.content.add-segment',$this->viewData);
	}
	public function editSegment(){
		$this->viewData['selected'] = array('Segment','');
		$this->viewData['segment_id'] = Input::get('id', '');
		return view('page.content.edit-segment',$this->viewData);
	}

	public function tracker(){
		$this->viewData['selected'] = array('Tracker','') ;
		return view('page.content.tracker',$this->viewData);
	}
	public function addTracker(){
		$this->viewData['selected'] = array('Tracker','') ;
		return view('page.content.add-tracker',$this->viewData);
	}
	public function editTracker(){
		$this->viewData['selected'] = array('Tracker','');
		$this->viewData['tracker_id'] = Input::get('id', '');
		return view('page.content.edit-tracker',$this->viewData);
	}

 	public function experiment(){
 		$this->viewData['selected'] = array('Experiment','') ;
		return view('page.content.experiment',$this->viewData);
 	}
	public function addExperiment(){
		$this->viewData['selected'] = array('Experiment','') ;
		return view('page.content.add-experiment',$this->viewData);
	}
	public function editExperiment(){
		$this->viewData['selected'] = array('Experiment','') ;
		$this->viewData['experiment_id'] = Input::get('id', '');
		return view('page.content.edit-experiment',$this->viewData);
	}
 	public function goals(){
 		$this->viewData['selected'] = array('Goals','') ;
		return view('page.content.goals',$this->viewData);
 	}
	public function addGoals(){
		$this->viewData['selected'] = array('Goals','') ;
		return view('page.content.add-goals',$this->viewData);
	}
	public function editGoals(){
		$this->viewData['selected'] = array('Goals','') ;
		$this->viewData['goal_id'] = Input::get('id', '');
		return view('page.content.edit-goals',$this->viewData);
	}
}
