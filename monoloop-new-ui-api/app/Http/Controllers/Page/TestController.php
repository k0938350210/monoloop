<?php namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;

class TestController extends PageController {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function invocation()
	{
		return view('page.test.invocation');
	}

}
