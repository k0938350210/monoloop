<?php namespace App\Http\Controllers\Page;

use Auth;
use App\Http\Controllers\Controller;

class PageController extends Controller {
	protected $viewData = array() ;

	public function __construct()
	{
		$this->middleware('auth.token.session');
		$this->middleware('auth');


		if(Auth::guest())
			return ;

		#init global view data ;
		$this->viewData['account_selectors'] = Auth::User()->account_for_selector() ;
		$folders = array() ;
		$folders['Dashboard'] = array('icon' => 'icon-bar-chart' , 'href' => '#' , 'items' => array(
			'Dashboard' => array('href' => '/'),
			'Funnel' => array('href' => '/dashboard/funnel'),
            'Experiment'  => array('href' => '/reports/experiment'),

		)
		);
		$folders['Experiment'] = array('icon' => 'icon-beaker'  , 'href' => '/experiment' , 'items' => array());
		$folders['Segment'] = array('icon' => 'icon-group' , 'href' => '/segment' , 'items' => array());
		$folders['Content'] = array('icon' => 'icon-list-ul' , 'href' => '#' , 'items' => array(
			'Web' => array('href' => '/content/web'),
			'Blueprints' => array('href' => '/content/blueprints')
		)
		);
		$folders['Goals'] = array('icon' => 'icon-flag-checkered' , 'href' => '/goals' , 'items' => array());
		$folders['Trackers'] = array('icon' => 'icon-traker' , 'href' => '/trackers' , 'items' => array());
		$folders['Account'] = array('icon' => 'icone-cogs'  , 'href' => '#' , 'items' => array(
			'Profile' => array('href' => '/account/profile'),
			'Control Group' => array('href' => '/account/control-group'),
			'Codes & Scripts' => array('href' => '/account/code-and-scripts'),
			'API Token' => array('href' => '/account/api-token'),
			'Plugins' => array('href' => '/account/plugins'),
			'Users' => array('href' => '/account/users'),
			'Client Accounts' => array('href' => '/account/client-accounts') ,
			'Scores' => array('href' => '/account/scores') ,
			'Domains' => array('href' => '/account/domains') ,
			'Applications' => array('href' => '/account/applications') ,
			'Logs' => array('href' => '/account/logs')
		));
		$this->viewData['left_menus'] = $folders ;
	}
}
