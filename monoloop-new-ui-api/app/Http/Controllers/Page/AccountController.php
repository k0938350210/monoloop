<?php namespace App\Http\Controllers\Page;

use Auth;

use Illuminate\Http\Request;

class AccountController extends PageController {

	public function profile(){
		$this->viewData['selected'] = array('Account','Profile') ;
		return view('page.account.profile',$this->viewData);
	}

	public function controlgroup(){
		$this->viewData['selected'] = array('Account','Control Group') ;
		return view('page.account.controlgroup',$this->viewData);
	}

	public function codeandscripts(){
		$this->viewData['selected'] = array('Account','Codes & Scripts') ;
		return view('page.account.codeandscript',$this->viewData);
	}

	public function apitoken(){
		$this->viewData['selected'] = array('Account','API Token') ;
		return view('page.account.apitoken',$this->viewData);
	}

	public function plugins(){
		$this->viewData['selected'] = array('Account','Plugins') ;
		return view('page.account.plugins',$this->viewData);
	}

	public function users(){
		$this->viewData['selected'] = array('Account','Users') ;
		return view('page.account.users',$this->viewData);
	}

	public function installationmanual(){
		$this->viewData['selected'] = array('Account','Installation Manual') ;
		return view('page.account.installationmanual',$this->viewData);
	}

	public function clientaccounts(){

		$user = Auth::User();

		$user_type = isset($user->user_type) ? $user->user_type : "enterprise";

		if($user_type === "enterprise"){
			return redirect('/');
		}

		$this->viewData['selected'] = array('Account','Client Accounts') ;
		return view('page.account.clientaccounts',$this->viewData);
	}

	public function scores(){
		$this->viewData['selected'] = array('Account','Scores') ;
		return view('page.account.scores',$this->viewData);
	}

	public function domains(){
		$this->viewData['selected'] = array('Account','Domains') ;
		return view('page.account.domains',$this->viewData);
	}

	public function applications(){
		$this->viewData['selected'] = array('Account','Applications') ;
		return view('page.account.applications',$this->viewData);
	}

	public function logs(){
		$this->viewData['selected'] = array('Account','Logs') ;
		return view('page.account.logs',$this->viewData);
	}
}
