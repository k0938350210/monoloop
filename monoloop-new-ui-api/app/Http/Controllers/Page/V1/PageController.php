<?php namespace App\Http\Controllers\Page\V1;

use Auth; 
use App\Http\Controllers\Controller;  

class PageController extends Controller {
	protected $viewData = array() ; 
	
	public function __construct()
	{
		$this->middleware('auth');
		
		if(Auth::guest())
			return ; 
		#init global view data ; 
		$this->viewData['account_selectors'] = Auth::User()->account_for_selector() ; 
		$folders = array() ; 
		$folders['Dashboard'] = array('icon' => 'icon-bar-chart' , 'href' => '#' , 'items' => array(
			'Dashboard' => array('href' => '/v1/'),
			'Funnel' => array('href' => '/v1/dashboard/funnel')
		)
		);
		$folders['Experiment'] = array('icon' => 'icon-beaker'  , 'href' => '/v1/experiment' , 'items' => array());
		$folders['Segment'] = array('icon' => 'icon-group' , 'href' => '/v1/segment' , 'items' => array());
		$folders['Content'] = array('icon' => 'icon-list-ul' , 'href' => '#' , 'items' => array(
			'Web' => array('href' => '/v1/content/web'),
			'Blueprints' => array('href' => '/v1/content/blueprints')
		)
		);
		$folders['Goals'] = array('icon' => 'icon-flag-checkered' , 'href' => '/v1/goals' , 'items' => array());
		$folders['Trackers'] = array('icon' => 'icon-traker' , 'href' => '/v1/trackers' , 'items' => array());
		$folders['Account'] = array('icon' => 'icone-cogs'  , 'href' => '#' , 'items' => array(
			'Profile' => array('href' => '/v1/account/profile'),
			'Control Group' => array('href' => '/v1/account/control-group'),
			'Codes & Scripts' => array('href' => '/v1/account/code-and-scripts'),
			'API Token' => array('href' => '/v1/account/api-token'),
			'Plugins' => array('href' => '/v1/account/plugins'),
			'Users' => array('href' => '/v1/account/users'),
			'Client Accounts' => array('href' => '/v1/account/client-accounts') , #This should be some kind of rule 
			'Scores' => array('href' => '/v1/account/scores') , 
			'Domains' => array('href' => '/v1/account/domains') , 
			'Applications' => array('href' => '/v1/account/applications') , 
			'Logs' => array('href' => '/v1/account/logs')
		));
		$this->viewData['left_menus'] = $folders ;
	}
}