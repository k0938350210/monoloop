<?php namespace App\Http\Controllers\Page\V1;

use Illuminate\Http\Request;

class ContentController extends PageController {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function web()
	{
		$this->viewData['selected'] = array('Content','Web') ;
		return view('page.v1.content.web',$this->viewData);
	}

	public function bluepronts(){
		$this->viewData['selected'] = array('Content','Blueprints') ;
		return view('page.v1.content.bluepronts',$this->viewData);
	}

	public function segment(){
		$this->viewData['selected'] = array('Segment','') ;
		return view('page.v1.content.segment',$this->viewData);
	}

 	public function experiment(){
 		$this->viewData['selected'] = array('Experiment','') ;
		return view('page.v1.content.experiment',$this->viewData);
 	}

 	public function goals(){
 		$this->viewData['selected'] = array('Goals','') ;
		return view('page.v1.content.goals',$this->viewData);
 	}
}
