<?php namespace App\Http\Controllers\Page\V1;

use Illuminate\Http\Request;

class AccountController extends PageController {
 
	public function profile()
	{ 
		$this->viewData['selected'] = array('Account','Profile') ;
		return view('page.v1.account.profile',$this->viewData);
	}
	
	public function controlgroup(){
		$this->viewData['selected'] = array('Account','Control Group') ;
		return view('page.v1.account.controlgroup',$this->viewData);
	}
	
	public function codeandscripts(){
		$this->viewData['selected'] = array('Account','Codes & Scripts') ;
		return view('page.v1.account.codeandscript',$this->viewData);
	}
	
	public function apitoken(){
		$this->viewData['selected'] = array('Account','API Token') ;
		return view('page.v1.account.apitoken',$this->viewData);
	}
	
	public function plugins(){
		$this->viewData['selected'] = array('Account','Plugins') ;
		return view('page.v1.account.plugins',$this->viewData);
	}
	
	public function users(){
		$this->viewData['selected'] = array('Account','Users') ;
		return view('page.v1.account.users',$this->viewData);
	}
	
	public function clientaccounts(){
		$this->viewData['selected'] = array('Account','Client Accounts') ;
		return view('page.v1.account.clientaccounts',$this->viewData);
	}
	
	public function scores(){
		$this->viewData['selected'] = array('Account','Scores') ;
		return view('page.v1.account.scores',$this->viewData);
	}
	
	public function domains(){
		$this->viewData['selected'] = array('Account','Domains') ;
		return view('page.v1.account.domains',$this->viewData);
	}
	
	public function applications(){
		$this->viewData['selected'] = array('Account','Applications') ;
		return view('page.v1.account.applications',$this->viewData);
	}
	
	public function logs(){
		$this->viewData['selected'] = array('Account','Logs') ;
		return view('page.v1.account.logs',$this->viewData);
	}
}