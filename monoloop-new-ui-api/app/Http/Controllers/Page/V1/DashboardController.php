<?php namespace App\Http\Controllers\Page\V1;

use Auth; 
use Illuminate\Http\Request;

class DashboardController extends PageController {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{ 
		$this->viewData['selected'] = array('Dashboard','Dashboard') ;
		return view('page.v1.dashboard',$this->viewData);
	}
	
	public function funnel(){
		$this->viewData['selected'] = array('Dashboard','Funnel') ;
		return view('page.v1.funnel',$this->viewData);
	}
}