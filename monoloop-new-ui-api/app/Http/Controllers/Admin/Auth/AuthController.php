<?php namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;


use App\User;
use App\AccountUser;
use App\Account;

use Krucas\Notification\Facades\Notification;

class AuthController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct()
	{
		#$this->middleware('guest', ['except' => 'getLogout']);
	}

  public function getLogin()
	{
		return view('admin.auth.login');
	}

	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'username' => 'required', 'password' => 'required',
		]);

		$credentials = $request->only('username', 'password');

		if (Auth::attempt($credentials, true,false))
		{
      if(Auth::getLastAttempted()->isAdmin){
        Auth::login(Auth::getLastAttempted(),$request->has('remember'));
        return redirect()->intended($this->redirectPath());
      }else{
        Notification::error('Invalid access.');
      }
		}else{
      Notification::error('Invalid username or password.');
    }

		return redirect('mladmin/auth/login')
					->withInput($request->only('username', 'remember'))
					->withErrors([
						'username' => $this->getFailedLoginMessage(),
					]);

	}

}
