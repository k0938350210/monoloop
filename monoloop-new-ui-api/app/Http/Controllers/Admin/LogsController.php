<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Account ;
use App\User;
use App\Log ;
use Notification ;
use MongoId;

class LogsController extends Controller {
  public function index(Request $requests){
    $q = $requests->input('q') ;
    $start = $requests->input('start');
    $end = $requests->input('end');

    $account = $requests->input('account');
    $user = $requests->input('user');
    $logs = Log::search($q,$start,$end) ;
    $accountObj = null ;
    $userObj = null ;
    if(trim($account) != ''){
      $accountObj = Account::find($account) ;
      $logs = $logs->byAccount($account) ;
    }
    if(trim($user) != ''){
      $userObj = User::find($user);
      $logs = $logs->byUser($user) ;
    }
    $logs = $logs->orderBy('updated_at','DESC')->paginate(100)  ;

    return view('admin.log.list')->with('logs',$logs)->with('q',$q)->with('start',$start)->with('end',$end)->with('account',$account)->with('user',$user)->with('accountObj' , $accountObj)->with('userObj',$userObj);
  }
}
