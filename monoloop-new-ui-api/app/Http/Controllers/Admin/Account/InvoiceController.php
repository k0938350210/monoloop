<?php namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Account ;
use App\CustomerInvoice ;

use Notification ;
use MongoId;

class InvoiceController extends Controller {
  public function create(Request $request , $id){
    $account = Account::find($id) ;
    return view('admin.page.accounts.invoice.new')->with('account',$account);
  }

  public function store(Request $request , $id){
    $account = Account::find($id) ;

    $invoice = new CustomerInvoice() ;
    $invoice->invoiceDate = $request->input('invoiceDate') ;
    $invoice->includedMonlyVolume = $request->input('includedMonlyVolume') ;
    $invoice->price = $request->input('price') ;
    $invoice->currency = $request->input('currency') ;
    $invoice->account_id = new MongoId($account->_id);

    if ($invoice->save()) {
			Notification::success('Successfull create new invoice');
			return redirect('mladmin/accounts/'.$account->id) ;
		}else{
			Notification::error( $invoice->errors()->All());
      return redirect('mladmin/accounts/'.$account->id.'/invoice')->withInput() ;
		}
  }
}
