<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User ;
use App\Log ;
use Notification ;
use MongoId;

class UsersController extends Controller {
  public function index(Request $requests){
    $q = $requests->input('q') ;
    $users = User::search($q)->orderBy('username','ASC')->paginate(20)  ;
    return view('admin.page.users.list')->with('q' , $q)->with('users',$users) ;
  }

  public function show($id , Request $requests ){
    $user = User::find($id) ;
    return view('admin.page.users.show')->with('user',$user);
  }

  public function update(Request $request,$id){
    $user = User::find($id) ;
    $user->admin = (int)$request->input('admin',0);

    if ($user->save()) {
      Notification::success('Successfull update user status');
      return redirect('mladmin/users/' . $user->_id ) ;
		}else{
			Notification::error( $page->errors()->All());
			return redirect('mladmin/users/' . $user->_id  )->withInput() ;
		}
  }
}
