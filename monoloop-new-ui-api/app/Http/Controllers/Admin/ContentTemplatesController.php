<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ContentTemplate ;
use Notification ; 

class ContentTemplatesController extends Controller {
  public function index(){
    $content_templates = ContentTemplate::orderBy('name','asc')->get() ;
    return view('admin.page.content_template.list')->with('content_templates',$content_templates);
  }

  public function edit(Request $requests , $id)
	{
		$content_template = ContentTemplate::find($id);
		return view('admin.page.content_template.edit')
			->with('content_template', $content_template);
	}

  public function update(Request $requests , $id)
	{
		$content_template = ContentTemplate::find($id);
		$content_template->name = $requests->input('name');
		$content_template->template = $requests->input('template');

		if ($content_template->save()) {
      Notification::success('Successfull update template <a href="'.url('mladmin/content_templates/' . $content_template->_id . '/edit').'">'.$content_template->name.'</a>');
      return redirect('mladmin/content_templates') ;
		}else{
			Notification::error( $page->errors()->All());
			return redirect('mladmin/content_templates/' . $content_template->_id . '/edit')->withInput() ;
		}
	}
}
