<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Account ;
use App\PageElement ;
use App\Log ;
use App\CustomerInvoice ;
use App\BillingHistorical ;
use Notification ;
use MongoId;
use Carbon ;
use App\ReportingArchiveAggregated ;
use App\Jobs\FrontendInvalidate;
use App\Jobs\PrivacyDefault ;

class AccountsController extends Controller {
  public function index(Request $requests){
    $q = $requests->input('q') ;
    $order_by = $requests->input('order_by','uid');
    $dir = $requests->input('dir','asc');

    $accounts = Account::search($q)->orderBy($order_by,$dir)->paginate(20)  ;
    return view('admin.page.accounts.list')->with('q' , $q)->with('order_by',$order_by)->with('dir',$dir)->with('accounts',$accounts);
  }

  public function show(Request $requests,$id){
    $account = Account::find($id) ;
    $last_month = new Carbon('last month') ;
    $report_start = $requests->input('report_start', $last_month->format('m/d/Y') );
    $report_end = $requests->input('report_end',Carbon::now()->format('m/d/Y'));
    $logs = Log::where('account_id', new MongoId($id) )->limit(20)->get() ;
    //$invoices = CustomerInvoice::where('account_id',new MongoId($id))->orderBy('created_at','ASC')->limit(20)->get() ;
    $billings =BillingHistorical::where('account_id',$id)->orderBy('created_at','DESC')->limit(20)->get() ;

    list($month1, $day1, $year1) = explode('/', $report_start);
    list($month2, $day2, $year2) = explode('/', $report_end);

    $ts =  mktime(0, 0, 0, $month1, $day1, $year1);
    $ts2 = mktime(0, 0, 0, $month2, $day2, $year2); ;

    $reports = ReportingArchiveAggregated::where('datestamp','>=',$ts)->where('datestamp','<=',$ts2)->where('cid',$account->uid)->get() ;

    return view('admin.page.accounts.show')->with('account',$account)->with('logs',$logs)->with('billings',$billings)->with('report_start',$report_start)->with('report_end',$report_end)->with('reports',$reports)  ;
  }

  public function update(Request $request,$id){
    $account = Account::find($id) ;
    $account->AccType = $request->input('AccType');
    $account->account_type = $request->input('account_type');

    if ($account->save()) {
      Notification::success('Successfull update account status');

      $pageElement = PageElement::where('fullURL' , 'default')->where('cid',$account->uid)->first();
      if($pageElement){
        $pageElementContent = $pageElement->content()->where('name', 'privacy')->where('uid', NULL)->first();
        if($pageElementContent){
          if($account->AccType === 'Paid'){
            $pageElementContent->delete() ;
          }
        }else{
          if($account->AccType === 'Free'){
            $this->dispatch(new PrivacyDefault($account));
          }
        }
        $pageElement->save() ;
      }


      return redirect('mladmin/accounts/' . $account->_id ) ;
		}else{
			Notification::error( $page->errors()->All());
			return redirect('mladmin/accounts/' . $account->_id  )->withInput() ;
		}
  }
}
