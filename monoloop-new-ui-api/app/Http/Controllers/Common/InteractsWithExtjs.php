<?php namespace App\Http\Controllers\Common ; 

use App\Services\ExtjsFilter; 

trait InteractsWithExtjs
{
  /*------- custom --------*/ 
  public  function isExtjsFilter($request){ 
		if($request->input('filter_type') == 'extjs')
      return true ; 
    return false ; 
	}
  
  public function extjsFilter($query , $request , $mapOrder = [] ){
    return ExtjsFilter::filter($query,$request,[])  ;
  }
}