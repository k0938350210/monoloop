<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Proxy\MonoloopProxy;
use ForceUTF8\Encoding;

class PlacementProxyController extends Controller {
  public function show(Request $request , $id ){
    #id will type of each placement page ;
    $url = $request->input('l') ; 
    $config = $this->getConfigData($id,$url) ;
    $monoloopProxy = new MonoloopProxy() ;
    $monoloopProxy->process($id,$url) ;
    $monoloopProxy->processHeader(true,$config['additional_head']);
    $monoloopProxy->processForm() ;
    $monoloopProxy->processBody($config['additional_body']);
    $monoloopProxy->clean() ;
    $monoloopProxy->render();
  }

  private function getConfigData($id,$url){
    $additional_head = '' ;
    $additional_body = '' ;
    #why we no cache here ?
    $proxy_cbr = config('app.url').'/js/invocation/proxy_cbr.js?data='.time() ;
    switch ($id){
      case 'category-preview':
        $additional_head = '<script type="text/javascript">var ml_tf_url = "'.Encoding::toUTF8( $url ).'" ;  </script><script src="'.$proxy_cbr.'" type="text/javascript"></script>' ;
        $additional_body = '<input type="hidden" id="ml-tf-url" value="'.  Encoding::toUTF8( $url ).'" /><input type="hidden" id="ml-cate-testdata" value="" />' ;
      break;
    	case 'category-xpath':
        $additional_head = '<script type="text/javascript">var ml_tf_url = "'.Encoding::toUTF8( $url ).'" ;  </script><script src="'.$proxy_cbr.'" type="text/javascript"></script>' ;
        $additional_body = '<input type="hidden" id="ml-tf-url" value="'.  Encoding::toUTF8($url).'" /><div id="ml-border-l" class="ml-border"></div><div id="ml-border-r" class="ml-border"></div><div id="ml-border-s" class="ml-border"></div><div id="ml-border-u" class="ml-border"></div><div id="ml-borderb-l" class="ml-borderb"></div><div id="ml-borderb-r" class="ml-borderb"></div><div id="ml-borderb-s" class="ml-borderb"></div><div id="ml-borderb-u" class="ml-borderb"></div>' ;
    	break;
      case 'asset' :
        $additional_head = '<script type="text/javascript">var ml_tf_url = "'.Encoding::toUTF8( $url ).'" ;  </script><script src="'.$proxy_cbr.'" type="text/javascript"></script>' ;
        $additional_body = '<input type="hidden" id="ml-tf-url" value="'. Encoding::toUTF8( $url ).'" /><div id="ml-border-l" class="ml-border"></div><div id="ml-border-r" class="ml-border"></div><div id="ml-border-s" class="ml-border"></div><div id="ml-border-u" class="ml-border"></div><div id="ml-borderb-l" class="ml-borderb"></div><div id="ml-borderb-r" class="ml-borderb"></div><div id="ml-borderb-s" class="ml-borderb"></div><div id="ml-borderb-u" class="ml-borderb"></div>' ;
      break;
      case 'basket' :
        $additional_head = '<script type="text/javascript">var ml_tf_url = "'.Encoding::toUTF8( $url ).'" ;  </script><script src="'.$proxy_cbr.'" type="text/javascript"></script>' ;
        $additional_body = '<input type="hidden" id="ml-tf-url" value="'. Encoding::toUTF8( $url ).'" /><div id="ml-border-l-product" class="ml-border-product"></div><div id="ml-border-r-product" class="ml-border-product"></div><div id="ml-border-s-product" class="ml-border-product"></div><div id="ml-border-u-product" class="ml-border-product"></div> <div id="ml-border-l-row" class="ml-border-row"></div><div id="ml-border-r-row" class="ml-border-row"></div><div id="ml-border-s-row" class="ml-border-row"></div><div id="ml-border-u-row" class="ml-border-row"></div> <div id="ml-cart-table-list"></div><div id="ml-cart-table-select"><div class="l1" style="display:none">Cart table</div><div class="l2" style="display:none">Product row</div><div class="l3" style="display:none">Product Name</div><div class="l4" style="display:none">SKU</div><div class="l5" style="display:none">Price</div><div class="l6" style="display:none">Quantity</div></div>' ;
      break;
      case 'preview' :
        $additional_head = '<script type="text/javascript">var ml_tf_url = "'.Encoding::toUTF8( $url ).'" ;  </script><script src="'.$proxy_cbr.'" type="text/javascript"></script>' ;
        $additional_body = '<input type="hidden" id="ml-tf-url" value="'.  Encoding::toUTF8( $url ).'" />' ;
      break;
      case 'select-view' :
        $additional_head = '<script type="text/javascript">var ml_tf_url = "'.Encoding::toUTF8( $url ).'" ;  </script><script src="'.$proxy_cbr.'" type="text/javascript"></script>' ;
        $additional_body = '<div id="preplaced-panel" style="position:absolute;top:0px;left:0px"></div><div id="ml-borders"></div><div id="ml-border-l" class="ml-border"></div><div id="ml-border-r" class="ml-border"></div><div id="ml-border-s" class="ml-border"></div><div id="ml-border-u" class="ml-border"></div>' ;
      break;
      case 'testbench-view' :
        $additional_head = '<script type="text/javascript">var ml_tf_url = "'.Encoding::toUTF8( $url ).'" ;  </script><script src="'.$proxy_cbr.'" type="text/javascript"></script>' ;
        $additional_body = '<div id="preplaced-panel" style="position:absolute;top:0px;left:0px"></div>' ;
      break;
      case 'tracker-search' :
        $additional_head = '<script type="text/javascript">var ml_tf_url = "'.Encoding::toUTF8( $url ).'" ;  </script><script src="'.$proxy_cbr.'" type="text/javascript"></script>' ;
        $additional_body = '<input type="hidden" id="ml-tf-url" value="'. Encoding::toUTF8( $url  ).'" /><div id="ml-cart-table-list"></div><div id="ml-cart-table-select"><div class="l1" style="display:none">Search box</div><div class="l2" style="display:none">Search button</div></div>' ;

      break;
    	default :
    }
    return ['additional_head' => $additional_head , 'additional_body' => $additional_body ] ;
  }


}
