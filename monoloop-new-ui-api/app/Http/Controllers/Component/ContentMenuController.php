<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Input;
use Validator;
use View;

use App\PageElement;
use App\Content;

class ContentMenuController extends Controller
{
    /*
   *
   * @return view
   */

  public function index(Request $request)
  {
      $this->viewData = [];

      $validator = Validator::make($request->all(), ['xpath' => 'required', 'fullURL' => 'required'] );
      if($validator->fails()){
        $this->viewData['status'] = 'error';
        $this->viewData['msg'] = $validator->errors()->all();
        return response()->json($this->viewData, 400);
      }

      $this->viewData['xpath'] = $request->input('xpath', "");
      $this->viewData['xpath1'] = $request->input('xpath1', "");
      $this->viewData['fullURL'] = $request->input('fullURL', "");
      $this->viewData['page_element_id'] = $request->input('page_element_id', "");

      $this->viewData['content_id'] = $request->input('content_id', false);

      if($this->viewData['content_id']){
        $content = Content::find($this->viewData['content_id']);
        $this->viewData['content_name'] = $content->name;
        $this->viewData['content_condition'] = $content->condition;
      } else {
        $this->viewData['content_name'] = "";
        $this->viewData['content_condition'] = "";
      }

      $this->viewData['bk_segment_id'] = $request->input('bk_seg', "");
      $this->viewData['bk_goal_id'] = $request->input('bk_goal', "");

      $page_element = PageElement::find($this->viewData['page_element_id']);
      $this->viewData['includeWWWWebPageElement'] = $page_element ? $page_element->inWWW : 1;
      $this->viewData['includeHttpHttpsWebPageElement'] = $page_element ? $page_element->inHTTP_HTTPS : 0;
      $this->viewData['urlOptionWebPageElement'] = $page_element ? $page_element->urlOption : 0;
      $this->viewData['regularExpressionWebPageElement'] = $page_element ? $page_element->regExp : '';
      $this->viewData['remarkWebPageList'] = $page_element? $page_element->remark : '';
      $this->viewData['fullURLWebPageList'] = $page_element ? $page_element->originalUrl : $this->viewData['fullURL'];

  		$this->viewData['hashFn'] = $request->input('hash_fn', "_mp_hash_function_");

  		$this->viewData['xpath'] = str_replace($this->viewData['hashFn'], "#", $this->viewData['xpath']);

      $this->viewData['current_folder'] = $request->input('current_folder', "");
      $this->viewData['source'] = $request->input('source', "");
      $this->viewData['experiment_id'] = $request->input('exp', "");
      $this->viewData['page'] = $request->input('page', "");
      $this->viewData['totalVariations'] = $request->input('tVar', "");

      $this->viewData['havVar'] = $request->input('havVar', 0);
      $this->viewData['tagName'] = $request->input('tagName', "");

      return view('component.content.content_menu', $this->viewData);
  }

}
