<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;
use App\Services\Profile\Profile;
use Input;
use App\Segment;
use View;
use Response;

class ConditionBuilderController extends Controller
{
    /*
   * Return the properties of required condition parameters
   *
   * @return json
   */

  public function index()
  {
      $profile = new Profile();

      return response()->json($profile->getProperties());
  }

  /*
   * Return the template for condition builder
   *
   * @return html
   */

  public function template()
  {
      $this->viewData = [];

      $source = Input::get('source');

      switch ($source) {
      case 'segment':

      $segment_id = Input::get('id');

      if (strpos($segment_id, '__') !== false) {
          $srcTypeId = explode('__', Input::get('id'));
          $segment_id = $srcTypeId[1];
      }

      if (!empty($segment_id)) {
          $this->viewData['model'] = Segment::find($segment_id);
      } else {
      }

      $this->viewData['_id'] = $segment_id;

      if (!empty($this->viewData['_id'])) {
          $condition = $this->viewData['model']->condition_json;
      } else {
          $condition = '{}';
      }

      $this->viewData['_condition'] = $condition;
      $this->viewData['_source'] = $source;
      $this->viewData['_action'] = '/api/segments/update';
        break;

      default:
        $this->viewData['_id'] = false;
        $this->viewData['_condition'] = '{}';
        $this->viewData['_source'] = "default";
        $this->viewData['_action'] = '/api/default/update';
        break;
    }
      return view('component.condition_builder_template', $this->viewData);
  }
}
