<?php namespace App\Http\Controllers\Component;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use Response;

use Auth;
use Validator;
use App\PageElement;

class TestbenchController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function preview(Request $request){
    $url = $request->input('l');
    $urlEncode = urlencode($url);
    $data = $request->input('data');

    if(is_array($data)){
      foreach( $data as $k => &$v){
          if( ! isset($v['main']))
              continue ;

          $mainSplit = explode('|' , $v['main']);
          $connector = array_shift($mainSplit);
          $v['connector'] = $connector;

          $field = array_shift($mainSplit);
          $v['field'] = $field;

          $type = array_shift($mainSplit);
          $v['type'] = $type;

          $ReturnValue = array_shift($mainSplit);
          $v['ReturnValue'] = $ReturnValue;

          $value = implode('|' , $mainSplit);
          $v['value'] = $value;
      }
    }else{
      $data = [];
    }
    $testbench_tabel = view('/component/testbench/table')->with('data',$data);

    $active_account = Auth::user()->account;
    $pageElements = PageElement::where('originalUrl','=',$url)->where('deleted', '=', 0)->where('cid', '=', $active_account->uid)->get();
    $allCondition = null;
    $mongoDetail = [];
    foreach($pageElements as $pageElement){
      foreach( $pageElement->content  as $content ){
        $allCondition .= $content->extractCondition;
        $mongoDetail[] = $content->toArray();
      }
    }

    return view('/component/testbench/preview')->with('table',$testbench_tabel)->with('data',$data)->with('url',$url)->with('mongoDetail',$mongoDetail);
  }
}
