<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Services\Hasher;
use App\Services\Account\AccountUserService;
use App\Services\Account\Cbr;
use App\Services\Account\Invocation;
use App\Services\Account\SugarLead;
use App\Services\Account\AccountService;
use App\Services\Account\MailService;
use App\Services\Account\DefaultAudience;
use App\Services\Account\DefaultTracker;
use App\Services\Account\DefaultFunnel;
use App\Services\Trackers\PreconfiguredWpTrackers ;

use App\Services\Redis\RedisService;

use MongoId ;
use Validator;

use Auth;
use Session;

use App\User;
use App\AccountUser;
use App\Account;
use App\Folder;

use App\Events\Logger;

use App\Http\Controllers\Common\InteractsWithCbr ;
use App\Jobs\SugarLeadAndMailChimp;
use App\Jobs\PrivacyDefault ;
use App\AccountDomain;

use Notification ;

use Carbon\Carbon ;
use App\OCRConfigs;

class AuthController extends Controller {

  /*
  |--------------------------------------------------------------------------
  | Registration & Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users, as well as the
  | authentication of existing users. By default, this controller uses
  | a simple trait to add these behaviors. Why don't you explore it?
  |
  */

  use AuthenticatesAndRegistersUsers;
  use InteractsWithCbr;

  /**
   * Create a new authentication controller instance.
   *
   * @param  \Illuminate\Contracts\Auth\Guard  $auth
   * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
   * @return void
   */
  public function __construct(Guard $auth, Registrar $registrar)
  {
    $this->auth = $auth;
    $this->registrar = $registrar;

    $this->middleware('guest', ['except' => 'getLogout']);
  }

  public function postLogin(Request $request)
  {

    $this->validate($request, [
      'username' => 'required', 'password' => 'required',
    ]);

    $credentials = $request->only('username', 'password');
    $response = $this->authenticateLogin($credentials);
    $header = $response['header'];

    $headers = $this->getHederInfo($header);

    $body = $response['body'];
    $resArr = array();
    $resArr = json_decode($body);
    // var_dump($resArr);die;
    if($resArr->is_success === 1){

      $ml_token = $headers['Ml-Token'];
      $ml_token = substr($ml_token, 0, -1);
      $ml_token = substr($ml_token,1);

      $user = User::find($resArr->response->_id->{'$id'});

      if (strlen($user) > 0){
        Session::set('mlToken', $ml_token);

        Auth::login($user);
        \Event::fire(new Logger(Auth::user()->username, 'user login', null , "App\User"));
        // return redirect()->intended($this->redirectPath());
        // return redirect('/');
        return RedisService::intendedRedirect(Auth::user()->_id);
      } else {
        return redirect($this->loginPath())
              ->withInput($request->only('username', 'remember'))
              ->withErrors([
                'email' => 'Invalid username or password.',
              ]);
      }
    } else {
      return redirect($this->loginPath())
            ->withInput($request->only('username', 'remember'))
            ->withErrors([
              'email' => 'Invalid username or password.',
            ]);
    }


  }
  private function getHederInfo($header_string){
    $myarray=array();
    $data = explode("\n", $header_string);

    $myarray['status'] = $data[0];

    array_shift($data);

    foreach($data as $part){
      $middle = explode(":",$part);
      if(count($middle) === 2){
        $myarray[trim($middle[0])] = trim($middle[1]);
      }
    }
    return $myarray;
  }

  public function authenticateLogin($fields){
    $apihost = config('app.monoloop')['apihost'];
    $url = $apihost.'/api/user/login';
    $headers = ['Content-Type: application/json', 'ML-Version: v1.0'];

    $fields_string = json_encode($fields);
    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    //execute post
    $response = curl_exec($ch);
    $result = [];

    // Then, after your curl_exec call:
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $header_size);
    $result['body'] = substr($response, $header_size);
    //close connection
    curl_close($ch);
    return $result;
  }

  public function postRegister(Request $request)
  {

    $block_ips = ['178.159.37.84'];
    if(in_array($request->ip(), $block_ips)){
      return;
    }

    $this->validate($request, [
     'firstname' => 'required',
     'lastname' => 'required',
     'email' => 'required|email|unique:users,username',
     'confirmemail' => 'required|email|unique:users|same:email' ,
     // 'accountname' => 'required|unique:accounts,name' ,
     'user_type' => 'required|In:free,enterprise,agency',
     'password' => 'required|min:6' ,
     'confirmpassword' => 'required|same:password' ,
   ]);
   $formData = $request->all();
   $formData['username'] = $formData['confirmemail'];
   if($formData['user_type'] == 'free'){
    $formData['user_type'] = 'enterprise' ;
   }
  //  var_dump($formData);die;
  if(isset($formData['source'])){
    $source = !empty($formData['source']) ? $formData['source'] : 'Other';
  } else {
    $source = "Other";
  }

   $response = $this->register($formData);
   $header = $response['header'];

   $headers = $this->getHederInfo($header);

   $body = $response['body'];
   $resArr = array();
   $resArr = json_decode($body);

   if($resArr->is_success === 1){

     $ml_token = $headers['Ml-Token'];
     $ml_token = substr($ml_token, 0, -1);
     $ml_token = substr($ml_token,1);

     $account = Account::find($resArr->response->account_id);
     $adminUser = AccountUser::find($resArr->response->account_user_id);
     $user = User::find($resArr->response->user_id);

     #3 Save account signup url ;
     $account->signup_ip = $request->ip();
     $account->save() ;

     #save OCR settings in OCRConfigs collections
     $ocrModel = new OCRConfigs();
 		 $ocrModel->cid = $account->cid;
 		 $ocrModel->ocReady = false ;
 		 $ocrModel->save();

     #4. send confirmation mail ;
     $mailService = new MailService() ;
     $mailService->sendComfirmationMail($adminUser , $user) ;


     #6. call job for generate cbr
     $this->generateCbr($account) ;

     #7 generate root folder for collections
     // creating new folder
     $folder = new Folder();
     $folder->account_id = $user->active_account;
     $folder->cid = $account->uid;
     $folder->deleted = 0;
     $folder->hidden = 0;
     $folder->title = 'Root';
     $folder->parent_id = null;
     $folder->created_at = new \Datetime();
     $folder->updated_at = new \Datetime();
     $folder->save();

     #8 Create privacy default content
     $this->dispatch(new PrivacyDefault($account));

     #9 Add remaining_pageviews ;

     if($account->AccType == 'Paid'){
        $account->remaining_pageviews = config('services.billing.page_views.paid');
     }else{
        $account->remaining_pageviews = config('services.billing.page_views.free');
     }

     $account->last_billing_date = Carbon::today();
     $account->cms_site_url = $request->input('cms_site_url','') ;
     $account->wp_plugins = json_decode($request->input('wp_plugins',''),true);
     $account->source = $source ;

     $invocation = $account->invocation;
     $invocation['postback_delay'] = 1000;
     $account->invocation = $invocation;

     $account->save();

     /*
     $mailService->sendNewSignUptoAdmins($account, $adminUser, $user, $source);
     */
     #10. call job for ( mailchrimp & sugar lead )
     $this->dispatch(new SugarLeadAndMailChimp($account,$source));

     #Create default audeince

     $defaultAudience = new DefaultAudience() ;
     $defaultAudience->createDefault($account) ;

     #Create default tracker

    $default_tracker = new DefaultTracker();
    $default_tracker->createDefault($account);

     #Create default funnel

    $default_funnel = new DefaultFunnel();
    $default_funnel->createDefault($account);

     #preconfigured trackers

     if($account->source == 'WP Plugin'){
        $preconfiguredWpTrackers = new PreconfiguredWpTrackers() ;
        $preconfiguredWpTrackers->updateDefault($account);
     }

      #10 update null user_id with same email
      $account_users = AccountUser::whereNull('user_id')->where('email',$user->username)->get();
      foreach($account_users as $account_user){
        $account_user->user_id = $user->id;
        $account_user->save();
      }

      #11 if post with accountId so set with active account
      $account_id = $request->input('accountId');
      if($account_id && AccountUser::where('account_id',$account_id)->where('user_id',$user->id)->exists()){
        $user->active_account = new MongoId($account_id);
        $user->save();
      }

     return redirect('/please_confirm_your_registration')->withInput($request->only('name', 'email'));
   } else {

   }


  }

  public function register($fields){
    $apihost = config('app.monoloop')['apihost'];
    $url = $apihost.'/api/user/register';
    $headers = ['Content-Type: application/json', 'ML-Version: v1.0'];

    $fields_string = json_encode($fields);

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    //execute post
    $response = curl_exec($ch);
    $result = [];

    // Then, after your curl_exec call:
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $header_size);
    $result['body'] = substr($response, $header_size);

    //close connection
    curl_close($ch);
    return $result;
  }
  /**
   * Log the user out of the application.
   *
   * @return \Illuminate\Http\Response
   */
  public function getLogout()
  {
    \Event::fire(new Logger($this->auth->user()->username, 'user logout', null , "App\User"));
    $this->auth->logout();

    Session::set('mlToken', '');
    return redirect('/');

  }

}
