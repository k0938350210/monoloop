<?php

namespace App\Http\Controllers\data;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Segment;
use App\Goal;
use App\ControlGroup;

class ExperimentController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Experiment basic view
     *
     * @return Response
     */
    public function getbasic()
    {
      $dataSet = [];

      return response()->json($dataSet);
    }

    /**
     * Experiment segment
     *
     * @return Response
     */
    public function getSegment()
    {
      $dataSet = [];

      $account_id = Auth::user()->active_account;
      $account_cid = Auth::user()->account->uid;

      $dataSet['segments'] = Segment::GetAll($account_cid);

      return response()->json($dataSet);
    }

    /**
     * Experiment goal
     *
     * @return Response
     */
    public function getGoal()
    {
      $dataSet = [];

      $account_id = Auth::user()->active_account;
      $account_cid = Auth::user()->account->uid;

      $dataSet['goals'] = Goal::GetAll($account_cid);

      return response()->json($dataSet);
    }

    /**
     * Experiment palcement
     *
     * @return Response
     */
    public function getPlacement()
    {
      $dataSet = [];

      return response()->json($dataSet);
    }

    /**
     * Experiment control group
     *
     * @return Response
     */
    public function getControlGroup()
    {
      $dataSet = [];
      $dataSet['significantActions'] = ControlGroup::getSignificantActions();
      return response()->json($dataSet);
    }

    /**
     * Experiment confirm data
     *
     * @return Response
     */
    public function getConfirm()
    {
      $dataSet = [];
      return response()->json($dataSet);
    }
}
