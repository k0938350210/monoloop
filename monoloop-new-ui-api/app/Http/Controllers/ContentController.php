<?php namespace App\Http\Controllers;

use Auth ;
use App\Content ;
use DbView ;

class ContentController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function show($id){
    $content = Content::find($id) ;
    if(is_null($content)){
      abort(404);
    } ;
    $html = DbView::make($content->content_template)->field('template')->with($content->config)->with('account',$content->account)->with('uid',$content->uid)->render();
    return view('preview')->with('html',$html)->with('content',$content);
  }

  public function showPrivacy(){
  	return view('page.content.demo.privacy-center')->with('preview',1);
  }

}
