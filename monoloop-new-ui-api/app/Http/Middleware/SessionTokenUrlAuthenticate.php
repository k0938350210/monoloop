<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;


use App\User;
use App\Account;
use App\Services\Hasher;
use App\AccountUser ;

class SessionTokenUrlAuthenticate{

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
  	$token = $request->input('token');
  	if($token){
  		$user_id = \App\Services\Redis::getUserIdFromRedisToken("a".$token);
  		$this->auth->login(User::find($user_id)) ;
  	}
		return $next($request);
	}

}
