<?php

Route::group(['prefix' => 'api'], function()
{
  Route::get('dashboard','Api\DashboardController@index');

  Route::get('profile','Api\UserController@profile');
  Route::post('profile','Api\UserController@profileUpdate');
  Route::post('profile/comply-gdpr','Api\UserController@complyGdpr');
  Route::any('beanstalk2matis','Api\Beanstalk2MantisController@index');

  // Segment
  Route::get('segments/show', 'Api\SegmentController@show');
  Route::post('segments/create', 'Api\SegmentController@create');
  Route::post('segments/update', 'Api\SegmentController@update');
  Route::post('segments/update-status', 'Api\SegmentController@updateStatus');
  Route::post('segments/change-folder', 'Api\SegmentController@changeFolder');
  Route::post('segments/delete', 'Api\SegmentController@delete');
  Route::any('segments/{type?}', 'Api\SegmentController@index');
  Route::post('segment/{id}/duplicate', 'Api\SegmentController@duplicate');

  // Funnel
  Route::get('funnels/show', 'Api\FunnelController@show');
  Route::post('funnels/create', 'Api\FunnelController@create');
  Route::post('funnels/update', 'Api\FunnelController@update');
  Route::post('funnels/update-status', 'Api\FunnelController@updateStatus');
  Route::post('funnels/change-folder', 'Api\FunnelController@changeFolder');
  Route::post('funnels/delete', 'Api\FunnelController@delete');
  Route::any('funnels/{type?}', 'Api\FunnelController@index');

  // Tracker
  Route::get('trackers/show', 'Api\TrackerController@show');
  Route::post('trackers/create', 'Api\TrackerController@create');
  Route::post('trackers/update', 'Api\TrackerController@update');
  Route::post('trackers/update-status', 'Api\TrackerController@updateStatus');
  Route::post('trackers/change-folder', 'Api\TrackerController@changeFolder');
  Route::post('trackers/delete', 'Api\TrackerController@delete');
  Route::any('trackers/{type?}', 'Api\TrackerController@index');

  Route::post('media','Api\MediaController@index');
  Route::post('media/store','Api\MediaController@store');
  Route::post('media/crop','Api\MediaController@crop');
  Route::post('media/resize_detail','Api\MediaController@resizeDetail');
  Route::post('media/resize','Api\MediaController@resize');
  Route::post('media/move','Api\MediaController@move');

  Route::get('media/folder','Api\MediaController@folder');
  Route::post('media/folder/create','Api\MediaController@folderCreate');
  Route::post('media/folder/update','Api\MediaController@folderUpdate');
  Route::post('media/folder/delete','Api\MediaController@folderDelete');

  // Webhooks
  Route::get('webhooks/show', 'Api\WebhookController@show');
  Route::get('webhooks/getall', 'Api\WebhookController@getallWebhooks');
  Route::post('webhooks', 'Api\WebhookController@create');
  Route::post('webhooks/delete', 'Api\WebhookController@delete');
  Route::put('webhooks/{id}','Api\WebhookController@update');
  Route::any('webhooks/{type?}', 'Api\WebhookController@index');


  // Custom Var
  Route::resource('customvars', 'Api\CustomFieldController');

  // Experiments
  Route::get('experiments/show', 'Api\ExperimentController@show');
  Route::get('experiments/getallexp', 'Api\ExperimentController@getallExp');
  Route::get('experiments/getPages', 'Api\ExperimentController@getPages');
  Route::get('experiments/getCID', 'Api\ExperimentController@getAccountCID');
  Route::get('experiments/update-experiment-script','Api\UpdateExperiment@index');
  Route::post('experiments/create', 'Api\ExperimentController@create');
  Route::post('experiments/update', 'Api\ExperimentController@update');
  Route::post('experiments/update-status', 'Api\ExperimentController@updateStatus');
  Route::post('experiments/change-folder', 'Api\ExperimentController@changeFolder');
  Route::post('experiments/calculate-pvalue', 'Api\ExperimentController@calculatePValue');
  Route::post('experiments/delete', 'Api\ExperimentController@delete');
  Route::any('experiments/{type?}', 'Api\ExperimentController@index');


  // Blueprints
  Route::get('blueprints/show', 'Api\BlueprintController@show');
  Route::post('blueprints/create', 'Api\BlueprintController@create');
  Route::post('blueprints/update', 'Api\BlueprintController@update');
  Route::any('blueprints/{type?}', 'Api\BlueprintController@index');

  // web page elements
  Route::get('content/web/show', 'Api\ContentWebController@show');
  Route::post('content/web/create', 'Api\ContentWebController@create');
  Route::post('content/web/update', 'Api\ContentWebController@update');
  Route::post('content/web/update-status', 'Api\ContentWebController@updateStatus');
  Route::post('content/web/change-folder', 'Api\ContentWebController@changeFolder');
  Route::post('content/web/delete', 'Api\ContentWebController@delete');
  Route::any('content/web/{type?}', 'Api\ContentWebController@index');

  // web content list elements
  Route::get('content/web-content/show', 'Api\WebContentListController@show');
  Route::post('content/web-content/create', 'Api\WebContentListController@create');
  Route::post('content/web-content/update', 'Api\WebContentListController@update');
  Route::post('content/web-content/update-status', 'Api\WebContentListController@updateStatus');
  Route::post('content/web-content/change-folder', 'Api\WebContentListController@changeFolder');
  Route::post('content/web-content/delete', 'Api\WebContentListController@delete');
  Route::any('content/web-content/{type?}', 'Api\WebContentListController@index');

  // Goals
  Route::get('goals/show', 'Api\GoalController@show');
  Route::post('goals/create', 'Api\GoalController@create');
  Route::post('goals/update', 'Api\GoalController@update');
  Route::post('goals/update-status', 'Api\GoalController@updateStatus');
  Route::post('goals/change-folder', 'Api\GoalController@changeFolder');
  Route::post('goals/delete', 'Api\GoalController@delete');
  Route::any('goals/{type?}', 'Api\GoalController@index');
  Route::post('goals/{id}/duplicate', 'Api\GoalController@duplicate');

  // Folder / Segments
  Route::post('folders/create', 'Api\FolderController@create');
  Route::post('folders/update', 'Api\FolderController@update');
  Route::post('folders/delete', 'Api\FolderController@delete');
  Route::post('folders/move', 'Api\FolderController@move');
  Route::post('folders/status', 'Api\FolderController@status');

  //Account
  Route::get('account','Api\AccountController@profile');
  Route::post('account/controllgroup','Api\AccountController@controllgroupUpdate');
  Route::post('account/invocation','Api\AccountController@invocationUpdate');
  Route::post('account/invocation/prepost','Api\AccountController@prepostUpdate');
  Route::post('account/invocation/spa','Api\AccountController@spaUpdate');
  Route::post('account/scores/update','Api\AccountController@scoresUpdate');
  Route::post('account/update_wpplugins','Api\AccountController@wpPluginsUpdate');

  //Account - plugins
  Route::post('account/plugins','Api\Account\PluginController@index');
  Route::post('account/plugins/{id}','Api\Account\PluginController@update');

  //Account - logs
  Route::get('account/logs','Api\Account\LogController@index');

  // Account Domains
  Route::resource('domain', 'Api\Account\DomainController');
  Route::post('domains/test-domain/{id}','Api\Account\DomainController@testDomain');

  // Account applications
  Route::resource('applications', 'Api\Account\AppConfigController');

  // Account client accounts
  Route::resource('client-account', 'Api\Account\ClientAccountController');
  Route::post('client-account/status-update/{id}','Api\Account\ClientAccountController@statusUpdate');
  Route::post('client-account/invite/{id}','Api\Account\ClientAccountController@invite' );


  Route::post('users/password','Api\UserController@updatePassword');
  // Account users
  Route::resource('account-user', 'Api\Account\AccountUserController');
  Route::post('account-user/status-update/{id}','Api\Account\AccountUserController@statusUpdate');
  Route::post('account-user/invite/{id}','Api\Account\AccountUserController@invite' );

  // invitation
  Route::post('invitation/signup','Api\Account\InvitationController@signup');
  Route::post('invitation/signin','Api\Account\InvitationController@signin');

  //Account - users
  Route::post('account/users','Api\Account\UserController@index');

  Route::get('account/users/activate-new-account', 'Api\Account\UserController@activateNewAccount');

  // Category and rights
  Route::get('user-right-categories','Api\UserRightCategoryController@index' );

  # profile content
  Route::get('/profile-properties', 'Api\ProfileController@index');
  Route::get('/profile-properties-template', 'Component\ConditionBuilderController@template');

  # profile content
  Route::get('component/content-list','Api\Component\ContentListController@index');
  Route::post('component/content-list/update-status','Api\Component\ContentListController@updateStatus');
  Route::post('component/content-list/move-to-experiment','Api\Component\ContentListController@moveVariationsToExperiment');
  Route::post('component/content-list/delete','Api\Component\ContentListController@destroy');

  // Country Controller
  Route::resource('countries', 'Api\CountryController');
  // City Controller
  Route::resource('cities', 'Api\CityController');

  // Guideline Controller
  Route::get('guidelines', 'Api\Guideline\IndexController@index');
  Route::post('guidelines/save', 'Api\Guideline\SaveController@save');
  Route::any('guidelines/import', 'Api\Guideline\SaveController@import');
  Route::any('tooltips/import', 'Api\Tooltip\SaveController@import');

  // Tooltip Controller
  Route::post('tooltips/save', 'Api\Tooltip\SaveController@save');

  // upload ckeditor image
  Route::post('upload-image','Api\ImagesController@upload');
  Route::get('images', 'Api\ImagesController@index');
  Route::delete('images', 'Api\ImagesController@destroy');

  // save page elements
  Route::get('page-element','Api\Page\ElementController@show');
  Route::post('page-element/save','Api\Page\ElementController@store');

  // page elements content list

  Route::resource('page_elements', 'Api\PageElementsController');

  Route::any('page-element/content-list', 'Api\Page\ContentController@index');
  Route::post('page-element/content-show', 'Api\Page\ContentController@show');
  Route::post('page-element/content-create', 'Api\Page\ContentController@store');
  Route::post('page-element/content-update', 'Api\Page\ContentController@update');
  Route::post('page-element/content-update-condition', 'Api\Page\ContentController@updateCondition');
  Route::post('page-element/content-update-experience', 'Api\Page\ContentController@updateExperience');
  Route::post('page-element/content-delete', 'Api\Page\ContentController@destroy');
  Route::get('page-element/testbench','Api\Page\TestBenchController@show');
  Route::post('page-element/testbench-from-condition','Api\Page\TestBenchController@showFromCondition');

  //Reports
  Route::get('reports/experiments/getAll', 'Api\Component\Reports\Api\ExperimentsController@getAllExperiments');
  Route::get('reports/experiments/getExperiment/{id}', 'Api\Component\Reports\Api\ExperimentsController@getExperiment');
  Route::get('reports/experiments/getControlGroups/{id}', 'Api\Component\Reports\Api\ExperimentsController@getControlGroups');
  Route::get('reports/experiments/getTestResponse', 'Api\Component\Reports\Api\ExperimentsController@getTestResponse');
  Route::get('reports/experiments/getCR', 'Api\Component\Reports\Api\ExperimentsController@getCR');
  Route::get('reports/experiments/getCombinedCR', 'Api\Component\Reports\Api\ExperimentsController@getCombinedCR');
  Route::get('reports/experiments/getExperimentOverview', 'Api\Component\Reports\Api\ExperimentsController@getExperimentOverview');


  //Reports for Goals
  Route::get('reports/goals/getAll', 'Api\Component\Reports\Api\GoalsController@getAllGoals');
  Route::get('reports/goals/getGoals/{id}', 'Api\Component\Reports\Api\GoalsController@getGoals');
  Route::get('reports/goals/getGoals/{id}/table', 'Api\Component\Reports\Api\GoalsController@getGoalsDataTable');
  Route::get('reports/goals/getControlGroups/{id}', 'Api\Component\Reports\Api\GoalsController@getControlGroups');
  Route::get('reports/goals/getTestResponse', 'Api\Component\Reports\Api\GoalsController@getTestResponse');
  Route::get('reports/goals/getCR', 'Api\Component\Reports\Api\GoalsController@getCR');
  Route::get('reports/goals/getCombinedCR', 'Api\Component\Reports\Api\GoalsController@getCombinedCR');
  Route::get('reports/goals/getGoalsOverview', 'Api\Component\Reports\Api\GoalsController@getGoalsOverview');

  //Reports for funnels
  Route::get('reports/funnels/getFunnels/{id}', 'Api\Component\Reports\Api\FunnelsController@getFunnels');

  //Reports for segments
  Route::get('reports/segments/get/{id}', 'Api\Component\Reports\Api\SegmentsController@getSegment');


  // redis session store
  Route::post('store-session-state', 'Api\StoreSessionStateController@store');
  Route::post('condition-builder/parse','Api\Component\ConditionBuilderController@parseCondition' );
  Route::resource('profiles', 'Api\ProfilesController');
  Route::get('lead/typo3', 'Api\CrmLeadController@typo3');
  Route::resource('resource', 'Api\ResourceController');


  Route::post('requester', 'Api\RequesterController@index');
});
