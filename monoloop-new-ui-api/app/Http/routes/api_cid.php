<?php

Route::group(['prefix' => '{cid}','middleware' => 'auth.api'], function()
{
  Route::resource('external-objects', 'Account\ExternalObjectController');
});