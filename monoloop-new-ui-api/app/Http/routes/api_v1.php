<?php

Route::group(['prefix' => 'api/v1'], function()
{
  Route::group(['middleware' => 'auth.api'], function(){
    #account_id

    #pages
    Route::get('pages/','Api\V1\PageController@index');
    Route::get('pages/{id}/','Api\V1\PageController@show');
    Route::post('pages/','Api\V1\PageController@store');
    Route::put('pages/{id}/','Api\V1\PageController@update');
    Route::delete('pages/{id}/','Api\V1\PageController@destroy');
    #page elements
    Route::get('pages/{id}/elements/','Api\V1\Page\ElementController@index');
    Route::get('pages/{id}/elements/{eid}','Api\V1\Page\ElementController@show');
    Route::post('pages/{id}/elements/','Api\V1\Page\ElementController@store');
    Route::put('pages/{id}/elements/{eid}/','Api\V1\Page\ElementController@update');
    Route::delete('pages/{id}/elements/{eid}/','Api\V1\Page\ElementController@destroy');
    #elements
    Route::get('elements/','Api\V1\ContentController@index');
    Route::post('elements/images','Api\V1\ContentController@imageUpload');
    Route::get('elements/{id}/','Api\V1\ContentController@show');
    Route::post('elements/','Api\V1\ContentController@store');
    Route::put('elements/{id}/','Api\V1\ContentController@update');
    Route::delete('elements/{id}/','Api\V1\ContentController@destroy');
    #image
    Route::get('images', 'Api\ImagesController@index');
    Route::delete('images', 'Api\ImagesController@destroy');
    Route::post('images','Api\V1\ContentController@imageUpload');
    #template type
    Route::get('tmplTypes/','Api\V1\ContentTemplateController@index');
    Route::get('tmplTypes/{id}/','Api\V1\ContentTemplateController@show');
    #condition property
    Route::get('properties/','Api\V1\ConditionPropertyController@index');
    Route::get('/properties/{id}/','Api\V1\ConditionPropertyController@show');

    ###profile
    Route::get('profile','Api\UserController@profile');
    Route::post('profile','Api\UserController@profileUpdate');
    ###Account
    Route::get('account','Api\AccountController@profile');
    Route::post('account/controlgroup','Api\AccountController@controllgroupUpdate');
    Route::post('account/invocation','Api\AccountController@invocationUpdate');
    Route::post('account/invocation/prepost','Api\AccountController@prepostUpdate');
    Route::post('account/scores/update','Api\AccountController@scoresUpdate');
    ###plugins
    Route::get('plugins','Api\Account\PluginController@index');
    Route::post('plugins/{id}','Api\Account\PluginController@update');
    ###domains
    Route::resource('domains', 'Api\Account\DomainController');
    Route::get('domains/test-domain/{id}','Api\Account\DomainController@testDomain');
    ###users
    Route::resource('users', 'Api\Account\AccountUserController');
    Route::post('users/{id}/status-update/','Api\Account\AccountUserController@statusUpdate');
    Route::post('users/{id}/invite','Api\Account\AccountUserController@invite');
    ###segments;
    Route::get('segments', 'Api\SegmentController@index');
    Route::get('segments/show', 'Api\SegmentController@show');
    Route::post('segments/create', 'Api\SegmentController@create');
    Route::post('segments/update', 'Api\SegmentController@update');
    Route::post('segments/update-status', 'Api\SegmentController@updateStatus');
    Route::post('segments/delete', 'Api\SegmentController@delete');
    ###goals
    Route::get('goals', 'Api\GoalController@index');
    Route::get('goals/show', 'Api\GoalController@show');
    Route::post('goals/create', 'Api\GoalController@create');
    Route::post('goals/update', 'Api\GoalController@update');
    Route::post('goals/update-status', 'Api\GoalController@updateStatus');
    Route::post('goals/delete', 'Api\GoalController@delete');
    ###experiment
    Route::get('experiments/show', 'Api\ExperimentController@show');
    Route::post('experiments/create', 'Api\ExperimentController@create');
    Route::post('experiments/update', 'Api\ExperimentController@update');
    Route::post('experiments/update-status', 'Api\ExperimentController@updateStatus');
    Route::post('experiments/delete', 'Api\ExperimentController@delete');
    Route::get('experiments/{type?}', 'Api\ExperimentController@index');
    // Custom Var
    Route::resource('customvars', 'Api\CustomFieldController');

    // save page elements
    Route::get('page-element','Api\Page\ElementController@show');
    Route::post('page-element/save','Api\Page\ElementController@store');

    Route::post('condition-builder/parse','Api\Component\ConditionBuilderController@parseCondition');
  });
});
