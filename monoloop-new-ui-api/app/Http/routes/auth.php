<?php 

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
  # Test function page
  'test' => 'TestController' ,
]);
