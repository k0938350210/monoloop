<?php

Route::group(['prefix' => 'mladmin'], function()
{
  #public access
  Route::controllers([
  	'auth' => 'Admin\Auth\AuthController',
  ]);

  #auth restric;
  Route::group(['middleware' => 'auth.admin'], function(){
    Route::get('/', 'Admin\DashboardController@index');
    Route::resource('content_templates', 'Admin\ContentTemplatesController');
    Route::get('account/usage', 'Admin\AccountController@usage');
    Route::get('account/usage_per_account', 'Admin\AccountController@usage_per_account');

    Route::resource('accounts','Admin\AccountsController');
    Route::resource('users','Admin\UsersController');

    Route::get('accounts/{id}/invoice/create', 'Admin\Account\InvoiceController@create');
    Route::post('accounts/{id}/invoice', 'Admin\Account\InvoiceController@store');

    Route::get('logs', 'Admin\LogsController@index');
  });
});
