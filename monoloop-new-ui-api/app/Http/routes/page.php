<?php

// UI MAP

Route::get('/', 'WelcomeController@index');
Route::get('/home', 'Page\DashboardController@index');
Route::get('/dashboard/funnel', 'Page\DashboardController@funnel');

Route::get('reports/experiment', 'Api\Component\Reports\ExperimentsController@index');
Route::get('reports/experiment/detail/{id}', 'Api\Component\Reports\ExperimentsController@experimentdetail');

Route::get('reports/goals', 'Api\Component\Reports\GoalsController@index');
Route::get('reports/goals/detail/{id}', 'Api\Component\Reports\GoalsController@goalsdetail');

Route::get('reports/segment/detail/{id}', 'Api\Component\Reports\SegmentsController@segmentdetail');

# Page - Account
Route::get('/account/profile', 'Page\AccountController@profile');
Route::get('/account/control-group', 'Page\AccountController@controlgroup');
Route::get('/account/code-and-scripts', 'Page\AccountController@codeandscripts');
Route::get('/account/api-token', 'Page\AccountController@apitoken');
Route::get('/account/plugins', 'Page\AccountController@plugins');
Route::get('/account/users', 'Page\AccountController@users');
Route::get('/account/client-accounts', 'Page\AccountController@clientaccounts');
Route::get('/account/scores', 'Page\AccountController@scores');
Route::get('/account/domains', 'Page\AccountController@domains');
Route::get('/account/applications', 'Page\AccountController@applications');
Route::get('/account/logs', 'Page\AccountController@logs');
# Page Trackers
Route::get('/trackers', 'Page\TrackerController@index');
# Page Content
Route::get('/content/web', 'Page\ContentController@web');
Route::get('/content/web/add', 'Page\ContentController@addWeb');
Route::get('/content/web/edit', 'Page\ContentController@editWeb');
# web content list
Route::get('/content/web-content', 'Page\ContentController@webcontent');

Route::get('/content/blueprints', 'Page\ContentController@bluepronts');
Route::get('/segment', 'Page\ContentController@segment');
Route::get('/segment/add', 'Page\ContentController@addSegment');
Route::get('/segment/edit', 'Page\ContentController@editSegment');
Route::get('/experiment', 'Page\ContentController@experiment');
Route::get('/experiment/add', 'Page\ContentController@addExperiment');
Route::get('/experiment/edit', 'Page\ContentController@editExperiment');
Route::get('/goals', 'Page\ContentController@goals');
Route::get('/goals/add', 'Page\ContentController@addGoals');
Route::get('/goals/edit', 'Page\ContentController@editGoals');

// test
Route::get('welcome','WelcomeController@index' );

# Coonfirmation
Route::get('/please_confirm_your_registration','Page\ConfirmSignupController@index');
Route::get('/confirmation/{id}','Page\ConfirmSignupController@confirm');

# reset
Route::get('/reset/{id}','Page\ResetController@reset');

// user invites
Route::get('invitation/user/{id}/{token}', 'Page\Invitation\UserController@index');
// account invites
Route::get('invitation/account/{id}/{token}', 'Page\Invitation\AccountController@index');
# Proxy
Route::get('/placement-proxy/{id}','PlacementProxyController@show');

Route::get('/centralize_domain','CentralizeDomainController@index');

Route::get('/test/invocation','Page\TestController@invocation');

Route::get('/content/demo/privacy','ContentController@showPrivacy');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
