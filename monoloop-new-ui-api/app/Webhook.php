<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
// use DB ;

class Webhook extends Eloquent {
  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }
  /**
   * The database table used by the model.
   *
   * @var string
   */
    protected $collection = 'webhooks';

    public function account(){
      return $this->belongsTo(\App\Account::class,'account_id');
    }
    public static function webhooks($folder = NULL, $isRoot = false, $cid = NULL){

    	$branch = [];

      if($isRoot){
        $webhooks = self::where('cid', $cid)->where('deleted', '<>', 1)->get();
      }
       else {
        $webhooks = self::where('folder_id', new \MongoId($folder))->where('deleted', '<>', 1)->get();
      }
      // var_dump($webhooks);die;
      foreach ($webhooks as $key => $webhook) {
        $node = new \stdClass();
        $node->id = $webhook->_id;
        // $node->name = $webhook->name;
        // $node->webhookID = $webhook->webhookID;
        $node->cid = $webhook->cid;
        $node->event_type = $webhook->event_type;
        $node->value = $webhook->event_name;
        $node->event_id = $webhook->event_id;
        $node->direction = $webhook->direction;
        $node->callback_URL = $webhook->callback_url;
        $node->payload_type = $webhook->payload_type;
        $node->status = $webhook->status;
        // $node->aws_message_id = $webhook->aws_message_id ;
        // $node->retry_count = $webhook->retry_count;
        $node->timestamp = $webhook->timestamp;
        // $node->deleted = $webhook->deleted;

		    array_push($branch, $node);
    	}
      $res['data'] = $branch;
      $res['parent'] = 'folder__'.$folder;
    	return $res;
    }
}
