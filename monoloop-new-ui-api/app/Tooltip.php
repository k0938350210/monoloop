<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;

class Tooltip extends Eloquent{

  protected $collection = 'tooltips';

  public static function tooltipsByPageId($page_id = NULL){

    $tooltips = [];

    if($page_id){
      $tooltips = Tooltip::where('page_id',$page_id)->where('deleted','<>', 1)->get();
    }

    return $tooltips;
  }
  public static function takeAllToolTips(){
     $tooltips = [];
     $tooltips = Tooltip::where('deleted','<>', 1)->get();
     return $tooltips;
 }

}
