<?php

namespace App\Helpers;

use Auth;
use App\Guideline;
use App\Tooltip;
use App\Services\Redis\RedisService;
use App\Account;
use Redis;

class AllGuidelines {

  const UserGuidelineState = "UserGuidelineState";
	const UserTooltipState = "UserTooltipState";

  static function takeAllGuides(){

    $groups = array();

    $allGuidelines = Guideline::takeAll();

    foreach($allGuidelines as $item){
			$key = $item['page_id'];
			if (!isset($groups[$key])) {
					if($key != "" || $key != null){
						$guideline_key = RedisService::getKey(Auth::user()->_id, self::UserGuidelineState).':'.$key.':default';
						$redisService = new RedisService();

		        $groups[$key] = array(
		            'currentPageGuidelines' => array($item),
								'currentPageStatus' => json_decode($redisService->get($guideline_key))
		        );
					}

	    } else {
	        $groups[$key]['currentPageGuidelines'][] = $item;

	    }
		}

    return $groups;
  }
  static function takeAllTooltips(){

    $tooltipgroup = array();

    $allTooltips = Tooltip::takeAllToolTips();

    foreach($allTooltips as $item){
			$key = $item['page_id'];
			if (!isset($tooltipgroup[$key])) {
					if($key != "" || $key != null){
						$tooltip_key = RedisService::getKey(Auth::user()->_id, self::UserTooltipState).':'.$key;
						$redisService = new RedisService();

		        $tooltipgroup[$key] = array(
		            'currentPageToolTips' => array($item),
								'currentPageTTStatus' => json_decode($redisService->get($tooltip_key))//,
		            //'count' => 1,
		        );
					}

	    } else {
	        $tooltipgroup[$key]['currentPageToolTips'][] = $item;
	        //$groups[$key]['count'] += 1;

	    }
		}

    return $tooltipgroup;
  }
}
