<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;

class Guideline extends Eloquent{

  protected $collection = 'guidelines';

  public static function guidelinesByPageId($page_id = NULL){

    $guidelines = [];

    if($page_id){
      $guidelines = Guideline::where('page_id',$page_id)->where('deleted','<>', 1)->get();
    }

    return $guidelines;
  }
  public static function takeAll(){
      $guidelines = [];
      $guidelines = Guideline::where('deleted','<>', 1)->get();
      return $guidelines;
   }

}
