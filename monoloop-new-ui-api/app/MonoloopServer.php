<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class MonoloopServer extends Eloquent {

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }

  protected $collection = 'MonoloopServers';

  public function scopeActive($query){
    return $query->where(function($query){
      return $query->where('status', 'Active')->orWhere('status', 'active');
    });
  }
}
