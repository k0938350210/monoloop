<?php  namespace App\Observers;

use App\Experiment;
use App\PageElement;
use App\PageElementContent;
use App\Services\Frontend\Invalidator;
use App\Services\Experiment\DefautlExperiencePageElement;
use App\Jobs\FrontendInvalidate;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;

class ExperimentObserver {
  use DispatchesJobs;
  public function saved(Experiment $experiment)
  {
    // invalidate frontend cache
    if ($experiment->deleted) {
      $pageElements = PageElement::where('deleted', '<>', 1)->where('experiment.experimentID',$experiment->experimentID)->get();
      foreach($pageElements as $pageElement){
        $contents = $pageElement->content ;
        if (!empty($contents)) {
          $found_contents = $contents->where('experimentID',(int)$experiment->experimentID);
          if (!empty($found_contents)) {
            foreach($found_contents as $found_content){
              if($experiment->deleted ){
                // content related with deleted exp should be delete
                $found_content->delete();
              }
            }
          }
        }
        if ($pageElement->experiments) {
          foreach ($pageElement->experiments as $found_exp) {
            if ($found_exp->experimentID == $experiment->experimentID) {
              if($experiment->deleted ){
                // experiment related with deleted experiment should be delete
                $found_exp->delete();
              }
            }
          }
        }
      }
    }

    $default_expedrience_pageelement = new DefautlExperiencePageElement();
    $default_expedrience_pageelement->generate($experiment->cid);

    $this->dispatch(new FrontendInvalidate($experiment->cid  . '_pageelements'));
    $this->dispatch(new FrontendInvalidate($experiment->cid  . '_segment'));
  }

}
