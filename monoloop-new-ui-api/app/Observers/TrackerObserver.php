<?php  namespace App\Observers;

use App\Tracker ;
use App\Jobs\GenerateCbr;

use App\Services\Trackers\PostProcessCode ;

use Illuminate\Foundation\Bus\DispatchesJobs ;

class TrackerObserver {
  use DispatchesJobs;

  public function saving(Tracker $tracker){
    if($tracker->id && $tracker->type !== 'auto'){
      //we need rerender code ;
      $postPorcessCode = new PostProcessCode();
      $postPorcessCode->run($tracker);
    }
  }

  public function saved(Tracker $tracker)
  {
    $this->dispatch(new GenerateCbr($tracker->account));
  }

}
