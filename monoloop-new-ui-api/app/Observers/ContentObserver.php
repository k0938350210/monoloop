<?php  namespace App\Observers;

use App\Content ;
use App\PageElement ;
use App\Services\Frontend\Invalidator ;
use App\Jobs\FrontendInvalidate;
use Illuminate\Foundation\Bus\DispatchesJobs ;

class ContentObserver {
  use DispatchesJobs;
  public function saved(Content $content)
  {
    $pageElements =  PageElement::where('content.uid',(int)$content->uid)->get();
    $cid = 0;
    foreach($pageElements as $pageElement){
      $contents = $pageElement->content ;
      $found_contents = $contents->where('uid',(int)$content->uid);
      foreach($found_contents as $found_content){
        if($content->deleted || $content->hidden){
          $found_content->delete();
        }else{
          $found_content->code = $content->code;
          if($content->xpath){
            $found_content->mappedTo = $content->xpath;
          }
          if ($content->placement_type) {
            $found_content->placementType = $content->placement_type;
          }
          $found_content->save();
        }
      }
      $this->dispatch(new FrontendInvalidate($pageElement->cid  . '_pageelements'));
    }
  }

}
