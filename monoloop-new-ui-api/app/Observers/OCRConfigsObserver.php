<?php  namespace App\Observers;

use App\OCRConfigs ;
use App\Services\Frontend\Invalidator ;
use App\Jobs\FrontendInvalidate;
use Illuminate\Foundation\Bus\DispatchesJobs ;

class OCRConfigsObserver {
  use DispatchesJobs;

  public function updated(OCRConfigs $ocrConfigs)
  {
    #invalidate frontend server
    $this->dispatch(new FrontendInvalidate($ocrConfigs->cid  . '_ocr'));
  }

}
