<?php  namespace App\Observers;

use App\Goal ;
use App\PageElement ;
use App\PageElementContent ;
use App\Services\Frontend\Invalidator ;
use App\Jobs\FrontendInvalidate;
use Illuminate\Foundation\Bus\DispatchesJobs ;
use App\Services\PageElement\ContentGenerator ;

class GoalObserver {
  use DispatchesJobs;

  public function creating(Goal $goal){
    $goal->uid = ContentGenerator::newUid() ;
  }

  public function saved(Goal $goal)
  {
    // invalidate frontend cache
    $this->dispatch(new FrontendInvalidate($goal->cid  . '_pageelements'));

    if($goal->page_element_id == null){
      return ;
    }

    $pageElements =  PageElement::where('content.uid',(int)$goal->uid)->get();
    foreach($pageElements as $pageElement){
      $contents = $pageElement->content;
      if(empty($pageElement->experiment)){
        $found_contents = $contents->where('uid',(int)$goal->uid);
        foreach($found_contents as $found_content){
          $found_content->delete() ;
        }
      }
    }

    if($goal->deleted || $goal->hidden){
      return;
    }

    $pageElement = PageElement::find($goal->page_element_id);
    if($pageElement == null){
      return;
    }


    $content = new PageElementContent();
    $content->goal = true;
    $content->p = (int)$goal->point;
    $content->code = $goal->code;
    $content->uid = (int)$goal->uid;

    $pageElement->content()->save($content);
  }

}
