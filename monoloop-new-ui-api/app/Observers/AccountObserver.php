<?php  namespace App\Observers;

use App\Account ;
use Illuminate\Foundation\Bus\DispatchesJobs ;
use Auth ;

class AccountObserver {
  use DispatchesJobs;

  public function creating(Account $account){
    $account->timezone = 'UTC';
    $account->date_format = 'D MMMM,YYYY';
    $account->time_format = 'HH:mm:ss' ;
  }
}
