<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Blueprint extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $collection = 'blueprints';

    /*
     * The experiments associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public function getFolderItems($folder = NULL, $isRoot = false, $account_id = NULL){

    	$branch = [];

      if($isRoot){
          $segments = Blueprint::where('folder_id', new \MongoId($folder))
                              ->orwhere(function ($query) use ($folder, $account_id) {
                                          $query->where('account_id', $account_id);
                            } )->get();
      } else {
          $segments = Blueprint::where('folder_id', new \MongoId($folder))->get();
      }


        foreach ($segments as $key => $segment) {
            $node = new \stdClass();
            $node->id = "segment__".$segment->_id;
            $node->value = $segment->name;
            $node->type = "file";
            $node->description = $segment->description;

            $node->date = date('j F, Y', strtotime($segment->created_at));
            $node->hidden = 0;
            $node->condition = "";
    		    array_push($branch, $node);
    	}

    	return $branch;
    }
}
