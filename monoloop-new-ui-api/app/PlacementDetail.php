<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;

class PlacementDetail extends Eloquent{
  /*--- Scope ---*/
  /* Not work recheck ++
  public function scopeByUid($query,$uid){
    return $query->where('uid','=',(int)$uid) ;
  }
  */
  /*--- relations ---*/
  public function content()
  {
    return $this->belongsTo('\App\Content','content_id');
  }
}
