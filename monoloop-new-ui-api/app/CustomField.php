<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use MongoId ;

class CustomField extends Eloquent{
  protected $collection = 'custom_fields';

  /*--- Scope ---*/

  public function scopeByAccount($query , $account_id){
    return $query->where('account_id', '=', new MongoId($account_id));
  }

  public function scopeUid($query , $uid){
    return $query->where('uid',(int)$uid) ;
  }

  /*--- attribute ---*/
  public function getDatatypeIdAttribute(){
    $type = array('integer' => 1 , 'string' => 2 , 'bool' => 3 , 'date' => 4) ;
    return $type[$this->datatype] ;
  }

  public function scopeAvailable($query){
    return $query->where(function ($query){
      $query->where('deleted', 'exists', false)->orWhere('deleted' , 0) ;
    });
  }
}
