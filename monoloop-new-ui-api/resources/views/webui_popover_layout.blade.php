<!doctype html>
<html ng-app='APP'>

<head>
	<meta name="description" content="condition builder" />

	<!-- <title>Placement Window</title> -->



	<!-- <link href="/conditionbuilder/css/bootstrap.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="/webix/webix.css?{{Config::get('app.version') }}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{ elixir('css/webix.css') }}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{ elixir('webui_popover/jquery.webui-popover.css') }}" media="all" />
	<!-- Fonts -->

</head>

<body>
  @yield('content')

	<script src="{{ elixir('webix/webix.js') }}" type="text/javascript"></script>
 	<!-- Integrate jquery with webix -->
 	<script src="{{ elixir('js/jquery.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var MONOloop  = {};
		MONOloop.jq = $;
		var monolop_base_url = '{{config('app.url')}}' ;
		var monolop_api_base_url = '{{config('app.monoloop_api_base_url')}}' ;
		var ajaxHeaders = {'Content-Type': 'application/x-www-form-urlencoded', 'ML-Token': "{{ Session::get('mlToken') }}" };
		webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers){
			headers["Content-type"]= ajaxHeaders['Content-Type'];
			headers["ML-Token"]= ajaxHeaders['ML-Token'];
		});
	</script>
	<script src="{{ elixir('webui_popover/jquery.webui-popover.js') }}" type="text/javascript"></script>
	<script src="{{ elixir('js/guidelines.js') }}" type="text/javascript"></script>
  @yield('footerjs')
</body>

</html>
