<!doctype html>
<html ng-app='APP'>

<head>
	<meta name="description" content="condition builder" />

	<!-- <title>Placement Window</title> -->

	<!-- <meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'> -->

	<script src="{{ elixir('webix/webix.js') }}" type="text/javascript"></script>
	<!-- Integrate jquery with webix -->
	<script src="{{ elixir('js/jquery.js') }}" type="text/javascript"></script>

	<!-- Integrate CKEditor -->
	<script src="{{ elixir('ckeditor/ckeditor.js') }}" type="text/javascript"></script>

	<script src="/js/clipboard.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		var MONOloop  = {};
		MONOloop.jq = $;
		var monolop_base_url = '{{config('app.url')}}' ;
		var monolop_api_base_url = '{{config('app.monoloop_api_base_url')}}' ;
		var s3_public_url = '{{config('filesystems.disks.s3.public_url')}}' ;
		var activeAccount = {!! json_encode(UserAccount::activeAccount()) !!};

		var ajaxHeaders = {'Content-Type': 'application/x-www-form-urlencoded', 'ML-Token': "{{ Session::get('mlToken') }}" };
		webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers){
			headers["Content-type"]= ajaxHeaders['Content-Type'];
			headers["ML-Token"]= ajaxHeaders['ML-Token'];
		});
		var page = '{{ $page }}';
		var url = '{{ $url }}';
		var source = '{{ $source }}';
		var new_url = '{{ $new_url }}';
	</script>
	<!-- <link href="/conditionbuilder/css/bootstrap.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="/webix/webix.css?{{Config::get('app.version') }}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{ elixir('css/webix.css') }}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{ elixir('webui_popover/jquery.webui-popover.css') }}" media="all" />
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&{{Config::get('app.version') }}' rel='stylesheet' type='text/css'>
	<!-- <script src="/js/clipboard/clipboard.min.js" type="text/javascript"></script> -->
</head>

<body class="pwBody">
	<!-- <div class="container" style="width: inherit;"> -->
	<!-- <div id="test" class="" >sadadassadasdasdsad</div> -->
	<div id="containerId" class="container" style="height:100% !important;width: 100% !important;">
		<div class="row">
			<div id="placement_window" style="height:100% !important;width: 100% !important;"></div>
		</div>
	</div>
	<!-- </div> -->
<script src="{{ elixir('webui_popover/jquery.webui-popover.js') }}" type="text/javascript"></script>
<script src="{{ elixir('js/guidelines.js') }}" type="text/javascript"></script>
<script src="{{ elixir('js/webix/component/placement_window.js') }}" type="text/javascript"></script>
<script>
	// $(document).ready(function (e){
	// 	$("#test").html('<div class="test" id="div1" >we qwh jeh w qjehqwje hwqe hjqwj ekwewe q eqwe</div>');
	// 	$("#div1").attr('contenteditable', true);
	// });
</script>
</body>

</html>
