@extends('webui_popover_layout')

@section('content')
	<div id="goal_container"></div>
@endsection

@section('footerjs')
<script type="text/javascript">
	var monolop_base_url = '{{config('app.url')}}' ;
	var urlParams = {};
	urlParams.fullURL = '{{ $fullURL }}';
	urlParams.current_folder = '{{ $current_folder }}';
	urlParams.page_element_id = '{{ $page_element_id }}';
	// console.log("urlParams", urlParams);
	var contentListResponse,
			iFrameWin,
			isConditionsGeneratedSegment = false,
			tempConditionSegment = undefined,
			conditionBuilderUrlSegment = '/component/profile-properties-template',
			urlOptions = [
				{id: 'exactMatch', value: 'Exact Match'},
				{id: 'allowParams', value: 'Allow Parameters'},
				{id: 'startsWith', value: 'Starts With'},
			],
			getUrlOption = function(id){
					id = id || 0;
					id = parseInt(id);
					switch (id) {
						case 0: return "exactMatch"; break;
						case 1: return "allowParams"; break;
						case 2: return "startsWith"; break;
						default: return "exactMatch"; break;
					}
			},
			pageOptions = {
				placement_urlGoal: urlParams.fullURL,
				placement_inc_wwwGoal: 1,
				placement_inc_http_httpsGoal: 0,
				placement_url_optionGoal: 0,
				placement_reg_exGoal: '',
				placement_remarkGoal: '',
			},
			goalTypes = [{
		    id: "page_related",
		    value: "Page related"
		  },{
		    id: "page_related_with_condition",
		    value: "Page related with condition"
		  }, {
		    id: "condition_related",
		    value: "Condition related"
		  }],
			currentStepIndex = 0,
			currentGoalType = goalTypes[0].id;

	if(urlParams.page_element_id.trim().length > 0){
		pageOptions.placement_urlGoal = '{{ $fullURLWebPageList }}';
		pageOptions.placement_inc_wwwGoal = '{{ $includeWWWWebPageElement }}';
		pageOptions.placement_inc_http_httpsGoal = '{{ $includeHttpHttpsWebPageElement }}';
		pageOptions.placement_url_optionGoal = '{{ $urlOptionWebPageElement }}';
		pageOptions.placement_reg_exGoal = '{{ $regularExpressionWebPageElement }}';
		pageOptions.placement_remarkGoal = '{{ $remarkWebPageList }}';
	}
	function showView(t){
		t = t || 'newGoal';

		switch (t) {
			case 'newGoal':
				$("#goal_container").show();
				showNewSegment();

				// init();
				break;
			default:
				break;

		}
	}

	function showNewSegment(){
		$("#goal_container").html('');
		isConditionsGeneratedSegment = false;
		tempConditionSegment = undefined;


		function submit(){
			var form = $$("createNewGoalFormID");
			if(form.validate()){
				var formValues = $$("createNewGoalFormID").getValues();
				formValues.source = urlParams.current_folder;
				webix.ajax().post(monolop_api_base_url+"/api/goals/create", formValues, {
		      error: function(text, data, XmlHttpRequest) {
		        alert("error");
		      },
		      success: function(text, data, XmlHttpRequest) {
		        var response = JSON.parse(text);
		        if (response.success === true) {
							webix.message('Segment created!');
							var goal = response.content.goal;

							parent.postMessage({t: "page-level-goal-created", goal_id: goal._id, created: true}, "*");
						} else {
							webix.alert('An error occoured, please refresh and try again.');
						}
		      }
		    });
			}
		}

		function back(){
		  $$("createNewGoalMultiview").back();
		}

		function next(){
		  var parentCell = this.getParentView().getParentView(); //the way you get parent cell object may differ
		    var index = $$("createNewGoalMultiview").index(parentCell);
				// if(index === 0){
				// 	if(currentGoalType === 'condition_related'){
				// 		var next = $$("createNewGoalMultiview").getChildViews()[index+2];
				// 	} else {
				// 		var next = $$("createNewGoalMultiview").getChildViews()[index+1];
				// 	}
				// } else {
				// 	var next = $$("createNewGoalMultiview").getChildViews()[index+1];
				// }
				var next = $$("createNewGoalMultiview").getChildViews()[index+1];
		    if(next){
						var form = $$("createNewGoalFormID");
						if(form.validate()){
		        	next.show();

						}
				}
		}

		webix.ui({
				container: 'goal_container',
				id: 'goal_container',
				scroll:false,
				css: 'goal_container',
				width: 340,
				// height: 500,
				rows: [
					{
						view:"form",
					  id:"createNewGoalFormID",
						elementsConfig: {
	            labelPosition: "top",
							// labelWidth: 100,
							align:"center",
							bottomPadding: 5
	          },
					  elements:[
					    {view:"multiview", id:"createNewGoalMultiview", cells:[
					      {rows:[
									{
				              view: "text",
				              id: "nameGoal",
				              // label: 'Name',
				              name: "nameGoal",
											placeholder: "Name",
											label: "Name",
											bottomPadding: 15,
				              // readonly: true,
				                // invalidMessage: "Name can not be empty"
				            },{
								      view: "richselect",
								      id: "typeGoal",
								      name: "typeGoal",
											label: "Goal type",
								      value: currentGoalType,
								      options: goalTypes,
											bottomPadding: 15,
								    },{
					              view: "text",
					              id: "pointsGoal",
												name: "pointsGoal",
												placeholder: "Points",
												bottomPadding: 15,
												label: "Points",
												value: "0",
					              // readonly: true,
					                // invalidMessage: "Name can not be empty"
					            },
											{
									      view: "text",
									      id: "conditionGoal",
									      name: "conditionGoal",
												value: iFrameWin ? iFrameWin.returnCondition.logical  : "",
									      hidden: true,
									    },
											{
												id: "conditionGoalLayout",
												visible: false,
												cols: [
													{
								              view: "text",
								              id: "conditionLabel",
								              name: "conditionLabel",
															css: "rightIconTextbox var",
															placeholder: "Condition",
															label: "Condition",
															value: "All visitors.",
								              readonly: true,
															bottomPadding: 15,
															click: function(){
																parent.postMessage({
																	t: 'open-condition-builder',
																	condition: $$("conditionGoal").getValue(),
																	type: 'goal'
																}, urlParams.fullURL);
															}
								                // invalidMessage: "Name can not be empty"
								            },{
															view: "icon",
															icon: "users",
															css: "conditionIcon var",
															click: function(){
																console.log("hello");
																parent.postMessage({
																	t: 'open-condition-builder',
																	condition: $$("conditionGoal").getValue(),
																	type: 'goal'
																}, urlParams.fullURL);
															}
														}
												],
											}, {

										}, {

										}, {

										}, {

										},
					        {cols:[
					          {view: "button",value: "Reset",type: "danger",click: function(){
											$$("typeGoal").setValue("page_related")
											showView('newGoal');
										}},
					          {view:"button", id: 'basicStepNextbtn', value:"Next", css: "orangeBtn", click:next},
										{view:"button", id: 'basicStepAddBtn', hidden: true, value:"Add", click:submit},

					        ]}

					      ]},
								{rows:[
									{
										view: "text",
										id: "placement_urlGoal",
										placeholder: 'URL',
										label: "URL",
										name: "placement_urlGoal",
										readonly: true,
										value: pageOptions.placement_urlGoal,
										// invalidMessage: "Name can not be empty"
									}, {
										cols: [
											{
												view:"checkbox",
												id:"placement_inc_wwwGoal",
												name:"placement_inc_wwwGoal",
												label:"Include WWW",
												labelWidth: 150,
												// readonly: true,
												value: pageOptions.placement_inc_wwwGoal
											}, {
												view:"checkbox",
												id:"placement_inc_http_httpsGoal",
												name:"placement_inc_http_httpsGoal",
												label:"Include Http / Https",
												labelWidth: 150,
												align:"center",
												// readonly: true,
												value: pageOptions.placement_inc_http_httpsGoal
											}
										]
									}, {
										view: "richselect",
										// label: "URL Options",
										id: "placement_url_optionGoal",
										name: "placement_url_optionGoal",
										value: getUrlOption(pageOptions.placement_url_optionGoal),
										options: urlOptions,
										labelAlign: 'left',
									},{
										view: "text",
										id: "placement_reg_exGoal",
										placeholder: 'Regular Expression',
										label: "Regular Expression",
										width: 500,
										align:"center",
										name: "placement_reg_exGoal",
										// readonly: true,
										value: pageOptions.placement_reg_exGoal
										// invalidMessage: "Name can not be empty"
									}, {
										view: "text",
										id: "placement_remarkGoal",
										label: "Remark",
										placeholder: 'Remark',
										width: 500,
										align:"center",
										name: "placement_remarkGoal",
										// readonly: true,
										value: pageOptions.placement_remarkGoal
										// invalidMessage: "Description can not be empty"
									}, {

									}, {

									}, {

									},
									{cols:[
										{view: "button",value: "Reset",type: "danger",click: function(){showView('newGoal');}},
										{view:"button", value:"Back", click:back },
										{view:"button", value:"Finish", css: "orangeBtn", click:submit},
									]}

								]}
					    ]
					    }
					  ],
						rules: {
		          "nameGoal": webix.rules.isNotEmpty,
							"pointsGoal": webix.rules.isNumber,
		        }
					}
				]
		});
		// hide condition layout div in default case => page-related
		$$("conditionGoalLayout").hide();

		$$("typeGoal").attachEvent("onChange", function(newv, oldv) {
			currentGoalType = newv;
			if(newv === 'condition_related'){
				$$("basicStepNextbtn").hide();
				$$("basicStepAddBtn").show();
			} else {
				$$("basicStepNextbtn").show();
				$$("basicStepAddBtn").hide();
			}

			if(newv.indexOf("condition") > -1){
				$$("conditionGoalLayout").show();
			}else{
				$$("conditionGoal").hide();
				$$("conditionGoal").setValue("");
				$$("conditionGoalLayout").hide();
			}
	  });

	}



	function init (){
		$("#goal_container").html('');

		showNewSegment();

	}

	init();
	// listen from the child
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];

	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	// Listen to message from child IFrame window
	eventer(messageEvent, function (e) {
				 var data = JSON.parse(e.data);
				 switch (data.t) {
					case "set-condition":
						if(data.data.condition){
							data.data.condition = data.data.condition.trim();
						}
						if(data.data.condition && data.data.condition !== 'if (){|}'){
							$$("conditionGoal").setValue(data.data.condition);
							$$("conditionLabel").setValue("Condition is available.");
						} else {
							$$("conditionGoal").setValue("");
							$$("conditionLabel").setValue("All visitors.");
						}
						break;
					default:
						break;
				 }
				 // Do whatever you want to do with the data got from IFrame in Parent form.
	}, false);
	// parent.IframeImageUploaderWin = window;

</script>
@endsection
