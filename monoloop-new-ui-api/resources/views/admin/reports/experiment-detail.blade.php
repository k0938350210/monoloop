@extends('webix')
@section('footerjs')

<link href="https://cdn.rawgit.com/csaladenes/ddc0b326b93ec641c84f/raw/nv.d3.css?{{Config::get('app.version') }}" rel="stylesheet"/>
<script src="/webix/d3/d3.v3.min.js?{{Config::get('app.version') }}"></script>
<script src="/js/jquery.tipsy.js?{{Config::get('app.version') }}"></script>


			 <!-- Include our javascript file. -->
			 <script type="text/javascript" src="/DateRangesWidget/js/datepicker.js"></script>
	 <script type="text/javascript" src="/DateRangesWidget/js/DateRange.js"></script>
	 <script type="text/javascript" src="/DateRangesWidget/js/DateRangesWidget.js"></script>

			 <!-- STYLE SHEETS -->
			 <link rel="stylesheet" href="/DateRangesWidget/css/datepicker/base.css" />
	 <!-- <link rel="stylesheet" href="../css/datepicker/clean.css" /> -->
	 <link rel="stylesheet" href="/DateRangesWidget/css/DateRangesWidget/base.css" />
	 <script type="text/javascript" src="/datepicker/js/datepicker.js?{{Config::get('app.version') }}"></script>
	 <link rel="stylesheet" type="text/css" href="/datepicker/css/base.css?{{Config::get('app.version') }}" />
	 <link rel="stylesheet" type="text/css" href="/datepicker/css/clean.css?{{Config::get('app.version') }}" />
<style>

.listView svg {
	width: 100%;
	height: 100%;

}

.listView path.slice{
	stroke-width:2px;
}

.listView polyline{
	opacity: .3;
	stroke: black;
	stroke-width: 2px;
	fill: none;
}

.listView .labelValue
{
	font-size: 60%;
	opacity: .5;

}

.listView .tooltip{
position: absolute;
  text-align: center;
  width: 100px;
  height: 28px;
  padding: 2px;
  font: 12px sans-serif;
  background: green;
  border: 0px;
  border-radius: 8px;
  color:white;
  box-shadow: -3px 3px 15px #888888;
  opacity:0;

}

.listView .axis path,
.listView .axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.listView .x.axis path {
  display: none;
}

.listView .line {
  fill: none;
  stroke: steelblue;
  stroke-width: 1.5px;
}

.exp-runtime.number {
	font-size: 15px;
	font-weight: bolder;
}

</style>
<script type="text/javascript">
webix.ready(function() {
  var options = {};
  options.link = config.dashboard.experimentdetail.link;
  options.headerName = config.dashboard.experimentdetail.headerName;
  options.script = config.dashboard.experimentdetail.script;
	options.pageId = config.dashboard.experimentdetail.pageId;
  generatePageContent(options);
})
</script>
@endsection
