@extends('layouts.admin.main')
@section('content')
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Accounts</li>
    </ol>
</div>

<div class="innerLR">
  <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Accounts Management</h2>
        </div>
        <div class="col-lg-5">
            {!! Form::open(array( 'method' => 'get')) !!}
                 <input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" />
            {!! Form::close() !!}
        </div>
    </div>
  <div class="widget">
    {!! Notification::showAll() !!}
    <table class="table dataTable">
      <thead>
        <tr>
          <th class="@if($order_by == 'uid') @if($dir == 'asc') sorting_asc  @else sorting_desc @endif @else sorting @endif" data-id="uid">#</th>
          <th class="@if($order_by == 'contact_name') @if($dir == 'asc') sorting_asc  @else sorting_desc @endif @else sorting @endif" data-id="contact_name">Name</th>
          <th class="@if($order_by == 'AccType') @if($dir == 'asc') sorting_asc  @else sorting_desc @endif @else sorting @endif" data-id="AccType">Payment Status</th>
          <th class="@if($order_by == 'account_type') @if($dir == 'asc') sorting_asc  @else sorting_desc @endif @else sorting @endif" data-id="account_type">Type</th>
          <th class="@if($order_by == 'billing_pageviews') @if($dir == 'asc') sorting_asc  @else sorting_desc @endif @else sorting @endif" data-id="billing_pageviews">Usage/Limit</th>
          <th class="@if($order_by == 'last_billing_date') @if($dir == 'asc') sorting_asc  @else sorting_desc @endif @else sorting @endif" data-id="last_billing_date">Last Billing Date</th>
          <th class="@if($order_by == 'cms_site_url') @if($dir == 'asc') sorting_asc  @else sorting_desc @endif @else sorting @endif" data-id="cms_site_url">CMS site url</th>
          <th class="@if($order_by == 'created_at') @if($dir == 'asc') sorting_asc  @else sorting_desc @endif @else sorting @endif" data-id="created_at">Created</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach($accounts as $k => $account)
        <tr>
          <td>{{ $account->uid }}</td>
          <td>
            {{ $account->contact_name }}
          </td>
          <td><span class="label label-default">{{ $account->AccType }}</span></td>
          <td><span class="label label-default">
            @if($account->account_type)
              {{ ucfirst($account->account_type) }}
            @else
              free
            @endif
          </span></td>
          <td>{{$account->billing_pageviews}} / {{$account->max_pageviews}}</td>
          <td>{{$account->last_billing_date}}</td>
          <td><a href="{{$account->cms_site_url}}">{{$account->cms_site_url}}</a></td>
          <td>{{ $account->created_at }}</td>
          <td>
            <a href="/mladmin/accounts/{{$account->_id}}" class="btn btn-primary"><i class="fa fa-file-text-o"></i>
</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="text-center">
  		<?php echo $accounts->appends(array('q' => $q))->render(); ?>
  	</div>
  </div>
</div>


@stop


@section('footerjs')
<script type="text/javascript">
  $( document ).ready(function(){
    $('table.dataTable thead th').click(function(){
      if($(this).hasClass('sorting') || $(this).hasClass('sorting_desc') || $(this).hasClass('sorting_asc') ){
        var order_by = $(this).data('id');
        var dir = 'asc' ;
        if($(this).hasClass('sorting_asc')){
          dir = 'desc' ;
        }
        document.location.href = '/mladmin/accounts?order_by=' + order_by + '&dir=' + dir ;
      }
    });
  });

</script>
@stop