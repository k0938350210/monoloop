<div class="innerLR row">
  <div class="col-lg-12">
    <h4>Page Views</h4>
  </div>
  {!! Form::open(array( 'method' => 'get')) !!}
  <div class="col-lg-10">
    <div class="input-daterange input-group" id="datepicker">
      <input type="text" class="input-sm form-control" name="report_start" value="{{ $report_start }}" />
      <span class="input-group-addon">to</span>
      <input type="text" class="input-sm form-control" name="report_end" value="{{ $report_end }}" />
    </div>
  </div>
  <div class="col-lg-2">
    <input type="submit" class="btn btn-primary" value="Search" />
  </div>
  {!! Form::close() !!}
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <td>Date</td>
      <td>Page Views</td>
    </tr>
  </thead>
  <tbody>
    @foreach($reports as $report)
    <tr>
    <td>{{ date('m/d/Y H:i',$report->datestamp) }}</td>
    <td>{{ $report->totalPages}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
