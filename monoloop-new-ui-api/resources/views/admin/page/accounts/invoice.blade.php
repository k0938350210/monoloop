<div class="innerLR row">
  <h4>Billings</h4>
  <table class="table table-striped">
    <thead>
      <tr>
        <td>Date</td>
        <td>Page Views</td>
        <td>Status</td>
      </tr>
    </thead>
    <tbody>
    @foreach($billings as $billing)
    <tr>
    <td>{{ $billing->billing_date->format('m/d/Y') }}</td>
    <td>{{ $billing->billing_pageviews }}</td>
    <td>{{ $billing->payment_type }}</td>
    </tr>
    @endforeach
    </tbody>
  </table>
</div>
