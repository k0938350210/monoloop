@extends('layouts.admin.login')

@section('content')
<div class="wrapper">
  <div class="container">
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="panel">
          <div class="panel-body">
            <h2>Please sign in</h2>
            {!! Notification::showAll() !!}
            <form role="form" action="{{ url('mladmin/auth/login') }}" method="POST">
{{ url('mladmin/auth/login') }}
              <div class="form-group">
                <label for="email">Username</label>
  							<input type="input" class="form-control" id="username" name="username" placeholder="Username" value="{{ old('username') }}"/>
  						</div>
              <div class="form-group">
						    <label for="passowrd1">Password</label>
						    <input type="password" class="form-control" id="passowrd1" name="password" placeholder="Password" />
  						</div>
              <button type="submit" class="btn btn-primary">Sign in</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
