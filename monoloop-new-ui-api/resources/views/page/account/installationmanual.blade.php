
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.installationManual.link;
	options.headerName = config.installationManual.headerName;
	options.script = config.installationManual.script;
	options.pageId = config.installationManual.pageId;
	generatePageContent(options);
});

</script>
@endsection
