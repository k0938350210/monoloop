
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.addcontentWeb.link;
	options.headerName = config.addcontentWeb.headerName;
	options.script = config.addcontentWeb.script;
	options.pageId = config.addcontentWeb.pageId;
	generatePageContent(options);
	window.placement_url = '{{ $url }}' ;
});

</script>
@endsection
