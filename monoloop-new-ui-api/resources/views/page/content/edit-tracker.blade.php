
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.tracker_id = '{{ $tracker_id }}';
	options.link = config.editTracker.link;
	options.headerName = config.editTracker.headerName;
	options.script = config.editTracker.script;
	options.pageId = config.editTracker.pageId;
	generatePageContent(options);
});

</script>
@endsection
