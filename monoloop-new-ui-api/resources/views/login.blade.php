<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Monoloop</title>

	<link href="{{ elixir('css/monoloop.css') }}" rel="stylesheet"> 									
	<script type="text/javascript" src="{{ elixir('admin/js/core.js') }}"></script>
	<script type="text/javascript" src="{{ elixir('js/jquery.backstretch.min.js') }}"></script>
	<script type="text/javascript" src="{{ elixir('js/validaion.js') }}"></script>
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&{{Config::get('app.version') }}' rel='stylesheet' type='text/css'>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js?{{Config::get('app.version') }}"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js?{{Config::get('app.version') }}"></script>
	<![endif]-->
	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/58105ef7f434cc6ca4628b04/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
	</script>
	<!--End of Tawk.to Script-->
	<!-- Start Of Google Analytics Script -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25035461-3', 'auto');
  ga('send', 'pageview');
	</script>
	<!-- End Of Google Analytics Script -->
</head>
<body class="login">
	@yield('content')
</body>
</html>
