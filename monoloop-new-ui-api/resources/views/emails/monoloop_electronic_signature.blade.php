@extends('layouts.email')

@section('subject', '[monoloop.com] Reset password your Monoloop account')

@section('content')

<p>Hi {{$account->contact_name}}</p>
<p>This email is to confirm that you have now electronically signed the following agreements:</p>

<ul>
  <li><a href="https://www.monoloop.com/terms-of-service/">Monoloops Terms of Service</a></li>
  <li><a href="https://www.monoloop.com/acceptable-use-policy/">Acceptable Use Policy</a></li>
  <li><a href="https://www.monoloop.com/data-processing-agreement/">Data Processing Agreement</a></li>
</ul>

<p>
By your signature you also confirm that you are authorized to represent {{$account->legal_entity}} and enter into legally binding agreements on behalf of {{$account->legal_entity}}.
</p>
<br/>
<p>
Your signature have been recorded when logged in with these properties<br/>
{{$account->updated_at}}<br/>
{{$user->username}}<br/>
{{$ip}}
</p>
<br/>
<p>
If this signature is not signed by you - please reach out to us immediately on our support chat in either the Monoloop user interface or on our website.
</p>
<br/>
<p>
Best regards,<br />
Monoloop<br />
</p>

@endsection
