@extends('layouts.email')

@section('subject', 'Invitation from monoloop')

@section('content')
<p>Hi {{$user->name}},</p>
</br>
<p>This is an invitation to create your own user account on Monoloop. The invitation is initiated by {{$user->account->contact_name}} so you are able to work together on the same account.</p>
</br>
<p>Please click link to accept invitation</p>


<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
  <tbody><tr>
    <td style="width:120px;background:#008000;">
      <div>
      <!--[if mso]>
        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:40px;v-text-anchor:middle;width:200px;" stroke="f" fillcolor="#008000">
          <w:anchorlock/>
          <center>
        <![endif]-->
            <a style="background-color:#008000;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:14px;line-height:25px;text-align:center;text-decoration:none;width:120px;-webkit-text-size-adjust:none;" href="{{$user->invitationURL}}">Accept Invitation</a>
        <!--[if mso]>
          </center>
        </v:rect>
      <![endif]-->
      </div>
    </td>
    <td width="440" style="background-color:#ffffff; font-size:0; line-height:0;">&nbsp;</td>
  </tr>
</tbody></table>

<p>Do you have questions regarding this invite you can email {{$user->account->contact_name}} on {{$user->account->contact_email}}.</p>
</br>
<p>
Best regards,<br />
Monoloop<br />
</p>

@endsection