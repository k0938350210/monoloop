@extends('layouts.email')

@section('subject', '[monoloop.com] We found your E-Mail on multiple accounts')

@section('content')

<p>Your username is {{$users}}</p>

Best regards,<br />
Monoloop

@endsection
