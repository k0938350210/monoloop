@extends('layouts.email')

@section('subject', 'Thanks for your signup at www.monoloop.com')

@section('content')
<p>Hi {{$account_user->name}},</p>
<p style="margin-top:10px;">Thank you for your interest in Monoloop!</p>
<p style="margin-top:10px;">We're happy to see you here and hope we can provide added value to your online initiatives. </p>
<p style="margin-top:10px;margin-bottom:15px;">To get started please click on the link below to activate your account.</p>
<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
  <tbody><tr>
    <td style="width:120px;background:#008000;">
      <div>
      <!--[if mso]>
        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:40px;v-text-anchor:middle;width:200px;" stroke="f" fillcolor="#008000">
          <w:anchorlock/>
          <center>
        <![endif]-->
            <a style="background-color:#008000;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:14px;line-height:25px;text-align:center;text-decoration:none;width:120px;-webkit-text-size-adjust:none;" href="{{$user->confirmationURL}}">Activate Account</a>
        <!--[if mso]>
          </center>
        </v:rect>
      <![endif]-->
      </div>
    </td>
    <td width="440" style="background-color:#ffffff; font-size:0; line-height:0;">&nbsp;</td>
  </tr>
</tbody></table>

<p style="margin-top:10px;">
Best regards,<br />Monoloop<br />
</p>

@endsection
