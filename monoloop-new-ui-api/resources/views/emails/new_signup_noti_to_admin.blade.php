@extends('layouts.email')

@section('subject', 'Notification from Monoloop [New Sign up]')

@section('content')
<p><b>New Signup notification</b></p>
<p style="margin-top:3px;"><b>Organization details:</b></p>
<p style="margin-top:3px;">Email: {{$account->contact_email}}</p>
<p style="margin-top:3px;">Account type: {{$account->account_type}}</p>

<br />
<p style="margin-top:3px;"><b>User details:</b></p>
<p style="margin-top:3px;">Email: {{$account->contact_name}}</p>
<p style="margin-top:3px;">IP Address: {{ $_SERVER['REMOTE_ADDR'] }}</p>

<p style="margin-top:3px;">Source: {{ $source }}</p>

<br />
Best regards,<br />
Monoloop<br />
<a href="http://www.monoloop.com/" target="_blank">www.monoloop.com</a>

@endsection
