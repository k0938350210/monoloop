@extends('login')

@section('content')

@include('blocks.fullbg')

<div class="wrapper">
  <div class="row">
    <div style="width:700px;margin-left:20px;">
      <div id="login-box" class="panel login-panel">
        <div class="panel-body">
          <form role="form" action="{{ url('auth/login')}}" onsubmit=" return validateLoginForm()" method="POST">
            <div class="row">
              <div class="col-md-7">
                <h2>Welcome to Monoloop</h2>
                <div>
                  <span id="username" ></span>
                </div>
                @include('blocks.error')

                @if( Notification::has('success') )
                  {!! Notification::showSuccess() !!}
                @else
                  @if (count($errors) === 0 )
                  <br/><br/>
                  @endif
                @endif

                <p>Monoloop enable you to build rich customer profiles, implement omnichannel real time personalization on any website and build more engaging, relevant user experiences.</p>
                <p>About <a href="//www.monoloop.com">Monoloop</a></p>
              </div>
              <div class="col-md-5"  >
                <img src="/images/monoloop-logo-v2.png" >
                <br/>
                <p>Sign In</p>
                <div class="form-group">
                  <input type="input" class="form-control" id="username" name="username" placeholder="Username" value="{{ old('username') }}"/>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" id="password1" name="password" placeholder="Password" />
                </div>
                  <span></span>
                <div class="row">
                  <div class="col-md-6 col-md-offset-6">
                   <button type="submit" class="btn btn-signin form-control">Sign In</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="panel login-panel">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-7">
            <a style="line-height: 2.42857;" href="{{ Request::secure() ? secure_url('/password/email') : url('/password/email') }}">Forgotten your password?</a>
            </div>
            <div class="col-md-5">
              <div class="row">
                <div class="col-md-6 text-right">
                  <span style="line-height: 2.42857;">New User</span>
                </div>
                <div class="col-md-6">
                 <a href="{{ Request::secure() ? secure_url('/auth/register') : url('/auth/register')}}" class="btn btn-signup form-control">Sign Up</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection
