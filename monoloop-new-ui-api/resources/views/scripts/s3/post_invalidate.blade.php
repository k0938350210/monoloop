<InvalidationBatch xmlns="http://cloudfront.amazonaws.com/doc/2012-05-05/?{{Config::get('app.version') }}">
  <Paths>
    <Quantity>{{count($batches)}}</Quantity>
    <Items>
    @foreach( $batches as $batch ) 
      <Path>{{$batch}}</Path>
    @endforeach
    </Items>
  </Paths>
  <CallerReference>{{ $call_id }}</CallerReference>
</InvalidationBatch>