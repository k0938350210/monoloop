<?php $i = 0 ; ?>
@if($tracker->tracker_events != null)
  @foreach($tracker->tracker_events as $event)
    <?php $i++ ; ?>
    @if($event['type'] === 'page-render')
      {{$function_name}}();
    @elseif($event['type'] === 'click')
      MONOloop.click(MONOloop.$('{{$event['selector']}}'),function(evt){

        if(evt.isTrusted == false){
          return true;
        }
        if (evt.preventDefault) evt.preventDefault();

        var me = this;

        {{$function_name}}() ;

        MONOloop.pushPostData(null,true);

        setTimeout(function(){
          evt.defaultPrevented = function(){ return false ;} ;
          me.click(evt);
        },500);
      });
    @elseif($event['type'] === 'custom')
      function {{$function_name}}_custom_{{$i}}(callback){
        try{
          {!!$event['custom']!!}
        }catch(err){
          //No call back for fail
          return;
        }
      }
      {{$function_name}}_custom_{{$i}}({{$function_name}});
    @endif

  @endforeach
@endif
