var {{$field_name}} = '' ;
@if($field_data['type'] === 'x-path')
  {{$field_name}} = MONOloop.html(MONOloop.$(selector + ' {{$field_data['selector']}}'));
@elseif($field_data['type'] === 'meta')
  {{$field_name}} = MONOloop.catReadMeta('{{$field_data['meta']}}');
@elseif($field_data['type'] === 'cookie')
  {{$field_name}} = MONOloop.getCookie('{{$field_data['cookie']}}');
@elseif($field_data['type'] === 'js')
  MONOloop.globalEval('var temp = window{!!addslashes($field_data['js'])!!};');
  {{$field_name}} = temp ;
@elseif($field_data['type'] == 'custom' )
  function monoloop_cate_customfunction_{{$index}}(){
    try{
      {!!$field_data['custom']!!}
    }catch(err){
      return '';
    }
  }
  {{$field_name}} =  monoloop_cate_customfunction_{{$index}}();
@endif

@if($field_data['filter'] == 'price')
  {{$field_name}} = MONOloop.getCartPriceDefaultFilter({{$field_name}});
@elseif($field_data['filter'] == 'image')
  {{$field_name}} = MONOloop.getImageFilter({{$field_name}});
@elseif($field_data['filter'] == 'custom')
  function monoloop_cate_customfilte_{{$index}}(result){
    try{
      {!!$field_name['filter_custom']!!}
    }catch(err){
      return '';
    }
  }
  {{$field_name}} = monoloop_cate_customfilter_{{$index}}({{$field_name}}) ;
@endif