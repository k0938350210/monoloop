var cart = MONOloop.$('table.cart') ;
if(cart.length > 0){
  var cart_rows = MONOloop.$('table.cart tbody tr.cart_item') ;
  var total_cart_row = cart_rows.length ;
  for(var i = 0 ; i < total_cart_row ; i++){
    var selector = 'table.cart tbody tr.cart_item:eq(' + i + ')' ;

    var name = MONOloop.html(MONOloop.$(selector + ' td[data-title="Product"] a'));
    var currency = MONOloop.html(MONOloop.$(selector + ' td[data-title="Price"] span span'));
    var price = MONOloop.html(MONOloop.$(selector + ' td[data-title="Price"] span'));
    price = MONOloop.getCartPriceDefaultFilter (price ) ;
    var quantity = MONOloop.val(MONOloop.$(selector + ' td[data-title="Quantity"] input'));
    var sku = MONOloop.$(selector + ' td.product-remove a');
    sku = sku[0].dataset.product_sku ;

    MONOloop.trackCart(sku,name,price, quantity, currency);
  }

}
