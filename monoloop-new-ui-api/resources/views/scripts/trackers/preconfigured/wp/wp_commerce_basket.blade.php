var cart = MONOloop.$('table.checkout_cart') ;
if(cart.length > 0){
  var cart_rows = MONOloop.$('table.checkout_cart tbody tr.product_row') ;
  var total_cart_row = cart_rows.length ;
  for(var i = 0 ; i < total_cart_row ; i++){
    var selector = 'table.checkout_cart tbody tr.product_row:eq(' + i + ')' ;
    var name = MONOloop.html(MONOloop.$(selector + ' td.wpsc_product_name a'));
    var price = MONOloop.html(MONOloop.$(selector + ' td span.pricedisplay:eq(0)'));
    var currency = price.replace(/\d+([,.]\d+)?/g,'');
    price = MONOloop.getCartPriceDefaultFilter (price ) ;
    var quantity = MONOloop.val(MONOloop.$(selector + ' td.wpsc_product_quantity input[name="quantity"]'));
    var sku = null ;
    MONOloop.trackCart(sku,name,price, quantity, currency);
  }
}