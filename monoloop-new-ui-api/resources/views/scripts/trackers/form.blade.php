if(MONOloop.passUrlConfig('{{$tracker->urlConfig->url}}','{{$tracker->urlConfig->url_option}}','{{$tracker->urlConfig->reg_ex}}',1,1)){
  function tracker_custom_{{$tracker->uid}}(){
    var detail_obj = [] ;
    <?php $i = 1 ; ?>
    @foreach($tracker->tracker_fields as $key => $field)
      var asd = {} ;
      var value = '' ;

      <?php if($i == 1 ){ $i++ ; continue ; } ?>

      @if($field['type'] == 'custom' )
        function monoloop_cate_customfunction_{{$tracker->uid}}_{{$key}}(){
          try{
            {!!$field['custom']!!}
          }catch(err){
            return '';
          }
        }
        value =  monoloop_cate_customfunction_{{$tracker->uid}}_{{$key}}();
      @endif

      @if($field['filter'] == 'price')
        value = MONOloop.getCartPriceDefaultFilter(value);
      @elseif($field['filter'] == 'image')
        value = MONOloop.getImageFilter(value);
      @elseif($field['filter'] == 'custom')
        function monoloop_cate_customfilter_{{$tracker->uid}}(result){
          try{
            {!!$field['filter_custom']!!}
          }catch(err){
            return '';
          }
        }
        value = monoloop_cate_customfilter_{{$tracker->uid}}(value) ;
      @endif
      asd['g'] = {{ $i }};
      asd['n'] = MONOloop.trim(value) ;
      asd['s'] = {{ $tracker->getOperatorNumber($field['operator']) }};
      asd['t'] = {{ $tracker->getFieldTypeNumber($field['field_type']) }};
      detail_obj.push(asd) ;
      <?php $i++ ?>
    @endforeach

    MONOloop.trackForm({{$tracker->uid}} , detail_obj);
  }

  @include('scripts.trackers.event', ['function_name' => 'tracker_custom_'.$tracker->uid , 'tracker' => $tracker])
}