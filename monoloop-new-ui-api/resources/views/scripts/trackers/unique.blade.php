if(MONOloop.passUrlConfig('{{$tracker->urlConfig->url}}','{{$tracker->urlConfig->url_option}}','{{$tracker->urlConfig->reg_ex}}',1,1)){
  function tracker_custom_{{$tracker->uid}}(){
    var detail_obj = [] ;
    <?php $i = 1 ; ?>
    @if(is_array($tracker->tracker_fields))
    @foreach($tracker->tracker_fields as $key => $field)
      @if($i > 1)
        <?php break; ?>
      @endif
      var asd = {} ;
      var value = '' ;
      @if($field['type'] === 'x-path')
        value = MONOloop.catReadXP('{{$field['selector']}}');
      @elseif($field['type'] == 'meta')
        value = MONOloop.catReadMeta('{{$field['meta']}}');
      @elseif($field['type'] == 'cookie')
        value = MONOloop.getCookie('{{$field['cookie']}}');
      @elseif($field['type'] == 'js')
        MONOloop.globalEval('var temp = window{!!addslashes($field['js'])!!};');
        value = temp ;
      @elseif($field['type'] == 'custom')
        function monoloop_cate_customfunction_{{$tracker->uid}}_{{$key}}(){
          try{
            {!!$field['custom']!!}
          }catch(err){
            return '';
          }
        }
        value =  monoloop_cate_customfunction_{{$tracker->uid}}_{{$key}}();
      @endif

      if(value === undefined){
        value = '';
      }

      @if($field['filter'] == 'price')
        value = MONOloop.getCartPriceDefaultFilter(value);
      @elseif($field['filter'] == 'image')
        value = MONOloop.getImageFilter(value);
      @elseif($field['filter'] == 'custom')
        function monoloop_cate_customfilter_{{$tracker->uid}}(result){
          try{
            {!!$field['filter_custom']!!}
          }catch(err){
            return '';
          }
        }
        value = monoloop_cate_customfilter_{{$tracker->uid}}(value);
      @endif
      MONOloop.uid = MONOloop.trim(value);
      <?php $i++ ?>
    @endforeach
    @endif
  }
  tracker_custom_{{$tracker->uid}}();
}