if(MONOloop.passUrlConfig('{{$tracker->urlConfig->url}}','{{$tracker->urlConfig->url_option}}','{{$tracker->urlConfig->reg_ex}}',1,1)){
  function tracker_category_{{$tracker->uid}}(){
    var name = '' ;
    @if($name_field)
      @if($name_field['type'] == 'x-path' )
        name = MONOloop.catReadXP('{{$name_field['selector']}}');
      @elseif($name_field['type'] == 'meta' )
        name = MONOloop.catReadMeta('{{$name_field['meta']}}');
      @elseif($name_field['type'] == 'cookie' )
        name = MONOloop.getCookie('{{$name_field['cookie']}}');
      @elseif($name_field['type'] == 'js' )
          MONOloop.globalEval('var temp = window{!!addslashes($name_field['js'])!!};');
        name = temp ;
      @elseif($name_field['type'] == 'custom' )
        function monoloop_cate_customfunction_{{$tracker->uid}}(){
          try{
            {!!$name_field['custom']!!}
          }catch(err){
            return '';
          }
        }
        name = monoloop_cate_customfunction_{{$tracker->uid}}() ;
      @endif
      @if($name_field['filter'] == 'price')
        name = MONOloop.getCartPriceDefaultFilter(name);
      @elseif($name_field['filter'] == 'image')
        name = MONOloop.getImageFilter(name);
      @elseif($name_field['filter'] == 'custom')
        function monoloop_cate_customfilter_{{$tracker->uid}}(result){
          try{
            {!!$name_field['filter_custom']!!}
          }catch(err){
            return '';
          }
        }
        name = monoloop_cate_customfilter_{{$tracker->uid}}(name) ;
      @endif
    @endif

    var id = '' ;
    @if($id_field)
      @if($id_field['type'] == 'x-path' )
        id = MONOloop.catReadXP('{{$id_field['selector']}}');
      @elseif($id_field['type'] == 'meta' )
        id = MONOloop.catReadMeta('{{$id_field['meta']}}');
      @elseif($id_field['type'] == 'cookie' )
        id = MONOloop.getCookie('{{$id_field['cookie']}}');
      @elseif($id_field['type'] == 'js' )
          MONOloop.globalEval('var temp = window{!!addslashes($id_field['js'])!!};');
        id = temp ;
      @elseif($id_field['type'] == 'custom' )
        function monoloop_cate_customfunction_id{{$tracker->uid}}(){
          try{
            {!!$id_field['custom']!!}
          }catch(err){
            return '';
          }
        }
        id = monoloop_cate_customfunction_id{{$tracker->uid}}() ;
      @endif
      @if($id_field['filter'] == 'price')
        id = MONOloop.getCartPriceDefaultFilter(id);
      @elseif($id_field['filter'] == 'image')
        id = MONOloop.getImageFilter(id);
      @elseif($id_field['filter'] == 'custom')
        function monoloop_cate_customfilter_id{{$tracker->uid}}(result){
          try{
            {!!$id_field['filter_custom']!!}
          }catch(err){
            return '';
          }
        }
        id = monoloop_cate_customfilter_id{{$tracker->uid}}(id);
      @endif
    @endif

    name = MONOloop.trim(name);
    id = MONOloop.trim(id);
    MONOloop.trackCategory({{$tracker->uid}},name,null,null,2,null,id);
  }


  @include('scripts.trackers.event', ['function_name' => 'tracker_category_'.$tracker->uid , 'tracker' => $tracker])
}