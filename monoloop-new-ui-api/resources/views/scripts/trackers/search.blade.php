if(MONOloop.passUrlConfig('{{$tracker->urlConfig->url}}','{{$tracker->urlConfig->url_option}}','{{$tracker->urlConfig->reg_ex}}',1,1)){
  function tracker_search_{{$tracker->uid}}(){
    console.log(1);
    var name = '' ;
  @if($tracker->tracker_fields[0]['type'] == 'x-path' )
    name = MONOloop.catReadXP('{{$tracker->tracker_fields[0]['selector']}}');
  @elseif($tracker->tracker_fields[0]['type'] == 'meta' )
    name = MONOloop.catReadMeta('{{$tracker->tracker_fields[0]['meta']}}');
  @elseif($tracker->tracker_fields[0]['type'] == 'cookie' )
    name = MONOloop.getCookie('{{$tracker->tracker_fields[0]['cookie']}}');
  @elseif($tracker->tracker_fields[0]['type'] == 'js' )
    MONOloop.globalEval('var temp = window{!!addslashes($tracker->tracker_fields[0]['js'])!!};');
    name = temp ;
  @elseif($tracker->tracker_fields[0]['type'] == 'custom' )
    function monoloop_cate_customfunction_{{$tracker->uid}}(){
      try{
        {!!$tracker->tracker_fields[0]['custom']!!}
      }catch(err){
        return '';
      }
    }
    name = monoloop_cate_customfunction_{{$tracker->uid}}() ;
  @endif
  @if($tracker->tracker_fields[0]['filter'] == 'price')
    name = MONOloop.getCartPriceDefaultFilter(name);
  @elseif($tracker->tracker_fields[0]['filter'] == 'image')
    name = MONOloop.getImageFilter(name);
  @elseif($tracker->tracker_fields[0]['filter'] == 'custom')
    function monoloop_cate_customfilter_{{$tracker->uid}}(result){
      try{
        {!!$tracker->tracker_fields[0]['filter_custom']!!}
      }catch(err){
        return '';
      }
    }
    name = monoloop_cate_customfilter_{{$tracker->uid}}(name) ;
  @endif
    MONOloop.trackST( name ) ;
  }


  @include('scripts.trackers.event', ['function_name' => 'tracker_search_'.$tracker->uid , 'tracker' => $tracker])
}