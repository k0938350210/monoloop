<!DOCTYPE html>
<html lang="en" ng-app='APP'>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Monoloop</title>

	<link href="{{ elixir('css/monoloop.css') }}" rel="stylesheet">
	<link href="/css/font-awesome.min.cssW" rel="stylesheet">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&{{Config::get('app.version') }}' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js?{{Config::get('app.version') }}"></script>
		<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js?{{Config::get('app.version') }}"></script>
	<![endif]-->


	@yield('customcss')
</head>
<body class="ui">
	<header class="header">
		<div class="logo">
			<a href="/"><img src="/images/logo.gif" alt="" /></a>
			<span>REALTIME BEHAVOIRAL TARGETING</span>
		</div>
		<div class="loginbar">
			<span>You are logged in as <a href="{{ url('account/profile') }}">{{ Auth::user()->username }}</a></span>
			<a href="{{ url('/auth/logout') }}" id="logout-btn"><span class="icon icon-power-off"></span></a>
		</div>
	</header>
    <aside class="sidebar">
		<div id="account-selector">
			<div id="account-main">{{ Auth::user()->account->name }}<span class="arrow icone-caret-down"></span></div>
			<ul>
				@foreach ($account_selectors as $account)
				<li onclick="changeAccount('{{ $account['id'] }}');">{{ $account['name'] }}</a></li>
				@endforeach
		</div>
		<nav id="nav" class="accordion">
			<ul id="navigation">
				<li class="divider">MAIN MENU</li>

				@foreach ($left_menus as $k => $v)
				<li class="accordion-group @if($k == $selected[0]) active @endif">
					<a href="{{ $v['href'] }}" data-parent="#" data-toggle="collapse" >
						<span class="icon {{$v['icon']}}"></span>
						<span class="text">{{$k}}</span>
						<span class="arrow @if(count($v['items'])) icone-caret-down @endif"></span>
					</a>
					<ul class="collapse @if($k == $selected[0]) in @endif" style="height: @if($k == $selected[0]) auto @else 0px @endif;" data-height="{{ count($v['items']) * 36 }}">
						@foreach ($v['items'] as $k2 => $v2)
						<li class="@if($k2 == $selected[1]) active @endif">
							<a href="{{ $v2['href'] }}">
								<span class="icon icone-angle-right"></span>
								{{ $k2 }}
							</a>
						</li>
						@endforeach
					</ul>
				</li>
				@endforeach
			</ul>
		</nav>
	</aside>
    <section id="main" style="min-height: 450px;">
        <div class="navbar navbar-static-top">
            <div class="navbar-inner">
                <ul class="breadcrumb">
                	@if( count($selected) == 1 )
                		<li class="active">{{ $selected[0] }}</li>
                	@else
                		<li id="ext-gen1074">
							<a href="#">{{ $selected[0] }}</a>
							<span class="divider"></span>
						</li>
                		<li class="active">{{ $selected[1] }}</li>
                	@endif
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            @yield('content')
        </div>
    </section>
    <footer id="footer">
 		<p>By Monoloop</p>
    </footer>
    @yield('footerjs')
</body>
</html>
