<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use DB ;

class Experiment extends Eloquent {

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $collection = 'Experiments';

    public function account(){
      return $this->belongsTo(\App\Account::class,'account_id');
    }

    /*
     * get Experiment's segment
     *
     * @rerurn Object
     */

     public function segment()
     {
       return $this->belongsTo('\App\Segment','segment_id');
     }

     /*
      * get Experiment's goal
      *
      * @rerurn Object
      */

      public function goal()
      {
        return $this->belongsTo('\App\Goal','goal_id');
      }

    /*
     * The experiments associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public static function experiments($folder = NULL, $isRoot = false, $cid = NULL){

    	$branch = [];

      if($isRoot){
        $experiments = self::where('deleted', '<>', 1)
                                            ->where(function ($query) use ($folder, $cid) {
                                                $query->where('folder_id', '=', new \MongoId($folder))
                                                      ->orWhere(function ($query) use ($folder, $cid) {
                                                        $query->where('folder_id', '=', null)
                                                              ->where('cid', '=', $cid);
                                                      });
                                          })->get();
      } else {
          $experiments = self::where('folder_id', '=', new \MongoId($folder))->where('deleted', '<>', 1)->get();
      }


        foreach ($experiments as $key => $experiment) {
            $node = new \stdClass();
            $node->id = "experiment__".$experiment->_id;
            $node->value = $experiment->name;
            $node->type = "file";
            $node->description = "";

            if(!empty($experiment->segment_id) && $experiment->segment){
              $node->segment = $experiment->segment->name;
            } else {
              $node->segment = "";
            }
            if(!empty($experiment->goal_id) && $experiment->goal){
              $node->goal = $experiment->goal->name;
            } else {
              $node->goal = "";
            }
            $node->experimentID = $experiment->experimentID;
            $node->date = date('j F, Y', strtotime($experiment->created_at));
            $node->hidden = $experiment->attributes['hidden'] ;
            $node->deleted = $experiment->deleted;

    		    array_push($branch, $node);
    	}
      $res['data'] = $branch;
      $res['parent'] = 'folder__'.$folder;
    	return $res;
    }

    public function scopeByAccount($query , $account_id){
      return $query->where('account_id', '=', new \MongoId($account_id));
    }

    public function scopeFolderExperiment($query , $folder_id){
      return $query->where('folder_id', '=', new \MongoId($folder_id));
    }

    public function unsetReportAggregated(){
      DB::collection('ReportingArchiveAggregated')->where('experiments.'.$this->experimentID,'exists',true)->where('cid',(int)$this->account->uid )->unset('experiments.' . $this->experimentID);
    }
}
