<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class UserAccount extends Eloquent{
	
	public function account()
    {
        return $this->belongsTo('\App\Account','account_id');
    }
}