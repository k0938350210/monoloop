<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;


class PageElementExperiment extends Eloquent{

  protected $fillable = ["priority", "controlGroupSize", "controlGroupDays", "experimentID", "goalID", "segmentID"];

  
}
