<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class ExperimentHistory extends Eloquent
{
    //
    protected $collection='experiment_histories';

}
