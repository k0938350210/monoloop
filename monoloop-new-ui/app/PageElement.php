<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;


class PageElement extends Eloquent {

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }

  protected $collection = 'PageElements';

  /*
   * The placements associated with specific folder
   *
   * @param folder Object
   * @rerurn array
   */
  public static function pages($folder = NULL, $isRoot = false, $cid = NULL){

    $branch = [];
    if($isRoot){
        $pageElements = PageElement::where('folder_id', '=', new \MongoId($folder))
                            ->orwhere(function ($query) use ($folder) {
                                        $query->where('folder_id', '=', NULL);
                          } )->where(function ($query) {
                                   $query->where('deleted', '=', 0)
                                         ->orwhere(function ($query) {
                                                     $query->where('deleted', '=', NULL);
                                       } );
                                 })->whereNotNull('originalUrl')->where('cid',$cid )->orderBy('created_at', 'desc')->get();

    } else {
        $pageElements = PageElement::where('folder_id', '=', new \MongoId($folder))
                                      ->where(function ($query) {
                                               $query->where('deleted', '=', 0)
                                                     ->orwhere(function ($query) {
                                                                 $query->where('deleted', '=', NULL);
                                                   } );
                                             })->whereNotNull('originalUrl')->orderBy('created_at', 'desc')->get();
    }


      foreach ($pageElements as $key => $pageElement) {
          $node = new \stdClass();
          $node->id = "webpagelist__".$pageElement->_id;
          $node->value = $pageElement->originalUrl;
          $node->type = "file";
          $node->description = "";

          $node->inWWW = $pageElement->inWWW;
          $node->inHTTP_HTTPS = $pageElement->inHTTP_HTTPS;
          $node->urlOption = $pageElement->urlOption;
          $node->regExp = $pageElement->regExp;
          $node->remark = $pageElement->remark;

          $node->date = date('j F, Y', strtotime($pageElement->created_at));
          if(isset($pageElement->attributes['hidden'])){
            $hidden = $pageElement->attributes['hidden'];
          } else {
            $hidden = 0;
          }
          $node->hidden = $hidden;
          $node->condition = "";
          array_push($branch, $node);
    }
    $res['data'] = $branch;
    $res['parent'] = 'folder__'.$folder;
    return $res;
  }

  public function content(){
    return $this->embedsMany(\App\PageElementContent::class);
  }

  public function scopeByAccountCid($query , $cid){
    return $query->where('cid', '=', (string)$cid );
  }


  public function save(array $options = [])
  {
    #beforesave
    $this->setUrlHash($options) ;

    parent::save();
  }

  #---- method
  public function setUrlConfig($urlConfig){
    $this->urlhash = $urlConfig->urlHash ;
    $this->fullURL = $urlConfig->cleanUrl ;
    $this->regExp = $urlConfig->reg_ex ;
    $url = parse_url($urlConfig->url) ;
    $query  = isset($url['query']) ? '?' . $url['query'] : '';
    if( $urlConfig->url_option == 0 || $urlConfig->url_option === 'exactMatch'){
      parse_str($query , $qs) ;
      ksort($qs);
      $this->exactQS = http_build_query($qs);
    }else if( $urlConfig->url_option == 1 || $urlConfig->url_option === 'allowParams'){
      parse_str($query, $qs) ;
      ksort($qs);
      $this->qs = $qs ;
    }else if( $urlConfig->url_option == 2 || $urlConfig->url_option === 'startsWith'){
      parse_str($query , $qs);
      ksort($qs);
      $this->qs = $qs ;
      $this->startsWith = true;
    }
    $this->urlOption = $urlConfig->url_option;
    if( $urlConfig->inc_www  == true ){
      $this->inWWW = true ;
      $this->exWWW = true ;
    }else{
      $pos = stripos( $urlConfig->url , '/www.'  );
      if( $pos === false){
        $this->inWWW = false ;
        $this->exWWW = true ;
      }else{
        $this->inWWW = true ;
        $this->exWWW = false ;
      }
    }
    // Process HTTP / HTTPS ;
    $this->inHTTP_HTTPS = false ;

    if($urlConfig->inc_http_https == 1)
      $this->inHTTP_HTTPS = true ;
  }

  public function setUrlHash(){
    // Clean URL from sune code .
    $url_info = parse_url($this->fullURL);
    if(count($url_info) == 1){
      $this->urlhash = substr(md5($this->fullURL), 0, 16) . sprintf('%x', crc32($this->fullURL));
      return ;
    }
    $port = '' ;
    if (isset($url_info['port']) && $url_info['port']!=80) {
      $port = ':'.$url_info['port'];
    }

    $path = '' ;
    if (isset($url_info['path'])) {
      $path = $url_info['path'] ;
    }
    if(!isset($url_info['host'])){
      $url_info['host'] = '' ;
    }
    $url = isset($url_info['scheme'])?$url_info['scheme']:'http' ;
    $url .= '://'.$url_info['host'].$port.$path;
    $this->urlhash = substr(md5($url), 0, 16) . sprintf('%x', crc32($url));
  }

  public function experiments()
  {
    return $this->embedsMany(\App\PageElementExperiment::class,'Experiment');
  }
}
