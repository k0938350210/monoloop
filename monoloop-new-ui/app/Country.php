<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use MongoId;

class Country extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'countries';

}
