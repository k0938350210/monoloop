<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Placement extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $collection = 'placements';

    /*
     * The placements associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public function getFolderItems($folder = NULL, $isRoot = false, $account_id = NULL){

    	$branch = [];

      if($isRoot){
          $placements = Placement::where('folder_id', '=', new \MongoId($folder))
                              ->orwhere(function ($query) use ($folder, $account_id) {
                                          $query->where('folder_id', '=', NULL)
                                                ->where('account_id', '=', $account_id);
                            } )->where('deleted', '=', 0)->get();
      } else {
          $placements = Placement::where('folder_id', '=', new \MongoId($folder))->get();
      }


        foreach ($placements as $key => $placement) {
            $node = new \stdClass();
            $node->id = "segment__".$placement->_id;
            $node->value = $placement->url;
            $node->type = "file";
            $node->description = $placement->remarks;


            $node->date = date('j F, Y', strtotime($placement->created_at));
            $node->hidden = 0;
            $node->condition = "";
    		    array_push($branch, $node);
    	}

    	return $branch;
    }

    /*--- Relation --- */
  	public function urlConfig(){
      return $this->embedsOne(\App\UrlConfig::class,null,'url_config','url_config');
    }

    public function details(){
      return $this->embedsMany(\App\PlacementDetail::class);
    }


  	/*--- Scope ---*/

    public function scopeByAccount($query , $account_id){
      return $query->where('account_id', '=', new \MongoId($account_id));
    }

    public function scopeByUid($query,$uid){
      return $query->where('uid','=',(int)$uid) ;
    }

    public function scopeAvailable($query){
      return $query->where('deleted' , 0) ;
    }

}
