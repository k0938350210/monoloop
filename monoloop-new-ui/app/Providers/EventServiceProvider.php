<?php namespace App\Providers;

use App\Events\ExperimentHistoryLog;
use App\Handlers\Events\GenerateExperimentHistory;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\Logger;
use App\Handlers\Events\GenerateLogger;
use App\Events\Invitation;

use App\Handlers\Events\SendEmail;
use App\Handlers\Events\UploadListener ;

use Unisharp\Laravelfilemanager\Events\ImageWasUploaded;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		Logger::class => [
			GenerateLogger::class,
		],
    ExperimentHistoryLog::class => [
      GenerateExperimentHistory::class,
    ],
		Invitation::class => [
			SendEmail::class,
		],
		'auth.login' => [
      'App\Handlers\Events\AuthLoginEventHandler',
    ],
    ImageWasUploaded::class => [
      UploadListener::class,
    ],
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
