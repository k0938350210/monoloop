<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider; 
use App\Queue\MonoloopConnector;

class MonoloopServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		\Auth::extend('monoloop', function($app)
    {
        return new MonoloopUserProvider();
    });
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
    // Add connector for queue support.
    $this->app->resolving('queue', function ($queue) {
      $queue->addConnector('monoloopmongodb', function () {
        return new MonoloopConnector($this->app['db']);
      });
    });
	}

}
