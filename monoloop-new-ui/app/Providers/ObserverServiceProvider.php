<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
    \App\Content::observe( new \App\Observers\ContentObserver );
    \App\Goal::observe( new \App\Observers\GoalObserver );
    \App\Segment::observe( new \App\Observers\SegmentObserver );
    \App\PageElement::observe( new \App\Observers\PageElementObserver );
    \App\PageElementContent::observe( new \App\Observers\PageElementContentObserver );
    \App\Funnel::observe( new \App\Observers\FunnelObserver );
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{

	}

}
