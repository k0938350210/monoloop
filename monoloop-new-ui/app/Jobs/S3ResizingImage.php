<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class S3ResizingImage extends Job implements SelfHandling, ShouldQueue
{
	use InteractsWithQueue, SerializesModels;
  protected $image_path;
  protected $filename ;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($image_path , $filename)
  {
    $this->image_path = $image_path ;
    $this->filename = $filename;
  }

  public function handle()
  {
  	//
  }
}