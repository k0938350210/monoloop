<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\AccountUser ; 
use App\Services\Account\Mailservice;

class SendInvitationEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $accountUser ; 
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AccountUser $accountUser)
    {
      $this->accountUser = $accountUser ; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      if ($this->attempts() < 3) {
        #call mail job ; 
        $mailService = new Mailservice() ; 
        $mailService->sendInvitationMail($this->accountUser) ; 
      }
      
    }
}
