<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use App\Services\MonoloopCondition ;

class Goal extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'goals';
    protected $fillable = ['name', 'point', 'type' , 'condition' , 'condition_json' , 'hidden' , 'deleted' ];
    /**
     * Options for goal types.
     *
     * @var array
     */
    public static $types = array('page_related', 'condition_related');

    /**
     * Url Config
     */
    public function urlConfig(){
      return $this->embedsOne(\App\UrlConfig::class,null,'url_config','url_config');
    }

    public function pageElement(){
      return $this->belongsTo(\App\PageElement::class,'page_element_id');
    }

    public function account(){
      return $this->belongsTo(\App\Account::class,'account_id');
    }

    /*
     * The experiments associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public static function goals($folder = null, $isRoot = false, $cid = null)
    {
        $branch = [];
        if ($isRoot) {
            $goals = self::where('deleted', '<>', 1)
                                                ->where(function ($query) use ($folder, $cid) {
                                                    $query->where('folder_id', '=', new \MongoId($folder))
                                                          ->orWhere(function ($query) use ($folder, $cid) {
                                                            $query->where('folder_id', '=', null)
                                                                  ->where('cid', '=', $cid);
                                                          });
                                              })->get();
        } else {
            $goals = self::where('folder_id', '=', new \MongoId($folder))->where('deleted', '<>', 1)->get();
        }

        foreach ($goals as $key => $goal) {
            $node = new \stdClass();
            $node->id = 'goal__'.$goal->_id;
            $node->value = $goal->name;
            $node->type = 'file';
            $node->goal_type = $goal->type;

            $node->url_config = $goal->urlConfig;
            if(is_object($goal->urlConfig)){
              $goal->urlConfig->url_option = $goal->urlConfig->getOptionStringAttribute($goal->urlConfig->url_option);
            }
            $node->point = $goal->point;
            $node->date = date('j F, Y', strtotime($goal->created_at));
            $node->hidden = $goal->attributes['hidden'] ;
            $node->deleted = $goal->deleted;
            $node->condition = $goal->condition;
            array_push($branch, $node);
        }

        $res['data'] = $branch;
        $res['parent'] = 'folder__'.$folder;
        return $res;
    }

    /*---- Attribute ---*/
    public function getCodeAttribute(){
      if($this->condition != ''){
        $condition = new MonoloopCondition($this->condition) ;
        $condition->replacePipe(' goals.push({ \'uid\' : '.$this->uid.',\'p\' : '.$this->point.' });');
        return  ' <% ' . $condition . ' %>' ;
      }else{
        return ' <%  ' . 'goals.push({ \'uid\' : '.$this->uid.',\'p\' : '.$this->point.' });'. ' %>'  ;
      }
    }

    /*
     * The Goals associated with account_id
     *
     * @param $query
     * @param $account_id
     *
     * @rerurn array
     */
    public function scopeCompactList($query , $cid){
      return $query->where('cid', '=', $cid)->where('deleted', '<>', 1 )->get();
    }

    public function scopeByAccount($query , $account_id){
      return $query->where('account_id', '=', new \MongoId($account_id));
    }

    public function scopeFolderGoal($query , $folder_id){
      return $query->where('folder_id', '=', new \MongoId($folder_id));
    }
}
