<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use MongoId ;

class CbrBatch extends Eloquent{

	protected $collection = 'cbr_batches';
}
