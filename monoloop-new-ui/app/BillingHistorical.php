<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class BillingHistorical extends Eloquent{

	protected $collection = 'billing_historicals';

	protected $dates = ['billing_date'];


	public function account(){
    return $this->belongsTo(\App\Account::class,'account_id');
  }


}