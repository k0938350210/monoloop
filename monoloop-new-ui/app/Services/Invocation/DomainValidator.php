<?php namespace App\Services\Invocation;

use Goutte\Client;

class DomainValidator {

  public function checkInvocation($domain){


    $status = 0 ;

    try {
        $fileSource = $domain;

        if(strpos($fileSource, 'http') === false){
          $fileSource = 'http://'.trim($fileSource);
        }


        $ch = curl_init($fileSource);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($retcode != 200 && $retcode != 301) {
            // var_dump($retcode, $domain);die;
            $status = 0 ;
        } else {
          $client = new Client();

          $crawler = $client->request('GET', 'http://' . $domain);
          if(!$crawler){
            throw new Exception("Invalid URL.");
          }
          $status_code = $client->getResponse()->getStatus();

          $status = 0;

          $crawler->filter('head script')->each(function ($node) use (&$status)  {
            $content = $node->text() ;
            $pos = strpos($content , 'ML_vars') ;
            if( $pos !== false){
              $status = 2 ;
            }
          });

          $crawler->filter('body script')->each(function ($node) use (&$status)  {
            $content = $node->text() ;
            $pos = strpos($content , 'ML_vars') ;
            if( $pos !== false){
              $status = 1 ;
            }
          });
        }


    } catch (Exception $e) {
        $status = 0;

    }

    return $status ;
  }
}
