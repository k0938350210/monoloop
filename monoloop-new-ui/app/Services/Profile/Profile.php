<?php namespace App\Services\Profile;

use URL;

class Profile
{
    private $MonoloopID = null;                    //String
    private $params;

    private $Ci_n = 3;
    private $Ri_n = 3;
    private $Ri_n_weeks = 3;
    private $Di_n = 4;
    private $Li_n = 422;
    private $Li_n_weeks = 2;

    private $searh_referers = array('google','bing');
    private $social_referers = array('facebook','twitter.(dk|com|.)');
    private $search_strings = array('q','search');

    private $GoalReached = false;

    //Static information
    private $TimeZone = 'GMT+1';            //Integer
    private $Firstname = 'SuneB';
    private $Lastname = 'Brodersen';
    private $OS = null;            //Integer
    private $Browser = null;            //Integer
    private $BrowserVersion = null;            //Integer
    private $BrowserLanguage = null;            //Integer
    private $Javascript = 1;            //Integer
    private $Cookies = 1;            //Integer
    private $Flash = null;            //Integer
    private $Quicktime = null;            //Integer
    private $Wma = null;            //Integer
    private $Silverlight = null;            //Integer
    private $Pdf = null;            //Integer
    private $Realplayer = null;            //Integer
    private $Gears = null;            //Integer
    private $Resolution = array(
                            'x' => null,
                            'y' => null,
                            );

    private $ScreenType = null;            //Integer
    private $IP = null;            //Integer
    private $Continent = null;            //Integer
    private $Country = 'al';            //Integer
    private $City = null;            //Integer
    private $ISP = null;            //Integer
    private $Longitude = null;
    private $Latitude = null;
    private $CurrentURL = null;

    // Temp property
    private $VisitReferer = null;
    private $PageViewCount = null;
    private $DaysSinceLastVisit = null;
    private $VisitCount = null;
    private $CurrentPageViewCount = null;

    //Category array
    private $Categories = array(
                            array(
                                'label' => null,
                                'name' => null,
                                'count' => null,
                                'index' => null,
                            ),
                          );

    //Visits
    private $Visits = array(
                            array(
                                'visitID' => null,
                                'entrypage' => null,
                                'exitpage' => null,
                                'totalPoints' => null,
                                'outlink' => null,
                                'type' => null,
                                'referer' => null,
                                'tstamp' => null,
                                'feedback' => null,
                                'achivedGoal' => null,
                                'duration' => null,
                                'clicks' => null,
                                'lastclick' => null ,
                                'firstclick' => null
                            ),
                      );

    //Goals
    private $Goals = array(
                        array(
                            'goalID' => null,
                            'tstamp' => null,
                            'visitID' => null,
                            'points' => null,
                        ),
                    );

    //Surveys
    private $Surveys = array(
                        array(
                            'surveyID' => null,
                            'tstamp' => null,
                            'visitID' => null,
                            'points' => null,
                        ),
                    );

    //Forms
    private $Forms = array(
                        array(
                            'formID' => null,
                            'tstamp' => null,
                            'visitID' => null,
                            'points' => null,
                        ),
                    );

    //Monetary
    private $Monetary = array(
                        array(
                            'purchaseID' => null,
                            'tstamp' => null,
                            'visitID' => null,
                            'revenue' => null,
                            'items' => array(
                                array(
                                    'itemID' => null,
                                    'price' => null,
                                    'number' => null,
                                ),
                            ),
                        ),
                    );

    //Calculated indexes
    private $ClickDepthIn = null;            //Integer
    private $RecencyIndex = null;                    //Integer
    private $DurationIndex = null;            //Integer
    private $BrandIndex = null;            //Integer
    private $FeedbackIndex = null;            //Integer
    private $InteractionIndex = null;            //Integer
    private $LoyaltyIndex = null;            //Integer
    private $SubscriptionIndex = null;            //Integer
    private $EngagementScore = null;

    //Calculated fields:
    private $AverageTimeOnPage = null;
    private $TotalPoints = null;
    private $EntryPage = null;
    private $LastExitPage = null;
    private $VisitDuration = null;
    private $VisitPoints = null;
    private $SocialMediaVisitor = null;
    private $FirstVisit = null;
    private $SearchVisitor = null;
    private $CustomerVisitor = null;
    private $DirectVisitor = null;
    private $VisitSearchString = null;

    //Control array for keeping control on what fields are changed:
    private $hasChanged = array();

    private function forceType()
    {
        settype($this->MonoloopID, 'string');
    }

    public function main()
    {
    }

    public function setProfileValues($data)
    {
        //Set VisitCount:
        $this->VisitCount = count($data['visits']);

        $this->Visits = $data['visits'];

        //Set static info:
        $this->TimeZone = 'timezone';
        $this->Resolution = $data['config_resolution'];
        $this->OS = $data['config_os'];
        $this->ScreenType = $data['config_resolution'];
        $this->Continent = '';
        $this->Country = '';
        $this->City = '';
        $this->ISP = '';
        $this->Longitude = '';
        $this->Latitude = '';
        $this->Browser = $data['config_browser_name'];
        $this->BrowserVersion = $data['config_browser_version'];
        $this->BrowserLanguage = $data['location_browser_lang'];
        $this->Javascript = $data['config_java'];
        $this->Flash = $data['config_flash'];
        $this->Quicktime = $data['config_quicktime'];
        $this->Wma = $data['config_windowsmedia'];
        $this->Silverlight = $data['config_silverlight'];
        $this->PDF = $data['config_pdf'];
        $this->Realplayer = $data['config_realplayer'];
        $this->Gears = $data['config_gears'];
    }

    private function getParam($paramname)
    {
        if (is_array($paramname) && in_array($paramname, $this->params)) {
            return $this->params[$paramname];
        }
    }

    public function __construct()
    {
        if (!empty($_GET['mid'])) {
            $MonoloopID = $_GET['mid'];
        }

        if (!empty($_GET['cid'])) {
            $Customer = $_GET['cid'];
        }

        if (isset($MonoloopID) && $MonoloopID && $Customer) {
            //Load profile
            $m = new Mongo('mongodb.talefod.dk');
            $profile = $m->Monoloop->Profiles->findOne(array('_id' => new MongoId($MonoloopID)));

            //Set countvalues etc:
            $this->setProfileValues($profile);

            //Do calculations:
            //$this->calculateIndexes();
        }
    }

    private function calculateIndexes()
    {
        //Set defailts:
        $clicks_over_n = null;
        $clicks_over_n_weeks = null;
        $duration_over = null;
        $brandcounter = null;
        $feedbackcounter = null;
        $interactioncounter = null;
        $loyaltycounter = null;
        $total_clicks = null;
        $duration_time = null;

        //Set VisitCount:
        $this->VisitCount = count($this->Visits);

        foreach ($this->Visits as $visit) {
            //Ci
            if ($visit['clicks'] > $this->Ci_n) {
                ++$clicks_over_n;
            }

            //Ri
            if (($visit['clicks'] > $this->Ri_n) && ($visit['tstamp'] > (time() - ($this->Ri_n_weeks * 7 * 24 * 60 * 60)))) {
                ++$clicks_over_n_weeks;
            }

            //Di
            if ($visit['duration'] / 60 > ($this->Di_n)) {
                ++$duration_over;
            }

            //Bi
            if ($visit['referer'] == '' or $this->isBrandReferer($visit['referer'])) {
                ++$brandcounter;
            }

            //Fi
            if ($visit['feedback'] == 1) {
                ++$feedbackcounter;
            }

            //Ii
            if ($visit['achivedGoal'] == 1) {
                ++$interactioncounter;
            }

            //Li
            if (($visit['tstamp'] > (time() - ($this->Li_n_weeks * 7 * 24 * 60 * 60)))) {
                ++$loyaltycounter;
            }

            //Count total points:
            $this->TotalPoints += $visit['TotalPoints'];
            $duration_time += $visit['duration'];
            $total_clicks += $visit['clicks'];
        }
        $this->ClickDepthIndex = round(($clicks_over_n / $this->VisitCount), 2);
        $this->RecencyIndex = round(($clicks_over_n_weeks / $this->VisitCount), 2);
        $this->DurationIndex = round(($duration_over / $this->VisitCount), 2);
        $this->BrandIndex = round(($brandcounter / $this->VisitCount), 2);
        $this->FeedbackIndex = round(($feedbackcounter / $this->VisitCount), 2);
        $this->InteractionIndex = round(($interactioncounter / $this->VisitCount), 2);
        $this->LoyaltyIndex = ($loyaltycounter > $this->Li_n) ? 1 : 0;
        $this->SubscriptionIndex = (count($this->Monetary) > 1) ? 1 : 0;
        $this->EngagementScore = round(($this->ClickDepthIndex + $this->RecencyIndex + $this->DurationIndex +
                                  $this->BrandIndex + $this->FeedbackIndex + $this->InteractionIndex +
                                  $this->LoyaltyIndex + $this->SubscriptionIndex) / 8, 2);

        $this->AverageTimeOnPage = round($duration_time / $total_clicks, 2);

        //Order visits to get the newest in top:
        usort($this->Visits, array('self', 'compare_visit'));

        //Now all settings for current visit is in $this->Visits[0] and last visit is in $this->Visits[1];
        $this->EntryPage = $this->Visits[0]['Entrypage'];
        $this->LastExitPage = $this->Visits[1]['Exitpage'];
        $this->VisitDuration = $this->Visits[0]['duration'];
        $this->VisitPoints = $this->Visits[0]['TotalPoints'];

        $this->FirstVisit = ($this->VisitCount > 1) ? false : true;

        $ref_parts = parse_url($this->Visits[0]['referer']);
        $this->SearchVisitor = (in_array($ref_parts['host'], $this->searh_referers)) ? 1 : 0;

        //print_r($this->Visits);
    }

    private function compare_visit($a, $b)
    {
        return strnatcmp($b['tstamp'], $a['tstamp']);
    }

    public function saveProfile()
    {
        //Save the profile if anything has changed!

        $doc = get_object_vars($this);
        foreach ($doc as $key => $value) {
            if (!in_array($key, $this->hasChanged)) {
                //This is an array. Check if any values inside it:
                unset($doc[$key]);
            }
        }
        print_r($doc);
    }

    public function __destruct()
    {
        //Save profile - if something was changed!
    }

    public function __set($var, $val)
    {
        switch ($var) {
            case 'MonoloopID':
            case 'hasChanged':
                //Read only vars so do nothing
                return;
            break;
            default:
                if (property_exists($this, $var)) {
                    $this->$var = $val;
                }
            break;

        }
        if (property_exists($this, $var)) {
            $this->hasChanged[] = $var;
        }
    }

    private function getDefault()
    {
        // This require typo3 request class ;


        if (!class_exists('t3lib_extmgm', false)) {
            $data['category'] = array();
            $data['city'] = '';
            $data['scores_pageviews'] = 0;
            $data['scores_minutes'] = 0;
            $data['scores_visits'] = 0;
            $data['scores_brands'] = 0;

            return $data;
        }
        $file = t3lib_extmgm::extPath('monoloopaccounting').'lib/class.tx_monoloopaccounting_core.php';
        if (!file_exists($file)) {
            return;
        }
        require_once $file;
        $core = tx_monoloopaccounting_core::getObjectSingleton();

        $data = $core->RunController('account/loadAccoutData');

        // $remove basket , purchase , search , download tracker type
        $category = array();
        foreach ($data['category'] as $cRow) {
            if (!in_array($cRow['tracker_type'], array(6, 7, 8, 9))) {
                $category[] = $cRow;
            }
        }

        //print_r($category) ;

        $data['category'] = $category;

        return $data;
    }

    public function getProperties()
    {
        // Prepare default data ;
        $defaulData = $this->getDefault();
        $allVisit = 'All Visits';
        $currentVisit = 'Current Visit';
        $engagement = 'Scores';
        $averages = $engagement;
        $averageList = array(
            'is higher than average' => '1' ,
            'is lower than average' => '-1' ,
            'equals average' => '0' ,
            'is in 1st quintile' => '1' ,
            'is in 2nd quintile' => '2'  ,
            'is in 3rd quintile' => '3'  ,
            'is in 4th quintile' => '4' ,
            'is in 5th quintile' => '5',
        );
        $qa = 'Quintiles & Averages';
        $qaList = array(
          array('Label' => 'is higher than average', 'Value' => 'avg||1'),
          array('Label' => 'is lower than average', 'Value' => 'avg||-1'),
          array('Label' => 'equals average', 'Value' => 'avg||0'),
          array('Label' => 'is in 1st quintile', 'Value' => 'quintile||1'),
          array('Label' => 'is in 2nd quintile', 'Value' => 'quintile||2'),
          array('Label' => 'is in 3rd quintile', 'Value' => 'quintile||3'),
          array('Label' => 'is in 4th quintile', 'Value' => 'quintile||4'),
          array('Label' => 'is in 5th quintile', 'Value' => 'quintile||5')
        );
        $testData = array();
        $testData['lab1'] = 'val1';
        $testData['lab2'] = 'val2';
        $testData['lab3'] = 'val3';
        $testData['lab4'] = 'val4';
        $testData['lab5'] = 'val5';
        $testData['lab6'] = 'val6';

        $timeUnitList = array(
          array('Label' => 'Seconds', 'Value' => 'Seconds'),
          array('Label' => 'Minutes', 'Value' => 'Minutes'),
          array('Label' => 'Hours', 'Value' => 'Hours'),
          array('Label' => 'Days', 'Value' => 'Days'),
          array('Label' => 'Months', 'Value' => 'Months'),
          array('Label' => 'Years', 'Value' => 'Years'),
        );

        $out = array(

                     /*'TotalPoints' =>	   array( 'Label' => 'Total Points', 'Group'=>  'All visits','Type'=> 'integer' ) ,*/
                     'LastEntryPage' => array('key' => 'LastEntryPage', 'Label' => 'Last Entry Page', 'Group' => $allVisit,'Type' => 'string' , 'qtipCfg' => array('text' => 'The full URL of the page that the visitor viewed first in his previous visit.')) ,
                     'LastExitPage' => array('key' => 'LastExitPage', 'Label' => 'Last Exit Page', 'Group' => $allVisit,'Type' => 'string' , 'qtipCfg' => array('text' => 'The full URL of the page that the visitor viewed last in his previous visit.')) ,
                     'VisitCount' => array('key' => 'VisitCount', 'Label' => 'Total Visits', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The total number of visits from the specific user. Monoloop defaults to track the last 365 days of history.')) ,
                     'PageViewCount' => array('key' => 'PageViewCount', 'Label' => 'Total Pageviews', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The total number of pageviews by this visitor.')) ,
                     'DaysSinceLastVisit' => array('key' => 'DaysSinceLastVisit', 'Label' => 'Days Since Last Visit', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The number of days since the visitor visited last time.')) ,
                    //  'LastViewedCategory' => array('key' => 'LastViewedCategory', 'Label' => 'Last viewed category', 'Group' => $allVisit, 'Type' => 'function' ,'ReturnValue' => 'bool','Default' => true , 'qtipCfg' => array('text' => 'The last category the visitor has viewed.'), 'Values' => array(array('Label' => 'Category Type:','Type' => 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type') , array('Label' => 'Category:' ,'Type' => 'lookup', 'Values' => $this->baseURL().'index.php?eID=t3p_conditional_content&pid=1&cmd=getCategory' , 'callback' => 'Category_CallBack'))) ,
                    //  'ViewedCategory' => array('key' => 'ViewedCategory', 'Label' => 'viewed category', 'Group' => $allVisit,'Type' => 'function' ,'ReturnValue' => 'bool' , 'Default' => true, 'qtipCfg' => array('text' => 'If the visitor has viewed a specific category across all his visits.'), 'Values' => array(array('Label' => 'Past n days:','Type' => 'integer','Name' => 'days', 'data' => '') , array('Label' => 'Category Type:','Type' => 'single', 'data' => '','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type') , array('Label' => 'Category:' ,'Type' => 'lookup', 'data' => '', 'Values' => $this->baseURL().'index.php?eID=t3p_conditional_content&pid=1&cmd=getCategory' , 'callback' => 'Category_CallBack'))) ,
                    //  'isTopCategory' => array('key' => 'isTopCategory', 'Label' => 'Highest score', 'Group' => $allVisit ,'Type' => 'function' ,'ReturnValue' => 'bool' , 'Default' => true, 'qtipCfg' => array('text' => 'Highest scoring product, category, subcategory or brand.'), 'Values' => array(array('Label' => 'Category Type:','Type' => 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type'))) ,

                    //  'topCategories' => array('key' => 'topCategories', 'Label' => 'Top categories', 'Group' => $allVisit ,'Type' => 'function' ,'ReturnValue' => 'string' , 'Default' => true, 'qtipCfg' => array('text' => 'Highest scoring product, category, subcategory or brand.'), 'Values' => array(array('Label' => 'Top-x:' , 'Type' => 'integer', 'data' => '') , array('Label' => 'Category Type:','Type' => 'single', 'data' => '','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type'))) ,

                    //  'isLowCategory' => array('key' => 'isLowCategory', 'Label' => 'Lowest score', 'Group' => $allVisit ,'Type' => 'function' ,'ReturnValue' => 'bool', 'Default' => true, 'qtipCfg' => array('text' => 'Lowest scoring product, category, subcategory or brand.'), 'Values' => array(array('Label' => 'Category Type:','Type' => 'single', 'data' => '','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type'))) ,
                    //  'isGrowthCategory' => array('key' => 'isGrowthCategory', 'Label' => 'Highest score growth', 'Group' => $allVisit ,'Type' => 'function' ,'ReturnValue' => 'bool', 'Default' => true, 'qtipCfg' => array('text' => 'Most growing score in product, category, subcategory or brand.'),'Values' => array(array('Label' => 'Category Type:','Type' => 'single', 'data' => '','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type'))) ,
                    //  'isDecreaseCategory' => array('key' => 'isDecreaseCategory', 'Label' => 'Highest score decrease', 'Group' => $allVisit ,'Type' => 'function' ,'ReturnValue' => 'bool', 'Default' => true, 'qtipCfg' => array('text' => 'Highest decreasing score in product, category, subcategory or brand.'),'Values' => array(array('Label' => 'Category Type:','Type' => 'single', 'data' => '','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type'))) ,
                    //  'lastViewedScore' => array('key' => 'lastViewedScore', 'Label' => 'Last viewed', 'Group' => $allVisit ,'Type' => 'function' ,'ReturnValue' => 'bool', 'Default' => true, 'qtipCfg' => array('text' => 'Category or product last viewed.'),'Values' => array(array('Label' => 'Category Type:','Type' => 'single', 'data' => '','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type'))) ,
                    //  'engagementTrend' => array('key' => 'engagementTrend', 'Label' => 'Engagement Trend', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The % change in engagement score in this visit compared to previous visit.')) ,
                    //  'lastBasketValue' => array('key' => 'lastBasketValue', 'Label' => 'Last Abandoned Basket value', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The value of products in the last abandoned shopping basket.')) ,
                    //  'lastBasketNoOfItems' => array('key' => 'lastBasketNoOfItems', 'Label' => 'Last Abandoned Basket size', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The number of items in the last abandoned shopping basket.')) ,
                    //  'daysSinceLastBasket ' => array('key' => 'daysSinceLastBasket', 'Label' => 'Days since abandonded basket', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The number of days since the last shopping basket was abandoned.')) ,
                    //  'totalViewedCategoryValueInDays' => array('key' => 'totalViewedCategoryValueInDays', 'Label' => 'Total Value Viewed', 'Group' => $allVisit , 'Type' => 'function' ,'ReturnValue' => 'integer', 'qtipCfg' => array('text' => 'The total value of all viewed products.'),'Values' => array(array('Label' => 'Category Type:','Type' => 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type')  , array('Label' => 'Days :','Type' => 'integer' ,'Name' => 'days'))) ,
                    //  'totalPurchaseValueInDays' => array('key' => 'totalPurchaseValueInDays', 'Label' => 'Total Purchase', 'Group' => $allVisit,'Type' => 'function' ,'ReturnValue' => 'integer'  , 'qtipCfg' => array('text' => 'The total value of all recorded purchases.') , 'Values' => array(array('Label' => 'Days:','Type' => 'integer' , 'Name' => 'days'))) ,
                    //  'lifetimePurchaseValue' => array('key' => 'lifetimePurchaseValue', 'Label' => 'Lifetime value', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The calculated customer lifetime value.')) ,
                    //  'totalPurchases' => array('key' => 'totalPurchases', 'Label' => 'Number of purchases', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The total number of purchases.')) ,
                    //  'avgPurchaseValue' => array('key' => 'avgPurchaseValue', 'Label' => 'Average transaction value', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The average transaction value.')) ,
                    //  'daysSinceLastPurchase' => array('key' => 'daysSinceLastPurchase', 'Label' => 'Days since last purchase', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'Days since the last purchase.')) ,
                    //  'purchaseRatio' => array('key' => 'purchaseRatio', 'Label' => 'Purchase ratio', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'Number of visits with purchase/total number of visits.')) ,
                    //  'downloadedFile' => array('key' => 'downloadedFile', 'Label' => 'Downloaded file', 'Group' => $allVisit ,'Type' => 'function' ,'ReturnValue' => 'bool', 'Default' => true, 'qtipCfg' => array('text' => 'Download of one or more specific files.'), 'Values' => array(array('Label' => 'filenames:','Type' => 'superboxselect', 'Values' => ''))) ,
                    //  'daysSinceLastDownload' => array('key' => 'daysSinceLastDownload', 'Label' => 'Days since Last download', 'Group' => $allVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'Days since download of one or more specific files.')) ,
                     'searchedFor' => array('key' => 'searchedFor', 'Label' => 'Search values', 'Group' => $allVisit ,'Type' => 'function' ,'ReturnValue' => 'bool', 'Default' => true, 'qtipCfg' => array('text' => 'Check on one or multiple search terms conducted on the local search engine.'), 'Values' => array()) ,

                     //Current visit;
                     'AverageTimeOnPage' => array('key' => 'AverageTimeOnPage', 'Label' => 'Time On Page (in Sec)', 'Group' => $currentVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The average number of seconds the visitor has spent on a page during this visit.')) ,
                     'EntryPage' => array('key' => 'EntryPage', 'Label' => 'Entry Page', 'Group' => $currentVisit,'Type' => 'string' , 'qtipCfg' => array('text' => 'The URL address of the first page in this visit.')) ,
                     'VisitDuration' => array('key' => 'VisitDuration', 'Label' => 'Length of visit', 'Group' => $currentVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The number of seconds of this visit.')) ,
                     'CurrentPageViewCount' => array('key' => 'CurrentPageViewCount', 'Label' => 'Total Pageviews', 'Group' => $currentVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'The total number of pages viewed during this visit.')) ,
                     'VisitPoints' => array('key' => 'VisitPoints', 'Label' => 'Points in this visit', 'Group' => $currentVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'TThe total number of points accumulated during this visit. Points are related to specific goals achieved.')) ,
                     'SocialMediaVisitor' => array('key' => 'SocialMediaVisitor', 'Label' => 'Social Media Visitor', 'Group' => $currentVisit,'Type' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Social Media visitor is true, if the visitor came from a known social network.')) ,
                     'FirstVisit' => array('key' => 'FirstVisit', 'Label' => 'First Visit', 'Group' => $currentVisit,'Type' => 'bool'  , 'Default' => true , 'qtipCfg' => array('text' => 'Is this visit the first recorded on this visitor.')) ,
                     'SearchVisitor' => array('key' => 'SearchVisitor', 'Label' => 'Search Visitor', 'Group' => $currentVisit,'Type' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Search Visitor is true, if the visitor came from a known search engine.')) ,
                     'DirectVisitor' => array('key' => 'DirectVisitor', 'Label' => 'Direct Visitor', 'Group' => $currentVisit,'Type' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Direct Visitor is true, if the visitor came from bookmark or entered the address in the browser directly..')) ,
                    //  'VisitSearchString' => array('key' => 'VisitSearchString', 'Label' => 'Visit searchstring', 'Group' => $currentVisit,'Type' => 'string' , 'qtipCfg' => array('text' => 'The search string that was captured by Monoloop if the visitor came from a search engine e.g. "maserati grand turismo"')) ,
                     'VisitReferer' => array('key' => 'VisitReferer', 'Label' => 'Visit referer', 'Group' => $currentVisit,'Type' => 'string'  , 'qtipCfg' => array('text' => 'The URL of the referring website e.g. http://www.facebook.com ')) ,
                    //  'PageReferer' => array('key' => 'PageReferer', 'Label' => 'Page referer', 'Group' => $currentVisit,'Type' => 'string'  , 'qtipCfg' => array('text' => 'The URL of the referring website e.g. http://www.facebook.com ')) ,
                     'CurrentURL' => array('key' => 'CurrentURL', 'Label' => 'CurrentURL', 'Group' => $currentVisit,'Type' => 'string'  , 'qtipCfg' => array('text' => 'The current URL the user is on e.g. http://www.myWebsite.com/page1.html')) ,
                    //  'currentIsTopCategory' => array('key' => 'currentIsTopCategory', 'Label' => 'Highest score', 'Group' => $currentVisit ,'Type' => 'function' ,'ReturnValue' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Highest scoring product, category, subcategory or brand.'), 'Values' => array(array('Label' => 'Category Type:','Type' => 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type')  , array('Label' => 'Values:','Type' => 'superboxselect', 'Values' => $this->baseURL().'index.php?eID=t3p_conditional_content&pid=1&cmd=getCategory'))) ,
                    //  'currentIsLowCategory' => array('key' => 'currentIsLowCategory', 'Label' => 'Lowest score', 'Group' => $currentVisit ,'Type' => 'function' ,'ReturnValue' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Lowest scoring product, category, subcategory or brand.'), 'Values' => array(array('Label' => 'Category Type:','Type' => 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type')  , array('Label' => 'Values:','Type' => 'superboxselect', 'Values' => $this->baseURL().'index.php?eID=t3p_conditional_content&pid=1&cmd=getCategory'))) ,
                     //'currentIsGrowthCategory' =>	   array( 'Label' => 'Highest Score Growth', 'Group'=>  $currentVisit ,'Type'=> 'function' ,'ReturnValue'=>'bool', 'qtipCfg' => array(  'text' => ''  ), 'Values' => array( array('Label'=>'Category Type:','Type'=> 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name'=>'category_type')  , array('Label'=>'Values:','Type'=> 'superboxselect', 'Values' => $this->baseURL() . 'index.php?eID=t3p_conditional_content&pid=1&cmd=getCategory' ))) ,
                     //'currentIsDecreaseCategory' =>	   array( 'Label' => 'Highest Score Decrease', 'Group'=>  $currentVisit ,'Type'=> 'function' ,'ReturnValue'=>'bool', 'qtipCfg' => array(  'text' => ''  ), 'Values' => array( array('Label'=>'Category Type:','Type'=> 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name'=>'category_type')  , array('Label'=>'Values:','Type'=> 'superboxselect', 'Values' => $this->baseURL() . 'index.php?eID=t3p_conditional_content&pid=1&cmd=getCategory' ))) ,
                    //  'currentLastViewedScore' => array('key' => 'currentLastViewedScore', 'Label' => 'Last viewed', 'Group' => $currentVisit ,'Type' => 'function' ,'ReturnValue' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Last viewed product, category, subcategory or brand.'), 'Values' => array(array('Label' => 'Category Type:','Type' => 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name' => 'category_type')  , array('Label' => 'Values:','Type' => 'superboxselect', 'Values' => $this->baseURL().'index.php?eID=t3p_conditional_content&pid=1&cmd=getCategory')))  ,
                    //  'currentBasketValue' => array('key' => 'currentBasketValue', 'Label' => 'Basket Value', 'Group' => $currentVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'Current shopping basket value.')) ,
                    //  'currentBasketAvgValue' => array('key' => 'currentBasketAvgValue', 'Label' => 'Basket Avg Value', 'Group' => $currentVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'Current shopping basket Avg value.')) ,
                    //  'currentBasketNoOfItems' => array('key' => 'currentBasketNoOfItems', 'Label' => 'Basket Items', 'Group' => $currentVisit,'Type' => 'integer' , 'qtipCfg' => array('text' => 'Current number of items in shopping basket.')) ,
                    //  'currentHasPurchaced' => array('key' => 'currentHasPurchaced', 'Label' => 'Has purchased', 'Group' => $currentVisit,'Type' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Has the visitor conducted a purchase in this session.')) ,
                    //  'currentDownloadedFile' => array('key' => 'currentDownloadedFile', 'Label' => 'Downloaded file', 'Group' => $currentVisit ,'Type' => 'function' ,'ReturnValue' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Has a file been downloaded in this session.'), 'Values' => array()) ,
                     'currentSearchedFor' => array('key' => 'currentSearchedFor', 'Label' => 'Search values', 'Group' => $currentVisit ,'Type' => 'function' ,'ReturnValue' => 'bool' , 'Default' => true , 'qtipCfg' => array('text' => 'Has a search for a specific search term been conducted in this session.'), 'Values' => array()) ,

                     //Location
                     'TimeZone' => array('key' => 'TimeZone', 'Label' => 'TimeZone',  'Group' => 'Location', 'Type' => 'single' , 'Values' => $this->getTimeZoneList() , 'qtipCfg' => array('text' => 'What timezone does the visit come from.')) ,
                     //'Continent' =>	   array( 'Label' => 'Continent', 'Group'=>  'Location','Type'=> 'single', 'Values' => $this->getContinentList() , 'qtipCfg' => array(  'text' => 'The continent the visit comes from. Remember that this value is GeoIP based so if a visitor is using a VPN connection it will not be accurate.'  )   ) ,
                     'Country' => array('key' => 'Country', 'Label' => 'Country', 'Group' => 'Location','Type' => 'single remote', 'qtipCfg' => array('text' => 'The country the visit comes from. Remember that this value is GeoIP based so if a visitor is using a VPN connection it will not be accurate.'), 'Values' => $this->baseURL().'api/countries') ,
                     'City' => array('key' => 'City', 'Label' => 'City', 'Group' => 'Location','Type' => 'single remote', 'qtipCfg' => array('text' => 'The city the visit comes from. Remember that this value is GeoIP based so it will not be accurate. The typical resolution of this information is +/- 50 km.') , 'Values' => $this->baseURL().'api/cities' , 'Default' => '') ,
                     //'ISP' =>	   array( 'Label' => 'ISP', 'Group'=>  'Location','Type'=> 'string', 'qtipCfg' => array(  'text' => 'The Internet Service Provider of the visitor.'  ) ) ,
                     'Longitude' => array('key' => 'Longitude', 'Label' => 'Longitude', 'Group' => 'Location','Type' => 'integer', 'qtipCfg' => array('text' => 'The longitude related to the visitors position. The typical resolution of this information is +/- 50 km.')) ,
                     'Latitude' => array('key' => 'Latitude', 'Label' => 'Latitude', 'Group' => 'Location','Type' => 'integer', 'qtipCfg' => array('text' => 'The latitude related to the visitors position. The typical resolution of this information is +/- 50 km.')) ,
                     'isWithinArea' => array('key' => 'isWithinArea', 'Label' => 'Within Area', 'Group' => 'Location','Type' => 'function' , 'Default' => true ,'ReturnValue' => 'bool'   , 'qtipCfg' => array('text' => 'Location within a radius of a specific longitude/latitude coordinate.') , 'Values' => array(array('Label' => 'Longtitude:','Type' => 'integer' , 'Name' => 'longitude', 'data' => '') , array('Label' => 'Latitude:' , 'Type' => 'integer' , 'Name' => 'latitude', 'data' => '') , array('Label' => 'Radius:' , 'Type' => 'integer' , 'Name' => 'radius', 'data' => '') , array('Label' => 'Miles:' , 'Type' => 'bool' , 'Name' => 'miles', 'data' => true , 'Default' => true))) ,
                     //Functions
                     // Score
                     /*
                     'EngagementScore' =>	   array( 'Label' => 'Engagement Score (in %)', 'Group'=> $engagement,'Type'=> 'function' , 'qtipCfg' => array(  'text' => 'Engagement score is a number between 1-99 expressing the visitors engagement. You can choose to look at the visitors engagement score across different timespans. Read more on  <br> <a href="http://www.monoloop.com/product/features/engagement.html" target="_blank">http://www.monoloop.com/product/features/engagement.html</a>'  ),'ReturnValue'=>'integer', 'Values' => array(array('Label'=>'Past n days:','Type'=>'integer','Name'=>'days'),array('Label'=>'Minimum pageviews in visit:','Type'=>'integer','Name'=>'pageviews'  , 'Default' => $defaulData['scores_pageviews']),array('Label'=>'Minimum length of visit in minutes:','Type'=>'integer','Name'=>'minutes' , 'Default' => $defaulData['scores_minutes']),array('Label'=>'Minimum number of visits:','Type'=>'integer','Name'=>'visits' , 'Default' => $defaulData['scores_visits']),array('Label'=>'Brands:','Type'=>'string','Name'=>'brands' , 'Default' => $defaulData['scores_brands']))   ) ,
                     'ClickDepthIndex' =>	   array( 'Label' => 'Click Depth Score (in %)', 'Group'=>  $engagement,'Type'=> 'function' , 'qtipCfg' => array(  'text' => 'Click Depth Score is the percent of sessions having more than "n" page views divided by all sessions. The value "n" is set in the Scores tab under Account. Read more on <br> <a href="http://www.monoloop.com/product/features/engagement.html" target="_blank">http://www.monoloop.com/product/features/engagement.html</a>')  ,'ReturnValue'=>'integer', 'Values' => array( array('Label'=>'Minimum pageviews in visit:','Type'=>'integer','Name'=>'pageviews' , 'Default' => $defaulData['scores_pageviews']))  ),
                     'RecencyIndex' =>	   array( 'Label' => 'Recency Score (in %)', 'Group'=> $engagement,'Type'=> 'function' , 'qtipCfg' => array(  'text' => 'Recency Score is the percent of sessions having more than "n" page views that occurred in the past "n" weeks divided by all sessions. The Recency Index captures recent sessions that were also deep enough to be measured in the Click-Depth Index. The value "n" is set in the Scores tab under Account. Read more on <br> <a href="http://www.monoloop.com/product/features/engagement.html" target="_blank">http://www.monoloop.com/product/features/engagement.html</a>')  ,'ReturnValue'=>'integer', 'Values' => array( array('Label'=>'Minimum pageviews in visit:','Type'=>'integer','Name'=>'pageviews' , 'Default' => $defaulData['scores_pageviews']),array('Label'=>'Past n days:','Type'=>'integer','Name'=>'days'))   ) ,
                     'DurationIndex' =>	   array( 'Label' => 'Duration Score (in %)', 'Group'=> $engagement,'Type'=> 'function', 'qtipCfg' => array(  'text' => 'Duration Score is the percent of sessions longer than "n" minutes divided by all sessions. The value "n" is set in the Scores tab under Account. Read more on <br> <a href="http://www.monoloop.com/product/features/engagement.html" target="_blank">http://www.monoloop.com/product/features/engagement.html</a>')  ,'ReturnValue'=>'integer', 'Values' => array( array('Label'=>'Minimum length of visit in minutes:','Type'=>'integer','Name'=>'minutes' , 'Default' => $defaulData['scores_minutes']))   ) ,
                     'BrandIndex' =>	   array( 'Label' => 'Brand Score (in %)', 'Group'=> $engagement,'Type'=> 'function' ,'ReturnValue'=>'integer', 'qtipCfg' => array(  'text' => 'Brand Score is the percent of sessions that either begin directly (i.e., have no referring URL) or are initiated by an external search for a "branded" term divided by all sessions. Branded terms are entered in the Scores tab under Account. Read more on <br> <a href="http://www.monoloop.com/product/features/engagement.html" target="_blank">http://www.monoloop.com/product/features/engagement.html</a>') , 'Values' => array( array('Label'=>'Brandterms (seperate with comma):','Type'=>'string','Name'=>'brands' , 'Default' => $defaulData['scores_brands']))   ) ,
#					 'FeedbackIndex' =>	   array( 'Label' => 'Feedback Score (in %)', 'Group'=>  'Scores','Type'=> 'integer' ) ,
#					 'InteractionIndex' =>	   array( 'Label' => 'Interaction Score (in %)', 'Group'=>  'Scores','Type'=> 'integer' ) ,#
#					 'LoyaltyIndex' =>	   array( 'Label' => 'Loyal Visitor', 'Group'=> $engagement,'Type'=> 'function', 'qtipCfg' => array(  'text' => 'Loyalty Score is scored as "1" if the visitor has come to the site more than "n" times during the time-frame under examination (and otherwise scored "0") Read more on <br> <a href="http://www.monoloop.com/product/features/engagement.html" target="_blank">http://www.monoloop.com/product/features/engagement.html</a>'),'ReturnValue'=>'bool' , 'Default' => true , 'Values' => array(array('Label'=>'Minimum number of visits:','Type'=>'integer','Name'=>'visits' , 'Default' => $defaulData['scores_visits']),array('Label'=>'Past n days:','Type'=>'integer','Name'=>'days'))   ) ,
#					 'SubscriptionIndex' =>	   array( 'Label' => 'Subscription Visitor', 'Group'=>  'Scores','Type'=> 'function' ,'ReturnValue'=>'bool', 'Values' => array(array('Label'=>'Days to go back:','Type'=>'integer','Name'=>'days'))   ) ,
                     'CategoryIndex' =>	   array( 'Label' => 'Category Score (in %)', 'Group'=> $engagement,'Type'=> 'function', 'qtipCfg' => array(  'text' => 'Engagement score  is the percent of sessions where the visitor gave direct feedback via a Voice of Customer technology (like a form, poll, signup) divided by all sessions (see additional explanation below) Read more on <br> <a href="http://www.monoloop.com/product/features/engagement.html" target="_blank">http://www.monoloop.com/product/features/engagement.html</a>') ,'ReturnValue'=>'integer', 'Values' => array( array('Label'=>'Past n days:','Type'=>'integer','Name'=>'days') , array('Label'=>'Category Type:','Type'=> 'single','Values' => $this->getCategoriesList($defaulData['category']),'Name'=>'category_type') , array('Label'=>'Category:' ,'Type'=>'lookup', 'Values' => $this->baseURL() . 'index.php?eID=t3p_conditional_content&pid=1&cmd=getCategory' , 'callback' => 'Category_CallBack' ) )   ) , */

                    //Goals:
                    //  'GoalReached' => array('key' => 'GoalReached', 'Label' => 'Goal reached', 'Group' => 'Goals','Type' => 'function' ,'ReturnValue' => 'bool' , 'Default' => true ,'qtipCfg' => array('text' => 'Choose between defined goals. The value can only be true or false.') , 'Values' => array(array('Label' => 'Goal:','Type' => 'single', 'data' => '', 'Values' => [] ))) ,
                     //Tracker:
                     //'cv' =>	   array( 'Label' => 'Custom field', 'Group'=>  'Trackers','Type'=> 'function' ,'ReturnValue'=>'integer'   ,'qtipCfg' => array(  'text' => ''  ) , 'Values' => array(array('Label'=>'Field:','Type'=>'single remote2', 'Values' => $this->baseURL() . 'index.php?eID=t3p_conditional_content&pid=1&cmd=getFieldList' , 'callback' => 'CV_CallBack' )   )) ,
                     // USer seting
                     'OS' => array('key' => 'OS', 'Label' => 'Operating System', 'Group' => 'User Settings','Type' => 'single' , 'Values' => $this->getOSList()  ,'qtipCfg' => array('text' => 'What operating system does the browser report, that the visitor is using. Choose from the list.')) ,
                     'Resolution' => array('key' => 'Resolution', 'Label' => 'Resolution', 'Group' => 'User Settings','Type' => 'string' ,'qtipCfg' => array('text' => 'What screen resolution does the browser report, that the visitor is using.')) ,
                     'Browser' => array('key' => 'Browser', 'Label' => 'Browser', 'Group' => 'User Settings','Type' => 'single', 'Values' => $this->getBrowserList() ,'qtipCfg' => array('text' => 'What browser family does the browser report, that the visitor is using. Choose from the list.')) ,
                     'BrowserVersion' => array('key' => 'BrowserVersion', 'Label' => 'Browser Version', 'Group' => 'User Settings','Type' => 'integer'  ,'qtipCfg' => array('text' => 'What browser version does the browser report, that the visitor is using.')) ,
                     'BrowserLanguage' => array('key' => 'BrowserLanguage', 'Label' => 'Browser Language', 'Group' => 'User Settings','Type' => 'single', 'Values' => $this->getLanguageList() ,'qtipCfg' => array('text' => 'What language does the browser report, that the visitor is using. Choose from the list.')) ,
                     'Javascript' => array('key' => 'Javascript', 'Label' => 'Javascript Enabled', 'Group' => 'User Settings','Type' => 'bool' , 'Default' => true ,'qtipCfg' => array('text' => 'Is Javascript enabled in the visitor\'s browser. True or false.')) ,
                     'Flash' => array('key' => 'Flash', 'Label' => 'Flash Enabled', 'Group' => 'User Settings','Type' => 'bool' , 'Default' => true,'qtipCfg' => array('text' => 'Is Flash enabled in the visitor\'s browser. True or false.')) ,
                     'Quicktime' => array('key' => 'Quicktime', 'Label' => 'Quicktime Enabled', 'Group' => 'User Settings','Type' => 'bool' , 'Default' => true,'qtipCfg' => array('text' => 'Is Quicktime enabled in the visitor\'s browser. True or false.')) ,
                     'Wma' => array('key' => 'Wma', 'Label' => 'Windows Media Enabled', 'Group' => 'User Settings','Type' => 'bool' , 'Default' => true,'qtipCfg' => array('text' => 'Is Windows Media enabled in the visitor\'s browser. Truel or false.')) ,
                     'Silverlight' => array('key' => 'Silverlight', 'Label' => 'Silverlight Enabled', 'Group' => 'User Settings','Type' => 'bool' , 'Default' => true ,'qtipCfg' => array('text' => 'Is Silverlight enabled in the visitor\'s browser. True or false.')) ,
                     'PDF' => array('key' => 'PDF', 'Label' => 'PDF Enabled', 'Group' => 'User Settings','Type' => 'bool' , 'Default' => true ,'qtipCfg' => array('text' => 'Is PDF enabled in the visitor\'s browser. True or false.')) ,
                     'Realplayer' => array('key' => 'Realplayer', 'Label' => 'Realplayer Enabled', 'Group' => 'User Settings','Type' => 'bool' , 'Default' => true ,'qtipCfg' => array('text' => 'Is Realplayer enabled in the visitor\'s browser. True or false.')) ,
                     'Gears' => array('key' => 'Gears', 'Label' => 'Gears Enabled', 'Group' => 'User Settings','Type' => 'bool' , 'Default' => true ,'qtipCfg' => array('text' => 'Is Google Gears enabled in the visitor\'s browser. True or false.')) ,
/*
                     //Category group:
                     'ViewedCategories' =>	   array( 'Label' => 'Viewed Categories:', 'Group'=>  'Categories','Type'=> 'function','ReturnValue'=>'bool','Values' => array(array('Label'=>'Category:','Type'=>'string','Name'=>'category'),array('Label'=>'Last days:','Type'=>'integer','Name'=>'days'))   ) ,
                     'TopCategories' =>	   array( 'Label' => 'Top Categories', 'Group'=>  'Categories','Type'=> 'function' ,'ReturnValue'=>'bool', 'Values' => array( array('Label'=>'Category:','Type'=>'string','Name'=>'category'),array('Label'=>'Top:','Type'=>'integer','Name'=>'top'),array('Label'=>'Last days:','Type'=>'integer','Name'=>'days'))   ) ,
                     'LastViewedCategory' =>	   array( 'Label' => 'Last Viewed Category:', 'Group'=>  'Categories','Type'=> 'single','Values' => $this->getCategoriesList()   ) ,
*/
                     'PageviewsQA' => array('key' => 'PageviewsQA', 'Label' => 'Pageviews in current visit' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                     'VisitsQA' => array('key' => 'VisitsQA', 'Label' => 'Visits'  , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'CategoriesQA' => array('key' => 'CategoriesQA', 'Label' => 'Categories in visit'  , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                     'UniqueCategoriesQA' => array('key' => 'UniqueCategoriesQA', 'Label' => 'Unique categories in visit', 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'ProductsQA' => array('key' => 'ProductsQA', 'Label' => 'Products in visit', 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'UniqueProductsQA' => array('key' => 'UniqueProductsQA', 'Label' => 'Unique products in visit' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                     'TimeOnPageQA' => array('key' => 'TimeOnPageQA', 'Label' => 'Time on page in visit' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                     'VisitDurationQA' => array('key' => 'VisitDurationQA', 'Label' => 'Length of visit' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'UniqueProductPrCateoryQA' => array('key' => 'UniqueProductPrCateoryQA', 'Label' => 'Unique products per category in visit', 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'UniqueCateoryRecencyQA' => array('key' => 'UniqueCateoryRecencyQA', 'Label' => 'Unique category recency across visits' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'UniqueCateoryRecencyCurrentQA' => array('key' => 'UniqueCateoryRecencyCurrentQA', 'Label' => 'Unique category recency in visit', 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'UniqueProductRecencyQA' => array('key' => 'UniqueProductRecencyQA', 'Label' => 'Unique product recency across visits' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'UniqueProductRecencyCurrentQA' => array('key' => 'UniqueProductRecencyCurrentQA', 'Label' => 'Unique product recency in visit' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                     'ProductRecencyQA' => array('key' => 'ProductRecencyQA', 'Label' => 'Product recency across visits', 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'ProductRecencyQA' => array('key' => 'ProductRecencyQA', 'Label' => 'Product recency in visit' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'CateoryRecencyQA' => array('key' => 'CateoryRecencyQA', 'Label' => 'Category recency across visits', 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))) ,

                    //  'CateoryRecencyCurrentQA' => array('key' => 'CateoryRecencyCurrentQA', 'Label' => 'Category recency in visit' , 'HideCompare' => true , 'Default' => true ,  'Group' => $qa ,'Type' => 'function','ReturnValue' => 'bool','Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'Values' => $qaList))),

                     );

        $out['timeAfterToday'] = array('key' => 'timeAfterToday', 'Label' => 'Time after today' , 'Group' => 'Time' , 'Type' => 'function' , 'ReturnValue' => 'integer' , 'Values' => array(array('Label' => '','Type' => 'single','Name' => '', 'data' => 'Seconds' , 'Values' => $timeUnitList)));
        $out['timeBeforeToday'] = array('key' => 'timeBeforeToday', 'Label' => 'Time before today' , 'Group' => 'Time' , 'Type' => 'function' , 'ReturnValue' => 'integer' , 'Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'data' => 'Seconds', 'Values' => $timeUnitList)));

        $out['timeBeforeDate'] = array('key' => 'timeBeforeDate', 'Label' => 'Time before date' , 'Group' => 'Time' , 'Type' => 'function' , 'ReturnValue' => 'integer' , 'Values' => array(array('Label' => '','Type' => 'single','Name' => '', 'data' => 'Seconds' , 'Values' => $timeUnitList)));
        $out['timeAfterDate'] = array('key' => 'timeAfterDate', 'Label' => 'Time after date' , 'Group' => 'Time' , 'Type' => 'function' , 'ReturnValue' => 'integer' , 'Values' => array(array('Label' => '','Type' => 'single','Name' => '' , 'data' => 'Seconds', 'Values' => $timeUnitList) ));

        // $out['compareCV'] = array('key' => 'compareCV', 'Label' => 'In Array' , 'Group' => 'Custom Fields' , 'Type' => 'function' , 'ReturnValue' => 'bool' , 'Values' => array());
        // Get custom field property .
        // if (class_exists('t3lib_extmgm', false)) {
        //     $file = t3lib_extmgm::extPath('t3p_listmanagement').'lib/class.tx_t3p_listmanagement_core.php';
        //     if (!file_exists($file)) {
        //         return;
        //     }
        //     require_once $file;
        //     $core = tx_t3p_listmanagement_core::getObjectSingleton();
        //     $uTypeObj = $core->GetModel('unique_type');
        //     $lists = $uTypeObj->GetAllUniqueType();
        //
        //     if (!empty($lists)) {
        //         foreach ($lists as $list) {
        //             $addData = json_decode($list['addition_data'], true);
        //             if (isset($addData['activeSaveFunction']) && $addData['activeSaveFunction'] == 4) {
        //                 continue;
        //             }
        //             $out[  'cv['.$list['idunique_type'].']' ] = array('Label' => $list['label'] , 'Group' => 'Custom Fields' , 'Type' => $list['datatype']);
        //         }
        //     }
        // } else {
        //     $whereCustomer = ' AND '.base::getCustomerFilterSQL('CU');
        //     $res = mysql_query(
        //             'SELECT UT.* FROM  tx_monoloop_customvars CU LEFT JOIN tx_t3p_listmanagement_unique_type UT ON CU.listfield_id =  UT.idunique_type'.
        //             ' WHERE UT.isMonoloop =  0 '.$whereCustomer.' AND CU.hideen = 0 ORDER BY UT.label ');
        //     if ($res) {
        //         while ($row = mysql_fetch_assoc($res)) {
        //             $out[  'cv['.$row['idunique_type'].']' ] = array('Label' => $row['label'] , 'Group' => 'Custom Fields' , 'Type' => $row['datatype']);
        //         }
        //     }
        // }

        return $out;
    }
    public function baseURL()
    {
        // $url = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
        // $url .= '://'.$_SERVER['HTTP_HOST'];
        // $url .= preg_replace('@/+$@', '', dirname($_SERVER['SCRIPT_NAME']));
        // $url = str_replace('\\', '', $url);
        // $url .=  '/';

        $url = URL::to('/');

        return $url.'/';
    }

    public function get($propertyname)
    {
        return $this->__get($propertyname);
    }

    public function __get($propertyname)
    {
        // if ($this->TestMode) {
        //     if (!property_exists($this, $propertyname)) {
        //         return;
        //     }
        //
        //     return $this->$propertyname;
        // }

        switch ($propertyname) {
            case 'PageViewCount':
                return $this->getPageViewCount();
                break;
            case 'CurrentPageViewCount':
                return $this->getCurrentPageViewCount();
                break;
            case 'DaysSinceLastVisit':
                return $this->getDaysSinceLastVisit();
                break;
            case 'VisitReferer':
                return $this->getReferer();
                break;
            case 'FeedbackIndex':
                //Calculate brandindex and return
                return $this->getFeedbackIndex();
                break;
            case 'InteractionIndex':
                return $this->getInteractionIndex();
                break;
            case 'AverageTimeOnPage':
                return $this->getAverageTimeOnPage();
                break;
            case 'EntryPage':
                return $this->getEntryPage();
                break;
            case 'VisitDuration':
                return $this->getVisitDuration();
                break;
            case 'VisitPoints':
                return $this->getVisitPoints();
                break;
            case 'SocialMediaVisitor':
                return $this->isSocialMediaVisitor();
                break;
            case 'FirstVisit':
                return $this->isFirstVisit();
                break;
            case 'SearchVisitor':
                return $this->isSearchVisitor();
                break;
            case 'DirectVisitor':
                return $this->isDirectVisitor();
                break;
            case 'VisitSearchString':
                return $this->getVisitSearchString();
                break;
            case 'TotalPoints':
                return $this->getTotalPoints();
                break;
            case 'LastExitPage':
                return $this->getLastExitPage();
                break;
            case 'VisitCount':
                return $this->getVisitCount();
                break;
            default:
                if (!property_exists($this, $propertyname)) {
                    return;
                }

                return $this->$propertyname;
                break;
        }
    }

    public function __call($func, $args)
    {
        if ($this->TestMode) {
            //Return the value from $this->MonoloopTestArray
            //TODO: Need to do some rewriting so eval() is not needed!!!! This could be hacked


            eval('$out = (isset($this->MonoloopTestArray[\''.$func.'\'][\''.implode("']['", $args).'\']))?$this->MonoloopTestArray[\''.$func.'\'][\''.implode("']['", $args).'\']:null;');

            return $out;
        } else {
            //Call real function

            if (method_exists($this, $func)) {
                return call_user_func_array(array($this, $func), $args);
            } else {
                return;
            }
        }
    }

    /*******************************************
     *
     * Private functions to calculate properties:
     *
     *******************************************/
    private function GoalReached($goalid)
    {
        //TODO: Chcek if goal is in profile:
        return true;
    }

    private function EngagementScore($days, $visits, $brands, $minutes, $pageviews)
    {
        $ES = round(((($this->SubscriptionIndex($days) * 100) +
              ($this->LoyaltyIndex($visits, $days) * 100) +
              $this->BrandIndex($brands) +
              $this->DurationIndex($minutes) +
              $this->RecencyIndex($pageviews, $days) +
              $this->ClickDepthIndex($pageviews) +
              $this->getFeedbackIndex() +
              $this->getInteractionIndex()) / 8), 2);

        return $ES;
    }

    private function CategoryIndex($category, $days)
    {
        //TODO: Make real function
        return 80;
    }

    private function ViewedCategory($days)
    {
    }

    private function SubscriptionIndex($days)
    {
        $SubscriptionIndex = (count($this->Monetary) > 1) ? 1 : 0;

        return $SubscriptionIndex;
    }

    private function LoyaltyIndex($visits, $days)
    {
        $$loyaltycounter = 0;
        foreach ($this->Visits as $visit) {
            if (($visit['firstclick'] > (time() - ($days * 24 * 60 * 60)))) {
                ++$loyaltycounter;
            }
        }
        $LoyaltyIndex = ($loyaltycounter >= $visits) ? 1 : 0;

        return $LoyaltyIndex;
    }

    private function BrandIndex($brands)
    {
        $arr_brands = explode(',', $brands);
        $brandcounter = 0;
        foreach ($this->Visits as $visit) {
            if ($visit['referer'] == '' or $this->isBrandReferer($visit['referer'], $arr_brands)) {
                ++$brandcounter;
            }
        }
        $BrandIndex = round(($brandcounter / count($this->Visits)), 2);

        return $BrandIndex * 100;
    }

    private function DurationIndex($minutes)
    {
        $duration_over = 0;
        foreach ($this->Visits as $visit) {
            if (($visit['lastclick'] - $visit['firstclick']) / 60 > ($minutes)) {
                ++$duration_over;
            }
        }

        $DurationIndex = round(($duration_over / count($this->Visits)), 2);

        return $DurationIndex * 100;
    }

    private function RecencyIndex($pageviews, $days)
    {
        $visitCounter = 0;
        foreach ($this->Visits as $visit) {
            if (($visit['clicks'] > $pageviews) && ($visit['firstclick'] > (time() - ($days * 24 * 60 * 60)))) {
                ++$visitCounter;
            }
        }
        $RecencyIndex = round(($visitCounter / count($this->Visits)), 2);

        return $RecencyIndex * 100;
    }

    private function ClickDepthIndex($pageviews)
    {
        $visitCounter = 0;
        foreach ($this->Visits as $visit) {
            if ($visit['clicks'] > $pageviews) {
                ++$visitCounter;
            }
        }
        $ClickDepthIndex = round(($visitCounter / count($this->Visits)), 2);

        return $ClickDepthIndex * 100;
    }

    /*******************************************
     *
     * Helper functions:
     *
     *******************************************/

    private function getPageViewCount()
    {
        $out = 0;
        if (is_array($this->Visits)) {
            foreach ($this->Visits as $visit) {
                $out += $visit['clicks'];
            }

            return $out;
        }
    }

    private function getCurrentPageViewCount()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);

            return $visit['clicks'];
        }
    }

    private function getDaysSinceLastVisit()
    {
        //TODO; getDaysSinceLastVisit()
    }

    private function getReferer()
    {
        //TODO: GetReferer
    }

    private function getAverageTimeOnPage()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);
            if ($visit['clicks'] > 1) {
                return round(($visit['lastclick'] - $visit['firstclick']) / ($visit['clicks'] - 1));
            } else {
                return $visit['lastclick'] - $visit['firstclick'];
            }
        }
    }

    private function getVisitCount()
    {
        //print_r($this->Visits);
        return $this->VisitCount;
    }

    private function getLastExitPage()
    {
        if (is_array($this->Visits)) {
            $visit = $this->Visits[count($this->Visits) - 2];

            return $visit['exitpage'];
        }
    }

    private function getTotalPoints()
    {
        $TotalPoints = 0;
        if (is_array($this->Visits)) {
            foreach ($this->Visits as $visit) {
                if ($visit['points'] > 0) {
                    $TotalPoints += $visit['points'];
                }
            }
        }

        return $TotalPoints;
    }

    private function getVisitSearchString()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);
            if ($visit['referer'] != '') {
                $parts_url = parse_url($visit['referer']);
                $query = isset($parts_url['query']) ? $parts_url['query'] : (isset($parts_url['fragment']) ? $parts_url['fragment'] : '');

                if (!$query) {
                    return '';
                }
                parse_str($query, $parts_query);
                foreach ($this->search_strings as $search_string) {
                    if (array_key_exists($search_string, $parts_query)) {
                        return $parts_query[$search_string];
                    }
                }
            };
        }

        return '';
    }

    private function isBrandReferer($ref, $brands)
    {
        $parts_url = parse_url($ref);
        $query = isset($parts_url['query']) ? $parts_url['query'] : (isset($parts_url['fragment']) ? $parts_url['fragment'] : '');

        if (!$query) {
            return '';
        }
        parse_str($query, $parts_query);
        foreach ($brands as $brand) {
            if (in_array($brand, $parts_query)) {
                return true;
            }
        }

        return false;
    }

    private function isDirectVisitor()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);
            if ($visit['referer'] == '') {
                return true;
            };
        }

        return false;
    }

    private function isFirstVisit()
    {
        if (is_array($this->Visits)) {
            if (count($this->Visits) > 1) {
                return false;
            }
        }

        return true;
    }

    private function isSearchVisitor()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);
            $ref = $visit['referer'];

            $parts = parse_url($ref);
            $ref = $parts['host'];

            foreach ($this->searh_referers as $searh_referers) {
                $searh_referers = "/$searh_referers/i";
                if (preg_match($searh_referers, $ref)) {
                    return true;
                }
            }

            return false;
        }
    }

    private function isSocialMediaVisitor()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);
            $ref = $visit['referer'];

            $parts = parse_url($ref);
            $ref = $parts['host'];

            foreach ($this->social_referers as $social_ref) {
                $social_ref = "/$social_ref/i";
                if (preg_match($social_ref, $ref)) {
                    return true;
                }
            }

            return false;
        }
    }

    private function getVisitPoints()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);

            return $visit['points'];
        }
    }

    private function getVisitDuration()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);

            return ($visit['lastclick'] - $visit['firstclick']);
        }
    }

    private function getEntryPage()
    {
        if (is_array($this->Visits)) {
            $visit = end($this->Visits);

            return $visit['entrypage'];
        }
    }

    private function getFeedbackIndex()
    {
        $visitCounter = 0;
        foreach ($this->Visits as $visit) {
            if ($visit['feedback'] == 1) {
                ++$visitCounter;
            }
        }
        $FeedbackIndex = round(($visitCounter / count($this->Visits)), 2);

        return $FeedbackIndex;
    }

    private function getInteractionIndex()
    {
        $visitCounter = 0;
        foreach ($this->Visits as $visit) {
            if ($visit['achivedGoal'] == 1) {
                ++$visitCounter;
            }
        }
        $InteractionIndex = round(($visitCounter / count($this->Visits)), 2);

        return $InteractionIndex;
    }

    private function getGoalList()
    {
        //TODO: Get dyncamic list of goals from DB
        return array(
                    'Goal 1' => 1,
                    'Goal 2' => 2,
                    'Goal 3' => 3,
                    'Goal 4' => 4,
                    );
    }

    private function getCategoriesList($cate)
    {
        //TODO: Fix dynamic list - get from Mongo
        //N. i load from customer config xml
        $ret = array();

        // test

        if (empty($cate)) {
            $cate = array(
                      array('Label' => 'Category 1', 'Value' => 34),
                      array('Label' => 'Category 2', 'Value' => 40),
                      array('Label' => 'Category 3', 'Value' => 54),
                      array('Label' => 'Category 4', 'Value' => 64),
                      array('Label' => 'Category 5', 'Value' => 78),
                      array('Label' => 'Category 6', 'Value' => 99),
                    );
        }
        $ret = $cate;
        // foreach ($cate as $row) {
        //     $ret[$row['name']] = $row['uid'];
        // }

        return $ret;
    }


    private function getLanguageList()
    {
        return array(
                  array('value' => 'aa', 'label' => 'Afar'),
                  array('value' => 'ab', 'label' => 'Abkhazian'),
                  array('value' => 'af', 'label' => 'Afrikaans'),
                  array('value' => 'am', 'label' => 'Amharic'),
                  array('value' => 'ar', 'label' => 'Arabic'),
                  array('value' => 'as', 'label' => 'Assamese'),
                  array('value' => 'ay', 'label' => 'Aymara'),

                // 'Azerbaijani' => 'az',
                // 'Bashkir' => 'ba',
                // 'Byelorussian' => 'be',
                // 'Bulgarian' => 'bg',
                // 'Bihari' => 'bh',
                // 'Bislama' => 'bi',
                // 'Bengali' => 'bn',
                // 'Tibetan' => 'bo',
                // 'Breton' => 'br',
                // 'Catalan' => 'ca',
                // 'Corsican' => 'co',
                // 'Czech' => 'cs',
                // 'Welsh' => 'cy',
                // 'Danish' => 'da',
                // 'German' => 'de',
                // 'Bhutani' => 'dz',
                // 'Greek' => 'el',
                // 'English' => 'en',
                // 'Esperanto' => 'eo',
                // 'Spanish' => 'es',
                // 'Estonian' => 'et',
                // 'Basque' => 'eu',
                // 'Persian' => 'fa',
                // 'Finnish' => 'fi',
                // 'Fiji' => 'fj',
                // 'Faroese' => 'fo',
                // 'French' => 'fr',
                // 'Frisian' => 'fy',
                // 'Irish' => 'ga',
                // 'Scots' => 'gd',
                // 'Galician' => 'gl',
                // 'Guarani' => 'gn',
                // 'Gujarati' => 'gu',
                // 'Hausa' => 'ha',
                // 'Hebrew' => 'he',
                // 'Hindi' => 'hi',
                // 'Croatian' => 'hr',
                // 'Hungarian' => 'hu',
                // 'Armenian' => 'hy',
                // 'Interlingua' => 'ia',
                // 'Indonesian' => 'id',
                // 'Interlingue' => 'ie',
                // 'Inupiak' => 'ik',
                // 'Icelandic' => 'is',
                // 'Italian' => 'it',
                // 'Inuktitut' => 'iu',
                // 'Japanese' => 'ja',
                // 'Javanese' => 'jw',
                // 'Georgian' => 'ka',
                // 'Kazakh' => 'kk',
                // 'Greenlandic' => 'kl',
                // 'Cambodian' => 'km',
                // 'Kannada' => 'kn',
                // 'Korean' => 'ko',
                // 'Kashmiri' => 'ks',
                // 'Kurdish' => 'ku',
                // 'Kirghiz' => 'ky',
                // 'Latin' => 'la',
                // 'Lingala' => 'ln',
                // 'Laothian' => 'lo',
                // 'Lithuanian' => 'lt',
                // 'Latvian' => 'lv',
                // 'Malagasy' => 'mg',
                // 'Maori' => 'mi',
                // 'Macedonian' => 'mk',
                // 'Malayalam' => 'ml',
                // 'Mongolian' => 'mn',
                // 'Moldavian' => 'mo',
                // 'Marathi' => 'mr',
                // 'Malay' => 'ms',
                // 'Maltese' => 'mt',
                // 'Burmese' => 'my',
                // 'Nauru' => 'na',
                // 'Nepali' => 'ne',
                // 'Dutch' => 'nl',
                // 'Norwegian' => 'no',
                // 'Occitan' => 'oc',
                // '(Afan)' => 'om',
                // 'Oriya' => 'or',
                // 'Punjabi' => 'pa',
                // 'Polish' => 'pl',
                // 'Pashto' => 'ps',
                // 'Portuguese' => 'pt',
                // 'Quechua' => 'qu',
                // 'Rhaeto-Romance' => 'rm',
                // 'Kirundi' => 'rn',
                // 'Romanian' => 'ro',
                // 'Russian' => 'ru',
                // 'Kinyarwanda' => 'rw',
                // 'Sanskrit' => 'sa',
                // 'Sindhi' => 'sd',
                // 'Sangho' => 'sg',
                // 'Serbo-Croatian' => 'sh',
                // 'Sinhalese' => 'si',
                // 'Slovak' => 'sk',
                // 'Slovenian' => 'sl',
                // 'Samoan' => 'sm',
                // 'Shona' => 'sn',
                // 'Somali' => 'so',
                // 'Albanian' => 'sq',
                // 'Serbian' => 'sr',
                // 'Siswati' => 'ss',
                // 'Sesotho' => 'st',
                // 'Sundanese' => 'su',
                // 'Swedish' => 'sv',
                // 'Swahili' => 'sw',
                // 'Tamil' => 'ta',
                // 'Telugu' => 'te',
                // 'Tajik' => 'tg',
                // 'Thai' => 'th',
                // 'Tigrinya' => 'ti',
                // 'Turkmen' => 'tk',
                // 'Tagalog' => 'tl',
                // 'Setswana' => 'tn',
                // 'Tonga' => 'to',
                // 'Turkish' => 'tr',
                // 'Tsonga' => 'ts',
                // 'Tatar' => 'tt',
                // 'Twi' => 'tw',
                // 'Uighur' => 'ug',
                // 'Ukrainian' => 'uk',
                // 'Urdu' => 'ur',
                // 'Uzbek' => 'uz',
                // 'Vietnamese' => 'vi',
                // 'Volapuk' => 'vo',
                // 'Wolof' => 'wo',
                // 'Xhosa' => 'xh',
                // 'Yiddish' => 'yi',
                // 'Yoruba' => 'yo',
                // 'Zhuang' => 'za',
                // 'Chinese' => 'zh',
                // 'Zulu' => 'zu',
                    );
    }

    private function getBrowserList()
    {
        return array(
                        array('value' => 'IE', 'label' => 'Internet Explorer'),
                        array('value' => 'Firefox Mobile', 'label' => 'Firefox Mobile'),
                        array('value' => 'Chrome', 'label' => 'Google Chrome'),
                        array('value' => 'Chrome Mobile', 'label' => 'Chrome Mobile'),
                        array('value' => 'Chrome Frame', 'label' => 'Chrome Frame'),
                        array('value' => 'Apple Safari', 'label' => 'Apple Safari'),
                        array('value' => 'Mobile Safari', 'label' => 'Mobile Safari'),
                        array('value' => 'Opera', 'label' => 'Opera'),
                        array('value' => 'Opera Mobile', 'label' => 'Opera Mobile'),
                        array('value' => 'Netscape', 'label' => 'Netscape'),
                  );
    }

    public function getSingleRemote2Data($field, $data)
    {
        if ($field == 'Country') {
            $array = $this->getContriesList();
            foreach ($array as $k => $v) {
                if ($v == $data) {
                    return $k;
                }
            }
        }
    }

    public function GoalReached_CallBack($data)
    {
        if (!class_exists(t3lib_extmgm)) {
            return;
        }
        $file = t3lib_extmgm::extPath('t3p_dynamic_content_placement').'lib/class.tx_t3p_dynamic_content_placement_core.php';
        if (!file_exists($file)) {
            return;
        }
        require_once $file;

        $core = tx_t3p_dynamic_content_placement_core::getObjectSingleton();
        $ttContent = $core->GetModel('ttcontent');

        return $ttContent->GetGoalName($data);
    }

    public function CV_CallBack($data)
    {
        if (!class_exists(t3lib_extmgm)) {
            return;
        }
        $file = t3lib_extmgm::extPath('t3p_listmanagement').'lib/class.tx_t3p_listmanagement_core.php';
        require_once $file;
        $core = tx_t3p_listmanagement_core::getObjectSingleton();
        $uTypeObj = $core->GetModel('unique_type');
        $uTypeObj->Load($data);

        return $uTypeObj->titel;
    }

    public function Category_CallBack($data)
    {
        return $data;
    }

    private function getContriesList()
    {
        return array(
                            'Afghanistan' => 'af',
                            'Aland Islands' => 'ax',
                            'Albania' => 'al',
                            'Algeria' => 'dz',
                            'American Samoa' => 'as',
                            'Andorra' => 'ad',
                            'Angola' => 'ao',
                            'Anguilla' => 'ai',
                            'Antarctica' => 'aq',
                            'Antigua And Barbuda' => 'ag',
                            'Argentina' => 'ar',
                            'Armenia' => 'am',
                            'Aruba' => 'aw',
                            'Australia' => 'au',
                            'Austria' => 'at',
                            'Azerbaijan' => 'az',
                            'Bahamas' => 'bs',
                            'Bahrain' => 'bh',
                            'Bangladesh' => 'bd',
                            'Barbados' => 'bb',
                            'Belarus' => 'by',
                            'Belgium' => 'be',
                            'Belize' => 'bz',
                            'Benin' => 'bj',
                            'Bermuda' => 'bm',
                            'Bhutan' => 'bt',
                            'Bolivia, Plurinational State Of' => 'bo',
                            'Bonaire, Saint Eustatius And Saba' => 'bq',
                            'Bosnia And Herzegovina' => 'ba',
                            'Botswana' => 'bw',
                            'Bouvet Island' => 'bv',
                            'Brazil' => 'br',
                            'British Indian Ocean Territory' => 'io',
                            'Brunei Darussalam' => 'bn',
                            'Bulgaria' => 'bg',
                            'Burkina Faso' => 'bf',
                            'Burundi' => 'bi',
                            'Cambodia' => 'kh',
                            'Cameroon' => 'cm',
                            'Canada' => 'ca',
                            'Cape Verde' => 'cv',
                            'Cayman Islands' => 'ky',
                            'Central African Republic' => 'cf',
                            'Chad' => 'td',
                            'Chile' => 'cl',
                            'China' => 'cn',
                            'Christmas Island' => 'cx',
                            'Cocos (Keeling) Islands' => 'cc',
                            'Colombia' => 'co',
                            'Comoros' => 'km',
                            'Congo' => 'cg',
                            'Congo, The Democratic Republic Of The' => 'cd',
                            'Cook Islands' => 'ck',
                            'Costa Rica' => 'cr',
                            'Cote D\'ivoire' => 'ci',
                            'Croatia' => 'hr',
                            'Cuba' => 'cu',
                            'Curacao' => 'cw',
                            'Cyprus' => 'cy',
                            'Czech Republic' => 'cz',
                            'Denmark' => 'dk',
                            'Djibouti' => 'dj',
                            'Dominica' => 'dm',
                            'Dominican Republic' => 'do',
                            'Ecuador' => 'ec',
                            'Egypt' => 'eg',
                            'El Salvador' => 'sv',
                            'Equatorial Guinea' => 'gq',
                            'Eritrea' => 'er',
                            'Estonia' => 'ee',
                            'Ethiopia' => 'et',
                            'Falkland Islands (Malvinas)' => 'fk',
                            'Faroe Islands' => 'fo',
                            'Fiji' => 'fj',
                            'Finland' => 'fi',
                            'France' => 'fr',
                            'French Guiana' => 'gf',
                            'French Polynesia' => 'pf',
                            'French Southern Territories' => 'tf',
                            'Gabon' => 'ga',
                            'Gambia' => 'gm',
                            'Georgia' => 'ge',
                            'Germany' => 'de',
                            'Ghana' => 'gh',
                            'Gibraltar' => 'gi',
                            'Greece' => 'gr',
                            'Greenland' => 'gl',
                            'Grenada' => 'gd',
                            'Guadeloupe' => 'gp',
                            'Guam' => 'gu',
                            'Guatemala' => 'gt',
                            'Guernsey' => 'gg',
                            'Guinea' => 'gn',
                            'Guinea-Bissau' => 'gw',
                            'Guyana' => 'gy',
                            'Haiti' => 'ht',
                            'Heard Island And Mcdonald Islands' => 'hm',
                            'Holy See (Vatican City State)' => 'va',
                            'Honduras' => 'hn',
                            'Hong Kong' => 'hk',
                            'Hungary' => 'hu',
                            'Iceland' => 'is',
                            'India' => 'in',
                            'Indonesia' => 'id',
                            'Iran, Islamic Republic Of' => 'ir',
                            'Iraq' => 'iq',
                            'Ireland' => 'ie',
                            'Isle Of Man' => 'im',
                            'Israel' => 'il',
                            'Italy' => 'it',
                            'Jamaica' => 'jm',
                            'Japan' => 'jp',
                            'Jersey' => 'je',
                            'Jordan' => 'jo',
                            'Kazakhstan' => 'kz',
                            'Kenya' => 'ke',
                            'Kiribati' => 'ki',
                            'Korea, Democratic People\'s Republic Of' => 'kp',
                            'Korea, Republic Of' => 'kr',
                            'Kuwait' => 'kw',
                            'Kyrgyzstan' => 'kg',
                            'Lao People\'s Democratic Republic' => 'la',
                            'Latvia' => 'lv',
                            'Lebanon' => 'lb',
                            'Lesotho' => 'ls',
                            'Liberia' => 'lr',
                            'Libyan Arab Jamahiriya' => 'ly',
                            'Liechtenstein' => 'li',
                            'Lithuania' => 'lt',
                            'Luxembourg' => 'lu',
                            'Macao' => 'mo',
                            'Macedonia, The Former Yugoslav Republic Of' => 'mk',
                            'Madagascar' => 'mg',
                            'Malawi' => 'mw',
                            'Malaysia' => 'my',
                            'Maldives' => 'mv',
                            'Mali' => 'ml',
                            'Malta' => 'mt',
                            'Marshall Islands' => 'mh',
                            'Martinique' => 'mq',
                            'Mauritania' => 'mr',
                            'Mauritius' => 'mu',
                            'Mayotte' => 'yt',
                            'Mexico' => 'mx',
                            'Micronesia, Federated States Of' => 'fm',
                            'Moldova, Republic Of' => 'md',
                            'Monaco' => 'mc',
                            'Mongolia' => 'mn',
                            'Montenegro' => 'me',
                            'Montserrat' => 'ms',
                            'Morocco' => 'ma',
                            'Mozambique' => 'mz',
                            'Myanmar' => 'mm',
                            'Namibia' => 'na',
                            'Nauru' => 'nr',
                            'Nepal' => 'np',
                            'Netherlands' => 'nl',
                            'New Caledonia' => 'nc',
                            'New Zealand' => 'nz',
                            'Nicaragua' => 'ni',
                            'Niger' => 'ne',
                            'Nigeria' => 'ng',
                            'Niue' => 'nu',
                            'Norfolk Island' => 'nf',
                            'Northern Mariana Islands' => 'mp',
                            'Norway' => 'no',
                            'Oman' => 'om',
                            'Pakistan' => 'pk',
                            'Palau' => 'pw',
                            'Palestinian Territory, Occupied' => 'ps',
                            'Panama' => 'pa',
                            'Papua New Guinea' => 'pg',
                            'Paraguay' => 'py',
                            'Peru' => 'pe',
                            'Philippines' => 'ph',
                            'Pitcairn' => 'pn',
                            'Poland' => 'pl',
                            'Portugal' => 'pt',
                            'Puerto Rico' => 'pr',
                            'Qatar' => 'qa',
                            'Reunion' => 're',
                            'Romania' => 'ro',
                            'Russian Federation' => 'ru',
                            'Rwanda' => 'rw',
                            'Saint Barthelemy' => 'bl',
                            'Saint Helena, Ascension And Tristan Da Cunha' => 'sh',
                            'Saint Kitts And Nevis' => 'kn',
                            'Saint Lucia' => 'lc',
                            'Saint Martin (French Part)' => 'mf',
                            'Saint Pierre And Miquelon' => 'pm',
                            'Saint Vincent And The Grenadines' => 'vc',
                            'Samoa' => 'ws',
                            'San Marino' => 'sm',
                            'Sao Tome And Principe' => 'st',
                            'Saudi Arabia' => 'sa',
                            'Senegal' => 'sn',
                            'Serbia' => 'rs',
                            'Seychelles' => 'sc',
                            'Sierra Leone' => 'sl',
                            'Singapore' => 'sg',
                            'Sint Maarten (Dutch Part)' => 'sx',
                            'Slovakia' => 'sk',
                            'Slovenia' => 'si',
                            'Solomon Islands' => 'sb',
                            'Somalia' => 'so',
                            'South Africa' => 'za',
                            'South Georgia And The South Sandwich Islands' => 'gs',
                            'Spain' => 'es',
                            'Sri Lanka' => 'lk',
                            'Sudan' => 'sd',
                            'Suriname' => 'sr',
                            'Svalbard And Jan Mayen' => 'sj',
                            'Swaziland' => 'sz',
                            'Sweden' => 'se',
                            'Switzerland' => 'ch',
                            'Syrian Arab Republic' => 'sy',
                            'Taiwan, Province Of China' => 'tw',
                            'Tajikistan' => 'tj',
                            'Tanzania, United Republic Of' => 'tz',
                            'Thailand' => 'th',
                            'Timor-Leste' => 'tl',
                            'Togo' => 'tg',
                            'Tokelau' => 'tk',
                            'Tonga' => 'to',
                            'Trinidad And Tobago' => 'tt',
                            'Tunisia' => 'tn',
                            'Turkey' => 'tr',
                            'Turkmenistan' => 'tm',
                            'Turks And Caicos Islands' => 'tc',
                            'Tuvalu' => 'tv',
                            'Uganda' => 'ug',
                            'Ukraine' => 'ua',
                            'United Arab Emirates' => 'ae',
                            'United Kingdom' => 'gb',
                            'United States' => 'us',
                            'United States Minor Outlying Islands' => 'um',
                            'Uruguay' => 'uy',
                            'Uzbekistan' => 'uz',
                            'Vanuatu' => 'vu',
                            'Vatican City State' => 'va',
                            'Venezuela, Bolivarian Republic Of' => 've',
                            'Viet Nam' => 'vn',
                            'Virgin Islands, British' => 'vg',
                            'Virgin Islands, U.S.' => 'vi',
                            'Wallis And Futuna' => 'wf',
                            'Western Sahara' => 'eh',
                            'Yemen' => 'ye',
                            'Zambia' => 'zm',
                            'Zimbabwe' => 'zw',
                         );
    }

    private function getContinentList()
    {
        return array(
                        'Africa' => 'AF',
                        'Antarctica' => 'AN',
                        'Asia' => 'AS',
                        'Europe' => 'EU',
                        'Oceania' => 'OC',
                        'North America' => 'NA',
                        'South America' => 'SA',
                    );
    }

    private function getOSList()
    {
        return array(
          array('label' => 'Windows 3.11', 'value' => 'Windows 3.11'),
          array('label' => 'Windows 95', 'value' => 'Windows 95'),
          array('label' => 'Windows 98', 'value' => 'Windows 98'),
          array('label' => 'Windows 2000', 'value' => 'Windows 2000'),
          array('label' => 'Windows XP', 'value' => 'Windows XP'),
          array('label' => 'Windows 7', 'value' => 'Windows 7'),
          array('label' => 'Windows 8', 'value' => 'Windows 8'),
          array('label' => 'Windows 10', 'value' => 'Windows 10'),
          array('label' => 'Windows Server 2003', 'value' => 'Windows Server 2003'),
          array('label' => 'Windows Vista', 'value' => 'Windows Vista'),
          array('label' => 'Windows NT 4.0', 'value' => 'Windows NT 4.0'),
          array('label' => 'Windows ME', 'value' => 'Windows ME'),
          array('label' => 'Mac OS X', 'value' => 'MAC OS X'),
          array('label' => 'Open BSD', 'value' => 'Open BSD'),
          array('label' => 'Sun OS', 'value' => 'Sun OS'),
          array('label' => 'QNX', 'value' => 'QNX'),
          array('label' => 'BeOS', 'value' => 'BeOS'),
          array('label' => 'OS/2', 'value' => 'OS/2'),
          array('label' => 'Search Bot', 'value' => 'Search Bot')
        );
    }

    private function getTimeZoneList()
    {
        return array(
          array('label' => '(GMT -12:00) Eniwetok, Kwajalein', 'value' => '-12.0'),
          array('label' => '(GMT -11:00) Midway Island, Samoa', 'value' => '-11.0'),
          array('label' => '(GMT -10:00) Hawaii', 'value' => '-10.0'),
          array('label' => '(GMT -9:00) Alaska', 'value' => '-9.0'),
          array('label' => '(GMT -8:00) Pacific Time (US & Canada)', 'value' => '-8.0'),
          array('label' => '(GMT -7:00) Mountain Time (US & Canada)', 'lavaluebel' => '-7.0'),
          array('label' => '(GMT -6:00) Central Time (US & Canada), Mexico City', 'value' => '-6.0'),
          array('label' => '(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima', 'value' => ''),
          array('label' => '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz', 'value' => '-4.0'),
          array('label' => '(GMT -3:30) Newfoundland', 'value' => '-3.5'),
          array('label' => '(GMT -3:00) Brazil, Buenos Aires, Georgetown', 'value' => '-3.0'),
          array('label' => '(GMT -2:00) Mid-Atlantic', 'value' => '-2.0'),
          array('label' => '(GMT -1:00 hour) Azores, Cape Verde Islands', 'value' => '-1.0'),
          array('label' => '(GMT) Western Europe Time, London, Lisbon, Casablanca', 'value' => '0.0'),
          array('label' => '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris', 'value' => '1.0'),
          array('label' => '(GMT +2:00) Kaliningrad, South Africa', 'value' => '2.0'),
          array('label' => '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg', 'value' => '3.0'),
          array('label' => '(GMT +3:30) Tehran', 'value' => '3.5'),
          array('label' => '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi', 'value' => '4.0'),
          array('label' => '(GMT +4:30) Kabul', 'value' => '4.5'),
          array('label' => '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent', 'value' => '5.0'),
          array('label' => '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi', 'value' => '5.5'),
          array('label' => '(GMT +5:45) Kathmandu', 'value' => '5.75'),
          array('label' => '(GMT +6:00) Almaty, Dhaka, Colombo', 'value' => '6.0'),
          array('label' => '(GMT +7:00) Bangkok, Hanoi, Jakarta', 'value' => '7.0'),
          array('label' => '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong', 'value' => '8.0'),
          array('label' => '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk', 'value' => '9.0'),
          array('label' => '(GMT +9:30) Adelaide, Darwin', 'value' => '9.5'),
          array('label' => '(GMT +10:00) Eastern Australia, Guam, Vladivostok', 'value' => '10.0'),
          array('label' => '(GMT +11:00) Magadan, Solomon Islands, New Caledonia', 'value' => '11.0'),
          array('label' => '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka', 'value' => '12.0')

                    );
    }

    public function getProperty($propertyname){
      $properties = $this->getProperties() ;
      if(!array_key_exists($propertyname , $properties))
        return false ;
      return $properties[$propertyname] ;
    }
}
