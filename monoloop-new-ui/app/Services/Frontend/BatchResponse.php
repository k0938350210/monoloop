<?php namespace App\Services\Frontend;

class BatchResponse{
	public $gmid ;
	public $vid ;
	public $skey ;
	public $mid ;
	public $data ;

	public function __construct($reponseString){
		/// Grab mid
	 	$startPos = strpos($reponseString,'\"mid\",\"');
	 	$endPos = strpos($reponseString,'\"',$startPos + 11);
	 	$this->mid = substr($reponseString, $startPos + 10 , $endPos - $startPos - 10);

	 	/// Grab gmid
	 	$startPos = strpos($reponseString,'\"gmid\",\"');
	 	$endPos = strpos($reponseString,'\"',$startPos + 12);
	 	$this->gmid = substr($reponseString, $startPos + 11 , $endPos - $startPos - 11);

	 	/// Grab vid
	 	$startPos = strpos($reponseString,'\"vid\",\"');
	 	$endPos = strpos($reponseString,'\"',$startPos + 11);
	 	$this->vid = substr($reponseString, $startPos + 10 , $endPos - $startPos - 10);

	 	/// Skey
	 	$startPos = strpos($reponseString,'\"skey\",\"');
	 	$endPos = strpos($reponseString,'\"',$startPos + 12);
	 	$this->skey = substr($reponseString, $startPos + 11 , $endPos - $startPos - 11);

	 	/// Data ;
	 	$startPos = strpos($reponseString,'"data":');
	 	$data = substr($reponseString, $startPos + 7 , strlen($reponseString) - 1 - $startPos - 7  );
	 	$this->data = json_decode($data,true);

	}
}