<?php namespace App\Services\Frontend;

use App\MonoloopServer ;
use App\Jobs\FrontendInvalidate;
use Goutte\Client;
use Illuminate\Foundation\Bus\DispatchesJobs ;

class Invalidator{
  use DispatchesJobs;

  private $_debug ;

  public function removeCached($hash , $debug = false){
    $this->_debug = $debug ;
    $servers = $this->allIPs() ;
    $endpoint = config('services.frontend.invalidate.endpoint');
    foreach ($servers as $server) {
      $url = 'http://' . $server->ip . $endpoint . $hash ;
      $isSuccessInvalidate = $this->callInvalidate($url,$hash) ;
      if( $debug){
        echo 'status : ' . intval($isSuccessInvalidate) . "\n" ;
      }
      if(!$isSuccessInvalidate){
        $server->status = 'Inactive' ;
        $server->save() ;
      }
    }
  }

  public function callInvalidate($url,$hash=''){
    /*
    $client = new Client([
      'defaults' => [
        'timeout' => 10
      ]
    ]);
    */
    $retry = config('services.frontend.invalidate.retry') ;

    for($i = 0 ; $i < $retry ; $i++ ){
      if( $this->_debug){
        echo 'call url : ' . $url . ' retry - ' . ($i+1) . "\n" ;
      }
      #$crawler = $client->request('GET', $url);
      #$status_code = $client->getResponse()->getStatus();
      #echo $status_code . "\n" ;
      $ret = $this->curl($url) ;
      if($ret['info']['http_code'] == 204){
        if(  $this->_debug){
          echo 'result 204 : Key removd' . "\n" ;
        }
        return true ;
      }elseif($ret['info']['http_code'] == 200){
        if(  $this->_debug){
          echo 'result 200 : ' . $ret['content'] . "\n" ;
        }
        if( strpos($ret['content'],'MONOloop.MonoloopData') !== -1){
          return true ;
        }
      }elseif($ret['info']['http_code'] == 502){
        //Frontend return bad gateway sometimes
        //then put to delay job and reprocess;
        $this->dispatch(new FrontendInvalidate($hash));
        return true ;
      }else{
        if(  $this->_debug){
          echo 'result '.$ret['info']['http_code'].' : ' . $ret['content'] . "\n" ;
        }
      }
    }
    return false ;
  }

  public function curl($url){
    $timeout = config('services.frontend.invalidate.timeout') ;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,$timeout);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT,$timeout);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $content = curl_exec( $ch );
    $header  = curl_getinfo( $ch );
    return ['info' => $header , 'content' => $content] ;
  }

  public function allIPs(){
    return MonoloopServer::active()->get() ;
  }

}
