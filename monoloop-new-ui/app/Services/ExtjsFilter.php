<?php namespace App\Services;

use Illuminate\Http\Request;

class ExtjsFilter{
	public static function filter($query,Request $request,$mapOrder = []){
		$limit = intval( $request->input('limit') ) ; 
		$page =  intval( $request->input('page') ); 
		$skip = ($page-1)*$limit;
		$order = json_decode($request->input('sort'),true) ; 
		$orderBy = $order[0]['property'] ; 
		if(isset($mapOrder[$orderBy])){
			$orderBy = $mapOrder[$orderBy] ; 
		}
		$orderDir = $order[0]['direction'] ; 
		$query = $query->orderBy($orderBy,$orderDir)->skip($skip)->limit($limit) ; 
		return $query ; 
	}
}