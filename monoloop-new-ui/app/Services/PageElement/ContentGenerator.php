<?php namespace App\Services\PageElement ;

use App\Goal ;
use App\Content ;

class ContentGenerator{
  public static function newUid(){
    return max(Goal::max('uid'),Content::max('uid'))+1 ;
  }
}
