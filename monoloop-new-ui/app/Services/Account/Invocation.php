<?php namespace App\Services\Account;
/** 
 * This class for generate invocation
 * */  
use App\Account ;  
 
class Invocation{
	
	private $account ; 
	
	/**
	 * Create a new   instance.
	 *
	 * @return void
	 */
	 
	public function __construct(Account $account)
	{
		$this->account = $account ; 
	}	
	
	public function getInvocation(){
		return View('scripts.invocation',['account'=>$this->account])->__toString() ; 
	}
}
	