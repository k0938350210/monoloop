<?php namespace App\Services\Account;
use Auth;
use MongoId;
use App\Account;
use App\User;
use App\AccountUser;
use App\AccountPlugin;

use Mail;


class MailService{

  public $prefix = '' ;

  public function sendInvitationMail(AccountUser $user){
    Mail::send('emails.invitation.user', ['user' => $user ], function($message)use ($user)
    {
      $message->to($user->email,$user->name)->subject($this->prefix . 'Please finish your Monoloop account creation');
    });
  }

  public function sendComfirmationMail(AccountUser $account_user , User $user ){
    Mail::send('emails.confirmation', ['account_user' => $account_user , 'user' => $user ], function($message)use ($account_user , $user)
    {
      $message->to($user->username,$account_user->name)->subject($this->prefix .  'Activate your account');
    });
  }

  public function sendNewSignUptoAdmins(Account $account, AccountUser $account_user , User $user , $source){
    Mail::send('emails.new_signup_noti_to_admin', ['account' => $account,'account_user' => $account_user , 'user' => $user, 'source' => $source ], function($message)use ($account, $account_user , $user, $source)
    {
      $message->to(config('app.monoloop.administrators'))->subject('Notification from Monoloop [New Sign up]');
    });
  }

  public function sendMultiUsersFromEmail($email , $account_users ){
    $usernames = [] ;
    foreach($account_users as $account_user){
      if( $account_user->user ){
        if(!in_array($account_user->user->username  ,$usernames )){
          $usernames[] = $account_user->user->username ;
        }
      }
    }
    Mail::send('emails.forgot_found_multi_user', ['email' => $email , 'users' => implode(' , ',$usernames) ], function($message)use ($email )
    {
      $message->to($email,$email)->subject('[monoloop.com] We found your E-Mail on multiple accounts');
    });
  }

  public function sendResetLink($email , $user  ){
    Mail::send('emails.reset_password_link', ['user' => $user ], function($message)use ($email , $user)
    {
      $message->to($email,$user->username)->subject($this->prefix . '[monoloop.com] Reset password your Monoloop account');
    });
  }
}
