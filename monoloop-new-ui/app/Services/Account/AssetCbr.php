<?php namespace App\Services\Account;
 
use App\Account ;   
use App\TrackerNode ;  

class AssetCbr{
  
  public $account;
  public $js;
  
  public function __construct(Account $account)
  {
    $this->account = $account;
    $this->renderJS() ; 
  }
  
  public function renderJS(){
    $assets = TrackerNode::byType('asset')->byAccount($this->account->_id)->where('hidden','=',0)->get() ;  
    $this->js = View('scripts.post_asset')->with('account',$this->account)->with('assets',$assets)->render() ;  
  }
}