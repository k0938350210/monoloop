<?php namespace App\Services\Account;
 
use App\Account ;   
use App\TrackerNode ;  

class TrackerCbr{
  
  public $account;
  public $js;
  
  public function __construct(Account $account)
  {
    $this->account = $account;
    $this->renderJS() ; 
  }
  
  public function renderJS(){
    $trackers = TrackerNode::byType('tracker')->byAccount($this->account->_id)->where('hidden','=',0)->get() ;   
    $this->js = View('scripts.post_tracker')->with('account',$this->account)->with('trackers',$trackers)->render() ; 
  }
}