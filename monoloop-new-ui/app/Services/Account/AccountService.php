<?php namespace App\Services\Account;
use Auth;
use MongoId;
use App\Account;
use App\AccountUser;
use App\AccountPlugin;
use App\Plugin;

use Carbon;


class AccountService{
	public function crateDefaultAccountData(Account $account){
		$account->controlgroup = ['enabled' => false , 'size' => 0 , 'days' => 0] ;
		if(!Auth::guest()){
    	$account->parent_id = new MongoId( Auth::user()->active_account ) ;
		}else{
			$account->parent_id = null ;
		}

    $account->uid = Account::max('uid') + 1 ;
    $plugins = Plugin::all() ;
    $p = [] ;
    foreach($plugins as $plugin){
      $p[] = ['_id' => new MongoId() , 'enabled' => false , 'plugin_id' => new MongoId($plugin->_id) ] ;
    }
    $account->cid = '' ;
    $account->plugins = $p ;
    $account->is_test = false ;
    $account->lock_account_type = false ;
    $account->auto_activate_after_trial = false ;
    #trial expire next month;
    $account->trialconvertion = new Carbon('+1 month');
    $account->cid = (string)(new MongoID());
    $account->s3_bucket = $account->cid ;
    $account->invocation = ['content_delivery' => 0 , 'anti_flicker' => false , 'timeout' => 0 , 'pre' => 0 , 'post' => 0 ] ;
    $account->scores = ['clickdepth' => 0 , 'duration' => 0 , 'loyalty' => 0 , 'recency' => 0 , 'brands' => 0] ;
    $account->control_group = ['enabled' => false , 'size' => 0 , 'days' => 0 ] ;

	}

}
