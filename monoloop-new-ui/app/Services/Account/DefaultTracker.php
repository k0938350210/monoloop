<?php namespace App\Services\Account;

use App\Account ;
use App\Tracker;
use App\Folder;
use App\UrlConfig;

class DefaultTracker{

  public function createDefault(Account $account){

    # Get folder id ;
    $rFid = Folder::rootFolders(new \MongoId($account->id))->first() ;
    if($rFid){
      $rFid =  new \MongoId($rFid->id);
    } else {
      $rFid = null;
    }

    $default_trackers = [
      [
        'name' => 'Unique ID',
        'type' => 'custom',
        'system_name' => 'uniqid',
        'system' => 1,
        'tracker_events' => [[
          "_id" => "event-page-render",
          "name" => "",
          "type" => "page-render",
          "selector" => "",
          "custom" => ""
        ]],
        'tracker_fields' => null,
      ]
    ];

    foreach($default_trackers as $default_tracker){
      $tracker = Tracker::where('system_name',$default_tracker['system_name'])->byCid($account->uid)->isSystem()->first();

      if(is_null($tracker)){
        $tracker = new Tracker();
      }else{
        // make sure it not overwrite exists value
        continue;
      }

      $tracker->name = $default_tracker['name'];
      $tracker->type = $default_tracker['type'];
      $tracker->system = $default_tracker['system'];
      $tracker->system_name = $default_tracker['system_name'];
      $tracker->cid = $account->uid;
      $tracker->account_id = new \MongoId($account->id);
      $tracker->deleted = 0;
      $tracker->hidden = 1;
      $tracker->folder_id = $rFid;
      $tracker->uid = Tracker::max('uid') + 1 ;
      $tracker->tracker_events = $default_tracker['tracker_events'];
      $tracker->tracker_fields = $default_tracker['tracker_fields'];
      $tracker->save();

      $url_config = new UrlConfig();
      $url_config->url = $account->first_domain;
      $url_config->remark = '';
      $url_config->url_option = 'startsWith';
      $url_config->reg_ex = '';
      $url_config->inc_www = true;
      $url_config->inc_http_https = true;

      $tracker->urlConfig()->save($url_config);

    }
  }
}