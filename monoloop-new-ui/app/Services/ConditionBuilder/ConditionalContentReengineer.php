<?php namespace App\Services\ConditionBuilder;
/**
 * Convert conditional string to json object.
 * */

use App\Services\Profile\Profile as MonoloopConnector;

class ConditionalContentReengineer {

    public $funcArray ;
    public $compareArray ;
    public $rules = [];

    public function __construct(){
        $this->funcArray = array('ml_contains' , 'ml_dose_not_contains' , 'ml_starts_with' , 'ml_ends_with' , 'ml_comes_on' , 'ml_different_from' , 'ml_comes_after' , 'ml_comes_before' , 'ml_advanced' , 'ml_contain_exactly' , 'ml_contain_all' , 'ml_contain_some' , 'ml_not_contain') ;
        $this->compareArray = array( '===' , '!==' , '==' ,'!=' , '<=' , '>=' , ' < ' , ' > ' ) ;
    }

    public function prepareCondition($curentValue){
        $curentValue = htmlspecialchars_decode($curentValue) ;
        $curentValue = str_replace('&nbsp;' , ' ' , $curentValue) ;
        // Remove unused char
        $curentValue = str_replace(array('if','IF',' | ' , '{|}' , '{|' , '|}' ,'{' ,'}'  ) , '' , $curentValue) ;
        // Trime once ;
        $curentValue = trim($curentValue) ;
        // remove first parenthesis
        $curentValue = substr($curentValue , 1 , strlen($curentValue)-2) ;

        return $curentValue ;
    }

    public function extendBraket($val){
		$len =  strlen($val) ;
		$ret = '' ;
		$expand = array('(' , ')' ) ;
		for($i = 0 ; $i < $len ; $i++){
			if( in_array($val[$i] , $expand )){

				$ret .= ' ' . $val[$i] . ' ' ;
			}else{
				$ret .= $val[$i] ;
			}
		}
		return $ret ;
	}

    public function splitAndOr($val){
        if(trim($val) == '')
            return '' ;



        $ret = array() ;
        $offset = 0 ;
        $search = true ;
        $i = 0 ;
        while( $search ){
            $posAnd =  stripos($val , ' && ' , $offset ) ;
            $posOr = stripos($val , ' || ' , $offset ) ;
            if( $posAnd === false && $posOr === false){
                break ;
            }

            if( ($posAnd < $posOr && $posAnd !== false   )|| $posOr === false){
                $offset = $posAnd + 1;
                $ret[] = $posAnd  ;
            }else{
                $offset = $posOr + 1;
                $ret[] = $posOr   ;
            }

            if( $i > 5)
                break ;
            $i++ ;

        }

        $ret[] = strlen($val) ;

        $bufferExpression = array() ;

        if( count($ret) == 1){
            $val = trim($val) ;
             if( $val[0] == '(' && $val[strlen($val)-1] == ')'){
                $val = substr($val , 1 ,strlen($val) -2 ) ;
             }

             $bufferExpression[] = array( 'condition' => $val , 'rules' => [] , 'fields' => $this->procesFieldSyntax($val) )   ;
            return $bufferExpression ;
        }


        $startPos = 0 ;
        for( $i = 0 ; $i < count($ret) ; $i++ ){
            $subStr = substr($val , $startPos , $ret[$i] - $startPos )  ;

            $andOr = '' ;

            $posOr = stripos($subStr , ' || ' ) ;
            $posAnd = stripos($subStr , ' && ') ;
            if($posAnd === 0 ){
                $andOr = 'AND' ;
                $subStr = substr($subStr , 5 , strlen($subStr) - 5) ;
            }elseif($posOr === 0){
                $andOr = 'OR' ;
                $subStr = substr($subStr , 4 , strlen($subStr) - 4) ;
            }

            //echo $subStr . '<br>' ;

            //$subStr = str_ireplace(array('and' , 'or') , '' , $subStr ) ;

            if($this->isCompleteParenthesis($subStr)){

                $startPos = $ret[$i]  ;
                $innerSubstr = trim( $subStr );
                if( $innerSubstr[0] == '(' && $innerSubstr[strlen($innerSubstr)-1] == ')'){
                    $innerSubstr = substr($innerSubstr , 1 ,strlen($innerSubstr) -2 ) ;
                }

                $buffer = array( 'condition' => $subStr , 'rules' => $this->splitAndOr($innerSubstr) , 'join' => $andOr )   ;
                if( count( $buffer['rules'] ) <= 1 ){
                    $buffer['rules'] = [] ;

                    $buffer['fields'] = $this->procesFieldSyntax($innerSubstr) ;

                }
                $bufferExpression[] = $buffer ;
                //$bufferExpression[] = $subStr ;
            }

        }
        return $bufferExpression ;

    }

    private $queue = [];

    public function formatParsedCondition( $condition, $parent_id, $rec  = false){

      $branch = [];


      if(count($condition) > 0){
        foreach ($condition as $k => &$c) {
          if(isset($c['rules']) && count($c['rules']) > 0 && $c['rules'] != ""){

            $c['key'] = $c['rules'][0]['fields']['id'];

          } else {

            $this->rules[] = $c;

          }

        }
      }

      return $this->rules;
    }

    public function procesFieldSyntax($val){

        $retMethod = $this->checkIsUsingMethod($val) ;
        if( $retMethod != ''){
            return $retMethod ;
        }

        $retFunc = $this->checkIsUsingFunction($val) ;
        if($retFunc !== false){
            return $this->extractSyntaxFunction($retFunc , $val) ;
        }


        //echo '+' . $val . '+' ;
        $retOperand = $this->checkIsUsingOperator($val) ;

        if($retOperand !== false ){

            return $this->extractSyntaxOperand($retOperand , $val) ;
        }
    }

    public function extractSyntaxFunction($funcName , $val ){

        $ret = array() ;
        $ret['operand'] = $this->convertFucntionToOperand($funcName) ;

        $val = str_ireplace($funcName , '' , $val) ;
        $val = trim( $val );
        if( $val[0] == '(' && $val[strlen($val)-1] == ')'){
            $val = substr($val , 1 ,strlen($val) -2 ) ;
        }
        $fields = explode(',' , $val) ;
        //print_r($fields) ;
        list($connector , $field) = explode('.' , $fields[0]) ;

        $ret['connector'] = trim( str_replace('$' , '' , $connector) ) ;
        $ret['id'] = trim( $field ) ;
        $ret['cls'] = 'file' ;
        $ret['leaf'] = true ;


        unset($fields[0]) ;

        $remainData = implode(',' ,  $fields) ;

        $remainData = trim( $remainData );
        if( $remainData[0] == '\'' && $remainData[strlen($remainData)-1] == '\''){
            $remainData = substr($remainData , 1 ,strlen($remainData) -2 ) ;
        }
        $ret['value'] = $remainData ;

        $connectorObj = new MonoloopConnector() ;
        $connect = $connectorObj->loadConnector( trim( $ret['connector']) ) ;
        // Not found this connector ;
        if($connect == null)
            return $ret ;

        $properties = $connect->getProperties() ;
        if( ! array_key_exists( $field  , $properties))
            return $ret ;



        $ret['text'] = $properties[$field]['Label'] ;
        $ret['type'] = $properties[$field]['Type'] ;
        $ret['values'] = $properties[$field]['Values'] ;

        if( $ret['type'] == 'single' || $ret['type'] == 'multi' ){
            $data = array() ;
            foreach($properties[$field]['Values'] as $k => $v){
                $data[] = array('label' => $k , 'value' => $v) ;
            }
            $ret['values'] = $data ;
        }


        return $ret ;
    }

    public function convertFucntionToOperand($functionName){
        $ret = '' ; //'ml_contains' , 'ml_dose_not_contains' , 'ml_starts_with' , 'ml_ends_with' , 'ml_comes_on' , 'ml_different_from' , 'ml_comes_after' , 'ml_comes_before' , 'ml_advanced' , 'ml_contain_exactly' , 'ml_contain_all' , 'ml_contain_some' , 'ml_not_contain'
        switch ($functionName){
        	case 'ml_contains': $ret = 'contains' ;  break;
            case 'ml_dose_not_contains': $ret = 'does not contain' ;  break;
            case 'ml_starts_with': $ret = 'starts with' ;  break;
            case 'ml_ends_with': $ret = 'ends with' ;  break;
            case 'ml_comes_on': $ret = 'comes on' ;  break;
            case 'ml_different_from': $ret = 'different from' ;  break;
            case 'ml_comes_after': $ret = 'comes after' ;  break;
            case 'ml_comes_before': $ret = 'comes before' ;  break;
            case 'ml_advanced': $ret = 'advanced' ;  break;
            case 'ml_contain_exactly': $ret = 'contain exactly' ;  break;
            case 'ml_contain_all': $ret = 'contain all' ;  break;
            case 'ml_contain_some': $ret = 'contain some' ;  break;
            case 'ml_not_contain': $ret = 'not contain' ;  break;
        	default : $ret = '' ;
        }
        return $ret ;
    }

    public function checkIsUsingFunction($val){
        foreach( $this->funcArray as $func){
            $pos = stripos($val , $func ) ;
            if( $pos !== false)
                return $func ;
        }
        return false ;
    }

    public function extractSyntaxOperand($name , $val){
        $ret = array() ;
        $ret['operand'] = $name ;
        $fields = explode($name , $val) ;
        list($connector , $field) = explode('.' , $fields[0]) ;
        $ret['connector'] = trim( str_replace('$' , '' , $connector) ) ;
        $field = trim($field) ;
        $ret['field'] = $field ;
        if (strpos($field,'(') !== false) {
            $explodeId = explode('(', $field);
            $ret['id'] = trim($explodeId[0]);
        } else {
          $ret['id'] = $field ;
        }

        unset($fields[0]) ;
        // var_dump($ret);
        $remainData = implode(',' ,  $fields) ;
        $remainData = trim( $remainData );
        if( $remainData[0] == '\'' && $remainData[strlen($remainData)-1] == '\''){
            $remainData = substr($remainData , 1 ,strlen($remainData) -2 ) ;
        }
        $ret['value'] = $remainData ;

        $connectorObj = new MonoloopConnector() ;
        $connect = $connectorObj->loadConnector( trim( $ret['connector']) ) ;
        // Not found this connector ;
        if($connect == null)
            return $ret ;

        $properties = $connect->getProperties() ;
        if( ! array_key_exists( $field  , $properties))
            return $ret ;

        $ret['text'] = $properties[$field]['Label'] ;
        $ret['type'] = $properties[$field]['Type'] ;
        $ret['hidecompare'] = $properties[$field]['HideCompare'] ;

        if( $ret['type'] == 'single remote2' && $ret['value'] != '' ){
            $ret['value2'] = array() ;
            $ret['value2'][] = array('k' => $connect->getSingleRemote2Data($field ,$ret['value'] ) , 'v' => $ret['value'] ) ;
        }

        if($ret['type'] == 'string' || $ret['type'] == 'single' ||  $ret['type'] == 'single remote' || $ret['type'] == 'single remote2' ){
            if( $ret['operand'] == '==' || $ret['operand'] == '==='){
                $ret['operand'] = 'is' ;
            }elseif( $ret['operand'] == '!=' || $ret['operand'] == '!=='){
                $ret['operand'] = 'is not' ;
            }
        }

        $ret['values'] = $properties[$field]['Values'] ;
        if( $ret['type'] == 'single' || $ret['type'] == 'multi' ){
            $data = array() ;
            foreach($properties[$field]['Values'] as $k => $v){
                $data[] = array('label' => $k , 'value' => $v) ;
            }
            $ret['values'] = $data ;
        }

        // Add condition if bool
        if( $ret['type'] == 'bool' && $ret['operand'] == '!='){
        	$ret['value'] = 'false' ;
        }


        return $ret  ;
     }

    public function checkIsUsingOperator($val){
        foreach( $this->compareArray as $func){
            $pos = stripos($val , $func ) ;
            if( $pos !== false)
                return $func ;
        }
        return false ;
    }

    public function checkIsUsingMethod($val){
        $val = trim($val) ;
        $conditionStr = $val ;

        // Recreate this .
        $regEx = '/[a-zA-Z][a-zA-Z0-9_]*(\.)[a-zA-Z][a-zA-Z0-9_]*/i' ;
        $ret = preg_match_all($regEx , $val , $match) ;
        if( $ret == 0 )
            return '' ;

        $match = $match[0] ;

        list($connector , $field ) =  explode('.' ,$match[0]) ;
        //$connector = str_replace('$' , '' , $connector) ;

        $connectorObj = new MonoloopConnector() ;

        $connect = $connectorObj->loadConnector($connector) ;
        if(! is_object($connect )){
            return '' ;
        }

        $properties = $connect->getProperties() ;

        foreach($properties as $k => $v ){
            if($v['Type'] == 'function' && $k == trim( $field ) ){
                $pos1 = strpos($val , '.') ;
                $pos2 = strpos($val , '(' , $pos1) ;
                $pos3 = strpos($val , ')' , $pos2) ;

                $paramStr = substr($val , $pos2+ 1 , $pos3 - $pos2 -1 ) ;
                $paras = str_getcsv($paramStr , ',' , '\'') ;
				//$paras = explode(',' , $paramStr) ;
                $i = 0 ;
                foreach($v['Values'] as &$val){
                    $val['value'] = $this->removeBoundQoute($paras[$i]) ;
                    $val['values'] = $val['Values'] ;
                    if( ( $val['Type'] == 'single remote2' ||  $val['Type'] == 'lookup' ) && $val['value'] != '' ){
                        $val['value2'] = array() ;
                        $val['value2'][] = array('label' => $connect->$val['callback']($val['value'] ) , 'value' => $val['value'] ) ;
                    }
                    if($val['Type'] == 'single' || $val['Type'] == 'multi'){

                        $data = array() ;
                        foreach($val['Values'] as $key4 => $val4){
                            $data[] = array('label' => $key4 , 'value' => $val4) ;
                        }
                        $val['Values'] = null ;
                        $val['values'] = $data ;


                    }
                    $i++ ;
                }

                $ret = array() ;

                $ret['cls'] = 'file' ;
                $ret['connector'] = $connector ;
                //$ret['id'] = $v
                $ret['type'] = 'function' ;
                $ret['text'] = $v['Label'] ;
                $ret['id'] = $k ;
                $ret['values'] = $v['Values'] ;
                $ret['ReturnValue'] = $v['ReturnValue'] ;


                $retOperand = $this->checkIsUsingOperator($conditionStr) ;
                if($retOperand !== false ){
                    $tmp = explode( $retOperand , $conditionStr) ;
                    $tmp = $tmp[1] ;
                    $tmp = explode(  ')' , $tmp) ;
                    $tmp = $tmp[0] ;

                    $tmp = trim($tmp) ;
                    $ret['value'] = $this->removeBoundQoute($tmp) ;
                    $ret['operand'] = $retOperand ;
                    if ( ($retOperand == '===' || $retOperand == '==') && $ret['ReturnValue']  == 'string' )
                        $ret['operand'] = 'is' ;
                    if ( ($retOperand == '!==' || $retOperand == '!=') && $ret['ReturnValue']  == 'string' )
                        $ret['operand'] = 'is not' ;
                    return $ret ;
                }

                $retFunc = $this->checkIsUsingFunction($val) ;
                if($retFunc !== false){
                    //return $this->extractSyntaxFunction($retFunc , $val) ;
                    $pos4 = strpos($conditionStr ,  ','  , $pos3) ;

                    $pos5 = strpos($conditionStr , ')' , $pos4) ;

                    $tmp =  substr($conditionStr , $pos4 + 1 , $pos5 - $pos4 - 1 ) ;
                    $tmp = trim($tmp) ;
                    $ret['value'] = $this->removeBoundQoute($tmp) ;
                    $ret['operand'] = $this->convertFucntionToOperand($retFunc) ;
                    return $ret ;
                }


             }
        }

        return '';

    }

    public function removeBoundQoute($remainData){
        $remainData = trim( $remainData );
        if( $remainData[0] == '\'' && $remainData[strlen($remainData)-1] == '\''){
            $remainData = substr($remainData , 1 ,strlen($remainData) -2 ) ;
        }
        return $remainData ;
    }

    public function isCompleteParenthesis($val){
        $open = 0 ;
        $close = 0 ;
        for( $i = 0 ; $i < strlen($val) ; $i++ ){
            if( $val[$i] == '(')
                $open++ ;
            elseif($val[$i] == ')')
                $close++ ;
        }
        if($open == $close)
            return true ;
        return false ;
    }
}
