<?php namespace App\Services\ConditionBuilder;
/**
 * Convert conditional string to json object.
 * */

use App\Services\Profile\Profile as MonoloopProfile;

class Syntax {

  public function isConnector($connector){
    #remove segment for now ;
    if( in_array($connector , array('MonoloopProfile'))){
      return true ;
    }
    return false ;
  }

  public function getConnector($conditionStr){
    $regEx = '/[a-zA-Z][a-zA-Z0-9_]*(\.)[a-zA-Z][a-zA-Z0-9_]*/i' ;
    $conditionStr = html_entity_decode($conditionStr) ;
    $ret = preg_match_all($regEx , $conditionStr , $match) ;

    $return1 = array() ;

    if( $ret == 0 ){
        ;
    }else{
      $match = $match[0] ;
      foreach($match as $val){
        list($connector , $field ) = explode('.' , $val) ;
        //$connector = str_replace('$' , '', $connector) ;
        $return1[$connector] = ''  ;
      }
    }
    /*
    $conditionStr = htmlspecialchars_decode($conditionStr) ;
    $htmlObj = str_get_html($conditionStr);
    $conditionMainsObj = $htmlObj->find('mlt' ) ;
    for( $i = 0 ; $i < count($conditionMainsObj) ; $i++){
      $conditionObj = $conditionMainsObj[$i] ;
      $inserted = $conditionObj->getAttribute('insert') ;
      if( trim($inserted) != ''){
        list($key , $val) = explode('.' , $inserted ) ;
        if( $key == 'ListField'){
          $return1['MonoloopProfile'] = ''  ;
        }else{
          $return1[$key] = ''  ;
        }
      }
    }
    */
    $return2 = array() ;
    foreach( $return1 as $key => $val){
      if( $this->isConnector($key))
        $return2[] = $key ;
    }

    return $return2  ;


    }

  public function getTestBebchPanelObject($conditionList){
    if( count($conditionList) == 0 )
      return  ;

    $conditionAll = null ;
    if(!is_array($conditionList)){
			$conditionAll = $conditionList ;
		}else{
    	$conditionAll = implode(' ' , $conditionList) ;
 		}

    $regEx = '/[a-zA-Z][a-zA-Z0-9_]*(\\.)[a-zA-Z][a-zA-Z0-9_\[\]]*/i' ;
    $ret = preg_match_all($regEx , $conditionAll , $match) ;

    if( $ret == 0 )
      return false ;

    $match = $match[0] ;


    $monoloopProfile = new MonoloopProfile() ;
    $connectorList = array() ;
    $groupList = array() ;

    $functionStore = array() ;

    foreach($match as $val){
      list( $connect , $field ) = explode( '.' , trim( $val )  ) ;
      $connectorName = trim(str_replace('$' , '' , $connect)) ;

      if(  $this->isConnector($connectorName ) == false){
        continue ;
      }

      if(! array_key_exists($connectorName , $connectorList )){
          $connectorList[$connectorName] = $monoloopProfile ;
          $connectorList[$connectorName]->main() ;
      }
      $property = $connectorList[$connectorName]->getProperty($field) ;

      if($property === false)
        continue ;

      $additional_id = '' ;

      if($property['Type'] == 'function'){
        $pos1 = strpos($conditionAll , $val) ;
        $pos2 = strpos($conditionAll , '(' , $pos1) ;
        $pos3 = strpos($conditionAll , ')' , $pos2) ;
        $insideFunction = substr($conditionAll , $pos2 + 1 , $pos3-$pos2 -1 ) ;
        $conditionAll = substr($conditionAll , 0  , $pos1) . substr($conditionAll , $pos3) ;

        //echo $conditionAll ;
        $params = explode(',' , $insideFunction) ;
        $i = 0 ;
        foreach($params as $param){
          $additional_id .= '_' . $this->removeBoundQoute($param);
          $property['Values'][$i]['value'] =  $this->removeBoundQoute($param);
          $i++ ;
        }

        if(! isset($functionStore[$connectorName . '.' . $field . $additional_id . '###' ])){
          $functionStore[$connectorName . '.' . $field . $additional_id . '###' ] = '' ;
          $functionStore[$connectorName . '.' . $field ][] = '' ;
        }
      }

      if( ! isset($property['Group']))
        $property['Group'] = 'Default' ;
      if( isset($groupList[$property['Group']][$connectorName . '_' . $field . $additional_id ]) ){
        continue ;
      }

      $f = array() ;

      $f['text'] = $property['Label'] ;
      $f['id'] = $field ;
      $f['cls'] = 'file';
      $f['leaf'] = true;
      $f['type'] = $property['Type'] ;
      $f['connector'] = $connectorName ;
      $f['values'] = '' ;

      if( $property['Type'] == 'single remote' || $property['Type'] == 'single remote2'  ){
        $f['values'] = $property['Values'] ;
        #$f['value2'] = $property['value2'] ;
      }
      if( $property['Type'] == 'single' || $property['Type'] == 'multi' ){

        $f['values'] = $property['Values'] ;
      }

      if( $property['Type'] == 'function'){

        $f['value'] = null ;
        $f['values'] = $property['Values'] ;
        if( ! isset($property['ReturnValue']))
          $property['ReturnValue'] = 'bool' ;
        $f['ReturnValue'] = $property['ReturnValue'] ;

        // Calcalate set ;
        $f['set'] = count($functionStore[$connectorName . '.' . $field ]) ;
        $i = 0 ;

      }else{
        $f['value'] = $connectorList[$connectorName]->$field ;
        if( $f['type'] == 'single remote2'){
          $f['value2'] = array() ;
          $f['value2'][] = array('k' => $connectorList[$connectorName]->getSingleRemote2Data($field ,$f['value'] ) , 'v' => $f['value'] ) ;
        }
      }

      if( session()->has('ml_placement_testbench')){
        $testBenchData = session('ml_placement_testbench') ;
        foreach($testBenchData as $tRow){
          $sessionConnectorName = $tRow['connector'] ;
          $sesionField = $tRow['field'] ;
          if( $tRow['type'] == 'function'){
            $additional_id2 = '' ;
            foreach($tRow['params'] as $keyS => $valS){
              $additional_id2 .= $valS ;
            }
            if($connectorName == $sessionConnectorName && $field == $sesionField && $additional_id == $additional_id2){
              $f['value'] = $tRow['value'] ;
              if( $f['type'] == 'single remote2'){
                $f['value2'] = array() ;
                $f['value2'][] = array('k' => $connectorList[$connectorName]->getSingleRemote2Data($field ,$f['value'] ) , 'v' => $f['value'] ) ;
              }
              break ;
            }
          }else{
            if($connectorName == $sessionConnectorName && $field == $sesionField){
              $f['value'] = $tRow['value'] ;
              if( $f['type'] == 'single remote2'){
                $f['value2'] = array() ;
                $f['value2'][] = array('k' => $connectorList[$connectorName]->getSingleRemote2Data($field ,$f['value'] ) , 'v' => $f['value'] ) ;
              }

              break ;
            }
          }
        }
      }

      $groupList[$property['Group']][$connectorName . '_' . $field . $additional_id ] = $f ;
    }
    return $groupList ;
  }

  public function removeBoundQoute($remainData){
    $remainData = trim( $remainData );
    if( $remainData[0] == '\'' && $remainData[strlen($remainData)-1] == '\''){
      $remainData = substr($remainData , 1 ,strlen($remainData) -2 ) ;
    }
    return $remainData ;
  }
}
