<?php namespace App\Services;

use \Wa72\HtmlPageDom\HtmlPageCrawler;

class MonoloopCondition{

  private $_condition ;

  public function __construct($conditionStr = ''){
    $this->_condition = $conditionStr ;
  }

  public function __toString(){
    return $this->_condition ;
  }

  public function setCondition($condition){
    $this->_condition = $condition ;
  }

  public function replacePipe($val = 'echo 1 ; '){
    $this->_condition = str_replace(' | ', $val, $this->_condition );
    $this->_condition = str_replace('{|', '{ ' . $val, $this->_condition );
    $this->_condition = str_replace('|}', $val .' }', $this->_condition );
  }

  public function processContentInCondition($content){
    $content = htmlspecialchars_decode($content) ;
    #https://github.com/wasinger/htmlpagedom/issues/9
    $content = '<div>' . $content . '</div>' ;
    $page = new HtmlPageCrawler($content);

    $mlts = $page->filter('div[isconditional="true"]');
    $total = count($mlts) ;

    /*
    echo "$total \n";
    $test = 0 ;

    while($test < 3){

      $condition = $mlts->getAttribute('condition') ;
      $this->setCondition($condition) ;
      $inner = $mlts->getInnerHtml();
      $this->replacePipe(' ###E### ' . $inner . ' ###S###' ) ;
      $replace = ' ###S### ' . $this->_condition . ' ###E### '  ;
      $mlts->replaceWith($replace) ;

      $content = $page->saveHTML() ;

      $page = new HtmlPageCrawler($content);
      $mlts = $page->filter('mlt');
      $total = count($mlts) ;

      echo "$total \n";
      $test++ ;
    }

    return $page->saveHTML() ;
    */
    for( $i = $total-1 ; $i >= 0 ; $i--){
      $mlt = $page->filter('div[isconditional="true"]')->reduce(function($c,$j) use ($i){
        if($i == $j){
          return true ;
        }
        return false ;
      });
      $condition = $mlt->getAttribute('condition') ;
      $this->setCondition($condition) ;
      $inner = $mlt->getInnerHtml();
      $this->replacePipe(' ###E### ' . $inner . ' ###S###' ) ;
      //echo 'replace : ' . $this->_condition . "\n" ;
      $replace = ' ###S### ' . $this->_condition . ' ###E### '  ;
      $mlt->replaceWith($replace) ;
    }

    $content = $page->saveHTML() ;
    $content = str_replace('###S###',' <% ',$content);
    $content = str_replace('###E###',' %> ',$content);

    $content = substr($content , 5 , strlen($content)-11);
    return trim($content) ;

  }


}
