<?php namespace App\Services\Proxy;

use Goutte\Client;
use Wa72\HtmlPageDom\HtmlPageCrawler;

class MonoloopProxy{
  private $content ;
  private $htmlObj ;
  private $url ;
  private $id ;

  private function baseHref(){
    $pathInfo = pathinfo($this->url);
    if($pathInfo['dirname'] != 'http:' && $pathInfo['dirname'] != 'https:')
      return $pathInfo['dirname'] . '/' ;
    return $this->url ;
  }

  private function baseDomain(){
    $pathInfo = parse_url($this->url);
    if(!isset($pathInfo['scheme'])){
      $pathInfo['scheme'] = 'http' ;
    }
    if(!isset($pathInfo['host'])){
      $pathInfo['host'] = $this->url ;
    }
    return $pathInfo['scheme'].'://'.$pathInfo['host'] ;
  }

  public function process($id ,$url){
    $this->id = $id ;
    $this->url = $url ;
    $client = new Client();
    $crawler = $client->request('GET',$url );
    $this->htmlObj = new HtmlPageCrawler('<html>'.$crawler->html().'</html>');
    #echo $this->htmlObj ; die() ;
    #$this->appendBaseHref() ;
  }

  public function appendBaseHref(){
    $base = $this->htmlObj->filter('base') ;
    $header = $this->htmlObj->filter('head') ;
    if(empty($base->html())){
      $header->setInnerHtml('<base href="'.$this->baseHref().'">' . $header->html());
    }
  }

  public function processHeader($includeAjax = true , $addionalScript){
    $header = $this->htmlObj->filter('head') ;
    if(!empty($header->html())){
      #base html
      $baseHtml = '' ;
      $base = $this->htmlObj->filter('base') ;
      if(empty($base->html())){
        $baseHtml = '<base href="'.$this->baseHref().'">' ;
      }
      $xhr = '' ;
      if( $includeAjax ){
        $xhr = '<script type="text/javascript" language="javascript">
          var monoloopBaseURL = \''.config('app.url').'/placement-proxy/'. $this->id. '?l='.'\' ;
          var monoloopClientURL1 = \''. $this->baseDomain()  . '/' . '\' ;
          var monoloopClientURL2 = \''. $this->baseHref(). '\' ;
          var monoloopClientURL3 = \'' . $this->baseHref() . '\' ;
          function changeXHROpen(prependURL){
            var oldOpen;
            oldOpen = XMLHttpRequest.prototype.open;
            // override the native open()
            XMLHttpRequest.prototype.open = function(){
              var pos = arguments[1].indexOf(\'http\') ;
              if( pos == -1){
              	var pos2 = arguments[1].indexOf(\'/\') ;
              	if ( pos2 == -1 ){
              		arguments[1] = "'.config('app.url').'/placement-proxy/'. $this->id.'?l="+monoloop_urlencode (prependURL+arguments[1] );
             		}else{
             			arguments[1] = "'.config('app.url').'/placement-proxy/'. $this->id. '?l="+monoloop_urlencode (monoloopClientURL1+arguments[1].substring(1) );
         			}

              }else{
                  arguments[1] = "'.config('app.url').'/placement-proxy/'. $this->id.'?l="+monoloop_urlencode (arguments[1] );
              }
              //Prepend our proxyURL

              // call the native open()
              oldOpen.apply(this, arguments);
            }
          }

          function monoloop_urlencode(str){
              return escape(str).replace(/\+/g,"%2B").replace(/%20/g, "+").replace(/\*/g, "%2A").replace(/\//g, "%2F").replace(/@/g, "%40");
          }

          //Change the xhr calls:
          changeXHROpen("'.$this->baseHref().'");

          function noError(){

                  return true;
              }

              window.onerror = noError;
          </script>';
      }

      $header->setInnerHtml($xhr . $baseHtml . $header->html() . $addionalScript);
    }
  }

  public function processForm(){
    $forms = $this->htmlObj->filter('form') ;
    foreach($forms as $form){
      $action = $form->getAttribute('action') ;
      if( $action == '' )
        continue ;
      $pos = stripos($action , 'http') ;
      if( $pos === false ){
        if( $action[0] == '?'){
        	$action = '' ;
        }else if( $action[0] == '/'){
          $action = substr($action , 1 , strlen($action)-1) ;
          $action = $this->baseDomain() . '/' . $action ;
        }else{
			     // Calculate from curernt html.
			   if( $this->url[strlen(self::$newBaseURL)-1] == '/' ){
				  $action = $this->url . $action ;
			   }else{
				  $list = explode( '/' , $this->url ) ;
				  unset($list[count($list) - 1] ) ;
				  $action = implode('/' , $list ) . '/' . $action ;
			   }
        }
      }
      $form->setAttribute('action' ,config('app.url'). '/placement-proxy' . $this->id . '?p='.urlencode( $action ) ) ;
    }
    return ;
  }

  public function processBody($additionData){
    $body = $this->htmlObj->filter('body' ) ;
    if(!empty($body)){
      $body->setInnerHtml(  $body->html() . $additionData);
    }
  }


  public function clean(){
    $body = $this->htmlObj->filter('body');
    $newBodyContent = str_replace('onclick="location.href=\'','onclick="location.href=\''. config('app.url').'/placement-proxy' . $this->id .'?l='.$this->baseHref()  ,$body->html());
    $body->setInnerHtml($newBodyContent) ;
  }

  public function render(){
    echo $this->htmlObj ;
    die() ;
  }
}
