<?php namespace App\Services;


class EjsFormat {

  private $_condition ;

  public function __construct(){
  }

  public static function html2Ejs($html){
      $html = preg_replace('/<div\sstyle="display:inline;color:green;" condition="(.+?)\| }">(.+?)<\/div>/is', "<% $1%>$2<%}%>", $html);
      return $html;
  }

  public static function ejs2Html($ejs){
      return $ejs;
  }

}
