<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class ReportingArchiveAggregated extends Eloquent
{
  protected $collection = 'ReportingArchiveAggregated';

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }
}
