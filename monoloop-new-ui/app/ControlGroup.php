<?php namespace App;


class ControlGroup {

	/**
	 * The list of significant actions
	 *
	 * @var array
	 */
	protected static $significantActions =  array('0' => 'Do nothing (continue experiment)', '1' => 'Serve Winner', '2' => 'Serve Original');


	public static function getSignificantActions()
	{
		return self::$significantActions;
	}
}
