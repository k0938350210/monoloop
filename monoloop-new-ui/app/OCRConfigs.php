<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class OCRConfigs extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mongodb';

	protected $collection = 'ocrConfigs';

  // protected $fillable = ['cid', 'ocReady'];

	public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }
}
