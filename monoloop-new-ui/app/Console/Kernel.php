<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\FrontendInvalidate' ,
		'App\Console\Commands\CbrCommand' ,
    'App\Console\Commands\CbrInvalidate',
    'App\Console\Commands\SubscribeMailChimp',
    'App\Console\Commands\SugarLead',
		'App\Console\Commands\FrontendInvalidateCommand',
		'App\Console\Commands\ExperimentAggregationReset',
		'App\Console\Commands\ContentToCode' ,
		'App\Console\Commands\ContentSave',
		'App\Console\Commands\TestCommand',
    'App\Console\Commands\PrivacyPatchCommand',
    'App\Console\Commands\ReportGenRandomCommand',
    'App\Console\Commands\PrivacyCompressCommand',
    'App\Console\Commands\DbPatchMissingCidCommand',
    'App\Console\Commands\FrontendServerRecheckStatusCommand'
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();
	}

}
