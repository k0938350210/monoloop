<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MonoloopServer;
use App\Services\Frontend\Invalidator ;

class FrontendInvalidateCommand extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'monoloop:frontend_invalidate {module} {option1}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Shortcut for process frontend invalidate';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $module = $this->argument('module');
    $option1 = $this->argument('option1');
    switch ($module){
      case 'list_frontend' :
        $monoloopServers = MonoloopServer::all() ;
        foreach($monoloopServers as $monoloopServer){
          $this->info($monoloopServer);
        }
      break;
      case 'endpoint' :
        $this->info(config('services.frontend.invalidate.endpoint'));
      break;
      case 'remove' :
        $this->info('Remove cached');
        $invalidator = new Invalidator() ;
        $invalidator->removeCached($option1,true) ;
      break ;
    }
  }
}
