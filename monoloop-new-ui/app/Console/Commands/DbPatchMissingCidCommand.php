<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ;
use App\Experiment ;
use App\Folder ;
use App\AppConfig ;
use App\Content ;
use App\ContentTemplate ;
use App\CustomField ;
use App\Goal ;
use App\Log ;
use App\Placement ;
use App\Tracker ;

class DbPatchMissingCidCommand extends Command
{
	protected $signature = 'monoloop:patch_cid';

  protected $description = 'patch missing cid';

  public function handle()
  {
  	$accounts = Account::all();
  	$experiment = new Experiment() ;
  	$folder = new Folder() ;
  	$appConfig = new AppConfig() ;
  	$content = new Content() ;
  	$contentTemplatee = new ContentTemplate() ;
  	$customField = new CustomField() ;
  	$goal = new Goal() ;
  	$log = new Log() ;
  	$placement = new Placement() ;
  	$tracker = new Tracker() ;

  	foreach ($accounts as $account) {
  		$this->info('Process account - ' . $account->uid );

  		//Experiment
  		$db = $experiment->getConnection()->getMongoDB() ;
  		$table = $experiment->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//Folder
  		$db = $folder->getConnection()->getMongoDB() ;
  		$table = $folder->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//AppConfig
  		$db = $appConfig->getConnection()->getMongoDB() ;
  		$table = $appConfig->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//Content
  		$db = $content->getConnection()->getMongoDB() ;
  		$table = $content->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//Content Template
  		$db = $contentTemplatee->getConnection()->getMongoDB() ;
  		$table = $contentTemplatee->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//Custom Field
  		$db = $customField->getConnection()->getMongoDB() ;
  		$table = $customField->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//Goal
  		$db = $goal->getConnection()->getMongoDB() ;
  		$table = $goal->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//Log
  		$db = $log->getConnection()->getMongoDB() ;
  		$table = $log->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//Placement
  		$db = $placement->getConnection()->getMongoDB() ;
  		$table = $placement->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);

  		//Tracker
  		$db = $tracker->getConnection()->getMongoDB() ;
  		$table = $tracker->getTable() ;
  		$db->$table->update(['account_id' => new \MongoId($account->id)],[ '$set' => [ 'cid' => $account->uid ]],['multiple' => true]);


  		//
  		//
  		/*
  		$experiment->getConnection()->getMongoDB()->collection($experiment->getTable())->where('account_id',new \MongoId($account->id))->update(['cid' => $account->uid ],['upsert' => true]);
  		#Experiment::all()->update(['cid' => $account->uid ],['upsert' => true]);
  		*/
  	}
  }
}