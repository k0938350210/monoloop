<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Storage ;


class PrivacyCompressCommand extends Command
{
	protected $signature = 'monoloop:privacy_compress';
	protected $description = 'Compress Privacy';

	public function __construct()
  {
    parent::__construct();
  }

  public function handle()
  {
  	$process = new Process('uglifyjs  ' . public_path('privacy_center/pc.js') .' --compress --mangle -o ' . public_path('privacy_center/pc.min.js' ));
	  $process->run();

	  $process = new Process('uglifycss  ' . public_path('privacy_center/pc.css') .' > ' . public_path('privacy_center/pc.min.css' ));
	  $process->run();

	  $process = new Process('uglifyjs  ' . public_path('privacy_center/pc2.js') .' --compress --mangle -o ' . public_path('privacy_center/pc2.min.js' ));
	  $process->run();

	  $process = new Process('uglifycss  ' . public_path('privacy_center/pc2.css') .' > ' . public_path('privacy_center/pc2.min.css' ));
	  $process->run();


	  $process = new Process('gzip  -9 ' . public_path('privacy_center/pc.min.js' ));
	  $process->run();
	  $process = new Process('mv ' . public_path('privacy_center/pc.min.js.gz' . ' ' . public_path('privacy_center/pc.min.js' ) ));
	  $process->run();

	  #Storage::disk('s3')->put( 'privacy_center/pc.min.js',file_get_contents(public_path('privacy_center/pc.min.js' )),'public');
	  Storage::disk('s3')->getDriver()->put('privacy_center/pc.min.js', file_get_contents(public_path('privacy_center/pc.min.js' )), [ 'visibility' => 'public', 'ContentEncoding' => 'gzip' , 'ContentType' => 'application/javascript']);


	  $process = new Process('gzip  -9 ' . public_path('privacy_center/pc2.min.js' ));
	  $process->run();
	  $process = new Process('mv ' . public_path('privacy_center/pc2.min.js.gz' . ' ' . public_path('privacy_center/pc2.min.js' ) ));
	  $process->run();

	  #Storage::disk('s3')->put( 'privacy_center/pc2.min.js',file_get_contents(public_path('privacy_center/pc2.min.js' )),'public');
	  Storage::disk('s3')->getDriver()->put('privacy_center/pc2.min.js', file_get_contents(public_path('privacy_center/pc2.min.js' )), [ 'visibility' => 'public', 'ContentEncoding' => 'gzip' , 'ContentType' => 'application/javascript']);

	  $process = new Process('gzip  -9 ' . public_path('privacy_center/pc.min.css' ));
	  $process->run();
	  $process = new Process('mv ' . public_path('privacy_center/pc.min.css.gz' . ' ' . public_path('privacy_center/pc.min.css' ) ));
	  $process->run();
	  #Storage::disk('s3')->put( 'privacy_center/pc.min.css',file_get_contents(public_path('privacy_center/pc.min.css' )),'public');
	  Storage::disk('s3')->getDriver()->put('privacy_center/pc.min.css', file_get_contents(public_path('privacy_center/pc.min.css' )), [ 'visibility' => 'public', 'ContentEncoding' => 'gzip' , 'ContentType' => 'text/css']);


	  $process = new Process('gzip  -9 ' . public_path('privacy_center/pc2.min.css' ));
	  $process->run();
	  $process = new Process('mv ' . public_path('privacy_center/pc2.min.css.gz' . ' ' . public_path('privacy_center/pc2.min.css' ) ));
	  $process->run();

	  #Storage::disk('s3')->put( 'privacy_center/pc2.min.css',file_get_contents(public_path('privacy_center/pc2.min.css' )),'public');
	  Storage::disk('s3')->getDriver()->put('privacy_center/pc2.min.css', file_get_contents(public_path('privacy_center/pc2.min.css' )), [ 'visibility' => 'public', 'ContentEncoding' => 'gzip' , 'ContentType' => 'text/css']);









	  //----asset
	  Storage::disk('s3')->put( 'images/ml-logo-small.png',file_get_contents(public_path('images/monoloop-logo-v2-small.png' )),'public');
	  Storage::disk('s3')->put( 'images/widget_icons_dark.png',file_get_contents(public_path('images/widget_icons_dark.png' )),'public');

  }
}