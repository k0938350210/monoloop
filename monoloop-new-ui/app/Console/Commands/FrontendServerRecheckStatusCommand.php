<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MonoloopServer;
use App\Services\Frontend\Invalidator ;

class FrontendServerRecheckStatusCommand extends Command
{
	protected $signature = 'monoloop:check_server_status';
	protected $description = 'Recheck inactive server';

	public function handle()
  {
  	$endpoint = config('services.frontend.invalidate.endpoint');
  	$hash = 'none' ;
  	$servers = MonoloopServer::where('status','Inactive')->get() ;
  	$invalidator = new Invalidator() ;
  	foreach ($servers as $server) {
  		$url = 'http://' . $server->ip . $endpoint . $hash ;
  		$this->info('check : ' . $url);

  		$response = $invalidator->curl($url) ;
  		$this->info('code : ' .  $response['info']['http_code'] ) ;

  		if($response['info']['http_code'] == 200 || $response['info']['http_code'] == 204){
  			$server->status = 'Active' ;
  			$server->save() ;
  		}
  	}
  }

}