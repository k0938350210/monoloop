<?php namespace App\Console\Commands;

use Illuminate\Console\Command; 
use DB ; 

class ProfilesConsolidateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monoloop:profiles_consolidate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate profile to new schema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start migrate profile version1');
        $profiles = DB::collection('profiles')->where('profiles','exists',false)->limit(5)->get(); 
        while(count($profiles) > 0 ){
          foreach($profiles as $profile){ 
            $this->processMigration($profile);
          }
          $profiles = DB::collection('profiles')->where('profiles','exists',false)->limit(5)->get(); 
        } 
        $this->info('End migrate profile version1');
    }
    
    public function processMigration($profile){
      $ret = [
        '_id' => $profile['_id'] ,  
        'customerID' => $profile['customerID'] , 
        'recently_used_device' => $profile['_id'] , 
        'events' => [] , 
        'cachedProperties' =>  isset($profile['cachedProperties'])?$profile['cachedProperties']:null ,  
        'profiles' => [$this->cleanUpProfile($profile)] , 
      ]; 
      
      $in = array_keys($ret);
      $all = array_keys($profile);
      $out = array_diff($all, $in);
      print_r($out);
      
      DB::collection('profiles')->where('_id',$ret['_id'])->update($ret);
      DB::collection('profiles')->where('_id',$ret['_id'])->unset($out);
    }
    
    public function cleanUpProfile($profile){
      return [
        '_id' => $profile['_id'] , 
        'aggregated' => null , 
        'fingerprint_hash' => null , 
        'device_info' => [
          'config_browser_language' => $profile['config_browser_language'],
          'config_browser_name' => $profile['config_browser_name'],
          'config_browser_version' => $profile['config_browser_version'],
          'config_cookie' => $profile['config_cookie'] ,
          'config_os' => $profile['config_os'] ,
          'config_resolution' => $profile['config_resolution'] ,
          'location_browser_lang' => $profile['location_browser_lang'],
          'location_city' => $profile['location_city'] ,
          'location_continent' => $profile['location_continent'] ,
          'location_country' => $profile['location_country'] ,
          'location_latitude' => $profile['location_latitude'] ,
          'location_longitude' => $profile['location_longitude'] ,
          'location_region' => $profile['location_region'] ,
          'location_timezone' => $profile['location_timezone'] , 
        ], 
        'visit_server_date' => $profile['visit_server_date'] , 
        'experiments' => $profile['experiments'], 
        'controlgroups' => $profile['controlgroups'], 
        'controlgroups' => $profile['controlgroups'],
        'goals' => $profile['goals'] , 
        'visits' => $profile['visits']
      ];
    }
 
}
