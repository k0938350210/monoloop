<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Experiment;


class ExperimentAggregationReset extends Command{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'monoloop:experiment-aggregation-reset {experiment_id}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Remove all aggregation data by experiment_id';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
      parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $experiment_id = $this->argument('experiment_id');
    $this->info('Process experiment remove affregation : ' . $experiment_id);
    $experiment = Experiment::where('experimentID' ,(int)$experiment_id)->first() ;
    if(is_null($experiment)){
      $this->error('Experiment not exists!');
      return ;
    }

    $experiment->unsetReportAggregated() ;
    #DB::collection('ReportingArchiveAggregated')->where('experiments.'.$experiment_id,'exists',true)->where('cid',(int)$experiment->account->uid )->unset('experiments.' . $experiment_id);
    $this->info('Finish experiment remove affregation : ' . $experiment_id);
  }
}
