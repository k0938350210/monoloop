<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Content ;

class ContentSave extends Command
{
  protected $signature = 'monoloop:contentsave {uid}';

  protected $description = 'save content -- test observer';

  public function __construct()
  {
      parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      $uid = $this->argument('uid');
      $content = Content::byUid($uid)->first() ;
      if($content){
        $content->save() ; 
      }
      $this->info('');
      $this->info('----');
  }
}
