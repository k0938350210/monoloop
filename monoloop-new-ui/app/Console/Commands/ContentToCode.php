<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Content ;

class ContentToCode extends Command
{
  protected $signature = 'monoloop:content2code {uid}';

  protected $description = 'Convert content config to code';

  public function __construct()
  {
      parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      $uid = $this->argument('uid');
      $content = Content::byUid($uid)->first() ;
      if($content){
        echo $content->code ;
      }
      $this->info('');
      $this->info('----');
  }
}
