<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account ; 
use App\Services\Account\Cbr ;

class CbrCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monoloop:cbr {cid}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Geneate cbr file by console';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cid = $this->argument('cid');
        if($cid == 'all'){
          $this->processAll();
        }else{
          $this->processSingle($cid) ; 
        }
    }
    
    private function processAll(){
      $cbrs = Account::byAvailable()->get() ; 
      $bar = $this->output->createProgressBar(count($cbrs));
      $this->info('Process cbr starts');
      $bar->setFormat('%current%/%max% %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% ' . "\n" ); 
      foreach($cbrs as $cbr){
        $cbrProcess = new Cbr($cbr) ; 
        $this->info('Cbr ['.$cbr->uid.'] in progress...');
        $cbrProcess->process() ; 
        $bar->advance(); 
      }
      $this->info('Process cbr finished');
      $bar->finish();
    }
    
    private function processSingle($cid){
      $account = Account::byUid($cid)->first() ; 
      #$this->info( (int)$account->isUnSafe ) ; 
      $this->info('Start process cid : ' . $cid ); 
      if(empty($account)){
        $this->error('Account not exists!');
      }else{
        $cbr = new Cbr($account) ; 
        $cbr->process() ; 
      }
      $this->info('End process cid : ' . $cid );
    }
}
