<?php

namespace App\Helpers;

use Auth;
use App\Account;

class UserAccounts {
  static function accounts(){
    $account_selectors = Auth::User()->account_for_selector();
    return $account_selectors;
  }
  static function activeAccount(){
    $account = Account::find(Auth::User()->active_account);
    return $account;
  }
}
