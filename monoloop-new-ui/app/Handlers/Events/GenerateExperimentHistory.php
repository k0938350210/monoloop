<?php

namespace App\Handlers\Events;

use App\ExperimentHistory;
use App\Events\ExperimentHistoryLog;
use Carbon\Carbon;

class GenerateExperimentHistory
{
    /**
     * Create the event handler.
     */
    public function __construct(ExperimentHistory $ExperimentHistory)
    {
        $this->experiment_history = $ExperimentHistory;
    }

    /**
     * Handle the event.
     *
     * @param ExperimentHistoryLog $event
     */
    public function handle(ExperimentHistoryLog $event)
    {
        if($event->isupdate){
            $this->updateExperiments($event);
        }else {
            $this->experiment_history->experimentID = !empty($event->experimentID) ? $event->experimentID : 0;
            $this->experiment_history->no_of_goals = !empty($event->no_of_goals) ? $event->no_of_goals : 0;
            $this->experiment_history->no_of_segments = !empty($event->no_of_segments) ? $event->no_of_segments : 0;
            $this->experiment_history->no_of_variation = !empty($event->no_of_variations) ? $event->no_of_variations : 0;
            $this->experiment_history->no_of_locations = !empty($event->no_of_locations) ? $event->no_of_locations : 0;
            $this->experiment_history->timestamp = $event->timestamp;
            $this->experiment_history->save();
        }
    }
    public function updateExperiments(ExperimentHistoryLog $event){
        $array=[
        "experimentID" => !empty($event->experimentID) ? $event->experimentID : 0,
        "no_of_goals" => !empty($event->no_of_goals) ? $event->no_of_goals : 0,
        "no_of_segments" => !empty($event->no_of_segments) ? $event->no_of_segments : 0,
        "no_of_variation" => !empty($event->no_of_variations) ? $event->no_of_variations : 0,
        "no_of_locations" => !empty($event->no_of_locations) ? $event->no_of_locations : 0,
        "timestamp" => Carbon::now()->timestamp
    ];
        $this->experiment_history->where('timestamp',">=",$event->timestamp)->update($array);

    }


}