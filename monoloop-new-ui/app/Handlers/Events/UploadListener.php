<?php namespace App\Handlers\Events;

use Unisharp\Laravelfilemanager\Events\ImageWasUploaded;

use App\Jobs\S3ResizingImage ;
use Illuminate\Foundation\Bus\DispatchesJobs;

use Storage;
use Auth ;
use File ;
use Image ;


class UploadListener{
  use DispatchesJobs;

	public function handle($event)
  {
    $method = 'on'.class_basename($event);
    if (method_exists($this, $method)) {
      call_user_func([$this, $method], $event);
    }
  }

  public function onImageWasUploaded(ImageWasUploaded $event)
	{
    $path = $event->path();
    $filename = $event->filename();

    //Do resizing and cropping process ;

    $this->dispatch(new S3ResizingImage($path,$filename));
    /*
    $attachment = array(
      'thumb' => array('w' => 130 , 'h' => 130 ) ,
      'medium' => array('w' => 300 , 'h' => 300 ) ,
      'large' => array('w' => 600 , 'h' => 600 ) ,
    ) ;

    $storePath_first = substr($storePath, 0 , strrpos($storePath, '.')  );
    $storePath_extension = substr($storePath, strrpos($storePath, '.') + 1);

    foreach($attachment as $key => $attach){

      $image = Image::make($path)->widen($attach['w']);
      $image->save(tempnam(sys_get_temp_dir(), 'Tux'));
      Storage::put( $storePath_first .'-' . $attach['w'] . '.' . $storePath_extension ,file_get_contents($image->basePath()), 'public');

    }
    */

  }
}