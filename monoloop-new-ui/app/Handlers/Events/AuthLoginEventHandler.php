<?php namespace App\Handlers\Events;

use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Session ;

class AuthLoginEventHandler
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Events  $event
     * @return void
     */
    public function handle(User $user, $remember)
    {

        $credentials = ['user_id' => $user->_id, 'by_id' => true, 'username' => $user->username, 'password' => ''];
        $response = $this->authenticateLogin($credentials);
        $header = $response['header'];
        $headers = $this->getHederInfo($header);

        $body = $response['body'];
        $resArr = array();
        $resArr = json_decode($body);
        if($resArr->is_success === 1){

          $ml_token = $headers['Ml-Token'];
          $ml_token = substr($ml_token, 0, -1);
          $ml_token = substr($ml_token,1);

          Session::set('mlToken', $ml_token);

        }
    }



  public function authenticateLogin($fields){
    $apihost = config('app.monoloop')['apihost'];
    $url = $apihost.'/api/user/login';
    $headers = ['Content-Type: application/json', 'ML-Version: v1.0'];

    $fields_string = json_encode($fields);
    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    //execute post
    $response = curl_exec($ch);
    $result = [];

    // Then, after your curl_exec call:
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $header_size);
    $result['body'] = substr($response, $header_size);
    //close connection
    curl_close($ch);
    return $result;
  }
  private function getHederInfo($header_string){
    $myarray=array();
    $data = explode("\n", $header_string);

    $myarray['status'] = $data[0];

    array_shift($data);

    foreach($data as $part){
      $middle = explode(":",$part);
      if(count($middle) === 2){
        $myarray[trim($middle[0])] = trim($middle[1]);
      }
    }
    return $myarray;
  }

}