<?php

namespace App\Handlers\Events;

use App\Events\Invitation;
use Mail;

class SendEmail
{
    /**
     * Create the event handler.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param Invitation $event
     */
    public function handle(Invitation $event)
    {
        switch ($event->type) {
          case 'account_user':

          $options = $event->options;

          Mail::send($options['layout'], $options, function ($message) use ($options) {
              $message->to($options['user']->email, $options['user']->name)->subject('Monoloop: User Invitation!');
          });

            break;

          default:
          $options = $event->options;

          Mail::send($options['layout'], $options, function ($message) use ($options) {
              $message->to($options['user']->email, $options['user']->name)->subject('Monoloop: User Invitation!');
          });
            break;
        }
        die('SENT');
    }
}
