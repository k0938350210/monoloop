<?php

namespace App\Handlers\Events;

use Auth;
use App\Events\Logger;
use App\Log;
use Carbon\Carbon;

class GenerateLogger
{
    /**
     * Create the event handler.
     */
    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    /**
     * Handle the event.
     *
     * @param Logger $event
     */
    public function handle(Logger $event)
    {
        if(Auth::User()->account){
          $this->log->account_id = new \MongoId(Auth::User()->account->_id);
          $this->log->cid = Auth::User()->account->uid;
        }
        $this->log->user_id = new \MongoId(Auth::User()->_id);

        $this->log->ip = $this->ip();
        $this->log->logable_type = !empty($event->logableType) ? $event->logableType : null;
        $this->log->logable_id = $event->logableId;
        $exMsg = !empty($event->extenededMessage) ?  ' ['.$event->extenededMessage.']' : "";
        $this->log->msg = trim('['.$event->username.']'.' '.$event->message.' '.$exMsg);
        $this->log->save();
    }

      /**
       * find the ip
       *
       * @return string
       */
      public function ip()
      {
          $ipaddress = '';
          if (getenv('HTTP_CLIENT_IP')) {
              $ipaddress = getenv('HTTP_CLIENT_IP');
          } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
              $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
          } elseif (getenv('HTTP_X_FORWARDED')) {
              $ipaddress = getenv('HTTP_X_FORWARDED');
          } elseif (getenv('HTTP_FORWARDED_FOR')) {
              $ipaddress = getenv('HTTP_FORWARDED_FOR');
          } elseif (getenv('HTTP_FORWARDED')) {
              $ipaddress = getenv('HTTP_FORWARDED');
          } elseif (getenv('REMOTE_ADDR')) {
              $ipaddress = getenv('REMOTE_ADDR');
          } else {
              $ipaddress = 'UNKNOWN';
          }

          return $ipaddress;
      }
}
