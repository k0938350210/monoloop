<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class AppConfig extends Eloquent{

	protected $connection = 'mongodb';

	public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'appsConfig';


	/**
	 * the account ID used by the app config collection
	 * @return void
	 */
	public function account()
  {
    return $this->belongsTo('\App\Account','account_id');
  }
}
