<?php  namespace App\Observers;

use App\Content ;
use App\PageElement ;
use Illuminate\Foundation\Bus\DispatchesJobs ;

class ContentObserver {

  public function saved(Content $content)
  {
    $pageElements =  PageElement::where('content.uid',(int)$content->uid)->get() ;
    $cid = 0 ;
    foreach($pageElements as $pageElement){
      $contents = $pageElement->content ;
      $found_contents = $contents->where('uid',(int)$content->uid);
      foreach($found_contents as $found_content){
        if($content->deleted){
          $found_content->delete() ;
        }else{
          $found_content->code = $content->code ;
          $found_content->save() ;
        }

      }
    }
  }

}
