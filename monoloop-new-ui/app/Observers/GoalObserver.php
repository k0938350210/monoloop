<?php  namespace App\Observers;

use App\Goal ;
use App\PageElement ;
use App\PageElementContent ;

class GoalObserver {
  public function saved(Goal $goal)
  {

    if($goal->page_element_id == null){
      return ;
    }

    $pageElements =  PageElement::where('content.uid',(int)$goal->uid)->get() ;
    foreach($pageElements as $pageElement){
      $contents = $pageElement->content ;
      $found_contents = $contents->where('uid',(int)$goal->uid);
      foreach($found_contents as $found_content){
        $found_content->delete() ;
      }
    }

    if($goal->deleted || $goal->hidden){
      return ;
    }

    $pageElement = PageElement::find($goal->page_element_id);
    if($pageElement == null){
      return ;
    }


    $content = new PageElementContent();
    $content->goal = true ;
    $content->p = (int)$goal->point ;
    $content->code = $goal->code ;
    $content->uid = (int)$goal->uid ;

    $pageElement->content()->save($content);
  }

}
