<?php  namespace App\Observers;

use App\PageElement ;
use App\Services\Frontend\Invalidator ;
use App\Jobs\FrontendInvalidate;
use Illuminate\Foundation\Bus\DispatchesJobs ;

class PageElementObserver {
  use DispatchesJobs;

  public function saved(PageElement $pageElement)
  {
    #invalidate frontend server
    $this->dispatch(new FrontendInvalidate($pageElement->cid  . '_pageelements'));
  }

}
