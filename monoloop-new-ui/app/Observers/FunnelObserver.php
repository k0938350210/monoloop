<?php  namespace App\Observers;

use App\Funnel;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Auth;

class FunnelObserver{

  public function creating(Funnel $funnel){
    $funnel->uid = Funnel::max('uid') + 1;
    $funnel->hidden = 0;
    $funnel->deleted = 0;
  }

}
