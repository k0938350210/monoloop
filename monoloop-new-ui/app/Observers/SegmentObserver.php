<?php  namespace App\Observers;

use App\Segment ;
use App\PageElement ;
use App\Services\Frontend\Invalidator ;
use App\Jobs\FrontendInvalidate;
use Illuminate\Foundation\Bus\DispatchesJobs ;
use Auth ;

class SegmentObserver {
  use DispatchesJobs;

  public function creating(Segment $segment){
    $segment->uid = Segment::max('uid') + 1;
  }

  public function saved(Segment $segment)
  {
    #invalidate frontend server
    $invalidator = new Invalidator() ;
    $this->dispatch(new FrontendInvalidate($segment->account->uid . '_segments'));
    $pageElements = PageElement::where('content.segments.segment_'.$segment->uid, 'exists', true)->get();

    if(count($pageElements) > 0){
      $this->dispatch(new FrontendInvalidate($pageElements[0]->cid  . '_pageelements_'));
    }
  }

}
