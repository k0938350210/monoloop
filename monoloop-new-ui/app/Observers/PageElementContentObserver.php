<?php  namespace App\Observers;

use App\PageElementContent ;
use App\Services\ConditionBuilder\Syntax ;
use App\Services\MonoloopCondition ;
use App\Segment ;

class PageElementContentObserver {

  public function saving(PageElementContent $pageElementContent)
  {
  	//connectors
  	$syntax = new Syntax() ;
  	$pageElementContent->connectors = $syntax->getConnector($pageElementContent->code) ;
  	//segments ;
  	$this->generateSegmentField($pageElementContent);

  }

  private function generateSegmentField(PageElementContent $pageElementContent){
  	//segment
  	$content = $pageElementContent->code ;
  	$pos = 0 ;
		$pos = strpos($content , 'Segments.segment_' , $pos) ;
		$segments = array() ;

		while( $pos !== false){

			$pos2 = strpos($content , '(' , $pos) ;
			if( $pos2 !== false){
				$segments[] = substr($content , $pos + 17 , $pos2 - $pos - 17) ;
			}
			$pos++ ;
			$pos = strpos($content , 'Segments.segment_' , $pos) ;
		}

		$segment_page = [] ;

		$syntax = new Syntax() ;

		if( count($segments) > 0){

			foreach( $segments as $segmentUid){
				$segmentObj = Segment::byUid($segmentUid)->first() ;
        $conditionObj = new MonoloopCondition($segmentObj->condition);
		    $conditionObj->replacePipe( '  return true ;  ') ;
		    $segment_page['segment_' . $segmentObj->uid] = ' <%  Segments[\'segment_' . $segmentObj->uid.'\']  = function(){ '.$conditionObj .'  return false ;};  %> ' ;
			}
		}
		if(!empty($segment_page)){
			$pageElementContent->segments=$segment_page ;
		}else{
			$pageElementContent->segments = null ;
		}

  }

}
