<?php
/**
 * Created by PhpStorm.
 * User: marslan
 * Date: 8/16/16
 * Time: 5:48 PM
 */

namespace App\Events;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Carbon\Carbon;
class ExperimentHistoryLog extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($experimentID,$goals,$segments,$variations,$locations,$update=0,$timestamp=0)
    {

        $this->experimentID = $experimentID;
        $this->no_of_goals = $goals;
        $this->no_of_segments  = $segments;
        $this->no_of_variations  = $variations;
        $this->no_of_locations  = $locations;
        if($timestamp){
            $this->timestamp=$timestamp;
        }else{
            $this->timestamp = Carbon::now()->timestamp;
        }
        $this->isupdate  = $update;

    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }


}