<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class UserRightCategory extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'user_right_categories';

	/**
	 * the right category ID has many rights
	 * @return void
	 */
	public function rights()
  {
    return $this->hasMany('\App\UserRight','user_right_category_id');
  }

}
