<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use App\Segment;
use App\Tracker;
use App\Experiment;
use App\Blueprint;
use App\Goal;
use App\Placement;
use App\PageElement;
use App\Content;
use App\Account ;
use MongoId;

class Folder extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $collection = 'folders';

    /*
     * all the folders
     *
     * @param account
     * @rerurn array
     */
    public function folders($account = NULL){
      $folders = Folder::where('account_id', '=', $account)->where('deleted', '=', 0)->get();
      return $folders;
    }

    /*
     * The folders hierarchy with their childern for an account
     *
     * @param account Object
     * @rerurn array
     */
    public function segmentsFolders($cid = NULL){

    	$tree = [];
      $segment = new Segment();

    	$folders = Folder::where('cid', '=', $cid)->where('deleted', '=', 0)->where('parent_id', '=', NULL)->get();
    	// $folders = Folder::where('account_id', '=', $account)->get();

    	foreach ($folders as $key => $folder) {
	         $node = new \stdClass();
	         $node->id = "folder__".$folder->_id;
	         $node->value = $folder->title;
	         $node->open = true;
	         $node->type = "folder";
           $node->webix_files = 1;
          //  $node->webix_branch = 1;
           // hidden showes if it's inactive
           $node->hidden = $folder->attributes['hidden'];
	         $node->date = date('j F, Y', strtotime($folder->created_at));
	         $node->data = $this->buildTreeSegment($folder);
           // merging segments associated with this folder

    	     array_push($tree, $node);
    	}

    	return $tree;
    }

    /*
     * The folders hierarchy with their childern for an account
     *
     * @param elements Object
     * @rerurn array
     */
    private function buildTreeSegment($node){

    	$branch = [];
      $segment = new Segment();
      $childFolders = Folder::where('parent_id', '=', new \MongoId($node->_id))->where('deleted', '=', 0)->get();

	    foreach ($childFolders as $cFkey => $childFolder) {
            $childNode = new \stdClass();
            $childNode->id = "folder__".$childFolder->_id;
            $childNode->value = $childFolder->title;
            $childNode->open = false;
            $childNode->type = "folder";
            $childNode->webix_files = 1;
            // $childNode->webix_branch = 1;
            // hidden showes if it's inactive
            $childNode->hidden = $childFolder->attributes['hidden'];
            $childNode->date = date('j F, Y', strtotime($childFolder->created_at));
            $childNode->data = $this->buildTreeSegment($childFolder);

	    	    array_push($branch, $childNode);
	    }

	    return $branch;
    }


    /*
     * The folders hierarchy with their childern for an account
     *
     * @param account Object
     * @rerurn array
     */
    public function trackersFolders($account = NULL){

    	$tree = [];

    	$folders = Folder::where('account_id', '=', $account)->where('deleted', '=', 0)->where('parent_id', '=', NULL)->get();
    	// $folders = Folder::where('account_id', '=', $account)->get();


    	foreach ($folders as $key => $folder) {
	         $node = new \stdClass();
	         $node->id = "folder__".$folder->_id;
	         $node->value = $folder->title;
	         $node->open = true;
	         $node->type = "folder";
           $node->webix_files = 1;
           // hidden showes if it's inactive
           $node->hidden = $folder->attributes['hidden'];
	         $node->date = date('j F, Y', strtotime($folder->created_at));
	         $node->data = $this->buildTreeTracker($folder);
           // merging segments associated with this folder
    	     array_push($tree, $node);
    	}

    	return $tree;
    }


    /*
     * The folders hierarchy with their childern for an account
     *
     * @param elements Object
     * @rerurn array
     */
    private function buildTreeTracker($node){

    	$branch = [];
      $childFolders = Folder::where('parent_id', '=', new \MongoId($node->_id))->where('deleted', '=', 0)->get();

	    foreach ($childFolders as $cFkey => $childFolder) {
            $childNode = new \stdClass();
            $childNode->id = "folder__".$childFolder->_id;
            $childNode->value = $childFolder->title;
            $childNode->open = false;
            $childNode->type = "folder";
            $childNode->webix_files = 1;
            // hidden showes if it's inactive
            $childNode->hidden = $childFolder->attributes['hidden'];
            $childNode->date = date('j F, Y', strtotime($childFolder->created_at));
            $childNode->data = $this->buildTreeTracker($childFolder);
            // merging segments associated with this folder
	    	array_push($branch, $childNode);
	    }

	    return $branch;
    }


    /*
     * The folders hierarchy with their childern for an account
     *
     * @param account Object
     * @rerurn array
     */
    public function experimentsFolders($cid = NULL){

      $tree = [];
      $folderChild = new Experiment();

      $folders = Folder::where('cid', '=', $cid)->where('deleted', '=', 0)->where('parent_id', '=', NULL)->get();
      // $folders = Folder::where('account_id', '=', $account)->get();


      foreach ($folders as $key => $folder) {
           $node = new \stdClass();
           $node->id = "folder__".$folder->_id;
           $node->value = $folder->title;
           $node->open = true;
           $node->type = "folder";
           $node->webix_files = 1;
           // hidden showes if it's inactive
           $node->hidden = $folder->attributes['hidden'];
           $node->date = date('j F, Y', strtotime($folder->created_at));
           $node->data = $this->buildTreeExperiment($folder);
           // merging segments associated with this folder
          //  $node->data = array_merge(isset($node->data) ? $node->data : [], $folderChild->getFolderItems($folder->_id, true, $account));
           array_push($tree, $node);
      }

      return $tree;
    }


    /*
     * The folders hierarchy with their childern for an account
     *
     * @param elements Object
     * @rerurn array
     */
    private function buildTreeExperiment($node){

      $branch = [];
      $tracker = new Experiment();
      $childFolders = Folder::where('parent_id', '=', new \MongoId($node->_id))->where('deleted', '=', 0)->get();

      foreach ($childFolders as $cFkey => $childFolder) {
            $childNode = new \stdClass();
            $childNode->id = "folder__".$childFolder->_id;
            $childNode->value = $childFolder->title;
            $childNode->open = false;
            $childNode->type = "folder";
            $childNode->webix_files = 1;
            // hidden showes if it's inactive
            $childNode->hidden = $childFolder->attributes['hidden'];
            $childNode->date = date('j F, Y', strtotime($childFolder->created_at));
            $childNode->data = $this->buildTreeExperiment($childFolder);
            // merging segments associated with this folder
            // $childNode->data = array_merge($childNode->data, $tracker->getFolderItems($childFolder->_id));
            array_push($branch, $childNode);
      }

      return $branch;
    }


    /*
     * The folders hierarchy with their childern for an account
     *
     * @param account Object
     * @rerurn array
     */
    public function blueprintsFolders($account = NULL){

      $tree = [];
      $folderChild = new Blueprint();

      $folders = Folder::where('account_id', '=', $account)->where('deleted', '=', 0)->where('parent_id', '=', NULL)->get();
      // $folders = Folder::where('account_id', '=', $account)->get();


      foreach ($folders as $key => $folder) {
           $node = new \stdClass();
           $node->id = "folder__".$folder->_id;
           $node->value = $folder->title;
           $node->open = true;
           $node->type = "folder";
           $node->webix_files = 1;
           // hidden showes if it's inactive
           $node->hidden = $folder->attributes['hidden'];
           $node->date = date('j F, Y', strtotime($folder->created_at));
           $node->data = $this->buildTreeExperiment($folder);
           // merging segments associated with this folder
           $node->data = array_merge(isset($node->data) ? $node->data : [], $folderChild->getFolderItems($folder->_id, true, $account));
           array_push($tree, $node);
      }

      return $tree;
    }


    /*
     * The folders hierarchy with their childern for an account
     *
     * @param elements Object
     * @rerurn array
     */
    private function buildTreeBlueprint($node){

      $branch = [];
      $blueprint = new Blueprint();
      $childFolders = Folder::where('parent_id', '=', new \MongoId($node->_id))->where('deleted', '=', 0)->get();

      foreach ($childFolders as $cFkey => $childFolder) {
            $childNode = new \stdClass();
            $childNode->id = "folder__".$childFolder->_id;
            $childNode->value = $childFolder->title;
            $childNode->open = false;
            $childNode->type = "folder";
            $childNode->webix_files = 1;
            // hidden showes if it's inactive
            $childNode->hidden = $childFolder->attributes['hidden'];
            $childNode->date = date('j F, Y', strtotime($childFolder->created_at));
            $childNode->data = $this->buildTreeBlueprint($childFolder);
            // merging segments associated with this folder
            $childNode->data = array_merge($childNode->data, $blueprint->getFolderItems($childFolder->_id));
        array_push($branch, $childNode);
      }

      return $branch;
    }
    /*
     * The folders hierarchy with their childern for an account
     *
     * @param account Object
     * @rerurn array
     */
    public function goalsFolders($account = NULL){
      $tree = [];
      $folders = Folder::where('account_id', '=', $account)->where('deleted', '=', 0)->where('parent_id', '=', NULL)->get();
      // $folders = Folder::where('account_id', '=', $account)->get();
      foreach ($folders as $key => $folder) {
           $node = new \stdClass();
           $node->id = "folder__".$folder->_id;
           $node->value = $folder->title;
           $node->open = true;
           $node->type = "folder";
           $node->webix_files = 1;
           // hidden showes if it's inactive
           $node->hidden = $folder->attributes['hidden'];
           $node->date = date('j F, Y', strtotime($folder->created_at));
           $node->data = $this->buildTreeGoal($folder);
           // merging segments associated with this folder
           array_push($tree, $node);
      }
      return $tree;
    }
    /*
     * The folders hierarchy with their childern for an account
     *
     * @param elements Object
     * @rerurn array
     */
    private function buildTreeGoal($node){
      $branch = [];
      $childFolders = Folder::where('parent_id', '=', new \MongoId($node->_id))->where('deleted', '=', 0)->get();
      foreach ($childFolders as $cFkey => $childFolder) {
            $childNode = new \stdClass();
            $childNode->id = "folder__".$childFolder->_id;
            $childNode->value = $childFolder->title;
            $childNode->open = false;
            $childNode->type = "folder";
            $childNode->webix_files = 1;
            // hidden showes if it's inactive
            $childNode->hidden = $childFolder->attributes['hidden'];
            $childNode->date = date('j F, Y', strtotime($childFolder->created_at));
            $childNode->data = $this->buildTreeGoal($childFolder);
            // merging segments associated with this folder
            array_push($branch, $childNode);
      }
      return $branch;
    }
    /*
     * The folders hierarchy with their childern for an account
     *
     * @param account Object
     * @rerurn array
     */
    public function placementsFolders($cid = NULL){
        $tree = [];

        $folders = Folder::where('cid', '=', $cid)->where('deleted', '=', 0)->where('parent_id', '=', NULL)->get();
        // $folders = Folder::where('account_id', '=', $account)->get();

        foreach ($folders as $key => $folder) {
             $node = new \stdClass();
             $node->id = "folder__".$folder->_id;
             $node->value = $folder->title;
             $node->open = true;
             $node->type = "folder";
             $node->webix_files = 1;
             // hidden showes if it's inactive
             $node->hidden = $folder->attributes['hidden'];
             $node->date = date('j F, Y', strtotime($folder->created_at));
             $node->data = $this->buildTreePlacement($folder);
             // merging segments associated with this folder
             array_push($tree, $node);
        }

        return $tree;
      }


      /*
       * The folders hierarchy with their childern for an account
       *
       * @param elements Object
       * @rerurn array
       */
      private function buildTreePlacement($node){

        $branch = [];
        $childFolders = Folder::where('parent_id', '=', new \MongoId($node->_id))->where('deleted', '=', 0)->get();

        foreach ($childFolders as $cFkey => $childFolder) {
              $childNode = new \stdClass();
              $childNode->id = "folder__".$childFolder->_id;
              $childNode->value = $childFolder->title;
              $childNode->open = false;
              $childNode->type = "folder";
              $childNode->webix_files = 1;
              // hidden showes if it's inactive
              $childNode->hidden = $childFolder->attributes['hidden'];
              $childNode->date = date('j F, Y', strtotime($childFolder->created_at));
              $childNode->data = $this->buildTreePlacement($childFolder);
              // merging segments associated with this folder
              array_push($branch, $childNode);
        }

        return $branch;
      }

      /*
       * The folders hierarchy with their childern for an account
       *
       * @param account Object
       * @rerurn array
       */
      public function webContentListFolders($account = NULL){

        $tree = [];
        $folderChild = new Content();

        $folders = Folder::where('account_id', '=', $account)->where('deleted', '=', 0)->where('parent_id', '=', NULL)->get();
        // $folders = Folder::where('account_id', '=', $account)->get();


        foreach ($folders as $key => $folder) {
             $node = new \stdClass();
             $node->id = "folder__".$folder->_id;
             $node->value = $folder->title;
             $node->open = true;
             $node->type = "folder";
             $node->webix_files = 1;
             // hidden showes if it's inactive
             $node->hidden = $folder->attributes['hidden'];
             $node->date = date('j F, Y', strtotime($folder->created_at));
             $node->data = $this->buildTreeWebContentList($folder);
             // merging segments associated with this folder
             $node->data = array_merge(isset($node->data) ? $node->data : [], $folderChild->getFolderItems($folder->_id, true, $account));
             array_push($tree, $node);
        }

        return $tree;
      }


      /*
       * The folders hierarchy with their childern for an account
       *
       * @param elements Object
       * @rerurn array
       */
      private function buildTreeWebContentList($node){

        $branch = [];
        $content = new Content();
        $childFolders = Folder::where('parent_id', '=', new \MongoId($node->_id))->where('deleted', '=', 0)->get();

        foreach ($childFolders as $cFkey => $childFolder) {
              $childNode = new \stdClass();
              $childNode->id = "folder__".$childFolder->_id;
              $childNode->value = $childFolder->title;
              $childNode->open = false;
              $childNode->type = "folder";
              $childNode->webix_files = 1;
              // hidden showes if it's inactive
              $childNode->hidden = $childFolder->attributes['hidden'];
              $childNode->date = date('j F, Y', strtotime($childFolder->created_at));
              $childNode->data = $this->buildTreeWebContentList($childFolder);
              // merging segments associated with this folder
              $childNode->data = array_merge($childNode->data, $content->getFolderItems($childFolder->_id));
          array_push($branch, $childNode);
        }

        return $branch;
      }
    /*--- Scope ---*/

  	public function scopeRoot($query, $account_id){
  		return $query->where('account_id', '=' , new MongoId($account_id))->where('uid',-1)->where('title','root') ;
  	}

    public function scopeByAccount($query, $account)
    {
      return $query->where('account_id', '=', new MongoId($account->_id));
    }

    public function scopeByNode($query, $node){
      if(is_null($node))
        return $query->where('parent_id', '=', null);
      return $query->where('parent_id', '=',  new MongoId($node));
    }

    public function scopeRootFolders($query, $account){
      return $query->where('account_id', '=', $account)->where('deleted', '=', 0)->where('parent_id', '=', NULL);
    }

    public function scopeByAvailable($query){
      return $query->where('hidden', '=', 0 )->where('deleted' , '=' , 0);
    }
}
