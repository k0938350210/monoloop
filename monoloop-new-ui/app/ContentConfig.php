<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use App\Services\EjsFormat;

class ContentConfig extends Eloquent{
  protected $fillable = ['content', 'content_type', 'image_link'];

  public function setContent(){
    if(!empty($this->content)){
      $this->content = EjsFormat::html2Ejs($this->content);
    }
  }


  public function toArray(){
    $array = parent::toArray();
    try{
      $array['content'] = $this->content ;
    }catch (Exception $e) {
      ;
    }
    return $array ;
  }

}
