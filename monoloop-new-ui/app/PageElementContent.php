<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;

use App\Services\EjsFormat;

class PageElementContent extends Eloquent{

  /*--- relations ---*/
  public function content()
  {
    return $this->belongsTo('\App\PageElement','content_id');
  }

  public function setCode(){
    if(!empty($this->code)){
      $this->code = EjsFormat::html2Ejs($this->code);
    }
  }

  /*---- attribute ----*/
  public function getExtractConditionAttribute(){
    $regex_all		= '/<%\s*.*?%>/si';
    $matches 		= array();
    $count_matches	= preg_match_all($regex_all,  $this->code  ,$matches);
    if(isset($matches[0])){
      return implode('',$matches[0]);
    }
    return '';
  }
}
