<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use App\Services\ConditonBuilder\ConditionalContentReengineer;

class Segment extends Eloquent {

  protected $fillable = ['name', 'description', 'condition' , 'condition_json' , 'hidden' , 'deleted'];

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }


  /**
   * The database table used by the model.
   *
   * @var string
   */
    protected $collection = 'Segments';



    /*---- relation ----*/

    public function account(){
      return $this->belongsTo(\App\Account::class,'account_id');
    }
    /*
     * The Segments associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public static function segments($folder = NULL){

      $branch = [];

      $segments = Segment::where('folder_id', '=', new \MongoId($folder))
                  ->where('deleted', '=', 0)
                  ->where('embedded', '<>', 1)
                  ->get();

        foreach ($segments as $key => $segment) {
            $node = new \stdClass();
            $node->id = "segment__".$segment->_id;
            $node->value = $segment->name;
            $node->type = "file";
            $node->description = (strlen($segment->description) < 50 ? $segment->description : substr($segment->description, 0,47).'...');
            $node->date = date('j F, Y', strtotime($segment->created_at));
            $node->hidden = $segment->attributes['hidden'];
            $node->condition = $segment->condition;
            $node->segmentid = $segment->uid;
            array_push($branch, $node);
      }

      $res['data'] = $branch;
      $res['parent'] = 'folder__'.$folder;

      return $res;
    }

    public function getConditionJson(){

      $conditionStr = $this->condition;

      $engineerConditionBuilder = new ConditionalContentReengineer();

      $conditionStr = $engineerConditionBuilder->prepareCondition($conditionStr) ;
      $conditionStr = $engineerConditionBuilder->extendBraket($conditionStr) ;

      $condition = $engineerConditionBuilder->splitAndOr($conditionStr) ;

      return $condition;
    }

    /*
     * The Segments associated with account_id
     *
     * @param $query
     * @param $account_id
     *
     * @rerurn array
     */
    public function scopeCompactList($query , $cid){
      return $query->where('cid', '=', $cid)->where('deleted', '<>', 1 )->where('embedded', '<>', '1')->get();
    }

    public function scopeByAccount($query , $account_id){
      return $query->where('account_id', '=', new \MongoId($account_id));
    }

    public function scopeFolderSegment($query , $folder_id){
      return $query->where('folder_id', '=', new \MongoId($folder_id));
    }

    public function scopeByUid($query,$uid){
      return $query->where('uid',(int)$uid);
    }

    public function scopeByCid($query,$cid){
      return $query->where('cid',(int)$cid);
    }

    public function scopeByName($query,$name){
      return $query->where('name',$name);
    }

    public function scopeBySystem($query){
      return $query->where('system',1);
    }

    /*
    * Returns segments with embedded flag to TRUE
    */
    public function scopeEmbeddedOnly($query, $cid){
      return $query
      ->where('cid', '=', $cid)
      ->where('deleted', '<>', 1 )
      ->where('embedded', '=', 1)
      ->get();
    }

    public function scopeGetAll($query, $cid){
      return $query
      ->where('cid', '=', $cid)
      ->where('deleted', '<>', 1 )
      // ->where('embedded', '=', 1)
      ->get();
    }
}
