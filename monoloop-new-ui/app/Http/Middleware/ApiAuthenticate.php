<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;


use App\User;
use App\Account;
use App\Services\Hasher;
use App\AccountUser ;

class ApiAuthenticate{

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
    $this->auth->logout() ;

    #--- Auth with basic authen ( old way api auth ) ---
    $user = $request->getUser() ;
    $pass = $request->getPassword() ;
    if ($this->auth->guest()){
    	#--- Auth with token authen ( new way api auth ) ---
			$account = Account::byUid($user)->first() ;
			$hasher = new Hasher() ;
			if(!empty($account) &&  $hasher->generateSign($account->uid) == $pass){
				$userAccount = AccountUser::byAccountId($account->_id)->byMonoloopAdmin(1)->first() ;
				if(!empty($userAccount)){
					$this->auth->login($userAccount->user) ;
				}
			}
    }
	  #$this->auth->login(User::first());

    if ($this->auth->guest()){
      return response('Unauthorized.', 401);
    }

		return $next($request);
	}

}
