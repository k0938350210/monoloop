<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\User;
use App\AccountUser;
use Auth;

use Request;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$isAjax = (bool) $request->input('ajax', false);
		if ($this->auth->guest())
		{
			$token = $request->header('ML-Token', '');


			if($token && !empty($token)){
				$response = $this->authenticateMlToken($token);
				$header = $response['header'];

				$headers = $this->getHederInfo($header);

				$body = $response['body'];
				$resArr = array();
				$resArr = json_decode($body);



				if($resArr->is_success === 1){
					$user_id = \App\Services\Redis::getUserIdFromRedisToken("a".$token);
					$this->auth->onceUsingId($user_id);
					return $next($request);
				} else {

					if ($request->ajax())
					{
						return response('Unauthorized.', 401);
					}
					else
					{
						return redirect()->guest('auth/login');
					}
				}
			} else {

				if ($isAjax)
				{
					return response('Unauthorized.', 401);
				}
				else
				{
					return redirect()->guest('auth/login');
				}
			}


		}else{
			// Recheck for valid active account;
			if(!$this->checkValidActiveAccount()){
				$this->auth->logout();
				if ($isAjax)
				{
					return response('Unauthorized.', 401);
				}
				else
				{
					return redirect()->guest('auth/login');
				}
			}
		}

		return $next($request);
	}

	private function checkValidActiveAccount(){
		$account_users = AccountUser::byUserId($this->auth->user()->id)->get();
		$valid = true;

		if( is_null($this->auth->user()->active_account)){
			$access_account_user = null;
			foreach($account_users as $account_user){
				if($account_user->isAccountValid()){
					$access_account_user = $account_user;
					break;
				}
			}
			$user = $this->auth->user();
			if($access_account_user){
				$user->active_account = new \MongoId($access_account_user->account_id);
				$user->save();
				return true;
			}else{
				return false;
			}

		}

		$match_account_user = $account_users->where('account_id',(string)$this->auth->user()->active_account)->first();
		if(!is_null($match_account_user) && !$match_account_user->isAccountValid()){
			$match_account_user->hidden = true;
			$match_account_user->save();
			// Find first accessable account

			$access_account_user = null;
			foreach($account_users as $account_user){
				if($account_user->isAccountValid()){
					$access_account_user = $account_user;
					break;
				}
			}
			$user = $this->auth->user();
			if($access_account_user){
				$user->active_account = new \MongoId($access_account_user->account_id);
			}else{
				$user->active_account = null;
				$valid = false;
			}
			$user->save();

		}

		return $valid;
	}


	private function getHederInfo($header_string){
		$myarray=array();
		$data = explode("\n", $header_string);

		$myarray['status'] = $data[0];

		array_shift($data);

		foreach($data as $part){
			$middle = explode(":",$part);
			if(count($middle) === 2){
				$myarray[trim($middle[0])] = trim($middle[1]);
			}
		}
		return $myarray;
	}

	private function authenticateMlToken($token){
		$apihost = config('app.monoloop')['apihost'];
		$url = $apihost.'/api/user/validate';
		$headers = ['Content-Type: application/json', 'ML-Version: v1.0', 'ML-Token: '.$token];

		$fields_string = json_encode([]);

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count([]));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		//execute post
		$response = curl_exec($ch);
		$result = [];

		// Then, after your curl_exec call:
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$result['header'] = substr($response, 0, $header_size);
		$result['body'] = substr($response, $header_size);

		//close connection
		curl_close($ch);
		return $result;
	}
}
