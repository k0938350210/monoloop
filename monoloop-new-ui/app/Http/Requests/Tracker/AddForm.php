<?php

namespace App\Http\Requests\Tracker;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class AddForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'source' => 'required',
          'nameTracker' => 'required|max:255',
          'typeTracker' => 'required',
      ];
    }

    /**
     * Get the proper failed validation response for the request.
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $res['status'] = 422;
        $res['message'] = 'Unprocessable Request';
        $res['errors'] = $errors;

        return new JsonResponse($res);
    }
}
