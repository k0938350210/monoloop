<?php namespace App\Http\Requests\CustomField;

use App\Http\Requests\Request;
use Auth ;
use App\CustomField ;
use MongoId ;
use MongoException;

class CreateRequest extends BaseRequest {

  public function authorize()
  {
    return true ;
  }

  public function rules()
  {
      return [
          'name' => 'required', 
          'datatype' => 'required|in:'.implode(",", $this->dataTypes()),
      ];
  }
}
