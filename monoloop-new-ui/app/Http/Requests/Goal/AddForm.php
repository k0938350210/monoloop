<?php

namespace App\Http\Requests\Goal;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;
use App\Goal;

class AddForm extends GoalBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'source' => 'required',
            'nameGoal' => 'required|max:255',
            'typeGoal' => 'required|in:'.implode(",", $this->goalTypes()),
            'pointsGoal' => 'numeric',
        ];
    }
}
