<?php namespace App\Http\Requests\Goal;

use App\Http\Requests\Request;
use Auth ;
use App\Goal ;
use MongoId ;
use MongoException ;

class GoalBase extends Request{

  public function authorize()
  {
    if (strpos($this->input('_id'), '__') !== false) {
        $srcTypeId = explode('__', $this->input('_id'));
        $srcId = $srcTypeId[1];
    } else {
        $srcId = $this->input('_id');
    }
    try {
      $srcId = new MongoId($srcId);
    } catch (MongoException $ex) {
      return false ;
    }
    return Goal::where('_id', $srcId )->byAccount(Auth::user()->active_account)->exists();
  }

  /**
   * Get get types
   *
   * @return array
   */
  public function goalTypes()
  {
      return Goal::$types;
  }
}
