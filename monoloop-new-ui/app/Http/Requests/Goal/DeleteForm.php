<?php

namespace App\Http\Requests\Goal;
use Illuminate\Http\JsonResponse;
use Auth ;
use App\Goal ;
use MongoId ;

class DeleteForm extends GoalBase
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_id' => 'required',
      ];
    }

}
