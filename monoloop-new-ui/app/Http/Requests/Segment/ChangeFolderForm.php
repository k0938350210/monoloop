<?php namespace App\Http\Requests\Segment; 

class ChangeFolderForm extends SegmentBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_id' => 'required',
          'target' => 'required',
      ];
    }

}
