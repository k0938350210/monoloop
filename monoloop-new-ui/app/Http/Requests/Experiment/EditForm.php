<?php namespace App\Http\Requests\Experiment;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;
use MongoId ;
use Auth;

use App\Segment;
use App\Goal;
use App\Experiment;

class EditForm extends ExperimentBase
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      #segment
      $segment = $this->input('segmentExperiment', false);
      $goal = $this->input('goalExperiment', false);
      $id = $this->input('_id', false);
      if($segment) {
        try {
          $segment_id = new MongoId($segment);
          if( Segment::where('_id', $segment_id )->byAccount(Auth::user()->active_account)->exists() === false){
            return false ;
          }
        } catch (MongoException $ex) {
          return false;
        }
      }
      if($goal){
        #goal
        try {
          $goal_id = new MongoId($goal);
          if( Goal::where('_id', $goal_id )->byAccount(Auth::user()->active_account)->exists() === false){
            return false ;
          }
        } catch (MongoException $ex) {
          return false;
        }
      }
      if($id){
        #goal
        try {
          $exp_id = new MongoId($id);
          if( Experiment::where('_id', $exp_id )->byAccount(Auth::user()->active_account)->exists() === false){
            return false ;
          }
        } catch (MongoException $ex) {
          return false;
        }
      }
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          '_id' => 'required',
          'nameExperiment' => 'required|max:255',
          // 'descExperiment' => 'required',
          // 'segmentExperiment' => 'required',
          // 'goalExperiment' => 'required',
          'cgSizeExperiment' => 'required|numeric|min:0|max:100',
          'cgDayExperiment' => 'required|numeric|min:0',
          'significantActionExperiment' => 'required|in:0,1,2',
      ];
    }
}
