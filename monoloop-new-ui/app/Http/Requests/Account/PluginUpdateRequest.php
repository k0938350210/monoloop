<?php namespace App\Http\Requests\Account;

use App\Http\Requests\Request;
use Auth ;
use MongoId ;
use MongoException ;

class PluginUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      $id = $this->route('id');
      try {
        $id = new MongoId($id);
      } catch (MongoException $ex) {
        return false ;
      } 
      return Auth::User()->account->plugins->where('_id','=',$id)->count() ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'enabled' => 'required|boolean'
      ];
    }
}
