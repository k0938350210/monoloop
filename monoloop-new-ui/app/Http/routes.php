<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$route_partials = [
  'page',
  'api',
  'data',
  'component',
  'admin',
  'api_v1'
];

foreach ($route_partials as $partial) {
  $file = __DIR__.'/routes/'.$partial.'.php';
  if ( ! file_exists($file))
  {
    $msg = "Route partial [{$partial}] not found.";
    throw new \Illuminate\Filesystem\FileNotFoundException($msg);
  }
  require $file;
}

