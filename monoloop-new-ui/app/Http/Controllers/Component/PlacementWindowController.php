<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;
use App\Services\Profile\Profile;
use Input;
use App\Segment;
use View;
use Response;

class PlacementWindowController extends Controller
{
    /*
   * Return the properties of required condition parameters
   *
   * @return json
   */

  public function __construct()
  {
    $this->middleware('auth.token.session');
  }

  public function index()
  {
      $profile = new Profile();

      return response()->json($profile->getProperties());
  }

  /*
   * Return the template for condition builder
   *
   * @return html
   */

  public function template()
  {
      $this->viewData = [];

      $this->viewData['url'] = Input::get('url', '');

      $this->viewData['new_url'] = Input::get('new_url', 0);

      $this->viewData['page'] = Input::get('page', 'component');
      $this->viewData['source'] = Input::get('source', '');

      return view('component.placement_window_template', $this->viewData);
  }
}
