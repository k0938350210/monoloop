<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;
use Input;
use View;

class ImageUploaderController extends Controller
{
    /*
   *
   * @return view
   */

  public function index()
  {
      $this->viewData = [];

      $this->viewData['uploadUrl'] = Input::get('url', '/api/upload-image');
      return view('component.image_uploader', $this->viewData);
  }

}
