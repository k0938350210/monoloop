<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Input;
use Validator;
use View;

use App\PageElement;
use App\Content;

class ContentListController extends Controller
{
    /*
   *
   * @return view
   */

  public function index(Request $request)
  {
      $this->viewData = [];

      $validator = Validator::make($request->all(), ['xpath' => 'required', 'fullURL' => 'required'] );
      if($validator->fails()){
        $this->viewData['status'] = 'error';
        $this->viewData['msg'] = $validator->errors()->all();
        return response()->json($this->viewData, 400);
      }

      $this->viewData['xpath'] = $request->input('xpath', "");
      $this->viewData['fullURL'] = $request->input('fullURL', "");
      $this->viewData['page_element_id'] = $request->input('page_element_id', "");
  		$this->viewData['hashFn'] = $request->input('hash_fn', "_mp_hash_function_");

  		$this->viewData['xpath'] = str_replace($this->viewData['hashFn'], "#", $this->viewData['xpath']);

      return view('component.content.content_list', $this->viewData);
  }

  /*
   *
   * @return view
   */

  public function create(Request $request)
  {
      $this->viewData = [];

      $validator = Validator::make($request->all(), ['xpath' => 'required', 'fullURL' => 'required'] );
      if($validator->fails()){
        $this->viewData['status'] = 'error';
        $this->viewData['msg'] = $validator->errors()->all();
        return response()->json($this->viewData, 400);
      }

      $this->viewData['source'] = $request->input('source', "default");
      $this->viewData['xpath'] = $request->input('xpath', "");
      $this->viewData['fullURL'] = $request->input('fullURL', "");
      $this->viewData['page_element_id'] = $request->input('page_element_id', "");
      $this->viewData['experiment_id'] = $request->input('exp', "");
      $this->viewData['hashFn'] = $request->input('hash_fn', "_mp_hash_function_");
      $this->viewData['totalVariations'] = $request->input('tVar', 0);

      $this->viewData['xpath'] = str_replace($this->viewData['hashFn'], "#", $this->viewData['xpath']);

      return view('component.content.content_create', $this->viewData);
  }

  /*
   *
   * @return view
   */

  public function update(Request $request)
  {
      $this->viewData = [];

      $validator = Validator::make($request->all(), ['xpath' => 'required', 'fullURL' => 'required', 'content_id' => 'required'] );
      if($validator->fails()){
        $this->viewData['status'] = 'error';
        $this->viewData['msg'] = $validator->errors()->all();
        return response()->json($this->viewData, 400);
      }

      $this->viewData['source'] = $request->input('source', "default");
      $this->viewData['xpath'] = $request->input('xpath', "");
      $this->viewData['fullURL'] = $request->input('fullURL', "");
      $this->viewData['page_element_id'] = $request->input('page_element_id', "");
      $this->viewData['content_id'] = $request->input('content_id', false);
      $this->viewData['hashFn'] = $request->input('hash_fn', "_mp_hash_function_");

      $this->viewData['xpath'] = str_replace($this->viewData['hashFn'], "#", $this->viewData['xpath']);

      $content = Content::find($this->viewData['content_id']);
      $this->viewData['content_name'] = $content->name;
      $this->viewData['content_condition'] = $content->condition;

      return view('component.content.content_update', $this->viewData);
  }

  /*
   *
   * @return view
   */

  public function addToExperiment(Request $request)
  {
      $this->viewData = [];

      $validator = Validator::make($request->all(), ['xpath' => 'required', 'fullURL' => 'required'] );
      if($validator->fails()){
        $this->viewData['status'] = 'error';
        $this->viewData['msg'] = $validator->errors()->all();
        return response()->json($this->viewData, 400);
      }

      $this->viewData['source'] = $request->input('source', "default");
      $this->viewData['xpath'] = $request->input('xpath', "");
      $this->viewData['fullURL'] = $request->input('fullURL', "");
      $this->viewData['page_element_id'] = $request->input('page_element_id', "");
      $this->viewData['content_id'] = $request->input('content_id', "");
      $this->viewData['current_folder'] = $request->input('current_folder', "");
      $this->viewData['hashFn'] = $request->input('hash_fn', "_mp_hash_function_");
      $this->viewData['xpath'] = str_replace($this->viewData['hashFn'], "#", $this->viewData['xpath']);
      $this->viewData['page'] = $request->input('page', "web");

      $this->viewData['bk_segment_id'] = $request->input('bk_seg', "");
      $this->viewData['bk_goal_id'] = $request->input('bk_goal', "");

      $page_element = PageElement::find($this->viewData['page_element_id']);
      $this->viewData['includeWWWWebPageElement'] = $page_element ? $page_element->inWWW : 1;
      $this->viewData['includeHttpHttpsWebPageElement'] = $page_element ? $page_element->inHTTP_HTTPS : 0;
      $this->viewData['urlOptionWebPageElement'] = $page_element ? $page_element->urlOption : 0;
      $this->viewData['regularExpressionWebPageElement'] = $page_element ? $page_element->regExp : '';
      $this->viewData['remarkWebPageList'] = $page_element? $page_element->remark : '';
      $this->viewData['fullURLWebPageList'] = $page_element ? $page_element->originalUrl : $this->viewData['fullURL'];



      return view('component.content.content_add_to_experiment', $this->viewData);
  }

  /*
   *
   * @return view
   */

  public function addSegment(Request $request)
  {
      $this->viewData = [];

      $validator = Validator::make($request->all(), [] );
      if($validator->fails()){
        $this->viewData['status'] = 'error';
        $this->viewData['msg'] = $validator->errors()->all();
        return response()->json($this->viewData, 400);
      }

      $this->viewData['source'] = $request->input('source', "default");
      $this->viewData['fullURL'] = $request->input('fullURL', "");
      $this->viewData['current_folder'] = $request->input('current_folder', "");

      return view('component.content.add_segment', $this->viewData);
  }

  /*
   *
   * @return view
   */

  public function addGoal(Request $request)
  {
      $this->viewData = [];

      $validator = Validator::make($request->all(), [] );
      if($validator->fails()){
        $this->viewData['status'] = 'error';
        $this->viewData['msg'] = $validator->errors()->all();
        return response()->json($this->viewData, 400);
      }

      $this->viewData['source'] = $request->input('source', "default");
      $this->viewData['fullURL'] = $request->input('fullURL', "");
      $this->viewData['page_element_id'] = $request->input('page_element_id', "");
      $this->viewData['current_folder'] = $request->input('current_folder', "");

      $page_element = PageElement::find($this->viewData['page_element_id']);
      $this->viewData['includeWWWWebPageElement'] = $page_element ? $page_element->inWWW : 1;
      $this->viewData['includeHttpHttpsWebPageElement'] = $page_element ? $page_element->inHTTP_HTTPS : 0;
      $this->viewData['urlOptionWebPageElement'] = $page_element ? $page_element->urlOption : 0;
      $this->viewData['regularExpressionWebPageElement'] = $page_element ? $page_element->regExp : '';
      $this->viewData['remarkWebPageList'] = $page_element? $page_element->remark : '';
      $this->viewData['fullURLWebPageList'] = $page_element ? $page_element->originalUrl : $this->viewData['fullURL'];


      return view('component.content.add_goal', $this->viewData);
  }
}
