<?php namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Input;
use View;
use Auth ;
use Validator;
use MongoId ;
use MongoException;

use App\Placement;
use App\PageElement;
use App\PageElementContent;
use App\UrlConfig;
use App\Content;
use App\ContentConfig;
use App\Experiment;
use App\PageElementExperiment;

class PageListController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  /*
   *
   * @return view
   */

  public function publish(Request $request)
  {
      $this->viewData = [];

      $validator = Validator::make($request->all(), ['source' => 'required', 'fullURL' => 'required'] );
      if($validator->fails()){
        $this->viewData['status'] = 'error';
        $this->viewData['msg'] = $validator->errors()->all();
        return response()->json($this->viewData, 400);
      }

      $this->viewData['source'] = $request->input('source', "default");
      $this->viewData['fullURL'] = $request->input('fullURL', "");
      $this->viewData['page_element_id'] = $request->input('page_element_id', "");
      $this->viewData['experiment_id'] = $request->input('exp', "");

      return view('component.page_list.publish', $this->viewData);
  }

  /*
   *
   * @return json
   */

  public function publishStore(Request $request)
  {
    $ret = ['status' => 'success' , 'msg' => 'Page created!'] ;

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['fullURLWebPageList' => 'required'] );

    if ($validator->fails()) {
      $ret['status'] = 'error';
      $ret['msg'] = $validator->errors()->all();
      return response()->json($ret, 400);
    }

    $fullURL = $request->input('fullURLWebPageList');
    $regExp = $request->input('regularExpressionWebPageElement', '');
    $inWWW = (int)$request->input('includeWWWWebPageElement', 0);
    $inHTTP_HTTPS = (int) $request->input('includeHttpHttpsWebPageElement', 0);
    $urlOption = $request->input('urlOptionWebPageElement', '');
    $additionalJS = $request->input('additionalJSWebPageElement', '');
    $remark = $request->input('remarkWebPageList', '');
    $source = $request->input('source', 'default'); // 1) experiment, 2) default etc etc
    $pageElementId = $request->input('page_element_id', '');
    $experiment_id = $request->input('experiment_id', '');
    $isExistingPageELementID = $request->input('isExistingPageELementID', false);

    if($isExistingPageELementID === "false") $isExistingPageELementID = false;
    if($isExistingPageELementID === "true") $isExistingPageELementID = true;

    $urlConfig = new UrlConfig();
    $urlConfig->reg_ex = $regExp;
    $urlConfig->url = $fullURL;
    $urlConfig->url_option = $urlOption;
    $urlConfig->inc_www = $inWWW;
    $urlConfig->inc_http_https = $inHTTP_HTTPS;
    $urlConfig->urlHash = $urlConfig->getUrlHashAttribute();
    $urlConfig->cleanUrl = $urlConfig->getCleanUrlAttribute();

    $urlConfig->setUrlOptionFromString($urlConfig->url_option);

    $pe = new PageElement();
    $pe->setUrlConfig($urlConfig);

    // if($source === 'default'){
    //
    //   if(strlen($pageElementId) > 0){
    //     $pageElement = PageElement::find($pageElementId);
    //   } else {
    //     $pageElement = PageElement::where('originalUrl','=',$fullURL)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->whereNull('Experiment')->first();
    //   }
    //
    // } else {
    //   switch ($source) {
    //     case 'experiment':
    //       $pageElement = PageElement::find($pageElementId);
    //       break;
    //     default:
    //       $pageElement = PageElement::find($pageElementId);
    //       break;
    //   }
    // }
    $found = false;
    if($isExistingPageELementID){
      if($pageElementId){
        $pageElement = PageElement::find($pageElementId);
        if($pageElement){
          $found = true;
        }
      }
    }
    if($found === false){
      $pageElement = PageElement::where('originalUrl','=',$fullURL)
                                  ->where('deleted', '<>', 1)
                                  ->where('cid', '=', $active_account->uid)
                                  ->where('inWWW', '=', $pe->inWWW)
                                  ->where('inHTTP_HTTPS', '=', $pe->inHTTP_HTTPS)
                                  ->where('urlOption', '=', (int) $pe->urlOption)
                                  ->where('regExp', '=', $pe->regExp)
                                  ->first();
    }


    if(!$pageElement){
      $pageElement = $pe;
      $pageElement->cid = $active_account->uid;

    } else {
      $pageElement->setUrlConfig($urlConfig);
    }
    $pageElement->originalUrl = $fullURL;
    $pageElement->pageID = 0;
    $pageElement->hidden = 0;
    if(in_array($source, ['experiment'])){
      $pageElement->deleted = 0;
    } else {
      $pageElement->deleted = 0;
    }

    $pageElement->additionalJS = $additionalJS;
    $pageElement->remark = $remark;
    $pageElement->save();

    $experiment = false;

    if($source === 'experiment'){
      $experiment = Experiment::find($experiment_id);
      if(!$experiment){
        $experiment = new Experiment();
        $experiment->name = 'no name';
        $experiment->description = '';
        $experiment->cg_size = 0.00;
        $experiment->cg_day = 0;
        $experiment->significant_action = 0;
        $experiment->account_id = Auth::user()->active_account;
        $experiment->deleted = 1;
        $experiment->hidden = 1;
        $experiment->folder_id = null;
        $experiment->experimentID = Experiment::max('experimentID') + 1;
        $experiment->save() ;
      }
      if($experiment){
        if($pageElement->experiment){
          $pageElementExperiment = $pageElement->experiment;
        } else {
          $pageElementExperiment = new PageElementExperiment();
        }
        $pageElementExperiment->controlGroupSize = $experiment->cg_size;
        $pageElementExperiment->controlGroupDays = $experiment->cg_day;
        $pageElementExperiment->experimentID = $experiment->experimentID;
        if($experiment->goal){
          $pageElementExperiment->goalID = $experiment->goal->uid;
        }
        if($experiment->segment){
          $pageElementExperiment->segmentID = $experiment->segment->uid;
        }

        $pageElement->experiments()->save($pageElementExperiment);
      }
    }

    $ret['source'] = 'webpaglelist';
    $ret['content'] = $pageElement;
    $ret['_source'] = $source;
    $ret['experiment'] = $experiment;

    return response()->json( $ret ) ;
  }

}
