<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Http\Request;

use App\User;
use App\AccountUser;
use App\Account;

use App\Services\Account\MailService ;
use MongoId ;
use Notification;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');
	}

	public function getEmail(Request $request)
	{
		$show_insert_username =  $request->session()->get('show_insert_username');
		return view('auth.password')->with('show_insert_username' , $show_insert_username );
	}

	/**
	 * Send a reset link to the given user.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postEmail(Request $request)
	{
			$this->validate($request, ['email' => 'required|email']);

			$email = $request->input('email','');

			$account_users = AccountUser::where('email', $email)->get(); // check for email based account

			$total = count($account_users);

			if($total === 1){
				$_user = User::where('_id', new \MongoId($account_users[0]->user_id))->first();
				$this->sendResetUser($email, $_user);
				Notification::success('Success send reset link.Please check your email');
				return redirect('auth/login');
			}else if($total > 1){
				$mailService = new MailService() ;
				$mailService->sendMultiUsersFromEmail($email , $account_users) ;
				return redirect()
								-> back()
								-> with('show_insert_username', true)
								-> withInput()
								-> withErrors(
										[
												'username' => 'Your email is associated with multiple accounts. We have sent you an email with the User Name for each account. Please specify your User Name below to reset your password.'
										]
									);
			} else if($total === 0){
				return redirect()
								->back()
								->withInput()
								->withErrors([
									'email' => 'The email account that you tried to reach does not exist.'
								]);
		  }
			// $username = $request->input('username','');
			// $total = User::byUsername($email)->count() ;

			// if($total > 1 && $username == ''){
			// 	#email all possible username ;
			// 	$mailService = new MailService() ;
		  //   $mailService->sendMultiUsersFromEmail($email , AccountUser::byEmail($email)->get()) ;
			//
			// 	return redirect()->back()->with('show_insert_username', true)->withInput()->withErrors([
			// 		'username' => 'Your email is associated with multiple accounts. We have sent you an email with the User Name for each account. Please specify your User Name below to reset your password.'
			// 	]);
			// }else if($total == 1 ){
			// 	$user = User::byUsername($email)->first() ;
			// 	$this->sendResetUser($email , $user) ;
			// 	Notification::success('Success send reset link.Please check your email');
			// 	// return redirect()->back()->withInput();
			// 	return redirect('auth/login');
			// }else if($total > 1 && $username != ''){
			// 	 $user =  User::byUsername($username)->first() ;
			// 	if(empty($user)){
			// 		return redirect()->back()->with('show_insert_username', true)->withInput()->withErrors([
			// 			'username' => 'Username not found.'
			// 		]);
			// 	}
			// 	$total2 = AccountUser::byEmail($email)->where('user_id', $user->_id  )->count() ;
			// 	if($total2 > 0){
			// 		$this->sendResetUser($email , $user) ;
			// 		Notification::success('Success send reset link.Please check your email');
			// 		// return redirect()->back()->withInput();
			// 		return redirect('auth/login');
			// 	}
			// }else{
			// 	return redirect()->back()->withInput()->withErrors([
			// 		'email' => 'The email account that you tried to reach does not exist.'
			// 	]);
			// }
	}

	private function sendResetUser($email , $user ){
		$user->pwdreset_key = (string)(new MongoId()) ;
		$user->save() ;
		$mailService = new MailService() ;
		$mailService->sendResetLink($email , $user) ;
	}

}
