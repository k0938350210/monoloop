<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Services\Hasher;
use App\Services\Account\AccountUserService;
use App\Services\Account\Cbr;
use App\Services\Account\Invocation;
use App\Services\Account\SugarLead;
use App\Services\Account\AccountService;
use App\Services\Account\MailService;
use App\Services\Account\DefaultAudience;
use App\Services\Account\DefaultTracker;
use App\Services\Account\DefaultFunnel;

use App\Services\Redis\RedisService;
use App\Events\Logger;

use MongoId ;
use Validator;

use Auth;
use Session;

use App\User;
use App\AccountUser;
use App\Account;
use App\Folder;

use App\Http\Controllers\Common\InteractsWithCbr ;
use App\Jobs\SugarLeadAndMailChimp;
use App\Jobs\PrivacyDefault ;
use App\AccountDomain;
use Carbon\Carbon;
use Notification ;
use App\OCRConfigs;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;
	use InteractsWithCbr;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	private function recaptchaVerify($request){
		$post = [
	    'secret' => config('services.recaptcha.secret_key'),
	    'response' => $request->input('token'),
	    'remoteip' => $request->ip(),
		];

		$ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		// execute!
		$response = curl_exec($ch);
		// close the connection, release resources used
		curl_close($ch);
		// do anything you want with your response
		$response = json_decode($response,true);
		return $response;
	}

	public function postLogin(Request $request)
	{

		$response = $this->recaptchaVerify($request);
		if($response['success'] == false){
			return redirect($this->loginPath())
				->withInput($request->only('username', 'remember'))
				->withErrors($response['error-codes']);
		}

		$this->validate($request, [
			'username' => 'required', 'password' => 'required',
		]);

		$credentials = $request->only('username', 'password');
		$response = $this->authenticateLogin($credentials);
		$header = $response['header'];

		$headers = $this->getHederInfo($header);

		$body = $response['body'];
		$resArr = array();
		$resArr = json_decode($body);
		// var_dump($resArr);die;
		if($resArr->is_success === 1){

			$ml_token = $headers['Ml-Token'];
			$ml_token = substr($ml_token, 0, -1);
			$ml_token = substr($ml_token,1);

			$user = User::find($resArr->response->_id->{'$id'});

			if (strlen($user) > 0){
				Session::set('mlToken', $ml_token);

				Auth::login($user);
				\Event::fire(new Logger(Auth::user()->username, 'user login', null , "App\User"));
				// return redirect()->intended($this->redirectPath());
				// return redirect('/');
				return RedisService::intendedRedirect(Auth::user()->_id);
			} else {
				return redirect($this->loginPath())
					->withInput($request->only('username', 'remember'))
					->withErrors([
						'email' => 'Invalid username or password.',
					]);
			}
		} else {
			return redirect($this->loginPath())
				->withInput($request->only('username', 'remember'))
				->withErrors([
					// 'email' => 'Invalid username or password.',
					$resArr->errCode => $resArr->exception
				]);
}


	}
	private function getHederInfo($header_string){
		$myarray=array();
		$data = explode("\n", $header_string);

		$myarray['status'] = $data[0];

		array_shift($data);

		foreach($data as $part){
			$middle = explode(":",$part);
			if(count($middle) === 2){
				$myarray[trim($middle[0])] = trim($middle[1]);
			}
		}
		return $myarray;
	}

	public function authenticateLogin($fields){
		$apihost = config('app.monoloop')['apihost'];
		$url = $apihost.'/api/user/login';
		$headers = ['Content-Type: application/json', 'ML-Version: v1.0'];

		$fields_string = json_encode($fields);
		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		//execute post
		$response = curl_exec($ch);
		$result = [];

		// Then, after your curl_exec call:
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$result['header'] = substr($response, 0, $header_size);
		$result['body'] = substr($response, $header_size);
		//close connection
		curl_close($ch);
		return $result;
	}

	public function postRegister(Request $request)
	{
		$response = $this->recaptchaVerify($request);
		$recaptcha_score = $response['score'];
		if($response['success'] == false){
			return redirect('auth/register')
				->withErrors($response['error-codes']);
		}

		$this->validate($request, [
		 'firstname' => 'required',
		 'email' => 'required|email|unique:users,username',
		 'confirmemail' => 'required|email|unique:users|same:email' ,
		 // 'accountname' => 'required|unique:accounts,name' ,
		 //'user_type' => 'required|In:enterprise,agency',
		 'password' => 'required|min:6' ,
		 'confirmpassword' => 'required|same:password' ,
	 ]);
	 $formData = $request->all();

	 $formData['username'] = $formData['confirmemail'];

	 $source = !empty($formData['source']) ? $formData['source'] : 'Other';

	//  var_dump($formData);die;

	 $response = $this->register($formData);
	 $header = $response['header'];

	 $headers = $this->getHederInfo($header);

	 $body = $response['body'];
	 $resArr = array();
	 $resArr = json_decode($body);

	 if($resArr->is_success == 1){

		 $ml_token = $headers['Ml-Token'];
		 $ml_token = substr($ml_token, 0, -1);
		 $ml_token = substr($ml_token,1);

		 $account = Account::find($resArr->response->account_id);
		 $adminUser = AccountUser::find($resArr->response->account_user_id);
		 $user = User::find($resArr->response->user_id);
		 $user->recaptcha_score = $recaptcha_score;
		 $user->save();

		 #3 Save account signup url ;
		 $account->signup_ip = $request->ip();
		 $account->save() ;

		 #save OCR settings in OCRConfigs collections
     $ocrModel = new OCRConfigs();
 		 $ocrModel->cid = $account->cid;
 		 $ocrModel->ocReady = false ;
 		 $ocrModel->save();

		 $mailService = new MailService() ;
		 $mailService->sendComfirmationMail($adminUser , $user) ;

		 $mailService->sendNewSignUptoAdmins($account, $adminUser, $user, $source);

		 $this->dispatch(new SugarLeadAndMailChimp($account,$source));

		 $this->generateCbr($account) ;

		 // creating new folder
		 $folder = new Folder();
		 $folder->account_id = $user->active_account;
		 $folder->cid = $account->uid;
		 $folder->deleted = 0;
		 $folder->hidden = 0;
		 $folder->title = 'Root';
		 $folder->parent_id = null;
		 $folder->created_at = new \Datetime();
		 $folder->updated_at = new \Datetime();
		 $folder->save();

		$this->dispatch(new PrivacyDefault($account));

		#9 Add remaining_pageviews ;

		 if($account->AccType == 'Paid'){
		 		$account->remaining_pageviews = config('services.billing.page_views.paid');
		 }else{
		 		$account->remaining_pageviews = config('services.billing.page_views.free');
		 }
		 $account->source = $source ;
		 $account->last_billing_date = Carbon::today();
		 $account->omni_channel_ready = 0;

		 $invocation = $account->invocation;
		 $invocation['postback_delay'] = 1000;
		 $account->invocation = $invocation;

		 $account->save() ;

		 #Create default audeince
		 $defaultAudience = new DefaultAudience() ;
		 $defaultAudience->createDefault($account) ;

		#Create default tracker
	  $default_tracker = new DefaultTracker();
	  $default_tracker->createDefault($account);

	  #Create default funnel
	  $default_funnel = new DefaultFunnel();
	  $default_funnel->createDefault($account);

	  	#10 update null user_id with same email
	  	$account_users = AccountUser::whereNull('user_id')->where('email',$user->username)->get();
	  	foreach($account_users as $account_user){
	  		$account_user->user_id = $user->id;
	  		$account_user->save();
	  	}

	  	#11 if post with accountId so set with active account
	  	$account_id = $request->input('accountId');
	  	if($account_id && AccountUser::where('account_id',$account_id)->where('user_id',$user->id)->exists()){
	  		$user->active_account = new MongoId($account_id);
	  		$user->save();
	  	}

		 return redirect('/please_confirm_your_registration?a_id='.$account->_id)->withInput($request->only('name', 'email'));

	 } else {
	 }


  }

	public function register($fields){
		$apihost = config('app.monoloop')['apihost'];
		$url = $apihost.'/api/user/register';
		$headers = ['Content-Type: application/json', 'ML-Version: v1.0'];

		$fields_string = json_encode($fields);

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		//execute post
		$response = curl_exec($ch);
		$result = [];

		// Then, after your curl_exec call:
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$result['header'] = substr($response, 0, $header_size);
		$result['body'] = substr($response, $header_size);

		//close connection
		curl_close($ch);
		return $result;
	}
	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout()
	{

		if(Auth::check()){
			\Event::fire(new Logger($this->auth->user()->username, 'user logout', null , "App\User"));
			$this->auth->logout();
			if (isset($_COOKIE['loggedin'])) {
		    unset($_COOKIE['loggedin']);
		    setcookie('loggedin', '', time() - 3600, '/'); // empty value and old timestamp
			}
			Session::set('mlToken', '');
		}

		return redirect('auth/login');

	}
}
