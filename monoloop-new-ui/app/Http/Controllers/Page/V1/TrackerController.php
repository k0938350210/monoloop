<?php namespace App\Http\Controllers\Page\V1;
 
use Illuminate\Http\Request;

class TrackerController extends PageController {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{ 
		$this->viewData['selected'] = array('Trackers','Trakers') ;
		return view('page.v1.trackers',$this->viewData);
	}
 
}