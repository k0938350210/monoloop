<?php namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;

class TrackerController extends PageController {

  public function __construct(){
    parent::__construct();
    $this->middleware('auth.role-admin');
  }

  /**
   * Show the application dashboard to the user.
   *
   * @return Response
   */
  public function index()
  {
    $this->viewData['selected'] = array('Trackers','') ;
    return view('page.trackers',$this->viewData);
  }

}