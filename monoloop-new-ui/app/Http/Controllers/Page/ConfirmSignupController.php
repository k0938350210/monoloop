<?php namespace App\Http\Controllers\Page;

use Auth;
use Validator ;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\UseMonoloopViewData ;
use Input;

use App\AccountUser;
use App\Account;
use App\User;
use MongoId ;
use Session;

class ConfirmSignupController extends Controller {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
  use UseMonoloopViewData ;

	public function index(Request $request)
	{
      $viewData['account_name'] = '';
      $account_id = Input::get('a_id','');
      if($account_id){
        $account = Account::find($account_id);
        $viewData['account_name'] = $account->contact_name;
      }
      return view('page.confirmation.wait',$viewData);
	}

  public function confirm(Request $request , $id){
    if(MongoId::isValid($id) == false){
      abort(404);
    }
    $user = User::where('confirmation_id',new MongoId($id))->first() ;
    if(empty($user)){
      $user = Auth::User();
      if(!empty($user)){
        $viewData = $this->getViewData() ;
        $viewData['user'] = Auth::user();
      } else {
        return redirect('/') ;
      }
      $viewData['selected'] = array('','') ;
      return view('page.confirmation.activated',$viewData);
    }
    if(empty($user)) {
      abort(404);
    }

		// activate the user account
		$user->confirmation_id = null;
    $user->save();

    $user->account->hidden = 0;
    $user->account->save();

    $credentials = ['user_id' => $user->_id, 'by_id' => true, 'username' => $user->username, 'password' => ''];

    $response = $this->authenticateLogin($credentials);

    $header = $response['header'];

    $headers = $this->getHederInfo($header);

    $body = $response['body'];
    $resArr = array();
    $resArr = json_decode($body);

    if($resArr->is_success === 1){

      $ml_token = $headers['Ml-Token'];
      $ml_token = substr($ml_token, 0, -1);
      $ml_token = substr($ml_token,1);

      Session::set('mlToken', $ml_token);

      Auth::login($user);

    } else {
      return redirect('/');
    }

    if($user->account->cms_site_url){
      return redirect()->away( $user->account->cms_site_url . '/wp-admin/admin.php?page=ml_activated' ) ;
    }

    $viewData = $this->getViewData() ;
    $viewData['selected'] = array('','') ;
    return view('page.confirmation.success',$viewData);
  }

  public function authenticateLogin($fields){
    $apihost = config('app.monoloop')['apihost'];
    $url = $apihost.'/api/user/login';
    $headers = ['Content-Type: application/json', 'ML-Version: v1.0'];

    $fields_string = json_encode($fields);
    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    //execute post
    $response = curl_exec($ch);
    $result = [];

    // Then, after your curl_exec call:
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $header_size);
    $result['body'] = substr($response, $header_size);
    //close connection
    curl_close($ch);
    return $result;
  }
  private function getHederInfo($header_string){
    $myarray=array();
    $data = explode("\n", $header_string);

    $myarray['status'] = $data[0];

    array_shift($data);

    foreach($data as $part){
      $middle = explode(":",$part);
      if(count($middle) === 2){
        $myarray[trim($middle[0])] = trim($middle[1]);
      }
    }
    return $myarray;
  }
}
