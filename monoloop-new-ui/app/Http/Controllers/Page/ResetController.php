<?php namespace App\Http\Controllers\Page;

use Auth;
use Validator ;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AccountUser;
use App\User;
use MongoId ;

use Session;

class ResetController extends Controller {


  public function reset(Request $request , $id){
    if(MongoId::isValid($id) == false){
      abort(404);
    }

    $user = User::where('pwdreset_key',$id)->first() ;
    if(empty($user))
      abort(404);

    Auth::login($user);

    return redirect('/account/profile');
  }

}
