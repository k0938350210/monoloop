<?php namespace App\Http\Controllers\Page\Invitation;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AccountUser;

class AccountController extends Controller {
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
  private $viewData = array() ;

	public function index(Request $request , $id, $token, $type = '')
	{
    $accountUser = AccountUser::where('account_id', $id)->first() ;
    if(is_null($accountUser)){
      abort(404);
    }
    $accountUser->account;
    $this->viewData['user'] = $accountUser ;
    $this->viewData['type'] = "account" ;
		return view('page.invitation',$this->viewData);
	}


}
