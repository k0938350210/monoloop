<?php namespace App\Http\Controllers\Page\Invitation;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AccountUser;
use App\User;
use MongoId;
use Session;

class UserController extends Controller {
  /**
   * Show the application dashboard to the user.
   *
   * @return Response
   */
  private $viewData = array();

  public function index(Request $request , $id, $token)
  {
    Auth::logout();

    $accountUser = AccountUser::where('account_id', $id)->where('_id',$token)->first();

    if(!is_null($accountUser->user_id)){
      //Login and redirect;
      $user = User::find($accountUser->user_id);
      $user->active_account = new MongoId($accountUser->account_id);
      $user->confirmation_id = null;
      $user->save();

      $credentials = ['user_id' => $user->_id, 'by_id' => true, 'username' => $user->username, 'password' => ''];

      $response = $this->authenticateLogin($credentials);

      $header = $response['header'];

      $headers = $this->getHederInfo($header);

      $body = $response['body'];
      $resArr = array();
      $resArr = json_decode($body);

      if($resArr->is_success === 1){

        $ml_token = $headers['Ml-Token'];
        $ml_token = substr($ml_token, 0, -1);
        $ml_token = substr($ml_token,1);

        Session::set('mlToken', $ml_token);

        Auth::login($user);

      }

      return redirect('/');
    }

    if(is_null($accountUser)){
      abort(404);
    }
    $accountUser->account;
    $this->viewData['user'] = $accountUser;
    $this->viewData['type'] = "accountUser";
    return view('page.invitation.user',$this->viewData);
  }


  public function authenticateLogin($fields){
    $apihost = config('app.monoloop')['apihost'];
    $url = $apihost.'/api/user/login';
    $headers = ['Content-Type: application/json', 'ML-Version: v1.0'];

    $fields_string = json_encode($fields);
    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    //execute post
    $response = curl_exec($ch);
    $result = [];

    // Then, after your curl_exec call:
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['header'] = substr($response, 0, $header_size);
    $result['body'] = substr($response, $header_size);
    //close connection
    curl_close($ch);
    return $result;
  }
  private function getHederInfo($header_string){
    $myarray=array();
    $data = explode("\n", $header_string);

    $myarray['status'] = $data[0];

    array_shift($data);

    foreach($data as $part){
      $middle = explode(":",$part);
      if(count($middle) === 2){
        $myarray[trim($middle[0])] = trim($middle[1]);
      }
    }
    return $myarray;
  }


}
