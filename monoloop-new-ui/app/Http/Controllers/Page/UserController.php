<?php namespace App\Http\Controllers\Page;

use Auth;

use Illuminate\Http\Request;

class UserController extends PageController {

	public function index(){
		$this->viewData['selected'] = array('User','Profile') ;
		return view('page.user.profile',$this->viewData);
	}
}