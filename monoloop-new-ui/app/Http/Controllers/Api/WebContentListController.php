<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Segment;
use App\Tracker;
use App\Folder;
use App\PageElement;
use App\Content;
use App\UrlConfig;

use Auth ;
use Validator;
use MongoId ;
use Input;

class WebContentListController extends Controller {

 	/*
 	 * Action is finding all contents and associated folders of logged in user
 	 *
 	 * @return Json
 	 */
	public function index()
	{
		$ret = [];
		$account_id = Auth::user()->active_account;
		$folder = new Folder();

		$ret['records'] = $folder->webContentListFolders($account_id);
		return response()->json( $ret['records'] ) ;
	}

	/*
	* show content element
	*
	* @return Json
	*/
	public function show()
	{
		$ret = [];
		$srcTypeId = explode("__", Input::get('_id'));

		$ret['webContent'] = Content::find($srcTypeId[1]);
		return response()->json( $ret['webContent'] ) ;
	}

	/*
	* create content element
	*
	* @return Json
	*/
	public function create(Request $request){
		$ret = ['status' => 'success' , 'msg' => 'Page created!'] ;

		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a content
	*
	* @return Json
	*/
	public function update(Request $request){
		$ret = ['status' => 'success' , 'msg' => 'Page updated!'] ;

		return response()->json( $ret ) ;
	}

	/*
	* Toggle hidden value active/inactive
	*
	* @return Json
	*/
	public function updateStatus(Request $request)
	{
			$ret = ['success' => true , 'msg' => 'Content activated!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$contentElement = Content::find($srcId);
			$contentElement->hidden = $request->input('hidden');
			$contentElement->save();


			if($contentElement->hidden == "1"){
				$ret['msg'] = 'Content deactivated';
			}

			$ret['source'] = 'webcontentlist';
			$ret['contentElement'] = $contentElement;
			return response()->json($ret);
	}

  /*
	* change folder of content
	*
	* @return Json
	*/
	public function changeFolder(Request $request)
	{
			$ret = ['success' => true , 'msg' => 'Content is moved to new folder.'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$target = explode('__', $request->input('target'));

			$content = Content::find($srcId);
			$content->folder_id = new \MongoId($target[1]);
			$content->save();

			$ret['source'] = 'webcontentlist';
			$ret['content']['webcontent'] = $content;
			return response()->json($ret);
	}

	/*
	* Action is deleting a web content list element
	*
	* @return Json
	*/
	public function delete(Request $request)
	{
			$ret = ['success' => true , 'msg' => 'Content deleted!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$contentElement = Content::find($srcId);
			$contentElement->deleted = 1;
			$contentElement->save();


			$ret['source'] = 'webcontentlist';
			$ret['contentElement'] = $contentElement;
			return response()->json($ret);
	}
}
