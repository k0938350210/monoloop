<?php namespace App\Http\Controllers\Api\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ConditionBuilder\Syntax ;

use Auth;
use Validator;
use App\PageElement;

class TestBenchController extends Controller {
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function show(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['url' => 'required'] );
    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $url = $request->input('url');
    $ret['status'] = 200;
    $ret['success'] = true ;
    $ret['msg'] = "";
    $ret['found'] = false;

    $mongoDetail = [] ;

    $pageElements = PageElement::where('originalUrl','=',$url)->where('deleted', '=', 0)->where('cid', '=', $active_account->uid)->get();
    $allCondition = null ;
    foreach($pageElements as $pageElement){
      foreach( $pageElement->content  as $content ){
        $allCondition .= $content->extractCondition ;
        $mongoDetail[] = $content->toArray() ;
      }
    }

    $syntax = new Syntax() ;
    $groups =  $syntax->getTestBebchPanelObject($allCondition) ;
    $ret['testbench'] = $groups ;
    $ret['mongoDetail'] = $mongoDetail ;
    return response()->json($ret , 200);
  }
}
