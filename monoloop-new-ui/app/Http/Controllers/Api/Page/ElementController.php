<?php namespace App\Http\Controllers\Api\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\V1\Common\ApiFilter;
use App\Http\Controllers\Api\V1\Common\ApiLink;
use App\Http\Controllers\Api\V1\Common\ResponseArray;

use Auth ;
use Validator;
use MongoId ;

use App\Placement;
use App\PageElement;
use App\PageElementContent;
use App\UrlConfig;
use App\Content;

class ElementController extends Controller {

  private $rawRequest ;

  public function __construct()
	{
		$this->middleware('auth');
	}

  private $postRules = [] ;

  public function index(Request $request){

  }

  public function show(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['url' => 'required'] );
    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $url = $request->input('url');
    $mode = $request->input('mode', 'default');
    $source = $request->input('source', 'default');
    $pageElementId = $request->input('page_element_id', '');

    $ret['status'] = 200;
    $ret['msg'] = "";
    $ret['found'] = false;
    $originalUrl = "";

    $pE = false;
    if(strlen($pageElementId) > 0){
      $pE = PageElement::find($pageElementId);
    }

    if($source === 'default'){
      // $pageElement = PageElement::find($pageElementId);
      $pageElements = pageElement::where('originalUrl', '=', $url)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->get();
    } else {
      switch ($source) {
        case 'experiment':
          $pageElements = pageElement::where('originalUrl', '=', $url)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->get();
          break;
        default:
          $pageElements = pageElement::where('originalUrl', '=', $url)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->get();
          break;
      }
    }

    $originalUrl = $url;

    $content = [];
    $variations = [];

    if(count($pageElements) > 0){
      foreach ($pageElements as $pageElement) {
        $content1 = $pageElement->content->get();
        foreach ($content1 as $value) {

          if($value->uid === null){
            $content[] = $value;
          } else {
            $value->content = Content::where('uid', '=',$value->uid)->first();
            if($value->content){
              if((int)$value->content->deleted !== 1){
                $variations[] = $value;
              }
            }
          }
        }
      }
    } else {
      $content = false;
    }



    $ret['inlineContent'] = $content;
    $ret['pageElement'] = $pE;
    $ret['originalUrl'] = $originalUrl;
    $ret['variations'] = $variations;
    $ret['found'] = true;
    $ret['domain_not_found'] = true;
    
    return response()->json($ret,200);
  }

  public function store(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['fullURL' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $fullURL = $request->input('fullURL');
    $regExp = $request->input('tRegularExpressionWebPageElement', '');
    $inWWW = (int)$request->input('tIncludeWWWWebPageElement', 0);
    $inHTTP_HTTPS = (int) $request->input('tIncludeHttpHttpsWebPageElement', 0);
    $urlOption = $request->input('tUrlOptionWebPageElement', '');
    $additionalJS = $request->input('tAdditionalJSWebPageElement', '');
    $remark = $request->input('tRemarkWebPageList', '');

    // to check if it's an experiment's page
    $mode = $request->input('mode', 'default');
    $source = $request->input('source', 'default');
    $pageElementId = $request->input('page_element_id', '');

    $urlConfig = new UrlConfig();
		$urlConfig->reg_ex = $regExp;
		$urlConfig->url = $fullURL;
		$urlConfig->url_option = $urlOption;
		$urlConfig->inc_www = $inWWW;
		$urlConfig->inc_http_https = $inHTTP_HTTPS;
		$urlConfig->urlHash = $urlConfig->getUrlHashAttribute();
		$urlConfig->cleanUrl = $urlConfig->getCleanUrlAttribute();

    $urlConfig->setUrlOptionFromString($urlConfig->url_option);

    $pe = new PageElement();


    if($source === 'default'){
      $pageElement = PageElement::where('originalUrl','=',$fullURL)->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->first();
    } else {
      switch ($source) {
        case 'experiment':
          $pageElement = PageElement::find($pageElementId);
          break;
        default:
          $pageElement = PageElement::find($pageElementId);
          break;
      }
    }

    if(!$pageElement){
      $pageElement = $pe;
			$pageElement->cid = $active_account->uid;
      $pageElement->hidden = 1;
      $pageElement->deleted = 0;
      $pageElement->pageID = 0;
    } else {
      $pageElement->hidden = $pageElement->hidden;
      $pageElement->deleted = $pageElement->deleted;
    }
		$pageElement->setUrlConfig($urlConfig);
    $pageElement->originalUrl = $fullURL;

    $pageElement->additionalJS = $additionalJS;
    $pageElement->remark = $remark;

    $pageElement->save();

    $input_content = $request->input('content', []);
    if(!empty($input_content)){
      foreach ($input_content as $content) {

        $pageElementContent = $pageElement->content->where('mappedTo', '=', $content['mappedTo'])->where('uid', '=', NULL)->first();
        if(!$pageElementContent){
          $pageElementContent = new PageElementContent();
        }
        $pageElementContent->name = "";
        $pageElementContent->connectors = [];

        $pageElementContent->uid = null;
        $pageElementContent->mappedTo = $content['mappedTo'];
        $pageElementContent->placementType = "4";
        $pageElementContent->order = "1";
        $pageElementContent->displayType = "1";
        $pageElementContent->addJS = "";
        $pageElementContent->startDate = null;
        $pageElementContent->endDate = null;
        $pageElementContent->content_type = $content['contentType'];
        $pageElementContent->code = $content['code'];
        $pageElementContent->setCode();



        $pageElement->content()->save($pageElementContent);
      }

    }


    return response()->json(['status' => 200, 'msg' => 'Settings have been saved.', 'pageElement' => $pageElement],201);
  }

  public function update(Request $request , $id , $eid){

  }

  public function destroy(Request $request , $id , $eid){

  }
}
