<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Segment;
use App\Content;
use App\Goal;
use App\Experiment;
use App\Tracker;
use App\Folder;

class FolderController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{
		$ret = [];
		return response()->json($ret) ;
	}

	/*
 	 * Action is createing new folder
 	 *
 	 * @return Json
 	 */
	public function create(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder created!'] ;

		$account_id = Auth::user()->active_account;

		// creating new folder
		$folder = new Folder();
		$folder->account_id = $account_id;
		$folder->deleted = 0;
		$folder->hidden = 0;
		$folder->title = $request->input('value');
		$folder->account_id = $request->input('value');
		$folder->cid = Auth::user()->account->uid;
		$folder->parent_id = new \MongoId(explode("__", $request->input('$parent'))[1]);
		$folder->created_at = new \Datetime();
		$folder->updated_at = new \Datetime();
		$folder->save();

		return response()->json( $ret ) ;
	}

	/*
 	 * Action is updating folder / segment
 	 *
 	 * @return Json
 	 */
	public function update(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder updated!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		// performing action in update (folder / segment edit) or status update
		$ret['action'] = $request->input('action');

		// cases for folder and segment
		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);

				switch ($ret['action']) {
					case 'rename':
						$folder->title = $request->input('target');
						break;
					case 'update_hidden':
						$folder->hidden = $request->input('hidden');
						break;

					default:
						# code...
						break;
				}

				$folder->updated_at = new \Datetime();
				$folder->save();
				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				switch ($ret['action']) {
					case 'rename':
            $segment->name = $request->input('name');
					  $segment->description = $request->input('desc');
						break;
					case 'update_hidden':
						$segment->hidden = $request->input('hidden');
						break;

					default:
						# code...
						break;
				}
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}


		return response()->json( $ret ) ;
	}

	/*
 	 * Action is deleting folder / segment
 	 *
 	 * @return Json
 	 */
	public function delete(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder deleted!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);
				$folder->deleted = ($request->input('action') === 'remove') ? 1 : 0;
				$folder->updated_at = new \Datetime();
				$folder->save();
				if($folder->deleted == 1){
					$segments = Segment::folderSegment($folder->_id)->get();
					foreach ($segments as $segment) {
						$segment->deleted = 1;
						$segment->save();
					}
					$contents = Content::folderContent($folder->_id)->get();
					foreach ($contents as $content) {
						$content->deleted = 1;
						$content->save();
					}
					$experiments = Experiment::folderExperiment($folder->_id)->get();
					foreach ($experiments as $experiment) {
						$experiment->deleted = 1;
						$experiment->save();
					}
					$goals = Goal::folderGoal($folder->_id)->get();
					foreach ($goals as $goal) {
						$goal->deleted = 1;
						$goal->save();
					}
					$trackers = Tracker::folderTracker($folder->_id)->get();
					foreach ($trackers as $tracker) {
						$tracker->deleted = 1;
						$tracker->save();
					}
				}

				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				$segment->deleted = ($request->input('action') === 'remove') ? 1 : 0;
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}

		return response()->json( $ret ) ;
	}

	/*
 	 * Action is moving folder / segment to folder / segment
 	 *
 	 * @return Json
 	 */
	public function move(Request $request){
		$ret = ['success' => true , 'msg' => 'Folder deleted!'] ;

		// source can be both (segment or folder). So the type will be concatinated at start of ID with delimeter __ appeneded.
		$srcTypeId = explode("__", $request->input('source'));

		$targetTypeId = explode("__", $request->input('target'));



		switch ($srcTypeId[0]) {
			case 'folder':
				// finding folder
				$folder = Folder::find($srcTypeId[1]);
				// moving folder to targeted folder
				$folder->parent_id = new \MongoId($targetTypeId[1]);
				$folder->updated_at = new \Datetime();
				$folder->save();
				break;
			case 'segment':
				// finding segment
				$segment = Segment::find($srcTypeId[1]);
				// moving segment to targeted folder
				$segment->folder_id = new \MongoId($targetTypeId[1]);
				$segment->updated_at = new \Datetime();
				$segment->save();
				break;
			default:

				break;
		}

		return response()->json( $ret ) ;
	}
}
