<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomField\CreateRequest;
use App\Http\Requests\CustomField\UpdateRequest;
use App\Http\Requests\CustomField\DeleteRequest;
use App\Services\ExtjsFilter;
use Auth ;
use MongoId ;
use Validator ;

use App\CustomField;

class CustomFieldController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

  /*--- REST FULL ----*/

  public function index(Request $request){
    $customfields = CustomField::byAccount(Auth::user()->active_account)->available()->orderBy('name')->get() ;
		return response()->json($customfields);
  }

  public function store(CreateRequest $request){
    $ret = ['success' => false , 'msg' => ''] ;

    $customField = new CustomField() ;
    $customField->account_id = Auth::user()->active_account ;
    $customField->name = $request->input('name') ;
    $customField->datatype = $request->input('datatype') ;
    $customField->uid = CustomField::max('uid')+1;
    $customField->save() ;
    $ret['success'] = true ;
    $ret['customField'] = $customField ;
    return response()->json($ret);
  }

	public function update($id,UpdateRequest $request){
		$ret = ['success' => false , 'msg' => ''] ;

    $customField = CustomField::uid($id)->first() ;
    $customField->name = $request->input('name') ;
    $customField->datatype = $request->input('datatype') ;
    $customField->save() ;
    $ret['success'] = true ;
    $ret['customField'] = $customField ;
    return response()->json($ret);
	}

	public function destroy($id ,DeleteRequest $request)
	{
			$ret = ['success' => true , 'msg' => 'Custom vars deleted!'];

			$customField = CustomField::uid($id)->first() ;
			$customField->deleted = 1;
			$customField->save();

			$ret['customField'] = $customField;

			return response()->json($ret);
	}
}
