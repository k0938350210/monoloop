<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Services\Profile\Profile;
use Input;
use App\Segment;
use App\Goal;
use App\Folder;

use View;
use Response;
use Auth;

class ProfileController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

    /*
   * Return the properties of required condition parameters
   *
   * @return json
   */

  public function index(Request $request)
  {
      $profile = new Profile();
      // $folder = new Folder();

      // $account_id = Auth::user()->active_account;

      $properties = $profile->getProperties();

      // $segments = Segment::ByAccount(Auth::user()->active_account)->get();
      //
      // $segmentsProperties = [];
      //
      // if(!empty($segments)){
      //
      //   // $folders = $folder->folders($account_id);
      //
      //   foreach ($segments as $segment) {
      //     $key = 'segment_'.$segment->uid;
      //     $segmentsProperties[$key] = ['key' => $key, 'Label' => $segment->name, 'Group' => 'Audiences', 'Type' => 'bool', 'Default' => true];
      //   }
      // }

      // $goals = Goal::ByAccount($account_id)->get();
      //
      // $goalsProperties = [];
      //
      // if(!empty($goals)){
      //   foreach ($goals as $goal) {
      //     $goalsProperties[] = array('Label' => $goal->name, 'Value' => $goal->uid);
      //   }
      // }
      //
      // if(count($goalsProperties) < 1){
      //   $properties['GoalReached']['Values'] = [];
      // } else {
      //   $properties['GoalReached']['Values'][0]['Values'] = $goalsProperties;
      // }


      // return response()->json($properties + $segmentsProperties);
      return response()->json($properties);
  }

}
