<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Hasher;
use App\Account;
use App\AccountUser;

use App\Http\Requests\Account\ProfileRequest ;

class UserController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function profile(Request $request)
	{
		$ret = ['success' => false ] ;

    $account = Auth::user()->active_account ;
    if(empty($account)){
      return response()->json($ret, 404);
    }
    $ret['success'] = true ;
    $ret['user'] = Auth::user() ;
		$ret['account'] = Account::find($account) ;
		$ret['accountUser'] = AccountUser::where('account_id', '=', $account->{'$id'})->first() ;
    return response()->json($ret, 200);

	}

	public function profileUpdate(ProfileRequest $request){
		$ret = ['success' => true , 'msg' => 'Save profile success.'];

		$account = Auth::user()->active_account ;

		$accountUser = AccountUser::where('account_id', '=', $account->{'$id'})->first();

		// $config = Auth::user()->currentAccountConfig() ;
		// var_dump($config);die;
		$accountUser->name = $request->input('name');
		$accountUser->email = $request->input('email');
		$accountUser->save() ;

		$accountModel = Account::find($account);
		$accountModel->contact_name = $request->input('name');
		$accountModel->contact_email = $request->input('email');
		$accountModel->account_type = $request->input('account_type');
		$accountModel->save();

		$pass = $request->input('password') ;
		if($pass != ''){
			$user = Auth::user() ;
			$hasher = new Hasher() ;
			$user->pass = $hasher->getHashedPassword($pass) ;
			$user->Save()  ;
	 	}

		return response()->json( $ret ) ;
	}

}
