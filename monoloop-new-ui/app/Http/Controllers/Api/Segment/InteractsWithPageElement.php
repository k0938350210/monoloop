<?php namespace App\Http\Controllers\Api\Segment ;

use DB ;
use App\Services\MonoloopCondition ;
use App\PageElement ;

trait InteractsWithPageElement
{

  private function UpdatePageElement($segment){
    $pagElement = new PageElement() ;
    $collection = $pagElement->getTable() ;
    $db = DB::getMongoDB();

    $conditionObj = new MonoloopCondition($segment->condition);
    $conditionObj->replacePipe( '  return true ;  ') ;
    $conditionstr = ' <%  Segments[\'segment_' . $segment->uid.'\']  = function(){ '.$conditionObj .'  return false ;};  %> ';

    $res = $db->$collection->update(['content.segments.segment_' . $segment->uid => ['$exists' => true ]],
                             ['$set' => [ 'content.$.segments.segment_' . $segment->uid  => $conditionstr ]] ,
                             ['multiple' => true]);
    return ;
  }
}
