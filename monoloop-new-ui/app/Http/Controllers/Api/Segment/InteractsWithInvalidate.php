<?php namespace App\Http\Controllers\Api\Segment ;

use DB ;
use App\Services\MonoloopCondition ;
use App\PageElement ;
use App\Services\Frontend\Invalidator ;


trait InteractsWithInvalidate
{
  private function Invalidate($segment){
    $invalidator = new Invalidator() ;
    $invalidator->removeCached( $segment->account->uid . '_segments') ;

    $pageElements = PageElement::where('content.segments.segment_'.$segment->uid, 'exists', true)->get();
    /*
    foreach($pageElements as $pageElement){
      $invalidator->removeCached(  $pageElement->cid  . '_pageelements_' .$pageElement->urlhash ) ;
    }
    */
    if(count($pageElements) > 0){
      $invalidator->removeCached(  $pageElement->cid  . '_pageelements_' ) ;
    }
  }
}
