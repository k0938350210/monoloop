<?php namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\V1\Common\ApiFilter;
use App\Http\Controllers\Api\V1\Common\ApiLink;

use App\Services\Profile\Profile ;

use Auth ;
use Validator;
use MongoId ;

class ConditionPropertyController  extends Controller {

  public function __construct()
	{
		$this->middleware('auth.api');
	}

  private function getAllProperties(){
    $profile = new Profile() ;
    $properties = $profile->getProperties() ;

    $data = array() ;
  	$connector = 'MonoloopProfile';
    foreach($properties as $k => $property){
      if( ! isset($property['qtipCfg'])){
 				$property['qtipCfg'] = array('text' => '') ;
 			}
 		 	$p = array(
			  	'href' => '/properties/' . $k . '/' ,
			  	'id' => $k ,
			  	'name' => $property['Label'] ,
			  	'description' => $property['qtipCfg']['text'] ,
			  	'type' =>  $property['Type']  ,
			) ;

			if( ! empty($property['Values'])){
				if( is_array($property['Values'])){
					$p['values'] = $property['Values'] ;
				}else{
					$p['remoteValues'] = $property['Values'] ;
				}
			}

			if( $p['type'] == 'single' || $p['type'] == 'single remote' || $p['type'] == 'single remote2' ){
				$p['type'] = 'string' ;
			}


			$p['connector'] = $connector ;
			if($p['type'] == 'function'){
				$p['returnValue'] = $property['ReturnValue'] ;
				if( ! isset($p['values']))
					continue ;
				$params = $p['values'] ;
				//print_r($params) ;
				$p2 = array() ;
				if(!empty($params)){
					foreach($params as $param){
						if(! isset($param['Name']))
							$param['Name'] = '' ;

						$temp = array(
							'label' => $param['Label'] ,
							'type' => $param['Type'] ,
							'name' => $param['Name'] ,
						) ;

						if( $temp['type'] == 'single'){
							$temp['values'] = $param['Values'] ;
							$temp['type']  = 'string' ;
						}else if( $temp['type'] == 'lookup' || $temp['type'] == 'superboxselect' ){
							$temp['remoteValues'] = $param['Values'] ;
							$temp['type']  = 'string' ;
						}
						$p['parameters'][] =  $temp ;
					}
				}
				unset($p['values']) ;
			}
			$data[] = $p ;
    }
    return $data ;
  }

  public function index(Request $request){


    $data = $this->getAllProperties() ;

    $post = array() ;
  	$post['order'] = $request->input('order','desc');
  	$post['limit'] = $request->input('limit',20);
  	$post['filter'] = $request->input('filter','');
  	$post['sortBy'] = $request->input('sortBy','name');
  	$post['offset'] = $request->input('offset',0);

    $newSort = array() ;

  	foreach($data as $d){
  		// Check for filter ;
  		if( trim($post['filter']) != '' ){
  			if( strpos($d['name'] . ' ' . $d['description'] . ' ' . $d['type'] , $post['filter']) === false){
  				continue ;
  			}
  		}
  		if( empty($newSort )){
  			$newSort[] = $d ;
  			continue ;
  		}

  		if( ! in_array($post['sortBy'], array('type' , 'name' , 'id' , 'connector') ) ){
  			$newSort[] = $d ;
  			continue ;
  		}

  		$newData = array($d) ;
  		if( $post['order'] == 'desc'){
  			for($i = 0 ; $i < count($newSort) ; $i++ ){
  				if( strcasecmp($d[$post['sortBy']] , $newSort[$i][$post['sortBy']] ) > 0){
  					// Hey it grater ;
  					array_splice( $newSort, $i, 0, $newData );
  					break ;
  				}
  				if( $i == count($newSort) -1 ){
  					$newSort[] = $d ;
  					break ;
  				}
  			}
  		}else{
  			for($i = 0 ; $i < count($newSort) ; $i++ ){
  				if( strcasecmp($d[$post['sortBy']] , $newSort[$i][$post['sortBy']] ) < 0){
  					// Hey it grater ;
  					array_splice( $newSort, $i, 0, $newData );
  					break ;
  				}
  				if( $i == count($newSort) -1 ){
  					$newSort[] = $d ;
  					break ;
  				}
  			}
  		}
  	}

  	$newSort2 = array() ;
  	for($i = 0 ; $i < count($newSort) ; $i++ ){
  		if( $i >= $post['offset'] && count($newSort2) <= $post['limit']){
  			$newSort2[] = $newSort[$i] ;
  		}

  		if(  count($newSort2) > $post['limit'] ){
  			break ;
  		}
  	}

    $ret = array()  ;
    $ret['totalCount'] = count($newSort) ;

      if( empty($newSort2) || count($newSort2) < $post['limit'])
  		$ret['next'] = null ;
  	else
  		$ret['next'] = '/properties/?order=' . $post['order']  . '&limit=' . $post['limit'] . '&filter=' . $post['filter'] . '&sortBy=' . $post['sortBy'] . '&offset=' . ($post['offset'] +  $post['limit']) ;
  	if( $post['offset'] == 0)
  		$ret['prev'] = null ;
  	else
  		$ret['prev'] = '/properties/?order=' . $post['order']   . '&limit=' . $post['limit'] . '&filter=' . $post['filter'] . '&sortBy=' . $post['sortBy'] . '&offset=' . ($post['offset'] -  $post['limit']) ;
    $ret['properties'] = $newSort2 ;

    return response()->json($ret,200);
  }

  public function show(Request $request , $id){
    $data = $this->getAllProperties() ;
    $ret = null ;
    foreach($data as $row){
      if($row['id'] == $id){
        $ret = $row ;
        break ;
      }
    }

    if(empty($ret) ){
      return response()->json(['status' => 'error' ,'href' => '/properties/' . $id . '/'  ,'message' => 'Resource not found'],400);
    }

    return response()->json($ret,200);
  }

}
