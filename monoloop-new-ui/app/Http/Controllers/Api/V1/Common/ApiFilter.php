<?php namespace App\Http\Controllers\Api\V1\Common;
use MongoId ;
trait ApiFilter{
  public function apifilter($request , $query){
    $limit = $request->input('limit',20) ;
    $sort = $request->input('sortBy','created_at') ;
    $order = $request->input('order','desc');
    $folder = $request->input('folder',null) ;
    $offset = $request->input('offset',0) ;
    if(! is_null($folder)){
      $query = $query->where('folder_id','=',new MongoId($folder)) ;
    }
    if($offset > 0){
      $query = $query->skip($offset) ; 
    }
    $query = $query->orderBy($sort,$order)->limit($limit) ;
    return $query ;
  }
}
