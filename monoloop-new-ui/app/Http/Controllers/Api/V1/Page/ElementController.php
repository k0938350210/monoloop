<?php namespace App\Http\Controllers\Api\V1\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\V1\Common\ApiFilter;
use App\Http\Controllers\Api\V1\Common\ApiLink;
use App\Http\Controllers\Api\V1\Common\ResponseArray;

use Auth ;
use Validator;
use MongoId ;

use App\Placement;
use App\PlacementDetail;
use App\UrlConfig;
use App\Content;

class ElementController extends Controller {
  use ApiFilter ;
  use ApiLink ;
  use ResponseArray ;

  private $rawRequest ;

  public function __construct()
	{
		$this->middleware('auth.api');
	}

  private $postRules = [
    'eid' => 'required|integer|exists:contents,uid' ,
    'position' => 'required|integer|max:4|min:1' ,
    'order' => 'integer' ,
    'displayType' => 'integer',
    'active' => 'boolean' ,
    'xpath' => 'required'
  ] ;

  private function formatRequest(Request $request){
    $order = $request->input('order','desc') ;
    $limit = $request->input('limit',20) ;
    $sortBy = $request->input('sortBy','created_at');
    $offset = $request->input('offset',0) ;

    $sortByMapp = [
      'updatedAt' => 'updated_at' ,
      'createdAt' => 'created_at' ,
      'active' => 'hidden'
    ];

    if(isset($sortByMapp[$sortBy])){
      $sortBy = $sortByMapp[$sortBy] ;
    }

    $request->merge(['order'=>$order,'limit'=>$limit,'sortBy'=>$sortBy,'offset'=>$offset]);
  }

  private function rowsFormat($rows , $id ){
    $ret = [];
    if(count($rows)>0){
      foreach($rows as $row){
        $ret[] = $this->rowFormat($row , $id);
      }
    }
    return $ret ;
  }

  private function rowFormat($row , $id ){
    return [
      'uid' => (int)$row->uid ,
      'href' => '/pages/' . $id . '/elements/' . (int)$row->uid . '/'  ,
      'updatedAt' => $row->updated_at->toIso8601String() ,
      'createdAt' => $row->created_at->toIso8601String() ,
      'active' => !(bool)$row->hidden ,
      'position' => (int)$row->position ,
      'order' => (int)$row->order ,
      'xpath' => $row->xpath ,
      'element' => $this->contentResponse($row->content)
    ];
  }

  public function index(Request $request , $id){
    $this->rawRequest = $request->all() ;
    $this->formatRequest($request) ;

    $validator = Validator::make($request->all(), [
		  'limit' => 'digits_between:0,500' ,
      'sortBy' => 'in:updated_at,created_at,hidden' ,
      'order' => 'in:asc,desc'
		]);

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }
    $placement = Placement::byAccount(Auth::user()->active_account)->byUid($id)->available()->first() ;
    if(empty($placement)){
      return response()->json(['status' => 'error' ,'href' => '/pages/'.$id . '/elements/','message' => 'Resource not found'],400);
    }

    $details = $placement->details()->where('deleted','=',0) ;

    $totalCount = $details->count() ;
    $this->apifilter($request,$details) ;
    $details = $details->get() ;

    return response()->json([
      'status' => 'success' ,
      'totalCount' => $totalCount ,
      'next' =>  $this->getNextURL($request, '/pages/'.$id . '/elements/') ,
      'prev' =>  $this->getPrevURL($request, '/pages/'.$id . '/elements/') ,
      'relations' => $this->rowsFormat($details , $id ) ,
    ],200);
  }

  public function show(Request $request , $id , $eid){
    $details = Placement::byAccount(Auth::user()->active_account)->byUid($id)->available()->first()->details() ;
    $detail = $details->where('uid','=',(int)$eid)->where('deleted','=',0)->first() ;
    //$detail = $details->ByUid($eid)->first() ;
    if(empty($details) || empty($detail)){
      return response()->json(['status' => 'error' ,'href' => '/pages/'.$id . '/elements/' . $eid . '/'  ,'message' => 'Resource not found'],400);
    }
    return response()->json($this->rowFormat($detail,$id),200);
  }

  public function store(Request $request , $id ){
    $validator = Validator::make($request->all(), $this->postRules );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }
    $placement = Placement::byAccount(Auth::user()->active_account)->byUid($id)->available()->first() ;
    $content = Content::byAccount(Auth::user()->active_account)->byUid($request->input('eid'))->available()->first();

    if(empty($placement) || empty($content)){
      return response()->json(['status' => 'error' ,'message' => 'Resource not found'],400);
    }

    $placementDetail = new PlacementDetail() ;
    $placementDetail->deleted = 0 ;
    $placementDetail->hidden = (int)!$request->input('active',false) ;
    $placementDetail->xpath = $request->input('xpath');
    $placementDetail->position = $request->input('position');
    $placementDetail->order = $request->input('order',1);
    $placementDetail->display_type = $request->input('displayType',1);
    $placementDetail->content_id = new MongoId( $content->_id ) ;
    $newUid = 1 ;
    $maxUid = Placement::max('details.uid') ;
    if(is_array($maxUid)){
      $newUid = $maxUid[0]+1 ;
    }

    $placementDetail->uid = $newUid;

    $placement->details()->save($placementDetail);

    return response()->json(['status' => 'created' , 'uid' => $placement->uid , 'href' => '/pages/'.$placement->uid.'/elements/' . $placementDetail->uid . '/' ],201);
  }

  public function update(Request $request , $id , $eid){
    $validator = Validator::make($request->all(), $this->postRules );
    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }
    $content = Content::byAccount(Auth::user()->active_account)->byUid($request->input('eid'))->available()->first();
    $placement = Placement::byAccount(Auth::user()->active_account)->byUid($id)->first() ;
    $placementDetail =  $placement->details()->where('uid','=',(int)$eid)->where('deleted','=',0)->first() ;
    if(empty($placement) || empty($placementDetail) || empty($content)){
      return response()->json(['status' => 'error' ,'href' => '/pages/'.$id . '/elements/' . $eid . '/' ,'message' => 'Resource not found'],400);
    }

    $placementDetail->hidden = (int)!$request->input('active',false) ;
    $placementDetail->xpath = $request->input('xpath');
    $placementDetail->position = $request->input('position');
    $placementDetail->order = $request->input('order');
    $placementDetail->display_type = $request->input('displayType',1);
    $placementDetail->content_id = new MongoId( $content->_id ) ;
    $placementDetail->save() ;
    return response()->json(['status' => 'updated' , 'uid' => $placement->uid , 'href' => '/pages/'.$placement->uid.'/elements/' . $placementDetail->uid . '/' ],201);

  }

  public function destroy(Request $request , $id , $eid){
    $placement = Placement::byAccount(Auth::user()->active_account)->byUid($id)->first() ;
    $detail =  $placement->details()->where('uid','=',(int)$eid)->where('deleted','=',0)->first() ;
    if(empty($placement) || empty($detail)){
      return response()->json(['status' => 'error' ,'href' => '/pages/'.$id . '/elements/' . $eid . '/' ,'message' => 'Resource not found'],400);
    }
    $detail->deleted = 1 ;
    $detail->save() ;

    return response()->json(['status' => 'success' ,  'message' => 'Page element deleted'],200);
  }
}
