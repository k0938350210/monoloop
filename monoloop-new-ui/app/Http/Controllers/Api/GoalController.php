<?php namespace App\Http\Controllers\Api;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Goal as FORMS;
use Illuminate\Contracts\Validation\Validator;
use App\Segment;
use App\Goal;
use App\UrlConfig ;
use App\Folder;
use Input;
use App\PageElement;

use App\Events\Logger;
use App\Services\PageElement\ContentGenerator ;
use App\Http\Controllers\Api\Goal\InteractsWithPageElement ;

class GoalController extends Controller
{
	use InteractsWithPageElement ;
		/**
		 * __construct performing pre actions before all the mentioned actions
		 * @return JSON
		 */
		public function __construct()
		{
			$this->middleware('auth');
		}
    /*
     * Action is finding all segments and associated folders of logged in user
     *
		 * @params $type
		 *
     * @return Json
     */
    public function index(Request $request, $type = "tree")
    {
        $ret = [];

        $account_id = Auth::user()->active_account;
				$account_cid = Auth::user()->account->uid;
				$folder_id = Input::get('source', NULL);

				if(!empty($folder_id)){
					$folder = explode('__', $folder_id);
					if(is_array($folder) && count($folder) === 2){
						$folder_id = $folder[1];
					}
				}

				$f = Folder::find($folder_id);
				$isRoot = false;
				if($f){
					$isRoot = empty($f->parent_id) ? true : false;
				}


				switch ($type) {
					case 'compact-list':
						$ret['records'] = Goal::CompactList($account_cid);
						break;
					case 'folder':
						$ret['records'] = Goal::goals($folder_id, $isRoot, $account_cid);
						break;
					default:
						$folder = new Folder();
						$ret['records'] = $folder->goalsFolders($account_id);
						break;
				}



        return response()->json($ret['records']);
    }

    /*
    * Action is a segment from id
    *
    * @return Json
    */
    public function show(FORMS\ShowRequest $request)
    {
        $ret = [];

        $srcTypeId = explode('__', $request->input('_id'));

        $ret['goal'] = Goal::find($srcTypeId[1]);
				if(is_object($ret['goal']->urlConfig)){
					$ret['goal']->urlConfig->url_option = $ret['goal']->urlConfig->getOptionStringAttribute($ret['goal']->urlConfig->url_option);
				}
				if(!empty($ret['goal']->page_element_id)){
					$ret['goal']->page_element = PageElement::find((string)$ret['goal']->page_element_id);
				}
        return response()->json($ret['goal']);
    }

    /*
    * Action is creating a new segment
    *
    * @return Json
    */
    public function create(FORMS\AddForm $request)
    {
        $ret = ['success' => true , 'msg' => 'Goal created!'];

				$account_id = Auth::user()->active_account;

				$src = $request->input('source', false);
				if($src && strlen($src) > 0){
					$folder_id = explode("__", $request->input('source'));
				} else {
					$folder_id = new \MongoId(Folder::rootFolders($account_id)->first()->_id);
				}

				$page_element_id = $request->input('page_element_id', '') ;



        $goal = new Goal();
        $goal->name = $request->input('nameGoal');
        $goal->type = $request->input('typeGoal');
        $goal->condition = $request->input('conditionGoal', '');
        $goal->point = $request->input('pointsGoal');
        $goal->account_id = $account_id;
				$goal->cid = Auth::user()->account->uid;
				$goal->uid = ContentGenerator::newUid() ;

				if(strlen($page_element_id) > 0){
					$goal->page_element_id  = new \MongoId($page_element_id);
				}

        $goal->deleted = 0;
        $goal->hidden = 0;
				if(is_array($folder_id)){
					$goal->folder_id = new \MongoId($folder_id[1]);
				} else {
					$goal->folder_id = new \MongoId(Folder::rootFolders($account_id)->first()->_id);
				}
        $goal->save();


        // $urlConfig =  new UrlConfig() ;
				// $urlConfig->url = $request->input('placement_urlGoal') ;
				// $urlConfig->reg_ex = $request->input('placement_reg_exGoal', '') ;
				// $urlConfig->remail = $request->input('placement_remarkGoal', '') ;
				// $urlConfig->inc_www = (int)$request->input('placement_inc_wwwGoal') ;
				// $urlConfig->inc_http_https = (int)$request->input('placement_inc_http_httpsGoal') ;
				// $urlConfig->setUrlOptionFromString($request->input('placement_url_optionGoal'));
				// $goal->urlConfig()->save($urlConfig);
				//
				// #update to page element ;
				// $this->UpdatePageElement($goal);

        $ret['source'] = 'goal';
        $ret['content']['goal'] = $goal;
        $goalId = new \MongoId($goal->_id);
        \Event::fire(new Logger(Auth::user()->account->name, " created goal", $goal->name , "App\Goals",$goalId));


        return response()->json($ret);
    }

    /*
    * Action is updating a goal
    *
    * @return Json
    */
    public function update(FORMS\EditForm $request)
    {
        $ret = ['success' => true , 'msg' => 'Goal updated!'];

        $srcId = $request->input('_id');

        $goal = Goal::find($srcId);
				$goal->name = !empty($request->input('nameGoal')) ? $request->input('nameGoal') : $goal->name;
        $goal->type = !empty($request->input('typeGoal')) ? $request->input('typeGoal') : $goal->type;
        $goal->condition = $request->input('conditionGoal', '');
        $goal->point = !empty($request->input('pointsGoal')) ? $request->input('pointsGoal') : $goal->point;
				// $urlConfig =  $goal->urlConfig ;
				// $urlConfig->url = $request->input('placement_urlGoal') ;
				// $urlConfig->reg_ex = $request->input('placement_reg_exGoal') ;
				// $urlConfig->inc_www = (int)$request->input('placement_inc_wwwGoal',0) ;
				// $urlConfig->inc_http_https = (int)$request->input('placement_inc_http_httpsGoal',0) ;
				// $urlConfig->setUrlOptionFromString($request->input('placement_url_optionGoal','exactMatch'));
				// $goal->urlConfig()->save($urlConfig);

        $goal->save();

				#update to page element ;
				// $this->UpdatePageElement($goal);

				$ret['source'] = 'goal';
        $ret['content']['goal'] = $goal;
        $goalId = new \MongoId($goal->_id);
        \Event::fire(new Logger(Auth::user()->account->name, " updated a goal", $goal->name , "App\Goals",$goalId));


        return response()->json($ret);
    }

		/*
    * Toggle hidden value active/inactive
    *
    * @return Json
    */
    public function updateStatus(FORMS\StatusUpdateForm $request)
    {
        $ret = ['success' => true , 'msg' => 'Goal activated!'];

				if (strpos($request->input('_id'), '__') !== false) {
            $srcTypeId = explode('__', $request->input('_id'));
            $srcId = $srcTypeId[1];
        } else {
            $srcId = $request->input('_id');
        }

        $goal = Goal::find($srcId);
				$goal->hidden = $request->input('hidden');
        $goal->save();

				#update to page element ;
				//$this->UpdatePageElement($goal);
        $goalId = new \MongoId($goal->_id);

				if($goal->hidden == "1"){
					$ret['msg'] = 'Goal deactivated';
                    \Event::fire(new Logger(Auth::user()->account->name, " has deactivated goal status", $goal->name , "App\Goals",$goalId));

                }else{
                    \Event::fire(new Logger(Auth::user()->account->name, " has activated goal status", $goal->name , "App\Goals",$goalId));

                }

				$ret['source'] = 'goal';
        $ret['content']['goal'] = $goal;


        return response()->json($ret);
    }

		/*
    * change folder of goal
    *
    * @return Json
    */
    public function changeFolder(FORMS\ChangeFolderForm $request)
    {
        $ret = ['success' => true , 'msg' => 'Goal is moved to new folder.'];

				if (strpos($request->input('_id'), '__') !== false) {
            $srcTypeId = explode('__', $request->input('_id'));
            $srcId = $srcTypeId[1];
        } else {
            $srcId = $request->input('_id');
        }

				$target = explode('__', $request->input('target'));

        $goal = Goal::find($srcId);
				$goal->folder_id = new \MongoId($target[1]);
        $goal->save();

				$ret['source'] = 'goal';
        $ret['content']['goal'] = $goal;


        return response()->json($ret);
    }

		/*
    * Action is deleting a goal
    *
    * @return Json
    */
    public function delete(FORMS\DeleteForm $request)
    {
        $ret = ['success' => true , 'msg' => 'Goal deleted!'];

        if (strpos($request->input('_id'), '__') !== false) {
            $srcTypeId = explode('__', $request->input('_id'));
            $srcId = $srcTypeId[1];
        } else {
            $srcId = $request->input('_id');
        }

        $goal = Goal::find($srcId);
				$goal->deleted = 1;
        $goal->save();

				$ret['source'] = 'goal';
        $ret['content']['goal'] = $goal;

				#remove from page element ;
				//$this->removaPageElement($goal);
        $goalId = new \MongoId($goal->_id);
        \Event::fire(new Logger(Auth::user()->account->name, " deleted a goal", $goal->name , "App\Goals",$goalId));

        return response()->json($ret);
    }

}
