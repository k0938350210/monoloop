<?php namespace App\Http\Controllers\Api;

use App\Events\ExperimentHistoryLog;
use App\ExperimentHistory;
use App\Segment;
use Auth;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExperimentHistory as History;
use App\Http\Requests\Experiment as FORMS;
use App\Webhook;
use App\Tracker;
use App\Folder;
use App\PageElement;
use App\PageElementContent;
use App\PageElementExperiment;
use App\UrlConfig;
use App\Goal;

use App\Events\Logger;
use App\Events\Invitation;
use Input;
use App\Http\Controllers\Api\Component\Reports\Api\ExperimentsController;


class WebhookController extends Controller{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function index(Request $request, $type = "tree")
  {

    $ret = [];

		$folder_id = Input::get('source', NULL);
		if(!empty($folder_id)){
			$folder = explode('__', $folder_id);
			if(is_array($folder) && count($folder) === 2){
				$folder_id = $folder[1];
			}
		}

		$account_id = Auth::user()->active_account;
		$account_cid = Auth::user()->account->uid;

		$f = Folder::find($folder_id);
		$isRoot = false;
		if($f){
			$isRoot = empty($f->parent_id) ? true : false;
		}

		$ret['webhooks'] = [];
    switch ($type) {
			case 'compact-list':
				break;
			case 'folder':
				$eids = [] ;
				$ret['webhooks'] = Webhook::webhooks($folder_id, $isRoot, $account_cid);
				break;
			default:
				$folder = new Folder();
				$ret['webhooks'] = $folder->experimentsFolders($account_cid);
				break;
		}
    return response()->json( $ret['webhooks'] ) ;
		}



  public function getWebhook(Request $request, $id){
      $account_id = Auth::user()->active_account;
      return response()->json(Webhook::where('account_id', '=', $account_id)
          ->where('webhookID', '=', (int)$id)
          ->get());
  }
  public function show(Request $request)
	{
		$ret = [];

		$srcTypeId = explode("__", $request->input('_id'));

		$ret['webhook'] = Webhook::find($srcTypeId[0]);
		return response()->json( $ret) ;
	}


  public function getallWebhooks(Request $request){
    $account_id = Auth::user()->active_account;
    return response()->json(Webhook::where('status', '=', 'active')->get());
  }



  public function create(Request $request){
  $ret = ['success' => true , 'msg' => 'Webhook created!'] ;

  $src = $request->input('source', false);
  if($src && strlen($src) > 0){
    $folder_id = explode("__", $request->input('source'));
  } else {
    $folder_id = null;
  }
  $account_id = Auth::user()->active_account;
  $active_account = Auth::user()->account;


  $webhook = new Webhook();
  $webhook->name = $request->input('nameWebhook');
  $webhook->cid = Auth::user()->account->uid;
  $webhook->account_id = $account_id;
  $webhook->event_type = $request->input('event_type');
  $webhook->event_name = $request->input('event_name');
  $webhook->event_id = $request->input('event_id');
  $webhook->direction = $request->input('direction');
  $webhook->callback_URL = $request->input('callback_URL');
  $webhook->payload_type = $request->input('payload_type');

  $webhook->aws_message_id = 0;
  $webhook->retry_count = 0;
  $webhook->timestamp = 0;

  $webhook->status = "active";
  $webhook->deleted = 0;




  if(is_array($folder_id)){
    $webhook->folder_id = new \MongoId($folder_id[1]);
  } else {
    $webhook->folder_id = new \MongoId(Folder::rootFolders($account_id)->first()->_id);
  }



  $webhook->save() ;
  $ret['source'] = 'webhook';
  $ret['content']['webhook'] = $webhook;
  return response()->json( $ret ) ;
      // $webhookId = new \MongoId($webhook->_id);
      // \Event::fire(new Logger(Auth::user()->account->name, " created experiment", $webhook->name , "App\Experiment",$webhookId));
      // $this->setHistoryLogs($request,$goal,$segment,$webhook);
}

public function delete(Request $request)
{

    $ret = ['success' => true , 'msg' => 'Webhook deleted!'];

    if (strpos($request->input('_id'), '__') !== false) {
        $srcTypeId = explode('__', $request->input('_id'));
        $srcId = $srcTypeId[1];
    } else {
        $srcId = $request->input('_id');
    }

    $webhook = Webhook::find($srcId);
    $webhook->deleted = 1;
    $webhook->save();

    $ret['source'] = 'webhook';
    $ret['content']['webhook'] = $webhook;


    return response()->json($ret);
}

}
