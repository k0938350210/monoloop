<?php namespace App\Http\Controllers\Api\Tracker;
use Auth;
use MongoId;
use Carbon;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Common\InteractsWithCbr ;

use App\Services\ExtjsFilter;

use App\TrackerNode ;
use App\Asset ;
use App\AssetDetail ;
use App\UrlConfig ;


class AssetController extends Controller {
  use InteractsWithCbr ;

  public function __construct()
	{
		$this->middleware('auth');
	}

  /*--- restful ---*/

  public function index(Request $request){

    $query = TrackerNode::byAccount(Auth::user()->active_account)->byAvailable()->where('type','asset') ;
		return response()->json([
			'assets' => $query->get() ,
			'totalCount' => $query->count()
		]);
  }

  public function store(Request $request){
    $ret = ['success' => true , 'msg' => 'Asset created!'] ;

    $trackerNode = new TrackerNode();

    $folder_id = $request->input('folder_id') ;
    if($folder_id == 0)
      $folder_id = null ;

    $trackerNode->account_id = Auth::user()->active_account ;
    $trackerNode->type = 'asset' ;
    $trackerNode->hidden = 1 ;
    $trackerNode->deleted = 0 ;
    $trackerNode->nid = TrackerNode::max('nid')+1;
    $trackerNode->folder_id = $folder_id ;
    $trackerNode->customvar = null ;
    $trackerNode->tracker = null ;
    $trackerNode->name = $request->input('name') ;
    $trackerNode->save() ;

    $urlConfig = new UrlConfig() ;
    $urlConfig->url = $request->input('url_config.url') ;
    $urlConfig->url_option = (int)$request->input('url_config.url_option',0);
    $urlConfig->inc_www = (int)$request->input('url_config.inc_www',0);
    $urlConfig->inc_http_https = (int)$request->input('url_config.inc_http_https',0);
    $urlConfig->reg_ex = $request->input('url_config.reg_ex');
    $trackerNode->urlConfig()->save( $urlConfig ) ;

    $asset = new Asset() ;
    $asset->ttl = (int)$request->input('ttl') ;
    $asset->example_url = $request->input('example_url');
    $trackerNode->asset()->save( $asset ) ;

    $details = $request->input('asset_details');
    foreach($details as $detail){
      $assetDetail = new AssetDetail() ;
      $assetDetail->field_name = $detail['field_name'] ;
      $assetDetail->field_type = $detail['field_type'] ;
      $assetDetail->type_id = (int)$detail['type_id'] ;
      $assetDetail->type_data = $detail['type_data'] ;
      $assetDetail->filter_type_id = (int)$detail['filter_type_id'] ;
      $assetDetail->filter_type_data = $detail['filter_type_data'] ;
      $assetDetail->save_function = (int)$detail['save_function'] ;
      $asset->assetDetails()->save($assetDetail) ;
    }

    $this->generateCbr($trackerNode->account) ;

    return response()->json($ret);
  }

  public function update(Request $request , $id ){
    $ret = ['success' => true , 'msg' => 'Asset updated!'] ;

    $trackerNode = TrackerNode::find($id) ;
    $trackerNode->name = $request->input('name') ;
    $trackerNode->hidden = 1 ;

    $urlConfig = $trackerNode->urlConfig ;
    $urlConfig->url = $request->input('url_config.url') ;
    $urlConfig->url_option = $request->input('url_config.url_option',0);
    $urlConfig->inc_www = $request->input('url_config.inc_www',0);
    $urlConfig->inc_http_https = $request->input('url_config.inc_http_https',0);
    $urlConfig->reg_ex = $request->input('url_config.reg_ex');
    $urlConfig->save() ;

    $asset = $trackerNode->asset ;
    $asset->ttl = (int)$request->input('ttl') ;
    $asset->example_url = $request->input('example_url');
    $asset->asset_details = [] ;
    $asset->save() ;


    $details = $request->input('asset_details');

    foreach($details as $detail){
      $assetDetail = new AssetDetail() ;
      $assetDetail->field_name = $detail['field_name'] ;
      $assetDetail->field_type = $detail['field_type'] ;
      $assetDetail->type_id = (int)$detail['type_id'] ;
      $assetDetail->type_data = $detail['type_data'] ;
      $assetDetail->filter_type_id = (int)$detail['filter_type_id'] ;
      $assetDetail->filter_type_data = $detail['filter_type_data'] ;
      $assetDetail->save_function = (int)$detail['save_function'] ;
      $asset->assetDetails()->save($assetDetail) ;
    }
    $trackerNode->save() ;

    $this->generateCbr($trackerNode->account) ;

    return response()->json($ret);
  }


}
