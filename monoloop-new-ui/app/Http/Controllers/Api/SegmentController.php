<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Requests\Segment as FORMS;
use App\Http\Controllers\Controller;
use App\Segment;
use App\Folder;
use App\Account;
use App\User;
use Input;


use App\Events\Logger;

use App\Http\Controllers\Api\Segment\InteractsWithPageElement ;

class SegmentController extends Controller {
	use InteractsWithPageElement ;

	public function __construct()
	{
		$this->middleware('auth');

	}
 	/*
 	 * Action is finding all segments and associated folders of logged in user
 	 *
	 * @params $type
	 *
 	 * @return Json
 	 */
	public function index(Request $request, $type = "tree")
	{
		$ret = [];

		$folder_id = Input::get('source', NULL);

		if(!empty($folder_id)){
			$folder = explode('__', $folder_id);
			if(is_array($folder) && count($folder) === 2){
				$folder_id = $folder[1];
			}
		}

		$ret['segments'] = [];
		$account_id = Auth::user()->active_account;
		$account_cid = Auth::user()->account->uid;

		switch ($type) {
			case 'compact-list':
				$ret['segments'] = Segment::CompactList($account_cid);
				break;
			case 'folder':
				$ret['segments'] = Segment::segments($folder_id);
				break;
			default:
				$folder = new Folder();
				$ret['segments'] = $folder->segmentsFolders($account_cid);
				break;
		}
		return response()->json( $ret['segments'] ) ;
	}

	/*
	* Action is a segment from id
	*
	* @return Json
	*/
	public function show()
	{
		$ret = [];
		$srcTypeId = explode("__", Input::get('_id'));

		$ret['segment'] = Segment::find($srcTypeId[1]);
		// $ret['segment']->condition_json = $ret['segment']->getConditionJson();

		return response()->json( $ret['segment'] ) ;
	}

	/*
	* Action is creating a new segment
	*
	* @return Json
	*/
	public function create(FORMS\AddForm $request){
		$ret = ['success' => true , 'msg' => 'Segment created!'] ;

		$src = $request->input('source', false);
		if($src && strlen($src) > 0){
			$folder_id = explode("__", $request->input('source'));
		} else {
			$folder_id = null;
		}

		$account_id = Auth::user()->active_account;

		$segment = new Segment();
		$segment->name = $request->input('nameSegment');
		$segment->description = $request->input('descSegment');
		$segment->condition = $request->input('conditionSegment' , '');
    $segment->cid = Auth::user()->account->uid ;
		// $segment->condition_json = !empty($request->input('condition_json')) ? $request->input('condition_json') : $segment->condition_json;
		$segment->account_id = $account_id;
		$segment->deleted = 0;
		$segment->hidden = 0;
		if(is_array($folder_id)){
			$segment->folder_id = new \MongoId($folder_id[1]);
		} else {
			$rFid = Folder::rootFolders($account_id)->first()->_id;
			if($rFid){
				$segment->folder_id =  new \MongoId($rFid);
			} else {
				$segment->folder_id = null;
			}
		}

		$segment->save() ;
		$segment_id = new \MongoId($segment->_id);
		\Event::fire(new Logger(Auth::user()->account->name, " created segment", $segment->name , "App\Segment",$segment_id));

		$ret['source'] = 'segment';
		$ret['content']['segment'] = $segment;
		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a segment
	*
	* @return Json
	*/
	public function update(FORMS\EditForm $request){
		$ret = ['success' => true , 'msg' => 'Segment updated!'] ;

		$srcId = $request->input('_id');


		$segment = Segment::find($srcId);
		$segment->name = !empty($request->input('nameSegment')) ? $request->input('nameSegment') : $segment->name;
		$segment->description = !empty($request->input('descSegment')) ? $request->input('descSegment') : $segment->description;
		$segment->condition = $request->input('conditionSegment', '');
		// $segment->condition_json = !empty($request->input('condition_json')) ? $request->input('condition_json') : $segment->condition_json;
		$segment->save() ;
		$segment_id = new \MongoId($segment->_id);
		\Event::fire(new Logger(Auth::user()->account->name, " updated segment", $segment->name , "App\Segment",$segment_id));

		#update page element ;
		$this->UpdatePageElement($segment);


		$ret['source'] = 'segment';
		$ret['content']['segment'] = $segment;
		return response()->json( $ret ) ;
	}

	/*
	* Toggle hidden value active/inactive
	*
	* @return Json
	*/
	public function updateStatus(FORMS\StatusUpdateForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Goal activated!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$segment = Segment::find($srcId);
			$segment->hidden = $request->input('hidden');
			$segment->save();

			#update page element ;
			$this->UpdatePageElement($segment);


			if($segment->hidden == "1"){
				$ret['msg'] = 'Segment deactivated';
			}
			$segment_id = new \MongoId($segment->_id);
			if($segment->hidden == "1"){
				\Event::fire(new Logger(Auth::user()->account->name, " has deactivated segment", $segment->name , "App\Segment",$segment_id));
			} else {
				\Event::fire(new Logger(Auth::user()->account->name, " has activated segment", $segment->name , "App\Segment",$segment_id));
			}

			$ret['source'] = 'segment';
			$ret['content']['segment'] = $segment;
			return response()->json($ret);
	}

	/*
	* change folder of segment
	*
	* @return Json
	*/
	public function changeFolder(FORMS\ChangeFolderForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Segment is moved to new folder.'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$target = explode('__', $request->input('target'));

			$segment = Segment::find($srcId);
			$segment->folder_id = new \MongoId($target[1]);
			$segment->save();
			$segment_id = new \MongoId($segment->_id);
			\Event::fire(new Logger(Auth::user()->account->name, " has moved segment to another folder", $segment->name , "App\Segment",$segment_id));

			$ret['source'] = 'segment';
			$ret['content']['segment'] = $segment;
			return response()->json($ret);
	}

	/*
	* Action is deleting a segment
	*
	* @return Json
	*/
	public function delete(FORMS\DeleteForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Segment deleted!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$segment = Segment::find($srcId);
			$segment->deleted = 1;
			$segment->save();
			$segment_id = new \MongoId($segment->_id);
			\Event::fire(new Logger(Auth::user()->account->name, " has deleted segment", $segment->name , "App\Segment",$segment_id));

			#update page element ;
			$this->UpdatePageElement($segment);


			$ret['source'] = 'segment';
			$ret['content']['segment'] = $segment;
			return response()->json($ret);
	}
}
