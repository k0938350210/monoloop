<?php namespace App\Http\Controllers\Api;

use App\Events\ExperimentHistoryLog;
use App\ExperimentHistory;
use App\Segment;
use Auth;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExperimentHistory as History;
use App\Http\Requests\Experiment as FORMS;
use App\Experiment;
use App\Tracker;
use App\Folder;
use App\PageElement;
use App\PageElementContent;
use App\PageElementExperiment;
use App\UrlConfig;
use App\Goal;

use App\Events\Logger;
use App\Events\Invitation;
use Input;
use App\Http\Controllers\Api\Component\Reports\Api\ExperimentsController;

class ExperimentController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

 	/*
 	 * Action is finding all experiments and associated folders of logged in user
 	 *
	 * @params $type
	 *
 	 * @return Json
 	 */

	public function index(Request $request, $type = "tree")
	{
		$ret = [];

		$folder_id = Input::get('source', NULL);

		if(!empty($folder_id)){
			$folder = explode('__', $folder_id);
			if(is_array($folder) && count($folder) === 2){
				$folder_id = $folder[1];
			}
		}

		$account_id = Auth::user()->active_account;
		$account_cid = Auth::user()->account->uid;

		$f = Folder::find($folder_id);
		$isRoot = false;
		if($f){
			$isRoot = empty($f->parent_id) ? true : false;
		}

		$ret['experiments'] = [];
		switch ($type) {
			case 'compact-list':
				break;
			case 'folder':
				$ret['experiments'] = Experiment::experiments($folder_id, $isRoot, $account_cid);
				break;
			default:
				$folder = new Folder();
				$ret['experiments'] = $folder->experimentsFolders($account_cid);
				break;
		}

		return response()->json( $ret['experiments'] ) ;
	}

	/*
	* Action is a experiment from id
	*
	* @return Json
	*/
	public function getAccountCID(Request $request){
			$account_cid = Auth::user()->account->uid;
			return response()->json($account_cid);
	}
    public function getExperiment(Request $request, $id){
        $account_id = Auth::user()->active_account;
        return response()->json(Experiment::where('account_id', '=', $account_id)
            ->where('experimentID', '=', (int)$id)
            ->get());
    }
		public function getallExp(Request $request){
				$account_id = Auth::user()->active_account;
				$account_cid = Auth::user()->account->uid;
				return response()->json(Experiment::where('deleted', '<>', 1)
				->where('cid','=',$account_cid)->get());
		}

	public function show(FORMS\ShowRequest $request)
	{
		$ret = [];

		$srcTypeId = explode("__", $request->input('_id'));

		$ret['experiment'] = Experiment::find($srcTypeId[1]);
		$ret['pageElement'] = PageElement::where('Experiment.experimentID', '=', $ret['experiment']->experimentID)->first();
		return response()->json( $ret) ;
	}

    /*
    * Action is to get all experiments regardless of folders
    *
    * @return Json
    */

    public function getAllExperiments(Request $request){
        $account_id = Auth::user()->active_account;
        return response()->json(Experiment::where('account_id', '=', $account_id)
            ->where('deleted', '<>', 1)
            ->where('hidden', '<>', 1)
            ->get());
    }

    /*
    * Action is a to get pages form experiment
    *
    * @return Json
    */
		public function getPages(Request $request)
		{
			$ret = [];
				$srcTypeId = $request->input('_id', '');
				// echo $srcTypeId;

				$ret['experiment'] = Experiment::find($srcTypeId);
				if($ret['experiment']){

					// code commented because there is no use of that while using the below query

					// if (sizeof($ret['experiment']->PageElements) > 0) {
					// 	for ($i=0; $i < sizeof($ret['experiment']->PageElements); $i++) {
					// 		$obj = (Object) $ret['experiment']->PageElements[$i];
					// 		$ret['pageElements'][$i] = PageElement::where('_id', '=', $obj->pageelementID)->where('deleted', '<>', 1)->get();
					// 	}
					// 	// $ret['pageElements'] = PageElement::where('_id', '=', $ret['experiment']->pageElementId)->where('deleted', '<>', 1)->get();
					// }
					// else {
					$ret['pageElements'] = [];
					$PElements = PageElement::where('deleted', '<>', 1)->where('Experiment.experimentID',$ret['experiment']->experimentID)->get() ;
					$count = 0;
					foreach ($PElements as $pages) {
						if ($pages->experiments) {
							foreach ($pages->experiments as $found_exp) {
								if ($found_exp->experimentID == $ret['experiment']->experimentID) {
									$ret['pageElements'][$count] = PageElement::where('_id', '=', $pages->_id)->get();
									$count = $count + 1;
								}
							}
						}
					}
					// }
				}
				else {
					$ret['pageElements'] = [];
				}
				return response()->json( $ret['pageElements']) ;
		}


	/*
	* Action is creating a new experiment
	*
	* @return Json
	*/
    public function create(FORMS\AddForm $request){
		$ret = ['success' => true , 'msg' => 'Experiment created!'] ;

		$src = $request->input('source', false);
		if($src && strlen($src) > 0){
			$folder_id = explode("__", $request->input('source'));
		} else {
			$folder_id = null;
		}
		$account_id = Auth::user()->active_account;
		$active_account = Auth::user()->account;

		$segment = $request->input('segmentExperiment', false);
		$goal = $request->input('goalExperiment', false);

		$experiment = new Experiment();
		$experiment->name = $request->input('nameExperiment');
		$experiment->description = $request->input('descExperiment');

		if($segment){
			$segment_id_mongo = new \MongoId($segment);
			$experiment->segment_id = $segment_id_mongo;
		}
		if($goal){
			$goal_id_mongo = new \MongoId($goal);
			$experiment->goal_id = $goal_id_mongo;
		}
		$experiment->cg_size = (float) $request->input('cgSizeExperiment');
		$experiment->cg_day = (float) $request->input('cgDayExperiment');
		$experiment->significant_action = $request->input('significantActionExperiment');
		$experiment->account_id = $account_id;
		$experiment->deleted = 0;
		$experiment->hidden = 1;

		if(is_array($folder_id)){
			$experiment->folder_id = new \MongoId($folder_id[1]);
		} else {
			$experiment->folder_id = new \MongoId(Folder::rootFolders($account_id)->first()->_id);
		}

		$experiment->experimentID = Experiment::max('experimentID') + 1;
		$experiment->cid = Auth::user()->account->uid;

		$experiment->save() ;
        $experimentId = new \MongoId($experiment->_id);
        \Event::fire(new Logger(Auth::user()->account->name, " created experiment", $experiment->name , "App\Experiment",$experimentId));
        $this->setHistoryLogs($request,$goal,$segment,$experiment);




		// page element


		$pageElementIds = $request->input('page_element_ids', '');

		if(strlen($pageElementIds) > 0){
			$breakPageElementIds = explode(',', $pageElementIds);
			if(count($breakPageElementIds) > 0){
				for ($i = 0; $i < count($breakPageElementIds); $i++) {
					$pageElementId = trim($breakPageElementIds[$i]);
					$pageElement = PageElement::find($pageElementId);
					if($pageElement->experiment){
						$pageElementExperiment = $pageElement->experiment;
					} else {
						$pageElementExperiment = new PageElementExperiment();
					}
					$pageElementExperiment->controlGroupSize = $experiment->cg_size;
					$pageElementExperiment->controlGroupDays = $experiment->cg_day;
					$pageElementExperiment->experimentID = $experiment->experimentID;
					if($goal){
						$pageElementExperiment->goalID = $experiment->goal->uid;
					}
					if($segment){
						$pageElementExperiment->segmentID = $experiment->segment->uid;
					}

					$pageElement->experiments()->save($pageElementExperiment);
                    \Event::fire(new Logger(Auth::user()->account->name, " created page_element experiment", $experiment->name , "App\Experiment",$experimentId));
				}
			}
		}


		$newPageElement = (int) $request->input('newPageElement', 0);
		$pageElement = '';
		if($newPageElement === 1){
			$fullURL = $request->input('fullURLWebPageList');
			$regExp = $request->input('regularExpressionWebPageElement', '');
			$inWWW = (int)$request->input('includeWWWWebPageElement', 0);
			$inHTTP_HTTPS = (int) $request->input('includeHttpHttpsWebPageElement', 0);
			$urlOption = $request->input('urlOptionWebPageElement', '');
			$additionalJS = $request->input('tAdditionalJSWebPageElement', '');
			$remark = $request->input('remarkWebPageList', '');

			$urlConfig = new UrlConfig();
			$urlConfig->reg_ex = $regExp;
			$urlConfig->url = $fullURL;
			$urlConfig->url_option = $urlOption;
			$urlConfig->inc_www = $inWWW;
			$urlConfig->inc_http_https = $inHTTP_HTTPS;
			$urlConfig->urlHash = $urlConfig->getUrlHashAttribute();
			$urlConfig->cleanUrl = $urlConfig->getCleanUrlAttribute();

			$urlConfig->setUrlOptionFromString($urlConfig->url_option);

			$pageElement = new PageElement();

			$pageElement->cid = $active_account->uid;
			$pageElement->hidden = 1;
			$pageElement->deleted = 0;
			$pageElement->pageID = 0;

			$pageElement->setUrlConfig($urlConfig);
	    $pageElement->originalUrl = $fullURL;

	    $pageElement->additionalJS = $additionalJS;
	    $pageElement->remark = $remark;

	    $pageElement->save();
            \Event::fire(new Logger(Auth::user()->account->name, " created page_element", $experiment->name , "App\Experiment",$experimentId));
            $this->setHistoryLogs($request,$goal,$segment,$experiment);
			if($pageElement->experiment){
				$pageElementExperiment = $pageElement->experiment;
			} else {
				$pageElementExperiment = new PageElementExperiment();
			}
			$pageElementExperiment->controlGroupSize = $experiment->cg_size;
			$pageElementExperiment->controlGroupDays = $experiment->cg_day;
			$pageElementExperiment->experimentID = $experiment->experimentID;
			if($goal){
				$pageElementExperiment->goalID = $experiment->goal->uid;
			}
			if($segment){
				$pageElementExperiment->segmentID = $experiment->segment->uid;
			}
            \Event::fire(new Logger(Auth::user()->account->name, " created page_element experiment", $experiment->name , "App\Experiment",$experimentId));

			$pageElement->experiments()->save($pageElementExperiment);

		}

		if($goal){
			$this->linkExperimentWithGoal($experiment->goal->uid, $active_account, $experiment);
		}
        if($segment){
            $segment_id_mongo = new \MongoId($segment);
            $segment = Segment::find($segment_id_mongo);
            \Event::fire(new Logger(Auth::user()->account->name, " has added a segment to an experiment ($experiment->name)", $segment->name , "App\Experiment",$experimentId));
        }

		$ret['source'] = 'experiment';
		$ret['content']['experiment'] = $experiment;
		$ret['content']['pageElement'] = $pageElement;
		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a experiment
	*
	* @return Json
	*/
	public function update(FORMS\EditForm $request){
		$ret = ['success' => true , 'msg' => 'Experiment updated!'] ;

		$srcId = $request->input('_id');
		$segment = $request->input('segmentExperiment', false);
		$goal = $request->input('goalExperiment', false);

		$account_id = Auth::user()->active_account;
		$active_account = Auth::user()->account;

		$experiment = Experiment::find($srcId);
		$experiment->name = $request->input('nameExperiment');
		$experiment->description = $request->input('descExperiment');
		if($segment){
			$segment_id_mongo = new \MongoId($segment);
			$experiment->segment_id = $segment_id_mongo;
		}
		if($goal){
			$goal_id_mongo = new \MongoId($goal);
			$experiment->goal_id = $goal_id_mongo;
		}
		$experiment->cg_size = (float) $request->input('cgSizeExperiment');
		$experiment->cg_day = (float) $request->input('cgDayExperiment');
		$experiment->significant_action = (int) $request->input('significantActionExperiment');
		$experiment->save() ;
        $experimentId = new \MongoId($experiment->_id);
        \Event::fire(new Logger(Auth::user()->account->name, " updated an experiment", $experiment->name , "App\Experiment", $experimentId));
        $this->setHistoryLogs($request,$goal,$segment,$experiment);

        // page element

		$pageElementIds = $request->input('page_element_ids', '');

		if(strlen($pageElementIds) > 0){
			$breakPageElementIds = explode(',', $pageElementIds);
			if(count($breakPageElementIds) > 0){
				for ($i = 0; $i < count($breakPageElementIds); $i++) {
					$pageElementId = trim($breakPageElementIds[$i]);
					$pageElement = PageElement::find($pageElementId);
					if($pageElement->experiment){
						$pageElementExperiment = $pageElement->experiment;
					} else {
						$pageElementExperiment = new PageElementExperiment();
					}
					$pageElementExperiment->controlGroupSize = $experiment->cg_size;
					$pageElementExperiment->controlGroupDays = $experiment->cg_day;
					$pageElementExperiment->experimentID = $experiment->experimentID;
					if($goal){
						$pageElementExperiment->goalID = $experiment->goal->uid;
					}
					if($segment){
						$pageElementExperiment->segmentID = $experiment->segment->uid;
					}

					$pageElement->experiments()->save($pageElementExperiment);
                    \Event::fire(new Logger(Auth::user()->account->name, " updated page elements experiment", $experiment->name , "App\Experiment",$experimentId));
                    $this->setHistoryLogs($request,$goal,$segment,$experiment);
                }
			}
		}

		if($goal){
			$this->linkExperimentWithGoal($experiment->goal->uid, $active_account, $experiment);
		}
        if($segment){
            $segment_id_mongo = new \MongoId($segment);
            $segment = Segment::find($segment_id_mongo);
            \Event::fire(new Logger(Auth::user()->account->name, " has updated a segment to an experiment ($experiment->name)", $segment->name , "App\Experiment",$experimentId));
        }
		$ret['source'] = 'experiment';
		$ret['content']['experiment'] = $experiment;

		return response()->json( $ret ) ;
	}

	private function linkExperimentWithGoal($goldUid, $account, $experiment){
		$pageElements = PageElement::where('cid',(int)$account->uid)->where('deleted', '=', 0)->whereNotNull('originalUrl')->get();
        if($goldUid){
            $goal = Goal::where('uid','=', $goldUid)->first();
        }
        if(count($pageElements) > 0){
			foreach ($pageElements as $pageElement) {
				$contents = $pageElement->content->where('uid','=', $goldUid)->get();
				if(count($contents) > 0){
					foreach ($contents as $content) {
						if(!isset($content->experiments)){
							$content->experiments = [];
								$content->experiments = array_merge($content->experiments,[$experiment->experimentID]);
						} else {
							if(!in_array($experiment->experimentID, $content->experiments)){
								// var_dump($content->experiments,$experimentID);die;
								// $content->experiments = (array) $content->experiments;
								$content->experiments = array_merge($content->experiments,[$experiment->experimentID]);
								// $content->experiments[] = $experimentID;
							}
						}
						// var_dump($content->experiments);die;
						$pageElement->content()->save($content);


                    }
				}
			}
		}

            if($goal){
                $experimentId = new \MongoId($experiment->_id);
                \Event::fire(new Logger(Auth::user()->account->name, " has added a goal to an experiment ($experiment->name)", $goal->name , "App\Experiment",$experimentId));
            }

	}

	/*
	* Toggle hidden value active/inactive
	*
	* @return Json
	*/
	public function updateStatus(FORMS\StatusUpdateForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Experiment activated!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$experiment = Experiment::find($srcId);
			$experiment->hidden = (int) $request->input('hidden');
			$experiment->save();
            $experimentId = new \MongoId($experiment->_id);

			if($experiment->hidden == "1"){
				$ret['msg'] = 'Experiment deactivated';
                \Event::fire(new Logger(Auth::user()->account->name, " has deactivated experiment ", $experiment->name , "App\Experiment",$experimentId));
                $this->setHistoryLogs($request,0,0,$experiment);

        }else{
            \Event::fire(new Logger(Auth::user()->account->name, " has activated experiment ", $experiment->name , "App\Experiment",$experimentId));
            $this->setHistoryLogs($request,0,0,$experiment);
        }


			// update related goal
			if(!empty($experiment->goal_id)){
				$_goal = Goal::find($experiment->goal_id);
				$_goal->hidden = $experiment->hidden;
				$_goal->save();
			}

			// update related segment
			if(!empty($experiment->segment_id)){
				$_segment = Segment::find($experiment->segment_id);
				$_segment->hidden = $experiment->hidden;
				$_segment->save();
			}

			$page = PageElement::where('Experiment.experimentID', $experiment->experimentID)->first();
			if(!empty($page)){
				$page->hidden = $experiment->hidden;
				$page->deleted = $experiment->deleted;
				$page->save();
			}

			$ret['source'] = 'experiment';
			$ret['content']['experiment'] = $experiment;


			return response()->json($ret);
	}

	/*
	* change folder of experiment
	*
	* @return Json
	*/
	public function changeFolder(FORMS\ChangeFolderForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Experiment is moved to new folder.'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$target = explode('__', $request->input('target'));

			if($target){
				try{
					$experiment = Experiment::find($srcId);
					$experiment->folder_id = new \MongoId($target[1]);
					$experiment->save();

					$ret['source'] = 'experiment';
					$ret['content']['experiment'] = $experiment;
				}
				catch(\Exception $e){
					// ToDo: Report error on invalid MongoId
					$ret['success'] = false;
					$ret['msg'] = $e->getMessage();
				}
			}

			return response()->json($ret);
	}

	/*
	* Action is deleting a goal
	*
	* @return Json
	*/
	public function delete(FORMS\DeleteForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Experiment deleted!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$experiment = Experiment::find($srcId);
			$experiment->deleted = 1;
			$experiment->save();
      $experimentId = new \MongoId($experiment->_id);
      \Event::fire(new Logger(Auth::user()->account->name, " has delete the experiment ", $experiment->name , "App\Experiment",$experimentId));
       $this->setHistoryLogs($request,0,0,$experiment);

			$page = PageElement::where('Experiment._id', new \MongoId($experiment->_id));
			if(!empty($page)){
				$page->hidden = $experiment->hidden;
				$page->deleted = $experiment->deleted;
				$page->save();
			}

      $ret['source'] = 'experiment';
			$ret['content']['experiment'] = $experiment;


			return response()->json($ret);
	}


    public function setHistoryLogs($request,$goal=0,$segment=0,$experiment)
    {
        $exp=new ExperimentsController();
        $response=$exp->getExperiment($request,$experiment->experimentID);
        $content=json_decode($response->getContent());
        $today = Carbon::today()->timestamp;
        $data= History::where('timestamp',">=", $today - (24*60*60))->get();
        if(sizeof($data)>0){
            for($i=0;$i<sizeof($data);$i++){
                if($goal && $segment){
                    \Event::fire(new ExperimentHistoryLog($experiment->experimentID,1,1,$content->variations,$content->locations,1,$data[$i]->timestamp));
                }else if($segment){
                    \Event::fire(new ExperimentHistoryLog($experiment->experimentID,0,1,$content->variations,$content->locations,1,$data[$i]->timestamp));
                }
                else if($goal){
                    \Event::fire(new ExperimentHistoryLog($experiment->experimentID,1,0,$content->variations,$content->locations,1,$data[$i]->timestamp));
                }else {
                    \Event::fire(new ExperimentHistoryLog($experiment->experimentID,0,0,$content->variations,$content->locations,1,$data[$i]->timestamp));
                }
            }
        } else{

            if($goal && $segment){
                \Event::fire(new ExperimentHistoryLog($experiment->experimentID,1,1,$content->variations,$content->locations));
            }else if($segment){
                \Event::fire(new ExperimentHistoryLog($experiment->experimentID,0,1,$content->variations,$content->locations));
            }
            else if($goal){
                \Event::fire(new ExperimentHistoryLog($experiment->experimentID,1,0,$content->variations,$content->locations));
            }else {
                \Event::fire(new ExperimentHistoryLog($experiment->experimentID,0,0,$content->variations,$content->locations));
            }
        }

    }
}
