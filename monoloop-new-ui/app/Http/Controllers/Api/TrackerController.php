<?php namespace App\Http\Controllers\Api;

use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tracker as FORMS;
use App\Tracker;
use App\Folder;

use Input;

class TrackerController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

 	/*
 	 * Action is finding all Trackers and associated folders of logged in user
 	 *
 	 * @return Json
 	 */
	public function index(Request $request, $type = "tree")
	{
		$ret = [];
		$ret['trackers'] = [];

		$account_id = Auth::user()->active_account;

		$folder_id = Input::get('source', NULL);

		if(!empty($folder_id)){
			$folder = explode('__', $folder_id);
			if(is_array($folder) && count($folder) === 2){
				$folder_id = $folder[1];
			}
		}

		$f = Folder::find($folder_id);
		$isRoot = false;
		if($f){
			$isRoot = empty($f->parent_id) ? true : false;
		}

		switch ($type) {
			case 'compact-list':
				$ret['trackers'] = Tracker::CompactList($account_id);
				break;
			case 'folder':
				$ret['trackers'] = Tracker::trackers($folder_id, $isRoot, $account_id);
				break;
			default:
				$folder = new Folder();
				$ret['trackers'] = $folder->trackersFolders($account_id);
				break;
		}
		return response()->json( $ret['trackers'] ) ;
	}

	/*
	* Action is a Tracker from id
	*
	* @return Json
	*/
	public function show()
	{
		$ret = [];
		$srcTypeId = explode("__", Input::get('_id'));

		$ret['tracker'] = Tracker::find($srcTypeId[1]);
		return response()->json( $ret['tracker'] ) ;
	}

	/*
	* Action is creating a new Tracker
	*
	* @return Json
	*/
	public function create(Request $request){
		$ret = ['success' => true , 'msg' => 'Tracker created!'] ;

		$srcTypeId = explode("__", $request->input('source'));

		$account_id = Auth::user()->active_account;

		$tracker = new Tracker();
		$tracker->name = $request->input('name');
		$tracker->description = $request->input('desc');
		$tracker->account_id = $account_id;
		$tracker->deleted = 0;
		$tracker->hidden = 0;
		$tracker->folder_id = new \MongoId($srcTypeId[1]);
		$tracker->save() ;

		$ret['source'] = 'tracker';
		$ret['content']['id'] = $tracker->id;
		$ret['content']['condition'] = $tracker->condition;

		return response()->json( $ret ) ;
	}

	/*
	* Action is updating a Tracker
	*
	* @return Json
	*/
	public function update(Request $request){
		$ret = ['success' => true , 'msg' => 'Tracker updated!'] ;

		if(strpos($request->input('source'), '__') !== false){
			$srcTypeId = explode("__", $request->input('source'));
			$srcId = $srcTypeId[1];
		} else {
			$srcId = $request->input('source');
		}


		$tracker = Tracker::find($srcId);
		$tracker->name = !empty($request->input('name')) ? $request->input('name') : $tracker->name;
		$tracker->description = !empty($request->input('desc')) ? $request->input('desc') : $tracker->description;
		$tracker->condition = !empty($request->input('condition')) ? $request->input('condition') : $tracker->condition;
		$tracker->condition_json = !empty($request->input('condition_json')) ? $request->input('condition_json') : $tracker->condition_json;
		$tracker->save() ;

		$ret['tracker'] = $tracker;
		return response()->json( $ret ) ;
	}

	/*
	* Toggle hidden value active/inactive
	*
	* @return Json
	*/
	public function updateStatus(FORMS\StatusUpdateForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Tracker activated!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$tracker = Tracker::find($srcId);
			$tracker->hidden = $request->input('hidden');
			$tracker->save();

			if($tracker->hidden == "1"){
				$ret['msg'] = 'Tracker deactivated';
			}

			$ret['source'] = 'tracker';
			$ret['content']['tracker'] = $tracker;
			return response()->json($ret);
	}

	/*
	* change folder of Tracker
	*
	* @return Json
	*/
	public function changeFolder(FORMS\ChangeFolderForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Tracker is moved to new folder.'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$target = explode('__', $request->input('target'));

			$tracker = Tracker::find($srcId);
			$tracker->folder_id = new \MongoId($target[1]);
			$tracker->save();

			$ret['source'] = 'tracker';
			$ret['content']['tracker'] = $tracker;
			return response()->json($ret);
	}

	/*
	* Action is deleting a Tracker
	*
	* @return Json
	*/
	public function delete(FORMS\DeleteForm $request)
	{
			$ret = ['success' => true , 'msg' => 'Tracker deleted!'];

			if (strpos($request->input('_id'), '__') !== false) {
					$srcTypeId = explode('__', $request->input('_id'));
					$srcId = $srcTypeId[1];
			} else {
					$srcId = $request->input('_id');
			}

			$tracker = Tracker::find($srcId);
			$tracker->deleted = 1;
			$tracker->save();

			$ret['source'] = 'tracker';
			$ret['content']['tracker'] = $tracker;
			return response()->json($ret);
	}
}
