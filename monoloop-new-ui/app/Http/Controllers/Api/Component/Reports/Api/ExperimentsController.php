<?php

namespace App\Http\Controllers\Api\Component\Reports\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Experiment;
use App\ReportingArchiveAggregated as Reports;
use App\Account as Account;
use App\PageElement;
use App\Content;
use App\ExperimentHistory;

use Carbon\Carbon;

use Input;

class ExperimentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function no_of_daily_visitors($reports, $experimentID){

    }

    private function noOfEstDays($existingCR, $expectedImprovement, $chance, $no_of_variations, $avg_no_of_daily_visitors){


      $existingCR = (float) number_format($existingCR,2);
      $expectedImprovement = (float)  number_format($expectedImprovement,2);
      $avg_no_of_daily_visitors = (int) round($avg_no_of_daily_visitors);

      if($existingCR != 0 && $expectedImprovement != 0){
        $total_visitors = $no_of_variations * ($chance * (pow(sqrt($existingCR * ((1- $existingCR) < 0 ? (-1* (1- $existingCR)) : (1- $existingCR)))/($existingCR * $expectedImprovement), 2)));
      } else {
        $total_visitors = $no_of_variations * ($chance * (pow(sqrt($existingCR * ((1- $existingCR) < 0 ? (-1* (1- $existingCR)) : (1- $existingCR))), 2)));
      }
      if($avg_no_of_daily_visitors != 0){
        $estdays = (int) round($total_visitors / $avg_no_of_daily_visitors);
      } else {
        $estdays = (int) round($total_visitors);
      }


      return $estdays;

    }

    private function getLastExUpdatedTimestamp($experimentID){
      $timestamp = false;

      $timestamp = ExperimentHistory::where('experimentID', '=', (int) $experimentID)->max('timestamp');

      if(is_null($timestamp)){
         $experiment = Experiment::where('experimentID',(int)$experimentID)->first() ;
         $date = new Carbon($experiment->updated_at);
         $timestamp  = $date->timestamp ;
      }

      return $timestamp;
    }

    public function getExperiment(Request $request, $id){
        $account_id = Auth::user()->active_account;

        $response = [];



        $response['experiment'] = Experiment::where('account_id', '=', $account_id)
            ->where('experimentID', '=', (int)$id)
            ->first();

        if(!$response['experiment']){
          $response['message'] = "No experiment found";
          return response()->json($response);
        }
        $timestamp = $this->getLastExUpdatedTimestamp($id);

        $response['from_timestamp'] = date('Y-m-d',$timestamp);
        $response['to_timestamp'] = date('Y-m-d');

        if($timestamp){
          $reports = Reports::where('datestamp','>=',$timestamp)
                            ->whereNotNull('experiments.'.$id)
                            ->get();
        } else {
          $reports = Reports::whereNotNull('experiments.'.$id)
                            ->get();
        }


        $response['pages'] = 0;
        $response['variations'] = 0;
        $response['locations'] = 0;

        $pages = PageElement::where('Experiment.experimentID','=',(int)$id)->where('deleted','=', 0)->get();

        $added = [];
        foreach ($pages as $page) {
          $response['pages']++;
          $contents = $page->content;
          foreach ($contents as $c) {

            $content = Content::where('uid','=',$c['uid'])->first();
            if($content->deleted === 0){
              $response['variations']++;
              if(!in_array($c['mappedTo'], $added)){
                $response['locations']++;
              }
              $added[] = $c['mappedTo'];
            }
          }
          $response['is_exp_running'] = 0;
          $parse = parse_url($page->originalUrl);
          $parse['host'] = str_replace('www.','',$parse['host']);
          $domain = Auth::user()->account->domains->where('domain',$parse['host'])->first() ;
          if($domain){
            if(isset($domain->status)){
              if($domain->status == 2){
                $response['is_exp_running'] = 1;
              }
            }
          } else {

          }
        }

        $response['cr'] = 0;
        $response['cr_cg'] = 0;
        $response['improvement'] = 0;
        $response['p_value'] = 0;
        $response['visitors'] = 0;
        $response['cg_visitors'] = 0;
        $response['direct'] = 0;
        $response['cg_direct'] = 0;
        $records = 0;
        if(count($reports) > 0){
          foreach ($reports as $report) {
            $experiments = isset($report['experiments']) ? $report['experiments'] : [];
            if(count($experiments) > 0){



              $experiment = isset($experiments[$id]) ? $experiments[$id] : null;
              if($experiment){


                $visitors = !empty($experiment['visitors']) ? $experiment['visitors'] : 0;
                $cg_visitors = !empty($experiment['cg_visitors']) ? $experiment['cg_visitors'] : 0;

                $direct = !empty($experiment['direct']) ? $experiment['direct'] : 0;
                $cg_direct = !empty($experiment['cg_direct']) ? $experiment['cg_direct'] : 0;

                if($visitors > 0){
                  $response['visitors'] += $visitors;
                }
                if($cg_visitors > 0){
                  $response['cg_visitors'] += $cg_visitors;
                }

                if($direct > 0){
                  $response['direct'] += $direct;
                }
                if($cg_direct > 0){
                  $response['cg_direct'] += $cg_direct;
                }



                $p_value = $this->functionP($experiment['visitors'], $experiment['direct'], $experiment['cg_visitors'], $experiment['cg_direct']);
                $response['p_value'] += $p_value;
                $records++;
              }
            }

          }
        }

        if($response['visitors'] != 0){
          $response['cr'] = ($response['direct'] / $response['visitors']) * 100;
        } else {
          $response['cr'] = ($response['direct'] / 1) * 100;
        }

        if($response['cg_visitors'] != 0){
          $response['cr_cg'] = ($response['cg_direct'] / $response['cg_visitors']) * 100;
        } else {
          $response['cr_cg'] = ($response['cg_direct'] / 1) * 100;
        }

        if($response['cr_cg'] != 0){
          $response['improvement'] = ($response['cr'] - $response['cr_cg'])/$response['cr_cg'] * 100;
        } else {
          $response['improvement'] = ($response['cr'] - 0)/1 * 100;;
        }

        $controlCr = $response['cr_cg']/100;
        $expCr = $response['cr']/100;
        if($controlCr == 0){
          $percent_change = ($expCr - $controlCr);
        } else {
          $percent_change = ($expCr - $controlCr)/$controlCr;
        }

        // var_dump($controlCr, $expCr, $percent_change);die;
        $avg_no_of_daily_visitors = $response['visitors'];
        if($records != 0){
          $avg_no_of_daily_visitors = $response['visitors'] / $records;
        }

        $response['estimatedDays80PercentChance'] = $this->noOfEstDays($controlCr, $percent_change,16, $response['variations'], $avg_no_of_daily_visitors);
        $response['estimatedDays95PercentChance'] = $this->noOfEstDays($controlCr, $percent_change, 26, $response['variations'], $avg_no_of_daily_visitors);
        // var_dump($estimatedDays80PercentChance, $estimatedDays95PercentChance);die;

        return response()->json($response);
    }

    /*
    * Action is to get all experiments regardless of folders
    *
    * @return Json
    */

    public function getAllExperiments(Request $request){
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        return response()->json(Experiment::where('account_id', '=', $account_id)
            ->where('deleted', '<>', 1)
            ->where('hidden', '<>', 1)
            ->get());
    }

    public function getExperimentOverview(){
        $experimentID = Input::get('eID');
        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        // var_dump($from_date,$to_date, '<br />');
        if(empty($from_date) && empty($to_date)){
          $timestamp = $this->getLastExUpdatedTimestamp($experimentID);
          $result = Reports::where('datestamp','>=',$timestamp)
                            ->where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->get();
        } else {
          $from_date = strtotime($from_date);
          $to_date = strtotime($to_date);
          // echo date('m/d/Y', $from_date); echo '<br />';
          $result = Reports::where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->where('datestamp','>=',$from_date)
                            ->where('datestamp','<=',$to_date)
                            ->get();
        }


        // $datestamp = $result->lists('datestamp');
        $experiments = $result->lists('experiments');
        $points = array();

        $points['experiment']['cr']['name'] = 'Conversation rate';
        $points['experiment']['cr']['value'] = 0;
        $points['experiment']['visitors']['name'] = 'Visitors';
        $points['experiment']['visitors']['value'] = 0;
        $points['experiment']['conversion']['name'] = 'Conversions';
        $points['experiment']['conversion']['value'] = 0;
        $points['experiment']['improvement']['name'] = 'Improvement';
        $points['experiment']['improvement']['value'] = 0;
        $points['experiment']['p_value']['name'] = 'P-Value';
        $points['experiment']['p_value']['value'] = 0;


        $points['cg']['cr']['name'] = 'Conversation rate';
        $points['cg']['cr']['value'] = 0;
        $points['cg']['visitors']['name'] = 'Visitors';
        $points['cg']['visitors']['value'] = 0;
        $points['cg']['conversion']['name'] = 'Conversions';
        $points['cg']['conversion']['value'] = 0;
        $points['cg']['improvement']['name'] = 'Improvement';
        $points['cg']['improvement']['value'] = '-';
        $points['cg']['p_value']['name'] = 'P-Value';
        $points['cg']['p_value']['value'] = '-';

        foreach($experiments as $key => $exp){
            if(is_array($exp)){
                foreach($exp as $key2 => $controlExp){
                        // var_dump($controlExp);die;
                    // if($key2 == $experimentID) {
                      // if (array_key_exists('cg_direct', $controlExp) && array_key_exists('cg_visitors', $controlExp)) {

                        if(!isset($controlExp['cg_visitors'])) $controlExp['cg_visitors'] = 1;
                        if(!isset($controlExp['cg_direct'])) $controlExp['cg_direct'] = 0;
                        if(!isset($controlExp['visitors'])) $controlExp['visitors'] = 1;
                        if(!isset($controlExp['direct'])) $controlExp['direct'] = 0;

                        $points['experiment']['visitors']['value'] += $controlExp['visitors'];
                        $points['experiment']['conversion']['value'] += $controlExp['direct'];


                        $p_value = $this->functionP($controlExp['visitors'], $controlExp['direct'], $controlExp['cg_visitors'], $controlExp['cg_direct']);
                        $points['experiment']['p_value']['value'] += $p_value;


                        $points['cg']['visitors']['value'] += $controlExp['cg_visitors'];
                        $points['cg']['conversion']['value'] += $controlExp['cg_direct'];

                        // if($cr_percent2 > 0){
                        //   $improvement = ($cr_percent - $cr_percent2)/$cr_percent2 * 100;
                        // } else {
                        //   $improvement = 0;
                        // }
                        // $points['cg']['improvement']['value'] += $improvement;
                        // $p_value = $this->functionP($controlExp['visitors'], $controlExp['direct'], $controlExp['cg_visitors'], $controlExp['cg_direct']);
                        // $points['cg']['p_value']['value'] += $p_value;


                      // }
                  //  }
                }
            }
        }

        if($points['experiment']['visitors']['value'] != 0){
          $points['experiment']['cr']['value'] = ($points['experiment']['conversion']['value'] / $points['experiment']['visitors']['value']) * 100;
        } else {
          $points['experiment']['cr']['value'] = ($points['experiment']['conversion']['value'] / 1) * 100;
        }
        if($points['cg']['visitors']['value'] != 0){
          $points['cg']['cr']['value'] = ($points['cg']['conversion']['value'] / $points['cg']['visitors']['value']) * 100;
        } else {
          $points['cg']['cr']['value'] = ($points['cg']['conversion']['value'] / 1) * 100;
        }


        if($points['cg']['cr']['value'] != 0){
          $improvement = ($points['experiment']['cr']['value'] - $points['cg']['cr']['value'])/$points['cg']['cr']['value'] * 100;
        } else {
          $improvement = 0;
        }
        $points['experiment']['improvement']['value'] = $improvement;

        echo json_encode($points);

    }
    private function NormalP($x){
    	$d1=0.0498673470;
  		$d2=0.0211410061;
  		$d3=0.0032776263;
  		$d4=0.0000380036;
  		$d5=0.0000488906;
  		$d6=0.0000053830;
  		$a=abs($x);
  		$t=1.0+$a*($d1+$a*($d2+$a*($d3+$a*($d4+$a*($d5+$a*$d6)))));
  		$t*=$t;$t*=$t;$t*=$t;$t*=$t;$t=1.0/($t+$t);
  		if($x>=0)
  			$t=1-$t;
  		return $t;
    }

    private function functionP($v_t,$v_c,$c_t,$c_c){
      if( $c_t == 0 || $v_t == 0 )
        return 0;
    	$c_p= $c_c/$c_t;
  		$v_p= $v_c/$v_t;
  		$std_error= sqrt(($c_p*(1-$c_p)/$c_t)+($v_p*(1-$v_p)/$v_t));
  		if($std_error == 0)
  			return 0 ;
  		$z_value=($v_p-$c_p)/$std_error;
  		#echo $z_value ; die() ;
  		$p_value= $this->NormalP($z_value);
  		#echo $p_value ; die() ;
  		if($p_value>0.5)
  			$p_value=1-$p_value;

  		$p_value = number_format($p_value,3) ;
  		if($p_value == 0)
  			return 0 ;
    	return $p_value ;
    }
    public function getControlGroups(Request $request, $id){
        $experimentID = $id;
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');

        $timestamp = $this->getLastExUpdatedTimestamp($experimentID);

        $td = Reports::where('datestamp','>=',$timestamp)
                      ->where('cid', $cid[0])
                      ->whereNotNull('experiments.'.$experimentID)
                      ->lists('experiments');

        $controlVisitors = 0;
        $visitors = 0;
        $controlGroup = array();
        foreach($td as $key => $exp){
            if(is_array($exp)){
                foreach($exp as $key2 => $controlExp){
                  //  if($key2 == $experimentID){
                        if (array_key_exists('visitors', $controlExp)) $visitors += $controlExp['visitors'];
                        if (array_key_exists('cg_visitors', $controlExp)) $controlVisitors += $controlExp['cg_visitors'];
                  //  }

                }

            }

        }
        if(($controlVisitors + $visitors) != 0){
          $cg = $controlVisitors / ($controlVisitors + $visitors) * 100;
        } else {
          $cg = $controlVisitors / 1 * 100;
        }
        if(($controlVisitors + $visitors) != 0){
          $exp = $visitors / ($controlVisitors + $visitors) * 100;
        } else {
          $exp = $visitors / 1 * 100;
        }

        $visitors_arr = array(['label'=> 'Control Group ', 'value'=> $cg, 'visitors' => $controlVisitors], ['label'=> 'Experiment', 'value'=> $exp, 'visitors' => $visitors]);
        return response()->json($visitors_arr);

    }

    public function getCombinedCR(){
        $experimentID = Input::get('eID');
        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        // var_dump($from_date,$to_date, '<br />');
        if(empty($from_date) && empty($to_date)){
          $timestamp = $this->getLastExUpdatedTimestamp($experimentID);
          $result = Reports::where('datestamp','>=',$timestamp)
                            ->where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->get();
        } else {
          $from_date = strtotime($from_date);
          $to_date = strtotime($to_date);
          // echo date('m/d/Y', $from_date); echo '<br />';
          $result = Reports::where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->where('datestamp','>=',$from_date)
                            ->where('datestamp','<=',$to_date)
                            ->get();
        }


        // $datestamp = $result->lists('datestamp');
        $experiments = $result->lists('experiments');
        $points = array();


        $points['experiment']['direct']['name'] = 'Conversions (Experiment)';
        $points['experiment']['direct']['value'] = 0;
        $points['experiment']['visitors']['name'] = 'Visitors (Experiment)';
        $points['experiment']['visitors']['value'] = 0;
        $points['experiment']['cr']['name'] = 'Experiment';
        $points['experiment']['cr']['value'] = 0;


        $points['cg']['cr']['name'] = 'Control group';
        $points['cg']['cr']['value'] = 0;
        $points['cg']['visitors']['name'] = 'Visitors (Control group)';
        $points['cg']['visitors']['value'] = 0;
        $points['cg']['direct']['name'] = 'Conversions (Control group)';
        $points['cg']['direct']['value'] = 0;


        foreach($experiments as $key => $exp){
            if(is_array($exp)){
                foreach($exp as $key2 => $controlExp){
                        // var_dump($controlExp);die;
                    // if($key2 == $experimentID) {
                      if (array_key_exists('cg_direct', $controlExp) && array_key_exists('cg_visitors', $controlExp)) {

                        $points['cg']['visitors']['value'] += $controlExp['cg_visitors'];
                        $points['cg']['direct']['value'] += $controlExp['cg_direct'];
                      }
                      if (array_key_exists('direct', $controlExp) && array_key_exists('visitors', $controlExp)) {

                        $points['experiment']['visitors']['value'] += $controlExp['visitors'];
                        $points['experiment']['direct']['value'] += $controlExp['direct'];

                      }
                  //  }
                }
            }
        }
        if($points['experiment']['visitors']['value'] != 0){
          $points['experiment']['cr']['value'] = ($points['experiment']['direct']['value'] / $points['experiment']['visitors']['value']) * 100;
        } else {
          $points['experiment']['cr']['value'] = ($points['experiment']['direct']['value'] / 1) * 100;
        }

        if($points['cg']['visitors']['value'] != 0){
          $points['cg']['cr']['value'] = ($points['cg']['direct']['value'] / $points['cg']['visitors']['value']) * 100;
        } else {
          $points['cg']['cr']['value'] = ($points['cg']['direct']['value'] / 1) * 100;
        }

        echo json_encode($points);

    }

    public function getCR(){
        $experimentID = Input::get('eID');
        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        // var_dump($from_date,$to_date, '<br />');
        if(empty($from_date) && empty($to_date)){
          $timestamp = $this->getLastExUpdatedTimestamp($experimentID);
          $result = Reports::where('datestamp','>=',$timestamp)
                            ->where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->get();
        } else {
          $from_date = strtotime($from_date);
          $to_date = strtotime($to_date);
          // echo date('m/d/Y', $from_date); echo '<br />';
          $result = Reports::where('cid', $cid[0])
                            ->whereNotNull('experiments.'.$experimentID)
                            ->where('datestamp','>=',$from_date)
                            ->where('datestamp','<=',$to_date)
                            ->get();
        }


        $datestamp = $result->lists('datestamp');
        $experiments = $result->lists('experiments');
        $points = array();
        foreach($experiments as $key => $exp){
            if(is_array($exp)){
                foreach($exp as $key2 => $controlExp){
                        // var_dump($controlExp);die;
                    // if($key2 == $experimentID) {
                      if (array_key_exists('visitors', $controlExp) && array_key_exists('cg_direct', $controlExp) && array_key_exists('cg_visitors', $controlExp)) {

                          $conversions = ['conversions' =>  (isset($controlExp['cg_direct']) ? $controlExp['cg_direct'] : 0) + (isset($controlExp['cg_direct']) ? $controlExp['cg_direct'] : 0), 'date' => date('m/d/y',$datestamp[$key]), 'type' => 'conversions'];
                          array_push($points, $conversions);

                          $visitors = ['visitors' =>  (isset($controlExp['visitors']) ? $controlExp['visitors'] : 0) + (isset($controlExp['cg_visitors']) ? $controlExp['cg_visitors'] : 0), 'date' => date('m/d/y',$datestamp[$key]), 'type' => 'visitors'];
                          array_push($points, $visitors);
                          $CR = ['cr' => ( (isset($controlExp['cg_direct']) ? $controlExp['cg_direct'] : 0) / (isset($controlExp['cg_visitors'])? $controlExp['cg_visitors'] : 1)) * 100, 'date' => date('m/d/y',$datestamp[$key]), 'type' => 'cr'];
                          array_push($points, $CR);
                      }
                  //  }
                }
            }
        }
        echo json_encode($points);

    }
}
