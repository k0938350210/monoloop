<?php

namespace App\Http\Controllers\Api\Component\Reports\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Experiment;
use App\Segment;
use App\ReportingArchiveAggregated as Reports;
use App\Account as Account;
use App\PageElement;
use App\Content;
use App\ExperimentHistory;

use Input;

class SegmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getSegment(Request $request, $id){
        $account_id = Auth::user()->active_account;

        $response = [];

        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');

        $response['segment'] = Segment::where('account_id', '=', $account_id)
            ->where('uid', '=', (int)$id)
            ->first();

        if(!$response['segment']){
          $response['message'] = "No segment found";
          return response()->json($response);
        }
        if(empty($from_date) && empty($to_date)){
          $from_date = date('Y-m-d', strtotime("-30 days"));
          $to_date = date('Y-m-d');
        } else {
          $from_date = date('Y-m-d', strtotime($from_date));
          $to_date = date('Y-m-d', strtotime($to_date));
        }
        $from_date_unix = strtotime($from_date);
        $to_date_unix = strtotime($to_date);

        $reports = Reports::whereNotNull('segments.'.$id)
                          ->where('datestamp','>=',$from_date_unix)
                          ->where('datestamp','<=',$to_date_unix)
                          ->get();


        $segments = $reports->lists('segments');

        $response['segment_cur_prev_dataset'] = [];
        $node = ['segmentid'=> $response['segment']->uid, 'name'=> $response['segment']->name,'currently'=> 0, 'previously' => 0];

        $fromData = array() ;
        $toData = array() ;

        foreach($segments as $key => $seg){
            if(is_array($seg)){
                foreach($seg as $key2 => $s){
                  if($key2 == $id){
                    $node['currently'] += isset($s['count']) ? $s['count'] : 0;
                    if(isset($s['from'])){
                      if(isset($s['from'][$id])){
                        if(isset($s['from'][$id]['count'])){
                          $node['previously'] += $s['from'][$id]['count'];
                        }
                      }
                    }


                    if(isset($s['from']) && count($s['from'])){
                      foreach($s['from']  as $k => $form){
                        if(! isset($fromData[$k]))
                          $fromData[$k] = 0 ;
                        $fromData[$k] += $form ;
                      }
                    }

                    if( isset($s['from'][$id]) ){
                      if( ! isset($toData[$k]))
                        $toData[$k] = 0  ;
                      $toData[$k] += $s['from'][$id] ;
                    }

                  }
                }
            }
        }

        if($node['previously'] != 0){
          $node['change'] = ($node['currently'] - $node['previously'])/$node['previously'] * 100;
        } else {
          $node['change'] = ($node['currently'] - $node['previously'])/1 * 100;
        }
        $node['change'] = number_format($node['change'],2);
        array_push($response['segment_cur_prev_dataset'], $node);
        $response['data'] = [];
        foreach($fromData as $k => $v){
          $response['data'][$k]['uid'] = $k ;
          $response['data'][$k]['I_1'] = $v ;
        }
        foreach($toData as $k => $v){
          $response['data'][$k]['uid'] = $k ;
          $response['data'][$k]['E_1'] = $v ;
        }
        foreach($response['data'] as $k => &$v){
          $uid = $v['uid'];

          $segment = Segment::where('uid','=', (int)$uid)->first();
          $response['data'][$k]['segment'] = $segment ;
          $response['data'][$k]['name'] = !empty($segment) ? $segment->name: '' ;
          if(!isset($v['E_1']))
              $v['E_1'] = 0 ;
          if(!isset($v['I_1']))
              $v['I_1'] = 0 ;
        }
        $response['selected'] = $id;

        $response['from_timestamp'] = $from_date;
        $response['to_timestamp'] = $to_date;

        return response()->json($response);
    }



}
