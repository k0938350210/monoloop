<?php

namespace App\Http\Controllers\Api\Component\Reports\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Experiment;
use App\ReportingArchiveAggregated as Reports;
use App\Account as Account;
use App\PageElement;
use App\Content;
use App\ExperimentHistory;
use App\Goal;
use Carbon\Carbon;
use Input;

class GoalsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getGoals(Request $request, $id)
    {
        $account_id = Auth::user()->active_account;
        $id = new \MongoId($id);
        $response = [];

        $response['goal'] = Goal::where('account_id', '=', $account_id)
            ->where('_id', '=', $id)
            ->first();

        if (!$response['goal']) {
            $response['message'] = "No experiment found";
            return response()->json($response);
        }
        $cid = Account::where('_id',$account_id)->lists('uid');
        $result = Reports::where('cid', $cid[0])
            ->whereNotNull('goals.'.$response['goal']['uid'])
            ->get();
        $response['goal']['result']=$result;
        return response()->json($response);
    }

    /*
    * Action is to get all experiments regardless of folders
    *
    * @return Json
    */

    public function getAllGoals(Request $request){
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        return response()->json(Goal::where('account_id', '=', $account_id)
            ->where('deleted', '<>', 1)
            ->where('hidden', '<>', 1)
            ->get());
    }

    public function getGoalsOverview(){
        $goalId = Input::get('uid');
        $account_id = Auth::user()->active_account;
        $cid = Account::where('_id',$account_id)->lists('uid');
        $from_date = Input::get('from_date', '');
        $to_date = Input::get('to_date', '');
        if(empty($from_date) && empty($to_date)){
          $timestamp = Carbon::now()->subWeeks(1)->timestamp;
          $goals = Reports::where('datestamp','>=',$timestamp)
                    ->where('cid', $cid[0])
                    ->whereNotNull('goals.'.$goalId)
                    ->get();
        } else {
          $from_date = strtotime($from_date);
          $to_date = strtotime($to_date);
          // echo date('m/d/Y', $from_date); echo '<br />';
          $goals = Reports::where('cid', $cid[0])
                            ->whereNotNull('goals.'.$goalId)
                            ->where('datestamp','>=',$from_date)
                            ->where('datestamp','<=',$to_date)
                            ->get();
        }

        echo json_encode($goals);

    }
}
