<?php

namespace App\Http\Controllers\Api\Component\Reports;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Page\PageController;
use Input;

use App\Experiment;
use App\Segment;
use Carbon\Carbon;

class SegmentsController extends PageController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->viewData['selected'] = array('Dashboard','Segment');
        return view('admin.reports.experiment',$this->viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function segmentdetail(Request $request , $id){
        $this->viewData['selected'] = array('Dashboard','Segment');


        $segment = Segment::where('uid', '=', (int)$id)->first();

        if($segment){
          $this->viewData['updated_at'] = \Carbon::parse($segment->updated_at)->timestamp;
        }else{
          $this->viewData['updated_at'] = \Carbon::now()->timestamp;
        }


        return view('admin.reports.segment-detail',$this->viewData);

    }
}
