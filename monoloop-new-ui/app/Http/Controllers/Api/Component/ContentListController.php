<?php namespace App\Http\Controllers\Api\Component;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ;
use Validator;
use Input;



use App\PageElement;
use App\PageElementContent;
use App\Content;
use App\Experiment;

class ContentListController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function show(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['content_id' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $content_id = $request->input('content_id');


    try {
        $content_id_mongo = new \MongoId($content_id);
        $newContent = Content::find($content_id_mongo);
    } catch (MongoException $ex) {
        return response()->json(['status'=>'error','msg'=> 'Invalid input.'], 400);
    }
    if(!$newContent){
      return response()->json(['status'=>'error','msg'=> 'Variation not found.'], 400);
    }

    return response()->json(['status'=>'success','content'=> $newContent], 200);
  }

	public function index(Request $request){
		// $ret['xpath'] = "HTML BODY DIV#frame DIV.content_bg DIV#main_wrapper DIV.maincol_front DIV#c123 P.bodytext:eq(2)";
		// $ret['fullURL'] = "http://www.monoloop.com/";

    $ret['status'] = 'success';
    $ret['msg'] = "";

    $active_account = Auth::user()->account;

		$start = (Input::get('start')) ? Input::get('start') : 0;
		$pageSize = (Input::get('pageSize')) ? Input::get('pageSize') : 5;

    $validator = Validator::make($request->all(), ['xpath1' => 'required', 'fullURL' => 'required'] );
    if($validator->fails()){
      $ret['status'] = 'error';
      $ret['msg'] = $validator->errors()->all();
      return response()->json($ret, 400);
    }

    $ret['xpath'] = $request->input('xpath1', "");
    $ret['fullURL'] = $request->input('fullURL', "");
		$ret['page_element_id'] = $request->input('page_element_id', "");
		$ret['hashFn'] = $request->input('hash_fn', "_mp_hash_function_");

		$ret['xpath'] = str_replace($ret['hashFn'], "#", $ret['xpath']);

    // echo $active_account->uid;die;

		$pageElement = PageElement::find($ret['page_element_id']);



    $pageElements = PageElement::where('originalUrl','=',$ret['fullURL'])->where('deleted', '<>', 1)->where('cid', '=', $active_account->uid)->get();


		$ret['variations'] = [];

		$ret['experiments'] = [];

		if(count($pageElements) > 0){
			// $ret['totalCount'] = $pageElement->content->where('mappedTo', '=', $ret['xpath'])->where('uid', '<>', null)->count();
			$ret['variationsBelongToExp'] = [];
			$index = 0;
			foreach ($pageElements as $pg) {
				$variations = $pg->content->where('mappedTo', '=', $ret['xpath'])->where('uid', '<>', null)->get();
				// $variations = $pg->content->where('mappedTo', '=', $ret['xpath'])->where('uid', '<>', null)->skip($start)->take($pageSize)->get();
				if(count($variations) > 0){

					if(isset($pg->Experiment)){
						if(isset($pg->Experiment->experimentID)){
							$ret['variationsBelongToExp'][] = $pg->_id;
						}
					}

					foreach ($variations as $value) {
		        $value->content = Content::where('uid', '=',$value->uid)->first();
		        if($value->content){
		          if((int)$value->content->deleted !== 1){
		            $ret['variations'][] = $value;
								$ret['variations'][$index]->page_element_id = $pg->_id;
								$index++;
		          }
		        }
		      }

				}

      }
			$index = 0;


			$pageElementSimilarURLs = PageElement::where('originalUrl','=',$ret['fullURL'])
																						->where('deleted', '<>', 1)
																						->whereNotNull('Experiment')
																						->where('cid', '=', $active_account->uid)
																						->get();
			$index = 0;
			foreach ($pageElementSimilarURLs as $value) {
				$ret['experiments'][] = Experiment::where('experimentID', '=', $value->experiment->experimentID)->first();
				$ret['experiments'][$index]['page_element_id'] = $value->_id;
				$index++;
			}
			$index = 0;
    }
		$ret['hasExperiment'] = false;
		$ret['experiment'] = false;
		if($pageElement){
			if($pageElement->experiment){
				$ret['hasExperiment'] = true;
				$ret['pageElement'] = $pageElement;
				$ret['experiment'] = $pageElement->experiment;
			}
		}


    return response()->json($ret,200);

  }

	public function updateStatus(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['content_id' => 'required', 'hidden' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $content_id = $request->input('content_id');
		$hidden = $request->input('hidden');


    try {
        $content_id_mongo = new \MongoId($content_id);
        $newContent = Content::find($content_id_mongo);
    } catch (MongoException $ex) {
        return response()->json(['status'=>'error','msg'=> 'Invalid input.'], 400);
    }
    if(!$newContent){
      return response()->json(['status'=>'error','msg'=> 'Variation not found.'], 400);
    }
    $newContent->hidden = (int) $hidden;

    $newContent->save();

    return response()->json(['status'=>'success','content'=> $newContent], 200);
  }


	public function moveVariationsToExperiment(Request $request){
		$ret['success'] = true;
		$ret['msg'] = "";

		$pageElementVariationSeperator = '____mp_page_element_variation_seperator____';

		$active_account = Auth::user()->account;

		$validator = Validator::make($request->all(),[
			'fullURL' => 'required',
			'xpath' => 'required',
			// 'experimentID' => 'required',
			'variationsToCopy' => 'required',
			// 'moveToExperimentID' => 'required',
			// 'page_element_id' => 'required'
		]);

		if($validator->fails()){
			$ret['success'] = false;
			$ret['msg'] = $validator->errors()->all();
			return response()->json($ret, 400);
		}

		$ret['fullURL'] = $request->input('fullURL');
		$ret['xpath'] = $request->input('xpath');
		$ret['currentExpID'] =  $request->input('experimentID', 0);
		$ret['variationsToCopy'] = $request->input('variationsToCopy');
		$ret['moveToExperimentID'] = trim($request->input('moveToExperimentID', ''));
		$ret['pageElementId'] = $request->input('page_element_id');
		$ret['allExperiments'] = trim($request->input('allExperiments'));

		if(strlen($ret['allExperiments']) > 0){
			$allExperiments = explode(',',$ret['allExperiments']);
		} else {
			$allExperiments = [];
		}

		if(strlen($ret['moveToExperimentID']) > 0){
			$moveToPageElement = explode(',',$ret['moveToExperimentID']);
		} else {
			$ret['success'] = false;
			$ret['msg'] = 'Please select at least one experiment.';
			return response()->json($ret);
		}


		$variationsToCopyArray = [];

		if(strlen(trim($ret['variationsToCopy'])) > 0){
			$variationsToCopyArray = explode(',',trim($ret['variationsToCopy']));
		}


		$tempVar = [];

		foreach ($variationsToCopyArray as $variationsToCopyArrayElement) {

			$broken = explode($pageElementVariationSeperator, $variationsToCopyArrayElement);

			$pe = $broken[0];
			$uid = $broken[1];

			$pelemenet = PageElement::find($pe);
			$ppec = $pelemenet->content->where('uid', '=', $uid)->first();

			foreach ($allExperiments as $e) {

				$mpe = PageElement::find($e);
			  $mpec = $mpe->content->where('uid', '=', $uid)->first();

				if(in_array($e,$moveToPageElement)){
					if(!$mpec){
						$pageElementContent = new PageElementContent();

				    $pageElementContent->name = $ppec->name;
				    $pageElementContent->connectors = $ppec->connectors;

				    $pageElementContent->uid = $ppec->uid;
				    $pageElementContent->mappedTo = $ppec->mappedTo;
				    $pageElementContent->placementType = $ppec->placementType;
				    $pageElementContent->order = $ppec->order;
				    $pageElementContent->displayType = $ppec->displayType;
				    $pageElementContent->addJS = $ppec->addJS;
				    $pageElementContent->startDate = $ppec->startDate;
				    $pageElementContent->endDate = $ppec->endDate;
				    $pageElementContent->content_type = $ppec->content_type;
				    $pageElementContent->code = $ppec->code;
				    $mpe->content()->save($pageElementContent);
					}
				}

			}

			if(isset($pelemenet->Experiment)){
				if(isset($pelemenet->Experiment->experimentID)){
					if(!in_array($pe, $moveToPageElement)){
						$ppec->delete();
					}
				}
			}
		}

		$ret['tempVar']= $tempVar;
		// var_dump($variationsToCopyArray);die;
		return response()->json($ret);
	}

	public function destroy(Request $request){

    $active_account = Auth::user()->account;

    $validator = Validator::make($request->all(), ['content_id' => 'required'] );

    if ($validator->fails()) {
      return response()->json(['status'=>'error','msg'=> $validator->errors()->all()], 400);
    }

    $content_id = $request->input('content_id');


    try {
        $content_id_mongo = new \MongoId($content_id);
        $newContent = Content::find($content_id_mongo);
    } catch (MongoException $ex) {
        return response()->json(['status'=>'error','msg'=> 'Invalid input.'], 400);
    }
    if(!$newContent){
      return response()->json(['status'=>'error','msg'=> 'Variation not found.'], 400);
    }
    $newContent->deleted = 1;

    $newContent->save();

    return response()->json(['status'=>'success','content'=> $newContent], 200);
  }

}
