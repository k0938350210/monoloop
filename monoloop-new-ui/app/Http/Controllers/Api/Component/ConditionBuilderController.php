<?php namespace App\Http\Controllers\Api\Component;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ;

use App\Services\ConditionBuilder\ConditionalContentReengineer;

class ConditionBuilderController extends Controller {

	public function __construct()
	{
		// $this->middleware('auth');
	}

  public function parseCondition(Request $request){
		// ini_set('memory_limit', '1280M');
		$response = array();
		$response['status'] = 200;
		$response['message'] = "";

		// $conditionStr = "if ( ( ( MonoloopProfile.LastEntryPage === '' ) || ( ( ( MonoloopProfile.LastExitPage === '' ) && ( ( MonoloopProfile.topCategories('', '') === '' ) || ( MonoloopProfile.VisitCount == '' ) ) ) || ( MonoloopProfile.PageViewCount == '' ) ) ) || ( MonoloopProfile.ViewedCategory('', '', '') === true ) ){ | }";
		// $conditionStr = "if ( ( MonoloopProfile.ViewedCategory('2222', '64', 'sad') === true ) ){ | }"; // case 1
		$conditionStr = "if ( ( MonoloopProfile.LastEntryPage === 'a' ) && ( MonoloopProfile.LastExitPage === 'a' ) ){|}"; // case 2
		$conditionStr = "if ( ( ( MonoloopProfile.LastEntryPage === '' ) && ( ( MonoloopProfile.VisitCount == '' ) ) ) || ( MonoloopProfile.LastExitPage === '' ) ){|}"; // case 3
		// $conditionStr = "if ( ( ( MonoloopProfile.LastEntryPage === '' ) && ( ( ( MonoloopProfile.DaysSinceLastVisit == 'asas' ) && ( ( MonoloopProfile.isLowCategory('avg', '2') === true ) ) ) || ( MonoloopProfile.VisitCount == '' ) ) ) || ( MonoloopProfile.LastExitPage === '' ) ){ | }"; // case 4
		// $conditionStr = " if (   ( MonoloopProfile.LastEntryPage === '' )  ||  ( (MonoloopProfile.DaysSinceLastVisit == 0) &&  (   ( MonoloopProfile.isLowCategory('4616','') == true )  ||  ( MonoloopProfile.ViewedCategory('','4616','') == true )  )  )  ){|}"; // case 1
		// $conditionStr = "  if ( ( ( MonoloopProfile.LastEntryPage === '' ) && ( ( ( MonoloopProfile.LastExitPage === '' ) && ( ( ( MonoloopProfile.VisitCount == '' ) && ( ( ( MonoloopProfile.PageViewCount == '' ) && ( ( ( MonoloopProfile.DaysSinceLastVisit == '' ) && ( ( ( MonoloopProfile.ViewedCategory('', '', '') === true ) && ( ( MonoloopProfile.isDecreaseCategory('', '') === true ) ) ) ) ) ) ) ) ) ) ) ) ) ){ | }"; // all and
		// $conditionStr = "if ( ( MonoloopProfile.LastEntryPage === '' ) || ( MonoloopProfile.LastExitPage === '' ) || ( MonoloopProfile.isTopCategory('undefined', 'undefined') === true ) || ( MonoloopProfile.VisitCount == '' ) || ( MonoloopProfile.DaysSinceLastVisit == '' ) || ( MonoloopProfile.ViewedCategory('', '', '') === true ) ){ | }"; // all or
		// $conditionStr = "if ( ( ( ml_dose_not_contains(MonoloopProfile.LastEntryPage, '' ) ) && ( ( MonoloopProfile.ViewedCategory('sdsad', '40', '') === true ) ) ) || ( MonoloopProfile.LastExitPage === 'sdsdsd' ) ){ | }"; // mixture
		// $conditionStr = "if ( ( MonoloopProfile.LastEntryPage === '' ) && ( ( MonoloopProfile.LastEntryPage === '' ) && ( ( ( MonoloopProfile.VisitCount == '' ) && ( ( MonoloopProfile.isLowCategory('', '') === true ) ) ) ) ) || ( MonoloopProfile.VisitCount == '' ) ){ | }"; // complex
		// $conditionStr = "if (( ( MonoloopProfile.LastEntryPage === '' ) && (  ( MonoloopProfile.LastExitPage === '' ) && ( ( MonoloopProfile.DaysSinceLastVisit == '' ) || ( MonoloopProfile.isTopCategory('undefined', 'undefined') === true ) || ( MonoloopProfile.isLowCategory('', '') === true ) ) ) )  || ( MonoloopProfile.LastExitPage === '' ) || ( MonoloopProfile.isGrowthCategory('', '') === true )){ | }"; // complex

		$conditionStr =  $request->input('c');

		$engineerConditionBuilder = new ConditionalContentReengineer();

		$conditionStr = $engineerConditionBuilder->prepareCondition($conditionStr) ;
		$conditionStr = $engineerConditionBuilder->extendBraket($conditionStr) ;

		// $response['content'] = $engineerConditionBuilder->splitAndOr($conditionStr) ;
		$response['conditionstr'] = $conditionStr;
		// $response['content_json']  =$engineerConditionBuilder->formatParsedCondition($response['content'], 0);
		   // die;
		return response()->json($response['conditionstr']);
  }
}
