<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth ;

use App\Country;

class CountryController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

  /*--- REST FULL ----*/

  public function index(Request $request){

		$response = array();
		$response['status'] = 200;
		$response['message'] = "";
		$response['content'] = [] ;

		$query = $request->input('query', "");
		$select = $request->input('select', "");
		$query = $request->input('filter.value', $query);

		if($query !== ''){
			$response['content'] = Country::where('name', 'like', '%'.$query.'%')->get();
		}else{
			$response['content'] =Country::get();
		}

		if($select == true){
			$countries = $response['content'] ;
			$content = [] ;
			foreach ($countries as $country) {
				$content[] = ['value' => $country->name , 'id' => $country->code ] ;
 			}
			$response['content'] = $content ;
		}
		return response()->json($response['content']);
  }
}
