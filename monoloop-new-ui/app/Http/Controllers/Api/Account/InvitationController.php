<?php namespace App\Http\Controllers\Api\Account;
use Auth;
use Carbon;
use DateTime;
use Mail;
use Session;
use MongoId ;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Account;
use App\AccountUser;
use App\AccountDomain;
use App\Services\Hasher;
use App\Placement ;
use App\Folder;

class InvitationController extends Controller {

  private $authenticated;


  public function signup(Request $request)
  {
    $ret = ['success' => false];
    $username = $request->input('email');

    $existingUser = User::where('username', $username)->first();
    if(!empty($existingUser)){
      $ret['msg'] = "Account".$username." already exists";
      return response()->json($ret, 200);
    }

    $email = $username;

    $user = new User();
    // $user = $existingUser;

    $user->active_account = new \MongoId($request->input('accountId'));
    $user->pwdreset_time = 0;
    $user->username = $username;

    $pass = $request->input('password') ;

		if($pass != ''){
			$hasher = new Hasher() ;
			$user->pass = $hasher->getHashedPassword($pass) ;

	 	}
    $user->save();

    $accountUser = AccountUser::find($request->input('accountUserId'));

    $accountUser->user_id = $user->_id;
    $accountUser->email = $email;
    $accountUser->name = $request->input('name');
    $accountUser->save();

    if(Auth::attempt(['username' => $username, 'password' => $pass])){
      $this->authenticated = true;
      $ret['success'] = true;
    } else {
      $this->authenticated = false;
    }


    $ret['redirect'] = '/';
    $ret['authenticated'] = $this->authenticated;


    $ret['user'] = $user;

    return response()->json($ret, 200);
  }

  public function signin(Request $request)
  {
    $ret = ['success' => false];

    $username = $request->input('email');

    $user = User::where('username', $username)->first();

    $email = $request->input('email');

    // $user = $existingUser;


    $pass = $request->input('password') ;

    $credentials = ['username' => $username, 'password' => $request->input('password')];


    $response = $this->authenticateLogin($credentials);
		$header = $response['header'];

		$headers = $this->getHederInfo($header);

		$body = $response['body'];
		$resArr = array();
		$resArr = json_decode($body);
		if($resArr->is_success === 1){

			$ml_token = $headers['Ml-Token'];
			$ml_token = substr($ml_token, 0, -1);
			$ml_token = substr($ml_token,1);

			$user = User::find($resArr->response->_id->{'$id'});

			Session::set('mlToken', $ml_token);



      $this->authenticated = true;

      $user->active_account = new \MongoId($request->input('accountId'));

      $user->save();

      $accountUser = AccountUser::find($request->input('accountUserId'));

      $accountUser->user_id = $user->_id;
      $accountUser->email = $email;
      $accountUser->name = $request->input('name');
      $accountUser->save();
      $ret['success'] = true;
      $ret['user'] = $user;

      Auth::login($user);

		} else {
      $this->authenticated = false;
      $ret['msg'] = "Incorrect username or password.";
      return response()->json($ret, 200);
		}

    $ret['redirect'] = '/';
    $ret['authenticated'] = $this->authenticated;


    return response()->json($ret, 200);
  }

  private function getHederInfo($header_string){
		$myarray=array();
		$data = explode("\n", $header_string);

		$myarray['status'] = $data[0];

		array_shift($data);

		foreach($data as $part){
			$middle = explode(":",$part);
			if(count($middle) === 2){
				$myarray[trim($middle[0])] = trim($middle[1]);
			}
		}
		return $myarray;
	}

	public function authenticateLogin($fields){
		$apihost = config('app.monoloop')['apihost'];
		$url = $apihost.'/api/user/login';
		$headers = ['Content-Type: application/json', 'ML-Version: v1.0'];

		$fields_string = json_encode($fields);
		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		//execute post
		$response = curl_exec($ch);
		$result = [];

		// Then, after your curl_exec call:
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$result['header'] = substr($response, 0, $header_size);
		$result['body'] = substr($response, $header_size);

		//close connection
		curl_close($ch);
		return $result;
	}

  /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    private function authenticate($username, $password)
    {
      if(Auth::attempt(['username' => $username, 'password' => $password])){
        $this->authenticated = true;
      } else {
        $this->authenticated = false;
      }

    }

  private function merge(){

  }
}
