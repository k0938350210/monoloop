<?php namespace App\Http\Controllers\Api\Account;
use Auth;
use MongoId;
use Carbon;
use DateTime;
use Mail;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\ExtjsFilter;
use App\Services\Placement\PlacementService;
use App\Services\PageElement\PageElementService;
use App\Services\Invocation\DomainValidator;
use App\Events\Logger;
use App\Account;
use App\AccountUser;
use App\AccountDomain;
use App\Placement ;
use App\Folder;

class ClientAccountController extends Controller {

	/**
	 * __construct performing pre actions before all the mentioned actions
	 * @return JSON
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

  #--- restful

	public function index(Request $request){
		// further client accounts of logged in account
    $accountUsers = Account::where('parent_id',new \MongoId(Auth::user()->account->_id))->where('deleted', 0)->get();

		// return account user array as json with their total count
		return response()->json([
			'topics' => 	$accountUsers ,
			'totalCount' => count($accountUsers)
		]);
	}

  public function show(Request $request , $id){
    $ret = ['success' => false ] ;
    $account = Account::find($id) ;
    if(!empty($account)){
      return response()->json($ret, 404); ;
    }

    $ret['success'] = true ;
    $ret['account'] = $account ;
    return response()->json($ret, 200);
  }

  public function store(Request $request){

		$this->validate($request, [
		 'firstname' => 'required',
		 'lastname' => 'required',
		 'company' => 'required',
		 'email' => 'required|email',
		 'confirmEmail' => 'required|email|same:email' ,
	 ]);

    $ret = ['success' => false ] ;
    // $account = Account::where('name',$request->input('email'))->first();
		//
    // if(!empty($account)){
    //   $ret['msg'] = 'Account ' . $request->input('name') . ' already exists' ;
    //   return response()->json($ret, 200);
    // }

    $account = Account::where('contact_email',$request->input('email'))
											->where('deleted', '<>', 1)
											->first();

    if(!empty($account)){
      $ret['msg'] = 'Account ' . $request->input('email') . ' already exists' ;
      return response()->json($ret, 200);
    }
    $account = new Account();

    $account->parent_id = new \MongoId(Auth::user()->account->_id);

		$newUid = 1 ;
    $maxUid = Account::max('uid') ;
    if($maxUid){
      $newUid = $maxUid + 1 ;
    }
    $account->uid = $newUid;

		$newCid = 1 ;
		$maxCid = Account::max('cid') ;
		if($maxCid){
			$newCid = $maxCid+1 ;
		}

    $account->cid = $newCid;
    $account->deleted = 0;
    $account->hidden = 0;
    $account->name = $request->input('firstname').' '.$request->input('lastname');
    $account->is_test = false;
    $account->company = $request->input('company');
    $account->contact_name = $request->input('firstname').' '.$request->input('lastname');
    $account->contact_email = $request->input('email');
    $account->phone = $request->input('phone');
    $account->is_reseller = false;
    $account->invoice_reseller = (bool) $request->input('invoice_through_agency', false);
    if(!$account->invoice_reseller){
      $account->type = $request->input('type');
    } else {
      $account->type = "";
    }
    $account->lock_account_type = false;
    $account->auto_activate_after_trial = false;
    $account->trialconvertion = 'ISODate("1970-01-01T00:00:00.000Z")';
    $account->save();

    $accountUser = new AccountUser();
    $accountUser->account_id = $account->_id;
    $accountUser->user_id = NULL;
    $accountUser->email = NULL;
    $accountUser->company = NULL;
    $accountUser->name = NULL;
    $accountUser->deleted = 0;
    $accountUser->hidden = 0;
    $accountUser->is_monoloop_admin = 0;
    $accountUser->is_monoloop_support = 0;
    $accountUser->dashboard_config = [];
    $accountUser->support_type = 0;
    $accountUser->support_enddate = NULL;
    $accountUser->save();

		// creating new folder
		$folder = new Folder();
		$folder->account_id = new \MongoId($account->_id);
		$folder->deleted = 0;
		$folder->hidden = 0;
		$folder->title = 'Root';
		$folder->parent_id = null;
		$folder->created_at = new \Datetime();
		$folder->updated_at = new \Datetime();
		$folder->save();

    $ret['success'] = true ;
		$ret['account'] = $account ;
		$ret['accountUser'] = $accountUser ;
		$ret['email'] = true ;
		\Event::fire(new Logger(Auth::user()->account->name, "account created", $account->name, "App\Account"));
    return response()->json($ret, 200);
  }

  public function update(Request $request , $id){

		$this->validate($request, [
		 'firstname' => 'required',
		 'lastname' => 'required',
		 'company' => 'required',
		 'email' => 'required|email',
		 'confirmEmail' => 'required|email|same:email' ,
	 ]);

    $ret = ['success' => false ] ;
    $account = Account::find($id);
    if(empty($account)){
      $ret['msg'] = 'An error occoured, please refresh the page.' ;
      return response()->json($ret, 200);
    }

    // $accountExisting = Account::where('name',$request->input('name'))->where('name', '<>', $account->name)->first();
		//
    // if(!empty($accountExisting)){
    //   $ret['msg'] = 'Account ' . $request->input('name') . ' already exists' ;
    //   return response()->json($ret, 200);
    // }

    $accountExisting = Account::where('contact_email',$request->input('email'))
															->where('contact_email', '<>', $account->contact_email)
															->where('deleted', '<>', 1)
															->first();

    if(!empty($accountExisting)){
      $ret['msg'] = 'Account ' . $request->input('email') . ' already exists' ;
      return response()->json($ret, 200);
    }

    $account->name = $request->input('firstname').' '.$request->input('lastname');
    $account->company = $request->input('company');
    $account->contact_name = $request->input('firstname').' '.$request->input('lastname');
    $account->contact_email = $request->input('email');
    $account->phone = $request->input('phone');
    $account->invoice_reseller = (bool) $request->input('invoice_through_agency', false);
		// var_dump($account->invoice_reseller);die;
    if(!$account->invoice_reseller){
      $account->type = $request->input('type');
    } else {
      $account->type = "";
    }
    $account->save();

    $ret['success'] = true ;
		$ret['email'] = true;
    $ret['account'] = $account ;

		\Event::fire(new Logger(Auth::user()->account->name, "account updated", $account->name, "App\Account"));
    return response()->json($ret, 200);
  }

  public function destroy(Request $request , $id){
    $ret = ['success' => false ] ;
    $account = Account::find($id) ;
    if(!empty($account)){
      $account->deleted = 1 ;
      $account->save() ;
    }

    $ret['success'] = true ;
		$ret['account'] = $account ;
		\Event::fire(new Logger(Auth::user()->account->name, "account deleted", $account->name, "App\Account"));
    return response()->json($ret, 200);
  }

  /*--- custom function ---*/

  public function statusUpdate(Request $request , $id){
    $ret = ['success' => false ] ;

    $account = Account::find($id) ;
    if(!empty($account)){
      $account->hidden = $request->input('hidden') ;
      $account->save() ;
    }

    $ret['success'] = true ;
		$ret['account'] = $account ;
    return response()->json($ret, 200);
  }

	public function invite(Request $request , $id){
    $ret = ['success' => false ] ;

    $account = Account::find($id) ;
    if(empty($account)){
      return response()->json($ret, 200);
    }
		$accountUser = AccountUser::where('account_id', $account->_id)->first();
		$accountUser->invitationType = "clientAccount";
		$mailData = [
									'user' => $accountUser,
									'loginAccount' => Auth::user()->account
								];

		Mail::send('emails.invitation.user', $mailData, function($message) use ($account)
		{
				$message->to($account->contact_email, $account->contact_name)->subject('Monoloop: Account Invitation!');
		});
		die;
  }
}
