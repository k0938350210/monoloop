<?php

namespace App\Http\Controllers\Api\Account;

use Auth;
use MongoId;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AppConfig;
use App\Services\Profile\Profile;
use Input;
use App\Events\Logger;
use Validator;

class AppConfigController extends Controller
{
    /**
     * __construct performing pre actions before all the mentioned actions.
     *
     * @return JSON
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


		/**
		 * Get the validation rules that apply to the request.
		 *
		 * @return array
		 */
		public function rules()
		{
		    return [
		        'name' => 'required',
		        'access_type' => 'required',
						'status_type' => 'required',
		        'livestream' => 'required',
						'authen_type' => 'required',
		    ];
		}

    /**
     * returning all the logs related to the current account.
     *
     * @return JSON
     */
    public function index(Request $request)
    {
        $data = AppConfig::where('account_id', '=', new \MongoId(Auth::user()->account->_id))->where('deleted', '=', 0)->get();

        $profileFields = $this->getProfileFieldData();

        return response()->json(array('topics' => $data, 'totalCount' => count($data), 'profileFields' =>  $profileFields));
    }

		/**
     * Get profile fields data .
     *
     * @return array
     */
    private function getProfileFieldData()
    {
        $profile = new Profile();

				$profileProperties = $profile->getProperties();

				$profileFieldsData = array_map(function($v){
					return ['id' => $v['key'], 'value' => $v['Label']];
				}, $profileProperties);
				return array_values($profileFieldsData);
    }


    public function show(Request $request, $id)
    {
        $ret = ['success' => false];
        $domain = Auth::user()->account->domains->where('domain', $id)->first();
        if (empty($domain)) {
            return response()->json($ret, 404);
        }
        $ret['success'] = true;
        $ret['domain'] = $domain;

        return response()->json($ret, 200);
    }

    public function store(Request $request)
    {
				$ret = ['success' => false];
				// validation rules
				$rules = $this->rules();

				// validate against the inputs from our form
				$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
					$messages = $validator->messages();
					$ret['msg'] = $messages;
					return response()->json($ret, 200);
				}
        $application = new AppConfig();
				$application->account_id = new \MongoId(Auth::user()->account->_id);
				$application->name = $request->input('name');
        $application->description = $request->input('description');
				$application->access_type = $request->input('access_type');
				$application->status_type = $request->input('status_type');
				$application->livestream = $request->input('livestream');
				$application->authen_type = $request->input('authen_type');
				$application->host = $request->input('host');
				$application->api_token = $request->input('api_token');
				$application->hidden = 0;
				$application->deleted = 0;
				$optionsTriggers = [];
				if(!empty($request->input('option_trigger'))){
					$optionsTriggers = explode(',', $request->input('option_trigger'));
				}
				$application->option_trigger = $optionsTriggers;
				$fields = [];
        if(!empty($request->input('fields'))){
          $fields = explode(',', $request->input('fields'));
        }
				$application->fields = $fields;



        $application->save();

        $ret['success'] = true;
        $ret['application'] = $application;


				\Event::fire(new Logger(Auth::user()->account->name, "application created", $application->name, "App\Application"));

        return response()->json($ret, 200);
    }

    public function update(Request $request, $id)
    {
			$ret = ['success' => false];
			// validation rules
			$rules = $this->rules();

			// validate against the inputs from our form
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
				$messages = $validator->messages();
				$ret['msg'] = $messages;
				return response()->json($ret, 200);
			}

			$application = AppConfig::find($id);
			$application->name = $request->input('name');
			$application->description = $request->input('description');
			$application->access_type = $request->input('access_type');
			$application->status_type = $request->input('status_type');
			$application->livestream = $request->input('livestream');
			$application->authen_type = $request->input('authen_type');
			$application->host = $request->input('host');
			$application->api_token = $request->input('api_token');
			$application->hidden = 0;
			$application->deleted = 0;
			$optionsTriggers = [];
			if(!empty($request->input('option_trigger'))){
				$optionsTriggers = explode(',', $request->input('option_trigger'));
			}
			$application->option_trigger = $optionsTriggers;
      $fields = [];
      if(!empty($request->input('fields'))){
        $fields = explode(',', $request->input('fields'));
      }
      $application->fields = $fields;



			$application->save();

			$ret['success'] = true;
			$ret['application'] = $application;

			\Event::fire(new Logger(Auth::user()->account->name, "application updated", $application->name, "App\Application"));
			return response()->json($ret, 200);
    }

    public function destroy(Request $request, $id)
    {
			$ret = ['success' => false];
			$application = AppConfig::find($id);
			$application->deleted = 1;
			$application->save();

			$ret['success'] = true;
			$ret['application'] = $application;

			\Event::fire(new Logger(Auth::user()->account->name, "application deleted", $application->name, "App\Application"));
			return response()->json($ret, 200);
    }
}
