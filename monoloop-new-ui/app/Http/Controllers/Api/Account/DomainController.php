<?php namespace App\Http\Controllers\Api\Account;
use Auth;
use MongoId;
use Carbon;
use DateTime;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\ExtjsFilter;
use App\Services\Placement\PlacementService;
use App\Services\PageElement\PageElementService;
use App\Services\Invocation\DomainValidator;
use App\Events\Logger;
use App\AccountDomain;
use App\Placement ;

use App\Http\Requests\Account\DomainRequest ;

class DomainController extends Controller {

	/**
	 * __construct performing pre actions before all the mentioned actions
	 * @return JSON
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

  #--- restful

	public function index(Request $request){

		$domains = Auth::user()->account->domains->get();



		// $testDomains = Auth::user()->account->domains->where('laststatus' , '<' , new DateTime('-1 weeks'))->limit(3)->get() ;
    // foreach($testDomains as $domain){
    //   $domainValidator = new DomainValidator();
    //   $domain->status = $domainValidator->checkInvocation($domain->domain);
    //   $domain->laststatus = Carbon::now();
    //   $domain->save() ;
    // }

		return response()->json([
			'topics' => 	$domains ,
			'totalCount' => count($domains)
		]);
	}

  public function show(Request $request , $id){
    $ret = ['success' => false ] ;

    $domain = Auth::user()->account->domains->where('domain',$id)->first() ;
    if(empty($domain)){
      return response()->json($ret, 404); ;
    }
    $ret['success'] = true ;
    $ret['domain'] = $domain ;
    return response()->json($ret, 200);
  }

  public function store(DomainRequest $request){
    $ret = ['success' => false ] ;

    $domainObj = Auth::user()->account->domains->where('domain',$request->input('domain'))->first() ;
    if(!empty($domainObj)){
      $ret['msg'] = 'Domain ' . $request->input('domain') . ' already exists' ;
      return response()->json($ret, 200);
    }

    $domain = new AccountDomain() ;
    $domain->domain = $request->input('domain') ;
    $domain->privacy_option = $request->input('privacy_option') ;
    $domain->privacy_usecustom = (bool)$request->input('privacy_usecustom');

		// Information
		$domain->i1 = $request->input('i1');

		// Preferences
		$domain->p1 = $request->input('p1');
		$domain->p2 = $request->input('p2');
		$domain->p3 = $request->input('p3');
		$domain->p4 = $request->input('p4');
		$domain->p5 = $request->input('p5');

		// Delete
		$domain->d1 = $request->input('d1');
		$domain->d2 = $request->input('d2');
		$domain->d3 = $request->input('d3');
		$domain->d4 = $request->input('d4');

    $domain->url_privacy = $request->input('url_privacy') ;

    Auth::user()->account->domains()->save($domain) ;

    #create placement mapped ;
    $placementService = new PlacementService() ;
    $placement = $placementService->getPrivacyCenterObject() ;
    $placement->url = $domain->url_privacy  ;
    if(trim($placement->url) == '' || $domain->privacy_option == 'manual'){
      $placement->hidden = 1 ;
    }
    $placement->save();

    $domain->placement_id = new  MongoId( $placement->_id ) ;
    $domain->save() ;

    #process placement to PageElement
    $pageElementService = new PageElementService() ;
    $pageElementService->processByPlacement($placement) ;

    $ret['success'] = true ;
		$ret['domain'] = $domain ;
		\Event::fire(new Logger(Auth::user()->account->name, "domain created", $domain->domain, "App\AccountDomain"));
    return response()->json($ret, 200);
  }

  public function update(DomainRequest $request , $id){
    $ret = ['success' => false ] ;

    $domain = Auth::user()->account->domains->where('domain',$id)->first() ;
    if(empty($domain)){
      return response()->json($ret, 404); ;
    }

    $total = Auth::user()->account->domains->where('domain',$request->input('domain'))->where('domain','<>',$id)->count() ;
    if($total == 1){
      $ret['msg'] = 'Domain ' . $request->input('domain') . ' already exists' ;
      return response()->json($ret, 200);
    }

    $domain->domain = $request->input('domain') ;
    $domain->privacy_option = $request->input('privacy_option') ;
		$domain->privacy_usecustom = (bool)$request->input('privacy_usecustom');

		// Information
		$domain->i1 = $request->input('i1');

		// Preferences
		$domain->p1 = $request->input('p1');
		$domain->p2 = $request->input('p2');
		$domain->p3 = $request->input('p3');
		$domain->p4 = $request->input('p4');
		$domain->p5 = $request->input('p5');

		// Delete
		$domain->d1 = $request->input('d1');
		$domain->d2 = $request->input('d2');
		$domain->d3 = $request->input('d3');
		$domain->d4 = $request->input('d4');

    $domain->url_privacy = $request->input('url_privacy') ;
    $domain->save() ;

    $placement = $domain->placement ;
    if(is_null($placement)){
      $placementService = new PlacementService() ;
      $placement = $placementService->getPrivacyCenterObject() ;
      $placement->save();
      $domain->placement_id = new  MongoId( $placement->_id ) ;
      $domain->save() ;
    }
    $placement->url = $domain->url_privacy  ;
    if(trim($placement->url) == '' || $domain->privacy_option == 'manual'){
      $placement->hidden = 1 ;
    }else{
      $placement->hidden = 0 ;
    }
    $placement->save();

    #process placement to PageElement
    $pageElementService = new PageElementService() ;
    $pageElementService->processByPlacement($placement) ;

    $ret['success'] = true ;
		$ret['domain'] = $domain ;
		\Event::fire(new Logger(Auth::user()->account->name, "domain updated", $domain->domain, "App\AccountDomain"));
    return response()->json($ret, 200);
  }

  public function destroy(Request $request , $id){
    $ret = ['success' => false ] ;

    $domain = Auth::user()->account->domains->where('_id',$id)->first() ;
		// var_dump($domain);die;
    if(empty($domain)){
      return response()->json($ret, 404); ;
    }

    $placement = $domain->placement ;
    if(!is_null($placement)){
      $placement->deleted = 1 ;
      $placement->save() ;

      $pageElementService = new PageElementService() ;
      $pageElementService->processByPlacement($placement) ;
    }

    $domain->delete();
    $ret['success'] = true ;
		\Event::fire(new Logger(Auth::user()->account->name, "domain deleted", $domain->domain, "App\AccountDomain"));
    return response()->json($ret, 200);
  }

  /*--- custom function ---*/

  public function testDomain(Request $request , $id){
    $ret = ['success' => false ] ;

    $domain = Auth::user()->account->domains->where('domain',$id)->first() ;
    if(!empty($domain)){
      $domainValidator = new DomainValidator() ;
			
      $domain->status = $domainValidator->checkInvocation($domain->domain) ;
      $domain->laststatus = Carbon\Carbon::now()->format('Y-m-d H:i:s') ;
      $domain->save() ;
    }

    $ret['success'] = true ;
		$ret['domain'] = $domain ;
		\Event::fire(new Logger(Auth::user()->account->name, "domain tested", $domain->domain, "App\AccountDomain"));
    return response()->json($ret, 200);
  }

}
