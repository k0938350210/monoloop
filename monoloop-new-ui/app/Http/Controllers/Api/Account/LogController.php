<?php namespace App\Http\Controllers\Api\Account;
use Auth;
use MongoId;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Log;
use Input;

class LogController extends Controller {

	/**
	* __construct performing pre actions before all the mentioned actions
	* @return JSON
	*/
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	* returning all the logs related to the current account
	* @return JSON
	*/
	public function index(Request $request){

		// starting position of log
		$start = (Input::get('start')) ? Input::get('start') : 0;
		$logableType=(Input::get('type')) ? Input::get('type') : false;
		$logableId=(Input::get('id')) ? Input::get('id') : false;
		$logableId= $logableId ? new \MongoId($logableId) : false;

		$log = Log::where('cid', Auth::user()->account->uid);
		$totalCount=0;
		if($logableType && $logableId) {

			$log = $log->where('logable_type','=',$logableType)->where('logable_id','=',$logableId);

		}if($logableType) {
			$log = $log->where('logable_type','=',$logableType);
		}if($logableId){
			$log = $log->where('logable_id','=',$logableId);
		}
		else{
			// count for all the logs
			$totalCount = $log->count();
			// retreiving logs belonging to the current account
			$log = $log->skip($start)->take(30);
		}

		$data = $log->orderBy('created_at','desc')->get();

		// return accounts and it's count as JSON array
		return response()->json( array('data' => $data, 'pos' => $start , 'total_count' => $totalCount )) ;
	}

}
