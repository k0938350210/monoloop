<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Profile;
use Illuminate\Database\Eloquent\ModelNotFoundException ;

class ProfilesController  extends Controller {
	public function show(Request $request , $gmid){

		$skey = $request->input('skey') ;
		if($this->generateSign($gmid) !== $skey){
			return response()->json(['error' => 'MID and skey does not match']);
		}

		try{
			$profile = Profile::findOrFail($gmid);
			return response()->json(['profile' => $profile->toArray()]);
		}catch(ModelNotFoundException $e)
		{
			return response()->json(['success' => false ],404);
		}
	}

	public function destroy(Request $request , $gmid){
		$skey = $request->input('skey') ;
		if($this->generateSign($gmid) !== $skey){
			return response()->json(['error' => 'MID and skey does not match']);
		}

		try{
			$profile = Profile::findOrFail($gmid);
			$profile->delete();
			return response()->json(['success' => true ]);
		}catch(ModelNotFoundException $e)
		{
			return response()->json(['success' => false ],422);
		}
	}

	private function generateSign($str){
    $salt = '130293+109k21jl1c31c9312i3c910202'  ;
    return hash('sha512',$str . $salt);
	}
}