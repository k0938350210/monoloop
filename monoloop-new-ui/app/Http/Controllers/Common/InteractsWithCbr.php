<?php namespace App\Http\Controllers\Common ; 
 
use App\Jobs\GenerateCbr;

trait InteractsWithCbr
{
  /*------- custom --------*/ 
  private function generateCbr($account){
		#add to cbr job 
    $this->dispatch(new GenerateCbr($account));
	}
}