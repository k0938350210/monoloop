<?php

namespace App\Http\Controllers\Common;

trait RedirectManager {
  public function isSecure(){
  	$isSecure = false;
  	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on"){
  		$isSecure = true;
  	}else if( !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on'){
  		$isSecure = true;
  	}
  	return $isSecure;
  }
}
