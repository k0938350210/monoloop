<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ReportingArchiveAggregated ;
use App\Account ;
use Notification ;
use Carbon;

class AccountController extends Controller {
  public function usage(){
    $date = date('d-m-Y');
  	list($day, $month, $year) = explode('-', $date);
  	$ts = (string)mktime(0, 0, 0, $month, $day, $year);
    $arr_ts = array(
      $ts ,
			$ts-(1*24*60*60),
            $ts-(2*24*60*60),
            $ts-(3*24*60*60),
            $ts-(4*24*60*60),
			$ts-(5*24*60*60),
			$ts-(6*24*60*60)
	  );

    $data = ReportingArchiveAggregated::where('datestamp','>=' , $arr_ts[6] )->get() ;

    $accounts = array() ;

    foreach($data as $row){
      $date = date('m/d/Y',$row->datestamp) ;
      if(!isset($accounts[$row->cid][$date])){
        $accounts[$row->cid][$date] = ['pages' => 0 , 'visits' => 0 , 'datestamp' => $row->datestamp];
      }
      $accounts[$row->cid][$date]['pages'] += $row->totalPages ;
      $accounts[$row->cid][$date]['visits'] += $row->totalVisits ;

    }

    foreach($accounts as $key => &$account){
      $account['account'] = Account::ByUid($key)->first() ;
    }


    return view('admin.account.usage')->with('dates',$arr_ts)->with('accounts',$accounts);
  }

  public function usage_per_account(Request $requests){
    $last_month = new Carbon('last month') ;
    $start = $requests->input('start', $last_month->format('m/d/Y') );
    $end = $requests->input('end',Carbon::now()->format('m/d/Y'));
    $uid = $requests->input('uid',0);

    $accounts = Account::orderBy('uid')->lists('company', 'uid');

    foreach ($accounts as $key => $account) {
      $accounts[$key] = $key . '-' . $account ;
    }

    $report = [] ;

    $total = 0 ;

    if($uid !==0 ){
      $account = Account::byUid($uid)->first();
      list($month1, $day1, $year1) = explode('/', $start);
      list($month2, $day2, $year2) = explode('/', $end);

      $ts =  mktime(0, 0, 0, $month1, $day1, $year1);
      $ts2 = mktime(0, 0, 0, $month2, $day2, $year2);
      $ts2 += 86400 ;

      $temp_reports = ReportingArchiveAggregated::where('datestamp','>=',$ts)->where('datestamp','<',$ts2)->where('cid',$account->uid)->get() ;

      foreach($temp_reports as $row){
        $date = date('m/d/Y',$row->datestamp) ;
        if(!isset($report[$date])){
          $report[$date] = ['pages' => 0 , 'datestamp' => $row->datestamp];
        }
        $report[$date]['pages'] += $row->totalPages ;
        $total += $row->totalPages ;
      }
    }



    return view('admin.account.usage_per_account')->with('start',$start)->with('end',$end)->with('accounts',$accounts)->with('uid',$uid)->with('report',$report)->with('total',$total);
  }
}
