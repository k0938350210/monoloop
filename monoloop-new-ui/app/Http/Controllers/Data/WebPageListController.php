<?php

namespace App\Http\Controllers\data;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;


class WebPageListController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Experiment basic view
     *
     * @return Response
     */
    public function getPWData(Request $request)
    {
      $dataSet = [];

      $defaultDomain = Auth::user()->account->domains->first();
  		$dataSet['defaultDomainUrl'] = "";
  		if(!empty($defaultDomain)){
  			$dataSet['defaultDomainUrl'] = $defaultDomain->domain;
  		}
      return response()->json($dataSet);
    }
}
