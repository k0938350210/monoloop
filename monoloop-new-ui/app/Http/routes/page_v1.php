<?php 

Route::group(['prefix' => 'v1'], function()
{
  // UI MAP
  # Page - Dashboard
  Route::get('/', 'Page\V1\DashboardController@index');
  Route::get('/dashboard/funnel', 'Page\V1\DashboardController@funnel');
  # Page - Account
  Route::get('/account/profile', 'Page\V1\AccountController@profile');
  Route::get('/account/control-group', 'Page\V1\AccountController@controlgroup');
  Route::get('/account/code-and-scripts', 'Page\V1\AccountController@codeandscripts');
  Route::get('/account/api-token', 'Page\V1\AccountController@apitoken');
  Route::get('/account/plugins', 'Page\V1\AccountController@plugins');
  Route::get('/account/users', 'Page\V1\AccountController@users');
  Route::get('/account/client-accounts', 'Page\V1\AccountController@clientaccounts');
  Route::get('/account/scores', 'Page\V1\AccountController@scores');
  Route::get('/account/domains', 'Page\V1\AccountController@domains');
  Route::get('/account/applications', 'Page\V1\AccountController@applications');
  Route::get('/account/logs', 'Page\V1\AccountController@logs');
  # Page Trackers 
  Route::get('/trackers', 'Page\V1\TrackerController@index');
  # Page Content
  Route::get('/content/web', 'Page\V1\ContentController@web');
  Route::get('/content/blueprints', 'Page\V1\ContentController@bluepronts');
  Route::get('/segment', 'Page\V1\ContentController@segment');
  Route::get('/experiment', 'Page\V1\ContentController@experiment');
  Route::get('/goals', 'Page\V1\ContentController@goals'); 
});
