<?php

Route::group(['prefix' => 'component'], function()
{
		Route::get('/profile-properties-template', 'Component\ConditionBuilderController@template');
		Route::get('/publish-condition-builder', 'Component\ConditionBuilderController@publicTemplate');
    Route::get('/placement-window', 'Component\PlacementWindowController@template');
		Route::get('/image-uploader', 'Component\ImageUploaderController@index');

		Route::get('/content-list', 'Component\ContentListController@index');
		Route::get('/content-create', 'Component\ContentListController@create');
		Route::get('/content-update', 'Component\ContentListController@update');
		Route::get('/content-add-to-experiment', 'Component\ContentListController@addToExperiment');
		Route::get('/content-add-segment', 'Component\ContentListController@addSegment');
		Route::get('/content-add-goal', 'Component\ContentListController@addGoal');
		Route::get('/page-list-publish', 'Component\PageListController@publish');
		Route::post('/page-list-publish/store', 'Component\PageListController@publishStore');
		Route::get('/testbench-preview','Component\TestbenchController@preview');

		Route::get('/content-menu', 'Component\ContentMenuController@index');
});
