<?php

Route::group(['prefix' => 'data'], function()
{
  Route::group(['prefix' => 'experiments'], function()
  {
      Route::get('basic','Data\ExperimentController@getbasic' );
      Route::get('segment','Data\ExperimentController@getSegment' );
      Route::get('goal','Data\ExperimentController@getGoal' );
      Route::get('placement','Data\ExperimentController@getPlacement' );
      Route::get('control-group','Data\ExperimentController@getControlGroup' );
      Route::get('confirm','Data\ExperimentController@getConfirm' );
  });
  Route::group(['prefix' => 'domains'], function()
  {
      Route::get('default','Data\WebPageListController@getPWData' );
  });
});
