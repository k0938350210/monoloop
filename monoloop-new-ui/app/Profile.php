<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;


class Profile extends Eloquent {

  protected $connection = 'mongodb';

  public function __construct(array $attributes = [])
  {
    parent::__construct($attributes);

    if(env('APP_ENV') == 'testing'){
      $this->connection = 'mongodb_testing';
    }
  }

  protected $collection = 'profiles';
}