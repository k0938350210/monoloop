<?php
namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class Tracker extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $collection = 'trackers';

    public function urlConfig(){
      return $this->embedsOne(\App\UrlConfig::class,null,'url_config','url_config');
    }

    /*
     * The trackers associated with specific folder
     *
     * @param folder Object
     * @rerurn array
     */
    public static function trackers($folder = NULL, $isRoot = false, $account_id = NULL){

    	$branch = [];

      if($isRoot){
          $trackers = Tracker::where('deleted', '=', 0)
                              ->where(function ($query) use ($folder, $account_id) {
                                  $query->where('folder_id', '=', new \MongoId($folder))
                                        ->orWhere(function($query) use ($folder, $account_id){
                                          $query->where('folder_id', '=', NULL)
                                                ->where('account_id', '=', $account_id);
                                        });
                            } )->get();
      } else {
          $trackers = Tracker::where('folder_id', '=', new \MongoId($folder))->where('deleted', '=', 0)->get();
      }


        foreach ($trackers as $key => $tracker) {
            $node = new \stdClass();
            $node->id = "tracker__".$tracker->_id;
            $node->value = $tracker->name;
            $node->type = "file";
            $node->description = $tracker->type;
            $node->date = date('j F, Y', strtotime($tracker->created_at));
            $node->hidden = $tracker->attributes['hidden'];
            $node->condition = $tracker->condition;
    		    array_push($branch, $node);
    	}

      $res['data'] = $branch;
      $res['parent'] = 'folder__'.$folder;
      return $res;
    }

    /*
     * The trckers associated with account_id
     *
     * @param $query
     * @param $account_id
     *
     * @rerurn array
     */
    public function scopeCompactList($query , $account_id){
      return $query->where('account_id', '=', new \MongoId($account_id))->where('deleted', '<>', 1 )->get();
    }

    public function scopeFolderTracker($query , $folder_id){
      return $query->where('folder_id', '=', new \MongoId($folder_id));
    }

    public function scopeByCid($query,$cid){
      return $query->where('cid',$cid);
    }

    public function scopeIsSystem($query){
      return $query->where('system',1);
    }

    public function scopeByName($query,$name){
      return $query->where('name',$name);
    }
}
