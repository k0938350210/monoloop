<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class UrlConfig extends Eloquent{
  protected $fillable = ['url','reg_ex','inc_www','inc_http_https','url_option' ];
  /*--- attribute ---*/
  public function getOptionStringAttribute(){
    switch ($this->url_option) {
      case 1:
        return 'allowParams';
        break;
      case 2:
        return 'startsWith';
        break;
    }
    return 'exactMatch';
  }

  public function getCleanUrlAttribute(){
    $url_info = parse_url($this->url);

    if(!isset($url_info['scheme']))
      return $this->url ;

    $port = '' ;
    if (isset($url_info['port']) && $url_info['port']!=80) {
      $port = ':'.$url_info['port'];
    }

    if(!isset($url_info['host']))
      $url_info['host'] = '' ;

    if(!isset($url_info['path']))
      $url_info['path'] = '' ;

    $host = strtolower($url_info['host']) ;
    $pos = strpos($host , 'www.' ) ;
    if( $pos === 0 ){
      $host = substr($host , 4) ;
    }

    $path = '' ;
    if (isset($url_info['path'])) {
      $path = $url_info['path'] ;
    }

    if(!isset($url_info['query']))
      $url_info['query'] = '' ;

    if( trim($url_info['query']) == ''){
      $url = strtolower($url_info['scheme']). '://' . $host . $port . $path  ;
    }else{
      $url = strtolower($url_info['scheme']). '://' . $host . $port . $path . '?' .  $url_info['query']   ;
    }
    if( $url ==  strtolower($url_info['scheme']). '://' . $host ){
			$url .= '/' ;
		}
    return $url;
  }

  public function getUrlHashAttribute(){
    // Clean URL from sune code .
    $url_info = parse_url($this->url);
    $port = '' ;
    if (isset($url_info['port']) && $url_info['port']!=80) {
      $port = ':'.$url_info['port'];
    }

    $path = '' ;
    if (isset($url_info['path'])) {
      $path = $url_info['path'] ;
    }
    if(!isset($url_info['host'])){
      $url_info['host'] = '' ;
    }
    
    $url = isset($url_info['scheme'])?$url_info['scheme']:'http' ;
    $url .= '://'.$url_info['host'].$port.$path;
    return substr(md5($url), 0, 16) . sprintf('%x', crc32($url));
  }

  /*--- setter ---*/
  public function setUrlOptionFromString($string){
    switch ($string) {
      case 'exactMatch' : $this->url_option = 0 ; break ;
      case 'allowParams' : $this->url_option = 1 ; break ;
      case 'startsWith' : $this->url_option = 2 ; break ;
    }
  }
}
