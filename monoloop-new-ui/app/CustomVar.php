<?php

namespace App;

class CustomVar extends TrackerBase
{
    /*------- relation -------- */

  public function custom_field()
  {
      return $this->belongsTo(\App\CustomField::class, 'custom_field_id');
  }

  /*-------- scope -----------*/

  /*--------------------------*/
  public function toArray()
  {
      $array = parent::toArray();
      $array['custom_field'] = $this->custom_field;

      return $array;
  }
}
