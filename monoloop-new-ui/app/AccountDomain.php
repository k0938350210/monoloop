<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

class AccountDomain extends Eloquent{

  protected $dates = array('laststatus');

  protected $fillable = ['domain', 'url_privacy', 'status' ,'privacy_option' ]; 

  /*------- relation -------- */

  public function placement(){
    return $this->belongsTo(\App\Placement::class,'placement_id');
  }

}
