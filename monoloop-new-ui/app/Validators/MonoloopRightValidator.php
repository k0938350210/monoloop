<?php namespace App\Validators;

use Validator;
use DB; 
use Auth ; 

class MonoloopRightValidator extends Validator {
  public function validate($attribute, $value, $parameters)
  { 
    if(\MongoId::isValid($value) == false){
      return false;
    }
    
    $row = DB::collection($parameters[0])->where($attribute , new \MongoId($value))->get() ; 
    if(count($row) == 0 ){
      return false ; 
    }
    
    if( (string)$row[0][$parameters[1]] == (string)Auth::user()->active_account ){
      return true ; 
    } 
    return false ; 
  }
}