<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;

use MongoId ;

class ContentTemplate extends Eloquent{
	protected $collection = 'content_templates';
	protected $fillable = ['uid', 'name', 'description','title','ds_uid','account_id','data_structure'];
  /*--- Scope ---*/

	public function scopeByName($query,$name){
		return $query->where('name','=',$name);
	}

  public function scopeByAccount($query, $account)
  {
    return $query->where('account_id', '=', new MongoId($account->_id));
  }

  public function scopeSearch($query,$search){
    if(trim($search) == '')
      return $query ;
    return $query->where('name','like','%' . $search . '%') ;
  }

  public function scopeByUid($query,$uid){
    return $query->where('uid','=',(int)$uid) ;
  }
}
