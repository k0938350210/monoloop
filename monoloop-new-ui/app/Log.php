<?php namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use MongoId ;
use Carbon;

class Log extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'logs';


	/**
	 * the account ID used by the log
	 * @return void
	 */
	public function account()
  {
    return $this->belongsTo('\App\Account','account_id');
  }

	public function user(){
		return $this->belongsTo('\App\User','user_id');
	}

	/*------- scope -----------*/
	public function scopeSearch($query,$s,$start,$end){
		if(trim($s) == '' && trim($start) == '' && trim($end) == ''){
			return $query ;
		}
		return $query->where(function ($query) use ($s,$start,$end) {
			if(trim($s) != ''){
				$query->where('msg', 'like', '%'.$s.'%') ;
			}
			if(trim($start) != '' && trim($end) != ''){
				$startDate = Carbon::createFromFormat('m/d/Y', $start) ;
				$endDate = Carbon::createFromFormat('m/d/Y', $end) ;
				$query->where('updated_at','<=',$endDate)->where('updated_at','>=',$startDate);
				#echo $startDate . ' ' . $endDate ; die() ;
			}
		});
	}

  public function scopeByAccount($query , $id){
    return $query->where('account_id','=', new MongoId($id)) ;
  }

	public function scopeByUser($query , $id){
    return $query->where('user_id','=', new MongoId($id)) ;
  }
  /*
    public function getCreatedAtAttribute($value){
        if (isset($_COOKIE["time_zone"])){
            $timezone = $_COOKIE["time_zone"];

        }else{
            $timezone = "UTC";
        }


        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone($timezone)
            ->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value){
        if (isset($_COOKIE["time_zone"])){
            $timezone = $_COOKIE["time_zone"];

        }else{
            $timezone = "UTC";
        }


        return Carbon::createFromTimestamp(strtotime($value))
            ->timezone($timezone)
            ->toDateTimeString();
    }
		*/

    public function getCreatedAtFormatAttribute(){
			$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at  );
			$date->setTimezone('Asia/Bangkok');
			return $date->format('d/m/Y H:i:s');
    }

    public function getUpdatedAtFormatAttribute(){
			$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at  );
			$date->setTimezone('Asia/Bangkok');
			return $date->format('d/m/Y H:i:s');
    }


}
