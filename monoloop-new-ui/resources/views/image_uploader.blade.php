<!doctype html>
<html ng-app='APP'>

<head>
	<meta name="description" content="condition builder" />

	<!-- <title>Placement Window</title> -->

	<script src="/webix/webix.js?{{Config::get('app.version') }}" type="text/javascript"></script>
	<!-- Integrate jquery with webix -->
	<script src="/js/jquery.js?{{Config::get('app.version') }}" type="text/javascript"></script>


	<script type="text/javascript">
		var monolop_base_url = '{{config('app.url')}}' ;
		var uploadUrl = '{{ $uploadUrl }}';
	</script>
	<!-- <link href="/conditionbuilder/css/bootstrap.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="/webix/webix.css?{{Config::get('app.version') }}" media="all" />
	<link rel="stylesheet" type="text/css" href="/css/webix.css?{{Config::get('app.version') }}" media="all" />
	<!-- Fonts -->

</head>

<body>
	<!-- <div class="container" style="width: inherit;"> -->
	<!-- <div id="test" class="" >sadadassadasdasdsad</div> -->
	<div id="image_uploader" style="width: 400px !important;"></div>
	<!-- </div> -->
<script>
	var uploadedFile = {};
	uploadedFile['status'] = 'error';
	parent.IframeImageUploaderWin = window;
	webix.ready(function() {
		webix.ui({
				container:"image_uploader",
				view:"form", rows: [
					{
						cols: [
							{
								view: "uploader", id:"upl1",
								autosend:false, value: 'Select image',
								multiple: false, accept:"image/png, image/gif, image/jpg, image/jpeg",
								name: "upload",
								link:"mylist",  upload: uploadUrl
							},
							{ view: "button", label: "Upload", click: function() {
								$$("upl1").send(function(response){
									webix.alert("Image uploaded!");
									if(response)
										uploadedFile =response;
								});
							}}
						]
					},
					{
					 	view:"list",  id:"mylist", type:"uploader",
						autoheight:true, borderless:true
					}
				]
			});
	});
</script>
</body>

</html>
