<!doctype html>
<html ng-app='APP'>

<head>
	<meta name="description" content="condition builder" />
<script>
	var version=" <?php echo config('app.version');?>";

</script>
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&{{Config::get('app.version') }}' rel='stylesheet' type='text/css'>
	<!-- conditionbuilder starts -->
	<link href="{{ elixir('conditionbuilder/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ elixir('conditionbuilder/css/styles.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ elixir('webui_popover/jquery.webui-popover.css') }}" media="all" />
</head>

<body>

<div ng-controller="QueryBuilderController">
		<!-- <div class="container"> -->
			<div class="row" style="margin-left:0px;margin-right:0px;">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<h4>Condition parameters</h4>
					<div class="panel-group" id="accordionCondParams" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default" ng-repeat=" (key, value) in conditionParameters track by key">
							<div class="panel-heading" role="tab" id="heading_@{{ $index }}_key">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordionCondParams" href="#_@{{ $index }}_key" aria-expanded="true" aria-controls="collapseOne">
                    @{{ key }}
                  </a>
								</h4>
							</div>
							<div id="_@{{ $index }}_key" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_@{{ $index }}_key">
								<ul class="list-group">
									<li class="list-group-item btn btn-default conditionParameter" ng-repeat="(k, v) in value track by v.key" ng-controller="ConditionParameterController">
										<div ng-model="v" data-drag="true" data-jqyoui-options="{revert: 'invalid'}" jqyoui-draggable="{index: @{{$index}},animate:true, deepCopy: true, placeholder: 'keep', onDrag: 'onConditionParameterDrag(conditionParameter)', onStop: 'onConditionParameterStop'}">@{{v.Label}}</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<h4>Condition Builder </h4>
					<div>@{{ message.success }}</div>
					<div class="row" style="  margin-top: -32px;margin-bottom: 5px;">
						<div class="col-md-8">
						</div>
						<div class="col-md-4 text-right">
							<form method="post"   name="submit_condition_form" id="submit_condition_form" >
								<input type="hidden" ng-model="conditionForm.id" name="id" ng-value="conditionForm.id" />
								<input type="hidden" ng-model="conditionForm.condition" name="condition"   />
								<input type="hidden" ng-model="conditionForm.source" name="source" ng-value="conditionForm.soure" />
								<input type="hidden" ng-model="conditionForm.action" name="action" ng-value="conditionForm.action" />
								<input type="button" class="btn btn-primary" ng-click="submit()" style="padding: 4px 12px; font-size:13px;visibility:hidden;" value="Save" id="save_condition">
							</form>

						</div>
					</div>
					<div class="panel-group cbuilderContainer" id="accordionCbuilderContainer2" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordionCbuilderContainer2" href="#Advanced" aria-expanded="true" aria-controls="collapseOne">
          					Advanced
        					</a>
								</h4>
							</div>
							<div id="Advanced" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body advancedPanel">
									<div class="alert">
										<code ng-bind-html="output" class="output_"></code>
									</div>
								</div>


							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionCbuilderContainer2" href="#conditionbuilderPanel" aria-expanded="false" aria-controls="collapseTwo">
          				Condition Builder
        					</a>
								</h4>
							</div>
							<div id="conditionbuilderPanel" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body conditionbuilderPanel">
									<div class="alert-warning instructions" ng-if="instructions">Drag profile property from left panel <br/> and drop it here to build condition</div>
									<div style="height: auto;" class="alert alert-warning alert-group conditionsList" data-drop="config.mainContainerDrop" ng-model="temp" jqyoui-droppable="{onDrop: 'onConditionDrop($index)', onOver:'onConditionOver', onOut:'onConditionOut'}" jqyoui-droppable="{index: @{{$index}}}">
										<collection collection='group.rules' group='group' config='config' ></collection>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>

			</div>

		<!-- </div> -->
	</div>




<script type="text/ng-template" id="memberInCollection.html">

		<ul class="collectionlist">
			<member ng-repeat='member in collection track by $index' member='member' group='group' config='config' ></member>

		</ul>

	</script>

	<script type="text/ng-template" id="member.html">
		<li>
			<div style="cursor: move;" class="oneNode" data-drag="false" data-jqyoui-options="{revert: 'invalid'}" ng-model="member" jqyoui-draggable="{animate:true, onStart: 'onInnerConditionStart()' , onDrag: 'onInnerConditionDrag', onStop: 'onInnerConditionStop'}">

				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						@{{ member.NodeProperties.linkingLogicalOperator }} @{{ member.Label }} @{{ member.binaryConditionsDefault }} @{{ member.value }}
						<span ng-if="member.extendedListParams.length > 0">
							, [
							<div ng-repeat="v in member.extendedListParams" style=" display: initial;">
								<span ng-if="v.Label == 'Miles:'">

									<span ng-if="v.data == true">
										Miles: true
									</span>
									<span ng-if="v.data != true">
										Km: true
									</span>

								</span>
								<span ng-if="v.Label != 'Miles:'">
									@{{ v.Label }} @{{ v.data }}
								</span>
							</div>]
						</span>
						<button style="" ng-click="removeCondition(member.uid)" class="btn btn-xs btn-danger pull-right">x</button>
					</div>
					<div class="panel-body">
						<div ng-switch="rule.hasOwnProperty('group')">
							<div ng-switch-when="true">
								<query-builder group="rule.group"></query-builder>
							</div>
							<div ng-switch-default="ng-switch-default">
								<div class="form-inline">
									<span ng-if="member.linkingLogicalOperator !== ''">
										<label class="">@{{ member.linkingLogicalOperator }}</label>
									</span>
									<span ng-if="member.Type === 'string' || member.Type === 'integer'">

										<label class="form-control input-sm">@{{ member.Label }}</label>
										<select style="margin-left: 5px" ng-options="c.name as c.name for c in member.binaryConditions" ng-model="member.binaryConditionsDefault" class="form-control input-sm"></select>
										<input style="margin-left: 5px" type="text" ng-model="member.value" class="form-control input-sm @{{ member.Type }}" />
										<span ng-if="member.Type === 'integer'">(numeric)</span>
										<!-- <button style="margin-left: 5px" ng-click="removeCondition(member.uid)" class="btn btn-sm btn-danger">
											x
										</button> -->
									</span>
									<span ng-if="member.Type === 'single remote'">

										<label class="form-control input-sm">@{{ member.Label }}</label>
										<select style="margin-left: 5px" ng-options="c.name as c.name for c in member.binaryConditions" ng-model="member.binaryConditionsDefault" class="form-control input-sm"></select>
										<input style="margin-left: 5px" type="text" ng-model="member.value" class="form-control input-sm @{{ member.key }}" />
										<!-- <button style="margin-left: 5px" ng-click="removeCondition(member.uid)" class="btn btn-sm btn-danger">
											x
										</button> -->
									</span>
									<span ng-if="member.Type === 'single'">

										<label class="form-control input-sm">@{{ member.Label }}</label>
										<select style="margin-left: 5px" ng-options="c.name as c.name for c in member.binaryConditions" ng-model="member.binaryConditionsDefault" class="form-control input-sm"></select>
										<select style="margin-left: 5px" ng-options="c.value as c.label for c in member.listValues" ng-model="member.value" class="form-control input-sm"></select>
										<!-- <button style="margin-left: 5px" ng-click="removeCondition(member.uid)" class="btn btn-sm btn-danger">
											x
										</button> -->
									</span>
									<span ng-if="member.Type === 'single remote2'">
										<label class="form-control input-sm">@{{ member.Label }}</label>
										<select style="margin-left: 5px" ng-options="c.name as c.name for c in member.binaryConditions" ng-model="member.binaryConditionsDefault" class="form-control input-sm"></select>
										<select style="margin-left: 5px" ng-options="c.value as c.label for c in member.listValues" ng-model="member.value" class="form-control input-sm"></select>
										<!-- <button style="margin-left: 5px" ng-click="removeCondition(member.uid)" class="btn btn-sm btn-danger">
											x
										</button> -->
									</span>

									<span ng-if="member.Type === 'bool'">
										<label class="form-control input-sm">@{{ member.Label }}</label>
										<span class="center_cond">@{{ member.binaryConditionsDefault }}</span>
										<input style="margin-left: 5px" type="checkbox" ng-model="member.value" class="" />
										<!-- <button style="margin-left: 5px" ng-click="removeCondition(member.uid)" class="btn btn-sm btn-danger">
											x
										</button> -->
									</span>
									<span ng-if="member.Type === 'function'">
										<span ng-if="1">
											<label class="form-control input-sm">@{{ member.Label }}</label>
											<span ng-if="member.ReturnValue !== 'bool'">
												<select style="margin-left: 5px" ng-options="c.name as c.name for c in member.binaryConditions" ng-model="member.binaryConditionsDefault" class="form-control input-sm"></select>
												<span ng-if="member.ReturnValue === 'integer'">
													<input style="margin-left: 5px" type="text" ng-model="member.value" class="form-control input-sm @{{ member.ReturnValue }}" />
													<span>(numeric)</span>
												</span>
												<span ng-if="member.ReturnValue !== 'integer'">
													<input style="margin-left: 5px" type="text" ng-model="member.value" class="form-control input-sm" />
												</span>
											</span>
											<span ng-if="member.ReturnValue === 'bool'">
												<span class="center_cond">@{{ member.binaryConditionsDefault }}</span>
												<input style="margin-left: 5px" type="checkbox" ng-model="member.value" class="" />
											</span>

											<!-- <button style="margin-left: 5px" ng-click="removeCondition(member.uid)" class="btn btn-sm btn-danger">
												x
											</button> -->

											<div ng-repeat="v in member.extendedListParams" class="condition">
												<span ng-if="v.Label !== ''">
													<label class="form-control input-sm">@{{ v.Label }}</label>
												</span>

												<span ng-if="v.Type === 'integer' || v.Type === 'string'">
													<input style="margin-left: 5px" type="text" ng-model="v.data" class="form-control input-sm @{{ v.Type }}" />
													<span ng-if="member.Type === 'integer'">(numeric)</span>
												</span>
												<span ng-if="v.Type === 'bool'">
													<input style="margin-left: 5px" type="checkbox" ng-model="v.data" class="" />
												</span>
												<span ng-if="v.Type === 'single'">
													<select style="margin-left: 5px;margin-bottom: 5px;" ng-model="v.data" class="form-control input-sm">
														<option ng-if="c.Value === v.data" selected="true" ng-repeat="c in v.Values" value="@{{c.Value}}">@{{c.Label}}</option>
														<option ng-if="c.Value !== v.data" ng-repeat="c in v.Values" value="@{{c.Value}}">@{{c.Label}}</option>
													</select>
												</span>
												<span ng-if="v.Type === 'lookup'">
													<input style="margin-left: 5px" type="text" ng-model="v.data" class="form-control input-sm" />
												</span>
												<span ng-if="v.Type === 'superboxselect'">
													<input style="margin-left: 5px" type="button" value="Select" ng-model="v.data" class="form-control input-sm" />
												</span>
												<span ng-if="v.Type === 'single remote2' || v.Type === 'single remote3'">
													<select style="margin-left: 5px;margin-bottom: 5px;" ng-model="v.data" class="form-control input-sm">
														<option ng-repeat="c in v.Values" value="@{{c.Value}}">@{{c.Label}}</option>
													</select>
												</span>
											</div>

										</span>
									</span>
								</div>
							</div>

							<div  ng-hide="false">
								<div  ng-hide="false" id="or" class="well or condition_ " data-drop="true" ng-model="temp" jqyoui-droppable="{onDrop: 'onConditionDrop($index)', onOver:'onConditionOver', onOut:'onConditionOut'}" jqyoui-droppable="{index: @{{$index}}}">
									or
								</div>
								<div ng-hide="false" id="and" class="well and condition_ " data-drop="true" ng-model="temp" jqyoui-droppable="{onDrop: 'onConditionDrop', onOver:'onConditionOver', onOut:'onConditionOut'}" jqyoui-droppable="{index: @{{$index}}}">
									and
									</div>
							</div>
						</div>
					</div>

				</div>
				<span ng-if="member.rules.length > 0">
					<div ng-hide="config.hideLogicalRelations"  ng-style="{top: member.styles.and.line.top, left: member.styles.and.line.left, height: member.styles.and.line.height, index: 100}"  class="cbuilder-line-and2"></div>
					<div ng-hide="config.hideLogicalRelations" ng-style="{top: member.styles.and.line.top, left: member.styles.and.icon.left}"  class="cbuilder-and2-icon"></div>
				</span>
				<span ng-if="member.styles.or.isEnabled === true">
					<div ng-hide="config.hideLogicalRelations" ng-style="{top: member.styles.or.line.lineUpper.top, left: member.styles.or.line.lineUpper.left, width: member.styles.or.line.lineUpper.width}"  class="cbuilder-line-or"></div>
					<div ng-hide="config.hideLogicalRelations" ng-style="{top: member.styles.or.line.lineLower.top, left: member.styles.or.line.lineLower.left, width: member.styles.or.line.lineLower.width}"  class="cbuilder-line-or"></div>
					<div ng-hide="config.hideLogicalRelations"  ng-style="{top: member.styles.or.line.lineMiddle.top, left: member.styles.or.line.lineMiddle.left, height: member.styles.or.line.lineMiddle.height}" class="cbuilder-line-or"></div>
					<div ng-hide="config.hideLogicalRelations" ng-style="{top: member.styles.or.icon.top, left: member.styles.or.icon.left}"  class="cbuilder-or-icon"></div>
				</span>

			</div>




		</li>

	</script>
	<script src="{{ elixir('build/vendor.js') }}"></script>
	<script src="{{ elixir('build/conditionbuilder.js') }}"></script>
	<script>
		var _id = '<?php echo $_id; ?>';
		var _condition = <?php echo json_encode($_condition); ?>;
		var _source = '<?php echo $_source; ?>';
		var _action = '<?php echo $_action; ?>';
		var monolop_api_base_url = '<?php echo config('app.monoloop_api_base_url'); ?>' ;
		var MONOloop  = {};
		MONOloop.jq = $;
		var ajaxHeaders = {'Content-Type': 'application/x-www-form-urlencoded', 'ML-Token': '<?php echo Session::get('mlToken'); ?>' };
		webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers){
			headers["Content-type"]= ajaxHeaders['Content-Type'];
			headers["ML-Token"]= ajaxHeaders['ML-Token'];
		});
		var profilePropertiesUrl = monolop_api_base_url + '/api/profile-properties';
		var profilePropertiesTemplateUrl = '/api/profile-properties-template';
	</script>
	<script src="/conditionbuilder/js/qb.js"></script>
</body>

</html>
