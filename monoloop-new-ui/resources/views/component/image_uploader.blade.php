<!doctype html>
<html ng-app='APP'>

<head>
	<meta name="description" content="condition builder" />

	<!-- <title>Placement Window</title> -->

	<script src="{{ elixir('webix/webix.js') }}" type="text/javascript"></script>
	<!-- Integrate jquery with webix -->
	<script src="{{ elixir('js/jquery.js') }}" type="text/javascript"></script>


	<script type="text/javascript">
		var monolop_base_url = '{{config('app.url')}}' ;
		var uploadUrl = '{{ $uploadUrl }}';
	</script>
	<!-- <link href="/conditionbuilder/css/bootstrap.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="/webix/webix.css?{{Config::get('app.version') }}" media="all" />		<!-- marc -->
	<link rel="stylesheet" type="text/css" href="{{ elixir('css/webix.css') }}" media="all" />			<!-- marc -->
	<!-- Fonts -->

</head>

<body>
	<!-- <div class="container" style="width: inherit;"> -->
	<!-- <div id="test" class="" >sadadassadasdasdsad</div> -->
	<div id="image_uploader" style="width: 400px !important;">

	</div>
	<!-- </div> -->
<script>
	var uploadedFile = {};
	uploadedFile['status'] = 'error';
	parent.IframeImageUploaderWin = window;
	webix.ready(function() {
		webix.ui({
				container:"image_uploader",
				view:"form", rows: [
					{
						cols: [
							{
								view: "uploader", id:"upl1",
								autosend:false, value: 'Select image',
								multiple: false, accept:"image/png, image/gif, image/jpg, image/jpeg",
								name: "upload",
								link:"mylist",  upload: uploadUrl
							},
							{ view: "button", id: "uploadBtn", label: "Upload", click: function() {
								$$("uploadBtn").disable();
								$$("upl1").send(function(response){
									if(response){
										if(response.status === 'server'){
											webix.alert("Image uploaded!");
											uploadedFile = response;
										}
									} else {
										parent.postMessage(JSON.stringify({
													t: "upload-image-required-message"
												}), "*");

										// webix.message({ type:"error", text:"Please select image to upload" });
									}
									$$("uploadBtn").enable();
								});
							}}
						]
					},
					{
					 	view:"list",  id:"mylist", type:"uploader",
						autoheight:true, borderless:true
					}
				]
			});
	});
</script>
</body>

</html>
