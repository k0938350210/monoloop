<!doctype html>
<html ng-app='APP'>

<head>
	<meta name="description" content="condition builder" />
	<script>
		var version=" <?php echo config('app.version');?>";
		console.log(version);

	</script>
	<!-- <title>Placement Window</title> -->

	<script src="/webix/webix.js" type="text/javascript"></script>
	<!-- Integrate jquery with webix -->
	<script src="/js/jquery.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="/webix/webix.css?version" media="all" />
	<link rel="stylesheet" type="text/css" href="{{ elixir('css/webix.css') }}" media="all" />
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&version' rel='stylesheet' type='text/css'>

</head>

<body>

	<div id="placement_window" style="height:100% !important;width: 100% !important;"></div>

<script src="/js/webix/component/placement_window.js?version" type="text/javascript"></script>
</body>

</html>
