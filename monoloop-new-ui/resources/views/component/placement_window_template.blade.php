<!doctype html>
<html ng-app='APP'>

<head>
	<meta name="description" content="condition builder" />

	<!-- <title>Placement Window</title> -->

	<!-- <meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'> -->
	<link rel="stylesheet" type="text/css" href="{{ elixir('build/app.css') }}" media="all" />
	<script type="text/javascript">
		var editor_bundlejs = '{{ elixir('js/invocation/editor-bundle.js') }}';

		var assets = {
      select_view: '{{ elixir('js/placementwindow/selectView.js') }}',
      webui_popover: '{{ elixir('webui_popover/jquery.webui-popover.js')}}',
      guildelines: '{{ elixir('js/guidelines.js')}}'
    }
	</script>
</head>

<body class="pwBody">
	<!-- <div class="container" style="width: inherit;"> -->
	<!-- <div id="test" class="" >sadadassadasdasdsad</div> -->
	<div id="containerId" class="container" style="height:100% !important;width: 100% !important;">
		<div class="row">
			<div id="placement_window" style="height:100% !important;width: 100% !important;"></div>
		</div>
	</div>
	<!-- </div> -->
	<script src="{{ elixir('build/vendor.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var MONOloop  = {};
		MONOloop.jq = $;
		var monolop_base_url = '{{config('app.url')}}' ;
		var monolop_api_base_url = '{{config('app.monoloop_api_base_url')}}' ;
		var s3_public_url = '{{config('filesystems.disks.s3.public_url')}}' ;
		var activeAccount = {!! json_encode(UserAccount::activeAccount()) !!};

		var ajaxHeaders = {'Content-Type': 'application/x-www-form-urlencoded', 'ML-Token': "{{ Session::get('mlToken') }}" };
		webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers){
			headers["Content-type"]= ajaxHeaders['Content-Type'];
			headers["ML-Token"]= ajaxHeaders['ML-Token'];
		});
		var page = '{{ $page }}';
		var url = '{{ $url }}';
		var source = '{{ $source }}';
		var new_url = '{{ $new_url }}';
	</script>
	<script src="{{ elixir('js/webix/component/placement_window.js') }}" type="text/javascript"></script>

</body>

</html>
