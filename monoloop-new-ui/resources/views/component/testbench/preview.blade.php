<!doctype html>
<html>
<head>
	<meta name="description" content="Monoloop testbench preview" />
	<script src="/webix/webix.js?{{Config::get('app.version') }}" type="text/javascript"></script>
	<script src="/js/jquery.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <script src="/js/placementwindow/testbench_preview.js?{{Config::get('app.version') }}" type="text/javascript"></script>
	<script type="text/javascript">
		webix.ready(function(){
			testbench_preview.extra = '{!! addslashes(json_encode($mongoDetail)) !!}' ;
			testbench_preview.params = '{!! addslashes(json_encode($data)) !!}' ;
			testbench_preview.url = '{{ $url }}' ;
			testbench_preview.urlencode = '{{ urlencode($url) }}' ;
			testbench_preview.baseUrl = '{{config('app.url')}}/' ;
			testbench_preview.baseRelativeUrl = '{{config('app.url')}}/' ;
			testbench_preview.temPath = '' ;
			testbench_preview.init() ;
		});
	</script>
  <style>
    html {overflow: auto;} html, body, div#wrapper , div#wrapper div, iframe {margin: 0px; padding: 0px; height: 100%; border: none;} iframe {display: block; width: 100%; border: none; overflow-y: auto; overflow-x: hidden;}
  </style>
</head>
<body>
  {!! $table !!}
  <iframe id='pw-test-view' name='pw-test-view' src ='' width='100%' height='100%' frameborder='0'  onLoad="testbench_preview.selectTabSrcChange();"><p>Your browser does not support iframes.</p></iframe>
</body>
</html>
