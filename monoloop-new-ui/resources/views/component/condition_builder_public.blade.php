<!doctype html>

<head>
  <meta name="description" content="condition builder" />
  <style type="text/css">
    body{
      margin: 0;
      padding: 0;
    }
    iframe{
      position: absolute;
      overflow: hidden;
      border: none;
    }
  </style>
  <script src="{{ elixir('conditionbuilder/js/jquery.js') }}"></script>
  <script type="text/javascript">
    var iFrameWin;

    if (window.addEventListener) {
      // For standards-compliant web browsers
      window.addEventListener("message", receiveMessage);
    }
    else {
      window.attachEvent("onmessage", receiveMessage);
    }

    function receiveMessage(evt){
      if(evt.data && evt.data.t){
        if(evt.data.t === 'get-condition-str'){
          // console.log(iFrameWin.returnCondition.logical);
          parent.postMessage({t: 'set-condition-str',v: iFrameWin.returnCondition.logical},'*');
        }else if(evt.data.t === 'set-condition-str'){
          var condition = evt.data.v;
          var cb_iframe = $("iframe");
          cb_iframe.get(0).contentWindow.postMessage(condition, "*");
        }
      }
    }

    function iframeLoad(){
      parent.postMessage({t: 'get-condition-str'},'*');
    }
  </script>
</head>
<body>
  <iframe src="/component/profile-properties-template" style="height: 100%;width: 100%;" onload="iframeLoad()"></iframe>
</body>

</html>