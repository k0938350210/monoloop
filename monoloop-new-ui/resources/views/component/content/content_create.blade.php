	@extends('webui_popover_layout')

	@section('content')
	<div id="create_variation_container" style="width: 100%;"></div>
	@endsection

	@section('footerjs')
	<script type="text/javascript">
		var monolop_base_url = '{{config('app.url')}}' ;
		var urlParams = {};
		urlParams.source = '{{ $source }}';
		urlParams.fullURL = '{{ $fullURL }}';
		urlParams.xpath = '{{ $xpath }}';
		urlParams.hashFn = '{{ $hashFn }}';
		urlParams.page_element_id = '{{ $page_element_id }}';
		urlParams.experiment_id = '{{ $experiment_id }}';
		urlParams.totalVariations = '{{ $totalVariations }}'

		urlParams.totalVariations++;
		var __icon3Form = [{
	    // view: "layout",
	    // height: 520,
	    width: 320,
	    rows: [{
	      view: "text",
	      id: "variation_title",
	      label: 'Name',
	      name: "variation_title",
				value: "Variation [" + urlParams.totalVariations + "]"
	      // invalidMessage: "Name can not be empty"
	    },
	    {
	      view: "text",
	      id: "condition",
	      label: 'Condition',
	      name: "condition",
	      hidden: true,
	    },
			{
				cols: [{
						view: "text",
						id: "conditionLabel",
						name: "conditionLabel",
						css: "rightIconTextbox var",
						label: "Condition",
						value: "All visitors.",
						readonly: true,
						bottomPadding: 15,
						click: function(){
							parent.postMessage({
								t: 'open-condition-builder',
								condition: $$("condition").getValue(),
								type: 'variation'
							}, urlParams.fullURL);
						}
							// invalidMessage: "Name can not be empty"
					},{
						view: "icon",
						icon: "users",
						css: "conditionIcon var",
						click: function(){
							parent.postMessage({
								t: 'open-condition-builder',
								condition: $$("condition").getValue(),
								type: 'variation'
							}, urlParams.fullURL);
						}
					}]
			},
			{
	        cols: [
	          {

	          }, {
	            view: "button",
	            value: "New",
							css: "orangeBtn",
	            hidden: true,
	            id: "variation_addnew_btn",
	            width: 60,
	            click: function(evt){
	              variationsModel.manageFormCase("new");
	            }
	          }, {
	            view: "button",
	            value: "Add",
							css: "orangeBtn",
	            id: "variation_add_btn",
	            width: 60,
	            click: function(evt){
	              var form = $$("create_variation_container_form");
	              if(form.validate()){
	                var formValues = form.getValues();

	                var data = {};
	                data.type = "variation";
	                data.variation_name = formValues.variation_title;
	                data.condition = formValues.condition;
	                data.fullURL = urlParams.fullURL;
									data.page_element_id = urlParams.page_element_id;
									data.experiment_id = urlParams.experiment_id;
	                parent.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
	              }
	            }
	          }
	        ]
	    }]
	  }];
	  var __icon3FormRules = {
	    "variation_title": webix.rules.isNotEmpty,
	  };
		webix.ui({
	    container: "create_variation_container",
	    id: "create_variation_container_id",

	    rows: [{
	      cols: [{}, {
	        view: "form",
	        id: "create_variation_container_form",
	        css: "",
	        complexData: true,
	        elements: __icon3Form,
	        rules: __icon3FormRules,
	        borderless: true,
	        margin: 3,
	        elementsConfig: {
	          labelPosition: "top",
	          labelWidth: 140,
	          bottomPadding: 18
	        },
	      }, {}, ]
	    }]
	  });
		var variationsModel = {
			setFormValues: function(content_id, name, condition){

		    content_id = content_id || "";
		    name = name || "";
		    condition = condition || "";
		    $$("create_variation_container_form").setValues({
		      content_id: content_id,
		      variation_title: name,
		      condition: condition
		    });
				if(condition === ''){
					$$("conditionLabel").setValue("All visitors.");
				}

		  },
			addVariation: function(url, data){
				$.ajax({
		       url: url,
		       type: "POST",
		       data: data,
		       beforeSend: function( xhr ) {
						 xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
						 xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
		       }
		     })
		       .done(function( response ) {
		         if (response.status === 'success') {
							 variationsModel.setFormValues('', '', '');
							 urlParams.page_element_id = response.pageElement._id;
							 parent.postMessage({t: "set-current-pageElement", data: {content: response.content, pageElement: response.pageElement, source: response._source, experiment: response.experiment}, variationCreated: true}, data.fullURL);

							 webix.message("Variation has been created!");

		         } else {
							 if(response.status === 'error'){
								 if(typeof response.msg === 'string'){
									 webix.message({type: 'error', text: response.msg});
								 } else {
									 webix.message({type: 'error', text: "An error occoured, please refreh page and try again"});
								 }
							 } else {
								 webix.message({type: 'error', text: "An error occoured, please refreh page and try again"});
							 }
		         }
		       });
			}

		};
	</script>


	<script>

	var __listener = function (e) {
				 var data = JSON.parse(e.data);
				 switch (data.t) {
					case "add-element-content-variation":

						variationsModel.addVariation(
							monolop_api_base_url+"/api/page-element/content-create",
							data.data
						);
						window.removeEventListener(messageEvent, __listener, false);
						break;
					case "set-condition":
						if(data.data.condition){
							data.data.condition = data.data.condition.trim();
						}
						if(data.data.condition && data.data.condition !== 'if (){|}'){
							$$("condition").setValue(data.data.condition);
							$$("conditionLabel").setValue("Condition is available.");
						} else {
							$$("condition").setValue("");
							$$("conditionLabel").setValue("All visitors.");
						}
						break;
					default:
					 	break;
				 }
				 // Do whatever you want to do with the data got from IFrame in Parent form.
	};
	// listen from the child
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];

	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	// Listen to message from child IFrame window
	eventer(messageEvent, __listener, false);
	</script>
	@endsection
