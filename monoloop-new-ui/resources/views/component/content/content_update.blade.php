	@extends('webui_popover_layout')

	@section('content')
	<div id="update_variation_container" style="width: 100%;"></div>
	@endsection

	@section('footerjs')
	<script type="text/javascript">
		var monolop_base_url = '{{config('app.url')}}' ;
		var urlParams = {};
		urlParams.source = '{{ $source }}';
		urlParams.fullURL = '{{ $fullURL }}';
		urlParams.xpath = '{{ $xpath }}';
		urlParams.hashFn = '{{ $hashFn }}';
		urlParams.page_element_id = '{{ $page_element_id }}';
		urlParams.content_id = '{{ $content_id }}';
		urlParams.content_name = '{{ $content_name }}';
		urlParams.content_condition = "{!! $content_condition !!}";

		var editVariationForm = [{
	    // view: "layout",
	    // height: 520,
	    width: 320,
	    rows: [{
	      view: "text",
	      id: "variation_title",
	      label: 'Name',
	      name: "variation_title",
	      // invalidMessage: "Name can not be empty"
	    },
	    {
	      view: "text",
	      id: "condition",
	      label: 'Condition',
	      name: "condition",
	      hidden: true,
	    },
			{
				cols: [{
						view: "text",
						id: "conditionLabel",
						name: "conditionLabel",
						css: "rightIconTextbox var",
						label: "Condition",
						value: "All visitors.",
						readonly: true,
						bottomPadding: 15,
						click: function(){
							parent.postMessage({
								t: 'open-condition-builder',
								condition: $$("condition").getValue(),
								type: 'variation'
							}, urlParams.fullURL);
						}
							// invalidMessage: "Name can not be empty"
					},{
						view: "icon",
						icon: "users",
						css: "conditionIcon var",
						click: function(){
							parent.postMessage({
								t: 'open-condition-builder',
								condition: $$("condition").getValue(),
								type: 'variation'
							}, urlParams.fullURL);
						}
					}]
			},
			{
	        cols: [
	          {

	          }, {

	          }, {
	            view: "button",
	            value: "Update",
							css: "orangeBtn",
	            id: "variation_add_btn",
	            width: 80,
	            click: function(evt){
	              var form = $$("update_variation_container_form");
	              if(form.validate()){
	                var formValues = form.getValues();

	                var data = {};
	                data.type = "variation";
	                data.variation_name = formValues.variation_title;
	                data.condition = formValues.condition;
	                data.fullURL = urlParams.fullURL;
									data.page_element_id = urlParams.page_element_id;
									data.content_id = urlParams.content_id;

	                parent.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
	              }
	            }
	          }
	        ]
	    }]
	  }];
	  var editVariationFormRules = {
	    "variation_title": webix.rules.isNotEmpty,
	  };
		webix.ui({
	    container: "update_variation_container",
	    id: "update_variation_container_id",

	    rows: [{
	      cols: [{}, {
	        view: "form",
	        id: "update_variation_container_form",
	        css: "",
	        complexData: true,
	        elements: editVariationForm,
	        rules: editVariationFormRules,
	        borderless: true,
	        margin: 3,
	        elementsConfig: {
	          labelPosition: "top",
	          labelWidth: 140,
	          bottomPadding: 18
	        },
	      }, {}, ]
	    }]
	  });
		var variationsModel = {
			setFormValues: function(content_id, name, condition){

		    content_id = content_id || "";
		    name = name || "";
		    condition = condition || "";
		    $$("update_variation_container_form").setValues({
		      content_id: content_id,
		      variation_title: name,
		      condition: condition,
					conditionLabel: condition && condition !== 'if (){ | }' ? "Condition is available." : "All visitors.",
		    });

		  },
			updateVariation: function(url, data){
				$.ajax({
		       url: url,
		       type: "POST",
		       data: data,
		       beforeSend: function( xhr ) {
						 xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
						 xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
		       }
		     })
		       .done(function( response ) {
		         if (response.status === 'success') {
							 webix.message("Variation has been updated!");
		         } else {
		           webix.alert("An error occoured, please refreh page and try again");
		         }
		       });
			}

		};
		variationsModel.setFormValues(urlParams.content_id, urlParams.content_name, urlParams.content_condition);
	</script>


	<script>
	// listen from the child
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];

	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	// Listen to message from child IFrame window
	eventer(messageEvent, function (e) {
				 var data = JSON.parse(e.data);
				 switch (data.t) {
					case "update-element-content-variation":

						variationsModel.updateVariation(
							monolop_api_base_url+"/api/page-element/content-update",
							data.data
						);

						break;
					case "set-condition":
						if(data.data.condition){
							data.data.condition = data.data.condition.trim();
						}
						if(data.data.condition && data.data.condition !== 'if (){ | }'){
							$$("condition").setValue(data.data.condition);
							$$("conditionLabel").setValue("Condition is available.");
						} else {
							$$("condition").setValue("");
							$$("conditionLabel").setValue("All visitors.");
						}
						break;
					default:
					 	break;
				 }
				 // Do whatever you want to do with the data got from IFrame in Parent form.
	}, false);
	</script>
	@endsection
