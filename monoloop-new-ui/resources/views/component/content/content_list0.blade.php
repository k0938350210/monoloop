	@extends('webui_popover_layout')

	@section('content')
		<div id="content_list_comp"></div>
		<div id="paging_here"></div>
		<div id="add_to_exp_btn"></div>
		<div id="experiment_container"></div>
	@endsection

	@section('footerjs')
	<script type="text/javascript">
		var monolop_base_url = '{{config('app.url')}}' ;
		var urlParams = {};
		urlParams.fullURL = '{{ $fullURL }}';
		urlParams.xpath = '{{ $xpath }}';
		urlParams.hashFn = '{{ $hashFn }}';
		urlParams.page_element_id = '{{ $page_element_id }}';
	</script>


	<script>
		var titleIdSeperator = '____mp_title_seperator____',
				webixDataSet = [],
				pagedDataSet = [],
				contentListResponse;

		var page = {
			size: 10,
			current: 0,
		}

		var apis = {
			update_status: '/api/component/content-list/update-status',
			delete: '/api/component/content-list/delete'
		}

		function getTitle(name_id){
			return name_id.split(titleIdSeperator)[0];
		}

		function updateHidden(id, hidden){
			webix.ajax().post(apis.update_status, {
				content_id: id,
				hidden: hidden
			}, {
				error: function(text, data, XmlHttpRequest) {
					alert("error");
				},
				success: function(text, data, XmlHttpRequest) {
					var response = JSON.parse(text);
					if (response.status === 'success') {

						var _id = response.content._id,
								_hidden = response.content.hidden;

						var element = $("#updateHidden_" + _id);
						if (_hidden == "0") {
							element.text("active");
							element.removeClass("status1");
							element.addClass("status0");
							element.prop('onclick',null).off('click');
							element.on('click', function(e){
								updateHidden(_id, 1);
							});
						} else if (_hidden == "1") {
							element.text("inactive");
							element.removeClass("status0");
							element.addClass("status1");
							element.prop('onclick',null).off('click');
							element.on('click', function(e){
								updateHidden(_id, 0);
							});
						}


						if(pagedDataSet[page.current].length > 0){
							for (var i = 0; i < pagedDataSet[page.current].length; i++) {
								if(pagedDataSet[page.current][i].id === _id){
									pagedDataSet[page.current][i].hidden = parseInt(_hidden);
								}
							}
						}

						$$("content_list_comp").refresh();
					}
				}
			});
		}


		function deleteVar(id){
			webix.ajax().post(apis.delete, {
				content_id: id,
				deleted: 1
			}, {
				error: function(text, data, XmlHttpRequest) {
					alert("error");
				},
				success: function(text, data, XmlHttpRequest) {
					var response = JSON.parse(text);
					if (response.status === 'success') {
						// init();
						webix.message("Deleted!");

						if(webixDataSet.length > 0){
							for (var i = 0; i < webixDataSet.length; i++) {
								if(webixDataSet[i].id === id){
									webixDataSet.splice(i, 1);
								}
							}
						}
						if(pagedDataSet[page.current].length > 0){
							for (var i = 0; i < pagedDataSet[page.current].length; i++) {
								if(pagedDataSet[page.current][i].id === id){
									pagedDataSet[page.current].splice(i, 1);
								}
							}
						}
						if(pagedDataSet[page.current].length === 0){
							if(page.current !== 0){
								page.current--;
							}
						}
						pagedDataSet = [];
						init();
					}
				}
			});
		}

		function showView(t){
			t = t || 'content';

			switch (t) {
				case 'content':
					$("#content_list_comp").show();
					$("#paging_here").show();
					$("#add_to_exp_btn").show();
					$("#experiment_container").hide();

					showContentList();

					// init();
					break;
				case 'experiment':
					$("#content_list_comp").hide();
					$("#paging_here").hide();
					$("#add_to_exp_btn").hide();
					$("#experiment_container").show();

					showExperiment();
					break;
				default:
					break;

			}
		}

		function showContentList(){
			webixDataSet = [];
			pagedDataSet = [];
			contentListResponse = undefined;

			$("#content_list_comp").html('');
			$("#paging_here").html('');
			$("#add_to_exp_btn").html('');
			$("#experiment_container").html('');
			// showView('content');


			webix.ajax().get('/api/component/content-list', urlParams , function(response, xml, xhr) {
				//response
				response = JSON.parse(response);
				contentListResponse  = response;
				// console.log("response",response);

				if(response.status === "success"){
					var variations = response.variations,
							totalCount = 0;
					variations = variations || [];


					totalCount = variations.length;
					if(totalCount > 0){
						for (var i = 0; i < totalCount; i++) {
							webixDataSet.push({
								id: variations[i].content._id,
								title: variations[i].content.name + titleIdSeperator + variations[i].content._id,
								hidden: variations[i].content.hidden,
								content: variations[i].content.config.content,
								deleted: variations[i].content.deleted,
							});
						}

						var a = webixDataSet;
						while (a.length > 0)
						    pagedDataSet.push(a.splice(0, page.size));

						if(pagedDataSet.length === 0){
							pagedDataSet.push([]);
						}

						webix.ready(function() {

							webix.ui({
									 container:"content_list_comp",
									 id: "content_list_comp",
									//  width: 340,
									 height: 350,
									 scroll:false,
									 view:"grouplist",
									 scheme:{
										 $group:function(obj){
											 return obj.title;
										 },
									 },

									 templateBack: function(obj){
										 var value = getTitle(obj.value);
										 return value;
									 },
									 templateGroup: function(obj){
										 var value = getTitle(obj.value);
										 return value;
									 },
									 templateItem: function(obj){
										 var html = "";
										 var id = obj.id;
										 if(obj.hidden == 1){
											 html = '<a href="javascript:void(0);" id="updateHidden_'+id+'" onclick=\'updateHidden( "' + id + '", 0);\' style="color:black;padding: 1px 7px;" class="updateStatus inactive status status1">inacive</a>&nbsp;&nbsp;&nbsp;  ';
										 } else {
											 html = '<a href="javascript:void(0);" id="updateHidden_'+id+'" onclick=\'updateHidden( "' + id + '", 1);\' style="color:black;padding: 1px 7px;" class="updateStatus active status status0">active</a>&nbsp;&nbsp;&nbsp;';
										 }
										 html += '|&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" id="updateHidden_'+id+'" onclick=\'deleteVar( "' + id + '");\' style="color:black;"><span class="webix_icon fa-times deleteVariation"></span></a>';

										 html += '<p>Preview</p> <hr>';

										 html += '<div style="height: 205px;overflow-y: auto;">' + obj.content + '</div>';

										 return html;
									 },
									 type:{
										 height:"auto"
									 },
									 select:false,

									 data:pagedDataSet[page.current],
								});

							webix.ui({
								container:"paging_here",
								id: "paging_here",
								view: 'pager', master:false, size: page.size, group: 10, count:totalCount,
								template: '{common.pages()}',
								hidden: pagedDataSet.length > 1 ? false : true,
								on: {
										onItemClick: function(pageNo, e, node) {
												page.current = pageNo;
												showView('content');
												// showContentList();
										}
								}
								});

								$('.webix_pager button').each(function(){
									var p = $(this).attr('webix_p_id'),
											c = $(this).attr('class');
									if(c === 'webix_pager_item_selected'){
										$(this).removeClass(c).addClass('webix_pager_item');
									}
									if(p == page.current){
										$(this).removeClass('webix_pager_item').addClass('webix_pager_item_selected');
									}
								});

								webix.ui({
									container:"add_to_exp_btn",
									id: 'add_to_exp_btan',
									rows: [
										{
											cols: [
												{
													view:"button",
											    id:"addToExp",
											    value:"Add to experiment",
													click: function(){
														showView('experiment');

														// showExperiment();
													}
												},{

												}
											]
										}
									]
									});


						});

					}else {

					}
				}


			});
		}

		function showExperiment(){
			$("#content_list_comp").html('');
			$("#paging_here").html('');
			$("#add_to_exp_btn").html('');
			$("#experiment_container").html('');

			// showView('experiment');
			var variationsToCopy = '',
					SEPERATOR_PEID_VID = '____mp_page_element_variation_seperator____',
					totalCount = 0;

			totalCount = contentListResponse.variations.length;
			console.log(contentListResponse.variations);
			if(totalCount > 0){
				for (var i = 0; i < totalCount; i++) {

					variationsToCopy += contentListResponse.variations[i].page_element_id + SEPERATOR_PEID_VID + contentListResponse.variations[i].uid;

					if(i !== (totalCount-1)){
						variationsToCopy += ',';
					}
				}

				var currentExp = '';

				var params = {
					fullURL: contentListResponse.fullURL,
					xpath: contentListResponse.xpath,
					experimentID: currentExp,
					page_element_id: contentListResponse.page_element_id,
					variationsToCopy: variationsToCopy,
					allExperiments: '',
				};

				var exps = [];
				if(contentListResponse.experiments !== undefined){
					for (var i = 0; i < contentListResponse.experiments.length; i++) {
						exps.push({
							id: contentListResponse.experiments[i].page_element_id,
							value: contentListResponse.experiments[i].name,
						});

						if(params.allExperiments.length === 0){
							params.allExperiments += contentListResponse.experiments[i].page_element_id;
						} else {
							params.allExperiments += (',' + contentListResponse.experiments[i].page_element_id);
						}

						if(contentListResponse.variationsBelongToExp.indexOf(contentListResponse.experiments[i].page_element_id) !== -1){
							if(currentExp.length === 0){
								currentExp += contentListResponse.experiments[i].page_element_id;
							} else {
								currentExp += (',' + contentListResponse.experiments[i].page_element_id);

							}
						}

					}
				}



				var formElements = [
					{
						view:"multiselect",
						// label:"Select an experiment to move",
						name: "moveToExperimentID",
						value: currentExp,
						options:exps,
					},{
	            cols: [
											{
							            view: "button",
							            value: "Add",
													click: function(){
														var form = $$("moveToExpForm"),
																url = '/api/component/content-list/move-to-experiment';

														if(form.validate()){
															var formValues = form.getValues();

															params.moveToExperimentID = formValues.moveToExperimentID;


															webix.ajax().post(url, params, {
									              error: function(text, data, XmlHttpRequest) {
									                alert("error");
									              },
									              success: function(response, data, XmlHttpRequest) {
																	console.log(response);
									                var response = JSON.parse(response);
									                if (response.success === true) {

																		showView('content');

									                  webix.alert("Content has been moved!");
									                } else {
																		webix.alert(response.msg);
																	}
									              }
									            });

														}

													}
							      },{
										}
							]
	          }
				];

				webix.ui({
						container: 'experiment_container',
						id: 'experiment_container',
						scroll:false,
						css: 'experiment_container',
						width: 360,
						rows: [
							{
								view: "label",
								label: "Select an experiment to move"
							},
							{
								view:"form",
								id: "moveToExpForm",
								scroll:false,
								elements: formElements
							},{
								cols: [
									{
											view:"button",
											id:"addToExp",
											value:"Back",
											click: function(){
												showView('content');
											}
									},{

									},{

									},{

									},{

									}
								]
							}
						]
				});



			} else {
				$$("addToExp").hide();
			}

		}

		function init (){
			$("#content_list_comp").html('');
			$("#paging_here").html('');
			$("#add_to_exp_btn").html('');
			$("#experiment_container").html('');

			showContentList();

		}

		init();

		// parent.IframeImageUploaderWin = window;

	</script>
	@endsection
