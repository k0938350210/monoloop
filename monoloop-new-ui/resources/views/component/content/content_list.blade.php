	@extends('webui_popover_layout')

	@section('content')
		<div id="content_list_comp"></div>
		<div id="variationGuides"></div>
		<div id="paging_here"></div>
		<div id="preview_next_prev"></div>
		<div id="edit_variatoin_cont"></div>
	@endsection

	@section('footerjs')
	<script type="text/javascript">
		var monolop_base_url = '{{config('app.url')}}' ;
		var urlParams = {};
		urlParams.fullURL = '{{ $fullURL }}';
		urlParams.xpath1 = '{{ $xpath }}';
		urlParams.xpath = urlParams.xpath1;
		urlParams.hashFn = '{{ $hashFn }}';
		urlParams.page_element_id = '{{ $page_element_id }}';
	</script>


	<script>
		var titleIdSeperator = '____mp_title_seperator____',
				webixDataSet = [],
				pagedDataSet = [],
				contentListResponse;

		var page = {
			size: 10,
			current: 0,
		}

		var original = {
			has: false,
			title: '<b>Original</b>',
			content: ''
		};

		var apis = {
			update_status: monolop_api_base_url+'/api/component/content-list/update-status',
			delete: monolop_api_base_url+'/api/component/content-list/delete'
		};

		var currentVariation = false;

		function initializeGuidesAndTips(mode){
		    var _pageId = mode || '';

		    // tooltips

		    guidelines.page.pageId = (_pageId);

		    // guidelines
		    var _guideLinesURl = monolop_api_base_url+'/api/guidelines?page_id=' + guidelines.page.pageId;
		    webix.ajax().get(_guideLinesURl, { }, function(text, xml, xhr){
		      var r = JSON.parse(text);

		      var cg = undefined;
		      if(r.currentGuide){
		        cg = JSON.parse(r.currentGuide);
		        guidelines.startedBefore = true;
		      }
		      guidelines[guidelines.page.pageId] = r.guidelines;
		      if(cg){
		        if(cg.currentGuide){

		          guidelines.page.currentGuide = cg.currentGuide;
		        } else {
		          guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
		        }
		      } else {
		        guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
		      }
		      if(guidelines.page.currentGuide){
		        if(guidelines.startedBefore === false){
		          setTimeout(function(){guidelines.generate(guidelines.page.currentGuide, {});},2000);
		        }else{
		          if($('#toggleGuides').hasClass('fa-toggle-on')){
		            $('#toggleGuides').removeClass('fa-toggle-on');
		            $('#toggleGuides').addClass('fa-toggle-off');
		            $('#toggleGuides').attr('title', 'Show Guides');
		          }
		        }
		      }else{
		        if($('#toggleGuides').hasClass('fa-toggle-on')){
		          $('#toggleGuides').removeClass('fa-toggle-on');
		          $('#toggleGuides').addClass('fa-toggle-off');
		          $('#toggleGuides').attr('title', 'Show Guides');
		        }
		      }
		      guidelines.startedBefore = false;
		    });
		}

		function getTitle(name_id){
			return name_id.split(titleIdSeperator)[0];
		}

		function updateHidden(id, hidden){
			webix.ajax().post(apis.update_status, {
				content_id: id,
				hidden: hidden
			}, {
				error: function(text, data, XmlHttpRequest) {
					alert("error");
				},
				success: function(text, data, XmlHttpRequest) {
					var response = JSON.parse(text);
					if (response.status === 'success') {

						var _id = response.content._id,
								_hidden = response.content.hidden;

						var element = $("#updateHidden_" + _id);
						if (_hidden == "0") {
							element.text("active");
							element.removeClass("status1");
							element.addClass("status0");
							element.prop('onclick',null).off('click');
							element.on('click', function(e){
								updateHidden(_id, 1);
							});
						} else if (_hidden == "1") {
							element.text("inactive");
							element.removeClass("status0");
							element.addClass("status1");
							element.prop('onclick',null).off('click');
							element.on('click', function(e){
								updateHidden(_id, 0);
							});
						}


						if(pagedDataSet[page.current].length > 0){
							for (var i = 0; i < pagedDataSet[page.current].length; i++) {
								if(pagedDataSet[page.current][i].id === _id){
									pagedDataSet[page.current][i].hidden = parseInt(_hidden);
								}
							}
						}

						$$("content_list_comp").refresh();
					}
				}
			});
		}


		function deleteVar(id){
			webix.ajax().post(apis.delete, {
				content_id: id,
				deleted: 1
			}, {
				error: function(text, data, XmlHttpRequest) {
					alert("error");
				},
				success: function(text, data, XmlHttpRequest) {
					var response = JSON.parse(text);
					if (response.status === 'success') {
						// init();
						webix.message("Deleted!");

						if(webixDataSet.length > 0){
							for (var i = 0; i < webixDataSet.length; i++) {
								if(webixDataSet[i].id === id){
									webixDataSet.splice(i, 1);
								}
							}
						}
						if(pagedDataSet[page.current].length > 0){
							for (var i = 0; i < pagedDataSet[page.current].length; i++) {
								if(pagedDataSet[page.current][i].id === id){
									pagedDataSet[page.current].splice(i, 1);
								}
							}
						}
						if(pagedDataSet[page.current].length === 0){
							if(page.current !== 0){
								page.current--;
							}
						}
						pagedDataSet = [];
						init();
					}
				}
			});
		}

		function listProgresser(id){
			webix.ui({
					 container: id,
					 id: id,
					//  width: 340,
					 height: 350,
					 scroll:false,
					 view:"list",
					 data:[],
			});
			//adding progress bar functionality to it
			webix.extend($$(id), webix.ProgressBar);

			$$(id).clearAll();
			$$(id).showProgress({

			});
		}

		function showView(t, id){
			t = t || 'content';

			switch (t) {
				case 'content':
					$("#content_list_comp").show();
					$("#paging_here").show();
					$("#preview_next_prev").show();
					$("#edit_variatoin_cont").hide();


					if(original.has){
						showContentList();
					} else {
						parent.postMessage({
							t: "get-element-content-by-xpath",
							xpath: urlParams.xpath
						}, "*");
					}

					// init();
					break;
				case 'edit_content':
					// $("#content_list_comp").hide();
					// $("#paging_here").hide();
					// $("#edit_variatoin_cont").show();
					postMessageInlineVar(id);
					break;
				default:
					break;

			}
		}

		function openCondition(id){
			var condition = '';
			if(currentVariation !== false){
				condition = currentVariation.condition || '';
			}

			parent.postMessage({
				t: "open-condition-builder",
				content_id: id,
				type: 'content-preview',
				condition: condition
			}, "*");
		}

		function showContentList(){
			webixDataSet = [];
			pagedDataSet = [];
			contentListResponse = undefined;
			currentVariation = false;
			$("#variationGuides").html('');
			$("#content_list_comp").html('');
			$("#paging_here").html('');
			$("#preview_next_prev").html('');
			$("#edit_variatoin_cont").html('');

			listProgresser("content_list_comp");

			webix.ajax().get(monolop_api_base_url+'/api/component/content-list', urlParams , function(response, xml, xhr) {
				response = JSON.parse(response);
				contentListResponse  = response;
				// webix.ui({
				// 	container: "variationGuides",
				//   view: "template",
				// 	height: 30,
				// 	css: "guideIcon",
				//   template: "<div class='guideIconClick'><span class='webix_icon fa-question ' title='Start guidelines for variation options'></span></div>",
				// 	onClick:{
				// 	        "guideIconClick":function(ev, id){
				// 						if(guidelines.open === false){
				// 	              guidelines.startGuideline('placementWindowEditContentListVariation',undefined,'tip');
				// 						}
				// 	        }
				// 				}
				// });

				var stGd = document.createElement('i');
				stGd.title = 'Show Guides';
				stGd.className = "webix_icon fa-comment-o";
				stGd.style.position = 'absolute';
				stGd.style.top = '4%';
				stGd.style.right = '11%';
				stGd.style.fontSize = '1em';


				var stGdToggle = document.createElement('i');
				stGdToggle.id = 'toggleGuides';
				stGdToggle.className = "webix_icon fa-toggle-on";
				stGdToggle.style.position = 'absolute';
				stGdToggle.style.top = '4%';
				stGdToggle.style.right = '4%';
				stGdToggle.style.cursor = 'pointer';
				stGdToggle.style.fontSize = '1em';
				stGdToggle.title = 'Hide Guides';

				stGdToggle.onclick = function(){
				  if($('#toggleGuides').hasClass('fa-toggle-off')){
				    $('#toggleGuides').removeClass('fa-toggle-off');
				    $('#toggleGuides').addClass('fa-toggle-on');
				    $('#toggleGuides').attr('title', 'Hide Guides');
				    // Start guides
				    if(guidelines.open === false) {
				      // this.style.pointerEvents = 'none';
				      guidelines.startGuideline('placementWindowEditContentListVariation',undefined,'tip');
				    }
				  }
				};

				$("#variationGuides").append(stGd, stGdToggle);

				if(response.status == 500){
					webix.alert("An error occoured. Please refresh and try again.");
				}
				if(response.status === "success"){
					var variations = response.variations,
							totalCount = 0;
					variations = variations || [];

					var originalVariationContent = {
						id: "thisistheorignalcontentid",
						title: original.title + titleIdSeperator + "thisistheorignalcontentid",
						hidden: 0,
						content: original.content,
						deleted: 0,
						mappedTo: urlParams.xpath,
						original: true,
					};

					totalCount = variations.length;
					if(totalCount > 0){
						parent.postMessage({
							t: "remove-or-decrease-variation-bubble-from-list",
							fullURL: urlParams.fullURL,
							remove_popup: false,
							xpath: urlParams.xpath,
						}, "*");
					} else {
						parent.postMessage({
							t: "remove-or-decrease-variation-bubble-from-list",
							fullURL: urlParams.fullURL,
							remove_popup: true,
							xpath: urlParams.xpath,
						}, "*");
					}
					if(totalCount > 0){
						for (var i = 0; i < totalCount; i++) {
							webixDataSet.push({
								id: variations[i].content._id,
								title: variations[i].content.name + titleIdSeperator + variations[i].content._id,
								hidden: variations[i].content.hidden,
								content: variations[i].content.config.content,
								condition: variations[i].content.condition,
								deleted: variations[i].content.deleted,
								mappedTo: variations[i].mappedTo,
								original: false,
							});
						}

						var a = webixDataSet;
						while (a.length > 0)
						    pagedDataSet.push(a.splice(0, page.size - 1));

						if(pagedDataSet.length === 0){
							pagedDataSet.push([]);
						}
						$("#content_list_comp").html('');
						pagedDataSet[page.current].unshift(originalVariationContent)
						webix.ready(function() {
							webix.ui({
									 container:"content_list_comp",
									 id: "content_list_comp",
									//  width: 340,
									 height: 350,
									 scroll:false,
									 view:"grouplist",
									 scheme:{
										 $group:function(obj){
											 return obj.title;
										 },
									 },

									 templateBack: function(obj){
										 //$(".guideIcon").hide();
										 $('#variationGuides').hide();
										 var value = getTitle(obj.value);
										 return value;
									 },
									 templateGroup: function(obj){
										 //$(".guideIcon").hide();
										 $('#variationGuides').hide();
										 var value = getTitle(obj.value);
										 return value;
									 },
									 templateItem: function(obj){
										 //$(".guideIcon").show();
										 currentVariation = obj;
										 var html = "",
										 		 id = obj.id,
												 condition = currentVariation.condition;
										 if(obj.original === false){
											 if(obj.hidden == 1){
												 html = '<a href="javascript:void(0);" id="updateHidden_'+id+'" onclick=\'updateHidden( "' + id + '", 0);\' style="color:black;padding: 1px 7px;" class="updateStatus inactive status status1">inactive</a>&nbsp;&nbsp;&nbsp;  ';
											 } else {
												 html = '<a href="javascript:void(0);" id="updateHidden_'+id+'" onclick=\'updateHidden( "' + id + '", 1);\' style="color:black;padding: 1px 7px;" class="updateStatus active status status0">active</a>&nbsp;&nbsp;&nbsp;';
											 }
											 html += '|&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" id="updateVariation_'+id+'" onclick=\'showView("edit_content", "' + id + '");\' style="color:black;" class="updateVariationIcon"><span class="webix_icon fa-edit updateVariation"></span></a>';
											 html += '&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" title="' + condition + '" id="updateVariation_'+id+'" onclick=\'openCondition("' + id + '");\' style="color:black;" class="updateVariationConditionIcon"><span class="webix_icon fa-code updateVariationCondition"></span></a>';
											 html += '&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" id="deleteVariation_'+id+'" onclick=\'deleteVar( "' + id + '");\' style="color:black;" class="deleteVariationIcon"><span class="webix_icon fa-times deleteVariation"></span></a>';
										 }

										 html += '<p class="previewCont">Preview</p> <hr>';

										 html += '<div style="height: 205px;overflow-y: auto;" >' + obj.content + '</div>';

										 $('#variationGuides').delay(1000).show(0);
										//  setTimeout(() => guidelines.startGuideline('placementWindowEditContentListVariation'), 1000)
										setTimeout(() => initializeGuidesAndTips('placementWindowEditContentListVariation'), 1000)

										 return html;
									 },
									 type:{
										 height:"auto"
									 },
									 select:false,

									 data:pagedDataSet[page.current],
							});

							//$(".guideIcon").hide();
							$('#variationGuides').hide();

							webix.ui({
								container:"paging_here",
								id: "paging_here",
								view: 'pager', master:false, size: page.size, group: 9, count:totalCount,
								template: '{common.pages()}',
								hidden: pagedDataSet.length > 1 ? false : true,
								on: {
										onItemClick: function(pageNo, e, node) {
												page.current = pageNo;
												showView('content');
												// showContentList();
										}
								}
							});
							webix.ui({
								container:"preview_next_prev",
								id: "preview_next_prev",
								cols: [{},{
										view: "button", type:"icon", icon:"backward", width: 30, click: function(){
											var arr = pagedDataSet[page.current],
													len = pagedDataSet[page.current].length,
													index = len;

											if(currentVariation === false){
												currentVariation = arr[len -1];
												index = len;
											} else {

												for (var i = 0; i < len; i++) {
													if(arr[i].id === currentVariation.id){
														if(arr[i - 1] !== undefined){
															currentVariation = arr[i - 1];
															index = i;
															break;
														} else {
															currentVariation = arr[len -1];
															index = len;
															break;
														}
													} else {
														index = len;
													}
												}
											}

											$("#content_list_comp .webix_scroll_cont .webix_list_item").css('border', '0px solid');
											$("#content_list_comp .webix_scroll_cont .webix_list_item:nth-child("+index+")").css('border', '1px solid');

											parent.postMessage({
												t: "set-element-content-by-xpath",
												xpath: currentVariation.mappedTo,
												content: currentVariation.content
											}, "*");
										},
									},{
										view: "button", type:"icon", icon:"forward", width: 30, click: function(){
											var arr = pagedDataSet[page.current],
													len = pagedDataSet[page.current].length,
													index = 1;

											if(currentVariation === false){
												currentVariation = arr[0];
												index = 1;
											} else {

												for (var i = 0; i < len; i++) {
													if(arr[i].id === currentVariation.id){
														if(arr[i + 1] !== undefined){
															currentVariation = arr[i + 1];
															index = i+2;
															break;
														} else {
															currentVariation = arr[0];
															index = 1;
															break;
														}
													} else {
														index = i+2;
													}
												}
											}
											$("#content_list_comp .webix_scroll_cont .webix_list_item").css('border', '0px solid');
											$("#content_list_comp .webix_scroll_cont .webix_list_item:nth-child("+index+")").css('border', '1px solid');

											parent.postMessage({
												t: "set-element-content-by-xpath",
												xpath: currentVariation.mappedTo,
												content: currentVariation.content
											}, "*");

										},
									},{}

								]
							});

								$('.webix_pager button').each(function(){
									var p = $(this).attr('webix_p_id'),
											c = $(this).attr('class');
									if(c === 'webix_pager_item_selected'){
										$(this).removeClass(c).addClass('webix_pager_item');
									}
									if(p == page.current){
										$(this).removeClass('webix_pager_item').addClass('webix_pager_item_selected');
									}
								});
						});

					} else {
						// console.log('currentVariationcurrentVariationcurrentVariation',currentVariation)


						$("#content_list_comp").html('');
						$("#paging_here").html('');
							webix.ui({
									 container:"content_list_comp",
									 view:"label",
									 label: "No variation added yet.",

							})
					}
				}else{
					webix.alert("An error occoured. Please refresh and try again.");
				}
			});
		}

		function postMessageInlineVar(id){
			webix.ajax().post(monolop_api_base_url+'/api/page-element/content-show', {content_id: id} , function(response, xml, xhr) {

				//response
				response = JSON.parse(response);
				var content = response.content;
				
				if(response.status === "success"){

					parent.postMessage({
						t: "init-FAB-from-edit-variation",
						content_id: content._id,
						name: content.name,
						condition: content.condition,
						xpath: urlParams.xpath,
						content: content.config.content
					}, "*");
				}

			});
		}

		function init (){
			$("#content_list_comp").html('');
			$("#paging_here").html('');
			$("#preview_next_prev").html('');
			showView('content');

		}

		init();

		// parent.IframeImageUploaderWin = window;

	</script>
	<script>
	$(document).keyup(function(e) {
	     if (e.keyCode == 27) { // escape key maps to keycode `27`
	       guidelines.saveInSession();
	    }
	});

	// listen from the child
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];

	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	// Listen to message from child IFrame window
	eventer(messageEvent, function (e) {
				 var data = JSON.parse(e.data);
				 switch (data.t) {
					case "set-condition":
						console.log("data.data.condition",data.data.condition);

						var condition = data.data.condition;
						if(condition){
							if(currentVariation !== false){
								var content_id = currentVariation.id;
								webix.ajax().post(monolop_api_base_url+'/api/page-element/content-update-condition?ajax=true', {content_id: content_id,condition: condition} , function(iresponse, xml, xhr) {
									iresponse = JSON.parse(iresponse);
									if(iresponse.status === 'success'){
										currentVariation.condition = iresponse.content.condition;
										webix.message("Condition has been updated.");
									} else {
										webix.alert("An eror occoured. Please refresh and try again.");
									}
								});
							}
						}


						break;
					case "load-variation-list":
						original.has = true;
						original.content = data.data.content;
						showContentList();
						break;
					default:
						break;
				 }
				 // Do whatever you want to do with the data got from IFrame in Parent form.
	}, false);
	</script>
	@endsection
