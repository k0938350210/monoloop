@extends('webui_popover_layout')

@section('content')
	<div id="create_new_exp_container"></div>
	<div id="experiment_container"></div>
@endsection

@section('footerjs')
<script type="text/javascript">
	var monolop_base_url = '{{config('app.url')}}' ;
	var urlParams = {};
	urlParams.fullURL = '{{ $fullURL }}';
	urlParams.xpath = '{{ $xpath }}';
	urlParams.hashFn = '{{ $hashFn }}';
	urlParams.page_element_id = '{{ $page_element_id }}';
	urlParams.content_id = '{{ $content_id }}';
	urlParams.current_folder = '{{ $current_folder }}';
	urlParams.page = '{{ $page }}';

	urlParams.bk_segment_id = '{{ $bk_segment_id }}';
	urlParams.bk_goal_id = '{{ $bk_goal_id }}';

	var contentListResponse,
	urlOptions = [
		{id: 'exactMatch', value: 'Exact Match'},
		{id: 'allowParams', value: 'Allow Parameters'},
		{id: 'startsWith', value: 'Starts With'},
	],
	getUrlOption = function(id){
			id = id || 0;
			switch (id) {
				case 0: return "exactMatch"; break;
				case 1: return "allowParams"; break;
				case 2: return "startsWith"; break;
				default: return "exactMatch"; break;
			}
	},
	pageOptions = {
		fullURLWebPageList: urlParams.fullURL,
		includeWWWWebPageElement: 1,
		includeHttpHttpsWebPageElement: 0,
		urlOptionWebPageElement: 0,
		regularExpressionWebPageElement: '',
		remarkWebPageList: '',
	},
	newExpBackup = undefined;

	if(urlParams.page_element_id.trim().length > 0){
		pageOptions.fullURLWebPageList = '{{ $fullURLWebPageList }}';
		pageOptions.includeWWWWebPageElement = '{{ $includeWWWWebPageElement }}';
		pageOptions.includeHttpHttpsWebPageElement = '{{ $includeHttpHttpsWebPageElement }}';
		pageOptions.urlOptionWebPageElement = '{{ $urlOptionWebPageElement }}';
		pageOptions.regularExpressionWebPageElement = '{{ $regularExpressionWebPageElement }}';
		pageOptions.remarkWebPageList = '{{ $remarkWebPageList }}';
	}

	function showView(t){
		t = t || 'content';

		switch (t) {
			case 'content':
				$("#create_new_exp_container").show();
				$("#experiment_container").hide();

				showNewExperiment();

				// init();
				break;
			case 'experiment':
				$("#create_new_exp_container").hide();
				$("#experiment_container").show();

				showExperiment();
				break;
			default:
				break;

		}
	}

	function showNewExperiment(){

		webix.ajax().get("/data/experiments/segment", {}, function(resSegment, xml, xhr) {
			webix.ajax().get("/data/experiments/goal", {}, function(resGoal, xml, xhr) {
				webix.ajax().get("/data/experiments/control-group", {}, function(resCG, xml, xhr) {
					//response segments
					var segments = [{
						'id': 'new',
						'value': '<b>Create new</b>',
					}];
					resSegment = JSON.parse(resSegment);

					for (var i = 0; i < resSegment.segments.length; i++) {
						segments.push({
							'id': resSegment.segments[i]._id,
							'value': '<b>' + resSegment.segments[i].name + '</b>' + ' - ' + resSegment.segments[i].description.substr(0, 30)
						});
					}

					//response goals
					var goals = [{
						'id': 'new',
						'value': '<b>Create new</b>',
					}];
					resGoal = JSON.parse(resGoal);
					for (var i = 0; i < resGoal.goals.length; i++) {
						goals.push({
							'id': resGoal.goals[i]._id,
							'value': '<b>' + resGoal.goals[i].name + '</b>'
						});
					}
					// reponse CG
					resCG = JSON.parse(resCG);
	        var significantActions = [];

	        for (var i = 0; i < resCG.significantActions.length; i++) {
	          significantActions.push({
	            'id': i,
	            'value': '<b>' + resCG.significantActions[i] + '</b>'
	          });
	        }

					function submit(){
						var form = $$("createNewExperimentFormID");
						if(form.validate()){
							var formValues = $$("createNewExperimentFormID").getValues();
							formValues.source = urlParams.current_folder;
							formValues.newPageElement = 1;
							webix.ajax().post(monolop_api_base_url+"/api/experiments/create", formValues, {
			          error: function(text, data, XmlHttpRequest) {
			            alert("error");
			          },
			          success: function(text, data, XmlHttpRequest) {
			            var response = JSON.parse(text);
			            if (response.success === true) {
										webix.message('Experiment created!')
										showView('experiment');
									} else {
										webix.alert('An error occoured, please refresh and try again.');
									}
			          }
			        });
						}
					}

					function back(){
					  $$("createNewExperimentMultiview").back();
					}

					function next(){
					  var parentCell = this.getParentView().getParentView(); //the way you get parent cell object may differ
					    var index = $$("createNewExperimentMultiview").index(parentCell);
					    var next = $$("createNewExperimentMultiview").getChildViews()[index+1]
					    if(next){
									var form = $$("createNewExperimentFormID");
									if(form.validate()){
					        	next.show();
									}
							}
					}
					webix.ui({
							container: 'create_new_exp_container',
							id: 'create_new_exp_container',
							scroll:false,
							css: 'create_new_exp_container',
							width: 340,
							rows: [
								{
									view:"form",
								  id:"createNewExperimentFormID",
								  width: 450,
									elementsConfig: {
	                  labelPosition: "top",
										bottomPadding: 5
	                },
								  elements:[
								    {view:"multiview", id:"createNewExperimentMultiview", cells:[
								      {rows:[
												{
							              view: "text",
							              id: "nameExperiment",
							              label: 'Name',
							              name: "nameExperiment",
							              // readonly: true,
							                // invalidMessage: "Name can not be empty"
							            }, {
							              view: "textarea",
							              id: "descExperiment",
							              label: 'Description',
							              name: "descExperiment",
							              height: 150,
							              // readonly: true,
							                // invalidMessage: "Description can not be empty"
							            }, {
													view: "richselect",
													label: "Please select the segment you wish to target:",
													id: "segmentExperiment",
													name: "segmentExperiment",
													value: "",
													options: segments,
													labelAlign: 'left',
												}, {
													view: "richselect",
													label: "Please select the goal you wish to target:",
													id: "goalExperiment",
													name: "goalExperiment",
													value: "",
													options: goals,
													labelAlign: 'left',
												},
								        {cols:[
								          {view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
								          {view:"button", value:"Next", css: "orangeBtn", click:next},

								        ]}

								      ]},
								      {rows:[
												{
													view: "slider",
													type: "alt",
													label: "Size of the control group (%)",
													value: 50,
													id: "cgSizeExperiment",
													name: "cgSizeExperiment",
													// disabled: true,
													title: webix.template("#value# %"),
													bottomPadding: 10
												},
												{
													view: "text",
													id: "cgDayExperiment",
													label: 'Days visitors stay in control group (days)',
													name: "cgDayExperiment",
													// readonly: true,
													value: 30,
													// invalidMessage: "Name can not be empty"
												}, {
													view: "select",
													label: "Action when significant:",
													id: "significantActionExperiment",
													name: "significantActionExperiment",
													value: significantActions[0].id,
													options: significantActions,
													labelAlign: 'left',
													// readonly: true,
												},

								        {cols:[
													{view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
								          {view:"button", value:"Back", click:back},
								          {view:"button", value:"Next", css: "orangeBtn", click:next}
								        ]}

								      ]},
								      {rows:[
												{
						              view: "text",
						              id: "fullURLWebPageList",
						              label: 'URL',
						              name: "fullURLWebPageList",
						              readonly: true,
													value: pageOptions.fullURLWebPageList,
						              // invalidMessage: "Name can not be empty"
						            }, {
						              cols: [
						                {
						                  view:"checkbox",
						                  id:"includeWWWWebPageElement",
						                  name:"includeWWWWebPageElement",
						                  label:"Include WWW",
						                  // readonly: true,
						                  value: pageOptions.includeWWWWebPageElement
						                }, {
						                  view:"checkbox",
						                  id:"includeHttpHttpsWebPageElement",
						                  name:"includeHttpHttpsWebPageElement",
						                  label:"Include Http / Https",
						                  // readonly: true,
						                  value: pageOptions.includeHttpHttpsWebPageElement
						                }
						              ]
						            },{
						              view: "richselect",
						              label: "URL Options",
						              id: "urlOptionWebPageElement",
						              name: "urlOptionWebPageElement",
						              value: getUrlOption(pageOptions.urlOptionWebPageElement),
						              options: urlOptions,
						              labelAlign: 'left',
						            },{
													view: "text",
													id: "regularExpressionWebPageElement",
													label: 'Regular Expression',
													name: "regularExpressionWebPageElement",
													// readonly: true,
													value: pageOptions.regularExpressionWebPageElement
													// invalidMessage: "Name can not be empty"
												}, {
													view: "text",
													id: "remarkWebPageList",
													label: 'Remark',
													name: "remarkWebPageList",
													// readonly: true,
													value: pageOptions.remarkWebPageList
													// invalidMessage: "Description can not be empty"
												},
								        {cols:[
													{view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
								          {view:"button", value:"Back", click:back },
								          {view:"button", value:"Finish", css: "orangeBtn", click:submit}
								        ]}

								      ]}
								    ]
								    }
								  ],
									rules: {
					          "nameExperiment": webix.rules.isNotEmpty,
					          "cgSizeExperiment": webix.rules.isNumber,
					          "cgDayExperiment": webix.rules.isNumber,
					          "significantActionExperiment": webix.rules.isNotEmpty,
										"fullURLWebPageList": webix.rules.isNotEmpty,
					        }
								}
							]
					});

					// on segment change
					$$("segmentExperiment").attachEvent("onChange", function(newv, oldv) {
						if(newv === 'new'){
							var formValues = $$("createNewExperimentFormID").getValues();
							parent.postMessage({t: 'open-new-segment-from-inlin-exp', data: formValues},urlParams.fullURL)
						}
				  });
					// on goal change
					$$("goalExperiment").attachEvent("onChange", function(newv, oldv) {
						if(newv === 'new'){
							var formValues = $$("createNewExperimentFormID").getValues();
							parent.postMessage({t: 'open-new-goal-from-inlin-exp', data: formValues},urlParams.fullURL)
						}
				  });
					if(newExpBackup){
						$$("createNewExperimentFormID").setValues(newExpBackup);
					}

				});
			});
		});


	}

	function showExperiment(){
		$("#create_new_exp_container").html('');
		$("#experiment_container").html('');

		webix.ajax().get(monolop_api_base_url+'/api/component/content-list', urlParams , function(response, xml, xhr) {
			//response
			response = JSON.parse(response);
			contentListResponse  = response;


					var variationsToCopy = '',
							variationsToCopyCount = 0,
							SEPERATOR_PEID_VID = '____mp_page_element_variation_seperator____',
							totalCount = 0;

					totalCount = contentListResponse.variations.length;
					if(totalCount > 0){
						for (var i = 0; i < totalCount; i++) {
							if(urlParams.content_id){
								if(contentListResponse.variations[i].content._id === urlParams.content_id){
									variationsToCopyCount++;
									variationsToCopy += contentListResponse.variations[i].page_element_id + SEPERATOR_PEID_VID + contentListResponse.variations[i].uid;
									break;
								}
							} else {
								variationsToCopyCount++;
								variationsToCopy += contentListResponse.variations[i].page_element_id + SEPERATOR_PEID_VID + contentListResponse.variations[i].uid;
								if(i !== (totalCount-1)){
									variationsToCopy += ',';
								}
							}


						}

						var currentExp = '';

						var params = {
							fullURL: contentListResponse.fullURL,
							xpath: contentListResponse.xpath,
							experimentID: currentExp,
							page_element_id: contentListResponse.page_element_id,
							variationsToCopy: variationsToCopy,
							allExperiments: '',
						};

						var exps = [];
						if(contentListResponse.experiments !== undefined){
							for (var i = 0; i < contentListResponse.experiments.length; i++) {
								exps.push({
									id: contentListResponse.experiments[i].page_element_id,
									title: contentListResponse.experiments[i].name,
									select: true,
								});

								if(params.allExperiments.length === 0){
									params.allExperiments += contentListResponse.experiments[i].page_element_id;
								} else {
									params.allExperiments += (',' + contentListResponse.experiments[i].page_element_id);
								}

								if(contentListResponse.variationsBelongToExp.indexOf(contentListResponse.experiments[i].page_element_id) !== -1){
									if(currentExp.length === 0){
										currentExp += contentListResponse.experiments[i].page_element_id;
									} else {
										currentExp += (',' + contentListResponse.experiments[i].page_element_id);

									}
								}

							}
						}


						var formElements = [
							{
								view:"list",
								// label:"Select an experiment to move",
								view:"list",
							  // select:true,
							  multiselect:"touch",

								id: "moveToExperimentID",
								height: 250,
								width: 100,
								template:"#title#",
								select: "multiselect",
								data:exps,
								value: currentExp,
								ready:function(){
					        this.select(contentListResponse.variationsBelongToExp);
					      },
								on: {
									onItemClick: function(id){
										var item = this.getItem(id);
									},
								},
							},{
									cols: [
													{
														view: "button",
														value: "Create new",
														hidden: urlParams.page === 'experiment',
														css: "orangeBtn",
														click: function(){
															showView('content');
														}
													},{
															view: "button",
															value: "Save",
															css: "orangeBtn",
															click: function(){
																var form = $$("moveToExpForm"),
																		url = monolop_api_base_url+'/api/component/content-list/move-to-experiment';

																if(form.validate()){
																	var formValues = form.getValues();
																	var selectedIds = $$("moveToExperimentID").getSelectedId().toString();
																	params.moveToExperimentID = selectedIds;


																	webix.ajax().post(url, params, {
																		error: function(text, data, XmlHttpRequest) {
																			alert("error");
																		},
																		success: function(response, data, XmlHttpRequest) {
																			var response = JSON.parse(response);
																			if (response.success === true) {

																				showView('experiment');

																				webix.message("Saved!");

																				parent.postMessage({t: 'variation-added-to-experiments', created: true},urlParams.fullURL)
																			} else {
																				webix.message({type: 'error', text: response.msg});
																			}
																		}
																	});

																}

															}
												},{
												}
									]
								}
						];

						webix.ui({
								container: 'experiment_container',
								id: 'experiment_container',
								scroll:false,
								css: 'experiment_container',
								width: 340,
								rows: [
									{
										view: "label",
										label: "Total number of variations to be saved: <b>"+variationsToCopyCount+"</b>",
										css: "labelVarCount",
									},{
										view: "label",
										label: "Select an experiment to move"
									},
									{
										view:"form",
										id: "moveToExpForm",
										scroll:false,
										elements: formElements
									}
								]
						});



					} else {
						webix.alert('There is no content added on this element.');
					}


		});


	}

	function init (){
		$("#create_new_exp_container").html('');
		$("#experiment_container").html('');

		parent.postMessage({t: 'get-new-experiment-backup'}, urlParams.fullURL);

	}

	init();


	// listen from the child
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];

	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	// Listen to message from child IFrame window
	eventer(messageEvent, function (e) {
				 var data = JSON.parse(e.data);
				 switch (data.t) {
						case "set-experiment-values-from-backup":
							if(!data.newExp){
									showView('experiment');
							} else {
								newExpBackup = data.data;
								showView('content');
							}

							break;
						default:
							break;
				 }
			 });
	// parent.IframeImageUploaderWin = window;

</script>
@endsection
