@extends('webui_popover_layout')

@section('content')
	<div id="segment_container"></div>
@endsection

@section('footerjs')
<script type="text/javascript">
	var monolop_base_url = '{{config('app.url')}}' ;
	var urlParams = {};
	urlParams.fullURL = '{{ $fullURL }}';
	urlParams.current_folder = '{{ $current_folder }}';
	// console.log("urlParams", urlParams);
	var contentListResponse;
	var iFrameWin;
	var isConditionsGeneratedSegment = false;
	var tempConditionSegment = undefined;
	var conditionBuilderUrlSegment = '/component/profile-properties-template';

	var stepIndex = 0;

	function showView(t){
		t = t || 'newSegment';

		switch (t) {
			case 'newSegment':
				$("#segment_container").show();
				showNewSegment();

				// init();
				break;
			default:
				break;

		}
	}

	function showNewSegment(){
		stepIndex = 0;
		$("#segment_container").html('');
		isConditionsGeneratedSegment = false;
		tempConditionSegment = undefined;


		function submit(){
			var form = $$("createNewSegmentFormID");
			if(form.validate()){
				var formValues = $$("createNewSegmentFormID").getValues();
				formValues.source = urlParams.current_folder;
				webix.ajax().post(monolop_api_base_url+"/api/segments/create", formValues, {
		      error: function(text, data, XmlHttpRequest) {
		        alert("error");
		      },
		      success: function(text, data, XmlHttpRequest) {
		        var response = JSON.parse(text);
		        if (response.success === true) {
							webix.message('Segment created!');
							var segment = response.content.segment;

							parent.postMessage({t: "page-level-segment-created", segment_id: segment._id, created: true}, "*");
						} else {
							webix.alert('An error occoured, please refresh and try again.');
						}
		      }
		    });
			}
		}

		function back(){
			stepIndex--;
		  $$("createNewSegmentMultiview").back();
		}

		function next(){
		  var parentCell = this.getParentView().getParentView(); //the way you get parent cell object may differ
		    var index = $$("createNewSegmentMultiview").index(parentCell);
		    var next = $$("createNewSegmentMultiview").getChildViews()[index+1];

		    if(next){
						var form = $$("createNewSegmentFormID");
						if(form.validate()){
							stepIndex++;
		        	next.show();
						}
				}
		}
		webix.ui({
				container: 'segment_container',
				id: 'segment_container',
				scroll:false,
				css: 'segment_container',
				// width: 1060,
				// height: 500,
				width: 340,
				// height: 500,
				rows: [
					{
						view:"form",
					  id:"createNewSegmentFormID",
						elementsConfig: {
	            labelPosition: "top",
							// labelWidth: 100,
							align:"center",
							bottomPadding: 15
	          },
					  elements:[
					    {view:"multiview", id:"createNewSegmentMultiview", cells:[
					      {rows:[
									{
				              view: "text",
				              id: "nameSegment",
				              // label: 'Name',
				              name: "nameSegment",
											label: "Name",
											placeholder: "Name",
				              // readonly: true,
				                // invalidMessage: "Name can not be empty"
				            }, {
				              view: "textarea",
				              id: "descSegment",
				              // label: 'Description',
				              name: "descSegment",
											height: 160,
											label: "Description",
											placeholder: "Write description here...",
				              // readonly: true,
				                // invalidMessage: "Description can not be empty"
				            },{
											view: "text",
											id: "conditionSegment",
											name: "conditionSegment",
											value: iFrameWin ? iFrameWin.returnCondition.logical  : "",
											hidden: true,
										},{
											cols: [
												{
							              view: "text",
							              id: "conditionLabel",
							              name: "conditionLabel",
														css: "rightIconTextbox var",
														label: "Condition",
														value: "All visitors.",
							              readonly: true,
														click: function(){
															parent.postMessage({
																t: 'open-condition-builder',
																condition: $$("conditionSegment").getValue(),
																type: 'segment'
															}, urlParams.fullURL);
														}
							                // invalidMessage: "Name can not be empty"
							            },{
														view: "icon",
														icon: "users",
														css: "conditionIcon var",
														click: function(){
															parent.postMessage({
																t: 'open-condition-builder',
																condition: $$("conditionSegment").getValue(),
																type: 'segment'
															}, urlParams.fullURL);
														}
													}
											],
										}, {

										}, {

										}, {

										}, {

										}, {

										},
					        {cols:[
					          {view: "button",value: "Reset",type: "danger",click: function(){showView('newSegment');}},
					          {view:"button", value:"Add", css: "orangeBtn", click:submit},

					        ]}

					      ]},
					    ]
					    }
					  ],
						rules: {
		          "nameSegment": webix.rules.isNotEmpty,
							"conditionSegment": webix.rules.isNotEmpty,
		        }
					}
				]
		});

	}



	function init (){
		$("#segment_container").html('');

		showNewSegment();

	}

	init();

	// listen from the child
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];

	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	// Listen to message from child IFrame window
	eventer(messageEvent, function (e) {
				 var data = JSON.parse(e.data);
				 switch (data.t) {
					case "set-condition":
						if(data.data.condition){
							data.data.condition = data.data.condition.trim();
						}
						if(data.data.condition && data.data.condition !== 'if (){|}'){
							$$("conditionSegment").setValue(data.data.condition);
							$$("conditionLabel").setValue("Condition is available.");
						} else {
							$$("conditionSegment").setValue("");
							$$("conditionLabel").setValue("All visitors.");
						}
						break;
					default:
						break;
				 }
				 // Do whatever you want to do with the data got from IFrame in Parent form.
	}, false);

</script>
@endsection
