@extends('webui_popover_layout')

@section('content')
	<!-- <div id="back"></div> -->
	<div id="variationGuides"></div>
	<div id="content_list_comp" style="padding: 22px 0px 5px 0px;"></div>
	<div id="content_options"></div>
	<div id="add_variation"></div>
	<div id="update_variation"></div>

	<div id="create_new_exp_container"></div>
	<div id="experiment_container"></div>

@endsection

@section('footerjs')
<script type="text/javascript">
	var monolop_base_url = '{{config('app.url')}}' ;
	var urlParams = {};
	urlParams.fullURL = '{{ $fullURL }}';
	urlParams.xpath = '{{ $xpath }}';
	urlParams.xpath1 = '{{ $xpath1 }}';
	urlParams.hashFn = '{{ $hashFn }}';
	urlParams.page_element_id = '{{ $page_element_id }}';
	urlParams.current_folder = '{{ $current_folder }}';
	urlParams.source = '{{ $source }}';
	urlParams.experiment_id = '{{ $experiment_id }}';
	urlParams.page = '{{ $page }}';
	urlParams.totalVariations = '{{ $totalVariations }}';
	urlParams.totalVariations++;

	urlParams.havVar = '{{ $havVar }}';

	urlParams.content_id = '{{ $content_id }}';
	urlParams.content_name = '{{ $content_name }}';
	urlParams.content_condition = "{!! $content_condition !!}";

	urlParams.bk_segment_id = '{{ $bk_segment_id }}';
	urlParams.bk_goal_id = '{{ $bk_goal_id }}';
	urlParams.tagName = '{{ $tagName }}';

	// console.log(urlParams);
</script>


<script>
var conditionBuilderValue = false;
var conditionBuilderLabel = false;
function initializeGuidesAndTips(mode){
		var _pageId = mode || '';

		// tooltips

		guidelines.page.pageId = (_pageId);

											parent.postMessage({t:"content-menu-guide-Ready"},"*");//marc added

		// guidelines
											/* remove
		var _guideLinesURl = monolop_api_base_url+'/api/guidelines?page_id=' + guidelines.page.pageId;
		webix.ajax().get(_guideLinesURl, { }, function(text, xml, xhr){
			var r = JSON.parse(text);

			var cg = undefined;
			if(r.currentGuide){
				cg = JSON.parse(r.currentGuide);
				guidelines.startedBefore = true;
			}
			guidelines[guidelines.page.pageId] = r.guidelines;
			console.log(guidelines);
			if(cg){
				if(cg.currentGuide){

					guidelines.page.currentGuide = cg.currentGuide;
				} else {
					guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
				}
			} else {
				guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
			}
			if(guidelines.page.currentGuide){
				if(guidelines.startedBefore === false){
					setTimeout(function(){guidelines.generate(guidelines.page.currentGuide, {width: 300});},2000);
				}else{
					if($('#toggleGuides').hasClass('fa-toggle-on')){
						$('#toggleGuides').removeClass('fa-toggle-on');
						$('#toggleGuides').addClass('fa-toggle-off');
						$('#toggleGuides').attr('title', 'Show Guides');
					}
				}
			}else{
				if($('#toggleGuides').hasClass('fa-toggle-on')){
					$('#toggleGuides').removeClass('fa-toggle-on');
					$('#toggleGuides').addClass('fa-toggle-off');
					$('#toggleGuides').attr('title', 'Show Guides');
				}
			}
			guidelines.startedBefore = false;
		});
											*/
}

var contentListResponse,
urlOptions = [
	{id: 'exactMatch', value: 'Exact Match'},
	{id: 'allowParams', value: 'Allow Parameters'},
	{id: 'startsWith', value: 'Starts With'},
],
getUrlOption = function(id){
		id = id || 0;
		switch (id) {
			case 0: return "exactMatch"; break;
			case 1: return "allowParams"; break;
			case 2: return "startsWith"; break;
			default: return "exactMatch"; break;
		}
},
pageOptions = {
	fullURLWebPageList: urlParams.fullURL,
	includeWWWWebPageElement: 1,
	includeHttpHttpsWebPageElement: 0,
	urlOptionWebPageElement: 0,
	regularExpressionWebPageElement: '',
	remarkWebPageList: '',
},
newExpBackup = undefined;

if(urlParams.page_element_id.trim().length > 0){
	pageOptions.fullURLWebPageList = '{{ $fullURLWebPageList }}';
	pageOptions.includeWWWWebPageElement = '{{ $includeWWWWebPageElement }}';
	pageOptions.includeHttpHttpsWebPageElement = '{{ $includeHttpHttpsWebPageElement }}';
	pageOptions.urlOptionWebPageElement = '{{ $urlOptionWebPageElement }}';
	pageOptions.regularExpressionWebPageElement = '{{ $regularExpressionWebPageElement }}';
	pageOptions.remarkWebPageList = '{{ $remarkWebPageList }}';
}

var variationsModel = {
	setFormValues: function(content_id, name, condition, t){

		content_id = content_id || "";
		name = name || "";
		condition = condition || "";

		if(t === 'update'){
			$$("update_variation_container_form").setValues({
				content_id: content_id,
				variation_title: name,
				condition: condition,
				conditionLabel: condition && condition !== 'if (){ | }' ? "Condition is available." : "All visitors.",
			});
		} else {
			$$("add_variation_form").setValues({
				content_id: content_id,
				variation_title: name,
				condition: condition
			});
		}

		if(condition === ''){
			$$("conditionLabel").setValue("All visitors.");
		}

	},
	addVariation: function(url, data){
		$.ajax({
			 url: url,
			 type: "POST",
			 data: data,
			 beforeSend: function( xhr ) {
				 xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
				 xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
			 }
		 })
			 .done(function( response ) {
				 if (response.status === 'success') {
					 urlParams.havVar = 1;
					 urlParams.totalVariations++;
					 if(data.hide !== true){
						variationsModel.setFormValues('', "Variation [" + urlParams.totalVariations + "]", '');
					 }
					 urlParams.page_element_id = response.pageElement._id;
					 parent.postMessage({t: "set-current-pageElement", data: {content: response.content, pageElement: response.pageElement, source: response._source, experiment: response.experiment}, variationCreated: true}, data.fullURL);
					 webix.message("Variation has been created!");
					 parent.postMessage({t:"hide-iframe",message:"variation has been created"},"*");



				 } else {
					 if(response.status === 'error'){
						 if(typeof response.msg === 'string'){
							 webix.message({type: 'error', text: response.msg});
						 } else {
							 webix.message({type: 'error', text: "An error occoured, please refreh page and try again"});
						 }
					 } else {
						 webix.message({type: 'error', text: "An error occoured, please refreh page and try again"});
					 }
				 }
			 });
	},
	updateVariation: function(url, data){
		$.ajax({
			 url: url,
			 type: "POST",
			 data: data,
			 beforeSend: function( xhr ) {
				 xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
				 xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
			 }
		 })
			 .done(function( response ) {
				 if (response.status === 'success') {
					 webix.message("Variation has been updated!");
					 parent.postMessage({t:"hide-iframe",message:"variation has been updated"},"*");

				 } else {
					 webix.alert("An error occoured, please refreh page and try again");
				 }
			 });
	}

};

function showView(t, id){
	t = t || 'content_options';

	switch (t) {
		case 'content_options':
			// $("#back").hide();
			$("#add_variation").hide();
			$("#content_options").show();

			$("#add_variation").empty();
			$("#add_to_experiment").empty();
			$("#experiment_container").empty();
			$("#create_new_exp_container").empty();
			$("#update_variation").empty();

			content_options();
			break;
		case 'add_variation':
			// $("#back").show();
			// toolbar();
			$("#content_options").hide();
			$("#update_variation").hide();
			$("#add_variation").show();
			add_variation();
			break;
		case 'update_variation':
			// $("#back").show();
			// toolbar();
			$("#content_options").hide();
			$("#add_variation").hide();
			$("#update_variation").show();
			update_variation();
			break;
		case 'content':
			$("#content_options").hide();
			$("#add_variation").hide();
			$("#update_variation").hide();

			$("#experiment_container").hide();
			$("#create_new_exp_container").show();

			showNewExperiment();

			// init();
			break;
		case 'experiment':
			$("#content_options").hide();
			$("#add_variation").hide();
			$("#update_variation").hide();

			$("#create_new_exp_container").hide();
			$("#experiment_container").show();

			showExperiment();
			break;
		case 'edit_content':
			postMessageInlineVar(id);
			$("#content_list_comp").hide();
			$("#update_variation").show();
			update_variation(id);
			break;
		default:
			break;

	}
}
function toolbar(){
	$("#back").empty();
	webix.ui({
		view:"toolbar",
		id:"myToolbar",
		cols:[
				{ view:"button", value:"back", width:100, align:"right" }]
	});
}


function add_variation(){
	$("#add_variation").empty();


	var __icon3Form = [{
		// view: "layout",
		// height: 520,
		width: 300,
		rows: [{
			view:"label",
			label: "<b>Add variation</b>",
			align:"left"
		},{
			view: "text",
			id: "variation_title",
			label: 'Name',
			name: "variation_title",
			css: "add_variation_title",
			value: "Variation [" + urlParams.totalVariations + "]"
			// invalidMessage: "Name can not be empty"
		},
		{
			view: "text",
			id: "condition",
			label: 'Condition',
			name: "condition",
			hidden: true,
		},
		{
			cols: [{
					view: "text",
					id: "conditionLabel",
					name: "conditionLabel",
					css: "rightIconTextbox var addConditionLabel",
					label: "Condition",
					value: "All visitors.",
					readonly: true,
					bottomPadding: 15,
					click: function(){
						parent.postMessage({
							t: 'open-condition-builder',
							condition: $$("condition").getValue(),
							type: 'variation'
						}, urlParams.fullURL);
					}
						// invalidMessage: "Name can not be empty"
				},{
					view: "icon",
					icon: "users",
					css: "conditionIcon var",
					click: function(){
						parent.postMessage({
							t: 'open-condition-builder',
							condition: $$("condition").getValue(),
							type: 'variation'
						}, urlParams.fullURL);
					}
				}]
		},
		{
				cols: [
					{

					}, {
						view: "button",
						value: "Back",
						// css: "orangeBtn",
						// hidden: true,
						id: "go_back",
						width: 60,
						click: function(evt){
							showView('content_options');
						}
					}, {
						view: "button",
						value: "Add",
						css: "orangeBtn addSaveBtn",
						id: "variation_add_btn",
						width: 60,
						click: function(evt){
							var form = $$("add_variation_form");
							if(form.validate()){
								var formValues = form.getValues();

								var data = {};
								data.type = "variation";
								data.variation_name = formValues.variation_title;
								data.condition = formValues.condition;
								data.fullURL = urlParams.fullURL;
								data.page_element_id = urlParams.page_element_id;
								data.experiment_id = urlParams.experiment_id;
								parent.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
							}
						}
					}
				]
		}]
	}];
	var __icon3FormRules = {
		"variation_title": webix.rules.isNotEmpty,
	};
	webix.ui({
		container: "add_variation",
		id: "add_variation_id",

		rows: [{
			cols: [{}, {
				view: "form",
				id: "add_variation_form",
				css: "",
				complexData: true,
				elements: __icon3Form,
				rules: __icon3FormRules,
				borderless: true,
				margin: 3,
				elementsConfig: {
					labelPosition: "top",
					labelWidth: 140,
					bottomPadding: 18
				},
			}, {}, ]
		}]
	});
	// guidelines.startGuideline('placementWindowEditContentOptionsAddVar', {width: 300});
	//startGuidesIcon('placementWindowEditContentOptionsAddVar'); marc remove
	if(conditionBuilderValue != false || conditionBuilderLabel != false){
		$$("condition").setValue(conditionBuilderValue);
		$$("conditionLabel").setValue(conditionBuilderLabel);

		conditionBuilderValue = false;
		conditionBuilderLabel = false;
	}

	initializeGuidesAndTips('placementWindowEditContentOptionsAddVar');
}

function update_variation(id){
	$("#update_variation").empty();


	var __icon3Form = [{
		// view: "layout",
		// height: 520,
		width: 300,
		rows: [{
			view:"label",
			label: "<b>Update variation</b>",
			align:"left"
		},{
			view: "text",
			id: "variation_title",
			label: 'Name',
			name: "variation_title",
			css: "update_variation_title",
			// value: "Variation [" + urlParams.totalVariations + "]"
			// invalidMessage: "Name can not be empty"
		},
		{
			view: "text",
			id: "condition",
			label: 'Condition',
			name: "condition",
			hidden: true,
		},
		{
			cols: [{
					view: "text",
					id: "conditionLabel",
					name: "conditionLabel",
					css: "rightIconTextbox var updateConditionLabel",
					label: "Condition",
					value: "All visitors.",
					readonly: true,
					bottomPadding: 15,
					click: function(){
						parent.postMessage({
							t: 'open-condition-builder',
							condition: $$("condition").getValue(),
							type: 'null'
						}, urlParams.fullURL);
					}
						// invalidMessage: "Name can not be empty"
				},{
					view: "icon",
					icon: "users",
					css: "conditionIcon var",
					click: function(){
						parent.postMessage({
							t: 'open-condition-builder',
							condition: $$("condition").getValue(),
							type: 'null'
						}, urlParams.fullURL);
					}
				}]
		},
		{
				cols: [
					{

					}, {
						view: "button",
						value: "Back",
						// css: "orangeBtn",
						// hidden: true,
						id: "go_back",
						width: 60,
						click: function(evt){
							$('#content_list_comp').show();
							showView('content_options');
						}
					}, {
						view: "button",
						value: "Update",
						css: "orangeBtn saveBtnEdit",
						id: "variation_add_btn",
						width: 60,
						click: function(evt){
							var form = $$("update_variation_container_form");
							if(form.validate()){
								var formValues = form.getValues();
								var data = {};
								data.type = "variation";
								data.variation_name = formValues.variation_title;
								data.condition = formValues.condition;
								data.fullURL = urlParams.fullURL;
								data.xpath = urlParams.xpath;
								data.page_element_id = urlParams.page_element_id;
								data.content_id = id;
								parent.postMessage({t: "saveChangedContent-Variation", data: data}, data.fullURL);
							}
						}
					}
				]
		}]
	}];
	var __icon3FormRules = {
		"variation_title": webix.rules.isNotEmpty,
	};
	webix.ui({
		container: "update_variation",
		id: "update_variation_id",

		rows: [{
			cols: [{}, {
				view: "form",
				id: "update_variation_container_form",
				css: "",
				complexData: true,
				elements: __icon3Form,
				rules: __icon3FormRules,
				borderless: true,
				margin: 3,
				elementsConfig: {
					labelPosition: "top",
					labelWidth: 140,
					bottomPadding: 18
				},
			}, {}, ]
		}]
	});
	// getting data of Variation
	webix.ajax().post(monolop_api_base_url+'/api/page-element/content-show', {content_id: id} , function(response, xml, xhr) {
		//response
		response = JSON.parse(response);
		// data_Cancelupdate = response;
		var content = response.content;
		if(response.status === "success"){
			variationsModel.setFormValues(content._id, content.name, content.condition,'update');
		}
	});
	// guidelines.startGuideline('placementWindowEditContentOptions', {width: 300});
	//startGuidesIcon('placementWindowEditContentOptions');  marc remove
	// initializeGuidesAndTips('placementWindowEditContentOptions');
}


	function showNewExperiment(){

		webix.ajax().get(monolop_api_base_url+"/data/experiments/segment", {}, function(resSegment, xml, xhr) {
			webix.ajax().get(monolop_api_base_url+"/data/experiments/goal", {}, function(resGoal, xml, xhr) {
				webix.ajax().get(monolop_api_base_url+"/data/experiments/control-group", {}, function(resCG, xml, xhr) {
					//response segments
					var segments = [{
						'id': 'new',
						'value': '<b>Create new</b>',
					}];
					resSegment = JSON.parse(resSegment);

					for (var i = 0; i < resSegment.segments.length; i++) {
						segments.push({
							'id': resSegment.segments[i]._id,
							'value': '<b>' + resSegment.segments[i].name + '</b>' + ' - ' + resSegment.segments[i].description.substr(0, 30)
						});
					}

					//response goals
					var goals = [{
						'id': 'new',
						'value': '<b>Create new</b>',
					}];
					resGoal = JSON.parse(resGoal);
					for (var i = 0; i < resGoal.goals.length; i++) {
						goals.push({
							'id': resGoal.goals[i]._id,
							'value': '<b>' + resGoal.goals[i].name + '</b>'
						});
					}
					// reponse CG
					resCG = JSON.parse(resCG);
					var significantActions = [];

					for (var i = 0; i < resCG.significantActions.length; i++) {
						significantActions.push({
							'id': i,
							'value': '<b>' + resCG.significantActions[i] + '</b>'
						});
					}

					function submit(){
						var form = $$("createNewExperimentFormID");
						if(form.validate()){
							var formValues = $$("createNewExperimentFormID").getValues();
							formValues.source = urlParams.current_folder;
							formValues.newPageElement = 1;
							webix.ajax().post(monolop_api_base_url+"/api/experiments/create", formValues, {
								error: function(text, data, XmlHttpRequest) {
									alert("error");
								},
								success: function(text, data, XmlHttpRequest) {
									var response = JSON.parse(text);
									if (response.success === true) {
										webix.message('Experience created!')
										showView('experiment');
										parent.postMessage({t:"hide-iframe",message:"Experience created!"},"*");
									} else {
										webix.alert('An error occoured, please refresh and try again.');
									}
								}
							});
						}
					}

					function back(){
						$$("createNewExperimentMultiview").back();
					}

					function next(){
						var parentCell = this.getParentView().getParentView(); //the way you get parent cell object may differ
							var index = $$("createNewExperimentMultiview").index(parentCell);
							var next = $$("createNewExperimentMultiview").getChildViews()[index+1]
							if(next){
									var form = $$("createNewExperimentFormID");
									if(form.validate()){
										next.show();
									}
							}
					}
					webix.ui({
							container: 'create_new_exp_container',
							id: 'create_new_exp_container',
							scroll:false,
							css: 'create_new_exp_container',
							width: 300,
							rows: [
								{
									view:"form",
									id:"createNewExperimentFormID",
									width: 450,
									elementsConfig: {
										labelPosition: "top",
										bottomPadding: 5
									},
									elements:[
										{view:"multiview", id:"createNewExperimentMultiview", cells:[
											{rows:[
												{
														view: "text",
														id: "nameExperiment",
														label: 'Name',
														name: "nameExperiment",
														// readonly: true,
															// invalidMessage: "Name can not be empty"
													}, {
														view: "textarea",
														id: "descExperiment",
														label: 'Description',
														name: "descExperiment",
														height: 150,
														// readonly: true,
															// invalidMessage: "Description can not be empty"
													}, {
													view: "richselect",
													label: "Please select the audience you wish to target:",
													id: "segmentExperiment",
													name: "segmentExperiment",
													value: "",
													options: segments,
													labelAlign: 'left',
												}, {
													view: "richselect",
													label: "Please select the goal you wish to target:",
													id: "goalExperiment",
													name: "goalExperiment",
													value: "",
													options: goals,
													labelAlign: 'left',
												},
												{cols:[
													{view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
													{view:"button", value:"Next", css: "orangeBtn", click:next},

												]}

											]},
											{rows:[
												{
													view: "slider",
													type: "alt",
													label: "Size of the control group (%)",
													value: 50,
													id: "cgSizeExperiment",
													name: "cgSizeExperiment",
													// disabled: true,
													title: webix.template("#value# %"),
													bottomPadding: 10
												},
												{
													view: "text",
													id: "cgDayExperiment",
													label: 'Days visitors stay in control group (days)',
													name: "cgDayExperiment",
													// readonly: true,
													value: 30,
													// invalidMessage: "Name can not be empty"
												}, {
													view: "select",
													label: "Action when significant:",
													id: "significantActionExperiment",
													name: "significantActionExperiment",
													value: significantActions[0].id,
													options: significantActions,
													labelAlign: 'left',
													// readonly: true,
												},

												{cols:[
													{view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
													{view:"button", value:"Back", click:back},
													{view:"button", value:"Next", css: "orangeBtn", click:next}
												]}

											]},
											{rows:[
												{
													view: "text",
													id: "fullURLWebPageList",
													label: 'URL',
													name: "fullURLWebPageList",
													readonly: true,
													value: pageOptions.fullURLWebPageList,
													// invalidMessage: "Name can not be empty"
												}, {
													cols: [
														{
															view:"checkbox",
															id:"includeWWWWebPageElement",
															name:"includeWWWWebPageElement",
															label:"Include WWW",
															// readonly: true,
															value: pageOptions.includeWWWWebPageElement
														}, {
															view:"checkbox",
															id:"includeHttpHttpsWebPageElement",
															name:"includeHttpHttpsWebPageElement",
															label:"Include Http / Https",
															// readonly: true,
															value: pageOptions.includeHttpHttpsWebPageElement
														}
													]
												},{
													view: "richselect",
													label: "URL Options",
													id: "urlOptionWebPageElement",
													name: "urlOptionWebPageElement",
													value: getUrlOption(pageOptions.urlOptionWebPageElement),
													options: urlOptions,
													labelAlign: 'left',
												},{
													view: "text",
													id: "regularExpressionWebPageElement",
													label: 'Regular Expression',
													name: "regularExpressionWebPageElement",
													// readonly: true,
													value: pageOptions.regularExpressionWebPageElement
													// invalidMessage: "Name can not be empty"
												}, {
													view: "text",
													id: "remarkWebPageList",
													label: 'Remark',
													name: "remarkWebPageList",
													// readonly: true,
													value: pageOptions.remarkWebPageList
													// invalidMessage: "Description can not be empty"
												},
												{cols:[
													{view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
													{view:"button", value:"Back", click:back },
													{view:"button", value:"Finish", css: "orangeBtn", click:submit}
												]}

											]}
										]
										}
									],
									rules: {
										"nameExperiment": webix.rules.isNotEmpty,
										"cgSizeExperiment": webix.rules.isNumber,
										"cgDayExperiment": webix.rules.isNumber,
										"significantActionExperiment": webix.rules.isNotEmpty,
										"fullURLWebPageList": webix.rules.isNotEmpty,
									}
								}
							]
					});

					// on segment change
					$$("segmentExperiment").attachEvent("onChange", function(newv, oldv) {
						if(newv === 'new'){
							var formValues = $$("createNewExperimentFormID").getValues();
							parent.postMessage({t: 'open-new-segment-from-inlin-exp', data: formValues},urlParams.fullURL)
						}
					});
					// on goal change
					$$("goalExperiment").attachEvent("onChange", function(newv, oldv) {
						if(newv === 'new'){
							var formValues = $$("createNewExperimentFormID").getValues();
							parent.postMessage({t: 'open-new-goal-from-inlin-exp', data: formValues},urlParams.fullURL)
						}
					});
					if(newExpBackup){
						$$("createNewExperimentFormID").setValues(newExpBackup);
					}

				});
			});
		});


	}

	function showExperiment(){
		$("#create_new_exp_container").html('');
		$("#experiment_container").html('');

		webix.ajax().get(monolop_api_base_url+'/api/component/content-list', urlParams , function(response, xml, xhr) {
			//response
			response = JSON.parse(response);
			// console.log("response",response);
			contentListResponse  = response;


					var variationsToCopy = '',
							variationsToCopyCount = 0,
							SEPERATOR_PEID_VID = '____mp_page_element_variation_seperator____',
							totalCount = 0;

					totalCount = contentListResponse.variations.length;
					if(totalCount > 0){
						for (var i = 0; i < totalCount; i++) {
							if(urlParams.content_id){
								if(contentListResponse.variations[i].content._id === urlParams.content_id){
									variationsToCopyCount++;
									variationsToCopy += contentListResponse.variations[i].page_element_id + SEPERATOR_PEID_VID + contentListResponse.variations[i].uid;
									break;
								}
							} else {
								variationsToCopyCount++;
								variationsToCopy += contentListResponse.variations[i].page_element_id + SEPERATOR_PEID_VID + contentListResponse.variations[i].uid;
								if(i !== (totalCount-1)){
									variationsToCopy += ',';
								}
							}


						}

						var currentExp = '';

						var params = {
							fullURL: contentListResponse.fullURL,
							xpath: contentListResponse.xpath,
							experimentID: currentExp,
							page_element_id: contentListResponse.page_element_id,
							variationsToCopy: variationsToCopy,
							allExperiments: '',
						};

						var exps = [];
						if(contentListResponse.experiments !== undefined){
							for (var i = 0; i < contentListResponse.experiments.length; i++) {
								exps.push({
									id: contentListResponse.experiments[i].page_element_id,
									title: contentListResponse.experiments[i].name,
									select: true,
								});

								if(params.allExperiments.length === 0){
									params.allExperiments += contentListResponse.experiments[i].page_element_id;
								} else {
									params.allExperiments += (',' + contentListResponse.experiments[i].page_element_id);
								}

								if(contentListResponse.variationsBelongToExp.indexOf(contentListResponse.experiments[i].page_element_id) !== -1){
									if(currentExp.length === 0){
										currentExp += contentListResponse.experiments[i].page_element_id;
									} else {
										currentExp += (',' + contentListResponse.experiments[i].page_element_id);

									}
								}

							}
						}


						var formElements = [
							{
								view:"list",
								// label:"Select an experiment to move",
								view:"list",
								// select:true,
								multiselect:"touch",

								id: "moveToExperimentID",
								height: 250,
								width: 100,
								template:"#title#",
								select: "multiselect",
								data:exps,
								value: currentExp,
								ready:function(){
									this.select(contentListResponse.variationsBelongToExp);
								},
								on: {
									onItemClick: function(id){
										var item = this.getItem(id);
									},
								},
							},{
									cols: [
													{
														view: "button",
														value: "Back",
														// css: "orangeBtn",
														// hidden: true,
														id: "go_back",
														width: 60,
														click: function(evt){
															showView('content_options');
														}
													}, {
														view: "button",
														value: "Create new",
														hidden: urlParams.page === 'experiment',
														css: "orangeBtn",
														width: 120,
														click: function(){
															showView('content');
														}
													},{
															view: "button",
															value: "Save",
															css: "orangeBtn",
															click: function(){
																var form = $$("moveToExpForm"),
																		url = monolop_api_base_url+'/api/component/content-list/move-to-experiment';

																if(form.validate()){
																	var formValues = form.getValues();
																	var selectedIds = $$("moveToExperimentID").getSelectedId().toString();
																	params.moveToExperimentID = selectedIds;


																	webix.ajax().post(url, params, {
																		error: function(text, data, XmlHttpRequest) {
																			alert("error");
																		},
																		success: function(response, data, XmlHttpRequest) {
																			var response = JSON.parse(response);
																			if (response.success === true) {

																				showView('experiment');

																				webix.message("Saved!");

																				parent.postMessage({t: 'variation-added-to-experiments', created: true},urlParams.fullURL);
																				parent.postMessage({t:"hide-iframe",message:"Experience has been saved"},"*");
																			} else {
																				webix.message({type: 'error', text: response.msg});
																			}
																		}
																	});

																}

															}
												},{
												}
									]
								}
						];

						webix.ui({
								container: 'experiment_container',
								id: 'experiment_container',
								scroll:false,
								css: 'experiment_container',
								width: 340,
								height:400,
								rows: [
									{
										view: "label",
										label: "Total number of variations to be saved: <b>"+variationsToCopyCount+"</b>",
										css: "labelVarCount",
									},{
										view: "label",
										label: "Select an experience to move"
									},
									{
										view:"form",
										id: "moveToExpForm",
										scroll:false,
										height:320,
										width:200,
										elements: formElements
									}
								]
						});



					} else {
						webix.alert('There is no content added on this element.');
						showView('content_options');
					}


		});


	}
			/* marc remove
function startGuidesIcon(t){
	$("#variationGuides").html('');
	// webix.ui({
	// 	container: "variationGuides",
	// 	view: "template",
	// 	height: 30,
	// 	css: "cl",
	// 	template: "<div class='guideIconClick'><span class='webix_icon fa-question ' title='Start guidelines'></span></div>",
	// 	onClick:{
	// 					"guideIconClick":function(ev, id){
	// 						if(guidelines.open === false){
	// 								guidelines.startGuideline(t,undefined,'tip');
	// 						}
	// 					}
	// 				}
	// });
	var stGd = document.createElement('i');
	stGd.title = 'Show Guides';
	stGd.className = "webix_icon fa-comment-o";
	stGd.style.position = 'absolute';
	stGd.style.top = '1.7%';
	stGd.style.right = '10%';
	stGd.style.fontSize = '1.2em';


	var stGdToggle = document.createElement('i');
	stGdToggle.id = 'toggleGuides';
	stGdToggle.className = "webix_icon fa-toggle-on";
	stGdToggle.style.position = 'absolute';
	stGdToggle.style.top = '2%';
	stGdToggle.style.right = '2.5%';
	stGdToggle.style.cursor = 'pointer';
	stGdToggle.style.fontSize = '1.2em';
	stGdToggle.title = 'Hide Guides';

	stGdToggle.onclick = function(){
		if($('#toggleGuides').hasClass('fa-toggle-off')){
			$('#toggleGuides').removeClass('fa-toggle-off');
			$('#toggleGuides').addClass('fa-toggle-on');
			$('#toggleGuides').attr('title', 'Hide Guides');
			// Start guides
			if(guidelines.open === false) {
				// this.style.pointerEvents = 'none';
				guidelines.startGuideline(t, {width: 300});
			}
		}
	};

	$("#variationGuides").append(stGd, stGdToggle);
}
			*/

function content_options(){
						webixDataSet = [];
						pagedDataSet = [];
						contentListResponse = undefined;
						currentVariation = false;

						$("#content_list_comp").html('');
						$("#content_options").html('');

						listProgresser("content_list_comp", "list");
						listProgresser("content_options", "list");


						var guideType = 'placementWindowEditContentOptions';
						/*
						var ops = [
							{
								"id":1,
								"title":"Add variation",
								"rank":"1",
								"info":"Change whatever you like in the highligted content element using the inline editor. When you are finished simply click this option."
							}
						];
						*/
						//
						var ops = [];
						if(urlParams.tagName.toLowerCase() == "img"){
							ops.push(
								{
									"id":5,
									"title":"Change image",
									"rank":"1",
									"info":"Change image"
								}
							);
						}else{
							ops.push(
								{
									"id":1,
									"title":"Add variation",
									"rank":"1",
									"info":"Change whatever you like in the highligted content element using the inline editor. When you are finished simply click this option."
								}
							);
						}

						if(urlParams.havVar == 1){
							ops.push(
								{
									"id":2,
									"title":"Add to experience",
									"rank":"2",
									"info": "All variations and their audience conditions are linked to a new or existing experience."
								}
							);
							guideType = 'placementWindowEditContentOptionsHasVar';
						}
						ops.push(
							{
								"id":3,
								"title":"Hide",
								"rank":"3",
								"info": "Select the content element you want to hide and select this option."
							}
						);
						if(urlParams.content_id){
							ops.push(
								{
									"id":4,
									"title":"Update variation",
									"rank":"4"
								}
							);
							guideType = 'placementWindowEditContentOptionsUpdate';
						}

						//startGuidesIcon(guideType);  marc remove

						$("#content_options").empty();
						webix.ajax().get(monolop_api_base_url+'/api/component/content-list', urlParams , function(response, xml, xhr) {
							response = JSON.parse(response);
							contentListResponse  = response;

							var variations = response.variations,
									totalCount = 0;
							variations = variations || [];

							var originalVariationContent = {
								id: "thisistheorignalcontentid",
								title: original.title + titleIdSeperator + "thisistheorignalcontentid",
								hidden: 0,
								content: original.content,
								deleted: 0,
								mappedTo: urlParams.xpath,
								original: true,
							};

							totalCount = variations.length;

							webix.ready(function() {
								// add a title
								/*webix.ui({
									container: "content_options",
									view: "label",
									// label: 'OK - now you have following options:',
									label: '',
									height: 20
								});*/

								// add list

								if(totalCount > 0){
									for (var i = 0; i < totalCount; i++) {
										webixDataSet.push({
											id: variations[i].content._id,
											title: variations[i].content.name + titleIdSeperator + variations[i].content._id,
											hidden: variations[i].content.hidden,
											content: variations[i].content.config.content,
											condition: variations[i].content.condition,
											deleted: variations[i].content.deleted,
											mappedTo: variations[i].mappedTo,
											original: false,
										});
									}

									var a = webixDataSet;
									while (a.length > 0)
											pagedDataSet.push(a.splice(0, page.size - 1));

									if(pagedDataSet.length === 0){
										pagedDataSet.push([]);
									}
									$("#content_list_comp").html('');
									pagedDataSet[page.current].unshift(originalVariationContent);

									webix.ui({
											 container:"content_list_comp",
											 id: "content_list_comp",
											 width: 338,
											 height: 146,
											 scroll:true,
											 view:"grouplist",
											 scheme:{
												 $group:function(obj){
													 return obj.title;
												 },
											 },

											 templateBack: function(obj){
												 //$(".guideIcon").hide();
												 $$('content_options').hide();
												 $('.webix_grouplist').css('height', '341px');
												 $('#variationGuides').hide();
												 var value = getTitle(obj.value);
												//  startGuidesIcon('placementWindowEditContentListVariation'); Imran remove
												 initializeGuidesAndTips('placementWindowEditContentListVariation');
												 return value;
											 },
											 templateGroup: function(obj){
												 //$(".guideIcon").hide();
												 if($$('content_options')){
													 $$('content_options').show();
													 $('.webix_grouplist').css('height', '146px');
												 }

												 $('#variationGuides').hide();
												 var value = getTitle(obj.value);
												//  startGuidesIcon('placementWindowEditContentListVariation'); Imran remove
												 initializeGuidesAndTips('placementWindowEditContentListVariation');
												 return value;
											 },
											 templateItem: function(obj){
												 //$(".guideIcon").show();
												 currentVariation = obj;
												 var html = "",
														 id = obj.id,
														 condition = currentVariation.condition;
												 if(obj.original === false){
													 if(obj.hidden == 1){
														 html = '<a href="javascript:void(0);" id="updateHidden_'+id+'" onclick=\'updateHidden( "' + id + '", 0);\' style="color:black;padding: 1px 7px;" class="updateStatus inactive status status1">inactive</a>&nbsp;&nbsp;&nbsp;  ';
													 } else {
														 html = '<a href="javascript:void(0);" id="updateHidden_'+id+'" onclick=\'updateHidden( "' + id + '", 1);\' style="color:black;padding: 1px 7px;" class="updateStatus active status status0">active</a>&nbsp;&nbsp;&nbsp;';
													 }
													 html += '|&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" id="updateVariation_'+id+'" onclick=\'showView("edit_content", "' + id + '");\' style="color:black;" class="updateVariationIcon"><span class="webix_icon fa-edit updateVariation"></span></a>';
													 html += '&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" title="' + condition + '" id="updateVariation_'+id+'" onclick=\'openCondition("' + id + '");\' style="color:black;" class="updateVariationConditionIcon"><span class="webix_icon fa-code updateVariationCondition"></span></a>';
													 html += '&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" id="deleteVariation_'+id+'" onclick=\'deleteVar( "' + id + '", '+totalCount+');\' style="color:black;" class="deleteVariationIcon"><span class="webix_icon fa-times deleteVariation"></span></a>';
												 }

												 html += '<p class="previewCont">Preview1</p> <hr>';

												 html += '<div style="height: 205px;overflow-y: auto;" >' + obj.content + '</div>';
												 $('#variationGuides').delay(1000).show(0);
												//  setTimeout(() => guidelines.startGuideline('placementWindowEditContentListVariation'), 1000)
												setTimeout(() => initializeGuidesAndTips('placementWindowEditContentListVariation'), 1000)

												 return html;
											 },
											 type:{
												 height:"auto"
											 },
											 select:false,

											 data:pagedDataSet[page.current],
									});

								}else {
									// console.log('currentVariationcurrentVariationcurrentVariation',currentVariation)


									$("#content_list_comp").html('');
									// $("#paging_here").html('');
									// 	webix.ui({
									// 			 container:"content_list_comp",
									// 			 id: "content_list_comp",
									// 			 view:"label",
									// 			 label: "No variation added yet.",
									//
									// 	})
								}

								var list = webix.ui({
									container:"content_options",
									id: "content_options",
									view:"list",
									width:338,
									height: 'auto',
									scroll: false,
									// template:"#title#",
									template: function(obj){
										var _html = '';
										_html += '<div class="item">';
										_html += '<div class="title">' + obj.title;

										// if(obj.info)
										// 	_html += '<a class="hide-below" href="javascript:void(0);">Hide below</a>';

										_html += '</div>';

										if(obj.info){
												_html += '<div class="info">' + obj.info + '</div>';
										}

										_html += '</div>';
										return _html;
									},
									select:true,
									data:ops
								});

								$('.webix_list').height('auto');

								// handler for hide-below
								// $('.hide-below').on('click', function(e){
								// 	var _elem = e.currentTarget;
								// 	var _info = $(_elem).parent().siblings('.info');
								// 	if(_info.is(':visible')){
								// 		$(_elem).parent().siblings('.info').fadeOut();
								// 		$(_elem).text('Show content');
								// 	}else{
								// 		$(_elem).parent().siblings('.info').fadeIn();
								// 		$(_elem).text('Hide below');
								// 	}
								// 	$(_elem).parents('.webix_list').height('auto');
								// });

								// make sure when hide-below link is clicked from list item, the parent event is not replicated
								// $('.webix_list .webix_scroll_cont .webix_list_item .item .title a').click(function(evt){
								// 	evt.stopPropagation();
								// });


								// guidelines.startGuideline(guideType, {width: 300});
								initializeGuidesAndTips(guideType);

								$$("content_options").attachEvent("onItemClick", function(id, e, node){
										var item = this.getItem(id);
										switch (id) {
											case "1":
												$$('content_list_comp').hide();
												showView('add_variation');
												break;
											case "2":
											$$('content_list_comp').hide();
											$("#create_new_exp_container").html('');
											$("#experiment_container").html('');

											parent.postMessage({t: 'get-new-experiment-backup'}, urlParams.fullURL);

												break;
											case "3":

												// parent.postMessage({t: 'hide-an-element'}, urlParams.fullURL);

												var data = {};
												data.type = "variation";
												data.variation_name = "Hidden";
												data.hide = true;
												data.condition = "";
												data.fullURL = urlParams.fullURL;
												data.page_element_id = urlParams.page_element_id;
												data.experiment_id = urlParams.experiment_id;
												parent.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);

												break;
											case "4":
												showView('update_variation');
												break;
											case "5":
												parent.parent.postMessage(JSON.stringify({t: "open-image-uploader"}), monolop_base_url);
												showView('add_variation');
												break;
											default:
												break;
										}
								});
							});

						});
}


function listProgresser(id, type){
							webix.ui({
									 container: id,
									 id: id,
									//  width: 340,
									 height: 350,
									 scroll:false,
									 view:type,
									 data:[],
							});
							//adding progress bar functionality to it
							webix.extend($$(id), webix.ProgressBar);

							$$(id).clearAll();
							$$(id).showProgress({

							});
						}

						function updateHidden(id, hidden){
							webix.ajax().post(apis.update_status, {
								content_id: id,
								hidden: hidden
							}, {
								error: function(text, data, XmlHttpRequest) {
									alert("error");
								},
								success: function(text, data, XmlHttpRequest) {
									var response = JSON.parse(text);
									if (response.status === 'success') {

										var _id = response.content._id,
												_hidden = response.content.hidden;

										var element = $("#updateHidden_" + _id);
										if (_hidden == "0") {
											element.text("active");
											element.removeClass("status1");
											element.addClass("status0");
											element.prop('onclick',null).off('click');
											element.on('click', function(e){
												updateHidden(_id, 1);
											});
										} else if (_hidden == "1") {
											element.text("inactive");
											element.removeClass("status0");
											element.addClass("status1");
											element.prop('onclick',null).off('click');
											element.on('click', function(e){
												updateHidden(_id, 0);
											});
										}


										if(pagedDataSet[page.current].length > 0){
											for (var i = 0; i < pagedDataSet[page.current].length; i++) {
												if(pagedDataSet[page.current][i].id === _id){
													pagedDataSet[page.current][i].hidden = parseInt(_hidden);
												}
											}
										}

										$$("content_list_comp").refresh();
									}
								}
							});
						}
						function postMessageInlineVar(id){
							webix.ajax().post(monolop_api_base_url+'/api/page-element/content-show', {content_id: id} , function(response, xml, xhr) {

								//response
								response = JSON.parse(response);
								var content = response.content;

								if(response.status === "success"){

									parent.postMessage({
										t: "init-FAB-from-edit-variation",
										content_id: content._id,
										name: content.name,
										condition: content.condition,
										xpath: urlParams.xpath,
										content: content.config.content
									}, "*");
								}

							});
						}

						function openCondition(id){
							var condition = '';
							if(currentVariation !== false){
								condition = currentVariation.condition || '';
							}

							parent.postMessage({
								t: "open-condition-builder",
								content_id: id,
								type: 'content-preview',
								condition: condition
							}, "*");
						}

						function deleteVar(id, variationCount){
							webix.ajax().post(apis.delete, {
								content_id: id,
								deleted: 1
							}, {
								error: function(text, data, XmlHttpRequest) {
									alert("error");
								},
								success: function(text, data, XmlHttpRequest) {
									var response = JSON.parse(text);
									if (response.status === 'success') {
										// init();
										webix.message("Deleted!");

										if(webixDataSet.length > 0){
											for (var i = 0; i < webixDataSet.length; i++) {
												if(webixDataSet[i].id === id){
													webixDataSet.splice(i, 1);
												}
											}
										}
										if(pagedDataSet[page.current].length > 0){
											for (var i = 0; i < pagedDataSet[page.current].length; i++) {
												if(pagedDataSet[page.current][i].id === id){
													pagedDataSet[page.current].splice(i, 1);
												}
											}
										}
										if(pagedDataSet[page.current].length === 0){
											if(page.current !== 0){
												page.current--;
											}
										}
										pagedDataSet = [];
										parent.postMessage({
											t: "remove-or-decrease-variation-bubble-from-list",
											fullURL: urlParams.fullURL,
											remove_popup: false,
											xpath: urlParams.xpath,
											variationCount: variationCount - 1,
										}, "*");
										init();
									}
								}
							});
						}

						function init (){
							$("#content_list_comp").html('');
							$("#content_options").html('');
							showView('content_options');

						}

						function getTitle(name_id){
							return name_id.split(titleIdSeperator)[0];
						}

						var titleIdSeperator = '____mp_title_seperator____',
								webixDataSet = [],
								pagedDataSet = [],
								contentListResponse;

						var page = {
							size: 10,
							current: 0,
						}

						var original = {
							has: false,
							title: '<b>Original</b>',
							content: ''
						};

						var apis = {
							update_status: monolop_api_base_url+'/api/component/content-list/update-status',
							delete: monolop_api_base_url+'/api/component/content-list/delete'
						};



// start
showView('content_options');
</script>
<script>

$(document).keyup(function(e) {
		 if (e.keyCode == 27) { // escape key maps to keycode `27`
			 guidelines.saveInSession();
		}
});

// listen from the child
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];

var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
// Listen to message from child IFrame window
eventer(messageEvent, function (e) {
			 var data = JSON.parse(e.data);
			 switch (data.t) {
				case "add-element-content-variation":

					variationsModel.addVariation(
						monolop_api_base_url+"/api/page-element/content-create",
						data.data
					);
					// 	window.removeEventListener(messageEvent, __listener, false);
					break;
				case "update-element-content-variation":

					variationsModel.updateVariation(
						monolop_api_base_url+"/api/page-element/content-update",
						data.data
					);

					break;
				case "set-condition":
					if(data.data.condition){
						data.data.condition = data.data.condition.trim();
					}
					if(data.data.condition && data.data.condition !== 'if (){|}'){
						if($$("condition")){
							$$("condition").setValue(data.data.condition);
							$$("conditionLabel").setValue("Condition is available.");
						}else {
							conditionBuilderValue = data.data.condition;
							conditionBuilderLabel = "Condition is available.";
						}
						urlParams.content_condition = data.data.condition;
					} else {
						if($$("condition")){
							$$("condition").setValue("");
							$$("conditionLabel").setValue("All visitors.");
						}else {
							conditionBuilderValue = "";
							conditionBuilderLabel = "All visitors.";
						}

					}
					break;
				case "set-condition1":

					var condition = data.data.condition;
					if(condition){
						if(currentVariation !== false){
							var content_id = currentVariation.id;
							webix.ajax().post(monolop_api_base_url+'/api/page-element/content-update-condition?ajax=true', {content_id: content_id,condition: condition} , function(iresponse, xml, xhr) {
								iresponse = JSON.parse(iresponse);
								if(iresponse.status === 'success'){
									currentVariation.condition = iresponse.content.condition;
									webix.message("Condition has been updated.");
								} else {
									webix.alert("An eror occoured. Please refresh and try again.");
								}
							});
						}
					}


					break;
				// case "load-variation-list":
				// 		original.has = true;
				// 		original.content = data.data.content;
				// 		showContentList();
				// 		break;
				case "set-experiment-values-from-backup":
					if(!data.newExp){
							showView('experiment');
					} else {
						newExpBackup = data.data;
						showView('content');
					}

					break;
				//marc added
				case "start-guideline":
	 			  guidelines.generate(guidelines.page.currentGuide, {width: 300});
				  break;
					default:
						break;
			 }
			 // Do whatever you want to do with the data got from IFrame in Parent form.															 //

}, false);
</script>
@endsection
