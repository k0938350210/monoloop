	@extends('webui_popover_layout')

	@section('content')
	<div id="publish_page_options_container" style="width: 100%;"></div>
	@endsection

	@section('footerjs')
	<script type="text/javascript">

		var parent_accessible = true ;
		try{
		  parent.parent.document;
		}catch(e){
		  parent_accessible = false ;
		}

		if(parent_accessible){
			if(parent.parent.webPageListConfig){
				parent.parent.webPageListConfig.placementWindow.publish = window;
			}
			if(parent.parent.experimentListConfig){
				if(parent.parent.experimentListConfig.placementWindow){
					parent.parent.experimentListConfig.placementWindow.publish = window;
				}
			}
		}
		var monolop_base_url = '{{config('app.url')}}';
		var urlParams = {};
		urlParams.source = '{{ $source }}';
		urlParams.fullURL = '{{ $fullURL }}';
		urlParams.page_element_id = '{{ $page_element_id }}';
		urlParams.experiment_id = '{{ $experiment_id }}';
		var temp;
		var formChanged = false;
		var checkIfFormChanged = function(){
			if(formChanged == false){
				parent.postMessage(JSON.stringify({t: "page-confirm-form-changed", formChanged: true}), monolop_base_url);
				formChanged = true;
			}
		}

		var urlOptions = [
      {id: 'exactMatch', value: 'Exact Match'},
      {id: 'allowParams', value: 'Allow Parameters'},
      {id: 'startsWith', value: 'Starts With'},
			{id: 'regularExpression', value: 'Regular Expression'}
    ];
		var getUrlOption = function(id){
				if(isNaN(parseInt(id))){
					return id;
				}
        id = id || 0;
        switch (id) {
          case 0: return "exactMatch"; break;
          case 1: return "allowParams"; break;
          case 2: return "startsWith"; break;
					case 3: return "regularExpression"; break;
          default: return "exactMatch"; break;
        }
    };

		var __form = [{
			view: "layout",
			height: 1024,
			rows: [{
				view: "label",
				id: "fullURLWebPageList",
				css: "fullURLWebPageList",
				// label: 'Name',
				name: "fullURLWebPageList",
				readonly: true,
			}, {
				view: "richselect",
				label: "URL Match Options",
				id: "urlOptionWebPageElement",
				css: "urlOptionWebPageElement",
				name: "urlOptionWebPageElement",
				value: urlOptions[0].id,
				options: urlOptions,
				labelAlign: 'left',
				on:{
					onChange: function(){
						// Mantis # 4023
						var selected = $$("urlOptionWebPageElement").getValue();
						selected == 'regularExpression' ? $('.content .webix_el_text').css('display','block') : $('.content .webix_el_text').css('display','none');

						checkIfFormChanged();
					}
				},
			},{
			 view: "textarea",
			 rows: 3,
			 height: 200,
			 id: "remarkWebPageList",
			 css: "remarkWebPageList",
			 label: 'Remark',
			 name: "remarkWebPageList",
			 on:{
				 onChange: function(){
					 checkIfFormChanged();
				 }
			 },
			 // readonly: true,
			 // value: webPageListConfig.allFormData.basic.tRemarkWebPageList
			 // invalidMessage: "Description can not be empty"
		 }, {
			 view: "text",
			 id: "regularExpressionWebPageElement",
			 css: "regularExpressionWebPageElement",
			 label: 'Regular Expression',
			 width:500,
			 name: "regularExpressionWebPageElement",
			 on:{
				 onChange: function(){
					 checkIfFormChanged();
				 }
			 },
			 // readonly: true,
			 // value: webPageListConfig.allFormData.basic.tRegularExpressionWebPageElement
			 // invalidMessage: "Name can not be empty"
		 }
		 // ,{
			//  cols: [
			// 	 {
			// 		 view:"checkbox",
			// 		 id:"includeWWWWebPageElement",
			// 		 css:"includeWWWWebPageElement",
			// 		 name:"includeWWWWebPageElement",
			// 		 label:"Include WWW",
			// 		 labelPosition: "left",
			// 		 on:{
			// 			 onChange: function(){
			// 				 checkIfFormChanged();
			// 			 }
			// 		 },
			// 		 // readonly: true,
			// 		 // value: webPageListConfig.allFormData.basic.tIncludeWWWWebPageElement
			// 	 }, {
			// 		 view:"checkbox",
			// 		 id:"includeHttpHttpsWebPageElement",
			// 		 css:"includeHttpHttpsWebPageElement",
			// 		 name:"includeHttpHttpsWebPageElement",
			// 		 label:"Include Http / Https",
			// 		 labelPosition: "left",
			// 		 on:{
			// 			 onChange: function(){
			// 				 checkIfFormChanged();
			// 			 }
			// 		 },
			// 		 // readonly: true,
			// 		 // value: webPageListConfig.allFormData.basic.tIncludeHttpHttpsWebPageElement
			// 	 }
			//  ]
		 // }
	 ]
		}, ];

			webix.ui({
				container: "publish_page_options_container",
				id: "publishPageID",
				css: "publishPageID",
				rows: [{
					cols: [{}, {
						view: "form",
						id: "publishFormID",
						css: "publishFormID",
						height: 550,
						// scroll: true,
						complexData: true,
						elements: __form,
						// rules: {},
						width: 600,
						borderless: true,
						margin: 3,
						elementsConfig: {
							labelPosition: "top",
							labelWidth: 140,
							bottomPadding: 18
						},
					}, {}, ]
				}, {
					cols: [{}, {
					}, {
						id: 'saveButton',
						view: "button",
						label: (urlParams.source == 'goal' ? "Select" : "Save"),
						css: "orangeBtn",
						width: 138,
						click: function() {
							var btnCtrl = this;
							btnCtrl.disable();
							var url = monolop_api_base_url+'/component/page-list-publish/store';
							var form = $$("publishFormID");
							webix.ajax().post(url, form.getValues(), {
								error: function(text, data, XmlHttpRequest) {
									alert("error");
									btnCtrl.enable();
								},
								success: function(text, data, XmlHttpRequest) {
									var response = JSON.parse(text);
									if (response.status === 'success') {
											btnCtrl.enable();
											console.log("page-published-xyz", "page-published");
											parent.postMessage(JSON.stringify({t: "page-published", source: urlParams.source, experiment: response.experiment, pageElement: response.content, temp: temp, message: 'Page is successfully published.'}), '*');
									}
								}
							});
						}
					}, {}, {}]
				}]
			});

			// custom changes in publish section
			var _template = '<div id="collapsableContainer" style="width:100%;">';
	 		_template    += '<fieldset class=\"collapse\" style=\"border:none;">';
			_template    += '<legend style=\"width: 104%;margin-left: -2%;\">Advanced <i class=\"fa fa-chevron-circle-up\"></i></legend>';
			_template    += '<div class=\"content\" style=\"margin-left:-2%;display:none;width:90%;\" ></div>';
			_template    += '</fieldset>';

			var controlURLMatchOption = $('[view_id=urlOptionWebPageElement]');
			var _pivot = $('div[view_id=remarkWebPageList]');
			// get webix control container div
			var _container = _pivot.nextAll('div');
			// remove existing elements from DOM
			_container.remove();
			controlURLMatchOption.remove();

			// add _template into DOM at above position
			$(_template).insertAfter(_pivot);
			// add existing element as above div's children
			// $('#collapsableContainer div.content').append(_container[1], _container[0], controlURLMatchOption);
			$('#collapsableContainer div.content').append(_container[1], controlURLMatchOption, _container[0]);

			// attach toggle handler
			$('#collapsableContainer legend').bind('click', function(){
				$(this).parent().find('div.content').slideToggle();
				// change icon
				if($(this).find('i').hasClass('fa-chevron-circle-up')){
					$(this).find('i').removeClass('fa-chevron-circle-up');
					$(this).find('i').addClass('fa-chevron-circle-down');
				}	else{
					$(this).find('i').removeClass('fa-chevron-circle-down');
					$(this).find('i').addClass('fa-chevron-circle-up');
				}
			});
	</script>
	<script>
	// listen from the child
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];

	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	// Listen to message from child IFrame window
	eventer(messageEvent, function (e) {
				 var data = e.data;
				 try{
					 data = JSON.parse(e.data);
				 }catch(e){
					 console.log('Parsing a JS Obj with JSON.parse exception');
				 }
				 switch (data.t) {
					 case "start-guideline-in-site":

 						var r = data.data;
 						var cg = undefined;
 						if(r.currentGuide){
 							cg = JSON.parse(r.currentGuide);
 							guidelines.startedBefore = true;
 						}

 						guidelines[data.pageId] = r.guidelines;

 						guidelines.page.pageId = data.pageId;
 						if(cg){
 							if(cg.currentGuide){
 								guidelines.page.currentGuide = cg.currentGuide;
 							} else {
 								guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
 							}
 						} else {
 							guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
 						}

 						// console.log("guidelines.page",guidelines.page.currentGuide);
 						if(guidelines.page.currentGuide && !MONOloop.jq(guidelines.page.currentGuide.location_id).attr('data-target')){
 							if(cg){
 								setTimeout(function(){
 									// console.log("guidelines.page",guidelines.page);
 									guidelines.generate(guidelines.page.currentGuide, {});
 									$(document).keyup(function(e) {
 											 if (e.keyCode == 27) { // escape key maps to keycode `27`
 												//  console.log("ESCAPED");
 												 guidelines.saveInSession('yes','iframe');

 											}
 									});
 								}, 1000);
 							}

 						} else {
 						}
 						break;
					case "set-publish-page-options":

						var form = $$("publishFormID");

						var urlOption = 0;
						urlOption = data.returnUrlConfig.url_option;

						urlOption = getUrlOption(urlOption);

						if(urlOption == 'regularExpression'){
							$('.content .webix_el_text').css('display','block');
						}
						else {
							$('.content .webix_el_text').css('display','none');
						}

						form.setValues({
							fullURLWebPageList: data.fullURL,
							includeWWWWebPageElement: data.returnUrlConfig.inc_www,
							includeHttpHttpsWebPageElement: data.returnUrlConfig.inc_http_https,
							urlOptionWebPageElement: urlOption,
							regularExpressionWebPageElement: data.returnUrlConfig.reg_ex,
							remarkWebPageList: data.returnUrlConfig.remark,
							source: urlParams.source,
							page_element_id: urlParams.page_element_id,
							experiment_id: urlParams.experiment_id,
							isExistingPageELementID: data.temp === undefined ? data.mode === 'edit' : data.temp.mode === 'edit',
							currentFolder: data.currentFolder
						});

						temp = data.temp;

						break;
					default:
					 	break;
				 }
				 // Do whatever you want to do with the data got from IFrame in Parent form.
	}, false);
	</script>
	@endsection
