
@include('application.emails.header')
<p>
Hi {{ $newAccount->contact_name }},
<br />
<br />
You got monoloop invite from {{ $loginAccount->contact_email || $loginAccount->contact_name }}
Please click link to accept invitation
<a href="">Click here</a>
</p>
@include('application.emails.footer')
