@extends('login')

@section('content')

@include('blocks.fullbg')

<div class="wrapper">
  <div class="row">
    <div style="width:700px;margin-left:20px;">
      <div id="login-box" class="panel login-panel">
        <div class="panel-body">

          <form role="form" action="{{ Request::secure() ? secure_url('password/email') : url('password/email') }}" onsubmit="return validateResetForm()" method="POST">
            <div class="row">
              <div class="col-md-7">
                <h2>Reset Password</h2>
                <span id="email"></span>
                {!! Notification::group('info', 'success' , 'warning')->showAll() !!}
                @include('blocks.error')
                @if (count($errors) === 0)
                <br/><br/>
                @endif
                <p>To reset your password, please enter your email. Upon submitting the reset you will receive a email with a link to reset your password.</p>
              </div>
              <div class="col-md-5" >
    						<input type="hidden" name="_token" value="{{ csrf_token() }}" >
                <img src="/images/monoloop-logo-v2.png" >
                <br/>
                <p>Reset your password?</p>

                <div class="form-group">

    							<input type="input" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}"/>
    						</div>
    						@if($show_insert_username)
                <div class="form-group">
    					    <label for="username">Username</label>
    					    <input type="input" class="form-control" id="username" name="username" placeholder="Username" />
    						</div>
    						@endif
                <div class="row">
                  <div class="col-md-6 col-md-offset-6">
                    <button type="submit" class="btn btn-signin form-control">Reset</button><br/><br/><br/>
                    <a class="btn btn-signup form-control" href="{{ Request::secure() ? secure_url('/auth/login') : url('/auth/login') }}" class="forgottenlink">Log in</a>
                  </div>
                </div>

              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
