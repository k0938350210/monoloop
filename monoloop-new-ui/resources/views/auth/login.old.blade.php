@extends('login')

@section('content')
<div id="login-box">
	<img style="width: 100%; border: 0px;" src="/images/monoloop-logo-w.png"/>
	<div id="monoloop-frame">
		@if (count($errors) > 0)
			<div class="error">
				 Invalid username or password
			</div>
		@endif
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="text" placeholder="User Name" name="username" value="{{ old('username') }}">
			<input type="password" placeholder="password" name="password">
			<input type="submit" value="LOG IN" id="btn_submit" />
			<a href="{{ url('/password/email') }}" class="forgottenlink">Forgotten your User Name or password?</a><br />
      <a href="{{ url('/auth/register') }}" class="forgottenlink">Sign up</a>
		</form>
	</div>
</div>
@endsection
