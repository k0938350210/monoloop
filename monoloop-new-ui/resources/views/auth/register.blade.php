@extends('login')

@section('content')

@include('blocks.fullbg')

<div class="wrapper">
    <div class="row">
      <div style="width:700px;margin-left:20px;">
        <div id="login-box" class="panel login-panel" style="margin-top:100px;">
          <div class="panel-body">
          	<div class="row">
          		<div class="col-md-5 col-md-offset-7">
          			<img src="/images/monoloop-logo-v2.png">
          		</div>
          	</div>
          	<br/>

						{!! Notification::group('info', 'success' , 'warning')->showAll() !!}
            @include('blocks.error')
            <form role="form" action="{{ url('auth/register') }}" onsubmit=" return validateSignupForm()" method="POST">
              <div>
                <span id="username"></span>

              </div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
		                <!--<label for="name">First name</label>-->
		  							<input type="input" class="form-control" id="firstname" name="firstname" placeholder="First Name" value="{{ old('name') }}"/>
		  						</div>
								</div>
                <div class="col-md-6">
									<div class="form-group">
		                <!--<label for="name">Last name</label>-->
		  							<input type="input" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="{{ old('name') }}"/>
		  						</div>
								</div>

							</div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <!--<label for="email">Email</label>-->
                    <input type="input" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}"/>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <!--<label for="email">Confirm email</label>-->
                    <input type="input" class="form-control" id="confirmemail" name="confirmemail" placeholder="Confirm email" value="{{ old('email') }}"/>
                  </div>
                </div>
              </div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
								    <!--<label for="password">Password</label>-->
								    <input class="form-control" type="password" id="password" name="password" placeholder="Password" />
		  						</div>
								</div>
								<div class="col-md-6">
		              <div class="form-group">
								    <!--<label for="confirmpassword">Confirm password</label>-->
								    <input class="form-control" type="password" id="confirmpassword" name="confirmpassword" placeholder="Confirm password" />
		  						</div>
								</div>
							</div>
              <input type="hidden" name="user_type" value="enterprise">
              <!-- <label class="radio-inline">
                <input type="radio" value="enterprise" name="user_type" checked=""> Enterprise
              </label>
              <label class="radio-inline">
                <input type="radio" value="agency" name="user_type">Agency
              </label> -->
              <!-- <div class="row">
								<div class="col-md-12">
									<div class="form-group">
								    <label for="domain">Domain</label>
								    <input type="input" class="form-control" id="domainname" name="domainname" placeholder="Domain" />
		  						</div>
								</div>
              </div> -->

              <div class="row">
								<div class="col-md-6 col-md-offset-6">
                  <input type="hidden" name="source" value="Web" />
              		<button type="submit" class="btn btn-signin form-control">CREAT NEW MONOLOOP ACCOUNT</button>
              	</div>
              </div>
              <input type="hidden" name="token" id="token"/>
            </form>
          </div>
        </div>

        <div class="panel login-panel">
	        <div class="panel-body">
	          <div class="row">
	            <div class="col-md-7">
	            <a style="line-height: 2.42857;" href="{{ url('/password/email') }}">Forgotten your User Name or password?</a>
	            </div>
	            <div class="col-md-5">
	              <div class="row">
	                <div class="col-md-6 text-right">
	                  <span style="line-height: 2.42857;">Existing User</span>
	                </div>
	                <div class="col-md-6">
	                 <a href="{{ url('/auth/login') }}" class="btn btn-signup form-control">Sign In</a>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
      </div>
    </div>
</div>


@endsection
