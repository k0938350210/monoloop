@extends('webix') 


@section('customcss')
    <link rel="stylesheet" type="text/css" href="/extjs4.2.1/resources/ext-theme-neptune/ext-theme-neptune-all.css?{{Config::get('app.version') }}" media="all" />
	<script src="/extjs4.2.1/ext-all.js?{{Config::get('app.version') }}" type="text/javascript"></script>
    <script src="/js/extjs4.js?{{Config::get('app.version') }}" type="text/javascript"></script>
@endsection
