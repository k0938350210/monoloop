<!doctype html>
<html ng-app='APP'>

<head>
	<meta name="description" content="condition builder" />

	<!-- <title>Placement Window</title> -->
	<link rel="stylesheet" type="text/css" href="{{ elixir('build/app.css') }}" media="all" />
</head>

<body>
  @yield('content')

  <script src="{{ elixir('build/vendor.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var monolop_base_url = '{{config('app.url')}}' ;
		var monolop_api_base_url = '{{config('app.monoloop_api_base_url')}}' ;
		var ajaxHeaders = {'Content-Type': 'application/x-www-form-urlencoded', 'ML-Token': "{{ Session::get('mlToken') }}" };
		webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers){
			headers["Content-type"]= ajaxHeaders['Content-Type'];
			headers["ML-Token"]= ajaxHeaders['ML-Token'];
		});
	</script>
  @yield('footerjs')
</body>

</html>
