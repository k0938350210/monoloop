@extends('ui') 
 

@section('customcss')
    <link rel="stylesheet" type="text/css" href="/extjs4.0/resources/css/ext-all-gray.css?{{Config::get('app.version') }}" media="all" />
	<script src="/extjs4.0/ext-all.js?{{Config::get('app.version') }}" type="text/javascript"></script>
    <script src="/js/extjs4.js?{{Config::get('app.version') }}" type="text/javascript"></script>
@endsection