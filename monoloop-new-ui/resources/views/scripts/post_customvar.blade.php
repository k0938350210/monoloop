@foreach($customVars as $customvar)
  @if( $customvar->customvar->type_id == 5 )
    <?php continue ; ?>
  @endif
  @if( $customvar->urlConfig->inc_www == 1 )
    monoloopTestURL = monoloopURL_includeWWW ;
  @else
    monoloopTestURL = monoloopURL ;
  @endif

  @if( $customvar->urlConfig->reg_ex == '' )
    @if( $customvar->urlConfig->url_option == 0 )
      if( MONOloop.catExactMatch( monoloopTestURL , '{{$customvar->urlConfig->cleanUrl}}' ) ){
    @elseif($customvar->urlConfig->url_option == 1 )
      if( MONOloop.catAllowPara( monoloopTestURL , '{{$customvar->urlConfig->cleanUrl}}' ) ){
    @elseif($customvar->urlConfig->url_option == 2 )
      if( MONOloop.catStartWith( monoloopTestURL , '{{$customvar->urlConfig->cleanUrl}}' ) ){
    @endif
  @else
    if( MONOloop.catCheckRegExpression( monoloopTestURL , '{{$customvar->urlConfig->reg_ex}}'  )){
  @endif

  @if($customvar->customvar->type_id == 4)
     function monoloop_cate_customfunction_{{$customvar->nid}}() { try{ {!! $customvar->customvar->type_data !!} }catch(err){ return '' ; } }
  @endif

  @if( $customvar->customvar->type_id == 1 )
    @if( $customvar->customvar->filter_type_id == 0 )
      name1 =  MONOloop.catReadXP('{{$customvar->customvar->type_data}}') ;
    @else
      name1 =  MONOloop.catReadXP_PostUserFunc('{{$customvar->customvar->type_data}}', '{{$customvar->customvar->filter_data}}' ) ;
    @endif
    MONOloop.trackCV({{$customvar->customvar->custom_field->uid}}, MONOloop.catReadMeta('{{$customvar->customvar->type_data}}') , {{(int)$customvar->customvar->save_function}} , {{(int)$customvar->customvar->custom_field->datatypeId}} );
  @elseif($customvar->customvar->type_id == 2)
    @if( $customvar->customvar->filter_type_id == 0 )
      name1 =  MONOloop.catReadXP('{{$customvar->customvar->type_data}}') ;
    @else
      name1 =  MONOloop.catReadXP_PostUserFunc('{{$customvar->customvar->type_data}}', '{{$customvar->customvar->filter_data}}' ) ;
    @endif
    MONOloop.trackCV({{$customvar->customvar->custom_field->uid}}, name1  ,{{(int)$customvar->customvar->save_function}} ,{{(int)$customvar->customvar->custom_field->datatypeId}});
  @elseif($customvar->customvar->type_id == 3)
    @if( $customvar->customvar->filter_type_id == 0 )
      name1 =  MONOloop.catReadXP('{{$customvar->customvar->type_data}}') ;
    @else
      name1 =  MONOloop.catRegExp_PostUserFunc('{{$customvar->customvar->type_data}}', '{{$customvar->customvar->filter_data}}'  ) ;
    @endif
    MONOloop.trackCV({{$customvar->customvar->custom_field->uid}}, name1  , {{(int)$customvar->customvar->save_function}} , {{(int)$customvar->customvar->custom_field->datatypeId}} );
  @elseif($customvar->customvar->type_id == 4)
    MONOloop.trackCV({{$customvar->customvar->custom_field->uid}}, monoloop_cate_customfunction_{{$customvar->nid}}()  ,  {{(int)$customvar->customvar->save_function}} , {{(int)$customvar->customvar->custom_field->datatypeId}} );
  @endif

  }
@endforeach
