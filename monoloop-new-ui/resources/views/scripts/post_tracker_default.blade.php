@if( $tracker->tracker->type2_id == 1 )
  @if( $tracker->tracker->filter2_type_id == 0 )
    val2 = MONOloop.catReadMeta('{{$tracker->tracker->type2_data}}') ;
  @else
    val2 = MONOloop.catReadMeta_PostUserFunc('{{$tracker->tracker->type2_data}}' , '{{$tracker->tracker->filter2_data}}') ;
  @endif
@elseif( $tracker->tracker->type2_id == 2 )
  @if( $tracker->tracker->filter2_type_id == 0 )
    val2 = MONOloop.getCartInnerContent('{{$tracker->tracker->type2_data}}') ;
  @elseif( $tracker->tracker->filter2_type_id == 2 )
    p = MONOloop.getCartInnerContent('{{$tracker->tracker->type2_data}}') ;
    val2 = MONOloop.getCartPriceDefaultFilter(p) ;
  @else
    val2 = MONOloop.catReadXP_PostUserFunc('{{$tracker->tracker->type2_data}}' , '{{$tracker->tracker->filter2_data}}') ;
  @endif
@elseif( $tracker->tracker->type2_id == 3 )
  @if( $tracker->tracker->filter2_type_id == 0 )
    val2 = MONOloop.catRegExp('{{$tracker->tracker->type2_data}}') ;
  @else
    val2 = MONOloop.catRegExp_PostUserFunc('{{$tracker->tracker->type2_data}}' , '{{$tracker->tracker->filter2_data}}') ;
  @endif
@elseif( $tracker->tracker->type2_id == 4 )
  var monoloop_cate_customfunction_{{$tracker->_id}}_2 = function() { try{ {!!$tracker->tracker->type2_data!!} }catch(err){ return '' ; } };
  val2 = monoloop_cate_customfunction_{{$tracker->_id}}_2() ;
@else
  val2 = null ;
@endif


@if( $tracker->tracker->tracker_type == 'product')
  @if( $tracker->tracker->type3_id == 1 )
    @if( $tracker->tracker->filter3_type_id == 0 )
      val3 = MONOloop.catReadMeta('{{$tracker->tracker->type3_data}}') ;
    @else
      val3 = MONOloop.catReadMeta_PostUserFunc('{{$tracker->tracker->type3_data}}' , '{{$tracker->tracker->filter3_data}}') ;
    @endif
  @elseif( $tracker->tracker->type3_id == 2 )
    @if( $tracker->tracker->filter3_type_id == 0 )
      val3 = MONOloop.getCartInnerContent('{{$tracker->tracker->type3_data}}') ;
    @elseif( $tracker->tracker->filter3_type_id == 2 )
      p = MONOloop.getCartInnerContent('{{$tracker->tracker->type3_data}}') ;
      val3 = MONOloop.getCartPriceDefaultFilter(p) ;
    @else
      val3 = MONOloop.catReadXP_PostUserFunc('{{$tracker->tracker->type3_data}}' , '{{$tracker->tracker->filter3_data}}') ;
    @endif
  @elseif( $tracker->tracker->type3_id == 3 )
    @if( $tracker->tracker->filter3_type_id == 0 )
      val3 = MONOloop.catRegExp('{{$tracker->tracker->type3_data}}') ;
    @else
      val3 = MONOloop.catRegExp_PostUserFunc('{{$tracker->tracker->type3_data}}' , '{{$tracker->tracker->filter3_data}}') ;
    @endif
  @elseif( $tracker->tracker->type3_id == 4 )
    var monoloop_cate_customfunction_{{$tracker->_id}}_3 = function() { try{ {!!$tracker->tracker->type3_data!!} }catch(err){ return '' ; } };
    val3 = monoloop_cate_customfunction_{{$tracker->_id}}_3() ;
  @else
    val3 = null ;
  @endif
@else
  val3 = null ;
@endif

@if( $tracker->tracker->type_id == 1 )
  @if( $tracker->tracker->filter_type_id == 0 )
    name1 = MONOloop.catReadMeta('{{$tracker->tracker->type_data}}') ;
  @else
    name1 = MONOloop.catReadMeta_PostUserFunc('{{$tracker->tracker->type_data}}' , '{{$tracker->tracker->filter_data}}') ;
  @endif
  MONOloop.trackCategory({{$tracker->nid}},name1 , val2 , val3 , {{intval($tracker->tracker->trackerTypeId)}}, {{intval($tracker->tracker->assetUid)}} );
@elseif( $tracker->tracker->type_id == 2 )
  @if( $tracker->tracker->filter_type_id == 0 )
    name1 =  MONOloop.catReadXP('{{$tracker->tracker->type_data}}') ;
  @elseif( $tracker->tracker->filter_type_id == 2 )
    p = MONOloop.getCartInnerContent('{{$tracker->tracker->type_data}}') ;
    name1 = MONOloop.getCartPriceDefaultFilter(p) ;
  @else
    name1 =  MONOloop.catReadXP_PostUserFunc('{{intval($tracker->tracker->type_data)}}', '{{intval($tracker->tracker->filter_data)}}') ;
  @endif
   MONOloop.trackCategory({{$tracker->nid}},name1, val2, val3 , {{intval($tracker->tracker->trackerTypeId)}}, {{intval($tracker->tracker->assetUid)}} );
@elseif( $tracker->tracker->type_id == 3 )
  @if( $tracker->tracker->filter_type_id == 0 )
    name1 =  MONOloop.catRegExp('{{$tracker->tracker->type_data}}') ;
  @else
    name1 =  MONOloop.catRegExp_PostUserFunc('{{$tracker->tracker->type_data}}', '{{$tracker->tracker->filter_data}}' ) ;
  @endif
  MONOloop.trackCategory({{$tracker->nid}},name1, val2, val3 , {{intval($tracker->tracker->trackerTypeId)}}, {{intval($tracker->tracker->assetUid)}});
@else
  var monoloop_cate_customfunction_{{$tracker->_id}} = function() { try{ {!! $tracker->tracker->type_data !!} }catch(err){ return '' ; } };
  MONOloop.trackCategory({{$tracker->nid}}, monoloop_cate_customfunction_{{$tracker->_id}}() ,val2 , val3 , {{intval($tracker->tracker->trackerTypeId)}}, {{intval($tracker->tracker->assetUid)}} );
@endif
