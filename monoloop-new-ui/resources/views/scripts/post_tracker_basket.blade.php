<?php list($step3_fisrt , $step3_second ) = explode(':' , $tracker->tracker->basket['step3_xpath']) ; ?>

MONOloopReady(function() { var rows =  MONOloop.$('{{$tracker->tracker->basket['step2_xpath']}} $step3_fisrt') ;
for(var i = 0 ; i < rows.length ; i++){
  @if( $tracker->tracker->basket['step_xpath_n_ft'] == 0 )
    var n = MONOloop.getCartInnerContent(MONOloop.find( [rows[i]] , '{{$tracker->tracker->basket['step4_xpath_n']}}'));
  @else
    function tracker_n{{$tracker->_id}}(i){
      try {
        var selector = '{{$tracker->tracker->basket['step2_xpath']}} {{$step3_fisrt}}:eq('+i+') {{$tracker->tracker->basket['step4_xpath_n']}}' ;
        {!! $tracker->tracker->basket['step_xpath_n_cjs'] !!}
      }catch(err) {
        return '' ;
      }
    }
    var n = MONOloop.trim( tracker_n{{$tracker->_id}}(i) ) ; 
  @endif
  
  @if( $tracker->tracker->basket['step_xpath_s_ft'] == 0 )
    var s = MONOloop.getCartInnerContent(MONOloop.find([rows[i]], '{{$tracker->tracker->basket['step4_xpath_s']}}'));
  @else
    function tracker_s{{$tracker->_id}}(i){
      try {
        var selector = '{{$tracker->tracker->basket['step2_xpath']}} {{$step3_fisrt}}:eq('+i+') {{$tracker->tracker->basket['step4_xpath_s']}}' ;
        {!! $tracker->tracker->basket['step_xpath_s_cjs'] !!}
      }catch(err) {
        return '' ;
      }
    }
    var s = MONOloop.trim( tracker_s{{$tracker->_id}}(i) ) ; 
  @endif
  
  @if( $tracker->tracker->basket['step_xpath_q_ft'] == 0 )
    var q = MONOloop.getCartInnerContent(MONOloop.find([rows[i]],'{{$tracker->tracker->basket['step4_xpath_q']}}'));
  @else
    function tracker_q{{$tracker->_id}}(i){
      try {
        var selector = '{{$tracker->tracker->basket['step2_xpath']}} {{$step3_fisrt}}:eq('+i+') {{$tracker->tracker->basket['step4_xpath_q']}}' ;
        {!! $tracker->tracker->basket['step_xpath_q_cjs'] !!}
      }catch(err) {
        return '' ;
      }
    }
    var q = MONOloop.trim( tracker_q{{$tracker->_id}}(i) ) ; 
  @endif
  
  @if( $tracker->tracker->basket['step_xpath_p_ft'] == 0 )
    var p = MONOloop.getCartInnerContent(MONOloop.find([rows[i]] , '{{$tracker->tracker->basket['step4_xpath_p']}}'));
    p = MONOloop.getCartPriceDefaultFilter(p) ; 
    var c = MONOloop.getCartInnerContent(MONOloop.find([rows[i]] , '{{$tracker->tracker->basket['step4_xpath_p']}}'));
    if( c != undefined){ c = c.replace(/[0-9\.]+/g,"").replace(\'&nbsp;\',"") ; }   
  @else
    function tracker_p{{$tracker->_id}}(i){
      try {
        var selector = '{{$tracker->tracker->basket['step2_xpath']}} {{$step3_fisrt}}:eq('+i+') {{$tracker->tracker->basket['step4_xpath_p']}}' ;
        {!! $tracker->tracker->basket['step_xpath_p_cjs'] !!}
      }catch(err) {
        return '' ;
      }
    }
    var p = MONOloop.trim( tracker_p{{$tracker->_id}}(i) ) ; 
    
    function tracker_c{{$tracker->_id}}(i){
      try {
        var selector = '{{$tracker->tracker->basket['step2_xpath']}} {{$step3_fisrt}}:eq('+i+') {{$tracker->tracker->basket['step4_xpath_p']}}' ;
        {!! $tracker->tracker->basket['step_xpath_p_cjs2'] !!}
      }catch(err) {
        return '' ;
      }
    }
    var c = MONOloop.trim( tracker_c{{$tracker->_id}}(i) ) ; 
    
  @endif
  
  MONOloop.trackCart(s , n , p , q , c) ;
  
}

});

