@if($account->invocation['anti_flicker'])
<style> 
    .hidejs body {visibility: hidden; display:none; } 
</style> 
<script type="text/javascript"> 
  ml_root = document.getElementsByTagName('html')[0]; ml_root.className += ' hidejs '; setTimeout(function(){ ml_root.className = ml_root.className.replace(/\bhidejs\b/,''); },{{$account->invocation['timeout']}}); 
</script>
@endif
<script type="text/javascript">
var ML_vars = { cid:{{$account->uid}} };
(function() { var ml = document.createElement('script'); ml.type = 'text/javascript'; ml.async = true; ml.id = 'monoloop_invoke'; ml.src = (("https:" == document.location.protocol) ? 'https://{{Config::get('app.monoloop.cbrhost')}}/'+ML_vars.cid+'_cbr.js' : 'http://{{Config::get('app.monoloop.cbrhost')}}/'+ML_vars.cid+'_cbr.js'); var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ml, s);}) ();
</script>

