@foreach($trackers as $tracker)
  @if( $tracker->urlConfig->inc_www == 1 )
    monoloopTestURL = monoloopURL_includeWWW ;
  @else
    monoloopTestURL = monoloopURL ;
  @endif

  @if( $tracker->urlConfig->reg_ex == '' )
    @if( $tracker->urlConfig->url_option == 0 )
      if( MONOloop.catExactMatch( monoloopTestURL , '{{$tracker->urlConfig->cleanUrl}}' ) ){
    @elseif($tracker->urlConfig->url_option == 1 )
      if( MONOloop.catAllowPara( monoloopTestURL , '{{$tracker->urlConfig->cleanUrl}}' ) ){
    @elseif($tracker->urlConfig->url_option == 2 )
      if( MONOloop.catStartWith( monoloopTestURL , '{{$tracker->urlConfig->cleanUrl}}' ) ){
    @endif
  @else
    if( MONOloop.catCheckRegExpression( monoloopTestURL , '{{$tracker->urlConfig->reg_ex}}'  )){
  @endif

  @if($tracker->tracker->tracker_type == 'search' )
    @include('scripts.post_tracker_search', ['tracker' => $tracker])
  @elseif($tracker->tracker->tracker_type == 'basket' )

  @elseif($tracker->tracker->tracker_type == 'purchase' )
    MONOloop.trackPurchase() ;
  @elseif($tracker->tracker->tracker_type == 'download' )
    @if($tracker->tracker->filter_type_id == 0 )
      MONOloop.setUpTrackerDownload('{{$tracker->tracker->type_data}}' , null) ;
    @else
      MONOloop.setUpTrackerDownload('{{$tracker->tracker->type_data}}' , '{{$tracker->tracker->filter_data}}' ) ;
    @endif
  @else
    @include('scripts.post_tracker_default', ['tracker' => $tracker])
  @endif

  }
@endforeach
