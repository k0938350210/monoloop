@foreach($assets as $asset)
  @if( $asset->urlConfig->inc_www == 1 ) 
    monoloopTestURL = monoloopURL_includeWWW ;
  @else
    monoloopTestURL = monoloopURL ;
  @endif
  
  @if( $asset->urlConfig->reg_ex == '' ) 
    @if( $asset->urlConfig->url_option == 0 ) 
      if( MONOloop.catExactMatch( monoloopTestURL , '{{$asset->urlConfig->cleanUrl}}' ) ){
    @elseif($asset->urlConfig->url_option == 1 ) 
      if( MONOloop.catAllowPara( monoloopTestURL , '{{$asset->urlConfig->cleanUrl}}' ) ){
    @elseif($asset->urlConfig->url_option == 2 ) 
      if( MONOloop.catStartWith( monoloopTestURL , '{{$asset->urlConfig->cleanUrl}}' ) ){
    @endif 
  @else
    if( MONOloop.catCheckRegExpression( monoloopTestURL , '{{$asset->urlConfig->reg_ex}}'  )){ 
  @endif
  
  @foreach($asset->asset->assetDetails as $detail)
    @if( $detail->type_id == 4 )
      function monoloop_assets_customfunction_{{$detail->_id}}() { try{ {!! $detail->type_data !!} }catch(err){ return '' ; } }
    @endif
  @endforeach
  
  var detailObj = new Array();
  var asD = {} ;
  @foreach($asset->asset->assetDetails as $detail)
    @if( $detail->filter_type_id  == 1 && (  $detail->type_id == 1 || $detail->type_id  == 2 || $detail->type_id == 3)  )
      function assets_{{$asset->_id}}(){ 
        try { 
          var selector = MONOloop.find(MONOloop.$(rows[i]) ,'{{$detail->type_id}}');
          {!! $detail->filter_data !!}
        }catch(err) { 
          return ''  ;
        }
      }
      asD['{{$detail->field_name}}'] = MONOloop.trim( assets_{{$asset->_id}}() ) ;
    @elseif( $detail->filter_type_id  == 2 &&  $detail->type_id == 2 )
      var price = MONOloop.getCartInnerContent( MONOloop.$('{{$detail->type_data }}') ) ;
      asD['{{$detail->field_name}}'] = MONOloop.getCartPriceDefaultFilter(price) ;
    @elseif( $detail->filter_type_id  == 3 &&  $detail->type_id == 2 )
      asD['{{$detail->field_name}}'] = MONOloop.getImageFilter('{{$detail->type_data }}') ; 
    @else
      @if( $detail->type_id == 1 ) 
        asD['{{$detail->field_name}}'] = MONOloop.catReadMeta('{{$detail->type_data }}') ;
      @elseif( $detail->type_id == 2 )
        asD['{{$detail->field_name}}'] = MONOloop.catReadXP('{{$detail->type_data }}') ;
      @elseif( $detail->type_id == 3 )
        asD['{{$detail->field_name}}'] = MONOloop.catRegExp('{{$detail->type_data }}') ;
      @elseif( $detail->type_id == 4 )
        if(!MONOloop.isTrackServerSide()){ 
          asD['{{$detail->field_name}}'] =   monoloop_assets_customfunction_{{$detail->_id}}();
          MONOloop.trackAssetsCustom({{$asset->nid}},'{{$detail->field_name}}') ;
        }
      @endif 
    @endif
  @endforeach
    detailObj.push(asD) ;
    MONOloop.trackAssets(2,{{$asset->nid}} , {{$asset->asset->ttl}} , detailObj ) ;
  }
@endforeach