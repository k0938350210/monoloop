MONOloopReady(function() {
  @if( $tracker->tracker->filter_type_id == 0 )
  var delay_{{$tracker->_id}} = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();

  var elements = MONOloop.$('{{$tracker->tracker->search_textbox_xpath}}') ;
  if(elements.length > 0){
    elements[0].addEventListener('keyup',function(event){
      var me = this ;
      var delay =2000 ; 
      if( parseInt(event.keyCode,10) === 13 ){
        delay = 1;
      }
      if(me.value !== ''){
        delay_{{$tracker->_id}}(function(){
          MONOloop.trackST( me.value ) ;
        }, delay );
      }
    });
  }
  @else
  MONOloop.trackST( MONOloop.STPostFunc( MONOloop.val(MONOloop.$('{{$tracker->tracker->search_textbox_xpath}}')) , '{{$tracker->tracker->filter_data}') ) ;
  @endif
}) ;
