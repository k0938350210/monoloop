@extends('layouts.email')

@section('subject', 'Invitation from monoloop')

@section('content')
Hi {{$user->name}},<br> You got monoloop invite from {{$user->account->name}} account.<br/> Please click link to accept invitation <a href="{{$user->invitationURL}}">{{$user->invitationURL}}</a>
@endsection