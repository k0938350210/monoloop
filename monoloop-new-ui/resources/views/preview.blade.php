@extends('layouts.preview')

@section('title', $content->name)

@section('content')
{!! $html !!}
@endsection
