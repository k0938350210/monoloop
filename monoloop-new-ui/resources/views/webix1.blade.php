<!DOCTYPE html>
<html lang="en" ng-app='APP'>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Monoloop Experience Builder</title>

	<script src="{{ elixir('webix/webix.js') }}" type="text/javascript"></script>
	<script src="{{ elixir('webix/filemanager/filemanager.js') }}" type="text/javascript"></script>

	<script src="{{ elixir('js/jstz.min.js') }}" type="text/javascript"></script>
	<script src="/js/clipboard.min.js" type="text/javascript"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>


	<!-- Integrate jquery with webix -->
	<script src="/js/jquery.js" type="text/javascript"></script>
	<script type="text/javascript">
			var jq182 = jQuery.noConflict(true);
	</script>
	<link rel="stylesheet" type="text/css" href="/webix/webix.css?{{Config::get('app.version') }}" media="all" />
  <link rel="stylesheet" type="text/css" href="{{ elixir('webix/filemanager/filemanager.css') }}" media="all" />
	<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"> -->
	<link href="/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ elixir('css/webix.css') }}" media="all" />
	<link rel="stylesheet" type="text/css" href="/webui_popover/jquery.webui-popover.css" media="all" />
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- Ad moment -->
	<script src="/js/moment/moment.min.js" type="text/javascript"></script>
	<script src="/js/moment/moment-timezone-with-data-2010-2020.js" type="text/javascript"></script>

	<script type="text/javascript">
		var MONOloop  = {};
		MONOloop.jq = $;
		var StateData = {
			guidelines: {!! json_encode(AllGuidelines::takeAllGuides()) !!}, tooltips: {!! json_encode(AllGuidelines::takeAllTooltips()) !!}
		};
		var accountName = '{!! Auth::User()->username !!}';
		var activeUser = {!! json_encode(Auth::User()); !!};
		var activeAccount = {!! json_encode(UserAccount::activeAccount()) !!};
		var accountNameUrl = '{!! url("/account/profile") !!}';
		var default_domain = '{{  Auth::User()->account->first_domain }}';
		var logoutUrl = '{!! url("/auth/logout") !!}';
		var accountSelectors = {!! json_encode(UserAccount::accounts()) !!};
		var monolop_base_url = '{{config('app.url')}}' ;
		var monolop_api_base_url = '{{config('app.monoloop_api_base_url')}}' ;
		if(typeof accountSelectors === 'object'){
			var accountSelectors = $.map(accountSelectors, function(value, index) {
			    return [value];
			});
		}
		var sessionData = {!! json_encode(Session::get('UserStateData')) !!};
		var userStateData = '';
		if(sessionData){
			userStateData = JSON.parse(sessionData);
		} else

		{{ Session::set('UserStateData', '') }}
		// console.log('UserStateData => ', {!! json_encode(Session::get('UserStateData')) !!});
		$(document).ready(function(){
			// populateFromSession(userStateData);
		});
		var ajaxHeaders = {'Content-Type': 'application/x-www-form-urlencoded', 'ML-Token': "{{ Session::get('mlToken') }}" };
		webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers , formData){
			if(!formData){
		  	headers["Content-type"]= ajaxHeaders['Content-Type'];
			}
		  headers["ML-Token"]= ajaxHeaders['ML-Token'];
		});

		function getCookie(cname) {
		    var name = cname + "=";
		    var decodedCookie = decodeURIComponent(document.cookie);
		    var ca = decodedCookie.split(';');
		    for(var i = 0; i <ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0) == ' ') {
		            c = c.substring(1);
		        }
		        if (c.indexOf(name) == 0) {
		            return c.substring(name.length, c.length);
		        }
		    }
		    return "";
		}
		if(getCookie('loggedin') != ajaxHeaders['ML-Token']){
			//document.cookie = "loggedin=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
	    document.cookie = "loggedin="+ajaxHeaders['ML-Token'] + "; path=/";
	  }

		// console.log("ajaxHeaders::",ajaxHeaders);
	</script>


	<script src="{{ elixir('webui_popover/jquery.webui-popover.js') }}" type="text/javascript"></script>
	<script src="{{ elixir('js/webix/layout/webix.js') }}" type="text/javascript"></script>
	<script src="{{ elixir('js/tooltips.js') }}" type="text/javascript"></script>
	<script src="{{ elixir('js/guidelines.js') }}" type="text/javascript"></script>
	<script src="/webix/d3/d3.v4.js" type="text/javascript"></script>
	<script src="/webix/d3/d3-funnel.min.js?v=3.0.0.12" type="text/javascript"></script>
	<script src="/js/jquery.counterup.min.js" type="text/javascript"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
	<script>

	</script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js?{{Config::get('app.version') }}"></script>
		<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js?{{Config::get('app.version') }}"></script>
	<![endif]-->
	@yield('customcss')
	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/58105ef7f434cc6ca4628b04/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
	</script>
	<!--End of Tawk.to Script-->
	<!-- Start Of Google Analytics Script -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-25035461-3', 'auto');
  ga('send', 'pageview');
	</script>
	<!-- End Of Google Analytics Script -->
</head>
<body class="ui {{ $nav? "hidenav" : "" }}" cms-page-url="{{$cms_page_url? $cms_page_url : ""}}">
	<header id="header" class="header"></header>
	<div id="wrapper" class="main_cont">
	  <aside class="sidebar">
			<nav id="nav" class="accordion"></nav>
		</aside>
	  <section id="main" style="min-height: 450px;">
	    <div id="mainContainer" style="width: 100%;">
	      @yield('content')
	    </div>
	  </section>
	</div>
	<!-- <div style="clear:both;"></div>
  <footer id="footer"></footer> -->
  @yield('footerjs')

<style>
    .hidejs body {visibility: hidden; display:none; }
</style>
<div id="tooltipBackDrop" class="webui-popover-backdrop tooltipBackDrop" style="display: none;"></div>
</body>
</html>
