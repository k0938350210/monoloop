@extends('ui') 
 

@section('customcss')
    <link rel="stylesheet" type="text/css" href="/extjs/resources/css/ext-all-notheme.css?{{Config::get('app.version') }}" media="all" />
    <link rel="stylesheet" type="text/css" href="/extjs/resources/css/xtheme-gray.css?{{Config::get('app.version') }}" media="all" />
	<script src="/extjs/adapter/ext/ext-base.js?{{Config::get('app.version') }}" type="text/javascript"></script>
	<script src="/extjs/ext-all.js?{{Config::get('app.version') }}" type="text/javascript"></script>
    <script src="/js/extjs3.js?{{Config::get('app.version') }}" type="text/javascript"></script>
@endsection