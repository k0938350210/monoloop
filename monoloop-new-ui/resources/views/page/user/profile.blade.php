@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
  var options = {};
  options.link = config.profile.link;
  options.headerName = config.profile.headerName;
  options.script = config.profile.script;
  options.pageId = config.profile.pageId;

  generatePageContent(options);
});

</script>
@endsection
