@extends('login')

@section('content')

@include('blocks.fullbg')

<div class="wrapper">
  <div class="row">
    <div style="width:700px;margin-left:20px;">
      <div id="login-box" class="panel login-panel">
        <div class="panel-body">
          <h1>Please confirm your registration! </h1>
          <h4>Hi {{ $account_name }}, </h4>
          <p>Thank you very much!</p>
          <p>&nbsp;</p>
          <p>Your information has been sent successfully. In order to complete your registration, please click the confirmation link in the email
          that we have sent to you.</p>
          <p>&nbsp;</p>
          <p>Please check, <strong>whether the email is in the junk folder of your email account. </strong></p>
        </div>
      </div>

    </div>
  </div>
</div>



@endsection
