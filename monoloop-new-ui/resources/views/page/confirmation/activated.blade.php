@extends('login')

@section('content')

@include('blocks.fullbg')

<div class="wrapper">
  <div class="row">
    <div style="width:700px;margin-left:20px;">
      <div id="login-box" class="panel login-panel">
        <div class="panel-body">
           <h1>Already activated! </h1>
          <h4>Hi {{ $user->username }}, </h4>
          <p>Thank you very much!</p>
          <p>&nbsp;</p>
          <p>You already have activated your account. Please <a style="color:blue;" href="{{ config('app.url') }}">click here</a> to login</p>
        </div>
      </div>

    </div>
  </div>
</div>



@endsection
