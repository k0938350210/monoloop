@extends('layouts.mail_landing')

@section('content')

<h1>Welcome to Monoloop!</h1>
<p>You have received an invite to join your team on Monoloop.</p>
<p>If you have an existing Monoloop UserID - you can link the new account to your user - just select Use existing user and log in.</p>
<p>If you are a new user - please select Create new user. This requires you to create your UserID and password and then you are automatically logged in to the new account.</p>
<br /><br />
<div id="mailContainer">

</div>
@endsection

@section('customcss')

  <script type="text/javascript">
    var user = {!! $user !!};

  </script>
  <script src="/js/webix/page/invitation.js?{{Config::get('app.version') }}" type="text/javascript"></script>
@endsection
