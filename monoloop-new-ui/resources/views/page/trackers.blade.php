
@extends('webix')


@section('footerjs')

<script src="/zeroclipboard1.2.3/ZeroClipboard.js?{{Config::get('app.version') }}" type="text/javascript"></script>
<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.trackers.link;
	options.headerName = config.trackers.headerName;
	options.script = config.trackers.script;
	options.pageId = config.trackers.pageId;
	generatePageContent(options);
});

</script>
@endsection
