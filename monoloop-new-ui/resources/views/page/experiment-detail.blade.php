@extends('webix')
@section('footerjs')
<link href="//cdn.rawgit.com/csaladenes/ddc0b326b93ec641c84f/raw/nv.d3.css?{{Config::get('app.version') }}" rel="stylesheet"/>
<script src="//d3js.org/d3.v3.min.js?{{Config::get('app.version') }}"></script>
<script src="//cdn.rawgit.com/csaladenes/ddc0b326b93ec641c84f/raw/nv.d3.min.js?{{Config::get('app.version') }}"></script>
<style>
table {
  border-collapse:collapse;
}
td {
  border:solid 1px;

}
#chart svg {
  width:100%;
  height:300px;
}
</style>



<script type="text/javascript">
webix.ready(function() {
  var options = {};
  options.link = config.dashboard.experimentdetail.link;
  options.headerName = config.dashboard.experimentdetail.headerName;
  options.script = config.dashboard.experimentdetail.script;
  options.pageId = config.dashboard.experimentdetail.pageId;
  generatePageContent(options);
})
</script>
@endsection
