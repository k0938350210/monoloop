@extends('webix') 

@section('content')  
<div style="padding : 0 0 4px 0 ;">
	<div class="header-content" style="float: left;margin: 0 0 0 45px;">Web Content</div>
	<div style="clear: both;"></div>
</div>
<div style="background: white;position: relative;padding: 42px;border:1px solid rgb(204,204,204) ;">
	<div id="create-btn" style="position:absolute;top:50px;left:50px;z-index:150;"></div>
	<div id="main-content"></div>
</div> 
<script type="text/javascript">
var pagelist = {
	view:"datatable",  
	columns:[ 
		{ id:"url",	header:"URL" , fillspace:1 },
		{ id:"description",	header:"Description" },
		{ id:"date", header:"Date" },
		{ id:"action", header:"Actions" },
		{ id:"status", header:"Status" }
	], 
	data: [
		{ id:1, url:'http://www.monoloop.com',description:'detail',date:'17 Apr 2015'},
		{ id:2, url:'http://www.monoloop.com/landing',description:'detail',date:'17 Apr 2015'}
	]
};

var contentlist = {
	view:"datatable",  
	columns:[ 
		{ id:"name",	header:"Description" , fillspace:1 },
		{ id:"type",	header:"Type" },
		{ id:"date", header:"Date" },
		{ id:"action", header:"Actions" },
		{ id:"status", header:"Status" }
	], 
	data: [
		{ id:1, name:'Shopping Cart',type:'Text',date:'17 Apr 2015'},
		{ id:2, name:'Shopping Cart2',type:'Text',date:'17 Apr 2015'}
	]
};
 
 		
webix.ui({
	container:"main-content",
    view:"tabview",  
    tabbar:{
        optionWidth:150
    },
    width:'100%' , 
    height : 440 , 
    cells:[
    	{  
			header:"Page Lists", 
			body:{  
				responsive:true,
				cols:[{
					template:"column 1",
    				width:200	
				},{
		 			view:"resizer"	
				},  pagelist]
			} 
		},
		{
			header:"Content Lists" , 
			body:{  
				responsive:true,
				cols:[{
					template:"column 1",
    				width:200	
				},{
		 			view:"resizer"	
				},  contentlist]
			} 
		} 
	], 
});
 
</script>
@endsection
 