@extends('extjs3') 

@section('content')  
<div style="padding : 0 0 4px 0 ;">
	<div class="header-content" style="float: left;margin: 0 0 0 45px;">Segments</div>
	<div id="search-text" style="float: right;"></div>
	<div style="clear: both;"></div>
</div>
<div style="background: white;position: relative;padding:  21px 42px 42px 42px ;border:1px solid rgb(204,204,204) ;">
	<div id="mdc-content" style="position: relative;">
		<div id="btn-new-segment" style="position: absolute ; top:3px ; left: 82px; z-index: 99;"></div> 
	</div> 
</div>
@endsection

@section('customcss')
  @parent 
  <link rel="stylesheet" type="text/css" href="/extjs/plugins/date-time-ux.css?{{Config::get('app.version') }}" media="all" />
  <link href="{{ asset('/css/extjs3.css') }}" rel="stylesheet">
@endsection

@section('footerjs')
  <script src="/extjs/util/monoloop_base.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <script src="/extjs/util/list_customfield.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  
  <script src="/extjs/plugins/PagingToolbar1.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <script src="/extjs/plugins/ProgressBarPager.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <script src="/extjs/plugins/date-time-ux.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  
  <script src="/js/3/segment/tree.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <script src="/js/3/segment/main.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  
  <script type="text/javascript">
  Ext.onReady(function(){   
		monoloop_base.pid = 0 ;
    monoloop_base.username = '{{Auth::user()->username}}' ; 
    //monoloop_base.loginRefresh = new Ext.ux.MONOLOOP.loginRefresh(); 
    segment_main.init() ; 
	}) ; 
  </script>
@endsection
 