@extends('extjs4_2') 

@section('content') 
<div style="padding : 0 0 0 0 ;">
  <div class="header-content" style="float: left;margin: 0 0 0 45px;">Trackers</div>
  <div id="tracker-search-text" style="float:right;margin: 4px  0 0 0 ;" ></div>
  <div style="clear: both;"></div>
</div>
<div style="background: white;position: relative;padding: 42px;border:1px solid rgb(204,204,204) ;">
  <div id="tracker-create-button" style="position: absolute ; top: 43px ; left: 131px; z-index: 99;"></div>
  <div id="tracker-panel"></div>
</div>
@endsection

@section('customcss')
  @parent  
  <link href="{{ asset('/css/extjs3.css') }}?{{Config::get('app.version') }}" rel="stylesheet">
@endsection
 
@section('footerjs') 
<script src="/js/4_2/apps/tracker/main/app.js?{{Config::get('app.version') }}" type="text/javascript"></script>
<script src="/extjs4.0/util/monoloop_base.js?{{Config::get('app.version') }}" type="text/javascript"></script>
<script type="text/javascript">
Ext.Loader.setConfig({
  enabled: true,
  paths: {
    UI : '/js/4_2' , 
    Tracker : '/js/4_2/apps/tracker/main' , 
    Util : '/js/4_2/shared/util' , 
    Monoloop : '/extjs4.0'  ,
    'Ext.ux' : '/js/4_2/shared/ux'
  },
  disableCaching: true
}); 

Ext.require('Ext.ux.form.MultiSelect'); 

Ext.onReady(function(){
  monoloopLog.enable =  {{config('app.debug')}}; 
  tracker_main.baseUrl = '{{config('app.url')}}/' ;
  tracker_main.temPath = '{{config('app.url')}}/placement-proxy/' ; 
  tracker_main.baseRelativeUrl = '{{config('app.url')}}/' ;
  tracker_main.firstDomain = '{{Auth::user()->account->firstDomain}}' ; 
  tracker_main.init() ; 
  customvars_main.baseUrl = '{{config('app.url')}}/' ;
  customvars_main.baseRelativeUrl = '{{config('app.url')}}/' ;
  customvars_main.temPath = '{{config('app.url')}}/placement-proxy/' ; 
  customvars_main.firstDomain = '{{Auth::user()->account->firstDomain}}' ; 
  
  assets.baseUrl = '{{config('app.url')}}/' ;
  assets.temPath = '{{config('app.url')}}/placement-proxy/' ; 
  assets.pid = 0 ; 
}) ; 
</script>
@endsection