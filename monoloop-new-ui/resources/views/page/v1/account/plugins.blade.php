@extends('extjs4_2') 

@section('content') 
<div style="padding : 0 0 4px 0 ;">
	<div class="header-content" style="float: left;margin: 0 0 0 45px;">Plugins</div>
	<div style="clear: both;"></div>
</div>
<div style="background: white;position: relative;padding: 42px;border:1px solid rgb(204,204,204) ;">
	<div id="main-content"></div>
</div>
@endsection

@section('footerjs')
<script src="/js/4_2/apps/account/integration/app.js?{{Config::get('app.version') }}" type="text/javascript"></script>
<script type="text/javascript">
Ext.Loader.setConfig({
    enabled: true,
    paths: {
        UI : '/js/4_2' , 
        Integration : '/js/4_2/apps/account/integration' , 
        Util : '/js/4_2/shared/util'
    },
    disableCaching: true
}); 
</script>
@endsection
 