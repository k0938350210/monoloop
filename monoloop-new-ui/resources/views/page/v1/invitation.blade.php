@extends('layouts.mail_landing') 

@section('content') 

<h1>Welcome to Monoloop!</h1>
<p>You have received an invite to join your team on Monoloop.</p>
<p>If you have an existing Monoloop UserID - you can link the new account to your user - just select Use existing user and log in.</p>
<p>If you are a new user - please select Create new user. This requires you to create your UserID and password and then you are automatically logged in to the new account.</p>
<br /><br />
<h1>Hi {{$user->name}}, </h1>
Account  {{$user->account->name}} send invitation to you . <br />
<a href="#" onclick="invitation.createnewuser() ; return false ; " >Create new user.</a>&nbsp;or&nbsp;<a href="#" onclick="invitation.loginuser() ; return false ;">Use exists user.</a>

@endsection
 
@section('customcss') 
  <link rel="stylesheet" type="text/css" href="/extjs4.0/resources/css/ext-all-gray.css?{{Config::get('app.version') }}" media="all" />
	<script src="/extjs4.0/ext-all.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <script src="/extjs4.0/util/monoloop_base.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <script src="/js/invitation/invitation.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <script type="text/javascript">
    Ext.onReady(function(){
      invitation.name = '{{$user->name}}' ; 
      invitation.email = '{{$user->email}}' ; 
      invitation.comment = '{{$user->invitation}}' ; 
      invitation.baseURL = '{{ url() }}' ; 
      Ext.Loader.setConfig({enabled:true}); 
    }) ;
  </script>
@endsection