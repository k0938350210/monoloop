@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
  var options = {};
  options.link = config.users.link;
  options.headerName = config.users.headerName;
  options.script = config.users.script;
  options.pageId = config.users.pageId;
  generatePageContent(options);
});

</script>
@endsection
