
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.accountPlugin.link;
	options.headerName = config.accountPlugin.headerName;
	options.script = config.accountPlugin.script;
	options.pageId = config.accountPlugin.pageId;
	generatePageContent(options);
});

</script>
@endsection
