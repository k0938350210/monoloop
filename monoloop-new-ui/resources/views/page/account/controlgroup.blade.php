@extends('webix')


@section('footerjs')

<script src="/zeroclipboard1.2.3/ZeroClipboard.js?{{Config::get('app.version') }}" type="text/javascript"></script>
<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.controlGroup.link;
	options.headerName = config.controlGroup.headerName;
	options.script = config.controlGroup.script;
	options.pageId = config.controlGroup.pageId;
	generatePageContent(options);
});

</script>
@endsection
