
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.accountLog.link;
	options.headerName = config.accountLog.headerName;
	options.script = config.accountLog.script;
	options.pageId = config.accountLog.pageId;
	generatePageContent(options);
});

</script>
@endsection
