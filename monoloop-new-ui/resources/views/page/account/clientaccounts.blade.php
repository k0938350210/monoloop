
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.clientAccounts.link;
	options.headerName = config.clientAccounts.headerName;
	options.script = config.clientAccounts.script;
	options.pageId = config.clientAccounts.pageId;
	generatePageContent(options);
});

</script>
@endsection
