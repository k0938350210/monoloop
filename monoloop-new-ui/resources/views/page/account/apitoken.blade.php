
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.accountApiToken.link;
	options.headerName = config.accountApiToken.headerName;
	options.script = config.accountApiToken.script;
	options.pageId = config.accountApiToken.pageId;
	generatePageContent(options);
});

</script>
@endsection
