@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
  var options = {};
  options.link = config.accountProfile.link;
  options.headerName = config.accountProfile.headerName;
  options.script = config.accountProfile.script;
  options.pageId = config.accountProfile.pageId;
  
  generatePageContent(options);
});

</script>
@endsection
