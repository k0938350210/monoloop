
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.accountApplication.link;
	options.headerName = config.accountApplication.headerName;
	options.script = config.accountApplication.script;
	options.pageId = config.accountApplication.pageId;
	generatePageContent(options);
});

</script>
@endsection
