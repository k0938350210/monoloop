@extends('webix')


@section('footerjs')

<script src="/zeroclipboard1.2.3/ZeroClipboard.js?{{Config::get('app.version') }}" type="text/javascript"></script>
<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.codenScript.link;
	options.headerName = config.codenScript.headerName;
	options.script = config.codenScript.script;
	options.pageId = config.codenScript.pageId;
	generatePageContent(options);
});

</script>
@endsection
