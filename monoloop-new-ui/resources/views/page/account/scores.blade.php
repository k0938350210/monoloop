@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
  var options = {};
  options.link = config.accountScore.link;
  options.headerName = config.accountScore.headerName;
  options.script = config.accountScore.script;
  options.pageId = config.accountScore.pageId;
  generatePageContent(options);
});

</script>
@endsection
