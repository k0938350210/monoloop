
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.accountDomain.link;
	options.headerName = config.accountDomain.headerName;
	options.script = config.accountDomain.script;
	options.pageId = config.accountDomain.pageId;
	generatePageContent(options);
});

</script>
@endsection
