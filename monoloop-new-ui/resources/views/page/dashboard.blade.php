
@extends('webix')


@section('footerjs')

<script type="text/javascript">
	webix.ready(function() {
		var options = {};
		options.link = config.dashboard.link;
		options.headerName = config.dashboard.headerName;
		options.script = config.dashboard.script;
		options.pageId = config.dashboard.pageId;
		generatePageContent(options);
	});
</script>
@endsection
