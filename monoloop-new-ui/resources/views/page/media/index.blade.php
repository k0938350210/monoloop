
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.media.link;
	options.headerName = config.media.headerName;
	options.script = config.media.script;
	options.pageId = config.media.pageId;
	generatePageContent(options);
});

</script>
@endsection
