@extends('layouts.mail_landing')

@section('content')
<div class="container">
	<div class="col-md-6">
		<h1>Welcome to Monoloop!</h1>
		<p>You have received an invite to join your team on Monoloop.</p>
		<p>Please select a password for your account access and enter your full name.</p>
	</div>
	<div class="col-md-6">
		<div id="mailContainer">

		</div>
	</div>

</div>
@endsection

@section('customcss')

  <script type="text/javascript">
    var user = {!! $user !!};
    var invitetype = '{!! $type !!}';
  </script>
  <script src="{{ elixir('js/webix/page/invitation/user.js') }}" type="text/javascript"></script>
@endsection
