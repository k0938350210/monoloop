
@extends('webix')


@section('footerjs')

<script type="text/javascript">
console.log("monoloop_api_base_url",monolop_api_base_url);
webix.ready(function() {
	var options = {};
	options.experiment_id = '{{ $experiment_id }}';
	options.link = config.editexperiment.link;
	options.headerName = config.editexperiment.headerName;
	options.script = config.editexperiment.script;
	options.pageId = config.editexperiment.pageId;
	generatePageContent(options);
});

</script>
@endsection
