
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.experiment.link;
	options.headerName = config.experiment.headerName;
	options.script = config.experiment.script;
	options.pageId = config.experiment.pageId;
	generatePageContent(options);
});

</script>
@endsection
