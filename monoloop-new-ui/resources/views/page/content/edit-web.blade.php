
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.page_id = '{{ $page_id }}';
	options.link = config.editcontentWeb.link;
	options.headerName = config.editcontentWeb.headerName;
	options.script = config.editcontentWeb.script;
	options.pageId = config.editcontentWeb.pageId;
	generatePageContent(options);
});

</script>
@endsection
