
@extends('webix')


@section('footerjs')

<script src="/zeroclipboard1.2.3/ZeroClipboard.js?{{Config::get('app.version') }}" type="text/javascript"></script>
<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.tracker.link;
	options.headerName = config.tracker.headerName;
	options.script = config.tracker.script;
	options.pageId = config.tracker.pageId;
	generatePageContent(options);
});

</script>
@endsection
