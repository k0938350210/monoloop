
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.addsegment.link;
	options.headerName = config.addsegment.headerName;
	options.script = config.addsegment.script;
	options.pageId = config.addsegment.pageId;
	generatePageContent(options);
});

</script>
@endsection
