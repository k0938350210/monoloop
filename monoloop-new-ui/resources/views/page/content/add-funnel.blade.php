
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.addfunnel.link;
	options.headerName = config.addfunnel.headerName;
	options.script = config.addfunnel.script;
	options.pageId = config.addfunnel.pageId;
	generatePageContent(options);
});

</script>
@endsection
