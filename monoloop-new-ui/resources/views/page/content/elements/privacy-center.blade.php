
<link type="text/css" rel="stylesheet" href="//mlres.s3.amazonaws.com/privacy_center/pc.min.css?v=2.0.4">
<script type="text/javascript">

	window.ml_pc_var ={
		app_url : "{{ config("app.url") }}" ,
		@if( config("app.debug"))
		frontend_url : "//{{ config("services.frontend.invoke") }}" ,
		@else
		frontend_url : "//"+ML_vars.cid+"{{ config("services.frontend.invoke") }}" ,
		@endif

		@if($preview)
		preview : true ,
		@else
		preview : false ,
		@endif
		pvc : <%= MonoloopProfile.PageViewCount %>
	};
	MONOloop.callURL('//mlres.s3.amazonaws.com/privacy_center/pc.min.js?v=2.0.4', function() {
		_ml_html_1();
		_ml_init();
  });
</script>
<style>
._ml_profile_dialog {
	opacity:0;
}
</style>
<div>
	<div id="_ml_pc_container"></div>
</div>
<div id="_ml_profile_modal" class="_ml_profile_dialog" >
</div>
