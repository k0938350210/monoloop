
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.contentBluePrints.link;
	options.headerName = config.contentBluePrints.headerName;
	options.script = config.contentBluePrints.script;
	options.pageId = config.contentBluePrints.pageId;
	generatePageContent(options);
});

</script>
@endsection
