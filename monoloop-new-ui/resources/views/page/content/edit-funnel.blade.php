
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.funnel_id = '{{ $funnel_id }}';
	options.link = config.editfunnel.link;
	options.headerName = config.editfunnel.headerName;
	options.script = config.editfunnel.script;
	options.pageId = config.editfunnel.pageId;
	generatePageContent(options);
});

</script>
@endsection
