
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.addTracker.link;
	options.headerName = config.addTracker.headerName;
	options.script = config.addTracker.script;
	options.pageId = config.addTracker.pageId;
	generatePageContent(options);
});

</script>
@endsection
