
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.webhook.link;
	options.headerName = config.webhook.headerName;
	options.script = config.webhook.script;
	options.pageId = config.webhook.pageId;
	generatePageContent(options);
});

</script>
@endsection
