
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.goal_id = '{{ $goal_id }}';
	options.link = config.editgoals.link;
	options.headerName = config.editgoals.headerName;
	options.script = config.editgoals.script;
	options.pageId = config.editgoals.pageId;
	generatePageContent(options);
});

</script>
@endsection
