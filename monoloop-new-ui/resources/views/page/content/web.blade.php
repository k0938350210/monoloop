
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.contentWeb.link;
	options.headerName = config.contentWeb.headerName;
	options.script = config.contentWeb.script;
	options.pageId = config.contentWeb.pageId;
	generatePageContent(options);
});

</script>
@endsection
