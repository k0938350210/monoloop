
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.addexperiment.link;
	options.headerName = config.addexperiment.headerName;
	options.script = config.addexperiment.script;
	options.pageId = config.addexperiment.pageId;
	generatePageContent(options);
});

</script>
@endsection
