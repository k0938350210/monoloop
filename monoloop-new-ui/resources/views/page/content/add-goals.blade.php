
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.addgoals.link;
	options.headerName = config.addgoals.headerName;
	options.script = config.addgoals.script;
	options.pageId = config.addgoals.pageId;
	generatePageContent(options);
});

</script>
@endsection
