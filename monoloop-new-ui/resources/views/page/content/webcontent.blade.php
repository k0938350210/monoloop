
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.contentWebContent.link;
	options.headerName = config.contentWebContent.headerName;
	options.script = config.contentWebContent.script;
	options.pageId = config.contentWebContent.pageId;
	generatePageContent(options);
});

</script>
@endsection
