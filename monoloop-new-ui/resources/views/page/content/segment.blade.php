
@extends('webix')


@section('footerjs')

<script src="/zeroclipboard1.2.3/ZeroClipboard.js?{{Config::get('app.version') }}" type="text/javascript"></script>
<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.segment.link;
	options.headerName = config.segment.headerName;
	options.script = config.segment.script;
	options.pageId = config.segment.pageId;
	generatePageContent(options);
});

</script>
@endsection
