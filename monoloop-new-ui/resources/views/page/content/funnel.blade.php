
@extends('webix')


@section('footerjs')
<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.funnel.link;
	options.headerName = config.funnel.headerName;
	options.script = config.funnel.script;
	options.pageId = config.funnel.pageId;
	generatePageContent(options);
});

</script>
@endsection
