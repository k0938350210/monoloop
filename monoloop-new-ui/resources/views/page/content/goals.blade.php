
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.goals.link;
	options.headerName = config.goals.headerName;
	options.script = config.goals.script;
	options.pageId = config.goals.pageId;
	generatePageContent(options);
});

</script>
@endsection
