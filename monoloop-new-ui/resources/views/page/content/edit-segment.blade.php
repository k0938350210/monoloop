
@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.segment_id = '{{ $segment_id }}';
	options.link = config.editsegment.link;
	options.headerName = config.editsegment.headerName;
	options.script = config.editsegment.script;
	options.pageId = config.editsegment.pageId;
	generatePageContent(options);
});

</script>
@endsection
