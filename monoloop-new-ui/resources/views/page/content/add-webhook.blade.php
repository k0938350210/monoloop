@extends('webix')


@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
	var options = {};
	options.link = config.addwebhook.link;
	options.headerName = config.addwebhook.headerName;
	options.script = config.addwebhook.script;
	options.pageId = config.addwebhook.pageId;
	generatePageContent(options);
});

</script>
@endsection
