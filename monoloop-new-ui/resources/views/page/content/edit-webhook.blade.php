
@extends('webix')


@section('footerjs')

<script type="text/javascript">
console.log("monoloop_api_base_url",monolop_api_base_url);
webix.ready(function() {
	var options = {};	
	options.webhook_id = '{{ $webhook_id }}';
	options.link = config.editwebhook.link;
	options.headerName = config.editwebhook.headerName;
	options.script = config.editwebhook.script;
	options.pageId = config.editwebhook.pageId;
	generatePageContent(options);
});

</script>
@endsection
