<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="LiveLong" />

	<link rel="shortcut icon" type="image/png" href="/js/test/jasmine/jasmine_favicon.png"/>
	<link rel="stylesheet" type="text/css" href="/js/test/jasmine/jasmine.css?{{Config::get('app.version') }}" />
	<script type="text/javascript" src="/js/test/jasmine/jasmine.js?{{Config::get('app.version') }}"></script>
	<script type="text/javascript" src="/js/test/jasmine/jasmine-html.js?{{Config::get('app.version') }}"></script>
	<script type="text/javascript" src="/js/test/jasmine/boot.js?{{Config::get('app.version') }}"></script>
	<!-- include source files here... -->
	<script type="text/javascript">
	var ML_vars = { cid:1 };
	</script>
	<script type="text/javascript" src="/fileadmin/cbr/1_cbr.js?{{Config::get('app.version') }}"></script>
	<script type="text/javascript" src="/fileadmin/cbr/1_post.js?{{Config::get('app.version') }}"></script>

	<!-- include spec files here... -->
	<script type="text/javascript" src="/js/test/postSpec.js?{{Config::get('app.version') }}"></script>
	<script type="text/javascript" src="/js/test/postJquerySpec.js?{{Config::get('app.version') }}"></script>
	<script type="text/javascript">
	/*
	MONOloopReady(function() {
		MONOloop.MonoloopData = {}
		MONOloop.MonoloopData.data = [] ;
		var obj = {
	        mappedTo : 'body' ,
	        placementType : '3',
	        content : "<script type='text\/javascript'>alert(1);<\/script>123",
	        abg : false
	 	} ;
		MONOloop.MonoloopData.data.push(obj) ;
	 	MONOloop.append(MONOloop.$(obj['mappedTo']), obj['content'])
	}) ;
	*/
	</script>

	<title>Invocation - unit test</title>
</head>

<body>


<div id="test" style="display: none;">

</div>
</body>
</html>
