<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Zen Flat Welcome Email</title>
  <style type="text/css" media="screen">

    /* Force Hotmail to display emails at full width */
    .ExternalClass {
      display: block !important;
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    body,
    p,
    td {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 15px;
      color: #333333;
      line-height: 1.5em;
    }


    .background {
      background-color: #333333;
    }

    table.background {
      margin: 0;
      padding: 0;
      width: 100% !important;
    }

    .block-img {
      display: block;
      line-height: 0;
    }

    a {
      color: white;
      text-decoration: none;
    }

    a,
    a:link {
      color: #2A5DB0;
      text-decoration: underline;
    }

    table td {
      border-collapse: collapse;
    }

    td {
      vertical-align: top;
      text-align: left;
    }

    .wrap {
      width: 600px;
    }

    .wrap-cell {
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .header-cell,
    .body-cell,
    .footer-cell {
      padding-left: 20px;
      padding-right: 20px;
    }

    .header-cell {
      background-color: #eeeeee;
      font-size: 24px;
      color: #ffffff;
    }

    .body-cell {
      background-color: #ffffff;
      padding-top: 30px;
      padding-bottom: 34px;
    }

    .footer-cell {
      background-color: #eeeeee;
      text-align: center;
      font-size: 13px;
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .card {
      width: 400px;
      margin: 0 auto;
    }

    .data-heading {
      text-align: right;
      padding: 10px;
      background-color: #ffffff;
      font-weight: bold;
    }

    .data-value {
      text-align: left;
      padding: 10px;
      background-color: #ffffff;
    }

    .force-full-width {
      width: 100% !important;
    }

  </style>
  <style type="text/css" media="only screen and (max-width: 600px)">
    @media only screen and (max-width: 600px) {
      body[class*="background"],
      table[class*="background"],
      td[class*="background"] {
        background: #eeeeee !important;
      }

      table[class="card"] {
        width: auto !important;
      }

      td[class="data-heading"],
      td[class="data-value"] {
        display: block !important;
      }

      td[class="data-heading"] {
        text-align: left !important;
        padding: 10px 10px 0;
      }

      table[class="wrap"] {
        width: 100% !important;
      }

      td[class="wrap-cell"] {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    }
  </style>

<style type="text/css">

</style>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="" style=" background-color: #333333;" >
  <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" >
    <tr>
      <td align="center" valign="top" width="100%">
        <center>
          <table cellpadding="0" cellspacing="0" width="600">
            <tr>
              <td valign="top" style="padding-top:30px; padding-bottom:30px;">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                  <tr>
                   <td height="40" valign="top" style="padding-top:5px;padding-left: 20px;padding-right: 20px;background-color: #eeeeee;font-size: 24px;color: #ffffff;">
                      <img width="144" src="https://i0.wp.com/www.monoloop.com/wp-content/uploads/2016/11/newLogo.png?fit=144%2C40&ssl=1" alt="logo">
                    </td>
                  </tr>
                  <tr>
                    <td valign="top" style="padding-left: 20px;padding-right: 20px;background-color: #ffffff;padding-top: 30px;padding-bottom: 34px;">
                      @yield('content')
                    </td>
                  </tr>
                  <tr>
                    <td valign="top" style="padding-left: 20px;padding-right: 20px;background-color: #eeeeee;text-align: left;padding-top: 10px;padding-bottom: 10px;">
                      <p style="font-size:12px;">Monoloop enable you to build rich customer profiles, implement omnichannel real time personalization on any website and build more engaging, relevant user experiences.</p>
                      <p style="font-size:12px;"><a href="www.monoloop.com">Visit us here www.monoloop.com</a></p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </center>
      </td>
    </tr>
  </table>

</body>
</html>
