<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Monoloop</title>

	<script src="/webix/webix.js?{{Config::get('app.version') }}" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="/webix/webix.css?{{Config::get('app.version') }}" media="all" />

	<!-- Integrate jquery with webix -->
	<script src="/js/jquery.js?{{Config::get('app.version') }}" type="text/javascript"></script>

	<link href="{{ elixir('build/css/monoloop.css') }}" rel="stylesheet">
	<link href="/css/font-awesome.min.css?{{Config::get('app.version') }}" rel="stylesheet">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&{{Config::get('app.version') }}' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js?{{Config::get('app.version') }}"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js?{{Config::get('app.version') }}"></script>
	<![endif]-->

	@yield('customcss')
</head>
<body class="ui">
	<header class="header">
		<div class="logo">
			<a href="/" style="margin-top:2px;"><img src="/images/logo_new.png" alt="" /></a>
		</div>
	</header>
    <section id="main2" style="min-height: 450px;background: none repeat scroll 0 0 #F1F1F1;">
        <div class="container-fluid" style="font-size: 13px;">
            @yield('content')
        </div>
    </section>
    <footer id="footer">
 		<p>By Monoloop</p>
    </footer>
    @yield('footerjs')
</body>
</html>
