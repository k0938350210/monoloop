<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>MONOLOOP ADMIN</title>
 	<link href="{{ elixir('admin/css/core.css') }}" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Oswald:300normal,400normal,700normal|Open+Sans:400normal|Droid+Sans:400normal|Pacifico:400normal|Lato:400normal|Josefin+Slab:400normal|Roboto:400normal|Open+Sans+Condensed:300normal|PT+Sans+Narrow:400normal|Allura:400normal|Merriweather:400normal&amp&{{Config::get('app.version') }};subset=all"/>
	@yield('headercss')
</head>
<body class="admin">
    <div class="container-fluid">
        @include('admin/blocks/leftmenu',array('items' =>array(
			array('href'=>'overview','title'=>'Overview','icon'=>'fa-home','sub'=>array(
			  array('path' => 'mladmin/accounts*','href'=>'/mladmin/accounts' , 'title'=>'Accounts','icon' => 'fa-file-text-o' ) ,
				array('path' => 'mladmin/users*','href'=>'/mladmin/users' , 'title'=>'Users','icon' => 'fa-file-text-o' ) ,
				array('path' => 'mladmin/account/usage','href'=>'/mladmin/account/usage' , 'title'=>'Usage','icon' => 'fa-file-text-o' ) ,
				array('path' => 'mladmin/account/usage_per_account*','href'=>'/mladmin/account/usage_per_account' , 'title'=>'Usage By Account','icon' => 'fa-file-text-o' ) ,
			)),

			array('href'=>'page-menu','title'=>'Pages','icon'=>'fa-file-text-o','sub'=>array(
				array('path' => 'mladmin/content_templates*','href'=>'/mladmin/content_templates' , 'title'=>'Content Template','icon' => 'fa-file-text-o' ) ,
			)),

			array('href'=>'system','title'=>'System','icon'=>'fa-cogs','sub'=>array(
				array('path' => 'mladmin/logs*','href'=>'/mladmin/logs' , 'title'=>'Logs','icon' => 'fa-file-text-o' ) ,
			)),
		)))
        <div id="content">
            @include('admin/blocks/navbar')
            @yield('content')
        </div>

    </div>
    <script src="{{ elixir('admin/js/core.js') }}"></script>
		<script src="{{ elixir('admin/js/admin.js') }}"></script>
    @yield('footerjs')
</body>
</html>
