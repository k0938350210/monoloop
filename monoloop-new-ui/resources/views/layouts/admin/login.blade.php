<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>Monoloop - Admin</title>
	<link href="{{ elixir('admin/css/core.css') }}" rel="stylesheet">
 	@yield('headercss')
</head>
<body class="login">
	@yield('content')
  <script src="{{ elixir('admin/js/core.js') }}"></script>
  @yield('footerjs')
</body>
</html>
