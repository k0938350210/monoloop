<div class="form-group">
	{!! Form::label('name', 'Name' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('template', 'Template' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::textarea('template', Input::old('template'), array('class' => 'form-control ckeditor')) !!}
	</div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
		{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
		<a href="{{ URL::to('mladmin/content_templates/') }}">&nbsp;Cancel</a>
	</div>
</div>
