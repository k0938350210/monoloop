@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/mladmin">Admin</a></li>
    <li><a href="/mladmin/pages">Pages</a></li>
    <li class="active">Content templates</li>
  </ol>
</div>

<div class="innerLR">
  <div class="row manage-header">
    <div class="col-lg-6">
    	<h2>Content Templates Management</h2>
    </div>
    <div class="col-lg-2">
    	<a class="btn btn-small btn-primary" href="{{ URL::to('mladmin/content_templates/create') }}">Create new template</a>
    </div>
  </div>
  <div class="widget">
    {!! Notification::showAll() !!}
    <div class="table-responsive">
      <table class="table">
        <thead>
	         <tr>
    				<th width="45">ID</th>
    				<th>Title</th>
    				<th>Updated</th>
    				<th>Actions</th>
		       </tr>
       </thead>
	     <tbody>
    		@foreach($content_templates as $content_template)
    			<tr>
    				<td>{{ $content_template->_id }}</td>
    				<td>{{ $content_template->name }}</td>
    				<td>{{ $content_template->updated_at }}</td>
    				<td>
    					<a class="btn btn-small btn-info" href="{{ URL::to('mladmin/content_templates/' . $content_template->_id . '/edit') }}">Edit</a>
    				</td>
    			</tr>
    		@endforeach
    		</tbody>
    	</table>
        <div class="text-center">
	      </div>
      </div>
    </div>
</div>
@stop
