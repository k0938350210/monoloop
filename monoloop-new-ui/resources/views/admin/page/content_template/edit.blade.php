@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/mladmin">Admin</a></li>
    <li><a href="/mladmin/pages">Pages</a></li>
    <li><a href="/mladmin/content_templates">Content templates</a></li>
    <li class="active">Edit temaplate - {{ $content_template->name }}</li>
  </ol>
</div>


<div class="innerLR">
  <div class="row manage-header">
    <div class="col-lg-7">
    	<h2>Edit template - {{ $content_template->name }}</h2>
    </div>
	</div>

  <div class="widget">
      {!! Notification::showAll() !!}
      {!! Form::model($content_template, array('url' => 'mladmin/content_templates/'. $content_template->_id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
      	 @include('admin/page/content_template/form')
      {!! Form::close() !!}
  </div>
</div>
@stop
