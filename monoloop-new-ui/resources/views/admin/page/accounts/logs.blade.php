<div class="innerLR row">
  <h4>Logs</h4>
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <td>Date</td>
      <td>Description</td>
      <td>By</td>
    </tr>
  </thead>
  <tbody>
    @foreach($logs as $log)
    <tr>
      <td>{{$log->updated_at}}</td>
      <td>{{$log->msg}}</td>
      <td>{{$log->user->username}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
