@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/mladmin">Admin</a></li>
    <li><a href="/mladmin/accounts">Accounts</a></li>
    <li class="active">Account - {{ $account->company }}</li>
  </ol>
</div>

<div class="innerLR">
  {!! Notification::showAll() !!}
  <div class="row">
    <div class="col-md-6">
      <div class="widget">
        @include('admin/page/accounts/detail')
      </div>

      <div class="widget">
        @include('admin/page/accounts/logs')
      </div>
    </div>
    <div class="col-md-6">
      <div class="widget">
        @include('admin/page/accounts/invoice')
      </div>
      <div class="widget">
        @include('admin/page/accounts/report')
      </div>
    </div>
  </div>
</div>

@stop


@section('footerjs')
  <script type="text/javascript">
  $( document ).ready(function() {
    $('.input-daterange').datepicker({
    });
  });
  </script>
@stop
