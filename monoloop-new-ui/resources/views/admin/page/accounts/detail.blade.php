{!! Form::model($account, array('url' => 'mladmin/accounts/'. $account->_id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
  <div class="innerLR row">
    <h4>Detail</h4>
  </div>
  <table class="table table-striped">
    <tr>
      <td class="col-md-4">CID</td>
      <td>{{$account->uid}}</td>
    </tr>
    <tr>
      <td class="col-md-4">Company</td>
      <td>{{$account->company}}</td>
    </tr>
    <tr>
      <td class="col-md-4">Contact Name</td>
      <td>{{$account->contact_name}}</td>
    </tr>
    <tr>
      <td class="col-md-4">Contact Email</td>
      <td>{{$account->contact_email}}</td>
    </tr>
    <tr>
      <td class="col-md-4">Create At</td>
      <td>{{$account->created_at}}</td>
    </tr>
    <tr>
      <td class="col-md-4">Account Type</td>
      <td>{!! Form::select('account_type', [
           'enterprise' => 'Enterprise',
           'agency' => 'Agency']
        ) !!}</td>
    </tr>
    <tr>
    <td class="col-md-4">Payment Status</td>
      <td>{!! Form::select('AccType', [
           'Free' => 'Free',
           'Paid' => 'Paid']
        ) !!}</td>
    </tr>
    <tr>
      <td class="col-md-4"></td>
      <td><input type="submit" value="Update" class="btn btn-primary" /></td>
    </tr>
  </table>
{!! Form::close() !!}
