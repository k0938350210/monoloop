@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/mladmin">Admin</a></li>
    <li><a href="/mladmin/accounts">Accounts</a></li>
    <li><a href="/mladmin/accounts/{{$account->_id}}">Account - {{ $account->name }}</a></li>
    <li class="active">Create new invoice</li>
  </ol>
</div>

<div class="innerLR">
  <div class="row manage-header">
    <div class="col-lg-7">
    	<h2>Create new invoice</h2>
    </div>
 	</div>

 	<div class="widget">
  {!! Notification::showAll() !!}

 	{!! Form::open(array('url' => '/mladmin/accounts/' . $account->_id . '/invoice'  , 'class'=>'form-horizontal' , 'role' => 'form' , 'files'=>true )) !!}
    @include('admin/page/accounts/invoice/form')
 	{!! Form::close() !!}
 	</div>
</div>

@stop
