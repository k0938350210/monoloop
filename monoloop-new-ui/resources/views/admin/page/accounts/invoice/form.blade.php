<div class="form-group">
	{!! Form::label('invoiceDate', 'Invoice Day' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('invoiceDate', Input::old('invoiceDate'), array('class' => 'form-control')) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('includedMonlyVolume', 'Included Monly Volume' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('includedMonlyVolume', Input::old('includedMonlyVolume'), array('class' => 'form-control')) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('price', 'Price' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('price', Input::old('price       '), array('class' => 'form-control')) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('currency', 'Currency' , array('class'=>'col-sm-2 control-label')) !!}
	<div class="col-sm-8">
		{!! Form::text('currency', Input::old('currency'), array('class' => 'form-control')) !!}
	</div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
		{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
		<a href="{{ URL::to('mladmin/accounts/'. $account->_id ) }}">&nbsp;Cancel</a>
	</div>
</div>
