{!! Form::model($user, array('url' => 'mladmin/users/'. $user->_id , 'method' => 'PUT'  , 'class'=>'form-horizontal' , 'role' => 'form' )) !!}
  <div class="innerLR row">
    <h4>Detail</h4>
  </div>
  <table class="table table-striped">
    <tr>
      <td class="col-md-4">User</td>
      <td>{{$user->username}}</td>
    </tr>
    <tr>
      <td class="col-md-4">Create</td>
      <td>{{$user->created_at->format('d/m/y H:i')}}</td>
    </tr>
    <tr>
      <td class="col-md-4">Update</td>
      <td>{{$user->updated_at->format('d/m/y H:i')}}</td>
    </tr>
    <tr>
      <td class="col-md-4">Is Admin</td>
      <td>{!! Form::checkbox('admin') !!}</td>
    </tr>
    <tr>
      <td class="col-md-4"></td>
      <td><input type="submit" value="Update" class="btn btn-primary" /></td>
    </tr>
  </table>
{!! Form::close() !!}
