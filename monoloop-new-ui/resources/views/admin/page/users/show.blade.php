@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/mladmin">Admin</a></li>
    <li><a href="/mladmin/users">Users</a></li>
    <li class="active">User - {{ $user->username }}</li>
  </ol>
</div>

<div class="innerLR">
  {!! Notification::showAll() !!}
  <div class="row">
    <div class="col-md-3">
      <div class="widget">
        @include('admin/page/users/detail')
      </div>
    </div>
    <div class="col-md-9">
      <div class="widget">
        @include('admin/page/users/accounts')
      </div>
    </div>
  </div>
</div>

@stop
