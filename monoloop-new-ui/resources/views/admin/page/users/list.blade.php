@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Users</li>
    </ol>
</div>


<div class="innerLR">
  <div class="row manage-header">
        <div class="col-lg-7">
        	<h2>Users Management</h2>
        </div>
        <div class="col-lg-5">
            {!! Form::open(array( 'method' => 'get')) !!}
                 <input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" />
            {!! Form::close() !!}
        </div>
    </div>
  <div class="widget">
    {!! Notification::showAll() !!}
    <table class="table">
      <thead>
        <tr>
          <th>Username</th>
          <th class="text-right">Total Account</th>
          <th>Updated</th>
          <th>Created</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
      @foreach($users as $k => $user)
      <tr>
        <td>{{ $user->username}} @if($user->isAdmin)<span class="label label-danger">admin</span> @endif</td>
        <td class="text-right">{{ $user->totalAccounts}}</td>
        <td>
          @if($user->updated_at)
          {{ $user->updated_at->format('d/m/Y H:i:s') }}
          @endif
        </td>
        <td>
          @if($user->created_at)
          {{ $user->created_at->format('d/m/Y H:i:s') }}
          @endif
        </td>
        <td><a class="btn btn-primary " href="/mladmin/users/{{$user->_id}}"><i class="fa fa-file-text-o"></i></a></td>
      </tr>
      @endforeach
      </tbody>
    </table>
    <div class="text-center">
  		<?php echo $users->appends(array('q' => $q))->render(); ?>
  	</div>
  </div>
</div>
@stop
