<div class="innerLR row">
  <h4>Accounts ({{ $user->accounts()->count()}})</h4>
  <table class="table table-striped">
    <thead>
      <th>Name</th>
      <th>Cid</th>
      <th>Email</th>
      <th>Company</th>
      <th>Name</th>
      <th>Create</th>
    </thead>

    <tbody>
      @foreach( $user->accounts as $account)
      <tr>
        <td>{{ $account->account->name}}</td>
        <td>{{ $account->account->uid}}</td>
        <td>{{ $account->email}}</td>
        <td>{{ $account->company}}</td>
        <td>{{ $account->name}}</td>
        <td>@if($account->created_at) {{ $account->created_at->format('d/m/y H:i') }} @endif</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
