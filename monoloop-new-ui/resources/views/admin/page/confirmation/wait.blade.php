@extends('layouts.mail_landing')

@section('content')

<h1>Please confirm your registration! </h1>
<h4>Hi {{ Input::old('name') }}, </h4>
<p>Thank you very much!</p>
<p>&nbsp;</p>
<p>Your information has been sent successfully. In order to complete your registration, please click the confirmation link in the email
that we have sent to you.</p>
<p>&nbsp;</p>
<p>Please check, <strong>whether the email is in the junk folder of your email account, </strong></p>
<br /><br />

@endsection
