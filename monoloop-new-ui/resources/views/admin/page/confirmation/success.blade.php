@extends('extjs3') 

@section('content') 
<div style="padding : 0 0 4px 0 ;">
	<div class="header-content" style="float: left;margin: 0 0 0 45px;">Thanks you</div>
	<div style="clear: both;"></div>
</div>
<div style="background: white;position: relative;padding: 42px;border:1px solid rgb(204,204,204) ;">
	<div id="main-content">
    <h2>Dear {{Auth::user()->username}},</h2>
    <br />
    <p>Thanks for your confirmation and your patience to me.</p>
    <br />
    <p>Best regard,</p> 
    <p>Monoloop</p> 
  </div>
</div>


@endsection
 