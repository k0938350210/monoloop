@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/accounts">Account</a></li>
    <li class="active">Accounts - Usage</li>
  </ol>
</div>

<div class="innerLR">
  <div class="widget">
    {!! Notification::showAll() !!}
    <div class="table-responsive">
      <table class="table">
        <thead>
           <tr>
             <th class="col-md-1">&nbsp;</th>
             <th class="text-center info" colspan="7">Page views</th>
             <th class="text-center success" colspan="7">Visits</th>
           </tr>
	         <tr>
    				<th width="45">Account</th>
    				@foreach($dates as $date)
            <th class="text-right info"><?php echo date('d-m',$date); ?></th>
            @endforeach
            @foreach($dates as $date)
            <th class="text-right success"><?php echo date('d-m',$date); ?></th>
            @endforeach
		       </tr>
       </thead>
       <tbody>

         @foreach($accounts as $k => $account)
         <tr>
           <td>
             @if(!is_null($account['account']))
             {{$account['account']->uid}} - {{$account['account']->company}}
             @else
             {{$k}}
             @endif
           </td>
           @foreach($dates as $date)
           <td class="text-right">
             @if(isset($account[date('m/d/Y',$date)]))
              {{$account[date('m/d/Y',$date)]['pages']}}
             @else
              &nbsp;
             @endif
           </td>
           @endforeach
           @foreach($dates as $date)
           <td class="text-right">
             @if(isset($account[date('m/d/Y',$date)]))
              {{$account[date('m/d/Y',$date)]['visits']}}
             @else
              &nbsp;
             @endif
           </td>
           @endforeach
        </tr>
         @endforeach

       </tbody>
     </table>
   </div>
  </div>
</div>
@stop
