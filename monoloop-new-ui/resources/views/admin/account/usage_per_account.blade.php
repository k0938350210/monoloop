@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/mladmin">Admin</a></li>
    <li><a href="/mladmin/usage_per_account">Usage By Account</a></li>
    <li class="active"></li>
  </ol>
</div>


<div class="innerLR">
  <div class="row manage-header">
    <div class="col-lg-5">
        <h2>Account Page View</h2>
    </div>
    {!! Form::open(array( 'method' => 'get')) !!}
    <div class="col-lg-2">
     {!! Form::select('uid', $accounts, $uid, ['class' => 'form-control']) !!}
    </div>
    <div class="col-lg-4">
      <div class="input-daterange input-group" id="datepicker">
        <input type="text" class="input-sm form-control" name="start" value="{{ $start }}" />
        <span class="input-group-addon">to</span>
        <input type="text" class="input-sm form-control" name="end" value="{{ $end }}" />
      </div>
    </div>
    <div class="col-lg-1">
      <input type="submit" class="btn btn-primary" value="Search" />
    </div>
    {!! Form::close() !!}
  </div>
  <div class="widget">
    {!! Notification::showAll() !!}
    <div class="table-responsive">
      <table class="table">
        <thead>
             <tr>
                <th width="200">Date</th>
                <th >Pages </th>
               </tr>
       </thead>
         <tbody>
            @foreach($report as $row)
            <tr>
                <td>{{ date('d-m-Y',$row['datestamp'] )}}</td>
                <td  >{{ $row['pages'] }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td><strong>Total</strong></td>
            <td><strong>{{ $total }}</strong></td>
          </tr>

        </tfoot>
      </table>
    </div>
</div>

@stop


@section('footerjs')
  <script type="text/javascript">
  $( document ).ready(function() {
    $('.input-daterange').datepicker({
    });
  });
  </script>
@stop
