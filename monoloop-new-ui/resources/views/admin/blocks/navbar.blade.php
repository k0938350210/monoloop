<div class="navbar hidden-print main navbar-default" role="navigation">
    <div class="user-action user-action-btn-navbar pull-right">
		<button class="btn btn-sm btn-navbar btn-inverse btn-stroke hidden-lg hidden-md"><i class="fa fa-bars fa-2x"></i></button>
	</div>
    <a class="logo" href="/">
		<img width="100%" alt="MONOLOOP" src="/images/logo_monoloop.gif"/>
	</a>
    <ul class="main pull-left hidden-xs ">
        <li class="notifications">
            <a href="/">
                <i class="fa fa-clock-o"></i>
                <span class="badge badge-warning"></span>
            </a>
        </li>
        <!--
        <li class="notifications">
			<a href="user.html?v=v1.0.0-rc1"><i class="fa fa-user"></i> <span class="badge badge-info">2</span></a>
		</li>
		-->
    </ul>
    <ul class="main pull-right">
        <li class="hidden-xs hidden-sm"><a data-toggle="modal" class="btn btn-info" href="">Create New Account  <i class="fa fa-fw icon-compose"></i></a>
        </li>
         <li class="dropdown username hidden-xs">
			<a data-toggle="dropdown" class="dropdown-toggle" href=""><img alt="Profile" class="img-circle" src="/images/placeholder-th.png" width="35" /> {{ Auth::user()->username }}<span class="caret"></span></a>

			<ul class="dropdown-menu pull-right">
				<li><a class="glyphicon glyphicon-user  " href="#"><i></i> Account</a></li>
				<li><a class="glyphicon glyphicon-envelope " href="#"><i></i> Messages</a></li>
				<li><a class="glyphicon glyphicon-lock " href="/mladmin/auth/logout"><i></i> Logout</a></li>
		    </ul>
		</li>
    </ul>
</div>
