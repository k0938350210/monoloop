<!-- Sidebar Menu -->
<div id="menu" class="hidden-print hidden-xs">
    <div id="sidebar-fusion-wrapper">
    	<div id="logoWrapper">
	        <div class="media">
	            <a class="pull-left" href=""><img class="img-circle" alt="" src="/images/"></a>
	            <div class="media-body">
	                <a class="name" href="">{{ Auth::user()->username }}</a>
	                <p><i class="fa fa-fw fa-circle-o text-success"></i> Online</p>
	            </div>
	            <div class="clearfix"></div>
	            <a class="btn btn-xs btn-inverse " href="user.html?{{Config::get('app.version') }}"><i class="fa fa-user"></i></a>
	            <a class="btn btn-xs btn-inverse " href="user/projects.html?{{Config::get('app.version') }}"><i class="fa fa-clock-o"></i></a>
	            <a class="btn btn-xs btn-inverse " href="user/messages.html?{{Config::get('app.version') }}"><i class="fa fa-envelope"></i></a>

	        </div>

	    </div>
	    <ul class="menu list-unstyled">
	    	@foreach ($items as $item)
	    		@if (!isset($item['sub']))
	    		 <li class="{{ (Request::is($item['path']) ? 'active' : '') }}">
					<a href="{{ $item['href'] }}">
						<i class="fa {{ $item['icon'] }}"></i>
	                	<span>{{ $item['title'] }}</span>
	                </a>
				</li>
				@else
				<li class="hasSubmenu @foreach ($item['sub'] as $sub) {{ (Request::is($sub['path']) ? 'active' : '') }} @endforeach  ">
					<a href="#{{ $item['href'] }}" data-toggle="collapse">
						<i class="fa {{ $item['icon'] }}"></i>
						<span>{{ $item['title'] }}</span>
					</a>
					<ul id="{{ $item['href'] }}" class="collapse @foreach ($item['sub'] as $sub) {{ (Request::is($sub['path']) ? 'in' : '') }} @endforeach">
						@foreach ($item['sub'] as $sub)
						<li class="{{ (Request::is($sub['path']) ? 'active' : '') }}">
							<a href="{{ $sub['href'] }}"><i class="fa {{ $sub['icon'] }}"></i><span>{{ $sub['title'] }}</span></a>
						</li>
						@endforeach
					</ul>
				</li>
				@endif
	    	@endforeach
		</ul>
    </div>
</div>
