@extends('webix')
@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
  var options = {};
  options.link = config.dashboard.experiment.link;
  options.headerName = config.dashboard.experiment.headerName;
  options.script = config.dashboard.experiment.script;
  options.pageId = config.dashboard.experiment.pageId;
  generatePageContent(options);
})
</script>
@endsection
