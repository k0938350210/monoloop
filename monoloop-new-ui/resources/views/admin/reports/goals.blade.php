@extends('webix')
@section('footerjs')

<script type="text/javascript">
webix.ready(function() {
  var options = {};
  options.link = config.dashboard.goals.link;
  options.headerName = config.dashboard.goals.headerName;
  options.script = config.dashboard.goals.script;
  options.pageId = config.dashboard.goals.pageId;
  generatePageContent(options);
})
</script>
@endsection
