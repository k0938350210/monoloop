@extends('layouts.admin.main')

@section('content')
<div class="breadcrumbs">
  <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
    <li><a href="/mladmin">Admin</a></li>
    <li><a href="/mladmin/logs">Logs</a></li>
    <li class="active">Logs
    @if(is_object($accountObj))
    {{ ' - account '. $accountObj->name }}
    @endif
    @if(is_object($userObj))
    {{ ' - user '. $userObj->username }}
    @endif
    </li>
  </ol>
</div>

<div class="innerLR">
  <div class="row manage-header">
    <div class="col-lg-5">
    	<h2>Monoloop Logs</h2>
    </div>
    {!! Form::open(array( 'method' => 'get')) !!}
    <div class="col-lg-2">
     <input type="text" name="q" placeholder="search" class="span3 pull-right form-control" value="{{ $q }}" />

    </div>
    <div class="col-lg-4">
      <div class="input-daterange input-group" id="datepicker">
        <input type="text" class="input-sm form-control" name="start" value="{{ $start }}" />
        <span class="input-group-addon">to</span>
        <input type="text" class="input-sm form-control" name="end" value="{{ $end }}" />
      </div>
    </div>
    <div class="col-lg-1">
      <input type="submit" class="btn btn-primary" value="Search" />
    </div>
    {!! Form::close() !!}
  </div>
  <div class="widget">
    {!! Notification::showAll() !!}
    <div class="table-responsive">
      <table class="table">
        <thead>
	         <tr>
    				<th width="200">Date</th>
    				<th>Msg</th>
    				<th>Account</th>
    				<th>User</th>
		       </tr>
       </thead>
	     <tbody>
    		@foreach($logs as $log)
    			<tr>
    				<td><?php echo (string)$log->updated_at_format  ?></td>
    				<td>{{ $log->msg }}</td>
            <td>
              @if($log->account)
              {!! link_to_action('Admin\LogsController@index',$log->account->company, ['q' => $q , 'account' => $log->account->_id   ] ) !!}
              @endif
            </td>
            <td>{!! link_to_action('Admin\LogsController@index',$log->user->username, ['q' => $q  , 'user' => $log->user->_id ] ) !!}</td>
    			</tr>
    		@endforeach
    		</tbody>
    	</table>
      <div class="text-center">
        <?php echo $logs->appends(array('q' => $q , 'account' => $account , 'user' => $user))->render(); ?>
      </div>
      </div>
    </div>
</div>
@stop

@section('footerjs')
  <script type="text/javascript">
  $( document ).ready(function() {
    $('.input-daterange').datepicker({
    });
  });
  </script>
@stop
