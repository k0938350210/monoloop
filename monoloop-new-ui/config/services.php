<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => env('MAIL_API_DOMAIN', 'monoloop.com'),
		'secret' => 'key-'.env('MAIL_API_KEY', '266b9731b3a80848d0e3b937f7abd4cb'),
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
    'key' => env('S3_KEY'),
    'secret' => env('S3_SECRET'),
    'region' => 'eu-west-1',  // e.g. us-east-1
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

	'frontend' => [
		'invoke' => env('FRONTEND_INVOKE','54.246.30.244') ,
		'tracks_path' => env('FRONTEND_INVOKE_TRACKS_PATH','Monoloop') ,
		'postback_path' => env('FRONTEND_INVOKE_POSTBACK_PATH','MonoloopPostback') ,
		'invalidate' => [
			'endpoint' => '/invalidatekey/?key=',
			'timeout' => env('INVALIDATE_TIMEOUT',5) ,
			'retry' => env('INVALIDATE_RETRY',3)
		]
	],

	'invocation' => [
		'centralize_domain' => [
				'endpoint' => env('INVOCATION_GMID_ENDPOINT','newui.monoloop.com/fileadmin/centralize_domain/index1.1.3.html')
		]
	],

	's3' => [
    'key' => env('S3_KEY') ,
    'secret' => env('S3_SECRET') ,
    'distribution_id' => env('CLOUDFRONT_DISTRIBUTION_ID')
  ],




  'mailchimp' => [
    'api_key' => env('MAILCHIMP_API_KEY') ,
    'list_ids' => env('MAILCHIMP_LIST_IDS')
  ],

  'sugar_crm' => [
    'url' => env('SUGAR_CRM_URL') ,
    'user' => env('SUGAR_CRM_USER') ,
    'pass' => env('SUGAR_CRM_PASS')
  ],

  'webhook' => [
  	'url' => env('WEBHOOK_URL','127.0.0.1'),
  	'port' => env('WEBHOOK_PORT','9010')
  ],

  'recaptcha' => [
  	'site_key' => env('RECAPTCHA_SITE_KEY'),
  	'secret_key' => env('RECAPTCHA_SECRET_KEY'),
  ],

];
