<?php

use App\User;
use App\Account;


class ApiAccountProfileTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testGetProfile()
	{
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('GET', '/api/v1/profile' , [] , [] , [] , $server);
		$this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(true,$content['success']);
    #check field
    $set = false ;
    if(isset($content['user'])&&isset($content['user']['updated_at'])&&isset($content['user']['created_at'])&&isset($content['user']['username'])&&isset($content['accountUser'])&&isset($content['accountUser']['email'])&&isset($content['accountUser']['name'])){
      $set = true ;
    }
    $this->assertTrue($set);
	}

  public function testUpdateInvalidEmail(){
    $server = $this->getServerRequest() ;
		#invalid email test
    $response = $this->call('POST', '/api/v1/profile' , ['email'=>'test'] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

	public function testUpdateBlank(){
    $server = $this->getServerRequest() ;
		#invalid email test
    $response = $this->call('POST', '/api/v1/profile' , [] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

	public function testConfirmPasswordWasBlank(){
		$server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/profile' , ['confirmPassword' => '' , 'email' => 'wowo@wow.com' , 'name' => 'test' , 'password' => '123456' ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
	}

	public function testSuccessUpdate(){
		$server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/profile' , ['confirmPassword' => '123456' , 'email' => 'wowo@wow.com' , 'name' => 'test' , 'password' => '123456' ] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());

		$this->assertEquals(User::first()->currentAccountConfig()->email,'wowo@wow.com') ;
		$this->assertEquals(User::first()->currentAccountConfig()->name,'test') ; 
	}

}
