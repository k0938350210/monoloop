<?php

use App\User;
use App\Account;
use App\Goal;
use App\Folder ;
use App\PageElement ;



class ApiGoalTest extends TestCase {
  public function setUp()
  {
    parent::setUp();
  }

  private function getGoalRequest(){
    return ['conditionGoal' => "if ( ( MonoloopProfile.VisitDuration == '12' ) ){ | }" , 'condition_json' => '
{"rules":[{"key":"VisitDuration","Label":"Length of visit","Group":"Current Visit","Type":"integer","qtipCfg"
:{"text":"The number of seconds of this visit."},"rules":[],"linkingLogicalOperator":"","uid":"19e11a0e55214862ae6903ae969f1ffa"
,"hideConditionLogicalOperators":true,"binaryConditions":[{"name":"=="},{"name":"!="},{"name":"<"},{"name"
:">"},{"name":"<="},{"name":">="}],"binaryConditionsDefault":"==","value":"12","styles":{"and":{"line"
:{"top":149,"left":62,"height":30},"icon":{"top":149,"left":48}},"or":{"line":{"lineUpper":{"top":21
,"left":-8,"width":8},"lineMiddle":{"top":21,"left":-8,"height":179},"lineLower":{"top":200,"left":-8
,"width":8}},"icon":{"left":-19}}}}]}' , 'nameGoal' => 'Test111' , 'placement_inc_http_httpsGoal' => 1 , 'placement_inc_wwwGoal' => 1 ,
'placement_reg_exGoal' => 1 , 'placement_urlGoal' => 'http://www.monoloop.com/' , 'placement_url_optionGoal' => 'exactMatch' ,
'pointsGoal' => 1 , 'source' => 'folder__' . Folder::first()->_id  , 'typeGoal' => 'page_related' ] ;
  }

  public function testListing(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/goals' ,[] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    //first element must be root & folder type
    $this->assertEquals(1,count($content));
    $this->assertEquals('root',$content[0]['value']);
    $this->assertEquals('folder',$content[0]['type']);
  }

  public function testCreate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/goals/create' , $this->getGoalRequest() , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    #print_r($content);die();
    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('msg',$content));
    $this->assertTrue(array_key_exists('source',$content));
    $this->assertTrue(array_key_exists('content',$content));
    $content = $content['content'] ;
    $this->assertTrue(array_key_exists('goal',$content));
    $goal = $content['goal'] ;
    $this->assertTrue(array_key_exists('name',$goal));
    $this->assertTrue(array_key_exists('type',$goal));
    $this->assertTrue(array_key_exists('condition',$goal));
    //$this->assertTrue(array_key_exists('condition_json',$goal));
    $this->assertTrue(array_key_exists('point',$goal));
    $this->assertTrue(array_key_exists('account_id',$goal));
    $this->assertTrue(array_key_exists('deleted',$goal));
    $this->assertTrue(array_key_exists('hidden',$goal));
    $this->assertTrue(array_key_exists('folder_id',$goal));
    $this->assertTrue(array_key_exists('updated_at',$goal));
    $this->assertTrue(array_key_exists('created_at',$goal));
    $this->assertTrue(array_key_exists('uid',$goal));
    $this->assertTrue(array_key_exists('_id',$goal));
    #$this->assertTrue(array_key_exists('page_element_id',$goal));

    $this->assertEquals('Test111',$goal['name']);
    $this->assertEquals('page_related',$goal['type']);
    $this->assertEquals(0,$goal['hidden']);
    $this->assertEquals(0,$goal['deleted']);
    $this->assertEquals(3,$goal['uid']);

  }

  public function testUpdate(){
    $server = $this->getServerRequest() ;
    $request = $this->getGoalRequest() ;
    $request['_id'] = '561cc3d3faba0dec298b4827' ;
    $request['placement_url_optionGoal'] = 'startsWith' ;
    $response = $this->call('POST', '/api/v1/goals/update' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $content = $content['content'] ;
    $goal = $content['goal'] ;
  }

  public function testShow(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/goals/show?_id=goal__561cc3d3faba0dec298b4827'  ,[] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $this->assertTrue(array_key_exists('name',$content));
    $this->assertTrue(array_key_exists('point',$content));
    $this->assertTrue(array_key_exists('type',$content));
    $this->assertTrue(array_key_exists('hidden',$content));
    $this->assertTrue(array_key_exists('deleted',$content));
    $this->assertTrue(array_key_exists('account_id',$content));
    $this->assertTrue(array_key_exists('folder_id',$content));
    $this->assertTrue(array_key_exists('updated_at',$content));
    $this->assertTrue(array_key_exists('created_at',$content));
    $this->assertTrue(array_key_exists('url_config',$content));
    $url_config = $content['url_config'];
    $this->assertTrue(array_key_exists('url',$url_config));
    $this->assertTrue(array_key_exists('reg_ex',$url_config));
    $this->assertTrue(array_key_exists('inc_www',$url_config));
    $this->assertTrue(array_key_exists('url_option',$url_config));

    $this->assertEquals('exactMatch',$url_config['url_option']);

  }

  public function testUpdateStatus(){
    $server = $this->getServerRequest() ;
    $request = [] ;
    $request['_id'] = '561cc3d3faba0dec298b4827' ;
    $request['hidden'] = 1 ;
    $response = $this->call('POST', '/api/v1/goals/update-status' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(1,$content['content']['goal']['hidden']);

    $request['hidden'] = 0 ;
    $response = $this->call('POST', '/api/v1/goals/update-status' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(0,$content['content']['goal']['hidden']);
  }

  public function testDelete(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/goals/delete' , [ '_id' => '561cc3d3faba0dec298b4827'] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(1,$content['content']['goal']['deleted']);
  }

  #--- Fail Testing
  public function testCreateRequireName(){
    $server = $this->getServerRequest() ;
    $request = $this->getGoalRequest() ;
    unset($request['nameGoal']);
    $response = $this->call('POST', '/api/v1/goals/create' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testUpdateInExists(){
    $server = $this->getServerRequest() ;
    $request = $this->getGoalRequest() ;
    $request['_id'] = '561cc3d3faba0dec198b4827' ;
    $request['placement_url_optionGoal'] = 'startsWith' ;
    $response = $this->call('POST', '/api/v1/goals/update' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testShowWithNotHave_Id(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/goals/show?xxxid=goal__561cc3d3faba0dec298b4827'  ,[] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testUpdateStatusWrongId(){
    $server = $this->getServerRequest() ;
    $request = [] ;
    $request['_id'] = '561cc3d3faba0dec298b482b' ;
    $request['hidden'] = 1 ;
    $response = $this->call('POST', '/api/v1/goals/update-status' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
  }

  public function testUpdateStatusHiddenForString(){
    $server = $this->getServerRequest() ;
    $request = [] ;
    $request['_id'] = '561cc3d3faba0dec298b4827' ;
    $request['hidden'] = 'xxx' ;
    $response = $this->call('POST', '/api/v1/goals/update-status' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
  }

  public function testDeleteWithWrongId(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/goals/delete' , [ '_id' => '561cxxx3faba0dec298b4827'] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }
}
