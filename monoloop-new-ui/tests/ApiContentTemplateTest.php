<?php

use App\ContentTemplate;


class ApiContentTemplateTest extends TestCase {
  public function testListing(){
    $response = $this->call('GET', '/api/v1/tmplTypes' , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);

    $this->assertEquals('success',$content['status']);
    foreach(['status','totalCount','next','prev','tmplTypes'] as $key){
      $this->assertTrue(array_key_exists($key,$content)) ;
    }
    $this->assertEquals(1,$content['totalCount']);
  }

  public function testShow(){
    $response = $this->call('GET', '/api/v1/tmplTypes/0' , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true);
    foreach(['href','uid','name','properties'] as $key){
      $this->assertTrue(array_key_exists($key,$content)) ;
    }
    $this->assertEquals(0,$content['uid']);
    $this->assertEquals('html',$content['name']);
  }
}
