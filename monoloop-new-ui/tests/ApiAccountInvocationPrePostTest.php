<?php

use App\User;
use App\Account;


class ApiAccountInvocationPrePostTest extends TestCase {
  public function testRequired(){
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('POST', '/api/v1/account/invocation/prepost' , [] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation/prepost' , ['post_invocation' => ''] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $response = $this->call('POST', '/api/v1/account/invocation/prepost' , ['pre_invocation' => ''] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testSuccessUpdate(){
    $server = $this->getServerRequest() ;

    $response = $this->call('POST', '/api/v1/account/invocation/prepost' , ['pre_invocation' => 'test1' ,  'post_invocation' => 'test2'] , [] , [] , $server); 

    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('success',$content));
    $this->assertTrue(array_key_exists('msg',$content));
    $this->assertEquals(true,$content['success']);

    $account = Account::first() ;
    $this->assertEquals('test1',$account->invocation['pre']);
    $this->assertEquals('test2',$account->invocation['post']);
  }


}
