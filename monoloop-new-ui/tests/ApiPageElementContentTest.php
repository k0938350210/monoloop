<?php

use App\Placement;
use App\Content ;
use App\PageElement ;
use App\User ;


class ApiPageElementContentTest extends TestCase {
	public function testContentSave(){
    $user = User::first() ;
    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){|}" ,
  		'content' => 'my content' ,
  		'contentType' => '' ,
  		'experiment_id' => null ,
  		'fullURL' => 'http://url_test001.com' ,
  		'page_element_id' => '' ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'my001' ,
  		'xpath' => 'BODY'
  	]);
  	$content = json_decode($response->getContent(),true) ;

  	###Expect pageElement.content.code should wrap by condition
  	$this->assertEquals($content['pageElement']['content'][0]['code'] ,"<% if ( ( MonoloopProfile.VisitCount == '1' ) ){ %>my content<% } %>");
 	}
}