<?php

use App\User;
use App\Account;
use App\Goal;
use App\Folder ;
use App\PageElement ;



class ApiGoalTest2 extends TestCase {
  public function setUp()
  {
    parent::setUp();
  }

  public function testCreateGoalFlow(){
  	#/component/page-list-publish/store
  	$user = User::first() ;

  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://someurl.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content1 = json_decode($response->getContent(),true) ;

  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://someurl.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content2 = json_decode($response->getContent(),true) ;

  	$this->assertEquals($content1['content']['_id'],$content2['content']['_id']);

  	$page_element_id = $content2['content']['_id'];

  	#/api/goals/create
  	$response = $this->actingAs($user)->call('POST','/api/goals/create',[
  		'conditionGoal' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){ | }" ,
  		'nameGoal' => 'New UI 02' ,
  		'page_element_id' => $page_element_id ,
  		'pointsGoal' => 1 ,
  		'typeGoal' => 'page_related'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content3 = json_decode($response->getContent(),true) ;

  	$this->assertTrue(array_key_exists('_id',$content3['content']['goal'] ));
  	$content_uid = $content3['content']['goal']['uid'] ;
  	$goal_id = $content3['content']['goal']['_id'] ;
  	#This content uid must exists in pageElements.content.uid

  	$pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
  	$this->assertTrue((bool)$pageElement);

  	#/api/goals/delete
  	$response = $this->actingAs($user)->call('POST','/api/goals/delete',[
  		'_id' => 'goal__' . $goal_id
  	]);

  	$this->assertEquals(200, $response->status());
  	$pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
  	$this->assertTrue(!(bool)$pageElement);
  }

  public function testUpdateStatusFlow(){
  	$user = User::first() ;

  	$response = $this->actingAs($user)->call('POST','/component/page-list-publish/store',[
  		'experiment_id	' => null ,
  		'fullURLWebPageList' => 'http://someurl.com' ,
  		'includeHttpHttpsWebPageElement' => 0 ,
  		'includeWWWWebPageElement' => 0 ,
  		'page_element_id' => null ,
  		'regularExpressionWebPageElement' => '' ,
  		'remarkWebPageList' => '' ,
  		'source' => 'goal' ,
  		'urlOptionWebPageElement' => 'exactMatch'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content1 = json_decode($response->getContent(),true) ;
  	$page_element_id = $content1['content']['_id'];

  	#/api/goals/create
  	$response = $this->actingAs($user)->call('POST','/api/goals/create',[
  		'conditionGoal' => "if ( ( MonoloopProfile.VisitCount == '1' ) ){ | }" ,
  		'nameGoal' => 'New UI 02' ,
  		'page_element_id' => $page_element_id ,
  		'pointsGoal' => 1 ,
  		'typeGoal' => 'page_related'
  	]);
  	$this->assertEquals(200, $response->status());
  	$content3 = json_decode($response->getContent(),true) ;
  	$content_uid = $content3['content']['goal']['uid'] ;
  	$goal_id = $content3['content']['goal']['_id'] ;

  	$pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
  	$this->assertTrue((bool)$pageElement);

  	#/api/goals/update-status
  	$response = $this->actingAs($user)->call('POST','/api/goals/update-status',[
  		'_id' => 'goal__' . $goal_id ,
  		'hidden' => 1
  	]);

  	$this->assertEquals(200, $response->status());
  	$pageElement = PageElement::where('content.uid',(int)$content_uid)->first() ;
  	$this->assertTrue(!(bool)$pageElement);
  }

 }