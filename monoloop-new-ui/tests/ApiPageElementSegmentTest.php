<?php

use App\PageElement ;
use App\Segment ;
use App\User;

class ApiPageElementSegmentTest extends TestCase {
	public function testSaveWithSegmentCondition(){
		$segment = Segment::first() ;
	}

	public function testCreateWithSegmentCondition(){
		$user = User::first() ;
  	$response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
  		'condition' => "if ( ( Segments.segment_1() === true ) ){ | }" ,
  		'content' => 'content test' ,
  		'contentType' => null ,
  		'experiment_id	' => null ,
  		'fullURL' => 'http://monoloop.aecstar.com/stag/gmid001.html?segmenttest001' ,
  		'page_element_id' => null ,
  		'source' => 'default' ,
  		'type' => 'variation' ,
  		'variation_name' => 'segment' ,
  		'xpath' => 'My Xpath'
  	]);

  	$this->assertEquals(200,$response->getStatusCode());
  	$content = json_decode($response->getContent(),true) ;
  	$content = $content['pageElement']['content'][0] ;
  	$this->assertTrue(array_key_exists('segments',$content ));
    //print_r($content);
    $this->assertEquals(" <%  Segments['segment_1']  = function(){ if ( ( MonoloopProfile.VisitDuration == '12' ) ){  return true ;  }  return false ;};  %> ",$content['segments']['segment_1']);
	}

  public function testEffectAfterSaveSegmentCondition(){
    $user = User::first() ;
    $response = $this->actingAs($user)->call('POST','/api/page-element/content-create',[
      'condition' => "if ( ( Segments.segment_1() === true ) ){ | }" ,
      'content' => 'content test' ,
      'contentType' => null ,
      'experiment_id  ' => null ,
      'fullURL' => 'http://monoloop.aecstar.com/stag/gmid001.html?segmenttest001' ,
      'page_element_id' => null ,
      'source' => 'default' ,
      'type' => 'variation' ,
      'variation_name' => 'segment' ,
      'xpath' => 'My Xpath'
    ]);

    $content = json_decode($response->getContent(),true) ;


    $segment = Segment::byUid(1)->first() ;

    $response = $this->actingAs($user)->call('POST','/api/segments/update',[
      '_id' => $segment->_id ,
      'conditionSegment' => "if ( ( MonoloopProfile.VisitCount > '0' ) ){ | }" ,
      'descSegment' => 'test' ,
      'nameSegment' => 'test'
    ]);

    $this->assertEquals(200,$response->getStatusCode());

    $pageElement = PageElement::find($content['pageElement']['_id']);
    $this->assertEquals($pageElement->content[0]->segments['segment_'.$segment->uid]," <%  Segments['segment_1']  = function(){ if ( ( MonoloopProfile.VisitCount > '0' ) ){  return true ;  }  return false ;};  %> ");


  }
}