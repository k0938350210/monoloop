<?php

use App\User;
use App\Account;
use App\Profile;
use App\Services\Frontend\BatchResponse ;

class ParseTest extends TestCase {
	public function testCountry(){
		$user = User::first() ;
		$response = $this->actingAs($user)->call('POST','/api/condition-builder/parse',[
  		'c' => "if ( ( MonoloopProfile.Country === 'dk' ) ){|}" ,
  	]);
  	$this->assertEquals(200,$response->getStatusCode());
  	$content = json_decode($response->getContent(),true) ;
	}
}
