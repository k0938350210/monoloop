<?php

use App\User;
use App\Account;
use App\Experiment ;


class ApiExperimentTest extends TestCase {
  public function setUp()
  {
    parent::setUp();
  }

  public function getExperimentRequest(){
    return [
      'cgDayExperiment' => 1 ,
      'cgSizeExperiment' => 100 ,
      'descExperiment' => 'description' ,
      'goalExperiment' => '561cc3d3faba0dec298b4827' ,
      'nameExperiment' => 'name' ,
      'segmentExperiment' => '562dcdeffaba0d811a8b4573' ,
      'significantActionExperiment' => 1 ,
      'source' => 'folder__561cc3d1faba0dec298b4572'
    ];
  }

  public function testListing(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/experiments' ,[] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    //first element must be root & folder type
    $this->assertEquals(1,count($content));
    $this->assertEquals('root',$content[0]['value']);
    $this->assertEquals('folder',$content[0]['type']);
  }

  public function testSusscessShow(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/experiments/show?_id=experiment__561cc3d5faba0dec298b4e33'  ,[] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $content = $content['experiment'];
    #check all key ;
    $this->assertTrue(array_key_exists('_id',$content));
    $this->assertTrue(array_key_exists('experimentID',$content));
    $this->assertTrue(array_key_exists('account_id',$content));
    $this->assertTrue(array_key_exists('segment_id',$content));
    $this->assertTrue(array_key_exists('goal_id',$content));
    $this->assertTrue(array_key_exists('name',$content));
    $this->assertTrue(array_key_exists('description',$content));
    $this->assertTrue(array_key_exists('folder_id',$content));
    $this->assertTrue(array_key_exists('cg_size',$content));
    $this->assertTrue(array_key_exists('cg_day',$content));
    $this->assertTrue(array_key_exists('ordering',$content));
    $this->assertTrue(array_key_exists('significant_action',$content));
    $this->assertTrue(array_key_exists('hidden',$content));
    $this->assertTrue(array_key_exists('deleted',$content));
    $this->assertTrue(array_key_exists('updated_at',$content));
    $this->assertTrue(array_key_exists('created_at',$content));
  }

  public function testShowWithIncorrectId(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/experiments/show?_id=experiment__561cc3d5faab0dec298b4e33'  ,[] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testSuccessUpdate(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $content = $content['content'];
    $exp = $content['experiment'];
    $this->assertEquals($exp['_id'],'561cc3d5faba0dec298b4e33');
    $this->assertEquals($exp['experimentID'],1);
    $this->assertEquals($exp['segment_id'],'562dcdeffaba0d811a8b4573');
    $this->assertEquals($exp['goal_id'],'561cc3d3faba0dec298b4827');
    $this->assertEquals($exp['name'],'name');
    $this->assertEquals($exp['description'],'description');
    $this->assertEquals($exp['cg_size'],100);
    $this->assertEquals($exp['cg_day'],1);
    $this->assertEquals($exp['significant_action'],1);
  }

  public function testUpdateIncorrectCgSize(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $request['cgSizeExperiment'] = 101 ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $request['cgSizeExperiment'] = 'A' ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    unset($request['cgSizeExperiment']);
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testUpdateIncorrectSegnificant(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $request['significantActionExperiment'] = 3 ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testUpdateIncorrectCgDay(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $request['cgDayExperiment'] = -1 ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $request['cgDayExperiment'] = 'A' ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    unset($request['cgDayExperiment']);
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testUpdateWrongSegmentId(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $request['segmentExperiment'] = '561cc3d5faba0dec298b4e33' ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());

    unset($request['segmentExperiment']);
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
  }

  public function testUpdateWronGoal(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $request['goalExperiment'] = '561cc3d5faba0dec298b4e33' ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());

    unset($request['goalExperiment']);
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
  }

  public function testNameRequire(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    unset($request['nameExperiment']);
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testUpdateWithInvalidId(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e23' ;
    $response = $this->call('POST', '/api/v1/experiments/update' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testCreate(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $response = $this->call('POST', '/api/v1/experiments/create' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $content = $content['content'];
    $exp = $content['experiment'];
    $this->assertEquals($exp['experimentID'],2);
    $this->assertEquals($exp['segment_id'],'562dcdeffaba0d811a8b4573');
    $this->assertEquals($exp['goal_id'],'561cc3d3faba0dec298b4827');
    $this->assertEquals($exp['name'],'name');
    $this->assertEquals($exp['description'],'description');
    $this->assertEquals($exp['cg_size'],100);
    $this->assertEquals($exp['cg_day'],1);
    $this->assertEquals($exp['significant_action'],1);
  }

  public function testCreateWronGoal(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['goalExperiment'] = '561cc3d5faba0dec298b4e33' ;
    $response = $this->call('POST', '/api/v1/experiments/create' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());

    unset($request['goalExperiment']);
    $response = $this->call('POST', '/api/v1/experiments/create' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
  }

  public function testCreateWrongSegmentId(){
    $server = $this->getServerRequest() ;
    $request = $this->getExperimentRequest() ;
    $request['segmentExperiment'] = '561cc3d5faba0dec298b4e33' ;
    $response = $this->call('POST', '/api/v1/experiments/create' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());

    unset($request['segmentExperiment']);
    $response = $this->call('POST', '/api/v1/experiments/create' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
  }

  public function testUpdateStatus(){
    $server = $this->getServerRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $request['hidden'] = 1 ;
    $response = $this->call('POST', '/api/v1/experiments/update-status' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(1,$content['content']['experiment']['hidden']);

    $request['hidden'] = 0 ;
    $response = $this->call('POST', '/api/v1/experiments/update-status' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(0,$content['content']['experiment']['hidden']);
  }

  public function testUpdateStatusWithIncorrectId(){
    $server = $this->getServerRequest() ;
    $request['_id'] = '561cc315faba0dec298b4e33' ;
    $request['hidden'] = 1 ;
    $response = $this->call('POST', '/api/v1/experiments/update-status' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testUpdateStatusRequireHiddenField(){
    $server = $this->getServerRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $response = $this->call('POST', '/api/v1/experiments/update-status' , $request , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }

  public function testUpdateStatusRequireId(){
    $server = $this->getServerRequest() ;
    $request['hidden'] = 1 ;
    $response = $this->call('POST', '/api/v1/experiments/update-status' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testDelete(){
    $server = $this->getServerRequest() ;
    $request['_id'] = '561cc3d5faba0dec298b4e33' ;
    $response = $this->call('POST', '/api/v1/experiments/delete' , $request , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals(1,$content['content']['experiment']['deleted']);
  }

  public function testDeleteWidthIncorrectId(){
    $server = $this->getServerRequest() ;
    $request['_id'] = '561cc3d1faba0dec298b4e33' ;
    $response = $this->call('POST', '/api/v1/experiments/delete' , $request , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }
}
