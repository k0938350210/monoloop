<?php

use App\User;
use App\UrlConfig;

class UrlHashTest extends TestCase {

	public function testBasic()
	{
    $urlConfig = new UrlConfig();
    $urlConfig->url = 'http://www.monoloop.com' ;
    $urlConfig->url_option = 0 ;
    $urlConfig->inc_www = 0 ;

    $this->assertEquals('63fcbb5f3aa64ffe5d81d221',$urlConfig->urlHash);
	}

  public function testWithParams()
	{
    $urlConfig = new UrlConfig();
    $urlConfig->url = 'http://www.monoloop.com?test=only' ;
    $urlConfig->url_option = 0 ;
    $urlConfig->inc_www = 0 ;

    $this->assertEquals('63fcbb5f3aa64ffe5d81d221',$urlConfig->urlHash);
	}

  public function testEndWithSlash()
	{
    $urlConfig = new UrlConfig();
    $urlConfig->url = 'http://www.monoloop.com/' ;
    $urlConfig->url_option = 0 ;
    $urlConfig->inc_www = 0 ;

    $this->assertEquals('d804b845eded145f35e74358',$urlConfig->urlHash);
	}

  public function test1()
	{
    $urlConfig = new UrlConfig();
    $urlConfig->url = 'http://monoloop.com/' ;
    $urlConfig->url_option = 0 ;
    $urlConfig->inc_www = 0 ;

    $this->assertEquals('a948135a3570479d53b265ff',$urlConfig->urlHash);
	}

  public function test2()
	{
    $urlConfig = new UrlConfig();
    $urlConfig->url = 'http://monoloop.com/' ;
    $urlConfig->url_option = 0 ;
    $urlConfig->inc_www = 1 ;

    $this->assertEquals('a948135a3570479d53b265ff',$urlConfig->urlHash);
	}

  public function test3()
	{
    $urlConfig = new UrlConfig();
    $urlConfig->url = 'https://monoloop.com/' ;
    $urlConfig->url_option = 0 ;
    $urlConfig->inc_www = 1 ;

    $this->assertEquals('b188f37d8b9c6137307a40ea',$urlConfig->urlHash);
	}

}
