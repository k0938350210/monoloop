<?php

use App\User;
use App\Account;


class ApiBasicTest extends TestCase {
 
	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicAuthen()
	{
    $server = $this->getServerRequest() ;
		$account = Account::first() ;
		$response = $this->call('GET', '/api/v1/account' , [] , [] , [] , $server);
		$this->assertEquals(200,$response->getStatusCode());

    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals($content['uid'],$account->uid);

	}

  public function testFailBasicAuthen(){
    $server = [
      'PHP_AUTH_USER' => 12 ,
      'PHP_AUTH_PW' => 'xxxx'
    ];

		$response = $this->call('GET', '/api/v1/account' , [] , [] , [] , $server);

		$this->assertEquals(401,$response->getStatusCode());
  }

	public function testNotFound(){
		$response = $this->call('GET', '/api/v1/never_exists_endpoint' , [] , [] , [] , $this->getServerRequest());
		$this->assertEquals(404,$response->getStatusCode());
	}

	public function testMethodNotAllow(){
		$response = $this->call('PATCH', '/api/v1/pages' , [] , [] , [] , $this->getServerRequest());
		$this->assertEquals(405,$response->getStatusCode());
	}

}
