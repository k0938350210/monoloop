<?php

use App\User;
use App\Account;
use App\Profile;
use App\Services\Frontend\BatchResponse ;

class SegmentTest extends TestCase {
	public function testBasicSegment(){
		$user = User::first() ;
		$this->clearCache('profilelist') ;
		$this->clearCache($user->account->uid . '_segments') ;

		$response = $this->actingAs($user)->call('POST','/api/segments/create',[
  		'conditionSegment' => "if ( ( MonoloopProfile.PageViewCount == '1' ) ){|}" ,
  		'descSegment' => 'desc' ,
  		'nameSegment' => 'my test'
  	]);

  	$content = json_decode($response->getContent(),true) ;
  	$segment1_id = $content['content']['segment']['uid'] ;

  	$response = $this->actingAs($user)->call('POST','/api/segments/create',[
  		'conditionSegment' => "if ( ( MonoloopProfile.PageViewCount == '2' ) ){|}" ,
  		'descSegment' => 'desc' ,
  		'nameSegment' => 'my test2'
  	]);

  	$content = json_decode($response->getContent(),true) ;
  	$segment2_id = $content['content']['segment']['uid'] ;


   	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
 		$batchResponse = new BatchResponse($data);
 		sleep(1);
 		$profile = Profile::find($batchResponse->gmid);
 		$visit = $profile->profiles[0]['visits'][0] ;
 		$this->assertTrue(array_key_exists( 'segment_' . $segment1_id  , $visit['segments'] ));


 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		$batchResponse = new BatchResponse($data);
 		sleep(1);
 		$profile = Profile::find($batchResponse->gmid);
 		$visit = $profile->profiles[0]['visits'][0] ;
		$this->assertTrue(array_key_exists( 'segment_' . $segment2_id  , $visit['segments'] ));
		$this->assertTrue( $visit['segments']['segment_' . $segment2_id] > $visit['segments']['segment_' . $segment1_id] ) ;


		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());

		sleep(1);

		#newvisit
		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=' ,false,$this->commonContext());
		sleep(1);
		$profile = Profile::find($batchResponse->gmid);
		$visit = $profile->profiles[0]['visits'][1] ;
		$this->assertTrue(! isset($visit['segments']) );
 		/*
 		$client = new \Predis\Client();
 		$profilelist = $client->lrange('profilelist',0,1);
 		$this->assertEquals(1,count($profilelist));

 		$profile = json_decode($profilelist[0],true);
 		$this->assertEquals($profile['gmid'],$batchResponse->gmid);
 		*/
	}

	public function testSegmentNewProfile(){
		$user = User::first() ;
		$this->clearCache('profilelist') ;
		$this->clearCache($user->account->uid . '_segments') ;


		$response = $this->actingAs($user)->call('POST','/api/segments/create',[
  		'conditionSegment' => "if ( ( MonoloopProfile.PageViewCount == '1' ) ){|}" ,
  		'descSegment' => 'desc' ,
  		'nameSegment' => 'my test'
  	]);

  	$content = json_decode($response->getContent(),true) ;
  	$segment1_id = $content['content']['segment']['uid'] ;

  	$response = $this->actingAs($user)->call('POST','/api/segments/create',[
  		'conditionSegment' => "if ( ( MonoloopProfile.PageViewCount == '2' ) ){|}" ,
  		'descSegment' => 'desc' ,
  		'nameSegment' => 'my test2'
  	]);

  	$content = json_decode($response->getContent(),true) ;
  	$segment2_id = $content['content']['segment']['uid'] ;


   	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

   	sleep(1);

   	$data = file_get_contents('http://localhost:9000/tracks/?cid='.$user->account->uid.'&url=http://url_test001.com/&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

   	sleep(1);

   	#need pass test1 first
	}


}