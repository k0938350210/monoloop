<?php

use App\User;
use App\Account;
use App\Profile;
use App\Services\Frontend\BatchResponse ;

class BasicVisitTest extends TestCase {
	public function testBasic(){
		$cid = Account::first()->uid ;

		#first visit
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=&fla=1&java=1&pdf=1',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);

 		$profile = Profile::find($batchResponse->gmid) ;
 		$this->assertEquals($profile->customerID , $cid);
 		$this->assertEquals($profile->_id , $batchResponse->gmid);
 		$this->assertEquals(1 , count($profile->profiles));
 		$this->assertEquals($profile->recently_used_device , $batchResponse->mid);
 		$this->assertEquals($profile->profiles[0]['_id'],$batchResponse->mid) ;
 		$this->assertEquals($profile->profiles[0]['device_info']['config_flash'],1) ;
 		$this->assertEquals($profile->profiles[0]['device_info']['config_java'],1) ;
 		$this->assertEquals($profile->profiles[0]['device_info']['config_pdf'],1) ;
 		$p = $profile->profiles[0];
 		$this->assertEquals(1 , count($p['visits']));
 		$v = $p['visits'][0];
 		$this->assertEquals($v['_id'],$batchResponse->vid);
 		$this->assertEquals($v['clicks'],1);
 		$this->assertEquals($v['entrypage'],'http://url_test001.com');
 		$this->assertEquals($v['exitpage'],'http://url_test001.com');
 		$this->assertEquals($v['firstclick'],$v['lastclick']);
 		$this->assertEquals(1 , count($p['current']['views']));
 		$cv = $p['current']['views'][count($p['current']['views'])-1];
 		$this->assertEquals($cv['url'],'http://url_test001.com');
 		#refresh same page
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());
 		$batchResponse = new BatchResponse($data);

 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$v = $p['visits'][0];
 		$this->assertEquals($v['clicks'],2);
 		$this->assertEquals(2 , count($p['current']['views']));

 		#click new page
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$this->assertEquals(1 , count($p['visits']));
 		$v = $p['visits'][0];
 		$this->assertEquals($v['clicks'],3);
 		$this->assertEquals($v['exitpage'],'http://url_test002.com');
 		$this->assertEquals(3 , count($p['current']['views']));
 		$cv = $p['current']['views'][count($p['current']['views'])-1];
 		$this->assertEquals($cv['url'],'http://url_test002.com');

 		#click on new visit
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());
 		$profile = Profile::find($batchResponse->gmid) ;

 		$p = $profile->profiles[0];
 		$this->assertEquals(2 , count($p['visits']));
 		$v0 = $p['visits'][0] ;
 		$v1 = $p['visits'][1];
 		$this->assertTrue($v0['firstclick'] < $v1['firstclick']) ;
 		$this->assertEquals(1 , count($p['current']['views']));
 		$cv = $p['current']['views'][count($p['current']['views'])-1];

 		#click on new visit
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());
 		$profile = Profile::find($batchResponse->gmid) ;

 		$p = $profile->profiles[0];
 		$this->assertEquals(3 , count($p['visits']));
 		$v1 = $p['visits'][1];
 		$v2 = $p['visits'][2] ;
 		$this->assertTrue($v1['firstclick'] < $v2['firstclick']) ;

 		#refresh
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='. $batchResponse->vid,false,$this->commonContext());

 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$v2 = $p['visits'][2];
 		$this->assertEquals($v2['clicks'],2);

	}

	public function testNewVisit(){
		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		$batchResponse = new BatchResponse($data);
		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());

		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		$batchResponse = new BatchResponse($data);
		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());
	}

	public function testFirstSecondClick(){

		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		$batchResponse = new BatchResponse($data);



 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());

 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='.$batchResponse->vid,false,$this->commonContext());

 		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];
 		$this->assertEquals(2 , count($p['visits']));
 		$v0 = $p['visits'][0] ;
 		$v1 = $p['visits'][1];
 		$this->assertTrue($v0['firstclick'] < $v1['firstclick']) ;


	}

	public function testPreviousLastClickVSNextFirstClick(){
		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
		$batchResponse = new BatchResponse($data);

		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());

		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());

		$profile = Profile::find($batchResponse->gmid) ;
 		$p = $profile->profiles[0];

 		$v1 = $p['visits'][1];
 		$v2 = $p['visits'][2];
 		$this->assertTrue($v2['firstclick'] > $v1['lastclick']) ;
	}

	public function testNewProfile(){
		$cid = Account::first()->uid ;

		#first visit
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());


 		$batchResponse = new BatchResponse($data);
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid=&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());
 		$batchResponse = new BatchResponse($data);
 		$profile = Profile::find($batchResponse->gmid) ;
 		$this->assertEquals(count($profile->profiles),2);
	}

	public function testNewGmid(){
		$cid = Account::first()->uid ;

		#first visit
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test001.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());

 		$batchResponse = new BatchResponse($data);
 		$profile = Profile::find($batchResponse->gmid) ;
 		$this->assertEquals(Profile::all()->count(),2);

 		$data = file_get_contents('http://localhost:9000/tracks/?cid=1&url=http://url_test002.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid='. $batchResponse->vid,false,$this->commonContext());
 		$this->assertEquals(Profile::all()->count(),3);
	}
}