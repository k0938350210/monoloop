<?php

use App\Profile;
use App\Services\Frontend\BatchResponse ;

class ProfileMigrateTest extends TestCase {
	public function testMigrate(){
		DB::collection('profiles')->insert([
			'__v' => 0 ,
			'_id' => new \MongoId("57c2f581193d10bc6634b699") ,
			'abg' => false ,
			'config_browser_language' => '' ,
			"config_browser_name" => "Firefox",
			"config_browser_version" => "47.0.undefined",
			"config_cookie" => 1,
			"config_os" => "Ubuntu",
    	"config_resolution" => "1920x1080",
    	"controlgroups" => [
    		'32' => [
    			 "visitorCount" => 1
    		]
    	],
    	"crmExists"  => false,
    	"current"  => [
        "views"  => [
          [
            "url" => "http://www.monoloop.com/",
            "extRef" => "",
            "timeStamp" => 1472394625.454,
            "_id" =>  new \MongoId("57c2f581193d10bc6634b69b"),
            "meta" => []
          ]
      	]
 			],
 			"customerID"  => 116,
	    "events"  => [],
	    "gmids" => [],
	    "goals"  => [],
	    "location_browser_lang"  => "en",
	    "location_city"  => "",
	    "location_continent"  => "AS",
	    "location_country"  => "TH",
	    "location_ip"  => "171.96.222.236",
	    "location_latitude"  => "13.7500",
	    "location_longitude"  => "100.4667",
	    "location_region"  => "",
	    "location_timezone"  => "",
	    "visit_server_date"  => 1472394625.454,
	    "visits" => [
	    	[
	    		"_id" => new \MongoId("57c2f581193d10bc6634b69a"),
    		 	"categories" => [
            "514_337647727" => [
              "c" => 1,
              "g" => 514,
              "n" => "Plans",
              "t" => 2,
              "v" => "10"
            ]
          ],
          "clicks" => 1,
          "downloads" => [],
          "entrypage" => "http://www.monoloop.com/",
          "exitpage" => "http://www.monoloop.com/",
          "experiments" => [
            "32" => [
              "views" => 1,
              "isPartOfExperiment" => true,
              "goal" => 5377
            ]
          ],
          "firstclick" => 1472394625.454,
          "lastclick" => 1472394625.454,
          "referer" => "",
          "referers" => [
            ""
          ],
          "searches" => [
            ""
          ]
	    	]
	    ]
		]);

		$mid = '57c2f581193d10bc6634b699' ;
		$skey = '4f986e9471bd9b93b94eb247ba48275e7ff38dece341e3520c8522d92ae35f6a81653527c2492c99851a40e26dbb0ea932e2beb5d3f7fe5a63262138cd283f92' ;
		$vid = '57c2f581193d10bc6634b69a' ;

		$data = file_get_contents('http://localhost:9000/tracks/?cid=116&url=http://monoloop.com?revisited&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid='.$mid.'&omid=&skey='.$skey.'&vid='.$vid,false,$this->commonContext());

		$batchResponse = new BatchResponse($data);
 		$profile = Profile::find($batchResponse->gmid) ;

 		$this->assertEquals(count($profile->profiles),1);
 		$first_profile = $profile->profiles[0];
 		$this->assertEquals(count($first_profile['visits']),2);
 		$first_visit = $first_profile['visits'][0] ;

    /*
 		$this->assertTrue(array_key_exists('categories', $first_visit));
 		$this->assertTrue(array_key_exists('experiments', $first_visit));

    $second_visit = $first_profile['visits'][1] ;
    $this->assertTrue(!array_key_exists('categories', $second_visit));
    $this->assertTrue(!array_key_exists('experiments', $second_visit));
    */

 		$data = file_get_contents('http://localhost:9000/tracks/?cid=116&url=http://no-experiment-here.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid=&mid=&omid=&skey=&vid=',false,$this->commonContext());
    /*
 		$data = file_get_contents('http://localhost:9000/tracks/?cid=116&url=http://no-experiment-here.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());

    $data = file_get_contents('http://localhost:9000/tracks/?cid=116&url=http://no-experiment-here.com&res=1920x1080&h=18&m=25&s=31&urlref=&rand=0.7312085209992335&abg=false&uid=&gmid='.$batchResponse->gmid.'&mid='.$batchResponse->mid.'&omid=&skey='.$batchResponse->skey.'&vid=',false,$this->commonContext());
    */

	}
}
