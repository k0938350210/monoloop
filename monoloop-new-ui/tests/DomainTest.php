<?php

use App\User;
use App\Account; 

class DomainTest extends TestCase {
   
	public function testFirstDomain()
	{ 
    $account = factory(App\Account::class)->create();
    $domain = factory(App\AccountDomain::class)->make();
    $domainName = $domain->domain ; 
    $account->domains()->save($domain); 
    $this->assertEquals($domainName,$account->firstDomain);
	} 

}
