<?php

use App\Placement;
use App\Folder ;


class ApiPageTest extends TestCase {
  public function testListing(){
    Placement::truncate() ;

    $response = $this->call('GET', '/api/v1/pages' , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());

    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(isset($content['totalCount'])&&isset($content['next'])&&is_null($content['prev'])&&is_array($content['pages'])) ;
  }

  public function testCreateSuccess(){
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $this->assertEquals('created',$content['status']);
    $this->assertEquals('/pages/'.$content['uid'].'/',$content['href']);
    #test default property
    $placement = Placement::byUid($content['uid'])->first() ;
    $this->assertEquals($placement->uid , (int)$content['uid']);
    $this->assertEquals($placement->urlConfig->url,'www.test.com');
    $this->assertEquals($placement->hidden,1);
    $this->assertEquals($placement->deleted,0);
    $this->assertEquals($placement->description,'');
    $this->assertEquals($placement->urlConfig->inc_www,1);
    $this->assertEquals($placement->urlConfig->url_option,0);

    $rootFolder = Folder::root($placement->account_id)->first()  ;
    $this->assertEquals($rootFolder->_id,$placement->folder_id);
    #test post value1
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com','description' => 'description' , 'urlOption' => 'allowParams' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $placement = Placement::byUid($content['uid'])->first() ;
    $this->assertEquals($placement->remark,'description');
    $this->assertEquals($placement->urlConfig->url_option,1);
    #test post value2
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com','description' => 'description' , 'urlOption' => 'startsWith' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $placement = Placement::byUid($content['uid'])->first() ;
    $this->assertEquals($placement->urlConfig->url_option,2);

    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com','description' => 'description' , 'active' => true ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $placement = Placement::byUid($content['uid'])->first() ;
    $this->assertEquals($placement->hidden,0);
  }

  public function testCreateFail(){
    #missing mandatory field
    $response = $this->call('POST', '/api/v1/pages' , [] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    #send not boolean
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' , 'incWWW' => 'not_boolean' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' , 'active' => 'not_boolean' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    #incorrect urlOption
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' , 'urlOption' => 'wrong_one' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

  }

  public function testShow(){
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;


    $response = $this->call('GET', '/api/v1/pages/' . $content['uid'] ,[], [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    foreach(['uid','href','updatedAt','createdAt','active','description','url','regExp','incWWW','urlOption'] as $key){
      $this->assertTrue(array_key_exists($key,$content)) ;
    }
  }

  public function testUpdate(){
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(201,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    #update success
    $response = $this->call('PUT', '/api/v1/pages/' . $content['uid'] , ['url' => 'new_url','description' => 'new_description' , 'incWWW' => false , 'urlOption' => 'startsWith' , 'active' => true] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());
    $content2 = json_decode($response->getContent(),true) ;
    $this->assertEquals('updated',$content2['status']);

    $placement = Placement::byUid($content['uid'])->first() ;
    $this->assertEquals($placement->uid , (int)$content['uid']);
    $this->assertEquals($placement->urlConfig->url,'new_url');
    $this->assertEquals($placement->hidden,0);
    $this->assertEquals($placement->remark,'new_description');
    $this->assertEquals($placement->urlConfig->inc_www,0);
    $this->assertEquals($placement->urlConfig->url_option,2);

    #update fails
    $response = $this->call('PUT', '/api/v1/pages/' . $content['uid'] , ['description' => 'new_description' , 'incWWW' => false , 'urlOption' => 'startsWith' , 'active' => true] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $response = $this->call('PUT', '/api/v1/pages/' . $content['uid'] , ['url' => 'new_url','description' => 'new_description' , 'incWWW' => false , 'urlOption' => 'not_exists' , 'active' => true] , [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());
  }

  public function testDelete(){
    $response = $this->call('POST', '/api/v1/pages' , ['url' => 'www.test.com' ] , [] , [] , $this->getServerRequest());
    $content = json_decode($response->getContent(),true) ;

    $response = $this->call('DELETE', '/api/v1/pages/'. $content['uid']   , ['url' => 'www.test.com' ] , [] , [] , $this->getServerRequest());
    $this->assertEquals(200,$response->getStatusCode());

    $response = $this->call('GET', '/api/v1/pages/' . $content['uid'] ,[], [] , [] , $this->getServerRequest());
    $this->assertEquals(400,$response->getStatusCode());

    $placement = Placement::byUid($content['uid'])->first() ;
    $this->assertEquals(1,$placement->deleted);
  }


}
