<?php

use App\CustomField;
use App\Account;


class ApiCustomVarsTest extends TestCase {
  public function testListing(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/customvars' , [] , [] , [] , $server);
    $account = Account::first() ;
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $customfield  = $content[0] ;

    $this->assertTrue(array_key_exists('name',$customfield));
    $this->assertTrue(array_key_exists('datatype',$customfield));
    $this->assertTrue(array_key_exists('account_id',$customfield));
    $this->assertTrue(array_key_exists('uid',$customfield));
  }

  public function testCreate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/customvars' , ['name' => 'test_integer' , 'datatype' => 'integer' ] , [] , [] , $server);

    $account = Account::first() ;
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
  }

  public function testCreateBlankName(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/customvars' , [ 'datatype' => 'integer' ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('name',$content['errors']));
  }

  public function testCreateBlankName2(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/customvars' , ['name' => '' , 'datatype' => 'integer' ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('name',$content['errors']));
  }

  public function testCreateFailDatatype(){
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/customvars' , ['name' => 'xxxx' , 'datatype' => '' ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('datatype',$content['errors']));

    $response = $this->call('POST', '/api/v1/customvars' , ['name' => 'xxxx' , 'datatype' => 'xx' ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $content = json_decode($response->getContent(),true) ;
    $this->assertTrue(array_key_exists('datatype',$content['errors']));
  }

  public function testUpdate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('PUT', '/api/v1/customvars/' . 1 , ['name' => 'test_integer2' , 'datatype' => 'string' ] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    $this->assertEquals($content['customField']['name'],'test_integer2');
    $this->assertEquals($content['customField']['datatype'],'string');
  }

  public function testFailUpdate(){
    $server = $this->getServerRequest() ;
    $response = $this->call('PUT', '/api/v1/customvars/' . 1 , ['name' => '' , 'datatype' => 'string' ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());

    $server = $this->getServerRequest() ;
    $response = $this->call('PUT', '/api/v1/customvars/' . 1 , ['name' => 'xxx' , 'datatype' => '' ] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());


    $server = $this->getServerRequest() ;
    $response = $this->call('PUT', '/api/v1/customvars/' . 999 , ['name' => 'xxx' , 'datatype' => '' ] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testDelete(){
    $server = $this->getServerRequest() ;
    $response = $this->call('DELETE', '/api/v1/customvars/' . 1 , [] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ; 
    $this->assertEquals($content['customField']['deleted'],true);
  }




}
