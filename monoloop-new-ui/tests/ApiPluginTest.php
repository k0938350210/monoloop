<?php

use App\User;
use App\Account;
use App\Plugin;


class ApiPluginTest extends TestCase {

  public function setUp()
  {
    parent::setUp();

  }
  public function testListing(){
    $server = $this->getServerRequest() ;
    $response = $this->call('GET', '/api/v1/plugins' , [] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;
    ##save total count
    $this->assertEquals($content['totalCount'],Plugin::count());
    $this->assertTrue(array_key_exists('topics',$content));
    $this->assertTrue(array_key_exists('totalCount',$content));
    $this->assertEquals($content['totalCount'],count($content['topics']));

    $plugin = $content['topics'][0] ;
    $this->assertTrue(array_key_exists('_id',$plugin));
    $this->assertTrue(array_key_exists('enabled',$plugin));
    $this->assertTrue(array_key_exists('plugin_id',$plugin));
    $this->assertTrue(array_key_exists('plugin',$plugin));

    $this->assertTrue(array_key_exists('_id',$plugin['plugin']));
    $this->assertTrue(array_key_exists('name',$plugin['plugin']));
    $this->assertTrue(array_key_exists('code',$plugin['plugin']));
  }

  public function testDisablePlugin(){
    $account = Account::first() ;
    $plugin = $account->plugins[0] ;
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/plugins/' . $plugin->_id , ['enabled' => 0] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $account = Account::first() ;
    $plugin = $account->plugins[0] ;
    $this->assertEquals(0,$plugin->enabled);
  }

  public function testEnablePlugin(){
    $account = Account::first() ;
    $plugin = $account->plugins[0] ;
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/plugins/' . $plugin->_id , ['enabled' => 1] , [] , [] , $server);
    $this->assertEquals(200,$response->getStatusCode());
    $content = json_decode($response->getContent(),true) ;

    $account = Account::first() ;
    $plugin = $account->plugins[0] ;
    $this->assertEquals(1,$plugin->enabled);
  }

  public function testUnexistsPlugin(){
    $newKey = new MongoId() ;
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/plugins/' .  $newKey , ['enabled' => 1] , [] , [] , $server);
    $this->assertEquals(403,$response->getStatusCode());
  }

  public function testEnabledParaIsRequired(){
    $account = Account::first() ;
    $plugin = $account->plugins[0] ;
    $server = $this->getServerRequest() ;
    $response = $this->call('POST', '/api/v1/plugins/' . $plugin->_id , [] , [] , [] , $server);
    $this->assertEquals(422,$response->getStatusCode());
  }
}
