<?php

use App\Content ;
use App\ContentConfig ;

class ContentConfigTest extends TestCase {
  public function testSetContent(){
    $content = new Content() ;
    $content->save() ;
    $config = new ContentConfig(['content' => 'data' , 'content_type' => 'default']) ;
    $content->config()->save($config);

    $array = $content->config->toArray() ; 
    $this->assertEquals($array['content'] , 'data');
  }
}
