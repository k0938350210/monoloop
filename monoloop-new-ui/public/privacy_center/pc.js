window.ml_pc_var.timer = null ;

	function ml_hasClass(el, className) {
	  if (el.classList)
	    return el.classList.contains(className)
	  else
	    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
	}

	function ml_addClass(el, className) {
	  if (el.classList)
	    el.classList.add(className)
	  else if (!hasClass(el, className)) el.className += " " + className
	}

	function ml_removeClass(el, className) {
	  if (el.classList)
	    el.classList.remove(className)
	  else if (hasClass(el, className)) {
	    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
	    el.className=el.className.replace(reg, ' ')
	  }
	}

	function _ml_html_1(){

		var content = '' ;
		if(window.ml_pc_var.preview){
			content += '<div id="_ml_pc" class="_ml_widget _ml_ltr _ml_skin_dark _ml_position_right _ml_widget_open " style="bottom: 0px;">' ;
		}else{
			var c = '_ml_widget_hidden' ;
			if(window.ml_pc_var.pvc <= 1 ){
				c = '_ml_widget_open _ml_widget_firsttime' ;
			}
			content += '<div id="_ml_pc" class="_ml_widget _ml_ltr _ml_skin_dark _ml_position_right '+c+'" style="bottom: 0px;">' ;
		}
		content += '<a class="_ml_widget_open_close _ml_action_toggle_widget"><span class="_ml_widget_icon"></span></a>' ;
		content += '<div class="_ml_widget_hidden_handle _ml_action_toggle_widget"></div>';
		content += '<div id="_ml_pc_body">'
							+'<div class="_ml_widget_body">'
								+'<p class="_ml_widget_title _ml_action_open_widget"><span class="_ml_header_text">Privacy Center</span></p>'
									+'<div class="_ml_widget_state _ml_widget_state_open">'
										+'<ul><li class="_ml_menu_intro active" onclick="ml_tab_switch(\'intro\')" ><a ><strong>Disclaimer</strong></a></li><li>|</li><li class="_ml_menu_preferences" onclick="ml_tab_switch(\'preferences\')"><a ><strong>Preferences</strong></a></li><li>|</li><li class="_ml_menu_data" onclick="ml_tab_switch(\'data\')"><a ><strong>Data</strong></a></li></ul>'
										+'<div style="clear: both;"></div>'
										+'<div class="_ml_intro active"><p><b>This site is using personalization.</b></p><p>You can manage your preferences, view your profile and read more about our policies here.</p><p><b>We honor and protect your privacy</b></p><p>We are committed to respecting the trust you place in us and we accept responsibility for protecting your privacy. Consequently, we seek full transparency in what information we track and collect when you visit this website, why we do so and how we use it to provide you with relevant and meaningful content that is matching your current interests and preferences.</p><p>This website use technology to personalize the content based on your behavior when you visit. The objective is to give you the best possible experience when you visit the site. The technology works for both anonymous, registered and logged in visitors. If you have enabled Private Browsing, disabled cookies or javascript - the technology will not work and you will be served static content.  The personalization is based on your behavior on the site and what your page views suggest that you are most interested in.</p><p>Like most other websites we use cookies to be able to identify and remember visitors who browse our website (a "cookie" is a small data file that can be placed on your hard drive when you visit certain websites).  All browsers (Internet Explorer, Google Chrome, Firefox, Opera) have privacy options and you are free to decline cookies in your browser settings, but it should be pointed out that declining cookies could disable features or make some parts of the website not work as intended. </p><p>A cookie helps us to identify and track what you are searching for as you browse the pages of our site, while still keeping your personal information anonymous - the cookie only contains a number and a key to your tracking data on our servers. </p></div>'
									+'</div>'
									+'<div class="_ml_widget_footer"><p>Powered by <a href="http://monoloop.com" target="_blank"><img style="height: 27px;vertical-align:bottom;" src="//mlres.s3.amazonaws.com/images/ml-logo-small.png"></a></p><div class="_hj-f5b2a1eb-9b07_clear_both"></div>'
							+'</div>'
						+'</div>';
		content += '</div></div>' ;
		MONOloop.html(MONOloop.$('#_ml_pc_container'),content);
	}

	function _ml_callCssURL(url, cb) {
    var content;
    content = {};
    content = document.createElement('link');
    if (!cb) {
      cb = function() {};
    }
    content.type = 'text/css';
    content.rel = 'stylesheet';
    if(url.indexOf("http") !== 0)
    	content.href = ("https:" === document.location.protocol ? 'https://' + url : 'http://' + url);
 	  else
 	    content.href = url ;
    document.getElementsByTagName("head")[0].appendChild(content);
    if (content.readyState) {
      content.onreadystatechange = function() {
        if (content.readyState === "loaded") {
          content.onreadystatechange = null;
          return cb();
        } else if (content.readyState === "complete") {
          content.onreadystatechange = null;
          return cb();
        }
      };
    } else {
      content.onload = function() {
        return cb();
      };
    }
    return content;
  }

	function _ml_widgetclick(){
		clearTimeout(window.ml_pc_var.timer);
		var widgetEL = MONOloop.$('._ml_widget')[0] ;
		if(ml_hasClass(widgetEL,'_ml_widget_hidden')){
			ml_addClass(widgetEL,'_ml_widget_open');
			ml_removeClass(widgetEL,'_ml_widget_hidden');
			ml_removeClass(widgetEL,'_ml_widget_firsttime');
		}else{
			ml_addClass(widgetEL,'_ml_widget_hidden');
			ml_removeClass(widgetEL,'_ml_widget_open');
		}
	}

	function _ml_init(){
		MONOloop.click(MONOloop.$('._ml_action_toggle_widget'),function(){
			_ml_widgetclick() ;
		});

		if(window.ml_pc_var.preview){
			window.ml_pc_var.timer = setTimeout(function(){
				MONOloop.$('._ml_action_toggle_widget')[0].click() ;
			}, 3000);
		}else{
			if(window.ml_pc_var.pvc <= 1 ){
		 		window.ml_pc_var.timer = setTimeout(function(){
					MONOloop.$('._ml_action_toggle_widget')[0].click() ;
				}, 3000);
	 		}
		}
	}

	function ml_tab_switch(module){
		if(MONOloop.$('._ml_preferences').length === 0 && module === 'intro'){
			return ;
		}else if(MONOloop.$('._ml_preferences').length === 0){
			//Load additional asset
      _ml_callCssURL('//mlres.s3.amazonaws.com/privacy_center/pc2.min.css?v=2.0.6',function(){
        MONOloop.callURL('//mlres.s3.amazonaws.com/privacy_center/pc2.min.js?v=2.0.6', function() {
        	_ml_html_2() ;
        	ml_tab_switch2(module) ;
        });
      });
		}else{
			ml_tab_switch2(module) ;
		}
	}

	function ml_tab_switch2(module){
		//remove active class ;
		var els = MONOloop.$('._ml_widget_state li') ;

		for(var i = 0 ; i < els.length ; i++){
			ml_removeClass(els[i],'active');
		}
		ml_removeClass(MONOloop.$('._ml_intro')[0],'active');
		ml_removeClass(MONOloop.$('._ml_preferences')[0],'active');
		ml_removeClass(MONOloop.$('._ml_data')[0],'active');

		if(module == 'preferences'){
			ml_addClass(MONOloop.$('._ml_preferences')[0],'active');
			ml_addClass(MONOloop.$('._ml_menu_preferences')[0],'active');
		}else if(module == 'data'){
			ml_addClass(MONOloop.$('._ml_data')[0],'active');
			ml_addClass(MONOloop.$('._ml_menu_data')[0],'active');
		}else if(module == 'intro'){
			ml_addClass(MONOloop.$('._ml_intro')[0],'active');
			ml_addClass(MONOloop.$('._ml_menu_intro')[0],'active');
		}
	}
