
	function _ml_change_privacy_config(event){
		if(event.target.tagName != 'INPUT'){
			var elem = document.getElementById("ml_personalization");
			elem.checked=!elem.checked;
		}
		_ml_save_privacy_config();

	};

	function _ml_save_privacy_config(){
		var ml_personalization = MONOloop.getCookie('ml_personalization');

		if(MONOloop.val(MONOloop.$('input:checkbox[id="ml_personalization"]:checked'))){
    	ml_personalization = '1' ;
    }else{
    	ml_personalization = '0' ;
    }

    if( ml_personalization == null){
      ml_personalization = '1' ;
    }

    //cal frontend ;
    var gmid = MONOloop.getCookie('gmid');
		var skey = MONOloop.getCookie('skey');
		var mid = MONOloop.getCookie('mid');
		var vid = MONOloop.getCookie('vid');

		if(gmid === null || gmid === undefined || gmid === '' || gmid === 'null'){
			//gmid = '57304ecd2c1a5b9241600364' ;
			return;
		}

		MONOloop.callURL(ml_pc_var.frontend_url  + '/updatePrivacy/?gmid=' + gmid + '&mid=' + mid  + '&skey=' + skey + '&vid=' + vid + '&cid=' + ML_vars.cid + '&personalization=' + ml_personalization , function(){
			alert('Preferences have been updated');
	  });


    MONOloop.setCookie('ml_personalization',ml_personalization  , 525600 , '/') ;

	};

	function _ml_delete_cookie(){

		if( confirm("You are about to delete your Monoloop cookie - by doing so this site will not be able to recognize you.\nNOTE: We will NOT be able to respect any Privacy Preferences you may have indicated under Preferences. If you return to this site again - you will be recorded as a new visitor and we will initiate default tracking.")){
      MONOloop.setCookie('monoloop_pc',''  , -1 , '/') ;
      MONOloop.setCookie('gmid',''  , -1 , '/') ;
      MONOloop.setCookie('mid',''  , -1 , '/') ;
      MONOloop.setCookie('skey',''  , -1 , '/') ;
      MONOloop.setCookie('vid',''  , -1 , '/') ;
      MONOloop.removeGmid() ;
      alert('Cookie has been deleted') ;
    } ;
	};

	function _ml_delete_profile(){
		if( confirm("You are about to delete your profile - by doing so this site will not be able to personalize its content to you. Are you sure you want to delete the profile?")){
			var gmid = MONOloop.getCookie('gmid');
			var skey = MONOloop.getCookie('skey');
			var mid = MONOloop.getCookie('mid');
			var vid = MONOloop.getCookie('vid');

			if(gmid === null || gmid === undefined || gmid === '' || gmid === 'null'){
				//gmid = '57304ecd2c1a5b9241600364' ;
				return;
			}

			var ml_personalization = MONOloop.getCookie('ml_personalization');

			if(MONOloop.val(MONOloop.$('input:checkbox[id="ml_personalization"]:checked'))){
	    	ml_personalization = '1' ;
	    }else{
	    	ml_personalization = '0' ;
	    }

	    if( ml_personalization == null){
	      ml_personalization = '1' ;
	    }


			MONOloop.callURL(ml_pc_var.frontend_url  + '/emptyProfile/?gmid=' + gmid + '&mid=' + mid  + '&skey=' + skey + '&vid=' + vid + '&cid=' + ML_vars.cid + '&personalization=' + ml_personalization , function() {
				MONOloop.setCookie('vid','', -1 , '/') ;
				alert('Profile has been deleted') ;
	  	});

		}
	};

	function _ml_show_profile(){
		var gmid = MONOloop.getCookie('gmid');
		var skey = MONOloop.getCookie('skey');
		ml_addClass(MONOloop.$('._ml_profile_dialog')[0],'active');
		if(gmid === null || gmid === undefined || gmid === '' || gmid === 'null'){
			//gmid = '5735b8bbd1d3138d3b81d174' ;
			_ml_generate_profile_nocontent() ;
			return ;
		}



		var xmlhttp=new XMLHttpRequest();
		xmlhttp.open("GET",  ml_pc_var.app_url + '/api/profiles/'+gmid + '?skey=' + skey);
		xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
        if(xmlhttp.status == 200){
          profile = JSON.parse(xmlhttp.responseText);
          _ml_generate_profile_content(profile) ;
        }else{
          _ml_generate_profile_nocontent() ;
        }
	    }
		}
		xmlhttp.send();
	};

	function _ml_close_dialog(){
		ml_removeClass(MONOloop.$('._ml_profile_dialog')[0],'active');
	};


	function _ml_generate_profile_content(profile){
		profile = profile.profile ;
		var data = '<table  width="100%"><tr><td>Gmid :</td><td>'+profile._id+'</td></tr></table><br><strong>Profiles</strong><br>' ;
		for(var i = 0 ; i < profile.profiles.length ; i++){
			data = data + _ml_generate_profile_detail(profile.profiles[i]);
		}
		MONOloop.html(MONOloop.$('._ml_dialog_inner_content'),data);
	};

	function _ml_generate_profile_nocontent(){
		var data = '<table  width="100%"><tr><td><p align="center">No data found</p></td></tr></table>' ;
		MONOloop.html(MONOloop.$('._ml_dialog_inner_content'),data);
	};

	function _ml_generate_profile_detail(profile){
		console.debug(profile);
		var id = profile._id['$id'] ;
		var datat = '' ;
		if(profile.device_info){
			data = '<div class="_ml_profile_group" onclick="_ml_profile_detail_plus_click(\''+id+'\')"><div class="l">Profile : '+id+'</div><div id="_ml_profile_icon_'+id+'" class="r plus" ></div><div class="_ml_clear"></div></div><div id="_ml_profile_detail_'+id+'" class="_ml_profile_detail"><table  width="100%"><tr><td>Browser :</td><td>'+profile.device_info.config_browser_name+' '+profile.device_info.config_browser_version+'</td></tr><tr><td>Os :</td><td>'+profile.device_info.config_os.family+'</td></tr><tr><td>Cookie :</td><td>'+profile.device_info.config_cookie+'</td></tr><tr><td>Visit server date :</td><td>'+_ml_timeConverter(profile.visit_server_date)+'</td></tr></table>' ;
		}else{
			data = '<div class="_ml_profile_group" onclick="_ml_profile_detail_plus_click(\''+id+'\')"><div class="l">Profile : '+id+'</div><div class="_ml_clear"></div>'
		}

		if(profile.visits){
			for(var i = 0 ; i < profile.visits.length ; i++){
				data = data + _ml_generate_visit_detail(profile.visits[i]);
			}

		}

		return data + '</div>' ;
	};

	function _ml_generate_visit_detail(visit){
		var id = visit._id['$id'] ;
		return '<div class="_ml_visit_group" onclick="_ml_visit_detail_plus_click(\''+id+'\')"><div class="l">Visit : '+_ml_timeConverter(visit.firstclick)+'</div><div id="_ml_visit_icon_'+id+'" class="r plus" ></div><div class="_ml_clear"></div></div><div id="_ml_visit_detail_'+id+'" class="_ml_visit_detail"><table  width="100%"><tr><td>Entrypage :</td><td>'+visit.entrypage+'</td></tr><tr><td>Exitpage :</td><td>'+visit.exitpage+'</td></tr><tr><td>Referer :</td><td>'+visit.referer+'</td></tr><tr><td>Firstclick :</td><td>'+_ml_timeConverter(visit.firstclick)+'</td></tr><tr><td>Lastclick :</td><td>'+_ml_timeConverter(visit.lastclick)+'</td></tr><tr><td>Clicks :</td><td>'+visit.clicks+'</td></tr></table></div>'
	};

	function _ml_profile_detail_plus_click(profile_id){
		if(ml_hasClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'plus')){
			ml_removeClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'plus');
			ml_addClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'minus');
			ml_addClass(MONOloop.$('#_ml_profile_detail_'+profile_id)[0],'active');
		}else{
			ml_removeClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'minus');
			ml_addClass(MONOloop.$('#_ml_profile_icon_'+profile_id)[0],'plus');
			ml_removeClass(MONOloop.$('#_ml_profile_detail_'+profile_id)[0],'active');
		}
	};

	function _ml_visit_detail_plus_click(visit_id){
		if(ml_hasClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'plus')){
			ml_removeClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'plus');
			ml_addClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'minus');
			ml_addClass(MONOloop.$('#_ml_visit_detail_'+visit_id)[0],'active');
		}else{
			ml_removeClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'minus');
			ml_addClass(MONOloop.$('#_ml_visit_icon_'+visit_id)[0],'plus');
			ml_removeClass(MONOloop.$('#_ml_visit_detail_'+visit_id)[0],'active');
		}
	};


	function _ml_timeConverter(UNIX_timestamp){
	  var a = new Date(UNIX_timestamp * 1000);
	  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	  var year = a.getFullYear();
	  var month = months[a.getMonth()];
	  var date = a.getDate();
	  var hour = a.getHours();
	  var min = a.getMinutes();
	  var sec = a.getSeconds();
	  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	  return time;
	}

	function _ml_html_2(){
		MONOloop.html(MONOloop.$('#_ml_profile_modal'),'<div class="_ml_dialog_content"><a  title="Close" class="close" onclick="_ml_close_dialog();">X</a><strong>Profile Information</strong><hr/><div class="_ml_dialog_inner_content"></div></div>');

		var content
						 ='<div class="_ml_info "></div>'
						 + '<div class="_ml_preferences"><p><b>Please select how we track and handle your data.</b></p><br/><table style="padding-left: 50px;margin:0;"><tbody><tr onclick="_ml_change_privacy_config(event);"><td><input type="checkbox" value="1" name="ml_personalization" id="ml_personalization"></td><td>Personalization</td></tr></tbody></table><br/><div class="_ml_pc_delete_by"><p>&nbsp;</p><table width="100%"><tr><td ><button class="_ml_btn_danger _ml_rounded_corners _ml_transition _ml_shadow" type="button" onclick="_ml_delete_profile();">Delete profile</button></td></tr></table></div></div>'
						 + '<div class="_ml_data"><p>&nbsp;</p><p align="center"><button class="_ml_btn_primary _ml_rounded_corners _ml_transition _ml_shadow" type="button" onclick="_ml_show_profile();">View profile</button></p><p>&nbsp;</p></div>' ;

		MONOloop.append(MONOloop.$('._ml_widget_state'), content);

		if(MONOloop.getCookie('ml_personalization') == '1' || MONOloop.getCookie('ml_personalization') == ''){
			MONOloop.$('input:checkbox[id="ml_personalization"]')[0].checked = true;
		}

	}