var testbench_preview = {
    extra : ''  ,
    params : '' ,
    url : '' ,
    urlencode : '' ,
    baseUrl : '' ,
    baseRelativeUrl : '' ,
    temPath : '' ,
    selectTabSrcChange : function(){
    	var me = this ;
        if( document.getElementById('pw-test-view') == null){
          return ;
        }
        if( me.baseUrl != document.getElementById('pw-test-view').src && mPostMessage.enable == false ){
          // Change to proxy mode ;
          // Wait 1 second to swith ;

          setTimeout(function(){
          	if(mPostMessage.enable == false){
          		//console.log('placement : brose tab change to proxy mode');
          		var url = testbench_preview.url ;
          		if( document.getElementById('pw-test-view') != null){
                document.getElementById('pw-test-view').src = testbench_preview.temPath + 'testbenchView.php?l='+testbench_preview.urlencode ;
                mPostMessage.enable = true ;
                mPostMessage.showProxyWarning() ;
                mPostMessage.addProxyList(url) ;
	            }
          	}
          }, 1500);
          return ;
       }
    } ,

    init : function(){
    	var me = this ;
    	// Add listenner for post message .
    	mPostMessage.addListener() ;
    	// Set testbench url
    	document.getElementById('pw-test-view').src = me.url ;
    } ,

    allassetloadcomplete : function(){ 
    	var me = this ;
    	//console.log('placement : start preview process');
    	var iframe = document.getElementById("pw-test-view") ;
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "testpreview-processTestbenchPreview" ,
	        extra : me.extra ,
	        params : me.params ,
	        html : document.getElementById('result-data').innerHTML ,
		}), mPostMessage.url);
    }
} ;

var mPostMessage = {
	enable : false ,
	url : '' ,
	prepTestMode : false ,
	addListener : function(){
		var me = this ;
		var listener;
		listener = function(event) {
			var data;
			data = JSON.parse(event.data);
	        switch (data.t) {
	          case 'confirm':
	            me.enable = true ;
	            me.url = data.url ;
                var time =  new Date().getTime() ;
                //console.log('Found invocation in customer site') ;
                //console.log('Start loading bundle.js');
                var iframe = document.getElementById("pw-test-view") ;
	            iframe.contentWindow.postMessage(JSON.stringify({
			        t: "loadbundle" ,
                    domain : testbench_preview.baseRelativeUrl ,
			        url : testbench_preview.baseRelativeUrl + 'js/invocation/editor-bundle.js?' + time
				}), me.url);

	            break;
              case 'loadbundle-success' :
                //Check frame init ;
                var iframe = document.getElementById('pw-test-view') ;

            	me.prepTestMode = false ;

            	iframe.contentWindow.postMessage(JSON.stringify({
			        t: "init-testmode"
				}), me.url);

				setTimeout(function(){
					if( me.prepTestMode == false ){
	                	iframe.contentWindow.postMessage(JSON.stringify({
					        t: "init-testmode"
						}), me.url);
					}
				},2000) ;

                break ;

			  /*--------- TESTBENCH MODE ------- */
              case 'test-allassetloadcomplete' :
              	me.prepTestMode = true ;
                testbench_preview.allassetloadcomplete() ;
              break ;
			  case 'test-bubbleClick' :
			  	testbenchTab.bubbleClick(data.selector) ;
			  break ;
	        }
		};
		if (window.addEventListener) {
	        return addEventListener("message", listener, false);
		} else {
	        attachEvent("onmessage", listener);
		}
	} ,

	showProxyWarning : function(){
    return ;
		/*
		Ext.getCmp('monoloop-placement-header-label').setValue('<p align="center" style="font-weight:bold;color:red;">Notice:This site does not have our code in the page, so we will pull your site content through our proxy. This can cause certain features, content not being available or styling missing. To avoid this - please place the invocation code from Account/Codes & Scripts section into the header section of your site template.</p>');
		*/
		var ret = Ext.query('#msg-div-2') ;
		if(ret.length == 0 || ret[0].innerHTML == '' )
			monoloop_base.msgCloseOnClick(document.getElementsByTagName("body")[0],'Notice' ,'This site does not have our code in the page, so we will pull your site content through our proxy. This can cause certain features, content not being available or styling missing. To avoid this - please place the invocation code from Account/Codes & Scripts section into the header section of your site template.') ;
	} ,

	hideProxyWarning : function(){
		//Ext.getCmp('monoloop-placement-header-label').setValue('&nbsp;&nbsp;') ;
	} ,

	processURL : function(url){
		// This function for filter url ;
		if(Ext.util.Format.trim(url) == '')
			return url ;

		if(url.indexOf('http') != 0){
			url = 'http://' + url ;
		}
		return url ;
	} ,

	/* N pending : as some customer not using invocation entire domain */

	addProxyList : function(url){
		var matches = url.match(/^https?\:\/\/(?!(?:www\.)?(?:youtube\.com|youtu\.be))([^\/:?#]+)(?:[\/:?#]|$)/i);
		var domain = matches && matches[1];
	} ,

	checkProxyList : function(url){

	}
} ;
