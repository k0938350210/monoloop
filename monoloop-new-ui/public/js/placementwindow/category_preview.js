var category_preview = {
    regMatch : function(str , setback , setbackID){
        //console.debug(str) ;    
        //http://www.regular-expressions.info/javascriptexample.html ; 
        var body =  MONOloop.jq('html').html()  ;
        var ret = '' ; 
        //body = ' Test xxx xxx ' ; 
        //console.debug(str) ; 
        try{ 
            var re = new RegExp(str);
        }catch(err){
            ret =  '' ; 
        }
        var m = re.exec(body);
        //console.debug(m) ; 
        if (m == null) {
            ret = '' ; 
        } 
        else {
            var s =  ''  ; 
            for (i = 0; i < m.length; i++) {
                s = s + m[i] ;
                break ; 
            }
            ret = s ; 
        } 
        
        if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" , 
			  setbackID : setbackID , 
              data : ret 
            }), '*');
        }else{
            return ret ; 
        } 
    } , 
    
    getRegMatchPostFunc : function(jsStr, setback , setbackID){
        var ret = '' ; 
        var str = 'function testX001(){'
                + ' try { '
                + jsStr
                + ' }catch(err) {'
                + ' return \'error\' ;' 
                + ' } } ' ; 
        MONOloop.jq.globalEval(str) ;  
        ret = MONOloop.jq.trim( testX001() ) ; 
        if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" , 
			  setbackID : setbackID , 
              data : ret 
            }), '*');
        }else{
            return ret ; 
        } 
    } ,
    
    downloadMatch : function(typeData){
        var me = this ; 
        var extlist =  typeData.split(',') ; 
        var result = '' ; 
        MONOloop.jq('a').each(function(index){
            var filename = MONOloop.jq(this).attr('href') ; 
            if( filename == undefined)
                return ; 
            filename = filename.replace('http://localhost/monoloop/typo3conf/ext/t3p_dynamic_content_placement/category_preview.php?l=' , '') ;  
            filename = decodeURIComponent((filename + '').replace(/\+/g, '%20')) ;  
            filename = decodeURIComponent((filename + '').replace(/\+/g, '%20')) ;  
            var ext = filename.split('.') ;
            var ret = '' ;
            /*
            for( var i = 1 ; i < ext.length ; i++){ 
                if( i > 1) 
                    ret = ret + '.' ; 
                ret = ret + ext[i] ; 
            }
            */
            ret = filename.split('.').pop() ;   
            for ( var i = 0 ; i < extlist.length ; i++){ 
                if( extlist[i] == ret){
                    if( result == ''){
                        result = filename ; 
                    }else{
                        result = result + ',' + filename ; 
                    }
                    break ; 
                }
            } 
        }) ;
        
        return  result ; 
    }  , 
    
    downloadMatchPostFunc : function(typeData  , jStr ){
        var me = this ; 
        var fileList = me.downloadMatch(typeData) ; 
        fileList = fileList.split(',') ;
        var str = 'function testX001(result){'
                + ' try { '
                + jStr
                + ' }catch(err) {'
                + ' return \'error\' ;' 
                + ' } } ' ; 
        MONOloop.jq.globalEval(str) ; 
        var ret = '' ; 
        for( var i = 0 ; i < fileList.length ; i++ ){
            if( i == 0 )
                ret = testX001(fileList[i]) ;
            else
                ret = ret + ',' +   testX001(fileList[i]) ;
        } 
        return ret ; 
    } , 
    
    catCustomFunc: function(js, setback , setbackID) {
        var cat = '' ; 
        var js = 'function ml_customfucntion(){ ' + js + ' } ' ; 
        js = js + ' var testdata =  ml_customfucntion() ; MONOloop.jq("#ml-cate-testdata").val(testdata) ; ' ; 
 
        MONOloop.jq.globalEval(js);
        cat = MONOloop.jq("#ml-cate-testdata").val() ; 
        if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" , 
			  setbackID : setbackID , 
              data : cat 
            }), '*');
        }else{
            return cat ; 
        } 
    } , 
    
    addslashes : function(str) {
        // Escapes single quote, double quotes and backslash characters in a string with backslashes  
        // 
        // version: 1103.1210
        // discuss at: http://phpjs.org/functions/addslashes    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Ates Goral (http://magnetiq.com)
        // +   improved by: marrtins
        // +   improved by: Nate
        // +   improved by: Onno Marsman    // +   input by: Denny Wardhana
        // +   improved by: Brett Zamir (http://brett-zamir.me)
        // +   improved by: Oskar Larsson H�gfeldt (http://oskar-lh.name/)
        // *     example 1: addslashes("kevin's birthday");
        // *     returns 1: 'kevin\'s birthday'    
        return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    }
};

 
MONOloop.jq(document).ready(function() {
    
     MONOloop.jq('a').live("click", function(event){ 
		if (window['monoloopClientURL2'] != undefined){  
    		return previewView.processATag(this) ;   
	    }else{  
     		return true ; 
   		} 
    }); 
    
    MONOloop.jq('a').click(function(){
        if (window['monoloopClientURL2'] != undefined){  
    		return previewView.processATag(this) ;   
	    }else{  
     		return true ; 
   		}
    }) ;
}) ; 
 