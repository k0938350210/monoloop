var currentselector = "";
var popover_flag = true;
var hover_flag = true; // stopping concurrent highlighting
var add_addcontent_enable = false;
var show_CKE = false;
var contentmenu_iframe = undefined;
var bubble_iframecontent = [];
var bubble_counter = 0,
    bubble_right= 0,
    bubble_top = 0,
    BubbleSelector = [];
var monoloop_select_view = {
  lock : false ,
  tempUnlock : false ,
  records : {} ,
  actionsOnXapth: [],
  toolbarType: "default",
  page: "",
  currentTransaction: {action: 'add', data: {}},
  variationsBubbleCalled: false,
  currentPageElement: '',
  editorID: '',
  currentFolder: '',
  source: "default",
  selectorsHaveVariations: [],
  currentExperiment: undefined,
  originalUrl: "",
  changedElements: [],
  addConditionToChangedElements: function(condition){
    for (var i = 0; i < monoloop_select_view.changedElements.length; i++) {
      if(monoloop_select_view.changedElements[i].xpath === CKEDITOR.config.mappedTo){
        monoloop_select_view.changedElements[i].condition = condition;
      }
    }
  },
  storeChangedContent: function(options){
    //console.log(options);
    var found  = false;
    for (var i = 0; i < monoloop_select_view.changedElements.length; i++) {
      if(monoloop_select_view.changedElements[i].content_id === options.content_id){
        monoloop_select_view.changedElements[i].content_id = options.content_id;
        if(options.content !== undefined){
          monoloop_select_view.changedElements[i].content = options.content;
        }
        if(options.condition !== undefined){
          monoloop_select_view.changedElements[i].condition = options.condition;
        }
        // Mantis #4556
        if(options.placement_type !== undefined){
          monoloop_select_view.changedElements[i].placement_type = options.placement_type;
        }
        if(options.xpath !== undefined){
          monoloop_select_view.changedElements[i].xpath = options.xpath;
        }
        found = true;
        break;
      }
    }
    if(found === false){
      monoloop_select_view.changedElements.push(options);
    }
  },

  saveChangedContent: function(varName,flag){
    for (var i = 0; i < monoloop_select_view.changedElements.length; i++) {
      if(monoloop_select_view.changedElements[i].xpath.includes('_mp_hash_function_')){
        var selector = monoloop_select_view.changedElements[i].xpath.replace('_mp_hash_function_','#');
      }else {
        var selector = monoloop_select_view.changedElements[i].xpath;
      }

      var element = monoloop_select_view.getSelectorByXPath(selector);

      if (element.prop("tagName").toLowerCase() == 'img') {
        var content = element[0].outerHTML;
      }else {
        var content = element.html();
      }
      var json_string = JSON.stringify({
        t: "save-changed-content-while-editing",
          data : {
            content_id: monoloop_select_view.changedElements[i].content_id,
            content: content,
            variation_name: varName,
            // Mantis Add Content Flow #4556
            // placement_type: monoloop_select_view.changedElements[i].placement_type,
            xPath:  monoloop_select_view.changedElements[i].xpath,
            condition: monoloop_select_view.changedElements[i].condition ? monoloop_select_view.changedElements[i].condition : ''
          }
      });
      monoloop_select_view.postToParent(json_string, "*");
      monoloop_select_view.changedElements.splice(i,1);
    }
  },
  originalContents: [],
  storeOriginalContent: function(options){
    var found  = false;
    for (var i = 0; i < monoloop_select_view.originalContents.length; i++) {
      if(monoloop_select_view.originalContents[i].xpath === options.xpath){
        found = true;
      }
    }
    if(found === false){
      monoloop_select_view.originalContents.push(options);
    }
  },
  restoreOriginalContents: function(){
    for (var i = 0; i < monoloop_select_view.originalContents.length; i++) {
      var selector = monoloop_select_view.originalContents[i].xpath,
          element = monoloop_select_view.getSelectorByXPath(selector);
      element.html(monoloop_select_view.originalContents[i].content);
    }
    MONOloop.jq('.removedElement').remove();
  },
  adjust_all_bubble_positions: function(){
    if (bubble_iframecontent.length > 0) {
      var xpath = "",
          element = "";
      for (var i = 0; i < bubble_iframecontent.length; i++) {
        xpath = bubble_iframecontent[i].split("&");
        xpath = xpath[1].split("=").pop(-1);

        element = monoloop_select_view.getSelectorByXPath(BubbleSelector[i]);
        monoloop_select_view.adjustBubblePosition(element);
        // setting top and left of bubble
        MONOloop.jq('#'+xpath).css("top",bubble_top - 5);
        MONOloop.jq('#'+xpath).css("left",bubble_right - 25);

      }
    }
  },
  isPWchangedPosted: false,
  selectedElementsOnPage: [],
  hiddenElementsOnPage: [],
  newExperimentBackUp: undefined,
  totalVariations: 0,
  toolbarGroups: function(type){
    switch (type) {
      case 'image':
        return [
          // { name: 'ePlugins'},
          { name: 'others', groups: [ 'others' ] },
          { name: 'uploader', groups: [ 'uploader' ] },
        ];
        break;
      default:
        return [
          { name: 'ePlugins'},{ name: 'insert'},
          { name: 'others', groups: [ 'others' ] },
          { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
          // { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
          { name: 'document', groups: [ 'document', 'doctools', 'mode' ] },
          { name: 'forms', groups: [ 'forms' ] },
          // '/',
          { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] }, // 'bidi' Mantis #4473
          { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'paragraph' ] },
          { name: 'links', groups: [ 'links' ] },
          { name: 'uploader', groups: [ 'uploader' ] },
          // '/',
          { name: 'styles', groups: [ 'styles'] },
          { name: 'colors', groups: [ 'colors' ] },
          { name: 'tools', groups: [ 'tools' ] },
        ];
        break;
    }
  },
  pushActionsOnXapth: function(xpath){
    if(monoloop_select_view.actionsOnXapth.indexOf(xpath) === -1){
      monoloop_select_view.actionsOnXapth.push(xpath);
    }
  },
  clearSelection : function(){
    var me = this ;
        MONOloop.jq('.monoloopBorderFix').removeClass('monoloopBorderFix') ;
        MONOloop.jq('.monoloopBorder').removeClass('monoloopBorder') ;
        MONOloop.jq('#temp-tooltip').remove();
        me.removeAllBorder() ;
        me.lock = false ;
        //monoloop_select_view.disPlayBorder() ;
        //monoloop_select_view.lock = false  ;
  } ,
  removeAllBorder : function(){
    var me = this ;
    MONOloop.jq('#ml-borders').html('') ;

    MONOloop.jq('.ml-border,.ml-border-dotted').css('top' ,0) ;
        MONOloop.jq('.ml-border, .ml-border-dotted').css('left' ,0) ;
        MONOloop.jq('.ml-border, .ml-border-dotted').css('width' ,0) ;
        MONOloop.jq('.ml-border, .ml-border-dotted').css('height' ,0) ;
  } ,
  checkParent : function(selectorString){
    var me = this ;
    var parent = MONOloop.jq(selectorString).parent() ;
    var selector = me.getXPath(parent) ;
    if( MONOloop.jq.trim(selector) == '')
      return false ;
    return true ;
  } ,
  checkChild : function(selectorString){
    var me = this ;
    if( MONOloop.jq(selectorString).children().length ){
      return true ;
    }
    return false ;
  } ,
  selectFocusParent : function(selectorString){
    var me = this ;




        if( MONOloop.jq(selectorString).parent().length ){
            me.clearSelection() ;
            var parent = MONOloop.jq(selectorString).parent() ;
            var selector = me.getXPath(parent) ;
            //top.selectTab.changeSelectorTxt(  selector  ) ;
            parent.click() ;
            monoloop_select_view.bindElement(selector);
        //     monoloop_select_view.postToParent(JSON.stringify({
        //       t: "select-changeSelectorTxt" ,
        // selector : selector
        //     }), "*");

        }else{
            alert('Not Found'); // Not found element
        }
    } ,
    processCustomXPath : function(selectorString){
      var me = this ;
      if( MONOloop.jq(selectorString).length ){
        MONOloop.jq(selectorString).addClass('monoloopBorderFix') ;
        me.disPlayAllBorder() ;
      }
    } ,
    selectFocusFirstChild : function(selectorString){
      var me = this ;
        if( MONOloop.jq(selectorString).children().length ){
            me.clearSelection() ;
            var firstChild = MONOloop.jq(selectorString).children();

            var selector = me.getXPath(firstChild) ;
            // monoloop_select_view.bindElement("#" + selector.attr('id'));
            //top.selectTab.changeSelectorTxt(  selector  ) ;
            // monoloop_select_view.postToParent(JSON.stringify({
            //   t: "select-changeSelectorTxt",
            //   selector : selector
            // }), "*");
          //firstChild.click() ;
        }else{
            alert('Not Found'); // Not found element
        }
    } ,
  getActiveElement: function( document ){

       document = document || window.document;

       // Check if the active element is in the main web or iframe
       if( document.body === document.activeElement
          || document.activeElement.tagName == 'IFRAME' ){
           // Get iframes
           var iframes = document.getElementsByTagName('iframe');
           for(var i = 0; i<iframes.length; i++ ){
               // Recall
               var focused = getActiveElement( iframes[i].contentWindow.document );
               if( focused !== false ){
                   return focused; // The focused
               }
           }
       }

      else return document.activeElement;

       return false;
  },
  processImage : function(dom){
    var me = this ;
    var base = MONOloop.jq('head base').attr('href') ;

    var src = MONOloop.jq(dom).attr('src') ;
    var t1 = src.substr(0,7).toLowerCase();
    var t2 = src.substr(0,8).toLowerCase();
    if( t1 == 'http://' || t2 == 'https://')
      return ;
    var t3 = src.substr(0,1) ;
    if( t3 == '/'){
      var pathArray = base.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      MONOloop.jq(dom).attr('src' , protocol + '//' + host + src ) ;
    }else{
      MONOloop.jq(dom).attr('src' , base + '/' + src) ;
    }

  } ,
  //  Old version of getting selected content ;
  getSelectContent : function(){
    var me = this ;
    select = MONOloop.jq('.monoloopBorderFix:eq(0)') ;

      if( select.length > 0){
        if(  select[0].nodeName == 'IMG' ){
        me.processImage(select[0]) ;
      }else{
        MONOloop.jq('.monoloopBorderFix:eq(0) img').each(function(index){
          me.processImage(this) ;
        }) ;
      }
      }
        var data = MONOloop.jq('.monoloopBorderFix:eq(0)').outerHTML() ;
        data = data.replace('monoloopBorderFix' , '') ;
        data = data.replace('monoloopBorder' , '') ;
        return  data ;
    } ,
    // New version for returning multiple data ;
    getSelectedData : function(){
      var me = this ;
      var borderFixes  = MONOloop.jq('.monoloopBorderFix') ;
      var data = {} ;
      for( var i = 0 ; i < borderFixes.length ; i++){
         var el = borderFixes[i] ;
         var xPath = me.getXPath(el);
         var content = me.getContentByXPath(xPath);
         data['content[' + i + '][xpath]'] = xPath;
         data['content[' + i + '][content]'] = content;
      }
      monoloop_select_view.postToParent(JSON.stringify({
            t: "select-receiveIframeMultiSelectedData" ,
        data : data
      }), "*");
    } ,
    getContentByXPath : function(xpath){
      var me = this ;
      select = MONOloop.jq(xpath) ;
      if( select.length > 0){
        if(  select[0].nodeName == 'IMG' ){
        me.processImage(select[0]) ;
      }else{
        MONOloop.jq(xpath + ' img').each(function(index){
          me.processImage(this) ;
        });
      }
      }
      var data = MONOloop.jq(xpath).outerHTML() ;
        data = data.replace('monoloopBorderFix' , '') ;
        data = data.replace('monoloopBorder' , '') ;
        return  data ;
    } ,
    setContentByXPath : function(xpath, updatedHtml, contentType){
      var me = this;
      var selector = monoloop_select_view.getSelectorByXPath(xpath);
      switch (contentType) {
        case 'image':
          var newSelector;
          selector.replaceWith(updatedHtml);
          newSelector = monoloop_select_view.getSelectorByXPath(xpath);
          newSelector.load(function(){
            monoloop_select_view.bindElement("#" + newSelector.attr('id'));
          });
          break;
        default:
          selector.html(updatedHtml);
          break;
      }
    } ,
    getSelectorByXPath : function(xpath){
      var me = this ;
      selector = MONOloop.jq(xpath) ;
      return  selector ;
    } ,
    getXPath : function(el){
      var selector = MONOloop.jq(el).parents()
                .map(function(){
                    var id = MONOloop.jq(this).attr("id");
                    var returnPart = this.tagName ;
                    if( returnPart == 'HTML'  || returnPart == 'BODY')
                      return returnPart ;
                    if (id) {
                      id = id.replace(/\./g, '\\.');
                      id = id.replace(' ', '\\ ');
                        returnPart += "#"+ id;
                    }else{
                        var classNames = MONOloop.jq(this).attr("class");
                        if (MONOloop.jq.trim(classNames) ) {
                        returnPart += "." + MONOloop.jq.trim(classNames).replace(/\s+/gi, ".");
                        }
                    }
                    return  returnPart ;
                })
                .get().reverse().join(" ");
        var selectorTest = selector ;
        if (selector) {
          selector += " "+ MONOloop.jq(el)[0].nodeName;
        }
        var id = MONOloop.jq(el).attr("id");
        if (id) {
          if(id.indexOf("ml_custom_ckedit_id_") === -1){
            id = id.replace(/\./g, '\\.');
            id = id.replace(' ', '\\ ');
            selector += "#"+ id;
          }
        }
        var classNames = MONOloop.jq(el).attr("class");
        if (MONOloop.jq.trim(classNames)) {

          selector += "." + MONOloop.jq.trim(classNames).replace(/\s+/gi, ".");
          //.monoloopBorder.monoloopBorderFix
          selector = selector.replace(/.monoloopBorderFix/ , '') ;
          selector = selector.replace(/.monoloopBorder/ , '') ;
          var excludePattern = /.cke_editable|.cke_editable_inline|_inline|.cke_contents_ltr|.cke_show_borders|.cke_focus|.monoloop_remove/ig;
          selector = selector.replace(
            excludePattern,
            function replacer(match){
              return "";
            }
          );

        }
        var curElement = MONOloop.jq(selector);
        if( curElement.length > 1){
             for( var i = 0 ; i < curElement.length ; i++){
                if( el ==  curElement[i]){
                    selector += ':eq('+i+')' ;
                }
             }
        }

        return this.cleanUpXpath(selector) ;
    } ,


    cleanUpXpath:function(selector){

      var res = selector.split(" ");

      var index = 0 ;
      var temp = null ;
      var xpath1 = '' ;
      var masternode = MONOloop.jq(selector).get( 0 ) ;

      while(index < res.length){

        temp = res.slice(0) ;
        if(res.length <= 2){
          break ;
        }

        res.splice(index, 1);

        xpath1 = res.join(" ");
        var node1 = MONOloop.jq(xpath1).get( 0 ) ;

        if((node1 === masternode) && MONOloop.jq(xpath1).length == 1 ){
          ;
        }else{
          index++ ;
          res = temp.slice(0) ;
        }
      }

      var last_id_index = -1 ;

      for(var i = res.length -1 ; i >= 0 ; i-- ){
        if(res[i].indexOf('#') !== -1 ){
          if( last_id_index === -1){
            last_id_index = i ;
          }else{
            res[i] = '';
          }
        }
      }

      return res.join(" ") ;
    },

    // For show popup menu position.
    getScrollXY : function () {
      var scrOfX = 0, scrOfY = 0;
      if( typeof( window.pageYOffset ) == 'number' ) {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
      } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
      } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
      }
      return [ scrOfX, scrOfY ];
    } ,
    addAndDisplayBorder : function(id ,  el){
        if( id != 'ml-border'){
          MONOloop.jq('#ml-borders').append('<div id="'+id+'-l" class="ml-border"></div><div id="'+id+'-r" class="ml-border"></div><div id="'+id+'-s" class="ml-border"></div><div id="'+id+'-u" class="ml-border"></div>');
        }

        var $el = MONOloop.jq(el);
        if($el.length > 0){
          var top = $el.offset().top ;
          var left = $el.offset().left;
          var hight = $el.outerHeight();
          var width =  $el.outerWidth();
          var xpath = monoloop_select_view.getXPath(el);
          if(xpath.split(/#(.+)/)[1] != undefined){
            var parent = xpath.split('#');
            var child = '#'+(xpath.split(/#(.+)/)[1])
          }else if(xpath.indexOf('.') != -1){
            var parent = xpath.split('.');
            var child = '.'+(xpath.substring(xpath.indexOf('.')+1));
          }else{
            var parent = xpath.split('.');
            var child = '';
          }

          if(id == 'ml-border'){
             MONOloop.jq('body').append('<div class="tooltiptext" id="temp-tooltip"><span style="color:#FBADFF;">'+parent[0]+'</span><span style="color:skyblue;">'+child+'</span>  |  <span style="font-size: 11px !important;">'+width+' x '+hight+'</span><span class="arrow" id="tooltipArrow"></span></div>');
          }
            MONOloop.jq('#'+id+'-l').css('top' , top -3) ;
            MONOloop.jq('#'+id+'-l').css('left' ,  left - 3) ;
            MONOloop.jq('#'+id+'-l').css('width' , 3) ;
            MONOloop.jq('#'+id+'-l').css('height' , hight + 6) ;

            MONOloop.jq('#'+id+'-u').css('top' , top -3) ;
            MONOloop.jq('#'+id+'-u').css('left' ,  left ) ;
            MONOloop.jq('#'+id+'-u').css('width' , width) ;
            MONOloop.jq('#'+id+'-u').css('height' , 3) ;

            MONOloop.jq('#'+id+'-r').css('top' , top -3) ;
            MONOloop.jq('#'+id+'-r').css('left' ,  left + width) ;
            MONOloop.jq('#'+id+'-r').css('width' , 3) ;
            MONOloop.jq('#'+id+'-r').css('height' , hight + 6) ;

            MONOloop.jq('#'+id+'-s').css('top' , top + hight) ;
            MONOloop.jq('#'+id+'-s').css('left' ,  left ) ;
            MONOloop.jq('#'+id+'-s').css('width' , width) ;
            MONOloop.jq('#'+id+'-s').css('height' , 3) ;
          // }// Add content flow
          var tooltipWidth = MONOloop.jq('#temp-tooltip').outerWidth();
          var screenWidth = MONOloop.jq(window).width();
          if(id == 'ml-border'){
            if(top < 21){
              top = hight + 30;
              MONOloop.jq('#temp-tooltip').removeClass('tooltiptext').addClass('tooltiptext-bottom');
            }else{
              top = top -31;
            }
            if((left + tooltipWidth) > screenWidth){
              MONOloop.jq('#temp-tooltip').css({'top':top, 'left': (screenWidth-tooltipWidth)-2});
              MONOloop.jq('#tooltipArrow').css('left', left - ((screenWidth-tooltipWidth)-2));
            }else{
              MONOloop.jq('#temp-tooltip').css({'top':top, 'left': left}) ;
            }
          }
        }

      } ,
  displayAddContentOptions : function(){
          var borderFixes  = MONOloop.jq('.monoloopBorderFix');
          for( var i = 0 ; i < borderFixes.length ; i++){

            var $el = MONOloop.jq(borderFixes[i]);
            if($el.length > 0){

              MONOloop.jq('body').append('<div id="add_temp_before" class="tooltiptext" style="background-color: #de8d03 !important;"><span style="color:#FBADFF;"><i class="fa fa-plus-circle" aria-hidden="true" style="color: white !important;" title="Before" onclick="monoloop_select_view.add_content_add_before()" ></i></div>');
              MONOloop.jq('body').append('<div id="add_temp_after" class="tooltiptext" style="background-color: #de8d03 !important;"><span style="color:#FBADFF;"><i class="fa fa-plus-circle" aria-hidden="true" style="color: white !important;" title="After" onclick="monoloop_select_view.add_content_add_after()"></i></div>');
              var top = $el.offset().top ;
              var hight = $el.outerHeight();
              var bottom_ = $el.offset().bottom;
              var tooltipWidth = MONOloop.jq('#add_temp_before').width();
              var screenWidth = MONOloop.jq(window).width();
              var right = $el.offset().left + $el.outerWidth();
              MONOloop.jq('#add_temp_before').css({'top':top-25, 'left': right - ($el.outerWidth()/2)}) ;
              MONOloop.jq('#add_temp_after').css({'top':top+hight+3.5, 'left': right - ($el.outerWidth()/2)}) ;
        }
      }
  },
  add_content_add_before : function(){
    var temp_currentselector = currentselector; // saving for further usage
    var random_placement_id = "";
    var placement_div = "";
    var selector_Element = "";
    var element_selector = "";
    // stopping concurrent highlighting
    hover_flag = true;
    random_placement_id = monoloop_select_view.randomID('beforeplacement');
    var min_height = MONOloop.jq(currentselector).height();
    placement_div = "<div id="+random_placement_id+" class='before_placement' style='min-height: "+min_height+"px;'></div>";
    // Mantis 4702
    MONOloop.jq(placement_div).insertBefore(currentselector);
    // jQuery(document).ready(function ($) {
    //   $(placement_div).insertBefore(currentselector);
    // });
    selector_Element = MONOloop.jq('#'+random_placement_id);
    element_selector = monoloop_select_view.getXPath(selector_Element) ;
    monoloop_select_view.bindElement(element_selector);
    selector_Element.click();
    monoloop_select_view.adjust_all_bubble_positions();
    // side panel working
    // var xpath_content = monoloop_select_view.getXPathId(temp_currentselector);
    monoloop_select_view.postToParent(JSON.stringify({
          t: "sidebar_adjust_add_content" ,
          data: {
            xpath_content:temp_currentselector,
            data_type: 1,
            full_url : monoloop_select_view.originalUrl }
    }), "*");
    MONOloop.jq('#add_temp_before').remove();
    MONOloop.jq('#add_temp_after').remove();
    MONOloop.jq('#temp-tooltip_new').remove();
  },
  add_content_add_after : function(){
    var temp_currentselector = currentselector; // saving for further usage
    var random_placement_id = "";
    var placement_div = "";
    var selector_Element = "";
    var element_selector = "";
    // stopping concurrent highlighting
    hover_flag = true;
    random_placement_id = monoloop_select_view.randomID('afterplacement');
    var min_height = MONOloop.jq(currentselector).height();
    placement_div = "<div id="+random_placement_id+" class='after_placement' style='min-height: "+min_height+"px;'></div>";
    // Mantis #4702
    MONOloop.jq(placement_div).insertAfter(currentselector);
    // jQuery(document).ready(function ($) {
    //   $(placement_div).insertAfter(currentselector);
    // });

    selector_Element = MONOloop.jq('#'+random_placement_id);

    element_selector = monoloop_select_view.getXPath(selector_Element) ;
    monoloop_select_view.bindElement(element_selector);

    selector_Element.click();
    monoloop_select_view.adjust_all_bubble_positions();
    // side panel working
    // var xpath_content = monoloop_select_view.getXPathId(temp_currentselector);
    monoloop_select_view.postToParent(JSON.stringify({
          t: "sidebar_adjust_add_content" ,
          data: {
            xpath_content:temp_currentselector,
            data_type: 2,
            full_url : monoloop_select_view.originalUrl }
    }), "*");
    MONOloop.jq('#add_temp_before').remove();
    MONOloop.jq('#add_temp_after').remove();
    MONOloop.jq('#temp-tooltip_new').remove();
  },
  addContent_flow : function(){
    show_CKE = true;
    monoloop_select_view.postToParent(JSON.stringify({
          t: "initialize_addcontent_flow"}), "*");
  },
  toggle_bubble : function(val){
    var bubble_iD = monoloop_select_view.getXPathId(currentselector);
    if (val) {
      MONOloop.jq("#"+bubble_iD).show();
    }else {
      MONOloop.jq("#"+bubble_iD).hide();
    }

  },
  addVariation_flow : function (){
    // monoloop_select_view.toggleCkeditor(true);
    show_CKE = true;
    monoloop_select_view.initElement(currentselector);
    monoloop_select_view.toggle_bubble(false);
    MONOloop.jq('#temp-tooltip_new').remove();
    monoloop_select_view.postToParent(JSON.stringify({
          t: "initialize_addvariation_flow"}), "*");
  },
  hideElement_flow : function(){
    monoloop_select_view.postToParent(JSON.stringify({
          t: "initialize_hideElement_flow"}), "*");
  },
  displayWinOptions : function(){
          var borderFixes  = MONOloop.jq('.monoloopBorderFix');
          for( var i = 0 ; i < borderFixes.length ; i++){
          // icon replacement
          // <i class="fa fa-plus" aria-hidden="true" style="color: white !important;" title="Add Variation" onclick="monoloop_select_view.addVariation_flow()" ></i>
          var $el = MONOloop.jq(borderFixes[i]);
          if($el.length > 0){

            MONOloop.jq('body').append('<div class="tooltiptext" id="temp-tooltip_new"><span style="color:#FBADFF;"><svg aria-hidden="true" data-prefix="fas" data-icon="layer-group" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-layer-group fa-w-16" style="width: 13px; height: 11px; color: white;" onclick="monoloop_select_view.addVariation_flow()"><title>Add Variation</title><path fill="currentColor" d="M12.41 148.02l232.94 105.67c6.8 3.09 14.49 3.09 21.29 0l232.94-105.67c16.55-7.51 16.55-32.52 0-40.03L266.65 2.31a25.607 25.607 0 0 0-21.29 0L12.41 107.98c-16.55 7.51-16.55 32.53 0 40.04zm487.18 88.28l-58.09-26.33-161.64 73.27c-7.56 3.43-15.59 5.17-23.86 5.17s-16.29-1.74-23.86-5.17L70.51 209.97l-58.1 26.33c-16.55 7.5-16.55 32.5 0 40l232.94 105.59c6.8 3.08 14.49 3.08 21.29 0L499.59 276.3c16.55-7.5 16.55-32.5 0-40zm0 127.8l-57.87-26.23-161.86 73.37c-7.56 3.43-15.59 5.17-23.86 5.17s-16.29-1.74-23.86-5.17L70.29 337.87 12.41 364.1c-16.55 7.5-16.55 32.5 0 40l232.94 105.59c6.8 3.08 14.49 3.08 21.29 0L499.59 404.1c16.55-7.5 16.55-32.5 0-40z" class=""></path></svg>  <i class="fa fa-plus-circle" aria-hidden="true" style="color: white !important;" title="Add Content" onclick="monoloop_select_view.addContent_flow()" ></i> <i class="fa fa-eye-slash" aria-hidden="true" style="color: white !important;" title="Hide Element" onclick="monoloop_select_view.hideElement_flow()" ></i>  <i class="fa fa-arrow-down" aria-hidden="true" style="color: white !important;" title="Child" onclick="monoloop_select_view.SelectNext_Element()" ></i>  <i class="fa fa-arrow-up" aria-hidden="true" style="color: white !important;" title="Parent" onclick="monoloop_select_view.SelectPrevious_Element()"></i>  <i class="fa fa-eject" aria-hidden="true" onclick="monoloop_select_view.destroySelection()" style="color: white !important;" title="Release"></i></span></span></div>');
            var top = $el.offset().top ;
            var hight = $el.outerHeight();
            var tooltipWidth = MONOloop.jq('#temp-tooltip_new').width();
            var screenWidth = MONOloop.jq(window).width();
            var right = $el.offset().left + $el.outerWidth();
            if(top < 21){
              top = hight + 30;
              MONOloop.jq('#temp-tooltip_new').removeClass('tooltiptext').addClass('tooltiptext-bottom');
            }else{
              top = top - 25;
            }
            if((right + tooltipWidth) > screenWidth){
              MONOloop.jq('#temp-tooltip_new').css({'top':top, 'left': right - 103});
            }else{
              MONOloop.jq('#temp-tooltip_new').css({'top':top, 'left': right - tooltipWidth - 5}) ;
            }
          }
        }
        // change Ckeditor Position to show up from tooltips
       monoloop_select_view.adjustcke_position();
      },
  adjustcke_position: function(){
        for(var name in CKEDITOR.instances){
          var editor = CKEDITOR.instances[name];
          if (editor) { // Instance Exists
             var toptooltip = parseInt(MONOloop.jq("#temp-tooltip_new").css("top"));
             var topcke = parseInt(MONOloop.jq("#cke_" + editor.name).css("top"));
             var bottom_border = parseInt(MONOloop.jq("#ml-border-0-s").css("top"));
             var top_border = parseInt(MONOloop.jq("#ml-border-0-u").css("top"));

             setTimeout(function() {
               if (!MONOloop.jq("#cke_" + editor.name).hasClass("topcke")) {
                 MONOloop.jq("#cke_" + editor.name).removeClass("topcke1");
                 MONOloop.jq("#cke_" + editor.name).addClass("topcke");
               }
               if (bottom_border < toptooltip) {
                 // overlaps with tooltip and bottom border
                 MONOloop.jq("#cke_" + editor.name).removeClass("topcke");
                 MONOloop.jq("#cke_" + editor.name).addClass("topcke1");
               }
               if (top_border >= 50 && top_border <= 70) {
                 if (MONOloop.jq("#cke_" + editor.name).hasClass("topcke")) {
                   MONOloop.jq("#cke_" + editor.name).removeClass("topcke");
                   MONOloop.jq("#cke_" + editor.name).css("top",bottom_border+10);
                 }
               }
             },180);

           }
    }
  },
  getpreviousele : function(selector) {
          var $element = MONOloop.jq(selector);
          return $element
              .prev().find("*:last")
              .add($element.parent())
              .add($element.prev())
              .last();
      },
  SelectNext_Element : function(selector_var){
        MONOloop.jq('#temp-tooltip_new').remove();
        popover_flag = false;
        // stopping concurrent highlighting
        hover_flag = true;
        var elem_selector;
        if (selector_var != undefined) {
          elem_selector = selector_var;
        }else {
          elem_selector = currentselector;
        }
        // selector has been initialized
        var click_next_ele = 0;
        if (MONOloop.jq(elem_selector).next().length > 0 ) {
          click_next_ele = MONOloop.jq(elem_selector).next();
          if (click_next_ele[0].tagName != "SCRIPT" && click_next_ele[0].tagName != "STYLE") {
            if ( click_next_ele[0].tagName == "HR" || click_next_ele[0].tagName == "I" || click_next_ele[0].tagName == "IMG" || click_next_ele[0].tagName == "BUTTON" || click_next_ele[0].tagName == "INPUT" || click_next_ele[0].tagName == "LABEL" || click_next_ele[0].tagName == "FIGURE" ) {
              monoloop_select_view.SelectNext_Element(click_next_ele);
            }else if ((click_next_ele[0].tagName == "UL" || click_next_ele[0].tagName == "OL") && click_next_ele[0].className != "sub-menu") {
              if (click_next_ele[0].firstElementChild.contentEditable == "true") {
                click_next_ele.click();
              }else {
                click_next_ele[0].firstElementChild.click();
              }
            }else if (click_next_ele[0].className == "sub-menu") {
              monoloop_select_view.SelectNext_Element(click_next_ele);
            }else if (click_next_ele[0].hidden == true) {
              monoloop_select_view.SelectNext_Element(click_next_ele);
            }
            else {
              click_next_ele.click();
            }
          }
        }else {
          var parent_element = MONOloop.jq(elem_selector).parents().filter(function(){
                return MONOloop.jq(this).next().length > 0;
              }).next().first();
              if (parent_element.length > 0 && parent_element[0].tagName != "SCRIPT" && parent_element[0].tagName != "STYLE" ) {
                if ( parent_element[0].tagName == "HR" || parent_element[0].tagName == "I" || parent_element[0].tagName == "IMG" || parent_element[0].tagName == "BUTTON" || parent_element[0].tagName == "INPUT" || parent_element[0].tagName == "LABEL" || parent_element[0].tagName == "FIGURE") {
                  monoloop_select_view.SelectNext_Element(parent_element);
                }else if ((parent_element[0].tagName == "UL" || parent_element[0].tagName == "OL") && parent_element[0].className != "sub-menu") {
                  if (parent_element[0].firstElementChild.contentEditable == "true") {
                    parent_element.click();
                  }else {
                    parent_element[0].firstElementChild.click();
                  }
                }else if (parent_element[0].className == "sub-menu") {
                  monoloop_select_view.SelectNext_Element(parent_element);
                }else if (parent_element[0].hidden == true) {
                  monoloop_select_view.SelectNext_Element(parent_element);
                }
                else {
                  parent_element.click();
                }
              }
        }
      },
  SelectPrevious_Element : function(selector_var){
        MONOloop.jq('#temp-tooltip_new').remove();
        popover_flag = false;
        // stopping concurrent highlighting
        hover_flag = true;
        var elem_selector, flag_check1;
        var Prev_Ele;
        if (selector_var != undefined) {
          elem_selector = selector_var;
        }else {
          elem_selector = currentselector;
        }
        // Check if Previous Sibling Exists
        if (MONOloop.jq(elem_selector).prev().length > 0 ){
            Prev_Ele = MONOloop.jq(elem_selector).prev();
            if (Prev_Ele[0].tagName != "SCRIPT" && Prev_Ele[0].tagName != "STYLE"  ) {
              if ( Prev_Ele[0].tagName == "HR" || Prev_Ele[0].tagName == "I" || Prev_Ele[0].tagName == "IMG" || Prev_Ele[0].tagName == "BUTTON" || Prev_Ele[0].tagName == "INPUT" || Prev_Ele[0].tagName == "LABEL" || Prev_Ele[0].tagName == "FIGURE" ) {
                monoloop_select_view.SelectPrevious_Element(Prev_Ele);
              }else if ((Prev_Ele[0].tagName == "UL" || Prev_Ele[0].tagName == "OL") && Prev_Ele[0].className != "sub-menu") {
                if (Prev_Ele[0].firstElementChild.contentEditable == "true") {
                  Prev_Ele.click();
                }else {
                  Prev_Ele[0].firstElementChild.click();
                }
              }else if (Prev_Ele[0].className == "sub-menu") {
                monoloop_select_view.SelectPrevious_Element(Prev_Ele);
              }else if (Prev_Ele[0].hidden == true) {
                monoloop_select_view.SelectPrevious_Element(Prev_Ele);
              }else {
                Prev_Ele.click();
                flag_check1 = true;
              }
            }else {
              flag_check1 = false;
            }
        }else { // Check for Parent
          Prev_Ele = monoloop_select_view.getpreviousele(elem_selector);
          if (Prev_Ele.length > 0) {
            Prev_Ele.click();
            flag_check1 = true;
          }else {
            flag_check1 = false;
          }
        }
        if (flag_check1) {
          MONOloop.jq(elem_selector).removeAttr('contenteditable');
          MONOloop.jq(elem_selector).removeClass ('monoloopBorderFix cke_editable cke_editable_inline cke_contents_ltr cke_show_borders cke_focus');
          MONOloop.jq(elem_selector).removeAttr('id');
        }
      },
  destroySelection : function(){
        MONOloop.jq('#temp-tooltip_new').remove();
        for(var name in CKEDITOR.instances){
          var editor = CKEDITOR.instances[name];
               if (editor) {
                   MONOloop.jq('#cke_'+CKEDITOR.instances[name].name).hide();
               }
        }
        monoloop_select_view.clearSelection();
        // stopping concurrent highlighting
        hover_flag = true;
        // MONOloop.jq('.webui-popover').remove();
        contentmenu_iframe = undefined;
        monoloop_select_view.postToParent(JSON.stringify({
              t: "sidebar_editpanel" ,
              data: {contentmenu_iframe,
                full_url : monoloop_select_view.originalUrl }
        }), "*");
      },

  disPlayAllBorder : function(){
    var me = this ;
    me.removeAllBorder();
    // Display border fixed.
    var borderFixes  = MONOloop.jq('.monoloopBorderFix');
    for( var i = 0 ; i < borderFixes.length ; i++){
      me.addAndDisplayBorder('ml-border-' + i , borderFixes[i]);
    }
  } ,
  displayCursorBorder : function(){
    var me = this;
    var cursorBorder = MONOloop.jq('.monoloopBorder');
    me.addAndDisplayBorder('ml-border'  , cursorBorder[0]);
  } ,
  variationsGroupByXpath: function(data, fn){
    var groups = {};
    data.forEach( function( o ){
      var group = JSON.stringify( fn(o) );
      groups[group] = groups[group] || [];
      groups[group].push( o );
    });
    return Object.keys(groups).map( function( group ){
      return groups[group];
    })
  },
  getXPathId: function(xpath){
    return xpath.replace(/ /g, "_").replace(/#/g, "_").replace(/\./g,"_").replace(/\(/g,"_").replace(/\)/g,"_").replace(/:/g,"_");
  },
  createImageCounterBubble: function(data){
    var selector = data.mappedTo,
        totalImages,
        element;
    MONOloop.jq('.imageCounter').remove();
    element = monoloop_select_view.getSelectorByXPath(selector);
    if(MONOloop.jq(element).length !== 0){
      totalImages = element.find('img').length;
      if(totalImages > 0){
        var right = MONOloop.jq(element).offset().left + MONOloop.jq(element).outerWidth();
        var top = MONOloop.jq(element).offset().top ;
        var bubbleHtml = '<div class="speech-bubble red-bubble imageCounter ' + data.selectorId + '" style="position: absolute;top: ' + (top + 20) + 'px' + ';left: ' + (right - 100) + 'px' + ';" >' + totalImages + '</div>';
        MONOloop.jq('body').prepend(bubbleHtml);
      }
    }
  },
  createDeleteBubble: function(data){
    var selector = data.mappedTo,
        totalVariations,
        element,
        selectorId = selector.replace(/#/g, monoloop_select_view.hashFn);

    element = monoloop_select_view.getSelectorByXPath(selector);
    if(MONOloop.jq(element).length !== 0){
      var right = MONOloop.jq(element).offset().left + MONOloop.jq(element).outerWidth();
      var top = MONOloop.jq(element).offset().top ;
      var id = "hiddenBubble_" + data.selectorId;
      var bubbleHtml = '<div id="'+id+'" originalUrl="'+monoloop_select_view.currentPageElement.originalUrl+'" selector="'+selector+'"  class="speech-bubble removedElement ' + data.selectorId + '" style="position: absolute;top: ' + top + 'px' + ';left: ' + (right - 50) + 'px' + ';" >' + 'X' + '</div>';
      MONOloop.jq('body').prepend(bubbleHtml);

      if(monoloop_select_view.currentPageElement){
        var iframeUrl = MONOloop.domain + '/component/content-list?fullURL=' + monoloop_select_view.currentPageElement.originalUrl;
        iframeUrl += ('&xpath=' + selectorId);
        iframeUrl += ('&hash_fn=' + monoloop_select_view.hashFn);
        iframeUrl += ('&page_element_id=' + monoloop_select_view.currentPageElement._id);
        var _id = monoloop_select_view.getXPathId(selector);
        if(MONOloop.jq("#"+_id).length > 0){
          // monoloop_select_view.openVariationPopover(iframeUrl, id, 'deleteBubble', {t: 'delete'});
        }
      }


    }
  },
  hashFn: "_mp_hash_function_",
  bubblePositions: [],
  showVariationBubbles: function(data, dataSet){
    monoloop_select_view.variationsBubbleCalled = true;
    var selector,
        totalVariations,
        element,
        iframeUrl,
        hashFn = monoloop_select_view.hashFn;


    data.forEach(function(node){
      iframeUrl = MONOloop.domain + '/component/content-list?fullURL=' + dataSet.originalUrl;
      selector = node[0].mappedTo;
      selectorId = selector.replace(/#/g, hashFn);
      totalVariations = node.length || 0;
      element = monoloop_select_view.getSelectorByXPath(selector);

      if(monoloop_select_view.selectorsHaveVariations.indexOf(selector) === -1){
        monoloop_select_view.selectorsHaveVariations.push(selector);
      }


      monoloop_select_view.storeOriginalContent({
        xpath: selector,
        content: element.html()
      });

      iframeUrl += ('&xpath=' + selectorId);
      iframeUrl += ('&hash_fn=' + hashFn);
      iframeUrl += ('&page_element_id=' + dataSet.pageElement._id);

      var contentSelectorId = monoloop_select_view.getXPathId(selector), iframeUrl = '';
      iframeUrl = MONOloop.domain + '/component/content-menu?fullURL=' + monoloop_select_view.originalUrl;
      iframeUrl += ('&xpath=' + contentSelectorId);
      iframeUrl += ('&hash_fn=' + monoloop_select_view.hashFn);

      var selectorId = selector.replace(/#/g, monoloop_select_view.hashFn);
      var element = monoloop_select_view.getSelectorByXPath(selector);
      iframeUrl += ('&xpath1=' + selectorId);

      if(monoloop_select_view.currentPageElement){
        iframeUrl += ('&page_element_id=' + monoloop_select_view.currentPageElement._id);
      }

      if(monoloop_select_view.source !== undefined){
        iframeUrl += ('&source=' + monoloop_select_view.source);
      }
      if(monoloop_select_view.currentExperiment !== undefined){
        iframeUrl += ('&exp=' + monoloop_select_view.currentExperiment._id);
      }
      if(monoloop_select_view.currentFolder){
        iframeUrl += ('&current_folder=' + monoloop_select_view.currentFolder);
      }
      if(monoloop_select_view.page){
        iframeUrl += ('&page=' + monoloop_select_view.page);
      }

      iframeUrl += ('&tVar=' + monoloop_select_view.totalVariations);

      if(monoloop_select_view.newExperimentBackUp){
        iframeUrl += ('&bk_seg=' + monoloop_select_view.newExperimentBackUp.segmentExperiment);
        iframeUrl += ('&bk_goal=' + monoloop_select_view.newExperimentBackUp.goalExperiment);
      }




      if(MONOloop.jq(element).length !== 0){


        // Adjust bubblePosition
        monoloop_select_view.adjustBubblePosition(element);
        var _id = monoloop_select_view.getXPathId(selector);
        var bubble = "bubble";
        var bubbleHtml = '<div id="'+_id+'" originalUrl="'+dataSet.originalUrl+'" selector="'+selector+'"onclick="monoloop_select_view.bubbleClick(\''+_id+'\',\''+ iframeUrl +'\',\''+ totalVariations +'\',\''+bubble+'\')" class="speech-bubble variations-counter"  style="position: absolute;top: ' + bubble_top + 'px' + ';left: ' + (bubble_right - 25) + 'px' + ';" >' + totalVariations + '</div>';
        MONOloop.jq('body').prepend(bubbleHtml);
        if(node[0].inlineContent != null){
          var extraClass = 'edit_temp' + node.length;
          MONOloop.jq(selector).addClass(extraClass);
          if(MONOloop.jq('.'+extraClass).length > 1){
            var inlineElement = MONOloop.jq('.'+extraClass)[0];
          }else{
            var inlineElement = MONOloop.jq('.'+extraClass);
          }

          var divID = 'inline_content_'+node[0]._id;
          var inlineHtml = '<div id="'+ divID + '" class="monoloop_inline_content" >' +  node[0].inlineContent + '</div>'

          if(  node[0].placementType == 0 ){
              MONOloop.jq( inlineElement).before(inlineHtml) ;
          }else if( node[0].placementType == 1 ){
              MONOloop.jq( inlineElement ).after(inlineHtml) ;
          }else if( node[0].placementType == 2  || node[0].placementType == 5){
              MONOloop.jq( inlineElement ).html(inlineHtml) ;
          }else if( node[0].placementType == 3 ){
              MONOloop.jq( inlineElement ).append(inlineHtml) ;
          }else if( node[0].placementType == 4 ){
              MONOloop.jq( inlineElement ).replaceWith(inlineHtml) ;
          }
        }
        // Mantis #4376
       BubbleSelector[bubble_counter] = selector;
       bubble_iframecontent[bubble_counter] = iframeUrl;
       bubble_counter++;
        // monoloop_select_view.openVariationPopover(iframeUrl, _id, 'varCountBubble', {title: "Variation(s)",t: 'content_list'});
      } else {

      }


    });
  },
  adjustBubblePosition: function(element){

      bubble_right = MONOloop.jq(element).offset().left + MONOloop.jq(element).outerWidth();
      bubble_top = MONOloop.jq(element).offset().top ;

      if(monoloop_select_view.bubblePositions.length > 0){
        for (var i = 0; i < monoloop_select_view.bubblePositions.length; i++) {
          if(monoloop_select_view.bubblePositions[i].right == bubble_right && monoloop_select_view.bubblePositions[i].top == bubble_right){
            bubble_right = bubble_right - 50;
            bubble_top = bubble_right;
            break;
          }
        }
      }
      monoloop_select_view.bubblePositions.push({right: bubble_right, top: bubble_top});

  },
  // adjust the popover position so that it doesn't intersects with CKEditor's Toolbar
  adjustPopoverPosition: function(){
    var _offset = MONOloop.jq('.webui-popover').offset();
    var _position = monoloop_select_view.getPopoverPosition();
    var _offsetDelta = (70);
    switch(_position){
      // as the toolbar is always on TOP, so we have to give popover DIV a small margin to keep it above the CKEITOR's Toolbar
      case "top":{
        // MONOloop.jq('.webui-popover').offset({top: _offset.top - _offsetDelta, left: _offset.left});
        MONOloop.jq('.webui-popover').offset({top: _offset.top - (_offsetDelta + 60), left: _offset.left});
        break;
      }
      case "bottom":{
        MONOloop.jq('.webui-popover').offset({top: _offset.top + (_offsetDelta - 30), left: _offset.left});
        break;
      }
    }
  },
  getPopoverPosition: function(){
    // get the current popover instance
    var currentPopover = MONOloop.jq('.webui-popover');
    var popoverArrowOffset = MONOloop.jq('.webui-arrow', currentPopover).offset();
    var popoverInnerOffset = MONOloop.jq('.webui-popover-inner', currentPopover).offset();
    return ((popoverArrowOffset.top < popoverInnerOffset.top) ? 'bottom' : 'top');
  },
  openVariationPopover: function(url, id, type, options){
    var settings = {
                  trigger:'click',
                  title:options.title,
                  content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
                  // width:auto,
                  multi:false,
                  closeable:true,
                  style:'',
                  delay:300,
                  padding:true,
                  backdrop:false
              };
    var iframeSettings = {  width:360,
                           height:420,
                           closeable:true,
                           padding:false,
                           multi:false,
                           trigger: "manual",
                           placement: "vertical",
                           dismissible: false,
                           autoHide:false,
                           type:'iframe',
                           cache:false,
                           url: url,
                           offsetTop:20,

                           onHide: function($element) {
                             monoloop_select_view.restoreOriginalContents();
                             if(type === 'content'){
                               var exID = MONOloop.jq(id).attr('id');
                               if(!exID){
                                 MONOloop.jq(id).prop('id', monoloop_select_view.randomID());
                               }
                               var id_ = MONOloop.jq(id).attr('id');

                               MONOloop.jq("#"+id_).webuiPopover('destroy');
                               MONOloop.jq('.webui-popover').remove();

                               monoloop_select_view.bindElement('#' + MONOloop.jq(id).attr('id'));
                             }
                           },
                           onShow: function($element) {
                             if (type == "varCountBubble" ) {
                               currentselector = monoloop_select_view.getXPathId(currentselector);
                               if (currentselector.indexOf(id) != 0) {
                                 for(var name in CKEDITOR.instances){
                                   var editor = CKEDITOR.instances[name];
                                        if (editor) {
                                            MONOloop.jq('#cke_'+CKEDITOR.instances[name].name).hide();
                                        }
                                      }
                                 monoloop_select_view.clearSelection();
                               }
                               MONOloop.jq('#temp-tooltip_new').remove();
                               popover_flag = true;
                             }else {
                               monoloop_select_view.adjustPopoverPosition($element);
                             }
                           }
                           // url: 'http://webix.com/'
                         };
    if(type === 'content'){
      MONOloop.jq(id).webuiPopover(MONOloop.jq.extend({},settings,iframeSettings));
    } else {
      iframeSettings.trigger = 'click'; // click-enabled popover incase of bubbles
      MONOloop.jq('#'+id).webuiPopover(MONOloop.jq.extend({},settings,iframeSettings));
    }
  },
  FAbEventFns: {
    variation: function(t, options){
      // if(options.selector){
      //
      //  // attach popover with add variation
      //  var url = options.url;
      //
      //  var selectorId = options.selector.replace(/#/g, options.hashFn);
      //  var element = monoloop_select_view.getSelectorByXPath(options.selector);
      //
      //  url += ('&xpath=' + selectorId);
      //  url += ('&hash_fn=' + options.hashFn);
      //  if(options.currentPageElement){
      //    url += ('&page_element_id=' + options.currentPageElement._id);
      //  }
      //  if(options.source !== undefined){
      //    url += ('&source=' + options.source);
      //  }
      //  if(options.experiment !== undefined){
      //    url += ('&exp=' + options.experiment._id);
      //  }
      //  if(t === 'edit'){
      //    if(options.content_id){
      //      url += ('&content_id=' + options.content_id);
      //    }
      //  }
      //  monoloop_select_view.initPopover(
      //    url,
      //    // '.actionButton.updateVariation.inline',
      //    options.domAddr,
      //    {
      //      width:360,
      //      placement: 'auto', // auto will place it automatically
      //      height:250,
      //      closeable:true,
      //      multi:true,
      //      padding:false,
      //      cache:false,
      //      offsetTop: (options.placement !== 'toolbar' ? -125 : 0),
      //      type:'iframe',
      //      title: options.title,
      //      url: url,
      //      onShow: function($element){
      //        if(options.placement !== 'toolbar'){
      //          var clientHeight = $element.height();
      //          $element.find('.webui-arrow').css('top', (clientHeight - 25) + 'px');
      //        }
      //      }
      //    },
      //    options.placement
      //  );
      // }
    },
    experiment: function(t, options){
      // // console.log("experiment: options", options);
      // // attach popover with add variation
      // var url = options.url;
      //
      // var selectorId = options.selector.replace(/#/g, options.hashFn);
      // var element = monoloop_select_view.getSelectorByXPath(options.selector);
      //
      // url += ('&xpath=' + selectorId);
      // url += ('&hash_fn=' + options.hashFn);
      // if(options.currentPageElement){
      //  url += ('&page_element_id=' + options.currentPageElement._id);
      // }
      // if(options.source !== undefined){
      //  url += ('&source=' + options.source);
      // }
      // if(options.currentFolder){
      //  url += ('&current_folder=' + options.currentFolder);
      // }
      // if(options.page){
      //  url += ('&page=' + options.page);
      // }
      // if(t === 'edit'){
      //  if(options.content_id){
      //    url += ('&content_id=' + options.content_id);
      //  }
      // }
      // if(monoloop_select_view.newExperimentBackUp){
      //  url += ('&bk_seg=' + monoloop_select_view.newExperimentBackUp.segmentExperiment);
      //  url += ('&bk_goal=' + monoloop_select_view.newExperimentBackUp.goalExperiment);
      // }
      // monoloop_select_view.initPopover(
      //  url,
      //  // '.actionButton.updateVariation.inline',
      //  options.domAddr,
      //  {
      //    width:360,
      //    placement: 'auto', // auto will place it automatically
      //    height:420,
      //    offsetTop: (options.placement !== 'toolbar' ? -210 : 0),
      //    closeable:true,
      //    padding:false,
      //    multi:true,
      //    cache:false,
      //    type:'iframe',
      //    title: options.title,
      //    url: url,
      //    onShow: function($element){
      //      if(options.placement !== 'toolbar'){
      //        var clientHeight = $element.height();
      //        $element.find('.webui-arrow').css('top', (clientHeight - 25) + 'px');
      //      }
      //
      //    }
      //  },
      //  options.placement
      // );
    },
    segment: function(t, options){
      // console.log("experiment: options", options);
      // attach popover with add variation
      var url = options.url;


      if(options.source !== undefined){
        url += ('&source=' + options.source);
      }
      if(options.currentFolder){
        url += ('&current_folder=' + options.currentFolder);
      }
      // console.log("(options.placement !== 'toolbar' ? -210 : 0)",(options.placement !== 'toolbar' ? -210 : 0));
      monoloop_select_view.initPopover(
        url,
        // '.actionButton.updateVariation.inline',
        options.domAddr,
        {
          // width:1100,
          width: 360,
          placement: 'top-left', // auto will place it automatically
          // height:520,
          height: 420,
          // offsetTop: (options.placement !== 'toolbar' ? -210 : 0),
          closeable:true,
          padding:false,
          multi: true,
          cache:false,
          style: "position: fixed;",
          type:'iframe',
          title: options.title,
          url: url,
          onShow: function($element){
            // console.log("options.placement out", options.placement);
            // if(options.placement !== 'toolbar'){
            //  console.log("options.placement", options.placement);
            //  var clientHeight = $element.height();
            //  $element.find('.webui-arrow').css('top', (clientHeight - 25) + 'px');
            // }
          },
          onHide: function(){
            MONOloop.jq('.subActionButton.segmentBuilder.inline.var').hide();
          },
        },
        options.placement
      );
    },
    goal: function(t, options){
      var url = options.url;


      if(options.source !== undefined){
        url += ('&source=' + options.source);
      }
      if(options.currentPageElement){
        url += ('&page_element_id=' + options.currentPageElement._id);
      }
      if(options.currentFolder){
        url += ('&current_folder=' + options.currentFolder);
      }

      monoloop_select_view.initPopover(
        url,
        // '.actionButton.updateVariation.inline',
        options.domAddr,
        {
          width:360,
          placement: 'top-left', // auto will place it automatically
          height:420,
          // offsetTop: (options.placement !== 'toolbar' ? -210 : 0),
          closeable:true,
          padding:false,
          multi: true,
          cache:false,
          type:'iframe',
          title: options.title,
          url: url,
          onShow: function($element){
            // if(options.placement !== 'toolbar'){
            //  var clientHeight = $element.height();
            //  $element.find('.webui-arrow').css('top', (clientHeight - 25) + 'px');
            // }
          },
          onHide: function(){
            MONOloop.jq('.subActionButton.goalBuilder.inline.var').hide();
          }
        },
        options.placement
      );
    }
  },
  actionButtonTypes: {
    type: "pageLevel",
    setType: function(t){
      t = t || "pageLevel";
      monoloop_select_view.actionButtonTypes.type = t;
    },
    fabOptions: false,
    pageLevel: {
      options: [
        {
          label: "Add audience",
          className: "segmentBuilder inline",
        }, {
          label: "Add goal",
          className: "goalBuilder inline",
        },
      ]
    },
    addVariation: {
      options: [

         {
          label: "Add condition",
          className: "conditionBuilder inline",
        },{
          label: "Add asset",
          className: "assetBuilder inline",
        },{
          label: "Hide",
          className: "remove inline",
        }
        ,
        {
          label: "Add audience",
          className: "segmentBuilder inline var",
        }, {
          label: "Add goal",
          className: "goalBuilder inline var",
        }
        ,{
          label: "Add to experience",
          className: "experimentBuilder inline",
        }
      ]
    },
    updateVariation: {
      options: [
        {
          label: "Add audience",
          className: "segmentBuilder inline var",
        }, {
          label: "Add goal",
          className: "goalBuilder inline var",
        }, {
          label: "Add condition",
          className: "conditionBuilder inline",
        },{
          label: "Add asset",
          className: "assetBuilder inline",
        },{
          label: "Hide",
          className: "remove inline",
        },{
          label: "Add to experience",
          className: "experimentBuilder inline",
        }
      ]
    }
  },
  initActionButtons: function(fabOptions){
    monoloop_select_view.actionButtonTypes.fabOptions = fabOptions;

    if(MONOloop.jq('#floatingContainer').length > 0){
      MONOloop.jq('#floatingContainer').remove();
    }

    var body = MONOloop.jq('body');

    var floatingContainer = document.createElement('div');
    floatingContainer.id = 'floatingContainer';
    floatingContainer.className = 'floatingContainer';



    var elementOption = monoloop_select_view.actionButtonTypes[monoloop_select_view.actionButtonTypes.type],
        buttons = elementOption.options;

    for (var i = 0; i < buttons.length; i++) {
      var subActionButton = document.createElement('div');
      subActionButton.className = "subActionButton " + buttons[i].className;

      var floatingText = document.createElement('p');
      floatingText.className = "floatingText";

      var floatingTextBG = document.createElement('span');
      floatingTextBG.className = "floatingTextBG";
      floatingTextBG.textContent = buttons[i].label;

      floatingText.appendChild(floatingTextBG);

      subActionButton.appendChild(floatingText);

      floatingContainer.appendChild(subActionButton);

    }

    var actionButton = document.createElement('div');
    actionButton.className = "actionButton";

    var floatingText = document.createElement('p');
    floatingText.className = "floatingText";

    var floatingTextBG = document.createElement('span');
    floatingTextBG.className = "floatingTextBG";
    floatingTextBG.textContent = "View Options";

    switch (monoloop_select_view.actionButtonTypes.type) {
      case 'pageLevel':
        // actionButton.className += " addVariation inline";
        // floatingTextBG.textContent = "Add Variation";
        break;
      case 'addVariation':
        actionButton.className += " addVariation inline";
        floatingTextBG.textContent = "Add Variation";
        break;
      case 'updateVariation':
        actionButton.className += " updateVariation inline";
        floatingTextBG.textContent = "Update Variation";
        break;
      default:
        break;

    }

    // floatingContainer.style.top = top + 'px';

    floatingText.appendChild(floatingTextBG);

    actionButton.appendChild(floatingText);

    floatingContainer.appendChild(actionButton);

    body.append(floatingContainer);

    monoloop_select_view.triggerActionButtonEvents(fabOptions);

  },
  triggerActionButtonEvents: function(fabOptions){
    MONOloop.jq('.floatingContainer').hover(function(){
      MONOloop.jq('.subActionButton').addClass('display'); // comment
    }, function(){
      MONOloop.jq('.subActionButton').removeClass('display'); // comment
      MONOloop.jq('.actionButton').removeClass('open'); // comment
    });
    MONOloop.jq('.subActionButton').hover(function(){
      MONOloop.jq(this).find('.floatingText').addClass('show');
    }, function(){
      MONOloop.jq(this).find('.floatingText').removeClass('show');
    });

    MONOloop.jq('.actionButton').hover(function(){
      MONOloop.jq(this).addClass('open');
      MONOloop.jq(this).find('.floatingText').addClass('show');
      MONOloop.jq('.subActionButton').addClass('display');
    }, function(){
      MONOloop.jq(this).find('.floatingText').removeClass('show');
    });
    monoloop_select_view.attachOnlickEvents(fabOptions);
  },
  attachOnlickEvents: function(fabOptions){

    var fns = monoloop_select_view.FAbEventFns;


    var editor = CKEDITOR.instances[fabOptions.editorID];
    switch (monoloop_select_view.actionButtonTypes.type) {
      case 'pageLevel':
        // for segment
        fns.segment('add', {
          url: MONOloop.domain + '/component/content-add-segment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.segmentBuilder.inline',
          title: 'Add audience',
        });
        // for goal
        fns.goal('add', {
          url: MONOloop.domain + '/component/content-add-goal?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentPageElement: monoloop_select_view.currentPageElement,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.goalBuilder.inline',
          title: 'Add goal',
        });
        break;
      case 'addVariation':
        var url = MONOloop.domain + '/component/content-create?fullURL=' + monoloop_select_view.originalUrl + '&tVar=' + monoloop_select_view.totalVariations;
        fns.variation('add', {
          url: url,
          originalUrl: monoloop_select_view.originalUrl,
          hashFn: monoloop_select_view.hashFn,
          selector: fabOptions.selector,
          currentPageElement: monoloop_select_view.currentPageElement,
          currentFolder: monoloop_select_view.currentFolder,
          source: monoloop_select_view.source,
          page: monoloop_select_view.page,
          experiment: monoloop_select_view.currentExperiment,
          domAddr: '.actionButton.addVariation.inline',
          title: 'Add variation',
        });
        // for experiments
        fns.experiment('add', {
          url: MONOloop.domain + '/component/content-add-to-experiment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          hashFn: monoloop_select_view.hashFn,
          selector: fabOptions.selector,
          currentPageElement: monoloop_select_view.currentPageElement,
          source: monoloop_select_view.source,
          page: monoloop_select_view.page,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.experimentBuilder.inline',
          title: 'Experiences',
        });

        // for segment
        fns.segment('add', {
          url: MONOloop.domain + '/component/content-add-segment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentFolder: monoloop_select_view.currentFolder,
          page: monoloop_select_view.page,
          domAddr: '.subActionButton.segmentBuilder.inline',
          title: 'Add audience',
        });

        // for goal
        fns.goal('add', {
          url: MONOloop.domain + '/component/content-add-goal?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentPageElement: monoloop_select_view.currentPageElement,
          page: monoloop_select_view.page,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.goalBuilder.inline',
          title: 'Add goal',
        });

        MONOloop.jq('.subActionButton.conditionBuilder.inline').on('click', function(e){
          editor.execCommand('insertCb', 'variation');
        });
        MONOloop.jq('.subActionButton.assetBuilder.inline').on('click', function(e){
          editor.execCommand('insertAsset');
        });
        MONOloop.jq('.subActionButton.remove.inline').on('click', function(e){
          editor.execCommand('insertRemove');
          if(monoloop_select_view.selectedElementsOnPage.length > 0){
            for (var i = 0; i < monoloop_select_view.selectedElementsOnPage.length; i++) {
              var view = monoloop_select_view.getSelectorByXPath(monoloop_select_view.selectedElementsOnPage[i]);
              view.html('');
            }
          }
        });
        break;
      case 'updateVariation':

        var url = MONOloop.domain + '/component/content-update?fullURL=' + monoloop_select_view.originalUrl;
        fns.variation('edit', {
          url: url,
          originalUrl: monoloop_select_view.originalUrl,
          hashFn: monoloop_select_view.hashFn,
          selector: fabOptions.selector,
          currentPageElement: monoloop_select_view.currentPageElement,
          source: monoloop_select_view.source,
          currentFolder: monoloop_select_view.currentFolder,
          page: monoloop_select_view.page,
          content_id: fabOptions.content_id,
          domAddr: '.actionButton.updateVariation.inline',
          title: 'Update variation',
        });

        // for experiments
        fns.experiment('edit', {
          url: MONOloop.domain + '/component/content-add-to-experiment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          hashFn: monoloop_select_view.hashFn,
          selector: fabOptions.selector,
          currentPageElement: monoloop_select_view.currentPageElement,
          page: monoloop_select_view.page,
          source: monoloop_select_view.source,
          currentFolder: monoloop_select_view.currentFolder,
          content_id: fabOptions.content_id,
          domAddr: '.subActionButton.experimentBuilder.inline',
          title: 'Experiences',
        });

        // for segment
        fns.segment('edit', {
          url: MONOloop.domain + '/component/content-add-segment?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          page: monoloop_select_view.page,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.segmentBuilder.inline',
          title: 'Add audience',
        });

        // for goal
        fns.goal('edit', {
          url: MONOloop.domain + '/component/content-add-goal?fullURL=' + monoloop_select_view.originalUrl,
          originalUrl: monoloop_select_view.originalUrl,
          source: monoloop_select_view.source,
          currentPageElement: monoloop_select_view.currentPageElement,
          page: monoloop_select_view.page,
          currentFolder: monoloop_select_view.currentFolder,
          domAddr: '.subActionButton.goalBuilder.inline',
          title: 'Add goal',
        });

        editor.title = fabOptions.content_condition;
        MONOloop.jq(editor.element.$).attr('title',fabOptions.content_condition);
        MONOloop.jq('.subActionButton.conditionBuilder.inline').on('click', function(e){
          editor.execCommand('insertCb', {
            type: 'variation',
            condition: fabOptions.content_condition
          });

          // monoloop_select_view.postToParent(JSON.stringify({
          //  t: 'open-condition-builder',
          //  condition: fabOptions.content_condition,
          //  type: 'variation'
          // }));
        });
        MONOloop.jq('.subActionButton.assetBuilder.inline').on('click', function(e){
          editor.execCommand('insertAsset');
        });
        MONOloop.jq('.subActionButton.remove.inline').on('click', function(e){
          editor.execCommand('insertRemove');
        });
        break;
      default:
        break;

    }
    MONOloop.jq('.subActionButton.segmentBuilder.inline.var').hide();
    MONOloop.jq('.subActionButton.goalBuilder.inline.var').hide();

    monoloop_select_view.actionButtonTypes.setType();
  },
  initPopover: function(url, id, iframeSettings, placement){

    if(placement !== 'toolbar'){
      MONOloop.jq(".webui-popover").remove();
    }

    var settings = {
                  trigger:'click',
                  title:'Variation(s)',
                  content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
                  // width:auto,
                  multi:false,
                  closeable:false,
                  style:'',
                  delay:300,
                  padding:true,
                  backdrop:false
              };
    // console.log("iframeSettings",iframeSettings);
    MONOloop.jq(id).webuiPopover(MONOloop.jq.extend({},settings,iframeSettings));

  },
  showPreplace : function( records ){
    var me = this ;
    me.records = records ;
        MONOloop.jq('#preplaced-panel').html('') ;
        data = {} ;
        for( i = 0 ; i < records.length ; i++ ){
            if( typeof data[records[i]['element_id']] == "undefined" )
                data[records[i]['element_id']] = 1 ;
            else
                data[records[i]['element_id']]++ ;
        }
        i = 0 ;
        for (var key in data) {
            if( MONOloop.jq(key).length == 0)
                continue ;
            var right = MONOloop.jq(key).offset().left + MONOloop.jq(key).outerWidth();
            var top = MONOloop.jq(key).offset().top ;
            MONOloop.jq('#preplaced-panel').append('<div class="speech-bubble" onclick="monoloop_select_view.bubbleClick(event , \''+key+'\')" id="preplace-'+i+'" onmouseover="monoloop_select_view.bubbleMouseOver(event , \''+key+'\')">'+data[key]+'</div>') ;
            MONOloop.jq('#preplace-'+i).addClass('speech-bubble') ;
            MONOloop.jq('#preplace-'+i).css('top' , top) ;
            MONOloop.jq('#preplace-'+i).css('left' ,  right - 25) ;
            i++ ;
        }
    } ,
    reset_selected_element : function(){
      MONOloop.jq('#temp-tooltip_new').remove();
      monoloop_select_view.makeSureIfAnyElementSelected();
      monoloop_select_view.clearSelection();
      // stopping concurrent highlighting
      hover_flag = true;
    },
    bubbleClick : function(id , iframeurl,tvar,type){
        // resetting the selected element
        monoloop_select_view.reset_selected_element();

        for (var i = 0; i < bubble_iframecontent.length; i++) {
          if (bubble_iframecontent[i].indexOf(id) != -1) {
            monoloop_select_view.postToParent(JSON.stringify({
                  t: "sidebar_editpanel" ,
                  data: {
                    contentmenu_iframe:bubble_iframecontent[i],
                    id: id,
                    tvar:tvar,
                    type: type,
                    full_url : monoloop_select_view.originalUrl }
            }), "*");
          }
        }
      //   MONOloop.jq(selector).addClass('monoloopBorderFix') ;
      //   monoloop_select_view.postToParent(JSON.stringify({
      //     t: "select-bubbleClick"  ,
      //     selector : selector
      //   }), "*")
      //   agent = MONOloop.jq.browser;
      // if(agent.msie) {
      //   window.event.cancelBubble = true;
      // } else {
      //   e.stopPropagation();
      // }
    } ,
    bubbleMouseOver : function(e , selector){
        if( monoloop_select_view.lock == false ){
            agent = MONOloop.jq.browser;
            if(agent.msie) {
            window.event.cancelBubble = true;
            } else {
              e.stopPropagation();
            }
            MONOloop.jq('*').removeClass('monoloopBorder') ;
            MONOloop.jq('#temp-tooltip').remove();
            MONOloop.jq(selector).addClass('monoloopBorder') ;
            monoloop_select_view.displayCursorBorder() ;
        }
    } ,
    bindElement : function(element, t){

      element = element || "";
      var appllyEventsTo;

      if(element){
        appllyEventsTo = element + ', ' + element + ' *'; // :not(em, strong, u, .cke, .cke *)
      } else {
        appllyEventsTo = '*'; // :not(em, strong, u, .cke, .cke *)
      }


      MONOloop.jq(appllyEventsTo).unbind("mouseenter");
      MONOloop.jq(document).on('mouseenter',appllyEventsTo, function(event) {
        if(MONOloop.jq(this).is('.speech-bubble, .variations-counter'))
          return false;
          if(['html', 'body'].indexOf(MONOloop.jq(this).prop('tagName').toLowerCase()) === -1){
            if( monoloop_select_view.lock == false ){
                event.stopPropagation();

                // if parent is in edit-mode, return FALSE
                if(monoloop_select_view.isParentAlreadyInEditMode(this))
                  return false;
                // stopping concurrent highlighting
                if (hover_flag) {
                  MONOloop.jq('*').removeClass('monoloopBorder');
                  MONOloop.jq('#temp-tooltip').remove();
                  MONOloop.jq(this).addClass('monoloopBorder');
                  monoloop_select_view.displayCursorBorder();
                }


            }
          }
          return false;
      });
      MONOloop.jq(appllyEventsTo).unbind("mouseleave");
      MONOloop.jq(appllyEventsTo).mouseleave(function(event) {
          if(['html', 'body'].indexOf(MONOloop.jq(this).prop('tagName').toLowerCase()) === -1){
            if( monoloop_select_view.lock == false ){
                event.stopPropagation();

                MONOloop.jq(this).removeClass('monoloopBorder') ;
                MONOloop.jq('#temp-tooltip').remove();
                monoloop_select_view.displayCursorBorder() ;
            }

          }
          return false ;
      });
      MONOloop.jq(appllyEventsTo).unbind("click");
      MONOloop.jq('body').on('click', appllyEventsTo, function(event) {
        // extra checks for Svg layer-group icon
        if(MONOloop.jq(this).is('.speech-bubble, .variations-counter'))
          return false;
        var target_class_name = false;
        if (event.target.className.animVal === undefined) {
          if (event.target.className.startsWith('cke')) {
            target_class_name = true;
          }
        }
        // stopping concurrent highlighting
        if (hover_flag == false) {
          return false;
        }
        // if(event.target.className.startsWith('cke')){
        if(target_class_name){
          return false;
        }else{
          event.stopPropagation();
          // if parent is in edit-mode, return FALSE
          if(monoloop_select_view.isParentAlreadyInEditMode(this))
            return false;
          if (popover_flag) {
              MONOloop.jq('.webui-popover').remove();
              popover_flag = true;
          }
          MONOloop.jq('#add_temp_before').remove();
          MONOloop.jq('#add_temp_after').remove();
          // monoloop_select_view.restoreOriginalContents();
          // if (add_addcontent_enable) {
          //   add_addcontent_enable = false;
          //   // console.log("add_addcontent_enable", currentselector);
          //   var temp_currentselector = monoloop_select_view.getXPath(this);
          //   var random_placement_id = monoloop_select_view.randomID('beforeplacement');
          //   var placement_div = "<div id="+random_placement_id+" class='before_placement' style='min-height:30px; height: auto;'></div>";
          //
          //   $(placement_div).insertBefore(this);
          //   var selector_Element = $('#'+random_placement_id);
          //
          //   var element_selector = monoloop_select_view.getXPath(selector_Element) ;
          //   monoloop_select_view.bindElement(element_selector);
          //
          //   selector_Element.click();
          //   monoloop_select_view.adjust_all_bubble_positions();
          //   // console.log("Printing THis object", currentselector);
          //   // side panel working
          //   // var xpath_content = monoloop_select_view.getXPathId(temp_currentselector);
          //   monoloop_select_view.postToParent(JSON.stringify({
          //         t: "sidebar_adjust_add_content" ,
          //         data: {
          //           xpath_content:temp_currentselector,
          //           data_type: 1,
          //           full_url : monoloop_select_view.originalUrl }
          //   }), "*");
          //   return false;
          // }


          if(['html', 'body'].indexOf(MONOloop.jq(this).prop('tagName').toLowerCase()) === -1){
            if( monoloop_select_view.lock == false /* || monoloop_select_view.tempUnlock == true || event.ctrlKey*/ ){
              event.preventDefault();
              // stopping concurrent highlighting
              hover_flag = false;
              var selector = monoloop_select_view.getXPath(this);
              monoloop_select_view.hiddenElementsOnPage.push(selector);
              if( event.ctrlKey ){
                monoloop_select_view.postToParent(JSON.stringify({
                      t: "sidebar_editpanel" ,
                      data: {type: "multi_select" }
                }), "*");
                if( MONOloop.jq(this).hasClass('monoloopBorderFix')){
                  MONOloop.jq(this).removeClass('monoloopBorderFix') ; // un select
                  if(monoloop_select_view.selectedElementsOnPage.indexOf(selector) !== -1){
                    monoloop_select_view.selectedElementsOnPage.splice(monoloop_select_view.selectedElementsOnPage.indexOf(selector), 1);
                  }
                  if(monoloop_select_view.hiddenElementsOnPage.indexOf(selector) !== -1){
                    monoloop_select_view.hiddenElementsOnPage.splice(monoloop_select_view.hiddenElementsOnPage.indexOf(selector), 1);
                  }
                }else{
                  MONOloop.jq(this).addClass('monoloopBorderFix') ; // select
                  if(monoloop_select_view.selectedElementsOnPage.indexOf(selector) === -1){
                    monoloop_select_view.selectedElementsOnPage.push(selector);
                  }
                  if(monoloop_select_view.hiddenElementsOnPage.indexOf(selector) === -1){
                    monoloop_select_view.hiddenElementsOnPage.push(selector);
                  }
                }
              }else{
                monoloop_select_view.clearSelection();
                MONOloop.jq(this).addClass('monoloopBorderFix') ; // single click without ctrl
                if(monoloop_select_view.selectedElementsOnPage.indexOf(selector) === -1){
                  monoloop_select_view.selectedElementsOnPage.push(selector);
                }
                monoloop_select_view.hiddenElementsOnPage = [];
                monoloop_select_view.hiddenElementsOnPage.push(selector);
              }
              /*
              monoloop_select_view.clearSelection() ;
              MONOloop.jq(this).addClass('monoloopBorderFix') ;
              */
              monoloop_select_view.disPlayAllBorder();




              // monoloop_select_view.selectFocusParent();
              if(! event.ctrlKey ){
                var closestCKE = MONOloop.jq(this).closest(".cke");
                // alert(closestCKE.length);
                if(closestCKE.length === 0 ){

                  var selector = monoloop_select_view.getXPath(this);
                  //top.pwMain.showMenu1(event.pageX , event.pageY , page[0] , page[1] , selector  ) ;
                  if(this.tagName !== 'BODY' && this.tagName !== 'HTML'){
                    var tFABOp = {},
                        content = '',
                        curAction = monoloop_select_view.currentTransaction.action,
                        curActionData = monoloop_select_view.currentTransaction.data,
                        defaultToolbarWidth = 235;

                    if(curAction === 'edit'){
                      tFABOp.type = 'updateVariation';
                    }else {
                      tFABOp.type = 'addVariation';
                    }
                    monoloop_select_view.initElement(selector, tFABOp); // here

                    currentselector = selector;
                    monoloop_select_view.displayWinOptions();
                    curAction = monoloop_select_view.currentTransaction.action;
                    curActionData = monoloop_select_view.currentTransaction.data;

                    var editor = CKEDITOR.instances[monoloop_select_view.editorID];
                    if(curAction === 'edit'){
                      content += '<button class="ml_custom_toolbar_btn updateVariation">Update variation</button> ';
                      defaultToolbarWidth += 125;
                    }
                    content += '<button class="ml_custom_toolbar_btn addVariation">Add variation</button> ';
                    var havVar = 0;
                    if(monoloop_select_view.selectorsHaveVariations.indexOf(selector) !== -1){
                      content += '<button class="ml_custom_toolbar_btn experimentBuilder">Add to experience</button> ';
                      defaultToolbarWidth += 135;
                      havVar = 1;
                    }

                    content += '<button class="ml_custom_toolbar_btn remove">Hide</button> ';
                    if(curAction === 'edit'){
                      content += '<button class="ml_custom_toolbar_btn conditionBuilder">Update condition</button> ';
                      defaultToolbarWidth += 130;
                    }

                    var _this = this;

                    //  content menu ifram start
                    var contentSelectorId = monoloop_select_view.getXPathId(selector), iframeUrl = '';
                    iframeUrl = MONOloop.domain + '/component/content-menu?fullURL=' + monoloop_select_view.originalUrl;
                    iframeUrl += ('&xpath=' + contentSelectorId);
                    iframeUrl += ('&hash_fn=' + monoloop_select_view.hashFn);

                    var selectorId = selector.replace(/#/g, monoloop_select_view.hashFn);
                    var element = monoloop_select_view.getSelectorByXPath(selector);
                    iframeUrl += ('&xpath1=' + selectorId);

                    if(monoloop_select_view.currentPageElement){
                      iframeUrl += ('&page_element_id=' + monoloop_select_view.currentPageElement._id);
                    }

                    if(monoloop_select_view.source !== undefined){
                      iframeUrl += ('&source=' + monoloop_select_view.source);
                    }
                    if(monoloop_select_view.currentExperiment !== undefined){
                      iframeUrl += ('&exp=' + monoloop_select_view.currentExperiment._id);
                    }
                    if(monoloop_select_view.currentFolder){
                      iframeUrl += ('&current_folder=' + monoloop_select_view.currentFolder);
                    }
                    if(monoloop_select_view.page){
                      iframeUrl += ('&page=' + monoloop_select_view.page);
                    }

                    iframeUrl += ('&tVar=' + monoloop_select_view.totalVariations);

                    if(monoloop_select_view.newExperimentBackUp){
                      iframeUrl += ('&bk_seg=' + monoloop_select_view.newExperimentBackUp.segmentExperiment);
                      iframeUrl += ('&bk_goal=' + monoloop_select_view.newExperimentBackUp.goalExperiment);
                    }
                    if(curAction === 'edit'){
                      if(curActionData.content_id){
                        iframeUrl += ('&content_id=' + curActionData.content_id);
                      }
                    }

                    iframeUrl += ('&havVar=' + havVar);
                    iframeUrl += ('&tagName=' + this.tagName);
                    // Mantis #4376
                    contentmenu_iframe = iframeUrl;
                    // calling function to send values to variation options in side bar
                    monoloop_select_view.postToParent(JSON.stringify({
                          t: "sidebar_editpanel" ,
                          data: {contentmenu_iframe,
                            full_url : monoloop_select_view.originalUrl }
                    }), "*");

                    // MONOloop.jq(this).webuiPopover('destroy');
                    // monoloop_select_view.openVariationPopover(iframeUrl, this, 'content', {title: "Editor", t: 'menu'});
                    // MONOloop.jq(this).webuiPopover('show');
                    //content menu iframe end



                    // MONOloop.jq(this).webuiPopover({
                    //  content:content,
                    //  width:defaultToolbarWidth,
                    //  height:24,
                    //  multi: false,
                    //  cache:false,
                    //  onHide: function(a){
                    //    var exID = MONOloop.jq(_this).attr('id');
                    //    if(!exID){
                    //      MONOloop.jq(_this).prop('id', monoloop_select_view.randomID());
                    //    }
                    //    var id = MONOloop.jq(_this).attr('id');
                    //
                    //    MONOloop.jq("#"+id).webuiPopover('destroy');
                    //
                    //    monoloop_select_view.bindElement('#' + MONOloop.jq(_this).attr('id'));
                    //  },
                    //  placement: 'bottom-left'
                    // });
                    // MONOloop.jq(this).webuiPopover('show');
                    // var url = MONOloop.domain + '/component/content-create?fullURL=' + monoloop_select_view.originalUrl + '&tVar=' + monoloop_select_view.totalVariations;
                    // monoloop_select_view.FAbEventFns.variation('add', {
                    //  url: url,
                    //  originalUrl: monoloop_select_view.originalUrl,
                    //  hashFn: monoloop_select_view.hashFn,
                    //  selector: selector,
                    //  currentPageElement: monoloop_select_view.currentPageElement,
                    //  currentFolder: monoloop_select_view.currentFolder,
                    //  source: monoloop_select_view.source,
                    //  page: monoloop_select_view.page,
                    //  experiment: monoloop_select_view.currentExperiment,
                    //  domAddr: '.ml_custom_toolbar_btn.addVariation',
                    //  title: 'Add variation',
                    //  placement: 'toolbar',
                    // });
                    // if(curAction === 'edit'){
                    //  var url = MONOloop.domain + '/component/content-update?fullURL=' + monoloop_select_view.originalUrl;
                    //  monoloop_select_view.FAbEventFns.variation('edit', {
                    //    url: url,
                    //    originalUrl: monoloop_select_view.originalUrl,
                    //    hashFn: monoloop_select_view.hashFn,
                    //    selector: selector,
                    //    currentPageElement: monoloop_select_view.currentPageElement,
                    //    source: monoloop_select_view.source,
                    //    currentFolder: monoloop_select_view.currentFolder,
                    //    page: monoloop_select_view.page,
                    //    content_id: curActionData.content_id,
                    //    domAddr: '.ml_custom_toolbar_btn.updateVariation',
                    //    title: 'Update variation',
                    //    placement: 'toolbar',
                    //  });
                    // }
                    // if(monoloop_select_view.selectorsHaveVariations.indexOf(selector) !== -1){
                    //  // for experiments
                    //  monoloop_select_view.FAbEventFns.experiment('add', {
                    //    url: MONOloop.domain + '/component/content-add-to-experiment?fullURL=' + monoloop_select_view.originalUrl,
                    //    originalUrl: monoloop_select_view.originalUrl,
                    //    hashFn: monoloop_select_view.hashFn,
                    //    selector: selector,
                    //    currentPageElement: monoloop_select_view.currentPageElement,
                    //    source: monoloop_select_view.source,
                    //    page: monoloop_select_view.page,
                    //    currentFolder: monoloop_select_view.currentFolder,
                    //    domAddr: '.ml_custom_toolbar_btn.experimentBuilder',
                    //    title: 'Experiments',
                    //    placement: 'toolbar',
                    //  });
                    // }
                    // if(curAction === 'edit'){
                    //  editor.title = (monoloop_select_view.currentTransaction.data ? monoloop_select_view.currentTransaction.data.condition : '');
                    //  // MONOloop.jq(editor.element.$).attr('title',(monoloop_select_view.currentTransaction.data ? monoloop_select_view.currentTransaction.data.condition : ''));
                    //  MONOloop.jq('.ml_custom_toolbar_btn.conditionBuilder').on('click', function(e){
                    //    editor.execCommand('insertCb', {
                    //      type: 'variation',
                    //      condition: (monoloop_select_view.currentTransaction.data ? monoloop_select_view.currentTransaction.data.condition : '')
                    //    });
                    //  });
                    // }
                    //
                    //
                    // MONOloop.jq('.ml_custom_toolbar_btn.remove').on('click', function(e){
                    //  editor.execCommand('insertRemove');
                    // });

                    // monoloop_select_view.bindElement() ;


                    // monoloop_select_view.createImageCounterBubble({
                    //  mappedTo: selector,
                    //  selectorId: monoloop_select_view.getXPathId(selector)
                    // });


                  }
                }
              }
              //monoloop_select_view.lock = true ;
            }
            return false ;
        }

        }

      }) ;

  },
  initElement: function(selector, fabOptions, placement){
    CKEDITOR.config.lastMappedTo = CKEDITOR.config.mappedTo || "";
    fabOptions = fabOptions || {};
    if(fabOptions.type !== 'updateVariation'){
      monoloop_select_view.currentTransaction.action = 'add';
      monoloop_select_view.currentTransaction.data = {};
    } else {

    }

    CKEDITOR.config.mappedTo = selector;
    var element = monoloop_select_view.getSelectorByXPath(selector);
    monoloop_select_view.storeOriginalContent({
      xpath: selector,
      content: element.html()
    });

    CKEDITOR.config.initData = {};
    CKEDITOR.config.initData.xpath = selector;

    monoloop_select_view.postToParent(JSON.stringify({
      t: "set-current-select-element-on-page",
      data: {
        xpath: selector,
        content: element.html(),
      }
    }));

    // this condition need to be revised as it overlaps with edit-variation-content flow
    // mantis # 3307
    if(show_CKE){
      monoloop_select_view.bindCkeditoraTo(selector, fabOptions);
      show_CKE = false;
    }else {
      monoloop_select_view.toggleCkeditor(false);
    }

  },
  randomID: function(prefix){
    prefix = prefix || "ml_custom_ckedit_id";
    var d = new Date().getTime();
    var uuid = 'xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return prefix + '_'+uuid;
  },
  toggleCkeditor: function(CK_state){
    for(var name in CKEDITOR.instances){
      var editor = CKEDITOR.instances[name];
           if (editor) {
             if (CK_state) {
               MONOloop.jq('#cke_'+CKEDITOR.instances[name].name).show();
             }else{
               MONOloop.jq('#cke_'+CKEDITOR.instances[name].name).hide();
             }
           }
    }
  },
  // revert_all_changes : function(){
  //     console.log("Reverting");
  //     monoloop_select_view.toggleCkeditor(false); // hide CKEDITOR
  //     monoloop_select_view.restoreOriginalContents(); // restoring original contents
  // },
  bindCkeditoraTo: function(selector, fabOptions){
    if(!selector) return false;
    fabOptions = fabOptions || {};
    var view = monoloop_select_view.getSelectorByXPath(selector),
        id,
        tagName = view.prop('tagName').toLowerCase(),
        toolbar = [];
    switch (tagName) {
      case 'img':
        for(var name in CKEDITOR.instances){  // marc
          monoloop_select_view.destroyCKE(CKEDITOR.instances[name].name);
        }
        return;
        //monoloop_select_view.toolbarType = "image";
        break;
      default:
        monoloop_select_view.toolbarType = "default";
        break;
    }
    if(!view.attr('contenteditable')){
      view.attr('contenteditable', true);
    }
    if(!view.attr('id')){
      id = monoloop_select_view.randomID();
      view.attr('id', id);
    } else {
      id = view.attr('id');
    }
    CKEDITOR.dtd.$editable.span = 1;
    CKEDITOR.dtd.p.div = 1;
    CKEDITOR.dtd.$editable.p = 1;
    CKEDITOR.dtd.$editable.strong = 1;
    CKEDITOR.dtd.$editable.ul = 1;
    CKEDITOR.dtd.$editable.li = 1;
    CKEDITOR.dtd.$editable.a = 1;
    CKEDITOR.dtd.$editable.b = 1;
    CKEDITOR.dtd.$editable.img = 1;
    CKEDITOR.dtd.$editable.input = 1;
    CKEDITOR.on( 'dialogDefinition', function( ev ){
      // Take the dialog name and its definition from the event data.
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;

      if (dialogName == 'image') {
        dialogDefinition.onShow = function () {
          setTimeout(function(){
            var dialog = CKEDITOR.dialog.getCurrent();
            dialog.parts.close.$.firstChild.click();
          },0);
        monoloop_select_view.postToParent(JSON.stringify({t: "openimage_uploader"}));
        };
      }
      if ( dialogName == 'link'  || dialogName == 'image') {
        dialogDefinition.removeContents( 'upload' );
        dialogDefinition.removeContents('Upload');
        // dialogDefinition.removeContents( 'target' );
      }
    });
    var editor_ = CKEDITOR.instances[id];

    // Funciton depending on editor selection,  will set the state of our command.
    function RefreshState() {
        var editable = editor_.editable(),
            // Command that we want to control.
            command = editor_.getCommand( 'insertCb' ),
            range,
            commandState;

        if ( !editable ) {
            // It might be a case that editable is not yet ready.
            return;
        }

        // We assume only one range.
        range = editable.getDocument().getSelection().getRanges()[ 0 ];

        // The state we're about to set for the command.
        commandState = ( range && !range.collapsed ) ? CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED;

        command.setState( commandState );
    }

    if (editor_ === undefined) {
      editor_ = CKEDITOR.inline(view.get(0), {
            mappedTo: selector,
            on: {
                instanceReady: function( event ) {
                    this.focus();
                    for(var name in CKEDITOR.instances){
                      if(CKEDITOR.instances[name].name != id){
                        monoloop_select_view.destroyCKE(name);
                        if(name.indexOf("ml_custom_ckedit_id_") != -1){
                          MONOloop.jq('#'+name).removeAttr('id'); // Changed By Imran Ali
                        }
                      }
                    }
                    RefreshState();

                },
                change: function( event ) {
                  // monoloop_select_view.pushChangedElement(event.editor, selector, view, tagName);
                  monoloop_select_view.pushActionsOnXapth(selector);

                  monoloop_select_view.makeSureIfAnyChangeOccoured();

                },
                blur: function( event ) {
                  var ae = MONOloop.jq(document.activeElement).get(0).tagName.toLowerCase(),
                      id;
                  if (ae != "iframe" && ae != "body" && ae != "button") {
                      // Commented By Imran Ali - Mantis #4442
                      // monoloop_select_view.saveChangedContent();
                      monoloop_select_view.restoreOriginalContents();

                      id = this.element.$.id;
                      // monoloop_select_view.destroyCKE(id);
                      return false;
                  } else {
                    return false;
                  }
                },
                doubleclick: function(event){
                  return false;
                },
                // this doesn't work everytime you select content in editor
                // selectionChange: function(event){
                //  console.log('selection change event fired at ', event.editor.getSelection().getSelectedText());
                // }
            }
        });
    }
    // We'll use throttled function calls, because this event can be fired very, very frequently.
    var throttledFunction = CKEDITOR.tools.eventsBuffer( 250, RefreshState );

    // Now this is the event that detects all the selection changes.
    editor_.on( 'selectionCheck', throttledFunction.input );

    fabOptions.selector = selector;
    fabOptions.tagName = tagName;
    fabOptions.editorID = id;
    monoloop_select_view.editorID = id;
    // monoloop_select_view.actionButtonTypes.type = "addVariation";
    if(CKEDITOR.config.mappedTo !== CKEDITOR.config.lastMappedTo){
      monoloop_select_view.currentTransaction.action = 'add';
      monoloop_select_view.currentTransaction.data = {};
      if(fabOptions.type !== undefined){
        monoloop_select_view.actionButtonTypes.type = fabOptions.type;
      } else {
        monoloop_select_view.actionButtonTypes.type = 'addVariation';
      }
      // monoloop_select_view.initActionButtons(fabOptions);
    } else {
      if(fabOptions.refresh){
        // monoloop_select_view.initActionButtons(fabOptions);
      }
    }

    //
  },
  destroyCKE: function(id, createNew){
    createNew = createNew || false;
    setTimeout(function() {
      var editor_ = CKEDITOR.instances[id];
      if(editor_) CKEDITOR.instances[id].destroy(true);
      MONOloop.jq('#'+id).removeAttr('contenteditable');
      monoloop_select_view.bindElement('#' + id);
      if(createNew){
        monoloop_select_view.initElement(CKEDITOR.config.mappedTo);
      }
   },0);
  },
  ejs2Html: function(ejsCode){
    var output = ejsCode || "";
    if(output.indexOf('<% if') !== -1) {
      output = output.replace(/<% if/g,'&nbsp;<div style="display:inline;color:green;" condition="if').replace(/{ %>/g, '{|}">').replace(/<%}%>/g, '</div>');
    }
    return output;
  },
  init: function(){
    monoloop_select_view.bindElement();
  },
  resizePopover: function(options){
    if(MONOloop.jq('.webui-popover').length > 0){
      var iframes = MONOloop.jq('.webui-popover'),
          iframe = iframes[iframes.length - 1];
          iframe.height(900);
          // console.log("iframeiframe", iframe.height(900));
    }
  },
  postToParent: function(json_obj, d){
    d = d || MONOloop.domain;
    parent.postMessage(json_obj, d);
  },
  makeSureIfAnyChangeOccoured: function(){
    if(!monoloop_select_view.isPWchangedPosted){
      monoloop_select_view.postToParent(JSON.stringify({t: 'customization-occoured-in--user-site', isPWchanged: true}));
      monoloop_select_view.isPWchangedPosted = true;
    }
  },
  makeSureIfAnyElementSelected: function(){
    MONOloop.jq(currentselector).removeAttr('contenteditable');
    MONOloop.jq(currentselector).removeClass ('monoloopBorderFix cke_editable cke_editable_inline cke_contents_ltr cke_show_borders cke_focus');
    // MONOloop.jq(currentselector).removeAttr('id');
  },
  isParentAlreadyInEditMode: function(elem){
    var _enabledParent = MONOloop.jq(elem).parents('.monoloopBorderFix:eq(0)');
    var _selfEditable = MONOloop.jq(elem).hasClass('monoloopBorderFix');
    var _isSpeechBubble = MONOloop.jq(elem).hasClass('speech-bubble');
    var _result = (_enabledParent.length || _selfEditable) ? true : false;
    return _result;
  }

}
// MONOloop.jq('#add_temp_before').
// $( "#ml-border-0-u" ).mouseover(function() {
//   // $( "#log" ).append( "<div>Handler for .mouseover() called.</div>" );
//   monoloop_select_view.displayBeforeContentOption();
// });
// MONOloop.jq('.monoloopBorderFix').hover(function(ev){
//   monoloop_select_view.displayBeforeContentOption();
// }, function(){
//   monoloop_select_view.displayAfterContentOption();
//
// });

var __listener = function (e) {
       var data = e.data;
       switch (data.t) {
         case "resize-popover":
          monoloop_select_view.resizePopover(data.options);
          break;
         case "hide-iframe":
                     monoloop_select_view.postToParent(JSON.stringify({t: "send-webix-message", message:data.message }));
                     MONOloop.jq('.webui-popover').has('iframe').remove();


           break;
        case "upload-image-required-message":
          monoloop_select_view.postToParent(JSON.stringify({t: data.t}));
          break;
        case "remove-or-decrease-variation-bubble-from-list":
          monoloop_select_view.postToParent(JSON.stringify({t: 'set-new-inline-variations', data: {}}));
          if(data.remove_popup === true){
            var selectorId = monoloop_select_view.getXPathId(data.xpath);
            MONOloop.jq("#"+selectorId).remove();
            // MONOloop.jq('.webui-popover').has('iframe').remove();
          }
          if(data.variationCount == 0){
            var selectorId = monoloop_select_view.getXPathId(data.xpath);
            MONOloop.jq("#"+selectorId).remove();
            // MONOloop.jq('.webui-popover').has('iframe').remove();
          }
          break;
        case "hide-an-element":
          var editor = CKEDITOR.instances[monoloop_select_view.editorID];
          editor.execCommand('insertRemove');
          break;
        case "reset_on_EditMode":
          // monoloop_select_view.reset_selected_element();
          monoloop_select_view.makeSureIfAnyElementSelected();
          monoloop_select_view.destroySelection(); // restoring original contents
          break;
        case "remove_variation_bubbles":
          MONOloop.jq('.speech-bubble, .variations-counter').remove();
          break;
        case "set-changed-content":
          monoloop_select_view.currentPageElement = data.pageElement;
          monoloop_select_view.originalUrl = data.originalUrl;
          monoloop_select_view.page = data.page;
          monoloop_select_view.source = data.source;
          if(monoloop_select_view.source === 'experiment') {
            monoloop_select_view.currentExperiment = data.experiment;
          }
          monoloop_select_view.currentFolder = data.currentFolder;
          // to restore original content after adding variation
          // Matins #4500
          monoloop_select_view.restoreOriginalContents();

          if(data.variations !== undefined && data.variations.length > 0){
            MONOloop.jq('.variations-counter').remove();
            var groupsVariations = monoloop_select_view.variationsGroupByXpath(data.variations, function(item){
              return [item.mappedTo];
            });
            monoloop_select_view.bubblePositions = [];
            monoloop_select_view.showVariationBubbles(groupsVariations, data);
          }

          monoloop_select_view.totalVariations = data.variations.length ? data.variations.length : 0;
          if(!data.userStateData){
              if(data.updateFab !== false){
                if(['experiment'].indexOf(monoloop_select_view.page) === -1){
                  // monoloop_select_view.initActionButtons({});
                }
              } else {
              }
          } else {
            // monoloop_select_view.initActionButtons({});
          }

          if(data.userStateData){
            if(data.userStateData.data.editRecordData){
              var element = monoloop_select_view.getSelectorByXPath(data.userStateData.data.editRecordData.xpath);
              element.html(data.userStateData.data.editRecordData.content);
              monoloop_select_view.actionButtonTypes.type = 'addVariation';
              monoloop_select_view.initElement(data.userStateData.data.editRecordData.xpath);
            }
          }
          // monoloop_select_view.disPlayAllBorder();
          // Add Content Flow
          monoloop_select_view.destroySelection();
          // monoloop_select_view.toggleCkeditor(false);
          // MONOloop.jq('#temp-tooltip_new').remove();
          // monoloop_select_view.clearSelection();
          MONOloop.jq('.before_placement').remove();
          MONOloop.jq('.after_placement').remove();
          monoloop_select_view.adjust_all_bubble_positions();
          monoloop_select_view.makeSureIfAnyElementSelected();
          // Mantis #4658
          if(data.updateFab === false){
                      monoloop_select_view.postToParent(JSON.stringify(
                        {
                          t: "hide_element"
                        }
                      ));
                    }
          break;
          case "content-menu-guide-Ready": // marc added this case
            if(guidelines.isStarted){
                var contentMenuIframe = MONOloop.jq("iframe[src*='" + MONOloop.domain + "//component/content-menu']")[0];
                if(contentMenuIframe){
                    contentMenuIframe.contentWindow.postMessage(JSON.stringify({t:"start-guideline"}), MONOloop.domain + '//component/content-menu');
                }
                guidelines.isStarted = false;
            }
          break;
          // case "revert_all_changes":
          //   console.log("HEre");
          //   monoloop_select_view.revert_all_changes();
          //   break;
          case "start-guideline-in-site":
          // marc added
					  var contentMenuIframe = MONOloop.jq("iframe[src*='staging.monoloop.com//component/content-menu']")[0];
            if(contentMenuIframe){
                contentMenuIframe.contentWindow.postMessage(JSON.stringify({t:"start-guideline"}), MONOloop.domain + '//component/content-menu');
            }

            var r = data.data;
            var cg = undefined;
            if(r.currentGuide){
              cg = JSON.parse(r.currentGuide);
              guidelines.startedBefore = true;
            }

            //marc added
	           if(r.guidelines){
              guidelines[data.pageId] = r.guidelines;
              guidelines.page.pageId = data.pageId;
              }
              guidelines.closed = function(){
                var contentMenuIframe = MONOloop.jq("iframe[src*='staging.monoloop.com//component/content-menu']")[0];
                if(contentMenuIframe){
                    contentMenuIframe.contentWindow.postMessage(JSON.stringify({t:"start-guideline"}), MONOloop.domain + '//component/content-menu');
                }else{
                    guidelines.wasStarted = true;
                }
            }
	           guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
            guidelines.generate(guidelines.page.currentGuide, {});
            /* marc remove
            if(cg){
              if(cg.currentGuide){
                guidelines.page.currentGuide = cg.currentGuide;
              } else {
                guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
              }
            } else {
              guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
            }

            // console.log("guidelines.page",guidelines.page.currentGuide);
            if(guidelines.page.currentGuide && !MONOloop.jq(guidelines.page.currentGuide.location_id).attr('data-target')){
              if(!cg){
                setTimeout(function(){
                  guidelines.generate(guidelines.page.currentGuide, {});
                  MONOloop.jq(document).keyup(function(e) {
                       if (e.keyCode == 27) { // escape key maps to keycode `27`
                         guidelines.saveInSession('yes','iframe');

                      }
                  });
                }, 1000);
              }

            } else {
            }
            */
            break;
          case "set-current-pageElement":
            // refresh page element and related items
            monoloop_select_view.currentPageElement = data.data.pageElement;

            monoloop_select_view.totalVariations++;

            // if(data.data.source && data.data.source === 'experiment'){
            //  monoloop_select_view.currentExperiment = data.data.experiment;
            //  monoloop_select_view.FAbEventFns.experiment('add', {
            //    url: MONOloop.domain + '/component/content-add-to-experiment?fullURL=' + monoloop_select_view.originalUrl,
            //    originalUrl: monoloop_select_view.originalUrl,
            //    hashFn: monoloop_select_view.hashFn,
            //    selector: monoloop_select_view.actionButtonTypes.fabOptions.selector,
            //    currentPageElement: monoloop_select_view.currentPageElement,
            //    source: monoloop_select_view.source,
            //    currentFolder: monoloop_select_view.currentFolder,
            //    domAddr: '.subActionButton.experimentBuilder.inline',
            //    title: 'Experiments',
            //  });
            // }
            monoloop_select_view.makeSureIfAnyChangeOccoured();
            // console.log("monoloop_select_view.actionButtonTypes.fabOptions", monoloop_select_view.actionButtonTypes.fabOptions);
            // if(data.variationCreated === true){
            //  monoloop_select_view.actionButtonTypes.type = 'addVariation';
            // }
            monoloop_select_view.postToParent(JSON.stringify({t: data.t, data: {content: data.data.content, pageElement: data.data.pageElement, source: data.data.source, experiment: monoloop_select_view.currentExperiment}}));
            monoloop_select_view.postToParent(JSON.stringify({t: 'set-new-inline-variations', data: {content: data.data.content, pageElement: data.data.pageElement, source: data.data.source, experiment: monoloop_select_view.currentExperiment}}));

            monoloop_select_view.disPlayAllBorder();
            // window.removeEventListener(messageEvent, __listener, false);
            break;
          case "get-element-content-variation":
            var data1 = data.data,
                element,
                tagName,
                t = "add-element-content-variation";
            var hidden = data1.hide ? data1.hide : false;
            if(CKEDITOR.config.initData || hidden){
              // data1.xpath = CKEDITOR.config.initData.xpath;
              // Mantis #4556
              if (data.data.form_xpath === undefined) {
                data1.xpath = CKEDITOR.config.initData.xpath;
              }else {
                data1.xpath = data.data.form_xpath;
              }
              // console.log(data1.xpath);
              element = monoloop_select_view.getSelectorByXPath(data1.xpath);
              // console.log(element);
              if(element.length === 0){
                alert("Xpath is not valid");
                monoloop_select_view.postToParent(JSON.stringify({t: "send-webix-message", message: 'Invalid X-path is selected to add variation.'}));
              }else {
                tagName = element.prop("tagName").toLowerCase();

                switch (tagName) {
                  case 'img':
                    var html = element[0].outerHTML;

                    if(data1.hide){
                      if(data1.hide === true){
                        html = "<!-- monoloop remove -->&nbsp;";

                        // var editor = CKEDITOR.instances[monoloop_select_view.editorID];
                        // console.log(monoloop_select_view.editorID);
                        // editor.execCommand('insertRemove');
                        // console.log(html);
                        // monoloop_select_view.createDeleteBubble({
                        //   mappedTo: data1.xpath,
                        //   selectorId: monoloop_select_view.getXPathId(data1.xpath)
                        // });

                        if(monoloop_select_view.hiddenElementsOnPage.length >= 1){
                          for (var i = 0; i < monoloop_select_view.hiddenElementsOnPage.length; i++) {
                            monoloop_select_view.createDeleteBubble({
                              mappedTo: monoloop_select_view.hiddenElementsOnPage[i],
                              selectorId: monoloop_select_view.getXPathId(monoloop_select_view.hiddenElementsOnPage[i])
                            });
                            data1.contentType = 'image';
                            data1.content = html;
                            // data1.contentType = 'image';
                            data1.imageLink = element.attr('src');
                            data1.source = monoloop_select_view.source;
                            data1.xpath = monoloop_select_view.hiddenElementsOnPage[i];
                            monoloop_select_view.postToParent(JSON.stringify({t: t, data: data1}));
                          }
                        }



                      }
                    } else {
                      if(element.hasClass('monoloop_remove')){
                        html = "<!-- monoloop remove -->&nbsp;";
                      }
                    }

                    data1.content = html;
                    data1.contentType = 'image';
                    data1.imageLink = element.attr('src');
                    break;
                  default:
                    if(data1.hide){
                      if(data1.hide === true){
                        data1.content = "<!-- monoloop remove -->&nbsp;";
                        // Commented because of new flow by Imran
                        // var editor = CKEDITOR.instances[monoloop_select_view.editorID];
                        // editor.execCommand('insertRemove');
                        // Mantis #4658 changed >= from >
                        if(monoloop_select_view.hiddenElementsOnPage.length >= 1){
                          for (var i = 0; i < monoloop_select_view.hiddenElementsOnPage.length; i++) {
                            monoloop_select_view.createDeleteBubble({
                              mappedTo: monoloop_select_view.hiddenElementsOnPage[i],
                              selectorId: monoloop_select_view.getXPathId(monoloop_select_view.hiddenElementsOnPage[i])
                            });
                            data1.contentType = '';
                            data1.source = monoloop_select_view.source;
                            data1.xpath = monoloop_select_view.hiddenElementsOnPage[i];
                            monoloop_select_view.postToParent(JSON.stringify({t: t, data: data1}));
                          }
                        }


                      }
                    } else {
                      if (data1.placement_type >= 0) {
                        var placement_content = '';
                        if (data1.placement_type == 0) {
                          placement_content = MONOloop.jq('.before_placement').html();
                        }else if (data1.placement_type == 1) {
                          placement_content = MONOloop.jq('.after_placement').html();
                        }
                        // else if (data1.placement_type == 2) {
                        //   placement_content = $('.prepned_placement').html();
                        // }else if (data1.placement_type == 3) {
                        //   placement_content = $('.appned_placement').html();
                        // }
                        var content = placement_content;
                      }else {
                        var content = element.html().trim();
                      }


                      if(content.includes('[<div style="display:inline;color:green;"')){
                        data1.inlineContent = content;
                        // content = content.replace(/[\[\]]+/g, '');
                        // content = content.replace(/color:green;/g, '');
                        content = content.split('[');
                        var remaining = content[1].split(']');
                        var htmlContent = MONOloop.jq(content[1]).html();
                        content = content[0]+htmlContent+remaining[1];

                      }
                      data1.content = content;
                    }
                    if (data1.hide != true) {
                      data1.contentType = '';
                    }
                    break;
                }
                if(data1.content_id){
                  t = "update-element-content-variation";
                }

                if(data1.hide != true){
                  data1.source = monoloop_select_view.source;
                  monoloop_select_view.postToParent(JSON.stringify({t: t, data: data1}));
                }
                // if(MONOloop.jq('.webui-popover').length > 0){
                //   var iframes = MONOloop.jq('.webui-popover iframe');
                //
                //   for (var i = 0; i < iframes.length; i++) {
                //       iframes[i].contentWindow.postMessage(JSON.stringify({t: t, data: data1}), iframes[i].src);
                //   }
                //
                // }
                }
              } else {
                monoloop_select_view.postToParent(JSON.stringify({t: "send-webix-message", message: 'No element is selected to add variation.'}));
              }
            break;
          case "image-is-uploaded":
          var editor = CKEDITOR.instances[monoloop_select_view.editorID];
          var xapth = CKEDITOR.config.mappedTo,
              selector = monoloop_select_view.getSelectorByXPath(xapth),
              rootSelector = selector,
              id = selector.attr('id'),
              rootId = id,
              tagName = selector.prop("tagName").toLowerCase(),
              rootTagName = tagName,
              src = "";
              switch (tagName) {
                case 'img':
                  src = selector.attr('src');
                  break;
                default:
                  var selector = MONOloop.jq(editor.getSelection().getStartElement().$);
                  if(selector){
                    tagName = selector.prop('tagName').toLowerCase();
                    if(tagName === 'img'){
                      id = selector.attr('id');
                      if(!id){
                        selector.attr('id', monoloop_select_view.randomID('inline_img'));
                        id = selector.attr('id');
                        src = selector.attr('src');
                      }
                    }
                  }
                  break;

              }
             // window.removeEventListener(messageEvent, listener, false);
             if(data.data.status === 'server'){
               var srcset = '' ;
               var url = data.data.url ;
               var file_name = url.substr(url.lastIndexOf('/')+1);
               var file_working_dir = url.substring(0,url.lastIndexOf('/'));
               srcset = encodeURI(file_working_dir + '/600/' + file_name) + ' 600w,' + encodeURI(file_working_dir + '/300/' + file_name) + ' 300w,' + encodeURI(file_working_dir + '/130/' + file_name) + ' 130w'
               if(tagName === 'img'){
                 selector.attr('src', encodeURI(data.data.url) + '?' + Math.random());
                 selector.attr('srcset',encodeURI(data.data.url));
                 selector.load(function(){
                   // monoloop_select_view.pushChangedElement(editor, xapth, rootSelector, rootTagName);
                   monoloop_select_view.pushActionsOnXapth(xapth);
                 });

               } else {
                //  editor.focus();
                //  editor.fire( 'saveSnapshot' );
                 editor.insertHtml('<img  src="' + encodeURI(data.data.url) + '" srcset="'+srcset+'" />');
                //  editor.fire( 'saveSnapshot' );
               }
             }

             break;
          case "set-element-content-by-xpath":
              var selector = data.xpath,
              element = monoloop_select_view.getSelectorByXPath(selector);
              data.content = monoloop_select_view.ejs2Html(data.content);
              element.html(data.content);
            break;
          case "get-element-content-by-xpath":
              var selector = data.xpath,
              element = monoloop_select_view.getSelectorByXPath(selector);
              monoloop_select_view.postToParent(
               JSON.stringify({
                   t: 'load-variation-list'
                   , data: {
                     content: element.html()
                     , xpath: selector
                   }
                 }
               )
             );
              // if(MONOloop.jq('.webui-popover').length > 0){
              //   var iframes = MONOloop.jq('.webui-popover iframe');
              //
              //   for (var i = iframes.length - 1; i < iframes.length; i++) {
              //       iframes[i].contentWindow.postMessage(
              //         JSON.stringify({
              //             t: 'load-variation-list'
              //             , data: {
              //               content: element.html()
              //               , xpath: selector
              //             }
              //           }
              //         )
              //         , iframes[i].src
              //       );
              //   }
              //
              // }
            break;
          case "reset_all_options_reverting":
            if(data.type == "content"){
              MONOloop.jq('.before_placement').remove();
              MONOloop.jq('.after_placement').remove();
              // adjustBubblePosition
              monoloop_select_view.adjust_all_bubble_positions();
              monoloop_select_view.destroySelection();
            }
            if (data.type == "update") {
              // displaying again the hidden bubble
              monoloop_select_view.toggle_bubble(true);
              // till here
            }
            // display again temp-tooltip
            monoloop_select_view.displayWinOptions();
            // hide ckeditor toolbar
            monoloop_select_view.toggleCkeditor(false);
            // restoreOriginalContents
            monoloop_select_view.restoreOriginalContents();
            // content back to non-editable state
            monoloop_select_view.makeSureIfAnyElementSelected();


            break;
          case "saveChangedContent-Variation":
            if(data.data.xpath.includes('_mp_hash_function_')){
              var selector = data.data.xpath.replace('_mp_hash_function_','#');
            }else {
              var selector = data.data.xpath;
            }
            element = monoloop_select_view.getSelectorByXPath(selector);
            var texttemp = element[0].id;
            if (element.prop("tagName").toLowerCase() == 'img') {
              var changedcontent = element[0].outerHTML;
            }else {
              var changedcontent = MONOloop.jq('#'+texttemp).text();
            }
            monoloop_select_view.storeChangedContent({
              content_id: data.data.content_id,
              condition: data.data.condition,
              content: changedcontent,
              placement_type: data.data.placement_type, // Mantis #4556
              xpath: data.data.xpath,
            });
            monoloop_select_view.saveChangedContent(data.data.variation_name);
            monoloop_select_view.restoreOriginalContents();
            monoloop_select_view.makeSureIfAnyElementSelected();
            // displaying again the hidden bubble
            monoloop_select_view.toggle_bubble(true);
            // till here
            break;
          case "adjust-bubble-borders-position":
             if (bubble_iframecontent.length > 0) {
               var xpath = "",
                   element = "";
               for (var i = 0; i < bubble_iframecontent.length; i++) {
                 xpath = bubble_iframecontent[i].split("&");
                 xpath = xpath[1].split("=").pop(-1);

                 element = monoloop_select_view.getSelectorByXPath(BubbleSelector[i]);
                 monoloop_select_view.adjustBubblePosition(element);
                 // setting top and left of bubble
                 MONOloop.jq('#'+xpath).css("top",bubble_top);
                 MONOloop.jq('#'+xpath).css("left",bubble_right - 25);

               }
             }
             break;
          case "init-FAB-from-edit-variation":
            // MONOloop.jq(".webui-popover").has('iframe').remove();
            if(data.xpath.includes('_mp_hash_function_')){
              var selector = data.xpath.replace('_mp_hash_function_','#');
            }else {
              var selector = data.xpath;
            }
            var element = monoloop_select_view.getSelectorByXPath(selector);

            data.content = monoloop_select_view.ejs2Html(data.content);

            // console.log("data.content",data.content);

            if(data.content === '<!-- monoloop remove -->&nbsp;'){
              data.content = element.html() + '<!-- monoloop remove -->&nbsp;';
              monoloop_select_view.createDeleteBubble({
                mappedTo: selector,
                selectorId: monoloop_select_view.getXPathId(selector)
              });
            }

            element.html(data.content);
            monoloop_select_view.actionButtonTypes.setType('updateVariation');

            monoloop_select_view.clearSelection() ;
            element.addClass('monoloopBorderFix');
            monoloop_select_view.disPlayAllBorder() ;

            monoloop_select_view.storeChangedContent({
              content_id: data.content_id,
              condition: data.condition,
              content: data.content,
              xpath: data.xpath,
            });

            monoloop_select_view.currentTransaction.action = 'edit';
            monoloop_select_view.currentTransaction.data = data;
            monoloop_select_view.initElement(selector, {
              content_id: data.content_id,
              content_name: data.name,
              content_condition: data.condition,
              content_content: data.content,
              xpath: data.xpath,
              refresh: true,
              type: "updateVariation",
            }, 'toolbar');

            // make element focus
            // console.log(currentselector);
            show_CKE = true;
            monoloop_select_view.initElement(selector);

            element.click();
            element.focus();
            monoloop_select_view.makeSureIfAnyChangeOccoured();
            MONOloop.jq('#temp-tooltip_new').remove();
            // hiding the current bubble
            // console.log("checking the id of bubble and currentselector", monoloop_select_view.getXPathId(currentselector));
          monoloop_select_view.toggle_bubble(false);


            break;
          case "condition-selected":
            var condition = data.returnCondition.logical;
            if(data.type !== undefined){
              if(data.type === "content-preview"){
                monoloop_select_view.postToParent(JSON.stringify({
                    t: 'set-updated-condition',
                    data: {
                      condition: condition,
                      type: data.type
                      }
                    }));
                  // if(MONOloop.jq('.webui-popover').length > 0){
                  //   var iframes = MONOloop.jq('.webui-popover iframe');
                  //
                  //   for (var i = 0; i < iframes.length; i++) {
                  //     iframes[i].contentWindow.postMessage(JSON.stringify({t: 'set-updated-condition', data: {condition: condition, type: data.type}}), iframes[i].src);
                  //   }
                  // }
                }else if(data.type === 'variation'){
                  monoloop_select_view.postToParent(JSON.stringify({
                      t: 'set-condition',
                      data: {
                        condition: condition,
                        type: data.type
                        }
                      }));
                  // if(MONOloop.jq('.webui-popover').length > 0){
                  //   var iframes = MONOloop.jq('.webui-popover iframe');
                  //
                  //   for (var i = 0; i < iframes.length; i++) {
                  //     iframes[i].contentWindow.postMessage(JSON.stringify({t: 'set-condition', data: {condition: condition, type: data.type}}), iframes[i].src);
                  //   }
                  // }
                }
              // if(data.type === 'variation'){
              //   if(monoloop_select_view.currentTransaction.action === 'edit'){
              //     monoloop_select_view.postToParent(JSON.stringify({
              //           t: "save-changed-content-while-editing" ,
              //             data : {
              //               content_id: monoloop_select_view.currentTransaction.data.content_id,
              //               content: monoloop_select_view.currentTransaction.data.content,
              //               condition: condition,
              //             }
              //     }), "*");
              //     monoloop_select_view.currentTransaction.data.condition = condition;
              //   }
              // }

              monoloop_select_view.makeSureIfAnyChangeOccoured();
            }
            break;
          case "page-level-segment-created":
            if(data.created){
              MONOloop.jq('.webui-popover').has('iframe').remove();
              if(monoloop_select_view.newExperimentBackUp){
                monoloop_select_view.newExperimentBackUp.segmentExperiment = data.segment_id;
                MONOloop.jq('.subActionButton.experimentBuilder.inline').click();
              }
              monoloop_select_view.makeSureIfAnyChangeOccoured();
            }
            break;
          case "page-level-goal-created":
            if(data.created){
              MONOloop.jq('.webui-popover').has('iframe').remove();
              if(monoloop_select_view.newExperimentBackUp){
                monoloop_select_view.newExperimentBackUp.goalExperiment = data.goal_id;
                MONOloop.jq('.subActionButton.experimentBuilder.inline').click();

              }
              monoloop_select_view.makeSureIfAnyChangeOccoured();
            }
            break;
          case "variation-added-to-experiments":
            if(data.created){
              monoloop_select_view.makeSureIfAnyChangeOccoured();
            }
            break;
          case "open-new-segment-from-inlin-exp":
            monoloop_select_view.newExperimentBackUp = data.data;
            // MONOloop.jq('.subActionButton.segmentBuilder.inline.var').show();
            // MONOloop.jq('.subActionButton.segmentBuilder.inline').click();
            MONOloop.jq('.ml_custom_toolbar_btn.segmentBuilder').click();
            break;
          case "open-new-goal-from-inlin-exp":
            monoloop_select_view.newExperimentBackUp = data.data;
            // MONOloop.jq('.subActionButton.goalBuilder.inline.var').show();
            // MONOloop.jq('.subActionButton.goalBuilder.inline').click();
            MONOloop.jq('.ml_custom_toolbar_btn.goalBuilder').click();
            break;
          case "add_addcontent-destroyCKE":
            monoloop_select_view.toggleCkeditor(false);
            MONOloop.jq('#temp-tooltip_new').remove();
            // monoloop_select_view.clearSelection();
            // disable edit actions
            // MONOloop.jq('#'+currentselector).css('contenteditable',false);
            monoloop_select_view.displayAddContentOptions();
            // Enabling Add content Flow
            add_addcontent_enable = true;
            break;
          // case "add_placement_type":
          //
          //   monoloop_select_view.restoreOriginalContents();
          //   var temp_currentselector = currentselector; // saving for further usage
          //   var random_placement_id = "";
          //   var placement_div = "";
          //   var selector_Element = "";
          //   var element_selector = "";
          //     if (data.type === 1) { // before
          //
          //       random_placement_id = monoloop_select_view.randomID('beforeplacement');
          //       placement_div = "<div id="+random_placement_id+" class='before_placement'>Before</div>";
          //
          //       $(placement_div).insertBefore(currentselector);
          //       selector_Element = $('#'+random_placement_id);
          //
          //       element_selector = monoloop_select_view.getXPath(selector_Element) ;
          //       monoloop_select_view.bindElement(element_selector);
          //
          //       selector_Element.click();
          //
          //     }else if (data.type === 2) {// after
          //       random_placement_id = monoloop_select_view.randomID('afterplacement');
          //       placement_div = "<div id="+random_placement_id+" class='after_placement'>After</div>";
          //
          //       $(placement_div).insertAfter(currentselector);
          //       selector_Element = $('#'+random_placement_id);
          //
          //       element_selector = monoloop_select_view.getXPath(selector_Element) ;
          //       monoloop_select_view.bindElement(element_selector);
          //
          //       selector_Element.click();
          //       // $(currentselector).append("<div class='after_placement'>After</div>");
          //       // monoloop_select_view.disPlayAllBorder();
          //     }
          //     // else if (data.type === 3) { // Prepend
          //     //   // $(currentselector).prepend("<hr></hr>");
          //     //   random_placement_id = monoloop_select_view.randomID('prependplacement');
          //     //   placement_div = "<div id="+random_placement_id+" class='prepned_placement'>Prepended</div>";
          //     //
          //     //   $(currentselector).prepend(placement_div);
          //     //   selector_Element = $('#'+random_placement_id);
          //     //
          //     //   element_selector = monoloop_select_view.getXPath(selector_Element) ;
          //     //   monoloop_select_view.bindElement(element_selector);
          //     //
          //     //   selector_Element.click();
          //     //   // $(currentselector).prepend("<div class='prepned_placement'>Prepended</div>");
          //     //   // monoloop_select_view.disPlayAllBorder();
          //     // }else if (data.type === 4) {// append
          //     //   // $(currentselector).append("<hr></hr>");
          //     //   random_placement_id = monoloop_select_view.randomID('appendplacement');
          //     //   placement_div = "<div id="+random_placement_id+" class='appned_placement'>Appended</div>";
          //     //
          //     //   $(currentselector).append(placement_div);
          //     //   selector_Element = $('#'+random_placement_id);
          //     //
          //     //   element_selector = monoloop_select_view.getXPath(selector_Element) ;
          //     //   // monoloop_select_view.destroySelection();
          //     //   console.log(element_selector);
          //     //   console.log(selector_Element);
          //     //   monoloop_select_view.bindElement(element_selector);
          //     //
          //     //   selector_Element.click();
          //     //   // $(currents1elector).append("<div class='appned_placement' id='appendplacement'>Appended</div>");
          //     //   // monoloop_select_view.disPlayAllBorder();
          //     //   // console.log($('.appned_placement').html());
          //     // }
          //
          //
          //
          //     monoloop_select_view.adjust_all_bubble_positions();
          //     // side panel working
          //     // var xpath_content = monoloop_select_view.getXPathId(temp_currentselector);
          //     monoloop_select_view.postToParent(JSON.stringify({
          //           t: "sidebar_adjust_add_content" ,
          //           data: {
          //             xpath_content:temp_currentselector,
          //             data_type: data.type,
          //             full_url : monoloop_select_view.originalUrl }
          //     }), "*");
          //   break;
          case "get-new-experiment-backup":
            var newExp = false;
            if(monoloop_select_view.newExperimentBackUp){
              newExp = true;
            }
            //Mantis #4376
            monoloop_select_view.postToParent(JSON.stringify({
              t: 'set-experiment-values-from-backup',
              newExp: newExp,
              data: monoloop_select_view.newExperimentBackUp
            }), "*");
            // if(MONOloop.jq('.webui-popover').length > 0){
            //   var iframes = MONOloop.jq('.webui-popover iframe');
            //
            //   for (var i = 0; i < iframes.length; i++) {
            //       iframes[i].contentWindow.postMessage(JSON.stringify({t: 'set-experiment-values-from-backup', newExp: newExp, data: monoloop_select_view.newExperimentBackUp}), "*");
            //   }
            //
            // }
            monoloop_select_view.newExperimentBackUp = undefined;
            break;
            //Mantis #4376
          // case "open-condition-builder":
          //   monoloop_select_view.postToParent(JSON.stringify({
          //     t: 'open-condition-builder',
          //     condition: data.condition,
          //     type: data.type,
          //   }));
          //   break;
         case "remove-wordpress-admin-bar":
           if(MONOloop.jq('#wpadminbar').length)
             MONOloop.jq('#wpadminbar').remove();
           break;
         default:
         break;
       }
       // Do whatever you want to do with the data got from IFrame in Parent form.
};

// listen from the child
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];

var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
// Listen to message from child IFrame window
eventer(messageEvent, __listener, false);
MONOloop.jq(document).ready(function() {
  monoloop_select_view.init();
  // testing something
  // var id = monoloop_select_view.getSelectorByXPath(currentselector);
  // MONOloop.jq('#'+id[0].id).bind('DOMSubtreeModified',function(event) {
  // console.log("Something has changed inside .classname or #id");
  // });




  // scrolling event on iframe
    var iframecontents = MONOloop.jq('div[view_id="PWIframePlacementWindowEditModeGoal"] iframe').contents();
    iframecontents = MONOloop.jq(iframecontents.context);
    MONOloop.jq(iframecontents).scroll(function(){
    for(var name in CKEDITOR.instances){
      var editor = CKEDITOR.instances[name];
      if (editor) { // Instance Exists
          var toptooltip = parseInt(MONOloop.jq("#temp-tooltip_new").css("top"));
          var topcke = parseInt(MONOloop.jq("#cke_" + editor.name).css("top"));
          var bottom_border = parseInt(MONOloop.jq("#ml-border-0-s").css("top"));
          var top_border = parseInt(MONOloop.jq("#ml-border-0-u").css("top"));
          if (top_border >= 50 && top_border <= 70) {
              if (!MONOloop.jq("#cke_" + editor.name).hasClass("topcke_height")) {
                MONOloop.jq("#cke_" + editor.name).addClass("topcke_height");
              }
          }else {
            if (toptooltip > topcke) {
              MONOloop.jq("#cke_" + editor.name).removeClass("topcke1");
              MONOloop.jq("#cke_" + editor.name).removeClass("topcke_height");
              MONOloop.jq("#cke_" + editor.name).addClass("topcke");
            }
            else { // cke toolbar overlaps
              MONOloop.jq("#cke_" + editor.name).removeClass("topcke");
            }
          }
          if (bottom_border < toptooltip) {
            // overlaps with tooltip and bottom border
            MONOloop.jq("#cke_" + editor.name).removeClass("topcke_height");
            MONOloop.jq("#cke_" + editor.name).removeClass("topcke");
            MONOloop.jq("#cke_" + editor.name).addClass("topcke1");
          }
       }
}
});
});
