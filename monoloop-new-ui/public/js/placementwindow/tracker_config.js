var tracker_config = {

  can_select_element: false,
  can_select_element_event: false,

  process_js : false ,
  lock_rerender: false ,

  field_data : null ,

  none: '',
  init: function(){
    // add event listener ;
    var me = this ;
      var listener = function(event) {
      var data;
      try{
        data = JSON.parse(event.data);
      }catch (e) {
        return ;
      }
      switch (data.t) {
        case 'event-display':
          me.renderSingleEvent(data.data);
        break;
        case 'field-display':
          me.renderSingleField(data.data);
        break;
        case 'config-listing':
          MONOloop.jq("#track-config-events").html('');
          MONOloop.jq("#track-config-fields").html('');
          me.can_select_element = false ;
          me.can_select_element_event = false ;
        break;
        case 'field-get-meta':
          me.getMetaData();
        break;
        case 'field-get-cookie':
          me.getCookieData();
        break;
        case 'field-get-js':
          me.getJs();
        break;
        case 'field-form-generate':
          me.generate_input_config(data.data);
        break;
      }
    };

    if (window.addEventListener) {
      addEventListener("message", listener, false);
    } else {
      attachEvent("onmessage", listener);
    }

    parent.postMessage(JSON.stringify({
      t : 'load-tracker-config-success'
    }),'*');

    MONOloop.jq('body').append('<div id="track-config-events"></div><div id="track-config-fields"></div><div id="track-config-field-focus"></div><div id="track-config-event-focus"></div>') ;

  },

  renderSingleEvent: function(data){
    MONOloop.jq("#track-config-events").html('');
    this.can_select_element = false ;
    this.can_select_element_event = false ;
    if(data.type === 'click'){
      this.can_select_element_event = true ;
      this.displayEventBorder(data.selector);
    }
  },

  displayEventBorder: function(xpath){
    if( MONOloop.jq(xpath).length == 0){
      return false ;
    }
    var els =  MONOloop.jq(xpath) ;
    var length = els.length ;
    for(var i = 0 ; i < length ; i++){
      var el = els[i];
      var border_count = MONOloop.jq('#track-config-events .border .border-l').length;
      var id = 'track-config-event-' + border_count ;
      MONOloop.jq('#track-config-events').append('<div id="'+id+'" class="border"><div class="border-l"></div><div class="border-r"></div><div class="border-t"></div><div class="border-b"></div></div>');


      var top = MONOloop.jq(el).offset().top ;
      var left = MONOloop.jq(el).offset().left ;
      var hight = MONOloop.jq(el).outerHeight();
      var width =  MONOloop.jq(el).outerWidth();

      var bl = '#'+ id + ' .border-l' ;
      MONOloop.jq(bl).css('top' , top -3) ;
      MONOloop.jq(bl).css('left' ,  left - 3) ;
      MONOloop.jq(bl).css('width' , 3) ;
      MONOloop.jq(bl).css('height' , hight + 6) ;

      var bt = '#'+ id + ' .border-t' ;
      MONOloop.jq(bt).css('top' , top -3) ;
      MONOloop.jq(bt).css('left' ,  left ) ;
      MONOloop.jq(bt).css('width' , width) ;
      MONOloop.jq(bt).css('height' , 3) ;

      var br = '#'+ id + ' .border-r' ;
      MONOloop.jq(br).css('top' , top -3) ;
      MONOloop.jq(br).css('left' ,  left + width) ;
      MONOloop.jq(br).css('width' , 3) ;
      MONOloop.jq(br).css('height' , hight + 6) ;

      var bb = '#'+ id + ' .border-b' ;
      MONOloop.jq(bb).css('top' , top + hight) ;
      MONOloop.jq(bb).css('left' ,  left ) ;
      MONOloop.jq(bb).css('width' , width) ;
      MONOloop.jq(bb).css('height' , 3) ;
    }

  },

  renderSingleField:function(data){
    if(this.lock_rerender){
      return ;
    }

    MONOloop.jq("#track-config-fields").html('');
    this.can_select_element = false ;
    this.can_select_element_event = false ;
    if(data.tracker_type === 'form' && data.field_index == 0){
      this.displayFieldBorder('form','pre_select');
      this.displayFieldBorder(data.selector);
      this.can_select_element = true;
    }else if(data.tracker_type === 'form'){
      this.displayFieldBorder(data.selector);
    }else if(data.type === 'x-path'){
      this.displayFieldBorder(data.selector);
      this.can_select_element = true;
    }
    this.field_data = data ;
    this.sendExampleDataBack(data);
  },

  displayFieldBorder: function(xpath , addition_class){
    if( MONOloop.jq(xpath).length == 0){
      return false ;
    }

    if(! addition_class){
      addition_class = '' ;
    }

    var els =  MONOloop.jq(xpath) ;
    var length = els.length ;
    for(var i = 0 ; i < length ; i++){
      var el = els[i];
      var border_count = MONOloop.jq('#track-config-fields .border .border-l').length;
      var id = 'track-config-field-' + border_count + addition_class ;
      MONOloop.jq('#track-config-fields').append('<div id="'+id+'" class="border '+addition_class+'"><div class="border-l"></div><div class="border-r"></div><div class="border-t"></div><div class="border-b"></div></div>');


      var top = MONOloop.jq(el).offset().top ;
      var left = MONOloop.jq(el).offset().left ;
      var hight = MONOloop.jq(el).outerHeight();
      var width =  MONOloop.jq(el).outerWidth();

      var bl = '#'+ id + ' .border-l' ;
      MONOloop.jq(bl).css('top' , top -3) ;
      MONOloop.jq(bl).css('left' ,  left - 3) ;
      MONOloop.jq(bl).css('width' , 3) ;
      MONOloop.jq(bl).css('height' , hight + 6) ;

      var bt = '#'+ id + ' .border-t' ;
      MONOloop.jq(bt).css('top' , top -3) ;
      MONOloop.jq(bt).css('left' ,  left ) ;
      MONOloop.jq(bt).css('width' , width) ;
      MONOloop.jq(bt).css('height' , 3) ;

      var br = '#'+ id + ' .border-r' ;
      MONOloop.jq(br).css('top' , top -3) ;
      MONOloop.jq(br).css('left' ,  left + width) ;
      MONOloop.jq(br).css('width' , 3) ;
      MONOloop.jq(br).css('height' , hight + 6) ;

      var bb = '#'+ id + ' .border-b' ;
      MONOloop.jq(bb).css('top' , top + hight) ;
      MONOloop.jq(bb).css('left' ,  left ) ;
      MONOloop.jq(bb).css('width' , width) ;
      MONOloop.jq(bb).css('height' , 3) ;
    }
  },

  disPlayFocusBorder(el , key ){
    var top = MONOloop.jq(el).offset().top ;
    var left = MONOloop.jq(el).offset().left ;
    var hight = MONOloop.jq(el).outerHeight();
    var width =  MONOloop.jq(el).outerWidth();
    var id = 'track-config-'+key+'-focus';
    MONOloop.jq('#' + id).html('<div  class="border"><div class="border-l"></div><div class="border-r"></div><div class="border-t"></div><div class="border-b"></div></div>');


    var top = MONOloop.jq(el).offset().top ;
    var left = MONOloop.jq(el).offset().left ;
    var hight = MONOloop.jq(el).outerHeight();
    var width =  MONOloop.jq(el).outerWidth();

    var bl = '#'+ id + ' .border-l' ;
    MONOloop.jq(bl).css('top' , top -3) ;
    MONOloop.jq(bl).css('left' ,  left - 3) ;
    MONOloop.jq(bl).css('width' , 3) ;
    MONOloop.jq(bl).css('height' , hight + 6) ;

    var bt = '#'+ id + ' .border-t' ;
    MONOloop.jq(bt).css('top' , top -3) ;
    MONOloop.jq(bt).css('left' ,  left ) ;
    MONOloop.jq(bt).css('width' , width) ;
    MONOloop.jq(bt).css('height' , 3) ;

    var br = '#'+ id + ' .border-r' ;
    MONOloop.jq(br).css('top' , top -3) ;
    MONOloop.jq(br).css('left' ,  left + width) ;
    MONOloop.jq(br).css('width' , 3) ;
    MONOloop.jq(br).css('height' , hight + 6) ;

    var bb = '#'+ id + ' .border-b' ;
    MONOloop.jq(bb).css('top' , top + hight) ;
    MONOloop.jq(bb).css('left' ,  left ) ;
    MONOloop.jq(bb).css('width' , width) ;
    MONOloop.jq(bb).css('height' , 3) ;
  },


  sendExampleDataBack: function(data){
    var result = '' ;
    if(data.type === 'x-path'){
      result = MONOloop.jq(data.selector).first().html();
    }else if(data.type === 'meta'){
      result = MONOloop.jq('meta[name="' + data.meta + '"]').attr('content');
    }else if(data.type === 'cookie'){
      result = MONOloop.getCookie(data.cookie);
    }else if(data.type === 'js'){
      MONOloop.jq.globalEval('var x = window' + data.js + ';');
      result = x ;
    }else if(data.type === 'custom'){
      var custom = data.custom ;
      /*
      var sl = new Function(custom);
      try{
        result = sl();
      }catch(err){
        console.log(err);
      }
      */
      var s = 'function process_custom(result){ try{ '+custom+' }catch(err){ return \'\';}}' ;
      MONOloop.jq.globalEval(s);
      result = process_custom(result) ;
    }

    if(data.filter === 'price'){
      result = MONOloop.getCartPriceDefaultFilter(result);
    }else if(data.filter === 'image'){
      result = MONOloop.getImageFilter(result);
    }else if(data.filter === 'custom'){
      var s = 'function filter_custom(result){ try{ '+data.filter_custom+' }catch(err){ return \'\';}}' ;
      MONOloop.jq.globalEval(s);
      result = filter_custom(result) ;
    }

    result = MONOloop.trim(result);

    parent.postMessage(JSON.stringify({
      t: 'display-example',
      result: result
    }),'*');

  },

  getXpathByElement : function(el){
    if(this.field_data && this.field_data.tracker_type && this.field_data.tracker_type === 'form' && this.field_data.field_index === 0){
      el = MONOloop.jq(el).closest('form');
    }

    //console.log(el);

    var selector = MONOloop.jq(el).parents()
              .map(function(){
                  var id = MONOloop.jq(this).attr("id");
                  var returnPart = this.tagName ;
                  if( returnPart == 'HTML'  || returnPart == 'BODY')
                    return returnPart ;
                  if (id) {
                    id = id.replace(/\./g, '\\.');
                    id = id.replace(' ', '\\ ');
                      returnPart += "#"+ id;
                  }else{
                      var classNames = MONOloop.jq(this).attr("class");
                      if (MONOloop.jq.trim(classNames) ) {
                      returnPart += "." + MONOloop.jq.trim(classNames).replace(/\s+/gi, ".");
                      }
                  }
                  return  returnPart ;
              })
              .get().reverse().join(" ");
      var selectorTest = selector ;
      if (selector) {
        selector += " "+ MONOloop.jq(el)[0].nodeName;
      }
      var id = MONOloop.jq(el).attr("id");
      if (id) {
        if(id.indexOf("ml_custom_ckedit_id_") === -1){
          id = id.replace(/\./g, '\\.');
          id = id.replace(' ', '\\ ');
          selector += "#"+ id;
        }
      }
      var classNames = MONOloop.jq(el).attr("class");
      if (MONOloop.jq.trim(classNames)) {
        selector += "." + MONOloop.jq.trim(classNames).replace(/\s+/gi, ".");
        //.monoloopBorder.monoloopBorderFix
        selector = selector.replace(/.monoloopBorderFix/ , '') ;
        selector = selector.replace(/.monoloopBorder/ , '') ;
        var excludePattern = /.cke_editable|.cke_editable_inline|_inline|.cke_contents_ltr|.cke_show_borders|.cke_focus|.monoloop_remove/ig;
        selector = selector.replace(
          excludePattern,
          function replacer(match){
            return "";
          }
        );
      }
      var curElement = MONOloop.jq(selector);
      //console.log(curElement.length);
      if( curElement.length > 1){
         for( var i = 0 ; i < curElement.length ; i++){
            if( el[0] ==  curElement[i] ){
              //console.log('found');
              selector += ':eq('+i+')' ;
            }
         }
      }
      //console.log(selector);
      //return selector ;
      var ret = MONOloop.trim( this.cleanUpXpath(selector) ) ;

      //console.log(ret);

      return ret ;
  } ,


  cleanUpXpath:function(selector){

    var res = selector.split(" ");

    var index = 0 ;
    var temp = null ;
    var xpath1 = '' ;
    var masternode = MONOloop.jq(selector).get( 0 ) ;

    while(index < res.length){
      temp = res.slice(0) ;
      res.splice(index, 1);

      if(res.length <= 2){
        break ;
      }

      xpath1 = res.join(" ");
      var node1 = MONOloop.jq(xpath1).get( 0 ) ;

      if((node1.compareDocumentPosition(masternode) === 0) && (temp[index].indexOf('#') === -1 ) ){
        ;
      }else{
        /*
        index++ ;
        res = temp.slice(0) ;
        */
        break;
      }
    }

    var last_id_index = -1 ;

    for(var i = res.length -1 ; i >= 0 ; i-- ){
      if(res[i].indexOf('#') !== -1 ){
        if( last_id_index === -1){
          last_id_index = i ;
        }else{
          res[i] = '' ;
        }
      }
    }

    return res.join(" ") ;
  },
  /*
  getXpathByElement: function(el){
    var selector = el.parents()
      .map(function() {
          var id = MONOloop.jq(this).attr("id");
          var returnPart = this.tagName ;
          if (id) {
              returnPart += "#"+ id;
          }else{
              var classNames = el.attr("class");
              if (classNames) {
              returnPart += "." + MONOloop.jq.trim(classNames).replace(/\s/gi, ".");
              }
          }
          return  returnPart ;
      })
      .get().reverse().join(" ");
    var selectorTest = selector ;
    if (selector) {
      selector += " "+ el[0].nodeName;
    }



    var id = el.attr("id");
    if (id) {
      selector += "#"+ id;
    }



    var classNames = el.attr("class");
    if (classNames) {
      selector += "." + MONOloop.jq.trim(classNames).replace(/\s/gi, ".");

    }

    // N : + support eq element ;
    var curElement = MONOloop.jq(selector);
    if( curElement.length > 1){
      for( var i = 0 ; i < curElement.length ; i++){
        if( this == curElement[i])
          selector += ':eq('+i+')' ;
      }
    }
    return selector ;
  },
  */
  getMetaData:function(){
    var metas = MONOloop.jq('meta');
    var data = []  ;
    for(var i = 0 ; i < metas.length ; i++){
      var name = MONOloop.jq(metas[i]).attr('name');
      if(name !== undefined){
        var content = MONOloop.jq(metas[i]).attr('content');
        data.push({
          id: name ,
          value: name + ' : ' + content
        });
      }

    }
    parent.postMessage(JSON.stringify({
      t: 'set-field-meta',
      metas: data
    }),'*');
  },

  getCookieData:function(){
    var cookies = MONOloop.getCookies();
    var data = [] ;
    MONOloop.jq.each(cookies, function(index, value) {
      if(index === ''){
        return;
      }
      data.push({
        id: index,
        value: index + ' : ' + value
      });
    });

    parent.postMessage(JSON.stringify({
      t: 'set-field-cookie',
      cookies: data
    }),'*');
  },


  iterate: function(obj, stack , data) {
    for (var property in obj) {
      if (obj.hasOwnProperty(property)) {
        if(typeof obj[property] == 'function'){
          continue;
        }

        if (typeof obj[property] == "object") {
          var n = {id: stack + '[\'' + property + '\']',value: property,data: []} ;
          data.push(n) ;
          this.iterate(obj[property], stack + '[\'' + property + '\']' , n.data);
        } else {
          data.push({id: stack + '[\'' + property + '\']',value: property});
          //data[property] = property + ':' + obj[property] ;
          //console.log(stack);
          //console.log(property + ":" + obj[property]);
        }
      }
    }
  },


  getJs: function(){
    var results, currentWindow,
    // create an iframe and append to body to load a clean window object
    iframe = document.createElement('iframe');
    var data = [] ;
    iframe.style.display = 'none';
    document.body.appendChild(iframe);
    // get the current list of properties on window
    currentWindow = Object.getOwnPropertyNames(window);
    // filter the list against the properties that exist in the clean window
    results = currentWindow.filter(function(prop) {
        return !iframe.contentWindow.hasOwnProperty(prop);
    });
    // log an array of properties that are different
    document.body.removeChild(iframe);

    for(var i = 0 ; i < results.length ; i++){
      var invalid_params = ['tracker_config','Sizzle','ml_root','MONOloop','jQuery','ml_asset_listener','MONOloopReady','monoloopPlacement','monoloop_tracker','0']

      if(invalid_params.includes(results[i])){
        continue;
      }

      if(results[i].startsWith('jQuery')){
        continue;
      }

      var temp = [] ;

      this.iterate(window[results[i]],'[\'' + results[i]  + '\']',temp);

      data.push({id:'[\'' + results[i]  + '\']',value: results[i],data:temp});

    }

    //console.log(data);

    //build tree data

    parent.postMessage(JSON.stringify({
      t: 'set-field-js',
      js: data
    }),'*');
  },

  generate_input_config: function(data){
    var elems = MONOloop.$(data.selector + ' input , ' + data.selector + ' textarea , ' + data.selector + ' select');
    var gen_fields = [];

    this.lock_rerender = true;
    this.field_data.tracker_type = '';

    for(var i = 0 ; i < elems.length ; i++ ){
      var elem = elems[i];
      var xpath = this.getXpathByElement([elem]);
      console.log(xpath);
      console.log(MONOloop.jq(xpath).attr('name'));
      if(MONOloop.jq(xpath).attr('name') == undefined){
        continue;
      }
      gen_fields.push({
        name: MONOloop.jq(xpath).attr('name'),
        type: 'string',
        operator: 'override',
        method_type: 'custom',
        selector: xpath,
        meta: '',
        cookie: '',
        js: '',
        custom: "return MONOloop.val(MONOloop.$('"+xpath+"')); ",
        filter: '',
        filter_custom: ''
      });
    }

    parent.postMessage(JSON.stringify({
      t: 'generate-form-field',
      data: gen_fields
    }),'*');

    this.lock_rerender = false;
  }
};


MONOloop.jq(document).ready(function() {

  MONOloop.jq('*').mouseenter(function(event) {
    if(tracker_config.can_select_element){
      //generate hover element;
      tracker_config.disPlayFocusBorder(MONOloop.jq(this),'field');
    }

    if(tracker_config.can_select_element_event){
      //generate hover element;
      tracker_config.disPlayFocusBorder(MONOloop.jq(this),'event');
    }
  });

  MONOloop.jq('*').mouseleave(function(event) {
    MONOloop.jq('#track-config-field-focus').html('');
    MONOloop.jq('#track-config-event-focus').html('');
  });

  MONOloop.jq('*').click(function(event){
    if(tracker_config.can_select_element){
      var xpath = tracker_config.getXpathByElement(MONOloop.jq(this));
      parent.postMessage(JSON.stringify({
        t: 'set-field-selector',
        selector: xpath
      }),'*');
    }
    if(tracker_config.can_select_element_event){
      var xpath = tracker_config.getXpathByElement(MONOloop.jq(this));
      parent.postMessage(JSON.stringify({
        t: 'set-event-selector',
        selector: xpath
      }),'*');
    }
    return false ;
  });

});