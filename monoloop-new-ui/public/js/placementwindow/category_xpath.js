var monoloop_select_view = {
    lock : false  , 
    tempUnlock : false , 
    mode : 1 ,
    
    setMode : function(m){
        monoloop_select_view.mode = m ; 
    }  ,
    
    setPreXPath : function(str){
        MONOloop.jq('*').removeClass('monoloopBorderFix') ;  
        MONOloop.jq(str).addClass('monoloopBorderFix') ; 
        monoloop_select_view.disPlayBorderBlue() ;   
    } , 
    
    getXPathContent : function( selector , setback , setbackID ){
        var ret = '' ; 
        if( MONOloop.jq.trim( selector ) == '' && selector == undefined)
            ret = '' ; 
        ret = MONOloop.getCartInnerContent( MONOloop.jq(selector));
        if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" , 
			  setbackID : setbackID , 
              data : ret 
            }), '*');
        }else{
            return ret ; 
        }
    } , 
    
    getXPathPrice : function(selector  , setback , setbackID){
        var price = MONOloop.getCartInnerContent( MONOloop.jq(selector)); 
        price =  price.replace(/[^0-9\.\,]+/g,"")    ;  
        var x1 = price.indexOf(',') ; 
        var x2 = price.indexOf('.') ;
        var convert = false ;   
        var l = price.length  ;  
        if( l - x1 > 3 && x1 != -1 && x2 == -1){
            ;  
        }else if(l - x2 > 3 && x2 != -1 && x1 == -1){ 
            convert = true ;  
        }else if( x2 < x1 && x1 != -1){
            convert = true ;  
        }
        
        if( convert){
            price = price.replace('.','###') ; 
            price = price.replace(',','.') ; 
            price = price.replace('###',',') ;  
        }   
        price =  price.replace(/[^0-9\.]+/g,"")    ; 
        price = Number(price) ;   
        
        if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" , 
			  setbackID : setbackID , 
              data : price 
            }), '*');
        }else{
            return price ; 
        }
    } , 
    
    url_domain : function(data) {
	  var ret = MONOloop.parseUri(data) ; 
		return ret['protocol'] + '://' + ret['host'] ; 
	} , 
    
    getXPathImageSrc : function(selector  , setback , setbackID){ 
    	var me = this ; 
       var img_src = MONOloop.jq(selector).parent().find("img").attr("src") ;
       var base = MONOloop.jq('base').attr('href') ; 
       
       var i = (img_src + '').indexOf('http') ; 
       if( i == -1){ 
       		i = (img_src + '').indexOf('/') ; 	 
       		if( i == 0){
       			img_src = me.url_domain(base) + img_src ;  
       		}else{
       			img_src = base + img_src ;  
       		}
           
       }
 
       if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" , 
			  setbackID : setbackID , 
              data : img_src 
            }), '*');
        }else{
            return img_src ; 
        }
    } ,
    
    getXPathPostFunc : function(jsStr  , setback , setbackID){
        var ret = '' ; 
        var str = 'function testX001(){'
                + ' try { '
                + jsStr
                + ' }catch(err) {'
                + ' return \'error\' ;' 
                + ' } } ' ; 
        MONOloop.jq.globalEval(str) ;  
        ret = MONOloop.jq.trim( testX001() ) ; 
        
        if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" , 
			  setbackID : ret , 
              data : img_src 
            }), '*');
        }else{
            return ret ; 
        }
    } ,
    
    clearSelection : function(){
        MONOloop.jq('.monoloopBorderFix').removeClass('monoloopBorderFix') ; 
        monoloop_select_view.disPlayBorder() ; 
        monoloop_select_view.lock = false  ;
    } , 
    
    strip_tags : function(html){
 
		//PROCESS STRING
		if(arguments.length < 3) {
			html=html.replace(/<\/?(?!\!)[^>]*>/gi, '');
		} else {
			var allowed = arguments[1];
			var specified = eval("["+arguments[2]+"]");
			if(allowed){
				var regex='</?(?!(' + specified.join('|') + '))\b[^>]*>';
				html=html.replace(new RegExp(regex, 'gi'), '');
			} else{
				var regex='</?(' + specified.join('|') + ')\b[^>]*>';
				html=html.replace(new RegExp(regex, 'gi'), '');
			}
		}
 
		//CHANGE NAME TO CLEAN JUST BECAUSE 
		var clean_string = html;
 
		//RETURN THE CLEAN STRING
		return clean_string;
    } , 
    
    getSelectContent : function(){
 
        var data = MONOloop.jq('.monoloopBorderFix').outerHTML() ;
        data = data.replace('monoloopBorderFix' , '') ;
        data = data.replace('monoloopBorder' , '') ;
        return  data ; 
         
    } , 
    
    getScrollXY : function () {
      var scrOfX = 0, scrOfY = 0;
      if( typeof( window.pageYOffset ) == 'number' ) {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
      } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
      } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
      }
      return [ scrOfX, scrOfY ];
    } , 
    
    selectFocusParent : function(selectorString){
        if( MONOloop.jq(selectorString).parent().length ){
            MONOloop.jq(selectorString).removeClass('monoloopBorderFix') ; 
            MONOloop.jq(selectorString).removeClass('monoloopBorder') ; 
            monoloop_select_view.disPlayBorder() ; 
            var parent = MONOloop.jq(selectorString).parent() ; 
            //parent.addClass('monoloopBorderFix') ; 
            monoloop_select_view.tempUnlock = true ; 
            //selector = monoloop_select_view.genJSelector(parent) ;
            //top.selectTab.changeSelectorTxt(  selector  ) ; 
            parent.click() ; 
        }else{
            alert('Not Found'); // Not found element
        }
    } ,
    
    selectFocusFirstChild : function(selectorString){
        if( MONOloop.jq(selectorString).children().length ){
            MONOloop.jq(selectorString ).removeClass('monoloopBorderFix') ; 
            MONOloop.jq(selectorString).removeClass('monoloopBorder') ; 
            monoloop_select_view.disPlayBorder() ; 
            var firstChild = MONOloop.jq(selectorString).children(); 
     
            monoloop_select_view.tempUnlock = true ;  
            firstChild.click() ; 
        }else{
            alert('Not Found'); // Not found element
        }
    } , 
    
    showPreplace : function( records ){
        MONOloop.jq('#preplaced-panel').html('') ; 
        data = {} ; 
        for( i = 0 ; i < records.length ; i++ ){
            if( typeof data[records[i].get('element_id')] == "undefined" )
                data[records[i].get('element_id')] = 1 ; 
            else
                data[records[i].get('element_id')]++ ; 
        }
        
        i = 0 ; 
        for (var key in data) {
            //alert(i) ; 
            if( MONOloop.jq(key).length == 0)
                continue ; 
            //MONOloop.jq(key).css('background-color' , 'red') ; 
            var right = MONOloop.jq(key).offset().left + MONOloop.jq(key).outerWidth();
            var top = MONOloop.jq(key).offset().top ; 
            //alert( right + ' ' + top) ;
            MONOloop.jq('#preplaced-panel').append('<div class="speech-bubble" onclick="monoloop_select_view.bubbleClick(event , \''+key+'\')" id="preplace-'+i+'" onmouseover="monoloop_select_view.bubbleMouseOver(event , \''+key+'\')">'+data[key]+'</div>') ;
            MONOloop.jq('#preplace-'+i).addClass('speech-bubble') ; 
            MONOloop.jq('#preplace-'+i).css('top' , top) ; 
            MONOloop.jq('#preplace-'+i).css('left' ,  right - 25) ; 
            i++ ;  
        }
    } , 
    
    bubbleClick : function(e , selector){
        top.pwMain.bubbleClick(selector) ; 
        agent = MONOloop.jq.browser;
    	if(agent.msie) {
    		window.event.cancelBubble = true;
    	} else {
    		e.stopPropagation();
    	}
    } , 
    
    bubbleMouseOver : function(e , selector){
        if( monoloop_select_view.lock == false ){
            agent = MONOloop.jq.browser;
            if(agent.msie) {
        		window.event.cancelBubble = true;
        	} else {
        		e.stopPropagation();
        	} 
            MONOloop.jq('*').removeClass('monoloopBorder') ; 
            MONOloop.jq(selector).addClass('monoloopBorder') ; 
            monoloop_select_view.disPlayBorder() ; 
        }
    } ,
    
    disPlayBorder : function(){
        MONOloop.jq('.ml-border').hide() ; 
        //console.debug('yy') ; 
        if( monoloop_select_view.disPlayBorder2('monoloopBorder') == false ){
             ; 
        }
    } ,
    
    disPlayBorder2 : function(myClass){
        if( MONOloop.jq('.' +  myClass ).length == 0){
            //console.debug('xx') ; 
            return false ; 
        }
 
        //console.debug('zz') ; 
        MONOloop.jq('.ml-border').show() ; 
        var top = MONOloop.jq('.' + myClass).offset().top ; 
        var left = MONOloop.jq('.' + myClass).offset().left ; 
        var hight = MONOloop.jq('.' + myClass).outerHeight();
        var width =  MONOloop.jq('.' + myClass).outerWidth();
         
 
        MONOloop.jq('#ml-border-l').css('top' , top -3) ; 
        MONOloop.jq('#ml-border-l').css('left' ,  left - 3) ; 
        MONOloop.jq('#ml-border-l').css('width' , 3) ;
        MONOloop.jq('#ml-border-l').css('height' , hight + 6) ; 
        
        MONOloop.jq('#ml-border-u').css('top' , top -3) ; 
        MONOloop.jq('#ml-border-u').css('left' ,  left ) ; 
        MONOloop.jq('#ml-border-u').css('width' , width) ;
        MONOloop.jq('#ml-border-u').css('height' , 3) ;
        
        MONOloop.jq('#ml-border-r').css('top' , top -3) ; 
        MONOloop.jq('#ml-border-r').css('left' ,  left + width) ; 
        MONOloop.jq('#ml-border-r').css('width' , 3) ;
        MONOloop.jq('#ml-border-r').css('height' , hight + 6) ; 
        
        MONOloop.jq('#ml-border-s').css('top' , top + hight) ; 
        MONOloop.jq('#ml-border-s').css('left' ,  left ) ; 
        MONOloop.jq('#ml-border-s').css('width' , width) ;
        MONOloop.jq('#ml-border-s').css('height' , 3) ;
        
        return true ; 
    } , 
    
    disPlayBorderBlue : function(){ 
        MONOloop.jq('.ml-borderb').hide() ; 
        var myClass = 'monoloopBorderFix' ; 
        if( MONOloop.jq('.' + myClass ).length == 0){ 
            return false ; 
        }
        //console.debug('zz') ; 
        MONOloop.jq('.ml-borderb').show() ; 
        var top = MONOloop.jq('.' + myClass).offset().top ; 
        var left = MONOloop.jq('.' + myClass).offset().left ; 
        var hight = MONOloop.jq('.' + myClass).outerHeight();
        var width =  MONOloop.jq('.' + myClass).outerWidth();
 
        MONOloop.jq('#ml-borderb-l').css('top' , top -3) ; 
        MONOloop.jq('#ml-borderb-l').css('left' ,  left - 3) ; 
        MONOloop.jq('#ml-borderb-l').css('width' , 3) ;
        MONOloop.jq('#ml-borderb-l').css('height' , hight + 6) ; 
        
        MONOloop.jq('#ml-borderb-u').css('top' , top -3) ; 
        MONOloop.jq('#ml-borderb-u').css('left' ,  left ) ; 
        MONOloop.jq('#ml-borderb-u').css('width' , width) ;
        MONOloop.jq('#ml-borderb-u').css('height' , 3) ;
        
        MONOloop.jq('#ml-borderb-r').css('top' , top -3) ; 
        MONOloop.jq('#ml-borderb-r').css('left' ,  left + width) ; 
        MONOloop.jq('#ml-borderb-r').css('width' , 3) ;
        MONOloop.jq('#ml-borderb-r').css('height' , hight + 6) ; 
        
        MONOloop.jq('#ml-borderb-s').css('top' , top + hight) ; 
        MONOloop.jq('#ml-borderb-s').css('left' ,  left ) ; 
        MONOloop.jq('#ml-borderb-s').css('width' , width) ;
        MONOloop.jq('#ml-borderb-s').css('height' , 3) ;
        
          
   
    }
} ; 

MONOloop.jq(document).ready(function() {
    
    MONOloop.jq('*').mouseenter(function(event) {
        if( monoloop_select_view.mode == 1 )
            return false ; 
        if( monoloop_select_view.lock == false ){
            event.stopPropagation();
            MONOloop.jq('*').removeClass('monoloopBorder') ; 
            MONOloop.jq(this).addClass('monoloopBorder') ; 
            monoloop_select_view.disPlayBorder() ; 
        }
    });
    
    MONOloop.jq('*').mouseleave(function(event) {
       
        if( monoloop_select_view.lock == false ){
            event.stopPropagation();
            MONOloop.jq(this).removeClass('monoloopBorder') ; 
            monoloop_select_view.disPlayBorder() ; 
        }
    }); 
    
    MONOloop.jq('a').live("click", function(event){ 
         if( monoloop_select_view.mode == 1 ){  
            if (window['monoloopClientURL2'] != undefined){  
	    		return previewView.processATag(this) ;   
		    }else{  
	     		return true ; 
	   		}
         }else{
            return false ; 
         }
    }); 
    
    MONOloop.jq('a').click(function(){
        if( monoloop_select_view.mode == 1 ){  
            if (window['monoloopClientURL2'] != undefined){  
	    		return previewView.processATag(this) ;   
		    }else{  
	     		return true ; 
	   		} 
         }else{
            return false ; 
         }
    }) ;
    
    MONOloop.jq('*').click(function(event) {
        if( monoloop_select_view.mode == 2 ){  
            event.stopPropagation();
            MONOloop.jq('*').removeClass('monoloopBorderFix') ; 
            MONOloop.jq(this).addClass('monoloopBorderFix') ; 
            monoloop_select_view.disPlayBorderBlue() ;  
    
            // --------- Get Selector String ----------- //
             
            //selector = monoloop_select_view.genJSelector(this) ;
            var selector = MONOloop.jq(this).parents()
                    .map(function() {
                        var id = MONOloop.jq(this).attr("id");
                        var returnPart = this.tagName ; 
                        if (id) { 
                            returnPart += "#"+ id;
                        }else{
                            var classNames = MONOloop.jq(this).attr("class");
                            if (classNames) {
                            returnPart += "." + MONOloop.jq.trim(classNames).replace(/\s/gi, ".");
                            }
                        }
                        return  returnPart ; 
                    })
                    .get().reverse().join(" ");
            var selectorTest = selector ;
            if (selector) { 
                selector += " "+ MONOloop.jq(this)[0].nodeName;
            }
            
            
    
            var id = MONOloop.jq(this).attr("id");
            if (id) { 
              selector += "#"+ id;
            }
            
            
    
            var classNames = MONOloop.jq(this).attr("class");
            if (classNames) {
              selector += "." + MONOloop.jq.trim(classNames).replace(/\s/gi, ".");
              //.monoloopBorder.monoloopBorderFix
              selector = selector.replace(/.monoloopBorderFix/ , '') ; 
              selector = selector.replace(/.monoloopBorder/ , '') ; 
              
            }
            
            // N : + support eq element ; 
            var curElement = MONOloop.jq(selector); 
            if( curElement.length > 1){
                 for( var i = 0 ; i < curElement.length ; i++){
                    if( this == curElement[i])
                        selector += ':eq('+i+')' ; 
                 }
                  
            }
            // --------- Get Selector String ----------- //  
            /*
            if( top.tracker_main != undefined){  
            	if( top.tracker_main.step2Obj != null)
                	top.tracker_main.step2Obj.changeSelectorTxt(  selector  ) ; 
            } 
			if( top.assets != undefined){  
				if( top.assets.xpathObj != null)
                	top.assets.xpathObj.changeSelectorTxt(  selector  ) ; 
   		
            }
            else if( top.events != undefined){  
                top.events.xpathObj.changeSelectorTxt(  selector  ) ; 
            }
            */
            parent.postMessage(JSON.stringify({
              t: "tracker-changeSelectorTxt" , 
			  selector : selector   
            }), '*');
            //top.ma_customvars.changeSelectorTxt(  selector  ) ; 
            return false ; 
        }
 
        
    }) ;  
});