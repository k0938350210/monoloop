monoloop_testbench_view = {

	bufferHTML : '' ,

    printPage : function(){
        print() ;
    } ,

    testData : {} ,
    mongoDetail : {} ,

    processTestbenchPreview : function(jSonStr , params , resultData){
    	//console.debug('inv : start preview process') ;
        var contentObj = MONOloop.jq.parseJSON( jSonStr );
        var paramsObj = MONOloop.jq.parseJSON( params );
        //console.debug(paramsObj) ;
        MONOloop.jq('body').prepend(  resultData  ) ;

        var data = {}  ;

        data['testbench'] = 1 ;

  		// default function
  		data['ml_contains'] = function(haystack,needle){
  			var i = (haystack + '').indexOf(needle);
  			return i === -1 ? false : true;
  		} ;

  		data['ml_dose_not_contains']  = function(haystack,needle){
  			var i = (haystack + '').indexOf(needle);
  			return i === -1 ? true : false;
  		} ;

  		data['ml_starts_with']  = function(haystack,needle){
  			var i = (haystack + '').indexOf(needle);
  			return i === 0 ? true : false;
  		} ;

  		data['ml_ends_with']  =  function(str, suffix) {
		    return str.indexOf(suffix, str.length - suffix.length) !== -1;
		}

  		for (key in paramsObj)
		{
			var obj = paramsObj[key] ;


			if( data[obj.connector] == undefined){
				data[obj.connector] = {} ;
			}

			var i = (obj.field + '').indexOf('[');
			if( i == -1){

				if( obj.type == 'integer'){
					data[obj.connector][obj.field] = parseInt( obj.value ) ;
				}else if(obj.type == 'function'){
					data[obj.connector][obj.field + '_001'] = obj.value ;
					if( obj.ReturnValue == 'bool' || obj.ReturnValue == 'integer'){
						;
					}else{
						obj.value = '\'' + obj.value + '\'' ;
	 				}
					var fString = 'data[\''+obj.connector+'\'][\''+obj.field+'\'] = function(){return '+ obj.value +' ; } ;' ;
					eval(fString) ;
					//data[obj.connector][obj.field] = function(){return   obj.value  ; } ;
				}else{
					data[obj.connector][obj.field] = obj.value ;
				}
			}else{
				var sub = obj.field.substring(0,i ) ;
				var j = (obj.field + '').indexOf(']');
				var subIndex =  obj.field.substring(i+1,j ) ;
				if( data[obj.connector][sub] == undefined){
					data[obj.connector][sub] = {} ;
	 			}

	 			if( obj.type == 'integer'){
					data[obj.connector][sub][subIndex] = parseInt( obj.value ) ;
				}else if(obj.type == 'function'){
					data[obj.connector][sub][subIndex + '_001'] = obj.value ;
					if( obj.ReturnValue == 'bool' || obj.ReturnValue == 'integer'){
						;
					}else{
						obj.value = '\'' + obj.value + '\'' ;
	 				}
					if( 'undefined'  === typeof functionParams[obj.connector + '-' + obj.field ] )
						functionParams[obj.connector + '-' + obj.field ] = new Array();
					functionParams[obj.connector + '-' + obj.field ].push(obj) ;
				}else{
					data[obj.connector][sub][subIndex] = obj.value ;
				}
			}
		}

		//console.debug(data) ;

        for( var i = 0 ; i < contentObj.length ; i++){
			var obj = contentObj[i] ;
			if( "placementType" in obj ) {
		 		 //console.debug(obj.code) ;
		 		 obj.code = obj.code.replace(/<%-/g , "<%=") ;
			     var html = new EJS({text: obj.code }).render(data);
			     console.debug(html) ;

			     var match = /MONOloop\.\$/.test(html);
          		 if (match) {
          		 	MONOloop.$ = MONOloop.jq ;
         		 }
			     if( MONOloop.jq.trim(  html )  != '' ){
			     	html
			     	if( MONOloop.jq( obj.mappedTo).length > 0){
	                    var  divID = 'monoloop_dynamic_content_' +  obj.monoloop_id ;
	                    html = '<div id="'+ divID + '" class="monoloop_content" >' +  html + '</div>' ;
	                    if(  obj.placementType == 0 ){
	                        MONOloop.jq( obj.mappedTo).before(html) ;
	                    }else if( obj.placementType == 1 ){
	                        MONOloop.jq( obj.mappedTo).after(html) ;
	                    }else if( obj.placementType == 2 ){ // inside before
	                        MONOloop.jq( obj.mappedTo).html(html + MONOloop.jq( obj.mappedTo).html()) ;
	                    }else if( obj.placementType == 3 ){ // inside after
	                        MONOloop.jq( obj.mappedTo).append(html) ;
	                    }else if( obj.placementType == 4 ){ // replace
	                        MONOloop.jq( obj.mappedTo).replaceWith(html) ;
	                    } // Mantis #4550
											else if( obj.placementType == 5 ){ // replace
	                        MONOloop.jq( obj.mappedTo).replaceWith(html) ;
	                    }
	                }

	                if( MONOloop.jq.trim( obj.addJS) != ''){
	                    //console.debug(data[j].content[i].addJS) ;
	                    MONOloop.jq.globalEval(obj.addJS) ;
	                }
			     }
			}
		}
    } ,

    processTestbench2 : function(testData , mongoDetail , loadDefault ){
 		//console.debug(testData) ;
 		//console.debug(mongoDetail) ;
 		var me = this ;

 		if(loadDefault){
 			if(me.bufferHTML == ''){
	 			me.bufferHTML = MONOloop.jq('BODY').html()  ;
	 		}else{
	 			// Load Default  Clear all data ;
	 			MONOloop.jq('BODY').html(me.bufferHTML) ;
	 		}
 		}

 		me.testData = testData ;
 		me.mongoDetail = mongoDetail ;

 		var found = false ;
 		for( var i = 0 ; i < mongoDetail.length ; i++){
			var obj = mongoDetail[i] ;
			if( obj == null || obj == undefined || obj.placed != undefined )
				continue ;
			//console.debug(obj.mappedTo) ;
			if( MONOloop.jq( obj.mappedTo).length > 0){
 				found = true ;
			}

			if( MONOloop.jq.trim( obj.additionalJS ) != ''){
				found = true ;
			}
		}

 		if( found == false){
 			//alert('not found');
 			return ;
		}

 		//console.debug(mongoDetail) ;


  		// Prepare data obj ;
  		var data = {}  ;

  		data['testbench'] = 1 ;

  		// default function
  		data['ml_contains'] = function(haystack,needle){
  			var i = (haystack + '').indexOf(needle);
  			return i === -1 ? false : true;
  		} ;

  		data['ml_dose_not_contains']  = function(haystack,needle){
  			var i = (haystack + '').indexOf(needle);
  			return i === -1 ? true : false;
  		} ;

  		data['ml_starts_with']  = function(haystack,needle){
  			var i = (haystack + '').indexOf(needle);
  			return i === 0 ? true : false;
  		} ;

  		data['ml_ends_with']  =  function(str, suffix) {
		    return str.indexOf(suffix, str.length - suffix.length) !== -1;
		}
  		// End default function ;

  		var functionParams = {} ;

  		//console.debug(testData) ;

  		for (key in testData)
		{
			var obj = testData[key] ;
			if( data[obj.connector] == undefined){
				data[obj.connector] = {} ;
 			}
			var i = (obj.field + '').indexOf('[');
			if( i == -1){
				if( obj.type == 'integer'){
					data[obj.connector][obj.field] = parseInt( obj.value ) ;
				}else if(obj.type == 'function'){
					//data[obj.connector][obj.field + '_001'] = obj.value ;
					if( obj.ReturnValue == 'bool' || obj.ReturnValue == 'integer'){
						;
					}else{
						obj.value = '\'' + obj.value + '\'' ;
	 				}
					//var fString = 'data[\''+obj.connector+'\'][\''+obj.field+'\'] = function(){return '+ obj.value +' ; } ;' ;
					//eval(fString) ;
					//console.debug(obj) ;
					//data[obj.connector][obj.field] = function(){return   obj.value  ; } ;
					if( 'undefined'  === typeof functionParams[obj.connector + '-' + obj.field ] )
						functionParams[obj.connector + '-' + obj.field ] = new Array();
					functionParams[obj.connector + '-' + obj.field ].push(obj) ;
				}else{
					data[obj.connector][obj.field] = obj.value ;
				}
			}else{
				var sub = obj.field.substring(0,i ) ;
				var j = (obj.field + '').indexOf(']');
				var subIndex =  obj.field.substring(i+1,j ) ;
				if( data[obj.connector][sub] == undefined){
					data[obj.connector][sub] = {} ;
	 			}

	 			var last_str = obj.field.substr(j+1);
	 			var i2 = (last_str + '').indexOf('[');
	 			var j2 = (last_str + '').indexOf(']');
	 			var sub_index2 =  last_str.substring(i2+1,j2 ) ;
	 			if( data[obj.connector][sub][subIndex] == undefined){
					data[obj.connector][sub][subIndex] = {} ;
	 			}

	 			if( obj.type == 'integer'){
					data[obj.connector][sub][subIndex][sub_index2] = parseInt( obj.value ) ;
				}else if(obj.type == 'function'){
					data[obj.connector][sub][subIndex][sub_index2 + '_001'] = obj.value ;
					if( obj.ReturnValue == 'bool' || obj.ReturnValue == 'integer'){
						;
					}else{
						obj.value = '\'' + obj.value + '\'' ;
	 				}
	 				/*
	 				console.debug(obj) ;
					var fString = 'data[\''+obj.connector+'\'][\''+sub+'\'][\''+subIndex+'\'] = function(){return '+ obj.value +' ; } ;' ;
					eval(fString) ;
					*/
					if( 'undefined'  === typeof functionParams[obj.connector + '-' + obj.field ] )
						functionParams[obj.connector + '-' + obj.field ] = new Array();
					functionParams[obj.connector + '-' + obj.field ].push(obj) ;
					//data[obj.connector][obj.field] = function(){return   obj.value  ; } ;
				}else{
					data[obj.connector][sub][subIndex][sub_index2] = obj.value ;
				}
			}
		}
		//console.debug(functionParams) ;
		for(var key in functionParams) {
		   //Process key ;
		   var str = ' var arg = \'\' ;   for( var i = 0 ; i < arguments.length ; i++ ){ arg += arguments[i].toString() ; } ' ;
 		   for( var i = 0 ; i < functionParams[key].length ; i++ ){
		   		var obj = functionParams[key][i] ;
		   		var str2 = '';
		   		for(var key2 in obj['params'] ){
		   			str2 += obj['params'][key2].toString() ;
		   		}
		   		str += ' if( \'' + str2 + '\' == arg ){ return ' + obj.value + ' ; } ' ;
		   		var j = (obj.field + '').indexOf('[');
				if( j == -1){
			   		var fString = 'data[\''+obj.connector+'\'][\''+obj.field+'\'] = function(){  '+ str +' ; } ;' ;
					eval(fString) ;
				}else{
					var sub = obj.field.substring(0,j ) ;
					var k = (obj.field + '').indexOf(']');
					var subIndex =  obj.field.substring(j+1,k ) ;
					if( data[obj.connector][sub] == undefined){
						data[obj.connector][sub] = {} ;
		 			}
		 			var fString = 'data[\''+obj.connector+'\'][\''+sub+'\'][\''+subIndex+'\'] = function(){  '+ str +' ; } ;' ;
					eval(fString) ;
				}
		   }
		}
		//console.debug(str) ;
		// Process Function ;
		//data['testData'] = testData  ;
		//console.debug(data) ;
		var arr = [] ;

		//Preprocess XPath ;
		for( var i = 0 ; i < mongoDetail.length ; i++){
			var obj = mongoDetail[i] ;
			if( obj == null || obj == undefined || obj.placed != undefined )
				continue ;
			var extraClass = 'ml_temp_' + i ;
			mongoDetail[i].extraClass = extraClass ;
			if( MONOloop.jq( obj.mappedTo).length > 0){
				MONOloop.jq( obj.mappedTo).addClass(extraClass) ;
				mongoDetail[i].placed = true ;
			}
		}


		for( var i = 0 ; i < mongoDetail.length ; i++){
			var obj = mongoDetail[i] ;
			if( obj == null || obj == undefined  )
				continue ;

			var urlAddJS = obj.additionalJS ;
            var currentTime = new Date() ;
            if( MONOloop.jq.trim(urlAddJS) != '' && MONOloop.jq.inArray(urlAddJS , arr) == -1 ){
            	arr.push(urlAddJS) ;
                MONOloop.jq.getScript(urlAddJS + '?time=' + currentTime.getSeconds() );
                MONOloop.$ = MONOloop.jq ;
            }
			/*
			var urlAddJS = data[j].pageAddJS ;
            var currentTime = new Date() ;
            if( MONOloop.jq.trim(urlAddJS) != ''){
                MONOloop.jq.getScript(urlAddJS + '?time=' + currentTime.getSeconds() );
            }
			*/

			if( "placementType" in obj ) {

				 if (typeof obj.code === 'string' || obj.code instanceof String) {
				 		obj.code = obj.code.replace(/<%-/g , "<%=") ;
				 }

				//console.debug(obj.code) ;
				//console.debug(data) ;

			     var html = new EJS({text: obj.code }).render(data);
				//console.debug(html) ;
			     var extraClassXpath = '.' + obj.extraClass ;
			     if( MONOloop.jq.trim(  html )  != '' && MONOloop.jq.trim(  html ) != ' '  ){
					//console.debug(extraClassXpath) ;
					//console.log(obj)  ;
			     	if( MONOloop.jq( extraClassXpath).length > 0){
			     		var  divID = 'monoloop_dynamic_content_' +  obj._id ;
	                    try{
		                    $html = MONOloop.jq(html) ;
		                    //console.log($html)  ;
		                    //console.log($html.outerHTML() );
		                    //$html.addClass('monoloop_content') ;
		                    //$html.addClass(divID) ;
		                    if( $html[0] != undefined && $html.outerHTML() == html){
		                    	//html = $html[0].outerHTML ;

		                    	html = '' ;
		                    	MONOloop.jq($html[0]).addClass('monoloop_content') ;
		                    	MONOloop.jq($html[0]).addClass(divID) ;
		                    	for(var k = 0 ; k < $html.length ; k++){
		                    		if( $html[k].outerHTML == undefined)
		                    			continue ;
		                    		html += $html[k].outerHTML ;
		                    	}
		                    	//console.debug($html[0].outerHTML) ;
		                    }
		                    html = '<div id="'+ divID + '" class="monoloop_content" >' +  html + '</div>' ;
	                    }catch(err){
	                    	html = '<div id="'+ divID + '" class="monoloop_content" >' +  html + '</div>' ;
	                    }

	                    var match = /MONOloop\.\$/.test(html);
		          		 if (match) {
		          		 	MONOloop.$ = MONOloop.jq ;
		         		 }

	                 	//console.debug(html) ;
	                    //html = '<div id="'+ divID + '" class="monoloop_content" >' +  html + '</div>' ;
	                    if(  obj.placementType == 0 ){
	                        MONOloop.jq( extraClassXpath).before(html) ;
	                    }else if( obj.placementType == 1 ){
	                    	//console.debug(html) ;
	                        MONOloop.jq( extraClassXpath ).after(html) ;
	                    }else if( obj.placementType == 2 ){ // inside before
	                        MONOloop.jq( extraClassXpath ).html(html + MONOloop.jq( obj.mappedTo).html() ) ;
	                    }else if( obj.placementType == 3 ){ // inside after
	                        MONOloop.jq( extraClassXpath ).append(html) ;
	                    }else if( obj.placementType == 4 ){ // replace
	                    	//console.debug(html) ;
	                        MONOloop.jq( extraClassXpath ).replaceWith(html) ;
	                    }else if( obj.placementType == 5 ){ // replace
	                    	//console.debug(html) ;
	                        MONOloop.jq( extraClassXpath ).html(html) ;
	                    }
	                }

	                if( MONOloop.jq.trim( obj.addJS) != ''){
	                    //console.debug(data[j].content[i].addJS) ;
	                    MONOloop.jq.globalEval(obj.addJS) ;
	                }

	                var divID =  '#monoloop_dynamic_content_' +  obj._id ;
	                // Orange bubble render ;
	                if( MONOloop.jq( divID ).length > 0 ){

		                var right = MONOloop.jq(divID).offset().left + MONOloop.jq(divID).outerWidth();
		                var top = MONOloop.jq(divID).offset().top ;
		                MONOloop.jq('#preplaced-panel').append('<div class="speech-bubble" onclick="monoloop_testbench_view.bubbleClick(event , \''+ obj.uid+'\')" id="preplace-'+ obj._id+'"> </div>') ;
		                MONOloop.jq('#preplace-'+ obj._id).addClass('speech-bubble') ;
		                MONOloop.jq('#preplace-'+ obj._id).css('top' , top) ;
		                MONOloop.jq('#preplace-'+ obj._id).css('left' ,  right - 25) ;

		                // Remove extraclass ;
		                MONOloop.jq(extraClassXpath).removeClass( obj.extraClass ) ;
	                }

			     }
					 else {
             //show bubble in case of condition is added
             if( MONOloop.jq(extraClassXpath).length > 0){
               var right = MONOloop.jq(extraClassXpath).offset().left + MONOloop.jq(extraClassXpath).outerWidth();
               var top = MONOloop.jq(extraClassXpath).offset().top ;
               MONOloop.jq('#preplaced-panel').append('<div class="speech-bubble" onclick="monoloop_testbench_view.bubbleClick(event , \''+ obj.uid+'\')" id="preplace-'+ obj._id+'"> </div>') ;
               MONOloop.jq('#preplace-'+ obj._id).addClass('speech-bubble') ;
               MONOloop.jq('#preplace-'+ obj._id).css('top' , top) ;
               MONOloop.jq('#preplace-'+ obj._id).css('left' ,  right - 25) ;

               // Remove extraclass ;
               MONOloop.jq(extraClassXpath).removeClass( obj.extraClass ) ;
	           }
			     }
			}
		}
		 me.mongoDetail = mongoDetail ;
   	} ,

    processTestbench : function(data ){
        /*
        if( urlAddJS != undefined){
            // Page page additionJS
            var urlAddJS_Array = urlAddJS.split("|") ;
            MONOloop.jq.ajaxSetup({async: false});
            for( var i = 0 ; i < urlAddJS_Array.length ; i++){
                var currentTime = new Date() ;
                if( MONOloop.jq.trim(urlAddJS_Array[i]) == ''){
                    continue ;
                }
                MONOloop.jq.getScript(urlAddJS_Array[i] + '?time=' + currentTime.getSeconds() );
            }
            MONOloop.jq.ajaxSetup({async: true});
        }
        */

        if( data == '' || data == undefined )
            return  ;



        for( var j = 0 ; j < data.length ; j++){
            var urlAddJS = data[j].pageAddJS ;
            var currentTime = new Date() ;
            if( MONOloop.jq.trim(urlAddJS) != ''){
                MONOloop.jq.getScript(urlAddJS + '?time=' + currentTime.getSeconds() );
            }

            for( var i = 0 ; i < data[j].content.length ; i++ ){
                if( MONOloop.jq( data[j].content[i].mappedTo).length > 0){
                    var  divID = 'monoloop_dynamic_content_' +  data[j].content[i].monoloop_id ;
                    var  html = '<div id="'+ divID + '" class="monoloop_content" >' +  data[j].content[i].content + '</div>' ;
                    if(  data[j].content[i].placementType == 0 ){
                        MONOloop.jq( data[j].content[i].mappedTo).before(html) ;
                    }else if( data[j].content[i].placementType == 1 ){
                        MONOloop.jq( data[j].content[i].mappedTo).after(html) ;
                    }else if( data[j].content[i].placementType == 2 ){ // inside before
                        MONOloop.jq( data[j].content[i].mappedTo).html(html + MONOloop.jq( data[j].content[i].mappedTo).html()) ;
                    }else if( data[j].content[i].placementType == 3 ){ // inside after
                        MONOloop.jq( data[j].content[i].mappedTo).append(html) ;
                    }else if( data[j].content[i].placementType == 4 ){ // replace
                        MONOloop.jq( data[j].content[i].mappedTo).replaceWith(html) ;
                    }
                }

                if( MONOloop.jq.trim( data[j].content[i].addJS) != ''){
                    //console.debug(data[j].content[i].addJS) ;
                    MONOloop.jq.globalEval( data[j].content[i].addJS) ;
                }
            }
            for( var i = 0 ; i <  data[j].content.length ; i++ ){
                var divID =  '#monoloop_dynamic_content_' +  data[j].content[i].monoloop_id ;
                if( MONOloop.jq(divID).length == 0)
                    continue ;
                var right = MONOloop.jq(divID).offset().left + MONOloop.jq(divID).outerWidth();
                var top = MONOloop.jq(divID).offset().top ;
                MONOloop.jq('#preplaced-panel').append('<div class="speech-bubble" onclick="monoloop_testbench_view.bubbleClick(event , \''+ data[j].content[i].uid+'\')" id="preplace-'+ data[j].content[i].monoloop_id+'"> </div>') ;
                MONOloop.jq('#preplace-'+ data[j].content[i].monoloop_id).addClass('speech-bubble') ;
                MONOloop.jq('#preplace-'+ data[j].content[i].monoloop_id).css('top' , top) ;
                MONOloop.jq('#preplace-'+ data[j].content[i].monoloop_id).css('left' ,  right - 25) ;
            }
        }


    } ,


    bubbleClick : function(e , selector){
    	return ;
        //top.pwMain.bubbleClick(selector) ;
        //top.testbenchTab.bubbleClick(selector) ;
        parent.postMessage(JSON.stringify({
          t: "test-bubbleClick" ,
		  selector : selector
        }), MONOloop.domain);
        agent = MONOloop.jq.browser;
    	if(agent.msie) {
    		window.event.cancelBubble = true;
    	} else {
    		e.stopPropagation();
    	}
    } ,

    bindElement : function(){
    	MONOloop.jq('*').click(function(event) {
	        if(MONOloop.jq(this)[0].nodeName == 'BODY' )
	            return true ;
	        if(MONOloop.jq(this)[0].nodeName == 'HTML' )
	            return true ;

	        return false ;
	    }) ;
    } ,

    reloadbody : function(){
    	MONOloop.jq('body').html(localStorage.getItem("ml-"+document.url));
    }
} ;




MONOloop.jq(document).ready(function() {
	// Mantis #4657 Enable all links on Page in Test Mode
	 // monoloop_testbench_view.bindElement() ;
	/*
	if( jQuery != undefined ){
		jQuery(document).ajaxComplete(function(){
	  		 monoloop_testbench_view.bindElement() ;
	  		 // Show preplace here .
	  		 if( ! MONOloop.jq.isEmptyObject(monoloop_testbench_view.mongoDetail)){

	  		 	monoloop_testbench_view.processTestbench2 (monoloop_testbench_view.testData , monoloop_testbench_view.mongoDetail , false ) ;
	  		 }
		});
	}
	*/


});
