var monoloop_select_view = {
    lock : false  ,
    tempUnlock : false ,
    mode : 1 ,

    step2_xpath : '' ,
    step3_xpath : '' ,
    step4_xpath_n : '' ,
    step4_xpath_s : '' ,
    step4_xpath_p : '' ,
    step4_xpath_q : '' ,

    initPosMessage : function(){
    	var me = this ;
    	var listener = function(event) {
			var data;
			try{
				data = JSON.parse(event.data);
			}catch (e) {
				return ;
			}
			switch (data.t) {
				case 'tracker-basket-checkNext':
				    var msg = me.checkNext() ;
				    parent.postMessage(JSON.stringify({
		              t: "tracker-basket-checkNext_callBack" ,
					  msg : msg
		            }), '*');
				break ;
				case 'tracker-basket-getDataObj' :
					var dObj = me.getDataObj() ;
					parent.postMessage(JSON.stringify({
		              t: "tracker-basket-getDataObj_callBack" ,
					  dObj : dObj
		            }), '*');
				break ;
				case 'init-tracker-setDefaultData' :
					me.setDefaultData(data.data) ;
				break ;
				case 'tracker-basket-setMode' :
					me.setMode(data.baseketStep);
				break ;
			}
		};

		if (window.addEventListener) {
	        return addEventListener("message", listener, false);
		} else {
	        attachEvent("onmessage", listener);
		}
    } ,

    processCustomFilter : function(jsStr  , setback , setbackID){
        var str = 'function testX001(){'
                + ' try { '
                + jsStr
                + ' }catch(err) {'
                + ' return \'error\' ;'
                + ' } } ' ;
        MONOloop.jq.globalEval(str) ;
        var ret = MONOloop.jq.trim( testX001() ) ;
		if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" ,
			  setbackID : setbackID ,
              data : ret
            }), '*');
        }else{
            return ret ;
        }
    } ,

    processDefaultFilter : function(selector  , setback , setbackID){
        var pos = selector.indexOf('>') ;
        var ret = '' ;
        if( pos == -1)
            ret = '' ;
        if( MONOloop.jq.trim( selector ) == '' && selector == undefined)
            ret = '' ;
        else
        	ret = MONOloop.getCartInnerContent( MONOloop.jq(selector));



       	if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" ,
			  setbackID : setbackID ,
              data : ret
            }), '*');
        }else{
            return ret ;
        }
    } ,

    processDefaultFilter_Price : function(selector  , setback , setbackID){
    	//console.debug(selector) ;
    	//console.debug(MONOloop.jq(selector)) ;
        var price = MONOloop.getCartInnerContent( MONOloop.jq(selector));
        var ret = '' ;
        //console.debug(price) ;
        price =  price.replace(/[^0-9\.\,]+/g,"")    ;
        var x1 = price.indexOf(',') ;
        var x2 = price.indexOf('.') ;
        var convert = false ;
        var l = price.length  ;
        if( l - x1 > 3 && x1 != -1 && x2 == -1){
            ;
        }else if(l - x2 > 3 && x2 != -1 && x1 == -1){
            convert = true ;
        }else if( x2 < x1 && x1 != -1){
            convert = true ;
        }

        if( convert){
            price = price.replace('.','###') ;
            price = price.replace(',','.') ;
            price = price.replace('###',',') ;
        }
        price =  price.replace(/[^0-9\.]+/g,"")    ;

        price = Number(price) ;
        ret = price ;

        if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" ,
			  setbackID : setbackID ,
              data : ret
            }), '*');
        }else{
            return ret ;
        }
    } ,

    processDefaultFilter_Currency : function( selector  , setback , setbackID ){
        var price = MONOloop.getCartInnerContent( MONOloop.jq(selector));
        var ret = '' ;
        price = price.replace(/[0-9\.\,]+/g,"") ;
        price = price.replace(/(?:&nbsp;|<br>)/g,'');
        price = MONOloop.jq.trim(price) ;
        ret = price ;

        if(setback){
            parent.postMessage(JSON.stringify({
              t: "tracker-setvalue-to-id" ,
			  setbackID : setbackID ,
              data : ret
            }), '*');
        }else{
            return ret ;
        }
    } ,

    setDefaultData : function(dataObj){
        var me = this ;
        if( dataObj == undefined){
            return ;
        }
        if( me.step2_xpath == ''){
            me.step2_xpath = dataObj.step2_xpath ;
        }
        if( me.step3_xpath == ''){
            me.step3_xpath = dataObj.step3_xpath ;
        }
        if( me.step4_xpath_n == ''){
            me.step4_xpath_n = dataObj.step4_xpath_n ;
        }
        if( me.step4_xpath_s == ''){
            me.step4_xpath_s = dataObj.step4_xpath_s ;
        }
        if( me.step4_xpath_p == ''){
            me.step4_xpath_p = dataObj.step4_xpath_p ;
        }
        if( me.step4_xpath_q == ''){
            me.step4_xpath_q = dataObj.step4_xpath_q ;
        }

    } ,

    showDefaultXpath : function(){
        var me = this ;
        MONOloop.jq('#ml-cart-table-select .l1').hide() ;
        MONOloop.jq('#ml-cart-table-select .l2').hide() ;
        MONOloop.jq('#ml-cart-table-select .l3').hide() ;
        MONOloop.jq('#ml-cart-table-select .l4').hide() ;
        MONOloop.jq('#ml-cart-table-select .l5').hide() ;
        MONOloop.jq('#ml-cart-table-select .l6').hide() ;
        if(  me.step2_xpath != '')
            me.showText('1' , me.step2_xpath) ;
        if(  me.step3_xpath != '')
            me.showText('2' , me.step2_xpath + ' ' + me.step3_xpath) ;
        if(  me.step4_xpath_n != '')
            me.showText('3' , me.step2_xpath + ' ' + me.step3_xpath + ' ' + me.step4_xpath_n) ;
        if(  me.step4_xpath_s != '')
            me.showText('4' , me.step2_xpath + ' ' + me.step3_xpath + ' ' + me.step4_xpath_s) ;
        if(  me.step4_xpath_p != '')
            me.showText('5' , me.step2_xpath + ' ' + me.step3_xpath + ' ' + me.step4_xpath_p) ;
        if(  me.step4_xpath_q != '')
            me.showText('6' , me.step2_xpath + ' ' + me.step3_xpath + ' ' + me.step4_xpath_q) ;


    } ,

    showText : function(textID , selector){
        var me = this ;

        if( MONOloop.jq(selector).length == 0)
            return ;

        var top = MONOloop.jq(selector).offset().top ;
        var left = MONOloop.jq(selector).offset().left ;
        MONOloop.jq('#ml-cart-table-select .l'+ textID).css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l'+ textID).css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l'+ textID).show() ;
    } ,

    setMode : function(m){
        var me = this ;
        me.mode = m ;
        me.removeYellowBorder() ;
        if( me.mode == 1 ){
            //console.debug(MONOloop.jq('#ml-cart-table-list').html() ) ;
        }else if( me.mode == 2){
            //console.debug('Set 2') ;
            me.genCartTable() ;
            me.showDefaultXpath() ;

        }else if( me.mode == 3){
            //console.debug('Set 3') ;

            me.genProductRow() ;
        }else if( me.mode == 4){
            me.genProductCollum() ;
        }else if( me.mode == 5){
            me.genProductCollum2() ;
        }else if( me.mode == 6){
            me.genProductCollum3() ;
        }else if( me.mode == 7){
            me.genProductCollum4() ;
        }
    }  ,

    checkNext : function(){
        var me = this ;
        if( me.mode == 2){
            if( MONOloop.jq(me.step2_xpath).length == 0){
                return 'Please select cart' ;
            }
        }
        if( me.mode == 3){
            if( MONOloop.jq(me.step3_xpath).length == 0){
                return 'Please select product row' ;
            }
        }
        return '' ;
    } ,

    getDataObj : function(){
        var data = {} ;
        var me = this ;
        data['step2_xpath'] = me.step2_xpath ;
        data['step3_xpath'] = me.step3_xpath ;
        data['step4_xpath_n'] = me.step4_xpath_n ;
        data['step4_xpath_s'] = me.step4_xpath_s ;
        data['step4_xpath_p'] = me.step4_xpath_p ;
        data['step4_xpath_q'] = me.step4_xpath_q ;
        return data ;
    } ,

    removeYellowBorder : function(){
        MONOloop.jq('#ml-cart-table-list').html('') ;
    } ,

    genCartTable : function(){
        var me  = this ;
        var tables = MONOloop.jq('table , dl , ' + me.step2_xpath) ;

        for( var i = 0 ; i < tables.length ; i++ ){
            var selector = me.getXpathByEL(tables[i] , true) ;
            //MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.cartTableClick(event,\''+selector+'\');"' , '')) ;
            MONOloop.jq('#ml-cart-table-list').html( MONOloop.jq('#ml-cart-table-list').html() + me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.cartTableClick(event,\''+selector+'\');"' , '') )
            me.disPlayBorderByElement(tables[i] , '-'+i) ;
        }


    } ,

    genProductRow : function(){
        var me  = this ;
        //console.debug(me.step2_xpath) ;
        var rows = MONOloop.jq(me.step2_xpath).find('tr ,' + me.step3_xpath)  ;
        //console.debug(rows) ;
        for( var i = 0 ; i < rows.length ; i++ ){
            //var selector = me.getXpathByEL(rows[i] , false) ;
            MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.productRowClick(event,\'tr:eq('+i+')\');"' , '')) ;
            me.disPlayBorderByElement(rows[i] , '-'+i) ;
        }

        var rows = MONOloop.jq(me.step2_xpath).find('dd')  ;
        //console.debug(rows) ;
        for( var i = 0 ; i < rows.length ; i++ ){
            //var selector = me.getXpathByEL(rows[i] , false) ;
            MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.productRowClick(event,\'dd:eq('+i+')\');"' , '')) ;
            me.disPlayBorderByElement(rows[i] , '-'+i) ;
        }
    } ,

    genProductCollum : function(){
        var me  = this ;

        var rows = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath).find('> *')  ;
        for( var i = 0 ; i < rows.length ; i++ ){
            MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.productCol1Click(event,\'> *:eq('+i+')\');"' , '')) ;
            me.disPlayBorderByElement(rows[i] , '-'+i) ;
        }
    } ,

    genProductCollum2 : function(){
        var me  = this ;
        var rows = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath).find('> *')  ;
        for( var i = 0 ; i < rows.length ; i++ ){
            if( '> *:eq('+i+')' ==  me.step4_xpath_n ){
                continue ;
            }
            MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.productCol2Click(event,\'> *:eq('+i+')\');"' , '')) ;
            me.disPlayBorderByElement(rows[i] , '-'+i) ;
        }
    } ,

    genProductCollum3 : function(){
        var me  = this ;
        var rows = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath).find('> *')  ;
        for( var i = 0 ; i < rows.length ; i++ ){
            if( '> *:eq('+i+')' ==  me.step4_xpath_n || '> *:eq('+i+')' ==  me.step4_xpath_s  ){
                continue ;
            }
            MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.productCol3Click(event,\'> *:eq('+i+')\');"' , '')) ;
            me.disPlayBorderByElement(rows[i] , '-'+i) ;
        }
    } ,


    genProductCollum4 : function(){
        var me  = this ;
        var rows = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath).find('> *')  ;
        for( var i = 0 ; i < rows.length ; i++ ){
            if( '> *:eq('+i+')' ==  me.step4_xpath_n || '> *:eq('+i+')' ==  me.step4_xpath_s ||  '> *:eq('+i+')' ==  me.step4_xpath_p ){
                continue ;
            }
            MONOloop.jq('#ml-cart-table-list').append(me.getNewBorder('-'+i , '-p1' , 'onclick="monoloop_select_view.productCol4Click(event,\'> *:eq('+i+')\');"' , '')) ;
            me.disPlayBorderByElement(rows[i] , '-'+i) ;
        }
    } ,

    checkBubble : function(e){
        var agent = MONOloop.jq.browser;
    	if(agent.msie) {
    		window.event.cancelBubble = true;
    	} else {
    		e.stopPropagation();
    	}
    } ,

    cartTableClick : function(e , selector){
        var me = this ;
        me.checkBubble(e) ;
        // Add select tex t ;
        var top = MONOloop.jq(selector).offset().top ;
        var left = MONOloop.jq(selector).offset().left ;

        if(me.step2_xpath == selector){
            me.step2_xpath = '' ;
            MONOloop.jq('#ml-cart-table-select .l1').hide() ;
            return ;
        }

        MONOloop.jq('#ml-cart-table-select .l1').css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l1').css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l1').show() ;

        me.step2_xpath = selector ;
    } ,


    productRowClick : function(e , selector){
        var me = this ;
        me.checkBubble(e) ;
        // Add select text ;
        var top = MONOloop.jq( me.step2_xpath + ' ' + selector).offset().top ;
        var left = MONOloop.jq(  me.step2_xpath + ' ' + selector).offset().left ;

        if(me.step3_xpath == selector){
            me.step3_xpath = '' ;
            MONOloop.jq('#ml-cart-table-select .l2').hide() ;
            return ;
        }

        MONOloop.jq('#ml-cart-table-select .l2').css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l2').css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l2').show() ;

        me.step3_xpath = selector ;
    } ,

    productCol1Click : function(e , selector){
        var me = this ;
        me.checkBubble(e) ;

        if(me.step4_xpath_n == selector){
            me.step4_xpath_n = '' ;
            MONOloop.jq('#ml-cart-table-select .l3').hide() ;
            return ;
        }
        // Add select text ;
        var top = MONOloop.jq(me.step2_xpath + ' '  + me.step3_xpath + ' ' + selector).offset().top ;
        var left = MONOloop.jq(me.step2_xpath + ' ' + me.step3_xpath + ' ' + selector).offset().left ;
        MONOloop.jq('#ml-cart-table-select .l3').css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l3').css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l3').show() ;

        me.step4_xpath_n = selector ;
    } ,

    productCol2Click : function(e , selector){
        var me = this ;
        me.checkBubble(e) ;

        if(me.step4_xpath_s == selector){
            me.step4_xpath_s = '' ;
            MONOloop.jq('#ml-cart-table-select .l4').hide() ;
            return ;
        }
        // Add select text ;
        var top = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath + ' ' + selector).offset().top ;
        var left = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath + ' ' + selector).offset().left ;
        MONOloop.jq('#ml-cart-table-select .l4').css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l4').css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l4').show() ;

        me.step4_xpath_s = selector ;
    } ,

    productCol3Click : function(e , selector){
        var me = this ;
        me.checkBubble(e) ;

        if(me.step4_xpath_p == selector){
            me.step4_xpath_p = '' ;
            MONOloop.jq('#ml-cart-table-select .l5').hide() ;
            return ;
        }
        // Add select text ;
        var top = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath + ' ' + selector).offset().top ;
        var left = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath + ' ' + selector).offset().left ;
        MONOloop.jq('#ml-cart-table-select .l5').css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l5').css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l5').show() ;

        me.step4_xpath_p = selector ;
    } ,

    productCol4Click : function(e , selector){
        var me = this ;
        me.checkBubble(e) ;

        if(me.step4_xpath_q == selector){
            me.step4_xpath_q = '' ;
            MONOloop.jq('#ml-cart-table-select .l6').hide() ;
            return ;
        }
        // Add select text ;
        var top = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath + ' ' + selector).offset().top ;
        var left = MONOloop.jq(me.step2_xpath + ' ' +me.step3_xpath + ' ' + selector).offset().left ;
        MONOloop.jq('#ml-cart-table-select .l6').css('top' , top -3) ;
        MONOloop.jq('#ml-cart-table-select .l6').css('left' , left -3) ;
        MONOloop.jq('#ml-cart-table-select .l6').show() ;

        me.step4_xpath_q = selector ;
    } ,

    getNewBorder : function(idSubFix , classSubFix , onClickString , onMouseOutString){
        return '<div class="ml-border-group" '+onClickString+' '+onMouseOutString+'><div id="ml-border-l'+idSubFix+'" class="ml-border'+classSubFix+'"></div></div>' ;
    } ,

    getXpathByEL : function(el , isIncludeEQ){
        var selector = MONOloop.jq(el).parents()
                .map(function(){
                    var id = MONOloop.jq(this).attr("id");
                    var returnPart = this.tagName ;
                    if (id){
                        returnPart += "#"+ id;
                    }else{
                        var classNames = MONOloop.jq(this).attr("class");
                        if (classNames) {
                        //returnPart += "." + MONOloop.jq.trim(classNames).replace(/\s/gi, ".");
                        }
                    }
                    return  returnPart ;
                })
                .get().reverse().join(" ");
        var selectorTest = selector ;
        if (selector) {
          selector += " "+ MONOloop.jq(el)[0].nodeName;
        }

        var id = MONOloop.jq(this).attr("id");
        if (id) {
          selector += "#"+ id;
        }

        var classNames = MONOloop.jq(this).attr("class");
        if (classNames) {
          selector += "." + MONOloop.jq.trim(classNames).replace(/\s/gi, ".");
          //.monoloopBorder.monoloopBorderFix
          selector = selector.replace(/.monoloopBorderFix/ , '') ;
          selector = selector.replace(/.monoloopBorder/ , '') ;
        }

        if( isIncludeEQ ){
            var curElement = MONOloop.jq(selector);
            if( curElement.length > 1){
                 for( var i = 0 ; i < curElement.length ; i++){
                    if( el == curElement[i])
                        selector += ':eq('+i+')' ;
                 }
            }
        }


        return selector ;
    } ,

    disPlayBorderByElement : function(el , subfix){

        var top = MONOloop.jq(el).offset().top ;
        var left = MONOloop.jq(el).offset().left ;
        var hight = MONOloop.jq(el).outerHeight();
        var width =  MONOloop.jq(el).outerWidth();

        //MONOloop.jq('#ml-border-l'+subfix).parent().css('width' , width) ;
        //MONOloop.jq('#ml-border-l'+subfix).parent().css('height' , hight) ;
        MONOloop.jq('#ml-border-l'+subfix).css('top' , top -3) ;
        MONOloop.jq('#ml-border-l'+subfix).css('left' ,  left - 3) ;
        MONOloop.jq('#ml-border-l'+subfix).css('width' , width ) ;
        MONOloop.jq('#ml-border-l'+subfix).css('height' , hight ) ;

    } ,

    disPlayBorder: function(selectorClass , subfix ){
         MONOloop.jq('.ml-border' + subfix ).hide() ;
        if( MONOloop.jq(selectorClass ).length == 0){
            //console.debug('yyy') ;
            return false ;
        }
        //console.debug('zz') ;
        MONOloop.jq('.ml-border' + subfix ).show() ;
        var top = MONOloop.jq( selectorClass).offset().top ;
        var left = MONOloop.jq( selectorClass).offset().left ;
        var hight = MONOloop.jq( selectorClass).outerHeight();
        var width =  MONOloop.jq( selectorClass).outerWidth();

        MONOloop.jq('#ml-border-l'+subfix).css('top' , top -3) ;
        MONOloop.jq('#ml-border-l'+subfix).css('left' ,  left - 3) ;
        MONOloop.jq('#ml-border-l'+subfix).css('width' , 3) ;
        MONOloop.jq('#ml-border-l'+subfix).css('height' , hight + 6) ;

        MONOloop.jq('#ml-border-u'+subfix).css('top' , top -3) ;
        MONOloop.jq('#ml-border-u'+subfix).css('left' ,  left ) ;
        MONOloop.jq('#ml-border-u'+subfix).css('width' , width) ;
        MONOloop.jq('#ml-border-u'+subfix).css('height' , 3) ;

        MONOloop.jq('#ml-border-r'+subfix).css('top' , top -3) ;
        MONOloop.jq('#ml-border-r'+subfix).css('left' ,  left + width) ;
        MONOloop.jq('#ml-border-r'+subfix).css('width' , 3) ;
        MONOloop.jq('#ml-border-r'+subfix).css('height' , hight + 6) ;

        MONOloop.jq('#ml-border-s'+subfix).css('top' , top + hight) ;
        MONOloop.jq('#ml-border-s'+subfix).css('left' ,  left ) ;
        MONOloop.jq('#ml-border-s'+subfix).css('width' , width) ;
        MONOloop.jq('#ml-border-s'+subfix).css('height' , 3) ;

        return true ;
    }
} ;

MONOloop.jq(document).ready(function() {
    /*
    MONOloop.jq('*').mouseenter(function(event) {
        if( monoloop_select_view.mode == 1 ){
            MONOloop.jq('*').removeClass('monoloopBorder') ;
            monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
            return false ;
        }else{
            event.stopPropagation();
            MONOloop.jq('*').removeClass('monoloopBorder') ;
            if( monoloop_select_view.mode == 2 ){
                // Check for table mouse over ;
                var foundTable = false ;
                var tableElement = MONOloop.jq('table');
                if( tableElement.length > 1){
                     for( var i = 0 ; i < tableElement.length ; i++){
                        if( this == tableElement[i]){
                            MONOloop.jq(this).addClass('monoloopBorder') ;
                            monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
                            foundTable = true ;
                        }
                     }

                }else{
                    if( this == tableElement){
                        MONOloop.jq(this).addClass('monoloopBorder') ;
                        monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
                        foundTable = true ;
                    }
                }
                if( foundTable == false){
                    MONOloop.jq(this).find('table:first').addClass('monoloopBorder') ;
                    monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
                }
            }else if( monoloop_select_view.mode == 3 ){
                var foundTR = false ;
                var trElement = MONOloop.jq('tr');
                if( trElement.length > 1){
                     for( var i = 0 ; i < trElement.length ; i++){
                        if( this == trElement[i]){
                            MONOloop.jq(this).addClass('monoloopBorder') ;
                            monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
                            foundTR = true ;
                        }
                     }

                }else{
                    if( this == trElement){
                        MONOloop.jq(this).addClass('monoloopBorder') ;
                        monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
                        foundTR = true ;
                    }
                }
                if( foundTR == false){
                    MONOloop.jq(this).find('tr:first').addClass('monoloopBorder') ;
                    monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
                    if( MONOloop.jq('.monoloopBorder' ).length > 0){
                        foundTR = true ;
                    }
                }

                if( foundTR == false){
                    MONOloop.jq(this).parentsUntil('tr:first').parent().addClass('monoloopBorder') ;
                    monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
                }
            }
        }
    });

    MONOloop.jq('*').mouseleave(function(event) {
        if( monoloop_select_view.lock == false ){
            event.stopPropagation();
            MONOloop.jq(this).removeClass('monoloopBorder') ;
            monoloop_select_view.disPlayBorder('monoloopBorder' , '') ;
        }
    });
    */
    MONOloop.jq('a').live("click", function(event){
    	if( monoloop_select_view.mode == 1){
    	    // Check if
    	    if (window['monoloopClientURL2'] != undefined){
    			return previewView.processATag(this) ;
		    }else{
	     		return true ;
	   		}
        }
        return false ;
    });

    MONOloop.jq('a').click(function(){
        if( monoloop_select_view.mode == 1){
    	    // Check if
    	    if (window['monoloopClientURL2'] != undefined){
	    		return previewView.processATag(this) ;
		    }else{
	     		return true ;
	   		}
        }
        return false ;
    }) ;

    MONOloop.jq('* :not(a)').click(function(event){
        if( monoloop_select_view.mode == 1){
            return true ;
        }
        return false ;
    }) ;

}) ;
