previewView = {
    getUrl : function(){
        return document.getElementById('ml-tf-url').value ;
    } , 
    
    processATag : function(cur){ 
        if(! MONOloop.jq(cur).hasClass('ml_p') ){
            var href = MONOloop.jq(cur).attr('href') ;  
            if( href == undefined){
                href = MONOloop.jq(cur).parent('a').attr('href') ; 
            }  
            var jsIndex = href.indexOf('javascript') ; 
            if( jsIndex == 0)
                return true ; 
           
            if( MONOloop.jq.trim( href ) != '' && MONOloop.jq.trim( href ) != undefined ){ 
                var pos = href.indexOf('http') ; 
                if( pos == -1){
                    if(href[0] == '/'){
                        href = href.substring(1) ; 
                        href = monoloopClientURL1 + href ; 
                        
                    }else{  
                        if( monoloopClientURL2[monoloopClientURL2.length -1] != '/'){ 
                            href = monoloopClientURL2 + '/' + href ; 
                        }else{ 
                            href = monoloopClientURL2 + href ; 
                        }  
                    } 
                } 
                
             
                MONOloop.jq(cur).attr('href' , monoloopBaseURL + MONOloop.urlencode(href) ) ; 
            
                if( MONOloop.jq(cur).attr('target') == '_top' || MONOloop.jq(cur).attr('target') == '_blank' || MONOloop.jq(cur).attr('target') == '_parent' ){
                    MONOloop.jq(cur).attr('target' , '_self') ; 
                } 
                
                MONOloop.jq(cur).addClass('ml_p') ;  
            }
        } 
        return true ; 
    }
}