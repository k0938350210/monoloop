// Generated by CoffeeScript 1.7.1
(function() {
  Ext.onReady(function() {
    var changeAccount;
    Ext.fly(document.body).on({
      click: function(e) {
        var parent, ul;
        if (!Ext.fly(e.getTarget()).findParent("#account-selector")) {
          parent = Ext.query("#account-selector");
          ul = Ext.get(parent[0]).query('ul');
          Ext.get(ul[0]).setVisible(false);
          return Ext.get(ul[0]).setHeight(0);
        }
      }
    });
    Ext.select('#account-main').on({
      click: function(e, t) {
        var h, parent, ul;
        parent = Ext.get(t).findParent('#account-selector');
        ul = Ext.get(parent).query('ul');
        h = Ext.get(ul[0]).getHeight();
        if (h === 0) {
          Ext.get(ul[0]).setHeight('auto');
          return Ext.get(ul[0]).setVisible(true);
        } else {
          Ext.get(ul[0]).setVisible(false);
          return Ext.get(ul[0]).setHeight(0);
        }
      }
    });
    Ext.select('.accordion-group a[data-toggle="collapse"]').on({
      click: function(e, t) {
        var datah, h, li, parent, ul;
        parent = Ext.get(t).findParent('.accordion-group');
        ul = Ext.get(parent).query('ul');
        li = Ext.get(ul[0]).query('li');
        if (li.length === 0) {
          return;
        }
        h = Ext.get(ul[0]).getHeight();
        Ext.select('.accordion-group ul').setHeight(0);
        if (!h) {
          datah = Ext.get(ul[0]).getAttribute('data-height');
          return Ext.get(ul[0]).setHeight(parseInt(datah));
        }
      }
    });
    changeAccount = function(feid) {
      alert(feid) ; 
    };
    return window.changeAccount = changeAccount;
  });

}).call(this);

window.monoloopLog = {
	enable : false , 
	log : function(msg){
		if(this.enable){
			console.log(msg);
		}
	}
};
