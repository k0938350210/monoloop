 // Somkiat : Tree object
 // Nut : Implement tree as object ; 
 
function monoloopTreeBase(idFolder , typeDefault){
    this.idFolder = idFolder ;
    this.tree = null ;
    this.typeDefault = typeDefault ;  
    this.createTree() ; 
    //this.tree.getRootNode().expand();
} ;

var tempObject = {
    treeObj : null 
} ; 

monoloopTreeBase.prototype.initData = function(){
    
} ;

monoloopTreeBase.prototype.createTree = function(){
    var Tree = Ext.tree;
    var mtObject = this ;
    var typeDefault = this.typeDefault ; 
    this.tree = new Tree.TreePanel({
        useArrows: true,
        autoScroll: true,
        animate: true,
        enableDD: true,
        ddGroup: 'grid2tree',
        containerScroll: true,
        border: false,
        height: 280,
        // auto create TreeLoader
        dataUrl: 'index.php?eID=t3p_dynamic_content_placement&pid='+monoloop_base.pid+'&cmd=listFolder&typedefault='+this.typeDefault,

        root: {
            nodeType: 'async',
            text: 'Root',
            draggable: false,
            id: '0',
            cls: 'folder'
        } , 
        
        listeners: {
	        	beforenodedrop :function(e) {

					// e.data.selections is the array of selected records
					if(Ext.isArray(e.data.selections)) {	
						var r;
						var cls = 'file';						
						var id = e.target.getPath('id').split('/');
			    		var pid = id[id.length-1];
						for(var i = 0; i < e.data.selections.length; i++) {	
							// get record from selectons
							r = e.data.selections[i];
	
							var data = {};
							data[ mdcBase.firstPI + '[uid]'] = r.get('uid');
			    			data[ mdcBase.firstPI + '[pid]'] = pid;
			    			data[ mdcBase.firstPI + '[cls]'] = cls;
			    			data[ mdcBase.firstPI + '[type]'] = typeDefault;
			    			
			    			mdcBase.showProcessBox('Loading data ....' , 'Loading') ;
			    			
			    			Ext.Ajax.request({
			    				url: 'index.php?eID=t3p_dynamic_content_placement&pid='+monoloop_base.pid+'&cmd=moveFolderSave',
			    				// send additional parameters to instruct server script
			    				method: 'POST',
			    				timeout: '30000',
			    				params:   data ,
			    				// process the response object to add it to the TabPanel:
			    				success: function(xhr) {
			    					Ext.MessageBox.hide();
		    						mtObject.idFolder = pid; 
                            		  
                                    goal_grid.refrehGrid() ; 
			    				},
			    				failure: function() {
			    					Ext.MessageBox.hide(); 
                                    Ext.MessageBox.alert('Error', '#310' ) ; 
			    				}
			    			});	
						}
	
						// we want Ext to complete the drop, thus return true
						return true;
					}
	
					// if we get here the drop is automatically cancelled by Ext
				} ,
			
	        	click : function( node, e ){
	        		mtObject.changeFolder(node);
	        	} ,
	        	
	        	dblclick : function( node, e ){
	        		mtObject.changeFolder(node);
	        	} ,
	        	
	        	expandnode : function( node ){
	        		mtObject.changeFolder(node);
	        	} ,
	        	
				movenode : function( tree, node, oldParent, newParent, index ){
		        	var data = {};
		    		var id = node.getPath('id').split('/');
		    		var uid = id[id.length-1];
		    		var pid = id[id.length-2];
		    		var cls = node.getPath('cls').split('/');
					cls = cls[cls.length-1];
					var oldCls = oldParent.getPath('id').split('/');
					oldCls = oldCls[oldCls.length-1];
					var newCls = newParent.getPath('id').split('/');
					newCls = newCls[newCls.length-1];
		    		
		    		if(oldCls != newCls){		    		
		    			data[ mdcBase.firstPI + '[uid]'] = uid;
		    			data[ mdcBase.firstPI + '[pid]'] = pid;
		    			data[ mdcBase.firstPI + '[cls]'] = cls;
		    			data[mdcBase.firstPI + '[type]'] = mtObject.typeDefault;
		    			
		    			mdcBase.showProcessBox('Loading data ....' , 'Loading') ;
		    			
		    			Ext.Ajax.request({
		    				url: 'index.php?eID=t3p_dynamic_content_placement&pid='+monoloop_base.pid+'&cmd=moveFolderSave',
		    				// send additional parameters to instruct server script
		    				method: 'POST',
		    				timeout: '30000',
		    				params:   data ,
		    				// process the response object to add it to the TabPanel:
		    				success: function(xhr) {
		    					Ext.MessageBox.hide();
	    						mtObject.idFolder = pid; 
                                goal_grid.refrehGrid() ; 
		    				},
		    				failure: function() {
		    					Ext.MessageBox.hide(); 
					            Ext.MessageBox.alert('Error', '#311' ) ; 
		    				}
		    			});	
		    		}
				} 
			}
     }) ; 
} ;

monoloopTreeBase.prototype.openCreateFolderWindow = function(){
    var treeObj =  this.tree ; 
	var form = new Ext.FormPanel({
        frame:true,
        bodyStyle:'padding:5px 5px 0',
        width: '100%',
        defaults: {
            allowBlank: false 
        },

        items: [ {
            xtype:'textfield',
            fieldLabel: 'folder name',
            allowBlank: false  ,
            name: mdcBase.firstPI + '[title]',
            //emptyText: 'Press any value',
            anchor:'90%'
        }],

        buttons: [{
            text: 'Save' , 
            handler: function(){
        		var data = {};
        		var sm = treeObj.getSelectionModel().getSelectedNode();
        		if(sm == null){
	        		var ownerId = 0;
	        		var pid = 0;
        		} else {
        			var ownerId = sm.getPath('owner_id');
	        		var pid = sm.getPath('id');
        		}
        		
        		data[mdcBase.firstPI + '[owner_id]'] = ownerId;
        		data[mdcBase.firstPI + '[pid]'] = pid;

                if(form.getForm().isValid()){
                	form.getForm().submit({
	                    url: 'index.php?eID=t3p_dynamic_content_placement&pid='+monoloop_base.pid+'&cmd=createFolderSave',
	                    waitMsg: 'Saving your data...',
	                    success: function(form , o){
                			mdcBase.msg('Success', o.result.msg);
	                        mainWindow.close() ;
	                        treeObj.loader.load(treeObj.root);
	                        treeObj.getRootNode().expand();
	                        
	                        //lmBase.lmGrid.store.load({params:{start:0, limit:mdc_placement_main.pageItemsCount}});
	                    } , 
	                    failure: function(form , o) {
	                    	mdcBase.msg('Fail',  o.result.msg);
						} ,
						params : data 
	                });
                }
            }

        }]
    });
	
	var mainWindow = new Ext.Window({
        title: 'Create folder .',
        closable:true,
        width:350,
        height:110,
        //border:false,
        plain:true , 
        items: [form]
    });
    
    mainWindow.show(this);
} ;

monoloopTreeBase.prototype.openDeleteFolderWindow = function(){
    var treeObj =  this.tree ;
	var sm = treeObj.getSelectionModel().getSelectedNode();
	if(sm != null){
		var cls = sm.getPath('cls').split('/');
		cls = cls[cls.length-1];
		if(cls == 'folder') {
		    tempObject.treeObj = treeObj ; 
			Ext.MessageBox.confirm('Confirm', 'Are you sure to delete this content ?', this.deleteProcess) ;
		} else {
			alert('Please select folder.');
		}
	}
} ;

monoloopTreeBase.prototype.deleteProcess = function(btn){
    var treeObj =   tempObject.treeObj ;
	if( btn == 'yes'){
		var data = {};
		var sm = treeObj.getSelectionModel().getSelectedNode();
		if(sm != null){
			var uid = sm.getPath('id');
			
			if(uid != ''){
				data[ mdcBase.firstPI + '[uid]'] = uid;
				
				mdcBase.showProcessBox('Loading data ....' , 'Loading') ;
				
				Ext.Ajax.request({
					url: 'index.php?eID=t3p_dynamic_content_placement&pid='+monoloop_base.pid+'&cmd=deleteFolderSave',
					// send additional parameters to instruct server script
					method: 'POST',
					timeout: '30000',
					params:   data ,
					// process the response object to add it to the TabPanel:
					success: function(xhr) {
						var jsonData = Ext.util.JSON.decode(xhr.responseText) ; 
						Ext.MessageBox.hide();
						treeObj.loader.load(treeObj.root);
						treeObj.getRootNode().expand();
						mdcBase.msg('', jsonData.msg);
					},
					failure: function() {
						Ext.MessageBox.hide(); 
					    Ext.MessageBox.alert('Error', '#312' ) ; 
					}
				});	
			} else {
				alert('Please select folder.');
			}
		}
	}
}  ; 

monoloopTreeBase.prototype.openEditFolderWindow = function(){
    var treeObj =  this.tree ;
	var data = {};
	var sm = treeObj.getSelectionModel().getSelectedNode();
	if(sm != null){
		var cls = sm.getPath('cls').split('/');
		cls = cls[cls.length-1];
		var uid = sm.getPath('id');	
		var text = sm.getPath('text').split('/');
		text = text[text.length-1];
		data[ mdcBase.firstPI + '[uid]'] = uid;
		
		if((uid != '') && (cls == 'folder')){
			var form = new Ext.FormPanel({
		        frame:true,
		        bodyStyle:'padding:5px 5px 0',
		        width: '100%',
		        defaults: {
		            allowBlank: false 
		        },
		
		        items: [ {
	                xtype:'textfield',
	                fieldLabel: 'folder name',
	                allowBlank: false  ,
	                name:  mdcBase.firstPI + '[title]',
	                //emptyText: 'Press any value',
	                anchor: '90%',
	                emptyText: text
	            }],
		
		        buttons: [{
		            text: 'Save' , 
		            handler: function(){
		                if(form.getForm().isValid()){
		                	form.getForm().submit({
			                    url: 'index.php?eID=t3p_dynamic_content_placement&pid='+monoloop_base.pid+'&cmd=editFolderSave',
			                    waitMsg: 'Saving your data...',
			                    success: function(form , o){
		                			mdcBase.msg('Success', o.result.msg);
			                        mainWindow.close() ;
			                        treeObj.loader.load(treeObj.root);
			                        treeObj.getRootNode().expand();
			                    } , 
			                    failure: function(form , o) {
			                    	mdcBase.msg('Fail',  o.result.msg);
								} ,
								params : data 
			                });
		                }
		            }
		
		        }]
		    });
			
			mainWindow = new Ext.Window({
		        title: 'Edit folder .',
		        closable:true,
		        width:350,
		        height:110,
		        //border:false,
		        plain:true , 
		        items: [form]
		    });
		    
		    mainWindow.show(this);
		} else {
			alert('Please select folder.');
		}
	}
}  ;

monoloopTreeBase.prototype.changeFolder = function(node){
    //alert(this.typeDefault) ; 
	var id = node.getPath('id').split('/');
	id = id[id.length-1];
	var cls = node.getPath('cls').split('/');
	cls = cls[cls.length-1];

	if((this.idFolder != id) && (cls == 'folder')){
		this.idFolder = id; 
        goal_grid.refrehGrid() ; 
			
	}
} ;
 