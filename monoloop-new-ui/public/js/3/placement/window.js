define(['require' ] ,function(require){
//---

/*
    Main.js
*/

Ext.ux.WindowAlwaysOnTop = function() {
    this.init = function(win) {
        win.on('deactivate', function() {
            var i = 1;
            this.manager.each(function() {
                i++;
            });
            this.setZIndex(this.manager.zseed + (i * 10));
        });
    };
};


Ext.ux.collapsedPanelTitlePlugin = function ()
{
    this.init = function(p) {
        if (p.collapsible)
        {
            var r = p.region;
            if ((r == 'north') || (r == 'south'))
            {
                p.on ('render', function()
                    {
                        var ct = p.ownerCt;
                        ct.on ('afterlayout', function()
                            {
                                if (ct.layout[r].collapsedEl)
                                {
                                    p.collapsedTitleEl = ct.layout[r].collapsedEl.createChild ({
                                        tag: 'span',
                                        cls: 'x-panel-collapsed-text',
                                        html: p.title
                                    });
                                    p.setTitle = Ext.Panel.prototype.setTitle.createSequence (function(t)
                                        {p.collapsedTitleEl.dom.innerHTML = t;});
                                }
                            }, false, {single:true});
                        p.on ('collapse', function()
                            {
                                if (ct.layout[r].collapsedEl && !p.collapsedTitleEl)
                                {
                                    p.collapsedTitleEl = ct.layout[r].collapsedEl.createChild ({
                                        tag: 'span',
                                        cls: 'x-panel-collapsed-text',
                                        html: p.title
                                    });
                                    p.setTitle = Ext.Panel.prototype.setTitle.createSequence (function(t)
                                        {p.collapsedTitleEl.dom.innerHTML = t;});
                                }
                            }, false, {single:true});
                    });
            }
        }
    };
}



window.pwMain = {
    pid : 1  ,
    defaultDomain : '' ,
    mainWindowObj : null ,
    headerPanel : null ,
    centerPanel : null ,
    popUpMenu   : null ,
    popUpmenu2 : null ,
    fistTimeOpen : 0 ,
    contentSelector : null ,
    myStore : null ,
    firstPI : 'tx_t3pdynamiccontentplacement_pi1' ,
    //------------------------
    temPath : 'http://localhost/monoloop/fileadmin/pwdemo/' ,
    testBenchPath : '' ,
    baseUrl : 'http://localhost/monoloop/',

    urlList : new Array() ,
    urlIndex : 0 ,

    //----- Select grid store
    beforeAfterStore : null  ,
    displayTypeStore :null ,

    //----- Make to support cross domain .
    crossDomain : false ,
    showStartBubble : false ,

     //----- Overwriteable Property
     disableCondition : false  ,
     experiment_id : 0 ,

    variationsID : 0 ,

    pushURL : function(){
        //alert(pwMain.urlList.length) ;
        if( pwMain.urlIndex == 0 || pwMain.urlIndex == pwMain.urlList.length -1){
            pwMain.urlList.push(Ext.getCmp('pw-url').getValue()) ;
            pwMain.urlIndex = pwMain.urlList.length -1 ;
        }else{
            pwMain.urlList = pwMain.urlList.slice(0 , pwMain.urlIndex ) ;
            pwMain.urlList.push(Ext.getCmp('pw-url').getValue()) ;
            pwMain.urlIndex = pwMain.urlList.length -1 ;
        }
    } ,

    urlMoveBack : function(){
        if( pwMain.urlIndex <= 0 ){
            pwMain.urlIndex = 0 ;
            return ;
        }
        pwMain.urlIndex-- ;
        Ext.getCmp('pw-url').setValue(pwMain.urlList[pwMain.urlIndex]) ;
        pwMain.processURL() ;
    } ,

    urlMoveForword : function(){
        if( pwMain.urlIndex >=  pwMain.urlList.length - 1){
            pwMain.urlIndex = pwMain.urlList.length - 1;
            return ;
        }
        pwMain.urlIndex++ ;
        Ext.getCmp('pw-url').setValue(pwMain.urlList[pwMain.urlIndex]) ;
        pwMain.processURL() ;
    } ,



    openMainWindow : function(loadUrl){

        // Empty Data ;
        var me = this ;
        if( pwMain.popUpMenu != null ){
            pwMain.popUpMenu.destroy() ;
        }
        if( pwMain.popUpMenu2 != null ){
            pwMain.popUpMenu2.destroy() ;
        }

        var scnWid = 0 ;
        var scnHei = 0 ;
    	if (self.innerHeight) // all except Explorer
    	{
    		scnWid = self.innerWidth;
    		scnHei = self.innerHeight;
    	}
    	else if (document.documentElement && document.documentElement.clientHeight)
    		// Explorer 6 Strict Mode
    	{
    		scnWid = document.documentElement.clientWidth;
    		scnHei = document.documentElement.clientHeight;
    	}
    	else if (document.body) // other Explorers
    	{
    		scnWid = document.body.clientWidth;
    		scnHei = document.body.clientHeight;
    	}



        pwMain.fistTimeOpen = 0 ;
        // init item inside window .
        pwMain.init() ;

        //alert(scnWid) ;
        // Normal call light box window .
        pwMain.mainWindowObj = new Ext.Window({
            width: scnWid - 50,
         	height: scnHei - 50,
            title:"Content Placement & Testing",
            plain:true ,
         	modal:true,
         	maximizable : true  ,
         	closable:true ,
            constrain : true ,
            items:[ pwMain.headerPanel , pwMain.centerPanel ,{
                    xtype: 'button' ,
                    id : 'pw-cookie-btn' ,
                    cls: 'monoloop-black-btn' ,
                    text : 'Show cookies' ,
                    style : 'position : absolute ; right : 12px ; bottom : 12px ;' ,
                   	listeners : {
                   	    'click' : function(button , e){
                             me.showCookieWindow() ;
                   	    }
                   	}
                } ]  ,
            listeners : {
                'show' : function(w){
                     if( loadUrl != ''){
                         Ext.getCmp('pw-url').setValue(loadUrl) ;
                         Ext.getCmp('pw-go-btn').fireEvent('click') ;
                         pwMain.pushURL() ;
                     }
                     me.showStartBubble = false ;
                     if( pwMain.crossDomain){
                         // Track


                         if (typeof(_gat) == 'object') {
                            _gat._getTrackerByName()._trackEvent('test placement', 'open placement');
                        }
                     }
                } ,

                'close' : function(w){
                     if( pwMain.crossDomain){
                        ml_testbubble.closeAll() ;

                        //monoloop_signup.openSignupInFancyBox() ;
                        ml_testbubble.openSignup() ;
                        //pwMain.test_signUP() ;
                        if (typeof(_gat) == 'object') {
                            _gat._getTrackerByName()._trackEvent('test placement', 'show signup');
                        }
                     }else{
                     	pwMain.onClosingWindow();
                     }
                } ,

                'resize' : function(w,width , height ){
                    //http://www.salanoa.com/monoloop_replacenew/2.html
                    pwMain.selectWindowSetHeight() ;
                    //Adjust bubble
                    pwMain.selectTabSrcChange() ;
                } ,

                'maximize' : function(w){
                    //pwMain.mainWindowObj.setSize( pwMain.mainWindowObj.getWidth() - 20 , pwMain.mainWindowObj.getHight() - 20) ;
                    //pwMain.mainWindowObj.setPositin(10 , 10) ;
                }
            }
        }) ;
        pwMain.mainWindowObj.show();

        //---
        //pwMain.mainWindowObj.maximize() ;


    } ,

    onClosingWindow : function(){
    	mdc_contentmanager.refreshContentGrid() ;
		mdc_placement_main.pageListContentRefresh() ;
    } ,

    init : function(){
        var me = this ;
        pwMain.headerPanel = new Ext.Panel({
            width:  '100%' ,
            border : false ,
            bodyBorder : false ,
            bodyStyle  : 'background-color: rgb(227,227,227); padding:10px  ;' ,
            height: 40 ,
            items: [  {
                xtype: 'compositefield',
                msgTarget : 'side',
                width : '100%' ,
                items: [{
                    xtype: 'button' ,
                    cls: 'monoloop-black-btn' ,
                   	text : '<span class="icon icon-angle-left"></span>',
                   	width : 23 ,
                   	listeners : {
                   	    'click' : function(button , e){
                   	        pwMain.urlMoveBack() ;
                   	    }
                   	}
                }, {
                    xtype: 'button' ,
                    cls: 'monoloop-black-btn' ,
                   	text : '<span class="icon icon-angle-right"></span>',
                   	width : 23 ,
                   	listeners : {
                   	    'click' : function(button , e){
                   	        pwMain.urlMoveForword()  ;
                   	    }
                   	}
                }, {
                    xtype: 'button' ,
                    cls: 'monoloop-black-btn' ,
                   	text : '<span class="icon icon-refresh"></span>',
                   	width : 23 ,
                   	listeners : {
                   	    'click' : function(button , e){
                   	        pwMain.pushURL() ;
                   	        pwMain.processURL() ;
                   	    }
                   	}
                }, {
                    xtype     : 'textfield',
                    name      : 'url',
                    id        : 'pw-url',
                    allowBlank: false ,
                    enableKeyEvents : true ,
                    width    : 500  ,
                    listeners : {
                        'keypress' : function(textField ,e){
                            if(e.keyCode == e.ENTER) {
                                pwMain.pushURL() ;
                                pwMain.processURL() ;
                            }
                        }
                    }
                },{
                    xtype: 'button' ,
                    id : 'pw-go-btn' ,
                    text : 'GO' ,
                    cls: 'monoloop-black-btn' ,
                   	listeners : {
                   	    'click' : function(button , e){
                   	        pwMain.pushURL() ;
                   	        pwMain.processURL() ;
                   	    }
                   	}
                },{
                    xtype: 'displayfield' ,
                    html : '&nbsp;' ,
                    flex : 1
                }]
            }
            ]
        }) ;

        if( pwMain.crossDomain == false){
            pwMain.myStore = new Ext.data.JsonStore({
    	        root: 'topics',
    	        totalProperty: 'totalCount',
    	        idProperty: 'uid',
    	        remoteSort: true,

    	        fields:  [
        	        {name: 'uid', type: 'int'},
      	        	{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'},
      	        	{name: 'element_id'},
      	        	{name: 'content_id', type: 'int'},
      	        	{name: 'befor_after', type: 'int'},
      	        	{name: 'sort_order', type: 'int'},
                    {name: 'hidden', type: 'int'},
                    {name: 'tmplType'} ,
      	            {name: 'header'},
      	            {name: 'display_type', type: 'int'} ,
                    {name: 'content_hidden' , type : 'int'} ,
                    {name: 'toUid', type: 'int'} ,
                    {name: 'additionJS'} ,
                    {name: 'CType'} , { name : 'conditionstr'}
      	        ],

    	        // load using script tags for cross domain, if the data in on the same domain as
    	        // this page, an HttpProxy would be better
    	        proxy: new Ext.data.HttpProxy({
    	            url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=placementmainlistmonolooppageURL'
    	        }),

                listeners : {
                    'load' : function(store , records , options){
                        if( document.getElementById('pw-select-view') == null)
                            return ;
                        /*
                        if( document.getElementById("pw-select-view").contentWindow.monoloop_select_view == undefined)
                            return ;

                        document.getElementById('pw-select-view').contentWindow.monoloop_select_view.showPreplace(records);
                        */

                        var newReacords = [] ;
						for( i = 0 ; i < records.length ; i++ ){
							newReacords.push({
								'element_id' : records[i].get('element_id')
							}) ;
				        }
				        var iframe = document.getElementById('pw-select-view') ;

                        if( mPostMessage.url == '')
                            return ;

				        iframe.contentWindow.postMessage(JSON.stringify({
					        t: "select-showPreplace" ,
					        records : newReacords
						}), mPostMessage.url);
                    }
                }
    	    });
        }else{
            pwMain.myStore = new Ext.data.JsonStore({
    	        root: 'topics',
    	        totalProperty: 'totalCount',
    	        idProperty: 'uid',
    	        remoteSort: true,

    	        fields:  [
        	        {name: 'uid', type: 'int'},
      	        	{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'},
      	        	{name: 'element_id'},
      	        	{name: 'content_id', type: 'int'},
      	        	{name: 'befor_after', type: 'int'},
      	        	{name: 'sort_order', type: 'int'},
                    {name: 'hidden', type: 'int'},
                    {name: 'tmplType'} ,
      	            {name: 'header'},
      	            {name: 'display_type', type: 'int'} ,
                    {name: 'content_hidden' , type : 'int'} ,
                    {name: 'toUid', type: 'int'} ,
                    {name: 'additionJS'} ,
                    {name: 'CType'}, { name : 'conditionstr'}
      	        ],

    	        // load using script tags for cross domain, if the data in on the same domain as
    	        // this page, an HttpProxy would be better
    	        proxy: new Ext.data.ScriptTagProxy({
    	            url: pwMain.baseUrl + 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=placementmainlistmonolooppageURL'
    	        }),

                listeners : {
                    'load' : function(store , records , options){

                    	/*
                        if( document.getElementById('pw-select-view') == null)
                            return ;

                        if( document.getElementById("pw-select-view").contentWindow.monoloop_select_view == undefined)
                            return ;
                        */
						var newReacords = [] ;
						for( i = 0 ; i < records.length ; i++ ){
							newReacords.push({
								'element_id' : records[i].get('element_id')
							}) ;
				        }
				        var iframe = document.getElementById("pw-select-view") ;
				        iframe.contentWindow.postMessage(JSON.stringify({
					        t: "select-showPreplace" ,
					        records : newReacords
						}), mPostMessage.url);

                        //document.getElementById('pw-select-view').contentWindow.monoloop_select_view.showPreplace(records);
                    }
                }
    	    });
        }




	    pwMain.myStore.setDefaultSort('crdate', 'desc');

        function renderActions(value, id, r){
            var id = Ext.id();
            createDeleteButton.defer(1, this, ['', id  , r]);
            createPropertyButton.defer(1, this, ['', id + '_2', r]) ;
            if( pwMain.disableCondition == false)
            	createConditionButton.defer(1, this, ['', id + '_3', r]) ;

            return('<table><tr><td><div id="' + id + '"></div></td><td><div id="' + id + '_2"></div></td><td><div id="' + id + '_3"></div></td></tr></table>');
        }

        function renderStatus(value, p, r){
            var hidden = '' ;
            if( r.data['hidden'] == 0 ){
				hidden = '<td><div onClick="pwMain.setPlacementHidden('+r.data['uid']+' , 1)"  class="placement-active"></div></td>' ;
			}else{
				hidden = '<td><div onClick="pwMain.setPlacementHidden('+r.data['uid']+' , 0)"   class="placement-inactive" ></div></td>' ;
			}
            	var ret =  '<table width="100%" ><tr align="center"> '
						 + hidden +
					   '</tr></table>'  ;
			return ret ;
        }

        function renderContentStatus(value, p, r){
            var hidden = '' ;
            if( r.data['content_hidden'] == 0 ){
				hidden = '<td><div onClick="pwMain.setContentHidden('+r.data['content_id']+' , 1)"  class="placement-active"></div></td>' ;
			}else{
				hidden = '<td><div onClick="pwMain.setContentHidden('+r.data['content_id']+' , 0)"   class="placement-inactive" ></div></td>' ;
			}
            	var ret =  '<table width="100%" ><tr align="center"> '
						 + hidden +
					   '</tr></table>'  ;
			return ret ;
        }

        function createDeleteButton(value, id, record) {
            new Ext.Button({
                text: value
                ,iconCls : 'pw-menu-delete-content'
                ,handler : function(btn, e) {
                    var sendData = {};
            		sendData[pwMain.firstPI + '[uid]'] = record.get('uid');
            		monoloop_base.showProgressbar('Deleting monoloop content ..' ) ;
                    if(pwMain.crossDomain){
                        sendData['eID'] = 't3p_dynamic_content_placement' ;
                        sendData['pid'] = pwMain.pid ;
                        sendData['cmd'] = 'deleteMonoloopContent' ;
                        sendData['time'] = new Date().getTime();
                        Ext.util.JSONP.request({
                            url: pwMain.baseUrl + 'index.php',
                            callbackKey: 'callback',
                            params : sendData ,
                            callback: function(result) {
                                Ext.MessageBox.hide();
                				pwMain.reStartContentGrid() ;
                				monoloop_base.msg('Success' , 'Monoloop content has been removed.')
                            }
                        });
                    }else{
                        Ext.Ajax.request({
                			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=deleteMonoloopContent' ,
                			// send additional parameters to instruct server script
                			method: 'POST',
                			timeout: '30000',
                			params:   sendData ,
                			// process the response object to add it to the TabPanel:
                			success: function(xhr) {
                				Ext.MessageBox.hide();
                				pwMain.reStartContentGrid() ;
                				monoloop_base.msg('Success' , 'Monoloop content has been removed.')
                			},
                			failure: function() {
                				Ext.MessageBox.hide();
    					        Ext.MessageBox.alert('Error', '#324' ) ;
                			}
                		});
                    }

                }
            }).render(document.body, id);
        }

        function createPropertyButton(value, id, record) {
            new Ext.Button({
                text: value
                ,iconCls : 'pw-menu-property'
                ,handler : function(btn, e) {
                   if ( record.get('CType') == 'goal'){
                        goal_placement.openContentEditWindow(record.get('content_id') ) ;
                        return ;
                   }
                   if ( record.get('toUid') == 0 ){
                        variation.additionalRecord = record ;
                        variation.openNewWindowWithPreload(record.get('content_id') , 3) ;
                   }else{
                        pwContentEditor.additionalRecord = record ;
                        pwContentEditor.elementName = record.get('header') ;
                        pwContentEditor.openMainWindow(record.get('toUid') , record.get('content_id') , 3) ;
                   }
                }
            }).render(document.body, id);
        }

        function createConditionButton(value , id , record ){
        	new Ext.Button({
                text: value
                ,width : 30
                ,iconCls : 'pw-condition-property'
                ,handler : function(btn, e) {
                 	condition.stage = 32 ;
                    condition.contentId = record.get('content_id') ;
                    condition.preconditionStr = record.get('conditionstr') ;
                    condition.openMainWindow() ;
                }
            }).render(document.body, id);
        }

        function renderDisplayType(value, p, r){
    		var ret;
    		switch(r.data['display_type']){
    			case 0:
    				ret = 'Iframe';
    			break;
    			case 1:
    				ret = 'Direct';
    			break;
    		}
    		return ret ;
    	}

        function renderBeforeAfter(value, p, r){
    		var ret;
    		switch(r.data['befor_after']){
    			case 0:
    				ret = 'Before';
    			break;
    			case 1:
    				ret = 'After';
    			break;
    			case 2:
    				ret = 'Inside Before';
    			break;
    			case 3:
    				ret = 'Inside After';
    			break;
    			case 4:
    				ret = 'Replace';
    			break;
    		}

    		if( ret == 'Replace' && r.data['tmplType'] == 'Remove' ){
    			ret = 'Remove' ;
    		}
    		return ret ;
    	}

        var displayStore = new Ext.data.JsonStore({
        fields : ['name',  {name: 'value', type: 'int'}],
            data   : [
                {name : 'Iframe',   value: 0 },
                {name : 'Direct',   value: 1 }
            ]
        }) ;


        pwMain.displayTypeStore = displayStore ;

        var beforeAfterStore = new Ext.data.JsonStore({
        fields : ['name',  {name: 'value', type: 'int'}],
            data   : [
                {name : 'Before',   value: 0 },
                {name : 'After',   value: 1 },
                {name : 'Inside Before',   value: 2 },
                {name : 'Inside After',   value: 3 },
                {name : 'Replace',   value: 4 }
            ]
        }) ;

        pwMain.beforeAfterStore = beforeAfterStore ;

	    var myGrid = new Ext.grid.GridPanel({
	        width: '100%',
	        height: 100,
            id : 'placementdetail-grid' ,
	        border : false ,
	        store:  pwMain.myStore,
	        trackMouseOver:false,
	        //disableSelection:false,
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
            ctCls : 'contentGrid' ,
            getFirstSelectedCondition : function(){
            	var ret = '' ;
            	this.getSelectionModel().each(function(item , index){
            		if( index == 0 )
            			ret = item.get('conditionstr') ;
		    	}) ;
		    	return ret ;
            } ,
            getSelectedRow : function(){
		    	var query = '' ;
		    	this.getSelectionModel().each(function(item , index){
		    		//console.debug(item) ;
		    		if( index == 0){
		   				query = item.get('content_id') ;
		    		}else{
		    			query = query + ',' + item.get('content_id') ;
		    		}
		    	}) ;
		    	return query
		    } ,
		    getSelectedDetailRow : function(){
		    	var query = '' ;
		    	this.getSelectionModel().each(function(item , index){
		    		//console.debug(item) ;
		    		if( index == 0){
		   				query = item.get('uid') ;
		    		}else{
		    			query = query + ',' + item.get('uid') ;
		    		}
		    	}) ;
		    	return query
		    } ,
            popup : new Ext.menu.Menu({
            	items: [{
            		text : 'Set Condition'	,
            		handler : function(){
            			var grid = Ext.getCmp('placementdetail-grid') ;
            			var uids = grid.getSelectedRow() ;
            			var conditionstr = grid.getFirstSelectedCondition() ;
            			condition.stage = 33 ;
	                    condition.contentId = uids ;
	                    condition.preconditionStr = conditionstr ;
	                    condition.openMainWindow() ;
            		}
           		},{
           			text : 'Set Active' ,
           			handler : function(){
           				var grid = Ext.getCmp('placementdetail-grid') ;
            			var uids = grid.getSelectedDetailRow() ;
            			grid.saveMultipleHidden( uids , 0) ;
           			}
           		},{
           			text : 'Set Inactive' ,
           			handler : function(){
           				var grid = Ext.getCmp('placementdetail-grid') ;
            			var uids = grid.getSelectedDetailRow() ;
            			grid.saveMultipleHidden( uids , 1) ;
           			}
           		},{
           			text : 'Delete' ,
           			handler : function(){
           				var grid = Ext.getCmp('placementdetail-grid') ;
            			var uids = grid.getSelectedDetailRow() ;
           				grid.deleteMultipleDetail(uids) ;
           			}
           		}]
           	}),

           	deleteMultipleDetail : function(uids ){
           		var sendData = {};
	    		sendData['uids'] = uids ;
	    		monoloop_base.showProgressbar('Deleting monoloop content ..' ) ;
	            if(pwMain.crossDomain){
	                sendData['eID'] = 't3p_dynamic_content_placement' ;
	                sendData['pid'] = pwMain.pid ;
	                sendData['cmd'] = 'contentplacement/deleteMultipleDetail' ;
	                sendData['time'] = new Date().getTime();
	                Ext.util.JSONP.request({
	                    url: pwMain.baseUrl + 'index.php',
	                    callbackKey: 'callback',
	                    params : sendData ,
	                    callback: function(result) {
	                        Ext.MessageBox.hide();
	        				pwMain.reStartContentGrid() ;
	        				monoloop_base.msg('Success' , 'Monoloop content has been removed.')
	                    }
	                });
	            }else{
	                Ext.Ajax.request({
	        			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentplacement/deleteMultipleDetail' ,
	        			// send additional parameters to instruct server script
	        			method: 'POST',
	        			timeout: '30000',
	        			params:   sendData ,
	        			// process the response object to add it to the TabPanel:
	        			success: function(xhr) {
	        				Ext.MessageBox.hide();
	        				pwMain.reStartContentGrid() ;
	        				monoloop_base.msg('Success' , 'Monoloop content has been removed.')
	        			},
	        			failure: function() {
	        				Ext.MessageBox.hide();
					        Ext.MessageBox.alert('Error', '#324' ) ;
	        			}
	        		});
	            }
           	} ,

           	saveMultipleHidden : function(uids , hidden ){
           		var data = {} ;
		        data['uids'] = uids ;
		        data['hidden'] = hidden ;
		        monoloop_base.showProgressbar('Updating data...'  ) ;
		        if(pwMain.crossDomain){
		            sendData['eID'] = 't3p_dynamic_content_placement' ;
		            sendData['pid'] = pwMain.pid ;
		            sendData['cmd'] = 'contentplacement/setMultipleDetailHidden' ;
		            sendData['time'] = new Date().getTime();
		            Ext.util.JSONP.request({
		                url: pwMain.baseUrl + 'index.php',
		                callbackKey: 'callback',
		                params : data ,
		                callback: function(result) {
		                    Ext.MessageBox.hide();
		    				pwMain.reStartContentGrid() ;
		    				monoloop_base.msg('Success' , 'Monoloop content has been updated.') ;
		                }
		            });
		        }else{
		            Ext.Ajax.request({
		    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentplacement/setMultipleDetailHidden' ,
		    			// send additional parameters to instruct server script
		    			method: 'POST',
		    			timeout: '30000',
		    			params:   data ,
		    			// process the response object to add it to the TabPanel:
		    			success: function(xhr) {
		    				Ext.MessageBox.hide();
		    				pwMain.reStartContentGrid() ;
		    				monoloop_base.msg('Success' , 'Monoloop content has been updated.') ;
		    			},
		    			failure: function() {
		    				Ext.MessageBox.hide();
		    		        Ext.MessageBox.alert('Error', '#324' ) ;
		    			}
		    		});
		        }
           	} ,

           	saveMultipleCondition : function(uids , conditionStr){
           		var data = {} ;
				mdcBase.showProcessBox('Saving Data...' , 'Saving... ') ;
		        data['uids'] = uids ;
		        data['condition'] = conditionStr ;
		        if( pwMain.crossDomain){
		            data['eID'] = 't3p_dynamic_content_placement' ;
		            data['pid'] = pwMain.pid ;
		            data['cmd'] = 'contentmanager/saveMultipleCondition' ;
		            data['time'] = new Date().getTime();
		            Ext.util.JSONP.request({
		                url: pwMain.baseUrl + 'index.php',
		                callbackKey: 'callback',
		                params : data ,
		                callback: function(jsonData) {
		                	Ext.MessageBox.hide();
							pwMain.reStartContentGrid() ;
		                	monoloop_base.msg('Success', 'Condition updated') ;
		                }
		            });
		        }else{
		            Ext.Ajax.request({
		    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentmanager/saveMultipleCondition'  ,
		                //url : 'testxml.php' ,
		    			method: 'POST',
		    			params:   data ,
		    			timeout: '30000',
		    			// process the response object to add it to the TabPanel:
		    			success: function(xhr) {
		    				Ext.MessageBox.hide();
		    				pwMain.reStartContentGrid() ;
		    				monoloop_base.msg('Success', 'Condition updated') ;
		    			},
		    			failure: function() {
		    				Ext.MessageBox.hide();
							Ext.MessageBox.alert('Error', '#323' ) ;
		    			}
		    		});
		        }
           	} ,

			saveCondition : function(contentId , conditionStr){
				var data = {} ;
				mdcBase.showProcessBox('Saving Data...' , 'Saving... ') ;
		        data['uid'] = contentId ;
		        data['condition'] = conditionStr ;
		        if( pwMain.crossDomain){
		            data['eID'] = 't3p_dynamic_content_placement' ;
		            data['pid'] = pwMain.pid ;
		            data['cmd'] = 'contentmanager/saveCondition' ;
		            data['time'] = new Date().getTime();
		            Ext.util.JSONP.request({
		                url: pwMain.baseUrl + 'index.php',
		                callbackKey: 'callback',
		                params : data ,
		                callback: function(jsonData) {
		                	Ext.MessageBox.hide();
							pwMain.reStartContentGrid() ;
		                	monoloop_base.msg('Success', 'Condition updated') ;
		                }
		            });
		        }else{
		            Ext.Ajax.request({
		    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentmanager/saveCondition'  ,
		                //url : 'testxml.php' ,
		    			method: 'POST',
		    			params:   data ,
		    			timeout: '30000',
		    			// process the response object to add it to the TabPanel:
		    			success: function(xhr) {
		    				Ext.MessageBox.hide();
		    				pwMain.reStartContentGrid() ;
		    				monoloop_base.msg('Success', 'Condition updated') ;
		    			},
		    			failure: function() {
		    				Ext.MessageBox.hide();
							Ext.MessageBox.alert('Error', '#323' ) ;
		    			}
		    		});
		        }
			} ,
	        // grid columns
	        columns:[{
	            header: "Element ID",
	            width: 150,
	            dataIndex: 'element_id',
	            id : 'placementdetail-grid-element-id' ,
	            sortable: true,
                hidden : true ,
	            editor: {
	        		xtype: 'textfield',
	        		readOnly: true
	        	}
	        },{
	            header: "Name",
	            id : 'placementdetail-grid-name' ,
	            dataIndex: 'header',
	            width: 80,
	            sortable: true
	        },{
	            header: "Type",
	            id : 'placementdetail-grid-type' ,
	            dataIndex: 'tmplType',
	            width: 80,
	            sortable: true
	        },{
	            header: "Create",
	            id : 'placementdetail-grid-crdate' ,
	            dataIndex: 'crdate',
	            renderer: Ext.util.Format.dateRenderer('d M Y'),
	            width: 70,
	            sortable: true
	        },{
	            header: "Position",
	            id : 'placementdetail-grid-position' ,
	            dataIndex: 'befor_after',
	            width: 90,
	            sortable: true ,
                editor: new Ext.form.ComboBox({
                    typeAhead: true,
                    triggerAction: 'all' ,
                    mode: 'local',
                    allowBlank: true  ,
                    editable: false,
                    displayField:   'name',
                    valueField:     'value' ,
                    store : beforeAfterStore
                }),
                renderer : renderBeforeAfter
	        },{
	            header: "Order",
	            id : 'placementdetail-grid-order' ,
	            dataIndex: 'sort_order',
	            editor: { xtype: 'numberfield' },
	            width: 50,
	            sortable: true
	        },{
	            header: "Type",
	            id : 'placementdetail-grid-display-type' ,
	            dataIndex: 'display_type',
	            width: 60,
	            sortable: true ,
                editor: new Ext.form.ComboBox({
                    typeAhead: true,
                    triggerAction: 'all' ,
                    mode: 'local',
                    allowBlank: true  ,
                    editable: false,
                    displayField:   'name',
                    valueField:     'value' ,
                    store : displayStore
                }),
                renderer : renderDisplayType
	        },{
	            header: "Action",
	            id : 'placementdetail-grid-action' ,
	            dataIndex: 'uid',
	            width: 50,
                renderer: renderActions,
	            sortable: false
	        },{
	            header: "Status",
	            id : 'placementdetail-grid-status' ,
	            dataIndex: 'hidden',
	            width: 50,
                renderer: renderStatus,
	            sortable: false
	        },{
	            header: "Content Status",
	            id : 'placementdetail-grid-content-status' ,
	            dataIndex: 'content_hidden',
	            width: 50,
                renderer: renderContentStatus,
	            sortable: false
	        }],

	        // customize view config
	        viewConfig: {
	            forceFit:true
	        } ,
            listeners : {
                /*
                'rowdblclick' : function(grid , rowIndex , e ){
                    var record = pwMain.myStore.getAt(rowIndex);
                    //alert(record.get('toUid')) ;
                    pwContentEditor.openMainWindow(record.get('toUid') , record.get('content_id') , 0) ;
                }*/
                'afteredit' : function(e){
                    if( e.field == 'sort_order' || e.field == 'element_id' ){
                        return ; // no need ;
                    }
                    var sendData = {};
                    sendData[ pwMain.firstPI + '[uid]' ] = e.record.get('uid');
	    			sendData[ pwMain.firstPI + '[field]'] = e.field;
	    			sendData[ pwMain.firstPI + '[value]' ] = e.value;

	    			Ext.Ajax.request({
	    				url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=editMonoloopContent' ,
	    				// send additional parameters to instruct server script
	    				method: 'POST',
	    				params:   sendData ,
	    				timeout: '30000',
	    				// process the response object to add it to the TabPanel:
	    				success: function(xhr) {
	    				   monoloop_base.msg('Success' , 'Updated monoloop .')
	    				},
	    				failure: function() {
	    				   Ext.MessageBox.hide();
					       Ext.MessageBox.alert('Error', '#325' ) ;
	    				}
	    			});
                } ,
                'rowcontextmenu' : function( grid , rowIndex , e){
	         		e.stopEvent()
					grid.popup.showAt(e.xy);
	        	}
            }

	    });




        pwMain.centerPanel = new Ext.TabPanel({
            activeTab: 0,
            width: '100%',
            tabPosition : 'bottom' ,
            height:420,
            plain:true,
            defaults:{autoScroll: true},
            items:[{
                    title: 'Browse',
                    html: "<iframe id='pw-browse' name='pw-browse' src ='' width='100%' height='100%' onLoad=\"pwMain.browserSrcChange();\" frameborder='0'><p>Your browser does not support iframes.</p></iframe>" ,
                   	listeners : {
                   	    'click' : function(button , e){
                   	        alert('panel click') ;
                   	    }
                   	}
                },{
                    title: 'Select',
                    id : 'pw-tab-select' ,
                    autoScroll : false ,
                    items : [{
                        xtype: 'panel' ,
                        width: '100%'  ,
                        title: 'Content Element on Page',
                        collapsible : true ,
                        height: 130 ,
                        collapsed : true ,
                        region : 'north' ,
                        id : 'pw-ce-panel' ,
                        items : [myGrid] ,
                        listeners : {
                            'collapse' : function(panel){
                                // Test show menu ;
                                Ext.getCmp('pw-select-window').setHeight(pwMain.centerPanel.getHeight() - Ext.getCmp('pw-ce-panel').getHeight() - 10 );

                            } ,
                            'expand' : function(panel){
                                Ext.getCmp('pw-select-window').setHeight(pwMain.centerPanel.getHeight() - Ext.getCmp('pw-ce-panel').getHeight() - 10 );
                            }
                        }
                    },{
                        xtype: 'panel' ,
                        width: '100%'  ,
                        title: 'Result',
                        autoScroll : true ,
                        collapsible : false ,
                        region : 'center' ,
                        id : 'pw-select-window' ,
                        html : "<iframe id='pw-select-view' name='pw-select-view' src ='' width='100%' height='100%' onLoad=\"pwMain.selectTabSrcChange();\" frameborder='0'><p>Your browser does not support iframes.</p></iframe>"
                    }]
                },{
                    title: 'Test',
                    id : 'pw-tab-test' ,
                    layout: 'border' ,
                    items : [
                    {
                        region: 'west',
            			title: 'Test Bench',
            	        split: true,
            	        width: 220,
            			margins:'0',
            	        cmargins:'0',
            	        collapsible: true ,
                        autoScroll : false ,
                        layout : 'border' ,
                        items : [{
                            id : 'testbench-field' ,
                            region: 'center',
                            autoScroll : true ,
                            header : false ,
                            width : '100%'
                        },{
                            id : 'testbench-static-link' ,
                            region : 'south' ,
                            title : 'Static Link' ,
                            collapsed : true ,
                            collapsible: true ,
                            autoScroll : false ,
                            floatable: false ,
                            width : '100%' ,
                            plugins: new Ext.ux.collapsedPanelTitlePlugin () ,
                            height : 100 ,
                            bodyStyle : 'padding-left : 10px ;padding-right : 10px ;' ,
                            titleCollapse : false ,
                            listeners : {
                                'expand' : function(p){
                                    testbenchTab.getStaticLink() ;
                                }
                            }
                        }]
                    },
                    {
                        region: 'center',
            	        margins:'3 3 3 0',
            	        title: 'Result',
                        width: '100%'  ,
                        html : "<iframe id='pw-test-view' name='pw-test-view' src ='' width='100%' height='100%' onLoad=\"testbenchTab.selectTabSrcChange();\" frameborder='0'><p>Your browser does not support iframes.</p></iframe>"
                    }
                    ]
                }
            ] ,

            listeners : {
                'tabchange' : function(tabPanel , Panel){
                    if (Panel.getId() == 'pw-tab-select' ){
                        //
                        selectTab.showTab() ;


                        //pwMain.myStore.load( );
                    }else if(Panel.getId() == 'pw-tab-test' ){
                        testbenchTab.showTab() ;

                    }

                    if( pwMain.crossDomain  ){
                        ml_testbubble.closeAll() ;
                    }

                    pwMain.selectWindowSetHeight() ;
                }
            }
        });

        testbenchTab.staticLinkPanel() ;

        pwMain.popUpmenu2 = new Ext.menu.Menu({
        	id : 'mainMenu2' ,
        	ignoreParentClicks : true ,
        	items : [/*{
        		text : '<b>Replace</b> selected' ,
        		handler : function(){
        			multiselected.show(0 , me.getIframeMultiSelectedData() ) ;
        		}
        	},*/{
        		text : '<b>Remove</b> selected' ,
        		handler : function(){
        			multiselected.show(1 , me.getIframeMultiSelectedData() ) ;
        		}
        	}]
        }) ;

        pwMain.popUpMenu = new Ext.menu.Menu({
            id: 'mainMenu',
            ignoreParentClicks : true ,

            items: [{
                text: '<b>Add</b> new content&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>',
                id : 'menu_1' ,
                menu : {

                    items : [{
                        text: 'Before' ,
                        id  : 'menu_1_1'
                    },{
                        text: 'After' ,
                        id  : 'menu_1_2'
                    },{
                        text: 'Inside Before' ,
                        id  : 'menu_1_3'
                    },{
                        text: 'Inside After' ,
                        id  : 'menu_1_4'
                    },{
                    	text: 'Replace' ,
                    	id : 'menu_1_5'
                    } ]  ,
                    listeners : {
                        'click' : function(menu , menuItem , e){
                            var menuID = menuItem.getId()  ;
                            var beforeAfter = 0 ;
                            switch(menuID){
                    			case 'menu_1_1':
                    				beforeAfter = 0;
                    			break;
                    			case 'menu_1_2':
                    				beforeAfter = 1;
                    			break;
                    			case 'menu_1_3':
                    				beforeAfter = 2;
                    			break;
                    			case 'menu_1_4':
                    				beforeAfter = 3;
                    			break;
                    			case 'menu_1_5':
                    				beforeAfter = 4 ;
                    			break;
                    		}
                            selectTab.beforeAfter = beforeAfter ;
                            selectTab.menuClick = true ;
                            selectTab.showContentElementSelectorWindow(1,1) ;
                        }
                    }
                }
            },{
                text: '<b>Add</b> pre-defined content&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>',
                id : 'menu_2' ,
                menu : {
                    items : [{
                        text: 'Before' ,
                        id  : 'menu_2_1'
                    },{
                        text: 'After' ,
                        id  : 'menu_2_2'
                    },{
                        text: 'Inside Before' ,
                        id  : 'menu_2_3'
                    },{
                        text: 'Inside After' ,
                        id  : 'menu_2_4'
                    },{
                    	text: 'Replace' ,
                    	id : 'menu_2_5'
                    }] ,
                    listeners : {
                        'click' : function(menu , menuItem , e){
                            var menuID = menuItem.getId()  ;
                            var beforeAfter = 0 ;
                            switch(menuID){
                    			case 'menu_2_1':
                    				beforeAfter = 0;
                    			break;
                    			case 'menu_2_2':
                    				beforeAfter = 1;
                    			break;
                    			case 'menu_2_3':
                    				beforeAfter = 2;
                    			break;
                    			case 'menu_2_4':
                    				beforeAfter = 3;
                    			break;
                    			case 'menu_2_5':
                    				beforeAfter = 4;
                    			break;
                    		}
                            selectTab.beforeAfter = beforeAfter ;
                            selectTab.menuClick = true ;
                            selectTab.showContentElementSelectorWindow(2,1) ;
                        }
                    }
                }
            }/*,{
                text: '<b>Replace</b> this content&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>' ,
                id : 'menu_3' ,
                menu : {
                    items : [{
                        text: '<b>With</b> new content' ,
                        id  : 'menu_3_1'
                    },{
                        text: '<b>With</b> pre-defined content' ,
                        id  : 'menu_3_2'
                    }  ] ,
                    listeners : {
                        'click' : function(menu , menuItem , e){
                            //alert( menuItem.getId() ) ;
                            selectTab.menuClick = true ;
                            selectTab.beforeAfter = 4 ;
                            var menuID = menuItem.getId()  ;
                            var typeID = 0 ;
                            if(menuID == 'menu_3_1'){
                                typeID = 1 ;
                            }else{
                                typeID = 2 ;
                            }
                            selectTab.showContentElementSelectorWindow(typeID,1) ;
                        }
                    }
                }
            }*/,{
                text: '<b>Add variation</b> of content' ,
                id : 'menu_4'
            },{
                text: '<b>Add variations</b> of content' ,
                id : 'menu_8'
            },{
            	text: '<b>Remove</b> content' ,
            	id : 'menu_9'
            },'-',{
                text: '<b>Add page</b> content&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;>' ,
                id : 'menu_6' ,
                menu : {
                    items : [{
                        text: '<b>With</b> new content' ,
                        id  : 'menu_6_1'
                    },{
                        text: '<b>With</b> pre-defined content' ,
                        id  : 'menu_6_2'
                    }  ] ,
                    listeners : {
                        'click' : function(menu , menuItem , e){
                            var menuID = menuItem.getId()  ;
                            var beforeAfter = 0 ;
                            switch(menuID){
                    			case 'menu_6_1':
                    				beforeAfter = 3;
                                    pwMain.contentSelector = 'HTML BODY' ;
                                    selectTab.beforeAfter = beforeAfter ;
                                    selectTab.menuClick = true ;
                                    selectTab.showContentElementSelectorWindow(1 , 2) ;
                    			break;
                    			case 'menu_6_2':
                    				beforeAfter = 3;
                                    pwMain.contentSelector = 'HTML BODY' ;
                                    selectTab.beforeAfter = beforeAfter ;
                                    selectTab.menuClick = true ;
                                    selectTab.showContentElementSelectorWindow(2,2) ;
                    			break;
                    		}

                        }
                    }
                }
            },{
                text: '<b>Add</b> Goal' ,
                id : 'menu_7'
            },'-',{
                text: '<b>Edit/View</b> pre-placed content'  ,
                id : 'menu_5' ,
                disabled : true
            }]  ,
            listeners : {
                'show' : function(cmp){
                    if( pwMain.crossDomain){
                        var pPos = cmp.getPosition() ;
                        var pWidth = cmp.getWidth() ;
                        var mPos = pwMain.mainWindowObj.getPosition() ;
                        var mWidth = pwMain.mainWindowObj.getWidth() ;
                        //console.debug(pPos[0] + pWidth) ;
                        //console.debug(mPos[0] + mWidth) ;
                        if( pPos[0] + pWidth + 400 <= mPos[0] + mWidth)
                            ml_testbubble.open3(cmp) ;
                        else
                            ml_testbubble.open15(cmp) ;
                    }
                } ,

                'hide' : function(cmp){
                    if( pwMain.crossDomain){
                        if( Ext.getCmp('test_bubble_3') != undefined){
                            Ext.getCmp('test_bubble_3').close() ;
                        }
                        if( Ext.getCmp('test_bubble_15') != undefined){
                            Ext.getCmp('test_bubble_15').close() ;
                        }
                    }
                    if( selectTab.menuClick == false ){
                        //pwMain.clearIframeSelection() ;
                    }
                } ,

                'click' : function(menu , menuItem , e){

                    var menuID = menuItem.getId()  ;
                    if(  menuID == 'menu_4'){
                        selectTab.menuClick = true ;
                        selectTab.beforeAfter = 4 ;
                        selectTab.showContentElementSelectorWindow(3,1) ;
                    }else if( menuID == 'menu_8'){
                    	monoloop_base.showProgressbar('Loading ..' ) ;
                    	selectTab.menuClick = true ;
                    	pwMain.selectorTxt = pwMain.contentSelector;
                    	selectTab.beforeAfter = 4 ;
                    	var sendData = {} ;
                    	if(me.variationsID == 0){
			                if(pwMain.crossDomain){
			                    sendData['eID'] = 't3p_dynamic_content_placement' ;
			                    sendData['pid'] = pwMain.pid ;
			                    sendData['cmd'] = 'contentmanager/getDynamicContainerID' ;
			                    sendData['time'] = new Date().getTime();
			                    Ext.util.JSONP.request({
			                        url: pwMain.baseUrl + 'index.php',
			                        callbackKey: 'callback',
			                        params : sendData ,
			                        callback: function(result) {
			                            Ext.MessageBox.hide();
			                            me.variationsID = result.uid ;
			                            pwContentEditor.openMainWindow( me.variationsID  , 0 , 0 ) ;
			                        }
			                    });
			                }else{
			                    Ext.Ajax.request({
			            			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentmanager/getDynamicContainerID' ,
			            			// send additional parameters to instruct server script
			            			method: 'POST',
			            			timeout: '30000',
			            			params:   sendData ,
			            			// process the response object to add it to the TabPanel:
			            			success: function(xhr) {
			            				Ext.MessageBox.hide();
			            				var data = Ext.util.JSON.decode(xhr.responseText) ;
			            				me.variationsID = data.uid ;
			            				pwContentEditor.openMainWindow( me.variationsID  , 0 , 0 ) ;
			            			},
			            			failure: function() {

			            				Ext.MessageBox.hide();
								        Ext.MessageBox.alert('Error', '#324' ) ;
			            			}
			            		});
			                }
	                	}else{
	                		pwContentEditor.openMainWindow( me.variationsID  , 0 , 0 ) ;
	                	}

                    }else if( menuID == 'menu_5'){
                        if( ! Ext.getCmp('menu_5').disabled){
                            selectTab.menuClick = true ;
                            preplaced.openWindow() ;
                        }
                    }else if( menuID == 'menu_7'){
                        selectTab.menuClick = true ;
                        goal_placement.openContentEditWindow(0) ;
                    }else if( menuID == 'menu_9'){
                    	/*
                    	selectTab.menuClick = true ;
                        selectTab.beforeAfter = 4 ;
                        selectTab.showContentElementSelectorWindow(6,1) ;
                        */
                        // Change to remove element.
                        selectTab.beforeAfter = 4 ;
                        selectTab.menuClick = true ;
                        selectTab.showContentElementSelectorWindow(7,1) ;
                    }
                }
            }
        }) ;

        if(pwMain.crossDomain){
            Ext.getCmp('menu_1').setDisabled(true) ;
            Ext.getCmp('menu_2').setDisabled(true) ;
            //Ext.getCmp('menu_3').setDisabled(true) ;
            Ext.getCmp('menu_6').setDisabled(true) ;
            Ext.getCmp('menu_7').setDisabled(true) ;
        }else{
            Ext.getCmp('menu_1').setDisabled(false) ;
            Ext.getCmp('menu_2').setDisabled(false) ;
            //Ext.getCmp('menu_3').setDisabled(false) ;
            Ext.getCmp('menu_6').setDisabled(false) ;
            Ext.getCmp('menu_7').setDisabled(false) ;
        }
    } ,

    selectWindowSetHeight : function(){
        var height = pwMain.mainWindowObj.getHeight() - 77 ;
        pwMain.centerPanel.setHeight(height) ;
        //alert( Ext.getCmp('pw-ce-panel').getFrameHeight() ) ;
        Ext.getCmp('pw-select-window').setHeight(pwMain.centerPanel.getHeight() -  10  );
    } ,

    selectTabSrcChange : function(){
        if( document.getElementById('pw-select-view') == null)
            return ;
        /*
        if( document.getElementById("pw-select-view").contentWindow.monoloop_select_view == undefined)
            return ;
        */
        if( pwMain.myStore == null)
            return ;

        monoloopLog.log('placement : select tab onload iframe');



        if( pwMain.baseUrl != document.getElementById('pw-select-view').src && mPostMessage.enable == false ){
            // Change to proxy mode ;
            // Wait 1 second to swith ;
            setTimeout(function(){
            	if(mPostMessage.enable == false){
            		monoloopLog.log('placement : select tab change to proxy mode');
            		var url = document.getElementById('pw-select-view').src ;
            		if( document.getElementById('pw-select-view') != null){
		                document.getElementById('pw-select-view').src = pwMain.temPath + 'selectView.php?l='+pwMain.urlencode(url) ;
		                mPostMessage.enable = true ;
		                mPostMessage.showProxyWarning() ;
						mPostMessage.addProxyList(url) ;
		            }
		            monoloopLog.log('placement : add domain to proxy list') ;

            	}
			}, 1500);

            return ;
         }
        //document.getElementById('pw-select-view').contentWindow.monoloop_select_view.showPreplace(pwMain.myStore.getRange(0 ,pwMain.myStore.getTotalCount() - 1 ));
        if( pwMain.crossDomain){
            ml_testbubble.open2() ;
        }
    } ,

    setPlacementHidden : function(uid , hidden){
        var sendData = {};
		sendData[pwMain.firstPI + '[uid]'] = uid;
        sendData[pwMain.firstPI + '[hidden]'] = hidden;
		monoloop_base.showProgressbar('Update monoloop content ..' ) ;
        if(pwMain.crossDomain){
            sendData['eID'] = 't3p_dynamic_content_placement' ;
            sendData['pid'] = pwMain.pid ;
            sendData['cmd'] = 'contentplacement/setDetailHidden' ;
            sendData['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : sendData ,
                callback: function(result) {
                    Ext.MessageBox.hide();
    				pwMain.reStartContentGrid() ;
    				monoloop_base.msg('Success' , 'Monoloop content has been updated.') ;
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentplacement/setDetailHidden' ,
    			// send additional parameters to instruct server script
    			method: 'POST',
    			timeout: '30000',
    			params:   sendData ,
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
    				Ext.MessageBox.hide();
    				pwMain.reStartContentGrid() ;
    				monoloop_base.msg('Success' , 'Monoloop content has been updated.') ;
    			},
    			failure: function() {
    				Ext.MessageBox.hide();
    		        Ext.MessageBox.alert('Error', '#324' ) ;
    			}
    		});
        }

    } ,

    setContentHidden : function(contentID , hidden){
       var data = {} ;
        data[ mdcBase.firstPI + '[uid]'] = contentID ;
        data[ mdcBase.firstPI + '[hidden]'] = hidden ;
        monoloop_base.showProgressbar('Updating data...'  ) ;
        if(pwMain.crossDomain){
            data['eID'] = 't3p_dynamic_content_placement' ;
            data['pid'] = pwMain.pid ;
            data['cmd'] = 'contentmanager/setHidden' ;
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data ,
                callback: function(result) {
                    Ext.MessageBox.hide();
                    pwMain.reStartContentGrid() ;
    				mdc_contentmanager.refreshContentGrid() ;
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentmanager/setHidden'  ,
    			// send additional parameters to instruct server script
    			method: 'POST',
    			timeout: '30000',
    			params:   data ,
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
    				Ext.MessageBox.hide();
                    pwMain.reStartContentGrid() ;
    				mdc_contentmanager.refreshContentGrid() ;
    			},
    			failure: function() {
    				Ext.MessageBox.hide();
    				Ext.MessageBox.alert('Error', '#309' ) ;
    			}
            });
        }

    } ,

    getExperimentID : function(){
    	return 0 ;
    } ,

    reStartContentGrid : function(){
        var me = this ;
        pwMain.myStore.baseParams ={
            'url' : Ext.getCmp('pw-url').getValue() ,
            'experiment_id' : me.getExperimentID()
        } ;
        pwMain.myStore.load() ;
    } ,

    clearIframeSelection : function(){
    	/*
        if( document.getElementById('pw-select-view') == null)
            return ;
        document.getElementById('pw-select-view').contentWindow.monoloop_select_view.clearSelection();
        */

        var iframe = document.getElementById('pw-select-view')  ;
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-clearSelection"
		}), mPostMessage.url);
    } ,

    getIframeSelectContent : function(){
    	/*
        if( document.getElementById('pw-select-view') == null)
            return ;
        return document.getElementById('pw-select-view').contentWindow.monoloop_select_view.getSelectContent();
        */
 		var iframe = document.getElementById('pw-select-view') ;
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-getSelectContent"
		}), mPostMessage.url);
    } ,

	getSelectContentRet : function(content){
		variation.contentHtml = content;
		variation.openMainWindow(0) ;
	} ,

    getIframeMultiSelectedData : function(){
    	if( document.getElementById('pw-select-view') == null)
            return ;
        //return document.getElementById('pw-select-view').contentWindow.monoloop_select_view.getSelectedData();
    	//getSelectedData
    	var iframe = document.getElementById('pw-select-view') ;
    	//console.log('placement : send get select data ');
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-getSelectedData"
		}), mPostMessage.url);
    } ,

    bubbleClick : function(selector){

        pwMain.contentSelector = selector ;
        selectTab.menuClick = true ;
        Ext.getCmp('mainMenu').hide() ;
        Ext.getCmp('mainMenu2').hide() ;
        preplaced.openWindow() ;
    } ,

    showMenu1 : function(pageX , pageY , pageOffsetX , pageOffsetY  , selector){
        pwMain.contentSelector = selector ;
        var winDowPos =  Ext.getCmp('pw-select-window').getPosition() ;
        //console.debug(winDowPos) ;
        Ext.getCmp('mainMenu').showAt([pageX+ winDowPos[0] - pageOffsetX , pageY + winDowPos[1] - pageOffsetY]   ) ;
        //Ext.getCmp('menu_5').enable() ;
        pwMain.checkDisplayPreplace() ;
    } ,

    showMenu2 : function( pageX , pageY , pageOffsetX , pageOffsetY ){
    	Ext.getCmp('mainMenu').hide() ;
    	var winDowPos =  Ext.getCmp('pw-select-window').getPosition() ;
        Ext.getCmp('mainMenu2').showAt([pageX+ winDowPos[0] - pageOffsetX , pageY + winDowPos[1] - pageOffsetY]   ) ;
    } ,

    hideMenu1 : function(){
        selectTab.menuClick =  false ;
        Ext.getCmp('mainMenu').hide() ;
        Ext.getCmp('mainMenu2').hide() ;
    } ,

    checkDisplayPreplace : function(){
        var totalRow = pwMain.myStore.getTotalCount() ;
        var enable = false ;
        for(i = 0 ; i < totalRow ; i++){
            var record = pwMain.myStore.getAt(i) ;
            if( record == undefined )
                break ;
            if(pwMain.contentSelector == record.get('element_id') ){
                enable = true ;
                break ;
            }
        }

        if( enable ){
            Ext.getCmp('menu_5').enable() ;
        }else{
            Ext.getCmp('menu_5').disable() ;
        }
    } ,

    //------------------------ Brower

    browserSrcChange : function(){
        var me = this ;
        /*
        pwMain.fistTimeOpen++ ;
        if( pwMain.fistTimeOpen > 1 ){
            alert('Pls not click link to change page') ;
            document.getElementById('pw-browse').src = document.getElementById('pw-browse').src ;
            pwMain.fistTimeOpen = 0 ;
        }
        */

        /*
        if( document.getElementById("pw-browse").contentWindow.previewView == undefined)
            return ;
        var url = document.getElementById("pw-browse").contentWindow.previewView.getUrl()  ;
        //url = String(url) ;
        //var urls = url.split("l=") ;
        //url =  pwMain.urldecode( urls[urls.length-1] )   ;
        Ext.getCmp('pw-url').setValue(url) ;
        */
        var url = mPostMessage.processURL(Ext.getCmp('pw-url').getValue() , 'preview.php?l=') ;

        if(url == '' || document.URL.indexOf( document.getElementById('pw-browse').src) == 0){
        	return ;
       	}

        if( pwMain.crossDomain && me.showStartBubble == false){
            ml_testbubble.open0() ;
            //ml_testbubble.open7(pwMain.mainWindowObj) ;
            me.showStartBubble = true ;
         }

		//console.log('placement : browse onload iframe');

        if( pwMain.baseUrl != document.getElementById('pw-browse').src && mPostMessage.enable == false ){
            // Change to proxy mode ;
            // Wait 1 second to swith ;
            setTimeout(function(){
            	if(mPostMessage.enable == false){
            		//console.log('placement : brose tab change to proxy mode');
            		var url = document.getElementById('pw-browse').src ;
            		if( document.getElementById('pw-browse') != null){
		                document.getElementById('pw-browse').src = pwMain.temPath + 'preview.php?l='+pwMain.urlencode(document.getElementById('pw-browse').src) ;
		                mPostMessage.enable = true ;
		                mPostMessage.showProxyWarning() ;
		                mPostMessage.addProxyList(url) ;
		            }

            	}
			}, 1500);

            return ;
         }
    } ,

    processURL : function(){
    	var me = this ;
    	var data = {} ;
        data[ 'url'] = Ext.getCmp('pw-url').getValue() ;
        monoloop_base.showProgressbar('Loading data...'  ) ;
        if(pwMain.crossDomain){
            data['eID'] = 't3p_dynamic_content_placement' ;
            data['pid'] = pwMain.pid ;
            data['cmd'] = 'contentplacement/checkDoaminList' ;
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data ,
                callback: function(result) {
                    Ext.MessageBox.hide();
                    if( result.success == true){
                    	me.processURL_success() ;
                    }else{
                    	me.processURL_domaininvalide() ;
                    }
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid='+pwMain.pid+'&cmd=contentplacement/checkDoaminList'  ,
    			// send additional parameters to instruct server script
    			method: 'POST',
    			timeout: '30000',
    			params:   data ,
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
    				Ext.MessageBox.hide();
                 	var jsonRoot = Ext.util.JSON.decode(xhr.responseText) ;
                 	if( jsonRoot.success == true){
                    	me.processURL_success() ;
                    }else{
                    	me.processURL_domaininvalide() ;
                    }
    			},
    			failure: function() {
    				Ext.MessageBox.hide();
    				Ext.MessageBox.alert('Error', '#309' ) ;
    			}
            });
        }
        //alert(pwMain.urlencode(Ext.getCmp('pw-url').getValue())) ;

    } ,

    processURL_success : function(){
    	//V3 Always recheck postMessage every url enter ;
    	mPostMessage.enable = false ;
    	mPostMessage.hideProxyWarning() ;



        var title = pwMain.getActiveFrameTitle();

        if( title == 'Select'){
            selectTab.showTab() ;
            pwMain.reStartContentGrid() ;
        }else if( title == 'Test'){
            testbenchTab.showTab() ;
        }else{
            if( document.getElementById('pw-browse') != null){
                //document.getElementById('pw-browse').src = pwMain.temPath + 'preview.php?l='+pwMain.urlencode(Ext.getCmp('pw-url').getValue()) ;
                document.getElementById('pw-browse').src = mPostMessage.processURL(Ext.getCmp('pw-url').getValue(), 'preview.php?l=') ;
            }
        }
    } ,


    processURL_domaininvalide : function(){
    	Ext.MessageBox.alert('Error', 'You are trying to add content to a domain that is not registered to your account. Please have your administrator add the domain to the accounts domain list or point the element to a valid domain.') ;
    } ,











    // Comfirm to create account ;
    test_signUP : function(){
        Ext.QuickTips.init();
        var step1 = new Ext.FormPanel({
            frame:false,
			monitorValid:true,
            id : 'test-placement-step1' ,
            bodyStyle : 'background : #FFF ; padding-top :  5px ' ,
            border : false ,
            labelWidth: 125,
            items: [{
	                fieldLabel: 'Your Email',
					xtype:'textfield',
	                name: 'placement_signup[email]',
                    id : 'placement-signup-email' ,
					vtype:'email',
                    width : 200 ,
	                allowBlank:false
	            },{
	                fieldLabel: 'Your Name',
					xtype:'textfield',
	                name: 'placement_signup[name]',
                    id : 'placement-signup-name' ,
                    width : 200 ,
	                allowBlank:false
	            },{
	                fieldLabel: 'Organisation',
					xtype:'textfield',
	                name: 'placement_signup[company]',
                    id : 'placement-signup-company' ,
                    width : 200 ,
	                allowBlank:false
	            },{
			        xtype: 'checkbox',
					hideLabel: true,
					name:'placement_signup[accept_term]',
                    id : 'placement-signup-accept-term' ,
					width:400,
			        boxLabel: 'I accept <a href="http://temp.monoloop.com/index.php?id=30" target="_blank">Terms and Conditions</a> and <a href="http://temp.monoloop.com/index.php?id=31" target="_blank">Privacy Policy</a>',
			        inputValue: '1',
	               	validateValue: function(){
						return this.getValue();
					}
				}
	        ] ,

            buttons: [{
		            text : '<img src="'+pwMain.baseUrl+'typo3conf/ext/t3p_base/js/extjs/resources/images/default/grid/loading.gif">'   ,
                    cls: 'loadingButton' ,
                    disabled : true ,
                    width : 100  ,
                    id : 'placement-signup-loadingBtn1' ,
                    hidden : true
		        },{
		            text: 'Next >>',
					cls: 'submitbutton',
                    id : 'placement-signup-stage1-next-btn' ,
					formBind:true,
			        handler: function(){
			            Ext.getCmp('placement-signup-loadingBtn1').setVisible(true) ;
                        Ext.getCmp('placement-signup-stage1-next-btn').setVisible(false) ;

                        var data = {} ;
                        data['email'] =  Ext.getCmp('placement-signup-email').getValue() ;
                        data['eID'] = 'monoloopaccounting' ;
                        data['cmd'] = 'account/checkMailExists' ;
                        Ext.util.JSONP.request({
                            url: pwMain.baseUrl + 'index.php',
                            callbackKey: 'callback',
                            params : data ,
                            callback: function(result) {
                                //console.debug(result) ;

                                if( result.success == false){
                                    Ext.fly('test-placement-msg').update('Email already exists.');


                                    Ext.getCmp('placement-signup-loadingBtn1').setVisible(false) ;
                                    Ext.getCmp('placement-signup-stage1-next-btn').setVisible(true) ;

                                }else{
                                    Ext.fly('test-placement-msg').update('') ;

                                    Ext.getCmp('placement-signup-loadingBtn1').setVisible(false) ;
                                    Ext.getCmp('placement-signup-stage1-next-btn').setVisible(true) ;

                                    Ext.getCmp('test-placement-step1').setVisible(false) ;
                                    Ext.getCmp('test-placement-step2').setVisible(true) ;
                                }
                            }
                        });
                    }
                }
            ]

        }) ;

        var step2 = new Ext.FormPanel({
            frame:false,
			monitorValid:true,
            id : 'test-placement-step2' ,
            bodyStyle : 'background : #FFF ; padding-top :  5px ' ,
            border : false ,
            labelWidth: 125,
            hidden : true ,
            items : [
            {
                fieldLabel: 'Account Name',
                name: 'placement_signup[user]',
                id : 'placement-signup-user' ,
				xtype:'textfield',
                allowBlank:false ,
                width : 200 ,
                vtype : 'username'
            },{
                fieldLabel: 'Create Password',
				xtype:'textfield',
				inputType: 'password',
                name: 'placement_signup[password]',
				id: 'placement-signup-password',
                width : 200 ,
                allowBlank:false
            },{
                fieldLabel: 'Confirm Password',
				xtype:'textfield',
				inputType: 'password',
                name: 'placement_signup[pass-cfrm]',
                id: 'placement-signup-pass-cfrm' ,
		        vtype: 'password',
		        initialPassField: 'placement-signup-password',
                width : 200 ,
                allowBlank:false
            },{
                fieldLabel: 'Domain name',
                name: 'placement_signup[domain]',
                id : 'placement-signup-domain' ,
				xtype:'textfield',
                width : 200 ,
                allowBlank:false
            }] ,

            buttons: [{
	            text : '<img src="'+pwMain.baseUrl+'typo3conf/ext/t3p_base/js/extjs/resources/images/default/grid/loading.gif">'   ,
                cls: 'loadingButton' ,
                disabled : true ,
                width : 100  ,
                id : 'placement-signup-loadingBtn' ,
                hidden : true
	        },{
	           text : 'Back' ,
               id : 'placement-signup-step2backBtn' ,
		        handler: function(){
		             Ext.getCmp('test-placement-step1').setVisible(true) ;
                     Ext.getCmp('test-placement-step2').setVisible(false) ;
                }

	        },{
	            text: 'Sign Up',
                id : 'placement-signup-step2signupBtn' ,
				cls: 'submitbutton',
				formBind:true,
		        handler: function(){
                    Ext.fly('test-placement-msg').update('');
                    if(! Ext.getCmp('test-placement-step2').getForm().isValid())
                        return ;

                    Ext.getCmp('placement-signup-step2signupBtn').setVisible(false) ;
                    Ext.getCmp('placement-signup-step2backBtn').setVisible(false) ;
                    Ext.getCmp('placement-signup-loadingBtn').setVisible(true) ;

                    var data = {} ;
                    data['eID'] = 'monoloopaccounting' ;
                    data['cmd'] = 'account/remoteCheckSession' ;
                    data['pid'] = 1 ;
                    data['user'] = Ext.getCmp('placement-signup-user').getValue() ;
                    Ext.util.JSONP.request({
                        url: pwMain.baseUrl + 'index.php',
                        callbackKey: 'callback',
                        params : data ,
                        callback: function(result) {
                            //console.debug(result) ;
                            Ext.getCmp('placement-signup-step2signupBtn').setVisible(true) ;
                            Ext.getCmp('placement-signup-step2backBtn').setVisible(true) ;
                            Ext.getCmp('placement-signup-loadingBtn').setVisible(false) ;

                            if( result.success == 0){
                                Ext.getCmp('placement-test-signup').close() ;
                                Ext.MessageBox.alert('Fail', result.msg ) ;
                            }else if( result.success == 1){
                                Ext.fly('test-placement-msg').update(result.msg);
                            }else if( result.success == 2){
                                //console.debug('Process Next') ;
                                var data2 = {} ;
                                data2['eID'] = 'monoloopaccounting' ;
                                data2['cmd'] = 'account/replaceTestAccount' ;
                                data2['pid'] = 1 ;
                                data2['data[user]'] = Ext.getCmp('placement-signup-user').getValue() ;
                                data2['data[password]'] = Ext.getCmp('placement-signup-password').getValue() ;
                                data2['data[email]'] = Ext.getCmp('placement-signup-email').getValue() ;
                                data2['data[name]'] = Ext.getCmp('placement-signup-name').getValue() ;
                                data2['data[company]'] = Ext.getCmp('placement-signup-company').getValue() ;
                                data2['data[domain]'] = Ext.getCmp('placement-signup-domain').getValue() ;


                                Ext.getCmp('placement-signup-step2signupBtn').setVisible(false) ;
                                Ext.getCmp('placement-signup-step2backBtn').setVisible(false) ;
                                Ext.getCmp('placement-signup-loadingBtn').setVisible(true) ;

                                Ext.util.JSONP.request({
                                    url: pwMain.baseUrl + 'index.php',
                                    callbackKey: 'callback',
                                    params : data2 ,
                                    callback: function(result) {
                                        //console.debug(result) ;
                                        // Relogin ;
                                        Ext.util.JSONP.request({
                                            url: pwMain.baseUrl + 'index.php',
                                            callbackKey: 'callback',
                                            params : {
                                               eID : 'monoloopaccounting' ,
                                               user : data2['data[user]'] ,
                                               pass : data2['data[password]'] ,
                                               cmd : 'account/remotelogin' ,
                                               logintype : 'login' ,
                                               pid : 13
                                            } ,
                                            callback: function(result) {
                                                 Ext.getCmp('test-placement-step2').setVisible(false) ;
                                                 Ext.getCmp('test-placement-step3').setVisible(true) ;
                                            }
                                        });
                                    }
                                }) ;
                            }
                        }
                    });
		        }

	        }]
        }) ;

        var step3 = new Ext.FormPanel({
            frame:false,
			monitorValid:true,
            id : 'test-placement-step3' ,
            bodyStyle : 'background : #FFF ; padding-top :  5px ' ,
            border : false ,
            labelWidth: 125,
            hidden : true ,
            items : [
            {
                xtype : 'displayfield' ,
                hideLabel : true ,
                html : 'You account has been created.<br /><br /><a target="_blank" href="'+pwMain.baseUrl+'index.php?id=480&no_cache=1">Click here to log in<a/>'
            }
            ]
        }) ;

        var window = new Ext.Window({
            width : 400 ,
            height : 300 ,
            id : 'placement-test-signup' ,
            bodyStyle : 'background : #FFF ; padding : 10px ' ,
            items : [
            {
                xtype : 'displayfield' ,
                html : '<img src="'+pwMain.baseUrl+'fileadmin/templates/ikea/images/monoloop_logo_clear.gif" />'
            } , {
                xtype : 'displayfield' ,
                html : '<BR><b>Create your new profie & save data .</b>'
            } , {
                xtype : 'displayfield' ,
                html : '<div id="test-placement-msg" style="color:red ;"></div>'
            } ,
            step1 , step2 , step3
            ]  ,
            closable :false ,
            border : false ,
            baseCls : 'ml-fancy' ,
            bodyBorder : false ,
            header : false ,
            hideBorders : true ,
            resizable  : false ,
            draggable  : false ,
            autoScroll:true,
         	modal:true,
         	maximizable : false
        }) ;
        window.show() ;


        var temp = '<a id="zendesk-close-ico" href="#" onclick="pwMain.test_signUP_close();  return false ; "  ><img  src="'+pwMain.baseUrl+'typo3conf/ext/t3p_base/js/jquery/fancybox/fancy_close.png" style="position:absolute ; top:-15px ; right :-15px  ; z-index: 99999; " /></a>' ;

        Ext.select("#placement-test-signup").createChild(temp);

    } ,

    test_signUP_close : function(){
        Ext.getCmp('placement-test-signup').close() ;
    } ,






    // util function

    urlencode : function(str) {
        return escape(str).replace(/\+/g,'%2B').replace(/%20/g, '+').replace(/\*/g, '%2A').replace(/\//g, '%2F').replace(/@/g, '%40');
    } ,

    urldecode : function(str) {
        return unescape(str.replace('+', ' '));
    } ,

    showCookieWindow : function(){
        var window = new Ext.Window({
            width : 500 ,
            height : 400 ,
            title : 'Monoloop cookie editor' ,
            maximizable : false ,
            resizable : false ,
	   		plain : true ,
	        closable : true ,
	        modal : true ,
	        layout : 'fit' ,
            items : [
            {
            	xtype : 'textarea'  ,
            	layout : 'fit' ,
                id : 'cookie-text'
            }
            ] ,
			buttons : [
	        {
	            text: 'CANCEL' ,
	            cls: 'monoloop-black-btn' ,
	            handler : function(){
	               window.close() ;
	        	}
		    } ,
	        {
	            text: 'SAVE' ,
	            cls: 'monoloop-black-btn' ,
	            handler: function(){
	            	monoloop_base.msg('Saving' , 'Save custom cookie data.') ;
	                //me.setLoading('Save custom cookie data.') ;
	                var data = {} ;
	                data['cookieData'] = Ext.getCmp('cookie-text').getValue() ;
	                Ext.Ajax.request({
	                    url: 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveCookieData' ,
	                    method: 'POST',
	                    params : data ,
	        			// process the response object to add it to the TabPanel:
	        			success: function(xhr) {
	        			     Ext.MessageBox.hide();
	                         window.close() ;
	        			},
	        			failure: function() {
	                        //Ext.MessageBox.alert('Error', '#502' ) ;
	                        Ext.MessageBox.hide();
	        			}
	        		});
	            }
	        }
	        ]  ,

			listeners : {
	            'afterrender' : function(w){
	            	monoloop_base.msg('Loading' , 'Loading cookie data.') ;
	                //w.setLoading('Loading cookie data') ;
	                Ext.Ajax.request({
			            url: 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/getCookieData' ,
						// process the response object to add it to the TabPanel:
						success: function(xhr) {
						    Ext.getCmp(  'cookie-text').setValue(xhr.responseText)  ;
			                Ext.MessageBox.hide();
						},
						failure: function() {
			                Ext.MessageBox.hide();
						}
					});
	            }
	        }
        }) ;
        window.show() ;
    } ,

    getActiveIframe : function(){
        var iframe = null ;
        var title = pwMain.centerPanel.getActiveTab( ).title ;
        if(title == 'Browse'){
            iframe = document.getElementById('pw-browse') ;
        }else if(title == 'Select'){
            iframe = document.getElementById('pw-select-view') ;
        }
        else if(title == 'Test'){
        	iframe = document.getElementById('pw-test-view') ;
        }
        return iframe ;
    } ,

    getActiveFrameTitle : function(){
    	return pwMain.centerPanel.getActiveTab( ).title ;
    }
} ;


Ext.apply(Ext.form.VTypes, {
    password : function(val, field) {
        if (field.initialPassField) {
            var pwd = Ext.getCmp(field.initialPassField);
            return (val == pwd.getValue());
        }
        return true;
    },

    passwordText : 'Passwords do not match'
});

Ext.form.VTypes["usernameVal"] = /^[a-zA-Z][-_.a-zA-Z0-9]{0,30}$/;
Ext.form.VTypes["username"]=function(v){
 return Ext.form.VTypes["usernameVal"].test(v);
}
Ext.form.VTypes["usernameText"]="Username must begin with a letter and cannot exceed 255 characters"
Ext.form.VTypes["usernameMask"]=/[-_.a-zA-Z0-9]/;

window.mPostMessage = {
	enable : false ,
	url : '' ,
	prepSelectMode : false ,
	prepTestMode : false ,
	proxyList : [] ,
	addListener : function(){
		var me = this ;
		var listener;
		listener = function(event) {
			var data;
			data = JSON.parse(event.data);
	        switch (data.t) {
	          case 'confirm':
	            me.enable = true ;
	            me.url = data.url ;
	            //V3 Always set url bar ;
	            Ext.getCmp('pw-url').setValue(data.displayURL) ;
                //Check Active Mode ;

                monoloopLog.log('Found invocation in customer site') ;
                monoloopLog.log('Start loading bundle.js');
                var iframe = pwMain.getActiveIframe() ;
                var time =  new Date().getTime() ;
	            iframe.contentWindow.postMessage(JSON.stringify({
			        t: "loadbundle" ,
                    domain : pwMain.baseRelativeUrl ,
			        url : pwMain.baseRelativeUrl + 'fileadmin/cbr/editor-bundle.js?' + time
				}), me.url);

	            break;
              case 'loadbundle-success' :
                //Check frame init ;
                var title = pwMain.getActiveFrameTitle();
                var iframe = pwMain.getActiveIframe() ;

				monoloopLog.log('placement : send init request') ;

                if( title == 'Select'){
                	monoloopLog.log('placement : send init select ' + me.url) ;
                	//alert(1) ;
                	/*
                	setTimeout(function(){
	                	iframe.contentWindow.postMessage(JSON.stringify({
					        t: "init-selectmode"
						}), me.url);
					},1000) ;
					*/
					me.prepSelectMode = false ;
					iframe.contentWindow.postMessage(JSON.stringify({
				        t: "init-selectmode"
					}), me.url);

					if( me.prepSelectMode == false ){
	                	iframe.contentWindow.postMessage(JSON.stringify({
					        t: "init-selectmode"
						}), me.url);
					}

                }else if( title == 'Test'){

                	me.prepTestMode = false ;

                	iframe.contentWindow.postMessage(JSON.stringify({
				        t: "init-testmode"
					}), me.url);

					if( me.prepTestMode == false ){
	                	iframe.contentWindow.postMessage(JSON.stringify({
					        t: "init-testmode"
						}), me.url);
					}
                }else if(title == 'Browse'){
                	// Only proxy had listener ;
                	iframe.contentWindow.postMessage(JSON.stringify({
				        t: "init-browse"
					}), me.url);

                }

                break ;
              /*--------- SELECT MODE ( MAPPING OLD FUNCTION WITH POSTMESSAGE) ----------- */
			  case 'select-hideMenu1' :
			  	pwMain.hideMenu1()
			  break ;
			  case 'select-showMenu2' :
			  	pwMain.showMenu2(data.pageX , data.pageY , data.pageOffsetX , data.pageOffsetY ) ;
			  break ;
			  case 'select-showMenu1' :
			  	pwMain.showMenu1(data.pageX , data.pageY , data.pageOffsetX , data.pageOffsetY , data.selector ) ;
			  break ;
			  case 'select-bubbleClick' :
			    pwMain.bubbleClick(data.selector) ;
			  break ;
			  case 'select-changeSelectorTxt' :
			 	selectTab.changeSelectorTxt(  data.selector  ) ;
			  break ;
			  case 'select-getSelectContentRet' :
			  	pwMain.getSelectContentRet(data.ret) ;
			  break ;
              case 'select-allassetloadcomplete' :
                me.prepSelectMode = true ;
                pwMain.reStartContentGrid() ;
              break ;
              case 'select-disableParentBtn' :
                selectTab.disableParentBtn(data.ret) ;
              break ;
              case 'select-disableChildBtn' :
                selectTab.disableChildBtn(data.ret) ;
              break ;
              case 'select-receiveIframeMultiSelectedData' :
              	pwMain.receiveIframeMultiSelectedData(data.data) ;
              break ;
			  /*--------- TESTBENCH MODE ------- */
              case 'test-allassetloadcomplete' :
              	me.prepTestMode = true ;
                testbenchTab.allassetloadcomplete() ;
              break ;
			  case 'test-selectTabSrcChange' :
			  	testbenchTab.selectTabSrcChange() ;
			  break ;
			  case 'test-bubbleClick' :
			  	testbenchTab.bubbleClick(data.selector) ;
			  break ;
	        }
		};
		if (window.addEventListener) {
	        return addEventListener("message", listener, false);
		} else {
	        attachEvent("onmessage", listener);
		}
	} ,

	showProxyWarning : function(){
		/*
		Ext.getCmp('monoloop-placement-header-label').setValue('<p align="center" style="font-weight:bold;color:red;">Notice:This site does not have our code in the page, so we will pull your site content through our proxy. This can cause certain features, content not being available or styling missing. To avoid this - please place the invocation code from Account/Codes & Scripts section into the header section of your site template.</p>');
		*/
		var ret = Ext.query('#msg-div-2') ;
		if(ret.length == 0 || ret[0].innerHTML == '' )
			monoloop_base.msgCloseOnClick(document.getElementsByTagName("body")[0],'Notice' ,'This site does not have our code in the page, so we will pull your site content through our proxy. This can cause certain features, content not being available or styling missing. To avoid this - please place the invocation code from Account/Codes & Scripts section into the header section of your site template.') ;
	} ,

	hideProxyWarning : function(){

	} ,

	processURL : function(url , proxy){
		var me = this ;

		// This function for filter url ;
		if(Ext.util.Format.trim(url) == '')
			return url ;

		for(var i = 0 ; i < me.proxyList.length ; i++ ){
			if(me.proxyList[i] == url)
				return pwMain.temPath + proxy+ pwMain.urlencode(url) ;  ;
		}

		if(url.toLowerCase().indexOf('http') != 0){
			url = '//' + url ;
		}
		return url ;
	} ,

	/* N pending : as some customer not using invocation entire domain */

	addProxyList : function(url){
		var me = this ;
		for(var i = 0 ; i < me.proxyList.length ; i++ ){
			if(me.proxyList[i] == url){
				return ;
			}
		}
		me.proxyList.push(url) ;
	} ,

	checkProxyList : function(url){

	}
} ;

Ext.onReady(function(){
	mPostMessage.addListener() ;
}) ;

return window.pwMain ;
//---
}) ;