define(function (require) {
// --- start
window.selectTab = {
     lastURL : '' , 
     menuClick : false , 
     contentElementSelectorWindow : null , 
     csePanel : null , 
     csePanel2 : null , 
     showContentEdit : null , 
     typeID : 0 , 
     group : 0 , 
     // For type2 --
     beforeAfter : 0 , 
     contentId : 0 , 
     // ------------
     showTab : function(){
        if(document.getElementById("pw-select-view") == null)
            return ; 
        mPostMessage.enable = false ; 
        mPostMessage.hideProxyWarning() ; 
        document.getElementById("pw-select-view").src =mPostMessage.processURL( Ext.getCmp('pw-url').getValue() , 'selectView.php?l=' ) ;
        /*
        var currentURL = ( document.getElementById("pw-select-view").contentDocument || document.getElementById("pw-select-view").contentWindow.document ).location  ;
        currentURL = String(currentURL) ; 
        var addressBarURL = pwMain.temPath + 'selectView.php?l='+pwMain.urlencode(Ext.getCmp('pw-url').getValue()) ;
         
        if(currentURL != addressBarURL ){
            pwMain.myStore.removeAll(false) ; 
            document.getElementById("pw-select-view").src = addressBarURL ; 
        }
        */
     } , 
     // 1 . Add new content 2. Predifined content 3. varaition content 4. Outter content.  5. CE element   ; 
     // Group 
     // 0 All
     // 1 normal 
     // 2 spetail 
     
     showContentElementSelectorWindow : function(typeID , group){
  		var me = this ; 
        
        selectTab.typeID = typeID ; 
        selectTab.contentId = 0 ;
        
        selectTab.group =  group ;
        
        if( typeID == 7){
        	me.DirectLoadElement('Remove')  ;
        	return ;
        }
        
        if( typeID == 1)
            selectTab.ContentElementInit() ;
        else if( typeID == 2)
            selectTab.ContentElementInit2() ;   
        else if ( typeID == 3 || typeID == 6)
            selectTab.ContentElementInit3() ; 
        else if ( typeID == 4)
            selectTab.ContentElementInit() ;
        else if ( typeID == 5)    
            selectTab.ContentElementInit4() ;
        if(typeID != 4 && typeID != 5)    
            selectTab.ContentElementInitPanel2() ; 
        else
            selectTab.ContentElementInitPanel2_1() ; 
        
        selectTab.contentElementSelectorWindow = new Ext.Window({
            width:300,
         	height:210,
            title:"CONTENT ELEMENT SELECTOR",
            plain:true , 
         	modal:true,
         	maximizable : false  , 
            resizable : false , 
         	closable:true , 
            id : 'ces-window' , 
            //bodyStyle : 'background : white ;' , 
            //baseCls : 'ces-window' ,
            //iconCls : 'ces-window-header-icon' ,  
            layout : 'vbox' , 
            //plugins: new Ext.ux.WindowAlwaysOnTop , 
            items : [selectTab.csePanel , selectTab.csePanel2] ,  
            buttons: [ 
            /*{
	            text:'PREVIEW' ,
                iconCls : 'pw-menu-inspect-content' , 
	            handler: function(){
					alert('Preview Here') ; 
	            }
	        },*/
            {
	            text:'CANCEL' ,
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
					selectTab.contentElementSelectorWindow.close() ;
	            }
	        },{
	            text:'OK' ,
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
                    if( selectTab.typeID == 1 ){
                        if( Ext.getCmp('cse-name').validate() == false  ) {
                            Ext.getCmp('cse-name').markInvalid('Pls enter name.') ; 
                            return ; 
                        } 
                        if( Ext.getCmp('cse-template-combo').validate() == false  ) {
                            Ext.getCmp('cse-template-combo').markInvalid('Pls select template.') ; 
                            return ; 
                        }
                        var toUid = Ext.getCmp('cse-template-combo').getValue() ; 
                        selectTab.showContentEdit = true ; 
                        pwContentEditor.elementName = Ext.getCmp('cse-name').getValue() ; 
                        pwMain.selectorTxt = Ext.getCmp('cse-selector').getValue() ; 
                        selectTab.contentElementSelectorWindow.close() ;
                        selectTab.contentElementSelectorWindow.destroy(); 
                        pwContentEditor.openMainWindow(toUid , 0 , 0 ) ; 
                    }else if( selectTab.typeID == 2 ){
                        me.savePredefined() ; 
                        
                    }else if( selectTab.typeID == 3){
                        if( Ext.getCmp('cse-name').getValue() == ''  ) {
                            Ext.getCmp('cse-name').markInvalid('Pls enter name.') ; 
                            return ; 
                        }
                        variation.resultType = 0 ; 
                        variation.contentHtml = pwMain.getIframeSelectContent() ;
                        variation.contentName = Ext.getCmp('cse-name').getValue() ; 
                        pwMain.selectorTxt = Ext.getCmp('cse-selector').getValue() ; 
                        selectTab.contentElementSelectorWindow.close() ;
                        selectTab.contentElementSelectorWindow.destroy();
                        //variation.openMainWindow(0) ; 
                    }else if( selectTab.typeID == 6){
                    	if( Ext.getCmp('cse-name').getValue() == ''  ) {
                            Ext.getCmp('cse-name').markInvalid('Pls enter name.') ; 
                            return ; 
                        }
                        
                        variation.resultType = 0 ;  
                        variation.contentName = Ext.getCmp('cse-name').getValue() ; 
                        pwMain.selectorTxt = Ext.getCmp('cse-selector').getValue() ; 
                        selectTab.contentElementSelectorWindow.close() ;
                        selectTab.contentElementSelectorWindow.destroy();
                        variation.contentHtml = '<!-- Removed by Monoloop -->' ;
 
                        variation.openMainWindow(0) ;
                        
                    }else if( selectTab.typeID == 4){
                        if( Ext.getCmp('cse-name').validate() == false  ) {
                            Ext.getCmp('cse-name').markInvalid('Pls enter name.') ; 
                            return ; 
                        } 
                        if( Ext.getCmp('cse-template-combo').validate() == false  ) {
                            Ext.getCmp('cse-template-combo').markInvalid('Pls select template.') ; 
                            return ; 
                        }
                        var toUid = Ext.getCmp('cse-template-combo').getValue() ; 
                        pwContentEditor.elementName = Ext.getCmp('cse-name').getValue() ;  
                        selectTab.contentElementSelectorWindow.close() ;
                        selectTab.contentElementSelectorWindow.destroy(); 
                        pwContentEditor.openMainWindow(toUid , 0 , 2 ) ; 
                    }else if( selectTab.typeID == 5){
                        if( Ext.getCmp('cse-name').validate() == false  ) {
                            Ext.getCmp('cse-name').markInvalid('Pls enter name.') ; 
                            return ; 
                        } 
                        if( Ext.getCmp('cse-template-combo').validate() == false  ) {
                            Ext.getCmp('cse-template-combo').markInvalid('Pls select template.') ; 
                            return ; 
                            
                        }
                        var toUid = Ext.getCmp('cse-template-combo').getValue() ; 
                        if( toUid == 0){
                            // call variation ;
                            variation.resultType = 5 ;  
                            variation.contentHtml = '';
                            variation.contentName = Ext.getCmp('cse-name').getValue() ;  
                            selectTab.contentElementSelectorWindow.close() ;
                            selectTab.contentElementSelectorWindow.destroy();
                            variation.openMainWindow(0) ; 
                        }else{  
                            pwContentEditor.elementName = Ext.getCmp('cse-name').getValue() ;  
                            selectTab.contentElementSelectorWindow.close() ;
                            selectTab.contentElementSelectorWindow.destroy(); 
                            pwContentEditor.openMainWindow(toUid , 0 , 5 ) ;
                        }
                    }    
	            }
	        }] , 
            listeners : {
                'show' : function(w){
                    //selectTab.contentElementSelectorWindow.setHeight(310) ;  
                    selectTab.showContentEdit = false ; 
                    
                    
	               if( pwMain != undefined){
                        if( typeID == 3 && pwMain.crossDomain){
                            ml_testbubble.open4(w) ; 
                        }
                    } 
                } , 
                'afterrender' : function(w){
                    
                } ,
                'close' : function(panel){
                    if(selectTab.showContentEdit == false)
                        pwMain.clearIframeSelection() ; 
                    selectTab.contentElementSelectorWindow.destroy() ; 
                    
                    if( pwMain != undefined){
                        if( typeID == 3 && pwMain.crossDomain){
                            if( Ext.getCmp('test_bubble_4') != undefined)
                                Ext.getCmp('test_bubble_4').close() ; 
                        }
                    }
                }
            }
        }) ; 
        selectTab.contentElementSelectorWindow.show();
     }  , 
     
     before_savePredefined : function(){
     	return {} ;  
     } ,
     
     savePredefined : function(){ 
     	var me = this ; 
     	var data = me.before_savePredefined() ;
   		if( selectTab.contentId == 0 ){
            Ext.getCmp('cse-name').markInvalid('Pls enter name.') ; 
            return ; 
        }
        
        data['url'] = Ext.getCmp('pw-url').getValue();
        data['contentId'] = selectTab.contentId ; 
        data['selector'] = Ext.getCmp('cse-selector').getValue() ; 
        data['beforeAfter'] = selectTab.beforeAfter ;  
        selectTab.contentElementSelectorWindow.hide()  ; 
        monoloop_base.showProgressbar('Initializing...') ;  
        if( pwMain.crossDomain){
            data['eID'] = 't3p_dynamic_content_placement' ; 
            data['pid'] = 1 ; 
            data['cmd'] = 'savePredifinedContent' ;  
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data , 
                callback: function(jsonData) {
                    if(jsonData.success == true){
                        monoloop_base.msg('Success', 'New monoloop predifined content has been created.') ;
                        selectTab.contentElementSelectorWindow.close() ;
                        selectTab.contentElementSelectorWindow.destroy(); 
                        pwMain.reStartContentGrid() ;
                        Ext.MessageBox.hide(); 
                    }else{
                        Ext.MessageBox.alert('Error', jsonData.msg , function(){
                        	selectTab.contentElementSelectorWindow.show() ; 
                        }) ;
                    }
                    
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=savePredifinedContent' , 
                //url : 'testxml.php' , 
    			method: 'POST',
    			params:   data ,
    			timeout: '30000',
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
     				var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
                    if(jsonData.success == true){
                        monoloop_base.msg('Success', 'New monoloop predifined content has been created.') ;
                        selectTab.contentElementSelectorWindow.close() ;
                        selectTab.contentElementSelectorWindow.destroy(); 
                        pwMain.reStartContentGrid() ;
                        Ext.MessageBox.hide(); 
                    }else{
                        Ext.MessageBox.alert('Error', jsonData.msg , function(){
                        	selectTab.contentElementSelectorWindow.show() ; 
                        }) ;
                        
                    }
                    
                }, 
                failure: function() {
				    Ext.MessageBox.hide();
					Ext.MessageBox.alert('Error', '#331' ) ; 
				}
            }) ; 
        }
     } , 
     
     DirectLoadElement : function(name){
     	var me = this ; 
     	var data = {}  ; 
     	data['name'] = name ; 
     	monoloop_base.showProgressbar('Initializing...') ;  
        if( pwMain.crossDomain){
            data['eID'] = 't3p_dynamic_content_placement' ; 
            data['pid'] =1; 
            data['cmd'] = 'contentmanager/getTouidByName' ;  
            
            data['time'] = new Date().getTime();
            Ext.util.JSONP.request({
                url: pwMain.baseUrl + 'index.php',
                callbackKey: 'callback',
                params : data , 
                callback: function(jsonData) {
                    if(jsonData.success == true){ 
                        var toUid = jsonData.uid ; 
                        Ext.MessageBox.hide(); 
                        selectTab.showContentEdit = true ; 
                        pwContentEditor.elementName = name ; 
                        pwMain.selectorTxt = pwMain.contentSelector  ;  
                        pwContentEditor.openMainWindow(toUid , 0 , 0 ) ; 
                    }else{
                        Ext.MessageBox.hide(); 
                    } 
                }
            });
        }else{
            Ext.Ajax.request({
    			url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanager/getTouidByName' , 
                //url : 'testxml.php' , 
    			method: 'POST',
    			params:   data ,
    			timeout: '30000',
    			// process the response object to add it to the TabPanel:
    			success: function(xhr) {
     				var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
                    if(jsonData.success == true){
                        Ext.MessageBox.hide(); 
                        var toUid = jsonData.uid ; 
                        selectTab.showContentEdit = true ; 
                        pwContentEditor.elementName = name ; 
                        pwMain.selectorTxt = pwMain.contentSelector  ;  
                        pwContentEditor.openMainWindow(toUid , 0 , 0 ) ; 
                    }else{
                        Ext.MessageBox.hide(); 
                        
                    }
                    
                }, 
                failure: function() {
				    Ext.MessageBox.hide();
					Ext.MessageBox.alert('Error', '#331' ) ; 
				}
            }) ; 
        }
     } , 
     
     ContentElementInit : function(){
        var minStore = null ; 
        
        if(pwMain.crossDomain){
            var minStore = new Ext.data.JsonStore({
                fields : ['title',  {name: 'uid', type: 'int'}] , 
                proxy: new Ext.data.ScriptTagProxy({
    	            url: pwMain.baseUrl +  'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanagerListCE&group='+ selectTab.group 
    	        })
                //index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanagerListCE
            }) ; 
        }else{
            var minStore = new Ext.data.JsonStore({
                fields : ['title',  {name: 'uid', type: 'int'}] , 
                proxy: new Ext.data.HttpProxy({
    	            url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanagerListCE&group='+ selectTab.group 
    	        })
                //index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanagerListCE
            }) ; 
        }
        
        

        selectTab.csePanel = new Ext.Panel({
            width:  '100%' ,
        	height: 125 , 
            id : 'ces-panel' , 
            border : false , 
            bodyBorder : false , 
            bodyCssClass : 'cse-panel-body' , 
            layout : 'form' , 
            defaults : {
            	anchor : '90%' 
            } , 
        
            items : [  
                {
                    xtype     : 'label',
                    text      : 'Content Name:'  
                } ,
                {
                    xtype     : 'textfield', 
                    hideLabel : true , 
                    id        : 'cse-name',
                    allowBlank: false  
                } ,
                {
                    xtype     : 'label',
                    text      : 'Select Monoloop Template:'  
                } , 
                {
                    xtype:          'combo',  
                    allowBlank: false  ,
                    hideLabel : true , 
                    triggerAction:  'all',
                    editable:       false,
                    emptyText : 'Please select template' , 
                    name: 'cse_template',
                    hiddenId : 'cse_template' , 
                    displayField:   'title',
                    valueField:     'uid',
                    id : 'cse-template-combo' ,  
                
                    store:  minStore,
                    validateOnBlur : false  
                } 
            ]
            
        }) ; 
        
        
     } , 
     
     ContentElementInit2 : function(){
        selectTab.csePanel = new Ext.Panel({
            width:  '100%' ,
			height: 125 , 
            id : 'ces-panel' , 
            border : false , 
            bodyBorder : false , 
            bodyCssClass : 'cse-panel-body' , 
            layout : 'form' , 
            defaults : {
            	anchor : '90%' 
            } , 
        
            items : [  
                {
                    xtype     : 'label',
                    text      : 'Content Name'  
                } ,
                {
                    xtype     : 'textfield', 
                    id        : 'cse-name',
                    hideLabel : true , 
                    allowBlank: true  ,
                    disabled  : true  , 
                    value     : 'Click below to choose content' 
                } ,
                {
                    xtype     : 'button',
                    style     : 'padding-top : 10px' , 
                    text      : 'Choose pre-defined content from list'  , 
                    listeners  : {
                        'click' : function(button , e){
                            selectTab.contentElementSelectorWindow.hide() ; 
                            predefinedWindow.openWindow() ;  
                        } 
                    }
                }  
            ]
            
        }) ; 
     } , 
     
     ContentElementInit3 : function(){
        selectTab.csePanel = new Ext.Panel({
            width:  '100%' ,
     		height: 125 , 
            id : 'ces-panel' , 
            border : false , 
            bodyBorder : false , 
            bodyCssClass : 'cse-panel-body' , 
            layout : 'form' , 
            defaults : {
            	anchor : '90%' 
            } , 
        
            items : [  
                {
                    xtype     : 'label',
                    text      : 'Content Name'  
                } ,
                {
                    xtype     : 'textfield', 
                    id        : 'cse-name',
                    allowBlank: true  , 
                    hideLabel : true , 
                    emptyText : 'Enter a name for your selection *' , 
                    emptyClass : 'orange-text'
                }  
            ]
            
        }) ; 
     } , 
     
     
     ContentElementInit4 : function(){
        var minStore = null ; 
        
        if(pwMain.crossDomain){
            var minStore = new Ext.data.JsonStore({
                fields : ['title',  {name: 'uid', type: 'int'}] , 
                proxy: new Ext.data.ScriptTagProxy({
    	            url: pwMain.baseUrl +  'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanager/singleElementList&group='+ selectTab.group 
    	        }) 
            }) ; 
        }else{
            var minStore = new Ext.data.JsonStore({
                fields : ['title',  {name: 'uid', type: 'int'}] , 
                proxy: new Ext.data.HttpProxy({
    	            url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanager/singleElementList&group='+ selectTab.group 
    	        }) 
            }) ; 
        }
        
        

        selectTab.csePanel = new Ext.Panel({
            width:  '100%' ,
       		height: 125 , 
            id : 'ces-panel' , 
            border : false , 
            bodyBorder : false , 
            bodyCssClass : 'cse-panel-body' , 
            layout : 'form' , 
            defaults : {
            	anchor : '90%' 
            } , 
        
            items : [  
                {
                    xtype     : 'label',
                    text      : 'Content Name:'  
                } ,
                {
                    xtype     : 'textfield', 
                    id        : 'cse-name',
                    hideLabel : true  ,
                    allowBlank: false  
                } ,
                {
                    xtype     : 'label',
                    text      : 'Select Monoloop Template:'  
                } , 
                {
                    xtype:          'combo',  
                    allowBlank: false  ,
                    hideLabel : true , 
                    width : 190 , 
                    triggerAction:  'all',
                    editable:       false,
                    emptyText : 'Please select template' , 
                    name: 'cse_template',
                    hiddenId : 'cse_template' , 
                    displayField:   'title',
                    valueField:     'uid',
                    id : 'cse-template-combo' ,  
                
                    store:  minStore,
                    validateOnBlur : false  
                } 
            ]
            
        }) ; 
        
        
     } , 
     
     ContentElementInitPanel2 : function(){
 		var me = this ; 
            
         selectTab.csePanel2 = new Ext.Panel({ 
       		height:  140 , 
       		width:  '100%' ,
            id : 'ces-panel2' ,  
            border : false , 
            bodyBorder : false , 
            //bodyStyle : 'background-color : white ; ' , 
            bodyCssClass : 'cse-panel-body' , 
            layout : 'form' , 
            defaults : {
            	anchor : '90%' 
            } , 
        
            items : [   
                {
                    xtype: 'displayfield',
                    hideLabel : true , 
                    html: '<div><div style="float:left; " id="cse-expand-div"><a onclick="selectTab.moreOptionExpand()" href="javascript:;"><img src="./typo3conf/ext/t3p_base/image/nolines_plus.gif"/></a></div><div class="cse-text" style="float:left;">more selection options...</div><div style="clear:both"></div></div>'  , 
                    style : 'left: 0px; top: 138px;'  
                } ,{
                	xtype : 'panel' ,  
					border : false ,
                	layout:'column',
                	items: [{
				        xtype : 'displayfield' , 
				        html : '<a id="td-parent" onclick="selectTab.selectParent();return false ; " href="#" class="select-box"><span class="select-box">Select parent</span></a>' , 
				        columnWidth: .45
				    },{
				        xtype : 'displayfield' , 
				        html : '<div style="text-align:center;width:100%;">or</div>' , 
				        columnWidth: 0.1
				    },{
				        xtype : 'displayfield' , 
				        html : '<a id="td-firstchild" onclick="selectTab.selectFirstChild(); return false ;" href="#" class="select-box"><span class="select-box">Select first child</span></a>' , 
				        columnWidth: .45
				    }]
                },{
                	xtype : 'panel' ,  
					border : false ,
					style : 'margin : 5px 0 5px 0 ;' , 
                	layout:'column',
                	items: [{
				        xtype : 'displayfield' , 
				        html : '&nbsp;' , 
				        columnWidth: .3
				    },{
				        xtype : 'displayfield' , 
				        html : '<a href="#" class="select-box" onclick="selectTab.selectReset(); return false ;"><span class="select-box">Reset</span></a>' , 
				        columnWidth: 0.4
				    },{
				        xtype : 'displayfield' , 
				        html : '&nbsp;' , 
				        columnWidth: .3
				    }]
                }/*,
				{
					
					defaults: { 
					  fieldLabel: '&nbsp;',
          				labelSeparator: '&nbsp;'
					},
					items : [{
						xtype : 'displayfield' , 
						html : '<img src="./typo3conf/ext/t3p_base/image/select_parent.png"/>' 
					},{
						xtype : 'displayfield' , 
						html : 'or'
					},{
						xtype : 'displayfield' ,
						html : 'Select first child'
					}]
				} ,
                {
                    xtype: 'displayfield',                
                    html : '<table width="100%"><tr><td id="td-parent"><a href="javascript:;" onclick="selectTab.selectParent();"><img src="./typo3conf/ext/t3p_base/image/select_parent.png"/></a></td><td  id="td-or">&nbsp;&nbsp;or&nbsp;&nbsp;</td><td  id="td-firstchild"><a href="javascript:;" onclick="selectTab.selectFirstChild();"><img src="./typo3conf/ext/t3p_base/image/select_first_child.png"/></a></td></tr></table>' 
                } , 
                {
                	xtype : 'displayfield',
                	html : '<a href="javascript:;" onclick="selectTab.selectReset();">Reset</a>'
                } */, 
                {
                    xtype     : 'label',
                    text      : 'Selector:'  
                } ,
                {
                    xtype     : 'textfield', 
                    width : '80%' ,   
                    hideLabel : true , 
                    id        : 'cse-selector',
                    allowBlank: false  , 
                    value     : pwMain.contentSelector , 
                    listeners : {
                    	change : function(t , n , o){
                    	 	me.changeSelectorTxt2(n) ; 
                    	}
                    }
                } 
            ] 
            
        }) ; 
     } , 
     
     ContentElementInitPanel2_1 : function(){
        selectTab.csePanel2 = new Ext.Panel({
            width:  '100%' ,
            //height: 15 , //105
            id : 'ces-panel2' , 
            border : false , 
            bodyBorder : false , 
            bodyCssClass : 'cse-panel-body' , 
            layout : 'vbox' , 
        
            items : [  
                  
                
            ]
            
        }) ; 
     } , 
     
     moreOptionExpand : function(){
        Ext.fly('cse-expand-div').dom.innerHTML = '<a onclick="selectTab.moreOptionCollapse()" href="javascript:;"><img src="./typo3conf/ext/t3p_base/image/nolines_minus.gif"/></a>';
        Ext.getCmp('ces-window').setHeight(330) ; 
        //Ext.getCmp('ces-panel2').setHeight(95) ; 
        var me = this ; 
        me.checkDisplayParentFirstChild() ; 
     } , 
     
     moreOptionCollapse : function(){
        Ext.fly('cse-expand-div').dom.innerHTML = '<a onclick="selectTab.moreOptionExpand()" href="javascript:;"><img src="./typo3conf/ext/t3p_base/image/nolines_plus.gif"/></a>';
        Ext.getCmp('ces-window').setHeight(220) ; 
        //Ext.getCmp('ces-panel2').setHeight(15) ; 
     } , 
     
     checkDisplayParentFirstChild : function(){
     	var selectorTxt = Ext.getCmp('cse-selector').getValue() ;  
     	Ext.get('td-parent').removeClass('inactive') ;
     	Ext.get('td-firstchild').removeClass('inactive') ;
     	/*
     	if( document.getElementById('pw-select-view').contentWindow.monoloop_select_view.checkParent(selectorTxt) == false ){
     		Ext.get('td-parent').addClass('inactive') ; 
     	}
     	
     	if( document.getElementById('pw-select-view').contentWindow.monoloop_select_view.checkChild(selectorTxt) == false ){
     		Ext.get('td-firstchild').addClass('inactive') ; 
     	}
     	*/
     	
     	var iframe = document.getElementById('pw-select-view') ;
     	
     	iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-checkParent"  , 
	        selectorTxt : selectorTxt
		}), mPostMessage.url);
		
		iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-checkChild"  , 
	        selectorTxt : selectorTxt
		}), mPostMessage.url);
     	
     	
     } , 
     
     selectParent : function(){
        if( document.getElementById('pw-select-view') == null )
            return ; 
            
        if(Ext.get('td-parent').hasClass('inactive')) {
        	return false ; 
        }
        
        var selectorTxt = Ext.getCmp('cse-selector').getValue() ; 
        var iframe = document.getElementById('pw-select-view') ;
        
        //document.getElementById('pw-select-view').contentWindow.monoloop_select_view.selectFocusParent(selectorTxt);    
        
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-selectFocusParent"  , 
	        selectorTxt : selectorTxt
		}), mPostMessage.url);
		
		
        var me = this ; 
        me.checkDisplayParentFirstChild() ;  
     } , 
     
     changeSelectorTxt : function(selectorTxt){
        pwMain.selectorTxt = selectorTxt ; 
        Ext.getCmp('cse-selector').setValue(selectorTxt) ; 
        
        if( document.getElementById('pw-select-view') == null )
            return ; 
            
        var iframe = document.getElementById('pw-select-view') ;    
        //document.getElementById('pw-select-view').contentWindow.monoloop_select_view.clearSelection() ;     
        //document.getElementById('pw-select-view').contentWindow.monoloop_select_view.processCustomXPath(selectorTxt);  
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-clearSelection"   
		}), mPostMessage.url);
		
		iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-processCustomXPath"  , 
	        selectorTxt : selectorTxt
		}), mPostMessage.url);
        
		var me = this ; 
		me.checkDisplayParentFirstChild() ;    
     } ,  
     
     selectReset : function(){
     	var me = this ; 
     	me.changeSelectorTxt(pwMain.contentSelector) ;  
     } ,
     
     changeSelectorTxt2 : function(selectorTxt){
        pwMain.selectorTxt = selectorTxt ;   
        
        if( document.getElementById('pw-select-view') == null )
            return ; 
            
        //document.getElementById('pw-select-view').contentWindow.monoloop_select_view.clearSelection() ; 
        //document.getElementById('pw-select-view').contentWindow.monoloop_select_view.processCustomXPath(selectorTxt);  
        var iframe = document.getElementById('pw-select-view') ;    
        
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-clearSelection"   
		}), mPostMessage.url);
		
		iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-processCustomXPath"  , 
	        selectorTxt : selectorTxt
		}), mPostMessage.url);
		
        
		var me = this ; 
		me.checkDisplayParentFirstChild() ;    
     } ,
     
     selectFirstChild : function(){
        if( document.getElementById('pw-select-view') == null )
            return ; 
            
        if(Ext.get('td-firstchild').hasClass('inactive')){
        	return false ; 
        }
        
        var selectorTxt = Ext.getCmp('cse-selector').getValue() ; 
        var iframe = document.getElementById('pw-select-view') ;    
        
        iframe.contentWindow.postMessage(JSON.stringify({
	        t: "select-selectFocusFirstChild"  , 
	        selectorTxt : selectorTxt
		}), mPostMessage.url);
        
        //document.getElementById('pw-select-view').contentWindow.monoloop_select_view.selectFocusFirstChild(selectorTxt);    
        var me = this ; 
        me.checkDisplayParentFirstChild() ; 
     } ,  
     
     getHtmlFormXpath : function(xPath){
     	if( document.getElementById('pw-select-view') == null )
            return ;
      	return document.getElementById('pw-select-view').contentWindow.monoloop_select_view.getContentFromXPath(xPath) ; 
     }
} ; 

window.multiselected = {
	data : null , 
	type : null , 
	condition : null , 
	show : function( type , selectedData ){
		//console.debug(selectedData) ; 
		var me = this ; 
		me.type = type ; 
		me.data = selectedData ; 
		me.condition = '' ;  
		
		variation.initField() ; 
        var title = 'Replace Selected' ; 
        if( me.type == 1 )
        	title = 'Remove Selected' ; 
        	
       	pwMain.hideMenu1();
        	
        var window = new Ext.Window({
            autoScroll : true , 
            width:670,
         	height:'auto',
            title:"",
            id : 'content-editor-window' , 
            //baseCls : 'ces-window' ,
            //iconCls : 'ces-window-header-icon' , 
            plain:true , 
         	modal: true,
            constrain : true ,  
         	maximizable : true  , 
         	bodyStyle : 'padding : 5px 15px 15px 15px;background-color: white ; ' , 
         	closable:true , 
            items : [ {
        		xtype : 'displayfield' , 
        		hideLabel : true , 
        		value : '<b>'+title+'</b> Content Element <img src="./typo3conf/ext/t3p_base/image/info.GIF" /> ' ,
        		style : 'font-family: \'PT Sans\';font-size: 18px;color:  rgb(20,65,138) ; margin-bottom:5px;'
      	 	},{
      	 		xtype : 'form' , 
      	 		bodyStyle : 'padding : 15px;' , 
      	 		id : 'multi-form' ,
      	 		border : true , 
      	 		hidden : true ,
	            bodyBorder : true ,  
				defaults : {
					anchor : '100%'	
				}  , 
      	 		items : [{
      	 			xtype : 'textfield' , 
	                id : 'v_contentName' , 
	                name : 'v_contentName'  , 
	                value : 'Remove'  ,  
	                fieldLabel : 'Content Name ' , 
	                labelStyle  : 'color: rgb(20,65,138) ; font-weight: bold' , 
	                hideLabel : false , 
	                ctCls  : 'cef-input'    
      	 		}]
      	 	}] ,  
            buttons: [ 
            {
	            text:'CREATE CONDITION...'  , 
	            cls: 'monoloop-black-btn' , 
	            handler: function(){
					condition.stage = 34 ; 
                    condition.preconditionStr = me.condition ; 
                    condition.openMainWindow() ; 
	            } 
	        } ,
            {
	            text:'CANCEL'  ,
	            cls: 'monoloop-black-btn' , 
	            handler: function(){ 
	            	window.close() ; 
	            }
	        },{
	            width : 40 , 
	            cls: 'monoloop-black-btn' , 
	            text:'OK'  ,
	            handler: function(b){ 
	            	var data = me.data ;  
	            	data['content_type'] = me.type ; 
	            	data['condition'] = me.condition ; 
	            	data['url'] = Ext.getCmp('pw-url').getValue(); 
            		data['beforeAfter'] = 4 ;  
	            	
                    if(  Ext.getCmp('multi-form').getForm().isValid()){
                        //alert('Yes . We pass validate ; ') ; 
                        
                        if(pwMain.crossDomain){
                            monoloop_base.showProgressbar('Save data...') ;  
                            var fromDatas = Ext.getCmp('multi-form').getForm().getValues() ; 
                            for (var attrname in fromDatas) { data[attrname] = fromDatas[attrname]; } 
                            data['eID'] = 't3p_dynamic_content_placement' ; 
                            data['pid'] = 1 ; 
                            data['cmd'] = 'contentmanager/saveMultiselected' ;  
                            data['time'] = new Date().getTime();
                            Ext.util.JSONP.request({
                                url: pwMain.baseUrl + 'index.php',
                                callbackKey: 'callback',
                                params : data , 
                                callback: function(result) {
                                	window.close() ; 
                                	mdc_contentmanager.refreshContentGrid() ; 
                                	pwMain.reStartContentGrid() ;
                                    Ext.MessageBox.hide();
                                 	//
                                }
                            }); 
                        }else{
                           
	                        //   return ; 
                           Ext.getCmp('multi-form').getForm().submit({
        	                    url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanager/saveMultiselected',
        	                    waitMsg: 'Saving data...',
                                params : data ,
        	                    success: function(simple, o){
        	                    	window.close() ; 
        	                    	 mdc_contentmanager.refreshContentGrid() ; 
                                	 pwMain.reStartContentGrid() ;
        	                    } ,
        	                    failure: function(simple, o) {
                                    Ext.MessageBox.hide();
    					            Ext.MessageBox.alert('Error', o.result.msg ) ; 
        		    			}
        	                });
                        }
    	                
                    } 
	            }
	        }] , 
            listeners : { 
            }  
        }) ; 
        
        window.show() ;
	}
} ;  

return window.selectTab ; 
//---	
}) ; 	