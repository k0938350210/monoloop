define(['require' ] ,function(require){

window.predefinedWindow = {
    window : null , 

    panel1 : null , 
    panel2 : null , 
    
    // Tree object 
    idFolder : 0,
    tree : null , 
    typeDefault : 'content',
    
    // Grid Object ; 
    contentStore : null , 
    contentGrid : null , 
    
    openWindow : function(){
        predefinedWindow.initData() ; 
        
         predefinedWindow.window = new Ext.Window({
            width:800,
         	height:400,
            title:"CONTENT LIST (PRE-DEFINED)",
            plain:true , 
         	modal: true,
         	maximizable : true  , 
            resizable : true , 
         	closable:true , 
            id : 'ces-window-predefined' , 
            //baseCls : 'ces-window' ,
            //iconCls : 'ces-window-header-icon' , 
            layout : 'border' , 
            items : [predefinedWindow.panel2 , predefinedWindow.panel1] , 
            listeners : {
                'close' : function(p){
                    selectTab.contentElementSelectorWindow.show() ; 
                } , 
                'resize' : function(w , width , height){
                    predefinedWindow.contentGrid.setHeight( height - 40) ; 
                }
            }
          }) ; 
          
        predefinedWindow.window.show() ;   
    } , 
    
    initData : function(){
        if(! pwMain.crossDomain){
            predefinedWindow.renderTree() ; 
        }else{
            predefinedWindow.renderBlankTree() ;
        }
        predefinedWindow.initGrid() ; 
        
        predefinedWindow.panel1 = new Ext.Panel({
	        region: 'center',
	        margins:'3 3 3 0', 
	        header : false  , 
            items : [predefinedWindow.contentGrid]
	    });
		 
		predefinedWindow.panel2 = new Ext.Panel({
	        region: 'west',
			title: 'Folder',
	        split: true,
	        width: 150,
			margins:'0',
	        cmargins:'0',
	        collapsible: true  , 
            items : [predefinedWindow.tree] , 
			listeners: {
				beforecollapse : function(panel){
					//if(snObj.snGrid != undefined)
					//	snObj.snGrid.setWidth(942);
				},
				
				expand : function(panel){
					//if(snObj.snGrid != undefined)
					//	snObj.snGrid.setWidth(745);
				}
			}
	    });
        
        if( pwMain.crossDomain){
            predefinedWindow.panel2.setVisible(false) ; 
        }
    } , 
    
    initGrid : function(){
        var field = [
	        	{name: 'uid', type: 'int'},
	        	{name: 'pid', type: 'int'},
	        	{name: 'type', type: 'int'},
	        	{name: 'width', type: 'int'},
	        	{name: 'height', type: 'int'},
	            {name: 'header'},
	            {name: 'tmplType'} ,
	 			{name: 'crdate', mapping: 'crdate', type: 'date', dateFormat: 'timestamp'} ,
	 			{name: 'css'}
	        ] ; 
        if(pwMain.crossDomain){
            predefinedWindow.contentStore = new Ext.data.JsonStore({
    	        root: 'topics',
    	        totalProperty: 'totalCount',
    	        idProperty: 'uid',
    	        remoteSort: true,
    	
    	        fields: field,
    	
    	        // load using script tags for cross domain, if the data in on the same domain as
    	        // this page, an HttpProxy would be better
    	        proxy: new Ext.data.ScriptTagProxy({
    	            url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanagerList&group='+selectTab.group
    	        }),
    	        
    	        baseParams:{
    				idfolder : predefinedWindow.idFolder
    			}
    	    });
        }
        else{
            predefinedWindow.contentStore = new Ext.data.JsonStore({
    	        root: 'topics',
    	        totalProperty: 'totalCount',
    	        idProperty: 'uid',
    	        remoteSort: true,
    	
    	        fields: field,
    	
    	        // load using script tags for cross domain, if the data in on the same domain as
    	        // this page, an HttpProxy would be better
    	        proxy: new Ext.data.HttpProxy({
    	            url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=contentmanagerList&group='+selectTab.group
    	        }),
    	        
    	        baseParams:{
    				idfolder : predefinedWindow.idFolder
    			}
    	    });
        }
        
        
	    predefinedWindow.contentStore.setDefaultSort('crdate', 'desc');
        
        function renderActions(value, id, r){
            var id = Ext.id();
            createSelectButton.defer(1, this, ['Select', id + '_1', r]);
            createPreviewButton.defer(1, this, ['Preview', id + '_2', r]);
            return('<table><tr><td><div id="' + id + '_1"></div></td><td><div id="' + id + '_2"></div></div></td></tr></table>');
        }
        
        function createSelectButton(value, id, record) {
            new Ext.Button({
                text: value 
                ,handler : function(btn, e) {
                    Ext.getCmp('cse-name').setValue(record.get('header')) ;
                    selectTab.contentId = record.get('uid') ;
                    predefinedWindow.window.close() ; 
                }
            }).render(document.body, id);
        }
        
        function createPreviewButton(value, id, record) {
            new Ext.Button({
                text: value 
                ,iconCls : 'pw-menu-inspect-content'  
                ,handler : function(btn, e) {
                    ml_preview_content.openMainWindow(record.get('uid')) ; 
                }
            }).render(document.body, id);
        }
        
        predefinedWindow.contentGrid = new Ext.grid.GridPanel({
	        width:'100%',
	        height:355,
	        //title:'Simple newsletter listing',
	        store:  predefinedWindow.contentStore, 
	        trackMouseOver:false,
	        //disableSelection:false,
	        loadMask: true,
	        stripeRows: true,
	        enableHdMenu: false,
	        //autoHeight: true,
	        enableDragDrop: true,
			ddGroup: 'grid2tree',
	        //clicksToEdit: 1,
 
	        columns:[{
	            header: "Description",
	            width: 120,
	            dataIndex: 'header',
	            sortable: true
	        },{
	            header: "Type",
	            dataIndex: 'tmplType',
	            width: 70,
	            sortable: true 
	        },
	   
	        {
	            id: 'crdate',  
	            header: "Date",
	            dataIndex: 'crdate',
	            width: 55,
	            renderer: Ext.util.Format.dateRenderer('d M Y'),
	            sortable: true   
	            
	        } ,{
	        	id : 'sn_action' , 
	            header: "Actions",
	            renderer: renderActions, 
	            dataIndex: 'uid',
	            width: 75,
	            sortable: false
	        }],
 
	        viewConfig: {
	            forceFit:true 
	        }, 
 
	        bbar: new Ext.PagingToolbar1({
	            pageSize: 15,
	            store: predefinedWindow.contentStore,
	            displayInfo: true,
	            displayMsg: 'Page {0} of {1}',
	            emptyMsg: "No topics to display"  
	        }), 
            
            listeners : {
                'rowclick' : function(grid , rowIndex , e){
                    var record = predefinedWindow.contentStore.getAt(rowIndex) ; 
                    Ext.getCmp('cse-name').setValue(record.get('header')) ;
                    selectTab.contentId = record.get('uid') ;
                    predefinedWindow.window.close() ; 
                }
            }
	    });
        predefinedWindow.contentGrid.store.load({params:{start:0, limit:15}});
    } , 
    
    refreshGrid : function(){
        predefinedWindow.contentGrid.store.baseParams = {
            idfolder : predefinedWindow.idFolder
        } ; 
        
        predefinedWindow.contentGrid.store.load({params:{start:0, limit:15}});
    } , 
    
    renderBlankTree : function(){
        predefinedWindow.tree = new Ext.Panel({
            html : ''
        }) ; 
    } , 
    
    renderTree : function(){
		// shorthand
	    var Tree = Ext.tree;
	
	    predefinedWindow.tree = new Tree.TreePanel({
	        useArrows: true,
	        autoScroll: true,
	        animate: true,
	        enableDD: true,
	        ddGroup: 'grid2tree',
	        containerScroll: true,
	        border: false,
	        height: 280,
	        // auto create TreeLoader
	        dataUrl: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=listFolder&typedefault='+predefinedWindow.typeDefault,
	
	        root: {
	            nodeType: 'async',
	            text: 'Root',
	            draggable: false,
	            id: '0',
	            cls: 'folder'
	        },
	        
	        listeners: {
	           
	        	click : function( node, e ){
	        		predefinedWindow.changeFolder(node);
	        	} ,
	        	
	        	dblclick : function( node, e ){
	        		predefinedWindow.changeFolder(node);
	        	} ,
	        	
	        	expandnode : function( node ){
	        		predefinedWindow.changeFolder(node);
	        	}  
	        	 
			}
	    });

	    predefinedWindow.tree.getRootNode().expand();
	} , 
    
    changeFolder : function(node){
		var id = node.getPath('id').split('/');
		id = id[id.length-1];
		var cls = node.getPath('cls').split('/');
		cls = cls[cls.length-1];

		if((predefinedWindow.idFolder != id) && (cls == 'folder')){
			predefinedWindow.idFolder = id;
			if(predefinedWindow.typeDefault == 'content')
				predefinedWindow.refreshGrid();//mdc_contentmanager.init();
			else
				predefinedWindow.refreshGrid();//mdc_placement_main.init();
		}
	}  
 } ; 
 
 //---	
}) ; 	