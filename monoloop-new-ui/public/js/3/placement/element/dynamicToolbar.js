define(function (require) {
// --- start 
window.dynamicToolbar = {
	
	id : 'd-tb' ,
	firstValidate : function(){
		return true ; 	
	} , 
	
	renderForm : function(data){
		var me = this ; 
		
		var main = new Ext.Panel({
			height : 870 , 
			border : false ,   
			baseCls : 'dtb-main' , 
			padding : 8 , 
			items : [{
				xtype : 'displayfield' , 
				value : '<img src="./typo3conf/ext/t3p_base/image/info.GIF" />' , 
				style : 'position: absolute; right : 16px ; top 15px ;'
			},{
				xtype : 'displayfield' , 
				value : '<b>1. Design </b> Slider:' , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'panel' ,
				height : 150 ,  
				border : false ,   
				baseCls : 'dtb-dimensions' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Enter a Width and Height for your Slider, or Choose \'Autosize\' for your Slider to automatically scale to fit your content." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Dimensions:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;' ,
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						name : 'cef[slider_dimensions]' , 
						id : 'slider_dimensions-manual' ,
						style : 'margin : 3px 0 0 0 ;' ,
						inputValue : 'manual' , 
						width : 15 
					},{
						xtype : 'displayfield' , 
						value : 'Width'
						
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						enableKeyEvents : true ,
						name : 'cef[slider_dimensions_width]' , 
						id : 'slider_dimensions_width',
						value : 0 , 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;x&nbsp;Height&nbsp;'
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						enableKeyEvents : true ,
						name : 'cef[slider_dimensions_height]' , 
						id : 'slider_dimensions_height' , 
						value : 0, 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : 'pixels'
					}]
				},{
					xtype : 'compositefield' , 
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						name : 'cef[slider_dimensions]' , 
						id : 'slider_dimensions-fixedTo' ,
						inputValue : 'fixedTo' , 
						style : 'margin : 3px 0 0 0 ;' ,
						width : 15 
					},{
						xtype : 'displayfield' , 
						value : 'Autofill Slider' 
					},{
						xtype:          'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        width : 70 ,
                        name: 'cef[slider_dimensions_autofil]',
                        hiddenId : 'slider_dimensions_autofil' , 
                        displayField:   'name',
                        valueField:     'value',   
                        value : 'height' ,  
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                                 'value' ,
                                'name'
                            ],
                            data: [['height', 'height'], [ 'width', 'width']]
                        }),
                        validateOnBlur : false 
					},{
						xtype : 'displayfield' , 
						value : 'to browser.'
					}]
				},{
					xtype : 'compositefield' ,  
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						id : 'slider_dimensions-fitContent' ,
						inputValue : 'fitContent' ,
						name : 'cef[slider_dimensions]' , 
						style : 'margin : 3px 0 0 0 ;' ,
						checked : true  , 
						width : 15 
					},{
						xtype : 'displayfield' , 
						value : 'Autosize Slider to fit content element.'
					} ]
				},{
                    xtype : 'container' , 
                    layout : 'column' ,  
                    items : [{
                        xtype : 'displayfield' , 
                        value : 'Maximum number of elements' , 
                        columnWidth : 0.6 , 
                    },{
                        xtype : 'spinnerfield' , 
                        allowBlank: false  ,  
                        name: 'cef[maxelements]', 
                        id : me.id + '-maxelements' ,    
                        validateOnBlur : false , 
                        columnWidth : 0.2  , 
                        enableKeyEvents : true , 
                        value : 1 , 
                        allowDecimals: false,
                        minValue: 0,
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                        listeners : {
                            'keyup' : function(textbox , e){     
                            } , 
                            'spin' : function(textbox){ 
                            }  
                        }
                    }]
                }]
			},{
				xtype : 'panel' ,
				height : 180 ,  
				border : false ,   
				baseCls : 'dtb-content' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define padding between content and Slider.\n �	Create or Add the Content for your Slider."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Content:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'container' , 
					style : 'margin : 5px 0 0 0' ,
                    layout : 'column' , 
                    items : [{
                    	xtype : 'displayfield' , 
                    	value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/dtb-padding.png" />' , 
                    	columnWidth : 0.4
                    },{
                    	xtype : 'displayfield' , 
                    	value : '&nbsp;' , 
                    	columnWidth : 0.05 
                    },{
                    	xtype : 'container' , 
	                    items : [{
	                    	xtype : 'displayfield' , 
	                    	value : 'Padding around<br>Slider Content:'  ,
	                    	style : 'margin : 0 0 10px 0 ;' 
	                    },{
	                        xtype : 'spinnerfield' ,  
	                        width : 80 ,  
	                        allowDecimals: false,
	                        name : 'cef[slider_padding]' , 
	                        id : 'slider_padding' , 
	                        minValue: 0,
	                    	decimalPrecision: 0,
	                    	incrementValue: 1,
	                    	alternateIncrementValue: 1,
	                    	accelerate: true , 
	                        value : 0
	                        
	                    }] , 
	                    columnWidth : 0.55
                    }]
				},{
					xtype : 'container' , 
					style : 'padding : 10px 15px 0 25px ;' , 
					defaults : {
						anchor : '100%'	
					} ,
					items : [{
						xtype : 'button' , 
						style : 'margin : 0 0 5px 0' , 
						width : '100%' , 
						iconCls : 'newpage-icon' ,  
						text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create New Slider Content</span> <div class="toolbar-btn"></div>' , 
						handler : function(){
							selectTab.showContentElementSelectorWindow(5,0) ;  
						}
					},{
						xtype : 'button' , 
						width : '100%' , 
						text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add Existing Content from List</span> <div class="toolbar-btn"></div>' , 
						handler : function(){
							predefinedCEWindow.openWindow() ; 
						}
					}]
				}]
			},{
				xtype : 'panel' ,
				height : 185 ,  
				border : false ,   
				baseCls : 'dtb-styling' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define the style and transparency for your Slider." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Styling:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 5px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Background Colour'
					},{
						xtype : 'colorfield' , 
						colorSelector:"mixer" ,  
						msgTarget: 'qtip' ,
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						allowBlank : true , 
						name : 'cef[slider_bgcolor]' ,  
						flex : 1 
					}]
				},{
					xtype : 'fileuploadfield' ,  
					name : 'cef[slider_backgroundimage]' , 
					id : 'slider_backgroundimage' , 
					width : '100%' , 
                    emptyText: 'Select a Background Image' ,
                    hideLabel : true ,   
                    buttonText: '' , 
                    buttonCfg: {
                        iconCls: 'upload-icon'
                    }  
				},{
					xtype : 'compositefield' ,
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        width : 115 ,
                        name: 'cef[slider_border]',
                        hiddenId : 'slider_border' , 
                        displayField:   'name',
                        valueField:     'value',   
                        emptyText : 'Select a Border' , 
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['None','None'],['solid', 'solid'],['dotted', 'dotted'],['double', 'double'],['dashed', 'dashed']]
                        }),
                        validateOnBlur : false , 
                        value : 'None'
					} ,{
						xtype : 'colorfield' , 
						colorSelector:"mixer" ,  
						emptyText : 'Border Colour' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						msgTarget: 'qtip' ,
						allowBlank : true , 
						flex : 1 , 
						name : 'cef[slider_bordercolor]' 
					}]
				},{
                    xtype : 'displayfield' , 
                    value : 'Slider Transparency: <span style="font-weight: bold;" id="slider-transparency"></span>'
                },{
                    xtype : 'sliderfield' , 
                    name:  'cef[slider_transparency]' , 
					id : 'slider_transparency' ,  
                    linkID : 'slider-transparency' , 
                    objValue : 0 , 
                    listeners : { 
                        'afterrender' : function(field){
                            if( field.objValue != ''){
                                field.setValue(field.objValue) ; 
                            }else{
                            	field.setValue(0) ; 
                            }
                            document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ; 
                            field.slider.on('change', function() { 
                                document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ;  
                            });  
                        }
                    }
                }]
			},{
				xtype : 'panel' ,
				height : 219 ,  
				border : false ,   
				baseCls : 'dtb-positioning' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define browser position for your Slider.\n�	Fix the position or scroll Slider with page."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Positioning (and Toolbar):</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'displayfield' , 
					style : 'margin : 5px 0 0 0 ;' , 
					value : 'Select browser edge for start position (click)'
				},{
					xtype : 'panel' , 
					baseCls : 'dtb-positioning-bg' ,
					height : 106  , 
					html  : '<div class="" id="dtb-pos-Left" onclick="Ext.getCmp(\'dtb_position\').setValue(\'Left\');"></div><div class="" id="dtb-pos-Right" onclick="Ext.getCmp(\'dtb_position\').setValue(\'Right\');"></div><div class="" id="dtb-pos-Top" onclick="Ext.getCmp(\'dtb_position\').setValue(\'Top\');"></div><div class="" id="dtb-pos-Bottom" onclick="Ext.getCmp(\'dtb_position\').setValue(\'Bottom\');"></div>'
				},{
					xtype : 'hidden' , 
					id : 'dtb_position' , 
					name : 'cef[slider_position]' ,
				 	setValue : function(val){
						dynamicToolbar.posClick(val) ; 
						this.setRawValue(val);
						return this; 
					} 
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Position Slider:'
					},{
						xtype : 'spinnerfield' ,  
                        width : 40 ,  
                        allowDecimals: false,
                        minValue: 0,
                        name : 'cef[slider_positioning_slider]' , 
                        id : 'slider_positioning_slider' , 
                    	decimalPrecision: 0,
                    	incrementValue: 1,
                    	alternateIncrementValue: 1,
                    	accelerate: true , 
                        value : 0
					},{
						xtype : 'displayfield' , 
						value : 'pixels from' 
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        flex : 1 , 
                        name: 'cef[slider_positioning_pixelform]',
                        hiddenId : 'slider_positioning_pixelform' , 
                        displayField:   'name',
                        valueField:     'value',    
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['Top','Top'],['Left','Left'],['Right','Right'],['Bottom', 'Bottom']]
                        }),
                        validateOnBlur : false 
					}]
				},{
					xtype : 'compositefield' ,
					items : [{
						xtype : 'radio' , 
						width : 15 ,
						style : 'margin : 3px 0 0 0 ;' ,
						name : 'cef[slider_positioning_type]' , 
						id : 'slider_positioning_type-Fixed' , 
						inputValue : 'Fixed' , 
						checked : true 
					},{
						xtype : 'displayfield' , 
						value : 'Fix position'
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;' , 
						width : 35
					},{
						xtype : 'radio' , 
						style : 'margin : 3px 0 0 0 ;' ,
						width : 15 ,
						name : 'cef[slider_positioning_type]' , 
						id : 'slider_positioning_type-Scroll' , 
						inputValue : 'Scroll'
					},{
						xtype : 'displayfield' , 
						value : 'Scroll with page'
					}]
				}]
			},{
				xtype : 'panel' ,
				height : 114 ,  
				border : false ,   
				padding : '5px 10px 5px 10px' ,
				baseCls : 'dtb-actions' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png"  title="�	Define the sliding speed for your Slider"  />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Slider Action:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Direction of Slider:'
					},{
						xtype : 'textfield' , 
						width : 150 , 
						disabled : true , 
						enableKeyEvents : true ,
						id : 'slider_direction' , 
						style : 'color : rgb(239,125,0) ; font-weight : bold ;' , 
						value : 'Left to Right', 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					}]
				},{
                    xtype : 'displayfield' , 
                    value : 'Slider Speed: <span style="font-weight: bold;" id="slider-speed"></span>'
                },{
                    xtype : 'sliderfield' , 
                    name:  'cef[slider_speed]' ,  
                    id : 'slider_speed' , 
                    linkID : 'slider-speed' , 
                    objValue : 1 , 
                    listeners : { 
                        'afterrender' : function(field){
                            if( field.objValue != ''){
                                field.setValue(field.objValue) ; 
                            }else{
                            	field.setValue(0) ; 
                            } 
                            document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ; 
                            field.slider.on('change', function() { 
                                document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ;  
                                
                                if( field.getValue() == 0){
                                	field.setValue(1) ; 
                                }
                            });  
                        }
                    }
                }]
			}]
		}) ; 
		
		Ext.getCmp(contentEditorCE.id + '-form').add(main) ; 
		
		var main2 =  new Ext.Panel({
			height : 730 , 
			border : false ,   
			baseCls : 'dtb2-main' , 
			padding : 8 , 
			items : [{
				xtype : 'displayfield' , 
				value : '<img src="./typo3conf/ext/t3p_base/image/info.GIF" />' , 
				style : 'position: absolute; right : 10px ; top 15px ;'
			},{
				xtype : 'displayfield' , 
				value : '<b>2. Set</b> Toolbar:' , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'panel' ,
				height : 137 ,  
				border : false ,   
				baseCls : 'dtb2-dimensions' ,
				padding : '5px 10px 5px 10px' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Enter a Width and Height for your Toolbar, or Choose \'Autosize\' for your Toolbar to automatically scale to fit your content." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Toolbar Dimensions:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px 0 0 0 ;' ,
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						name : 'cef[toolbar_dimensions]' , 
						id : 'toolbar_dimensions-manual' , 
						inputValue : 'manual' , 
						style : 'margin : 3px 0 0 0 ;' ,
						width : 15 
					},{
						xtype : 'displayfield' , 
						value : 'Width'
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						enableKeyEvents : true ,
						name  : 'cef[toolbar_width]' ,
						value : 0, 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;x&nbsp;Height&nbsp;'
					},{
						xtype : 'numberfield' , 
						width : 40 , 
						enableKeyEvents : true ,
						name  : 'cef[toolbar_height]' , 
						value : 0, 
						listeners : {
	                        'keypress' : function(textField ,e){
	                            if(e.keyCode == e.ENTER) {
	                                contentEditorCE.saveProcess(0) ;     
	                            }
	                        }
	                    }
					},{
						xtype : 'displayfield' , 
						value : 'pixels'
					}]
				},{
					xtype : 'compositefield' , 
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						name : 'cef[toolbar_dimensions]' , 
						id : 'toolbar_dimensions-fixedTo' , 
						inputValue : 'fixedTo' , 
						style : 'margin : 3px 0 0 0 ;' ,
						width : 15 
					},{
						xtype : 'displayfield' , 
						value : 'Autofil Slider' 
					},{
						xtype:          'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        width : 70 ,
                        name: 'cef[toolbar_dimensions_autofil]',
                        hiddenId : 'toolbar_dimensions_autofil' , 
                        displayField:   'name',
                        valueField:     'value',   
                        value : 'height' ,  
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                                 'value' ,
                                'name'
                            ],
                            data: [['height', 'height'], [ 'width', 'width']]
                        }),
                        validateOnBlur : false 
					},{
						xtype : 'displayfield' , 
						value : 'to browser.'
					}]
				},{
					xtype : 'compositefield' ,  
					height : 29 , 
					items : [{
						xtype : 'radio' , 
						name : 'cef[toolbar_dimensions]' ,
						id : 'toolbar_dimensions-fitContent' , 
						inputValue : 'fitContent' ,  
						style : 'margin : 3px 0 0 0 ;' ,
						checked : true , 
						width : 15 
					},{
						xtype : 'displayfield' , 
						value : 'Autosize Slider to fit content element.'
					} ]
				}]
			},{
				xtype : 'panel' ,
				height : 106 ,  
				border : false ,   
				padding : '5px 10px 5px 10px' ,
				baseCls : 'dtb2-content' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Create or Add the Text to appear on your Toolbar." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Toolbar Content:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'container' , 
					style : 'margin : 15px 0 0 0' ,
                    layout : 'column' , 
                    items : [{
                    	xtype : 'displayfield' , 
                    	columnWidth : 0.15
                    },{
                    	xtype : 'container' , 
                    	columnWidth : 0.7 , 
                    	items : [{
                    		xtype : 'button' ,  
							width : '100%' ,  
							text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create / Edit Toolbar Text</span> <div class="toolbar-btn"></div>' , 
							handler : function(){
								//---- Open toolbar text ; 
								me.openToolbalTextEditor(Ext.getCmp('toolbar_text').getValue()) ; 
							}
                    	},{
                    		xtype : 'compositefield' , 
                    		style : 'margin : 10px 0 0 0' ,
                    		items : [{
                    			xtype : 'displayfield' , 
                    			value : 'Text:'
                    		},{
                    			xtype : 'textfield' , 
                    			readOnly : true , 
                    			name : 'cef[toolbar_text]' , 
                    			id : 'toolbar_text' ,  
                    			style : 'color : black ;' , 
                    			flex : 1 , 
								listeners : {
			                        'keypress' : function(textField ,e){
			                            if(e.keyCode == e.ENTER) {
			                                contentEditorCE.saveProcess(0) ;     
			                            }
			                        }
			                    }
                    		},{
                    			xtype : 'button' , 
                    			text : '<img src="./typo3conf/ext/t3p_base/image/edit2.png" height="16px" />' ,    
    							width : 22 , 
    							handler : function(){
    								me.openToolbalTextEditor(Ext.getCmp('toolbar_text').getValue()) ; 
    							}
                    		}]
                    	}]
                    },{
                    	xtype : 'displayfield' , 
                    	columnWidth : 0.15
                    }]
				}]
			},{
				xtype : 'panel' ,
				height : 185 ,  
				border : false ,
				padding : '5px 10px 5px 10px' ,
				baseCls : 'dtb2-styling' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png"  title="�	Define the style and transparency for your Toolbar."/>' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Toolbar Styling:</b>' ,  
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;margin-bottom : 20px;'
				},/*{
					xtype : 'compositefield' , 
					style : 'margin : 5px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Background Colour'
					},{
						xtype : 'colorfield' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						colorSelector:"mixer" ,  
						msgTarget: 'qtip' ,
						allowBlank : true , 
						name : 'cef[toolbar_bgcolor]' , 
						flex : 1 
					}]
				},*/{
					xtype : 'fileuploadfield' ,  
					name : 'cef[toolbar_backgroundimage]' , 
					id : 'toolbar_backgroundimage' , 
					width : '100%' , 
                    emptyText: 'Select a Background Image' ,
                    hideLabel : true ,   
                    buttonText: '' , 
                    buttonCfg: {
                        iconCls: 'upload-icon'
                    }  
				},{
					xtype : 'compositefield' ,
					style : 'margin : 10px 0 10px 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Border'
					},{
						xtype: 'combo',  
                        allowBlank: true  ,
                        triggerAction:  'all',
                        mode: 'local',
                        editable:       false, 
                        width : 115 ,
                        name: 'cef[toolbar_border]',
                        hiddenId : 'toolbar_border' , 
                        displayField:   'name',
                        valueField:     'value',   
                        emptyText : 'Select a Border' , 
                        store: new Ext.data.ArrayStore({ 
                            fields: [
                            	'value' ,
                                'name'
                            ],
                            data: [['None','None'],['solid', 'solid'],['dotted', 'dotted'],['double', 'double'],['dashed', 'dashed']]
                        }),
                        validateOnBlur : false , 
                        value : 'None'
					} /*,{
						xtype : 'colorfield' , 
						triggerColorClass : 'x-cpm-field-trigger-color1' , 
						colorSelector:"mixer" ,  
						emptyText : 'Border Colour' , 
						allowBlank : false ,
						msgTarget: 'qtip' ,
						flex : 1 , 
						name : 'cef[toolbar_bordercolor]' 
					}*/]
				},{
                    xtype : 'displayfield' , 
                    value : 'Toolbar Transparency: <span style="font-weight: bold;" id="toolbar-transparency"></span>'
                },{
                    xtype : 'sliderfield' , 
                    name:  'cef[toolbar_transparency]' , 
					id : 'toolbar_transparency' ,  
                    linkID : 'toolbar-transparency' , 
                    objValue : 0 , 
                    listeners : { 
                        'afterrender' : function(field){
                            if( field.objValue != ''){
                                field.setValue(field.objValue) ; 
                            }else{
                            	field.setValue(0) ; 
                            } 
                            document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ; 
                            field.slider.on('change', function() { 
                                document.getElementById(field.linkID).innerHTML = field.getValue() + '%' ;  
                            });  
                        }
                    }
                }]
			},{
				xtype : 'panel' ,
				height : 68 ,  
				border : false ,   
				padding : '5px 10px 5px 10px' ,
				baseCls : 'dtb2-positioning' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Toolbar Positioning:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px  0 0 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Toolbar Action:'
					},{
						xtype : 'radio' , 
						style : 'margin : 3px 0 0 0 ;' ,
						name : 'cef[toolbar_positioning]' ,
						id :  'toolbar_positioning-push' , 
						inputValue : 'push' , 
						checked : true , 
						width : 10
					},{
						xtype : 'displayfield' , 
						value : 'Push website&nbsp;&nbsp;&nbsp;'
					},{
						xtype : 'radio' , 
						style : 'margin : 3px 0 0 0 ;' ,
						name : 'cef[toolbar_positioning]' , 
						id :  'toolbar_positioning-overlay' , 
						inputValue : 'overlay' , 
						width : 10
					},{
						xtype : 'displayfield' , 
						value : 'Overlay'
					}]
				}]
			},{
				xtype : 'panel' ,
				height : 68 ,  
				border : false ,   
				padding : '5px 10px 5px 10px' ,
				baseCls : 'dtb2-actions' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Define a trigger method for your Toolbar." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Toolbar Actions:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'compositefield' , 
					style : 'margin : 10px  0 0 0 ;' , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Toolbar Action:'
					},{
						xtype : 'radio' , 
						style : 'margin : 3px 0 0 0 ;' ,
						name : 'cef[toolbar_action]' , 
						id :  'toolbar_action-Click' , 
						checked : true , 
						inputValue : 'Click' , 
						width : 10
					},{
						xtype : 'displayfield' , 
						value : 'On click&nbsp;&nbsp;&nbsp;'
					},{
						xtype : 'radio' , 
						style : 'margin : 3px 0 0 0 ;' ,
						name : 'cef[toolbar_action]' , 
						id :  'toolbar_action-Hover' , 
						inputValue : 'Hover' , 
						width : 10
					},{
						xtype : 'displayfield' , 
						value : 'On hover'
					}]
				}]
			},{
				xtype : 'panel' ,
				height : 104 ,  
				border : false ,   
				padding : '5px 10px 5px 10px' ,
				baseCls : 'dtb2-advance' ,
				items : [{
					xtype : 'displayfield' , 
					value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/help-icon2.png" title="�	Customize Toolbar and Slider appearance using HTML/CSS editor." />' , 
					style : 'position: absolute; right : 16px ; top 15px ;'
				},{
					xtype : 'displayfield' , 
					value : '<b>Advance Options:</b>' , 
					style : 'font-family: \'PT Sans\';font-size: 12px ;color: rgb(20,65,138) ;'
				},{
					xtype : 'container' , 
					style : 'margin : 10px 0 0 0' ,
                    layout : 'column' , 
                    items : [{
                    	xtype : 'displayfield' , 
                    	columnWidth : 0.15
                    },{
                    	xtype : 'container' , 
                    	columnWidth : 0.7 , 
                    	items : [{
                    		xtype : 'button' ,  
							width : '100%' ,  
							text : '<span class="toolbar-label" style="color:  rgb(101,101,101) ;font-weight: bold;">Customize HTML code</span> <div class="toolbar-btn"></div>' , 
							handler : function(){
								
							}
                    	},{
                    		xtype : 'button' ,  
                    		style : 'margin : 10px 0 0 0' ,
							width : '100%' ,  
							text : '<span class="toolbar-label" style="color:  rgb(101,101,101) ;font-weight: bold;">Customize CSS code</span> <div class="toolbar-btn"></div>' , 
							handler : function(){
								
							}
                    	}]
                    },{
                    	xtype : 'displayfield' , 
                    	columnWidth : 0.15
                    }]
				}]
			}]
		});
		
		Ext.getCmp(contentEditorCE.id + '-form').add(main2) ; 
		
		Ext.getCmp(contentEditorCE.id + '-top-addin').add({
			xtype : 'compositefield' , 
			width : '100%' ,
			items : [{
				xtype : 'displayfield' , 
				value : '<b>1. Design </b> Slider:' , 
				width : 120 , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'displayfield' , 
				style : 'margin : 5px 0 0 0 ;' , 
				width : 20 , 
				value : '<img src="./typo3conf/ext/t3p_dynamic_content_placement/res/images/arrow.png" />'
			},{
				xtype : 'displayfield' , 
				value : '<b>2. Set</b> Toolbar:' , 
				width : 100 , 
				style : 'font-family: \'PT Sans\';font-size: 16px;color:  rgb(20,65,138) ;'
			},{
				xtype : 'button' , 
				style : 'margin : 0 0 5px 0' ,  
				iconCls : 'newpage-icon' ,  
				width : 200 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Create New Slider Content</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					selectTab.showContentElementSelectorWindow(5,0) ;  
				}
			},{
				xtype : 'button' ,  
				width : 170 , 
				text : '<span class="toolbar-label" style="color:  rgb(20,65,138) ;font-weight: bold;">Add Content from List</span> <div class="toolbar-btn"></div>' , 
				handler : function(){
					predefinedCEWindow.openWindow() ; 
				}
			}]		
		}) ;
		
		// Process Data ; 
		//console.debug(data) ; 
		
		
		//console.debug( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField("cef[slider_bgcolor]").setValue('#000000') ) ;   
	} , 
	
	afterrender : function(data){ 
		Ext.getCmp('dtb_position').setValue('Top');
		
		for( var i = 0 ; i < data.content.length ; i++){
			var field = data.content[i] ; 
			if( field['type'] == 'ce'){
				continue ; 
			}
			
			if( field['value'] == '' || field['value'] == null){
				continue ; 
			}
			
			if( field['el'] == 'slider_dimensions' || field['el'] == 'slider_positioning_type' || field['el'] == 'toolbar_dimensions' || field['el'] == 'toolbar_positioning' || field['el'] == 'toolbar_action' || field['el'] == 'slider_positioning_type' ){ 
				if( Ext.getCmp(field['el']+ '-' + field['value']) != undefined)
					Ext.getCmp(field['el']+ '-' + field['value']).setValue(true) ;   
			}else{
				if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef['+field['el']+']') != null)
					Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef['+field['el']+']').setValue(field['value']) ; 
			}
		}
 
	} , 
	
	posClick : function(posID){
		// remove all element class . 
		posID = posID.charAt(0).toUpperCase() + posID.slice(1);
		var me = this ; 
		Ext.get('dtb-pos-Left').removeClass('active') ; 
		Ext.get('dtb-pos-Right').removeClass('active') ; 
		Ext.get('dtb-pos-Top').removeClass('active') ; 
		Ext.get('dtb-pos-Bottom').removeClass('active') ; 
		
		//Ext.getCmp('dtb_position').setValue(posID);
		Ext.get('dtb-pos-' + posID).addClass('active') ;
		
		me.chagePos(posID) ; 
	} , 
	
	chagePos : function(posID){
		//console.debug(Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue()) ;
		//Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.removeAll() ; 
		if( posID == 'Top'){  
			Ext.getCmp('slider_direction').setValue('Top to Bottom') ;  
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.loadData([['Left','Left'],['Right','Right']]) ; 
			
			if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue() != 'Right')
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').setValue('Left') ; 
		}else if(posID == 'Left'){  
			Ext.getCmp('slider_direction').setValue('Left to Right') ; 
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.loadData([['Top','Top'],['Bottom','Bottom']]) ; 
			
			if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue() != 'Bottom')
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').setValue('Top') ; 
		}else if(posID == 'Right'){  
			Ext.getCmp('slider_direction').setValue('Right to Left') ; 
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.loadData([['Top','Top'],['Bottom','Bottom']]) ; 
			
			if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue() != 'Bottom')
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').setValue('Top') ; 
		}else{ 
			Ext.getCmp('slider_direction').setValue('Bottom to Top') ; 
			Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').store.loadData([['Left','Left'],['Right','Right']]) ; 
			
			if( Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').getValue() != 'Right')
				Ext.getCmp(contentEditorCE.id + '-form').getForm().findField('cef[slider_positioning_pixelform]').setValue('Left') ; 
		}
	} , 
	
	openToolbalTextEditor : function(data){
		var w = new Ext.Window({ 
            width:400,
            height : 300 ,  
            layout : 'fit' ,
            title : 'Toolbar text editor' ,  
            modal: true,
         	maximizable : false  , 
         	closable:true , 
            items : [{
                xtype : 'textarea' , 
                value :  data
            }] , 
            buttons : [{
                text : 'CANCEL' , 
                handler : function(){ 
                    w.close() ; 
                }
            },{
                text : 'OK' , 
                handler : function(){
                    var h = w.findByType('textarea') ; 
                    Ext.getCmp('toolbar_text').setValue(h[0].getValue()) ; 
                    w.close() ;
                }
            }]
        }) ; 
        
        w.show() ;
	}
	
} ; 

return window.dynamicToolbar ; 
//---	
}) ; 