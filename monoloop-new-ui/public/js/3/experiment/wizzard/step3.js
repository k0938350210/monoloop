define(function (require) {
// --- start	
var me = {
    xtype : 'container' , 
	layout : 'border' , 
	border : false , 
	id : 'wizard-step-3' ,
	cls : 'wizard-step' , 
    items : [{
		xtype : 'form' ,  
		region : 'center' , 
	    defaults : {
	        hideLabel : true , 
	        anchor : '100%'
	    } , 
	    border : false , 
	    bodyCssClass : 'main-form' , 
        items : [{
	        xtype : 'displayfield' ,
	        value : 'step 3 of 6' , 
	        anchor : '0%' ,
	        style : 'position : absolute ; right : 0 ; top : 0 ;'  
	    },{
	        xtype : 'displayfield' , 
	        value : '<h3>Goal</h3>'
	    },{
	        xtype : 'displayfield' , 
	        value : 'Please define a goal for your target segment:'
	    },{
            xtype : 'container' , 
            layout : 'column' , 
            items : [{
                xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                    xtype : 'container' , 
                    height : 256 , 
                    autoScroll : true , 
                    id : 'step3-list' , 
					cls : 'list-selector' , 
					items : []
                },{
                    xtype : 'displayfield' ,
                    value : '<a href="#" class="create-new-btn" onclick="Ext.getCmp(\'wizard-step-3\').createNewGoal() ; return false ;">Create new goal</a>'
                }]
            },{
            	xtype : 'container' , 
                columnWidth : .04 , 
                html : '&nbsp;'
            },{
                xtype : 'container' , 
                id : 'goal-description' , 
                columnWidth : .48 , 
                html : '<h4>Abount goals</h4>' + 
                		'<p>Setting goals is a way to calculate progress and achievements from your website visitors (e.g. visitors who viewed a certain page, amount of time used viewing a specific category, keywords used to search a website, etc).</p></br>' + 
                		'<h4>Examples of goals</h4>' + 
                		'<p>- amount of time visitor spent on a page </br>- a referral link the visitor came from </br>- search keywords used to reach site </br>- whenever a purchase is made</p>'
            }]
	    }]
    },{
		xtype : 'container' , 
		region : 'south' ,
		border : false , 
		height : 70 ,  
		layout : 'column' , 
		cls : 'main-form' , 
		items : [{
			xtype : 'container' , 
			columnWidth : 0.65 	, 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-3\').previousStep() ; return false ; "><div class="previous-btn"><span class="icon-chevron-left"></span></div>'
		},{
			xtype : 'container' , 
			columnWidth : 0.35 , 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-3\').nextStep() ; return false ; "><div class="next-btn"><div class="col-a">Next</div><div class="col-b"><span class="icon-chevron-right"></span></div></div></a>' 	
		}]
	}] , 
	
	// event 
	
	listeners : {
		afterrender : function(f){
			f.loadList() ; 	
		}
	} , 
    
    // custom function 
    createNewGoal : function(){
    	Ext.getCmp('wizard-container').removeAll() ;  
	    require(['experiment/wizzard/step3_1' ],function(wizzard){ 
	        Ext.getCmp('wizard-container').add(wizzard) ;   
	        Ext.getCmp('wizard-container').doLayout() ;       
	    }) ;   
    } , 
    
    nextStep : function(){  
		var form = Ext.getCmp('wizard-step-3').findByType('form')[0].getForm().getValues() ; 
		if( form['goal'] == undefined){
        	Ext.Msg.alert('Error', 'Please select goal.');
        }else{
        	Ext.getCmp('wizzard').data['goal'] = form['goal'] ; 
			
    		
    		if( Ext.getCmp('wizzard').data['uid'] == undefined || Ext.getCmp('wizzard').data['uid'] == 0 ){
    			 monoloop_base.showProgressbar('Save draft ...') ; 
    			 Ext.Ajax.request({
					url: 'index.php/firststepsave/?eID=ml_experiment'  , 
					method: 'POST',
					timeout: '30000',
		            params: Ext.getCmp('wizzard').data , 
		            success: function(xhr) { 
						var jsonData = Ext.util.JSON.decode(xhr.responseText) ;  
						Ext.getCmp('wizzard').data['uid'] = jsonData.uid ; 
 						Ext.MessageBox.hide();
 						Ext.getCmp('wizzard').addCompleteStep(3) ;  
    					Ext.getCmp('wizzard').changeStep(3,4) ; 
					}, failure: function() {
		                Ext.MessageBox.hide();
					    Ext.MessageBox.alert('Error', '#1201' ) ; 
					}
				}) ; 
    		}else{
    			Ext.getCmp('wizzard').addCompleteStep(3) ;  
    			Ext.getCmp('wizzard').changeStep(3,4) ; 
    		}
       	} 
    } , 
    
    previousStep : function(){ 
    	Ext.getCmp('wizzard').changeStep(3,2) ; 
    } , 
    
    loadList : function(){
    	var me = this ; 
    	monoloop_base.showProgressbar('Loading goal ...') ; 
		Ext.Ajax.request({
			url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=goals/getlist'  , 
			method: 'POST',
			timeout: '30000',
            params:   {
            	dir : 'DESC' , 
            	idfolder : 0 , 
            	limit : 1000 , 
            	sort : 'crdate' , 
            	start : 0 
            } , 
			success: function(xhr) { 
				var jsonData = Ext.util.JSON.decode(xhr.responseText) ;  
                Ext.MessageBox.hide(); 
                me.listData = jsonData ; 
                var goalID = Ext.getCmp('wizzard').data['goal'] ; 
                //console.debug(goalID) ; 
                var check = false ; 
                for( var i = 0 ; i < jsonData.topics.length ; i++ ){
                	var goal = jsonData.topics[i]  ; 
                	if( goal.uid == goalID)
                		check = true ; 
               		else
               			check = false ;  
						   
					if( goal.hidden == '1')
						continue ; 	
               			
                	Ext.getCmp('step3-list').add({
						xtype : 'container' , 
						cls : 'item-selector' , 
						layout : 'hbox' , 
						items : [{
							xtype : 'radio' , 
							name : 'goal' , 
							checked : check , 
							inputValue : goal.uid , 
							listeners : {
								check : function(c , checked){ 
								 	if( checked ){ 
								 		c.findParentByType('container').addClass('active') ;   
							 		}else{ 
							 			c.findParentByType('container').removeClass('active') ;  
						 			}
								}
							}
						},{
							xtype : 'displayfield' , 
							value : goal.name
						}]
					}) ; 
                } 
                Ext.getCmp('step3-list').doLayout() ; 
                 
            }, failure: function() {
                Ext.MessageBox.hide();
			    Ext.MessageBox.alert('Error', '#1201' ) ; 
			}
        }) ; 
    	 
    }
} ; 
return me ; 
// --- end
}) ; 