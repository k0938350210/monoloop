define(function (require) {
// --- start	
var me = {
    xtype : 'container' , 
	layout : 'border' , 
	border : false , 
	id : 'wizard-step-5' ,
	cls : 'wizard-step' , 
    items : [{
		xtype : 'form' ,  
		region : 'center' , 
	    defaults : {
	        hideLabel : true , 
	        anchor : '100%'
	    } , 
	    border : false , 
	    bodyCssClass : 'main-form' , 
        items : [{
	        xtype : 'displayfield' ,
	        value : 'step 5 of 6' , 
	        anchor : '0%' ,
	        style : 'position : absolute ; right : 0 ; top : 0 ;'  
	    },{
	        xtype : 'displayfield' , 
	        value : '<h3>Control Group</h3>'
	    },{
	        xtype : 'displayfield' , 
	        value : 'Please define the size of control group:'
	    },{
            xtype : 'container' , 
            layout : 'column' , 
            style : 'margin : 10px 0 0 0 ; ' , 
            items : [{
                xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                	xtype : 'displayfield' , 
                	value : 'Size of the control group' 
                },{
                	xtype : 'compositefield' , 
                	style : 'margin : 5px 0 0 0 ; ' , 
                	items : [{
                		xtype : 'numberfield' , 
                		name : 'controlGroupSize' , 
                		maxValue : 100 , 
                		minValue : 0 , 
                		allowBlank : false , 
                		width : '25%'
                	},{
                		xtype : 'displayfield' , 
                		value : '%'
                	}]
                },{
                	xtype : 'displayfield' , 
                	value : '<br/>Days visitors stay in control group:'
                },{
                	xtype : 'compositefield' , 
                	style : 'margin : 5px 0 0 0 ; ' , 
                	items : [{
                		xtype : 'numberfield' , 
                		name : 'controlGroupDays' ,
                		allowBlank : false , 
                		minValue : 0 , 
                		width : '25%'
                	},{
                		xtype : 'displayfield' , 
                		value : 'Days'
                	}]
                },{
                	xtype : 'displayfield' , 
                	value : '<br/>Action when significant:'
                },{
                    xtype : 'combo' , 
                    mode: 'local', 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,
                    name: 'significantAction',  
                    displayField:   'name',
                    valueField:     'value',
                    hiddenName :'significantAction' ,  
                	store: new Ext.data.JsonStore({
                        fields : ['name',  {name: 'value', type: 'int'}],
                        data   : [
                            {name : 'Do nothing (continue experiment)',   value: 0 },
                            {name : 'Serve Winner',   value: 1 } ,
                            {name : 'Serve Original',   value: 2 }  
                        ]
                    })  
                }]
            },{
            	xtype : 'container' , 
                columnWidth : .04 , 
                html : '&nbsp;'
            },{
                xtype : 'container' , 
                id : 'goal-description' , 
                columnWidth : .48 , 
                html : '<h4>Abount control groups</h4>' + 
                		'<p>Setting goals is a way to calculate progress and achievements from your website visitors (e.g. visitors who viewed a certain page, amount of time used viewing a specific category, keywords used to search a website, etc).</p></br>' 
            }]
	    }
        ]
    },{
		xtype : 'container' , 
		region : 'south' ,
		border : false , 
		height : 70 ,  
		layout : 'column' , 
		cls : 'main-form' , 
		items : [{
			xtype : 'container' , 
			columnWidth : 0.65 	, 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-5\').previousStep() ; return false ; "><div class="previous-btn"><span class="icon-chevron-left"></span></div>'
		},{
			xtype : 'container' , 
			columnWidth : 0.35 , 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-5\').nextStep() ; return false ; "><div class="next-btn"><div class="col-a">Next</div><div class="col-b"><span class="icon-chevron-right"></span></div></div></a>' 	
		}]
	}]   , 
	
	listeners : {
		afterrender : function(){
			var form = Ext.getCmp('wizard-step-5').findByType('form')[0].getForm() ; 
			if( Ext.getCmp('wizzard').data['controlGroupSize'] != undefined)
				form.findField('controlGroupSize').setValue(Ext.getCmp('wizzard').data['controlGroupSize']) ; 
			if( Ext.getCmp('wizzard').data['controlGroupDays'] != undefined)
				form.findField('controlGroupDays').setValue(Ext.getCmp('wizzard').data['controlGroupDays']) ; 
            if( Ext.getCmp('wizzard').data['significantAction'] != undefined)
				form.findField('significantAction').setValue(Ext.getCmp('wizzard').data['significantAction']) ; 
                
		}
	} , 
    
    
    // custom function  
    
    previousStep : function(){ 
    	Ext.getCmp('wizzard').changeStep(5,4) ; 
    } ,
    
    nextStep : function(){ 
    	var form = Ext.getCmp('wizard-step-5').findByType('form')[0].getForm() ;  
    	if( form.isValid()){
    		var vals = form.getValues() ;   
    		Ext.getCmp('wizzard').data['controlGroupSize'] = vals['controlGroupSize'] ; 
    		Ext.getCmp('wizzard').data['controlGroupDays'] = vals['controlGroupDays'] ;  
            Ext.getCmp('wizzard').data['significantAction'] = vals['significantAction'] ;  
    		Ext.getCmp('wizzard').addCompleteStep(5) ; 
    		Ext.getCmp('wizzard').changeStep(5,6) ;
    	}
         
    }
    
} ; 
return me ; 
// --- end
}) ; 