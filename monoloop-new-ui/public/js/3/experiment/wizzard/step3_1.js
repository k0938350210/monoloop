define(function (require) {
// --- start

var obj = {
	xtype : 'container' ,
	border : false ,
	id : 'wizard-step-3_1' ,
	cls : 'wizard-step' ,
	items : [{
    	xtype : 'displayfield'  ,
		value : '<h1>Goal</h1>' ,
		cls : 'main-form'
    },{
    	xtype : 'form' ,
    	border : true ,
    	defaults : {
	        hideLabel : true ,
	        anchor : '100%'
	    } ,
	    bodyCssClass : 'main-form-segment' ,
	    items : [{
	    	xtype : 'displayfield'  ,
	    	value : 'Goal name:'
	    },{
	    	xtype : 'textfield' ,
	    	name : 'name' ,
	    	allowBlank : false
	    },{
	    	xtype : 'displayfield' ,
	    	value : 'Page URL:'
	    },{
	    	xtype : 'textfield' ,
	    	name : 'url' ,
	    	allowBlank : false
	    },{
	    	xtype : 'displayfield' ,
	    	value : 'URL option:'
	    },{
            xtype:          'combo',
            mode:           'local',
            labelStyle: 'width:120px',
            allowBlank: false  ,
            triggerAction:  'all',
            editable:       false,
            name: 'urlOption',
            fieldLabel:     'URL Options',
            displayField:   'name',
            valueField:     'value',
            hiddenName :'urlOption' ,
        	store: new Ext.data.JsonStore({
                fields : ['name',  {name: 'value', type: 'int'}],
                data   : [
                    {name : 'Exact Match',   value: 0 },
                    {name : 'Starts with',   value: 2 }
                ]
            })
         },{
         	xtype : 'checkbox' ,
         	name : 'incWWW' ,
         	boxLabel : 'Include WWW'
         },{
	    	xtype : 'container' ,
	    	layout : 'column' ,
	    	items : [{
	    		xtype : 'displayfield' ,
	    		columnWidth : 0.2 ,
	    		value : '&nbsp;'
	    	},{
	    		xtype : 'displayfield' ,
            	value : '<a href="#" class="create-new-btn" onclick="Ext.getCmp(\'wizard-step-3_1\').openCondition() ; return false ;">Set the conditions for your goal <span class="icon icon-external-link"></span></a>' ,
            	columnWidth : 0.6
	    	},{
	    		xtype : 'displayfield' ,
	    		columnWidth : 0.2 ,
	    		value : '&nbsp;'
	    	}]
	    },{
	    	xtype : 'container' ,
	    	layout : 'column'  ,
	    	style : 'margin : 10px 0 0 0 ;' ,
	    	items : [{
	    		xtype : 'displayfield' ,
	    		columnWidth : 0.5 ,
	    		value : '&nbsp;'
	    	},{
	    		xtype : 'displayfield' ,
	    		columnWidth : 0.24 ,
	    		value : '<a href="#" onclick="Ext.getCmp(\'wizard-step-3_1\').previousStep() ; return false ; "><div class="previous-btn-segment"><span class="icon-chevron-left"></span>&nbsp;Cancel</div>'
	    	},{
	    		xtype : 'displayfield' ,
	    		columnWidth : 0.02 ,
	    		value : '&nbsp;'
	    	},{
	    		xtype : 'displayfield' ,
	    		columnWidth : 0.24 ,
	    		value : '<a href="#" onclick="Ext.getCmp(\'wizard-step-3_1\').save() ; return false ; "><div class="next-btn-segment">Save</div>'
	    	}]
	    }]
    }] ,

    // custom method

    openCondition : function(){
    	var me = this ;
    	var conditionStr  = me.condition ;
    	require(['placement/conditionWindow' ],function(){
    		condition.stage = 42 ;
            condition.preconditionStr = conditionStr ;
            condition.openMainWindow() ;
   		}) ;
    },

    saveCondition : function(conditionStr){
    	var me = this ;
    	me.condition = conditionStr ;
    	console.debug( me.condition ) ;
    },

    previousStep : function(){
    	Ext.getCmp('wizard-container').removeAll() ;
        require(['experiment/wizzard/step3' ],function(wizzard){
            Ext.getCmp('wizard-container').add(wizzard) ;
            Ext.getCmp('wizard-container').doLayout() ;
        }) ;
    },

    save : function(){
    	var me = this ;
    	var form = Ext.getCmp('wizard-step-3_1').findByType('form')[0].getForm() ;
    	if( form.isValid()){
    		var vals = form.getValues() ;
			monoloop_base.showProgressbar('Saving data ...') ;
    		Ext.Ajax.request({
				url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=goals/saveGoalMain'  ,
				method: 'POST',
				timeout: '30000',
	            params:   {
	            	'goal[name]' : vals['name'] ,
	            	'goal[point]' : 0 ,
	            	'goal[type]' : 1 ,
	            	'goal[forcepublish]' : 1 ,
	            } ,
				success: function(xhr) {
					var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
					if( jsonData.success){
						vals['uid'] = jsonData.uid  ;
						me.save2(vals) ;
					}


	            }, failure: function() {
	                Ext.MessageBox.hide();
				    Ext.MessageBox.alert('Error', '#1201' ) ;
				}
	        }) ;
    	}
    } ,

    save2 : function(vals){
    	var me = this ;
    	Ext.Ajax.request({
			url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=goals/saveGoalUrlConfig'  ,
			method: 'POST',
			timeout: '30000',
            params:   {
            	'data[inc_www]' : vals['incWWW'] ,
            	'data[uid]' : vals['uid'] ,
            	'data[urlOption]' : vals['urlOption'] ,
            	'data[url]' : vals['url']
            } ,
			success: function(xhr) {
				var jsonData = Ext.util.JSON.decode(xhr.responseText) ;
				if( jsonData.success){
					me.save3(vals) ;
				}
            }, failure: function() {
                Ext.MessageBox.hide();
			    Ext.MessageBox.alert('Error', '#1201' ) ;
			}
        }) ;
    } ,

    save3 : function(vals){
    	var me = this ;
    	Ext.Ajax.request({
			url: 'index.php?eID=t3p_dynamic_content_placement&pid=1&cmd=goals/saveCondition'  ,
			method: 'POST',
			timeout: '30000',
            params:   {
            	'condition' : me.condition ,
            	'uid' : vals['uid']
            } ,
			success: function(xhr) {
				Ext.getCmp('wizzard').data['goal']  = vals['uid']  ;
				me.previousStep() ;
            }, failure: function() {
                Ext.MessageBox.hide();
			    Ext.MessageBox.alert('Error', '#1201' ) ;
			}
        }) ;
    }
} ;

return obj ;
// --- end
}) ;