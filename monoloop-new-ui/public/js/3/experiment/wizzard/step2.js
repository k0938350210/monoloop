define(function (require) {
// --- start	
var obj = {
	xtype : 'container' , 
	layout : 'border' , 
	border : false , 
	id : 'wizard-step-2' ,
	cls : 'wizard-step' , 
	// custin params 
	listData : {} ,  
	items : [{
		xtype : 'form' ,  
		region : 'center' , 
	    defaults : {
	        hideLabel : true , 
	        anchor : '100%'
	    } , 
	    border : false , 
	    bodyCssClass : 'main-form' , 
		items : [{
	        xtype : 'displayfield' ,
	        value : 'step 2 of 6' , 
	        anchor : '0%' ,
	        style : 'position : absolute ; right : 0 ; top : 0 ;'  
	    },{
	        xtype : 'displayfield' , 
	        value : '<h3>Segments</h3>'
	    },{
	        xtype : 'displayfield' , 
	        value : 'Please select the segment you wish to target:'
	    },{
            xtype : 'container' , 
            layout : 'column' , 
            items : [{
                xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                    xtype : 'container' , 
                    height : 256 , 
                    autoScroll : true , 
                    id : 'step2-list' , 
					cls : 'list-selector' , 
					items : []
                },{
                    xtype : 'displayfield' ,
                    value : '<a href="#" class="create-new-btn" onclick="Ext.getCmp(\'wizard-step-2\').createNewSegment() ; return false ;">Create new segment</a>'
                }]
            },{
            	xtype : 'container' , 
                columnWidth : .04 , 
                html : '&nbsp;'
            },{
                xtype : 'container' , 
                id : 'segment-description' , 
                columnWidth : .48 , 
                items : []
            }]
	    }]
	},{
		xtype : 'container' , 
		region : 'south' ,
		border : false , 
		height : 70 ,  
		layout : 'column' , 
		cls : 'main-form' , 
		items : [{
			xtype : 'container' , 
			columnWidth : 0.65 	, 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-2\').previousStep() ; return false ; "><div class="previous-btn"><span class="icon-chevron-left"></span></div>' 
		},{
			xtype : 'container' , 
			columnWidth : 0.35 , 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-2\').nextStep() ; return false ; "><div class="next-btn"><div class="col-a">Next</div><div class="col-b"><span class="icon-chevron-right"></span></div></div></a>' 	
		}]
	}]  , 
	
	// Event 
	
	listeners : {
		afterrender : function(f){
			f.loadList() ; 	
		}
	} , 
	
    
    // custom function 
    createNewSegment : function(){
    	Ext.getCmp('wizard-container').removeAll() ;  
        require(['experiment/wizzard/step2_1' ],function(wizzard){ 
            Ext.getCmp('wizard-container').add(wizzard) ;   
            Ext.getCmp('wizard-container').doLayout() ;       
        }) ; 
    } , 
    
    nextStep : function(){ 
        
        var form = Ext.getCmp('wizard-step-2').findByType('form')[0].getForm().getValues() ; 
        if( form['segment'] == undefined){
        	Ext.Msg.alert('Error', 'Please select segment.');
        }else{
        	Ext.getCmp('wizzard').data['segment'] = form['segment'] ; 
        	Ext.getCmp('wizzard').addCompleteStep(2) ; 
    		Ext.getCmp('wizzard').changeStep(2,3) ; 
        }
    	
    } , 
    
    previousStep : function(){ 
    	Ext.getCmp('wizzard').changeStep(2,1) ; 
    } , 
    
    loadList : function(){
    	var me = this ; 
    	monoloop_base.showProgressbar('Loading segment ...') ; 
		Ext.Ajax.request({
			url: 'index.php?eID=monoloop_segments&pid=1&cmd=main/getlist'  , 
			method: 'POST',
			timeout: '30000',
            params:   {
            	dir : 'DESC' , 
            	idfolder : 0 , 
            	limit : 1000 , 
            	sort : 'crdate' , 
            	start : 0 
            } , 
			success: function(xhr) { 
				var jsonData = Ext.util.JSON.decode(xhr.responseText) ;  
                Ext.MessageBox.hide(); 
                me.listData = jsonData ; 
                var segmentID = Ext.getCmp('wizzard').data['segment'] ;  
                var check = false ; 
                for( var i = 0 ; i < jsonData.topics.length ; i++ ){
                	var segment = jsonData.topics[i]  ; 
                	if( segment.uid == segmentID){ 
                		check = true ; 
                		Ext.getCmp('segment-description').update('<h4>' + segment.name + '</h4><p>' + segment.description + '</p>') ; 
               		}else
               			check = false ; 
               			
 					if(segment.hidden == '1')
						continue ;  					
                	Ext.getCmp('step2-list').add({
						xtype : 'container' , 
						cls : 'item-selector' , 
						layout : 'hbox' , 
						items : [{
							xtype : 'radio' , 
							name : 'segment' , 
							checked : check , 
							inputValue : segment.uid , 
							listeners : {
								check : function(c , checked){ 
								 	if( checked ){ 
								 		c.findParentByType('container').addClass('active') ; 
								 		var uid = c.getRawValue() ; 
								 		for( var i = 0 ; i < me.listData.topics.length ; i++){
								 			var s = me.listData.topics[i] ; 
								 			if( s.uid == uid){
								 				Ext.getCmp('segment-description').hide() ; 
								 				Ext.getCmp('segment-description').update('<h4>' + s.name + '</h4><p>' + s.description + '</p>') ; 
								 				Ext.getCmp('segment-description').setVisible(true) ; 
 								 			}
								 		}
								 		// Show description 
							 		}else{ 
							 			c.findParentByType('container').removeClass('active') ;  
						 			}
								}
							}
						},{
							xtype : 'displayfield' , 
							value : segment.name
						}]
					}) ; 
                } 
                Ext.getCmp('step2-list').doLayout() ; 
                 
            }, failure: function() {
                Ext.MessageBox.hide();
			    Ext.MessageBox.alert('Error', '#1201' ) ; 
			}
        }) ; 
    	 
    }
} ; 
return obj ; 
// --- end
}) ; 