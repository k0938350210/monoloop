define(function (require) {
// --- start	
var me = {
	xtype : 'container' , 
	layout : 'border' , 
	id : 'wizard-step-1' , 
	cls : 'wizard-step' , 
	items : [{
		xtype : 'form' ,  
		region : 'center' , 
	    defaults : {
	        hideLabel : true , 
	        anchor : '100%'
	    } , 
	    border : false , 
	    bodyCssClass : 'main-form' , 
	    items : [{
	        xtype : 'displayfield' ,
	        value : 'step 1 of 6' , 
	        anchor : '0%' ,
	        style : 'position : absolute ; right : 0 ; top : 0 ;'  
	    },{
	        xtype : 'displayfield' , 
	        value : '<h3>Name</h3>'
	    },{
	        xtype : 'displayfield' , 
	        value : 'To get started, pleaase choose a name and description for the experiment:'
	    },{
	        xtype : 'displayfield' , 
	        value : 'Name:'
	    },{
	        xtype : 'textfield' , 
	        allowBlank : false  , 
	        name : 'name'  
	    },{
	        xtype : 'displayfield' , 
	        value : 'Discription:'
	    },{
	        xtype : 'textarea' , 
	        name : 'description' ,  
	        height : 150  
	    }]
		
	},{
		xtype : 'container' , 
		region : 'south' ,
		border : false , 
		height : 70 ,  
		layout : 'column' , 
		cls : 'main-form' , 
		items : [{
			xtype : 'container' , 
			columnWidth : 0.65 	, 
			html : '&nbsp' 
		},{
			xtype : 'container' , 
			columnWidth : 0.35 , 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-1\').nextStep() ; return false ; "><div class="next-btn"><div class="col-a">Next</div><div class="col-b"><span class="icon-chevron-right"></span></div></div></a>' 	
		}]
	}] , 
	
	listeners : {
		afterrender : function(){
			var form = Ext.getCmp('wizard-step-1').findByType('form')[0].getForm() ; 
			form.findField('name').setValue(Ext.getCmp('wizzard').data['name']) ; 
			form.findField('description').setValue(Ext.getCmp('wizzard').data['description']) ; 
		}
	} , 
	
	nextStep : function(){ 
    	var form = Ext.getCmp('wizard-step-1').findByType('form')[0].getForm() ; 
    	if( form.isValid()){
    		var vals = form.getValues() ;   
    		Ext.getCmp('wizzard').data['name'] = vals['name'] ; 
    		Ext.getCmp('wizzard').data['description'] = vals['description'] ;  
    		Ext.getCmp('wizzard').addCompleteStep(1) ; 
    		Ext.getCmp('wizzard').changeStep(1,2) ; 
    	}
    	
    }
	
    
}
return me ; 
// --- end
}) ; 