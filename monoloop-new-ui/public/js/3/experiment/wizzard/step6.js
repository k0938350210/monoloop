define(function (require) {
// --- start	
var me = {
    xtype : 'container' , 
	layout : 'border' , 
	border : false , 
	id : 'wizard-step-6' ,
	cls : 'wizard-step' , 
    items : [{
		xtype : 'form' ,  
		region : 'center' , 
	    defaults : {
	        hideLabel : true , 
	        anchor : '100%'
	    } , 
	    border : false , 
	    bodyCssClass : 'main-form' , 
        items : [{
	        xtype : 'displayfield' ,
	        value : 'step 6 of 6' , 
	        anchor : '0%' ,
	        style : 'position : absolute ; right : 0 ; top : 0 ;'  
	    },{
	        xtype : 'displayfield' , 
	        value : '<h3>Simply confirm your settings and your\' done!</h3>'
	    },{
	        xtype : 'displayfield' , 
	        value : 'Review your settings below and click save to confirm.'
	    },{
	    	xtype : 'container' , 
	    	layout : 'column' , 
	    	style : 'margin : 10px 0 10px 0' , 
	    	items : [{
	    		xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                	xtype : 'displayfield' , 
                	value : '<a href="#" class="edit-btn" onclick="Ext.getCmp(\'wizard-step-6\').editSegment() ; return false ;">Edit Segments</a>'
                }]
	    	},{
	    		xtype : 'container' , 
	    		columnWidth : .04 , 
	    		html : '&nbsp;'
	    	},{
	    		xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                	xtype : 'displayfield' , 
                	value : '<h4>Segments</h4><p>Your targeted segment is:</p>'
                },{
                	xtype : 'displayfield' , 
                	id: 'step6-segment-description' , 
                	value : ''
                }]
	    	}]
	    },{ 
    		xtype : 'displayfield' , 
    		value : '<hr/>' 
	    },{
	    	xtype : 'container' , 
	    	layout : 'column' , 
	    	style : 'margin : 10px 0 10px 0' , 
	    	items : [{
	    		xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                	xtype : 'displayfield' , 
                	value : '<a href="#" class="edit-btn" onclick="Ext.getCmp(\'wizard-step-6\').editGoal() ; return false ;">Edit Goals</a>'
                }]
	    	},{
	    		xtype : 'container' , 
	    		columnWidth : .04 , 
	    		html : '&nbsp;'
	    	},{
	    		xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                	xtype : 'displayfield' , 
                	value : '<h4>Goals</h4><p>Your goal name is:</p>'
                },{
                	xtype : 'displayfield' , 
                	id: 'step6-goal-description' , 
                	value : ''
                }]
	    	}]
	    },{ 
    		xtype : 'displayfield' , 
    		value : '<hr/>' 
	    },{
	    	xtype : 'container' , 
	    	layout : 'column' , 
	    	style : 'margin : 10px 0 10px 0' , 
	    	items : [{
	    		xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                	xtype : 'displayfield' , 
                	value : '<a href="#" class="edit-btn" onclick="Ext.getCmp(\'wizard-step-6\').editContent() ; return false ;">Edit Content</a>'
                }]
	    	},{
	    		xtype : 'container' , 
	    		columnWidth : .04 , 
	    		html : '&nbsp;'
	    	},{
	    		xtype : 'container' , 
                columnWidth : .48 , 
                items : [{
                	xtype : 'displayfield' , 
                	value : '<h4>Content</h4><p>Content linked to this experiment:</p>'
                },{
                	xtype : 'displayfield' , 
                	id: 'step6-content-description' , 
                	value : ''
                }]
	    	}]
	    },{ 
    		xtype : 'displayfield' , 
    		value : '<hr/>' 
	    }
        ]
    },{
		xtype : 'container' , 
		region : 'south' ,
		border : false , 
		height : 70 ,  
		layout : 'column' , 
		cls : 'main-form' , 
		items : [{
			xtype : 'container' , 
			columnWidth : 0.65 	, 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-6\').previousStep() ; return false ; "><div class="previous-btn"><span class="icon-chevron-left"></span></div>'
		},{
			xtype : 'container' , 
			columnWidth : 0.35 , 
			html : '<a href="#" onclick="Ext.getCmp(\'wizard-step-6\').nextStep() ; return false ; "><div class="next-btn"><div class="col-a">Save</div><div class="col-b"><span class="icon-chevron-right"></span></div></div></a>' 	
		}]
	}]   , 
    
    listeners : {
		afterrender : function(f){
			//console.debug( Ext.getCmp('wizzard').data ) ;  
			
			// Edit Selected Detail ; 
			monoloop_base.showProgressbar('Loading detail ...') ; 
			Ext.Ajax.request({
				url: 'index.php/selected_detail/?eID=ml_experiment'  , 
				method: 'POST',
				timeout: '30000',
	            params: Ext.getCmp('wizzard').data , 
				success: function(xhr) { 
					var jsonData = Ext.util.JSON.decode(xhr.responseText) ;   

					if( jsonData.s != null && jsonData.s != undefined && jsonData.s != false )
						Ext.getCmp('step6-segment-description').setValue('<ul class="descripiotn-list"><li>' + jsonData.s.name + '</li></ul>') ; 
					if( jsonData.g != null && jsonData.g != undefined && jsonData.g != false )
						Ext.getCmp('step6-goal-description').setValue('<ul class="descripiotn-list"><li>' + jsonData.g.header + '</li></ul>') ; 
						
					var x = '<ul class="descripiotn-list">' ; 
					for(var i = 0 ; i < jsonData.c.topics.length ; i++ ){
						x += '<li>' + jsonData.c.topics[i].name + '</li>' ; 
					}
					x += '</ul>' ; 
					Ext.getCmp('step6-content-description').setValue(x) ; 
					Ext.MessageBox.hide();
	                 
	            }, failure: function() {
	                Ext.MessageBox.hide();
				    Ext.MessageBox.alert('Error', '#1201' ) ; 
				}
	        }) ; 
		}
	} , 
    // custom function  
    
    previousStep : function(){
    	//console.log('store data with no validate') ; 
    	Ext.getCmp('wizzard').changeStep(6,5) ; 
    } ,
    
    nextStep : function(){
       // console.log('verify 6 before move next need code here') ;  
    	Ext.getCmp('wizzard').save() ; 
    } , 
    
    editSegment : function(){
    	Ext.getCmp('wizzard').changeStep(6,2) ; 
    } , 
    
    editGoal : function(){
    	Ext.getCmp('wizzard').changeStep(6,3) ; 
    } , 
    
    editContent : function(){
    	Ext.getCmp('wizzard').changeStep(6,4) ; 
    }
    
} ; 
return me ; 
// --- end
}) ; 