define(function (require) {
// --- start	
var obj = {
	xtype : 'container' ,  
	border : false , 
	id : 'wizard-step-2_1' ,
	cls : 'wizard-step' , 
	items : [{
    	xtype : 'displayfield'  , 
		value : '<h1>Segment</h1>' , 
		cls : 'main-form'
    },{
    	xtype : 'form' , 
    	border : true , 
    	defaults : {
	        hideLabel : true , 
	        anchor : '100%'
	    } , 
	    bodyCssClass : 'main-form-segment' , 
	    items : [{
	    	xtype : 'displayfield' , 
	    	value :  'Segment name:'
	    },{
	    	xtype : 'textfield' , 
	    	name : 'name' , 
	    	allowBlank : false 
	    },{
	    	xtype : 'displayfield' , 
	    	value : 'Segment description:'
	    },{
	    	xtype : 'textarea' , 
	    	height : 150 , 
	    	name : 'description'
	    },{
	    	xtype : 'container' , 
	    	layout : 'column' , 
	    	items : [{
	    		xtype : 'displayfield' , 
	    		columnWidth : 0.2 , 
	    		value : '&nbsp;'
	    	},{
	    		xtype : 'displayfield' ,
            	value : '<a href="#" class="create-new-btn" onclick="Ext.getCmp(\'wizard-step-2_1\').openCondition() ; return false ;">Set the conditions for your segment <span class="icon icon-external-link"></span></a>' , 
            	columnWidth : 0.6 
	    	},{
	    		xtype : 'displayfield' , 
	    		columnWidth : 0.2 , 
	    		value : '&nbsp;'
	    	}]
	    },{
	    	xtype : 'container' , 
	    	layout : 'column'  , 
	    	style : 'margin : 10px 0 0 0 ;' ,
	    	items : [{
	    		xtype : 'displayfield' ,
	    		columnWidth : 0.5 , 
	    		value : '&nbsp;'
	    	},{
	    		xtype : 'displayfield' , 
	    		columnWidth : 0.24 , 
	    		value : '<a href="#" onclick="Ext.getCmp(\'wizard-step-2_1\').previousStep() ; return false ; "><div class="previous-btn-segment"><span class="icon-chevron-left"></span>&nbsp;Cancel</div>'
	    	},{
	    		xtype : 'displayfield' ,
	    		columnWidth : 0.02 , 
	    		value : '&nbsp;'
	    	},{
	    		xtype : 'displayfield' , 
	    		columnWidth : 0.24 , 
	    		value : '<a href="#" onclick="Ext.getCmp(\'wizard-step-2_1\').save() ; return false ; "><div class="next-btn-segment">Save</div>'
	    	}]
	    }]
    }] , 
    
    // custom function 
    
    openCondition : function(){
    	var me = this ; 
    	var conditionStr  = me.condition ; 
    	require(['placement/conditionWindow' ],function(){
    		condition.stage = 41 ; 
            condition.preconditionStr = conditionStr ; 
            condition.openMainWindow() ; 
   		}) ; 
    } , 
    
    saveCondition : function(conditionStr){
    	var me = this ; 
    	me.condition = conditionStr ; 
    	console.debug( me.condition ) ; 
    } , 
    
    previousStep : function(){
    	Ext.getCmp('wizard-container').removeAll() ;  
        require(['experiment/wizzard/step2' ],function(wizzard){ 
        	//Save new segment data 
        	
        	
            Ext.getCmp('wizard-container').add(wizzard) ;   
            Ext.getCmp('wizard-container').doLayout() ;       
        }) ; 
    } , 
    
    save : function(){
    	var me = this ; 
    	var form = Ext.getCmp('wizard-step-2_1').findByType('form')[0].getForm() ; 
    	if( form.isValid()){
    		form.submit({
    			url: 'index.php?eID=monoloop_segments&pid=1&cmd=main/save',
                waitMsg: 'Saving data...',
                params : {
                	condition : me.condition
                } ,
                success: function(simple, o){ 
                	Ext.getCmp('wizzard').data['segment'] = o.result.uid ; 
                     me.previousStep() ;  
                } ,
                failure: function(simple, o) {  
		             Ext.MessageBox.alert('Error', o.result.msg ) ;  
    			}
    		}) ; 
    	}
    }
} ; 

return obj ; 
// --- end
}) ; 	