define(['require', 'placement/variationContentEditor'] ,function (require) {
// --- start

var obj = require('placement/variationContentEditor') ; 

obj.disableCondition = true ; 

obj.before_retrieveDataForSubmit = function(){
	return {
		'experiment_id' : Ext.getCmp('wizzard').data['uid'] 
	} ; 
} ; 

return obj ; 
//---	
}) ; 