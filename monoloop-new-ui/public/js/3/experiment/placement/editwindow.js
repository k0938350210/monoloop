define(['require' , 'placement/editwindow' ] ,function (require) {
// --- start
var obj = require('placement/editwindow') ; 

obj.openWindowByURL = function(url){
    var me = this ; 
    // Load Data by URL ; 
    monoloop_base.showProgressbar('Loading data...'  ) ;
    Ext.Ajax.request({
		url: 'index.php/'+Ext.getCmp('wizzard').data['uid']+ '/placementmain/?eID=ml_experiment' , 
		// send additional parameters to instruct server script
		method: 'POST',
        params : {
            url : url 
        } , 
		timeout: '30000', 
		// process the response object to add it to the TabPanel:
		success: function(xhr) {
			Ext.MessageBox.hide(); 
         	var jsonRoot = Ext.util.JSON.decode(xhr.responseText) ;
         	if( jsonRoot.success == true){
                obj.data = jsonRoot.data ; 
                me.openWindow() ; 
            }else{
            	 
            }
		},
		failure: function() {
			Ext.MessageBox.hide();
			Ext.MessageBox.alert('Error', '#309' ) ; 
		}
    });
} ; 

 

obj.windowClose = function(){
    var me = this ;
    me.editWindow.close() ; 
    Ext.getCmp('wizard-step-4').refreshGrid() ; 
} ; 
return obj ; 
//---	
}) ; 	