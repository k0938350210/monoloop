define(['require', 'placement/contentEditorCE'] ,function (require) {
// --- start

var obj = require('placement/contentEditorCE') ; 

obj.before_retrieveDataForSubmit = function(){
	return {
		'experiment_id' : Ext.getCmp('wizzard').data['uid'] 
	} ; 
}
 
return obj ; 
//---	
}) ; 