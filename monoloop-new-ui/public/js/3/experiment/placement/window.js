define(['require' , 'placement/window' , 'experiment/placement/selectTab', 'experiment/placement/testbenchTab' , 'experiment/placement/contentEditor'  , 'experiment/placement/contentEditorCE' , 'experiment/placement/variationContentEditor' , 'placement/conditionWindow','placement/predefinedWindow'] ,function (require) {
	
// --- start
var obj = require('placement/window') ;  
                                
obj.temPath = window.temPath ; 
obj.defaultDomain = window.defaultDomain ; 
obj.baseUrl = window.baseUrl ; 
obj.baseRelativeUrl = window.baseRelativeUrl ; 
obj.testBenchPath = window.testBenchPath ; 
obj.disableCondition = true ; 
obj.getExperimentID = function(){
	return  Ext.getCmp('wizzard').data['uid'] ;
} ;

obj.onClosingWindow = function(){
	Ext.getCmp('wizard-step-4').refreshGrid() ; 	
} ; 

return obj ; 
//---	
}) ; 	