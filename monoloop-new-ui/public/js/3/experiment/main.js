define(['require' , 'experiment/tree' , 'experiment/main_grid'] ,function (require , tree ) {
// --- start	
var me = {
	clearPageIndex : true , 
	init : function(){ 
		me.initCreateBtn() ; 
		
		tree.init({
			'typeDefault' : 'experiment'
		}) ; 
		
		var grid = require('experiment/main_grid') ; 
		grid.init({'tree':tree}) ; 
		
		 var tabPanel = new Ext.TabPanel({
            activeTab: 0,
            width: '100%',
            plain:true,
            defaults:{autoScroll: true}, 
	        height:570,
            renderTo: 'experiment-content' , 
            items : [
            {
                title : 'Experiment Lists' , 
                id : 'mdc-pagelist' , 
                layout: 'border' ,    
                items :[
                {
                    region: 'west',
        			title: 'Folder',
        	        split: true,
        	        width: 215,
        	        style : 'border-right : 1px solid rgb(221,221,221)' , 
        	        border : false , 
        			margins:'0',
        	        cmargins:'0',
        	        collapsible: true,
                    tbar: [
                        {  
                            cls : 'tree-button' , 
        			        iconCls: 'new-symbol1',
                            tooltip: 'Create new folder',
        			        handler: function(){
                        		tree.createNewWindow() ; 
        			        }
        			    }, { 
        			        cls : 'tree-button' , 
        			        iconCls: 'edit-symbol1',
                            tooltip: 'Edit folder',
        			        handler: function(){ 
        			    		tree.openEditWindow() ; 
        			        }
        			    }, { 
        			        cls : 'tree-button' , 
        			        iconCls: 'delete-symbol1',
                            tooltip: 'Delete folder',
        			        handler: function(){
        			    		tree.openDeleteWindow() ; 
        			        }
        			    }
                    ] , 
                    items :[tree.tree]
                } , {
                    region: 'center',
                    margins:'3 3 3 0', 
	                header : false ,
	                border : false , 
                    items : [Ext.getCmp('experiment-main-grid')]
                }
                ]
            }]
        }) ; 
        
        // Only test purpose ; 
        return ; 
        require(['experiment/wizzard/mainwindow'],function(wizzard){
       		wizzard.init({
       			id : 'wizzard' , 
       			data : {}
       		}) ; 
   		}) ;
	} , 
	
	initCreateBtn : function(){
		var btn = new Ext.Button({
            text : 'Create new experiment' ,
            id : 'btn-new-experiment' ,
            iconCls : 'newpage-icon' ,  
            height : 28 , 
            cls : 'tabcreate-btn' , 
            renderTo : 'btn-new-experiment' ,
            handler : function(){
            	require(['experiment/wizzard/mainwindow'],function(wizzard){
               		wizzard.init({
               			id : 'wizzard' , 
                        data : {}
               		}) ; 
           		}) ; 
            }
        }) ; 
        /*
        var btn2 = new Ext.Button({
            text : 'Test data tool' ,
            id : 'btn-test-experiment' ,
            iconCls : 'newpage-icon' ,  
            height : 28 , 
            cls : 'tabcreate-btn' , 
            renderTo : 'btn-test-experiment' ,
            handler : function(){
               require(['experiment/test/window'],function(test){
               		test.init({
               			parent_id : 'experiment-main-grid'
               		}) ; 
               }) ; 
            }
        }) ; 
		*/
		var searchBox = new Ext.form.CompositeField({
			width : 203 ,
			renderTo : 'search-text' ,
			items : [{
				xtype : 'textfield' , 
				width : 180 , 
				id : 'serach-textbox' , 
				emptyText : 'Enter Search Keyword' , 
				enableKeyEvents : true , 
	            listeners : {
	                'keypress' : function(textField ,e){
	                    if(e.keyCode == e.ENTER) {   
	                    }
	                }
	            }
			},{
				xtype : 'button' , 
				cls : 'search-btn' , 
				style : 'margin : 0 0 0 -5px ; ' , 
				width : 23 , 
				handler : function(){  
					
				}
			}]
		})  ;
	}				
} ;  
return me ; 

// --- end
}) ; 