define(function (require) {
// --- start	
var me = {
	id : 'test-window' , 
	parent_id : null , 
	init : function(config){
		me.parent_id = config.parent_id
		
		var myStore = new Ext.data.ArrayStore({
		    fields: ['id', 'name'] 
		});
		
		var myData = [
		    ['1','Test case 001 - Basic test'] 
		];
		myStore.loadData(myData);
		
		function renderAction(value, p, r){ 
            return '<a href="#" onclick="Ext.getCmp(\''+me.id+'\').process('+r.data['id']+') ; return false ; "><span class="icon icon-play"></span></a>' ; 
        }
        
		var window = new Ext.Window({
            width:400,
         	height:300,
            title:"Test window ( Experiment )",
            plain:true , 
         	modal:true,
            maximizable : false  , 
            resizable : false , 
         	closable:true , 
         	constrain : true , 
            id : me.id , 
            baseCls : 'ces-window' ,
            iconCls : 'ces-window-header-icon' ,   
            layout : 'border' , 
            items : [{
            	height : 20 , 
            	region : 'north' , 
            	border : false , 
            	bodyStyle : 'padding : 5% ;' , 
            	defaults : {
            		anchor : '100%' 
            	} , 
            	items : [{
            		xtype : 'textfield' , 
            		width : '90%' , 
            		id : me.id + '-url' , 
            		allowBlank : false , 
            		emptyText : 'http://www.example.com'
            	}]
            },{
            	xtype : 'grid' , 
            	region : 'center' ,
				store : myStore ,  
            	columns:[{
            		header: "Name",dataIndex: 'name',width : 300
           		},{
           			header: "Action",dataIndex: 'id', width : 60 , renderer: renderAction 
           		}]
            }]  , 
            
            // custom function 
            
            process : function(id){ 
            	Ext.MessageBox.confirm('Confirm', 'Are you sure to process this record ?', function(btn){
            		if(btn == 'yes'){
            			 
            			if( Ext.getCmp(me.id + '-url').validate() == false ) 
							return ;   
            			monoloop_base.showProgressbar('Initializing data...') ; 
            			Ext.Ajax.request({
			    			url: 'index.php/test/'+id+'?eID=ml_experimenttest'  , 
			    			method: 'POST',
			    			timeout: '30000',
			                params:   {
			                	url : Ext.getCmp(me.id + '-url').getValue() 
			                } , 
			    			success: function(xhr) { 
								var jsonData = Ext.util.JSON.decode(xhr.responseText) ;  
			                    Ext.MessageBox.hide(); 
			                    Ext.getCmp(me.parent_id).refrehGrid() ; 
			                    Ext.getCmp(me.id).close() ; 
			                     
			                }, failure: function() {
			                    Ext.MessageBox.hide();
			    			    Ext.MessageBox.alert('Error', '#1201' ) ; 
			    			}
			            }) ; 
            		}
            	}) ;
            	
            }
  		}) ; 
  		
  		window.show() ; 
	}
	
}

return me ; 

// --- end 
}) ; 