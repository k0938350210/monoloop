var tooltips = {};

tooltips.page = {};
tooltips.open = false;
tooltips.startedBefore = false;
tooltips.completedBefore = false;


tooltips.page.pageId = '';
tooltips.page.currentTip = {};
tooltips.guidlineShow = false;
tooltips.generate = function(g, options) {
    options = options || {};

    if (g && (tooltips.page.pageId != undefined) &&  (tooltips.page.pageId === "experiment" || tooltips.page.pageId === "goals"|| tooltips.page.pageId === "segment")) {
        // set total tooltips and header text
        var currentTipIndex = g.tooltip_no;

        var totalIndex = tooltips[tooltips.page.pageId].length;

        // prepare header content
        var _head = '<div class="head-content">';
        _head += '<div class="title">' + g.name + '</div>';
        // _head += '<div class="index"> Page ' + (currentTipIndex + ' of ' + totalIndex)+ ' </div>';
        _head += '<div class="index">Page (' + (currentTipIndex + '/' + totalIndex)+ ')</div>';
        _head += '</div>';


        // marc added webuiPopover
        MONOloop.jq("div.webix_template").webuiPopover({
            title: g.name,
            content: this.getContentNew(g),
            animation: 'pop',
            trigger:'manual',
            css: "hello",
            width: 400,
            placement:'auto',
            cache: true,
            multi: false,
            arrow: false,
            closeable: false,
            backdrop: false,
            offsetTop: options.offsetTop ? options.offsetTop : 0,
            dismissible: false,
            offsetLeft: $("#main").width() / 2 - 200,
            offsetTop: 100,
            type: 'html',
            onhide: function(element){
              this.webuiPopover('destroy');
            }
        }).webuiPopover('show');

        // if (true) {
        //     $("#tooltipBackDrop").show();
        //
        //     tooltips.open = true;
        //
        //     tooltips.page.currentTip = g;
        //
        //     webix.ui({
        //         view: "window",
        //         css: 'tooltipWindow',
        //         height: 500,
        //         autoheight: true,
        //         width: 700,
        //         scroll: true,
        //         left: 50,
        //         top: 50,
        //         zIndex: 10000000000,
        //         position: "center",
        //         move: true,
        //         //head: g.name + ' (' + g.tooltip_no + ')',
        //         head: _head,
        //         body: {
        //             // template: tooltips.getContent(g)
        //             template: tooltips.getContentNew(g)
        //         }
        //     }).show();
        // }

        // tooltips.setCookie();
    }
    else{
      console.log("generate");
    }

};

tooltips.saveInSession = function(close, src) {

    close = close || 'yes';

    var page = tooltips.page.pageId;
    var current = tooltips.page.currentTip;

    var isCompleted = 0;

    if (current) {
        if (!current.next) {
            isCompleted = 1;
        }
    }

    close === 'yes' && tooltips.close(src);
    $.ajax({
        type: 'POST',
        url: monolop_api_base_url + "/api/tooltips/save",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
            xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
        },
        data: {
            pageId: page,
            isCompleted: isCompleted,
            state: JSON.stringify({
                pageId: page,
                isCompleted: isCompleted,
                currentTip: current
            })
        },
        success: function(data) {
					if(data.status == 200){
						StateData.tooltips[page].currentPageTTStatus = {"istipsViewed":1};
					}
        }
    });


};

tooltips.close = function(src) {
    MONOloop.jq("div.webix_template").webuiPopover('destroy');
    if (tooltips.page.currentTip) {
        if (src !== 'guidelines') {
            if (!tooltips.page.currentTip.next || !tooltips.page.currentTip.prev) {
                if (guidelines.startedBefore === false) {
                    setTimeout(function() {
                        guidelines.generate(guidelines.page.currentGuide);
                    }, 1000);
                }
            }
        }


        if (!tooltips.page.currentTip.next || !tooltips.page.currentTip.prev) {
            tooltips.page.currentTip = '';
            if (src !== 'guidelines') {
                tooltips.saveInSession('no');
            }
        }
    }


    tooltips.open = false;
    MONOloop.jq('.tooltipWindow').remove();
    $("#tooltipBackDrop").hide();
    // guidelines.open === false && setTimeout(function(){guidelines.generate(guidelines.page.currentGuide);},1000);

    if(!tooltips.guidlineShow){
      // update toggle button status
      if($('#toggleTips').hasClass('fa-toggle-on')){
        $('#toggleTips').removeClass('fa-toggle-on');
        $('#toggleTips').addClass('fa-toggle-off')
      }
    }
};
tooltips.next = function() {
    MONOloop.jq('.tooltipWindow').remove();
    $("#tooltipBackDrop").hide();
    var _next = tooltips.findByTooltipNo(tooltips[tooltips.page.pageId], tooltips.page.currentTip.next);

    tooltips.page.currentTip = _next;

    if (_next === undefined) {
        tooltips.guidlineShow = true;
        tooltips.close();
        guidelines.startGuideline();
        tooltips.page.currentTip = "";
        // tooltips.next();
    } else {
        $("div.webui-popover-content").html(this.getContentNew(_next));
        //tooltips.generate(_next);
    }

};
tooltips.prev = function() {
    MONOloop.jq('.tooltipWindow').remove();
    $("#tooltipBackDrop").hide();

    var _prev = tooltips.findByTooltipNo(tooltips[tooltips.page.pageId], tooltips.page.currentTip.prev);
    tooltips.page.currentTip = _prev;

    if (_prev === undefined) {
        tooltips.prev();
    } else {
        tooltips.generate(_prev);
    }
};
tooltips.getContent = function(g) {
    var close = 0;
    var html = '';
    html += '<div id="tooltipPopup" class="tooltipPopup">';
    if (g.screenshot) {
        html += '<img  class="image" src="' + g.screenshot + '" />';
    }
    html += '<div class="desc">' + g.description + '</div>';
    if (g.prev) {
        html += '<button class="guidelineBtn" onclick="tooltips.prev();" type="button">Back</button>';
    } else {
        if (tooltips[tooltips.page.pageId].length > 1) {
            html += '<button class="guidelineBtn" onclick="tooltips.close();" type="button">Close</button>';
            close = 1;
        }
    }
    // html += '&nbsp;&nbsp;';
    if (g.next) {
        html += '<button class="guidelineBtn" onclick="tooltips.next();" style="float: right;"  type="button">Next</button>';
    } else {
        if (close !== 1) {
            html += '<button class="guidelineBtn" onclick="tooltips.close();" style="float: right;"  type="button">Close</button>';
        }
    }

    html += '</div>';

    return html;
};
tooltips.getContentNew = function(g){
  // new approach
  var _html = '<div class="tooltip-container">';

  // container
  _html += '<div class="tooltip-content">';

  // if contains screenshot
  if(g.screenshot){
    _html += '<div class="title-image">';
    _html += '<img src="' + g.screenshot + '" alt="Tooltip title image!">';
    _html += '</div>';
  }

  if(g.description){
    _html += '<div class="content">';
    _html += g.description;
    _html += '</div>';
  }

  _html += '</div>';

  // button panel at the bottom
  //_html += '<div class="tooltip-btns" >';
  _html += '<div style="margin-top:5%;">';

  // close button
  _html += '<a href="javascript:void(0);" onclick="tooltips.close();" class="done-link" style="color: #155A3E;text-decoration: none;line-height: 1.8;">Done</a>'; //marc added

  //_html += '<button class="close-btn guidelineBtn" onclick="tooltips.close()">Close</button>'
  // move previous button
  /*
  if(g.prev){
      _html += '<button class="prev-btn guidelineBtn" onclick="tooltips.prev()">';
      // _html += '<i class="icon-chevron-left icon-large"></i>';
      _html += 'Prev';
      _html += '</button>';
  }
  */
  // move forward button
  /*
  if(g.next){
      _html += '<button class="next-btn guidelineBtn" onclick="tooltips.next()">';
      _html += 'Next';
      // _html += '<i class="icon-chevron-right icon-large"></i>';
      _html += '</button>';
  }*/

  _html += '<button class="next-btn guidelineBtn" style="float:right;" onclick="tooltips.next()">';
  _html += 'Next';
  _html += '</button>';

  _html += '</div>';

  // end
  _html += '</div>';

  return _html;
};
tooltips.first = function(g) {
    if (g.length > 0) {
        for (var i = 0; i < g.length; i++) {
            if (g[i].prev === null) {
                return g[i];
            } else {
                continue;
            }
        }
    }
};
tooltips.isNextExists = function(g, t_no) {

};
tooltips.findByTooltipNo = function(g, t_no) {
    if (g.length > 0) {
        for (var i = 0; i < g.length; i++) {
            if (g[i].tooltip_no == t_no) {
                return g[i];
            } else {
                continue;
            }
        }
    }
};

tooltips.last = function(g) {
    if (g.length > 0) {
        for (var i = 0; i < g.length; i++) {
            if (g[i].next === null) {
                return g[i];
            } else {
                continue;
            }
        }
    }

};

tooltips.startTooltip = function(pageId, options) {
// Changes done by Imran Ali

pageId = pageId || tooltips.page.pageId;
if (pageId === "experiment" || pageId === "goals" || pageId === "segment" ) {

var StateDataobj_Tooltip = StateData.tooltips[pageId];
var cg = undefined;
if(StateDataobj_Tooltip.currentPageTTStatus){
  cg = StateDataobj_Tooltip.currentPageTTStatus;
  }
tooltips[pageId] = StateDataobj_Tooltip.currentPageToolTips;
tooltips.page.pageId = pageId;
if (cg) {
           if (cg.currentTip) {
               tooltips.page.currentTip = cg.currentTip;
           } else {
               tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
           }
       } else {
           tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
       }

       tooltips.generate(tooltips.page.currentTip, options);
       options._this.style.pointerEvents = 'auto';

}
// Changes Ends Here


// Below Code is Commented because of Above changes

    // pageId = pageId || tooltips.page.pageId;
    //
    // var fetchtooltipsUrl = monolop_api_base_url + '/api/guidelines?page_id=' + pageId;
    // $.ajax({
    //     type: 'GET',
    //     url: fetchtooltipsUrl,
    //     beforeSend: function(xhr) {
    //         xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
    //         xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
    //     },
    //     success: function(r) {
    //         var cg = undefined;
    //         if (r.currentTip) {
    //             cg = JSON.parse(r.currentTip);
    //             tooltips.startedBefore = true;
    //         }
    //
    //         tooltips[pageId] = r.tooltips;
    //         tooltips.page.pageId = pageId;
    //
    //         if (cg) {
    //             if (cg.currentTip) {
    //                 tooltips.page.currentTip = cg.currentTip;
    //             } else {
    //                 tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
    //             }
    //         } else {
    //             tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
    //         }
    //
    //         tooltips.generate(tooltips.page.currentTip, options);
    //         options._this.style.pointerEvents = 'auto';
    //     }
    // });
};
tooltips.startSubGuideline = function(subWin, options) {

  // Changes done by Imran Ali
  tooltips.page.actionID = subWin;
  tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
  pageId = pageId || tooltips.page.pageId;
  var StateDataobj_Tooltip = StateData.tooltips[pageId];
  var cg = undefined;
  if(StateDataobj_Tooltip.currentPageTTStatus){
    cg = StateDataobj_Tooltip.currentPageTTStatus;
    }
    tooltips[tooltips.page.pageId] = StateDataobj_Tooltip.currentPageToolTips;

           if (cg) {
               if (cg.currentTip) {
                   tooltips.page.currentTip = cg.currentTip;
               } else {
                   tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
               }
           } else {
               tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
           }

           setTimeout(function() {
               tooltips.generate(tooltips.page.currentTip, options);
           }, 2000);
  // Changes Ends Here


  // Below Code is Commented because of Above changes
    // tooltips.page.actionID = subWin;
    // tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
    //
    //
    // var fetchtooltipsUrl = monolop_api_base_url + '/api/tooltips?page_id=' + tooltips.page.pageId + '&action_id=' + tooltips.page.actionID;
    // $.ajax({
    //     type: 'GET',
    //     url: fetchtooltipsUrl,
    //     beforeSend: function(xhr) {
    //         xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
    //         xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
    //     },
    //     success: function(r) {
    //
    //
    //         var cg = undefined;
    //         if (r.currentTip) {
    //             cg = JSON.parse(r.currentTip);
    //             tooltips.startedBefore = true;
    //         }
    //
    //         tooltips[tooltips.page.pageId] = r.tooltips;
    //
    //         if (cg) {
    //             if (cg.currentTip) {
    //                 tooltips.page.currentTip = cg.currentTip;
    //             } else {
    //                 tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
    //             }
    //         } else {
    //             tooltips.page.currentTip = tooltips.first(tooltips[tooltips.page.pageId]);
    //         }
    //
    //         setTimeout(function() {
    //             tooltips.generate(tooltips.page.currentTip, options);
    //         }, 2000);
    //     }
    // });


};
