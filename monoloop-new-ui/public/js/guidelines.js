var guidelines = {};

guidelines.page = {};
guidelines.open = false;
guidelines.page.pageId = '';
guidelines.page.actionID = 'default';
guidelines.startedBefore = false;
guidelines.completedBefore = false;

guidelines.page.currentGuide = {};
guidelines.popoversArray = ['placementWindowEditContentOptions', 'placementWindowEditContentOptionsHasVar', 'placementWindowEditContentOptionsUpdate', 'placementWindowEditContentOptionsAddVar', 'placementWindowEditContentOptionsUpdateVar', 'placementWindowEditContentListVariation'];
guidelines.iframesArray = ['conditionBuilder', 'placementWindow'];
guidelines.topPlacementArray = ['accountLog'];

guidelines.generate = function(g, options){
  options = options || {};
  if(g){
    if(guidelines.popoversArray.indexOf(guidelines.page.pageId) !== -1){
      options.width = 250;
    }
    if(guidelines.topPlacementArray.indexOf(guidelines.page.pageId) !== -1){
      options.placement = 'top';
      options.offsetTop = 130;
    }
    if(true){
      if(MONOloop.jq(g.location_id).length === 0){
        guidelines.next(options);
        return ;
      }

      guidelines.page.currentGuide = g;
      guidelines.open = true;
      guidelines.startedBefore = true;
      // Changed By Imran Ali
    // Mantis #4311
    if (g.location_id === "div.account_id:first" || g.location_id === "div.api_token:first" || g.location_id === "div.username:first"){
        options.placement = 'bottom';
    }
    if (g.location_id === "div.copy_api_token:first" && guidelines.page.pageId === "accountApiToken") {
        options.placement = 'top-right';
      }

    // alert(g.location_id);
      // MONOloop.jq(g.location_id).webuiPopover({
      //   title: g.name,
      //   content: this.getContent(g, options),
      //   animation: 'pop',
      //   css: "hello",
      //   width: options.width ? options.width : 400,
      //   placement: options.placement ? options.placement : 'auto',
      //   cache: true,
      //   multi: false,
      //   arrow: true,
      //   closeable: false,
      //   backdrop: false,
      //   offsetTop: options.offsetTop ? options.offsetTop : 0,
      //   dismissible: false,
      //   trigger:"manual",//changes from Marc
      //   type: 'html',
      //   onhide: function(element){
      //     this.webuiPopover('destroy');
      //   }
      // }).webuiPopover('show');

    }

    // guidelines.setCookie();
  } else {
    guidelines.open = false;
  }

};

guidelines.saveInSession = function(close, src, isDone){
  close = close || 'yes';
  var isCompleted = 0;

  var page = guidelines.page.pageId;
  var action = guidelines.page.actionID;
  var current = guidelines.page.currentGuide;
  if(current){
    if(!current.next){
      isCompleted = 1;
    }
  }
  if(isDone === 1){
    current = '';
  }
  if(page){
    if(src === 'iframe'){
      close === 'yes' && guidelines.close(undefined, 'no');
      monoloop_select_view.postToParent(JSON.stringify({
        t: 'save-guides-in-session',
        data: {
          pageId: page,
          actionID: action,
          currentGuide: current,
          isDone: isDone,
        }
      }));
    } else {
      close === 'yes' && guidelines.close();
      $.ajax({
        type: 'POST',
        url: monolop_api_base_url+ "/api/guidelines/save",
        beforeSend: function(xhr){
          xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
          xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
        },
        data: { pageId: page, actionID: action, state: JSON.stringify({pageId: page, isCompleted: isCompleted, currentGuide: current}) },
        success: function(data){
          if(data.status == 200){
            if(StateData.guidelines[page]){
              StateData.guidelines[page].currentPageStatus = {"isguidesViewed":1};
            }
					}
        }
      });
    }

  }


};

guidelines.close = function(g, save){
  save = save || "yes";
  if(guidelines.page.currentGuide){
    MONOloop.jq(guidelines.page.currentGuide.location_id).webuiPopover('destroy');
    if(!guidelines.page.currentGuide.next){
      guidelines.page.currentGuide = '';
      if(save === 'yes'){
        if(['placementWindowEdit'].indexOf(guidelines.page.pageId) === -1){
          guidelines.saveInSession('no');
        } else {
          guidelines.saveInSession('no', 'iframe');
        }
      }
    }
  }
  guidelines.startedBefore = true;
  guidelines.open = false;

  // update toggle button status
  if($('#toggleTips').hasClass('fa-toggle-on')){
    $('#toggleTips').removeClass('fa-toggle-on');
    $('#toggleTips').addClass('fa-toggle-off');
  }
};
guidelines.next = function(options){
  MONOloop.jq(guidelines.page.currentGuide.location_id).webuiPopover('destroy');
  var _next = guidelines.findByLocationId(guidelines[guidelines.page.pageId], guidelines.page.currentGuide.next,guidelines.page.currentGuide.action_id);


  if(_next !== undefined && MONOloop.jq(_next.location_id).length < 1){
    guidelines.page.currentGuide = _next;
    guidelines.next();
  } else {
    guidelines.generate(_next, options);
  }

};
guidelines.prev = function(options){
  MONOloop.jq(guidelines.page.currentGuide.location_id).webuiPopover('destroy');
  var _prev = guidelines.findByLocationId(guidelines[guidelines.page.pageId], guidelines.page.currentGuide.prev,guidelines.page.currentGuide.action_id);
  if(MONOloop.jq(_prev.location_id).length < 1){
    guidelines.page.currentGuide = _prev;
    guidelines.prev();
  } else {
    guidelines.generate(_prev, options);
  }
};
guidelines.getContent = function (g, options){
  var close = 0;
  var html = '';
  html += '<div>';
  html += '<p>'+g.description+'</p>';
  if(g.prev){
    // html += '<button class="guidelineBtn" onclick="guidelines.prev();" type="button">OK</button>';
  } else {
    // html += '<button class="guidelineBtn" onclick="guidelines.close();" type="button">OK</button>';
    // close = 1;
  }
  // html += '&nbsp;&nbsp;';
  // third param 1 === done
  if(g.next){
    html += '<a href="javascript:void(0);" onclick="guidelines.saveInSession(undefined, undefined, 1);" class="done-link" style="color: #155A3E;text-decoration: none;line-height: 1.8;">Done</a>';
    html += '<button class="guidelineBtn nextBtn" onclick=\'guidelines.next();\' style="float: right;"  type="button">Next</button>';
    // html += '<button class="guidelineBtn gotIt" onclick=\'guidelines.saveInSession();\' style="float: right;"  type="button">Got it</button>';
  } else {
    if(close !== 1){
      html += '<button class="guidelineBtn" onclick="guidelines.close();" style="float: right;"  type="button">Got it</button>';
    }
  }
  if(g.screenshot){
    // html += '&nbsp;&nbsp;';
    html += '<a href="' + g.screenshot + '" target="_blank">screenshot</a>';
  }
  html += '</div>';
  return html;
};
guidelines.first = function(g){
  if(g && g.length > 0){
    for (var i = 0; i < g.length; i++) {
      if(guidelines.page.actionID === 'default'){
        if(g[i].action_id){
          continue;
        } else {
          if(g[i].prev === null){
              return g[i];
          }else {
            continue;
          }
        }
      } else {
        if(g[i].action_id === guidelines.page.actionID){
          if(g[i].prev === null){
              return g[i];
          }else {
            continue;
          }
        } else {
          continue;
        }
      }

    }
  }

};
guidelines.isNextExists = function(g,loc_id){

};
guidelines.findByLocationId = function(g,loc_id,action_id){
  if(g && g.length > 0){
    for (var i = 0; i < g.length; i++) {
      if(g[i].location_id === loc_id && g[i].action_id == action_id){
        return g[i];
      }else {
        continue;
      }
    }
  }
};
guidelines.nextPossible = function(g,j){
  if(g && g.length > 0){
    for (var i = j; i < g.length; i++) {
      if(MONOloop.jq(g[i].location_id).length > 0){
        return g[i];
      }else {
        continue;
      }
    }
  }
  return -1;
};
guidelines.last = function(g){
  if(g && g.length > 0){
    for (var i = 0; i < g.length; i++) {
      if(guidelines.page.actionID === 'default'){
        if(g[i].action_id){
          continue;
        } else {
          if(g[i].next === null){
              return g[i];
          }else {
            continue;
          }
        }
      } else {
        if(g[i].action_id === guidelines.page.actionID){
          if(g[i].next === null){
              return g[i];
          }else {
            continue;
          }
        } else {
          continue;
        }
      }

    }
  }

};

guidelines.startGuideline = function(pageId, options, src){

  // Changes By Imran Ali
  // console.log(pageId);
  pageId = pageId || guidelines.page.pageId;
    if (pageId.indexOf("placement") == -1) {
    obj1 = StateData.guidelines[pageId];
    var cg = undefined;
    if(obj1.currentPageStatus){
      cg = obj1.currentPageStatus;
      }
    guidelines[pageId] = obj1.currentPageGuidelines;
    guidelines.page.pageId = pageId;
    if(cg){
      if(cg.currentGuide){
        guidelines.page.currentGuide = cg.currentGuide;
      } else {
        guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
      }
    } else {
      guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
    }
    guidelines.generate(guidelines.page.currentGuide, options);
    if(options && options._this){
      options._this.style.pointerEvents = 'auto';
    }
  }else {
    // console.log("placementWindow");
    var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+pageId;
    $.ajax({
      type: 'GET',
      url: fetchGuidelinesUrl,
      beforeSend: function(xhr){
        xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
        xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
      },
      success: function(r){
            var cg = undefined;
            if(r.currentGuide){
              cg = JSON.parse(r.currentGuide);
            }

            //ToDo: Not sure about why this code is here, but it is blocking tooltips in IFRAME for some unknown reason
            // if(src !== 'tip'){
            //   var fullArray = guidelines.iframesArray.concat(guidelines.popoversArray);
            //   if(fullArray.indexOf(pageId) !==-1 && cg){
            //     return false;
            //   }
            // }

            guidelines[pageId] = r.guidelines;
            guidelines.page.pageId = pageId;
            if(cg){
              if(cg.currentGuide){
                guidelines.page.currentGuide = cg.currentGuide;
              } else {
                guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
              }
            } else {
              guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
            }

            guidelines.generate(guidelines.page.currentGuide, options);
            if(options && options._this) options._this.style.pointerEvents = 'auto';
      }
    });

  }

  // Ends here


  // Below Code is Commented because of above changes


    // pageId = pageId || guidelines.page.pageId;
    //
    // var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+pageId;
    // $.ajax({
    //   type: 'GET',
    //   url: fetchGuidelinesUrl,
    //   beforeSend: function(xhr){
    //     xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
    //     xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
    //   },
    //   success: function(r){
    //         var cg = undefined;
    //         if(r.currentGuide){
    //           cg = JSON.parse(r.currentGuide);
    //         }
    //
    //         //ToDo: Not sure about why this code is here, but it is blocking tooltips in IFRAME for some unknown reason
    //         // if(src !== 'tip'){
    //         //   var fullArray = guidelines.iframesArray.concat(guidelines.popoversArray);
    //         //   if(fullArray.indexOf(pageId) !==-1 && cg){
    //         //     return false;
    //         //   }
    //         // }
    //
    //         guidelines[pageId] = r.guidelines;
    //         guidelines.page.pageId = pageId;
    //         if(cg){
    //           if(cg.currentGuide){
    //             guidelines.page.currentGuide = cg.currentGuide;
    //           } else {
    //             guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
    //           }
    //         } else {
    //           guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
    //         }
    //
    //         guidelines.generate(guidelines.page.currentGuide, options);
    //         if(options && options._this) options._this.style.pointerEvents = 'auto';
    //   }
    // });

};

guidelines.startSubGuideline = function(subWin, options){
  // Changes By Imran Ali
   guidelines.page.actionID = subWin;
   guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
   if (guidelines.page.pageId.indexOf("placement") == -1) {
   obj1 = StateData.guidelines[guidelines.page.pageId];
   var cg = undefined;
   if(obj1 && obj1.currentPageStatus){
     cg = obj1.currentPageStatus;
     guidelines.startedBefore = true;
     guidelines[guidelines.page.pageId] = obj1.currentPageGuidelines;
   }

   if(cg){
     if(cg.currentGuide){
       guidelines.page.currentGuide = cg.currentGuide;
     } else {
       guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
     }
   } else {
     guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
   }

   setTimeout(function(){
     guidelines.generate(guidelines.page.currentGuide, options);
   },2000);
 }else {
   var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+guidelines.page.pageId + '&action_id='+guidelines.page.actionID;
   $.ajax({
     type: 'GET',
     url: fetchGuidelinesUrl,
     beforeSend: function(xhr){
       xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
       xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
     },
     success: function(r){

       var cg = undefined;
       if(r.currentGuide){
         cg = JSON.parse(r.currentGuide);
         guidelines.startedBefore = true;
       }

       guidelines[guidelines.page.pageId] = r.guidelines;

       if(cg){
         if(cg.currentGuide){
           guidelines.page.currentGuide = cg.currentGuide;
         } else {
           guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
         }
       } else {
         guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
       }

       setTimeout(function(){
         guidelines.generate(guidelines.page.currentGuide, options);
       },2000);
     }
   });
 }
     // Ends here


     // Below Code is Commented because of above changes



     // guidelines.page.actionID = subWin;
     // guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
     //
     //
     // var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+guidelines.page.pageId + '&action_id='+guidelines.page.actionID;
     // $.ajax({
     //   type: 'GET',
     //   url: fetchGuidelinesUrl,
     //   beforeSend: function(xhr){
     //     xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
     //     xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
     //   },
     //   success: function(r){
     //
     //     var cg = undefined;
     //     if(r.currentGuide){
     //       cg = JSON.parse(r.currentGuide);
     //       guidelines.startedBefore = true;
     //     }
     //
     //     guidelines[guidelines.page.pageId] = r.guidelines;
     //
     //     if(cg){
     //       if(cg.currentGuide){
     //         guidelines.page.currentGuide = cg.currentGuide;
     //       } else {
     //         guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
     //       }
     //     } else {
     //       guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
     //     }
     //
     //     setTimeout(function(){
     //       guidelines.generate(guidelines.page.currentGuide, options);
     //     },2000);
     //   }
     // });

};

guidelines.removeHangedTooltips = function(){
  MONOloop.jq('.webui-popover').remove();
}
