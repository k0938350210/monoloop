"use strict";
(function(global){
    var dashboardExperimentConfig = {
        folderManager: false,
        msg: "",
        currentStepInfo: {},
        apis: {
            getAll: "/api/reports/experiments/getAll"
        },
        formElement: {
            id: 'pageElementIdExperimentList',
        }
    };

    function timeDifference(current, previous) {

        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;

        var elapsed = current - previous;

        if (elapsed < msPerMinute) {
            return Math.round(elapsed/1000) + ' seconds ago';
        }

        else if (elapsed < msPerHour) {
            return Math.round(elapsed/msPerMinute) + ' minutes ago';
        }

        else if (elapsed < msPerDay ) {
            return Math.round(elapsed/msPerHour ) + ' hours ago';
        }

        else if (elapsed < msPerMonth) {
            return 'approximately ' + Math.round(elapsed/msPerDay) + ' days ago';
        }

        else if (elapsed < msPerYear) {
            return 'approximately ' + Math.round(elapsed/msPerMonth) + ' months ago';
        }

        else {
            return Math.round(elapsed/msPerYear) + ' Years ago';
        }
    }
    function openLayoutDashboardExperimentDetail(){
        var formElement = document.createElement('div');
        formElement.id = 'webixDashboardElement';
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "name_you_want");
        input.setAttribute("value", "value_you_want");
        document.getElementById("innerContent").appendChild(input);



        document.getElementById("innerContent").appendChild(formElement);
        var href = location.href;
        var expid = href.substr(href.lastIndexOf('/') + 1);
        webix.ready(function() {

            webix.ajax().get('/api/reports/experiments/getExperiment/'+expid, function(response, xml, xhr) {
                response = JSON.parse(response);
                var format = webix.Date.dateToStr("%M %j, %Y");
                var string = format(new Date(response[0].created_at));
                var goal = (response[0].goal_id  == null)?0:1;
                var getTemplate = function(){
                    var strVar="";
                    strVar += "<div id=\"DIV_1\">";
                    strVar += "	<!--relative class on it breaks the editor-->";
                    strVar += "	<!-- ui-view-anchor -->";
                    strVar += "	<div id=\"DIV_3\">";
                    strVar += "		<div id=\"DIV_4\">";
                    strVar += "			<!-- windows -->";
                    strVar += "			<svg id=\"svg_5\">";
                    strVar += "				<path id=\"path_6\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- apple -->";
                    strVar += "			<svg id=\"svg_7\">";
                    strVar += "				<path id=\"path_8\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- linux -->";
                    strVar += "			<svg id=\"svg_9\">";
                    strVar += "				<path id=\"path_10\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- Andorid -->";
                    strVar += "			<svg id=\"svg_11\">";
                    strVar += "				<path id=\"path_12\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- Desktop -->";
                    strVar += "			<svg id=\"svg_13\">";
                    strVar += "				<g id=\"g_14\">";
                    strVar += "					<path id=\"path_15\">";
                    strVar += "					<\/path>";
                    strVar += "				<\/g>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- Mobile -->";
                    strVar += "			<svg id=\"svg_16\">";
                    strVar += "				<path id=\"path_17\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- tablet -->";
                    strVar += "			<svg id=\"svg_18\">";
                    strVar += "				<path id=\"path_19\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- Question Mark -->";
                    strVar += "			<svg id=\"svg_20\">";
                    strVar += "				<path id=\"path_21\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- cloud download -->";
                    strVar += "			<svg id=\"svg_22\">";
                    strVar += "				<path id=\"path_23\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- ellipsize-v -->";
                    strVar += "			<svg id=\"svg_24\">";
                    strVar += "				<g id=\"g_25\">";
                    strVar += "					<g id=\"g_26\">";
                    strVar += "						<g id=\"g_27\">";
                    strVar += "							<g id=\"g_28\">";
                    strVar += "								<g id=\"g_29\">";
                    strVar += "									<g id=\"g_30\">";
                    strVar += "										<g id=\"g_31\">";
                    strVar += "											<path id=\"path_32\">";
                    strVar += "											<\/path>";
                    strVar += "											<path id=\"path_33\">";
                    strVar += "											<\/path>";
                    strVar += "											<path id=\"path_34\">";
                    strVar += "											<\/path>";
                    strVar += "										<\/g>";
                    strVar += "									<\/g>";
                    strVar += "								<\/g>";
                    strVar += "							<\/g>";
                    strVar += "						<\/g>";
                    strVar += "					<\/g>";
                    strVar += "				<\/g>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- ellipsize-h -->";
                    strVar += "			<svg id=\"svg_35\">";
                    strVar += "				<path id=\"path_36\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- pause -->";
                    strVar += "			<svg id=\"svg_37\">";
                    strVar += "				<g id=\"g_38\">";
                    strVar += "					<g id=\"g_39\">";
                    strVar += "						<g id=\"g_40\">";
                    strVar += "							<g id=\"g_41\">";
                    strVar += "								<g id=\"g_42\">";
                    strVar += "									<g id=\"g_43\">";
                    strVar += "										<g id=\"g_44\">";
                    strVar += "											<path id=\"path_45\">";
                    strVar += "											<\/path>";
                    strVar += "											<path id=\"path_46\">";
                    strVar += "											<\/path>";
                    strVar += "											<path id=\"path_47\">";
                    strVar += "											<\/path>";
                    strVar += "										<\/g>";
                    strVar += "									<\/g>";
                    strVar += "								<\/g>";
                    strVar += "							<\/g>";
                    strVar += "						<\/g>";
                    strVar += "					<\/g>";
                    strVar += "				<\/g>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- play -->";
                    strVar += "			<svg id=\"svg_48\">";
                    strVar += "				<path id=\"path_49\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- step-backward -->";
                    strVar += "			<svg id=\"svg_50\">";
                    strVar += "				<path id=\"path_51\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- step forward -->";
                    strVar += "			<svg id=\"svg_52\">";
                    strVar += "				<path id=\"path_53\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- chrome -->";
                    strVar += "			<svg id=\"svg_54\">";
                    strVar += "				<path id=\"path_55\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- safari -->";
                    strVar += "			<svg id=\"svg_56\">";
                    strVar += "				<path id=\"path_57\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- firefox -->";
                    strVar += "			<svg id=\"svg_58\">";
                    strVar += "				<path id=\"path_59\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- IE -->";
                    strVar += "			<svg id=\"svg_60\">";
                    strVar += "				<path id=\"path_61\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- opera -->";
                    strVar += "			<svg id=\"svg_62\">";
                    strVar += "				<path id=\"path_63\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "			<svg id=\"svg_64\">";
                    strVar += "				<g id=\"g_65\">";
                    strVar += "					<g id=\"g_66\">";
                    strVar += "						<g id=\"g_67\">";
                    strVar += "							<path id=\"path_68\">";
                    strVar += "							<\/path>";
                    strVar += "						<\/g>";
                    strVar += "					<\/g>";
                    strVar += "				<\/g>";
                    strVar += "			<\/svg>";
                    strVar += "			<svg id=\"svg_69\">";
                    strVar += "				<g id=\"g_70\">";
                    strVar += "					<g id=\"g_71\">";
                    strVar += "						<g id=\"g_72\">";
                    strVar += "							<rect id=\"rect_73\">";
                    strVar += "							<\/rect>";
                    strVar += "							<rect id=\"rect_74\">";
                    strVar += "							<\/rect>";
                    strVar += "							<rect id=\"rect_75\">";
                    strVar += "							<\/rect>";
                    strVar += "							<rect id=\"rect_76\">";
                    strVar += "							<\/rect>";
                    strVar += "						<\/g>";
                    strVar += "					<\/g>";
                    strVar += "				<\/g>";
                    strVar += "			<\/svg>";
                    strVar += "			<svg id=\"svg_77\">";
                    strVar += "				<g id=\"g_78\">";
                    strVar += "					<g id=\"g_79\">";
                    strVar += "						<path id=\"path_80\">";
                    strVar += "						<\/path>";
                    strVar += "					<\/g>";
                    strVar += "				<\/g>";
                    strVar += "			<\/svg>";
                    strVar += "			<svg id=\"svg_81\">";
                    strVar += "				<g id=\"g_82\">";
                    strVar += "					<g id=\"g_83\">";
                    strVar += "						<path id=\"path_84\">";
                    strVar += "						<\/path>";
                    strVar += "					<\/g>";
                    strVar += "				<\/g>";
                    strVar += "			<\/svg>";
                    strVar += "			<!-- Share icon -->";
                    strVar += "			<svg id=\"svg_85\">";
                    strVar += "				<path id=\"path_86\">";
                    strVar += "				<\/path>";
                    strVar += "			<\/svg>";
                    strVar += "		<\/div>";
                    strVar += "		<ul id=\"UL_142\">";
                    strVar += "			<li id=\"LI_143\">";
                    strVar += "				<a href=\"#\/campaign\/2\/summary\" id=\"A_144\">Summary<\/a>";
                    strVar += "			<\/li>";
                    strVar += "			<li id=\"LI_145\">";
                    strVar += "<a href=\"#\/campaign\/2\/report\" id=\"A_146\">Detailed Report<\/a>";
                    strVar += "				<ul id=\"UL_147\">";
                    strVar += "					<!-- ngIf: !isSurveyCampaign -->";
                    strVar += "					<li id=\"LI_148\">";
                    strVar += "						 <a href=\"#\/campaign\/2\/report\" id=\"A_149\">Goals<\/a>";
                    strVar += "					<\/li>";
                    strVar += "					<!-- end ngIf: !isSurveyCampaign -->";
                    strVar += "					<!-- ngIf: !isSurveyCampaign && !isUsabilityCampaign && !isConversionCampaign -->";
                    strVar += "					<li id=\"LI_150\">";
                    strVar += "						 <a href=\"#\/campaign\/2\/report\/variations\" id=\"A_151\">Variations<\/a>";
                    strVar += "					<\/li>";
                    strVar += "					<!-- end ngIf: !isSurveyCampaign && !isUsabilityCampaign && !isConversionCampaign -->";
                    strVar += "					<!-- ngIf: campaign.surveys.length && !isSurveyCampaign -->";
                    strVar += "					<!-- ngIf: isSurveyCampaign -->";
                    strVar += "					<!-- ngIf: isSurveyCampaign -->";
                    strVar += "					<!-- ngIf: isSurveyCampaign -->";
                    strVar += "				<\/ul>";
                    strVar += "				<!-- end ngIf: canShowDetailedReport -->";
                    strVar += "			<\/li>";
                    strVar += "			<!-- end ngIf: !isHeatmapCampaign -->";
                    strVar += "			<!-- ngIf: canShowPreview -->";
                    strVar += "			<li id=\"LI_152\">";
                    strVar += "				 <a href=\"#\/campaign\/2\/preview\" id=\"A_153\">Previews<\/a>";
                    strVar += "			<\/li>";
                    strVar += "			<!-- end ngIf: canShowPreview -->";
                    strVar += "			<!-- ngIf: isAnalyticsCampaign && campaign.analysisConfig.isVisitorRecordingsEnabled -->";
                    strVar += "			<!-- ngIf: isAnalyticsCampaign && campaign.analysisConfig.isFormAnalysisEnabled -->";
                    strVar += "			<!-- ngIf: !isShareView -->";
                    strVar += "			<li id=\"LI_157\">";
                    strVar += "				 <a href=\"#\/campaign\/2\/edit\" id=\"A_158\">Settings<\/a>";
                    strVar += "				<ul id=\"UL_159\">";
                    strVar += "					<!-- ngRepeat: step in steps -->";
                    strVar += "					<!-- ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<li id=\"LI_160\">";
                    strVar += "						 <a href=\"#\/campaign\/2\/edit\" id=\"A_161\">Summary<\/a>";
                    strVar += "					<\/li>";
                    strVar += "					<!-- end ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<!-- end ngRepeat: step in steps -->";
                    strVar += "					<!-- ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<li id=\"LI_162\">";
                    strVar += "						 <a href=\"#\/campaign\/2\/edit\/urls\" id=\"A_163\">URL(s)<\/a>";
                    strVar += "					<\/li>";
                    strVar += "					<!-- end ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<!-- end ngRepeat: step in steps -->";
                    strVar += "					<!-- ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<li id=\"LI_164\">";
                    strVar += "						 <a href=\"#\/campaign\/2\/edit\/variations\" id=\"A_165\">Variations<\/a>";
                    strVar += "					<\/li>";
                    strVar += "					<!-- end ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<!-- end ngRepeat: step in steps -->";
                    strVar += "					<!-- ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<li id=\"LI_166\">";
                    strVar += "						 <a href=\"#\/campaign\/2\/edit\/goals\" id=\"A_167\">Goals<\/a>";
                    strVar += "					<\/li>";
                    strVar += "					<!-- end ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<!-- end ngRepeat: step in steps -->";
                    strVar += "					<!-- ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<li id=\"LI_168\">";
                    strVar += "						 <a href=\"#\/campaign\/2\/edit\/others\" id=\"A_169\">Others<\/a>";
                    strVar += "					<\/li>";
                    strVar += "					<!-- end ngIf: CampaignSectionEnum.DURATION !== step.id || (CampaignSectionEnum.DURATION === step.id && isSidakEnabled) -->";
                    strVar += "					<!-- end ngRepeat: step in steps -->";
                    strVar += "				<\/ul>";
                    strVar += "			<\/li>";
                    strVar += "			<!-- end ngIf: !isShareView -->";
                    strVar += "			<!-- ngIf: isShareView -->";
                    strVar += "		<\/ul>";
                    strVar += "		<!-- end ngIf: !campaign.isCodeShareView -->";
                    strVar += "		<!-- ui-view-anchor -->";
                    strVar += "		<div id=\"DIV_170\">";
                    strVar += "			<div id=\"DIV_171\">";
                    strVar += "				<div id=\"DIV_172\">";
                    strVar += "					<!-- ngIf: canShowNewNotification && campaign.status !== CampaignStatusEnum.NOT_STARTED && campaignData.notifications.length -->";
                    strVar += "					<div id=\"DIV_173\">";
                    strVar += "						<div id=\"DIV_174\">";
                    strVar += "							<div id=\"DIV_175\">";
                    strVar += "								<img src=\"\/assets\/images\/low-traffic.svg\" id=\"IMG_176\" alt='' \/>";
                    strVar += "							<\/div>";
                    strVar += "							<div id=\"DIV_177\">";
                    strVar += "								<div id=\"DIV_178\">";
                    strVar += "									No visitors have been tracked yet!";
                    strVar += "									<div id=\"DIV_179\">";
                    strVar += "										<span id=\"SPAN_180\"><strong id=\"STRONG_181\">Recommended action:<\/strong><\/span> Please ensure that the VWO smart code is installed correctly across all the campaign pages.";
                    strVar += "									<\/div>";
                    strVar += "								<\/div>";
                    strVar += "							<\/div>";
                    strVar += "							<!-- ngIf: isBayesian && canChangeCampaignStatus() -->";
                    strVar += "							<div id=\"DIV_182\">";
                    strVar += "								<div id=\"DIV_183\">";
                    strVar += "									<div id=\"DIV_184\">";
                    strVar += "										<div id=\"DIV_185\">";
                    strVar += "											<div id=\"DIV_186\">";
                    strVar += "												<!-- ngIf: !gearMode -->";
                    strVar += " <a id=\"A_187\"><span id=\"SPAN_188\"><\/span><span id=\"SPAN_189\"><\/span><i id=\"I_190\"><\/i><\/a>";
                    strVar += "												<!-- end ngIf: !gearMode -->";
                    strVar += "												<!-- ngIf: gearMode -->";
                    strVar += "												<div id=\"DIV_191\">";
                    strVar += "													Certainty vs Speed";
                    strVar += "													<div id=\"DIV_192\">";
                    strVar += "														Select a certainty mode based on whether you require quick predictions for a winning variation or a more certain winner based on more visitor data";
                    strVar += "													<\/div>";
                    strVar += "													<ul id=\"UL_193\">";
                    strVar += "														<li id=\"LI_194\">";
                    strVar += "															 <a id=\"A_195\"><span id=\"SPAN_196\"><\/span><\/a>";
                    strVar += "															<svg id=\"svg_197\">";
                    strVar += "																<use id=\"use_198\">";
                    strVar += "																<\/use>";
                    strVar += "															<\/svg> <span id=\"SPAN_199\">High Certainty<\/span>";
                    strVar += "														<\/li>";
                    strVar += "														<li id=\"LI_200\">";
                    strVar += "															 <a id=\"A_201\"><span id=\"SPAN_202\"><\/span><\/a>";
                    strVar += "															<svg id=\"svg_203\">";
                    strVar += "																<use id=\"use_204\">";
                    strVar += "																<\/use>";
                    strVar += "															<\/svg> <span id=\"SPAN_205\">Balanced Mode<\/span>";
                    strVar += "														<\/li>";
                    strVar += "														<li id=\"LI_206\">";
                    strVar += "															 <a id=\"A_207\"><span id=\"SPAN_208\"><\/span><\/a>";
                    strVar += "															<svg id=\"svg_209\">";
                    strVar += "																<use id=\"use_210\">";
                    strVar += "																<\/use>";
                    strVar += "															<\/svg> <span id=\"SPAN_211\">Quick Learnings<\/span>";
                    strVar += "														<\/li>";
                    strVar += "													<\/ul>";
                    strVar += "													<div id=\"DIV_212\">";
                    strVar += "														<i id=\"I_213\"><\/i>";
                    strVar += "														<div id=\"DIV_214\">";
                    strVar += "															This is an extreme selection. You are compromising on certainty for quickness. To be on the safer side, select Balanced or High Certainty mode.";
                    strVar += "														<\/div>";
                    strVar += "													<\/div>";
                    strVar += "												<\/div>";
                    strVar += "											<\/div>";
                    strVar += "											<!-- end .btn-group -->";
                    strVar += "											<svg id=\"svg_215\">";
                    strVar += "												<defs id=\"defs_216\">";
                    strVar += "													<g id=\"g_217\">";
                    strVar += "														<g id=\"g_218\">";
                    strVar += "															<g id=\"g_219\">";
                    strVar += "																<g id=\"g_220\">";
                    strVar += "																	<g id=\"g_221\">";
                    strVar += "																		<path id=\"path_222\">";
                    strVar += "																		<\/path>";
                    strVar += "																	<\/g>";
                    strVar += "																<\/g>";
                    strVar += "															<\/g>";
                    strVar += "														<\/g>";
                    strVar += "													<\/g>";
                    strVar += "													<g id=\"g_223\">";
                    strVar += "														<g id=\"g_224\">";
                    strVar += "															<g id=\"g_225\">";
                    strVar += "																<g id=\"g_226\">";
                    strVar += "																	<g id=\"g_227\">";
                    strVar += "																		<path id=\"path_228\">";
                    strVar += "																		<\/path>";
                    strVar += "																	<\/g>";
                    strVar += "																<\/g>";
                    strVar += "															<\/g>";
                    strVar += "														<\/g>";
                    strVar += "													<\/g>";
                    strVar += "													<g id=\"g_229\">";
                    strVar += "														<g id=\"g_230\">";
                    strVar += "															<g id=\"g_231\">";
                    strVar += "																<g id=\"g_232\">";
                    strVar += "																	<g id=\"g_233\">";
                    strVar += "																		<path id=\"path_234\">";
                    strVar += "																		<\/path>";
                    strVar += "																		<path id=\"path_235\">";
                    strVar += "																		<\/path>";
                    strVar += "																		<path id=\"path_236\">";
                    strVar += "																		<\/path>";
                    strVar += "																	<\/g>";
                    strVar += "																<\/g>";
                    strVar += "															<\/g>";
                    strVar += "														<\/g>";
                    strVar += "													<\/g>";
                    strVar += "												<\/defs>";
                    strVar += "											<\/svg>";
                    strVar += "										<\/div>";
                    strVar += "									<\/div>";
                    strVar += "								<\/div>";
                    strVar += "							<\/div>";
                    strVar += "							<!-- end ngIf: isBayesian && canChangeCampaignStatus() -->";
                    strVar += "						<\/div>";
                    strVar += "					<\/div>";
                    strVar += "					<!-- end ngIf: canShowNewNotification && campaign.status !== CampaignStatusEnum.NOT_STARTED && campaignData.notifications.length -->";
                    strVar += "					<div id=\"DIV_237\">";
                    strVar += "						<!-- ngRepeat: widget in widgets track by $index -->";
                    strVar += "						<div id=\"DIV_238\">";
                    strVar += "							<div id=\"DIV_239\">";
                    strVar += "								<div id=\"DIV_240\">";
                    strVar += "									<!-- ngIf: isCreatedOnToBeShown -->";
                    strVar += "									<div id=\"DIV_241\">";
                    strVar += "										<dl id=\"DL_242\">";
                    strVar += "											<dt id=\"DT_243\">";
                    strVar += "												<h3 id=\"H3_244\">";
                    strVar += "													Created On";
                    strVar += "												<\/h3>";
                    strVar += "											<\/dt>";
                    strVar += "											<dd id=\"DD_245\">";
                    strVar += "												Jun 27, 2016";
                    strVar += "											<\/dd>";
                    strVar += "										<\/dl>";
                    strVar += "									<\/div>";
                    strVar += "									<!-- end ngIf: isCreatedOnToBeShown -->";
                    strVar += "									<!-- ngIf: !isUsabilityCampaign && !isHeatmapCampaign && !isAnalyticsCampaign -->";
                    strVar += "									<div id=\"DIV_246\">";
                    strVar += "										<dl id=\"DL_247\">";
                    strVar += "											<dt id=\"DT_248\">";
                    strVar += "												<h3 id=\"H3_249\">";
                    strVar += "													Goals";
                    strVar += "												<\/h3>";
                    strVar += "											<\/dt>";
                    strVar += "											<dd id=\"DD_250\">";
                    strVar += "												<h2 id=\"H2_251\">";
                    strVar += "													1";
                    strVar += "												<\/h2>";
                    strVar += "											<\/dd>";
                    strVar += "										<\/dl>";
                    strVar += "										<!-- ngIf: isMVTCampaign -->";
                    strVar += "										<!-- ngIf: !isConversionCampaign && !isMVTCampaign -->";
                    strVar += "										<dl id=\"DL_252\">";
                    strVar += "											<dt id=\"DT_253\">";
                    strVar += "												<h3 id=\"H3_254\">";
                    strVar += "													Variations";
                    strVar += "												<\/h3>";
                    strVar += "											<\/dt>";
                    strVar += "											<dd id=\"DD_255\">";
                    strVar += "												<h2 id=\"H2_256\">";
                    strVar += "													3";
                    strVar += "												<\/h2>";
                    strVar += "											<\/dd>";
                    strVar += "										<\/dl>";
                    strVar += "										<!-- end ngIf: !isConversionCampaign && !isMVTCampaign -->";
                    strVar += "<i id=\"I_257\"><\/i>";
                    strVar += "									<\/div>";
                    strVar += "									<!-- end ngIf: !isUsabilityCampaign && !isHeatmapCampaign && !isAnalyticsCampaign -->";
                    strVar += "									<!-- ngIf: isUsabilityCampaign -->";
                    strVar += "									<!-- ngIf: isHeatmapCampaign -->";
                    strVar += "									<!-- ngIf: isAnalyticsHeatmapSummary() -->";
                    strVar += "									<!-- ngIf: isAnalyticsFormSummary() -->";
                    strVar += "									<!-- ngIf: isAnalyticsRecordingsSummary() -->";
                    strVar += "									<!-- ngIf: isAnalyticsVisitorInfoSummary() -->";
                    strVar += "								<\/div>";
                    strVar += "							<\/div>";
                    strVar += "						<\/div>";
                    strVar += "						<!-- end ngRepeat: widget in widgets track by $index -->";
                    strVar += "						<div id=\"DIV_258\">";
                    strVar += "							<div id=\"DIV_259\">";
                    strVar += "								<div id=\"DIV_260\">";
                    strVar += "									<div id=\"DIV_261\">";
                    strVar += "										<!-- ngIf: !isThumbnailClickable -->";
                    strVar += "										<!-- ngIf: isThumbnailClickable -->";
                    strVar += "										<div id=\"DIV_262\">";
                    strVar += "											<div>";
                    strVar += "												0 || isGeneratingQuickScreenshot, 'is-upvoted': voteStatus === 1, 'is-downvoted': voteStatus === -1}\" data-on-thumbnail-clicked=\"onThumbnailClicked()\" data-quick-img-url=\"\/\/s3.amazonaws.com\/wfyss.visualwebsiteoptimizer.com\/247799\/2_1_2cda9df1d193c41699b1c3fd920311d3.png\" data-browser=\"quickScreenshotData.browserId\" data-variation=\"1\" data-hide-overlay=\"true\" id=\"DIV_263\"><img alt='' \/> 0 || (isQuick &amp;&amp; !imgUrl)\" vwo-on-error=\"onImgError()\" src=\"\/\/s3.amazonaws.com\/wfyss.visualwebsiteoptimizer.com\/247799\/2_1_2cda9df1d193c41699b1c3fd920311d3.png\" id=\"IMG_264\">";
                    strVar += "												<div id=\"DIV_265\">";
                    strVar += "												<\/div>";
                    strVar += "												<!-- ngIf: !hideOverlay -->";
                    strVar += "												<div id=\"DIV_266\">";
                    strVar += "													<i id=\"I_267\"><\/i>";
                    strVar += "													<div id=\"DIV_268\">";
                    strVar += "														 <span id=\"SPAN_269\">Preview not generated.<\/span>";
                    strVar += "														<div id=\"DIV_270\">";
                    strVar += "															Please ensure that code is installed on the page.";
                    strVar += "														<\/div>";
                    strVar += "													<\/div>";
                    strVar += "												<\/div>";
                    strVar += "												<div>";
                    strVar += "													0 || isGeneratingQuickScreenshot\" class=\"screenshot-thumb__generating-msg-wrap ng-hide\" id=\"DIV_271\"><span id=\"SPAN_272\"><\/span>";
                    strVar += "													<div id=\"DIV_273\">";
                    strVar += "														Generating Preview";
                    strVar += "													<\/div>";
                    strVar += "													<!-- ngIf: !isQuick -->";
                    strVar += "												<\/div>";
                    strVar += "												<div id=\"DIV_274\">";
                    strVar += "													<div id=\"DIV_275\">";
                    strVar += "														Browser excluded";
                    strVar += "													<\/div>";
                    strVar += "												<\/div>";
                    strVar += "												<!-- ngIf: !hideOverlay -->";
                    strVar += "												<!-- <span ng-show=\"queueObject.waitTime === -1\" class=\"soft-half\">Sorry, an error occured while generating previews.<br><\/span> -->";
                    strVar += "												<!-- ngIf: !hideOverlay -->";
                    strVar += "											<\/div>";
                    strVar += "										<\/div>";
                    strVar += "										<!-- end ngIf: isThumbnailClickable -->";
                    strVar += "									<\/div>";
                    strVar += "								<\/div>";
                    strVar += "							<\/div>";
                    strVar += "						<\/div>";
                    strVar += "						<!-- end ngRepeat: widget in widgets track by $index -->";
                    strVar += "					<\/div>";
                    strVar += "				<\/div>";
                    strVar += "				<!-- ngIf: !isMobile -->";
                    strVar += "				<div id=\"DIV_276\">";
                    strVar += "					<div id=\"DIV_277\">";
                    strVar += "						<!-- ngIf: !isShareView -->";
                    strVar += "						<div id=\"DIV_278\">";
                    strVar += "							<h5 id=\"H5_279\">";
                    strVar += "								Timeline";
                    strVar += "							<\/h5>";
                    strVar += "						<\/div>";
                    strVar += "						<!-- end ngIf: !isShareView -->";
                    strVar += "						<!-- ngIf: !isShareView -->";
                    strVar += "						<div id=\"DIV_280\">";
                    strVar += "							<ul id=\"UL_281\">";
                    strVar += "								<!-- ngRepeat: item in items track by item.id -->";
                    strVar += "								<li id=\"LI_282\">";
                    strVar += "									<div id=\"DIV_283\">";
                    strVar += "										<div id=\"DIV_284\">";
                    strVar += "											04 Jul, 13:14<img width=\"24\" height=\"24\" alt=\"\" src=\"\/\/gravatar.com\/avatar\/e5ccc46d3217e0d3738d93fc0ee59c1d?s=24&amp;d=https%3A%2F%2Fapp.vwo.com%2Fassets%2Fimages%2Favatar-new.png\" id=\"IMG_285\" \/>";
                    strVar += "										<\/div> <strong id=\"STRONG_286\">Sheraz Javed<\/strong>";
                    strVar += "									<\/div>";
                    strVar += "									<p id=\"P_287\">";
                    strVar += "										Campaign <b id=\"B_288\">started<\/b>";
                    strVar += "									<\/p>";
                    strVar += "									<!-- ngRepeat: note in item.notes -->";
                    strVar += "								<\/li>";
                    strVar += "								<!-- end ngRepeat: item in items track by item.id -->";
                    strVar += "								<li id=\"LI_289\">";
                    strVar += "									<div id=\"DIV_290\">";
                    strVar += "										<div id=\"DIV_291\">";
                    strVar += "											27 Jun, 14:02<img width=\"24\" height=\"24\" alt=\"\" src=\"\/\/gravatar.com\/avatar\/e5ccc46d3217e0d3738d93fc0ee59c1d?s=24&amp;d=https%3A%2F%2Fapp.vwo.com%2Fassets%2Fimages%2Favatar-new.png\" id=\"IMG_292\" \/>";
                    strVar += "										<\/div> <strong id=\"STRONG_293\">Sheraz Javed<\/strong>";
                    strVar += "									<\/div>";
                    strVar += "									<p id=\"P_294\">";
                    strVar += "										Campaign created with 3 variations and 1 goal";
                    strVar += "									<\/p>";
                    strVar += "									<!-- ngRepeat: note in item.notes -->";
                    strVar += "								<\/li>";
                    strVar += "								<!-- end ngRepeat: item in items track by item.id -->";
                    strVar += "								<li id=\"LI_295\">";
                    strVar += "									<span id=\"SPAN_296\"><\/span>";
                    strVar += "								<\/li>";
                    strVar += "								<li id=\"LI_297\">";
                    strVar += "									 <a title=\"Load more feeds\" id=\"A_298\">Load More<\/a>";
                    strVar += "								<\/li>";
                    strVar += "								<li id=\"LI_299\">";
                    strVar += "									You have no more feeds.";
                    strVar += "								<\/li>";
                    strVar += "								<li id=\"LI_300\">";
                    strVar += "									You have no feeds yet.";
                    strVar += "								<\/li>";
                    strVar += "							<\/ul>";
                    strVar += "							<!-- <a ng-if=\"!isShareView\" href=\"#\" class=\"btn btn--small theme--grey\">Load more<\/a> -->";
                    strVar += "						<\/div>";
                    strVar += "						<!-- end ngIf: !isShareView -->";
                    strVar += "					<\/div>";
                    strVar += "				<\/div>";
                    strVar += "				<!-- end ngIf: !isMobile -->";
                    strVar += "			<\/div>";
                    strVar += "		<\/div>";
                    strVar += "	<\/div>";
                    strVar += "	<div id=\"DIV_301\">";
                    strVar += "		<span id=\"SPAN_302\"><\/span><span id=\"SPAN_303\"><\/span><i id=\"I_304\"><\/i><span id=\"SPAN_305\"><\/span>";
                    strVar += "	<\/div>";
                    strVar += "	<div id=\"DIV_306\">";
                    strVar += "		<!-- HACK: Footer hidden in onboarding to remove scrolling -->";
                    strVar += "		<!-- ngIf: state.current.name.indexOf('onboard') === -1 && state.current.name.indexOf('create') === -1 -->";
                    strVar += "		<footer id=\"FOOTER_307\">";
                    strVar += "			<!-- ngIf: !isMobile -->";
                    strVar += "			<div id=\"DIV_308\">";
                    strVar += "				<!-- ngIf: !isMobile -->";
                    strVar += "				<div id=\"DIV_309\">";
                    strVar += "					 <a title=\"Raise a ticket\" id=\"A_310\">Get Support<\/a> <a title=\"Developers\" href=\"#\/developers\" id=\"A_311\">Developers<\/a> <a href=\"tel:+1-844-822-8378\" title=\"Give us a call\" id=\"A_312\">+1-844-822-8378<\/a>";
                    strVar += "				<\/div>";
                    strVar += "				<!-- end ngIf: !isMobile -->";
                    strVar += "				<div id=\"DIV_313\">";
                    strVar += "					<a title=\"Visit wingify.com\" href=\"http:\/\/wingify.com\" id=\"A_314\"><\/a>";
                    strVar += "				<\/div>";
                    strVar += "			<\/div>";
                    strVar += "			<!-- end ngIf: !isMobile -->";
                    strVar += "			<div id=\"DIV_315\">";
                    strVar += "				<div id=\"DIV_316\">";
                    strVar += "					 <a href=\"#\" id=\"A_317\">Request Desktop Site<\/a>";
                    strVar += "				<\/div>";
                    strVar += "				<div id=\"DIV_318\">";
                    strVar += "					<!-- ngIf: !isShareView -->";
                    strVar += "					<ul id=\"UL_319\">";
                    strVar += "						<li id=\"LI_320\">";
                    strVar += "							 <a id=\"A_321\">Show who else is logged on in this account<\/a>";
                    strVar += "						<\/li>";
                    strVar += "						<li id=\"LI_322\">";
                    strVar += "							 <a href=\"http:\/\/stats.pingdom.com\/yd4ybaf8hhh2\" id=\"A_323\">Uptime Status<\/a>";
                    strVar += "						<\/li>";
                    strVar += "					<\/ul>";
                    strVar += "					<!-- end ngIf: !isShareView -->";
                    strVar += "				<\/div>";
                    strVar += "				<ul id=\"UL_324\">";
                    strVar += "					<li id=\"LI_325\">";
                    strVar += "						 <a id=\"A_326\">English<\/a>";
                    strVar += "					<\/li>";
                    strVar += "					<li id=\"LI_327\">";
                    strVar += "						 <a id=\"A_328\">日本語<\/a>";
                    strVar += "					<\/li>";
                    strVar += "				<\/ul>";
                    strVar += "			<\/div>";
                    strVar += "		<\/footer>";
                    strVar += "		<!-- end ngIf: state.current.name.indexOf('onboard') === -1 && state.current.name.indexOf('create') === -1 -->";
                    strVar += "	<\/div>";
                    strVar += "<\/div>";

                    return strVar;
                };
                var getStaticBox = function(){
                    var staticBox="";
                    staticBox += "<div id=\"ng-isolate-scope\">";
                    staticBox += "	<div id=\"ng-summary\">";
                    staticBox += "		<div id=\"ng-stats\">";
                    staticBox += "			<div id=\"ng-stat-group\">";
                    staticBox += "				<dl id=\"ng-date-scope\">";
                    staticBox += "					<dt id=\"ng-date-created\">";
                    staticBox += "						<h3 id=\"ng-date-summary\">";
                    staticBox += "							Created On";
                    staticBox += "						<\/h3>";
                    staticBox += "					<\/dt>";
                    staticBox += "					<dd id=\"gn-scope-date\">";
                    staticBox +=                    string;
                    staticBox += "					<\/dd>";
                    staticBox += "				<\/dl>";
                    staticBox += "			<\/div>";
                    staticBox += "			<div id=\"ng-goals-main\">";
                    staticBox += "				<dl id=\"ng-goals-sub\">";
                    staticBox += "					<dt id=\"ng-goals-head\">";
                    staticBox += "						<h3 id=\"ng-goals-heading\">";
                    staticBox += "							Goals";
                    staticBox += "						<\/h3>";
                    staticBox += "					<\/dt>";
                    staticBox += "					<dd id=\"ng-goals-no\">";
                    staticBox += "						<h2 id=\"ng-goals-count\">";
                    staticBox +=                          goal;
                    staticBox += "						<\/h2>";
                    staticBox += "					<\/dd>";
                    staticBox += "				<\/dl>";
                    staticBox += "			<\/div>";
                    staticBox += "		<\/div>";
                    staticBox += "	<\/div>";
                    staticBox += "<\/div>";
                    return staticBox;
                };

                var mainDescription = "";
                mainDescription += "		<div id=\"DIV_87\">";
                mainDescription += "			<div id=\"DIV_88\">";
                mainDescription += "				<div id=\"DIV_89\">";
                mainDescription += " <a title=\"Download campaign reports\" href=\"\/campaign\/2\/download\" id=\"A_105\"><i id=\"I_106\"><\/i><\/a>";
                mainDescription += " <a title=\"Print campaign reports\" href=\"#\/campaign\/2\/print-view\" id=\"A_107\"><i id=\"I_108\"><\/i><\/a>";
                mainDescription += "					<div id=\"DIV_109\">";
                mainDescription += "						 <a title=\"More options\" href=\"javascript:void(0)\" id=\"A_110\"><i id=\"I_111\"><\/i><i id=\"I_112\"><\/i><\/a>";
                mainDescription += "						<ul id=\"UL_113\">";
                mainDescription += "							<li id=\"LI_114\">";
                mainDescription += "								<button id=\"BUTTON_115\">";
                mainDescription += "									Edit in Campaign Builder";
                mainDescription += "								<\/button>";
                mainDescription += "							<\/li>";
                mainDescription += "							<li id=\"LI_116\">";
                mainDescription += "							<\/li>";
                mainDescription += "							<li id=\"LI_117\">";
                mainDescription += "								 ";
                mainDescription += "								<button id=\"BUTTON_118\">";
                mainDescription += "									Archive Campaign";
                mainDescription += "								<\/button>";
                mainDescription += "							<\/li>";
                mainDescription += "							<li id=\"LI_119\">";
                mainDescription += "								 ";
                mainDescription += "								<button id=\"BUTTON_120\">";
                mainDescription += "									Trash Campaign";
                mainDescription += "								<\/button>";
                mainDescription += "							<\/li>";
                mainDescription += "							<li id=\"LI_121\">";
                mainDescription += "								 ";
                mainDescription += "								<button id=\"BUTTON_122\">";
                mainDescription += "									Flush Data";
                mainDescription += "								<\/button>";
                mainDescription += "							<\/li>";
                mainDescription += "							<li id=\"LI_123\">";
                mainDescription += "								 ";
                mainDescription += "								<button id=\"BUTTON_124\">";
                mainDescription += "									Clone Campaign";
                mainDescription += "								<\/button>";
                mainDescription += "							<\/li>";
                mainDescription += "						<\/ul>";
                mainDescription += "					<\/div>";
                mainDescription += "				<\/div>";
                mainDescription += "				<div id=\"DIV_125\">";
                mainDescription += "					<button id=\"BUTTON_126\">";
                mainDescription += "						<span id=\"SPAN_127\"><\/span><i id=\"I_128\"><\/i> <span id=\"SPAN_129\">Start<\/span> <span id=\"SPAN_130\">Running<\/span>";
                mainDescription += "					<\/button>";
                mainDescription += "					<button id=\"BUTTON_131\">";
                mainDescription += "						<span id=\"SPAN_132\"><\/span><i id=\"I_133\"><\/i> <span id=\"SPAN_134\">Pause<\/span> <span id=\"SPAN_135\">Paused<\/span>";
                mainDescription += "					<\/button>";
                mainDescription += "				<\/div>";
                mainDescription += "				<div id=\"DIV_136\">";
                mainDescription += "					<i id=\"I_137\"><\/i>";
                mainDescription += "					<div id=\"DIV_138\">";
                mainDescription += "						<h3 id=\"H3_139\">";
                mainDescription += "							#name#";
                mainDescription += "						<\/h3>";
                mainDescription += "<a href=\"http:\/\/mail.google.com\/\" id=\"A_140\">http:\/\/mail.google.com\/<\/a>";
                mainDescription += " <span id=\"SPAN_141\">Sample Report<\/span>";
                mainDescription += "					<\/div>";
                mainDescription += "				<\/div>";
                mainDescription += "			<\/div>";
                mainDescription += "		<\/div>";

                webix.ajax().get('/api/account/logs', function(response, xml, xhr) {
                });

                var timeline = "<div id=\"DIV_276\">";
                timeline += "<div id=\"DIV_277\">";
                timeline += "<div id=\"DIV_278\">";
                timeline += "<h5 id=\"H5_279\">";
                timeline += "Timeline";
                timeline += "<\/h5>";
                timeline += "<\/div>";
                timeline += "<div id=\"DIV_280\">";
                timeline += "							<ul id=\"UL_281\">";
                timeline += "								<li id=\"LI_282\">";
                timeline += "									<div id=\"DIV_283\">";
                timeline += "										<div id=\"DIV_284\">";
                timeline += "											04 Jul, 13:14<img width=\"24\" height=\"24\" alt=\"\" src=\"\/\/gravatar.com\/avatar\/e5ccc46d3217e0d3738d93fc0ee59c1d?s=24&amp;d=https%3A%2F%2Fapp.vwo.com%2Fassets%2Fimages%2Favatar-new.png\" id=\"IMG_285\" \/>";
                timeline += "										<\/div> <strong id=\"STRONG_286\" style=\"text: wrap\">Webshop<\/strong>";
                timeline += "									<\/div>";
                timeline += "									<p id=\"P_287\">";
                timeline += "										Experiment  <b id=\"B_288\">started<\/b>";
                timeline += "									<\/p>";
                timeline += "								<\/li>";
                timeline += "								<li id=\"LI_289\">";
                timeline += "									<div id=\"DIV_290\">";
                timeline += "										<div id=\"DIV_291\">";
                timeline += "											27 Jun, 14:02<img width=\"24\" height=\"24\" alt=\"\" src=\"\/\/gravatar.com\/avatar\/e5ccc46d3217e0d3738d93fc0ee59c1d?s=24&amp;d=https%3A%2F%2Fapp.vwo.com%2Fassets%2Fimages%2Favatar-new.png\" id=\"IMG_292\" \/>";
                timeline += "										<\/div> <strong id=\"STRONG_293\">Webshop<\/strong>";
                timeline += "									<\/div>";
                timeline += "									<p id=\"P_294\">";
                timeline += "										Experiment created with 1 variations and 1 goal";
                timeline += "									<\/p>";
                timeline += "									<!-- ngRepeat: note in item.notes -->";
                timeline += "								<\/li>";
                timeline += "								<!-- end ngRepeat: item in items track by item.id -->";
                timeline += "								<li id=\"LI_295\">";
                timeline += "									<span id=\"SPAN_296\"><\/span>";
                timeline += "								<\/li>";
                timeline += "								<li id=\"LI_297\">";
                timeline += "									 <a title=\"Load more feeds\" id=\"A_298\">Load More<\/a>";
                timeline += "								<\/li>";
                timeline += "								<li id=\"LI_299\">";
                timeline += "									You have no more feeds.";
                timeline += "								<\/li>";
                timeline += "								<li id=\"LI_300\">";
                timeline += "									You have no feeds yet.";
                timeline += "								<\/li>";
                timeline += "							<\/ul>";
                timeline += "<\/div>";


                var tabbar = {
                    view:"tabbar", type:"bottom", multiview:true, options: [
                        { value: "<li id=\"LI_143\"><a href=\"#\/campaign\/2\/summary\" id=\"A_144\">Summary<\/a><\/li>", id: 'listView', css:'custom' },
                        { value: "<li id=\"LI_145\"><a href=\"#\/campaign\/2\/report\" id=\"A_146\">Detailed Report<\/a><\/li>", id: 'aboutView' },
                        { value: "" },
                        { value: "" },
                        { value: "",id: "timeline"}
                    ],height:40, width: 900, margin: 0
                };

                webix.ajax().post("/api/reports/experiments/getControlGroups","eID="+expid, function(response, xml, xhr){


                });
                var data = {
                    cells:[
                        {
                            id:"listView",
                            height: 900,
                            width: 660,
                            type:"space", rows:[
                                {template: getStaticBox()},
                            { view:"d3-chart",
                                resize:true,
                                data:{},
                                ready:function() {
                                    $(this.$view).append('<form>'+
                                    '<label><input type="radio" name="dataset" id="dataset" value="total" checked> Total</label>'+
                                    '<label><input type="radio" name="dataset" id="dataset" value="option1"> Option 1</label>'+
                                    '<label><input type="radio" name="dataset" id="dataset" value="option2"> Option 2</label>'+
                                    '</form>');



                                    var svg = d3.select(this.$view)
                                        .append("svg")
                                        .append("g")
                                    svg.append("g")
                                        .attr("class", "slices");
                                    svg.append("g")
                                        .attr("class", "labels");
                                    svg.append("g")
                                        .attr("class", "lines");
                                    var width = 960,
                                        height = 450,
                                        radius = Math.min(width, height) / 2;
                                    var pie = d3.layout.pie()
                                        .sort(null)
                                        .value(function(d) {
                                            return d.value;
                                        });
                                    var arc = d3.svg.arc()
                                        .outerRadius(radius * 0.8)
                                        .innerRadius(radius * 0.4);
                                    var outerArc = d3.svg.arc()
                                        .innerRadius(radius * 0.9)
                                        .outerRadius(radius * 0.9);
                                    svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
                                    var key = function(d){ return d.data.label; };
                                    var color = d3.scale.ordinal()
                                        .domain(["Lorem ipsum", "dolor sit", "amet", "consectetur", "adipisicing", "elit", "sed", "do", "eiusmod", "tempor", "incididunt"])
                                        .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
                                    function randomData (){
                                        var labels = color.domain();
                                        return labels.map(function(label){
                                            return { label: label, value: Math.random() }
                                        });
                                    }
                                    change(randomData());
                                    d3.select(".randomize")
                                        .on("click", function(){
                                            change(randomData());
                                        });
                                    function change(data) {
                                        /* ------- PIE SLICES -------*/
                                        var slice = svg.select(".slices").selectAll("path.slice")
                                            .data(pie(data), key);
                                        slice.enter()
                                            .insert("path")
                                            .style("fill", function(d) { return color(d.data.label); })
                                            .attr("class", "slice");
                                        slice
                                            .transition().duration(1000)
                                            .attrTween("d", function(d) {
                                                this._current = this._current || d;
                                                var interpolate = d3.interpolate(this._current, d);
                                                this._current = interpolate(0);
                                                return function(t) {
                                                    return arc(interpolate(t));
                                                };
                                            })
                                        slice.exit()
                                            .remove();
                                        /* ------- TEXT LABELS -------*/
                                        var text = svg.select(".labels").selectAll("text")
                                            .data(pie(data), key);
                                        text.enter()
                                            .append("text")
                                            .attr("dy", ".35em")
                                            .text(function(d) {
                                                return d.data.label;
                                            });

                                        function midAngle(d){
                                            return d.startAngle + (d.endAngle - d.startAngle)/2;
                                        }
                                        text.transition().duration(1000)
                                            .attrTween("transform", function(d) {
                                                this._current = this._current || d;
                                                var interpolate = d3.interpolate(this._current, d);
                                                this._current = interpolate(0);
                                                return function(t) {
                                                    var d2 = interpolate(t);
                                                    var pos = outerArc.centroid(d2);
                                                    pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                                                    return "translate("+ pos +")";
                                                };
                                            })
                                            .styleTween("text-anchor", function(d){
                                                this._current = this._current || d;
                                                var interpolate = d3.interpolate(this._current, d);
                                                this._current = interpolate(0);
                                                return function(t) {
                                                    var d2 = interpolate(t);
                                                    return midAngle(d2) < Math.PI ? "start":"end";
                                                };
                                            });
                                        text.exit()
                                            .remove();
                                        /* ------- SLICE TO TEXT POLYLINES -------*/
                                        var polyline = svg.select(".lines").selectAll("polyline")
                                            .data(pie(data), key);

                                        polyline.enter()
                                            .append("polyline");
                                        polyline.transition().duration(1000)
                                            .attrTween("points", function(d){
                                                this._current = this._current || d;
                                                var interpolate = d3.interpolate(this._current, d);
                                                this._current = interpolate(0);
                                                return function(t) {
                                                    var d2 = interpolate(t);
                                                    var pos = outerArc.centroid(d2);
                                                    pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                                                    return [arc.centroid(d2), outerArc.centroid(d2), pos];
                                                };
                                            });

                                        polyline.exit()
                                            .remove();
                                    };


                                }
                            }
                            ]
                        },
                        {
                            id:"formView",
                            template:"Place for the form control"
                        },
                        {
                            id:"aboutView",
                            width: 960,
                            height: 600,
                            type:"space", rows:[
                            { view:"d3-chart",
                                resize:true,
                                data:{},
                                ready:function(){
                                    // set the dimensions and margins of the graph
                                    var margin = {top: 20, right: 20, bottom: 30, left: 50},
                                        width = 960 - margin.left - margin.right,
                                        height = 500 - margin.top - margin.bottom;

// parse the date / time
                                    var parseTime = d3.timeParse("%d-%b-%y");

// set the ranges
                                    var x = d3.scaleTime().range([0, width]);
                                    var y = d3.scaleLinear().range([height, 0]);

// define the line
                                    var valueline = d3.line()
                                        .x(function(d) { return x(d.date); })
                                        .y(function(d) { return y(d.cr); });

// append the svg obgect to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
                                    var svg = d3.select(this.$view).append("svg")
                                        .attr("width", width + margin.left + margin.right)
                                        .attr("height", height + margin.top + margin.bottom)
                                        .append("g")
                                        .attr("transform",
                                        "translate(" + margin.left + "," + margin.top + ")");

// gridlines in x axis function
                                    function make_x_gridlines() {
                                        return d3.axisBottom(x)
                                            .ticks(5)
                                    }

// gridlines in y axis function
                                    function make_y_gridlines() {
                                        return d3.axisLeft(y)
                                            .ticks(5)
                                    }


                                   d3.json("/api/reports/experiments/getCR", function(error, data) {
                                        if (error) throw error;

                                        // format the data
                                        data.forEach(function(d) {
                                            d.date = parseTime(d.date);
                                            console.log(d.date);
                                            d.cr = +d.cr;
                                        });

                                        // Scale the range of the data
                                        x.domain(d3.extent(data, function(d) { return d.date; }));
                                        y.domain([0, d3.max(data, function(d) { return d.cr; })]);

                                        // add the X gridlines
                                        svg.append("g")
                                            .attr("class", "grid")
                                            .attr("transform", "translate(0," + height + ")")
                                            .call(make_x_gridlines()
                                                .tickSize(-height)
                                                .tickFormat("")
                                        )

                                        // add the Y gridlines
                                        svg.append("g")
                                            .attr("class", "grid")
                                            .call(make_y_gridlines()
                                                .tickSize(-width)
                                                .tickFormat("")
                                        )

                                        // add the valueline path.
                                        svg.append("path")
                                            .data([data])
                                            .attr("class", "line")
                                            .attr("d", valueline);

                                        // add the X Axis
                                        svg.append("g")
                                            .attr("transform", "translate(0," + height + ")")
                                            .call(d3.axisBottom(x));

                                        // add the Y Axis
                                        svg.append("g")
                                            .call(d3.axisLeft(y));

                                    });
                                }}
                        ]



                        },


                    ]
                };
                webix.protoUI({
                    name:"d3-chart",
                    defaults:{
                    },
                    $init:function(){
                        this._ready_awaits = 0;

                        this.attachEvent("onAfterLoad", function(){
                            if (this._ready_awaits == 2){
                                if (this.config.ready){
                                    this.config.ready.call(this, this.data);
                                    this._ready_awaits = 3;
                                }
                            } else this._ready_awaits = 1;
                        });

                        webix.delay(webix.bind(this._render_once, this));
                    },
                    _render_once:function(){
                        webix.require("d3/d3.v3.min.js",function(first_init){

                            if (this.config.init)
                                this.config.init.call(this);
                            if (this._ready_awaits == 1 && this.config.ready){
                                this.config.ready.call(this, this.data);
                                this._ready_awaits = 3;
                            } else
                                this._ready_awaits = 2;


                        }, this);
                    },
                    $setSize:function(x,y){
                        if (webix.ui.view.prototype.$setSize.call(this,x,y)){
                            if (this._ready_awaits == 3 && this.config.resize){
                                this.$view.innerHTML = "";
                                this.config.ready.call(this, this.data);
                            }
                        }
                    }
                }, webix.AtomDataLoader, webix.EventSystem, webix.ui.view );


                webix.ui({
                    container: "webixDashboardElement",
                    type: 'wide',
                    margin: 0,
                    rows:[
                        {
                            type: 'space',
                            padding: 0,
                            height: 80,
                            width: 1163,
                            template:mainDescription,
                            data: response
                        },
                        {cols:[{

                            rows: [
                                tabbar,
                                data,

                            ]
                        },{
                            padding: 0,
                            template: timeline
                        }
                        ]},
                    ]


                });


            });
        });

    }

    function openLayoutDashboardExperiment(){
        var formElement = document.createElement('div');
        formElement.id = 'webixDashboardElement';

        document.getElementById("innerContent").appendChild(formElement);

        webix.ready(function() {
            webix.ajax().get('/api/reports/experiments/getAll', function(response, xml, xhr) {
                var getTemplate = function(){
                    return "<div class='title'><a href='/reports/experiment/detail/#experimentID#'>#name#</a></div>"
                };

                webix.ui({
                    container:"webixDashboardElement",
                    type:"space",
                    rows:[
                        {cols:[
                            { view:"button", value:"prev",
                                click:function(){ $$('pager1').select("prev"); } },
                            {},
                            { view:"button", value:"next",
                                click:function(){ $$('pager1').select("next"); } }
                        ]},
                        { view:"list", data:response, yCount:10,
                            type:{
                                width: 500,
                                height: 65,
                                template:getTemplate()
                            },
                            pager:{
                                apiOnly:true, id:"pager1", size:10, animate:{
                                    direction:"top"
                                }
                            }
                        }
                    ]
                });
            });
        });

    }
    global.openLayoutDashboardExperiment = openLayoutDashboardExperiment;
    global.openLayoutDashboardExperimentDetail = openLayoutDashboardExperimentDetail;
    global.experimentListConfig = dashboardExperimentConfig;
    global.page = experimentListConfig.page;
    global.placementWindowIframe = undefined;

})(window);


var showDetail = function(){

};