"use strict";
(function(win) {


  function renderFunnelGraph(data){
    const options = {
      chart: {
        curve: {
          enabled: true
        },
        bottomPinch: 0,
        animate: 200
      },
      block: {
        dynamicHeight: true,
        highlight: true,
        minHeight: 100,
        highlight: true
      },
    };

    const chart = new D3Funnel('#funnel');


    var no_data = true;
    for(var i = 0; i < data.length-1; i++){
      if(data[i].value != 0){
        no_data = false;
        break;
      }
    }

    if(no_data){
      $('#funnel').html('<p style="text-align:center;"><strong>No data found.</strong></p>');
    }else{
      $('#funnel').height(data.length * 100);
      $$('funnel-template').define("height", data.length * 100);
      $$('funnel-template').resize();
      chart.draw(data, options);
    }


  }

  function generateFunnelGraph(start,end){
    var params = '';
    if(dates){
      params = '?';
      // params += 'start_date=' + (new Date(dates[0]).getTime()/1000);
      // params += '&end_date=' + (new Date(dates[1]).getTime()/1000);
      params += 'start_date=' + dates[0];
      params += '&end_date=' + dates[1];
    }
    var href = location.href;
    var funnel_id = href.substr(href.lastIndexOf('/') + 1);
    funnel_id = funnel_id.replace('#', '');

    var url = monolop_api_base_url + '/api/reports/funnels/getFunnels/' + funnel_id + params;

    webix.ajax().get(url, function(response, xml, xhr) {
      response = JSON.parse(response);
      renderFunnelGraph(response.data);
    });
  }

	function openLayoutDashboardFunnelsDetail(){

		var formElement = document.createElement('div');
    formElement.id = 'webixDashboardElement';
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", "name_you_want");
    input.setAttribute("value", "value_you_want");
    document.getElementById("innerContent").appendChild(input);
    document.getElementById("innerContent").appendChild(formElement);

    var href = location.href;
    var funnel_id = href.substr(href.lastIndexOf('/') + 1);
    funnel_id = funnel_id.replace('#', '');
    var params = funnel_id.indexOf('?') ;
    if(params !== -1){
      funnel_id = funnel_id.substr(0 , params );
    }

    webix.ready(function() {
      webix.ajax().get(monolop_api_base_url + '/api/reports/funnels/getFunnels/' + funnel_id, function(response, xml, xhr) {
				response = JSON.parse(response);
				if (response.funnel && response.funnel._id) {
          $("#viewHeader div.header:first").html(response.funnel.name);
          $("#viewHeader div.header:first").attr('style', 'margin-left: 2%;font-size:24px;');

          webix.ui({
            container: "webixDashboardElement",
            id: "webixDashboardElementFunnel",
            css: 'funnel-report',
            margin: 0,
            rows:[{height:10},{
            	cols:[{},{
								height: 80,
								view: "template",
								template: '<div id="dataRange"></div>',
								borderless: true
            	},{}]
            },{
            	cols:[{},{
                id: 'funnel-template',
            		view: "template",
            		height: 600,
								template: '<div id="funnel" ></div>',
								borderless: true
            	},{}]
            }]
          });


          var to = new Date();
          var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 6);

          var calendarStart = from, calendarEnd= to;

          $('#dataRange').DateRangesWidget({
	          inline: true,
	          date: [calendarStart, calendarEnd],
	          calendars: 3,
	          mode: 'tworanges',
	          showOn: 'focus',
	          onChange: function(dates, el) {
              dates = getDates();
              var calendarStart = dates[0].replace(/,/g, "").replace(/ /g, "-");
              var calendarEnd = dates[1].replace(/,/g, "").replace(/ /g, "-");

              generateFunnelGraph(calendarStart,calendarEnd);
              /*
              $('#chartArea').empty();
              generateReportGraph(dates[0],
                dates[1],
                'data_range', response);
              */
	          }
		      });

          renderFunnelGraph(response.data);
        }
			});

		});
	};


	win.openLayoutDashboardFunnelsDetail = openLayoutDashboardFunnelsDetail;

})(window);