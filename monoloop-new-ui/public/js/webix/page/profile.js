var isSessional = false;
var my_profile_form = [
  {
    view: "text",
    label: 'Username',
    css: 'username',
    name: "username",
    id: "username",
    disabled: true
  },{
    view: "text",
    type: "password",
    css: 'password',
    label: 'New Password',
    name: "password",
    id: "password",
    invalidMessage: "Password does not match with confirm password."
  }, {
    view: "text",
    type: "password",
    css: 'confirmPassword',
    label: 'Re-type New Password',
    name: "confirmPassword",
    id: "confirmPassword",
    invalidMessage: "Confirm password does not match with password."
  },{
    view: "button",
    value: "Save profile settings",
    css: "save orangeBtn",
    width: 150,
    click: function() {
      var btnCtrl = this;
      btnCtrl.disable();
      var form = this.getParentView();
      if (form.validate()) {
        webix.ajax().post(monolop_api_base_url + "/api/users/password?ajax=true", form.getValues(), function(res, data, xhr) {
          res = JSON.parse(res);
          if (res.success === true) {
            btnCtrl.enable();
            // var ac = $$("account_type").getValue();
            // if(ac === 'agency'){
            //  activeAccount.account_type = 'agency';
            //  $("div.webix_tree_branch_2").has('div[webix_tm_id="clientAccounts"]').show();
            // } else {
            //  activeAccount.account_type = 'enterprise';
            //  $("div.webix_tree_branch_2").has('div[webix_tm_id="clientAccounts"]').hide();
            // }

            webix.message("Profile settings are updated.");
            // location.reload();
          }
        });
      }else{
        btnCtrl.enable();
        alert("Please fill all required values to proceed.");
      }
    }
  }
];

function openLayout() {
  if (userStateData) {
      isSessional = userStateData.section === 'profile';
  }

  var formElement = document.createElement('div');
  formElement.id = 'webixFormElement';
  document.getElementById("innerContent").appendChild(formElement);

  webix.ui({
    view: "form",
    scroll: false,
    id: "profile-form",
    container: "webixFormElement",
    elements: my_profile_form,
    width: ($('body').width() - $('.sidebar').width() - 10),
    margin: 3,
    rules: {
      "username": webix.rules.isNotEmpty,
       "password": function(value) {
          return value === $$("confirmPassword").getValue();
        },
        "confirmPassword": function(value) {
          return value === $$("password").getValue();
        },
    },
    elementsConfig: {
      labelPosition: "top",
      labelWidth: 140,
      bottomPadding: 18
    }

  });


  webix.ajax().get(monolop_api_base_url + "/api/profile?ajax=true", function(res) {
    res = JSON.parse(res);
    $$("username").setValue(res.user.username);
  });

  $(window).on('resize', function(){
    $$('profile-form').define('width', $('body').width() - $('.sidebar').width() - 1);
    $$('profile-form').resize();
  });
}
