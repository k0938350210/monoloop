function validateEmail(email){
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
var _form = [
			{ view:"text", label:'Email', name:"email", invalidMessage: "Incorrect e-mail address"},
			// { view:"text", label:'Username', name:"username"},
			{ view:"text",type: "password", label:'Password', name:"password", invalidMessage: "Password can not be empty."},
			{ view:"text", label:'Name', name:"name", invalidMessage: "Name can not be empty."},
      { view:"button", value: "Save and sign in", id: "createNewAccountSbbtn", align:"right", width: 200, click:function(){
          var form = this.getParentView();
          if (form.validate()){
            webix.ajax().post("/api/invitation/signin", form.getValues(), function(res, data, xhr){
              res  = JSON.parse(res);
              console.log("res", res);
              if(res.success === true){

                $$('openSignInFormlayoutPopup').close();
                document.location = res.redirect;
              } else {
                webix.message({type:"error", text:res.msg});
              }
            });

          }
        }}
		];
var _formRules = {
  // "username": webix.rules.isNotEmpty,
	"email": webix.rules.isEmail,
  "password": webix.rules.isNotEmpty,
	"name": webix.rules.isNotEmpty,
};

function openSignInFormlayout(){
  webix.ui({
    view: "window",
    id: "openSignInFormlayoutPopup",
    modal: true,
    position: "center",
    height: 450,
    width: 400,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "Sign in"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('openSignInFormlayoutPopup').close();"
      }]
    },
    body: {
      view: "form",
      id: "__signinForm",
      height: 600,
      width: 400,
      complexData: true,
      elements: _form,
      rules: _formRules,
      margin:4,
      elementsConfig:{
        labelPosition: "top",
        labelWidth: 140,
        bottomPadding: 18
      }
    }
  }).show();

  if(user !== undefined) {
    $$("__signinForm").setValues({
      name: user.account.contact_name,
      email: user.account.contact_email,
      accountId: user.account._id,
      accountUserId: user._id
    });
  }
}
