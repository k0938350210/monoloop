
var pluginData = [];
function openLayout(){
  var formElement = document.createElement('div');
  formElement.id = 'webixpluginsElement';

  document.getElementById("innerContent").appendChild(formElement);

  webix.ajax().post(monolop_api_base_url+"/api/account/plugins?ajax=true", function(res, data, xhr){
      res  = JSON.parse(res);

      // var pluginData = [];

      if(res.totalCount > 0) {
        for (var i = 0; i < res.topics.length; i++) {
          // console.log(res.topics);
          pluginData.push({
            id: i+1,
            pluginId: res.topics[i]._id,
            pluginName: res.topics[i].plugin.name,
            statusName: (res.topics[i].enabled == false) ? 'inactive': 'active',
            status: (res.topics[i].enabled == false) ? 1: 0,
            experience:(res.topics[i].experience == undefined) ? 0 : res.topics[i].experience,
            audience:(res.topics[i].audience == undefined) ? 0 : res.topics[i].audience,
            goal:(res.topics[i].goal == undefined) ? 0 : res.topics[i].goal,
          });
        }

        webix.ui({
          container: "webixpluginsElement",
          id: "pluginDatatable",
          view:"datatable", select:false, editable:false, height: 600,
          columns:[
            {id:"id", header:"#", width:50},
            {id:"pluginName", header:["Plugin name", {content:"textFilter"} ], sort:"string", minWidth:'70%', fillspace: 2, editor:"text"},
            {id:"settings", header:["Settings"],  template:'<a webix_l_id="setting" styles="padding: 10px !important;"  title="select event type" onclick="openPluginConfig();" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-cog"></span></a>'},
            {id:"statusName", header:["Status"], minWidth:'25%', minWidth:'25%', fillspace: 1, template:"<span style='color:black;' class='updateStatus status status#status#'>#statusName#</span>"},
          ],
          // pager:"pagerA",
          data: pluginData,
          onClick:{
            "updateStatus":function(e,id,node){
              console.log(id);
          //    webix.confirm({
            //    text:"Are you sure?", ok:"Yes", cancel:"Cancel",
            //    callback:function(res){
            //      if(res){
                    var item = webix.$$("pluginDatatable").getItem(id);


                    var enabled;

                    if(item.statusName === 'active'){
                      enabled = 0;
                      item.status = 1;
                      item.statusName = "inactive";
                    } else {
                      enabled = 1;
                      item.status = 0;
                      item.statusName = "active";
                    }

                    webix.ajax().post(monolop_api_base_url+"/api/account/plugins/"+ item.pluginId + '?ajax=true', {enabled: enabled}, function(res, data, xhr){
                      res = JSON.parse(res);
                      if(res.success === true){
                        webix.$$("pluginDatatable").refresh(id);
                      }
                    });


                //  }
              //  }
            //  });
            }
          },
        });

      } else {
        webix.ui({
          container: "webixpluginsElement",
          view: "label",
          label: "No plugins has been added yet."
        });
      }

      initIntialTTandGuides();
  });





}
function openPluginConfig(){
  console.log("Data ==>", pluginData);
  var _form = [{
    view: "label",
    label: 'Please select events that you want to push to this plugin',
  },{
    view: "label",
    label: "Note: Once you enabled any option you won't be able to uncheck that",
    css:{"color": "red"},
  },{
    view: "checkbox",
    label: 'Experience',
    id: "experience",
    value: pluginData[0].experience,
    disabled:pluginData[0].experience == 0 ? false: true
  },{
    view: "checkbox",
    label: 'Audience',
    id: "audience",
    value: pluginData[0].audience,
    disabled:pluginData[0].audience == 0 ? false: true
  },{
    view: "checkbox",
    label: 'Goals',
    id: "goal",
    value: pluginData[0].goal,
    disabled:pluginData[0].goal == 0 ? false: true
  },{
    view: "button",
    value: "Save",
    css: "orangeBtn saveBtn",
    id: "submitAccountUserForm",
    align: "right",
    width: 150,
    click: function() {
      var btnSave = this;
      btnSave.disable();
      webix.ajax().post(monolop_api_base_url+"/api/account/plugins/"+ pluginData[0].pluginId + '?ajax=true', {enabled:Number(!pluginData[0].status), event: true, experience:$$('experience').getValue(), audience: $$('audience').getValue(), goal: $$('goal').getValue()}, function(res, data, xhr){
        res = JSON.parse(res);
        if(res.success === true){
          pluginData[0].experience = $$('experience').getValue();
          pluginData[0].audience = $$('audience').getValue();
          pluginData[0].goal =  $$('goal').getValue();
          $$('pluginConfigPopup').close();
          webix.alert("Configurations Saved");
        }
      });
    },
  }];

  var width = $('body').width() - $('.sidebar').width();
  if(width > 500){
      width = 500;
  }
  webix.ui({
      view: "window",
      id: "pluginConfigPopup",
      modal: true,
      position: "center",
      height: 380,
      width: width,
      head: {
          view: "toolbar",
          margin: -4,
          cols: [{
              view: "label",
              label: "Plugin Configuration"
          }, {
              view: "icon",
              icon: "times-circle",
              css: "alter",
              click: "$$('pluginConfigPopup').close();"
          }]
      },
      body: {
          view: "form",
          id: "_form",
          height: 'auto',
          scroll: true,
          width: $('body').width() - $('.sidebar').width(),
          complexData: true,
          elements: _form,
          margin: 4,
          elementsConfig: {
              labelPosition: "left",
              labelWidth: 140,
              bottomPadding: 16
          }
      }
  }).show();
}
