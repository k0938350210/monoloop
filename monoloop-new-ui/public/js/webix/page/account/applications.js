var accessTypeOptions = [{
  id: "private",
  value: 'Private'
}, {
  id: "public",
  value: 'Public'
}];
var statusTypeOptions = [{
  id: "live",
  value: 'Live'
}, {
  id: "development",
  value: 'Development'
} ];
var liveStreamOptions = [{
  id: 1,
  value: 'On'
}, {
  id: 0,
  value: 'Off'
}];
var authTypeOptions = [{
  id: "soap",
  value: 'Soap'
}, {
  id: "rest",
  value: 'Rest'
}];
var triggerTypeOptions = [{
  id: "visit_start",
  value: 'Visit start'
}, {
  id: "goal",
  value: 'Goal'
}];
var profileFieldsOptions;
var extendedFab = {
  actionButtonTypes: {
    type: "create",
    setType: function(t){
      t = t || "create";
      extendedFab.actionButtonTypes.type = t;
    },
    fabOptions: false,
    create: {
      options: [
        // {
        // 	label: "This is to test",
        // 	className: "segmentBuilder inline",
        //   callback: function(){
        //     webix.message("This is to test......");
        //   }
        // }
      ]
    },
  },
  fabOptions: false,
  primary: {
    primaryBtnLabelCallback: function(actionButton, floatingTextBG){
      actionButton.className += " createApplication inline";
      floatingTextBG.textContent = "Create application";
      actionButton.onclick = function(){
          OpenApplicationPopup('new', undefined, undefined);
      };
    },
  }
};
function OpenApplicationPopup(source, id, data) {
  var form;

  var applicationLeftColumnForm = [{
      view: "text",
      label: 'Name',
      css: "name",
      name: "name"
    }, {
      view: "textarea",
      label: 'Description',
      name: "description",
      css: "description",
      id: "description",
      height: 200
    }, {
      view: "richselect",
      id: "access_type",
      css: "access_type",
      name: "access_type",
      label: "Type",
      options: accessTypeOptions
    }, {
      view: "richselect",
      id: "status_type",
      css: "status_type",
      name: "status_type",
      label: "Status",
      options: statusTypeOptions
    }, {
      view: "richselect",
      id: "livestream",
      name: "livestream",
      css: "livestream",
      label: "Live stream",
      options: liveStreamOptions
    }
  ];
  var applicationRightColumnForm = [{
      view: "richselect",
      id: "authen_type",
      css: "authen_type",
      name: "authen_type",
      label: "Authentication type",
      options: authTypeOptions
    }, {
      view: "text",
      label: 'Host URL',
      css: "host",
      name: "host"
    }, {
      view: "text",
      label: 'Access token',
      css: "api_token",
      name: "api_token"
    }, {
      view: "multiselect",
      id: "option_trigger",
      name: "option_trigger",
      css: "option_trigger",
      label: "Trigger",
      options: triggerTypeOptions
    }, {
      view: "multiselect",
      id: "fields",
      name: "fields",
      css: "fields",
      label: "Fields",
      options: profileFieldsOptions
    }
  ];


  var applicationFormRules = {
    "name": webix.rules.isNotEmpty,
    "access_type": webix.rules.isNotEmpty,
    "status_type": webix.rules.isNotEmpty,
    "livestream": webix.rules.isNotEmpty,
    "authen_type": webix.rules.isNotEmpty,
  };



  var applicationAddButton = [{
    view: "button",
    value: "Save",
    css: "orangeBtn",
    id: "submitapplicationForm",
    align: "right",
    width: 150,
    click: function() {
      var form = $$("applicationForm");
      if (form.validate()) {

        webix.ajax().post(monolop_api_base_url+"/api/applications?ajax=true", form.getValues(), function(res, data, xhr) {
          res = JSON.parse(res);
          if (res.success === true) {

            $$('applicationsDatatable').add({
              id: $$('applicationsDatatable').getLastId() + 1,
              _id: res.application._id,
              name: res.application.name,
              description: res.application.description,
              access_type: res.application.access_type,
              status_type: res.application.status_type,
              statusName: (res.application.hidden == 1) ? 'deactivate' : 'activate',
              status: res.application.hidden,
							livestream: res.application.livestream,
							api_key: res.application.api_key,
							api_token: res.application.api_token,
							host: res.application.host,
							authen_type: res.application.authen_type,
							option_trigger: res.application.option_trigger,
              fields: res.application.fields,
              deleted: res.application.deleted,
            }, $$('applicationsDatatable').getLastId() + 1);


            $$('createNewApplicationPopup').close();
            webix.message("Application added successfully.");
          } else {
            webix.message({
              type: "error",
              text: res.msg
            });
          }
        });


      }
    }
  }];
  var applicationEditButton = [{
    view: "button",
    value: "Save",
    css: "orangeBtn",
    id: "submitapplicationForm",
    align: "right",
    width: 150,
    click: function() {
      var form = $$("applicationForm");
      if (form.validate()) {
        var item = webix.$$("applicationsDatatable").getItem(id);
        webix.ajax().put(monolop_api_base_url+"/api/applications/" + item._id + '?ajax=true', form.getValues(), function(res, data, xhr) {
          res = JSON.parse(res);
          if (res.success === true) {

            var item = webix.$$("applicationsDatatable").getItem(id);

            item.name = res.application.name;
            item.description = res.application.description;
            item.access_type = res.application.access_type;
            item.status_type = res.application.status_type;
            item.statusName = (res.application.hidden == 1) ? 'deactivate' : 'activate';
            item.status = res.application.hidden;
						item.livestream = res.application.livestream;
						item.api_key = res.application.api_key;
						item.api_token = res.application.api_token;
						item.host = res.application.host;
						item.authen_type = res.application.authen_type;
						item.option_trigger = res.application.option_trigger;
            item.fields = res.application.fields;
            item.deleted = res.application.deleted;



            $$("applicationsDatatable").refresh();

            $$('createNewApplicationPopup').close();
            webix.message("Application updated successfully.");
          } else {
            webix.message({
              type: "error",
              text: res.msg
            });
          }
        });


      }
    }
  }];

  if (id === undefined) {
    applicationRightColumnForm = applicationRightColumnForm.concat(applicationAddButton);
  } else {
    applicationRightColumnForm = applicationRightColumnForm.concat(applicationEditButton);
  }

  webix.ui({
    view: "window",
    id: "createNewApplicationPopup",
    modal: true,
    position: "center",
    height: 700,
    width: 800,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "New Application"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('createNewApplicationPopup').close();"
      }]
    },
    body: {
      view: "form",
      id: "applicationForm",
      height: 700,
      width: 800,
      complexData: true,
      elements: [{
        cols: [{
          rows: applicationLeftColumnForm,
          width: 370,
          paddingX: 20
        }, {
          rows: applicationRightColumnForm,
          width: 370,
          paddingX: 20
        },

      ]
      }, ],
      rules: applicationFormRules,
      margin: 3,
      elementsConfig: {
        labelPosition: "top",
        labelWidth: 140,
        bottomPadding: 18
      }
    }
  }).show();

  if (id !== undefined) {
    $$("applicationForm").setValues({
			name: data.name,
			description: data.description,
			access_type: data.access_type,
			status_type: data.status_type,
			livestream: data.livestream,
			api_key: data.api_key,
			api_token: data.api_token,
			host: data.host,
			authen_type: data.authen_type,
			option_trigger: data.option_trigger.toString(),
      fields: data.fields.toString()
    }, true);
  }
  guidelines.startSubGuideline('newApplicationPopup');
}

function openLayout() {
  console.log("openLayout");
  var formElement = document.createElement('div');
  formElement.id = 'webixDomainsElement';

  document.getElementById("innerContent").appendChild(formElement);

  // Layout for domains
  webix.ui({
    container: "webixDomainsElement",
    rows: [{
      template: "<div id='actionMenu'></div>",
      height: 50
    }, {
      template: "<div id='webixDomainsGrid'></div>",
      height: 600
    }, ]
  });
  // template for domain action menue
  webix.ui({
    container: "actionMenu",
    cols: [{
      view: "button",
      id: "createNewDomain",
      label: "Create new application",
      css: "orangeBtn",
      width: 200,
      click: function() {
        // open add new domain form
        OpenApplicationPopup('new', undefined, undefined);
      }
    }, ]
  });

  webix.ajax().get(monolop_api_base_url+"/api/applications?ajax=true", function(res, data, xhr) {
    res = JSON.parse(res);
    // console.log(res);
    var gridData = [];
    profileFieldsOptions = res.profileFields;
    for (var i = 0; i < res.topics.length; i++) {
      gridData.push({
        id: i + 1,
        _id: res.topics[i]._id,
        name: res.topics[i].name,
        description: res.topics[i].description,
        access_type: res.topics[i].access_type,
        status_type: res.topics[i].status_type,
        statusName: (res.topics[i].hidden == 1) ? 'deactivate' : 'activate',
        status: res.topics[i].hidden,
				livestream: res.topics[i].livestream,
				api_key: res.topics[i].api_key,
				api_token: res.topics[i].api_token,
				host: res.topics[i].host,
				authen_type: res.topics[i].authen_type,
				option_trigger: res.topics[i].option_trigger,
        fields: res.topics[i].fields,
        deleted: res.topics[i].deleted,
      });
    }

    webix.ui({
      container: "webixDomainsGrid",
      id: "applicationsDatatable",
      view: "datatable",
      select: false,
      editable: false,
      height: 600,
      columns: [{
          id: "id",
          header: "#",
          width: 50
        }, {
          id: "name",
          header: ["Name", {
            content: "textFilter"
          }],
          sort: "string",
          minWidth: '20%',
          fillspace: 2,
          editor: "text"
        }, {
          id: "description",
          header: ["Description", {
            content: "textFilter"
          }],
          sort: "string",
          minWidth: '20%',
          fillspace: 2,
          editor: "text"
        }, {
          id: "access_type",
          header: ["Type", {
            content: "textFilter"
          }],
          sort: "string",
          minWidth: '20%',
          fillspace: 2,
          editor: "text"
        }, {
          id: "status_type",
          header: ["Status", {
            content: "textFilter"
          }],
          sort: "string",
          minWidth: '20%',
          fillspace: 2,
          editor: "text"
        }, {
          id: "edit",
          header: "&nbsp;",
          width: 35,
          template: "<span  style=' cursor:pointer;' class='webix_icon fa-pencil editApplication'></span>"
        }, {
          id: "delete",
          header: "&nbsp;",
          width: 35,
          template: "<span  style='cursor:pointer;' class='webix_icon fa-trash-o deleteApplication'></span>"
        }
      ],
      // pager:"pagerA",
      data: gridData,
      onClick: {
        "deleteApplication": function(e, id, node) {
          webix.confirm({
            text: "Are you sure that you <br />want to delete?",
            ok: "Yes",
            cancel: "Cancel",
            callback: function(res) {
              if (res) {
                var item = webix.$$("applicationsDatatable").getItem(id);
                webix.ajax().del(monolop_api_base_url+"/api/applications/" + item._id + '?ajax=true', {
                  id: item._id
                }, function(res, data, xhr) {
                  res = JSON.parse(res);
                  if (res.success === true) {
                    webix.$$("applicationsDatatable").remove(id);
                    webix.message("Selected application is deleted!");
                  }
                });
              }
            }
          });
        },
        "editApplication": function(e, id, node) {
          var item = webix.$$("applicationsDatatable").getItem(id);
          OpenApplicationPopup('edit', id, item);

        }
      },
      on:{
          onBeforeLoad:function(){
          },
          onAfterLoad:function(){
            initIntialTTandGuides();
          }
      },
    });

  });
  fab.initActionButtons({
    type: "applicationList",
    actionButtonTypes: extendedFab.actionButtonTypes,
    primary: extendedFab.primary,
  });
}
