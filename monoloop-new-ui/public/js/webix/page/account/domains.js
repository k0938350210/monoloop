
webix.protoUI({
	name:"ckeditor",
	$init:function(config){
		this.$view.className += " webix_selectable";
	},
	defaults:{
		borderless:true,
		language:"en",
		toolbar: [
			[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
			[ 'FontSize', 'TextColor', 'BGColor' ]
		]
	},
	_init_ckeditor_once:function(){
		var tid = this.config.textAreaID = "t"+webix.uid();
		this.$view.innerHTML = "<textarea id='"+tid+"'>"+this.config.value+"</textarea>";

		window.CKEDITOR_BASEPATH =  webix.codebase+"ckeditor/";
		webix.require("ckeditor/ckeditor.js", function(){
			this._3rd_editor = CKEDITOR.replace( this.config.textAreaID, {
				toolbar: this.config.toolbar,
				language: this.config.language,
				width:this.$width -2,
				height:this.$height - 44
			});
		}, this);
	},
	_set_inner_size:function(x, y){
		if (!this._3rd_editor || !this._3rd_editor.container || !this.$width) return;
		this._3rd_editor.resize(x, y);
	},
	$setSize:function(x,y){
		if (webix.ui.view.prototype.$setSize.call(this, x, y)){
			this._init_ckeditor_once();
			this._set_inner_size(x,y);
		}
	},
	setValue:function(value){
		this.config.value = value;
		if (this._3rd_editor)
			this._3rd_editor.setData(value);
		else webix.delay(function(){
			this.setValue(value);
		},this,[],100);
	},
	getValue:function(){
		return this._3rd_editor?this._3rd_editor.getData():this.config.value;
	},
	focus:function(){
		this._focus_await = true;
		if (this._3rd_editor)
			this._3rd_editor.focus();
	},
	getEditor:function(){
		return this._3rd_editor.getData();
	}
}, webix.ui.view);


var domainActions = ['Automaticly add notificationtab to page', 'Manually add link to privacy center'];
var customFormDataTemp = {};
var resetCustomPrivacySettings = function(){
	customFormDataTemp = {};
};
var enableCheckbox = [
	{ view:"checkbox", label:'Enable', name:"privacy_usecustom", css: "cus_privacy_usecustom111"},
];
var message;
var customInformation = [
	{ view:"ckeditor", label:'Information', value: "", name:"i1", id: "i1", height:200},
];

var customPrefs = [
	{ view:"text", label:'Header', name:"p1"},
	{ view:"text", label:'Tracking onsite behaviour', name:"p2"},
	{ view:"text", label:'Onsite personalization', name:"p3"},
	{ view:"text", label:'Remarketing', name:"p4"},
	{ view:"text", label:'Ok', name:"p5"},
];
var customDeletePrefs = [
	{ view:"text", label:'Delete your monoloop profile', name:"d1"},
	{ view:"textarea", label:'Confirm delete profile', name:"d2", id:"d2", height:100},
	{ view:"text", label:'Delete your monoloop cookie', name:"d3"},
	{ view:"textarea", label:'Comfirm delete cookie', name:"d4", id:"d4", height:100},
];
var defaulCustomPrivacytValues = {
	privacy_usecustom: 1,
	i1: '<b>We honor and protect your privacy</b> <br /> We are committed to respecting the trust you place in us and we accept responsibility for protecting your privacy. Consequently, we seek full transparency in what information we track and collect when you visit this website, why we do so and how we use it to provide you with relevant and meaningful content that is matching your current interests and preferences. <br />This website use technology to personalize the content based on your behavior when you visit. The objective is to give you the best possible experience when you visit the site. The technology works for both anonymous, registered and logged in visitors. If you have enabled Private Browsing, disabled cookies or javascript – the technology will not work and you will be served static content. The personalization is based on your behavior on the site and what your page views suggest that you are most interested in. <br />Like most other websites we use cookies to be able to identify and remember visitors who browse our website (a “cookie” is a small data file that can be placed on your hard drive when you visit certain websites). All browsers (Internet Explorer, Google Chrome, Firefox, Opera) have privacy options and you are free to decline cookies in your browser settings, but it should be pointed out that declining cookies could disable features or make some parts of the website not work as intended.<br /> A cookie helps us to identify and track what you are searching for as you browse the pages of our site, while still keeping your personal information anonymous – the cookie only contains a number and a key to your tracking data on our servers.',
	p1: "Please select how we track and handle your data.",
	p2: "Tracking onsite behavior",
	p3: "Onsite personalization",
	p4: "Remarketing",
	p5: "OK",
	d1: "Delete your monoloop profile",
	d2: "Delete your monoloop cookie",
	d3: "You are about to delete your profile - by doing so this site will not be able to personalize its content to you. Are you sure you want to delete the profile?",
	d4: "You are about to delete your Monoloop cookie - by doing so this site will not be able to recognize you. NOTE: We will NOT be able to respect any Privacy Preferences you may have indicated under Preferences. If you return to this site again - you will be recorded as a new visitor and we will initiate default tracking.",
};

var customButtons = [
	{ view:"button", value: "Ok", css: "okCustomSaveBtn", align:"center", width: 150, click:function(){
		var form1 = $$("createCustomPrivacyForm");
		if (form1.validate()){
				var domainFormValues = $$("domainsForm").getValues();
				customFormValues = customFormDataTemp = form1.getValues();
				$$("domainsForm").setValues({
					domain: domainFormValues.domain,
					url_privacy: domainFormValues.url_privacy,
					privacy_option: domainFormValues.privacy_option,
					id: domainFormValues.id,
					privacy_usecustom: customFormValues.privacy_usecustom,
					i1: customFormValues.i1,
					p1: customFormValues.p1,
					p2: customFormValues.p2,
					p3: customFormValues.p3,
					p4: customFormValues.p4,
					p5: customFormValues.p5,
					d1: customFormValues.d1,
					d2: customFormValues.d2,
					d3: customFormValues.d3,
					d4: customFormValues.d4,
				});
				$$('createCustomPrivacyPopup').close();
		}

	}},{
		view: "button", value: "Default", css: "cus_default", width: 200, click: function(){
			var enabled = $$("privacy_usecustom").getValue();
			if(enabled == 1){
				$$("createCustomPrivacyForm").setValues(defaulCustomPrivacytValues);
			} else {
				webix.message({type: "error", text: "Please enable privacy settings to fill in default values."});
			}

		}
	},
];
var domainsForm = [
			{ view:"text", label:'Domain', name:"domain", css: "def_domain"},
			{ view:"text", label:'Privacy Policy URL', name:"url_privacy", css: "def_url_privacy"},
      { view:"select", label:"Action", name: "privacy_option", id: 'privacy_option', css: "def_privacy_option", options:[
					{id:'empty', value: "Select" },
					{id:'auto', value: domainActions[0] }, // the initially selected value
          {id:'manual', value: domainActions[1] }
        ], labelAlign:'left'
      },



		];

var validateDomain = function(the_domain){
    // strip off "http://" and/or "www."
    the_domain = the_domain.replace("http://","");
		the_domain = the_domain.replace("https://","");
    the_domain = the_domain.replace("www.","");

    var reg = /^[\w\-]+\.\w[\w\-\.]*((\/\w+)*|\/)$/igm;
    return reg.test(the_domain);
} // end validateDomain()
var validateURL = function(this_url){
	this_url = this_url.trim();

	if(this_url.length === 0){
		if($$("privacy_option").getValue() === 'empty'){
			return true;
		}
	}
	this_url = this_url.replace("www.", "");

	if(this_url.indexOf('http') === -1){
		this_url = 'http://' + this_url;
	}

	var message;
	var myRegExp =/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
	var urlToValidate = this_url;
	if (!myRegExp.test(urlToValidate)){
		return false;
	} else {
		return true;
	}
};
var domainsFormRules = {
  "domain": function(value){
		value = value.trim();
		if(!value){
			$$("domainsForm").elements.domain.define("invalidMessage", "Domain name can not be empty."); return false;
		}
		var r = validateDomain(value);
		if(!r){
			$$("domainsForm").elements.domain.define("invalidMessage", "Domain name is invalid."); return false;
		}
		return true;
	},
	"url_privacy": function(value){
		value = value.trim();
		// if(!value){
		// 	$$("domainsForm").elements.url_privacy.define("invalidMessage", "Privacy url can not be empty."); return false;
		// }
		var r = validateURL(value);
		if(!r){
			$$("domainsForm").elements.url_privacy.define("invalidMessage", 'Privacy url is invalid.'); return false;
		}
		return true;
	},
  "privacy_option": webix.rules.isNotEmpty,
};

var extendedFab = {
  actionButtonTypes: {
    type: "create",
    setType: function(t){
      t = t || "create";
      extendedFab.actionButtonTypes.type = t;
    },
    fabOptions: false,
    create: {
      options: [{
        	label: "Test domains",
        	className: "testDomain inline",
          callback: function(){
            testDomains();
          }
        }
      ]
    },
  },
  fabOptions: false,
  primary: {
    primaryBtnLabelCallback: function(actionButton, floatingTextBG){
      actionButton.className += " createDomain inline";
      floatingTextBG.textContent = "Create domain";
      actionButton.onclick = function(){
          openDomainPopup('new', undefined, undefined, true);
      };
    },
  }
};


function openCustomPrivacyPopup(source, data){

	webix.ui({
    view: "window",
    id: "createCustomPrivacyPopup",
    modal: true,
    position: "center",
    // height: 480,
		autoheight:true,
    width: ($('body').width() - $('.sidebar').width()) - 100,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "Customize privacy center"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('createCustomPrivacyPopup').close();resetCustomPrivacySettings();"
      }]
    },
    body: {
      view: "form",
			// scroll: true,
      id: "createCustomPrivacyForm",
      // height: 'auto',
			autoheight:true,
      width:$('body').width() - $('.sidebar').width() ,
      complexData: true,
      elements: [
					// rows: [
						{
							height: 480,
							css: "domainCustomForm",
							scroll: true,
							rows: [
								{ view:"checkbox", label:'Enable', id:"privacy_usecustom", name:"privacy_usecustom", css: "cus_privacy_usecustom"},
								{ view:"fieldset", label:"Information", body:{
									rows: customInformation
								}},
								{ view:"fieldset", label:"Preferences", body:{
									rows: customPrefs
								}},
								{ view:"fieldset", label:"Delete", body:{
									rows: customDeletePrefs
								}},
							]
						},{
							margin:15, cols:customButtons
						}
					// ]
				],
      rules: [],
      margin:3,
      elementsConfig:{
        labelPosition: "top",
        labelWidth: 140,
        bottomPadding: 18
      }
    }
  }).show();


	if(customFormDataTemp.i1 !== undefined){
		$$("createCustomPrivacyForm").setValues({
			privacy_usecustom: customFormDataTemp.privacy_usecustom,
			i1: customFormDataTemp.i1,
			p1: customFormDataTemp.p1,
			p2: customFormDataTemp.p2,
			p3: customFormDataTemp.p3,
			p4: customFormDataTemp.p4,
			p5: customFormDataTemp.p5,
			d1: customFormDataTemp.d1,
			d2: customFormDataTemp.d2,
			d3: customFormDataTemp.d3,
			d4: customFormDataTemp.d4,
		});
		if(customFormDataTemp.privacy_usecustom == 0){
			disablePrivacyOptions();
		} else {
			enablePrivacyOptions();
		}
		setTimeout(function(){$$("i1").setValue(customFormDataTemp.i1);  }, 1000);
	}

	if(source === 'edit'){

		if(data.privacy_usecustom == true){
			data.privacy_usecustom = 1
			enablePrivacyOptions();
		} else {
			data.privacy_usecustom = 0;
			disablePrivacyOptions();
		}
		$$("createCustomPrivacyForm").setValues({
			privacy_usecustom: data.privacy_usecustom,
			i1: data.i1,
			p1: data.p1,
			p2: data.p2,
			p3: data.p3,
			p4: data.p4,
			p5: data.p5,
			d1: data.d1,
			d2: data.d2,
			d3: data.d3,
			d4: data.d4,
		});

		setTimeout(function(){$$("i1").setValue(data.i1);  }, 1000);

	} else {
		if(customFormDataTemp.i1 === undefined){
			$$("createCustomPrivacyForm").setValues({
				privacy_usecustom: 0,
				i1: "",
				p1: "",
				p2: "",
				p3: "",
				p4: "",
				p5: "",
				d1: "",
				d2: "",
				d3: "",
				d4: "",
			});
			disablePrivacyOptions();
		}

	}



	$$("privacy_usecustom").attachEvent("onChange",function(v){
    if(v === 1){
			enablePrivacyOptions();
		} else {
			disablePrivacyOptions();
		}
	});
	if (!guidelines.startedBefore) {
		guidelines.startSubGuideline('customizePopup');
	}

}
function enablePrivacyOptions(){
	$$("i1").enable();
	$$("p1").enable();
	$$("p2").enable();
	$$("p3").enable();
	$$("p4").enable();
	$$("p5").enable();
	$$("d1").enable();
	$$("d2").enable();
	$$("d3").enable();
	$$("d4").enable();
}
function disablePrivacyOptions(){
	$$("i1").disable();
	$$("p1").disable();
	$$("p2").disable();
	$$("p3").disable();
	$$("p4").disable();
	$$("p5").disable();
	$$("d1").disable();
	$$("d2").disable();
	$$("d3").disable();
	$$("d4").disable();
}
function openDomainPopup(source, id, data, updateDataTable){
  var form;

  var domainsAddButton = [
		{ view:"button", value: "Customize", id: "custommizeDomain", align:"left", width: 150, click:function(){
			openCustomPrivacyPopup('new', undefined);
		}},
    { view:"button", value: "Save", css: "orangeBtn saveBtn", id: "submitDomainsForm", align:"right", width: 150, click:function(){
			  var btnCaller = this;
				btnCaller.disable();
        var form = this.getParentView();
        if (form.validate()){

          webix.ajax().post(monolop_api_base_url+"/api/domain?ajax=true", form.getValues(), function(res, data, xhr){
            res  = JSON.parse(res);
            if(res.success === true){
							btnCaller.enable();
							if(updateDataTable == true){
                $$('domainsDatatable').add({
                  id: $$('domainsDatatable').getLastId() + 1,
                  domainId: res.domain._id,
                  domainName: res.domain.domain,
                  privacyUrl: res.domain.url_privacy,
                  privacyProcedure: ((res.domain.privacy_option === 'auto') ? domainActions[0] : ((res.domain.privacy_option === 'manual') ? domainActions[1] : '')),
                  privacyProcedureOrg: res.domain.privacy_option,
                  scriptStatus: (res.domain.status == 1) ? 'Present/Account incorrect': ((res.domain.status == 2) ? 'Present/Account correct' : 'Not present'),
                  status: res.domain.status,
                                privacy_usecustom: res.domain.privacy_usecustom,
                                i1: res.domain.i1,
                                p1: res.domain.p1,
                                p2: res.domain.p2,
                                p3: res.domain.p3,
                                p4: res.domain.p4,
                                p5: res.domain.p5,
                                d1: res.domain.d1,
                                d2: res.domain.d2,
                                d3: res.domain.d3,
                                d4: res.domain.d4,
                }, $$('domainsDatatable').getLastId() + 1);
              }else{
                message =  webix.message("Testing. Please wait...");
                webix.ajax().post(monolop_api_base_url+"/api/domains/test-domain/" + res.domain.domain + '?ajax=true',{
                            // Error callback
                            error:function(text, data, XmlHttpRequest){
                                    console.log("error");
                            },
                            //Success callback
                            success:function(text, data, XmlHttpRequest){
                                    var data = JSON.parse(text);
                                    if(data.success === true){
                                            webix.message.hide(message);
                                            webix.message({type: "Default", text: res.domain.domain + ": Tested"});
                                        }
                            }
                    });
              }


              $$('createNewDomainPopup').close();
              webix.message("Domain added successfully.");
            } else {
              webix.message({type:"error", text:res.msg});
            }
          });
        }else{
					btnCaller.enable();
					alert("Please fill all values in form to proceed.");
				}
      }}
  ];
  var domainsEditButton = [
		{ view:"button", value: "Customize", css: "custommizeDomain", id: "custommizeDomain", align:"left", width: 150, click:function(){
			var item = webix.$$("domainsDatatable").getItem(id);
			openCustomPrivacyPopup('edit', item);
		}},
    { view:"button", value: "Save", css: "orangeBtn saveBtn", id: "submitDomainsForm", align:"right", width: 150, click:function(){
        var form = this.getParentView();
        if (form.validate()){
          var item = webix.$$("domainsDatatable").getItem(id);
          webix.ajax().put(monolop_api_base_url+"/api/domain/" + item.domainName + '?ajax=true', form.getValues(), function(res, data, xhr){
            res  = JSON.parse(res);
            if(res.success === true){

              var item = webix.$$("domainsDatatable").getItem(id);

                item.domainName = res.domain.domain;
                item.privacyUrl = res.domain.url_privacy;
                item.privacyProcedure = ((res.domain.privacy_option === 'auto') ? domainActions[0] : ((res.domain.privacy_option === 'manual') ? domainActions[1] : ''));
                item.privacyProcedureOrg = res.domain.privacy_option;
                item.scriptStatus =  (res.domain.status == 1) ? 'Present/Account incorrect': ((res.domain.status == 2) ? 'Present/Account correct' : 'Not present');
                item.status = res.domain.status ;
								item.privacy_usecustom = res.domain.privacy_usecustom;
								item.i1 = res.domain.i1;
								item.p1 = res.domain.p1;
								item.p2 = res.domain.p2;
								item.p3 = res.domain.p3;
								item.p4 = res.domain.p4;
								item.p5 = res.domain.p5;
								item.d1 = res.domain.d1;
								item.d2 = res.domain.d2;
								item.d3 = res.domain.d3;
								item.d4 = res.domain.d4;

                $$("domainsDatatable").refresh();

							resetCustomPrivacySettings();

              $$('createNewDomainPopup').close();
              webix.message("Domain updated successfully.");
            } else {
              webix.message({type:"error", text:res.msg});
            }
          });


        }
      }}
  ];

  if(id === undefined) {
    form = domainsForm.concat(domainsAddButton);
  } else {
    // when editing the existing domain
    form = domainsForm.concat(domainsEditButton);
  }

  webix.ui({
    view: "window",
    id: "createNewDomainPopup",
    modal: true,
    position: "center",
    height: 400,
    width: 400,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "New Domain"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('createNewDomainPopup').close();resetCustomPrivacySettings();"
      }]
    },
    body: {
      view: "form",
      id: "domainsForm",
      height: 400,
      width: 400,
      complexData: true,
      elements: form,
      rules: domainsFormRules,
      margin:3,
      elementsConfig:{
        labelPosition: "top",
        labelWidth: 140,
        bottomPadding: 18
      }
    }
  }).show();

	var accType = (activeAccount.AccType !== undefined) ? activeAccount.AccType : 'Free';
  if(id !== undefined){
    $$("domainsForm").setValues({
      domain: data.domainName,
      url_privacy: data.privacyUrl,
      privacy_option: data.privacyProcedureOrg,
      privacy_option: data.privacyProcedureOrg,
      id: data.domainId
    }, true);
		if (!guidelines.startedBefore) {
			guidelines.startSubGuideline('editDomainPopup');
		}
  }else{
		if (!guidelines.startedBefore) {
			guidelines.startSubGuideline('newDomainPopup');
		}

		$$("domainsForm").setValues({
      domain: '',
      url_privacy: '',
      privacy_option: (accType === 'Free' ? 'auto' : 'manual')
    }, true);
	}

	if(accType === 'Free')
			$$('privacy_option').disable();
}
var testDomains = function(){
	// test domains here
	message =  webix.message("Testing. Please wait...");
	var testDomain = function(item){
		webix.ajax().post(monolop_api_base_url+"/api/domains/test-domain/" + item.domainName + '?ajax=true',{
				// Error callback
				error:function(text, data, XmlHttpRequest){
						console.log("error");
				},

				//Success callback
				success:function(text, data, XmlHttpRequest){
						var data = JSON.parse(text);
						if(data.success === true){
								webix.message.hide(message);
								webix.message({type: "Default", text: item.domainName + ": Tested"});
								item.scriptStatus = (data.domain.status == 1) ? 'Present/Account incorrect': ((data.domain.status == 2) ? 'Present/Account correct' : 'Not present');
								item.status = data.domain.status;
								$$("domainsDatatable").refresh();
							}
				}
		});
	}

	var curItem;
	$$("domainsDatatable").eachRow(
			function (row){
				curItem =  $$("domainsDatatable").getItem(row);
					testDomain(curItem);
			}
	);
}
function openLayout(){
  var formElement = document.createElement('div');
  formElement.id = 'webixDomainsElement';

  document.getElementById("innerContent").appendChild(formElement);

 // Layout for domains
  webix.ui({
        container: "webixDomainsElement",
				id: "webixDomainsElement",
        rows:[
                {template:"<div id='actionMenu'></div>", height: 50},
				        {template:"<div id='webixDomainsGrid'></div>", height: 600},
            ]
  });
  // template for domain action menue
  webix.ui({
        container: "actionMenu",
        cols:[
              {view:"button", id: "createNewDomain", label:"Create domain", css: "createNewDomain orangeBtn", width: 150, click:function(){
                // open add new domain form
                  openDomainPopup('new', undefined, undefined, true);
              }},
              {view:"button", id: "testDomains", label:"Test domains", css: "testDomains orangeBtn", width: 150, click:function(){
								testDomains();
              }},
            ]
  });

  webix.ajax().get(monolop_api_base_url+"/api/domain?ajax=true", function(res, data, xhr){
      res  = JSON.parse(res);
      var gridData = [];

      // if(res.totalCount > 0) {
        for (var i = 0; i < res.topics.length; i++) {
          gridData.push({
            id: i+1,
            domainId: res.topics[i]._id,
            domainName: res.topics[i].domain,
            privacyUrl: res.topics[i].url_privacy,
            privacyProcedure: ((res.topics[i].privacy_option === 'auto') ? domainActions[0] : ((res.topics[i].privacy_option === 'manual') ? domainActions[1] : '')),
            privacyProcedureOrg: res.topics[i].privacy_option,
            scriptStatus: (res.topics[i].status == 1) ? 'Present/Account incorrect': ((res.topics[i].status == 2) ? 'Present/Account correct' : 'Not present'),
            status: res.topics[i].status,
						privacy_usecustom: res.topics[i].privacy_usecustom,
						i1: res.topics[i].i1,
						p1: res.topics[i].p1,
						p2: res.topics[i].p2,
						p3: res.topics[i].p3,
						p4: res.topics[i].p4,
						p5: res.topics[i].p5,
						d1: res.topics[i].d1,
						d2: res.topics[i].d2,
						d3: res.topics[i].d3,
						d4: res.topics[i].d4,
          });
        }

        webix.ui({
          container: "webixDomainsGrid",
          id: "domainsDatatable",
          view:"datatable", select:false, editable:false, height: 600,
          columns:[
            {id:"id", header:"#", width:50},
            {id:"domainName", header:["Domain name", {content:"textFilter"} ], sort:"string", minWidth:'20%', fillspace: 2, editor:"text"},
            {id:"privacyUrl", header:["Privacy Url", {content:"textFilter"} ], sort:"string", minWidth:'20%', fillspace: 2, editor:"text"},
            {id:"privacyProcedure", header:["Privacy Procedure", {content:"textFilter"} ], sort:"string", minWidth:'20%', fillspace: 2, editor:"text"},
            {id:"scriptStatus", header:["Script Status"], minWidth:'20%', fillspace: 1, template:"<span style='color:black;' class='status status#status#'>#scriptStatus#</span>"},

            {id:"edit", header:"&nbsp;", width:35, template:"<span  style=' cursor:pointer;' class='webix_icon fa-pencil editDomain'></span>"},
		        {id:"delete", header:"&nbsp;", width:35, template:"<span  style='cursor:pointer;' class='webix_icon fa-trash-o deleteDomain'></span>"}

          ],
          // pager:"pagerA",
          data: gridData,
          onClick:{
            "deleteDomain":function(e,id,node){
              webix.confirm({
                text:"Are you sure that you <br />want to delete?", ok:"Yes", cancel:"Cancel",
                callback:function(res){
                  if(res){
                    var item = webix.$$("domainsDatatable").getItem(id);


                    webix.ajax().del(monolop_api_base_url+"/api/domain/" + item.domainId + '?ajax=true', {id: item.domainId}, function(res, data, xhr){
                      res = JSON.parse(res);
                      if(res.success === true){
                        webix.$$("domainsDatatable").remove(id);
                        webix.message("Selected domain is deleted!");
                      }
                    });


                  }
                }
              });
            },
            "editDomain": function(e,id,node){
              var item = webix.$$("domainsDatatable").getItem(id);
              openDomainPopup('edit', id, item, true);

            }
          },
					on:{
							onBeforeLoad:function(){
							},
							onAfterLoad:function(){
								initIntialTTandGuides();
							}
					},
        });

  });

	$(window).on('resize', function(){
		$('webixDomainsElement').css('width', ($('body').width() - $('.sidebar').width()) + 'px');

		$$('domainsDatatable').define('width', $('body').width() - $('.sidebar').width());
		$$('domainsDatatable').resize();
	});

	fab.initActionButtons({
		type: "domainList",
		actionButtonTypes: extendedFab.actionButtonTypes,
		primary: extendedFab.primary,
	});
	// guidelines.generate(guidelines.page.currentGuide);
}
