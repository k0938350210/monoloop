var isSessional = false;
var profileForm = [{
        view: 'text',
        label: 'Legal entity',
        name: "legal_entity",
        id: 'legal_entity'
    },{
        view: "text",
        label: 'E-mail address',
        css: 'email',
        name: "email",
        id: "email",
        invalidMessage: "Incorrect e-mail address"
    }, {
        view: "text",
        label: 'Full name',
        css: 'name',
        name: "name",
        id: "name",
        invalidMessage: "Full name can not be empty."
    },
    /*
    { view:"radio", name:"account_type", id:"account_type",
    	value: 'enterprise',
    	css: "account_type",
    	options:[
    	{ value:"Enterprise", id: "enterprise" },
    	{ value:"Agency", id: "agency" }
    ] , align:"center"},
    */
    {
        view: "select",
        options: "/data/timezone.json",
        label: "Time zone",
        name: "timezone",
        id: "timezone",
        value: 'UTC'
    }, {
        view: "text",
        label: 'Date format',
        name: "date_format",
        id: "date_format",
    }, {
      view: "text",
      label: 'Time format',
      name: "time_format",
      id: "time_format"
    }, {
      view: "checkbox",
      name: "omni_channel_ready",
      id: "omni_channel_ready",
      label: 'Enable/Disable Omni Channel Ready',
      value: 0,
      on:{
      'onItemClick': function(id){
        if($$("omni_channel_ready").getValue() == 1)
          {
            webix.confirm({
            title:"Enable/Diaable Omni Channel Ready",
            ok:"Yes",
            cancel:"No",
            text:"This operation is irreversible !",
            callback:function(res){
                  switch(res){
                  case true:
                   $$("omni_channel_ready").disable();
                   break;
                  case false:
                   $$("omni_channel_ready").setValue(0);
                   $$("omni_channel_ready").enable();
                   break;
                  }
                 }
            });
          }
        }
      }
    },{
      view: "checkbox",
      name: "cross_domain_tracking",
      id: "cross_domain_tracking",
      label: 'Enable/Disable Cross Domain Tracking'
    },
    {
        view: 'label',
        label: '<a href="https://www.monoloop.com/data-processing-agreement/" target="_blank">Data Processing Agreement</a>'
    },
    {
        view: 'label',
        label: '<a href="https://www.monoloop.com/acceptable-use-policy/" target="_blank">Acceptable Use Policy</a>'
    },{
        view: 'label',
        label: '<a href="https://www.monoloop.com/terms-of-service/" target="_blank">Terms of Service</a>'
    },
    {
        view: "button",
        value: "Sign",
        css: "save orangeBtn",
        id: "sign-btn",
        width: 150,
        click: function(){
          if(!$$('legal_entity').getValue()){
            webix.message("legal entity required");
            return;
          }
          var btnCtrl = this;
          btnCtrl.disable();
          var form = this.getParentView();
          webix.ajax().post(monolop_api_base_url + "/api/profile/comply-gdpr", form.getValues(), function(res, data, xhr) {
            webix.message("Account gdpr is updated.");
          });
        }
    },
    {
        view: "button",
        value: "Save profile settings",
        css: "save orangeBtn",
        width: 150,
        click: function() {
						var btnCtrl = this;
						btnCtrl.disable();
            var form = this.getParentView();
            if (form.validate()) {
                webix.ajax().post(monolop_api_base_url + "/api/profile?ajax=true", form.getValues(), function(res, data, xhr) {
                    res = JSON.parse(res);
                    if (res.success === true) {
										    btnCtrl.enable();
												// var ac = $$("account_type").getValue();
												// if(ac === 'agency'){
												// 	activeAccount.account_type = 'agency';
												// 	$("div.webix_tree_branch_2").has('div[webix_tm_id="clientAccounts"]').show();
												// } else {
												// 	activeAccount.account_type = 'enterprise';
												// 	$("div.webix_tree_branch_2").has('div[webix_tm_id="clientAccounts"]').hide();
												// }

                        webix.message("Profile settings are updated.");
                        // location.reload();
                    }
                });
            }else{
								btnCtrl.enable();
								alert("Please fill all required values to proceed.");
						}
        }
    }
];

function openLayout() {
    if (userStateData) {
        isSessional = userStateData.section === 'account-profile';
    }

    var formElement = document.createElement('div');
    formElement.id = 'webixFormElement';

    document.getElementById("innerContent").appendChild(formElement);

    webix.ui({
        view: "form",
        scroll: false,
        id: "profileForm",
        container: "webixFormElement",
        elements: profileForm,
        width: ($('body').width() - $('.sidebar').width() - 10),
        margin: 3,
        rules: {
            "email": webix.rules.isNotEmpty,
            "name": webix.rules.isNotEmpty,
            //"account_type": webix.rules.isNotEmpty,
            "email": webix.rules.isEmail,
            "date_format": webix.rules.isNotEmpty,
            "time_format": webix.rules.isNotEmpty,
            // $obj:function(data){
            //   if (data.password != data.confirmPassword){
            //       webix.message({type: "error", text: "Passwords are not the same"});
            //       return false;
            //   }
            // return true;
            // }
        },
        elementsConfig: {
            labelPosition: "top",
            labelWidth: 140,
            bottomPadding: 18
        }

    });
    webix.ajax().get(monolop_api_base_url + "/api/profile?ajax=true", function(res) {
        res = JSON.parse(res);

        var account_type = 'enterprise';
        if (res) {
            if (res.account) {
                account_type = res.account.account_type;
            }
        }
        if (!account_type) {
            account_type = 'enterprise';
        }
        if(res.account.comply_gdpr){
          $$('sign-btn').disable();
        }

        $$("email").setValue(res.account.contact_email);
        $$("name").setValue(res.account.contact_name);
        $$('legal_entity').setValue(res.account.legal_entity);
        $$("omni_channel_ready").setValue(res.account.omni_channel_ready);
        $$("cross_domain_tracking").setValue(res.account.cross_domain_tracking);
        if(res.account.omni_channel_ready == 1)
        {
           $$("omni_channel_ready").disable();
        }
        var timezone = res.account.timezone;
        if (timezone === undefined) {
            timezone = 'UTC'
        }
        $$('timezone').setValue(timezone);

        var date_format = res.account.date_format;
        var time_format = res.account.time_format;

        if (date_format === undefined) {
            date_format = 'D MMMM, YYYY';
        }
        if (time_format === undefined) {
            time_format = 'HH:mm:ss';
        }

        $$('date_format').setValue(date_format);
        $$('time_format').setValue(time_format);

        //$$("account_type").setValue(account_type);

        if (isSessional && userStateData) {
            if (userStateData.data.basicData) {
                $$("profileForm").setValues(userStateData.data.basicData);
            }
        }
        isSessional = false;
        userStateData = '';

        initIntialTTandGuides();
    });

    $(window).on('resize', function(){
      $$('profileForm').define('width', $('body').width() - $('.sidebar').width() - 1);
      $$('profileForm').resize();
    });
}
