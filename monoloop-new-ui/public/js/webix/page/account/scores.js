var isSessional = false;
var scoresForm = [
			{ view:"text", css: "clickdepth", label:'Click depth', name:"clickdepth", invalidMessage: "Click depth is not valid" },
      { view:"text", css: "duration", label:'Duration', name:"duration", invalidMessage: "Duration is not valid"},
			{ view:"text", css: "recency", label:'Recency', name:"recency", invalidMessage: "Recency is not valid"},
			{ view:"text", css: "latency", label:'Latency', name:"latency", invalidMessage: "Latency is not valid"},
			{ view:"button", value: "Save", css: "save orangeBtn", width: 70, click:function(){
				var form = this.getParentView();
				if (form.validate()){

          webix.ajax().post(monolop_api_base_url+"/api/account/scores/update?ajax=true", form.getValues(), function(res, data, xhr){
            res  = JSON.parse(res);
            if(res.success === true){
              webix.message("Changes saved successfully.");
            }
          });


				}

			}}
		];
function openLayout()
{
	if(userStateData){
		isSessional = userStateData.section === 'account-scores';
	}
  var formElement = document.createElement('div');
  formElement.id = 'webixScoresFormElement';

  document.getElementById("innerContent").appendChild(formElement);

  webix.ui({
			view:"form", scroll:false,
      id: "scoresForm",
			container: "webixScoresFormElement",
			elements: scoresForm,
			width: 350,
			margin:3,
			rules:{
        "clickdepth": webix.rules.isNumber,
        "duration": webix.rules.isNumber,
				"recency": webix.rules.isNumber,
        "latency": webix.rules.isNumber
			},
			elementsConfig:{
				labelPosition: "top",
				labelWidth: 140,
				bottomPadding: 18
			}

		});

		webix.ajax(monolop_api_base_url+"/api/account?ajax=true", function(res){

				res  = JSON.parse(res);
				if(res.scores){
					$$("scoresForm").setValues({
						clickdepth: res.scores.clickdepth,
						duration: res.scores.duration,
						recency: res.scores.recency,
						latency: res.scores.latency,
					}, true);
				}
				if(isSessional && userStateData){
					if(userStateData.data.basicData){
						$$("scoresForm").setValues(userStateData.data.basicData);
					}
				}
				isSessional = false;
				userStateData = '';

				initIntialTTandGuides();
		});

}
