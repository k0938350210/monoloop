var isSessional = false;
var invocationForm = [{
    view: "text",
    label: 'Postback delay (MS)',
    css: 'timeout',
    name: "postback_delay",
    invalidMessage: "Entered Timeout is not valid."
  },{
    cols:[  // marc added coll
      {
        width: 420,
        view: "radio",
        label: "SPA page view option",
        css: 'anti_flicker',
        name: "spa_pageview_option",
        value: 'url-history-changed',
        options: [{
            id: 'url-history-changed',
            value: "Url history changed"
          },
          {
            id:  'hash-changed',
            value: "Url hash changed"
          },
          {
            id: 'custom',
            value: "Custom"
          }
        ],
        on:{
          onChange: function(newv,oldv){
            if(newv == 'custom'){
              $$('custom-invoke-script').show();
            }else{
              $$('custom-invoke-script').hide();
            }
          }
        }
      },{
        view: "text",
        id: 'custom-invoke-script',
        hidden: true,
        label: '&nbsp;',
        value: 'MONOloop.invoke();'
      }
    ]},/*{
    view: "radio",
    label: "Injection method",
    css: 'content_delivery',
    name: "content_delivery",
    value: 1,
    options: [{
        id: 1,
        value: "Instantly"
      }, //the initially selected item
      {
        id: 0,
        value: "Fast"
      }, {
        id: 2,
        value: "Safe"
      }
    ]
},*/ {
    view: "radio",
    label: "Anti.flicker",
    css: 'anti_flicker',
    name: "anti_flicker",
    value: 1,
    options: [{
        id: 1,
        value: "On"
      }, //the initially selected item
      {
        id: 0,
        value: "Off"
      },
    ]
}, {
  view: "text",
  label: 'Anti-flicker Timeout (MS)',
  css: 'timeout',
  name: "timeout",
  invalidMessage: "Entered Timeout is not valid."
}, {
  view: "label",
  label: "Maximum milliseconds to wait for Monoloop content injection (if service fails)."
}, {
  view: "button",
  value: "Generate",
  css: "saveGenerate orangeBtn",
  width: 90,
  click: function() {
    var btnGenerate = this;
    btnGenerate.disable();
    var form = this.getParentView();
    if (form.validate()) {
      webix.ajax().post(
        monolop_api_base_url + "/api/account/invocation?ajax=true",
        form.getValues(),
        function(res, data, xhr) {
          btnGenerate.enable();
          res = JSON.parse(res);
          if (res.success === true) {
            $$("scriptLabel").setValue(res.invocation.str2);

            var labelTemp = $("div[view_id=scriptLabel] div:first-child");
            labelTemp.css('line-height', '18px').attr('id', 'scriptText');

            webix.message("Changes saved successfully.");
          }
        }
      );
    }else {
      btnGenerate.enable();
    }
  }
}];
var advancedScriptForm = [
  {cols:[  // marc added coll
    {
      view: "textarea",
      label: 'Pre Invocation Script',
      css: 'pre_invocation',
      name: "pre_invocation",
      id: "pre_invocation",
      height: 200
    }, {
      view: "textarea",
      label: 'Post Invocation Script',
      css: 'post_invocation',
      name: "post_invocation",
      id: "post_invocation",
      height: 200
    }
  ]},{
    view: "button",
    value: "Add to invocation setup",
    css: "addToInvocation orangeBtn",
    width: 200,
    click: function() {
      var btnInvocationSetup = this;
      btnInvocationSetup.disable();
      var form = this.getParentView();
      if (form.validate()) {
        webix.ajax().post(
          monolop_api_base_url + "/api/account/invocation/prepost?ajax=true",
          form.getValues(),
          function(res, data, xhr) {
            res = JSON.parse(res);
            if (res.success === true) {
              btnInvocationSetup.enable();
              webix.message("Changes saved successfully.");
            }
        });
      }
    }
  }

];

function openLayout() {


    if (!webix.env.touch && webix.ui.scrollSize)
        webix.CustomScroll.init();

    var formElement = document.createElement("div");
    formElement.id = 'webixCodeScriptAccordion';
    formElement.style.minHeight = '800px';
    formElement.style.width = '100%';

    if (userStateData) {
      isSessional = userStateData.section === 'account-code-and-scripts';
    }

    document.getElementById("innerContent").appendChild(formElement);

    // accordion
    webix.ui({
      container: "webixCodeScriptAccordion",
      view: "accordion",
      id: "webixCodeScriptAccordion",
      scroll: "y",
      multi: false,
      rows: [ //or rows
        {
          header: "Invocation Code",
          body: "<div id='invocationCodeLayout'></div>",
          height: 470
        }, {
          header: "Single Page Application",
          body: "<div id='spa-layout'></div>",
          height: 250,
          collapsed: true
        }, {
          header: "Advanced Script",
          body: {
            view: "form",
            scroll: false,
            id: "advancedScriptForm",
            elements: advancedScriptForm,
            rules: {},
            elementsConfig: {
              labelPosition: "top",
              labelWidth: 140,
              bottomPadding: 18
            }
          },
          height: 400,
          collapsed: true
        }
      ]
    });

    // invocation layout
    webix.ui({
      container: "invocationCodeLayout",
      id: "invocationCodeLayoutId",
      scroll: "y",
      height: 470,
      width: 'auto',
      rows: [{
        cols: [{id: "columnOne",
          rows: [{
            view: "button",
            css: "copy_api_token",
            value: "Copy the code below and paste it into your HTML",
            id: "copy_api_token",
            align: "center",
            height: 30,
            click: function() {
              var doc = document, text = doc.getElementById('scriptText'), range, selection;
              if (doc.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
              } else if (window.getSelection) {
                selection = window.getSelection();
                range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
              }

              try {
                var successful = document.execCommand('copy');
                webix.message("Copied.");
              } catch (err) {
                alert('Oops, unable to copy');
              }
            }
          }, {
            view: "label",
            height: 30,
            id: "noteLabel",
            label: "<strong>NOTE: For older browsers you may need to copy and paste the code manually</strong>"
          }, {
            view: "label",
            height: 300,
            id: "scriptLabel",
            text: ""
          }
        ]},{
            view: "form",
            scroll: false,
            id: "invocationForm",
            elements: invocationForm,
            width: '50%',
            margin: 3,
            rules: {
              "timeout": webix.rules.isNumber,
            },
            elementsConfig: {
              labelPosition: "top",
              labelWidth: 140,
              bottomPadding: 18
            }
        }]
      }]
    }).show();
    // advanced script layout

    // populate invocation form
    webix.ajax().get(monolop_api_base_url + "/api/account?ajax=true", function(res) {

      res = JSON.parse(res);

      if(!res.invocation.spa_pageview_option){
        res.invocation.spa_pageview_option = 'url-history-changed';
      }

      // populate invocation form
      $$("invocationForm").setValues({
          /*content_delivery: res.invocation.content_delivery,*/
          anti_flicker: res.invocation.anti_flicker,
          timeout: res.invocation.timeout,
          postback_delay: res.invocation.postback_delay,
          spa_pageview_option: res.invocation.spa_pageview_option
      }, true);

      if (isSessional && userStateData) {
          if (userStateData.data.invocation) {
              $$("invocationForm").setValues(userStateData.data.invocation);
          }
      }

      // var invocationStr = document.createTextNode(res.invocation.str);

      $$("scriptLabel").setValue(res.invocation.str2);

      var labelTemp = $("div[view_id=scriptLabel] div:first-child");
      var labelTemp2 = $("div[view_id=noteLabel] div:first-child");
      labelTemp.css('line-height', '18px').attr('id', 'scriptText');
      labelTemp2.css('line-height', '18px');

      // populate advacned script form
      $$("advancedScriptForm").setValues({
          pre_invocation: res.invocation.pre,
          post_invocation: res.invocation.post
      }, true);

      if (isSessional && userStateData) {
        if (userStateData.data.advancedScript) {
          $$("advancedScriptForm").setValues(userStateData.data.advancedScript);
        }
      }
      isSessional = false;
      userStateData = '';


      initIntialTTandGuides();
    });

    // enabling copy to clip board on button

    // $("div[view_id=scriptLabel] div").attr('id', 'api_token_text_id');
    /*
    var copyBtn = $("div[view_id=copy_api_token] button");
    copyBtn.attr('id', 'api_token_text_btn_id');
    copyBtn.attr('data-clipboard-target', 'api_token_text_id');
    copyBtn.attr('data-clipboard-text', 'Default text');
    var client = new ZeroClipboard( document.getElementById("api_token_text_btn_id") );
    */
    //
    // new Clipboard('.copy_api_token button', {
    //     target: function(trigger) {
    //         return $('#api_token_text_id')[0];
    //     }
    // });

    /*
    $(window).on('resize', function(){
      $$('invocationCodeLayoutId').define('width', $('body').width() - $('.sidebar').width() - 1);
      $$('invocationCodeLayoutId').resize();

      $$('advancedScriptForm').define('width', $('body').width() - $('.sidebar').width() - 1);
      $$('advancedScriptForm').resize();
    });

    webix.ready(function(){
      var width = ($('body').width() - $('.sidebar').width() - 1) / 2;
      $$('columnOne').define('width', width);
      $$('columnOne').resize();
      $$('invocationForm').define('width', width);
      $$('invocationForm').resize();
      $('div[view_id="columnOne"]').removeClass('webix_layout_line');

      $$('pre_invocation').define('width', width); // marc
      $$('pre_invocation').resize();               // marc
      $$('post_invocation').define('width', width);// marc
      $$('post_invocation').resize();              // marc
    });
    */
}
