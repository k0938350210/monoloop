var isSessional = false;
var invocationForm = [ /*{
    view: "radio",
    label: "Injection method",
    css: 'content_delivery',
    name: "content_delivery",
    value: 1,
    options: [{
        id: 1,
        value: "Instantly"
      }, //the initially selected item
      {
        id: 0,
        value: "Fast"
      }, {
        id: 2,
        value: "Safe"
      }
    ]
},*/ {
    view: "radio",
    label: "Anti.flicker",
    css: 'anti_flicker',
    name: "anti_flicker",
    value: 1,
    options: [{
        id: 1,
        value: "On"
      }, //the initially selected item
      {
        id: 0,
        value: "Off"
      },
    ]
}, {
  view: "text",
  label: 'Anti-flicker Timeout (MS)',
  css: 'timeout',
  name: "timeout",
  invalidMessage: "Entered Timeout is not valid."
}, {
  view: "label",
  label: "Maximum milliseconds to wait for Monoloop content injection (if service fails)."
}, {
  cols:[{
    view: "button",
    width: 300,
    value: "Generate",
    css: "saveGenerate orangeBtn",
    click: function() {
      var btnGenerate = this;
      btnGenerate.disable();
      var form =  $$('invocationForm');
      if (form.validate()) {
        webix.ajax().post(
          monolop_api_base_url + "/api/account/invocation?ajax=true",
          form.getValues(),
          {
            success:function(res, data, xhr) {
              btnGenerate.enable();
              res = JSON.parse(res);
              if (res.success === true) {
                $$("scriptLabel").setValue(res.invocation.str2);

                var labelTemp = $("div[view_id=scriptLabel] div:first-child");
                labelTemp.css('line-height', '18px').attr('id', 'scriptText');

                webix.message("Changes saved successfully.");
              }
            },
            error:function(text, data, XmlHttpRequest){
             webix.message("Error saved fail.");
             btnGenerate.enable();
            }
          }
        );
      }else {
        btnGenerate.enable();
      }
    }
  },{}]
}];

var spa_form = [{
  view: "text",
  label: 'Postback delay (MS)',
  css: 'timeout',
  name: "postback_delay",
  invalidMessage: "Entered postback delay is not valid."
},{
  cols:[  // marc added coll
  {
    view: "radio",
    label: "SPA page view option",
    css: 'anti_flicker',
    name: "spa_pageview_option",
    value: 'url-history-changed',
    options: [{
        id: 'url-history-changed',
        value: "Url history changed"
      },
      {
        id:  'hash-changed',
        value: "Url hash changed"
      },
      {
        id: 'custom',
        value: "Custom <div style='display:inline;' class='webix_el_text'><input type='text' value='MONOloop.invoke();'></div>"
      }
    ],
    on:{
      onChange: function(newv,oldv){
        if(newv == 'custom'){
          //$$('custom-invoke-script').show();
        }else{
          //$$('custom-invoke-script').hide();
        }
      }
    }
  }]
}, {
  cols:[{
    view: "button",
    width: 300,
    value: "Update",
    css: "saveGenerate orangeBtn",
    click: function() {
      var btnInvocationSetup = this;
      btnInvocationSetup.disable();
      var form = $$('spa-form');
      if (form.validate()) {
        webix.ajax().post(
          monolop_api_base_url + "/api/account/invocation/spa?ajax=true",
          form.getValues(),
          function(res, data, xhr) {
            res = JSON.parse(res);
            if (res.success === true) {
              btnInvocationSetup.enable();
              webix.message("Changes saved successfully.");
            }
        });
      }else {
        btnInvocationSetup.enable();
      }
    }
  },{}]
}];

var advancedScriptForm = [
  {
    cols:[  // marc added coll
    {
      view: "textarea",
      label: 'Pre Invocation Script',
      css: 'pre_invocation',
      name: "pre_invocation",
      id: "pre_invocation",
      height: 200
    }, {
      view: "textarea",
      label: 'Post Invocation Script',
      css: 'post_invocation',
      name: "post_invocation",
      id: "post_invocation",
      height: 200
    }
  ]},{
    cols:[{
      view: "button",
      value: "Add to invocation setup",
      width: 200,
      css: "addToInvocation orangeBtn",
      click: function() {
        var btnInvocationSetup = this;
        btnInvocationSetup.disable();
        var form = $$('advancedScriptForm');
        if (form.validate()) {
          webix.ajax().post(
            monolop_api_base_url + "/api/account/invocation/prepost?ajax=true",
            form.getValues(),
            function(res, data, xhr) {
              res = JSON.parse(res);
              if (res.success === true) {
                btnInvocationSetup.enable();
                webix.message("Changes saved successfully.");
              }
          });
        }
      }
    },{}]

  }

];


function openLayout() {
  if (!webix.env.touch && webix.ui.scrollSize)
    webix.CustomScroll.init();

  var formElement = document.createElement("div");
    formElement.id = 'webixCodeScriptAccordion';
    formElement.style.minHeight = '800px';
    formElement.style.width = '100%';

  if (userStateData) {
    isSessional = userStateData.section === 'account-code-and-scripts';
  }

  document.getElementById("innerContent").appendChild(formElement);

  webix.ui({
    container: "webixCodeScriptAccordion",
    view: "accordion",
    id: "webixCodeScriptAccordion",
    rows: [ //or rows
      {
        header: "Invocation Code",
        body: {
          cols: [{
            body: {
              rows: [{
                view: "button",
                css: "copy_api_token",
                value: "Copy the code below and paste it into your HTML",
                id: "copy_api_token",
                align: "center",
                height: 30,
                click: function() {
                  var doc = document, text = doc.getElementById('scriptText'), range, selection;
                  if (doc.body.createTextRange) {
                    range = document.body.createTextRange();
                    range.moveToElementText(text);
                    range.select();
                  } else if (window.getSelection) {
                    selection = window.getSelection();
                    range = document.createRange();
                    range.selectNodeContents(text);
                    selection.removeAllRanges();
                    selection.addRange(range);
                  }

                  try {
                    var successful = document.execCommand('copy');
                    webix.message("Copied.");
                  } catch (err) {
                    alert('Oops, unable to copy');
                  }
                }
              },{
                view: "label",
                height: 30,
                id: "noteLabel",
                label: "<strong>NOTE: For older browsers you may need to copy and paste the code manually</strong>"
              },{
                view: "label",
                height: 300,
                id: "scriptLabel",
                text: ""
              }]
            }
          },{
            view: "form",
            scroll: false,
            id: "invocationForm",
            elements: invocationForm,
            margin: 3,
            rules: {
              "timeout": webix.rules.isNumber,
            },
            elementsConfig: {
              labelPosition: "top",
              labelWidth: 140,
              bottomPadding: 18
            }
          }]
        },
        height: 400
      }, {
        header: "Single Page Application",
        height: 300,
        collapsed: true,
        body:{
          view: "form",
          scroll: false,
          id: "spa-form",
          elements: spa_form,
          margin: 3,
          rules: {
            "postback_delay": webix.rules.isNumber,
          },
          elementsConfig: {
            labelPosition: "top",
            labelWidth: 140,
            bottomPadding: 18
          }
        }
      }, {
          header: "Advanced Script",
          body: {
            view: "form",
            scroll: false,
            id: "advancedScriptForm",
            rows:advancedScriptForm,
            rules: {},
            elementsConfig: {
              labelPosition: "top",
              labelWidth: 140,
              bottomPadding: 18
            }
          },
          height: 400,
          collapsed: true
        }
    ]
  });

  webix.ajax().get(monolop_api_base_url + "/api/account?ajax=true", function(res) {

    res = JSON.parse(res);

    if(!res.invocation.spa_pageview_option){
      res.invocation.spa_pageview_option = 'url-history-changed';
    }

    // populate invocation form
    $$("invocationForm").setValues({
      /*content_delivery: res.invocation.content_delivery,*/
      anti_flicker: res.invocation.anti_flicker,
      timeout: res.invocation.timeout
    }, true);

    if (isSessional && userStateData) {
      if (userStateData.data.invocation) {
        $$("invocationForm").setValues(userStateData.data.invocation);
      }
    }

    $$("spa-form").setValues({
        postback_delay: res.invocation.postback_delay,
        spa_pageview_option: res.invocation.spa_pageview_option
    }, true);

    // var invocationStr = document.createTextNode(res.invocation.str);

    $$("scriptLabel").setValue(res.invocation.str2);

    var labelTemp = $("div[view_id=scriptLabel] div:first-child");
    var labelTemp2 = $("div[view_id=noteLabel] div:first-child");
    labelTemp.css('line-height', '18px').attr('id', 'scriptText');
    labelTemp2.css('line-height', '18px');

    // populate advacned script form
    $$("advancedScriptForm").setValues({
        pre_invocation: res.invocation.pre,
        post_invocation: res.invocation.post
    }, true);

    if (isSessional && userStateData) {
      if (userStateData.data.advancedScript) {
        $$("advancedScriptForm").setValues(userStateData.data.advancedScript);
      }
    }
    isSessional = false;
    userStateData = '';

    initIntialTTandGuides();
  });


  $(window).on('resize', function(){
    $$('webixCodeScriptAccordion').resize();
  });

  webix.ready(function(){
    $$('webixCodeScriptAccordion').resize();
  });
}