var supportTypeOptions = [{
  id: 'permanant',
  value: 'Permanent'
}, {
  id: 'fix-until',
  value: 'Expiry date'
}];

var user_roles = [{
  id: 'admin',
  value: 'Admin'
},{
  id: 'basic',
  value: 'Basic'
}];

var extendedFab = {
  actionButtonTypes: {
    type: "create",
    setType: function(t) {
      t = t || "create";
      extendedFab.actionButtonTypes.type = t;
    },
    fabOptions: false,
    create: {
      options: [
        // {
        // 	label: "This is to test",
        // 	className: "segmentBuilder inline",
        //   callback: function(){
        //     webix.message("This is to test......");
        //   }
        // }
      ]
    },
  },
  fabOptions: false,
  primary: {
    primaryBtnLabelCallback: function(actionButton, floatingTextBG) {
      actionButton.className += " createAccount inline";
      floatingTextBG.textContent = "Create User";
      actionButton.onclick = function() {
        openAccountUserPopup('new', undefined, undefined);
      };
    },
  }
};

function openAccountUserPopup(source, id, data) {


    webix.ajax().get(monolop_api_base_url + "/api/user-right-categories?ajax=true", function(rightsCategories, resData, XmlHttpRequest) {

        rightsCategories = JSON.parse(rightsCategories);

        var rightsData = [{
          id: "root",
          value: "All Rights",
          open: true,
          data: []
        }];

        // var rightsData = [{id:"root", value:"Films data", open:true, data:[]}];

        if (rightsCategories.totalCount > 0) {
          for (var i = 0; i < rightsCategories.topics.length; i++) {
            rightsData[0].data.push({
              id: 'category_' + i,
              open: true,
              value: rightsCategories.topics[i].title,
              data: []
            });
            if (rightsCategories.topics[i].rights !== undefined && rightsCategories.topics[i].rights.length > 0) {

              for (var j = 0; j < rightsCategories.topics[i].rights.length; j++) {
                rightsData[0].data[i].data.push({
                  id: rightsCategories.topics[i].rights[j]._id,
                  open: true,
                  value: rightsCategories.topics[i].rights[j].title
                });
              }
            }
          }

        }

        // rightsCategories.topics


        var form;

        var _form = [{
          view: "text",
          label: 'First name',
          css: "ip_firstname",
          name: "firstname",
          invalidMessage: "First name can not be empty."
        }, {
          view: "text",
          label: 'Last name',
          name: "lastname",
          css: "ip_lastname",
          invalidMessage: "Last name can not be empty."
        }, {
          view: "text",
          label: 'Email',
          id: "email",
          name: "email",
          css: "ip_email",
          invalidMessage: "Incorrect e-mail address"
        }, {
          view: "text",
          label: 'Confirm email',
          id: "confirmEmail",
          name: "confirmEmail",
          id: "confirmEmail",
          css: "ip_confirmEmail",
          invalidMessage: "Incorrect e-mail address"
        },{
          view: "richselect",
          label: "Role",
          id: "role",
          name: "role",
          css: "ip_support_type",
          options: user_roles
        }, {
            cols: [{
              view: "checkbox",
              label: 'Disabled',
              name: "hidden",
              css: "ip_hidden"
            }]
        },{
          view: "fieldset",
          label: "User expiry",
          id: "monoloopSupoort",
          css: "ip_monoloopSupoort",
          body: {
            rows: [{
              view: "richselect",
              label: "Type",
              id: "support_type",
              name: "support_type",
              css: "ip_support_type",
              options: supportTypeOptions,
              labelAlign: 'left'
            }, {
              view: "datepicker",
              id: "support_enddate",
              name: "support_enddate",
              css: "ip_support_enddate",
              stringResult: true,
              format: webix.i18n.dateFormatStr,
              label: 'Date',
              hidden: true
            }, ]
          }
        }/*, {
            view: "tree",
            css: "ip_rulesData",
            template: "{common.icon()} {common.checkbox()} {common.folder()} #value#",
            threeState: true,
            id: "rulesData",
            data: rightsData,
            ready: function() {
                this.openAll();
                // this.checkItem("1.2");
            }
        }*/];

        var _formRules = {
            "firstname": webix.rules.isNotEmpty,
            "lastname": webix.rules.isNotEmpty,
            "email": function(value) {
                value = value.trim();
                if (value === '') {
                    $$("_form").elements.email.define("invalidMessage", "Email can not be empty.");
                    return false;
                }
                if (!validateEmail(value)) {
                    $$("_form").elements.email.define("invalidMessage", "Incorrect e-mail address.");
                    return false;
                }
                if (value !== $$("confirmEmail").getValue()) {
                    $$("_form").elements.email.define("invalidMessage", "Email does not match with confirm email.");
                    return false
                }
                return true;
            },
            "confirmEmail": function(value) {
                value = value.trim();
                if (value === '') {
                    $$("_form").elements.confirmEmail.define("invalidMessage", "Confirm email can not be empty.");
                    return false;
                }
                if (!validateEmail(value)) {
                    $$("_form").elements.confirmEmail.define("invalidMessage", "Incorrect confirm e-mail address.");
                    return false;
                }
                if (value !== $$("email").getValue()) {
                    $$("_form").elements.confirmEmail.define("invalidMessage", "Confirm email does not match with email.");
                    return false
                }
                return true;
            },
        };

        var addButton = [{
            view: "button",
            value: "Save",
            css: "orangeBtn saveBtn",
            id: "submitAccountUserForm",
            align: "right",
            width: 150,
            click: function() {
                var btnConfirm = this;
                btnConfirm.disable();

                var form = this.getParentView();

                // var checkedRulesData = $$("rulesData").getChecked();
                form.setValues({
                    name: form.getValues().name,
                    email: form.getValues().email,
                    dashboard_config: form.getValues().dashboard_config,
                    role: form.getValues().role,
                    support_enddate: form.getValues().support_enddate,
                    support_type: form.getValues().support_type,
                    company: form.getValues().company,
                    hidden: form.getValues().hidden,
                    deleted: form.getValues().deleted
                    // rulesData: checkedRulesData,
                }, true);

                if (form.validate()) {
                    webix.ajax().post(
                      monolop_api_base_url + "/api/account-user?ajax=true",
                      form.getValues(),
                      function(res, data, xhr) {
                        res = JSON.parse(res);
                        if (res.success === true) {
                          btnConfirm.enable();
                          if (res.email === true) {
                            webix.ajax().post(monolop_api_base_url + "/api/account-user/invite/" + res.accountUser._id + '?ajax=true', {}, function(ires, idata, ixhr) {
                              ires = JSON.parse(ires);
                            });
                          }

                          $$('AccountUsersDatatable').add({
                            id: $$('AccountUsersDatatable').getLastId() + 1,
                            _id: res.accountUser._id,
                            name: res.accountUser.name,
                            email: res.accountUser.email,
                            dashboard_config: res.accountUser.dashboard_config,
                            role: res.accountUser.role,
                            is_monoloop_support: res.accountUser.is_monoloop_support,
                            support_enddate: res.accountUser.support_enddate,
                            support_type: res.accountUser.support_type,
                            can_resend_invitation: res.accountUser.can_resend_invitation,
                            company: res.accountUser.company,
                            rights: res.accountUser.rights,
                            statusName: (res.accountUser.hidden == 1) ? 'inactive' : 'active',
                            status: res.accountUser.hidden,
                            deleted: res.accountUser.deleted,
                            hidden: res.accountUser.hidden
                          }, $$('AccountUsersDatatable').getLastId() + 1);

                          $$('AccountUsersDatatable').refresh();

                          $$('createNewAccountUserPopup').close();
                          webix.message("User added successfully <br /> and invitation has been sent.");
                        } else {
                          btnConfirm.enable();
                          webix.message({
                            type: "error",
                            text: res.msg
                          });
                        }
                    });
                }else{
                  btnConfirm.enable();
                  error('Please fill the required fields to proceed.');;
                }
            }
        }];
        var editButton = [{
            view: "button",
            value: "Save",
            css: "orangeBtn saveBtn",
            id: "submitAccountUserForm",
            align: "right",
            width: 150,
            click: function() {
                var btnEditConfirm = this;
                btnEditConfirm.disable();

                var form = this.getParentView();

                // var checkedRulesData = $$("rulesData").getChecked();
                form.setValues({
                    _id: form.getValues()._id,
                    name: form.getValues().name,
                    email: form.getValues().email,
                    dashboard_config: form.getValues().dashboard_config,
                    role: form.getValues().role,
                    support_enddate: form.getValues().support_enddate,
                    support_type: form.getValues().support_type,
                    company: form.getValues().company,
                    hidden: form.getValues().hidden,
                    deleted: form.getValues().deleted
                    // rulesData: checkedRulesData,
                }, true);

                if (form.validate()) {
                    var item = webix.$$("AccountUsersDatatable").getItem(id);
                    webix.ajax().put(monolop_api_base_url + "/api/account-user/" + item._id + '?ajax=true', form.getValues(), function(res, data, xhr) {
                        res = JSON.parse(res);
                        if (res.success === true) {
                            btnEditConfirm.enable();
                            if (res.email === true) {
                                webix.ajax().post(monolop_api_base_url + "/api/account-user/invite/" + res.account._id + '?ajax=true', {}, function(ires, idata, ixhr) {
                                    ires = JSON.parse(ires);
                                });
                            }

                            var item = webix.$$("AccountUsersDatatable").getItem(id);

                            item._id = res.accountUser._id;
                            item.name = res.accountUser.name;
                            item.email = res.accountUser.email;
                            item.dashboard_config = res.accountUser.dashboard_config;
                            item.role = res.accountUser.role;
                            item.is_monoloop_support = res.accountUser.is_monoloop_support;
                            item.support_enddate = res.accountUser.support_enddate;
                            item.support_type = res.accountUser.support_type;
                            item.can_resend_invitation = res.accountUser.can_resend_invitation;
                            item.company = res.accountUser.company;
                            item.rights = res.accountUser.rights;
                            item.statusName = (res.accountUser.hidden == 1) ? 'inactive' : 'active';
                            item.status = res.accountUser.hidden;
                            item.deleted = res.accountUser.deleted;
                            item.hidden = res.accountUser.hidden;

                            $$("AccountUsersDatatable").hideOverlay();


                            $$('createNewAccountUserPopup').close();
                            $$("AccountUsersDatatable").refresh();
                            webix.message("User updated successfully.");
                        } else {
                            btnEditConfirm.enable();
                            webix.message({
                                type: "error",
                                text: res.msg
                            });
                        }
                    });
                }else{
                  btnEditConfirm.enable();
                  alert("Please fill all required fields to proceed.");
                }
            }
        }];
        if (id === undefined) {
            form = _form.concat(addButton);
        } else {
            // when editing the existing domain
            form = _form.concat(editButton);
        }

        var width = $('body').width() - $('.sidebar').width();
        if(width > 500){
            width = 500;
        }

        webix.ui({
            view: "window",
            id: "createNewAccountUserPopup",
            modal: true,
            position: "center",
            height: 550,
            width: width,
            head: {
                view: "toolbar",
                margin: -4,
                cols: [{
                    view: "label",
                    label: (id === undefined) ? "New User" : "Edit user"
                }, {
                    view: "icon",
                    icon: "times-circle",
                    css: "alter",
                    click: "$$('createNewAccountUserPopup').close();"
                }]
            },
            body: {
                view: "form",
                id: "_form",
                height: 'auto',
                scroll: true,
                width: $('body').width() - $('.sidebar').width(),
                complexData: true,
                elements: form,
                rules: _formRules,
                margin: 4,
                elementsConfig: {
                    labelPosition: "left",
                    labelWidth: 140,
                    bottomPadding: 16
                }
            }
        }).show();

        $$("support_type").attachEvent("onChange", function(newv, oldv) {
          if (newv == 'fix-until') {
            $$("support_enddate").show();
            $$('createNewAccountUserPopup').define("height", 600);
            $$('createNewAccountUserPopup').resize();

          } else {
            $$("support_enddate").hide();
            $$('createNewAccountUserPopup').define("height", 550);
            $$('createNewAccountUserPopup').resize();
          }
        });
        /*
        $$("is_monoloop_admin").attachEvent("onChange", function(newv, oldv) {
            newv = parseInt(newv);
            if (newv == 1) {
                $$("rulesData").hide();
            } else {
                $$("rulesData").show();
            }
        });
        */

        if (id !== undefined) {

            var names = data.name.split(' ');
            if (names[1] === undefined) {
                names[1] = '';
            }

            $$("_form").setValues({
                _id: data._id,
                firstname: names[0],
                lastname: names[1],
                email: data.email,
                confirmEmail: data.email,
                dashboard_config: data.dashboard_config,
                role: data.role,
                support_enddate: data.support_enddate ? new Date(data.support_enddate) : '',
                support_type: data.support_type,
                company: data.company,
                hidden: data.hidden,
                deleted: data.deleted,
            }, true);
        }else{
          $$("_form").setValues({
            role: 'admin'
          });
        }

        /*
        if(data && data.rights !== undefined){
          if(data.rights.length !== undefined && data.rights.length > 0){
            for (var i = 0; i < data.rights.length; i++) {
              $$("rulesData").checkItem(data.rights[i].toString());
            }
          }
        }
        */
    });
    if (id === undefined) {
        guidelines.startSubGuideline('newUserPopup');
    } else {
        guidelines.startSubGuideline('editUserPopup');
    }

}

function openLayout() {
    var formElement = document.createElement('div');
    formElement.id = 'webixclientAccountElement';

    document.getElementById("innerContent").appendChild(formElement);

    // Layout for domains
    webix.ui({
        container: "webixclientAccountElement",
        id: 'parentLayout',
        rows: [{
            template: "<div id='webixClientAccountGrid'></div>",
            height: 600
        }, ]
    });


    webix.ajax().get(monolop_api_base_url + "/api/account-user?ajax=true", function(res, data, xhr) {
        res = JSON.parse(res);
        //console.log(res);
        var gridData = [];
        var username = null;
        if (res.totalCount > 0) {
            for (var i = 0; i < res.topics.length; i++) {
                if (res.topics[i].user !== undefined) {
                    username = res.topics[i].user.username;
                } else {
                    username = '';
                }

                gridData.push({
                    id: i + 1,
                    _id: res.topics[i]._id,
                    name: res.topics[i].name,
                    username: username,
                    email: res.topics[i].email,
                    dashboard_config: res.topics[i].dashboard_config,
                    role: res.topics[i].role,
                    is_monoloop_support: res.topics[i].is_monoloop_support,
                    support_enddate: res.topics[i].support_enddate,
                    support_type: res.topics[i].support_type,
                    can_resend_invitation: res.topics[i].can_resend_invitation,
                    company: res.topics[i].company,
                    rights: res.topics[i].rights,
                    statusName: (res.topics[i].hidden == 1) ? 'inactive' : 'active',
                    status: res.topics[i].hidden,
                    deleted: res.topics[i].deleted,
                    hidden: res.topics[i].hidden
                });
            }
        }
        webix.ui({
            container: "webixClientAccountGrid",
            id: "AccountUsersDatatable",
            view: "datatable",
            select: false,
            editable: false,
            height: 600,
            on: {
                onAfterLoad: function() {
                    // if (!this.count())
                    //   this.showOverlay("No client account has been added yet.");
                }
            },
            columns: [{
                    id: "id",
                    header: "#",
                    // width: 50
                }, {
                    id: "name",
                    header: ["Full Name"],
                    sort: "string",
                    // minWidth: '20%',
                    fillspace: 2,
                    editor: "text"
                }, {
                    id: "email",
                    header: ["Email"],
                    sort: "string",
                    // minWidth: '20%',
                    fillspace: 2,
                    editor: "text"
                },{
                  id: 'role',
                  header: ['Role'],
                  sort: 'string',
                  fillspace: 2
                },{
                  id: 'expiry',
                  header: 'Expiry',
                  sort: 'date',
                  fillspace: 2,
                  template: function(obj){
                    if(obj.support_type != 'fix-until'){
                      return '-';
                    }
                    var format = webix.Date.dateToStr("%M %j, %Y");
                    return format(new Date(obj.support_enddate));
                  }
                },{
                    id: "statusName",
                    header: ["Status"],
                    // minWidth: '20%',
                    fillspace: 1,
                    template: "<span style='color:black;' class='updateStatus status status#status#'>#statusName#</span>"
                },

                {
                    id: "edit",
                    header: "&nbsp;",
                    // width: 35,
                    template: "<span  style=' cursor:pointer;' class='webix_icon fa-pencil editUser'></span>"
                }, {
                    id: "delete",
                    header: "&nbsp;",
                    // width: 35,
                    template: "<span  style='cursor:pointer;' class='webix_icon fa-trash-o deleteUser'></span>"
                }

            ],
            // pager:"pagerA",
            data: gridData,
            onClick: {
                "updateStatus": function(e, id, node) {
                    //  webix.confirm({
                    //    text: "Are you sure?",
                    //    ok: "Yes",
                    //    cancel: "Cancel",
                    //    callback: function(res) {
                    //      if (res) {
                    var item = webix.$$("AccountUsersDatatable").getItem(id);


                    var enabled;

                    if (item.statusName === 'active') {
                        enabled = 1;
                        item.status = 1;
                        item.statusName = "inactive";
                    } else {
                        enabled = 0;
                        item.status = 0;
                        item.statusName = "active";
                    }
                    item.hidden = enabled;
                    webix.ajax().post(monolop_api_base_url + "/api/account-user/status-update/" + item._id + '?ajax=true', {
                        hidden: enabled
                    }, function(res, data, xhr) {
                        res = JSON.parse(res);
                        if (res.success === true) {
                            webix.$$("AccountUsersDatatable").refresh(id);
                        }
                    });


                    //      }
                    //    }
                    //  });
                },
                "deleteUser": function(e, id, node) {
                    webix.confirm({
                        text: "Are you sure that you <br />want to delete?",
                        ok: "Yes",
                        cancel: "Cancel",
                        callback: function(res) {
                            if (res) {
                                var item = webix.$$("AccountUsersDatatable").getItem(id);


                                webix.ajax().del(monolop_api_base_url + "/api/account-user/" + item._id + '?ajax=true', {
                                    id: item._id
                                }, function(res, data, xhr) {
                                    res = JSON.parse(res);
                                    if (res.success === true) {
                                        webix.$$("AccountUsersDatatable").remove(id);
                                        webix.message("Selected account is deleted!");
                                    }
                                });


                            }
                        }
                    });
                },
                "editUser": function(e, id, node) {
                    var item = webix.$$("AccountUsersDatatable").getItem(id);
                    openAccountUserPopup('edit', id, item);

                }
            },
            on: {
                onBeforeLoad: function() {},
                onAfterLoad: function() {
                    initIntialTTandGuides();
                }
            },
        });

    });


    fab.initActionButtons({
        type: "accountList",
        actionButtonTypes: extendedFab.actionButtonTypes,
        primary: extendedFab.primary,
    });
    // guidelines.generate(guidelines.page.currentGuide);

    $(window).on('resize', function(){
      $('webixclientAccountElement').css('width', ($('body').width() - $('.sidebar').width()) + 'px');

      $$('parentLayout').define('width', $('body').width() - $('.sidebar').width());
      $$('parentLayout').resize();

      $$('AccountUsersDatatable').define('width', $('body').width() - $('.sidebar').width());
      $$('AccountUsersDatatable').resize();
    });
}
