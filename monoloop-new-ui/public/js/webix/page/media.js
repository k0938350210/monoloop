var folderFileManagerMedia; // file manager variable




function openLayoutMedia() {
	$("#innerContent").html("");



  var formElement = document.createElement('div');
  formElement.id = 'fileManagerParentIdMedia';
  formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
  document.getElementById("innerContent").appendChild(formElement);


  // webix.ready(function() {
  webix.MonoloopFileManager = {
    uploadFile2: function(id,e){
      if(!this.data.branch[id] && this.getItem(id).type != "folder"){
        id = this.getParentId(id);
      }
      this._uploaderFolder = id;
      var uploader = this.getUploader();
      uploader.fileDialog();
    }
  };

  folderFileManagerMedia = webix.ui({
    container: "fileManagerParentIdMedia",
    view: "filemanager",
    //noFileCache: true,
    url: monolop_api_base_url + "/api/media/folder?ajax=true", // loading data from the URL
    id: "webixFilemanagerMedia",
    filterMode: {
      showSubItems: true,
      openParents: true
    },
    mode: "table", // specify mode selected by default
    modes: ["files", "table", "custom"], // all available modes including a new mode
    // save and handle all the menu actionsTracker from here,
    // disable editing on double-click
    handlers: {
      "upload":  monolop_api_base_url + "/api/media/store",
      // "download": "data/saving.php",
      // "copy": "data/saving.php",
      "move": monolop_api_base_url + "/api/media/move",
      "remove": monolop_api_base_url + "/api/media/folder/delete",
      "rename": monolop_api_base_url + "/api/media/folder/update",
      "create": monolop_api_base_url + "/api/media/folder/create",
      "files": monolop_api_base_url +"/api/media?ajax=true",
      "search": monolop_api_base_url +"/api/media?ajax=true"
    },
    structure: {
      "actionsData":{
        config: function(){
          return [
            {id: "create", batch: "item,root" ,method: "createFolder", icon: "folder-o", value: webix.i18n.filemanager.create},
            {id: "remove", batch: "item", method: "deleteFile", icon: "times", value: webix.i18n.filemanager.remove},
            {id: "edit", batch: "item", method: "editFile",  icon: "edit", value: webix.i18n.filemanager.rename},

            {id: "upload", batch: "item,root" , method: "uploadFile2", icon: "upload", value: webix.i18n.filemanager.upload},
            { $template:"Separator" },
            {id: "crop", batch: "item", method: "cropFile",  icon: "crop", value: 'Crop'},
            {id: "resize", batch: "item", method: "resizeFile",  icon: "expand", value: 'Resize'}
          ];

        }
      }
    },

    on: {
      "onViewInit": function(name, config) {
        if(name === 'tree'){
          //config.drag = false ;
        }
        if (name == "table" || name == "files") {
          //config.drag = false ;
        }
        if(name == 'table'){
          // an array with columns configuration
          config.fixedRowHeight = false ;
          config.rowLineHeight = 30 ;
          config.rowHeight = 60 ;

          var columns = config.columns;
          config.columns[3].header = 'Filesize';
          // configuration of a new column
          var newColumn = {
              id: "new",
              header:"Thumb",
              width:80,
              template: function(obj,common){
                 var id = obj.id ;
                 if(obj.type && obj.type == 'image'){
                    var parentFolder = id.substr(0, id.lastIndexOf('/'));
                    var file = id.split(/(\\|\/)/g).pop() ;
                    return '<div class="filemanager-image-thumbs" ><img src="' + parentFolder + '/thumbs/' + file + '?' + obj.date + '" style="width:60px" /><div class="filemanager-image-preview"><img src="' + parentFolder + '/thumbs/' + file + '?' + obj.date + '"  /></div></div>' ;
                 }
                 return '' ;
              }
           };
           // insert a column
           webix.toArray(columns).insertAt(newColumn,0);
        }
        if(name == 'files'){
          config.template = function(obj,common){
            var icon = obj.type ||"file";
            icon = common.icons[icon] || common.icons["file"];
            var css = "webix_fmanager_data_icon";
            var name = common.templateName(obj,common);
            var id = obj.id ;
            if(obj.type && obj.type == 'image'){
                var parentFolder = id.substr(0, id.lastIndexOf('/'));
                var file = id.split(/(\\|\/)/g).pop() ;

                return "<div class='webix_fmanager_file'><div class='"+css+"'>"+ '<img src="' + parentFolder + '/thumbs/' + file + '?' + obj.date + '" style="width:55px;" />' +"</div>"+name+"</div>";
             }
            return "<div class='webix_fmanager_file'><div class='"+css+"'>"+common.templateIcon(obj,common)+"</div>"+name+"</div>";
          }
        }

      },

      onBeforeRequest:function(obj,data){
        this.showProgress();
      },

      onSuccessResponse:function(obj,data){
        if(obj.action && obj.action === 'remove'){
          for(var i = 0 ; i < data.names.length ; i++){
            webix.message({type: "success", text: "Remove file "+data.names[i]+"."});
          }
        }

        if(obj.action && obj.action === 'rename'){
          webix.message({type: "success", text: "Success rename "+data.value+"."});
        }

        if(obj.action && obj.action === 'move'){
          for(var i = 0 ; i < data.length ; i++){
            webix.message({type: "success", text: "Success move "+data[i].value+"."});
          }
        }

        this.hideProgress();
      },

      onErrorResponse:function(obj,data){
        var data_obj = webix.DataDriver.json.toObject(data) ;

        if(data_obj && data_obj.errors){
          for(var i = 0 ; i < data_obj.errors.length ; i++ ){
            webix.message({type: "error", text: data_obj.errors[i]+"."});
          }
        }

        this.showProgress();
      }
    }
  });
	$$('webixFilemanagerMedia').$$('modes').hide(); //Fixed Mantis#4163
  webix.extend($$("webixFilemanagerMedia"),webix.MonoloopFileManager);

  //webix.extend($$("webixFilemanagerMedia"),webix.MonoloopS3FileManager);
  var uploader = $$("webixFilemanagerMedia").getUploader() ;

  uploader.attachEvent('onBeforeFileAdd',function(file){
    if(file.size > 10000000){
      webix.message({type: "error", text: "Cannot upload "+file.name+" - max file size is 10 MB" });
      return false ;
    }

  });

  uploader.attachEvent("onAfterFileAdd",function(file){
    var fm = $$("webixFilemanagerMedia") ;
    fm._uploaderFolder = null;
    file.oldId = file.id;
    if(fm.config.uploadProgress){
      fm.showProgress(fm.config.uploadProgress);
    }
    fm.refreshCursor();
  });

  uploader.attachEvent("onFileUpload",function(item){
    var fm = $$("webixFilemanagerMedia") ;
    var t = fm.getItem(item.id) ;
    if(!t){
      fm.add({
        "id"   : item.id,
        "value": item.name,
        "type" : item.type,
        size   : item.size,
        date   : item.file.lastModifiedDate ,
      }, -1, uploader.config.formData.target);
    }
    webix.message({type: "success", text: "Success upload file "+item.name+"."});

    fm.refreshCursor();
    fm.hideProgress();

  });

  uploader.attachEvent("onFileUploadError", function(item, response){
    webix.message({type: "error", text: "Cannot upload "+item.name+" - "+response.data.upload.join() });
  });


  /*******************************Menu Customization******************************/
  // only FILES mode should be available
  //$$('webixFilemanagerTracker').$$('modes').hide();

  // updating options from menu
  var actionsMenu = $$("webixFilemanagerMedia").getMenu();
  actionsMenu.attachEvent("onBeforeShow", function(e){
    var c = this.getContext();

    var id = c.id ;
    if(id && id.row){
      id = id.row;
    }

    if(id == undefined || id.indexOf('asset') === 0){
      this.disableItem('crop');
      this.disableItem('resize');

      this.showItem('create');
    }else{
      this.enableItem('crop');
      this.enableItem('resize');

      this.showItem('create');
      this.hideItem('create');
    }
  });



  $$("webixFilemanagerMedia").$$('table').attachEvent("onItemDblClick",function(id){
    if(id.row.startsWith('http')){
      if(window.parent && window.parent.setFileManagerSelectUrl){
        window.parent.setFileManagerSelectUrl(id.row);
      }
    }
  });

  $$("webixFilemanagerMedia").$$('files').attachEvent("onItemDblClick",function(id){
    if(id.startsWith('http')){
      if(window.parent && window.parent.setFileManagerSelectUrl){
        window.parent.setFileManagerSelectUrl(id);
      }
    }
  });



  actionsMenu.attachEvent("onItemClick",function(id,e){
    if(id == 'crop'){
      var menu = $$("webixFilemanagerMedia").getMenu().getItem(id);
      var c = this.getContext();
      var id = c.id ;
      if(id.row){
        id = id.row;
      }

      if(id.indexOf('asset') !== 0){
        $$("webixFilemanagerMedia").getMenu().hide();
        mediaShowCropWindow(id);
        //console.log($$("webixFilemanagerMedia").$$("tree").getSelectedId());
      }
    }

    if(id === 'resize'){
      var menu = $$("webixFilemanagerMedia").getMenu().getItem(id);
      var c = this.getContext();
      var id = c.id ;
      if(id.row){
        id = id.row;
      }
      if(id.indexOf('asset') !== 0){
        $$("webixFilemanagerMedia").getMenu().hide();
        mediaShowResizeindow(id);
      }
    }
  });
  //actionsMenu.clearAll();
  /*
  actionsTracker.clearAll();
  var newDataTracker = [

    {
      id: "create",
      method: "createFolder",
      icon: "folder-o",
      value: webix.i18n.filemanager.create // "Create Folder"
    }, {
      id: "deleteFile",
      method: "deleteFile",
      icon: "times",
      value: webix.i18n.filemanager.remove // "Delete"
    }, {
      id: "edit",
      method: "editFile",
      icon: "edit",
      value: webix.i18n.filemanager.rename // "Rename"
    }
  ];
  actionsTracker.parse(newDataTracker);
  // add new option for the menu to add new Tracker
  actionsTracker.add({
    id: "createTracker",
    icon: "file",
    value: "Create Audience"
  });
  */
  $('<link>')
  .appendTo('head')
  .attr({
      type: 'text/css',
      rel: 'stylesheet',
      href: '/vendor/laravel-filemanager/css/cropper.min.css'
  });
  $('<link>')
  .appendTo('head')
  .attr({
      type: 'text/css',
      rel: 'stylesheet',
      href: '/vendor/laravel-filemanager/css/lfm.css'
  });

  $('<link>')
  .appendTo('head')
  .attr({
      type: 'text/css',
      rel: 'stylesheet',
      href: '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css'
  });

  $.getScript('//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js');
  $.getScript('/vendor/laravel-filemanager/js/cropper.min.js');
  $.getScript('//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js');

	$(window).on('resize', function(){
    $$('webixFilemanagerMedia').define('width', $('body').width() - $('.sidebar').width() - 1);
    $$('webixFilemanagerMedia').resize();
  });
}


function resize(target, new_width, new_height){
    var $wrapper = $(target).resizable('widget'),
        $element = $wrapper.find('.ui-resizable'),
        dx = $element.width()  - new_width,
        dy = $element.height() - new_height;

    $element.width( new_width );
    $wrapper.width( $wrapper.width() - dx );
    $element.height( new_height );
    $wrapper.height( $wrapper.height() - dy );
}


function mediaShowCropWindow(file){
  var option = null ;
  var $dataX , $dataY , $dataHeight , $dataWidth ;

  var w =  webix.ui({
    view:"window",
    id:"crop_win",
    fullscreen:true ,
    head:"Crop window",
    body:{
      margin:10, padding:0,
      cols:[
        {
          view: "template",
          template:'<div class="crop-container"><img src="'+file+'?'+ new Date().getTime() +'" class="img img-responsive"></div>'
        },{
          type:"space",
          rows:[{
            view:"toolbar",
            elements: [
              {
                view:"segmented" ,
                options:[
                  {id:"r16-9", value:"16:9"},
                  {id:"r4-3", value:"4:3"},
                  {id:"r1-1", value:"1:1"},
                  {id:"r2-3", value:"2:3"}
                ],
                on:{
                  onChange:function(newv,oldv){
                    var aspectRatio = 0 ;
                    if(newv == 'r16-9'){
                      aspectRatio = 16/9 ;
                    }else if(newv == 'r4-3'){
                      aspectRatio = 4/3
                    }else if(newv == 'r1-1'){
                      aspectRatio = 1
                    }else if(newv == 'r2-3'){
                      aspectRatio = 2/3
                    }
                    options.aspectRatio = aspectRatio;
                    $('.img-preview').removeAttr('style');
                    $image.cropper('destroy').cropper(options);
                  }
                }
              }
            ]
          },
          {
            template:'<br/><div class="text-center" style="text-align: center;"><div class="img-preview center-block" ></div><br/></div>',

          },{
            align:"top,center",
            cols:[{
                template : ''
              },
              {
                 view : 'button' ,
                 value : 'Crop' ,
                 on : {
                  onItemClick : function(id,e){
                    $$(id).disable();
                    webix.ajax().post(monolop_api_base_url + '/api/media/crop',
                      {
                        dataX : $dataX ,
                        dataY : $dataY ,
                        dataWidth : $dataWidth ,
                        dataHeight : $dataHeight ,
                        source : file
                      }, {
                      error: function(text, data, XmlHttpRequest) {
                        $$(id).enable();
                        webix.message({type: "error", text: "Error."});
                      },
                      success: function(text, data, XmlHttpRequest) {
                        $$('crop_win').close();
                        $$("webixFilemanagerMedia").clearAll();
                        $$("webixFilemanagerMedia").load(monolop_api_base_url+ "/api/media/folder?ajax=true");
                        var filename = file.split(/(\\|\/)/g).pop() ;
                        webix.message({type: "success", text: "Success crop file "+filename+"."});
                      }
                    });
                  }
                 }
              },{
                 view : 'button' ,
                 value : 'Cancel' ,
                 on : {
                    onItemClick : function(id,e){
                      $$('crop_win').close();
                    }
                 }
              },{
                template : ''
              }
            ]
          },{
            template:''
          }
          ]

        }
      ]
    },
    on: {
      onShow:function(){
        $image = $('.crop-container > img');
        options = {
          aspectRatio: 16 / 9,
          preview: ".img-preview",
          strict: false,
          checkImageOrigin:false,
          crop: function (data) {
            $dataX = Math.round(data.x);
            $dataY = Math.round(data.y);
            $dataHeight = Math.round(data.height);
            $dataWidth = Math.round(data.width);
          }
        };
        $image.cropper(options);
      }
    }
  }).show();
}

function mediaShowResizeindow(file){
  var fm = $$("webixFilemanagerMedia") ;
  fm.showProgress();
  webix.ajax().post(monolop_api_base_url + '/api/media/resize_detail',
  {
    source : file
  }, {
  error: function(text, data, XmlHttpRequest) {
    fm.hideProgress();
    webix.message({type: "error", text: "Error."});
  },
  success: function(text, data, XmlHttpRequest) {
    fm.hideProgress();
    var data = JSON.parse(text);
    mediaShowResizeindow2(data);
  }
});
}

function mediaShowResizeindow2(data){
  var option = null ;
  var $dataX , $dataY , $dataHeight , $dataWidth ;
  var stop_auto_resize = true ;

  var w =  webix.ui({
    view:"window",
    id:"resize_win",
    fullscreen:true ,
    head:"Resize window",
    body:{
      margin:10, padding:0,
      cols:[
        {
          view: "template",
          template:'<div id="resize-containment"><img  id="resize" src="'+data.img+'?'+ new Date().getTime() +'" width="'+data.width+'"  height="'+data.height+'"></div>',
          scroll:true
        },{
          type:"space",
          width:450,
          rows:[{
              template : '<table class="table table-compact table-striped"><thead></thead><tbody><tr><td><strong>Original Height:</strong></td><td>'+data.original_height+'</td></tr><tr><td><strong>Original Width:</strong></td><td>'+data.original_width+'</td></tr><tr><td><strong>Height:</strong></td><td><input type="text" id="height_display" /></td></tr><tr><td><strong>Width:</strong></td><td><input type="text" id="width_display" value=""/></td></tr></tbody></table>'
            },{
            align:"top,center",
            cols:[{
                template : ''
              },
              {
                 view : 'button' ,
                 value : 'Resize' ,
                 on : {
                  onItemClick : function(id,e){
                    $$(id).disable();
                    webix.ajax().post(monolop_api_base_url + '/api/media/resize',
                      {
                        dataWidth : $("#resize").width() ,
                        dataHeight : $("#resize").height() ,
                        source : data.img
                      }, {
                      error: function(text, data, XmlHttpRequest) {
                        $$(id).enable();
                        webix.message({type: "error", text: "Error."});
                      },
                      success: function(text, data2, XmlHttpRequest) {
                        $$('resize_win').close();
                        $$("webixFilemanagerMedia").clearAll();
                        $$("webixFilemanagerMedia").load(monolop_api_base_url+ "/api/media/folder?ajax=true");
                        var filename = data.img.split(/(\\|\/)/g).pop() ;
                        webix.message({type: "success", text: "Success resize file "+filename+"."});
                      }
                    });
                  }
                 }
              },{
                 view : 'button' ,
                 value : 'Cancel' ,
                 on : {
                    onItemClick : function(id,e){
                      $$('resize_win').close();
                    }
                 }
              },{
                template : ''
              }
            ]
          },{
            template:''
          }]
        }
      ]
    },
    on: {
      onShow:function(){
         $("#resize").resizable({
          aspectRatio: true,
          resize: function(event, ui){
            $("#height_display").val($("#resize").height() );
            $("#width_display").val($("#resize").width() );
          }
        });

        $("#height_display").val($("#resize").height() );
        $("#width_display").val($("#resize").width() );

        $('#height_display').keyup(function (event) {
          var $this = $(this);
          $this.val($this.val().replace(/[^\d.]/g, ''));
          var new_height = parseInt($('#height_display').val());
          var new_width = parseInt(new_height*data.original_width/data.original_height);
          resize('#resize',new_width,new_height);
          $("#width_display").val(new_width);
        });


        $('#width_display').keyup(function (event) {
          var $this = $(this);
          $this.val($this.val().replace(/[^\d.]/g, ''));
          var new_width = parseInt($('#width_display').val());
          var new_height = parseInt(new_width*data.original_height/data.original_width);
          resize('#resize',new_width,new_height);
          $("#height_display").val(new_height);
        });
      }
    }
  }).show();
}
