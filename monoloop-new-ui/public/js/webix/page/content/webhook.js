"use strict";
(function(global){
  var msg; // default variable for user messages
  var isSessional = false;

  // var _cms_page_url = $('body').attr('cms-page-url') !== undefined ? $('body').attr('cms-page-url') : '';
  // console.log('---', _cms_page_url);
  var webhookListConfig = {
    replicateGoal: true,
    replicateSegment:true,
    folderManager: false,
    msg: "",
    placementWindow: {},
    currentStepInfo: {},
    page: 'webhook',
    apis: {
      show: monolop_api_base_url + '/api/webhooks/show',
      list: monolop_api_base_url + "/api/webhooks?ajax=true",
      // create: monolop_api_base_url + "/api/experiments/create",
      // update: monolop_api_base_url + "/api/experiments/update",
      // update_status: monolop_api_base_url + "/api/experiments/update-status",
      // change_folder: monolop_api_base_url + "/api/experiments/change-folder",
      delete: monolop_api_base_url + "/api/webhooks/delete",
      files: monolop_api_base_url + "/api/webhooks/folder?ajax=true",
      // webhookcreate: monolop_api_base_url + '/api/webhooks/create',
    },
    formElement: {
      id: 'pageElementIdWebhookList',
    },
    pw: {mode: 'add', data: {}},
    pageElementDocumentEdit: {},
    pageElements: [],
    experiment:  {},
    placementIframeSrc: "/component/placement-window",
    defaultSteps: [],
    steps: [],
    activeStep: "",
    stepsGenerated: {
      basic: false,
      segment: false,
      goal: false,
      pw: false,
      cg: false,
      confirm: false
    },
    containerId: "innerContent",
    showHeader: function (){
      $('div[view_id="webixWizardHeaderMenuToolbarWebhook"]').show();
      $("#viewHeader").show();
    },

    hideHeader: function(){
      $('div[view_id="webixWizardHeaderMenuToolbarWebhook"]').hide();
      $("#viewHeader").hide();
    },

    urlOptions: [
      {id: 'exactMatch', value: 'Exact Match'},
      {id: 'startsWith', value: 'Starts With'},
    ],

    getUrlOption: function(id){
        id = id || 0;
        switch (id) {
          case 0: return "exactMatch"; break;
          case 1: return "allowParams"; break;
          case 2: return "startsWith"; break;
          default: return "exactMatch"; break;
        }
    },
    highLightCurrentStep: function (current) {
      $(".wizardStepBtn .webix_badge").removeClass('active');
      switch (current) {
        case "basic":
          $(".innerContent .wizardStepBtn.Basic .webix_badge").addClass('active');
          break;
        case "segment":
          $(".innerContent .wizardStepBtn.SegmentAndGoal .webix_badge").addClass('active');
          break;
        case "goal":
          $(".innerContent .wizardStepBtn.Goal .webix_badge").addClass('active');
          break;
        case "placement":
          $(".innerContent .wizardStepBtn.Placement .webix_badge").addClass('active');
          break;
        case "controlGroup":
          $(".innerContent .wizardStepBtn.controlGroup .webix_badge").addClass('active');
          break;
        case "confirm":
          $(".innerContent .wizardStepBtn.Confirm .webix_badge").addClass('active');
          break;
        default:
          break;
      }
    },
    updateFile: function (id, src) {
      config.currentElementID = 'webixWizardHeaderMenuWebhook';
      var url = webhookListConfig.apis.show;

      // retreiving form data and setting into the form
      var formData;
      webix.ajax().get(url, {
        _id: id,
        ajax: true,
      }, function(text, xml, xhr) {
        //response
        text = JSON.parse(text);
        formData = text.webhook;
        webhookListConfig.openWizard('edit', formData, src);
      });
    },
    deleteFile: function (id, aws_id) {
      webix.confirm({
        text: "Do you want to delete?",
        ok: "Yes",
        cancel: "No",
        callback: function(result) {
          if (result) {

            webix.ajax().post(webhookListConfig.apis.delete, {
              _id: id,
              aws_message_id: aws_id,
              ajax: true
            }, {
              error: function(text, data, XmlHttpRequest) {
                alert("error");
              },
              success: function(text, data, XmlHttpRequest) {
                var response = JSON.parse(text);
                if (response.success == true) {
                  webix.message("Webhook Deleted");
                  webhookListConfig.folderManager.deleteFile(id);
                }
              }
            });
          }
        }
      });

    },
    openWizard: function (mode, formData, src) {
      // reinitialize the settings
      webhookListConfig.replicateGoal = true;
      webhookListConfig.replicateSegment = true;

      if($('#floatingContainer').length > 0){
        $('#floatingContainer').remove();
      }

      // console.log(formData._id);
      var f = window.location.hash;
      if(mode === 'add'){
        window.history.pushState(config.addwebhook.headerName, config.addwebhook.headerName, config.addwebhook.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);
      }else if(mode === 'edit'){
        window.history.pushState(config.editwebhook.headerName, config.editwebhook.headerName, config.editwebhook.link + '?id=' + formData._id +   ($('body').hasClass('hidenav') ? '&nav=1' : '') + f);
      }

      webhookListConfig.defaultSteps = ["segment", "placement", "confirm"];
      webhookListConfig.activeStep = webhookListConfig.defaultSteps[0];
      webhookListConfig.stepsGenerated.basic = false;
      webhookListConfig.stepsGenerated.segment = false;
      webhookListConfig.stepsGenerated.goal = false;
      webhookListConfig.stepsGenerated.pw = false;
      webhookListConfig.stepsGenerated.cg = false;
      webhookListConfig.stepsGenerated.confirm = false;

      if(userStateData){
        isSessional = userStateData.section === 'experiment';
        webhookListConfig.activeStep = userStateData.data.step;
      }

      $("#innerContent").html("");

      var wizard = document.createElement('div');
      wizard.id = 'webixWizardParentIdWebhook';
      // wizard.style.minHeight = '680px';
      document.getElementById("innerContent").appendChild(wizard);


      webhookListConfig.steps = webhookListConfig.defaultSteps;

      var wizardBtnWebhook = {
        view: "button",
        css: "wizardStepBtn SegmentAndGoal",
        id: "wizardBtnWebhook",
        label: "Create Webhook",
        width: 200,
        // badge: 1,
        disabled: true
        // click: function() {
        //   webhookListConfig.activeStep = webhookListConfig.steps[0];
        //
        // }
      };



      webix.ui({
        container: "webixWizardParentIdWebhook",
        height: config.styles.defaultHeight.getFileManagerHeight(),
        id: "webixWizardHeaderMenuWebhook",
        rows: [{
          view: "toolbar",
          paddingY: 1,
          height: 50,
          id: "webixWizardHeaderMenuToolbarWebhook",
          hidden: false,
          css: "webixWizardHeader",
          elements: [{
              gravity: 1
            },  wizardBtnWebhook,{
              gravity: 1
            }
          ]
        }, {
          template: "<div id='wizardbasicSectionId'></div><div id='wizardWebhookSection'></div>"
        }]
      });
      webhookListConfig.openWizardWebhook(mode, formData);

    },
    openWizardWebhook: function(mode, formData){
        $("#wizardWebhookSection").html("");
        // if (mode === 'edit') {
        //   console.log($('#wizardBtnWebhook'));
        //   $('#wizardBtnWebhook').prop('disabled',"false");
        //   $('#wizardBtnWebhook').html('Update Webhook');
        // }
        var expurl = monolop_api_base_url+"/api/experiments/getallexp";
        var goalURL = monolop_api_base_url + "/data/experiments/goal";
        var segmentURL = monolop_api_base_url + "/data/experiments/segment";
        // var getCid = monolop_api_base_url + "/api/experiments/getCID";
        webix.ajax().get(expurl, {ajax: true}, function(resExp, xml, xhr) {
          webix.ajax().get(segmentURL, {ajax: true}, function(resSegment, xml, xhr) {
            webix.ajax().get(goalURL, {ajax: true}, function(resGoal, xml, xhr) {
              // webix.ajax().get(getCid, {ajax: true}, function(resCID, xml, xhr) {
              // resCID = JSON.parse(resCID);
              resSegment = JSON.parse(resSegment);
              resGoal = JSON.parse(resGoal);
              resExp = JSON.parse(resExp);

            var _form = [{
                 view: "layout",
                 height: 520,
                 rows: [{
                   //   view: "text",
                   //   id: "nameWebhook",
                   //   label: 'Name',
                   //   name: "nameWebhook",
                   //   invalidMessage:"Name field cannot be empty"
                   //   // readonly: true,
                   //     // invalidMessage: "Name can not be empty"
                   // }, {
                     view: "richselect",
                     label: "Please select the Type you wish to target:",
                     id: "typeWebhook",
                     css: "selectSegment",
                     name: "typeWebhook",
                     value: "",
                     options: [
                       "Experiment",
                       "Goal",
                       "Audience",
                       "Inactive"
                     ],
                     labelAlign: 'left',
                     invalidMessage:"Field cannot be empty"
                 }, {
                   view: "richselect",
                   label: "Please select Target:",
                   id: "targetWebhook",
                   css: "selectSegment",
                   name: "targetWebhook",
                   value: "",
                   options: ["Please Choose Webhook Type"],
                   labelAlign: 'left',
                   invalidMessage:"Field cannot be empty"
                 },{
                  view: "text",
                  label: "Inactive:",
                  attributes:{ type:"number" },
                  id: "target-inactive",
                  css: "selectSegment",
                  hidden: true,
                  name: "target-inactive",
                  value: "",
                  labelAlign: 'left'
                 },{
                   view: "richselect",
                   label: "Unit:",
                   id: "target-unit",
                   css: "selectSegment",
                   name: "target-unit",
                   hidden: true,
                   value: "days",
                   options: ["days","hours","minutes"],
                   labelAlign: 'left',
                   invalidMessage:"Field cannot be empty"
                 },
                 {
                     view: "richselect",
                     label: "Please select the Direction",
                     id: "directionWebhook",
                     css: "selectGoal",
                     name: "directionWebhook",
                     value: "",
                     options: [
                       "In",
                       "Out"
                     ],
                     labelAlign: 'left',
                     invalidMessage:"Field cannot be empty"
                 },
                 {
                     view: "text",
                     label: "Callback URL",
                     id: "CallbackUrlWebhook",
                     // css: "selectGoal",
                     name: "CallbackUrlWebhook",
                     value: "",
                     // options: createNewGoalOption.concat(goals),
                     labelAlign: 'left',
                     invalidMessage:"Url is Required"
                 },
                 {
                   view: "richselect",
                   label: "Please select Payload",
                   id: "payloadWebhook",
                   css: "selectGoal",
                   name: "payloadWebhook",
                   value: "",
                   options: [
                     "Full Json Profile",
                     "Lean Notification"
                   ],
                   labelAlign: 'left',
                   invalidMessage:"Field cannot be empty"
               }
                 // {
                 //   cols: [
                 //     {
                 //       view:"checkbox",
                 //       id:"payloadWebhook",
                 //       name:"payloadWebhook",
                 //       css: "webhookcheckbox",
                 //       label:"Full Json Profile",
                 //       labelAlign: 'left',
                 //       // invalidMessage:"Must be Checked"
                 //       // readonly: true,
                 //       // value: 0
                 //     }, {
                 //       view:"checkbox",
                 //       id:"payloadWebhook1",
                 //       name:"payloadWebhook1",
                 //       css: "webhookcheckbox",
                 //       label:"Lean Notification",
                 //       // labelWidth: 20,
                 //       labelAlign: 'left',
                 //       // invalidMessage:"Url is Required"
                 //       // readonly: true,
                 //       // value: 0
                 //     }
                 //   ]
                 // }
               ]
               }, {
                 cols: [{}, {}, {
                     view: "button",
                     value: "Save",
                     id: "btnCreateWebhook",
                     name: "btnCreateWebhook",
                     css: "orangeBtn",
                     width: 138,
                     click: function() {



                       // form validity check is remaining
                       // $$("CreateFormWebhook").validate();
                       var validationflag = $$("CreateFormWebhook").validate();
                       // var urlflag = true;
                       // if (validationflag && ($$("payloadWebhook").getValue() === 1 || $$("payloadWebhook1").getValue() === 1 ) ) {
                       if (validationflag ) {
                         $$("CallbackUrlWebhook").getValue()
                         if ($$("CallbackUrlWebhook").getValue().match("^http://") || $$("CallbackUrlWebhook").getValue().match("^https://") ) {
                           // var nameWebhook = $$("nameWebhook").getValue();
                           var hook_type = $$("typeWebhook").getText();
                           var targetWebhook = $$("targetWebhook").getText();
                           var dir_webhook = $$("directionWebhook").getText();
                           var callback_url = $$("CallbackUrlWebhook").getValue();
                           var payloadWebhook = $$("payloadWebhook").getText();
                           if(hook_type == 'Inactive'){
                            targetWebhook = $$("target-inactive").getValue() + ' ' + $$("target-unit").getValue();
                           }
                           // var payloadWebhook1 = $$("payloadWebhook1").getValue();
                           var uID = 0;
                           var cID = 0;
                           var payload = "";

                           if (hook_type === "Experiment") {

                             for (var i = 0; i < resExp.length; i++) {
                               if (targetWebhook === resExp[i].name) {
                                 uID = resExp[i].experimentID;
                                 cID = resExp[i].cid;
                               }
                             }
                           }
                           else if (hook_type === "Audience") {

                             for (var i = 0; i < resSegment.segments.length; i++) {
                               if (targetWebhook === resSegment.segments[i].name) {
                                 uID = resSegment.segments[i].uid;
                                 cID = resSegment.segments[i].cid;
                               }
                             }
                           }
                           else if (hook_type === "Goal") {

                             for (var i = 0; i < resGoal.goals.length; i++) {
                               if (targetWebhook === resGoal.goals[i].name) {
                                 uID = resGoal.goals[i].uid;
                                 cID = resGoal.goals[i].cid;
                               }
                             }
                           }
                           if (payloadWebhook === "Full Json Profile") {
                             payload = "Full";
                           }
                           else if(payloadWebhook === "Lean Notification"){
                             payload = "Lean";
                           }
                           hook_type = hook_type + "_" +dir_webhook;
                           var successmsg = "";
                           var url= "";
                           var request_type = 'POST'
                           var data = {
                             "event_type": hook_type,
                             "event_name": targetWebhook,
                             "event_id": uID,
                             "callback_url": callback_url,
                             "payload_type": payload
                            }
                           if (mode === 'add') {
                             successmsg = "Webhook Created";
                             url = monolop_api_base_url + "/api/webhooks";
                             //url = "http://"+webhook_url+":"+webhook_port+"/subscribe?cid="+cID+"
                             webix.ajax().post(url,data,{
                              error: function(text, data, XmlHttpRequest) {
                                if(text == "error"){
                                  webix.message({
                                    text:"Error: Server is not responding",
                                    type:"error",
                                    expire: 10000,
                                    id:"message1"
                                  });
                                }
                              },
                              success: function(text, data, XmlHttpRequest) {
                                webix.message(successmsg);
                                openLayoutWebhook();
                              }
                            });
                           }else if(mode === 'edit'){
                            request_type = 'PUT';
                             successmsg = "Webhook Updated";
                             url = monolop_api_base_url + '/api/webhooks/'+formData._id;
                             //url = "http://"+webhook_url+":"+webhook_port+"/webhook/edit?id="+formData._id+"&event_type="+hook_type+"&event_name="+targetWebhook+"&event_id="+uID+"&payload_type="+payload+"&callback_url="+callback_url;

                            webix.ajax().put(url,data,{
                              error: function(text, data, XmlHttpRequest) {
                                if(text == "error"){
                                  webix.message({
                                    text:"Error: Server is not responding",
                                    type:"error",
                                    expire: 10000,
                                    id:"message1"
                                  });
                                }
                              },
                              success: function(text, data, XmlHttpRequest) {
                                webix.message(successmsg);
                                openLayoutWebhook();
                              }
                            });
                           }




                         }else {
                           // urlflag = false;
                           webix.message("Please Enter a Valid URL");
                         }
                       }else {
                         webix.message("Please Fill all the Fields");
                       }
                     }
                   }, {}, {},

                 ]
               }];
           var __formRules = {
             // "nameWebhook": webix.rules.isNotEmpty,
             "typeWebhook": webix.rules.isNotEmpty,
             "targetWebhook": webix.rules.isNotEmpty,
             "directionWebhook": webix.rules.isNotEmpty,
             "CallbackUrlWebhook": webix.rules.isNotEmpty,
             "payloadWebhook": webix.rules.isNotEmpty,
           };

           webix.ui({
                   container: "wizardWebhookSection",
                   id: "stepWebhook",
                   rows: [{
                     cols: [{}, {
                       view: "form",
                       id: "CreateFormWebhook",
                       complexData: true,
                       elements: _form,
                       rules: __formRules,
                       width: 600,
                       borderless: true,
                       margin: 3,
                       elementsConfig: {
                         labelPosition: "top",
                         labelWidth: 140,
                         bottomPadding: 18
                       },
                     }, {}, ]
                   }]
                 });
                 // check if mode is edit, to fill the required data in field
                 if (mode === 'edit') {
                   $$("typeWebhook").setValue(formData.event_type);
                   $$("directionWebhook").setValue(formData.direction);
                   $$("CallbackUrlWebhook").setValue(formData.callback_url);

                   if (formData.event_type === "Experiment") {
                     var exp = [];
                     for (var i = 0; i < resExp.length; i++) {
                       exp[i] = resExp[i].name;
                     }
                     $$("targetWebhook")
                     .define("options",
                     exp);
                   }
                   else if (formData.event_type === "Audience") {
                     var segment = [];
                     for (var i = 0; i < resSegment.segments.length; i++) {
                       segment[i] = resSegment.segments[i].name;
                     }
                     $$("targetWebhook")
                     .define("options",
                     segment);
                   }
                   else if (formData.event_type === "Goal") {
                     var goal = [];
                     for (var i = 0; i < resGoal.goals.length; i++) {
                       goal[i] = resGoal.goals[i].name;
                     }
                     $$("targetWebhook")
                     .define("options",
                     goal);
                   }else if(formData.event_type === "Inactive"){
                    var data = ["30 minutes", "30 days", "120 days"];
                    $$("targetWebhook").define("options", data);
                   }

                   $$("targetWebhook").setValue(formData.event_name);

                   if(formData.event_type === "Inactive"){
                    $$("targetWebhook").hide();
                    $$("target-inactive").show();
                    $$("target-unit").show();

                    var inactive_data = formData.event_name.split(' ');
                    $$("target-inactive").setValue(inactive_data[0]);
                    $$("target-unit").setValue(inactive_data[1]);
                    $$("directionWebhook").hide();
                   }else{
                    $$("targetWebhook").show();
                    $$("target-inactive").hide();
                    $$("target-unit").hide();
                    $$("directionWebhook").show();
                   }

                   if (formData.payload_type === 'Full') {
                     $$("payloadWebhook")
                     .setValue("Full Json Profile");
                   }else {
                     $$("payloadWebhook")
                     .setValue("Lean Notification");
                   }

                 }
                 // $$("payloadWebhook").attachEvent("onChange", function(newv, oldv){
                 //   if ($$("payloadWebhook").getValue() == 1) {
                 //     $$("payloadWebhook1")
                 //     .setValue(0);
                 //   }
                 //
                 // });
                 // $$("payloadWebhook1").attachEvent("onChange", function(newv, oldv){
                 //   if ($$("payloadWebhook1").getValue() == 1) {
                 //     $$("payloadWebhook")
                 //     .setValue(0);
                 //   }
                 // });
                     $$("typeWebhook").attachEvent("onChange", function(newv, oldv){
                        if (newv === "Experiment") {
                          var exp = [];
                          for (var i = 0; i < resExp.length; i++) {
                            exp[i] = resExp[i].name;
                          }
                          $$("targetWebhook")
                          .define("options",
                          exp);
                        }
                        else if (newv === "Audience") {
                          var segment = [];
                          for (var i = 0; i < resSegment.segments.length; i++) {
                            segment[i] = resSegment.segments[i].name;
                          }
                          $$("targetWebhook").define("options", segment);
                        }
                        else if (newv === "Goal") {
                          var goal = [];
                          for (var i = 0; i < resGoal.goals.length; i++) {
                            goal[i] = resGoal.goals[i].name;
                          }
                          $$("targetWebhook").define("options", goal);
                        }
                        else if(newv === "Inactive"){
                          var data = ["30 minutes", "30 days", "120 days"];
                          $$("targetWebhook").define("options", data);
                        }
                        $$("targetWebhook").setValue("");
                        if(newv === "Inactive"){
                          $$("targetWebhook").hide();
                          $$("target-inactive").show();
                          $$("target-unit").show();
                          $$("directionWebhook").hide();
                         }else{
                          $$("targetWebhook").show();
                          $$("target-inactive").hide();
                          $$("target-unit").hide();
                          $$("directionWebhook").show();
                         }
                      });
                     // });
                    });
                   });
                  });
      },
    fab: {
      actionButtonTypes: {
    		type: "create",
    		setType: function(t){
    			t = t || "create";
    			webhookListConfig.fab.actionButtonTypes.type = t;
    		},
    		fabOptions: false,
    		create: {
    			options: [
            // {
    				// 	label: "This is to test",
    				// 	className: "segmentBuilder inline",
            //   callback: function(){
            //     webix.alert("This is to test......");
            //   }
  				  // }
    			]
    		},
    	},
      fabOptions: false,
      primary: {
        primaryBtnLabelCallback: function(actionButton, floatingTextBG){
          actionButton.className += " createExperiment inline";
          floatingTextBG.textContent = "Create Webhook";
          actionButton.onclick = function(){
              config.currentElementID = 'webixWizardHeaderMenuWebhook';
              webhookListConfig.openWizard('add');
          };
        },
      }


    },
    postMessageListener: function(event) {
      console.log('Check Event =>', event);
      var data = JSON.parse(event.data);
      switch (data.t) {
        case "set-current-experiment":
          if(data.created){
            webhookListConfig.experiment = data.experiment;
          }
          break;
        case "page-published-" + webhookListConfig.page:

          if(data.source === 'experiment'){
            isSessional = false;
            userStateData = '';
            // webix.message(data.message);
            webhookListConfig.pageElementDocumentEdit = {};
            // webhookListConfig.next(data.temp.mode, data.temp.formData);
            // webhookListConfig.showView(false, true, false, data.temp.mode, data.temp.formData);
          }
          break;
        case "set-active-step-sessional":
          webhookListConfig.currentStepInfo.step = data.step;
          webhookListConfig.currentStepInfo.url = data.url;
          break;
        default:
          break;
      }
    }
  };

  function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
      tokens,
      re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
      params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
  };

  function openLayoutWebhook() {
    //include url from params
    var params = getQueryParams(document.location + '');
    if(params.url){
      webhookListConfig.apis.list = webhookListConfig.apis.list + '&url=' + params.url ;
      webhookListConfig.apis.files = webhookListConfig.apis.files + '&url=' + params.url ;
    }
    var f = window.location.hash;
    // console.log(f);
    window.history.pushState(config.webhook.headerName, config.webhook.headerName, config.webhook.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);

    webhookListConfig.pageElementDocumentEdit = {};
    webhookListConfig.webhook = {};
    $("#innerContent").html("");

    var formElement = document.createElement('div');
    formElement.id = webhookListConfig.formElement.id;
    formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
    document.getElementById("innerContent").appendChild(formElement);

    // webix.ready(function() {

    webhookListConfig.folderManager = webix.ui({
      container: webhookListConfig.formElement.id,
      view: "filemanager",
      url: webhookListConfig.apis.list, // loading data from the URL
      id: "webixFilemanagerWebhook",
      filterMode: {
        showSubItems: true,
        openParents: true
      },
      editable:true,
      editor:"text",
      editValue:"value",
      mode: "table", // specify mode selected by default
      // modes: ["files", "table"], // all available modes including a new mode
      modes: ["table"],
      // modes: ["table"],
      // save and handle all the menu actions from here,
      // disable editing on double-click
      handlers: {
        // "upload": webhookListConfig.apis.create,
        // "download": "data/saving.php",
        // "copy": "data/saving.php",
        // "move": "/api/folders/move",
        "remove": "/api/folders/delete",
        // "rename": "/api/folders/update",
        // "create": "/api/folders/create",
        "files": webhookListConfig.apis.files,
      },
      on: {
        "onViewInit": function(name, config) {

          if (name == "table" || name == "files") {

            // disable multi-selection for "table" and "files" views
            config.select = true;

            if (name == "table") {
              // disable editing on double-click
              config.editaction = false;

              // an array with columns configuration
              var columns = config.columns;
              //  disabling columns date, type, size
              columns.splice(1, 3);

              // changes related to Mantis # 3974
              var TypeColumnWebhook = {
                id: "TypeColumnWebhook",
                header: "Type",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.event_type || 0; // "description" property of files
                }
              };



              var StatusColumnWebhook = {
                id: "StatusColumnWebhook",
                header: "Status",
                fillspace: 2,
                template: function(obj, common) {
                  return obj.status || 0; // "description" property of files
                }
              };



              var DirectionColumnWebhook = {
                id: "DirectionColumnWebhook",
                header: "Direction",
                fillspace: 1.5,
                template: function(obj, common) {
                  return obj.direction || 0; // "description" property of files
                }
              };



              var UrlColumnWebhook = {
                id: "UrlColumnWebhook",
                header: "Callback URL",
                fillspace: 3,
                template: function(obj, common) {
                  return obj.callback_URL || 0; // "description" property of files
                }
              };

              var PayloadColumnWebhook = {
                id: "PayloadColumnWebhook",
                header: "Payload Type",
                fillspace: 2,
                template: function(obj, common) {
                  return  obj.payload_type || 0; // "description" property of files
                }
              };
              var actionsColumnWebhook = {
                id: "actionsColumnExperiment",
                header: "Actions",
                width: 100,
                template: function(obj, common) {
                  var aws = 0;
                  return '<a  title="Edit Webhook" onclick="webhookListConfig.updateFile(\'' + obj.id + '\');" class="webix_list_item updateExperiment" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" onclick="webhookListConfig.deleteFile(\'' + obj.id + '\',\'' + aws + '\');" class="webix_list_item deleteExperiment" title="Delete Webhook" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
                }
              };


              // changes related to Mantis # 3974
              webix.toArray(columns).insertAt(TypeColumnWebhook, 1);
              webix.toArray(columns).insertAt(DirectionColumnWebhook, 2);
              webix.toArray(columns).insertAt(UrlColumnWebhook,3);
              webix.toArray(columns).insertAt(PayloadColumnWebhook,4);
              webix.toArray(columns).insertAt(StatusColumnWebhook, 5);
              webix.toArray(columns).insertAt(actionsColumnWebhook, 6);
              // webix.toArray(columns).insertAt(statusColumnExperiment, 7);
            }
          }
        }
      }
    });

    /*******************************Menu Customization******************************/
    // only FILES mode should be available

    $$('webixFilemanagerWebhook').$$('modes').hide();

    // updating options from menu

    var actions = $$("webixFilemanagerWebhook").getMenu();
    actions.clearAll();
    var newData = [

      // {
      //   id: "create",
      //   method: "createFolder",
      //   icon: "folder-o",
      //   value: webix.i18n.filemanager.create // "Create Folder"
      // },
      {
        id: "deleteFile",
        method: "deleteFile",
        icon: "times",
        value: webix.i18n.filemanager.remove // "Delete"
      }
      // , {
      //   id: "edit",
      //   method: "editFile",
      //   icon: "edit",
      //   value: webix.i18n.filemanager.rename // "Rename"
      // }
    ];
    actions.parse(newData);

    // add new option for the menu to add new segment
    actions.add({
      id: "createWebhook",
      icon: "file",
      value: "Create Webhook"
    });

    /*******************************Segment Add**********************************/

    actions.attachEvent("onItemClick", function(id) {
      this.hide();
      // check if the action is createWebhook
      if (id == "createWebhook") {
        config.currentElementID = 'webixWizardHeaderMenuWebhook';
        webhookListConfig.openWizard('add');

      }
    });

    // actions.getItem("create").batch = "item, root";
    actions.getItem("deleteFile").batch = "item, root";
    // actions.getItem("edit").batch = "item, root";
    actions.getItem("createWebhook").batch = "item, root";

    $$("webixFilemanagerWebhook").$$("table").attachEvent("onBeforeSelect", function(id, p){
      var rowName = id.row,
          columnName = id.column;
      var item = this.getItem(rowName);

      // if (item.type !== "folder"){
      //   if(['reportColumnExperiment', 'actionsColumnExperiment', 'statusColumnExperiment'].indexOf(columnName) === -1){
      //   //  webhookListConfig.updateFile(item.id);
      //   }
      // }
      //note that 'id' for datatable is an object with several attributes
    });

    $$("webixFilemanagerWebhook").attachEvent("onBeforeDeleteFile",function(id){
        // Fix: mantis # 3945
        console.log("before Delete");
        if (id.indexOf('webhook__') === -1) {
  				var item = this.getItem(id);
  				if(item.$parent === 0){
  					webix.message({type: "error", text: "Root folder can not be deleted."})
  					return false;
  				}else{
  					checkFolderStatus(id, webhookListConfig.folderManager);
  					return false;
  				}
  			}
    });

    // before editing file
    // $$("webixFilemanagerWebhook").attachEvent("onBeforeEditFile", function(id) {
    //
    //   var srcTypeId = id.split('__');
    //
    //   if (srcTypeId[0] == 'folder') {
    //     return true;
    //   }
    //
    //   // webhookListConfig.updateFile(id);
    //
    //   return false;
    // });

    // reload grid after folder creation
    // $$("webixFilemanagerWebhook").attachEvent("onAfterCreateFolder", function(id) {
    //   // webhookListConfig.refreshManager();
    //   setTimeout(function(){ openLayoutWebhook(); }, 500);
    //   return false;
    // });

    // it will be triggered before deletion of file
    // $$("webixFilemanagerWebhook").attachEvent("onBeforeDeleteFile", function(ids) {
    //   // webhookListConfig.deleteFile(ids);
    //   if (ids.indexOf('webhook__') === -1) {
    //     return true
    //   }
    //   webhookListConfig.deleteFile(ids);
    //   return false;
    // });

    // it will be triggered before dragging the folder/segment
    // $$("webixFilemanagerWebhook").attachEvent("onBeforeDrag", function(context, ev) {
    //   return true;
    // });

    // $$("webixFilemanagerWebhook").attachEvent("onBeforeDrop", function(context, ev) {
    //   if (context.start.indexOf('experiment__') === -1) {
    //     return true
    //   }
    //   webix.ajax().post("/api/experiments/change-folder", {
    //     _id: context.start,
    //     target: context.target,
    //     ajax: true
    //   }, {
    //     error: function(text, data, XmlHttpRequest) {
    //       alert("error");
    //     },
    //     success: function(text, data, XmlHttpRequest) {
    //       var response = JSON.parse(text);
    //       if (response.success == true) {}
    //     }
    //   });
    //   return true;
    // });

    // it will be triggered after dropping the folder to the destination
    // $$("webixFilemanagerWebhook").attachEvent("onAfterDrop", function(context, ev) {
    //   webix.message("Experiment has been moved to new folder.");
    //   return false;
    // });

    // $$("webixFilemanagerWebhook").$$("files").attachEvent("onBeforeRender", filterFilesExperiment);
    $$("webixFilemanagerWebhook").$$("table").attachEvent("onBeforeRender", filterFilesExperiment);

    var _exe = false;
    // $$("webixFilemanagerWebhook").attachEvent("onAfterLoad", function(context, ev) {
    //   if(_exe === false){
    //     _exe = true;
    //     // initIntialTTandGuides();
    //   }
    // });

    // fix: mantis # 3946
    // $$("webixFilemanagerWebhook").attachEvent("onSuccessResponse", function(req, res) {
    // 	if(res.success && (req.source === "newFolder" || req.source.indexOf("folder") != -1)){
    // 		switch(req.action){
    // 			case "rename":
    // 				webix.message("Folder renamed");
    // 				break;
    // 			case "create":
    // 				webix.message("Folder Created");
    // 				break;
    // 			case "remove":
    // 				webix.message("Folder deleted");
    // 				break;
    // 		}
    // 	}
    // });

    /*******************************Custom Events End****************************/
    // });
    fab.initActionButtons({
      type: "experimentList",
      actionButtonTypes: webhookListConfig.fab.actionButtonTypes,
      primary: webhookListConfig.fab.primary,
    });

    $(window).on('resize', function(){
      // console.log($$('webixFilemanagerWebhook'));
      $$(config.currentElementID).define('width', $('body').width() - $('.sidebar').width() - 10);
      $$(config.currentElementID).resize();
      if($$('wizardPlacementSectionIdExperimentPagesList')){
        $$('wizardPlacementSectionIdExperimentPagesList').define('width', $('body').width() - $('.sidebar').width() - 10);
        $$('wizardPlacementSectionIdExperimentPagesList').resize();
      }
      if($$('placementStepExperiment')){
        $$('placementStepExperiment').define('width', $('body').width() - $('.sidebar').width() - 10);
        $$('placementStepExperiment').resize();
      }

    });
  }


  global.openLayoutWebhook = openLayoutWebhook;
  global.webhookListConfig = webhookListConfig;
  global.page = webhookListConfig.page;
  global.placementWindowIframe = undefined;
})(window);
// insert postmessageListener ;
if (window.addEventListener) {
  addEventListener("message", webhookListConfig.postMessageListener, false);
} else {
  attachEvent("onmessage", webhookListConfig.postMessageListener);
}
// filter segments only
function filterFilesExperiment(data) {
  data.blockEvent();
  data.filter(function(item) {
    return item.type != "folder"
  });
  data.unblockEvent();
}
