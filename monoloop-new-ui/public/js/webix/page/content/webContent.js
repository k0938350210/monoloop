"use strict";
(function(global){
  var msg; // default variable for user messages

  var webContentListConfig = {
    folderManager: false,
    msg: "",
    apis: {
      show: monolop_api_base_url+"/api/content/web-content/show",
      list: monolop_api_base_url+"/api/content/web-content",
      create: monolop_api_base_url+"/api/content/web-content/create",
      update: monolop_api_base_url+"/api/content/web-content/update",
      update_status: monolop_api_base_url+"/api/content/web-content/update-status",
      change_folder: monolop_api_base_url+"/api/content/web-content/change-folder",
      delete: monolop_api_base_url+"/api/content/web-content/delete",
    },
    formElement: {
      id: 'pageElementIdWebContentList',
    },
    allFormData: {
      basic: false,
      pw: false,
      confirm: false,
    },
    placementIframeSrc: "/component/placement-window",
    defaultSteps: [],
    steps: [],
    activeStep: "",
    stepsGenerated: {
      basic: false,
      pw: false,
      confirm: false
    },
    containerId: "innerContent",

    deleteFile: function (id) {

      webix.confirm({
        text: "Do you want to delete?",
        ok: "Yes",
        cancel: "No",
        callback: function(result) {
          if (result) {
            webix.ajax().post(webContentListConfig.apis.delete, {
              _id: id
            }, {
              error: function(text, data, XmlHttpRequest) {
                alert("error");
              },
              success: function(text, data, XmlHttpRequest) {
                var response = JSON.parse(text);
                if (response.success == true) {
                  webContentListConfig.folderManager.deleteFile(id);
                }
              }
            });
          }
        }
      });


    },
    updateHidden: function (params) {
      // var params = {source: id, action: action, hidden: hidden};
      if (params.hidden == 1) {
        var message = webix.message("deactivating...");
      } else if (params.hidden == 0) {
        var message = webix.message("activating...");
      }

      webix.ajax().post(webContentListConfig.apis.update_status, params, {
        error: function(text, data, XmlHttpRequest) {
          alert("error");
        },
        success: function(text, data, XmlHttpRequest) {
          var response = JSON.parse(text);
          if (response.success == true && params.action == "update_hidden") {
            var element = document.getElementById("updateHidden_" + params._id);
            if (response.contentElement.hidden == "0") {
              element.innerHTML = "active";
              element.style.color = "green";
              element.onclick = function() {
                params.hidden = 1;
                webContentListConfig.updateHidden(params);
              };
            } else if (response.contentElement.hidden == "1") {
              element.innerHTML = "inactive";
              element.style.color = "red";
              element.onclick = function() {
                params.hidden = 0;
                webContentListConfig.updateHidden(params);
              };
            }
            webContentListConfig.refreshManager();
            webix.message.hide(message);
          }
        }
      });
    },
    refreshManager: function () {
      webContentListConfig.folderManager.clearAll();
      webContentListConfig.folderManager.load(webContentListConfig.apis.list);
    }
  };

  function openLayoutWebContent()
  {
    $("#innerContent").html("");

    var formElement = document.createElement('div');
    formElement.id = webContentListConfig.formElement.id;
    formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
    document.getElementById("innerContent").appendChild(formElement);

    // webix.ready(function() {

      webContentListConfig.folderManager = webix.ui({
        container: webContentListConfig.formElement.id,
        view: "filemanager",
        url: webContentListConfig.apis.list, // loading data from the URL
        id: "webixFilemanagerWebContentList",
        filterMode:{
          showSubItems:false,
          openParents:false
        },
        mode: "table", // specify mode selected by default
        modes: ["files", "table", "custom"], // all available modes including a new mode
        // save and handle all the menu actions from here,
        // disable editing on double-click
        handlers: {
          "upload": webContentListConfig.apis.create,
          // "download": "data/saving.php",
          // "copy": "data/saving.php",
          "move": monolop_api_base_url+"/api/folders/move",
          "remove": monolop_api_base_url+"/api/folders/delete",
          "rename": monolop_api_base_url+"/api/folders/update",
          "create": monolop_api_base_url+"/api/folders/create"
        },
        structure: {
          // specify the view of the new mode
          "custom": {
            view: "list",
            template: function(obj, common) {
              return common.templateIcon(obj, common) + obj.value;
            },
            select: "multiselect",
            editable: false,
            editaction: false,
            editor: "text",
            editValue: "value",
            drag: true,
            navigation: false,
            tabFocus: false,
            onContext: {}
          }
        },
        on: {
          "onViewInit": function(name, config) {
            if (name == "table" || name == "files") {
              // disable multi-selection for "table" and "files" views
              config.select = true;

              if (name == "table") {
                // disable editing on double-click
                config.editaction = false;
                // an array with columns configuration
                var columns = config.columns;
                //  disabling columns date, type, size
                columns.splice(1, 3);

                // configuration of a new column description
                var descriptionColumn = {
                  id: "descriptionColumn",
                  header: "Description",
                  fillspace: 2,
                  template: function(obj, common) {
                    return obj.description || ""; // "description" property of files
                  }
                };
                // configuration of a new column date
                var dateColumn = {
                  id: "dateColumn",
                  header: "Date",
                  fillspace: 2,
                  template: function(obj, common) {
                    return obj.date || ""; // "description" property of files
                  }
                };
                // configuration of a new column actions
                var actionsColumn = {
                  id: "actionsColumn",
                  header: "Actions",
                  fillspace: 1,
                  template: function(obj, common) {

                    if(typeof(obj.condition) === undefined || obj.condition === null){
                      obj.condition = '';
                    }

                    var params = {
                      source: 'segment',
                      id: obj.id
                    };
                    return '<a webix_l_id="remove" title="delete" onclick="webContentListConfig.deleteFile(\'' + obj.id + '\');" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
                  }
                };
                // configuration of a new column status
                var statusColumn = {
                  id: "statusColumn",
                  header: "Status",
                  fillspace: 1,
                  template: function(obj, common) {
                    var obj_ = {
                      action: "update_hidden",
                      hidden: "1"
                    };
                    var params = {
                      _id: obj.id,
                      action: "update_hidden",
                      node: this
                    };
                    if (obj.hidden == 0) {
                      params.hidden = 1;
                      return '<a id="updateHidden_' + obj.id + '"  onclick=\'webContentListConfig.updateHidden( ' + JSON.stringify(params) + ');\' class="webix_list_item status status0" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actions" property of files ;
                    }
                    params.hidden = 0;
                    return '<a id="updateHidden_' + obj.id + '"  onclick=\'webContentListConfig.updateHidden(' + JSON.stringify(params) + ');\' class="webix_list_item status status1" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
                  }
                };
                // insert columns
                webix.toArray(columns).insertAt(descriptionColumn, 1);
                webix.toArray(columns).insertAt(dateColumn, 2);
                webix.toArray(columns).insertAt(actionsColumn, 3);
                webix.toArray(columns).insertAt(statusColumn, 4);
              }

            }
          }
        }
      });

      /*******************************Menu Customization******************************/

      // updating options from menu
      var actions = $$("webixFilemanagerWebContentList").getMenu();
      actions.clearAll();
      var newData = [

        {
          id: "create",
          method: "createFolder",
          icon: "folder-o",
          value: webix.i18n.filemanager.create // "Create Folder"
        }, {
          id: "deleteFile",
          method: "deleteFile",
          icon: "times",
          value: webix.i18n.filemanager.remove // "Delete"
        }, {
          id: "edit",
          method: "editFile",
          icon: "edit",
          value: webix.i18n.filemanager.rename // "Rename"
        }
      ];
      actions.parse(newData);

      /*******************************Segment Add**********************************/

      actions.attachEvent("onItemClick", function(id) {
        this.hide();
      });

      actions.getItem("deleteFile").batch = "item, root";
      actions.getItem("edit").batch = "item, root";

      /******************************Segment Add End*******************************/


      /*****************************Menu Customization End*************************/


      /*******************************Custom Events********************************/

      /********************************Segment Edit********************************/

      // before editing file
      $$("webixFilemanagerWebContentList").attachEvent("onBeforeEditFile", function(id) {

        var srcTypeId = id.split('__');

        if (srcTypeId[0] == 'folder') {
          return true;
        }
        return false;
      });

      /*****************************Segment edit End*******************************/

      // reload grid after folder creation
      $$("webixFilemanagerWebContentList").attachEvent("onAfterCreateFolder", function(id) {
        // webContentListConfig.refreshManager();
        openLayoutWebContent();
        setTimeout(function(){ openLayoutWebContent(); }, 500);
        return false;
      });


      // it will be triggered before deletion of file
      $$("webixFilemanagerWebContentList").attachEvent("onBeforeDeleteFile", function(ids) {
        if (ids.indexOf('webcontentlist__') === -1) {
          return true
        }
        webContentListConfig.deleteFile(ids);
        return false;
      });

      // it will be triggered before dragging the folder/segment
      $$("webixFilemanagerWebContentList").attachEvent("onBeforeDrag", function(context, ev) {
        msg = webix.message("copying...");
        return true;
      });
      $$("webixFilemanagerWebContentList").attachEvent("onBeforeDrop", function(context, ev) {
        if (context.start.indexOf('webcontentlist__') === -1) {
          return true
        }
        webix.ajax().post(webContentListConfig.apis.change_folder, {
          _id: context.start,
          target: context.target
        }, {
          error: function(text, data, XmlHttpRequest) {
            alert("error");
          },
          success: function(text, data, XmlHttpRequest) {
            var response = JSON.parse(text);
            if (response.success == true) {}
          }
        });
        return true;
      });
      // it will be triggered after dropping the folder to the destination
      $$("webixFilemanagerWebContentList").attachEvent("onAfterDrop", function(context, ev) {
        webix.message.hide(msg);
        return true;
      });

      $$("webixFilemanagerWebContentList").$$("files").attachEvent("onBeforeRender", filterFiles);
      $$("webixFilemanagerWebContentList").$$("table").attachEvent("onBeforeRender", filterFiles);

      /*******************************Custom Events End****************************/
    // });
  }


  // filter segments only
  function filterFiles(data){
     data.blockEvent();
     data.filter(function(item){
      return item.type != "folder"
     });
     data.unblockEvent();
  }


  function getAsUriParameters(data) {
     var url = '';
     for (var prop in data) {
        url += encodeURIComponent(prop) + '=' +
            encodeURIComponent(data[prop]) + '&';
     }
     return url.substring(0, url.length - 1)
  }

  global.openLayoutWebContent = openLayoutWebContent;
  global.webContentListConfig = webContentListConfig;
  global.placementWindowIframe = undefined;
})(window);
