function validateEmail(email){
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
var _form = [
			{ view:"text", label:'Email', name:"email"},
			// { view:"text", label:'Confirm email', name:"confirmEmail"},
			{ view:"text",type: "password", label:'Password', name:"password"},
			{ view:"text", label:'Name', name:"name"},
      { view:"button", value: "Save and sign in", id: "createNewAccountSbbtn", align:"right", width: 200, click:function(){
          var form = this.getParentView();
          if (form.validate()){
						var formValues = form.getValues();
						formValues.ajax = true;
            webix.ajax().post(inviteOptions.signinUrl, formValues, function(res, data, xhr){
              res  = JSON.parse(res);
              if(res.success === true){

                // $$('openSignInFormlayoutPopup').close();
								webix.alert("Successfully login, click ok to go to dashboard", function(result){
									webix.send('/', null, 'GET');
								});
              } else {
                webix.message({type:"error", text:res.msg});
              }
            });


          }
        }}
		];
var _formRules = {
  "email": webix.rules.isNotEmpty,
	"email": webix.rules.isEmail,
  "password": webix.rules.isNotEmpty,
	"name": webix.rules.isNotEmpty,
};

function openSignInFormlayout(){
  webix.ui({
    view: "window",
    id: "openSignInFormlayoutPopup",
    modal: true,
    position: "center",
    height: 450,
    width: 400,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "Sign in"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('openSignInFormlayoutPopup').close();"
      }]
    },
    body: {
      view: "form",
      id: "__signinForm",
      height: 600,
      width: 400,
      complexData: true,
      elements: _form,
      rules: _formRules,
      margin:4,
      elementsConfig:{
        labelPosition: "top",
        labelWidth: 140,
        bottomPadding: 18
      }
    }
  }).show();

  if(user !== undefined) {
    $$("__signinForm").setValues({
			name: inviteOptions.name,
			email: inviteOptions.email,
			accountId: inviteOptions.accountId,
			accountUserId: inviteOptions.accountUserId
    });
  }
}
