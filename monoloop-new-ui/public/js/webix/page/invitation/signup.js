function validateEmail(email){
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

var _form = [
			{ view:"text", label:'Email', name:"email", id:'email', disabled:true},
			{ view:"text", label:'Confirm email', name:"confirmemail", id:'confirmemail', disabled:true},
			{ view:"text",type: "password", label:'Password', name:"password", id:'password'},
      { view:"text",type: "password", label:'Confirm Password', name:"confirmpassword", id:'confirmpassword'},
			{ view:"text", label:'Full name', name:"firstname", id:'name'},
      { view:"button", value: "Create", id: "createNewAccountSbbtn", align:"right", width: 150, click:function(){
          var form = this.getParentView();

          if (form.validate()){
          	$$('createNewAccountSbbtn').disable();
						var formValues = form.getValues();
						formValues.ajax = true;
            webix.ajax().post(inviteOptions.signupUrl, formValues, function(res, data, xhr){
            	if(xhr.status == 200){
            		location.reload();
            	}else {
                webix.message({type:"error", text:'Something went wrong'});
                $$('createNewAccountSbbtn').enable();
              }

            	/*
              res  = JSON.parse(res);
              if(res.success === true){

                if(res.authenticated === true){
									webix.alert("Successfully registered, click ok to go to dashboard", function(result){
										webix.send('/', null, 'GET');
									});
                } else {
                    webix.message({type:"error", text: "Account couldn't authenticated"});
                }

                $$('openSignUpFormlayoutPopup').close();
              } else {
                webix.message({type:"error", text:res.msg});
              }
              */
            });
          }else{
          	 webix.message({type:"error", text:'Invalid'});
          }
        }}
		];
var _formRules = {
	"password": function(value){
			value = value.trim();
			if(value === ''){
				 $$("__signupForm").elements.email.define("invalidMessage", "Password can not be empty.");
				 return false;
			}
			if(value !== $$("confirmpassword").getValue()){
				$$("__signupForm").elements.email.define("invalidMessage", "Password does not match with confirm password.");
				return false
			}
		 return true;
	 },
	"confirmpassword": function(value){
		value = value.trim();
		if(value === ''){
			 $$("__signupForm").elements.confirmpassword.define("invalidMessage", "Confirm password can not be empty.");
			 return false;
		}
		if(value !== $$("password").getValue()){
			$$("__signupForm").elements.confirmpassword.define("invalidMessage", "Confirm password does not match with password.");
			return false
		}
	 return true;
	},
	"firstname": webix.rules.isNotEmpty
};

function openSignUpFormlayout(){
  webix.ui({
    view: "window",
    id: "openSignUpFormlayoutPopup",
    modal: true,
    position: "center",
    height: 550,
    width: 400,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "New Account"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('openSignUpFormlayoutPopup').close();"
      }]
    },
    body: {
      view: "form",
      id: "__signupForm",
      height: 600,
      width: 400,
      complexData: true,
      elements: _form,
      rules: _formRules,
      margin:4,
      elementsConfig:{
        labelPosition: "top",
        labelWidth: 140,
        bottomPadding: 18
      }
    }
  }).show();

  if(user !== undefined) {
    $$("__signupForm").setValues({
      firstname: inviteOptions.name,
      lastname: '',
      email: inviteOptions.email,
			confirmemail: inviteOptions.email,
      accountId: inviteOptions.accountId,
      accountUserId: inviteOptions.accountUserId,
      user_type: 'enterprise'
    });
  }

}
