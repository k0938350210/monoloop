
function openSignUpForm(){
  $.getScript('/js/webix/page/signup.js', function() {
    openSignUpFormlayout();
  });
}
function openSignInForm(){
  $.getScript('/js/webix/page/signin.js', function() {
    
    openSignInFormlayout();
  });
}
function openLayout(){
  webix.ready(function() {
      webix.ui({
        container:"mailContainer", //corresponds to the ID of the div block
        height: 40,
        cols:[
            { view:"button", value: "Sign up", id: "signupaccount", align:"right", width: 150, click:function(){
              openSignUpForm();
            }},
            { view:"button", value: "Sign in", id: "signinaccount", align:"right", width: 150, click:function(){
              openSignInForm();
            }},
        ]
    });
});
}

openLayout();
