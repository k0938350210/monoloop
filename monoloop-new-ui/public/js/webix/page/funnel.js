var folderFileManagerFunnel; // file manager variable
var segment_listing = [];
var funnel_obj = {
  id: null,
  name: '',
  description: '',
  items: [],

  mode: function(){
    if(this.id){
      return 'edit';
    }
    return 'add';
  },

  reset: function(){
    this.id = null;
    this.name = '';
    this.description = '';
    this.items = [];
  },

  load: function(data){
    this.id = data._id;
    this.name = data.name;
    this.description = data.description;
    this.items = data.items;
  }
}



var extendedFab = {
  actionButtonTypes: {
    type: "create",
    setType: function(t){
      t = t || "create";
      extendedFab.actionButtonTypes.type = t;
    },
    fabOptions: false,
    create: {
      options: [
        // {
        // 	label: "This is to test",
        // 	className: "segmentBuilder inline",
        //   callback: function(){
        //     webix.alert("This is to test......");
        //   }
        // }
      ]
    },
  },
  fabOptions: false,
  primary: {
    primaryBtnLabelCallback: function(actionButton, floatingTextBG){
      actionButton.className += " createtracker inline";
      floatingTextBG.textContent = "Create funnel";
      actionButton.onclick = function(){
        config.currentElementID = 'webixWizardHeaderMenuFunnel';
        openWizardFunnel('add');
      };
    },
  }
};

function openLayoutFunnel() {
  var params = getQueryParams(document.location + '');
  var f = window.location.hash;
  window.history.pushState(config.funnel.headerName, config.funnel.headerName, config.funnel.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);

  $("#innerContent").html("");

  var formElement = document.createElement('div');
  formElement.id = 'fileManagerParentIdFunnel';
  formElement.style.minHeight = config.styles.defaultHeight.getFileManagerHeight() + 'px';
  document.getElementById("innerContent").appendChild(formElement);

  // webix.ready(function() {

  folderFileManagerFunnel = webix.ui({
    container: "fileManagerParentIdFunnel",
    view: "filemanager",
    url: monolop_api_base_url+ "/api/funnels?ajax=true" + (params.url ? ('&url=' + params.url) : ''), // loading data from the URL
    id: "webixFilemanagerFunnel",
    // height: 500,
    filterMode: {
      showSubItems: true,
      openParents: true
    },
    mode: "table", // specify modeFunnel selected by default
    modes: ["files", "table"], // all available modes including a new modeFunnel
    // save and handle all the menu actionsFunnel from here,
    // disable editing on double-click

    handlers: {
      "upload": monolop_api_base_url+"/api/funnels/create",
      "move":  monolop_api_base_url+"/api/folders/move",
      "remove": monolop_api_base_url+"/api/folders/delete",
      "rename": monolop_api_base_url+"/api/folders/update",
      "create": monolop_api_base_url+"/api/folders/create",
      "files": monolop_api_base_url+ "/api/funnels/folder?ajax=true" + (params.url ? ('&url=' + params.url) : '')
    },
    structure: {

    },
    on: {
      "onViewInit": function(name, config) {
        if (name == "table" || name == "files") {
          // disable multi-selection for "table" and "files" views
          config.select = true;

          if (name == "table") {
            // disable editing on double-click
            config.editaction = false;
            // an array with columns configuration
            var columns = config.columns;
            //  disabling columns date, type, size
            columns.splice(1, 3);

            // configuration of a new column description
            var descriptionColumnFunnel = {
              id: "descriptionColumnFunnel",
              header: "Description",
              fillspace: 3,
              template: function(obj, common) {
                return obj.description || ""; // "description" property of files
              }
            };
            // configuration of a new column date
            var dateColumnFunnel = {
              id: "dateColumnFunnel",
              header: "Date",
              fillspace: 2,
              sort: function(a, b){
                var _a = new Date(a.date), _b = new Date(b.date);
                return (_a > _b ? 1 : (_b > _a ? -1: 0));
              },
              template: function(obj, common) {
                return obj.date || ""; // "description" property of files
              }
            };

            // configuration of a new column status
            var statusColumnTracker = {
              id: "statusColumnTracker",
              header: "Status",
              css: "statusCol",
              fillspace: 1,
              sort: function(a, b){
                var _a = Number(a.hidden), _b = Number(b.hidden);
                return (_a > _b ? 1 : (_b > _a ? -1: 0));
              },
              template: function(obj, common) {
                var obj_ = {
                  action: "update_hidden",
                  hidden: "1"
                };
                var paramsTracker = {
                  _id: obj.id,
                  action: "update_hidden",
                  node: this
                };
                if (obj.hidden == 0) {
                  paramsTracker.hidden = 1;
                  return '<a title="Deactive tracker" id="updateHiddenTracker' + obj.id + '"  onclick=\'updateHiddenTracker( ' + JSON.stringify(paramsTracker) + ');\' class="webix_list_item updateTrackerStatus status status0" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actionsTracker" property of files ;
                }
                paramsTracker.hidden = 0;
                return '<a title="Activate tracker" id="updateHiddenTracker' + obj.id + '"  onclick=\'updateHiddenTracker(' + JSON.stringify(paramsTracker) + ');\' class="webix_list_item updateTrackerStatus status status1" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
              }
            };
            var actionsColumnTracker = {
              id: "actionsColumnTracker",
              header: "Actions",
              fillspace: 1,
              template: function(obj, common) {
                var funnel_id = obj.id.split('__')[1];
                var edit = '<a  title="Edit" onclick="updateFunnel(\'' + obj.id + '\');" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' ;
                var del = '<a webix_l_id="remove" title="delete" onclick="deleteFunnel(\'' + obj.id + '\');" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
                var url  = '/reports/funnel/detail/'+ funnel_id + ($('body').hasClass('hidenav') ? '?nav=1' : '') + (params.url ? ('&url=' + encodeURI(params.url)) : '');
                var report =  '<a webix_l_id="remove" title="Funnel report" onclick="window.location = \'' + url + '\';" class="webix_list_item viewReport" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-bar-chart"></span></a>';
                if(obj.system != 1){
                  return edit + '|' + del + '|' + report;
                }
                return edit + '|' + report;
              }
            };
            // insert columns
            // webix.toArray(columns).insertAt(descriptionColumnTracker, 1);
            // webix.toArray(columns).insertAt(dateColumnTracker, 2);
            // webix.toArray(columns).insertAt(reportColumnTracker, 3);
            // webix.toArray(columns).insertAt(actionsColumnTracker, 4);
            webix.toArray(columns).insertAt(descriptionColumnFunnel, 1);
            webix.toArray(columns).insertAt(actionsColumnTracker, 2);
            // webix.toArray(columns).insertAt(statusColumnTracker, 3);

          }

        }
      }
    }
  });

  /*******************************Menu Customization******************************/
  // only FILES mode should be available
  // updating options from menu
  var actionsFunnel = $$("webixFilemanagerFunnel").getMenu();
  actionsFunnel.clearAll();
  var newDataFunnel = [

    {
      id: "create",
      method: "createFolder",
      icon: "folder-o",
      value: webix.i18n.filemanager.create // "Create Folder"
    }, {
      id: "deleteFile",
      method: "deleteFile",
      icon: "times",
      value: webix.i18n.filemanager.remove // "Delete"
    }, {
      id: "edit",
      method: "editFile",
      icon: "edit",
      value: webix.i18n.filemanager.rename // "Rename"
    }
  ];
  actionsFunnel.parse(newDataFunnel);
  // add new option for the menu to add new segment
  actionsFunnel.add({
    id: "createFunnel",
    icon: "file",
    value: "Create Funnel"
  });

  actionsFunnel.getItem("create").batch = "item, root";
  actionsFunnel.getItem("deleteFile").batch = "item, root";
  actionsFunnel.getItem("edit").batch = "item, root";
  actionsFunnel.getItem("createFunnel").batch = "item, root";

  actionsFunnel.attachEvent("onItemClick", function(id) {
    this.hide();
    // check if the action is createTracker
    if (id == "createFunnel") {
      openWizardFunnel('add');
    }
  });

  // reload grid after folder creation
  $$("webixFilemanagerFunnel").attachEvent("onAfterCreateFolder", function(id) {
    setTimeout(function(){ openLayoutFunnel(); }, 500);
    return true;
  });


  // it will be triggered before deletion of file
  $$("webixFilemanagerFunnel").attachEvent("onBeforeDeleteFile", function(ids) {
    // deleteSegment(ids);
    if (ids.indexOf('funnel__') === -1) {
      return true;
    }

    deleteFunnel(ids);
    return false;
  });

  // it will be triggered before dragging the folder/tracker
  $$("webixFilemanagerFunnel").attachEvent("onBeforeDrag", function(context, ev) {
    return true;
  });

  $$("webixFilemanagerFunnel").attachEvent("onBeforeDrop", function(context, ev) {
    if (context.start.indexOf('funnel__') === -1) {
      return true
    }
    webix.ajax().post(monolop_api_base_url + "/api/funnels/change-folder", {
      _id: context.start,
      target: context.target
    }, {
      error: function(text, data, XmlHttpRequest) {
        alert("error");
      },
      success: function(text, data, XmlHttpRequest) {
        var response = JSON.parse(text);
        if (response.success == true) {}
      }
    });
    return true;
  });
  // it will be triggered after dropping the folder to the destination
  $$("webixFilemanagerFunnel").attachEvent("onAfterDrop", function(context, ev) {
    webix.message("Funnel has been moved to new folder.");
    return false;
  });

  $$("webixFilemanagerFunnel").$$("files").attachEvent("onBeforeRender", filterFilesFunnel);
  $$("webixFilemanagerFunnel").$$("table").attachEvent("onBeforeRender", filterFilesFunnel);

  var _exe = false;
  $$("webixFilemanagerFunnel").attachEvent("onAfterLoad", function(context, ev) {
    if(_exe === false){
      _exe = true;
      initIntialTTandGuides();
    }

  });

  fab.initActionButtons({
    type: "funnelList",
    actionButtonTypes: extendedFab.actionButtonTypes,
    primary: extendedFab.primary
  });
}

function filterFilesFunnel(data) {
  data.blockEvent();
  data.filter(function(item) {
    return item.type != "folder"
  });
  data.unblockEvent();
}

function openWizardFunnel(method){
  if($('#floatingContainer').length > 0){
    $('#floatingContainer').remove();
  }

  if(method === 'add'){
    funnel_obj.reset();
  }

  var f = window.location.hash;
  if(funnel_obj.id === null){
    window.history.pushState(config.addfunnel.headerName, config.addfunnel.headerName, config.addfunnel.link + ($('body').hasClass('hidenav') ? '?nav=1' : '') + f);
  } else {
    window.history.pushState(config.editfunnel.headerName, config.editfunnel.headerName, config.editfunnel.link + '?id=' + funnel_obj.id + ($('body').hasClass('hidenav') ? '&nav=1' : '') + f);
  }

  var containerId = "innerContent";
  $("#"+containerId).html("");

  var wizardFunnel = document.createElement('div');
  wizardFunnel.id = 'webixWizardParentIdFunnel';
  wizardFunnel.style.minHeight = '680px';
  document.getElementById(containerId).appendChild(wizardFunnel);

  var label = '';
  if(funnel_obj.id === null){
    label = 'Add new funnel';
  }else{
    label = 'Edit funnel';
  }

  var singleStip = {
    view: "button",
    label: label,
    css: "wizardStepBtn Basic",
    id: "wizardBasicBtnTracker",
    width: 270,
    click: function() {
    }
  };

  webix.ui({
    container: "webixWizardParentIdFunnel",
    height: 680,
    id: "webixWizardHeaderMenuFunnel",
    rows: [{
      view: "toolbar",
      paddingY: 1,
      height: 50,
      id: "webixWizardHeaderMenuToolbarFunnel",
      hidden: false,
      css: "webixWizardHeader",
      elements: [{
          gravity: 1
        }, singleStip, {
          gravity: 1
        }

      ]
    }, {
      template: "<div id='wizardSectionFunnel'></div>"
    }]
  });

  var _form = [{
    css: 'funnel-form',
    view: "layout",
    rows: [
    {
      cols:[{
        view: 'text',
        id: "funnal-name",
        label: 'Name',
        name: "name",
        value: funnel_obj.name,
        gravity:0.4
      },{
        gravity:0.05
      },{
        view: 'text',
        label: 'Description',
        name: 'description',
        value: funnel_obj.description
      }]
    }]
  },{
    view:"label",
    label: "Select minimum of two audiences that you would like to track on this funnel"
  },{
    view: 'layout',
    elementsConfig: {
      labelPosition: "left"
    },
    id: 'item-container',
    rows:[/*{
      cols:[{
        view:"richselect",
        value:1, options:[
          { "id":1, "value":"Banana"},
          { "id":2, "value":"Papaya"},
          { "id":3, "value":"Apple"}
        ],
        gravity:0.1
      },{
        view: "template",
        css: 'funnel-link-step',
        template: '&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;&nbsp;',
        borderless: true,
        width: 30
      },{
        view:"richselect",
        value:1, options:[
          { "id":1, "value":"Banana"},
          { "id":2, "value":"Papaya"},
          { "id":3, "value":"Apple"}
        ],
        gravity:0.1
      },{
        view: "template",
        css: 'funnel-link-step',
        template: '&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;&nbsp;',
        borderless: true,
        width: 30
      },{
        view:"richselect",
        value:1, options:[
          { "id":1, "value":"Banana"},
          { "id":2, "value":"Papaya"},
          { "id":3, "value":"Apple"}
        ],
        gravity:0.1
      },{
        view: "template",
        css: 'funnel-link-step',
        template: '&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;&nbsp;',
        borderless: true,
        width: 30
      }]
    }*/]
  },{
    view: 'template'
  }, {
    cols: [{}, {
        view: "button",
        value: "Save",
        css: "orangeBtn",
        click: function() {
          var btnCtrl = this;
          btnCtrl.disable();

          var url;
          url = monolop_api_base_url+ '/api/funnels/create';
          if (funnel_obj.mode() === 'edit') {
            url = monolop_api_base_url+ '/api/funnels/update'
          }
          // adding parent folder id


          //console.log(formValues);
          var form_basic = $$("basicFormFunnel");

          if(!form_basic.validate()) { btnCtrl.enable();return; }

          var basic_config = form_basic.getValues();
          basic_config['ajax'] = true;
          var current_folder = folderFileManagerFunnel ? folderFileManagerFunnel.getCurrentFolder() : window.location.hash.substr(3);
          basic_config['folder_id'] = current_folder;
          basic_config['id'] = funnel_obj.id;
          webix.ajax().post(url,basic_config,{
            error: function(text, data, XmlHttpRequest) {
              btnCtrl.enable();
              alert("error");
            },
            success: function(text, data, XmlHttpRequest) {
              btnCtrl.enable();
              var response = JSON.parse(text);
              if (response.success == true) {
                config.currentElementID = 'webixFilemanagerFunnel';
                openLayoutFunnel();
                webix.message(response.msg);
              }
            }
          });
        }
      },{},

    ]
  }];

  var __formRules = {
    "name": webix.rules.isNotEmpty
  };


  webix.ui({
    container: "wizardSectionFunnel",
    id: "basicStepFunnel",
    rows: [{
      cols: [{}, {
        view: "form",
        id: "basicFormFunnel",
        complexData: true,
        elements: _form,
        rules: __formRules,
        width: 980,
        borderless: true,
        margin: 3,
        elementsConfig: {
          labelPosition: "top",
          labelWidth: 140,
          bottomPadding: 18
        },
      }, {}]
    }]
  });

  var segmentURL = monolop_api_base_url + "/data/experiments/segment";

  webix.ajax().get(segmentURL, {ajax: true}, function(resSegments, xml, xhr) {
    resSegments = JSON.parse(resSegments);
    for(var i = 0; i < resSegments.segments.length; i++){
      var segment = resSegments.segments[i];
      var value =  '<b>' + segment.name + '</b>';
      if(segment.description){
        value = value + ' - ' + segment.description.substr(0, 30);
      }
      segment_listing.push({
        id: segment.uid,
        value: value
      });
    }



    renderItems();

  });
}

function renderItems(){
  if(funnel_obj.items.length < 2){
    var l = funnel_obj.items.length;
    for(var i = 0 ; i < ( 2 - l) ; i++){
      funnel_obj.items.push({
        type: 'audience',
        value: 0
      });
    }
  }

  var total_item = funnel_obj.items.length;

  var total_row = Math.floor(funnel_obj.items.length / 3 ) ;

  for(var i = 0 ; i <= total_row ; i++){
    $$('item-container').addView({
      id: 'item-container-' + ( i + 1 ),
      cols: []
    });

    for(j = 0 ; j < 3 ; j++){
      if(3*i + j + 1 > total_item){
        break;
      }

      var id = 'item-container-' + ( i + 1 );
      $$(id).addView({
        view:"richselect",
        value: funnel_obj.items[3*i + j].value,
        options:segment_listing,
        width: 285,
        name:'items['+(3*i + j)+'][value]'
      });

      $$(id).addView({
        view: "template",
        css: 'funnel-link-step',
        template: '&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;&nbsp;',
        borderless: true,
        width: 30
      });
    }
  }

  addAnotherAudienceBtn();
}

function addNewSelectAudience(){

  var total_row = Math.floor(funnel_obj.items.length / 3 );
  var remain = funnel_obj.items.length % 3;
  var id = 'item-container-' + (total_row + 1);

  $$(id).addView({
    view:"richselect",
    value: 0,
    options:segment_listing,
    width: 285,
    name:'items['+(funnel_obj.items.length )+'][value]'
  });

  $$(id).addView({
    view: "template",
    css: 'funnel-link-step',
    template: '&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;&nbsp;',
    borderless: true,
    width: 30
  });

  funnel_obj.items.push({
    type: 'audience',
    value: 0
  });

  addAnotherAudienceBtn();
}



function addAnotherAudienceBtn(){
  var total_row = Math.floor(funnel_obj.items.length / 3 );
  var remain = funnel_obj.items.length % 3;
  var id = 'item-container-' + (total_row + 1);
  if( $$(id) === undefined){
    $$('item-container').addView({
      id: id,
      cols: []
    });
  }

  if(funnel_obj.items.length > 2){

    $$(id).addView({
      id: 'add-another-audience-btn',
      cols:[{
        view: "button",
        value: ' - ',
        width: 140,
        click: function(){
          removeAnotherAudience2Btn();
        }
      },{
        view: "button",
        value: ' + ',
        width: 140,
        click: function(){
          removeAnotherAudienceBtn();
        }
      }]
    });

  }else{

    $$(id).addView({
      id: 'add-another-audience-btn',
      view: "button",
      value: ' + ',
      width: 285,
      click: function(){
        removeAnotherAudienceBtn();
      }
    });
  }
}

function removeAnotherAudienceBtn(){
  var total_row = Math.floor(funnel_obj.items.length / 3 );
  var remain = funnel_obj.items.length % 3;
  var id = 'item-container-' + (total_row + 1);
  $$(id).removeView('add-another-audience-btn');

  addNewSelectAudience();
}

function removeAnotherAudience2Btn(){
  /*
  var total_row = Math.floor(funnel_obj.items.length / 3 );
  var remain = funnel_obj.items.length % 3;
  var id = 'item-container-' + (total_row + 1);
  $$(id).removeView('add-another-audience-btn');
  */

  var form_basic = $$("basicFormFunnel");
  var basic_config = form_basic.getValues();


  for(var i = 0 ; i < funnel_obj.items.length ; i++){
    funnel_obj.items[i].value = basic_config['items['+i+'][value]'];
  }

  var childs = $$('item-container').getChildViews();
  var childs_total = childs.length;
  for(var i = 0 ; i < childs_total ; i++){
    $$('item-container').removeView('item-container-'+(i+1));
  }

  funnel_obj.items.pop();
  renderItems();
}


function updateFunnel(id, src) {
  config.currentElementID = 'webixWizardHeaderMenuTracker';
  var url = monolop_api_base_url+ '/api/funnels/show';

  // retreiving form data and setting into the form
  var formDataFunnel;
  webix.ajax().get(url, {
    _id: id,
    ajax: true
  }, function(text, xml, xhr) {
    //response
    formDataFunnel = JSON.parse(text);
    funnel_obj.load(formDataFunnel);
    openWizardFunnel('edit');
  });
}

function deleteFunnel(id) {
  webix.confirm({
    text: "Do you want to delete?",
    ok: "Yes",
    cancel: "No",
    callback: function(result) {
      if (result) {
        webix.ajax().post(monolop_api_base_url+ "/api/funnels/delete", {
          _id: id,
          ajax: true,
        }, {
          error: function(text, data, XmlHttpRequest) {
            alert("error");
          },
          success: function(text, data, XmlHttpRequest) {
            var response = JSON.parse(text);
            if (response.success == true) {
              folderFileManagerFunnel.deleteFile(id);
            }
          }
        });
      }
    }
  });

}


function refreshManagerTracker() {
  folderFileManagerFunnel.clearAll();
  folderFileManagerFunnel.load("/api/funnels");
}
