var folderFileManagerTracker; // file manager variable

var defaultStepsTracker;
var activeStepTracker;

var isBasicGeneratedTracker, isTrackerGeneratedTracker, isTrackerNameGeneratedTracker, isTrackerValueGeneratedTracker, isConfirmGeneratedTracker;

var extendedFab = {
  actionButtonTypes: {
    type: "create",
    setType: function(t){
      t = t || "create";
      extendedFab.actionButtonTypes.type = t;
    },
    fabOptions: false,
    create: {
      options: [
        // {
        // 	label: "This is to test",
        // 	className: "segmentBuilder inline",
        //   callback: function(){
        //     webix.alert("This is to test......");
        //   }
        // }
      ]
    },
  },
  fabOptions: false,
  primary: {
    primaryBtnLabelCallback: function(actionButton, floatingTextBG){
      actionButton.className += " createtracker inline";
      floatingTextBG.textContent = "Create tracker";
      actionButton.onclick = function(){
      };
    },
  }


};

var allFormDataTracker = {};

function openLayoutTracker() {

  $("#innerContent").html("");

  var formElement = document.createElement('div');
  formElement.id = 'fileManagerParentIdTracker';
  formElement.style.minHeight = '680px';
  document.getElementById("innerContent").appendChild(formElement);

  // webix.ready(function() {

  folderFileManagerTracker = webix.ui({
    container: "fileManagerParentIdTracker",
    view: "filemanager",
    url: "/api/trackers?ajax=true", // loading data from the URL
    id: "webixFilemanagerTracker",
    filterMode: {
      showSubItems: false,
      openParents: false
    },
    mode: "table", // specify mode selected by default
    modes: ["files", "table", "custom"], // all available modes including a new mode
    // save and handle all the menu actionsTracker from here,
    // disable editing on double-click
    handlers: {
      "upload": "/api/trackers/create",
      // "download": "data/saving.php",
      // "copy": "data/saving.php",
      "move": "/api/folders/move",
      "remove": "/api/folders/delete",
      "rename": "/api/folders/update",
      "create": "/api/folders/create",
      "files": "/api/trackers/folder?ajax=true",
    },
    structure: {

    },
    on: {
      "onViewInit": function(name, config) {
        if (name == "table" || name == "files") {
          // disable multi-selection for "table" and "files" views
          config.select = true;

          if (name == "table") {
            // disable editing on double-click
            config.editaction = false;
            // an array with columns configuration
            var columns = config.columns;
            //  disabling columns date, type, size
            columns.splice(1, 3);

            // configuration of a new column description
            var descriptionColumnTracker = {
              id: "descriptionColumnTracker",
              header: "Type",
              fillspace: 2,
              template: function(obj, common) {
                return obj.description || ""; // "description" property of files
              }
            };
            // configuration of a new column date
            var dateColumnTracker = {
              id: "dateColumnTracker",
              header: "Date",
              fillspace: 2,
              template: function(obj, common) {
                return obj.date || ""; // "description" property of files
              }
            };
            // configuration of a new column actionsTracker
            var actionsColumnTracker = {
              id: "actionsColumnTracker",
              header: "Actions",
              fillspace: 1,
              template: function(obj, common) {

                return '<a  title="Condition" onclick="updateTracker(\'' + obj.id + '\');" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-edit"></span></a>' + ' | ' + '<a webix_l_id="remove" title="delete" onclick="deleteTracker(\'' + obj.id + '\');" class="webix_list_item" style="width:auto; height:34px; overflow:hidden;"><span class="webix_icon fa-times"></span></a>';
              }
            };
            // configuration of a new column status
            var statusColumnTracker = {
              id: "statusColumnTracker",
              header: "Status",
              fillspace: 1,
              template: function(obj, common) {
                var obj_ = {
                  action: "update_hidden",
                  hidden: "1"
                };
                var params = {
                  _id: obj.id,
                  action: "update_hidden",
                  node: this
                };
                if (obj.hidden == 0) {
                  params.hidden = 1;
                  return '<a id="updateHiddenTracker' + obj.id + '"  onclick=\'updateHiddenTracker( ' + JSON.stringify(params) + ');\' class="webix_list_item status status0" style="color:green;width:auto; height:34px; overflow:hidden;">active</a>'; // "actionsTracker" property of files ;
                }
                params.hidden = 0;
                return '<a id="updateHiddenTracker' + obj.id + '"  onclick=\'updateHiddenTracker(' + JSON.stringify(params) + ');\' class="webix_list_item status status1" style="color:red;width:auto; height:34px; overflow:hidden;">inactive</a>'; // "status" property of files
              }
            };
            // insert columns
            webix.toArray(columns).insertAt(descriptionColumnTracker, 1);
            webix.toArray(columns).insertAt(dateColumnTracker, 2);
            webix.toArray(columns).insertAt(actionsColumnTracker, 3);
            webix.toArray(columns).insertAt(statusColumnTracker, 4);
          }

        }
      }
    }
  });

  /*******************************Menu Customization******************************/

  // updating options from menu
  var actionsTracker = $$("webixFilemanagerTracker").getMenu();
  actionsTracker.clearAll();
  var newDataTracker = [

    {
      id: "create",
      method: "createFolder",
      icon: "folder-o",
      value: webix.i18n.filemanager.create // "Create Folder"
    }, {
      id: "deleteFile",
      method: "deleteFile",
      icon: "times",
      value: webix.i18n.filemanager.remove // "Delete"
    }, {
      id: "edit",
      method: "editFile",
      icon: "edit",
      value: webix.i18n.filemanager.rename // "Rename"
    }
  ];
  actionsTracker.parse(newDataTracker);
  // add new option for the menu to add new segment
  actionsTracker.add({
    id: "createTracker",
    icon: "file",
    value: "Create Tracker"
  });

  actionsTracker.getItem("create").batch = "item, root";
  actionsTracker.getItem("deleteFile").batch = "item, root";
  actionsTracker.getItem("edit").batch = "item, root";
  actionsTracker.getItem("createTracker").batch = "item, root";

  /*******************************Segment Add**********************************/

  actionsTracker.attachEvent("onItemClick", function(id) {
    this.hide();
    // check if the action is createTracker
    if (id == "createTracker") {
      openWizardTracker('add');
    } else if (id == "createTracker") {
      // setting url
      var url = '/api/trackers/create';
      var pathIds = folderFileManagerTracker.getPath();
      var segmentForm = [{
        view: "text",
        id: "name",
        label: 'Name',
        name: "name",
        // invalidMessage: "Name can not be empty"
      }, {
        view: "textarea",
        id: "desc",
        label: 'Description',
        name: "desc",
        // invalidMessage: "Description can not be empty"
      }, {
        view: "button",
        id: "sub",
        name: "submit",
        value: "Save and Next",
        click: function() {
          var form = this.getParentView();
          // adding parent folder id
          form.setValues({
            source: folderFileManagerTracker.getCurrentFolder()
          }, true)
          if (form.validate()) {
            webix.ajax().post(url, form.getValues(), {
              error: function(text, data, XmlHttpRequest) {
                alert("error");
              },
              success: function(text, data, XmlHttpRequest) {
                var response = JSON.parse(text);
                if (response.success == true) {
                  $$("add_new_segment").hide();

                  var params = {
                    source: response.source,
                    id: response.content.id,
                    condition: response.content.condition,
                  };
                  // folderFileManagerTracker.refresh();


                  refreshManagerTracker();
                  openConditionalBuilder(params);
                  // webix.alert(response.msg);
                }
              }
            });

          }
        }
      }, {
        view: "button",
        id: "cancel",
        name: "submit",
        value: "cancel",
        click: function() {
          $$("add_new_segment").hide();
        }
      }];
      var segmentFormRules = {
        "name": webix.rules.isNotEmpty,
        "desc": webix.rules.isNotEmpty,
      };

      webix.ui({
        view: "window",
        id: "add_new_segment",
        modal: true,
        position: "center",
        height: 400,
        width: 400,
        head: {
          view: "toolbar",
          margin: -4,
          cols: [{
            view: "label",
            label: "New Segment"
          }, {
            view: "icon",
            icon: "times-circle",
            css: "alter",
            click: "$$('add_new_segment').close();"
          }]
        },
        body: {
          view: "form",
          complexData: true,
          elements: segmentForm,
          rules: segmentFormRules,
        }
      }).show();
    }
  });

  /******************************Segment Add End*******************************/


  /*****************************Menu Customization End*************************/


  /*******************************Custom Events********************************/

  /********************************Segment Edit********************************/

  // before editing file
  $$("webixFilemanagerTracker").attachEvent("onBeforeEditFile", function(id) {

    var srcTypeId = id.split('__');

    if (srcTypeId[0] == 'folder') {
      return true;
    }


    var url = '/api/trackers/update';

    var segmentForm = [{
      view: "text",
      id: "name",
      label: 'Name',
      name: "name",
      // invalidMessage: "Name can not be empty"
    }, {
      view: "textarea",
      id: "desc",
      label: 'Description',
      name: "desc",
      // invalidMessage: "Description can not be empty"
    }, {
      view: "button",
      id: "sub",
      name: "submit",
      value: "Sumbit",
      click: function() {
        var form = this.getParentView();
        // adding parent folder id

        if (form.validate()) {
          webix.ajax().post(url, form.getValues(), {
            error: function(text, data, XmlHttpRequest) {
              alert("error");
            },
            success: function(text, data, XmlHttpRequest) {
              var response = JSON.parse(text);
              if (response.success == true) {
                $$("edit_segment").hide();
                webix.message(response.msg);
              }
            }
          });

        }
      }
    }];
    var segmentFormRules = {
      "name": webix.rules.isNotEmpty,
      "desc": webix.rules.isNotEmpty,
    };
    // form
    webix.ui({
      view: "window",
      id: "edit_segment",
      modal: true,
      position: "center",
      height: 400,
      width: 400,
      head: {
        view: "toolbar",
        margin: -4,
        cols: [{
          view: "label",
          label: "Update Segment"
        }, {
          view: "icon",
          icon: "times-circle",
          css: "alter",
          click: "$$('edit_segment').close();"
        }]
      },
      body: {
        view: "form",
        id: "edit_segment_form",
        complexData: true,
        elements: segmentForm,
        rules: segmentFormRules,
      }
    }).show();

    // retreiving form data and setting into the form
    var formDataTracker;
    webix.ajax().get("/api/trackers/show", {
      source: id
    }, function(text, xml, xhr) {
      //response
      formDataTracker = JSON.parse(text);

      $$("edit_segment_form").setValues({
        name: formDataTracker.name,
        desc: formDataTracker.description,
        source: id
      }, true);
    });
    return false;
  });

  /*****************************Segment edit End*******************************/

  // reload grid after folder creation
  $$("webixFilemanagerTracker").attachEvent("onAfterCreateFolder", function(id) {
    // refreshManagerTracker();

    setTimeout(function(){ openLayoutTracker(); }, 500);
    return true;
  });


  // it will be triggered before deletion of file
  $$("webixFilemanagerTracker").attachEvent("onBeforeDeleteFile", function(ids) {
    // deleteSegment(ids);
    if (ids.indexOf('tracker__') === -1) {
      return true
    }

    deleteTracker(ids);
    return false;
  });

  // it will be triggered before dragging the folder/tracker
  $$("webixFilemanagerTracker").attachEvent("onBeforeDrag", function(context, ev) {
    return true;
  });

  $$("webixFilemanagerTracker").attachEvent("onBeforeDrop", function(context, ev) {
    if (context.start.indexOf('segment__') === -1) {
      return true
    }
    webix.ajax().post("/api/trackers/change-folder", {
      _id: context.start,
      target: context.target
    }, {
      error: function(text, data, XmlHttpRequest) {
        alert("error");
      },
      success: function(text, data, XmlHttpRequest) {
        var response = JSON.parse(text);
        if (response.success == true) {}
      }
    });
    return true;
  });
  // it will be triggered after dropping the folder to the destination
  $$("webixFilemanagerTracker").attachEvent("onAfterDrop", function(context, ev) {
    webix.message("Tracker has been moved to new folder.");
    return false;
  });

  $$("webixFilemanagerTracker").$$("files").attachEvent("onBeforeRender", filterFilesTracker);
  $$("webixFilemanagerTracker").$$("table").attachEvent("onBeforeRender", filterFilesTracker);

  var _exe = false;
  $$("webixFilemanagerTracker").attachEvent("onAfterLoad", function(context, ev) {
    if(_exe === false){
      _exe = true;
      initIntialTTandGuides();
    }

  });
  /*******************************Custom Events End****************************/
  // });
  fab.initActionButtons({
    type: "trackerList",
    actionButtonTypes: extendedFab.actionButtonTypes,
    primary: extendedFab.primary,
  });
}
// filter segments only
function filterFilesTracker(data) {
  data.blockEvent();
  data.filter(function(item) {
    return item.type != "folder"
  });
  data.unblockEvent();
}

/**********************************Functions***********************************/
function updateTracker(id) {
  var url = '/api/trackers/show';

  // retreiving form data and setting into the form
  var formDataTracker;
  webix.ajax().get(url, {
    _id: id
  }, function(text, xml, xhr) {
    //response
    formDataTracker = JSON.parse(text);
    openWizardTracker('edit', formDataTracker);
  });
}

function deleteTracker(id) {
  webix.confirm({
    text: "Do you want to delete?",
    ok: "Yes",
    cancel: "No",
    callback: function(result) {
      if (result) {
        webix.ajax().post("/api/trackers/delete", {
          _id: id
        }, {
          error: function(text, data, XmlHttpRequest) {
            alert("error");
          },
          success: function(text, data, XmlHttpRequest) {
            var response = JSON.parse(text);
            if (response.success == true) {
              folderFileManagerTracker.deleteFile(id);
            }
          }
        });
      }
    }
  });

}

function updateHiddenTracker(paramsTracker) {
  // var paramsTracker = {source: id, action: action, hidden: hidden};
  if (paramsTracker.hidden == 1) {
    var message = webix.message("deactivating...");
  } else if (paramsTracker.hidden == 0) {
    var message = webix.message("activating...");
  }

  webix.ajax().post("/api/trackers/update-status", paramsTracker, {
    error: function(text, data, XmlHttpRequest) {
      alert("error");
    },
    success: function(text, data, XmlHttpRequest) {
      var response = JSON.parse(text);
      if (response.success == true && paramsTracker.action == "update_hidden") {
        var element = document.getElementById("updateHiddenTracker" + paramsTracker._id)
        if (paramsTracker.hidden == 0) {
          element.innerHTML = "active";
          element.style.color = "green";
          element.onclick = function() {
            paramsTracker.hidden = 1;
            updateHiddenTracker(paramsTracker);
          };
          webix.message("Tracker has been activated.");
        } else if (paramsTracker.hidden == 1) {
          element.innerHTML = "inactive";
          element.style.color = "red";
          element.onclick = function() {
            paramsTracker.hidden = 0;
            updateHiddenTracker(paramsTracker);
          };
          webix.message("Tracker has been deactivated.");
        }
        refreshManagerTracker();
        webix.message.hide(message);
      }
    }
  });
}

function openWizardTracker(modeTracker, formDataTracker, referenceOptionsTracker) {

  defaultStepsTracker = ["basic", "tracker", "trackerName", "trackerValue", "confirm"];
  activeStepTracker = defaultStepsTracker[0];
  trackerTypeTracker = "tracker";
  isBasicGeneratedTracker = false;
  isTrackerGeneratedTracker = false;
  isTrackerNameGeneratedTracker = false;
  isTrackerValueGeneratedTracker = false;
  isConfirmGeneratedTracker = false;


  var containerId = "innerContent";

  if (referenceOptionsGoal !== undefined) {
    containerId = referenceOptionsGoal.container;
  }

  $("#" + containerId).html("");

  var wizardGoal = document.createElement('div');
  wizardGoal.id = 'webixWizardParentIdGoal';
  wizardGoal.style.minHeight = '680px';
  document.getElementById(containerId).appendChild(wizardGoal);


  stepsTracker = defaultStepsTracker;

  var basicButtonGoal = {
    view: "button",
    label: "Basic",
    css: "wizardStepBtn Basic",
    id: "wizardBasicBtnGoal",
    width: 170,
    badge: 1,
    click: function() {
      if (trackerTypeTracker == "tracker") {
        activeStepTracker = stepsTracker[0];
      } else {
        activeStepTracker = stepsTracker[0];
      }

      switch (trackerTypeTracker) {
        case "tracker":
          activeStepTracker = stepsTracker[0];
        case "customvar":
          activeStepTracker = stepsTracker[0];
        case "asset":
          activeStepTracker = stepsTracker[0];
          break;
        default:
          activeStepTracker = stepsTracker[0];
          break;
      }

      showViewGoal(true, false, false, false, false, modeTracker, formDataTracker);
    }
  };

  var placementWindowButtonGoal = {
    view: "button",
    css: "wizardStepBtn Placement",
    id: "wizardPlacementBtnGoal",
    label: "Placement",
    width: 170,
    badge: 2,
    disabled: true,
    click: function() {
      if (trackerTypeTracker == "condition_related") {
        activeStepTracker = stepsTracker[1];
      } else {
        activeStepTracker = stepsTracker[1];
      }

      showViewGoal(false, true, false, false, false, modeTracker, formDataTracker);
    }
  };

  var conditionsButtonGoal = {
    view: "button",
    css: "wizardStepBtn Conditions",
    id: "wizardConditionsGoal",
    label: "Conditions",
    width: 170,
    badge: 2,
    disabled: true,
    click: function() {
      if (trackerTypeTracker == "condition_related") {
        activeStepTracker = stepsTracker[1];
      } else {
        activeStepTracker = stepsTracker[2];
      }

      showViewGoal(false, false, true, false, false, modeTracker, formDataTracker);
    }
  };

  var conditionsPageButtonGoal = {
    view: "button",
    css: "wizardStepBtn Conditions",
    id: "wizardConditionsPageGoal",
    label: "Conditions",
    width: 170,
    badge: 3,
    disabled: true,
    click: function() {
      if (trackerTypeTracker == "condition_related") {
        activeStepTracker = stepsTracker[1];
      } else {
        activeStepTracker = stepsTracker[2];
      }

      showViewGoal(false, false, true, false, false, modeTracker, formDataTracker, referenceOptionsGoal);
    }
  };

  var pointsButtonGoal = {
    view: "button",
    css: "wizardStepBtn Points",
    id: "wizardPointsBtnGoal",
    label: "Points",
    width: 170,
    badge: 3,
    disabled: true,
    click: function() {
      if (trackerTypeTracker == "condition_related") {
        activeStepTracker = stepsTracker[2];
      } else {
        activeStepTracker = stepsTracker[3];
      }

      showViewGoal(false, false, false, true, false, modeTracker, formDataTracker, referenceOptionsGoal);
    }
  };

  var pointsPageButtonGoal = {
    view: "button",
    css: "wizardStepBtn Points",
    id: "wizardPointsPageBtnGoal",
    label: "Points",
    width: 170,
    badge: 4,
    disabled: true,
    click: function() {
      if (trackerTypeTracker == "condition_related") {
        activeStepTracker = stepsTracker[2];
      } else {
        activeStepTracker = stepsTracker[3];
      }

      showViewGoal(false, false, false, true, false, modeTracker, formDataTracker, referenceOptionsGoal);
    }
  };

  var consfirmButtonGoal = {
    view: "button",
    css: "wizardStepBtn Confirm",
    id: "wizardConfirmBtnGoal",
    label: "Confirm",
    width: 170,
    badge: 4,
    disabled: true,
    click: function() {
      if (trackerTypeTracker == "condition_related") {
        activeStepTracker = stepsTracker[3];
      } else {
        activeStepTracker = stepsTracker[4];
      }

      showViewGoal(false, false, false, false, true, modeTracker, formDataTracker, referenceOptionsGoal);
    }
  };
  var consfirmPageButtonGoal = {
    view: "button",
    css: "wizardStepBtn Confirm",
    id: "wizardConfirmPageBtnGoal",
    label: "Confirm",
    width: 170,
    badge: 5,
    disabled: true,
    click: function() {
      if (trackerTypeTracker == "condition_related") {
        activeStepTracker = stepsTracker[3];
      } else {
        activeStepTracker = stepsTracker[4];
      }

      showViewGoal(false, false, false, false, true, modeTracker, formDataTracker, referenceOptionsGoal);

    }
  };

  webix.ui({
    container: "webixWizardParentIdGoal",
    height: 680,
    id: "webixWizardHeaderMenuGoal",
    rows: [{
      view: "toolbar",
      paddingY: 1,
      height: 50,
      id: "webixWizardConditionalHeaderMenuToolbarGoal",
      hidden: false,
      css: "webixWizardHeader",
      elements: [{
          gravity: 1
        }, basicButtonGoal, conditionsButtonGoal, pointsButtonGoal, consfirmButtonGoal, {
          gravity: 1
        }

      ]
    }, {
      view: "toolbar",
      paddingY: 1,
      height: 50,
      id: "webixWizardPageHeaderMenuToolbarGoal",
      hidden: true,
      css: "webixWizardHeader",
      elements: [{
          gravity: 1
        }, basicButtonGoal, placementWindowButtonGoal, conditionsPageButtonGoal, pointsPageButtonGoal, consfirmPageButtonGoal, {
          gravity: 1
        }

      ]
    }, {
      template: "<div id='wizardbasicSectionGoal'></div><div id='wizardPlacementSectionGoal'></div><div id='wizardConditionSectionGoal'></div><div id='wizardPointsSectionGoal'></div><div id='wizardConfirmSectionGoal'></div>"
    }]
  });

  openBasicSectionGoal(modeTracker, formDataTracker, referenceOptionsGoal);
  if (modeTracker === 'edit') {

    if (formDataTracker.type === "page_related") {
      stepsTracker = defaultStepsTracker;
      if (stepsTracker[1] !== "placement") {
        stepsTracker.splice(1, 0, "placement");
      }
      trackerTypeTracker = "page_related";
      $$("webixWizardConditionalHeaderMenuToolbarGoal").hide();
      $$("webixWizardPageHeaderMenuToolbarGoal").show();
      openPlacementSectionGoal(modeTracker, formDataTracker, referenceOptionsGoal);
    } else {
      stepsTracker = ["basic", "conditions", "points", "confirm"];
      trackerTypeTracker = "condition_related";
      $$("webixWizardConditionalHeaderMenuToolbarGoal").show();
      $$("webixWizardPageHeaderMenuToolbarGoal").hide();
    }
    openConditionsSectionGoal(modeTracker, formDataTracker, referenceOptionsGoal);
    openPointsSectionGoal(modeTracker, formDataTracker, referenceOptionsGoal);
    openConfirmSectionGoal(modeTracker, formDataTracker, referenceOptionsGoal);
  }
}

function refreshManagerTracker() {
  folderFileManagerTracker.clearAll();
  folderFileManagerTracker.load("/api/trackers");
}
