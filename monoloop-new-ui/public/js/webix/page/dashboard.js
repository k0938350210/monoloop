var last_response = null;

function randomIntFromInterval(min,max)
{
  return Math.floor(Math.random()*(max-min+1)+min);
}

function dashboardLoadData(){
  var url = monolop_api_base_url + '/api/dashboard?range='+$$('date-selector').getValue();

  // $$('top-category-table').showOverlay("Loading...");
  $$('experiences-table').showOverlay("Loading...");
  $$('audiences-table').showOverlay("Loading...");

  // $$('top-category-table').clearAll();
  $$('experiences-table').clearAll();
  $$('audiences-table').clearAll();
  $$('life-cycle-table').clearAll();

  webix.ajax().get(url, function(response, xml, xhr) {
    response = JSON.parse(response);
    window.last_response = response;
    // $$('top-category-table').hideOverlay();
    $$('experiences-table').hideOverlay();
    $$('audiences-table').hideOverlay();
    /*
    $('#dashboard-new-users .counter').html(response.general.new_users);
    $('#dashboard-new-users .progress-bar').css('width',Math.abs(response.general.new_users_p) + '%');
    $('#dashboard-new-users .sr-only').html(response.general.new_users_p + '% progress');
    $('#dashboard-new-users .status-number').html(response.general.new_users_p + '%');

    $('#dashboard-removed-users .counter').html(response.general.removed_users);
    $('#dashboard-removed-users .progress-bar').css('width',Math.abs(response.general.removed_users_p) + '%');
    $('#dashboard-removed-users .sr-only').html(response.general.removed_users_p + '% progress');
    $('#dashboard-removed-users .status-number').html(response.general.removed_users_p + '%');

    $('#dashboard-total-users .counter').html(response.general.total_users);
    */
    $('#dashboard-total-visits .counter').html(response.general.total_visits);
    $('#dashboard-total-visits .progress-bar').css('width',Math.abs(response.general.total_visits_p) + '%');
    $('#dashboard-total-visits .sr-only').html(response.general.total_visits_p + '% progress');
    $('#dashboard-total-visits .status-number').html(response.general.total_visits_p + '%');

    // $$('top-category-table').parse(response.categories);
    $$('experiences-table').parse(response.experiments);
    $$('audiences-table').parse(response.audiences);
    $$('life-cycle-table').parse(response.general.life_cycle_data);

    $('#lf-anonymous').html(response.general.life_cycle_a_p + '%');
    $('#lf-identifined').html(response.general.life_cycle_i_p + '%');

    $('.counter').counterUp({
      delay: 10,
      time: 1000
    });
  });

  return;
  const options = {
    chart: {
      curve: {
        enabled: true
      },
      bottomPinch: 0,
      animate: 200,
      height: 300
    },
    block: {
      highlight: true,
      minHeight: 50,
      highlight: true
    },
  };
  var data = [{label: "First time visitors", value: 9576}, {label: "Scouting(69.45%)", value: 6651}];

  const chart = new D3Funnel('#dashboard-funnel-1');
  chart.draw(data, options);


}

function openLayout(){

  webix.ready(function() {
    initIntialTTandGuides();
    // reports list

    webix.ui({
      view: 'richselect',
      container: 'optional-div',
      id: 'date-selector',
      options:[
        {value: 'Today',id: 'today'},
        {value: 'Yesterday',id: 'yesterday'},
        {value: 'Last 7 days',id: 'last-7-days'},
        {value: 'Last 30 days',id: 'last-30-days'}
      ],
      value: 'last-7-days',
      on:{
        onChange: function(newv,oldv){
          dashboardLoadData();
        }
      }
    });

    webix.ui({
      container:"innerContent",
      css: 'dashboard-landing',
      rows:[
        {
          height: 150,
          body:{
            cols: [
              /*{
                body: '<div class="dashboard-stat2" id="dashboard-new-users"><div class="display"><div class="number"><h3 class="font-green-sharp"><span class="counter">&nbsp;</span></h3><small>NEW USERS</small></div><div class="icon"><i class="icon-user"></i></div></div>'+
                          '<div class="progress-info">'+
                            '<div class="progress">' +
                              '<span style="" class="progress-bar progress-bar-success green-sharp">'+
                                '<span class="sr-only"></span>' +
                              '</span>' +
                            '</div>' +
                            '<div class="status">' +
                              '<div class="status-title"> change </div>' +
                              '<div class="status-number"></div>' +
                            '</div>' +
                          '</div></div>'
              },
              {
                body: '<div class="dashboard-stat2" id="dashboard-removed-users"><div class="display"><div class="number"><h3 class="font-red-haze"><span class="counter">&nbsp;</span></h3><small>REMOVED USERS</small></div><div class="icon"><i class="icon-user"></i></div></div>'+
                          '<div class="progress-info">'+
                            '<div class="progress">' +
                              '<span style="" class="progress-bar progress-bar-success red-haze">'+
                                '<span class="sr-only"></span>' +
                              '</span>' +
                            '</div>' +
                            '<div class="status">' +
                              '<div class="status-title"> change </div>' +
                              '<div class="status-number"></div>' +
                            '</div>' +
                          '</div></div>'
              },
              {
                body: '<div class="dashboard-stat2" id="dashboard-total-users"><div class="display"><div class="number"><h3 class="font-green-sharp"><span class="counter">&nbsp;</span></h3><small>TOTAL USERS</small></div><div class="icon"><i class="icon-user"></i></div></div>'+
                          '</div>'
              },*/
              {
                body: '<div class="dashboard-stat2" id="dashboard-total-visits"><div class="display"><div class="number"><h3 class="font-green-sharp"><span class="counter">&nbsp;</span></h3><small>TOTAL VISITS</small></div><div class="icon"><i class="icon-user"></i></div></div>'+
                          '<div class="progress-info">'+
                            '<div class="progress">' +
                              '<span style="" class="progress-bar progress-bar-success green-sharp">'+
                                '<span class="sr-only"></span>' +
                              '</span>' +
                            '</div>' +
                            '<div class="status">' +
                              '<div class="status-title"> change </div>' +
                              '<div class="status-number"></div>' +
                            '</div>' +
                          '</div></div>'
              }
            ]
          }
        } ,{
          cols:[
            {
              body: {
                rows:[
                  { view:"template", template:"<div class='caption'>Audiences List</div><div class='actions'><a href='javascript:;' onclick='$$(\"audiences-table\").config.expand();' class='btn btn-circle btn-icon-only btn-default'><i class='icon-resize-full'></i></a></div>", type:"header" },
                  {
                    id: 'audiences-table',
                    view:"datatable",
                    columns:[
                      { id:"name",   header:"Audience", fillspace:true, template: function(obj){
                        return '<a href="/reports/segment/detail/'+obj.uid+'">' + obj.name + '</a>';
                      }},
                      { id:"current",    header:"Last&nbsp;7&nbsp;days", adjust:true, css:{'text-align':'right'}  },
                      { id:"previous",   header:"Previous&nbsp;7&nbsp;days", adjust:true, css:{'text-align':'right'}  },
                      { id:"change",   header:"Change (%)", adjust:true, css:{'text-align':'right'}  }
                    ],
                    height: 370,
                    expand: function(){
                      webix.ui({
                        view:"window",
                        id: 'audiences-table-window',
                        height:550,
                        width:$(window).width() * 3 / 4   ,
                        head:{
                          view:"toolbar",
                          margin:-4,
                          cols:[
                            {
                              view:"label", label: "Audiences List"
                            },
                            {
                              view:"icon", icon:"times-circle",
                              click:"$$('audiences-table-window').close();"
                            }
                          ]
                        },
                        position:"center",
                        body:{
                          id: 'audiences-table2',
                          view:"datatable",
                          columns:[
                            { id:"name",   header:"Audience", fillspace:true, template: function(obj){
                              return '<a href="/reports/segment/detail/'+obj.uid+'">' + obj.name + '</a>';
                            }},
                            { id:"current",    header:"Last&nbsp;7&nbsp;days", adjust:true, css:{'text-align':'right'}  },
                            { id:"previous",   header:"Previous&nbsp;7&nbsp;days", adjust:true, css:{'text-align':'right'}  },
                            { id:"change",   header:"Change (%)", adjust:true, css:{'text-align':'right'}  }
                          ]
                        }
                      }).show();
                      $$('audiences-table2').parse(window.last_response.audiences);
                    }
                  }
                ]
              }
            },{
              body: {
                rows:[
                  { view:"template", template:"<div class='caption'>Experiences List</div><div class='actions'><a href='javascript:;' onclick='$$(\"experiences-table\").config.expand();' class='btn btn-circle btn-icon-only btn-default'><i class='icon-resize-full'></i></a></div>", type:"header" },
                  {
                    id: 'experiences-table',
                    view:"datatable",
                    columns:[
                      { id:"name",   header:"Name", fillspace:true, template: function(obj){
                        return '<a href="/reports/experiment/detail/'+obj.uid+'">' + obj.name + '</a>';
                      }},
                      { id:"visitors",    header:"Visitors", adjust:true, css:{'text-align':'right'}},
                      { id:"conversions",   header:"Conversions", adjust:true, css:{'text-align':'right'}},
                      { id:"cr",   header:"CR%", adjust:true, css:{'text-align':'right'}}
                    ],
                    height: 370,
                    expand: function(){
                      webix.ui({
                        view:"window",
                        id: 'experiments-table-window',
                        height:550,
                        width:$(window).width() * 3 / 4   ,
                        head:{
                          view:"toolbar",
                          margin:-4,
                          cols:[
                            {
                              view:"label", label: "Experiences List"
                            },
                            {
                              view:"icon", icon:"times-circle",
                              click:"$$('experiments-table-window').close();"
                            }
                          ]
                        },
                        position:"center",
                        body: {
                          id: 'experiences-table2',
                          view:"datatable",
                          columns:[
                            { id:"name",   header:"Name", fillspace:true, template: function(obj){
                              return '<a href="/reports/experiment/detail/'+obj.uid+'">' + obj.name + '</a>';
                            }},
                            { id:"visitors",    header:"Visitors", adjust:true, css:{'text-align':'right'}},
                            { id:"conversions",   header:"Conversions", adjust:true, css:{'text-align':'right'}},
                            { id:"cr",   header:"CR%", adjust:true, css:{'text-align':'right'}}
                          ]
                        }
                      }).show();
                      $$('experiences-table2').parse(window.last_response.experiments);
                    }
                  }
                ]
              }
            }/*{
              header: 'Top 5 categories',
              body: {
                id: 'top-category-table',
                view:"datatable",
                columns:[
                  { id:"name",   header:"Category", fillspace:true},
                  { id:"score",    header:"Score", adjust:true, css:{'text-align':'right'} }
                ],
                autoheight:true
              }
            }*/
          ]
        },{
          cols:[{
            body:{
              rows:[
                { view:"template", template:"<div class='caption'>Customer lifecycle stages</div>", type:"header" },
                {
                  view:"template",
                  height: 150,
                  template: '<div style="display: flex;margin-top:10px;">'+
                      '<div style="width: 32%;text-align: center;" id="lf-anonymous">'+

                      '</div>'+
                      '<div style="width: 62%;text-align: center;" id="lf-identifined">'+

                      '</div>'+
                    '</div>'+
                    '<div style="display: flex;height: 15px;">'+
                      '<div style="width: 0.25%;"></div>'+
                      '<div style="border-top:1px solid black;border-left:1px solid black;border-right:1px solid black; width: 32%;">'+
                      '</div>'+
                      '<div style="width: 0.25%;"></div>'+
                      '<div style="border-top:1px solid black;border-left:1px solid black;border-right:1px solid black; width: 63%;">'+
                      '</div>'+
                    '</div>'+
                    '<ul class="lifecycle" style="margin-top:2px;margin-bottom: 2px">'+
                      '<li class="lc-1">New</li>'+
                      '<li class="lc-2"><span class="trian tr-1 lc-2"></span>Previouse</li>'+
                      '<li class="lc-3"><span class="trian tr-2 lc-3"></span>Known</li>'+
                      '<li class="lc-4"><span class="trian tr-3 lc-4"></span>Trialist</li>'+
                      '<li class="lc-5"><span class="trian tr-4 lc-5"></span>Customer</li>'+
                      '<li class="lc-6"><span class="trian tr-5 lc-6"></span>Reactive<span class="trian-last"></span></li>'+
                    '</ul>'+
                    '<div style="display: flex;height: 15px;">'+
                      '<div style="width: 0.25%;"></div>'+
                      '<div style="border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black; width: 32%;">'+
                      '</div>'+
                      '<div style="width: 0.25%;"></div>'+
                      '<div style="border-bottom:1px solid black;border-left:1px solid black;border-right:1px solid black; width: 63%;">'+
                      '</div>'+
                    '</div>'+
                    '<div style="display: flex;">'+
                      '<div style="width: 32%;text-align: center;">'+
                        'Anonymous'+
                      '</div>'+
                      '<div style="width: 62%;text-align: center;">'+
                        'Identified'+
                      '</div>'+
                    '</div>'
                },{
                  view:"datatable",
                  id: 'life-cycle-table',
                  height: 220,
                  columns:[
                    { id:"name",   header:"Name", fillspace:true },
                    { id:"visitors",    header:"Visitors", adjust:true, css:{'text-align':'right'}},
                    { id:"conversions",   header:"Conversions", adjust:true, css:{'text-align':'right'}},
                    { id:"conversion_rate",   header:"Conversion rate (%)", adjust:true, css:{'text-align':'right'}}
                  ],
                }
              ]
            }

          },{
            body:{
              rows:[
                { view:"template", template:"<div class='caption'></div>", type:"header" }
              ]
            }
          }]
        }

      ]
    });



    dashboardLoadData();
  });
}
