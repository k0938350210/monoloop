function validateEmail(email){
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

var _form = [
			{ view:"text", label:'Email', name:"email"},
			{ view:"text", label:'Confirm email', name:"confirmEmail"},
			{ view:"text",type: "password", label:'Password', name:"password"},
      { view:"text",type: "password", label:'Confirm Password', name:"confirmPassword"},
			{ view:"text", label:'Full name', name:"name", invalidMessage: "Full name can not be empty."},
      { view:"button", value: "Create", id: "createNewAccountSbbtn", align:"right", width: 150, click:function(){
          var form = this.getParentView();
          if (form.validate()){
            webix.ajax().post("/api/invitation/signup", form.getValues(), function(res, data, xhr){
              res  = JSON.parse(res);
              if(res.success === true){

                if(res.authenticated === true){
                  webix.send('/', null, 'GET');
                } else {
                    webix.message({type:"error", text: "Account couldn't authenticated"});
                }

                $$('openSignUpFormlayoutPopup').close();
              } else {
                webix.message({type:"error", text:res.msg});
              }
            });


          }
        }}
		];
var _formRules = {
	"password": function(value){
			value = value.trim();
			if(value === ''){
				 $$("__signupForm").elements.password.define("invalidMessage", "Password can not be empty.");
				 return false;
			}
			if(value !== $$("confirmPassword").getValue()){
				$$("__signupForm").elements.password.define("invalidMessage", "Password does not match with confirm password.");
				return false
			}
		 return true;
	 },
	"confirmPassword": function(value){
		value = value.trim();
		if(value === ''){
			 $$("__signupForm").elements.confirmPassword.define("invalidMessage", "Confirm password can not be empty.");
			 return false;
		}
		if(value !== $$("password").getValue()){
			$$("__signupForm").elements.confirmPassword.define("invalidMessage", "Confirm password does not match with password.");
			return false
		}
	 return true;
	},
	"name": webix.rules.isNotEmpty,
	"email": function(value){
			value = value.trim();
			if(value === ''){
				 $$("__signupForm").elements.email.define("invalidMessage", "Email can not be empty.");
				 return false;
			}
			if(!validateEmail(value)){
				 $$("__signupForm").elements.email.define("invalidMessage", "Incorrect e-mail address.");
				 return false;
			}
			if(value !== $$("confirmEmail").getValue()){
				$$("__signupForm").elements.email.define("invalidMessage", "Email does not match with confirm email.");
				return false
			}
		 return true;
	 },
	"confirmEmail": function(value){
		value = value.trim();
		if(value === ''){
			 $$("__signupForm").elements.confirmEmail.define("invalidMessage", "Confirm email can not be empty.");
			 return false;
		}
		if(!validateEmail(value)){
			 $$("__signupForm").elements.confirmEmail.define("invalidMessage", "Incorrect confirm e-mail address.");
			 return false;
		}
		if(value !== $$("email").getValue()){
			$$("__signupForm").elements.confirmEmail.define("invalidMessage", "Confirm email does not match with email.");
			return false
		}
	 return true;
	},
};

function openSignUpFormlayout(){
  webix.ui({
    view: "window",
    id: "openSignUpFormlayoutPopup",
    modal: true,
    position: "center",
    height: 550,
    width: 400,
    head: {
      view: "toolbar",
      margin: -4,
      cols: [{
        view: "label",
        label: "New Account"
      }, {
        view: "icon",
        icon: "times-circle",
        css: "alter",
        click: "$$('openSignUpFormlayoutPopup').close();"
      }]
    },
    body: {
      view: "form",
      id: "__signupForm",
      height: 600,
      width: 400,
      complexData: true,
      elements: _form,
      rules: _formRules,
      margin:4,
      elementsConfig:{
        labelPosition: "top",
        labelWidth: 140,
        bottomPadding: 18
      }
    }
  }).show();

  if(user !== undefined) {
    $$("__signupForm").setValues({
      name: user.account.name,
      email: user.account.email,
			confirmEmail: user.account.email,
      accountId: user.account._id,
      accountUserId: user._id
    });
  }

}
