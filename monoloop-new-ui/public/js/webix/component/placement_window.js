var parent_accessible = true ;
try{
  parent.document;
}catch(e){
  parent_accessible = false ;
}
if(parent_accessible){
  parent.placementWindowIframe = window;
}
var page = null ;
if(parent_accessible){
  page = parent.page;
}
// Content-menu file variables
var xpath_flag = true; // Mantis #4556
// var placement_type_pass = true;
var conditionBuilderValue = false;
var conditionBuilderLabel = false;
var check_selected_Item = true;
var bubble_content = false; // Mantis #4467
var hidden_flag = false; // to control variation options to not open when side bar is closed
var is_load_buddle = false;
var selected_experiment_id = 'all';
var allExperiments = [{
  'id': 'all',
  'value': '<b>All</b>',
}];
var urlParams = {
  fullURL: "",
  xpath: "",
  xpath1: "",
  hashFn: "",
  page_element_id:  "",
  current_folder:  "",
  source: "",
  experiment_id: "",
  page: "",
  totalVariations: "",
  havVar: "",
  content_id: "",
  content_name: "",
  content_condition: "",
  bk_segment_id: "",
  bk_goal_id: "",
  tagName: "",
}
var contentListResponse,
  urlOptions = [
	{id: 'exactMatch', value: 'Exact Match'},
	{id: 'startsWith', value: 'Starts With'},
],
  pageOptions = {
	fullURLWebPageList: urlParams.fullURL,
	includeWWWWebPageElement: 1,
	includeHttpHttpsWebPageElement: 0,
	urlOptionWebPageElement: 0,
	regularExpressionWebPageElement: '',
	remarkWebPageList: '',
},
getUrlOption = function(id){
		id = id || 0;
		switch (id) {
			case 0: return "exactMatch"; break;
			case 1: return "allowParams"; break;
			case 2: return "startsWith"; break;
			default: return "exactMatch"; break;
		}
},
  newExpBackup = undefined;
var titleIdSeperator = '____mp_title_seperator____',
    webixDataSet = [],
    pagedDataSet = [],
    contentListResponse;
var page_CM = {
  size: 10,
  current: 0,
}
var original = {
  has: false,
  title: '<b>Original</b>',
  content: ''
};
var apis = {
  update_status: monolop_api_base_url+'/api/component/content-list/update-status',
  delete: monolop_api_base_url+'/api/component/content-list/delete'
};
var variationsModel = {
	setFormValues: function(content_id, name, condition, t, xpath){
  // setFormValues: function(content_id, name, condition, t){

		content_id = content_id || "";
		name = name || "";
		condition = condition || "";

		if(t === 'update'){
			$$("update_variation_container_form").setValues({
				content_id: content_id,
				variation_title: name,
        variation_xpath: xpath,
        // Mantis #4556
        // placement_type: placement_type,
				condition: condition,
				conditionLabel: condition && condition !== 'if (){ | }' ? "Condition is available." : "All visitors.",
			});
		}else if(t === 'add_content'){
      $$("add_addcontent_form").setValues({
        content_id: content_id,
        variation_title: name,
        // placement_type: placement_type,
        condition: condition
      });
    } else {
			$$("add_variation_form").setValues({
				content_id: content_id,
				variation_title: name,
				condition: condition
			});
		}

		if(condition === ''){
			$$("conditionLabel").setValue("All visitors.");
		}

	},
	addVariation: function(url, data){
		$.ajax({
			 url: url,
			 type: "POST",
			 data: data,
			 beforeSend: function( xhr ) {
				 xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
				 xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
			 }
		 })
			 .done(function( response ) {
				 if (response.status === 'success') {
					 urlParams.havVar = 1;
					 urlParams.totalVariations++;
           if(data.hide !== true && data.placement_type === undefined){
						variationsModel.setFormValues('', "Variation [" + urlParams.totalVariations + "]",'', '');
          }else if (data.placement_type){
            variationsModel.setFormValues('', "Variation [" + urlParams.totalVariations + "]",'', 'add_content');
          }
           // variationsModel.setFormValues('', "Variation [" + urlParams.totalVariations + "]",'','');
					//  if(data.hide !== true && data.placement_type === undefined){
					// 	variationsModel.setFormValues('', "Variation [" + urlParams.totalVariations + "]",'','', '');
          // }else {
          //   variationsModel.setFormValues('', "Variation [" + urlParams.totalVariations + "]",data.placement_type,'', 'add_content');
          // }
					 urlParams.page_element_id = response.pageElement._id;
           var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
           var iframe_ = $('iframe')[index];
					 iframe_.contentWindow.postMessage({
             t: "set-current-pageElement",
             data: {
               content: response.content,
               pageElement: response.pageElement,
               source: response._source,
               experiment: response.experiment
             },
             variationCreated: true
           }, data.fullURL);
					 webix.message("Variation has been created!");
					 // iframe_.contentWindow.postMessage({t:"hide-iframe",message:"variation has been created"},"*");



				 } else {
					 if(response.status === 'error'){
						 if(typeof response.msg === 'string'){
							 webix.message({type: 'error', text: response.msg});
						 } else {
							 webix.message({type: 'error', text: "An error occoured, please refreh page and try again"});
						 }
					 } else {
						 webix.message({type: 'error', text: "An error occoured, please refreh page and try again"});
					 }
				 }
			 });
	},
	updateVariation: function(url, data){
		$.ajax({
			 url: url,
			 type: "POST",
			 data: data,
			 beforeSend: function( xhr ) {
				 xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
				 xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
			 }
		 })
			 .done(function( response ) {
				 if (response.status === 'success') {
					 webix.message("Variation has been updated!");
					 // parent.postMessage({t:"hide-iframe",message:"variation has been updated"},"*");

				 } else {
					 webix.alert("An error occoured, please refreh page and try again");
				 }
			 });
	}

};
// Ends here Content-menu file variables
var scriptNotExistsMsg = '<p style="padding: 0 10px;">This page does not have the Monoloop script in the page. This is required for Monoloop to work.<br /> Please copy the code from Account/Snippet section and place in the header section of your site template and try again.</p>';
var domainNotRegisteredMsg = '<p style="padding: 0 10px;">This domain is not registered. Please register this domain to edit this page.</p>';
var canWeConfirm = false;
var editFirstTime = true;
var enableEditmode = true;
var enableTestMode = true ;
var iFrameConditionBuilderWin;
var IframeImageUploaderWin;
var customerSiteUrlLink = "";
var currentPageElement = {added: false, data: {}};
if(page === 'goal' || page === 'tracker'){
  enableEditmode = false;
  enableTestMode = false;
}

var isPWchanged = false;
glowSaveBtn(false); // By SJ
var basicPlacementWindowFormValues = {};
var returnUrlConfig = {};
returnUrlConfig.url = "";
returnUrlConfig.url_option = 0;
returnUrlConfig.inc_www = 1;
returnUrlConfig.inc_http_https = 1;
returnUrlConfig.reg_ex = "";
returnUrlConfig.remark = "";
if(parent_accessible){
  parent.PWIFrameWin = window;
}

var testbench = {
isFirstTime : true ,
 fieldArray : {},
 mongoDetail: []
};

var defaultStepsPlacementWindowGoal = ['pw', 'edit_record','testbench', 'publish'];
var activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];

var currentStepPWGoal = 'pw';

var isPlacementWindowGeneratedGoal = false,
    isEditModeGeneratedGoal = false,
    isTestBenchGeneratedGoal = false,
    isPublishGeneratedGoal = false;

var pageExistingData = {
  variations: [],
  inlineContent: [],
  pageElement: false,
  originalUrl: "",
  reset: function(){
    this.variations = [];
    this.inlineContent = [];
    this.pageElement = false;
    this.originalUrl = "";
  }
};

var allFormDataPlacementWindowGoal = {};
allFormDataPlacementWindowGoal.data = {};


var urlHistoryPWGoal = [];
var indexUrlHistoyPWGoal = 0;
var currentUrlHosturyPWGoal = 0;
var traversingHistoryPWGoal = false;
var editPWUrl = '', testPWUrl = '', publishPWUrl = '';
var postMessageConfig = {
  stage: 0, // 0 init , 1 comfirm , 2 re-confirm , 3 - proxy
  url: ''
};

var fullscreen = {
  getHeight: function(){
    var height;
    if(activeStepPlacementWindowGoal === 'edit_record'){
      height = screen.innerHeight - 170;
    } else {
      height = screen.innerHeight - 220;
    }
    return height;
  },
  updateHeight: function(){
    $('.fullscreenMode .pw_divs').css('height', fullscreen.getHeight() + 'px');
  },
  full: function(){
    parent.postMessage(JSON.stringify({
          t: "pw-fullscreen-full"
        }), '*');


    $("#containerId").addClass('fullscreenMode');

    fullscreen.updateHeight();

    $$("fullscreen").hide();
    $$("fullscreen_exit").show();
  },
  exit: function(){

    parent.postMessage(JSON.stringify({
          t: "pw-fullscreen-exit"
        }), '*');

    $("#containerId").removeClass('fullscreenMode');

    $$("fullscreen").show();
    $$("fullscreen_exit").hide();
  }
}

//open domain registration popup
function registerDomain(){
  $("#innerContent").hide();
  $.getScript('/js/webix/page/account/domains.js', function() {
    openDomainPopup('new', undefined, undefined, false);
    $('#domain').val($('.webix_el_box>input').val());
    $('#url_privacy').val($('.webix_el_box>input').val());
  });
}

function xFrameCSPHeaderCheck(callback){
  var processResponse = function(response){
    var iFrameEmbeddEnabled = true;
    var message = '';
    var xFrameHeader = response['X-Frame-Options'];
    var cspHeader = response['Content-Security-Policy'];
    if(xFrameHeader != undefined){
      if(Array.isArray(xFrameHeader)){
        iFrameEmbeddEnabled = !(xFrameHeader.indexOf('SAMEORIGIN') > -1);
        if(!iFrameEmbeddEnabled){
          message +=  ' Your site seems to have X-Frame-Options enabled. This setting block us from loading our user interface on top of your site. To solve this problem permanently, please check our documentation (link).';
        }
      }else{
        iFrameEmbeddEnabled = !(xFrameHeader == 'SAMEORIGIN');
        if(!iFrameEmbeddEnabled){
          message +=  ' Your site seems to have X-Frame-Options enabled. This setting block us from loading our user interface on top of your site. To solve this problem permanently, please check our documentation (link).';
        }
      }
    }else if (cspHeader != undefined) {
      if(Array.isArray(cspHeader)){
        iFrameEmbeddEnabled = !(cspHeader.indexOf("default-src 'self';") > -1);
        if(!iFrameEmbeddEnabled){
          message +=  ' Your site seems to have Content Security Policies (CSP) enabled. This setting block us from loading our user interface on top of your site. To solve this problem permanently, please check our documentation (link).';
        }
      }else{
        iFrameEmbeddEnabled = !(cspHeader == "default-src 'self';");
        if(!iFrameEmbeddEnabled){
          message +=  ' Your site seems to have Content Security Policies (CSP) enabled. This setting block us from loading our user interface on top of your site. To solve this problem permanently, please check our documentation (link).';
        }
      }
    }

    if(iFrameEmbeddEnabled)
      callback();
    else{
      var isFirefox = typeof InstallTrigger !== 'undefined';
      var isChrome = !!window.chrome && !!window.chrome.webstore;


      // message +=  ' Your site seems to have X-Frame-Options or Content Security Policies (CSP) enabled. This setting block us from loading our user interface on top of your site. To solve this problem permanently, please check our documentation (link).';

      if(isChrome || isFirefox){
         message += '<br />';
         message += '  To Temporarily solve this now, please install this <a href=\"';
         if(isChrome){
           message +=  'https://goo.gl/SYSmvN';
         }else if(isFirefox){
           message += 'https://goo.gl/PMpMBr';
         }
         message += '\" target=\"_blank\">plugin</a> for your browser.';
         message += ' to enable this UI.';
      }
      webix.modalbox({
          title: 'Your site is blocking our user interface',
          buttons: ['OK'],
          text: message,
          width: '600px',
          callback: function(result){
          }
      });
    }
  }
  var value = $$('customerSiteUrlLink').getValue();
  value = refineUrl(value);
  if(is_valid_url(value)){
      value = refineUrl(value);
      var url = monolop_api_base_url + '/api/requester';
      $.post(
        url
        , { url: value}
        , function(response){
            if(response.success){processResponse(response.data);}
          }
      );
  }
}
// webix.ready(function() {
var placement_window_toolbar = webix.ui({
  container: "placement_window", //corresponds to the ID of the div block
  id: "placement_window_id",
  // height: getPWHeight(160),
  height: $(window).height(),
  css: "placementMenu",
  width: 120,
  rows: [{
    view: "toolbar",
    paddingY: 1,
    height: 56,
    id: "webixPlacementWindowWizardHeader",
    hidden: false,
    css: "webixWizardHeader placement_windowMenu pw_steps",
    elements: [
    //   {
    //   view: "button",
    //   id: "browseBtnPlacementWindowGoal",
    //   type: "iconButton",
    //   icon: "search",
    //   label: "Browse",
    //   css: 'pw_wizardBtn pw active',
    //   width: 150,
    //   click: function() {
    //     activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
    //     showViewPlacementWindowGoal(true, false, false, false, null, null, null);
    //     fullscreen.updateHeight();
    //   }
    // },
    {
      css: "searchBtnGroup",
      hidden : ! parent_accessible ,
      height: 40,
      cols: [
        {
          view: "text",
          id: 'customerSiteUrlLink',
          css: "browseSearchText",
          hidden : ! parent_accessible ,
          value: url,
          height: 40,
          // readonly: true,
          //width: 364,
          click: function(){

            stayOrLeavePage('textbox');


          }
        }, {
          view: "button",
          css: "browseSearchGo goBtn",
          hidden : ! parent_accessible ,
          label: "GO",
          id: "goBtnClick",
          height: 40,
          //width: 38,
          click: function() {
            xFrameCSPHeaderCheck(function(){
              stayOrLeavePage();
            });
          }
        }
      ]
    },
    {
      view: "button",
      id: "editRecordBtnPlacementWindowGoal",
      type: "iconButton",
      icon: "edit",
      label: "Edit",
      hidden: (enableEditmode === false) ? true : false,
      css: 'pw_wizardBtn edit_record',
      //width: 150,
      disabled: true,
      click: function() {
        // send request for resetting selectors
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_ = $('iframe')[index];
        iframe_.contentWindow.postMessage({t: 'reset_on_EditMode'}, allFormDataPlacementWindowGoal.data.url);
        // till here
        $('#customerSiteUrlEditRecordSection').show();
        guidelines.removeHangedTooltips();

        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[1];
        showViewPlacementWindowGoal(false, true, false, false, null, null, null);
        fullscreen.updateHeight();
      }
    }, {
      view: "button",
      id: "testbenchRecordBtnPlacementWindowGoal",
      type: "iconButton",
      icon: "laptop",
      label: "Test",
      hover: {"color":"rgba(255, 255, 255, 0.8) !important","box-shadow": "0 5px 15px rgba(145, 92, 182, .4)"},
      hidden: (enableTestMode === false) ? true : false,
      css: 'pw_wizardBtn testbench',
      //width: 150,
      disabled: true,
      click: function() {
        guidelines.removeHangedTooltips();
        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[2];
        showViewPlacementWindowGoal(false, false, true, false, null, null, null);
        fullscreen.updateHeight();
      }
    }, {
      view: "button",
      id: "publishBtnPlacementWindowGoal",
      type: "iconButton",
      icon: "laptop",
      label: (page == 'goal' || page == 'tracker') ? "Select" : "Save",
      hidden: false,
      css: 'pw_wizardBtn publish',
      //width: 150,
      disabled: true,
      click: function() {
        guidelines.removeHangedTooltips();
        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[3];
        if(page === 'tracker'){
          showTrackerConfig();
        }else{
          if (basicPlacementWindowFormValues.mode == 'add' || basicPlacementWindowFormValues.mode == 'edit'){
            savePage(); // By SJ
          }else{
            showViewPlacementWindowGoal(false, false, false, true, null, null, null);
          }
        }

        fullscreen.updateHeight();
      }
    },

    {
      css : 'logout-link inside' ,
      id: "logoutLinkId",
      view : 'template' ,
      margin:0,
      hidden: (page === 'web') ? false : true,
      template:function(obj){
        return '<a href="javascript:void(0);" onclick="parent.storeSessionState();" title="Logout"><strong><i class="fa fa-power-off"></i></strong></a>' ;
      }
    },

    {
      view:"button",
      type: "image",
      image: monolop_base_url + "/images/fullscreen.png",
      //width: 46,
      css: "fullscreen screenicon",
      id: "fullscreen",
      click: fullscreen.full
    }, {
      view:"button",
      type: "image",
      image: monolop_base_url + "/images/fullscreen_exit.png",
      css: "fullscreen_exit screenicon",
      //width: 46,
      id: "fullscreen_exit",
      hidden: true,
      click: fullscreen.exit
    }
    // , {
    //   view:"icon",
    //   css: "pw_guideline_icon",
    //   icon:"fa-comment-o",
    //   tooltip: "Start guidelines for PW",
    //   width: 46,
    //   height: 46,
    //   click: function(){
    //     if(guidelines.open === false){
    //       guidelines.startGuideline('placementWindow', undefined, 'tip');
    //     }
    //   }
    // }
  ]
  },
  {
    css: 'pw_divs',
    template: "<div id='customerSiteUrlPlacementWindowGoalSection'></div><div id='customerSiteUrlEditRecordSection'></div><div id='customerSiteTestBenchSection'></div><div id='customerSitePublishSection'></div>"
  }]
});

// add guidelines icon into placement window header
var pwHeader = $('[view_id=webixPlacementWindowWizardHeader] div.webix_scroll_cont');


/*
var stGd = document.createElement('i');
stGd.id = 'stGd';
stGd.title = 'Show Guides';
stGd.className = "webix_icon fa-comment-o";
stGd.style.position = 'relative';
stGd.style.marginTop = '1.4%';
stGd.style.marginLeft = '-4%';
if(page == 'experiment' || page == 'goal'){
  stGd.style.left = '14%';
  stGd.style.marginLeft = '6%';
}
else{
  stGd.style.left = '18%';
}
stGd.style.fontSize = '1.2em';


var stGdToggle = document.createElement('i');
stGdToggle.id = 'toggleGuides';
stGdToggle.className = "webix_icon fa-toggle-on";
stGdToggle.style.position = 'relative';
stGdToggle.style.marginTop = '1.4%';
stGdToggle.style.marginLeft = '0%';
if(page == 'web')
  stGdToggle.style.marginLeft = '-1%';
if(page == 'experiment' || page == 'goal'){
  stGdToggle.style.right = '0%';
  stGdToggle.style.marginLeft = '8%';
}else{
  stGdToggle.style.left = '13.5%';
}
stGdToggle.style.cursor = 'pointer';
stGdToggle.style.fontSize = '1.2em';
stGdToggle.title = 'Hide Guides';

stGdToggle.onclick = function(){
  if($('#toggleGuides').hasClass('fa-toggle-off')){
    $('#toggleGuides').removeClass('fa-toggle-off');
    $('#toggleGuides').addClass('fa-toggle-on');
    $('#toggleGuides').attr('title', 'Hide Guides');
    // Start guides
    if(guidelines.open === false) {
      switch(activeStepPlacementWindowGoal){
        case "edit_record":{
          parent.placementWindowIframe.postMessage(JSON.stringify({t: 'start-guideline', pageId: 'placementWindowEdit', url: postMessageConfig.url}), monolop_base_url);
          break;
        }
        case "testbench": {
          guidelines.page.pageId = 'placementWindowTest';
          guidelines.startGuideline('placementWindowTest', {placement: 'right'}, 'tip');
          break;
        }
        case "publish": {
          parent.placementWindowIframe.postMessage(JSON.stringify({t: 'start-guideline', pageId: 'placementWindowPublish', url: (monolop_base_url + '/component/page-list-publish?')}), monolop_base_url);
          break;
        }
        case "pw":
        default: {
          guidelines.page.pageId = 'placementWindow';
          guidelines.startGuideline('placementWindow', undefined, 'tip');
        }
      }
    }
  }
};

pwHeader.append(stGd, stGdToggle);

*/

function initializeGuidesAndTipsOnPlacementWindow(mode){
    var _placementWindowMode = mode || '';
    // tooltips
    guidelines.page.pageId = ('placementWindow' + _placementWindowMode);
    // guidelines
    var _guideLinesURl = monolop_api_base_url+'/api/guidelines?page_id=' + guidelines.page.pageId;
    webix.ajax().get(_guideLinesURl, { }, function(text, xml, xhr){
      var r = JSON.parse(text);

      var cg = undefined;
      if(r.currentGuide){
        cg = JSON.parse(r.currentGuide);
        guidelines.startedBefore = true;
      }
      guidelines[guidelines.page.pageId] = r.guidelines;
      if(cg){
        if(cg.currentGuide){

          guidelines.page.currentGuide = cg.currentGuide;
        } else {
          guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
        }
      } else {
        guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
      }
      if(guidelines.page.currentGuide){
        // guidelines.generate(guidelines.page.currentGuide);
        if(guidelines.startedBefore === false){
          setTimeout(function(){guidelines.generate(guidelines.page.currentGuide);},2000);
        }else{
          if($('#toggleGuides').hasClass('fa-toggle-on')){
            $('#toggleGuides').removeClass('fa-toggle-on');
            $('#toggleGuides').addClass('fa-toggle-off');
            $('#toggleGuides').attr('title', 'Show Guides');
          }
        }
      }else{
        if($('#toggleGuides').hasClass('fa-toggle-on')){
          $('#toggleGuides').removeClass('fa-toggle-on');
          $('#toggleGuides').addClass('fa-toggle-off');
          $('#toggleGuides').attr('title', 'Show Guides');
        }
      }
      guidelines.startedBefore = false;
    });
}

initializeGuidesAndTipsOnPlacementWindow();
// guidelines.startGuideline('placementWindow', undefined, 'tip');
// submit form when enter key pressed


$('input').keypress(function(e) {
  if (e.which == 13) {
    // refine a valid URL
    if(is_valid_url($$('customerSiteUrlLink').getValue())){
      $$('customerSiteUrlLink').setValue(refineUrl($$('customerSiteUrlLink').getValue()));
      xFrameCSPHeaderCheck(function(){
        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
        showViewPlacementWindowGoal(true, false, false, false, null, null, null);
        openPlacementWindowGoalSection();
      });
    }
    return false; //<---- Add this line
  }
});
$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
       guidelines.saveInSession();
    }
});

// By SJ
function glowSaveBtn(isGlow){
  if (isGlow){
    highLightCurrentPlacementWindowGoal("publish");
  }else{
    highLightCurrentPlacementWindowGoal("unpublish");
  }

}

function stayOrLeavePage(clickedOn){
  if(activeStepPlacementWindowGoal !== 'pw'){
    if(isPWchanged){
      glowSaveBtn(true);
      if(basicPlacementWindowFormValues.source === 'default'){
        webix.confirm({
            title:"",
            width: 450,
            css: "leaveStayConfirm",
            ok:"Stay on page",
            cancel:"Leave the page",
            text:"Changes on page will be gone if you don't save them first. By leaving the page, new page wizard will be started.",
            callback: function(result){
              if(result === false){
                savePage(); // By SJ
                basicPlacementWindowFormValues.formData = {};

                currentPageElement.added = false;
                currentPageElement.data = {};

                pageExistingData.reset();

                activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
                showViewPlacementWindowGoal(true, false, false, false, null, null, null);

                if(isPlacementWindowGeneratedGoal === false){
                  openPlacementWindowGoalSection();
                }
              }
            }
        });
      } else {
        activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
        showViewPlacementWindowGoal(true, false, false, false, null, null, null);
      }

    } else {
      activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
      showViewPlacementWindowGoal(true, false, false, false, null, null, null);
    }
  } else {
    activeStepPlacementWindowGoal = defaultStepsPlacementWindowGoal[0];
    showViewPlacementWindowGoal(true, false, false, false, null, null, null);
    if(clickedOn !== 'textbox'){
      // to reload all the files and data for edit mode
      editFirstTime = true; // changed due to reloading the page on Go clicked
      openPlacementWindowGoalSection();
    } else {
      if(isPlacementWindowGeneratedGoal === false){
        openPlacementWindowGoalSection();
      }
    }
  }
}
//temp for test
var url = '';
var postMessageListener = function(event) {
  var data = JSON.parse(event.data);
  switch (data.t) {
    case "customization-occoured-in--user-site":
      if(data.isPWchanged){
        isPWchanged = true;
        glowSaveBtn(true);
      }
      break;
    case "upload-image-required-message":
      webix.message({ type:"error", text:"Please upload an image first." });
      break;
    case "page-confirm-form-changed":
      if(data.formChanged){
        isPWchanged = true;
        glowSaveBtn(true);
      }
      break;
    case "initialize_addvariation_flow":
      // initializing the Add variation flow
      $$('content_list_comp').hide();
      $('#var_action').html('');
      $('#variationGuides').html('');
      webix.html.removeCss( $$('PWsidebarpanel').getNode(), "sidebar_width");
      webix.html.addCss( $$('PWsidebarpanel').getNode(), "sidebar_width_resize");
      if(urlParams.tagName.toLowerCase() == "img"){
        openImageUploader()
      }
      showView('add_variation');
      break;
    case "initialize_addcontent_flow":
      // initializing the Add Content flow
      // $$('content_list_comp').hide();
      // $('#var_action').html('');
      // $('#variationGuides').html('');
      var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
      var iframe_ = $('iframe')[index];
      iframe_.contentWindow.postMessage({t: 'add_addcontent-destroyCKE'}, urlParams.fullURL);
      // showView('add_addcontent');
      break;
    case "initialize_hideElement_flow":
      $('#var_action').hide();
      $('#variationGuides').html('');
      $("#content_list_comp").hide();
      webix.html.removeCss( $$('PWsidebarpanel').getNode(), "sidebar_width");
      webix.html.addCss( $$('PWsidebarpanel').getNode(), "sidebar_width_resize");
      // hidecontent();
      showView('hide');
      // var data = {};
      // data.type = "variation";
      // data.variation_name = "Hidden";
      // data.hide = true;
      // data.condition = "";
      // data.fullURL = urlParams.fullURL;
      // data.page_element_id = urlParams.page_element_id;
      // data.experiment_id = urlParams.experiment_id;
      // var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
      // var iframe_ = $('iframe')[index];
      // iframe_.contentWindow.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
      break;
    case "sidebar_adjust_add_content":
      // $$('content_list_comp').hide();
      $$('content_list_comp').hide();
      // $('#var_action').html('');
      $('#variationGuides').html('');
      showView('add_addcontent');
      $('#var_action').hide();
      // $('#variationGuides').html('');
      // showView('add_addcontent');
      $("#content_options").hide();
      $("#add_addcontent").show();
      webix.html.removeCss( $$('PWsidebarpanel').getNode(), "sidebar_width");
      webix.html.addCss( $$('PWsidebarpanel').getNode(), "sidebar_width_resize");
      add_addcontent(data.data.xpath_content,data.data.data_type);
      // $(document).ready(function() {
      //   $('#variation_xpath').val(data.data.xpath_content);
      // });
      break;
    case "sidebar_editpanel":
        if (data.data.type === "multi_select") {
          $('#add_variation').css('display', 'none');
          $('#update_variation').css('display', 'none');
          $('#create_new_exp_container').css('display', 'none');
          $('#experiment_container').css('display', 'none');
          $('#content_list_comp').css('display', 'none');
          $('#add_addcontent').css('display', 'none');
          $('#hide_content').css('display', 'none');
          bubble_content = true;
          SB_content_options(bubble_content);

          break;
        }
        if (data.data.contentmenu_iframe === undefined) {
          check_selected_Item = true;
          $('#content_list_comp').html('');
          $('#content_options').html('');
          $('#3Addexp').hide();
          $('#add_variation').html('');
          $('#add_addcontent').html('');
          $('#update_variation').html('');
          $('#create_new_exp_container').html('');
          $('#experiment_container').html('');
          $('#var_action').html('');
          $("#variationGuides").html("No element is Selected! \n Please Select any element to open Variation Editor");
          $("#variationGuides").show();
          sidebarcollapse_close();
        }else {
          check_selected_Item = false;
          var content_array = data.data.contentmenu_iframe.split('&');
          bubble_content = false; // Mantis #4467
          // reassigning values
          urlParams.fullURL = data.data.full_url;
          for (var i = 1; i < content_array.length; i++) {
            // content_Array1[i] =content_array[i].split('=').pop(-1);
            if (content_array[i].indexOf("xpath=") != -1) {
              urlParams.xpath = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("xpath1=") != -1) {
              urlParams.xpath1 = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("hash_fn=") != -1) {
              urlParams.hashFn = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("page_element_id=") != -1) {
              urlParams.page_element_id = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("source=") != -1) {
              urlParams.source = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("exp=") != -1) {
              urlParams.experiment_id = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("page=") != -1) {
              urlParams.page = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("tVar=") != -1) {
              urlParams.totalVariations = content_array[i].split("=").pop(-1);
              urlParams.totalVariations++;
            }
            else if (content_array[i].indexOf("havVar=") != -1) {
              urlParams.havVar = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("tagName=") != -1) {
              urlParams.tagName = content_array[i].split("=").pop(-1);
            }
            else if (content_array[i].indexOf("current_folder=") != -1) {
              urlParams.current_folder = content_array[i].split("=").pop(-1);
            }
          }
          urlParams.content_id = undefined;
          if (data.data.type == "bubble") {
            urlParams.totalVariations = data.data.tvar;
          	urlParams.totalVariations++;
          	urlParams.havVar = data.data.tvar>0 ? 1 : 0;
          	urlParams.tagName = urlParams.xpath1.split(" ").pop(-1).split(".").pop(0);
            bubble_content = true; // Mantis #4467
          }
          pageOptions_init();
          $("#variationGuides").html('Variations');
          if($('#add_variation').css('display') != 'none'){
            $('#add_variation').css('display', 'none');
            $('#add_variation').html('');
          }
          if($('#add_addcontent').css('display') != 'none'){
            $('#add_addcontent').css('display', 'none');
            $('#add_addcontent').html('');
          }
          if($('#hide_content').css('display') != 'none'){
            $('#hide_content').css('display', 'none');
            $('#hide_content').html('');
          }
          if($('#update_variation').css('display') != 'none'){
            $('#update_variation').css('display', 'none');
            $('#update_variation').html('');
          }
          if($('#create_new_exp_container').css('display') != 'none'){
            $('#create_new_exp_container').css('display', 'none');
            $('#create_new_exp_container').html('');
          }
          if($('#experiment_container').css('display') != 'none'){
            $('#experiment_container').css('display', 'none');
            $('#experiment_container').html('');
          }
          if($('#content_list_comp').css('display') == 'none'){
            $('#content_list_comp').css('display', 'block');
          }
          if($('#content_options').css('display') == 'none'){
            $('#content_options').css('display', 'block');
          }


          if (hidden_flag) {
            SB_content_options(bubble_content);
          }else {
            sidebarcollapse_expand();
          }
        }

        // var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        // var iframe_ = $('iframe')[index];
        // iframe_.contentWindow.postMessage({t: "revert_all_changes"}, urlParams.fullURL);
        // var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        // var iframe_ = $('iframe')[index];
        // iframe_.contentWindow.postMessage({t: 'revert_all_changes'}, urlParams.fullURL);
        // var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        // var iframe_edit = $('iframe')[index];
        // iframe_edit.contentWindow.postMessage(JSON.stringify({t: 'revert_all_changes'}),'*');
      break;
      // cases from content-menu file
     case "add-element-content-variation":
       variationsModel.addVariation(
         monolop_api_base_url+"/api/page-element/content-create",
         data.data
       );
       // 	window.removeEventListener(messageEvent, __listener, false);
       break;
     case "update-element-content-variation":

       variationsModel.updateVariation(
         monolop_api_base_url+"/api/page-element/content-update",
         data.data
       );

       break;
     case "set-condition":
       if(data.data.condition){
         data.data.condition = data.data.condition.trim();
       }
       if(data.data.condition && data.data.condition !== 'if (){|}'){
         if($$("condition")){
           $$("condition").setValue(data.data.condition);
           $$("conditionLabel").setValue("Condition is available.");
         }else {
           conditionBuilderValue = data.data.condition;
           conditionBuilderLabel = "Condition is available.";
         }
         urlParams.content_condition = data.data.condition;
       } else {
         if($$("condition")){
           $$("condition").setValue("");
           $$("conditionLabel").setValue("All visitors.");
         }else {
           conditionBuilderValue = "";
           conditionBuilderLabel = "All visitors.";
         }

       }
       break;
     case "set-condition1":

       var condition = data.data.condition;
       if(condition){
         if(currentVariation !== false){
           var content_id = currentVariation.id;
           webix.ajax().post(monolop_api_base_url+'/api/page-element/content-update-condition?ajax=true', {content_id: content_id,condition: condition} , function(iresponse, xml, xhr) {
             iresponse = JSON.parse(iresponse);
             if(iresponse.status === 'success'){
               currentVariation.condition = iresponse.content.condition;
               webix.message("Condition has been updated.");
             } else {
               webix.alert("An eror occoured. Please refresh and try again.");
             }
           });
         }
       }


       break;

     case "set-updated-condition":

       var condition = data.data.condition;
       if(condition){
         if(currentVariation !== false){
           var content_id = currentVariation.id;
           webix.ajax().post(monolop_api_base_url+'/api/page-element/content-update-condition?ajax=true', {content_id: content_id,condition: condition} , function(iresponse, xml, xhr) {
             iresponse = JSON.parse(iresponse);
             if(iresponse.status === 'success'){
               currentVariation.condition = iresponse.content.condition;
               webix.message("Condition has been updated.");
             } else {
               webix.alert("An eror occoured. Please refresh and try again.");
             }
           });

           $('.updateVariationConditionIcon').attr('title',condition);
         }
       }


       break;

     case "set-experiment-values-from-backup":
       if(!data.newExp){
           showView('experiment');
       } else {
         newExpBackup = data.data;
         showView('content');
       }
       break;
    // Cases From Content Menu Ends Here
    case "start-guideline":
      var pageId = data.pageId;
      var fetchGuidelinesUrl = '/api/guidelines?page_id='+pageId;
      $.get( fetchGuidelinesUrl, function( r ) {
        // var r = JSON.parse(r);


        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
        guidelines.removeHangedTooltips();
        iframe_edit.contentWindow.postMessage({
          t: "start-guideline-in-site",
          pageId: pageId,
          data: r
        }, data.url);


      });
      break;
    case "posted-from-monoloopui":
      // if(new_url != 1){
      //   var c = readCookie('customerSiteUrlLink');
      //   if(c){
      //     $$("customerSiteUrlLink").setValue(c);
      //     eraseCookie('customerSiteUrlLink');
      //   }
      // }
      basicPlacementWindowFormValues = data.data;
      if(basicPlacementWindowFormValues.mode == "edit"){
        $$("customerSiteUrlLink").disable();
        $$("goBtnClick").disable();
      }
      xFrameCSPHeaderCheck(function(){


        var proceedWithPW = function(){
          allFormDataPlacementWindowGoal.data.url = $$("customerSiteUrlLink").getValue();
          if(basicPlacementWindowFormValues.type !== undefined && basicPlacementWindowFormValues.type === 'publish'){

            //openPublishGoalSection(); //By SJ
            openPlacementWindowGoalSection();
          // } else if( ['web', 'experiment'].indexOf(basicPlacementWindowFormValues.page) !== -1 && basicPlacementWindowFormValues.mode === 'edit'){
          } else if(basicPlacementWindowFormValues.type !== undefined && basicPlacementWindowFormValues.type === 'edit_record'){

            openEditRecordPlacementWindowGoalSection();
          } else if(basicPlacementWindowFormValues.type !== undefined && basicPlacementWindowFormValues.type === 'testbench'){

            openTestRecordPlacementWindowGoalSection();
          } else {

            openPlacementWindowGoalSection();
          }
        };

        if(basicPlacementWindowFormValues.source == 'experiment' && basicPlacementWindowFormValues.source.experiment){
          if(basicPlacementWindowFormValues.experiment.tempSegment){
            var segmentURL = monolop_api_base_url+ '/api/segments/show';
            webix.ajax().get(segmentURL, {
              _id: '__' + basicPlacementWindowFormValues.experiment.tempSegment
            }, function(rawResponse){
              basicPlacementWindowFormValues.experiment._segment = JSON.parse(rawResponse);
              proceedWithPW();
            });
          }else{ proceedWithPW();}
        }else{ proceedWithPW();}
      });

      break;
    case "page-published":
      currentPageElement.added = true;
      currentPageElement.data = data.pageElement;
      basicPlacementWindowFormValues.formData = data.pageElement;
      basicPlacementWindowFormValues.fullURL = data.pageElement.originalUrl;
      if(basicPlacementWindowFormValues.source === 'experiment'){
        basicPlacementWindowFormValues.experiment = data.experiment;
      }
      parent.postMessage(JSON.stringify({t: 'set-current-experiment', created: true, source: basicPlacementWindowFormValues.source, experiment: basicPlacementWindowFormValues.experiment}), '*');
      parent.postMessage(JSON.stringify({t: 'page-published' + '-' + page, source: data.source, pageElement: data.pageElement, temp: data.temp, message: data.message}), '*');
      break;
    case "set-current-pageElement":
        currentPageElement.added = true;
        currentPageElement.data = data.data.pageElement;
        basicPlacementWindowFormValues.formData = data.data.pageElement;
        basicPlacementWindowFormValues.fullURL = data.data.pageElement.originalUrl;
        if(basicPlacementWindowFormValues.source === 'experiment'){
          basicPlacementWindowFormValues.experiment = data.data.experiment;
          parent.postMessage(JSON.stringify({t: 'set-current-experiment', created: true, source: basicPlacementWindowFormValues.source, experiment: basicPlacementWindowFormValues.experiment}), monolop_base_url);
        }
        break;
    case 'confirm':
        //set url in text box
      $$("customerSiteUrlLink").setValue(data.url);
      if(parent_accessible){
        checkClientProtocal();
      }
      allFormDataPlacementWindowGoal.data.url = data.url;
      $$("customerSiteUrlLink").setValue(data.displayURL);
      if (!traversingHistoryPWGoal) {
        addnewHistoryUrlGoal(data.url);
      }
      traversingHistoryPWGoal = false;
      //set found invocation
      postMessageConfig.stage = 1;
      canWeConfirm = true;      // Changed by Imran Ali
      postMessageConfig.url = data.url;
      //send message for load editor-bundle
      if(currentStepPWGoal === 'pw' && is_load_buddle === false){
        is_load_buddle = true;
        var iframe = $('iframe')[0];
        iframe.contentWindow.postMessage(JSON.stringify({
          t: "loadbundle",
          domain: monolop_base_url + '/',
          url: monolop_base_url + editor_bundlejs
        }), postMessageConfig.url);
      }else if(currentStepPWGoal == 'testbench'){
        $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().contentWindow.postMessage(JSON.stringify({
          t: "loadbundle",
          domain: monolop_base_url + '/',
          url: monolop_base_url + editor_bundlejs
        }), postMessageConfig.url);
      } else if(currentStepPWGoal == 'edit_record'){
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
          iframe_edit.contentWindow.postMessage(JSON.stringify({
            t: "loadbundle",
            domain: monolop_base_url + '/',
            url: monolop_base_url + editor_bundlejs
          }), postMessageConfig.url);
      }

      break;
    case 'loadbundle-success':
      if(currentStepPWGoal === 'pw'){
        var iframe = $('iframe')[0];
        iframe.contentWindow.postMessage(JSON.stringify({
          t: "init-browse"
        }), postMessageConfig.url);
      }else if(currentStepPWGoal == 'testbench'){

        $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().contentWindow.postMessage(JSON.stringify({
          t: "init-testmode"
        }), postMessageConfig.url);
      }else {
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
        iframe_edit.contentWindow.postMessage(JSON.stringify({
          t: "init-selectmode",
          assets: window.assets
        }), postMessageConfig.url);
      }

      break;
    case 'set-stored-values':
      $$("customerSiteUrlLink").setValue(data.data.fullURL);
      allFormDataPlacementWindowGoal.data = data;
      basicPlacementWindowFormValues = data.data;
      if (data.url !== '') {
        openPlacementWindowGoalSection();
      }
      break;
    // case 'open-condition-builder':
    //   openConditionBuilder(data.condition, data.type);
    //   break;
    case 'openimage_uploader':
      openImageUploader();
      break;
    case 'set-new-inline-variations':
      postInlineContentVariations();
      break;
    case "set-current-select-element-on-page":
      if(parent_accessible){
        if(parent.webPageListConfig){
          parent.webPageListConfig.placementWindow.edit_record = data.data;
        }
        if(parent.experimentListConfig){
          parent.experimentListConfig.placementWindow.edit_record = data.data;
        }
      }
      break;
    case "save-guides-in-session":
      guidelines.page.pageId = data.data.pageId;
      guidelines.page.actionID = data.data.actionID;
      guidelines.page.currentGuide = data.data.currentGuide;
      guidelines.saveInSession();
      break;
    case "save-changed-content-while-editing":
      //console.log(data);
      $.ajax({
         url: monolop_api_base_url+"/api/page-element/content-update",
         type: "POST",
         data: data.data,
         beforeSend: function(xhr){
           xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
           xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
         },
       })
         .done(function( response ) {
           if (response.status === 'success') {
             webix.message("Variation has been updated!");
           } else {
             webix.alert("An error occoured, please refreh page and try again");
           }
         });
      break;
    case "load-uploaded-image-ckeditor":
      alert(data.t);
      break;
    case "add-element-content-variation":
      break;
    case "update-element-content-variation":
      break;
    case "edit-content-element-variation":
      break;
    case "get-page-data":
      var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
      var iframe_edit = $('iframe')[index];
      var userStateData = null ;
      if(parent_accessible){
        userStateData = parent.userStateData ;
      }
      iframe_edit.contentWindow.postMessage({
        t: "set-changed-content",
        originalUrl: pageExistingData.originalUrl,
        inlineContent: pageExistingData.inlineContent,
        pageElement: pageExistingData.pageElement,
        experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
        source: basicPlacementWindowFormValues.source,
        page: page,
        variations: pageExistingData.variations,
        userStateData: userStateData
      }, allFormDataPlacementWindowGoal.data.url);
      break;
    case "send-webix-message":
      webix.message(data.message);
      break;
    case "hide_element":
      showView('content_options');
      break;
    case 'test-allassetloadcomplete' :
      // Process testbench fields
      if(testbench.isFirstTime === false){
        processTestBenchData() ;
        return ;
      }

      let page_element_id = ''
      if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null && basicPlacementWindowFormValues.formData._id){
        page_element_id = basicPlacementWindowFormValues.formData._id;
      }
      $.ajax({
         url: monolop_api_base_url+"/api/page-element/testbench",
         type: "GET",
         data: { url: $$("customerSiteUrlLink").getValue(),page_element_id: page_element_id, selected_experiment_id: selected_experiment_id},
         beforeSend: function( xhr ) {
           xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
           xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
         }
       })
       .done(function( response ) {
         if( response.success == true){
           testbench.mongoDetail = response.mongoDetail ;
           generateTestBenchPanel(response.testbench) ;
           processTestBenchData() ;
         }
       });
      break;

    /*---- from monoloop thrid party extension ---*/
    case 'remove-wordpress-admin-bar' :
      setTimeout(function(){
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
        iframe_edit.contentWindow.postMessage({
          t: "remove-wordpress-admin-bar" ,
          data:{}
        }, postMessageConfig.url);
      }, 2000);
   case "resize-toolbar":
      webix.toNode("placement_window").style.width = data.data.width+"px";
      placement_window_toolbar.adjust();
    break;
  case "set-add-goal-url":
    $('div[view_id="customerSiteUrlLink"] input').val(data.data.fullURL);
    break;
  }
};
// insert postmessageListener ;
if (window.addEventListener) {
  addEventListener("message", postMessageListener, false);
} else {
  attachEvent("onmessage", postMessageListener);
}

function highLightCurrentPlacementWindowGoal(current) {
  $(".pw_wizardBtn").removeClass('active');
  switch (current) {
    case "pw":
      $(".pw_wizardBtn.pw").addClass('active');
      break;
    case "edit_record":
      $(".pw_wizardBtn.edit_record").addClass('active');
      break;
    case "testbench":
      $(".pw_wizardBtn.testbench").addClass('active');
      break;
    case "publish":
      $(".pw_wizardBtn.publish").addClass('active');
      break;
    default:
      break;
  }
}

//By SJ
function savePage(){
  var url = monolop_api_base_url+'/component/page-list-publish/store';
  var urlOption = '';
  var regExp='';
  var remark='';
  var experiment_id='';
  var page_element_id='';
  var isExistingPageELementID = false;

  if (basicPlacementWindowFormValues.urlOptData != null && basicPlacementWindowFormValues.mode == 'add'){
    urlOption = basicPlacementWindowFormValues.urlOptData.urlOpt;
    regExp = basicPlacementWindowFormValues.urlOptData.regExp;
    remark = basicPlacementWindowFormValues.urlOptData.regExp;
  }else if (basicPlacementWindowFormValues.mode == 'edit' && basicPlacementWindowFormValues.source == "experiment" && basicPlacementWindowFormValues.formData[0] !=undefined){
    urlOption = basicPlacementWindowFormValues.formData[0].urlOption;
    regExp = basicPlacementWindowFormValues.formData[0].regExp;
    remark = basicPlacementWindowFormValues.formData[0].remark;
    page_element_id = basicPlacementWindowFormValues.formData[0]._id;
  }else {
    urlOption = basicPlacementWindowFormValues.formData.urlOption;
    regExp = basicPlacementWindowFormValues.formData.regExp;
    remark = basicPlacementWindowFormValues.formData.remark;
    page_element_id = basicPlacementWindowFormValues.formData._id;
  }

  if (currentPageElement.added){
    if(currentPageElement.data._id !== undefined){
      page_element_id = currentPageElement.data._id;
    }
  }else
  if(page_element_id == ''){
    page_element_id = urlParams.page_element_id;
  }

  if(page_element_id != ''){
    isExistingPageELementID = true;
  }

  if (basicPlacementWindowFormValues.experiment !=undefined){
    experiment_id = basicPlacementWindowFormValues.experiment._id;
  }else if(parent_accessible){
    if(parent.userStateData){
      if(parent.userStateData.data.publishData){
        experiment_id  = parent.userStateData.data.publishData.experiment_id;
      }
    }
  }else if (basicPlacementWindowFormValues.formData[0].Experiment[0]._id){
    experiment_id = basicPlacementWindowFormValues.formData[0].Experiment[0]._id;
  }else{
    experiment_id = urlParams.experiment_id;
  }

  var pagePostDataObj={
    // fullURLWebPageList: basicPlacementWindowFormValues.fullURL,
    fullURLWebPageList: $$("customerSiteUrlLink").getValue(),
    includeWWWWebPageElement: returnUrlConfig.inc_www,
    includeHttpHttpsWebPageElement: returnUrlConfig.inc_http_https,
    urlOptionWebPageElement: urlOption,
    regularExpressionWebPageElement: regExp,
    remarkWebPageList: remark,
    source: basicPlacementWindowFormValues.source,
    page_element_id: page_element_id,
    experiment_id: experiment_id,
    isExistingPageELementID: isExistingPageELementID,
    currentFolder: basicPlacementWindowFormValues.currentFolder
  };

  var	temp = basicPlacementWindowFormValues.temp;

  webix.ajax().post(url, pagePostDataObj, {
    error: function(text, data, XmlHttpRequest) {
      alert("error");
    },
    success: function(text, data, XmlHttpRequest) {
      var response = JSON.parse(text);
      if (response.status === 'success') {
          window.postMessage(JSON.stringify({t: "page-published", source: basicPlacementWindowFormValues.source, experiment: response.experiment, pageElement: response.content, temp: temp, message: 'Page is successfully published.'}), '*');
      }
    }
  });

}

function showViewPlacementWindowGoal(placementWindowGoal, editRecordGoal, testBench, publish, modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal) {

    if (isPlacementWindowGeneratedGoal) {

      if (placementWindowGoal) {
        highLightCurrentPlacementWindowGoal("pw");

        $('#customerSiteUrlPlacementWindowGoalSection').show();
      } else {
        $('#customerSiteUrlPlacementWindowGoalSection').hide();
      }
    }
    if (isEditModeGeneratedGoal) {
      if (editRecordGoal) {
        if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
          highLightCurrentPlacementWindowGoal("edit_record");
          openEditRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
          $$("webixWizardEditRecordGoalId").show();
          initializeGuidesAndTipsOnPlacementWindow('Edit');
          // $$("webixPlacementWindowSearchHeader").hide();
        } else {
          webix.message("Enter the URL and press go.");
        }
        // $$("webixPlacementWindowSearchHeader").hide();
      } else {
        $("#customerSiteUrlEditRecordSection").hide();
      }
    } else {
      if (editRecordGoal) {
        if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
          // $$("webixPlacementWindowSearchHeader").hide();
          openEditRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
          // start placementWindowEdit Guidelines
          //guidelines.startGuideline('placementWindowEdit', undefined, 'tip');
          initializeGuidesAndTipsOnPlacementWindow('Edit');
        } else {
          webix.message("Enter the URL and press go.");
        }
      }
    }

    if (isTestBenchGeneratedGoal) {
      if (testBench) {
        if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
          highLightCurrentPlacementWindowGoal("testbench");
          openTestRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
          $$("webixWizardEditRecordGoalId").show();
          // $$("webixPlacementWindowSearchHeader").hide();
          initializeGuidesAndTipsOnPlacementWindow('Test');
        } else {
          webix.message("Enter the URL and press go.");
        }

        // $$("webixPlacementWindowSearchHeader").hide();
      } else {
        $("#customerSiteTestBenchSection").html("");
      }
    } else {
      if (testBench) {
        if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
          // $$("webixPlacementWindowSearchHeader").hide();
          openTestRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
          // start placementWindowTest Guidelines
          // guidelines.startGuideline('placementWindowTest', undefined, 'tip');
          initializeGuidesAndTipsOnPlacementWindow('Test');
        } else {
          webix.message("Enter the URL and press go.");
        }
      }
    }

    if (isPublishGeneratedGoal) {
      if (publish) {
        if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
          highLightCurrentPlacementWindowGoal("publish");
          openPublishGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
          $$("webixWizardPublishGoalId").show();
          initializeGuidesAndTipsOnPlacementWindow('Publish');
          // $$("webixPlacementWindowSearchHeader").hide();
          // start placementWindowTest Guidelines
          // guidelines.startGuideline('placementWindowPublish', undefined, 'tip');
        } else {
          webix.message("Enter the URL and press go.");
        }
        // $$("webixPlacementWindowSearchHeader").hide();
      } else {
        $("#customerSitePublishSection").html("");
      }
    } else {
      if (publish) {
        if(isPlacementWindowGeneratedGoal || isEditModeGeneratedGoal || isTestBenchGeneratedGoal || isPublishGeneratedGoal){
          // $$("webixPlacementWindowSearchHeader").hide();
          openPublishGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal);
          initializeGuidesAndTipsOnPlacementWindow('Publish');
        } else {
          webix.message("Enter the URL and press go.");
        }
      }
    }

}

function showViewPlacementWindowTestbench(){
  if(isPlacementWindowGeneratedGoal === false){
    webix.message("Enter the URL and press go.");
    return ;
  }
  highLightCurrentPlacementWindowGoal("testbench");
  $('#customerSiteUrlPlacementWindowGoalSection').hide();
  // $$("webixPlacementWindowSearchHeader").hide();

  openTestRecordPlacementWindowGoalSection();

  testbench.isFirstTime = true ;
}
function getPWHeight(defH){
  defH = defH || 230;
  var h = 990;
  if(h < window.innerHeight){
    h = window.innerHeight;
  }
  // h = h - 49 - 20 - 65 - 55;
  h = h - defH;
  return h;
}

function resizeIframe() {
   $("#main div").css('width', '100%');
    var $iframes = $("iframe");
    $iframes.each(function() {
        var iframe = this;
        $(iframe).load(function() {
            iframe.height = iframe.contentWindow.document.body.scrollHeight + 35;
        });
    });
}


function openPlacementWindowGoalSection() {

  if(parent_accessible){
    checkClientProtocalForHttps();
  }
  fullscreen.updateHeight();
  currentStepPWGoal = 'pw';
  highLightCurrentPlacementWindowGoal("pw");
  $("#customerSiteUrlPlacementWindowGoalSection").html('');
  // $$("webixPlacementWindowSearchHeader").show();

  isPlacementWindowGeneratedGoal = true;

  var linkPlacementWindow = $$("customerSiteUrlLink").getValue();
  customerSiteUrlLink = linkPlacementWindow;
  linkPlacementWindow  = linkPlacementWindow.trim();
  linkPlacementWindow = refineUrl(linkPlacementWindow);
  // if(linkPlacementWindow.indexOf('?') === -1){
  //   linkPlacementWindow += '?';
  // } else {
  //   linkPlacementWindow += '&';
  // }
  // linkPlacementWindow += 'ver=' + (new Date()).getTime() + Math.floor(Math.random() * 1000000);

  if (is_valid_url(linkPlacementWindow)) {
    $('#customerSiteUrlEditRecordSection').html('');
    webix.ui({
      container: "customerSiteUrlPlacementWindowGoalSection",
      id: "webixWizardPlacementWindowGoalId",
      // height: getPWHeight(0),
      height: $(window).height()-60,
      //height: window.innerHeight,
      //height: 1990,
      css: "customerSiteUrlPlacementWindowGoalSection",
      rows: [{
        view: "iframe",
        id: "PWIframePlacementWindowGoal",
        src: linkPlacementWindow,
        css: "pw_iframe",
        borderless: true,
        on: {
          onAfterLoad: function() {
          // $$('PWIframePlacementWindowGoal').define('height', $(window).height());
          // $$('PWIframePlacementWindowGoal').resize();
            is_load_buddle = false;
            hidePageProgressBar("webixWizardPlacementWindowGoalId");
            $('#customerSiteUrlEditRecordSection').html('');
            var view = this;
            //resizeIframe();
            customerSiteUrlLink = $$("customerSiteUrlLink").getValue();
            parent.postMessage(JSON.stringify({
              t: "set-active-step-sessional",
              step: "pw",
              url: customerSiteUrlLink,
              data: allFormDataPlacementWindowGoal.data
            }), "*");

            if (postMessageConfig.stage == 1) {
              //found comfirm
              canWeConfirm = true;
              //change to re-confirm
              postMessageConfig.stage = 2;
              setTimeout(function(){ enableEditActions(); }, 2000);

            } else {
              //delay 1 second
              setTimeout(function() {
                canWeConfirm = false;
                disableEditActions();
                if (postMessageConfig.stage != 1) {
                  //seem we not found invocation
                  //change to proxy mode ;
                  // webix.alert(scriptNotExistsMsg);
                  // $('iframe')[0].src =  monolop_base_url + '/placement-proxy/preview?l=' +  urlencode($('iframe')[0].src);
                  postMessageConfig.stage = 3;

                } else {
                }
              }, 1000);
              if (!traversingHistoryPWGoal) {
                addnewHistoryUrlGoal(customerSiteUrlLink);
              }
              traversingHistoryPWGoal = false;
              allFormDataPlacementWindowGoal.data.url = customerSiteUrlLink;

            }

            //edit record section
            if(enableEditmode){
              webix.ui({
                container: "customerSiteUrlEditRecordSection",
                css: "customerSiteUrlEditRecordSection",
                id: "webixWizardEditRecordGoalId",
                // height: screen.height,
                // height: getPWHeight(0),
                height: $(window).height()-55,
                borderless: true,
                cols: [{
                 id: 'PWsidebarpanel',
                 css: 'sidebar_panel sidebar_width',
                 rows :[
                   {
                     id: "variation_options",
                     css: "variation_options dark-bg",
                     template:'<i id="sidebar_icon" onclick="sidebarcollapse_close()" class="fa fa-chevron-left" aria-hidden="true" style="display: inline; font-size: 20px;position:absolute;right:25px;top:10px;"></i><div id="experiment_list_container" style="margin-top: 20px;"></div><div style="font-size: large;font-weight: bold; height: 25px !important; width: 260px !important; margin-top: 40px;" id="variationGuides"></div><div id="content_list_comp" style="padding: 0px 0px 5px 0px;"></div><div id="var_action" style ="font-size: large;font-weight: bold; opacity: 0;">Actions</div><div id="content_options" style="opacity:0;"></div><div id="add_variation"></div><div id="add_addcontent"></div><div id="hide_content"></div><div id="update_variation"></div><div id="create_new_exp_container"></div>	<div id="experiment_container"></div>'
                   }
                 ]
               }
               ,{
                 id: "xpand_variation",
                 css: "sidebar_expand dark-bg",
                 template: '<div style="display: block;position:relative;"><i id="sidebar_icon" class="fa fa-chevron-right" aria-hidden="true" style="display: inline; font-size: 20px;position:absolute;right:7px;"></i></div>',
                 onClick: {
                   "fa" : function(e, id, trg){
                     setTimeout(function(){
                          sidebarcollapse_expand();
                     }, 200);

                   }
                 }
               },{
                             view: "iframe",
                             id: "PWIframePlacementWindowEditModeGoal",
                             src: $$("customerSiteUrlLink").getValue(),
                             css: "pw_iframe PWIframePlacementWindowEditModeGoal",
                             borderless: true,
                             on: {
                               onAfterLoad: function() {
                                //  $$('PWIframePlacementWindowEditModeGoal').define('height', $(window).height());
                                //  $$('PWIframePlacementWindowEditModeGoal').resize();
                                 $('#customerSiteUrlEditRecordSection').hide();
                               }
                             }
                           }
             ]

             });
             showPageProgressBar("webixWizardEditRecordGoalId");
           }
         }
       }
     }]
   });

   showPageProgressBar("webixWizardPlacementWindowGoalId");

  } else {
    webix.message("Please enter the valid URL to browse.");
  }
  // $$("browseBtnPlacementWindowGoal").enable();

}
function sidebarcollapse_close(){
  webix.html.addCss( $$('PWsidebarpanel').getNode(), "sidebar_width");
  webix.html.removeCss( $$('PWsidebarpanel').getNode(), "sidebar_width_resize");
    // $('#content_list_comp').html('');
    // $('#content_options').html('');
    // $('#add_variation').html('');
    // $('#update_variation').html('');
    // $('#create_new_exp_container').html('');
    // $('#experiment_container').html('');
    // $('#variationGuides').html('');
    hidden_flag = false;
}
function sidebarcollapse_expand(){

  // changing width of the div


  hidden_flag = true;
  if (check_selected_Item) {
    webix.html.removeCss( $$('PWsidebarpanel').getNode(), "sidebar_width");
    webix.html.addCss( $$('PWsidebarpanel').getNode(), "sidebar_width_resize");
    $("#variationGuides").html("No element is Selected! \n Please Select any element to open Variation Editor");
    $("#variationGuides").show();
    $('#var_action').html('');
  }else {
    if ($('#add_variation').css('display') == 'block' || $('#update_variation').css('display') == 'block' || $('#create_new_exp_container').css('display') == 'block' || $('#experiment_container').css('display') == 'block') {
      webix.html.removeCss( $$('PWsidebarpanel').getNode(), "sidebar_width");
      webix.html.addCss( $$('PWsidebarpanel').getNode(), "sidebar_width_resize");
    }else {
      SB_content_options(bubble_content);
    }

  }



  // change Icon Class
  // $('#sidebar_icon').toggleClass('fa-chevron-left fa-chevron-right');
  // Adjusting Bubble and Borders Position on expand
  // var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
  // var iframe_ = $('iframe')[index];
  // iframe_.contentWindow.postMessage({
  //   t: 'adjust-bubble-borders-position',
  // },'*');
  // change css of views
  // if ($('#sidebar_icon').hasClass("fa fa-chevron-right")) { // collapse
  //   $('#content_list_comp').html('');
  //   $('#content_options').html('');
  //   $('#add_variation').html('');
  //   $('#update_variation').html('');
  //   $('#create_new_exp_container').html('');
  //   $('#experiment_container').html('');
  //   $('#variationGuides').html('');
  //   $$("variation_options").hide();// hiding the variation options view
  //   hidden_flag = false;
  //   $('#sidebar_icon').css("margin-left","0px");
  //   $("#var_title").attr("hidden","true");
  //   webix.html.removeCss( $$('PWsidebarpanel').getNode(), "webix_layout_head");
  //   webix.html.addCss( $$("PWsidebarpanel").getNode(), "sidebar_collapse");
  //   //
  //   // webix.html.removeCss( $$('PWIframePlacementWindowEditModeGoal').getNode(), "iframe_collapse");
  //   // webix.html.addCss( $$("PWIframePlacementWindowEditModeGoal").getNode(), "pw_iframe PWIframePlacementWindowEditModeGoal");
  // }else if ($('#sidebar_icon').hasClass("fa fa-chevron-left")) { // expand
  //   $('#sidebar_icon').css("margin-left","45%");
  //   $('#var_title').removeAttr('hidden');
  //   $("#var_title").attr("dispay","inline");
  //   $("#var_title").attr("dispay","block");
  //   webix.html.removeCss( $$('PWsidebarpanel').getNode(), "sidebar_collapse");
  //   webix.html.addCss( $$("PWsidebarpanel").getNode(), "webix_layout_head");
  //   //
  //   // webix.html.removeCss( $$('PWIframePlacementWindowEditModeGoal').getNode(), "pw_iframe PWIframePlacementWindowEditModeGoal");
  //   // webix.html.addCss( $$("PWIframePlacementWindowEditModeGoal").getNode(), "iframe_collapse");
  //   $$("variation_options").show();
  //   hidden_flag = true;
  //   if (check_selected_Item) {
  //     $("#variationGuides").html("No element is Selected! \n Please Select any element to open Variation Editor");
  //   }
  //   else {
  //     SB_content_options(bubble_content);
  //   }
  // }

}
function SB_content_options(type){
      webixDataSet = [];
      pagedDataSet = [];
      contentListResponse = undefined;
      currentVariation = false;

      $("#content_list_comp").html('');
      $("#content_options").html('');

      listProgresser("content_list_comp", "list");
      listProgresser("content_options", "list");


      var guideType = 'placementWindowEditContentOptions';
      /*
      var ops = [
        {
          "id":1,
          "title":"Add variation",
          "rank":"1",
          "info":"Change whatever you like in the highligted content element using the inline editor. When you are finished simply click this option."
        }
      ];
      */
      //
      var ops = [];
      if(urlParams.tagName.toLowerCase() == "img"){
        // Mantis#4714 changes
        // openImageUploader();
        // showView('add_variation');
        // ops.push(
        //   {
        //     "id":"5img",
        //     "title":"Add Variation",
        //     "rank":"1",
        //     // "info":"Change image"
        //   }
        // );
      }else{
        // if (type === false) {
        //   ops.push(
        //     {
        //       "id":"1Add",
        //       "title":"Add variation",
        //       "rank":"1",
        //       // "info":"Change whatever you like in the highligted content element using the inline editor. When you are finished simply click this option."
        //     }
        //   );
        // }

      }
      ops.push(
        // {
        //   "id":"2AddContent",
        //   "title":"Add Content",
        //   "rank":"2",
        //   // "info": "Select the content element you want to hide and select this option."
        // }
      );
      if(urlParams.havVar == 1){
        // ops.push(
        //   {
        //     "id":"3Addexp",
        //     "title":"Add to experience",
        //     "rank":"3",
        //     // "info": "All variations and their audience conditions are linked to a new or existing experience."
        //   }
        // );
        guideType = 'placementWindowEditContentOptionsHasVar';
      }
      // ops.push(
      //   {
      //     "id":"4Hide",
      //     "title":"Hide",
      //     "rank":"4",
      //     // "info": "Select the content element you want to hide and select this option."
      //   }
      // );

      if(urlParams.content_id){
        ops.push(
          {
            "id":"6Updatevar",
            "title":"Update variation",
            "rank":"6"
          }
        );
        guideType = 'placementWindowEditContentOptionsUpdate';
      }

      //startGuidesIcon(guideType);  marc remove

      $("#content_options").empty();
      urlParams.selected_experiment_id = selected_experiment_id;
      webix.ajax().get(monolop_api_base_url+'/api/component/content-list', urlParams , function(response, xml, xhr) {
        response = JSON.parse(response);
        contentListResponse  = response;

        var variations = response.variations,
            totalCount = 0;
        variations = variations || [];
        if(variations.length > 0){
          webix.html.removeCss( $$('PWsidebarpanel').getNode(), "sidebar_width");
          webix.html.addCss( $$('PWsidebarpanel').getNode(), "sidebar_width_resize");
        }
        var originalVariationContent = {
          id: "thisistheorignalcontentid",
          title: original.title + titleIdSeperator + "thisistheorignalcontentid",
          hidden: 0,
          content: original.content,
          deleted: 0,
          mappedTo: urlParams.xpath,
          original: true,
        };

        totalCount = variations.length;

        webix.ready(function() {

          if(totalCount > 0){
            for (var i = 0; i < totalCount; i++) {
              webixDataSet.push({
                id: variations[i].content._id,
                title: variations[i].content.name + titleIdSeperator + variations[i].content._id,
                hidden: variations[i].content.hidden,
                content: variations[i].content.config.content,
                condition: variations[i].content.condition,
                deleted: variations[i].content.deleted,
                mappedTo: variations[i].mappedTo,
                original: false,
              });
            }

            var a = webixDataSet;
            while (a.length > 0)
                pagedDataSet.push(a.splice(0, page_CM.size - 1));

            if(pagedDataSet.length === 0){
              pagedDataSet.push([]);
            }
            $('#variationGuides').html('Variations');
            $("#content_list_comp").html('');
            pagedDataSet[page_CM.current].unshift(originalVariationContent);

            webix.ui({
                 container:"content_list_comp",
                 id: "content_list_comp",
                 width: 240,
                 height: 146,
                 scroll:true,
                 view:"grouplist",
                 scheme:{
                   $group:function(obj){
                     return obj.title;
                   },
                 },

                 templateBack: function(obj){
                   //$(".guideIcon").hide();
                   $$('content_options').hide();
                   $('.webix_grouplist').css('height', '341px');
                   $('#variationGuides').html('Variation Editor');
                   $('#var_action').html('');
                   var value = getTitle(obj.value);
                  //  startGuidesIcon('placementWindowEditContentListVariation'); Imran remove
                   // initializeGuidesAndTips('placementWindowEditContentListVariation');
                   return value;
                 },
                 templateGroup: function(obj){
                   //$(".guideIcon").hide();
                   if($$('content_options')){
                     $$('content_options').show();
                     $('.webix_grouplist').css('height', '146px');
                     $('#variationGuides').html('Variations');
                     $('#var_action').html('Actions');
                   }


                   var value = getTitle(obj.value);
                  //  startGuidesIcon('placementWindowEditContentListVariation'); Imran remove
                   // initializeGuidesAndTips('placementWindowEditContentListVariation');
                   return value;
                 },
                 templateItem: function(obj){
                   //$(".guideIcon").show();
                   currentVariation = obj;
                   var html = "",
                       id = obj.id,
                       condition = currentVariation.condition;
                   if(obj.original === false){
                     if(obj.hidden == 1){
                       html = '<a href="javascript:void(0);" title="inactive" id="updateHidden_'+id+'" onclick=\'updateHidden( "' + id + '", 0);\' style="color:white;padding: 1px 7px;" class="updateStatus inactive status"><span class="webix_icon fa-toggle-off status1" style="border-radius: 7px;height: 15px;"></span></a>';
                     } else {
                       html = '<a href="javascript:void(0);" title="active" id="updateHidden_'+id+'" onclick=\'updateHidden( "' + id + '", 1);\' style="color:white;padding: 1px 7px;" class="updateStatus active status"><span class="webix_icon fa-toggle-on status0" style="border-radius: 7px;height: 15px;"></span></a>';
                     }
                     html += '&nbsp;|&nbsp;&nbsp; <a href="javascript:void(0);" id="updateVariation_'+id+'" onclick=\'showView("edit_content", "' + id + '");\' style="color:white;" class="updateVariationIcon"><span class="webix_icon fa-edit updateVariation"></span></a>';
                     html += '&nbsp;&nbsp;|&nbsp;&nbsp; <a href="javascript:void(0);" title="' + condition + '" id="updateVariation_'+id+'" onclick=\'openCondition("' + id + '");\' style="color:white;" class="updateVariationConditionIcon"><span class="webix_icon fa-code updateVariationCondition"></span></a>';
                     html += '&nbsp;&nbsp;|&nbsp;&nbsp; <a href="javascript:void(0);" id="deleteVariation_'+id+'" onclick=\'deleteVar( "' + id + '", '+totalCount+');\' style="color:white;" class="deleteVariationIcon"><span class="webix_icon fa-times deleteVariation"></span></a>';
                     if(basicPlacementWindowFormValues.source != "experiment"){
                       html += '&nbsp;&nbsp;|&nbsp;&nbsp; <a href="javascript:void(0);" title = "Change Experience" id="changeExperience_'+id+'" onclick=\'updateSelectedExp( "' + id + '");\'  style="color:white;" class="deleteVariationIcon"><span class="icon icon-beaker deleteVariation"></span></a>';
                     }
                   }

                   html += '<p class="previewCont">Preview1</p> <hr>';

                   html += '<div style="height: 205px;overflow-y: auto;" >' + obj.content + '</div>';
                   return html;
                 },
                 type:{
                   height:"auto"
                 },
                 select:false,

                 data:pagedDataSet[page_CM.current],
            });
          }else {
            $("#content_list_comp").html('');
            $('#variationGuides').html('');
            // $("#paging_here").html('');
            // 	webix.ui({
            // 			 container:"content_list_comp",
            // 			 id: "content_list_comp",
            // 			 view:"label",
            // 			 label: "No variation added yet.",
            //
            // 	})
          }

          var list = webix.ui({
            container:"content_options",
            id: "content_options",
            view:"list",
            width:240,
            height: 'auto',
            scroll: false,
            // template:"#title#",
            template: function(obj){
              var _html = '';
              _html += '<div class="item">';
              _html += '<div class="title">' + obj.title;

              // if(obj.info)
              // 	_html += '<a class="hide-below" href="javascript:void(0);">Hide below</a>';

              _html += '</div>';

              if(obj.info){
                  _html += '<div class="info">' + obj.info + '</div>';
              }

              _html += '</div>';
              return _html;
            },
            select:true,
            data:ops
          });

          $('.webix_list').height('auto');

          if(MONOloop.jq('#content_options')[0].childElementCount > 1){
            for (var i = 1; i < MONOloop.jq('#content_options')[0].childElementCount; i++) {
              $(MONOloop.jq('#content_options')[0].children[i]).remove();
            }
          }
          // $( 'div[webix_l_id="1Add"]' ).on({
          //   mouseenter: function() {
          //     $( 'div[webix_l_id="1Add"]' ).attr('title', 'Change whatever you like in the highligted content element using the inline editor. When you are finished simply click this option.');
          //   }
          // });
          // $( 'div[webix_l_id="2AddContent"]' ).on({
          //   mouseenter: function() {
          //     $( 'div[webix_l_id="2AddContent"]' ).attr('title', 'Change whatever you like in the highligted content element using the inline editor. When you are finished simply click this option.');
          //   }
          // });
          $( 'div[webix_l_id="3Addexp"]' ).on({
            mouseenter: function() {
              $( 'div[webix_l_id="3Addexp"]' ).attr('title', 'All variations and their audience conditions are linked to a new or existing experience.');
            }
          });
          // $( 'div[webix_l_id="4Hide"]' ).on({
          //   mouseenter: function() {
          //     $( 'div[webix_l_id="4Hide"]' ).attr('title', 'Select the content element you want to hide and select this option.');
          //   }
          // });
          $( 'div[webix_l_id="5img"]' ).on({
            mouseenter: function() {
              $( 'div[webix_l_id="5img"]' ).attr('title', 'Change image');
            }
          });
          $('.closebtn').on({
            mouseenter: function() {
              $( '.closebtn' ).attr('title', 'Close Side Panel');
            }
          });
          $('#var_action').html('Actions');
          $$("content_options").attachEvent("onItemClick", function(id, e, node){
              var item = this.getItem(id);
              switch (id) {
                // case "1Add":
                //   $$('content_list_comp').hide();
                //   $('#var_action').html('');
                //   $('#variationGuides').html('');
                //   showView('add_variation');
                //   break;
                // case "2AddContent":
                //   $$('content_list_comp').hide();
                //   $('#var_action').html('');
                //   $('#variationGuides').html('');
                //   var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                //   var iframe_ = $('iframe')[index];
                //   iframe_.contentWindow.postMessage({t: 'add_addcontent-destroyCKE'}, urlParams.fullURL);
                //   showView('add_addcontent');
                //   break;
                case "3Addexp":
                $$('content_list_comp').hide();
                $("#create_new_exp_container").html('');
                $("#experiment_container").html('');
                $('#variationGuides').html('');
                $('#var_action').html('');

                var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                var iframe_ = $('iframe')[index];
                iframe_.contentWindow.postMessage({t: 'get-new-experiment-backup'}, urlParams.fullURL);

                  break;
                // case "4Hide":
                //   var data = {};
                //   data.type = "variation";
                //   data.variation_name = "Hidden";
                //   data.hide = true;
                //   data.condition = "";
                //   data.fullURL = urlParams.fullURL;
                //   data.page_element_id = urlParams.page_element_id;
                //   data.experiment_id = urlParams.experiment_id;
                //   var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                //   var iframe_ = $('iframe')[index];
  							// 	iframe_.contentWindow.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
                //
                //   break;
                case "6":
                  showView('update_variation');
                  break;
                case "5img":
                  // parent.parent.postMessage(JSON.stringify({t: "open-image-uploader"}), monolop_base_url);
                  $('#var_action').html('');
                  $('#variationGuides').html('');
                  openImageUploader();
                  showView('add_variation');
                  break;
                default:
                  break;
              }
          });
        });

      });
      // var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
      // var iframe_edit = $('iframe')[index];
      // iframe_edit.contentWindow.postMessage({
      //   t: "revert_all_changes"
      // }, urlParams.fullURL);
}


function pageOptions_init(){

  if(urlParams.page_element_id.trim().length > 0){
  	pageOptions.fullURLWebPageList = urlParams.fullURL;
  	// pageOptions.includeWWWWebPageElement = 1;
  	// pageOptions.includeHttpHttpsWebPageElement = 0;
  	// pageOptions.urlOptionWebPageElement = 0;
  	// pageOptions.regularExpressionWebPageElement = '{{ $regularExpressionWebPageElement }}';
  	// pageOptions.remarkWebPageList = '{{ $remarkWebPageList }}';
  }
}

function showView(t, id){
	t = t || 'content_options';

	switch (t) {
		case 'content_options':
			// $("#back").hide();
			$("#add_variation").hide();
			$("#content_options").show();

			$("#add_variation").empty();
			$("#add_to_experiment").empty();
			$("#experiment_container").empty();
			$("#create_new_exp_container").empty();
			$("#update_variation").empty();
      $("#add_addcontent").empty();
      $("#hide_content").empty();


			SB_content_options(bubble_content);
			break;
		case 'add_variation':
			// $("#back").show();
			// toolbar();
			$("#content_options").hide();
			$("#update_variation").hide();
			$("#add_variation").show();
			add_variation();
			break;
    case "hide":
      $("#content_options").hide();
      $("#update_variation").hide();
      $("#hide_content").show();
      hidecontent();
      break;
    case 'add_addcontent':
      $("#content_options").hide();
      $("#update_variation").hide();
      $("#add_addcontent").show();
      add_addcontent();
      break;
		case 'update_variation':
			// $("#back").show();
			// toolbar();
			$("#content_options").hide();
			$("#add_variation").hide();
			$("#update_variation").show();
			update_variation();
			break;
		case 'content':
			$("#content_options").hide();
			$("#add_variation").hide();
			$("#update_variation").hide();

			$("#experiment_container").hide();
			$("#create_new_exp_container").show();

			showNewExperiment();

			// init();
			break;
		case 'experiment':
			$("#content_options").hide();
			$("#add_variation").hide();
			$("#update_variation").hide();

			$("#create_new_exp_container").hide();
			$("#experiment_container").show();

			showExperiment();
			break;
		case 'edit_content':
      var tagName = urlParams.tagName.toLowerCase();
			postMessageInlineVar(id);
			$("#content_list_comp").hide();
			$("#update_variation").show();
      $('#variationGuides').html('');
			update_variation(id);
      if(tagName == "img" || tagName.includes("image")){
        openImageUploader();
      }
			break;
		default:
			break;

	}
}
function toolbar(){
	$("#back").empty();
	webix.ui({
		view:"toolbar",
		id:"myToolbar",
		cols:[
				{ view:"button", value:"back", width:100, align:"right" }]
	});
}
function updateHidden(id, hidden){
  webix.ajax().post(apis.update_status, {
    content_id: id,
    hidden: hidden
  }, {
    error: function(text, data, XmlHttpRequest) {
      alert("error");
    },
    success: function(text, data, XmlHttpRequest) {
      var response = JSON.parse(text);
      if (response.status === 'success') {

        var _id = response.content._id,
            _hidden = response.content.hidden;

        var element = $("#updateHidden_" + _id);
        if (_hidden == "0") {
          element.text("active");
          element.removeClass("status1");
          element.addClass("status0");
          element.prop('onclick',null).off('click');
          element.on('click', function(e){
            updateHidden(_id, 1);
          });
        } else if (_hidden == "1") {
          element.text("inactive");
          element.removeClass("status0");
          element.addClass("status1");
          element.prop('onclick',null).off('click');
          element.on('click', function(e){
            updateHidden(_id, 0);
          });
        }


        if(pagedDataSet[page_CM.current].length > 0){
          for (var i = 0; i < pagedDataSet[page_CM.current].length; i++) {
            if(pagedDataSet[page_CM.current][i].id === _id){
              pagedDataSet[page_CM.current][i].hidden = parseInt(_hidden);
            }
          }
        }

        $$("content_list_comp").refresh();
      }
    }
  });
}
function postMessageInlineVar(id){
  webix.ajax().post(monolop_api_base_url+'/api/page-element/content-show', {content_id: id} , function(response, xml, xhr) {

    //response
    response = JSON.parse(response);
    var content = response.content;

    if(response.status === "success"){

      var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
      var iframe_ = $('iframe')[index];
      iframe_.contentWindow.postMessage({
        t: "init-FAB-from-edit-variation",
        content_id: content._id,
        name: content.name,
        condition: content.condition,
        xpath: urlParams.xpath1,
        content: content.config.content
      }, "*");
    }

  });
}
function deleteVar(id, variationCount){
  webix.ajax().post(apis.delete, {
    content_id: id,
    deleted: 1
  }, {
    error: function(text, data, XmlHttpRequest) {
      alert("error");
    },
    success: function(text, data, XmlHttpRequest) {
      var response = JSON.parse(text);
      if (response.status === 'success') {
        // init();
        webix.message("Deleted!");

        if(webixDataSet.length > 0){
          for (var i = 0; i < webixDataSet.length; i++) {
            if(webixDataSet[i].id === id){
              webixDataSet.splice(i, 1);
            }
          }
        }
        if(pagedDataSet[page_CM.current].length > 0){
          for (var i = 0; i < pagedDataSet[page_CM.current].length; i++) {
            if(pagedDataSet[page_CM.current][i].id === id){
              pagedDataSet[page_CM.current].splice(i, 1);
            }
          }
        }
        if(pagedDataSet[page_CM.current].length === 0){
          if(page_CM.current !== 0){
            page_CM.current--;
          }
        }
        pagedDataSet = [];
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_ = $('iframe')[index];
        iframe_.contentWindow.postMessage({
          t: "remove-or-decrease-variation-bubble-from-list",
          fullURL: urlParams.fullURL,
          remove_popup: false,
          xpath: urlParams.xpath,
          variationCount: variationCount - 1,
        }, "*");
        init();
      }
    }
  });
}

function updateSelectedExp(id){
  var updated_experiment_id = selected_experiment_id;
  postMessageInlineVar(id);
  $("#content_list_comp").hide();
  $("#update_variation").show();
  $('#variationGuides').html('');
  $("#update_variation").empty();
  var __icon3Form = [{
		// view: "layout",
		// height: 520,
		width: 240,
		rows: [{
			view:"label",
			label: "<b>Update Experience</b>",
			align:"left"
		},{
    view: "richselect",
    id: "updateExperimentList",
    name: "updateExperimentList",
    value: selected_experiment_id,
    options:allExperiments,
    on: {
       onChange: function(newValue, oldValue){
         if(newValue == selected_experiment_id){
           $$("variation_exp_update_btn").disable();
         }else{
           $$("variation_exp_update_btn").enable();
         }
           updated_experiment_id = newValue;
       }
   }
 },
		{
				cols: [
					{

					}, {
						view: "button",
						value: "Back",
						id: "go_back",
						width: 60,
						click: function(evt){
							$('#content_list_comp').show();
							showView('content_options');
              reset_function("update");
						}
					}, {
						view: "button",
						value: "Update",
						css: "orangeBtn saveBtnEdit",
						id: "variation_exp_update_btn",
            disabled: true,
						width: 60,
						click: function(evt){
              $.ajax({
                 url: monolop_api_base_url+"/api/page-element/content-update-experience",
                 type: "POST",
                 data: {'content_id':id, "updated_experiment_id":updated_experiment_id},
                 beforeSend: function(xhr){
                   xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
                   xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
                 },
               })
                 .done(function( response ) {
                   if (response.status === 'success') {
                     webix.message("Experience has been updated!");

                    var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                    var iframe_ = $('iframe')[index];
                    iframe_.contentWindow.postMessage({t: 'reset_on_EditMode'}, allFormDataPlacementWindowGoal.data.url);
                    $.ajax({
                      type: 'GET',
                      url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams + '&selected_experiment_id=' + selected_experiment_id,
                      beforeSend: function(xhr){
                        xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
                        xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
                      },
                      success: function( response ) {
                        if(response.status == 200){
                          iframe_.contentWindow.postMessage({t: 'remove_variation_bubbles'}, allFormDataPlacementWindowGoal.data.url);

                          var inlineContent = {},
                              pageElement = false,
                              variations = [];
                          pageExistingData.inlineContent = inlineContent = response.inlineContent;
                          pageExistingData.pageElement = pageElement = response.pageElement;
                          pageExistingData.variations = variations = response.variations;

                          var userStateData = null ;
                          if(parent_accessible){
                            userStateData = parent.userStateData ;
                          }
                          iframe_.contentWindow.postMessage({
                            t: "set-changed-content",
                            originalUrl: urlParams.fullURL,
                            source: basicPlacementWindowFormValues.source,
                            experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
                            inlineContent: inlineContent,
                            pageElement: pageElement,
                            page: page,
                            currentFolder: basicPlacementWindowFormValues.currentFolder,
                            variations: variations,
                            userStateData: userStateData
                          }, allFormDataPlacementWindowGoal.data.url);
                        }
                      }
                    });
                   } else {
                     webix.alert("An error occoured, please refreh page and try again");
                   }
                 });

              $$(this).disable();
              setTimeout(function(){
                $('#content_list_comp').show();
                showView('content_options');
              },200);

						}
					}
				]
		}]
	}];

  webix.ui({
		container: "update_variation",
		id: "update_experience_id",
    width: 240,
		rows: [{
			cols: [{}, {
				view: "form",
				id: "update_experience_container_form",
        width: 240,
				css: "",
				complexData: true,
				elements: __icon3Form,
				borderless: true,
				margin: 3,
				elementsConfig: {
					labelPosition: "top",
					labelWidth: 140,
					bottomPadding: 18
				},
			}, {}, ]
		}]
	});
}

function init (){
  $("#content_list_comp").html('');
  $("#content_options").html('');
  showView('content_options');

}
function add_addcontent(new_xpath,placemet_type_content){
  $("#add_addcontent").empty();
  var __icon3Form = [{
    // view: "layout",
    // height: 520,
    width: 240,
    rows: [{
      view:"label",
      label: "<b>Add Content</b>",
      align:"left"
    },{
      view: "text",
      id: "variation_title",
      label: 'Name',
      name: "variation_title",
      css: "add_variation_title",
      value: "Variation [" + urlParams.totalVariations + "]"
      // invalidMessage: "Name can not be empty"
    },
    // Mantis #4556
    // {
    // 	view: "richselect",
    // 	id: "placement_type",
    // 	label: 'Placement Type',
    // 	name: "placement_type",
    // 	css: "add_variation_title",
    //   value:placemet_type_content || "",
    // 	options:[
    //     { "id":1, "value":"Before"},
    //     { "id":2, "value":"After"}
    //     // { "id":3, "value":"Prepend"},
    //     // { "id":4, "value":"Append"}
    //     // { "id":5, "value":"Replace"},
    //     // { "id":6, "value":"Replace Inner"}
    //   ]
    // 	// invalidMessage: "Name can not be empty"
    // },
    {
      view: "text",
      id: "condition",
      label: 'Condition',
      name: "condition",
      hidden: true,
    },
    {
      cols: [{
          view: "text",
          id: "conditionLabel",
          name: "conditionLabel",
          css: "rightIconTextbox var addConditionLabel",
          label: "Condition",
          value: "All visitors.",
          readonly: true,
          bottomPadding: 15,
          click: function(){
            openConditionBuilder($$("condition").getValue(),'variation');
            // if (placement_type_pass) {
            //   openConditionBuilder($$("condition").getValue(),'variation','content');
            // }else {
            //   alert("Placement Type is not Selected");
            // }

          }
        },{
          view: "icon",
          icon: "users",
          css: "conditionIcon var",
          click: function(){
            openConditionBuilder($$("condition").getValue(),'variation');
          }
        }]
    },
    // Mantis #4556
    {
      view: "button",
      type: "icon",
      id: "variation_advance",
      label: 'Advanced',
      name: "variation_advance",
      css: "conditionIcon var",
      click: function(){
          if(xpath_flag){
            $$('variation_xpath').show();
            xpath_flag = false;
          }else {
            $$('variation_xpath').hide();
            xpath_flag = true;
          }

      }
      // invalidMessage: "Name can not be empty"
    },
      {
        view: "text",
        id: "variation_xpath",
        label: 'X-Path',
        name: "variation_xpath",
        css: "add_variation_title",
        value: new_xpath || urlParams.xpath,
        hidden:true
        // invalidMessage: "Name can not be empty"
      },
    {
        cols: [
          {

          }, {
            view: "button",
            value: "Back",
            id: "go_back",
            width: 60,
            click: function(evt){
              showView('content_options');
              reset_function("content");
            }
          }, {
						view: "button",
						value: "Save",
						css: "orangeBtn addSaveBtn",
						id: "content_add_btn",
						width: 60,
						click: function(evt){
              // if (check_type === "updatevariation") {
                  // do Nothing
                // }else{
                  // setTimeout(function(){
                    // if (check_type === "addvariation") {
                    //   var form = $$("add_variation_form");
                    // }else if(check_type === "content"){
                    //   var form = $$("add_addcontent_form");
                    // }
                    // var form = $$("add_variation_form");
                    var form = $$("add_addcontent_form");
                    if(form.validate()){
                     var formValues = form.getValues();
                     var data = {};
                     data.type = "variation";
                     data.variation_name = formValues.variation_title;
                     data.condition = formValues.condition;
                     data.fullURL = urlParams.fullURL;
                     data.page_element_id = urlParams.page_element_id;
                     data.experiment_id = urlParams.experiment_id;
                     // Mantis #4556
                     // data.placement_type = formValues.placement_type - 1;
                     if (formValues.variation_xpath === urlParams.xpath) {
                       data.form_xpath = undefined;
                     }else {
                       data.form_xpath = formValues.variation_xpath;
                     }
                     if($$("add_addcontent_form")){
                       data.placement_type = placemet_type_content - 1;
                     }
                      var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                      var iframe_ = $('iframe')[index];
                     iframe_.contentWindow.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
                      // Mantis 4510
                     // setTimeout(function(){
                     //     showView('content_options');
                     //     $("#content_list_comp").show();
                     // }, 200);
                    }
                  // }, 500);
                // }
						}
					}

        ]
    }]
  }];

  // var __icon3FormRules = {
  //   "variation_title": webix.rules.isNotEmpty,
  // };
  webix.ui({
    container: "add_addcontent",
    id: "add_addcontent_id",
    width: 240,

    rows: [{
      cols: [{}, {
        view: "form",
        id: "add_addcontent_form",
        css: "",
        width: 240,
        complexData: true,
        elements: __icon3Form,
        // rules: __icon3FormRules,
        borderless: true,
        margin: 3,
        elementsConfig: {
          labelPosition: "top",
          labelWidth: 140,
          bottomPadding: 18
        },
      }, {}, ]
    }]
  });
  // guidelines.startGuideline('placementWindowEditContentOptionsAddVar', {width: 300});
  //startGuidesIcon('placementWindowEditContentOptionsAddVar'); marc remove
  if(conditionBuilderValue != false || conditionBuilderLabel != false){
    $$("condition").setValue(conditionBuilderValue);
    $$("conditionLabel").setValue(conditionBuilderLabel);

    conditionBuilderValue = false;
    conditionBuilderLabel = false;
  }
  // $$("placement_type").attachEvent("onChange", function(newv,oldv){
  //   var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
  //   var iframe_ = $('iframe')[index];
  //   iframe_.contentWindow.postMessage({t: 'add_placement_type', type:newv}, urlParams.fullURL);
  //   placement_type_pass = true;
  // })
  // initializeGuidesAndTips('placementWindowEditContentOptionsAddVar');
}
function hidecontent(){
  $("#hide_content").empty();
  var __icon3Form = [{
    // view: "layout",
    // height: 520,
    width: 240,
    rows: [{
      view:"label",
      label: "<b>Hide Content</b>",
      align:"left"
    },{
      view: "text",
      id: "variation_title",
      label: 'Name',
      name: "variation_title",
      css: "add_variation_title",
      value: "Hidden"
    },
    {
      view: "text",
      id: "condition",
      label: 'Condition',
      name: "condition",
      hidden: true,
    },
    {
      cols: [{
          view: "text",
          id: "conditionLabel",
          name: "conditionLabel",
          css: "rightIconTextbox var addConditionLabel",
          label: "Condition",
          value: "All visitors.",
          readonly: true,
          bottomPadding: 15,
          click: function(){
            openConditionBuilder($$("condition").getValue(),'variation');
          }
        },{
          view: "icon",
          icon: "users",
          css: "conditionIcon var",
          click: function(){
            openConditionBuilder($$("condition").getValue(),'variation');
          }
        }]
    },
    {
        cols: [
          {

          }, {
            view: "button",
            value: "Back",
            id: "go_back",
            width: 60,
            click: function(evt){
              showView('content_options');
            }
          }, {
						view: "button",
						value: "Save",
						css: "orangeBtn addSaveBtn",
						id: "content_add_btn",
						width: 60,
						click: function(evt){
              var form = $$("hide_content_form");
              if(form.validate()){
                var formValues = form.getValues();
                var data = {};
                data.type = "variation";
                data.variation_name = formValues.variation_title;
                data.hide = true;
                data.condition = formValues.condition;
                data.fullURL = urlParams.fullURL;
                data.page_element_id = urlParams.page_element_id;
                data.experiment_id = urlParams.experiment_id;
                var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                var iframe_ = $('iframe')[index];
                iframe_.contentWindow.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
              }
						}
					}

        ]
    }]
  }];
  webix.ui({
    container: "hide_content",
    id: "hide_content_id",
    width: 240,

    rows: [{
      cols: [{}, {
        view: "form",
        id: "hide_content_form",
        css: "",
        width: 240,
        complexData: true,
        elements: __icon3Form,
        // rules: __icon3FormRules,
        borderless: true,
        margin: 3,
        elementsConfig: {
          labelPosition: "top",
          labelWidth: 140,
          bottomPadding: 18
        },
      }, {}, ]
    }]
  });
  if(conditionBuilderValue != false || conditionBuilderLabel != false){
    $$("condition").setValue(conditionBuilderValue);
    $$("conditionLabel").setValue(conditionBuilderLabel);

    conditionBuilderValue = false;
    conditionBuilderLabel = false;
  }
}
function add_variation(){
	$("#add_variation").empty();


	var __icon3Form = [{
		// view: "layout",
		// height: 520,
		width: 240,
		rows: [{
			view:"label",
			label: "<b>Add variation</b>",
			align:"left"
		},{
			view: "text",
			id: "variation_title",
			label: 'Name',
			name: "variation_title",
			css: "add_variation_title",
			value: "Variation [" + urlParams.totalVariations + "]"
			// invalidMessage: "Name can not be empty"
		},
    // Mantis #4556
    // {
		// 	view: "richselect",
		// 	id: "placement_type",
		// 	label: 'Placement Type',
		// 	name: "placement_type",
		// 	css: "add_variation_title",
    //   value:1,
		// 	options:[
    //     { "id":1, "value":"Before"},
    //     { "id":2, "value":"After"},
    //     { "id":3, "value":"Prepend"},
    //     { "id":4, "value":"Append"},
    //     { "id":5, "value":"Replace"},
    //     { "id":6, "value":"Replace Inner"}
    //   ]
		// 	// invalidMessage: "Name can not be empty"
		// },
		{
			view: "text",
			id: "condition",
			label: 'Condition',
			name: "condition",
			hidden: true,
		},
		{
			cols: [{
					view: "text",
					id: "conditionLabel",
					name: "conditionLabel",
					css: "rightIconTextbox var addConditionLabel",
					label: "Condition",
					value: "All visitors.",
					readonly: true,
					bottomPadding: 15,
					click: function(){
						openConditionBuilder($$("condition").getValue(),'variation');
					}
				},{
					view: "icon",
					icon: "users",
					css: "conditionIcon var",
					click: function(){
						openConditionBuilder($$("condition").getValue(),'variation');
					}
				}]
		},
    // Mantis #4556
    {
      view: "button",
      type: "icon",
      id: "variation_advance",
      label: 'Advanced',
      name: "variation_advance",
      // icon:"fa fa-caret-down",
      css: "conditionIcon var",
      click: function(){
          if(xpath_flag){
            $$('variation_xpath').show();
            xpath_flag = false;
          }else {
            $$('variation_xpath').hide();
            xpath_flag = true;
          }

      }
      // invalidMessage: "Name can not be empty"
    },
      {
  			view: "text",
  			id: "variation_xpath",
  			label: 'X-Path',
  			name: "variation_xpath",
  			css: "add_variation_title",
  			value: urlParams.xpath,
        hidden:true
  			// invalidMessage: "Name can not be empty"
  		},
		{
				cols: [
					{

					}, {
						view: "button",
						value: "Back",
						id: "go_back",
						width: 60,
						click: function(evt){
							showView('content_options');
              reset_function("variation");
						}
					}
          , {
						view: "button",
						value: "Save",
						css: "orangeBtn addSaveBtn",
						id: "variation_add_btn",
						width: 60,
						click: function(evt){
              var form = $$("add_variation_form");
              if(form.validate()){
               var formValues = form.getValues();
               var data = {};
               data.type = "variation";
               data.variation_name = formValues.variation_title;
               data.condition = formValues.condition;
               data.fullURL = urlParams.fullURL;
               data.page_element_id = urlParams.page_element_id;
               data.experiment_id = urlParams.experiment_id;
               data.selected_experiment_id = source == 'experiment' ? basicPlacementWindowFormValues.experiment.experimentID : selected_experiment_id;
               // Mantis #4556
               // data.placement_type = formValues.placement_type - 1;
               if (formValues.variation_xpath === urlParams.xpath) {
                 data.form_xpath = undefined;
               }else {
                 data.form_xpath = formValues.variation_xpath;
               }
                var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                var iframe_ = $('iframe')[index];
               iframe_.contentWindow.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
             }
						}
					}
				]
		}]
	}];

	var __icon3FormRules = {
		"variation_title": webix.rules.isNotEmpty,
	};
	webix.ui({
		container: "add_variation",
		id: "add_variation_id",
		width: 240,

		rows: [{
			cols: [{}, {
				view: "form",
				id: "add_variation_form",
				css: "",
				width: 240,
				complexData: true,
				elements: __icon3Form,
				rules: __icon3FormRules,
				borderless: true,
				margin: 3,
				elementsConfig: {
					labelPosition: "top",
					labelWidth: 140,
					bottomPadding: 18
				},
			}, {}, ]
		}]
	});
	// guidelines.startGuideline('placementWindowEditContentOptionsAddVar', {width: 300});
	//startGuidesIcon('placementWindowEditContentOptionsAddVar'); marc remove
	if(conditionBuilderValue != false || conditionBuilderLabel != false){
		$$("condition").setValue(conditionBuilderValue);
		$$("conditionLabel").setValue(conditionBuilderLabel);

		conditionBuilderValue = false;
		conditionBuilderLabel = false;
	}

	// initializeGuidesAndTips('placementWindowEditContentOptionsAddVar');
}
function reset_function(type_id){
  var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
  var iframe_ = $('iframe')[index];
  iframe_.contentWindow.postMessage({t: "reset_all_options_reverting",type:type_id}, urlParams.fullURL);
}
function openCondition(id){
  var condition = '';
  if(currentVariation !== false){
    condition = currentVariation.condition || '';
  }
  openConditionBuilder(condition,'content-preview');
  // var iframe_ = $('iframe')[1];
  // iframe_.contentWindow.postMessage({
  //   t: "open-condition-builder",
  //   content_id: id,
  //   type: 'content-preview',
  //   condition: condition
  // }, "*");
}

function update_variation(id){
	$("#update_variation").empty();


	var __icon3Form = [{
		// view: "layout",
		// height: 520,
		width: 240,
		rows: [{
			view:"label",
			label: "<b>Update variation</b>",
			align:"left"
		},{
			view: "text",
			id: "variation_title",
			label: 'Name',
			name: "variation_title",
			css: "update_variation_title",
			// value: "Variation [" + urlParams.totalVariations + "]"
			// invalidMessage: "Name can not be empty"
		},
    // Mantis #4556
    // {
    //   view: "richselect",
    //   id: "placement_type",
    //   label: 'Placement Type',
    //   name: "placement_type",
    //   css: "add_variation_title",
    //   value:1,
    //   options:[
    //     { "id":1, "value":"Before"},
    //     { "id":2, "value":"After"},
    //     { "id":3, "value":"Prepend"},
    //     { "id":4, "value":"Append"},
    //     { "id":5, "value":"Replace"},
    //     { "id":6, "value":"Replace Inner"}
    //   ]
    //   // invalidMessage: "Name can not be empty"
    // },
		{
			view: "text",
			id: "condition",
			label: 'Condition',
			name: "condition",
			hidden: true,
		},

		{
			cols: [{
					view: "text",
					id: "conditionLabel",
					name: "conditionLabel",
					css: "rightIconTextbox var updateConditionLabel",
					label: "Condition",
					value: "All visitors.",
					readonly: true,
					bottomPadding: 15,
					click: function(){
						openConditionBuilder($$("condition").getValue(),'variation');
					}
						// invalidMessage: "Name can not be empty"
				},{
					view: "icon",
					icon: "users",
					css: "conditionIcon var",
					click: function(){
						openConditionBuilder($$("condition").getValue(),'variation');
					}
				}]
		},

        {
      view: "button",
      type: "icon",
      id: "variation_advance",
      label: 'Advanced',
      name: "variation_advance",
      // icon:"fa fa-caret-down",
      css: "conditionIcon var",
      click: function(){
          if(xpath_flag){
            $$('variation_xpath').show();
            xpath_flag = false;
          }else {
            $$('variation_xpath').hide();
            xpath_flag = true;
          }

      }
      // invalidMessage: "Name can not be empty"
    },
    {
      view: "text",
      id: "variation_xpath",
      label: 'X-Path',
      name: "variation_xpath",
      value: urlParams.xpath1,
      css: "add_variation_title",
      hidden:true
      // invalidMessage: "Name can not be empty"
    },
		{
				cols: [
					{

					}, {
						view: "button",
						value: "Back",
						id: "go_back",
						width: 60,
						click: function(evt){
							$('#content_list_comp').show();
							showView('content_options');
              reset_function("update");
						}
					}, {
						view: "button",
						value: "Update",
						css: "orangeBtn saveBtnEdit",
						id: "variation_add_btn",
						width: 60,
						click: function(evt){
              //console.log('update btn');
							var form = $$("update_variation_container_form");
							if(form.validate()){
                //console.log('pass');
								var formValues = form.getValues();
								var data = {};
								data.type = "variation";
								data.variation_name = formValues.variation_title;
								data.condition = formValues.condition;
								data.fullURL = urlParams.fullURL;
								data.xpath = formValues.variation_xpath;
								data.page_element_id = urlParams.page_element_id;
								data.content_id = id;
                data.selected_experiment_id = selected_experiment_id;
                // Mantis #4556
                // data.placement_type = formValues.placement_type - 1;
                var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                var iframe_ = $('iframe')[index];
                //console.log(data);
								iframe_.contentWindow.postMessage({t: "saveChangedContent-Variation", data: data}, data.fullURL);
							}
              //Mantis #4566
              $$(this).disable();
              setTimeout(function(){
                $('#content_list_comp').show();
                showView('content_options');
              },200);

						}
					}
				]
		}]
	}];
	var __icon3FormRules = {
		"variation_title": webix.rules.isNotEmpty,
	};
	webix.ui({
		container: "update_variation",
		id: "update_variation_id",
    width: 240,
		rows: [{
			cols: [{}, {
				view: "form",
				id: "update_variation_container_form",
        width: 240,
				css: "",
				complexData: true,
				elements: __icon3Form,
				rules: __icon3FormRules,
				borderless: true,
				margin: 3,
				elementsConfig: {
					labelPosition: "top",
					labelWidth: 140,
					bottomPadding: 18
				},
			}, {}, ]
		}]
	});
	// getting data of Variation
	webix.ajax().post(monolop_api_base_url+'/api/page-element/content-show', {content_id: id} , function(response, xml, xhr) {
		//response
		response = JSON.parse(response);
		// data_Cancelupdate = response;
		var content = response.content;
    //console.log(content);
		if(response.status === "success"){
      // Mantis #4556
      variationsModel.setFormValues(content._id, content.name,content.condition,'update',content.xpath);
      // if (content.placement_type) {
      //   content.placement_type = parseInt(content.placement_type) + 1;
  		// 	variationsModel.setFormValues(content._id, content.name,content.placement_type, content.condition,'update');
      // }else {
      //   variationsModel.setFormValues(content._id, content.name, null,content.condition,'update');
      // }

		}
	});

	// guidelines.startGuideline('placementWindowEditContentOptions', {width: 300});
	//startGuidesIcon('placementWindowEditContentOptions');  marc remove
	// initializeGuidesAndTips('placementWindowEditContentOptions');
}


function showNewExperiment(){

		webix.ajax().get(monolop_api_base_url+"/data/experiments/segment", {}, function(resSegment, xml, xhr) {
			webix.ajax().get(monolop_api_base_url+"/data/experiments/goal", {}, function(resGoal, xml, xhr) {
				webix.ajax().get(monolop_api_base_url+"/data/experiments/control-group", {}, function(resCG, xml, xhr) {
					//response segments
					var segments = [{
						'id': 'new',
						'value': '<b>Create new</b>',
					}];
					resSegment = JSON.parse(resSegment);

					for (var i = 0; i < resSegment.segments.length; i++) {
						segments.push({
							'id': resSegment.segments[i]._id,
							'value': '<b>' + resSegment.segments[i].name + '</b>' + ' - ' + resSegment.segments[i].description.substr(0, 30)
						});
					}

					//response goals
					var goals = [{
						'id': 'new',
						'value': '<b>Create new</b>',
					}];
					resGoal = JSON.parse(resGoal);
					for (var i = 0; i < resGoal.goals.length; i++) {
						goals.push({
							'id': resGoal.goals[i]._id,
							'value': '<b>' + resGoal.goals[i].name + '</b>'
						});
					}
					// reponse CG
					resCG = JSON.parse(resCG);
					var significantActions = [];

					for (var i = 0; i < resCG.significantActions.length; i++) {
						significantActions.push({
							'id': i,
							'value': '<b>' + resCG.significantActions[i] + '</b>'
						});
					}

					function submit(){
						var form = $$("createNewExperimentFormID");
						if(form.validate()){
							var formValues = $$("createNewExperimentFormID").getValues();
							formValues.source = basicPlacementWindowFormValues.currentFolder;
							formValues.newPageElement = 1;
							webix.ajax().post(monolop_api_base_url+"/api/experiments/create", formValues, {
								error: function(text, data, XmlHttpRequest) {
									alert("error");
								},
								success: function(text, data, XmlHttpRequest) {
									var response = JSON.parse(text);
									if (response.success === true) {
										webix.message('Experience created!')
										showView('experiment');
										// parent.postMessage({t:"hide-iframe",message:"Experience created!"},"*");
									} else {
										webix.alert('An error occoured, please refresh and try again.');
									}
								}
							});
						}
					}

					function back(){
						$$("createNewExperimentMultiview").back();
					}

					function next(){
						var parentCell = this.getParentView().getParentView(); //the way you get parent cell object may differ
							var index = $$("createNewExperimentMultiview").index(parentCell);
							var next = $$("createNewExperimentMultiview").getChildViews()[index+1]
							if(next){
									var form = $$("createNewExperimentFormID");
									if(form.validate()){
										next.show();
									}
							}
					}
					webix.ui({
							container: 'create_new_exp_container',
							id: 'create_new_exp_container',
							scroll:false,
							css: 'create_new_exp_container',
							width: 240,
              height: 500,
							rows: [
								{
									view:"form",
									id:"createNewExperimentFormID",
									width: 240,
									elementsConfig: {
										labelPosition: "top",
										bottomPadding: 5
									},
									elements:[
										{view:"multiview", id:"createNewExperimentMultiview", cells:[
											{rows:[
												{
														view: "text",
														id: "nameExperiment",
														label: 'Name',
														name: "nameExperiment",
														// readonly: true,
															// invalidMessage: "Name can not be empty"
													}, {
														view: "textarea",
														id: "descExperiment",
														label: 'Description',
														name: "descExperiment",
														height: 150,
														// readonly: true,
															// invalidMessage: "Description can not be empty"
													}, {
													view: "richselect",
													label: "Please select the audience you wish to target:",
													id: "segmentExperiment",
													name: "segmentExperiment",
													value: "",
													options: segments,
													labelAlign: 'left',

												}, {
													view: "richselect",
													label: "Please select the goal you wish to target:",
													id: "goalExperiment",
													name: "goalExperiment",
													value: "",
													options: goals,
													labelAlign: 'left',
												},
												{cols:[
													{view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
													{view:"button", value:"Next", css: "orangeBtn", click:next},

												]}

											]},
											{rows:[
												{
													view: "slider",
													type: "alt",
													label: "Size of the control group (%)",
													value: 50,
													id: "cgSizeExperiment",
													name: "cgSizeExperiment",
													// disabled: true,
													title: webix.template("#value# %"),
													bottomPadding: 10
												},
												{
													view: "text",
													id: "cgDayExperiment",
													label: 'Days visitors stay in control group (days)',
													name: "cgDayExperiment",
													// readonly: true,
													value: 30,
													// invalidMessage: "Name can not be empty"
												}, {
													view: "select",
													label: "Action when significant:",
													id: "significantActionExperiment",
													name: "significantActionExperiment",
													value: significantActions[0].id,
													options: significantActions,
													labelAlign: 'left',
													// readonly: true,
												},

												{cols:[
													{view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
													{view:"button", value:"Back", click:back},
													{view:"button", value:"Next", css: "orangeBtn", click:next}
												]}

											]},
											{rows:[
												{
													view: "text",
													id: "fullURLWebPageList",
													label: 'URL',
													name: "fullURLWebPageList",
													readonly: true,
													value: pageOptions.fullURLWebPageList,
													// invalidMessage: "Name can not be empty"
												}, {
													cols: [
														{
															view:"checkbox",
															id:"includeWWWWebPageElement",
															name:"includeWWWWebPageElement",
															label:"Include WWW",
															// readonly: true,
															value: pageOptions.includeWWWWebPageElement
														}, {
															view:"checkbox",
															id:"includeHttpHttpsWebPageElement",
															name:"includeHttpHttpsWebPageElement",
															label:"Include Http / Https",
															// readonly: true,
															value: pageOptions.includeHttpHttpsWebPageElement
														}
													]
												},{
													view: "richselect",
													label: "URL Options",
													id: "urlOptionWebPageElement",
													name: "urlOptionWebPageElement",
													value: getUrlOption(pageOptions.urlOptionWebPageElement),
													options: urlOptions,
													labelAlign: 'left',
												},{
													view: "text",
													id: "regularExpressionWebPageElement",
													label: 'Regular Expression',
													name: "regularExpressionWebPageElement",
													// readonly: true,
													value: pageOptions.regularExpressionWebPageElement
													// invalidMessage: "Name can not be empty"
												}, {
													view: "text",
													id: "remarkWebPageList",
													label: 'Remark',
													name: "remarkWebPageList",
													// readonly: true,
													value: pageOptions.remarkWebPageList
													// invalidMessage: "Description can not be empty"
												},
												{cols:[
													{view: "button",value: "Cancel",type: "danger",click: function(){showView('experiment');}},
													{view:"button", value:"Back", click:back },
													{view:"button", value:"Finish", css: "orangeBtn", click:submit}
												]}

											]}
										]
										}
									],
									rules: {
										"nameExperiment": webix.rules.isNotEmpty,
										"cgSizeExperiment": webix.rules.isNumber,
										"cgDayExperiment": webix.rules.isNumber,
										"significantActionExperiment": webix.rules.isNotEmpty,
										"fullURLWebPageList": webix.rules.isNotEmpty,
									}
								}
							]
					});

					// on segment change
					$$("segmentExperiment").attachEvent("onChange", function(newv, oldv) {
						if(newv === 'new'){
							var formValues = $$("createNewExperimentFormID").getValues();
              var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
              var iframe_ = $('iframe')[index];
              iframe_.contentWindow.postMessage({t: 'open-new-segment-from-inlin-exp', data: formValues},urlParams.fullURL)
						}
					});
					// on goal change
					$$("goalExperiment").attachEvent("onChange", function(newv, oldv) {
						if(newv === 'new'){
							var formValues = $$("createNewExperimentFormID").getValues();
              var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
              var iframe_ = $('iframe')[index];
              iframe_.contentWindow.postMessage({t: 'open-new-goal-from-inlin-exp', data: formValues},urlParams.fullURL)
						}
					});
					if(newExpBackup){
						$$("createNewExperimentFormID").setValues(newExpBackup);
					}

				});
			});
		});


	}

function showExperiment(){
		$("#create_new_exp_container").html('');
		$("#experiment_container").html('');

		webix.ajax().get(monolop_api_base_url+'/api/component/content-list', urlParams , function(response, xml, xhr) {
			//response
			response = JSON.parse(response);

			contentListResponse  = response;


					var variationsToCopy = '',
							variationsToCopyCount = 0,
							SEPERATOR_PEID_VID = '____mp_page_element_variation_seperator____',
							totalCount = 0;

					totalCount = contentListResponse.variations.length;
					if(totalCount > 0){
						for (var i = 0; i < totalCount; i++) {
							if(urlParams.content_id){
								if(contentListResponse.variations[i].content._id === urlParams.content_id){
									variationsToCopyCount++;
									variationsToCopy += contentListResponse.variations[i].page_element_id + SEPERATOR_PEID_VID + contentListResponse.variations[i].uid;
									break;
								}
							} else {
								variationsToCopyCount++;
								variationsToCopy += contentListResponse.variations[i].page_element_id + SEPERATOR_PEID_VID + contentListResponse.variations[i].uid;
								if(i !== (totalCount-1)){
									variationsToCopy += ',';
								}
							}


						}

						var currentExp = '';

						var params = {
							fullURL: contentListResponse.fullURL,
							xpath: contentListResponse.xpath,
							experimentID: currentExp,
							page_element_id: contentListResponse.page_element_id,
							variationsToCopy: variationsToCopy,
							allExperiments: '',
						};

						var exps = [];
						if(contentListResponse.experiments !== undefined){
							for (var i = 0; i < contentListResponse.experiments.length; i++) {
								exps.push({
									id: contentListResponse.experiments[i].page_element_id,
									title: contentListResponse.experiments[i].name,
									select: true,
								});

								if(params.allExperiments.length === 0){
									params.allExperiments += contentListResponse.experiments[i].page_element_id;
								} else {
									params.allExperiments += (',' + contentListResponse.experiments[i].page_element_id);
								}

								if(contentListResponse.variationsBelongToExp.indexOf(contentListResponse.experiments[i].page_element_id) !== -1){
									if(currentExp.length === 0){
										currentExp += contentListResponse.experiments[i].page_element_id;
									} else {
										currentExp += (',' + contentListResponse.experiments[i].page_element_id);

									}
								}

							}
						}


						var formElements = [
							{
								view:"list",
								// label:"Select an experiment to move",
								view:"list",
								// select:true,
								multiselect:"touch",

								id: "moveToExperimentID",
								height: 250,
								width: 100,
								template:"#title#",
								select: "multiselect",
								data:exps,
								value: currentExp,
								ready:function(){
									this.select(contentListResponse.variationsBelongToExp);
								},
								on: {
									onItemClick: function(id){
										var item = this.getItem(id);
									},
								},
							},{
									cols: [
													{
														view: "button",
														value: "Back",
														id: "go_back",
														width: 55,
														click: function(evt){
															showView('content_options');
														}
													}, {
														view: "button",
														value: "Create new",
														hidden: urlParams.page === 'experiment',
														css: "orangeBtn",
														width: 90,
														click: function(){
															showView('content');
														}
													},{
															view: "button",
															value: "Save",
															css: "orangeBtn",
                              width: 55,
															click: function(){
																var form = $$("moveToExpForm"),
																		url = monolop_api_base_url+'/api/component/content-list/move-to-experiment';

																if(form.validate()){
																	var formValues = form.getValues();
																	var selectedIds = $$("moveToExperimentID").getSelectedId().toString();
																	params.moveToExperimentID = selectedIds;


																	webix.ajax().post(url, params, {
																		error: function(text, data, XmlHttpRequest) {
																			alert("error");
																		},
																		success: function(response, data, XmlHttpRequest) {
																			var response = JSON.parse(response);
																			if (response.success === true) {

																				showView('experiment');

																				webix.message("Saved!");

                                        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                                        var iframe_ = $('iframe')[index];
                                        iframe_.contentWindow.postMessage({t: 'variation-added-to-experiments', created: true},urlParams.fullURL);
																				// parent.postMessage({t:"hide-iframe",message:"Experience has been saved"},"*");
																			} else {
																				webix.message({type: 'error', text: response.msg});
																			}
																		}
																	});

																}

															}
												},{
												}
									]
								}
						];

						webix.ui({
								container: 'experiment_container',
								id: 'experiment_container',
								scroll:false,
								css: 'experiment_container',
								width: 240,
								height:400,
								rows: [
									{
										view: "label",
										label: "Total number of variations to be saved: <b>"+variationsToCopyCount+"</b>",
										css: "labelVarCount",
									},{
										view: "label",
										label: "Select an experience to move"
									},
									{
										view:"form",
										id: "moveToExpForm",
										scroll:false,
										height:320,
										width:200,
										elements: formElements
									}
								]
						});



					} else {
						webix.alert('There is no content added on this element.');
						showView('content_options');
					}


		});


	}
function listProgresser(id, type){
							webix.ui({
									 container: id,
									 id: id,
									//  width: 340,
									 height: 350,
									 scroll:false,
									 view:type,
									 data:[],
							});
							//adding progress bar functionality to it
							webix.extend($$(id), webix.ProgressBar);

							$$(id).clearAll();
							$$(id).showProgress({
                type:"icon"
							});
            }
function showPageProgressBar(id){

              webix.extend($$(id), webix.ProgressBar);
              $$(id).showProgress({
                 type:"icon",
                 delay:5000,
                 hide:true
              });

            }
function hidePageProgressBar(id){
              $$(id).hideProgress();

            }

function getTitle(name_id){
  return name_id.split(titleIdSeperator)[0];
}

function enableEditActions(){
  $$("editRecordBtnPlacementWindowGoal").enable();
  $$("testbenchRecordBtnPlacementWindowGoal").enable();
  $$("publishBtnPlacementWindowGoal").enable();

  // mantis: 3624
  if(page == 'goal'){
    $('div[view_id=publishBtnPlacementWindowGoal]').css('background-color', 'green');
  }
}
function disableEditActions(){
  $$("editRecordBtnPlacementWindowGoal").enable();
  $$("testbenchRecordBtnPlacementWindowGoal").disable();
  $$("publishBtnPlacementWindowGoal").disable();
}
function postInlineContentVariations(){
  var urlParams = '';
  // checking if opening up in experiments or not
  if(basicPlacementWindowFormValues.source !== undefined){
    urlParams += 'source=' + basicPlacementWindowFormValues.source + '&';
  }
  if(basicPlacementWindowFormValues.pw !== undefined){
    if(basicPlacementWindowFormValues.pw.mode !== undefined){
      urlParams += 'mode=' + basicPlacementWindowFormValues.pw.mode + '&';
    }
  }
  if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null){
    if(basicPlacementWindowFormValues.formData._id){
      urlParams += 'page_element_id=' + basicPlacementWindowFormValues.formData._id + '&';
    }
  }
  urlParams += 'selected_experiment_id=' + selected_experiment_id;
  // $.get( monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams, );

  $.ajax({
    type: 'GET',
    url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams,
    beforeSend: function(xhr){
      xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
      xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
    },
    success: function( response ) {
        // response = JSON.parse(response);

        var inlineContent = {},
            pageElement = false,
            variations = [],
            originalUrl= ""  ;
        if(response.status == 200){

          if(response.found === true){
            pageExistingData.inlineContent = inlineContent = response.inlineContent;
            pageExistingData.pageElement = pageElement = response.pageElement;
            pageExistingData.variations = variations = response.variations;

            pageExistingData.originalUrl = originalUrl = response.originalUrl;
          }else{
            pageExistingData = {
              variations: [],
              inlineContent: [],
              pageElement: false,
              originalUrl: "",
            }
          }
        }
        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
        var iframe_edit = $('iframe')[index];
        var userStateData = null ;
        if(parent_accessible){
          userStateData = parent.userStateData ;
        }
        iframe_edit.contentWindow.postMessage({
          t: "set-changed-content",
          originalUrl: originalUrl,
          source: basicPlacementWindowFormValues.source,
          inlineContent: inlineContent,
          pageElement: pageElement,
          variations: variations,
          experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
          currentFolder: basicPlacementWindowFormValues.currentFolder,
          page: page,
          updateFab: false,
          userStateData: userStateData
        }, allFormDataPlacementWindowGoal.data.url);
      }
  });
}


function openEditRecordPlacementWindowGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal) {

  if(parent_accessible){
    checkClientProtocalForHttps();
  }
  fullscreen.updateHeight();
  var urlParams = '';
  // checking if opening up in experiments or not
  if(basicPlacementWindowFormValues.source !== undefined){
    urlParams += 'source=' + basicPlacementWindowFormValues.source + '&';
  }
  if(basicPlacementWindowFormValues.pw !== undefined){
    if(basicPlacementWindowFormValues.pw.mode !== undefined){
      urlParams += 'mode=' + basicPlacementWindowFormValues.pw.mode + '&';
    }
  }
  if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null){
    if(basicPlacementWindowFormValues.formData._id){
      urlParams += 'page_element_id=' + basicPlacementWindowFormValues.formData._id;
    }
  }

  currentStepPWGoal = 'edit_record';
  highLightCurrentPlacementWindowGoal("edit_record");
  // comment line below due to mantis#4103
  // $("#customerSiteUrlEditRecordSection").html("");

  // $$("webixPlacementWindowSearchHeader").hide();
  isEditModeGeneratedGoal = true;
  $.ajax({
    type: 'GET',
    url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams,
    beforeSend: function(xhr){
      xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
      xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
    },
    success: function( response ) {
        // response = JSON.parse(response);

        var inlineContent = {},
            pageElement = false,
            variations = [],
            is_domain_registered = response.is_domain_registered,
            originalUrl= ""  ;
        if(response.status == 200){

          if(response.found === true){
            pageExistingData.inlineContent = inlineContent = response.inlineContent;
            pageExistingData.pageElement = pageElement = response.pageElement;
            pageExistingData.variations = variations = response.variations;

            pageExistingData.originalUrl = originalUrl = response.originalUrl;
          }else{
            pageExistingData = {
              variations: [],
              inlineContent: [],
              pageElement: false,
              originalUrl: "",
            }
          }
        }
        var iframeURL = allFormDataPlacementWindowGoal.data.url;
        if(parent_accessible){
          if(parent.isSessional && parent.userStateData){
            if(parent.userStateData.data.url){
              iframeURL = parent.userStateData.data.url;
            }
          }
        }
        iframeURL  = iframeURL.trim();
        iframeURL = refineUrl(iframeURL);

        if($("#customerSiteUrlEditRecordSection").is(':empty')){

          webix.ui({
              container: "customerSiteUrlEditRecordSection",
              css: "customerSiteUrlEditRecordSection",
              id: "webixWizardEditRecordGoalId",
              // height: screen.height,
              // height: getPWHeight(0),
              height: $(window).height(),
              borderless: true,
              cols: [{
               id: 'PWsidebarpanel',
               css: 'sidebar_panel sidebar_width',
               rows :[
                 {
                   id: "variation_options",
                   css: "variation_options dark-bg",
                   template:'<i id="sidebar_icon" onclick="sidebarcollapse_close()" class="fa fa-chevron-left" aria-hidden="true" style="display: inline; font-size: 20px;position:absolute;right:25px;top:10px;"></i><div id="experiment_list_container" style="margin-top: 20px;"></div><div style="font-size: large; font-weight: bold; height: 25px !important; width: 260px !important; margin-top: 40px;" id="variationGuides"></div><div id="content_list_comp" style="padding: 0px 0px 5px 0px;"></div><div id="var_action" style ="font-size: large;font-weight: bold; opacity: 0;">Actions</div><div id="content_options" style="opacity:0;"></div><div id="add_variation"></div><div id="add_addcontent"></div><div id="update_variation"></div><div id="create_new_exp_container"></div>	<div id="experiment_container"></div>'
                 }
               ]
             },{
               id: "xpand_variation",
               css: "sidebar_expand dark-bg",
               template: '<div style="display: block;position:relative;"><i id="sidebar_icon" class="fa fa-chevron-right" aria-hidden="true" style="display: inline; font-size: 20px;position:absolute;right:7px;"></i></div>',
               onClick: {
                 "fa" : function(e, id, trg){
                   setTimeout(function(){
                        sidebarcollapse_expand();
                   }, 200);

                 }
               }
             },{
                view: "iframe",
                id: "PWIframePlacementWindowEditModeGoal",
                src: iframeURL,
                css: "pw_iframe PWIframePlacementWindowEditModeGoal",
                borderless: true,
                on: {
                  onAfterLoad: function() {
                    // $$('PWIframePlacementWindowEditModeGoal').define('height', $(window).height());
                    // $$('PWIframePlacementWindowEditModeGoal').resize();
                    hidePageProgressBar("webixWizardEditRecordGoalId");
                    if(is_domain_registered === true){
                      var view = this;
                      if (postMessageConfig.stage == 1 || canWeConfirm == true) {
                        //found comfirm
                        enableEditActions();
                        //change to re-confirm
                        postMessageConfig.stage = 2;

                        parent.postMessage(JSON.stringify({
                          t: "set-active-step-sessional",
                          step: "edit_record",
                          url: iframeURL
                        }), "*");

                        var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                        var iframe_edit = $('iframe')[index];
                        var userStateData = null ;
                        if(parent_accessible){
                          userStateData = parent.userStateData ;
                        }
                        iframe_edit.contentWindow.postMessage({
                          t: "set-changed-content",
                          originalUrl: originalUrl,
                          source: basicPlacementWindowFormValues.source,
                          experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
                          inlineContent: inlineContent,
                          pageElement: pageElement,
                          page: page,
                          currentFolder: basicPlacementWindowFormValues.currentFolder,
                          variations: variations,
                          userStateData: userStateData
                        }, allFormDataPlacementWindowGoal.data.url);



                        var pageId = 'placementWindowEdit';
                        var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+pageId;
                        $.ajax({
                          type: 'GET',
                          url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams,
                          beforeSend: function(xhr){
                            xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
                            xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
                          },
                          success: function(r){
                            // r = JSON.parse(r);
                            var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                            var iframe_edit = $('iframe')[index];
                            setTimeout(function(){
                              iframe_edit.contentWindow.postMessage({
                                t: "start-guideline-in-site",
                                pageId: pageId,
                                data: r
                              }, postMessageConfig.url);
                            }, 2000);
                            for (var i = 0; i < r.experimentsList.length; i++) {
                  						allExperiments.push({
                  							'id': r.experimentsList[i].experimentID,
                  							'value': '<b>' + r.experimentsList[i].name + '</b>'
                  						});
                  					}
                            if(basicPlacementWindowFormValues.source != "experiment"){
                              showExperienceList(originalUrl);
                            }

                          }
                        });

                      } else {
                        //delay 1 second
                        webix.alert(scriptNotExistsMsg);
                        postMessageConfig.stage = 3;

                        if (!traversingHistoryPWGoal) {
                          addnewHistoryUrlGoal($$("customerSiteUrlLink").getValue());
                        }
                        traversingHistoryPWGoal = false;
                        allFormDataPlacementWindowGoal.data.url = $$("customerSiteUrlLink").getValue();
                        // iframe_edit.contentWindow.CKEDITOR.tools.callFunction(data.CKEditorFuncNum, data.url, data.msg);
                      }
                    } else {
                      webix.confirm({
                          title: "Confirmation", // the text of the box header
                          ok: "Register Now",
                          cancel: "Cancel",
                          text: domainNotRegisteredMsg,
                          callback: function(result) {
                              if (result) {
                                  registerDomain();
                              }
                          }
                      });
                    }



                  }
                }
              }]

            });

          showPageProgressBar("webixWizardEditRecordGoalId");
        }else{
          if(is_domain_registered === true){
            var view = this;
            if(editFirstTime === true){
              editFirstTime = false;
              if (postMessageConfig.stage == 1 || canWeConfirm == true) {

                var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                var iframe_edit = $('iframe')[index];

                iframe_edit.contentWindow.postMessage(JSON.stringify({
                  t: "loadbundle",
                  domain: monolop_base_url + '/',
                  url: monolop_base_url + editor_bundlejs
                }), postMessageConfig.url);
                //found comfirm
                enableEditActions();
                //change to re-confirm
                postMessageConfig.stage = 2;

                parent.postMessage(JSON.stringify({
                  t: "set-active-step-sessional",
                  step: "edit_record",
                  url: iframeURL
                }), "*");

                var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                var iframe_edit = $('iframe')[index];
                var userStateData = null ;
                if(parent_accessible){
                  userStateData = parent.userStateData ;
                }
                iframe_edit.contentWindow.postMessage({
                  t: "set-changed-content",
                  originalUrl: originalUrl,
                  source: basicPlacementWindowFormValues.source,
                  experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
                  inlineContent: inlineContent,
                  pageElement: pageElement,
                  page: page,
                  currentFolder: basicPlacementWindowFormValues.currentFolder,
                  variations: variations,
                  userStateData: userStateData
                }, allFormDataPlacementWindowGoal.data.url);





                var pageId = 'placementWindowEdit';
                var fetchGuidelinesUrl = monolop_api_base_url+'/api/guidelines?page_id='+pageId;
                $.ajax({
                  type: 'GET',
                  url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams,
                  beforeSend: function(xhr){
                    xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
                    xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
                  },
                  success: function(r){
                    // r = JSON.parse(r);
                    var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                    var iframe_edit = $('iframe')[index];
                    setTimeout(function(){
                      iframe_edit.contentWindow.postMessage({
                        t: "start-guideline-in-site",
                        pageId: pageId,
                        data: r
                      }, postMessageConfig.url);
                    }, 2500);

          					for (var i = 0; i < r.experimentsList.length; i++) {
          						allExperiments.push({
          							'id': r.experimentsList[i].experimentID,
          							'value': '<b>' + r.experimentsList[i].name + '</b>'
          						});
          					}
                    if(basicPlacementWindowFormValues.source != "experiment"){
                      showExperienceList(originalUrl);
                    }
                  }
                });

              } else {
                //delay 1 second
                webix.alert(scriptNotExistsMsg);
                postMessageConfig.stage = 3;

                if (!traversingHistoryPWGoal) {
                  addnewHistoryUrlGoal($$("customerSiteUrlLink").getValue());
                }
                traversingHistoryPWGoal = false;
                allFormDataPlacementWindowGoal.data.url = $$("customerSiteUrlLink").getValue();
                // iframe_edit.contentWindow.CKEDITOR.tools.callFunction(data.CKEditorFuncNum, data.url, data.msg);
              }
            }

          } else {
            webix.confirm({
                title: "Confirmation", // the text of the box header
                ok: "Register Now",
                cancel: "Cancel",
                text: domainNotRegisteredMsg,
                callback: function(result) {
                    if (result) {
                        registerDomain();
                    }
                }
            });
          }

        }

      }
  });
  $$("editRecordBtnPlacementWindowGoal").enable();
}


openTestRecordPlacementWindowGoalSection = function(){
  if(parent_accessible){
    checkClientProtocalForHttps();
  }
  window.testbench.isFirstTime = true ;
  fullscreen.updateHeight();
  currentStepPWGoal = 'testbench';
  highLightCurrentPlacementWindowGoal("testbench");
  isTestBenchGeneratedGoal = true;
  $("#customerSiteTestBenchSection").html("");
  //$("testbench-panel").html("");

  var iframeURL = allFormDataPlacementWindowGoal.data.url;
  if(parent_accessible){
    if(parent.isSessional && parent.userStateData){
      if(parent.userStateData.data.url){
        iframeURL = parent.userStateData.data.url;
      }
    }
  }
  webix.ui({
    container: "customerSiteTestBenchSection",
    css: "customerSiteUrlEditRecordSection",
    id: "webixWizardTestRecordGoalId",
    // height: screen.height,
    // height: getPWHeight(0),
    height: $(window).height()-60,
    borderless: true,
    cols: [{
      type : 'head' ,
      width : 300 ,
      rows :[
        // {
        //   header : 'Static Link',
        //   body : {
        //     padding : 10 ,
        //     css:'placement-testbench-panel dark-bg',
        //     rows :[{
        //       view : 'text' ,
        //       id : 'static-link-textbox'
        //     },{
        //       view : 'label' ,
        //       label : '<button id="testbench-preview-clipboard-btn" data-clipboard-target="#static-link-textbox-inner" class="btn" data-clipboard-target="#static-link-textbox-inner">Copy to Clipboard</button>'
        //     }]
        //   }
        // },
        {
          id: 'testbench-panel',
          padding:10,
          multi:true,
          view:"accordion",
          // height : getPWHeight(0) - 120 ,
          height: $(window).height(),
          css:'placement-testbench-panel dark-bg',
          rows: []
        }
      ]
    },{ view:"resizer" },{
      view: "iframe",
      id: "PWIframePlacementWindowTestbenchModeGoal",
      src: iframeURL,
      css: "pw_iframe PWIframePlacementWindowEditModeTestbenchl",
      borderless: true,
      on: {
        onAfterLoad: function() {
          hidePageProgressBar("webixWizardTestRecordGoalId");
          parent.postMessage(JSON.stringify({
            t: "set-active-step-sessional",
            step: "testbench",
            url: iframeURL
          }), "*");

          var view = this;
          if (postMessageConfig.stage == 1 || canWeConfirm == true) {
            postMessageConfig.stage = 2;
          } else {
            //delay 1 second
            setTimeout(function() {
              if (postMessageConfig.stage != 1) {
                //console.debug($('iframe')[0]);
                //console.debug(monolop_base_url + '/placement-proxy/preview?l=' +  urlencode($('iframe')[0].src));
                $('iframe')[0].src =  monolop_base_url + '/placement-proxy/preview?l=' +  urlencode($('iframe')[0].src);
                postMessageConfig.stage = 3;
              }
            }, 1000);
            if (!traversingHistoryPWGoal) {
              addnewHistoryUrlGoal($$("customerSiteUrlLink").getValue());
            }
            traversingHistoryPWGoal = false;
            allFormDataPlacementWindowGoal.data.url = $$("customerSiteUrlLink").getValue();
          }
          enableEditActions();
        }
      }
    }]
  });

  showPageProgressBar("webixWizardTestRecordGoalId");

  $('[view_id="static-link-textbox"] input').attr('id','static-link-textbox-inner') ;
  new Clipboard('#testbench-preview-clipboard-btn');
};

generateTestBenchPanel = function(testbench){
  var panel = $$('testbench-panel')  ;
  for (var group in testbench) {
    var groupVar = camelize(group);
    panel.addView({
      view: 'accordionitem',
      header: group ,
      body : {
        id : groupVar ,
        padding : 10 ,
        rows : []
      }
    });

    for(var field in testbench[group]){
      testbenchAddField(groupVar , testbench[group][field]) ;
    }
  }

  panel.addView({
    view: 'accordionitem',
    header: "Static Link" ,
    body : {
      id : "static-link" ,
      padding : 10 ,
      rows : [{
        view : 'text' ,
        id : 'static-link-textbox',
      },{
        view: "button",
        value: "Copy to Clipboard",click:function(){
  				var copyText = document.querySelector('div[view_id="static-link-textbox"] input');
  			  copyText.select();

  			  try {
  			    var successful = document.execCommand('copy');
  					webix.message("Copied.");
  			  } catch (err) {
  			    alert('Oops, unable to copy');
  			  }
        }
      }]
    }
  });

  window.testbench.isFirstTime = false ;

};

testbenchAddField = function(groupId , fieldObj){
  if( fieldObj.type == 'integer'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    $$(groupId).addView({
      view : 'text' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      validate:webix.rules.isNumber ,
      on : {
        onChange : function(code , e){
          updateTestbench();
        }
      }
    });
  }else if( fieldObj.type == 'string'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    $$(groupId).addView({
      view : 'text' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      on : {
        onChange : function(code , e){
          updateTestbench();
        }
      }
    });
  }else if( fieldObj.type == 'bool'){
    $$(groupId).addView({
      view : "checkbox" ,
      label : '<strong>' + fieldObj.text + '</strong>' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      checkValue : true ,
      uncheckValue : false ,
      on:{
				onChange: function(){
					updateTestbench();
				}
			}
    });
  }else if( fieldObj.type == 'date'){
    $$(groupId).addView({
      view : "datepicker" ,
      label : '<strong>' + fieldObj.text + '</strong>' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      timepicker:false,
      on:{
				onChange: function(){
					updateTestbench();
				}
			}
    });
  }else if( fieldObj.type == 'single'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    var options = [] ;
    for(var i = 0 ; i < fieldObj.values.length ; i++){
      options.push({id:fieldObj.values[i].value , value:fieldObj.values[i].label});
    }

    $$(groupId).addView({
      view : 'richselect' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      options: options ,
      on : {
        onChange : function(code , e){
          updateTestbench();
        }
      }
    });
  }else if( fieldObj.type == 'single remote'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    $$(groupId).addView({
      view : 'combo' ,
      id : fieldObj.connector + '_' +  fieldObj.id ,
      options: fieldObj.values + '?select=true' ,
      on : {
        onChange : function(code , e){
          updateTestbench();
        }
      }
    });
  }else if( fieldObj.type == 'function'){
    $$(groupId).addView({
      view : 'label' ,
      label : '<strong>' + fieldObj.text + '</strong>'
    });

    if( fieldObj.values[0].Label != undefined){
     $$(groupId).addView({
       view : 'label' ,
       label : '<u>Set : ' +   fieldObj.set + '</u>'
     }) ;
    }

    for( var i = 0 ; i < fieldObj.values.length ; i++){
      if(  fieldObj.values[i].value_label != undefined ) {
        $$(groupId).addView({
          view : 'label' ,
          label : fieldObj.values[i].Label + ' is ' + fieldObj.values[i].value_label
        }) ;
      }else{
        $$(groupId).addView({
          view : 'label' ,
          label : fieldObj.values[i].Label + ' is ' + fieldObj.values[i].value
        }) ;
      }
    }
    if( fieldObj.ReturnValue == 'string'){
      $$(groupId).addView({
        view : 'text' ,
        id : fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set,
        on : {
          onChange : function(code , e){
            updateTestbench();
          }
        }
      });
    }else if( fieldObj.ReturnValue == 'integer'){
      $$(groupId).addView({
        view : 'text' ,
        id : fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set,
        validate:webix.rules.isNumber ,
        on : {
          onChange : function(code , e){
            updateTestbench();
          }
        }
      });
    }else{
      $$(groupId).addView({
        view : "checkbox" ,
        id : fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set,
        checkValue : true ,
        uncheckValue : false ,
        value : fieldObj.value,
        on:{
    			onChange: function(){
    				updateTestbench();
    			}
    		}
      });
    }
  }

  if(fieldObj.type == 'function'){
    testbench.fieldArray[fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set] = {
        connector : fieldObj.connector ,
        field : fieldObj.id ,
        value : '' ,
        type  : fieldObj.type ,
        ReturnValue : fieldObj.ReturnValue ,
        id    : fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set
    }  ;
    testbench.fieldArray[fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set]['params'] = {}
    for( var i = 0 ; i < fieldObj.values.length ; i++){
      testbench.fieldArray[fieldObj.connector + '_' +  fieldObj.id + '_' + fieldObj.set]['params']['param' + (i + 1 ) ] = fieldObj.values[i].value ;
   }
  }else{
    testbench.fieldArray[fieldObj.connector + '_' +  fieldObj.id] = {
      connector : fieldObj.connector ,
      field : fieldObj.id ,
      value : '' ,
      type  : fieldObj.type ,
      id    : fieldObj.connector + '_' +  fieldObj.id
    }  ;
  }
}

updateTestbench = function(){
  $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().src = $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().src ;
}

getStaticLink = function(){

  var static_link = '' ;
  var fieldList = testbench.fieldArray ;
  static_link = static_link + '?l=' + $$('customerSiteUrlLink').getValue();
  for (var key in fieldList) {
    if( $$(fieldList[key].id) == undefined)
      continue ;
    var keyname = key.replace("[", "_");
    keyname = keyname.replace("]", "");
    static_link =  static_link + '&data[' + keyname + ']' + '[main]=' + fieldList[key].connector + '|' +   fieldList[key].field + '|' + fieldList[key].type + '|' ;
    if( fieldList[key].ReturnValue != undefined){
      static_link =  static_link + fieldList[key].ReturnValue  + '|' ;
    }else{
      static_link =  static_link   + '|' ;
    }


    if(  fieldList[key].type == 'date'){
      var val = Ext.getCmp(fieldList[key].id).getValue() ;
      static_link =  static_link + val.format('d/m/Y') ;
    }else{
      static_link =  static_link + $$(fieldList[key].id).getValue() ;
    }
    if(  fieldList[key].type == 'function'){
      for (var key2 in fieldList[key].params) {
        static_link =  static_link + '&data[' + key + ']' + '[params][' + key2 + ']=' + fieldList[key]['params'][key2] ;
      }
    }
  }

  $$('static-link-textbox').setValue( monolop_base_url + '/component/testbench-preview' + static_link ) ;
  //$('#static-link-data').val($$('static-link-textbox').getValue());
}

testbenchValue = function(){
  var data = {} ;
  var fieldList = testbench.fieldArray ;
  for (var key in fieldList) {
    if( data[key] == undefined){
      data[key] = {} ;
    }
    data[key]['connector']= fieldList[key].connector ;
    data[key]['field'] = fieldList[key].field ;
    data[key]['type']  = fieldList[key].type ;
    if( $$(fieldList[key].id) == undefined)
      continue ;

    data[key]['value'] = $$(fieldList[key].id).getValue() ;

    if(  fieldList[key].type == 'function'){
      data[key]['ReturnValue'] =  fieldList[key].ReturnValue ;
      for (var key2 in fieldList[key].params) {
        if( data[key]['params'] == undefined){
          data[key]['params'] = {} ;
        }
          data[key]['params'][key2]  = fieldList[key]['params'][key2] ;
      }
    }
    /*
    if(  fieldList[key].type == 'date'){
        var val = Ext.getCmp(fieldList[key].id).getValue() ;
        data[key]['value'] = val.format('d/m/Y') ;
        //console.debug(val.format('d/m/Y')) ;
    }else{
        data[key]['value'] = Ext.getCmp(fieldList[key].id).getValue() ;
    }
    if(  fieldList[key].type == 'function'){
      data[key]['ReturnValue'] =  fieldList[key].ReturnValue ;
        for (var key2 in fieldList[key].params) {
          if( data[key]['params'] == undefined){
            data[key]['params'] = {} ;
          }
            data[key]['params'][key2]  = fieldList[key]['params'][key2] ;
        }
    }
    */

  }
  getStaticLink();
  return data ;
}

function processTestBenchData(){
  var data = testbenchValue() ;

  $$('PWIframePlacementWindowTestbenchModeGoal').getIframe().contentWindow.postMessage(JSON.stringify({
    t: "test-processTestbench2" ,
    testData : data ,
    mongoDetail : testbench.mongoDetail ,
    loadDefault : true
  }), postMessageConfig.url);
}


function openPublishGoalSection(modePlacementWindowGoal, formDataPlacementWindowGoal, referenceOptionsPlacementWindowGoal) {
  if(parent_accessible){
    checkClientProtocalForHttps();
  }

  fullscreen.updateHeight();
  currentStepPWGoal = 'publish';
  highLightCurrentPlacementWindowGoal('publish');
  $("#customerSitePublishSection").html('');

  isPublishGeneratedGoal = true;
  var isPageElementIdAdded = false;
  var publishURL = monolop_base_url + '/component/page-list-publish?';
  publishPWUrl = publishURL;

  if(basicPlacementWindowFormValues !== undefined){
    if(basicPlacementWindowFormValues.source !== undefined){
      publishURL += ('source='+basicPlacementWindowFormValues.source + '&');
    }
    if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null){
      if(basicPlacementWindowFormValues.formData._id !== undefined){
        publishURL += ('page_element_id='+basicPlacementWindowFormValues.formData._id + '&');
        isPageElementIdAdded = true;
      }
    }
    if(basicPlacementWindowFormValues.experiment !== undefined && basicPlacementWindowFormValues.experiment != null){
      publishURL += ('exp='+basicPlacementWindowFormValues.experiment._id + '&');
    }
  }

  if(isPageElementIdAdded === false){
    if(currentPageElement.added){
      if(currentPageElement.data._id !== undefined){
        publishURL += ('page_element_id='+currentPageElement.data._id + '&');
      }
    }
  }
  var browseUrl = $$("customerSiteUrlLink").getValue().trim();
  if(browseUrl.length > 0){
    publishURL += ('fullURL='+browseUrl + '&');
  }

  // alert(publishURL);

  webix.ui({
    container: "customerSitePublishSection",
    id: "webixWizardPublishGoalId",
    css: "webixWizardPublishGoalId",
    height: 1024,
    rows: [{
      view: "iframe",
      id: "PWIframePublishGoal",
      src: publishURL,
      css: "pw_iframe",
      borderless: true,
      on: {
        onAfterLoad: function() {

          parent.postMessage(JSON.stringify({
            t: "set-active-step-sessional",
            step: "publish",
            url: browseUrl
          }), "*");

          var view = this, currentFolder = '';
          customerSiteUrlLink = $$("customerSiteUrlLink").getValue();

          if(basicPlacementWindowFormValues.formData !== undefined && basicPlacementWindowFormValues.formData != null){
            if(basicPlacementWindowFormValues.formData._id !==undefined){
              returnUrlConfig.url = customerSiteUrlLink;
              returnUrlConfig.url_option = basicPlacementWindowFormValues.formData.urlOption;
              returnUrlConfig.inc_www = basicPlacementWindowFormValues.formData.inWWW;
              returnUrlConfig.inc_http_https = basicPlacementWindowFormValues.formData.inHTTP_HTTPS;
              returnUrlConfig.reg_ex = basicPlacementWindowFormValues.formData.regExp;
              returnUrlConfig.remark = basicPlacementWindowFormValues.formData.remark;
            }
          }
          if(basicPlacementWindowFormValues.currentFolder !== undefined){
            currentFolder = basicPlacementWindowFormValues.currentFolder;
          }
          var page_element_id = "";
          var experiment_id = "";
          if(parent_accessible){
            if(parent.userStateData){
              if(parent.userStateData.data.publishData){
                customerSiteUrlLink = parent.userStateData.data.publishData.fullURLWebPageList;
                returnUrlConfig.url = parent.userStateData.data.publishData.fullURLWebPageList;
                returnUrlConfig.url_option = parent.userStateData.data.publishData.urlOptionWebPageElement;
                returnUrlConfig.inc_www = parent.userStateData.data.publishData.includeWWWWebPageElement;
                returnUrlConfig.inc_http_https = parent.userStateData.data.publishData.includeHttpHttpsWebPageElement;
                returnUrlConfig.reg_ex = parent.userStateData.data.publishData.regularExpressionWebPageElement;
                returnUrlConfig.remark = parent.userStateData.data.publishData.remarkWebPageList;
                currentFolder = parent.userStateData.data.publishData.currentFolder;
                page_element_id  = parent.userStateData.data.publishData.page_element_id;
                experiment_id  = parent.userStateData.data.publishData.experiment_id;
              }
            }
          }
          // commnet lines below for mantis#4103
          // var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
          // var iframe_edit = $('iframe')[index];
          $$('PWIframePublishGoal').getIframe().contentWindow.postMessage(JSON.stringify({
            t: "set-publish-page-options",
            fullURL: customerSiteUrlLink,
            currentFolder: currentFolder,
            experiment_id: experiment_id,
            page_element_id: page_element_id,
            mode: basicPlacementWindowFormValues.mode,
            returnUrlConfig: returnUrlConfig,
            temp: basicPlacementWindowFormValues.temp,
          }), publishURL);


          enableEditActions();
        }
      }
    }]
  });

  $$("publishBtnPlacementWindowGoal").enable();
}

function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
    return index == 0 ? match.toLowerCase() : match.toUpperCase();
  });
}

function urlencode(str) {
  return escape(str).replace(/\+/g, '%2B').replace(/%20/g, '+').replace(/\*/g, '%2A').replace(/\//g, '%2F').replace(/@/g, '%40');
};

function refineUrl(url) {
  url = url || "";
  if(url.length < 1){
    return url;
  }
  var prefix = ['http://', 'https://'];
  if (url.substr(0, prefix[1].length) === prefix[1] || url.substr(0, prefix[0].length) === prefix[0]) {
    return url;
  }
  url = prefix[0] + url;
  return url;
}

function is_valid_url(url) {
  // validate URL here
  if(url.length < 1){
    return false;
  }
  return true;
}

function addnewHistoryUrlGoal(url) {
  if ((currentUrlHosturyPWGoal + 1) < indexUrlHistoyPWGoal) {
    urlHistoryPWGoal.splice(currentUrlHosturyPWGoal + 1);
    indexUrlHistoyPWGoal = currentUrlHosturyPWGoal;
  }
  if (urlHistoryPWGoal.length > 0) {
    indexUrlHistoyPWGoal++;
    currentUrlHosturyPWGoal = indexUrlHistoyPWGoal;
  }
  urlHistoryPWGoal.push(url);
}

function backwardPWGoal() {
  if (indexUrlHistoyPWGoal === 0 || currentUrlHosturyPWGoal === 0) {
    return;
  }
  traversingHistoryPWGoal = true;
  currentUrlHosturyPWGoal = currentUrlHosturyPWGoal - 1;
  $$("customerSiteUrlLink").setValue(urlHistoryPWGoal[currentUrlHosturyPWGoal]);
  openPlacementWindowGoalSection();
}

function forwardPWGoal() {
  if (indexUrlHistoyPWGoal === 0 || currentUrlHosturyPWGoal === urlHistoryPWGoal.length + 1) {
    return;
  }
  if (currentUrlHosturyPWGoal >= indexUrlHistoyPWGoal) {
    return;
  }
  traversingHistoryPWGoal = true;
  currentUrlHosturyPWGoal = currentUrlHosturyPWGoal + 1;
  $$("customerSiteUrlLink").setValue(urlHistoryPWGoal[currentUrlHosturyPWGoal]);
  openPlacementWindowGoalSection();
}

function setFileManagerSelectUrl(url){
  /*
  var index = url.indexOf('/photos');
  url = url.substring(index+7);
  url = s3_public_url  + 'assets' + url ;
  url = url.replace(activeAccount['_id'],activeAccount['uid']);
  */

  var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
  var iUrl = $$("customerSiteUrlLink").getValue();
  var iFrame = $("iframe[src*='"+ iUrl +"']")[index];


  if(iFrame){
    iFrame.contentWindow.postMessage({
        t: "image-is-uploaded", data: {
          status : 'server' ,
          url : url
        }
      },
      $$("customerSiteUrlLink").getValue()
    );
  }
  $$('upload_an_image1').close();

}

function openImageUploader(data){
  //window.open('/filemanager?type=image', 'FileManager', 'width=900,height=600');
  var url = monolop_base_url + '/media?nav=1';
  webix.ui({
    view: "window",
    id: "upload_an_image1",
    modal: true,
    scroll: true,
    position: "center",
    height: 650,
    width: 900,
    headHeight : 0,
    body: {
      rows: [
        {
          view:"iframe",
          id:"frame-body",
          src: url,
          on: {
            onAfterLoad: function() {
            }
          }
        },{
          cols: [{}, {}, {}, {}, {
              view: "button",
              "type": "danger",
              value: "Cancel",
              width: 138,
              click: "$$('upload_an_image1').close();"
          }]
        }
      ]
    }
  }).show();
}

function getChildComponentDimensionAsPerParent(parentWindow){
  var parentWidth = $(parentWindow).width();
  var parentHeight = $(parentWindow).height();

  var _width = parentWidth - (parentWidth * (5/100));
  var _height = parentHeight - (parentHeight * (30/100));

  return {width: _width, height: _height};
}

function openConditionBuilder(condition, type) {
  var dimensions = this.getChildComponentDimensionAsPerParent(window);
  condition === undefined && (condition = "if( ) {|}");
  var url  = '/api/profile-properties-template';

  // pass query_string flag[inherit_condition=true] to disable pointer event if condition is forced to be inherited in contents
  if(basicPlacementWindowFormValues.source == 'experiment'){
    if(basicPlacementWindowFormValues.experiment){
      if(basicPlacementWindowFormValues.experiment._segment){
        condition = basicPlacementWindowFormValues.experiment._segment.condition;
        url += '?inherit_condition=true';
      }
    }
  }


   webix.ui({
     view: "window",
     id: "edit_condition_bulder",
     modal: true,
     scroll: true,
     position: "top",
     height: /*700*/ dimensions.height,
     width: /*1200*/ dimensions.width,
     move: true,
     resize: true,
     head: {
       view: "toolbar",
       margin: -4,
       cols: [{
         view: "label",
         label: "Condition Builder"
       }, {
         view: "icon",
         icon: "times-circle",
         css: "alter",
         click: "$$('edit_condition_bulder').close();"
       }]
     },
     body: {
       rows: [
         {
           view:"iframe",
           id:"frame-body",
           src: url,
           on: {
             onAfterLoad: function() {
               var index = isPlacementWindowGeneratedGoal === false ? 1 : 2;
               var iframe = $('iframe')[index];
               iframe.contentWindow.postMessage(condition, monolop_base_url + url);
             }
           }
       },
         {
           cols: [{}, {}, {}, {}, {
               view: "button",
               "type": "danger",
               value: "Cancel",
               width: 138,
               click: "$$('edit_condition_bulder').close();"
             }, {
               view: "button",
               value: "Ok",
               css: "orangeBtn",
               width: 138,
               click: function(evt){
                 var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                 var iFrame = $("iframe")[index];
                 iFrame.contentWindow.postMessage({t: "condition-selected", type: type, returnCondition: iFrameConditionBuilderWin.returnCondition},"*");
                 // Mantis #4648 variation change
                 // if (check_type === "updatevariation") {
                 //   // do Nothing
                 // }else{
                 //   setTimeout(function(){
                 //     if (check_type === "addvariation") {
                 //       var form = $$("add_variation_form");
                 //     }else if(check_type === "content"){
                 //       var form = $$("add_addcontent_form");
                 //     }
                 //     // var form = $$("add_variation_form");
       						// 	 if(form.validate()){
       						// 		var formValues = form.getValues();
       						// 		var data = {};
       						// 		data.type = "variation";
       						// 		data.variation_name = formValues.variation_title;
       						// 		data.condition = formValues.condition;
       						// 		data.fullURL = urlParams.fullURL;
       						// 		data.page_element_id = urlParams.page_element_id;
       						// 		data.experiment_id = urlParams.experiment_id;
                 //      // Mantis #4556
                 //      // data.placement_type = formValues.placement_type - 1;
                 //      if (formValues.variation_xpath === urlParams.xpath) {
                 //        data.form_xpath = undefined;
                 //      }else {
                 //        data.form_xpath = formValues.variation_xpath;
                 //      }
                 //      if($$("add_addcontent_form")){
                 //        data.placement_type = placement_option - 1;
                 //      }
                 //       var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                 //       var iframe_ = $('iframe')[index];
       						// 		iframe_.contentWindow.postMessage({t: "get-element-content-variation", data: data}, data.fullURL);
                 //       // Mantis 4510
                 //      // setTimeout(function(){
                 //
                 //      //     showView('content_options');
                 //      //     $("#content_list_comp").show();
                 //      // }, 200);
       						// 	 }
                 //   }, 500);
                 // }
                 // till here
                 $$('edit_condition_bulder').close();
               }
             }
           ]
         }
       ]
     }
   }).show();
 }
 function closeConditionBuilder(){
   $$('edit_condition_bulder').close();
   webix.message("Segment conditions are updated.");
 }
 function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}
$(window).bind('beforeunload', function(){
		createCookie("customerSiteUrlLink", $$("customerSiteUrlLink").getValue(), 1);
	});


function checkClientProtocal(){
  var parent_url = parent.document.location.href ;
  var current_url = $$("customerSiteUrlLink").getValue() ;
  var parent_scheme = isHttps(parent_url);
  var current_scheme = isHttps(current_url);
  if(parent_scheme !== current_scheme){
    if(parent_scheme === true){
      //convert to http
      parent.document.location.href = parent.document.location.href.replace('https','http');
    }else{
      //convert tp https
      parent.document.location.href = parent.document.location.href.replace('http','https');
    }
  }
}

function checkClientProtocalForHttps(){
  var parent_url = parent.document.location.href ;
  var current_url = $$("customerSiteUrlLink").getValue() ;
  var parent_scheme = isHttps(parent_url);
  var current_scheme = isHttps(current_url);
  if(parent_scheme !== current_scheme){
    if(parent_scheme === true){
      //convert tp https
      parent.document.location.href = parent.document.location.href.replace('https','http');
    }
  }
}

function isHttps(url){
  if(url.indexOf('https') == -1)
    return false ;
  return true ;
}

/******
Tracker add-on
******/

function showTrackerConfig(){
  parent.postMessage(JSON.stringify({
    t: 'to-config-step',
    url: $$('customerSiteUrlLink').getValue()
  }),'*');
}

function showExperienceList (originalUrl){
  if($.trim($('div[view_id="experiment_list_container"]').html()).length == 0){
    webix.ui({
        container: 'experiment_list_container',
        id: 'experiment_list_container',
        css: 'experiment_container',
        width: 240,
        rows: [
          {
            view: "label",
            label: "Select Experience",
          },
           {
           view: "richselect",
           id: "experimentList",
           name: "experimentList",
           value: "all",
           options:allExperiments,
           on: {
              onChange: function(newValue, oldValue){
                  selected_experiment_id = newValue;
                  var index = isPlacementWindowGeneratedGoal === false ? 0 : 1;
                  var iframe_ = $('iframe')[index];
                  iframe_.contentWindow.postMessage({t: 'reset_on_EditMode'}, allFormDataPlacementWindowGoal.data.url);
                  $.ajax({
                    type: 'GET',
                    url: monolop_api_base_url+'/api/page-element?url=' + allFormDataPlacementWindowGoal.data.url + '&' + urlParams + '&selected_experiment_id=' + selected_experiment_id,
                    beforeSend: function(xhr){
                      xhr.setRequestHeader('Content-type', ajaxHeaders['Content-Type']);
                      xhr.setRequestHeader('ML-Token', ajaxHeaders['ML-Token']);
                    },
                    success: function( response ) {
                      if(response.status == 200){
                        iframe_.contentWindow.postMessage({t: 'remove_variation_bubbles'}, allFormDataPlacementWindowGoal.data.url);

                        var inlineContent = {},
                            pageElement = false,
                            variations = [];
                        pageExistingData.inlineContent = inlineContent = response.inlineContent;
                        pageExistingData.pageElement = pageElement = response.pageElement;
                        pageExistingData.variations = variations = response.variations;

                        var userStateData = null ;
                        if(parent_accessible){
                          userStateData = parent.userStateData ;
                        }
                        iframe_.contentWindow.postMessage({
                          t: "set-changed-content",
                          originalUrl: originalUrl,
                          source: basicPlacementWindowFormValues.source,
                          experiment: basicPlacementWindowFormValues.experiment ? basicPlacementWindowFormValues.experiment : false,
                          inlineContent: inlineContent,
                          pageElement: pageElement,
                          page: page,
                          currentFolder: basicPlacementWindowFormValues.currentFolder,
                          variations: variations,
                          userStateData: userStateData
                        }, allFormDataPlacementWindowGoal.data.url);
                      }
                    }
                  });

              }
          }
         },
        ]
     });
  }

}

// window.onresize = function(){
//
// }

// window.addEventListener('resize', function(){
//   alert('hi');
// }, true);
