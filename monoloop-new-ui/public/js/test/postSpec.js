var _gaq = {
	push : function(){

	}
} ;
MONOloopReady(function() {

	describe('Post.js', function() {

	 	it("Should monoloop ready", function() {
			expect(true).toEqual(true);
		}) ;

        describe("Custom function checking" , function(){
            it("Should define restoreEC" , function(){
                expect(MONOloop.restoreEC).toBeDefined() ;
            }) ;
            /*
            it("Should define evercookie" , function(){
            	var ec = new evercookie();
            	expect(ec instanceof  evercookie ).toEqual(true);
            }) ;

            it("Should get/set evercookie" , function(){
            	var ec = new evercookie();
            	ec.set('q12w' , '555');

            	ec.get('q12w',function(val){
            		alert(val) ;
            	}) ;
            }) ;
            */
        }) ;

		describe("Basic jquery function test", function() {
			it("Should MONOloop.before", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.before(MONOloop.$('#test p'), 'T');
				expect(document.getElementById('test').innerHTML).toEqual('T<p>T</p>');

				MONOloop.before(MONOloop.$('')) ;
			}) ;

			it("Should MONOloop.after", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.after(MONOloop.$('#test p'), 'T');
				expect(document.getElementById('test').innerHTML).toEqual('<p>T</p>T');
			}) ;

			it("Should MONOloop.append", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.append(MONOloop.$('#test p'), 'X');
				expect(document.getElementById('test').innerHTML).toEqual('<p>TX</p>');
			}) ;

			it("Should MONOloop.prepend", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.prepend(MONOloop.$('#test p'), 'X');
				expect(document.getElementById('test').innerHTML).toEqual('<p>XT</p>');
			}) ;

			it("Should MONOloop.replaceWith", function() {
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.replaceWith(MONOloop.$('#test p'), 'replace');
				expect(document.getElementById('test').innerHTML).toEqual('replace');
			}) ;

			it("Should MONOloop.children", function() {
				document.getElementById('test').innerHTML = '<p>T</p><p>T2</p>' ;
				var l = MONOloop.children (MONOloop.$('#test')).length ;
				expect(l).toEqual(2);
			}) ;

			it("Should MONOloop.parent", function() {
				document.getElementById('test').innerHTML = '<div id="child">T</div>' ;
				var p = MONOloop.parent((MONOloop.$('#child'))) ;
				expect(p[0].getAttribute('id')).toEqual('test');
			}) ;

			describe("Basic Monoloop.val input[type='text']", function() {
			 	it("Should val from textbox", function() {
			 		document.getElementById('test').innerHTML = '<input id="test1" type="text" value="text" />' ;
			 		var val = MONOloop.val(MONOloop.$('#test1'));
			 		expect(val).toEqual('text');

			 		MONOloop.val(MONOloop.$('#test1'),'text1');
			 		var val = MONOloop.val(MONOloop.$('#test1'));
			 		expect(val).toEqual('text1');
		 		}) ;
		 	}) ;

		 	it("Should MONOloop.html", function(){
		 		document.getElementById('test').innerHTML = '<p>T</p>' ;
		 		var val = MONOloop.html(MONOloop.$('#test')) ;
		 		expect(val).toEqual('<p>T</p>');
		 		expect(MONOloop.html(MONOloop.$('xxxx'))).toEqual( undefined ) ;
	 		}) ;

	 		it("Should MONOloop.trim", function(){
		 		document.getElementById('test').innerHTML = ' T ' ; 
		 		var val = MONOloop.trim(MONOloop.html(MONOloop.$('#test'))) ;
		 		expect(val).toEqual('T');
		 		expect(undefined).toEqual(MONOloop.trim(undefined));
		 		expect(null).toEqual(MONOloop.trim(null));

		 		expect('11').toEqual(MONOloop.trim(11));
	 		}) ;

	 		it("Should MONOloop.attr", function(){
	 			var val = MONOloop.attr(MONOloop.$('#test'), 'id');
	 			expect(val).toEqual('test');

	 			expect(MONOloop.attr(MONOloop.$('#test'), 'xxx') , undefined ) ;
	 			expect(MONOloop.attr(MONOloop.$('#test'), undefined) , undefined ) ;
	 			expect(MONOloop.attr(MONOloop.$('#test'), null) , undefined ) ;

	 			expect(MONOloop.attr(MONOloop.$('#xxx'), 'xxx') , undefined ) ;
 			}) ;

 			it("Should MONOloop.is", function(){
 				expect(MONOloop.is(MONOloop.$('#test'),'div')).toEqual(true);
 				expect(MONOloop.is(MONOloop.$(''),'divxxx')).toEqual(false);
			}) ;

			it("Should MONOloop.next", function(){
				document.getElementById('test').innerHTML = '<p id="a">12</p><p id="b">12</p>' ;
				var n = MONOloop.next( MONOloop.$('#a') ) ;
				expect(n[0].getAttribute('id')).toEqual('b') ;
				MONOloop.next( MONOloop.$('xxx') ) ;
			}) ;

			it("Should MONOloop.val",function(){
				document.getElementById('test').innerHTML = '<input id="a" value="123" />' ;
				expect(MONOloop.val(MONOloop.$('#a'))).toEqual('123');
				expect(MONOloop.val(MONOloop.$('#x'))).toEqual(undefined);
			}) ;

			it("should get image src" , function(){
				document.getElementById('test').innerHTML = '<p id="a"><img src="/test.img"/></p>' ;
				expect(MONOloop.getImageFilter('#a img')).toContain('test.img') ;
				expect(MONOloop.getImageFilter('#a imgxx')).toEqual(undefined) ;

				document.getElementById('test').innerHTML = '<p id="a"><img xxx="/test.img"/></p>' ;
				expect(MONOloop.getImageFilter('#a img')).toEqual('') ;
			}) ;

			it("should MONOloop.getCartPriceDefaultFilter " , function(){
				expect( MONOloop.getCartPriceDefaultFilter('$12.12')).toEqual('12.12') ;
				expect( MONOloop.getCartPriceDefaultFilter(undefined)).toEqual(undefined) ;
				expect( MONOloop.getCartPriceDefaultFilter(null)).toEqual(null) ;
				expect( MONOloop.getCartPriceDefaultFilter(12.12)).toEqual('12.12') ;
 			}) ;

 			it("should globalEval " , function(){
 				MONOloop.globalEval('var x = 123 ;') ;
 				expect(x).toEqual(123) ;
 			}) ;

 			it("should MONOloop.strip_tags " , function(){
 				var html = '<p>Test paragraph.</p> <a href="#fragment">Other text</a>' ;
 				html = MONOloop.strip_tags(html) ;
 				expect(html).toEqual('Test paragraph. Other text');

 				html = MONOloop.strip_tags(undefined) ;
 				expect(html).toEqual(undefined);

 				html = MONOloop.strip_tags(11233) ;
 				expect(html).toEqual('11233');
 			}) ;

 			it("should parseAdditionalJSFiles " , function(){
 				html = MONOloop.parseAdditionalJSFiles(undefined) ;
 				expect(html).toEqual(undefined);
 			}) ;

			it("Should MONOloopReady multiple place", function(){
				var x = 0 ;

				MONOloopReady(function() {
					x = x +1 ;
				}) ;
				MONOloopReady(function() {
					x = x +1 ;
				}) ;

				expect(x).toEqual(2) ;
			}) ;

			it("Should same result from php / node hash",function(){
				var test = 'nuttest' ;
				var result = 'd34012aeb23b92bb69a2d678' ;
				expect(MONOloop.genHash(test)).toEqual(result);
			}) ;

			it("Should track asset",function(){
				MONOloop.trackAssets(1,1,10,[{'field1':'data1'},{'field2':'data2'}]) ;
				expect(MONOloop.assets[0].datahash).toEqual('cfeb239a0bd5a0d6f8881ad7');
			}) ;

			it("Should not track empty asset",function(){
				MONOloop.assets = [] ;
				MONOloop.trackAssets(1,1,10,[]) ;
				MONOloop.trackAssets(1,1,10,[{}]) ;
				MONOloop.trackAssets(1,1,10,[{},{}]) ;
				MONOloop.trackAssets(1,1,10,[{'field1':'data1'},{'field2':'data2'}]) ;
				expect(MONOloop.assets.length).toEqual(1);
			}) ;
		}) ;

        describe("Params Checking",function(){
            it("isServerLoading",function(){
                expect(MONOloop.isTrackServerSide()).toEqual(false);
                expect(MONOloop.isTrackServerSide('http://moimet.net/thinnguyen/index.html?trackServerSide=true')).toEqual(true);
            }) ;
            it("getUrlParameters",function(){
            	var testURL = 'http://moimet.net/thinnguyen/index.html?trackServerSide=true&vid=53ce29ec4326d0be4b731f6e' ;
            	var vid = MONOloop.getUrlParameters('vid' , testURL ) ;
            	expect(vid).toEqual('53ce29ec4326d0be4b731f6e') ;
            	var trackServerSide = MONOloop.getUrlParameters('trackServerSide' , testURL ) ;
            	expect(trackServerSide).toEqual('true') ;
            }) ;
            it("removeServerSideTrack",function(){
            	var testURL = 'http://moimet.net/thinnguyen/index.html?trackServerSide=true&vid=53ce29ec4326d0be4b731f6e' ;
            	var url = MONOloop.removeServerSideTrack(testURL) ;
            	expect(url).toEqual('http://moimet.net/thinnguyen/index.html') ;
            	testURL = 'http://monoloop.com/test/?data=xxx&trackServerSide=true&vid=53ce29ec4326d0be4b731f6e' ;
            	var url = MONOloop.removeServerSideTrack(testURL) ;
            	expect(url).toEqual('http://monoloop.com/test/?data=xxx') ;

            	testURL = 'http://monoloop.com/test/?data=xxx&trackServerSide=false&vid=53ce29ec4326d0be4b731f6e' ;
            	var url = MONOloop.removeServerSideTrack(testURL) ;
            	expect(url).toEqual('http://monoloop.com/test/?data=xxx&trackServerSide=false&vid=53ce29ec4326d0be4b731f6e') ;
            }) ;
        }) ;

		describe("Placement Process", function() {
			it("Should place content include javascript", function(){
				document.getElementById('test').innerHTML = '' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test' ,
			        placementType : '3',
			        content : "123 <script type='text\/javascript'>var t = 1 ; <\/script>",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	expect(t).toEqual(1) ;
			});



			it("Should place before" , function(){
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test p' ,
			        placementType : '0',
			        content : "T",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	expect(document.getElementById('test').innerHTML).toEqual('T<p>T</p>') ;
			}) ;

			it("Should place after" , function(){
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test p' ,
			        placementType : '1',
			        content : "T",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	expect(document.getElementById('test').innerHTML).toEqual('<p>T</p>T') ;
			}) ;

			it("Should place inside before" , function(){
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test p' ,
			        placementType : '2',
			        content : "X",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	expect(document.getElementById('test').innerHTML).toEqual('<p>XT</p>') ;
			}) ;

			it("Should place inside after" , function(){
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test p' ,
			        placementType : '3',
			        content : "X",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	expect(document.getElementById('test').innerHTML).toEqual('<p>TX</p>') ;
			}) ;

			it("Should place replace" , function(){
				document.getElementById('test').innerHTML = '<p>T</p>' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test p' ,
			        placementType : '4',
			        content : "X",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	expect(document.getElementById('test').innerHTML).toEqual('X') ;


			 	//MONOloop.MonoloopData={"success":true,"eval":"MONOloop.setCookie(\"mid\",\"536107893ebb02be670011d8\", 525600); MONOloop.setCookie(\"vid\",\"536107893ebb02be670011d9\", 30); MONOloop.setCookie(\"skey\",\"b38c53eef00ab24fc98d47837fc7c4150558c824494565d97d4cf42b7775da7bb1844a5508ce11f06038859a9c9d07031bff49433b21f79933c5808df1f7d6cc\", 525600)","data":[{"name":"First Time Visitor (USB)","uid":"6958","mappedTo":"HTML BODY DIV#service DIV#page DIV.wrap.clear DIV#content DIV.grids.eqHeights.mt15","placementType":"4","order":"2","displayType":"1","addJS":"","pageID":"1066","urlOption":null,"incWWW":null,"fullURL":"https://shop.telia.dk/index.ep","additionalJS":"","content":"  <div class=\"grids eqHeights mt15\">\t<img src=\"https://mlres.s3.amazonaws.com/assets/813/USP-Banner_940x300_2.jpg\" alt=\"\" /> </div>  "},{"name":"MobileBanner","uid":"6951","mappedTo":"HTML BODY DIV#service DIV#page DIV.wrap.clear DIV#content DIV.grids.eqHeights.mt15","placementType":"4","order":"2","displayType":"1","addJS":"","pageID":"1066","urlOption":null,"incWWW":null,"fullURL":"https://shop.telia.dk/index.ep","additionalJS":"","content":"  <div class=\"grids eqHeights mt15\">\n\n\t\n<div id=\"banner\" style=\"font-family: Arial, HelveticaNeue, Helvetica, sans-serif;background-image: url(https://mlres.s3.amazonaws.com/assets/813/banner-bg.jpg);width: 940px;height: 300px;\">\n\t\t<div id=\"ml_title\" style=\"float: left;background-image: url(https://mlres.s3.amazonaws.com/assets/813/se_lige_her.png);width: 299px;height: 300px;position: relative;\">\n\t\t\t\n\t\t</div>\n\t\t<div class=\"content\" style=\"float: left;position: relative;\">\n\t\t\t<img src=\"https%3A%2F%2Fshop.telia.dk%2FrenderImage.image%3FimageName%3DiPhone_4S_white_340x380_rabat960.png%26padding%3D0\" alt=\"\" style=\"height: 280px;padding-top: 10px;\">\n\t\t</div>\n\t\t<div class=\"text\" style=\"float: left;position: relative;top: 130px;left: 23px;\">\n\t\t\t<h1 id=\"ml_header\" style=\"margin: 0;\tpadding: 0;\tfont-size: 19px;width: 300px;\">Apple iPhone 4S 8GB White</h1>\n\t\t\t<ul id=\"ml_details\" style=\"list-style-type: none;padding: 0;margin: 10px 0 0 0;font-size: 12px;width: 180px;list-style-image: none;\">\n\t\t\t\t<li class=\"border monoloopBorder\" style=\"padding: 5px 0;border-bottom: 1px solid black;margin: 0px;\">\n\t\t\t\t\t<div class=\"left\" style=\"position: relative;float: left;width: 106px;\">\n\t\t\t\t\t\tMobilaftale <span class=\"period\">24</span> mdr.\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"right\" style=\"position: relative;font-weight: bold;float: right;\">70kr./md.</div>\n\t\t\t\t\t<div style=\"clear:both;position: relative;\"></div>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"border\" style=\"padding: 5px 0;border-bottom: 1px solid black;margin: 0px;\">\n\t\t\t\t\t<div class=\"left\" style=\"position: relative;float: left;width: 106px;\">\n\t\t\t\t\t\tFri tale, 20 GB & Spotify Premium\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"right\" style=\"position: relative;font-weight: bold;float: right;\">\n\t\t\t\t\t\t<div style=\"padding-top:13px;\" class=\"\">299kr./md.</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div style=\"clear:both;position: relative;\"></div>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"\" style=\"padding: 5px 0;margin: 0px;\">\n\t\t\t\t\t<div class=\"left\" style=\"position: relative;float: left;width: 106px;\">Min. Pris</div>\n\t\t\t\t\t<div class=\"right nobold\" style=\"position: relative;font-weight: bold;float: right;font-weight: normal;\">3.610,50 kr.</div>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</div>\n<a href=\"https%3A%2F%2Fshop.telia.dk%2Fapple-iphone-4s-8gb%2Fp2abd5603_bf8b_42f8_b94b_794262f31515.html\"> \n<div id=\"ml_button\" style=\"width: 92px;height: 34px;background-image: url(https://mlres.s3.amazonaws.com/assets/813/button.png);position: relative;top: 246px;\tleft: 809px;\"></div> </a>\n\t</div>\n\n</div> "}]} ;



			}) ;

			it("Should place content include jquery" , function(){
				document.getElementById('test').innerHTML = '' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test' ,
			        placementType : '3',
			        content : "123 <script type='text\/javascript'>var t = MONOloop.$.trim(' HAHAHA   ') ; <\/script>",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	expect(t).toEqual('HAHAHA') ;
			}) ;

			it("Should place content with jquery - complex 1 ( globalEval) " , function(){
				var val = 123 ;
				var pos = 1 ;
				var evl = 'var COUNT_' + pos + ' = ' + val + ';';

				document.getElementById('test').innerHTML = '' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test' ,
			        placementType : '3',
			        content : "<script type='text\/javascript'>MONOloop.$.globalEval('"+ evl +"')<\/script>",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	expect(COUNT_1).toEqual(123) ;
			}) ;

			it("Should place content with jquery - complex 2 " , function(){
				document.getElementById('test').innerHTML = '' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				var obj = {
			        mappedTo : '#test' ,
			        placementType : '3',
			        content : "<script type='text\/javascript'>var x = MONOloop.$('html,body').animate({scrollTop:0}, 'slow');<\/script>",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;
			 	expect(x).toBeDefined() ;
			}) ;

			it("it should work with _gaq.push" , function(){
				document.getElementById('test').innerHTML = '' ;
				MONOloop.MonoloopData = {}
				MONOloop.MonoloopData.data = [] ;
				MONOloop.GAEnabled = true ;

				var obj = {
					uid : 12 ,
					name : 'Test' ,
			        mappedTo : '#test' ,
			        placementType : '3',
			        content : "123",
			        abg : false
			 	} ;
			 	MONOloop.unsafe = true ;
			 	MONOloop.contentReady = true ;
		  		MONOloop.delivered = false ;
				MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			 	obj.abg = true ;
			 	MONOloop.delivered = false ;
			 	MONOloop.MonoloopData.data.push(obj) ;
			 	MONOloop.placeContent() ;

			}) ;

			it("Should work with empty return script",function(){
				delete MONOloop.MonoloopData ;
				MONOloop.placeContent() ;
			}) ;
		}) ;

	});

}) ;
