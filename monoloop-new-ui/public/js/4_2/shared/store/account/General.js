// Ajax loading ; 
Ext.define('UI.shared.store.account.General', {
	
	url: '/api/account',
    method: 'GET',
    timeout: 30000,
    autoRequest : true , 
    
    constructor: function(config) {
        var me = this; 
		Ext.apply(me,config);
        me.callParent();
        
        if( me.autoRequest == true)
        	me.request() ; 
    },

    success: function(response, opts) { 
     
    },

    failure: function(response, opts) {
        alert('Fail! ' + response.status);
    },
    
    params: {},

    request: function() {
        var me = this;
        Ext.Ajax.request({
            success: me.success,
            failure: me.failure,
            url: me.url,
            method: me.method,
            params: me.params
        });
    }
}) ; 