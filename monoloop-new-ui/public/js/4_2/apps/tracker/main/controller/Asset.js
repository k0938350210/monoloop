Ext.define('Tracker.controller.Asset', {
	extend: 'Ext.app.Controller',
	
	init: function() {
		var me = this; 
		me.control({  	
			'#main-panel' : {
				'newAsset' : me.newAsset , 
				'assetEdit' : me.assetEdit 
			} , 
			//----------- Field Set UP 
			'assetfieldsetup' : {
				beforeclose : 'onRemoveStage'
			} , 
			'assetfieldsetup button[text="CANCEL"]' : {
				'click' : me.closeFieldSetup
			} , 
			'assetfieldsetup button[text="NEXT"]' : {
				'click' : me.processStep1
			} , 
			
			'assetfieldsetup button[text="Add new field"]' : {
				'click' : me.addNewField
			} , 
			
			//----------- New Field Window 
			'assetnewfieldwindow button[text="SAVE"]' : {
				'click' : me.newFieldWindowSave
			} , 
			'assetnewfieldwindow button[text="CANCEL"]' : {
				'click' : me.newFieldWindowClose 
			}
		});	
	} , 
	
	onRemoveStage : function(){
		Ext.util.History.add(''); 
	} ,
	
	// Event ; 
	
	newAsset : function(config){
		Ext.create('Tracker.view.asset.FieldSetup',{
			name : config.activeName
		}).show() ;
	} ,
	
	assetEdit : function(config){ 
		var fieldSetup = Ext.create('Tracker.view.asset.FieldSetup',{
            uid :parseInt( config.data.uid ) , 
            activeURL : config.data.example_url , 
            originalURL : config.data.url , 
            name : config.data.name ,
            ttl :parseInt( config.data.ttl ) , 
            activeRegex : config.data.regex  ,
            activeUrlOption :  parseInt(  config.data.urlOption ) ,
            activeInc_www :parseInt(  config.data.inc_www ) ,
            activeInc_http_https : parseInt( config.data.inc_http_https )  
        }).show() ; 
        
        for(var i = 0 ; i < config.details.length ; i++){
            var field = config.details[i] ; 
            fieldSetup.addField(  parseInt(field.uid) , field.name , field.fieldType ,  parseInt(field.typeId) , field.typeData ,  parseInt(field.filterType) , field.postUserFunc ,  parseInt(field.saveFunction) , field.exampleData  ) ; 
        }
	}  ,
	//----------- Field Set UP 
	
	closeFieldSetup : function(btn){
		btn.up('window').close() ; 
		Ext.util.History.add('');
	} , 
	
	processStep1 : function(btn){
		// Check as 1 field minimum ;
		var win = btn.up('window') ; 
        if( Ext.getCmp(win.id + '-nofield') != undefined ){
            Ext.Msg.alert('Error', 'Require minimum 1 field.');
            return ; 
        } 
        win.activeURL = Ext.getCmp( win.id + '-example-url').getValue() ;  
        var form = Ext.getCmp(win.id + '-form').getForm() ;  
        if( form.isValid()){ 
            // Generate post data ; 
            var url = win.originalURL; 
            if(url == ''){
                url = win.activeURL ; 
            }
            var postData = {} ;  
            postData['uid'] = win.uid ; 
            postData['name'] = Ext.getCmp(win.id + '-name').getValue() ; 
            postData['ttl'] = Ext.getCmp(win.id + '-ttl').getValue() ; 
            postData['example_url'] = win.activeURL  ;  
            for(var i = 0 ; i <  Ext.getCmp(win.id + '-root-fieldset').items.length - 1 ; i++ ){ 
                var fieldObj = Ext.getCmp(win.id + '-root-fieldset').items.items[i] ;
                if( fieldObj.flagDelete == 1)
                	continue ;
                postData['detail['+i+'][uid]'] = fieldObj.uid ; 
                postData['detail['+i+'][name]'] = fieldObj.title ; 
                postData['detail['+i+'][fieldType]'] = fieldObj.fieldType ; 
                postData['detail['+i+'][activeTypeId]'] = fieldObj.items.items[1].getValue() ; 
                postData['detail['+i+'][activeTypeData]'] = fieldObj.activeTypeData ; 
                postData['detail['+i+'][activeFilterType]'] = fieldObj.activeFilterType ; 
                postData['detail['+i+'][activePostUserFunc]'] = fieldObj.activePostUserFunc ; 
                postData['detail['+i+'][activeSaveFunction]'] = fieldObj.activeSaveFunction ; 
                postData['detail['+i+'][activeExample]'] = fieldObj.activeExample ; 
                postData['detail['+i+'][activeTTL]'] = fieldObj.activeTTL ; 
            } 
            // Open last window .
            var postURL = 'index.php?eID=monoloop_assets&pid='+assets.pid+'&cmd=assets/save' ; 
            Ext.create('Tracker.view.tracker.Stage4Window' , {
                id : win.id + 'lastwindow' , 
                activeRegex : win.activeRegex , 
                activeUrlOption : win.activeUrlOption , 
                activeInc_www : win.activeInc_www ,
                activeInc_http_https : win.activeInc_http_https ,
                activeURL : url , 
                postData : postData , 
                postURL : postURL , 
                saveCompleteProcess : function(){
                    this.close() ; 
                    win.close() ; 
                    Ext.getCmp('main-panel').fireEvent('refreshData') ; 
                    //Ext.getCmp('assets-list').mLoadData() ;  
                }
            }).show() 
        }
	} ,  
	
	addNewField : function(btn){
		var win = btn.up('window') ; 
		Ext.create('Tracker.view.asset.NewFieldWindow',{
            parent_id : win.id 
        }).show() ; 
	} , 
	
	//----------- New Field Window 
	
	newFieldWindowClose : function(btn){
		var win = btn.up('window') ; 
		win.close() ;
	} , 
	
	newFieldWindowSave : function(btn){
		var win = btn.up('window') ; 
		var form = win.bodyPanel.getForm() ; 
        if( form.isValid()){
            if( win.editfield_id != null ){
                var editField = Ext.getCmp(win.editfield_id) ; 
                editField.title = Ext.getCmp(win.id + '-name').getValue() ; 
                editField.setTitle(editField.title) ; 
                editField.fieldType =  Ext.getCmp(win.id + '-type').getValue() ;   
                win.close() ; 
            }else{
                Ext.getCmp(win.parent_id).addField(0 ,  Ext.getCmp(win.id + '-name').getValue() , Ext.getCmp(win.id + '-type').getValue() , null , '' , 0 , '' , 0  , '' )  ; 
                win.close() ; 
            } 
        }
	}
}) ; 