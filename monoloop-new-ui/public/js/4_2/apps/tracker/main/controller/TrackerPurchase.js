Ext.define('Tracker.controller.TrackerPurchase', {
	extend: 'Ext.app.Controller',
	
	activeUID : 0 , 
    activeName : '' , 
    activeType : 0 ,
    activeURL : '' , 
    activeTypeData : '' , 
    activeHidden : 0 , 
    activeRegex : '' , 
    activeUrlOption : 0 , 
    activeInc_www : 0 ,
    
    init: function() {
		var me = this; 
		me.control({  	
			'#main-panel' : {
				'trackerPurchaseEdit' : me.trackerPurchaseEdit  
			}  
		});	
	} , 
	
	trackerPurchaseEdit : function(config){
		var me = this ; 
		me.activeUID  = parseInt(config.data.uid) ; 
        me.activeName = config.data.name ; 
        me.activeURL = config.data.url ; 
        me.activeRegex = config.data.regex ;
        me.activeUrlOption = parseInt(config.data.urlOption) ;
        me.activeInc_www = parseInt(config.data.inc_www) ;
        me.tracker_type = parseInt(config.data.tracker_type) ;  
        me.activeTypeData = config.data.addition_data ; 
        me.createFlow() ; 
	} , 
    
    resetActiveData : function(){ 
        var me = this ; 
        me.activeUID = 0 ; 
        me.activeName = '' ; 
        me.activeType = 0 ; 
        me.activeURL = '' ; 
        me.activeTypeData = '' ; 
        me.activeHidden = 0 ; 
        me.activeRegex = ''; 
        me.activeUrlOption = 0 ; 
        me.activeInc_www = 0 ; 
    } ,  
    
    createFlow : function(){
        var me = this ; 
        me.createFlowStep2() ; 
    } , 
    
    createFlowStep2 : function(){
        var me = this ; 
        tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2Window', { 
            id : 'stage2-flow' , 
            activeType : 1 ,
            activeURL : me.activeURL , 
            activeTypeData : me.activeTypeData , 
            temPath : tracker_main.temPath , 
            firstDomain : tracker_main.firstDomain  , 
            resetActiveData : function(){  
                //tracker_value.resetActiveData() ; 
            } , 
            select_process : function(){ 
                me.activeURL = Ext.getCmp('stage2-flow').activeURL ;    
                me.createFlowStep4() ; 
            } 
        }).show() ;  
    } , 
    
    createFlowStep4 : function(){
        var me = this ; 
        
        var postData = {} ; 
        postData['activeUID'] = me.activeUID ;     
        postData['activeHidden'] = me.activeHidden ;    
        
        var postURL = 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveTrackersPurchase' ; 
        
        Ext.create('Tracker.view.tracker.Stage4Window' , {
            id : 'stage4' , 
            activeRegex : me.activeRegex , 
            activeUrlOption : me.activeUrlOption , 
            activeInc_www : me.activeInc_www ,
            activeURL : me.activeURL , 
            postData : postData , 
            postURL : postURL , 
            resetActiveData : function(){ 
                me.resetActiveData() ; 
            } , 
            saveCompleteProcess : function(){
                Ext.getCmp('stage4').close() ;  
                Ext.getCmp('stage2-flow').close() ;
                Ext.getCmp('trackers-list').mLoadData() ;  
            }
        }).show() ;
    }
}) ; 