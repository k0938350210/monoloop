Ext.define('Tracker.controller.Node', {
	extend: 'Ext.app.Controller',
 
	
	init: function() {
		var me = this; 
		me.control({
			'#main-panel' : {
				refreshData : me.refreshData
			} , 
			'trackernodelist' : {
				editNode : me.onListEditNode , 
				nodeEdit2 : me.onNodeEdit2 , 
				nodeSetHidden : me.onNodeSetHidden  , 
				nodeDelete : me.onNodeDelete 
			},  
			'trackeredit2base' : {
				beforeclose : 'onCloseEditWindow'
			} , 
			'trackeredit2base button[text="CANCEL"]':{
				click : 'onCancelNewNode'
			} , 
			'button[cls="tabcreate2-btn"]' : {
				click : 'onCreateTrackerClick' 
			},
			'newtrackerwindow' : {
				beforeclose : 'onCancelWindowNewNode' 
			},
			'newtrackerwindow button[text="NEXT"]' : {
				click : 'onCreateNewNode' 
			},
			'newtrackerwindow button[text="CANCEL"]' : {
				click : 'onCancelNewNode'
			}		
		});	
	} , 
	
	// Method 
	
	newnode : function(config){
		Ext.create('Tracker.view.NewTrackerWindow').show() ;
	} , 
	
	editnode : function(config){  
		Ext.getCmp('main-panel').setLoading('Editing') ; 
 
        Ext.Ajax.request({
            url: 'index.php/tracker/'+config.id+'?eID=ml_tracker' , 
			// process the response object to add it to the TabPanel:
			success: function(xhr){
      			var data = Ext.JSON.decode(xhr.responseText) ;
      			if(data.type == 'customvar'){
      				Ext.getCmp('main-panel').fireEvent('customvarEdit' , data) ;
      			}else if(data.type == 'asset'){
      				Ext.getCmp('main-panel').fireEvent('assetEdit' , data ) ; 
      			}else if(data.type == 'tracker'){ 
      				var tracker_type = parseInt(data.data.tracker_type) ;   
      				if( tracker_type == 1){
      					Ext.getCmp('main-panel').fireEvent('trackerProductEdit' , data) ; 
      				}else if( tracker_type <= 4){
      					Ext.getCmp('main-panel').fireEvent('trackerValueEdit' , data) ; 
      				}else if( tracker_type == 6){
      					Ext.getCmp('main-panel').fireEvent('trackerSearchEdit' , data) ; 
      				}else if( tracker_type == 7){
      					Ext.getCmp('main-panel').fireEvent('trackerBasketEdit' , data) ; 
      				}else if( tracker_type == 8){
      					Ext.getCmp('main-panel').fireEvent('trackerPurchaseEdit' , data) ; 
      				}else if( tracker_type == 9){
      					Ext.getCmp('main-panel').fireEvent('trackerDownloadEdit' , data) ; 
      				}
      			}
				Ext.getCmp('main-panel').setLoading(false) ; 
			},
			failure: function(){ 
                Ext.MessageBox.alert('Error', '#502' ) ; 
			}
		});	
	} , 
	
	editnode2 : function(config){
		Ext.getCmp('main-panel').setLoading('Editing') ; 
 
        Ext.Ajax.request({
            url: 'index.php/tracker/'+config.id+'?eID=ml_tracker' , 
			// process the response object to add it to the TabPanel:
			success: function(xhr){
      			var data = Ext.JSON.decode(xhr.responseText) ;
      			if(data.type == 'customvar'){
      				Ext.getCmp('main-panel').fireEvent('customvarEdit2' , data) ;
      			}else if(data.type == 'tracker'){
      				Ext.getCmp('main-panel').fireEvent('trackerEdit2' , data) ;  
      			}
				Ext.getCmp('main-panel').setLoading(false) ; 
			},
			failure: function(){ 
                Ext.MessageBox.alert('Error', '#502' ) ; 
			}
		});	
	} , 
	
	//Event ; 
	onCreateTrackerClick : function(btn){
		var url = NodeApp.router.generate('nodeNew', {controller: 'Tracker.controller.Node', action: 'new' });
		Ext.util.History.add(url); 
	} , 
	
	onListEditNode : function(config){
		var url = NodeApp.router.generate('nodeEdit', {controller: 'Tracker.controller.Node', action: 'edit' , id : config.uid });
		Ext.util.History.add(url); 
	} , 
	
	onNodeEdit2 : function(config){
		var url = NodeApp.router.generate('nodeEdit2', {controller: 'Tracker.controller.Node', action: 'edit2' , id : config.uid });
		Ext.util.History.add(url); 
	} ,
	
	onNodeSetHidden : function(config){
		Ext.getCmp('main-panel').setLoading('Updating');
		Ext.Ajax.request({
            url: 'index.php/tracker/' + config.uid + '/sethidden?eID=ml_tracker'  ,
            params:  {
            	hidden : config.hidden 
            },
			// process the response object to add it to the TabPanel:
			success: function(xhr){
               	Ext.getCmp('main-panel').setLoading(false);
               	var data = Ext.JSON.decode(xhr.responseText) ;
               	if(data.success == true){
               		Ext.getCmp('main-panel').fireEvent('refreshData') ; 
               	}else{
               		Ext.MessageBox.alert('Error', data.msg ) ; 
               	}
			},
			failure: function(){ 
				Ext.getCmp('main-panel').setLoading(false);
			}
		});	
	} , 
	
	onNodeDelete : function(config){
		Ext.getCmp('main-panel').setLoading('Deleting');
		Ext.Ajax.request({
            url: 'index.php/tracker/' + config.uid + '/delete?eID=ml_tracker'  , 
            params:  {
            	token : '4AWD'  
            },
			// process the response object to add it to the TabPanel:
			success: function(xhr){
               	Ext.getCmp('main-panel').setLoading(false);
               	var data = Ext.JSON.decode(xhr.responseText) ;
               	if(data.success == true){
               		Ext.getCmp('main-panel').fireEvent('refreshData') ; 
               	}else{
               		Ext.MessageBox.alert('Error', data.msg ) ; 
               	}
			},
			failure: function(){ 
				Ext.getCmp('main-panel').setLoading(false);
			}
		});	
	} , 
	
	refreshData : function(){
		// Refresh Data ; 
		Ext.util.History.add(''); 
		Ext.getCmp('main-panel').down('trackernodelist').mLoadData() ; 
	} , 
	
	onCancelWindowNewNode : function(){
		Ext.util.History.add('');
	} , 
	
	// New tracker window ; 
	onCancelNewNode : function(btn){
		btn.up('window').close() ; 
		Ext.util.History.add('');
	} , 
	
	onCreateNewNode : function(btn){
		var frm =	btn.up('window').down('form') ;
		if(frm.getForm().isValid()){ 
			var name = frm.down('textfield[name="name"]').getValue()  ; 
			var type = frm.down('combo[name="type"]').getValue() ;   
			if(type == 'tracker'){
				Ext.getCmp('main-panel').fireEvent('newTrackerValue',{
					activeName : name
				}) ;  
 			}else if(type == 'customvar'){
 				Ext.getCmp('main-panel').fireEvent('newCustomvar',{
					activeName : name
				}) ;  
 			}else{
 				Ext.getCmp('main-panel').fireEvent('newAsset',{
					activeName : name
				}) ; 
 			}
 			btn.up('window').close() ; 
		} 
	} , 
	
	onCloseEditWindow : function(window){
		Ext.util.History.add('');
	}	 
}) ; 