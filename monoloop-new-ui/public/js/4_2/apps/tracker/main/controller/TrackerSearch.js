Ext.define('Tracker.controller.TrackerSearch', {
	extend: 'Ext.app.Controller',
	
	activeUID : 0 , 
    activeName : '' , 
    activeType : 0 ,
    activeURL : '' , 
    activeTypeData : '' , 
    activeHidden : 0 , 
    activeRegex : '' , 
    activeUrlOption : 0 , 
    activeInc_www : 0 ,
    
    activeAdditionDataObj : {} , 
	
	init: function() {
		var me = this; 
		me.control({  	
			'#main-panel' : {
				'trackerSearchEdit' : me.trackerSearchEdit  
			}  
		});	
	} , 
	
	trackerSearchEdit : function(config){
		var me = this ; 
		
		me.activeUID  = parseInt(config.data.uid) ; 
        me.activeName =  config.data.name ; 
        me.activeURL =  config.data.url ; 
        me.activeRegex = config.data.regex ;
        me.activeUrlOption = parseInt(config.data.urlOption) ;
        me.activeInc_www = parseInt(config.data.inc_www) ;
        me.tracker_type = parseInt(config.data.tracker_type) ; 
        if( Ext.String.trim( config.data.addition_data ) != ''  ){
             var jsonData = Ext.JSON.decode( config.data.addition_data ) ;     
             me.activeAdditionDataObj =  jsonData ; 
        }
        me.showFlowStep2() ;  
	} , 
	
	resetActiveData : function(){
        var me = this ; 
        me.activeUID = 0 ; 
        me.activeName = '' ; 
        me.activeURL = '' ; 
        me.activeHidden = 0 ; 
        me.activeRegex = '' ; 
        me.activeUrlOption = 0 ; 
        me.activeInc_www = 0 ;
    } , 
    
    showFlowStep2 : function(){
        var me = this ;  
        tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2SearchWindow', { 
            firstDomain :  tracker_main.firstDomain , 
            temPath : tracker_main.temPath , 
            activeURL : me.activeURL , 
            dataObj : me.activeAdditionDataObj ,
            id : 'flow2-search' , 
            processNext : function(){ 
                me.activeURL = Ext.getCmp('flow2-search').activeURL ; 
                me.activeAdditionDataObj = Ext.getCmp('flow2-search').dataObj ; 
                me.showFlowStep4() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep4 : function(){
        var me = this ; 
        
        var postData = {} ; 
        postData['activeUID'] = me.activeUID ; 
        postData['activeURL'] = me.activeURL ; 
        postData['activeName'] = me.activeName ;  
        postData['activeHidden'] = me.activeHidden ;  
        postData['searchTextbox_xpath'] = me.activeAdditionDataObj.searchTextbox_xpath ;   
        postData['searchButton_xpath'] = me.activeAdditionDataObj.searchButton_xpath ;   
        
        var postURL = 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveTrackersSearch2' ; 
        
        Ext.create('Tracker.view.tracker.Stage4Window' , {
            id : 'stage4' , 
            activeRegex : me.activeRegex , 
            activeUrlOption : me.activeUrlOption , 
            activeInc_www : me.activeInc_www ,
            activeURL : me.activeURL , 
            postData : postData , 
            postURL : postURL , 
            resetActiveData : function(){ 
                me.resetActiveData() ; 
            } , 
            saveCompleteProcess : function(){
                Ext.getCmp('stage4').close() ; 
                if( Ext.getCmp('flow2-search') != undefined){
                    Ext.getCmp('flow2-search').close() ; 
                }
                Ext.getCmp('trackers-list').mLoadData() ;  
            }
        }).show() ; 
    }
	
}) ; 