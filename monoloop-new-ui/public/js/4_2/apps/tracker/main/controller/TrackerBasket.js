Ext.define('Tracker.controller.TrackerBasket', {
	extend: 'Ext.app.Controller',
	
	activeUID : 0 , 
    activeName : '' , 
    activeURL : '' , 
    activeHidden : 0 , 
    activeRegex : '' , 
    activeUrlOption : 0 , 
    activeInc_www : 0 ,
    tracker_type : 0 , 
    
    activeAdditionDataObj : {} , 
    
    init: function() {
		var me = this; 
		me.control({  	
			'#main-panel' : {
				'trackerBasketEdit' : me.trackerBasketEdit  
			}  
		});	
	} , 
	
	trackerBasketEdit : function(config){
		var me = this ; 
		me.activeUID  = parseInt(config.data.uid) ; 
        me.activeName = config.data.name ; 
        me.activeURL =  config.data.url ; 
        me.activeRegex =  config.data.regex ;
        me.activeUrlOption = parseInt(config.data.urlOption) ;
        me.activeInc_www =  parseInt(config.data.inc_www) ;
        me.tracker_type =  parseInt(config.data.tracker_type) ; 
        if( Ext.String.trim(  config.data.addition_data ) != ''  ){
             var jsonData = Ext.JSON.decode(  config.data.addition_data ) ;     
             me.activeAdditionDataObj =  jsonData ; 
        }
        me.create_flow() ; 
	} , 
	
	resetActiveData : function(){
        var me = this ; 
        me.activeUID = 0 ; 
        me.activeName = '' ; 
        me.activeURL = '' ; 
        me.activeHidden = 0 ; 
        me.activeRegex = '' ; 
        me.activeUrlOption = 0 ; 
        me.activeInc_www = 0 ;
    } , 
    
    create_flow : function(){
        var me = this ; 
         
        
        if( me.tracker_type == 8){ 
            tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2BasketWindow', { 
                step1Text : 'Goto comfirm purchase page' ,
                step2Text : 'Select purchase table' ,
                firstDomain :  tracker_main.firstDomain , 
                temPath : tracker_main.temPath , 
                activeURL : me.activeURL , 
                dataObj : me.activeAdditionDataObj ,
                id : 'flow2-basket' , 
                processNext : function(){
                    me.activeURL = Ext.getCmp('flow2-basket').activeURL ; 
                    me.activeAdditionDataObj = Ext.getCmp('flow2-basket').dataObj ; 
                    me.showFlowStep4() ; 
                }
            }).show() ; 
        }else{
            tracker_main.step2Obj =  Ext.create('Tracker.view.tracker.Stage2BasketWindow', { 
                firstDomain :  tracker_main.firstDomain , 
                temPath : tracker_main.temPath , 
                activeURL : me.activeURL , 
                dataObj : me.activeAdditionDataObj ,
                id : 'flow2-basket' , 
                processNext : function(){
                    me.activeURL = Ext.getCmp('flow2-basket').activeURL ; 
                    me.activeAdditionDataObj = Ext.getCmp('flow2-basket').dataObj ; 
                    me.showFlowStep3() ; 
                }
            }).show() ; 
        } 
        
        //me.showFlowStep3() ; 
    } , 
    
    showFlowStep3 : function(){
        var me = this ; 
         
        //trackerFlowStage3Basket
        Ext.create('Tracker.view.tracker.Stage3BasketWindow', { 
            id : 'flow3-basket' , 
            dataObj : me.activeAdditionDataObj ,
            activeURL : me.activeURL , 
            processNext : function(){

                me.activeAdditionDataObj = Ext.getCmp('flow3-basket').dataObj ; 
                Ext.getCmp('flow3-basket').close() ;  
                me.showFlowStep4() ; 
            }
        }).show() ; 
    } , 
    
    showFlowStep4 : function(){
        var me = this ; 
         
        
        var postData = {} ; 
        postData['activeUID'] = me.activeUID ; 
        postData['activeURL'] = me.activeURL ; 
        postData['activeName'] = me.activeName ;  
        postData['activeHidden'] = me.activeHidden ;  
        for (variable in me.activeAdditionDataObj){
            postData['addition[' + variable + ']'] = me.activeAdditionDataObj[variable] ; 
        } 
        
        var postURL = 'index.php?eID=monoloop_trackers&pid=480&cmd=trackers/saveTrackersBasket' ; 
        
        Ext.create('Tracker.view.tracker.Stage4Window' , {
            id : 'stage4' , 
            activeRegex : me.activeRegex , 
            activeUrlOption : me.activeUrlOption , 
            activeInc_www : me.activeInc_www ,
            activeURL : me.activeURL , 
            postData : postData , 
            postURL : postURL , 
            resetActiveData : function(){ 
                me.resetActiveData() ; 
            } , 
            saveCompleteProcess : function(){
                Ext.getCmp('stage4').close() ; 
                if( Ext.getCmp('flow2-basket') != undefined){
                    Ext.getCmp('flow2-basket').close() ; 
                }
                Ext.getCmp('trackers-list').mLoadData() ;  
            }
        }).show() ; 
    }
}) ; 