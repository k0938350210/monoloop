var NodeApp = null ; 

var tracker_main = {
	step2Obj : null , 
	baseUrl : '' , 
    temPath : '' , 
	init : function(){
		
	}
} ; 

var customvars_main = {
	baseUrl : '' , 
    temPath : '' ,  
    firstDomain : '' , 
	init : function(){
		
	}
} ;

var assets = {
    baseUrl : '' , 
    temPath : '' , 
    xpathObj : null , 
    pid : 0 
} ; 

Ext.onReady(function() { 
	Ext.tip.Tip.prototype.minWidth = 300;
	NodeApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Node', 
	  	controllers: ['Tracker.controller.Node' , 'Tracker.controller.Tracker' , 'Tracker.controller.CustomVar' , 'Tracker.controller.Asset' , 'Tracker.controller.TrackerProduct' , 'Tracker.controller.TrackerSearch' , 'Tracker.controller.TrackerBasket' , 'Tracker.controller.TrackerPurchase' , 'Tracker.controller.TrackerDownload' ], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('Tracker.view.Mainpanel', {  
				id : 'main-panel' , 
	        	renderTo: Ext.get('tracker-panel') 	        		        	
	        });
	      
	        Ext.create('Ext.Button', {
	            text    : 'Create new tracker',
	            iconCls : 'newpage-icon' ,  
	            cls : 'tabcreate2-btn' , 
	            height : 27 , 
	            renderTo: 'tracker-create-button'  
	        }) ; 
	       
	        Ext.create('Ext.form.FieldContainer',{
	        	width: 203,
	        	layout : 'hbox' , 
	        	renderTo : 'tracker-search-text' ,
	        	items : [{
	        		xtype : 'textfield' , 
					width : 180 , 
					id : 'tracker-search-text-id' , 
					emptyText : 'Enter Search Keyword' , 
					enableKeyEvents : true , 
					listeners : {
		                'keypress' : function(textField ,e){
		                    if(e.keyCode == e.ENTER) { 
		                        Ext.getCmp('trackers-list').mLoadData() ; 
		                    }
		                }
		            }
	        	},{
	        		xtype : 'button' , 
					cls : 'search-btn' ,  
					width : 23 , 
					height : 22 , 
					handler : function(){ 
						Ext.getCmp('trackers-list').mLoadData() ; 
					}
	        	}]
	        }) ;
	        
	        router.name('nodeNew', 'node/new', {controller: 'Tracker.controller.Node', action: 'newnode'});
	        router.name('nodeEdit', 'node/:id', {controller: 'Tracker.controller.Node', action: 'editnode'});
	        router.name('nodeEdit2', 'nodeproperty/:id', {controller: 'Tracker.controller.Node', action: 'editnode2'});
	    }
	    
	});
}) ; 