Ext.define('Tracker.view.tracker.Stage1ProductWindow', { 
    extend: 'Tracker.view.tracker.Stage1ValueWindow',
    alias: 'widget.trackerflowstage1product',
    
    activeType3 : 0 , 
    
    initComponent: function(){ 
        var me = this ; 
		this.callParent(arguments); 
		me.height = 575 ; 
		me.bodyPanel.insert(4,{
            xtype: 'fieldset',  
            title : 'Category' , 
            defaults: {
                labelWidth: 89,
                anchor: '100%' 
            },
            items : [
                {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<b>Tracker type :</b>'
                },{
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : 'mac-type3'  ,
                    hiddenName: 'mac[type3]', 
                    displayField:   'name',
                    valueField:     'value' ,
                    invalidText : 'Tracker type must be selected' , 
                    emptyText : 'Please select tracker type' , 
                    store: 
                        Ext.create('Ext.data.Store', {
                        fields: ['name', {name: 'value', type: 'int'}],
                        data : [
                            {name : 'Metadata',   value: 1 },
                            {name : 'Element',   value: 2 } ,  
                            {name : 'Regular expression',   value: 3 } ,
                            {name : 'Custom function',   value: 4 }
                        ]
                    })
                }, 
                {
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    hideLabel : true , 
                    text : 'Process' ,
                    handler : function(){
                        if( Ext.getCmp('mac-type3').isValid()){
                            me.activeType3 = Ext.getCmp('mac-type3').getValue() ; 
                            me.process3() ; 
                        }
                    }
                }
           ]
       } ) ; 
       
       me.listeners = {
            'afterrender' : function(w){ 
       
                if( me.activeType3 != 0 ){
                    Ext.getCmp('mac-type3').setValue(me.activeType3) ; 
                }  
            } , 
            'beforeclose' : function(w){
            	Ext.util.History.add('');
            }
         }  ; 
    } , 
    
    process3 : function(){
    	
    }
}) ; 