Ext.define('Tracker.view.tracker.Edit2BaseWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.trackeredit2base',

    bodyPanel : null  ,

    isAjaxLoading : false ,

    nameDisabled : true ,

    uid : 0 ,
   	name : null  ,
    url : null ,
   	regex : null ,
    urlOption : 0 ,
    inc_www : 0 ,
    inc_http_https : 0 ,
    type_id : 0 ,
    typeData : 0 ,
    asset : 0 ,

    saveDataURL : '' ,
    postData : {} ,



    initComponent: function(){
        var me = this;

        me.closable = true ;
        me.modal = true ;
        me.maximizable = true ;
        me.resizable = true ;
        me.plain = true ;


        me.buttons = [
        {
            text: 'SAVE' ,
            cls : 'monoloop-submit-form' ,
            handler : function(){
                var form = me.bodyPanel ;
                if( form.form.isValid() ){
                    me.postData['activeUID'] = me.uid ;
                    me.postData['activeURL'] = me.url ;
                    if( me.nameDisabled  ){
                        me.postData['activeName'] = me.name ;
                    }
                    form.form.submit({
                        url: me.saveDataURL ,
                        waitMsg: 'Saving data...',
                        params : me.postData ,
                        success: function(xx, o){
                            me.saveCompleteProcess() ;
                        } ,
                        failure: function(xx, o) {
                             Ext.MessageBox.hide();
                             Ext.MessageBox.alert('Error', '#502' ) ;
                		}
                    });
                }

        	}
	    } ,
        {
            text: 'CANCEL' ,
            cls : 'monoloop-submit-form'
        }
        ] ;


        Ext.apply(me ,{
            layout : 'fit' ,
            items :  []
        }) ;

        this.callParent(arguments);

        if( me.isAjaxLoading ){

        }else{
           me.initBodyPanel() ;
           me.add(me.bodyPanel) ;
        }
    },

    initBodyPanel : function(){
        var me = this ;

        me.bodyPanel = Ext.create('Ext.form.Panel' , {
            width: '100%',
            border : false ,
            bodyBorder : false ,
            region : 'center' ,
            autoScroll : true ,
            bodyStyle: 'padding : 10px ;' ,
            //bodyCls : 'cse-panel-body' ,
            //labelWidth: 100,
            defaults: {
                anchor: '100%',
                allowBlank: false ,
                msgTarget: 'side'
            },
            items: [{
                xtype : 'displayfield' ,
                hideLabel : true ,
                value : 'URL:' ,
                style : { marginBottom: '0px' }
            },{
                hideLabel : true ,
                disabled : false ,
                name: 'data[url]',
                id : me.id + '-url' ,
                xtype: 'textfield' ,
                allowBlank: false  ,
                value : me.url

            },{
                xtype : 'displayfield' ,
                hideLabel : true ,
                value : 'Include www:',
                style : { marginBottom: '0px' }
            },{
                hideLabel : true ,
                name: 'data[inc_www]',
                id : me.id + '-inc-www' ,
                xtype: 'checkbox' ,
                checked: parseInt(me.inc_www)
            },{
                xtype : 'displayfield' ,
                hideLabel : true ,
                value : 'Include http/https:',
                style : { marginBottom: '0px' }
            },{
                hideLabel : true ,
                name: 'data[inc_http_https]',
                id : me.id + '-inc-http-https' ,
                xtype: 'checkbox' ,
                checked: parseInt(me.inc_http_https)
            },{
                xtype : 'displayfield' ,
                hideLabel : true ,
                value : 'URL Options:',
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,
                hideLabel :     true ,
                displayField:   'name',
                valueField:     'value',
                value       :   me.urlOption ,
                id : me.id + '-urloption' ,
                name :  'data[urlOption]' ,
                store:     Ext.create('Ext.data.Store', {
                    fields: ['name', {name: 'value', type: 'int'}],
                    data : [
                        {name : 'Exact Match',   value: 0 },
                        {name : 'Starts with',   value: 2 }
                    ]
                })
            },{
                xtype : 'displayfield' ,
                hideLabel : true ,
                value : 'Regular Expression:',
                style : { marginBottom: '0px' }
            },{
                hideLabel : true ,
                name: 'data[regex]',
                id : me.id + '-regex' ,
                xtype: 'textfield' ,
                allowBlank: true ,
                value : me.regex
            },{
                xtype : 'displayfield' ,
                hideLabel : true ,
                value : '<hr>'
            }
            ]
        }) ;
        me.addName() ;
        me.addBodyItems() ;

        me.bodyPanel.add([{
        	xtype : 'displayfield' ,
        	value : 'Asset (optional)'
        },{
            xtype:          'combo',
            mode:           'local',
            hideLabel : true ,
            allowBlank: true  ,
            triggerAction:  'all',
            editable:       false,
            id : 'activeAsset'  ,
            name : 'activeAsset' ,
            displayField:   'name',
            valueField:     'uid' ,
            emptyText : 'Please select asset' ,
            store:
                Ext.create('Ext.data.Store', {
                fields: ['name', {name: 'uid', type: 'int'}],
                proxy: {
	                type: 'ajax',
	                url : 'index.php?eID=monoloop_assets&pid=480&cmd=assets/list',
	                reader: {
	                    type: 'json',
	                    root: 'topics',
	                    totalProperty: 'totalCount'

	                } ,
	                actionMethods: {
	                    read: 'POST'
	                } ,
	                pageParam: undefined ,
	                limitParam: undefined
	            }

            })  ,
            listeners : {
            	'afterrender' : function(cb){
            		if( me.asset != 0){
            			cb.store.load(function(){
            				cb.setValue(me.asset) ;
            			}) ;
            		}
            	}
            }
        }]) ;
    }  ,

    // interface ;

    addName : function(){
        var me = this ;
        me.bodyPanel.add( {
            xtype : 'displayfield' ,
            hideLabel : true ,
            value : 'Tracker Name:',
            style : { marginBottom: '0px' }
        },{
            hideLabel : true ,
            name: 'activeName',
            id : me.id + '-name' ,
            xtype: 'textfield' ,
            allowBlank: false ,
            disabled : me.nameDisabled   ,
            value : me.name
        } ) ;
    } ,

    addBodyItems : function(){

    } ,

    saveCompleteProcess : function(){

    }
}) ;