Ext.define('Tracker.view.asset.NewFieldWindow', {
    extend : 'Ext.window.Window' ,
    alias: 'widget.assetnewfieldwindow',
    title : 'New Field' , 
    modal : true , 
    resizable : false , 
    width : 300 ,
    height : 250 , 
    layout : 'fit' , 
    
    bodyPanel : null , 
    parent_id : '' , 
    
    editfield_id : null , 
    fieldName : null , 
    fieldType : null , 
    ttl : null , 
    
    initComponent: function(){ 
        var me = this ; 
        
        me.buttons = [{
            text : 'SAVE' , 
            cls : 'monoloop-submit-form' 
        },{
            text : 'CANCEL' , 
            cls : 'monoloop-submit-form'  
        }] ; 
        
        me.initBodyPanel() ; 
        
        me.items = [me.bodyPanel] ; 
        
        this.callParent(arguments); 
    } , 
    
    initBodyPanel : function(){
        var me = this ;
        
        var fieldStore = Ext.create('Ext.data.Store', {
            fields: ['name'],
            data : [ 
                {name : 'Text' } , 
                {name : 'Number' } , 
                {name : 'Boolean' } , 
                {name : 'Image' }
            ]
        }) ;
        
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
            width: '100%',  
            id: me.id + '-form',
            border : false , 
            bodyBorder : false ,  
            bodyStyle: 'padding : 15px ;' ,
            defaults: {
                anchor: '90%',
                allowBlank: false ,
                msgTarget: 'side'
            },  
            items: [{
                xtype : 'displayfield' , 
                value : '<b>Field name</b>'
            },{
                xtype : 'textfield' , 
                id : me.id + '-name' , 
                value : me.fieldName 
            },{
                xtype : 'displayfield' , 
                value : '<b>Field type</b>'
            },{
                xtype: 'combo',
                mode: 'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction: 'all',
                editable: false,
                id : me.id+ '-type'  ,  
                displayField: 'name',
                valueField: 'name' ,
                emptyText : 'Please select type' , 
                store: fieldStore , 
                value : me.fieldType  
            } ]
        }) ; 
    }
}) ;
    