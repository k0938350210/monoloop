Ext.define('Tracker.view.customvar.Stage1Window', { 
    extend: 'Ext.window.Window',
    alias: 'widget.customvarsstage1',
    
    bodyPanel : null  , 
    
    listFieldStore : null , 
    activeName : '' , 
    activeType : 0 , 
    activeListFieldID : 0 , 
    activeDatatype : 'string' , 
    
    initComponent: function(){ 
        var me = this ; 
        
        me.listFieldStore = Ext.create('Ext.data.Store', {
        fields: [{name: 'label'},
	 			{name: 'value' , type: 'int'}] 
        }) ; 
        
        me.title = 'CUSTOM VARS TRACKER',
        me.closable = true,
        me.width = 370,
        me.height = 315,
        //id : 'ces-window' , 
        //me.baseCls = 'ces-window' ,
        //me.iconCls = 'ces-window-header-icon' ,  
        //border:false,
        me.modal = true,
        me.maximizable = false  , 
        me.resizable = false , 
        me.plain = true , 
        me.layout = 'fit' , 
		//items : [myForm] , 
        me.buttons = [
        {
            text: 'CANCEL' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){
                   me.close() ;  
        	}       
	    } , 
        {
            text: 'NEXT' , 
            cls : 'monoloop-submit-form' , 
            handler: function(){
                 if(me.bodyPanel.getForm().isValid()){  
                    var fieldID =  Ext.getCmp('mac-name').getValue() ; 
                     if((parseFloat(fieldID) == parseInt(fieldID)) && !isNaN(fieldID)){
                        me.activeListFieldID = fieldID
                        me.activeType = Ext.getCmp('mac-type').getValue() ;   
                        me.activeDatatype = Ext.getCmp('mac-fieldtype').getValue() ; 
                        me.processNext() ; 
                    } else {
                        me.setLoading('Create new field ...') ; 
                        var data = {}  ; 
                        data['newField'] = fieldID ; 
                        data['fieldtype'] = Ext.getCmp('mac-fieldtype').getValue() ;   
                        Ext.Ajax.request({
                            url: 'index.php?eID=monoloop_trackers&pid=480&cmd=customvars/createNewField' , 
                			// process the response object to add it to the TabPanel:
                            params : data , 
                			success: function(xhr) {
                			    var data = Ext.JSON.decode(xhr.responseText) ; 
                                me.setLoading(false)  ;  
                                
                                me.activeListFieldID = data.idunique_type
                                me.activeType = Ext.getCmp('mac-type').getValue() ;  
								me.activeDatatype = Ext.getCmp('mac-fieldtype').getValue() ;  
                                me.processNext() ; 
                                
                			},
                			failure: function() { 
                                Ext.MessageBox.alert('Error', '#502' ) ; 
                                me.setLoading(false)  ;
                			}
                		});
                    } 
                     
                } 
            }

        }
         ] ;
         
        me.listeners = {
            'afterrender' : function(w){ 
                
                w.setLoading('Loading field list') ; 
                
                Ext.Ajax.request({
		            url: 'index.php?eID=monoloop_trackers&pid=480&cmd=customvars/getField' , 
					// process the response object to add it to the TabPanel:
					success: function(xhr) {
					    var data = Ext.JSON.decode(xhr.responseText) ; 
		                me.listFieldStore.loadData(data.data) ; 
		                
		                if( me.activeType != 0 ){
		                    Ext.getCmp('mac-type').setValue(parseInt(me.activeType)) ; 
		                }
		                if( me.activeListFieldID != 0 ){
		                    Ext.getCmp('mac-name').setValue(parseInt(me.activeListFieldID)) ; 
		                }
		                 
		                Ext.getCmp('mac-fieldtype').setValue(me.activeDatatype) ; 
		                
		                me.setLoading(false)  ;  
					},
					failure: function() { 
		                Ext.MessageBox.alert('Error', '#502' ) ; 
		                me.setLoading(false)  ;
					}
				});
            }
        } ; 
        
        me.initBodyPanel() ; 
        
        Ext.apply(me ,{   
            items :  [ me.bodyPanel ]
        }) ; 
        
        this.callParent(arguments); 
        
        
    } , 
    
    initBodyPanel : function(){
        var me = this ; 
        me.bodyPanel = Ext.create('Ext.form.Panel' , { 
            border : false , 
            bodyBorder : false , 
            bodyStyle: 'padding:5px 5px 0', 
            //bodyCls : 'cse-panel-body' , 
            labelWidth: 100,
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
            },
      
            items: [{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : '<b>Field :</b>'
            } ,{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  , 
                triggerAction:  'all',
                queryMode: 'local',
                editable:       true,
                hideTrigger : true ,
                id : 'mac-name'  ,
                hiddenName: 'mac[name]', 
                displayField:   'label', 
                valueField:     'value',  
                emptyText : 'Please select field' , 
                value : me.activeName , 
                store:   me.listFieldStore
            },{
            	xtype : 'displayfield' , 
            	hideLabel : true , 
            	value : '<b>Field type :</b>'
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all', 
                editable:       false,
                id : 'mac-fieldtype'  ,
                hiddenName: 'mac[fieldtype]', 
                displayField:   'name',
                valueField:     'value',  
                emptyText : 'Please select tracker type' , 
                store: Ext.create('Ext.data.Store', {
                    fields: ['name', {name: 'value'}],
                    data : [
                        {name : 'Integer',   value: 'integer' },
                        {name : 'String',   value: 'string' } ,  
                        {name : 'Boolean',   value: 'bool' } , 
                        {name : 'Date' , value: 'date'}
                    ]
                })
            }, 
            {
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : '<b>Tracker type :</b>'
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                
                editable:       false,
                id : 'mac-type'  ,
                hiddenName: 'mac[type]', 
                displayField:   'name',
                valueField:     'value',  
                emptyText : 'Please select tracker type' , 
                store: Ext.create('Ext.data.Store', {
                    fields: ['name', {name: 'value', type: 'int'}],
                    data : [
                        {name : 'Metadata',   value: 1 },
                        {name : 'Element',   value: 2 } ,  
                        {name : 'Regular expression',   value: 3 } ,
                        {name : 'Custom function',   value: 4 } , 
                        {name : 'No tracker',   value: 5 }
                    ]
                })
            }
			]

	    });
    } , 
    // Interface ; 
    processNext : function(){
        
    }
}) ; 