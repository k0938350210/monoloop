Ext.define('Tracker.view.tracker.Stage4Window', {
    extend: 'Tracker.view.tracker.Stage3BaseWindow',
    alias: 'widget.trackerflowstage4',

    postData : {} ,
    postURL : '' ,

    bodyPanel : null ,

    activeRegex : null ,
    activeUrlOption : 0 ,
    activeInc_www : null ,
    activeInc_http_https : null ,
    activeURL : '' ,

    initComponent: function(){
        var me = this ;
        me.title = 'Edit record' ;
        me.closable = true ;
        me.width = 550 ;
        me.layout = 'fit' ;
        me.height = 250 ;
        //id : 'mc-edit-url-window' ,
        me.modal = true ;
        me.plain = true ;
		//items : [form] ,
        me.buttons = [{
            text: 'Save' ,
            cls : 'monoloop-submit-form' ,
            handler: function(){
                var form = Ext.getCmp(me.id + '-config') ;
                if( form.form.isValid() ){

                    form.form.submit({
		                url: me.postURL ,
		                waitMsg: 'Saving data...',
                        params : me.postData ,
		                success: function(xx, o){
		                    me.clearMessage() ;
		                    me.saveCompleteProcess() ;
                            me.resetActiveData();
		                } ,
		                failure: function(xx, o) {
		                     Ext.MessageBox.hide();
		                     Ext.MessageBox.alert('Error', o.result.msg ) ;
		    			}
                    });
                }
            }

        },{
            text: 'Close' ,
            cls : 'monoloop-submit-form' ,
            handler : function(){
				 me.close();
			}
        }] ;

        me.listeners = {
            'afterrender' : function(w){
                if( me.activeRegex != ''){
                    Ext.getCmp('mc-url-regex').setValue(me.activeRegex) ;
                }
                if( me.activeUrlOption != 0){
                    Ext.getCmp('mc-url-urloption').setValue(parseInt(me.activeUrlOption)) ;
                }
            	//console.debug(me.activeInc_www) ;
                if( me.activeInc_www != 0 && me.activeInc_www != false  && me.activeInc_www != 'false'){
                    Ext.getCmp('mc-url-inc-www').setValue(true) ;
                }
                //console.debug(me.activeInc_http_https) ;
                if( me.activeInc_http_https != 0 && me.activeInc_http_https != false  && me.activeInc_http_https != 'false'){
                    Ext.getCmp('mc-url-inc-http-https').setValue(true) ;
                }

                monoloop_base.showMessage('Step 4:' , 'Modify the tracker to only track the relevant part of the site') ;
            } ,
            'close' : function(){
                Ext.get('main-msg-div').dom.innerHTML = '' ;
            }
        } ;

        me.initBodyPanel() ;

        Ext.apply(me ,{
        	border : false ,
            items :  [ me.bodyPanel ]
        }) ;

        this.callParent(arguments);
    } ,

    initBodyPanel : function(){
        var me = this ;
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
            id : me.id + '-config' ,
		    fileUpload: false,
	        width: '100%',
	        height : 190 ,
	        border : false ,
	        bodyStyle: 'padding : 10px ;' ,

	        defaults: {
	            anchor: '95%',
	            allowBlank: false,
	            labelWidth: 140,
	            msgTarget: 'side'
	        },
            items : [
                {
					anchor: '95%',
	                fieldLabel: 'URL',
                    disabled : false , //--
	                name: 'data[url]',
	                id : 'mc-url-url' ,
	                xtype: 'textfield' ,
                    allowBlank: false  ,
                    value : me.activeURL

	            } ,
                {
					anchor: '35%',
	                fieldLabel: 'Include www',
	                name: 'data[inc_www]',
	                id : 'mc-url-inc-www' ,
	                xtype: 'checkbox' ,
	                checked: false
	            }  ,
                {
					anchor: '35%',
	                fieldLabel: 'Include http/https',
	                name: 'data[inc_http_https]',
	                id : 'mc-url-inc-http-https' ,
	                xtype: 'checkbox' ,
	                checked: false
	            } ,
                {
                    xtype:          'combo',
                    mode:           'local',
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,
                    fieldLabel:     'URL Options',
                    displayField:   'name',
                    valueField:     'value',
                    value       :   0  ,
                    id : 'mc-url-urloption' ,
                    name :  'data[urlOption]' ,
                    hiddenName :   'data[urlOption]' ,
                    store:     Ext.create('Ext.data.Store', {
                        fields: ['name', {name: 'value', type: 'int'}],
                        data : [
                            {name : 'Exact Match',   value: 0 },
                            {name : 'Starts with',   value: 2 }
                        ]
                    })  ,
                    anchor:'95%'
	            } ,
                {
					anchor: '95%',
	                fieldLabel: 'Regular Expression',
	                name: 'data[regex]',
	                id : 'mc-url-regex' ,
	                xtype: 'textfield' ,
                    allowBlank: true

	            }
            ]
        }) ;
    } ,

    // Interface ;

    resetActiveData : function(){

    } ,

    saveCompleteProcess : function(){

    }
}) ;