Ext.define('Tracker.view.tracker.Stage3MetaWindow', { 
    extend: 'Tracker.view.tracker.Stage3BaseWindow',
    alias: 'widget.trackerflowstage3meta',
    
    bodyPanel : null ,
    
    activeURL : null , 
    activeTypeData : '' ,  
    activeFilterType : null , 
    activePostUserFunc : '' ,
    activeSaveFunction : 0 , 
    activeExample : '' , 
    
    filterStore : null , 
    
    initComponent: function(){ 
        var me = this ; 
        me.preload() ;
        
        me.title = 'TRACKER : Meta ' ;
        me.closable = true ; 
        me.width = 370 ; 
        me.height = 420 ;
        me.modal = true ;
        me.maximizable = false ; 
        me.resizable = false ; 
        me.plain = true ; 
        
        me.buttons = [
        {
            text: 'CANCEL' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){ 
               me.close() ; 
        	}       
	    } , 
        {
            text: 'NEXT' , 
            cls : 'monoloop-submit-form' , 
            handler: function(){
               if(Ext.getCmp( me.id + 'meta-form').getForm().isValid()){  
                   me.activeTypeData = Ext.getCmp('mc-meta-typedata').getSubmitValue() ; 
                   me.activeFilterType = Ext.getCmp(me.id + '-ft').getValue() ; 
                   me.activePostUserFunc = Ext.getCmp(me.id + '-post-user-func').getValue() ; 
                   me.activeSaveFunction = Ext.getCmp(me.id + '-savefunction').getValue() ; 
                   //me.close() ; 
                   me.processNext() ; 
               }
            } 
        }
        ] ; 
        
        var data = {} ;  
        data['url'] = me.activeURL ;   
        
        Ext.Ajax.request({
            url: 'index.php?eID=monoloopaccounting&pid=480&cmd=category/getMeta' ,
            params:  data,
			// process the response object to add it to the TabPanel:
			success: function(xhr) {
			    me.showEditStep3_metaWindow(Ext.JSON.decode(xhr.responseText)) ;  
			},
			failure: function() { 
                Ext.MessageBox.alert('Error', '#502' ) ; 
			}
		});	
        
        Ext.apply(this, {
            layout: 'fit' ,  
            items: [] , 
            listeners : {
                'afterrender' : function(w){
                    w.setLoading('Loading meta data') ; 
                    monoloop_base.showMessage('Step 3:' , 'Check that the tracker captures the information you want and apply filters if appropriate ') ;
                }
            }
        });
        
        this.callParent(arguments); 
    } , 
    
    showEditStep3_metaWindow : function(jsonData){
        var me = this ; 
        
        me.filterStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [ 
                {name : '--- None --- ',   value: 0 } , 
                {name : 'Custom filter',   value: 1 } 
            ]
        }) ; 
 
         
         
        var singleLocalStore = Ext.create('Ext.data.Store', {
                fields : ['name', 'content' ],
                data   : jsonData.data 
            }) ; 
             
        
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
        width: '100%', 
        id: me.id + 'meta-form', 
        border : false , 
        bodyBorder : false , 
        bodyStyle: 'padding : 10px ;' , 
        //bodyCssClass : 'cse-panel-body' , 
        labelWidth: 100,
        autoScroll : true ,
        defaults: {
            anchor: '100%',
            allowBlank: false,
            msgTarget: 'side'
        },
  
        items: [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    hidden : true ,
                    value : '<b>Example URL :</b>', 
                    style : { marginBottom: '0px' }
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    hidden : true ,
                    name : 'mc_meta[url]'  ,
                    id : 'mc-meta-url' ,  
                    allowBlank: false  , 
                    disabled  : false  , //--
                    value : me.activeURL   
                }, 
                {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<b>Metadata :</b>', 
                    style : { marginBottom: '0px' }
                } ,{   
                    xtype: 'multiselect',  
                    hideLabel : true ,  
                    height: 150,
                    id : 'mc-meta-typedata' ,
                    displayField:   'content',
                    valueField:     'name',  
                    delimiter : '||' , 
                    allowBlank:false,
                    store: singleLocalStore 
                }, 
                {
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<hr><b>Filter Type :</b>', 
                    style : { marginBottom: '0px' }
                },{
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: true  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : me.id+ '-ft'  ,  
                    displayField:   'name',
                    valueField:     'value' ,
                    emptyText : 'Please select filter type' , 
                    store:   me.filterStore , 
                    listeners : {
                        'afterrender' : function( combo , eObj){
                            if( me.activeFilterType == '' || me.activeFilterType == 0 || me.activeFilterType == null || isNaN(me.activeFilterType)){
                                combo.setValue(0) ; 
                            }else{
                                combo.setValue(me.activeFilterType) ; 
                            }  
                        },
                        'change' : function( combo , newVal , oldVal , eObj ){
                            if( newVal > 0){
                                Ext.getCmp(me.id + '-post-user-func').setVisible(true) ; 
                            }else{
                                Ext.getCmp(me.id + '-post-user-func').setVisible(false) ; 
                            } 
                            Ext.getCmp( me.id + '-ft-btn').fireEvent('click') ; 
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-post-user-func' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){  
                            
                            if( me.activePostUserFunc == undefined || me.activePostUserFunc == ''){
                                textArea.setValue('return meta ; ') ; 
                            }else{
                                textArea.setValue(me.activePostUserFunc) ; 
                            } 
                        }
                    }
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    hideLabel : true ,
                    id : me.id + '-ft-btn' , 
                    text : 'test'  , 
                    listeners : {
                        'click' : function(button , e , eOpt){
                          
                            if( Ext.getCmp(me.id + '-ft').getValue() == 0){
                                Ext.getCmp( me.id + '-post-user-ret' ).setValue(  Ext.getCmp('mc-meta-typedata').getSubmitValue() ) ;
                            }else{
                                var metaList =  Ext.getCmp('mc-meta-typedata').getSubmitValue()  ; metaList = metaList.split( '||') ; 
                                var jsStr = 
                                      ' var ret = \'\' ; '
                                          + ' function testX001(result){' 
                                          + '  ' + Ext.getCmp(me.id + '-post-user-func' ).getValue()  
                                          + ' }'
                                          + ' for( var i = 0 ; i < metaList.length ; i++) {' 
                                          + '   var newret = testX001(metaList[i]) ; '
                                          + '   if( ret == \'\') '
                                          + '     ret = newret ;'
                                          + '   else  '
                                          + '   ret = ret + \'||\' + newret  ; '
                                          + ' } ' ;  
                                eval(jsStr) ; 
                                Ext.getCmp( me.id + '-post-user-ret' ).setValue(ret) ;
                            }
                           
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-post-user-ret'  , 
                    allowBlank : true  
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : '<hr><b>Save Function :</b>', 
                    hidden : me.hideSaveFunction , 
                    style : { marginBottom: '0px' }
                },{
                    xtype: 'combo',
                    mode: 'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,  
                    displayField:   'name',
                    valueField:     'value' , 
                    emptyText : 'Please select' , 
                    id : me.id + '-savefunction' , 
                    store:  me.saveFunctionStore ,
                    hidden : me.hideSaveFunction , 
                    allowBlank :  me.hideSaveFunction , 
                    listeners : {
                    	afterrender : function(cb){
                    		if(me.activeSaveFunction == 0 || me.activeSaveFunction == null)
                    			return ; 
                   			cb.setValue(me.activeSaveFunction) ; 
                    	}
                    }
                }
			]

	    });
        
        me.add(me.bodyPanel)  ;
        me.setLoading(false)  ;
        
        // Set default value ; 
        if( me.activeTypeData != ''){
            var temp = me.activeTypeData.split('||') ; 
            Ext.getCmp('mc-meta-typedata').setValue(temp) ; 
        }
    }  , 
    
    // Interface ; 
    
    processNext : function(){
        
    } 
}) ; 