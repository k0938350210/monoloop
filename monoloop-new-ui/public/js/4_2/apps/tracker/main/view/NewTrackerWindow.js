Ext.define('Tracker.view.NewTrackerWindow', { 
    extend: 'Ext.window.Window',
    alias: 'widget.newtrackerwindow',
    xtype : 'newtrackerwindow', 
    
    initComponent: function() {
    	var me = this ; 
    	
    	Ext.apply(this, { 
    		closable:true,
            width:270,  
            height:255,
            layout : 'fit' ,
    		modal : true , 
    		title : 'Create New Tracker' , 
    		items : [{
    			xtype : 'form' , 
    			id : me.id + '-form' ,
    			border : false , 
    			bodyPadding: 15 ,  
				layout: 'anchor',
			    defaults: {
			        anchor: '95%'
			    },
			    items : [{
				 
	    			xtype : 'displayfield' , 
	    			value : 'Name:'
	    		},{
	    			xtype : 'textfield' , 
	    			name : 'name' , 
	    			allowBlank : false ,
	   				defaultValue : 'Name of Tracker'
	    		},{
	    			xtype : 'displayfield' , 
	    			value : 'Tracker Type:'
	    		},{
	    			xtype : 'combo' , 
	    			name : 'type' , 
	    			allowBlank : false ,
	    			store : Ext.create('Tracker.store.Type') , 
                    queryMode: 'local',
                    displayField: 'label',
    				valueField: 'name' 
	    		}]  , 
	    		buttons : [{
					'text' : 'NEXT' ,
					cls : 'monoloop-submit-form' 
				},{
					'text' : 'CANCEL' , 
					cls : 'monoloop-submit-form' 
				}]
			}]
   		}) ; 
    	this.callParent(arguments); 
   	}
}) ; 
    