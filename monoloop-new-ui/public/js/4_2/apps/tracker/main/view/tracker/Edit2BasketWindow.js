Ext.define('Tracker.view.tracker.Edit2BasketWindow', { 
    extend: 'Tracker.view.tracker.Edit2BaseWindow',
    alias: 'widget.trackeredit2basket',
    
    dataObj : {} , 
    filterStore : null , 
    
    initComponent: function(){
        var me = this ;  
        this.callParent(arguments); 
    } , 
    
    addBodyItems : function(){
        var me = this ; 
        
        me.filterStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [
                {name : 'Monoloop default filter',   value: 0 },
                {name : 'Custom filter',   value: 1 } 
            ]
        }) ; 
        
        var field = Ext.create('Ext.form.field.Display', {
            hideLabel : true , 
            value : 'Product row x-path:' , 
            style : { marginBottom: '0px' }
        }) ; 
        
        me.bodyPanel.add(field) ; 
        
        field = Ext.create('Ext.form.field.Text', { 
            hideLabel : true , 
            disabled : false ,  
            name: 'addition[step3_xpath]', 
            id : me.id + '-step3_xpath' ,    
            allowBlank: false  ,   
            value : me.dataObj.step3_xpath
            
        }) ; 
        
        me.bodyPanel.add(field) ; 
        
        field  = Ext.create('Ext.form.FieldSet', {
            title: 'Product Name:',
            frame: true,
            bodyStyle: 'padding:5px 5px 0', 
            defaults: {
                anchor: '100%',
                allowBlank: true,
                msgTarget: 'side'
            },
            items : [
            {
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Xpath :', 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textfield' , 
                hideLabel : true ,  
                name : 'addition[step4_xpath_n]'  ,
                id : me.id + '-step4_xpath_n' ,  
                allowBlank: true  , 
                disabled  : false  , //--
                value : me.dataObj.step4_xpath_n  
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Filter type :', 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id+ '-step_xpath_n_ft'  ,
                name: 'addition[step_xpath_n_ft]' , 
                displayField:   'name',
                valueField:     'value' ,
                emptyText : 'Please select filter type' , 
                store:   me.filterStore , 
                listeners : {
                    'afterrender' : function( combo , eObj){
                        if( me.dataObj.step_xpath_n_ft == undefined)
                            me.dataObj.step_xpath_n_ft = 0 ;  
                        else 
                            me.dataObj.step_xpath_n_ft = parseInt(me.dataObj.step_xpath_n_ft) ; 
                        combo.setValue(me.dataObj.step_xpath_n_ft) ;   
                        if( me.dataObj.step_xpath_n_ft == 0 ){
                            Ext.getCmp(me.id + '-step_xpath_n_cjs').setVisible(false) ; 
                        }  
                    } , 
                    'change' : function( combo , newVal , oldVal , eObj ){
                        if( newVal > 0){
                            Ext.getCmp(me.id + '-step_xpath_n_cjs').setVisible(true) ; 
                        }else{
                            Ext.getCmp(me.id + '-step_xpath_n_cjs').setVisible(false) ; 
                        } 
                    }
                }   
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-step_xpath_n_cjs' , 
                name : 'addition[step_xpath_n_cjs]' ,   
                listeners : {
                    'afterrender' : function(textArea , eObj){
                        if( me.dataObj.step_xpath_n_cjs == undefined)
                            me.dataObj.step_xpath_n_cjs = 'return MONOloop.jq(selector).html() ;' ; 
                        textArea.setValue(me.dataObj.step_xpath_n_cjs) ;    
                    }
                }
            }
            ]
        }) ; 
        
        me.bodyPanel.add(field) ;
        
        field  = Ext.create('Ext.form.FieldSet', {
            title: 'SKU:',
            frame: true,
            bodyStyle: 'padding:5px 5px 0', 
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
            },
            items : [
            {
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Xpath :', 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textfield' , 
                hideLabel : true ,  
                name : 'addition[step4_xpath_s]'  ,
                id : me.id + '-step4_xpath_s' ,  
                allowBlank: true  , 
                disabled  : false  , //--
                value : me.dataObj.step4_xpath_s 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Filter type :', 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id+ '-step_xpath_s_ft'  ,
                name: 'addition[step_xpath_s_ft]' , 
                displayField:   'name',
                valueField:     'value' ,
                emptyText : 'Please select filter type' , 
                store:   me.filterStore , 
                listeners : {
                    'afterrender' : function( combo , eObj){
                        if( me.dataObj.step_xpath_s_ft == undefined)
                            me.dataObj.step_xpath_s_ft = 0 ;  
                        else 
                            me.dataObj.step_xpath_s_ft = parseInt(me.dataObj.step_xpath_s_ft) ; 
                        combo.setValue(me.dataObj.step_xpath_s_ft) ;   
                        if( me.dataObj.step_xpath_s_ft == 0 ){
                            Ext.getCmp(me.id + '-step_xpath_s_cjs').setVisible(false) ; 
                        }  
                    } , 
                    'change' : function( combo , newVal , oldVal , eObj ){
                        if( newVal > 0){
                            Ext.getCmp(me.id + '-step_xpath_s_cjs').setVisible(true) ; 
                        }else{
                            Ext.getCmp(me.id + '-step_xpath_s_cjs').setVisible(false) ; 
                        } 
                    }
                }    
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-step_xpath_s_cjs' , 
                name : 'addition[step_xpath_s_cjs]' ,   
                listeners : {
                    'afterrender' : function(textArea , eObj){
                        if( me.dataObj.step_xpath_s_cjs == undefined)
                            me.dataObj.step_xpath_s_cjs = 'return MONOloop.jq(selector).html() ;' ; 
                        textArea.setValue(me.dataObj.step_xpath_s_cjs) ;    
                    }
                }
            }
            ]
        }) ; 
        
        me.bodyPanel.add(field) ;
        
        field  = Ext.create('Ext.form.FieldSet', {
            title: 'Price:',
            frame: true,
            bodyStyle: 'padding:5px 5px 0', 
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
            },
            items : [
            {
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Xpath :', 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textfield' , 
                hideLabel : true ,  
                name : 'addition[step4_xpath_p]'  ,
                id : me.id + '-step4_xpath_p' ,  
                allowBlank: true  , 
                disabled  : false  , //--
                value : me.dataObj.step4_xpath_p 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Filter type :', 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id+ '-step_xpath_p_ft'  ,
                name: 'addition[step_xpath_p_ft]' , 
                displayField:   'name',
                valueField:     'value' ,
                emptyText : 'Please select filter type' , 
                store:   me.filterStore , 
                listeners : {
                    'afterrender' : function( combo , eObj){
                        if( me.dataObj.step_xpath_p_ft == undefined)
                            me.dataObj.step_xpath_p_ft = 0 ;  
                        else 
                            me.dataObj.step_xpath_p_ft = parseInt(me.dataObj.step_xpath_p_ft) ; 
                        combo.setValue(me.dataObj.step_xpath_p_ft) ;   
                        if( me.dataObj.step_xpath_p_ft == 0 ){
                            Ext.getCmp(me.id + '-step_xpath_p_cjs').setVisible(false) ; 
                            Ext.getCmp(me.id + '-step_xpath_p_cjs2').setVisible(false) ; 
                        }  
                    } , 
                    'change' : function( combo , newVal , oldVal , eObj ){
                        if( newVal > 0){
                            Ext.getCmp(me.id + '-step_xpath_p_cjs').setVisible(true) ;
                            Ext.getCmp(me.id + '-step_xpath_p_cjs2').setVisible(true) ;  
                        }else{
                            Ext.getCmp(me.id + '-step_xpath_p_cjs').setVisible(false) ; 
                            Ext.getCmp(me.id + '-step_xpath_p_cjs2').setVisible(false) ; 
                        } 
                    }
                }   
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Price:', 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-step_xpath_p_cjs' , 
                name : 'addition[step_xpath_p_cjs]' ,   
                listeners : {
                    'afterrender' : function(textArea , eObj){
                        if( me.dataObj.step_xpath_p_cjs == undefined)
                            me.dataObj.step_xpath_p_cjs = 'return MONOloop.jq(selector).html() ;' ; 
                        textArea.setValue(me.dataObj.step_xpath_p_cjs) ;    
                    }
                }
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Currency:', 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-step_xpath_p_cjs2' , 
                name : 'addition[step_xpath_p_cjs2]' ,   
                listeners : {
                    'afterrender' : function(textArea , eObj){
                        if( me.dataObj.step_xpath_p_cjs2 == undefined)
                            me.dataObj.step_xpath_p_cjs2 = 'return MONOloop.jq(selector).html() ;' ; 
                        textArea.setValue(me.dataObj.step_xpath_p_cjs2) ;    
                    }
                }
            }
            ]
        }) ; 
        
        me.bodyPanel.add(field) ;
        
        field  = Ext.create('Ext.form.FieldSet', {
            title: 'Quantity:',
            frame: true,
            bodyStyle: 'padding:5px 5px 0', 
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
            },
            items : [
            {
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Xpath :', 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textfield' , 
                hideLabel : true ,  
                name : 'addition[step4_xpath_q]'  ,
                id : me.id + '-step4_xpath_q' ,  
                allowBlank: true  , 
                disabled  : false  , //--
                value : me.dataObj.step4_xpath_q 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Filter type :', 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id+ '-step_xpath_q_ft'  ,
                name: 'addition[step_xpath_q_ft]' , 
                displayField:   'name',
                valueField:     'value' ,
                emptyText : 'Please select filter type' , 
                store:   me.filterStore , 
                listeners : {
                    'afterrender' : function( combo , eObj){
                        if( me.dataObj.step_xpath_q_ft == undefined)
                            me.dataObj.step_xpath_q_ft = 0 ;  
                        else 
                            me.dataObj.step_xpath_q_ft = parseInt(me.dataObj.step_xpath_q_ft) ; 
                        combo.setValue(me.dataObj.step_xpath_q_ft) ;   
                        if( me.dataObj.step_xpath_q_ft == 0 ){
                            Ext.getCmp(me.id + '-step_xpath_q_cjs').setVisible(false) ; 
                        }  
                    } , 
                    'change' : function( combo , newVal , oldVal , eObj ){
                        if( newVal > 0){
                            Ext.getCmp(me.id + '-step_xpath_q_cjs').setVisible(true) ; 
                        }else{
                            Ext.getCmp(me.id + '-step_xpath_q_cjs').setVisible(false) ; 
                        } 
                    }
                }   
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-step_xpath_q_cjs' , 
                name : 'addition[step_xpath_q_cjs]' ,   
                listeners : {
                    'afterrender' : function(textArea , eObj){
                        if( me.dataObj.step_xpath_q_cjs == undefined)
                            me.dataObj.step_xpath_q_cjs = 'return MONOloop.jq(selector).html() ;' ; 
                        textArea.setValue(me.dataObj.step_xpath_q_cjs) ;    
                    }
                }
            }
            ]
        }) ; 
        
        me.bodyPanel.add(field) ;
    }
    
}) ; 
    