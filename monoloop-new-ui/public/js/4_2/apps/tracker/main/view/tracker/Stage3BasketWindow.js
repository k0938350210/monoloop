Ext.define('Tracker.view.tracker.Stage3BasketWindow', { 
    extend: 'Tracker.view.tracker.Stage3BaseWindow',
    alias: 'widget.trackerflowstage3basket',
    
    bodyPanel : null ,
     
    dataObj : '' , 
    
    filterStore : null , 
    
    initComponent: function(){ 
        var me = this ; 
        
        me.title = 'TRACKER : Basket ' ;
        me.closable = true ; 
        me.width = 390 ; 
        me.height = 600 ;
        me.modal = true ;
        me.maximizable = false ; 
        me.resizable = false ; 
        me.plain = true ;    
        
        me.buttons = [
        {
            text: 'CANCEL' , 
            cls : 'monoloop-submit-form' , 
            handler : function(){ 
               me.close() ; 
        	}       
	    } , 
        {
            text: 'NEXT' , 
            cls : 'monoloop-submit-form' , 
            handler: function(){
               if(Ext.getCmp( me.id + '-form').getForm().isValid()){  
                   //Save value to object ; 
                   me.dataObj.step2_xpath = Ext.getCmp(me.id + '-xpath-2').getValue() ; 
                   me.dataObj.step3_xpath = Ext.getCmp(me.id + '-xpath-3').getValue() ; 
                   me.dataObj.step4_xpath_n = Ext.getCmp(me.id + '-xpath-4n').getValue() ; 
                   me.dataObj.step4_xpath_s = Ext.getCmp(me.id + '-xpath-4s').getValue() ; 
                   me.dataObj.step4_xpath_q = Ext.getCmp(me.id + '-xpath-4q').getValue() ; 
                   me.dataObj.step4_xpath_p = Ext.getCmp(me.id + '-xpath-4p').getValue() ;  
                   
                   me.dataObj.step_xpath_n_ft = Ext.getCmp(me.id+ '-xpath-4n-ft'  ).getValue() ; 
                   me.dataObj.step_xpath_s_ft = Ext.getCmp(me.id+ '-xpath-4s-ft'  ).getValue() ; 
                   me.dataObj.step_xpath_q_ft = Ext.getCmp(me.id+ '-xpath-4q-ft'  ).getValue() ; 
                   me.dataObj.step_xpath_p_ft = Ext.getCmp(me.id+ '-xpath-4p-ft'  ).getValue() ; 
                   
                   me.dataObj.step_xpath_n_cjs = Ext.getCmp( me.id + '-xpath-4n-cjs').getValue() ; 
                   me.dataObj.step_xpath_s_cjs = Ext.getCmp( me.id + '-xpath-4s-cjs').getValue() ; 
                   me.dataObj.step_xpath_q_cjs = Ext.getCmp( me.id + '-xpath-4q-cjs').getValue() ; 
                   me.dataObj.step_xpath_p_cjs = Ext.getCmp( me.id + '-xpath-4p-cjs').getValue() ; 
                   me.dataObj.step_xpath_p_cjs2 = Ext.getCmp( me.id + '-xpath-4p-cjs2').getValue() ; 
                   //me.close() ; 
                   me.processNext() ; 
               }
            } 
        }
        ] ;
        
        me.listeners = {} ; 
        
        me.initBodyPanel() ; 
        
        Ext.apply(this, {
            layout: 'fit' ,  
            items: [me.bodyPanel]  
        });
        
        this.callParent(arguments);  
    }, 
    
    ProcessDefaultFilter : function(selector , setBackID){
    	var me = this ;  
    	/*
        if( document.getElementById("pw-browse").contentWindow.monoloop_select_view != undefined){
            return document.getElementById("pw-browse").contentWindow.monoloop_select_view.processDefaultFilter(selector) ; 
         }
         return 'Cannot connect to page' ; 
         */ 
         var iframe = document.getElementById("pw-browse") ;
	     iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-processDefaultFilter"   , 
		        activeTypeData : selector , 
                setBack : true , 
                setBackID : setBackID
		}), me.activeURL); 
    } , 
    
    ProcessDefaultFilter_Price : function(selector , setBackID){
    	var me = this ; 
    	/*
        if( document.getElementById("pw-browse").contentWindow.monoloop_select_view != undefined){
            return document.getElementById("pw-browse").contentWindow.monoloop_select_view.processDefaultFilter_Price(selector) ; 
         }
         return 'Cannot connect to page' ; 
         */
         var iframe = document.getElementById("pw-browse") ;
	    iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-processDefaultFilter_Price"   , 
		        activeTypeData : selector , 
                setBack : true , 
                setBackID : setBackID
		}), me.activeURL); 
    } , 
    
    ProcessDefaultFilter_Currency : function(selector , setBackID){
    	var me = this ; 
    	/*
        if( document.getElementById("pw-browse").contentWindow.monoloop_select_view != undefined){
            return document.getElementById("pw-browse").contentWindow.monoloop_select_view.processDefaultFilter_Currency(selector) ; 
         }
         return 'Cannot connect to page' ; 
         */
         var iframe = document.getElementById("pw-browse") ;
	    iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-processDefaultFilter_Currency"   , 
		        activeTypeData : selector , 
                setBack : true , 
                setBackID : setBackID
		}), me.activeURL); 
    } , 
    
    ProcessCustomFilter : function(jsStr , setBackID){
    	var me = this ; 
    	/*
         if( document.getElementById("pw-browse").contentWindow.monoloop_select_view != undefined){
            return document.getElementById("pw-browse").contentWindow.monoloop_select_view.processCustomFilter(jsStr) ; 
         }
         return 'Cannot connect to page' ; 
         */
         var iframe = document.getElementById("pw-browse") ;
	    iframe.contentWindow.postMessage(JSON.stringify({
		        t: "tracker-processCustomFilter"   , 
		        activeTypeData : jsStr , 
                setBack : true , 
                setBackID : setBackID
		}), me.activeURL); 
    } , 
    
    initBodyPanel : function(){
        var me = this ; 
        
        //console.debug(me.dataObj) ; 
        
        me.filterStore = Ext.create('Ext.data.Store', {
            fields: ['name', {name: 'value', type: 'int'}],
            data : [
                {name : 'Monoloop default filter',   value: 0 },
                {name : 'Custom filter',   value: 1 } 
            ]
        }) ; 
        
        me.bodyPanel = Ext.create('Ext.form.Panel' , {
            width: '100%',
            height: 400, 
            id: me.id + '-form', 
            border : false , 
            bodyBorder : false , 
            autoScroll : true ,
            //bodyCssClass : 'cse-panel-body' , 
            labelWidth: 100,
            bodyStyle: 'padding : 10px ;' , 
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
            },
            items : [{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : '<b>Table xpath :</b>'
            },{
                xtype : 'textfield' , 
                hideLabel : true ,  
                name : 'data[xpath2]'  ,
                id : me.id + '-xpath-2' ,  
                allowBlank: false  , 
                disabled  : false  , //--
                value : me.dataObj.step2_xpath  
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : '<b>Product row xpath :</b>'
            },{
                xtype : 'textfield' , 
                hideLabel : true ,  
                name : 'data[xpath3]'  ,
                id : me.id + '-xpath-3' ,  
                allowBlank: false  , 
                disabled  : false  , //--
                value : me.dataObj.step3_xpath  
            },{
                xtype : 'fieldset' , 
                title: 'Product Name Properties',
                frame: true,
                bodyStyle: 'padding:5px 5px 0',
                collapsible : true ,  
                collapsed : true , 
                defaults: {
                    anchor: '100%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items : [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Xpath :'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    name : 'data[xpath4n]'  ,
                    id : me.id + '-xpath-4n' ,  
                    allowBlank: true  , 
                    disabled  : false  , //--
                    value : me.dataObj.step4_xpath_n  
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Filter type :'
                },{ 
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : me.id+ '-xpath-4n-ft'  ,
                    name: 'data[xpath4n_ft]' , 
                    displayField:   'name',
                    valueField:     'value' ,
                    emptyText : 'Please select filter type' , 
                    store:   me.filterStore , 
                    listeners : {
                        'afterrender' : function( combo , eObj){
                            if( me.dataObj.step_xpath_n_ft == undefined)
                                me.dataObj.step_xpath_n_ft = 0 ;  
                            else 
                                me.dataObj.step_xpath_n_ft = parseInt(me.dataObj.step_xpath_n_ft) ; 
                            combo.setValue(me.dataObj.step_xpath_n_ft) ;   
                            if( me.dataObj.step_xpath_n_ft == 0 ){
                                Ext.getCmp(me.id + '-xpath-4n-cjs').setVisible(false) ; 
                            }  
                        } , 
                        'change' : function( combo , newVal , oldVal , eObj ){
                            if( newVal > 0){
                                Ext.getCmp(me.id + '-xpath-4n-cjs').setVisible(true) ; 
                            }else{
                                Ext.getCmp(me.id + '-xpath-4n-cjs').setVisible(false) ; 
                            }
                            // Also process result ; 
                            Ext.getCmp( me.id + '-xpath-4n-test-btn').fireEvent('click')  ; 
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4n-cjs' , 
                    name : 'data[xpath4n_cjs]' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){
                            if( me.dataObj.step_xpath_n_cjs == undefined)
                                me.dataObj.step_xpath_n_cjs = 'return MONOloop.$(selector).html() ;' ; 
                            textArea.setValue(me.dataObj.step_xpath_n_cjs) ;    
                        }
                    }
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4n-test-btn'  ,
                    text : 'Test'  , 
                    listeners : {
                        'click' : function(btn , e , eOpt){
                             var ret = '' ; 
                            if( Ext.getCmp( me.id+ '-xpath-4n-ft').getValue() == 1){
                                var firstString = ' var selector = \''+ Ext.getCmp(me.id + '-xpath-2').getValue() + ' ' +   Ext.getCmp(me.id + '-xpath-3').getValue() + ' ' + Ext.getCmp(me.id + '-xpath-4n' ).getValue() +'\' ; ' ; 
                           		me.ProcessCustomFilter(firstString + Ext.getCmp(me.id + '-xpath-4n-cjs').getValue(), me.id + '-xpath-4n-ret') ;
                            }else{
                                var selector =  Ext.getCmp(me.id + '-xpath-2').getValue() + ' ' + Ext.getCmp(me.id + '-xpath-3').getValue() + ' ' + Ext.getCmp(me.id + '-xpath-4n' ).getValue() ; 
                                me.ProcessDefaultFilter(selector , me.id + '-xpath-4n-ret') ;
                            } 
                        }
                    }
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Result :'
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4n-ret' , 
                    name : 'data[xpath4n_ret]' , 
                    allowBlank : true 
                }] 
            },{
                xtype : 'fieldset' , 
                title: 'SKU Properties',
                frame: true,
                bodyStyle: 'padding:5px 5px 0',
                collapsible : true ,  
                collapsed : true , 
                defaults: {
                    anchor: '95%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items : [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Xpath :'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    name : 'data[xpath4s]'  ,
                    id : me.id + '-xpath-4s' ,  
                    allowBlank: true  , 
                    disabled  : false  , //--
                    value : me.dataObj.step4_xpath_s 
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Filter type :'
                },{ 
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : me.id+ '-xpath-4s-ft'  ,
                    name: 'data[xpath4s_ft]' , 
                    displayField:   'name',
                    valueField:     'value' ,
                    emptyText : 'Please select filter type' , 
                    store:   me.filterStore , 
                    listeners : {
                        'afterrender' : function( combo , eObj){
                            if( me.dataObj.step_xpath_s_ft == undefined)
                                me.dataObj.step_xpath_s_ft = 0 ;  
                            else 
                                me.dataObj.step_xpath_s_ft = parseInt(me.dataObj.step_xpath_s_ft) ; 
                            combo.setValue(me.dataObj.step_xpath_s_ft) ;   
                            if( me.dataObj.step_xpath_s_ft == 0 ){
                                Ext.getCmp(me.id + '-xpath-4s-cjs').setVisible(false) ; 
                            }  
                        } , 
                        'change' : function( combo , newVal , oldVal , eObj ){
                            if( newVal > 0){
                                Ext.getCmp(me.id + '-xpath-4s-cjs').setVisible(true) ; 
                            }else{
                                Ext.getCmp(me.id + '-xpath-4s-cjs').setVisible(false) ; 
                            }
                            // Also process result ; 
                            Ext.getCmp( me.id + '-xpath-4s-test-btn').fireEvent('click')  ; 
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4s-cjs' , 
                    name : 'data[xpath4s_cjs]' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){
                            if( me.dataObj.step_xpath_s_cjs == undefined)
                                me.dataObj.step_xpath_s_cjs = 'return MONOloop.$(selector).html() ;' ; 
                            textArea.setValue(me.dataObj.step_xpath_s_cjs) ;    
                        }
                    }
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4s-test-btn'  ,
                    text : 'Test'  , 
                    listeners : {
                        'click' : function(btn , e , eOpt){
                             var ret = '' ; 
                             var selector = Ext.getCmp(me.id + '-xpath-2').getValue() + ' ' +  Ext.getCmp(me.id + '-xpath-3').getValue() + ' ' + Ext.getCmp(me.id + '-xpath-4s' ).getValue() ;
                            if( Ext.getCmp( me.id+ '-xpath-4s-ft').getValue() == 1){
                                var firstString = ' var selector = \''+  selector +'\' ; ' ; 
                            	me.ProcessCustomFilter(firstString + Ext.getCmp(me.id + '-xpath-4s-cjs').getValue(),me.id + '-xpath-4s-ret') ;
                            }else{
                                
                            	me.ProcessDefaultFilter(selector,me.id + '-xpath-4s-ret') ;
                            } 
                        }
                    }
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Result :'
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4s-ret' , 
                    name : 'data[xpath4s_ret]' , 
                    allowBlank : true 
                }] 
            },{
                xtype : 'fieldset' , 
                title: 'Price Properties',
                frame: true,
                bodyStyle: 'padding:5px 5px 0',
                collapsible : true ,  
                collapsed : true , 
                defaults: {
                    anchor: '95%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items : [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Xpath :'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    name : 'data[xpath4p]'  ,
                    id : me.id + '-xpath-4p' ,  
                    allowBlank: false  , 
                    disabled  : false  , //--
                    value : me.dataObj.step4_xpath_p 
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Filter type :'
                },{ 
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : me.id+ '-xpath-4p-ft'  ,
                    name: 'data[xpath4p_ft]' , 
                    displayField:   'name',
                    valueField:     'value' ,
                    emptyText : 'Please select filter type' , 
                    store:   me.filterStore , 
                    listeners : {
                        'afterrender' : function( combo , eObj){
                            if( me.dataObj.step_xpath_p_ft == undefined)
                                me.dataObj.step_xpath_p_ft = 0 ;  
                            else 
                                me.dataObj.step_xpath_p_ft = parseInt(me.dataObj.step_xpath_p_ft) ; 
                            combo.setValue(me.dataObj.step_xpath_p_ft) ;   
                            if( me.dataObj.step_xpath_p_ft == 0 ){
                                Ext.getCmp(me.id + '-xpath-4p-cjs').setVisible(false) ; 
                                Ext.getCmp(me.id + '-xpath-4p-cjs2').setVisible(false) ; 
                            }  
                        } , 
                        'change' : function( combo , newVal , oldVal , eObj ){
                            if( newVal > 0){
                                Ext.getCmp(me.id + '-xpath-4p-cjs').setVisible(true) ; 
                                Ext.getCmp(me.id + '-xpath-4p-cjs2').setVisible(true) ; 
                            }else{
                                Ext.getCmp(me.id + '-xpath-4p-cjs').setVisible(false) ; 
                                Ext.getCmp(me.id + '-xpath-4p-cjs2').setVisible(false) ;
                            }
                            // Also process result ; 
                            Ext.getCmp( me.id + '-xpath-4p-test-btn').fireEvent('click')  ; 
                        }
                    }
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4p-test-btn'  ,
                    text : 'Test'  , 
                    listeners : {
                        'click' : function(btn , e , eOpt){
                            var ret = '' ; 
                            var selector =  Ext.getCmp(me.id + '-xpath-2').getValue() + ' ' +Ext.getCmp(me.id + '-xpath-3').getValue() + ' ' + Ext.getCmp(me.id + '-xpath-4p' ).getValue() ; 
                            // Price ; 
                            if( Ext.getCmp( me.id+ '-xpath-4p-ft').getValue() == 1){
                                var firstString = ' var selector = \''+  selector +'\' ; ' ; 
                            	me.ProcessCustomFilter(firstString + Ext.getCmp(me.id + '-xpath-4p-cjs').getValue(),me.id + '-xpath-4p-price') ;
                            }else{   
                           		me.ProcessDefaultFilter_Price(selector,me.id + '-xpath-4p-price') ;
                            } 
                            // Currency ; 
                            if( Ext.getCmp( me.id+ '-xpath-4p-ft').getValue() == 1){
                                var firstString = ' var selector = \''+  selector +'\' ; ' ; 
                            	me.ProcessCustomFilter(firstString + Ext.getCmp(me.id + '-xpath-4p-cjs2').getValue(),me.id + '-xpath-4p-currency') ;
                            }else{   
                            	me.ProcessDefaultFilter_Currency(selector,me.id + '-xpath-4p-currency') ;
                            } 
                        }
                    }
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Price :'
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4p-cjs' , 
                    name : 'data[xpath4p_cjs]' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){
                            if( me.dataObj.step_xpath_p_cjs == undefined)
                                me.dataObj.step_xpath_p_cjs = 'return MONOloop.$(selector).html() ;' ; 
                            textArea.setValue(me.dataObj.step_xpath_p_cjs) ;    
                        }
                    }
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Result :'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    name : 'data[xpath4p-price]'  ,
                    id : me.id + '-xpath-4p-price' ,  
                    allowBlank: true    
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Currency :'
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4p-cjs2' , 
                    name : 'data[xpath4p_cjs2]' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){
                            if( me.dataObj.step_xpath_p_cjs2 == undefined)
                                me.dataObj.step_xpath_p_cjs2 = 'return MONOloop.$(selector).html() ;' ; 
                            textArea.setValue(me.dataObj.step_xpath_p_cjs2) ;    
                        }
                    }
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Result :'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    name : 'data[xpath4p-currency]'  ,
                    id : me.id + '-xpath-4p-currency' ,  
                    allowBlank: true    
                }] 
            },{
                xtype : 'fieldset' , 
                title: 'Quantity Properties',
                frame: true,
                bodyStyle: 'padding:5px 5px 0',
                collapsible : true ,  
                collapsed : true , 
                defaults: {
                    anchor: '95%',
                    allowBlank: false,
                    msgTarget: 'side'
                },
                items : [{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Xpath :'
                },{
                    xtype : 'textfield' , 
                    hideLabel : true ,  
                    name : 'data[xpath4q]'  ,
                    id : me.id + '-xpath-4q' ,  
                    allowBlank: false  , 
                    disabled  : false  , //--
                    value : me.dataObj.step4_xpath_q 
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Filter type :'
                },{ 
                    xtype:          'combo',
                    mode:           'local',
                    hideLabel : true , 
                    allowBlank: false  ,
                    triggerAction:  'all',
                    editable:       false,
                    id : me.id+ '-xpath-4q-ft'  ,
                    name: 'data[xpath4q_ft]' , 
                    displayField:   'name',
                    valueField:     'value' ,
                    emptyText : 'Please select filter type' , 
                    store:   me.filterStore , 
                    listeners : {
                        'afterrender' : function( combo , eObj){
                            if( me.dataObj.step_xpath_q_ft == undefined)
                                me.dataObj.step_xpath_q_ft = 0 ;  
                            else 
                                me.dataObj.step_xpath_q_ft = parseInt(me.dataObj.step_xpath_q_ft) ; 
                            combo.setValue(me.dataObj.step_xpath_q_ft) ;   
                            if( me.dataObj.step_xpath_q_ft == 0 ){
                                Ext.getCmp(me.id + '-xpath-4q-cjs').setVisible(false) ; 
                            }  
                        } , 
                        'change' : function( combo , newVal , oldVal , eObj ){
                            if( newVal > 0){
                                Ext.getCmp(me.id + '-xpath-4q-cjs').setVisible(true) ; 
                            }else{
                                Ext.getCmp(me.id + '-xpath-4q-cjs').setVisible(false) ; 
                            }
                            // Also process result ; 
                            Ext.getCmp( me.id + '-xpath-4q-test-btn').fireEvent('click')  ; 
                        }
                    }
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4q-cjs' , 
                    name : 'data[xpath4q_cjs]' ,   
                    listeners : {
                        'afterrender' : function(textArea , eObj){
                            if( me.dataObj.step_xpath_q_cjs == undefined)
                                me.dataObj.step_xpath_q_cjs = 'return MONOloop.$(selector).html() ;' ; 
                            textArea.setValue(me.dataObj.step_xpath_q_cjs) ;    
                        }
                    }
                },{
                    xtype : 'button' , 
                    cls : 'monoloop-submit-form' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4q-test-btn'  ,
                    text : 'Test'  , 
                    listeners : {
                        'click' : function(btn , e , eOpt){
                             var ret = '' ; 
                             var selector = Ext.getCmp(me.id + '-xpath-2').getValue() + ' ' +  Ext.getCmp(me.id + '-xpath-3').getValue() + ' ' + Ext.getCmp(me.id + '-xpath-4q' ).getValue() ; 
                            if( Ext.getCmp( me.id+ '-xpath-4q-ft').getValue() == 1){
                                var firstString = ' var selector = \''+  selector +'\' ; ' ; 
                            	me.ProcessCustomFilter(firstString + Ext.getCmp(me.id + '-xpath-4q-cjs').getValue(),me.id + '-xpath-4q-ret') ;
                            }else{ 
                            	me.ProcessDefaultFilter(selector,me.id + '-xpath-4q-ret') ;
                            } 
                        }
                    }
                },{
                    xtype : 'displayfield' , 
                    hideLabel : true , 
                    value : 'Result :'
                },{
                    xtype : 'textareafield' , 
                    hideLabel : true ,
                    id : me.id + '-xpath-4q-ret' , 
                    name : 'data[xpath4q_ret]' , 
                    allowBlank : true 
                }] 
            }]
        }) ; 
    } , 
    
    // Interface ; 
    
    processNext : function(){
        
    }
}) ; 