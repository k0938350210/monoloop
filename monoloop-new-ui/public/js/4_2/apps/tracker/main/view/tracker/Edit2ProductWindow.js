Ext.define('Tracker.view.tracker.Edit2ProductWindow', { 
    extend: 'Tracker.view.tracker.Edit2ValueWindow',
    alias: 'widget.trackeredit2product',
    
 	type_id3 : 0 , 
    typeData3 : 0 ,
     
    
    activeFilterType3 : 0 , 
    activePostUserFunc3 : '' , 
    
    initComponent: function(){ 
        this.callParent(arguments); 
    } , 
    
    addCategoryGroup : function(){
        var me = this ; 
        
        if( me.type_id3 == 0)
        	me.type_id3 = null
           
        var field = Ext.create('Ext.form.FieldSet', {
            title: 'Value',
            frame: true,
            bodyStyle: 'padding:5px 5px 0', 
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side'
            },
            items : [{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Tracker Type:' , 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: false  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id + '-type-id3'  ,
                name: 'activeType3', 
                displayField:   'name',
                valueField:     'value' ,
                invalidText : 'Tracker type must be selected' , 
                emptyText : 'Please select tracker type' , 
                store:  me.typeStore , 
                value : me.type_id3 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Data:' , 
                style : { marginBottom: '0px' }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-typeData3' , 
                name : 'activeTypeData3' , 
                allowBlank : true ,
                value : me.typeData3 
            },{
                xtype : 'displayfield' , 
                hideLabel : true , 
                value : 'Filter Type:' , 
                style : { marginBottom: '0px' }
            },{
                xtype:          'combo',
                mode:           'local',
                hideLabel : true , 
                allowBlank: true  ,
                triggerAction:  'all',
                editable:       false,
                id : me.id+ '-ft3'  ,  
                name : 'activeFilterType3' , 
                displayField:   'name',
                valueField:     'value' ,
                emptyText : 'Please select filter type' , 
                store:   me.filterStore , 
                listeners : {
                    'afterrender' : function( combo , eObj){  
                        if( me.activeFilterType3 == '' || me.activeFilterType3 == 0 || me.activeFilterType3 == null){ 
                            combo.setValue(0) ; 
                        }else{
                            combo.setValue(me.activeFilterType3) ; 
                        }  
                    },
                    'change' : function( combo , newVal , oldVal , eObj ){
                        if( newVal > 0){
                            Ext.getCmp(me.id + '-post-user-func3').setVisible(true) ; 
                        }else{
                            Ext.getCmp(me.id + '-post-user-func3').setVisible(false) ; 
                        }  
                    }
                }
            },{
                xtype : 'textareafield' , 
                hideLabel : true ,
                id : me.id + '-post-user-func3' ,   
                name : 'activePostUserFunc3' , 
                allowBlank : true , 
                listeners : {
                    'afterrender' : function(textArea , eObj){  
                        if( me.activePostUserFunc3 == undefined || me.activePostUserFunc3 == ''){
                            //textArea.setValue('return MONOloop.jq(selector).html() ; ') ; 
                            textArea.setValue('') ; 
                        }else{
                            textArea.setValue(me.activePostUserFunc3) ; 
                        }
                    }
                }
            }
            ]
        }) ; 
        
        me.bodyPanel.add(field) ; 
    }
 }) ; 