var LogApp = null ; 

Ext.onReady(function() {  
	
	LogApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Log', 
	  	controllers: ['Log'], 
	  	
	    //autoCreateViewport: true,
	     
 		defaultHistoryToken: '',
	    
	    initRoutes: function(router) { 
			Ext.create('Log.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        });   
	        
	        Ext.create('Ext.form.field.ComboBox',{
	        	store : Ext.create('Log.store.UserList') , 
                queryMode: 'remote', 
                displayField: 'username',
				valueField: 'uid',
				id : 'user-cb' , 
	        	emptyText : '(All)' , 
	        	renderTo: Ext.get('search-text')  
	        }) ; 
	    }
	    
	});
}) ; 