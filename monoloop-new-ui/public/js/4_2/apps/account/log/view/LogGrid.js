Ext.define('User.view.LogGrid', { 
    extend: 'Ext.grid.Panel',
    alias: 'widget.loggrid',
    xtype : 'loggrid', 
    
    initComponent: function() {
    	var me = this ; 
    	var store = Ext.create('Log.store.Log') ; 
    	
    	Ext.apply(this, { 
    		disableSelection: true,
    		store : store , 
    		columns: [
              {header:'Date',width : 200,dataIndex : 'tstamp' ,  sortable : true ,  renderer: Ext.util.Format.dateRenderer('d M Y h:i:s') }, 
              {header:'Message',width : 200 , sortable : false , dataIndex : 'msg' ,  flex : 1 } , 
              {header:'IP',width : 200 , sortable : false , dataIndex : 'ip'  } 
           ] , 
		   bbar: Ext.create('Ext.PagingToolbar', {
	            store: store,
	            displayInfo: true,
	            displayMsg: 'Displaying users {0} - {1} of {2}',
	            emptyMsg: "No users to display",
	            items:[]
	        }) 
   		}) ; 
   		
   		this.callParent(arguments); 
   	} 
}) ; 