Ext.define('Score.controller.Score', {
	extend: 'Ext.app.Controller',
	
	mainLoading : false , 
	
	init: function() {
        var me = this; 
        
        me.control({ 
			scoremainpanel : {
				afterrender : 'onMainPanelRender' 
			} , 
			'scoremainpanel button[text="SAVE"]' : {
				click : 'onSaveClick'
			}
        });
    },
    
    onLaunch: function(){
        
    } , 
    
    // Event ; 
    
    onMainPanelRender : function(panel){
    	panel.setLoading('Loading data') ; 
    	var generalRequest = Ext.create('UI.shared.store.account.General',{
    		success : function(response, opts){ 
				var decodedString = Ext.decode(response.responseText); 
				//console.debug(decodedString) ;  
				Ext.getCmp(panel.id + '-clickdepth').setValue(decodedString.scores.clickdepth) ; 
				Ext.getCmp(panel.id + '-duration').setValue(decodedString.scores.duration) ; 
				//Ext.getCmp(panel.id + '-loyalty').setValue(decodedString.loyalty) ; 
				Ext.getCmp(panel.id + '-recency').setValue(decodedString.scores.recency) ; 
				Ext.getCmp(panel.id + '-latency').setValue(decodedString.scores.latency) ; 
				
				Ext.getCmp(panel.id + '-avg-clickdepth').setValue('&nbsp;&nbsp;&nbsp;<span style="color:rgb(181,181,181)">Currently '+decodedString.scores_avg_clickdepth+' page views per visit in average</span>') ; 
				Ext.getCmp(panel.id + '-avg-duration').setValue('&nbsp;&nbsp;&nbsp;<span style="color:rgb(181,181,181)">Currently '+decodedString.scores_avg_duration+' minutes visit in average</span>') ; 
				//Ext.getCmp(panel.id + '-avg-loyalty').setValue('&nbsp;&nbsp;&nbsp;<span style="color:rgb(181,181,181)">Currently '+decodedString.scores_avg_loyalty+' number visit in average</span>') ; 
				Ext.getCmp(panel.id + '-avg-recency').setValue('&nbsp;&nbsp;&nbsp;<span style="color:rgb(181,181,181)">Currently '+decodedString.scores_avg_recency+' days between each visit</span>') ; 
				Ext.getCmp(panel.id + '-avg-latency').setValue('&nbsp;&nbsp;&nbsp;<span style="color:rgb(181,181,181)">Currently '+decodedString.scores_avg_latency+' minutes visit in average</span>') ;
				//Ext.getCmp(panel.id + '-brands').setValue(decodedString.scores_brands) ; 
				panel.setLoading(false) ; 
    		}
    	}) ;  
    } , 
    
    onSaveClick : function(btn){
    	var frm = btn.up('form') ; 
    	if(frm.getForm().isValid()){ 
			btn.up('scoremainpanel').setLoading('Saving') ; 
			frm.submit({
			 	url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/updateScores',
                success: function(form, action) {
                	btn.up('scoremainpanel').setLoading(false) ;  
					Ext.Msg.alert('Status', 'Changes saved successfully.');
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Failed', action.result.msg);
                    btn.up('scoremainpanel').setLoading(false) ; 
                }
            });
     	}
    }
}) ; 