var CodesApp = null ; 

Ext.onReady(function() { 
	
	ZeroClipboard.setDefaults({
      moviePath: "/zeroclipboard1.2.3/ZeroClipboard.swf"
    }); 
	
	CodesApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Codes', 
	  	controllers: ['Codes'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: 'invocation',
 
	    
	    initRoutes: function(router) {
			Ext.create('Codes.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        });
	    	// 
	        router.name('codeView', 'advance', {controller: 'Codes', action: 'advance'});
	      	router.name('codeIndex', 'invocation', {controller: 'Codes', action: 'index'});
	    }
	    
	});
}) ; 