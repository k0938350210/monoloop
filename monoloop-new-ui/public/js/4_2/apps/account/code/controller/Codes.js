Ext.define('Codes.controller.Codes', {
	extend: 'Ext.app.Controller',
	
	mainLoading : false , 
	
	init: function() {
        var me = this; 
        
        me.control({
            '#main-panel' : {  
                afterrender  : me.onMainAfterRender
            } , 
            '#instant-on' : {
            	change : me.onInstantChange
            } ,
            '[name=instant]' : {
                change : me.onInstantChange
            } , 
            '#antiflicker-on' : {
            	change : me.onInstantChange
            } , 
            '[name=timeout]' : {
            	blur :  me.onInstantChange
            } , 
            
            '#add-to-invocation-btn' : {
            	click : me.onAddInvocationSetup 
            }

        });
    },
    
    onLaunch: function(){
         
    },
    
    index: function(config){
        //alert(123); 
    },
    
    advance: function(config){
    	//alert('view');
    	Ext.getCmp('main-panel').child('[xtype=advancepanel]').expand() ;  
    },
    
    //Events
    
    onMainAfterRender : function(){
    	var me = this ; 
    	me.mainLoading = true ; 
		Ext.getCmp('main-panel').setLoading('Load data') ; 
    	var generalRequest = Ext.create('UI.shared.store.account.General',{
    		success : function(response, opts){ 
				var data = Ext.decode(response.responseText); 
				//console.debug(data) ;    
				Ext.getCmp('invocation-code-hidden').setValue(data.invocation.str) ; 
				Ext.getCmp('invocation-code').update(data.invocation.str2);  
				Ext.ComponentQuery.query('[name=timeout]')[0].setValue(data.invocation.timeout);   
				Ext.ComponentQuery.query('[name=instant]')[0].setValue(data.invocation.content_delivery);  
				Ext.ComponentQuery.query('[name=antiflicker]')[0].setValue(+data.invocation.anti_flicker);
				
				Ext.getCmp('pre-invocation').setValue(data.invocation.pre) ; 
				Ext.getCmp('post-invocation').setValue(data.invocation.post) ; 
				
				Ext.getCmp('main-panel').setLoading(false) ; 
				me.mainLoading = false ; 
    		}
    	}) ; 
    } , 
    
    onInstantChange : function(){ 
    	var me = this ;  
    	if( me.mainLoading )
    		return  ; 
        
        var instant = Ext.getCmp('instanttype').getValue() ; 
        if( instant['instant'].length > 1)
            return ; 
    	
    	Ext.getCmp('main-panel').setLoading('Saving') ;  
		 Ext.Ajax.request({
            success: function(response, opts){
            	Ext.getCmp('main-panel').setLoading(false) ; 
 				var data = Ext.decode(response.responseText); 
 				Ext.getCmp('invocation-code-hidden').setValue(data.invocation.str) ; 
				Ext.getCmp('invocation-code').update(data.invocation.str2);  
            }, 
            url: '/api/account/invocation',
            method: 'POST',
            params: {
            	'c' : instant['instant']  , 
            	'a' : (Ext.getCmp('antiflicker-on').getValue() )? Ext.getCmp('antiflicker-on').inputValue : 0  , 
            	't' : Ext.ComponentQuery.query('[name=timeout]')[0].getValue()
            }
        });
    } , 
    
    onAddInvocationSetup : function(){
    	 //http://localhost/monoloop/index.php?eID=monoloopaccounting&pid=480&cmd=account/savePrePostInvocation
    	 Ext.getCmp('main-panel').setLoading('Saving') ;  
		 Ext.Ajax.request({
            success: function(response, opts){
            	Ext.getCmp('main-panel').setLoading(false) ;  
            }, 
            url: '/api/account/invocation/prepost',
            method: 'POST',
            params: {
            	'post_invocation' : Ext.getCmp('post-invocation').getValue() , 
            	'pre_invocation' : Ext.getCmp('pre-invocation').getValue()
            }
        });
    }
}) ; 