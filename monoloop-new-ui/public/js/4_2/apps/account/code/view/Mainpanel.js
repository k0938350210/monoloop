Ext.define('Codes.view.Mainpanel', {
	alias: 'widget.codemainpanel',
	extend: 'Ext.panel.Panel',
	layout: 'accordion',
	height: 440, 
	
	requires: [
        'Codes.view.InvocationPanel',
        'Codes.view.AdvancePanel' 
    ],
    
    initComponent: function() {
        Ext.apply(this, {
        	border : false , 
            items: [{
            	xtype : 'invocationpanel' , 
                title: 'Invocation Code', 
	            listeners : {
	            	'expand' : function(p , eOpts){
	            		var url = CodesApp.router.generate('codeIndex', {controller: 'Codes', action: 'index'});
            			Ext.util.History.add(url);
	            	}
	            }
            },{
            	xtype : 'advancepanel' , 
                title: 'Advanced Script', 
	            listeners : {
	            	'expand' : function(p , eOpts){
	            		
	            		var url = CodesApp.router.generate('codeView', {controller: 'Codes', action: 'advance'});  
            			Ext.util.History.add(url);
	            	}
	            }
            }] 
        });
        this.callParent();
    }
}) ; 