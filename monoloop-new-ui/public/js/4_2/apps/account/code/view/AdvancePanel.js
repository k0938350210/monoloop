Ext.define('Codes.view.AdvancePanel', {
    alias: 'widget.advancepanel',
    extend: 'Ext.panel.Panel',
    xtype : 'advancepanel', 
    
    initComponent: function() { 
    	var me = this ; 
    	Ext.apply(this, { 
    		autoScroll : true , 
    		layout: { 
		        pack: 'center',
		        type: 'vbox'
		    },
        	bodyPadding: 15, 
        	items : [{
        		xtype : 'container' , 
        		width : '100%' , 
        		layout : 'column' , 
        		style : 'padding : 0 0 10px 0 ' , 
        		items : [{
        			xtype : 'container' , 
        			columnWidth : 0.475 , 
        			items :[{
        				xtype : 'container' , 
        				layout : 'column' , 
        				items : [{
	        				xtype : 'displayfield' , 
	        				value : 'Pre Invocation Script' , 
	        				columnWidth : 0.49
	        			},{
	        				xtype : 'displayfield' , 
	        				value : '&nbsp;' ,
	        				columnWidth : 0.02
	        			},{
	        				xtype : 'displayfield' , 
	        				value : 'This script is always executed <strong>before</strong><br/>the Monoloop code is executed.' , 
	        				columnWidth : 0.49
	        			}
						] 
        			},{
        				xtype : 'textarea' , 
        				style : 'margin : 10px 0 0 0 ; ' , 
        				id : 'pre-invocation' , 
        				height : 200 , 
        				width : '100%'
        			}]
        			
				},{
        			xtype : 'container' , 
        			html : '&nbsp;' , 
        			columnWidth : 0.05
        		},{
        			xtype : 'container' , 
        			columnWidth : 0.475 , 
        			items :[{
        				xtype : 'container' , 
        				layout : 'column' , 
        				items : [{
	        				xtype : 'displayfield' , 
	        				value : 'Post Invocation Script' , 
	        				columnWidth : 0.49
	        			},{
	        				xtype : 'displayfield' , 
	        				value : '&nbsp;' ,
	        				columnWidth : 0.02
	        			},{
	        				xtype : 'displayfield' , 
	        				value : 'This script is always executed <strong>after</strong><br/>the Monoloop code is executed.' , 
	        				columnWidth : 0.49
	        			}
						] 
        			},{
        				xtype : 'textarea' , 
        				id : 'post-invocation' , 
        				height : 200 , 
        				style : 'margin : 10px 0 0 0 ; ' , 
        				width : '100%'
        			}]
        			
				}]
        	},{
        		xtype : 'container' , 
        		width : '100%' , 
        		layout: {  
			        pack: 'center',
			        type: 'hbox'
			    } , 
			    items : [
				{
	        		xtype : 'button' , 
	        		cls : 'monoloop-submit-form' , 
					id : 'add-to-invocation-btn' ,  
	        		text : 'Add to invocation setup' 
	        	}	
				]
        	},{
        		xtype : 'container' , 
        		width : '100%' , 
        		layout: {  
			        pack: 'center',
			        type: 'hbox'
			    } , 
			    items : [
				{
	        		xtype : 'displayfield' ,  
	        		value : 'NOTE: For older browsers you may need to copy and paste the script manually.' 
	        	}	
				]
        	}]	
   		}) ; 
        this.callParent(arguments);
    }
}) ; 