Ext.define('Domain.view.PrivacyWindow', { 
    extend: 'Ext.window.Window',
    alias: 'widget.privacywindow',
    xtype : 'privacywindow', 
    
    data : null , 
    parentWindow : null , 
    
    initComponent: function() {
    	var me = this ; 
    	Ext.apply(this, { 
    		title: 'Customize Privacy Center',
            closable:true,
    		width:600,
            height:415,
    		 
            autoScroll  : true , 
            modal : true , 
     		items : [{
    			xtype : 'form' ,
    			style : 'padding : 10px' ,
    			width : 570 , 
				border : false ,   
    			items : [{
    				xtype : 'checkbox' , 
    				inputValue : 1 , 
    				fieldLabel : 'Enable' , 
    				id : me.id + '-usecustom' , 
    				
    				listeners : {
	                    'change' : function(chkBox , newValue, oldValue, eOpts){
	                        var c = chkBox.nextSibling('container') ; 
	                        if( newValue ){
                        		c.setDisabled(false) ; 
	                        }else{
	                        	c.setDisabled(true) ; 
	                        }
	                    } 
	                }
    			},{
    				xtype : 'container' , 
    				border : false , 
    				disabled : true ,
    				items : [{
    					xtype : 'fieldset' , 
    					title : 'Information' , 
    					width : '95%' , 
	    				items : [{
	    					xtype : 'displayfield' , 
	    					fieldLabel : 'body' , 
	    					value : '&nbsp;'
	    				},{
	    					xtype : 'htmleditor',
	    					id : me.id + '-i1' 
	    				}]
	    			},{
	    				xtype : 'fieldset' , 
	    				title : 'Preferences' , 
	    				width : '95%' , 
	    				defaults : {
		    				anchor : '100%'
		    			},
	    				items : [{
	    					xtype : 'textfield' , 
	    					fieldLabel : 'header' , 
	    					id : me.id + '-p1'
	    				},{
	    					xtype : 'textfield' , 
	    					fieldLabel : 'Tracking onsite behaviour' , 
	    					id : me.id + '-p2'
	    				},{
	    					xtype : 'textfield' , 
	    					fieldLabel : 'Onsite personalization' , 
	    					id : me.id + '-p3'
	    				},{
	    					xtype : 'textfield' , 
	    					fieldLabel : 'Remarketing' , 
	    					id : me.id + '-p4'
	    				},{
	    					xtype : 'textfield' , 
	    					fieldLabel : 'OK' , 
	    					id : me.id + '-p5'
	    				}]
	    			},{
	    				xtype : 'fieldset' , 
	    				fieldLabel : 'Data' , 
	    				width : '95%' , 
	    				defaults : {
		    				anchor : '100%'
		    			},
	    				items : [{
	    					xtype : 'textfield' , 
	    					fieldLabel : 'Delete your monoloop profile' , 
	    					id : me.id + '-d1'
	    				},{
	    					xtype : 'textarea' , 
	    					fieldLabel : 'Confirm delete profile' , 
	    					id : me.id + '-d2'
	    				},{
	    					xtype : 'textfield' , 
	    					fieldLabel : 'Delete your monoloop cookie' , 
	    					id : me.id + '-d3'
	    				},{
	    					xtype : 'textarea' , 
	    					fieldLabel : 'Comfirm delete cookie' , 
	    					id : me.id + '-d4'
	    				}]
	    			}]
    			} ]
    		}] , 
    		buttons : [{
    			text : 'Default' , 
    			cls : 'monoloop-submit-form' 
    		},{
    			text : 'OK' , 
    			cls : 'monoloop-submit-form'
    		}]
   		}) ; 
   		this.callParent(arguments); 
   	} , 
   	
   	loadData : function(){
   		var me = this ; 
   		//console.debug(me.data) ; 
   		Ext.getCmp(me.id + '-i1').setValue(me.data.i1) ; 
   		
   		Ext.getCmp(me.id + '-p1').setValue(me.data.p1) ; 
   		Ext.getCmp(me.id + '-p2').setValue(me.data.p2) ; 
   		Ext.getCmp(me.id + '-p3').setValue(me.data.p3) ; 
   		Ext.getCmp(me.id + '-p4').setValue(me.data.p4) ; 
   		Ext.getCmp(me.id + '-p5').setValue(me.data.p5) ; 
   		
   		Ext.getCmp(me.id + '-d1').setValue(me.data.d1) ; 
   		Ext.getCmp(me.id + '-d2').setValue(me.data.d2) ; 
   		Ext.getCmp(me.id + '-d3').setValue(me.data.d3) ; 
   		Ext.getCmp(me.id + '-d4').setValue(me.data.d4) ; 
   		
   		Ext.getCmp(me.id + '-usecustom').setValue(me.data.privacy_usecustom) ; 
   	} , 
   	
   	saveToParentData : function(){
   		var me = this ; 
   		me.parentWindow.data.i1 = Ext.getCmp(me.id + '-i1').getValue() ; 
   		
   		me.parentWindow.data.p1 = Ext.getCmp(me.id + '-p1').getValue() ; 
   		me.parentWindow.data.p2 = Ext.getCmp(me.id + '-p2').getValue() ; 
   		me.parentWindow.data.p3 = Ext.getCmp(me.id + '-p3').getValue() ; 
   		me.parentWindow.data.p4 = Ext.getCmp(me.id + '-p4').getValue() ; 
   		me.parentWindow.data.p5 = Ext.getCmp(me.id + '-p5').getValue() ; 
   		
   		me.parentWindow.data.d1 = Ext.getCmp(me.id + '-d1').getValue() ; 
   		me.parentWindow.data.d2 = Ext.getCmp(me.id + '-d2').getValue() ; 
   		me.parentWindow.data.d3 = Ext.getCmp(me.id + '-d3').getValue() ; 
   		me.parentWindow.data.d4 = Ext.getCmp(me.id + '-d4').getValue() ;  
   		me.parentWindow.data.privacy_usecustom = Ext.getCmp(me.id + '-usecustom').getValue() ; 
   	} 
}) ; 