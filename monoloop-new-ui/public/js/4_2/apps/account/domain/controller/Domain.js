Ext.define('Domain.controller.Domain', {
	extend: 'Ext.app.Controller',
	  
	
	init: function() {
        var me = this; 
        
        me.control({ 
			'domaingrid' : {   
                'editDomain' : me.onEditDomain , 
                'deleteDomain' : me.onDeleteDomain
            } , 
            'editdomainwindow' : {
            	'afterrender' : me.onEditWindowAfterrender , 
            	'close' : me.onEditWindowClose
            },
            'editdomainwindow button[text="SAVE"]' : {
            	'click' : me.onEditWindowSave
            },
            'editdomainwindow button[text="Customize"]':{
            	'click' : me.onLoadPrivacyWindow 
            }, 
            'privacywindow': {
            	afterrender : me.onPrivacyWindowAfterrender 
            }, 
            'privacywindow button[text="Default"]':{
            	click : me.onPrivacyWindowLoadDefault
            },
            'privacywindow button[text="OK"]':{
            	click : me.onPrivacyWindowOK
            },
            '#add-new-domain':{
            	click : me.onCreateNewDomain 
            },
            '#test-domain':{
          		click : me.reTestAllDomain
            }
        });
    },
    
    onLaunch: function(){
         
    },
    // Controller
    index: function(config){
 
    },
    edit : function(config){
    	//console.debug(config) ; 
		Ext.create('Domain.view.EditDomainWindow',{
    		title : 'Edit Domain' , 
    		uid : config.id 
    	}).show() ; 	
    },
    
    newdomain : function(config){
    	Ext.create('Domain.view.EditDomainWindow',{
    		title : 'Create New Domain'  
    	}).show() ; 	
    } , 
    // Event
    // domaingrid
	onEditDomain : function(cmp , url){
		url = encodeURIComponent(url); ; 
		var url = DomainApp.router.generate('domainEdit', {controller: 'Domain', action: 'edit', id: url});
		Ext.util.History.add(url);
	} ,
	onDeleteDomain : function(cmp , url){
		Ext.MessageBox.show({
    		title:'Messagebox Title',
    		msg: 'Are you sure want to delete?',
    		buttons: Ext.Msg.YESNO,
    		fn : function(btn){
    			if( btn == 'yes'){
    				Ext.getCmp('main-panel').setLoading('Deleting') ; 
    				Ext.Ajax.request({
						success: function(response, opts){
							Ext.getCmp('main-panel').setLoading(false) ;   
							Ext.getCmp('main-panel').child('[xtype=domaingrid]').store.load();
						}, 
						url: 'index.php?eID=monoloopaccounting&pid=480&cmd=domain/remove',
						method: 'POST',
						params: {
							'uid' : url  
						}
					});
    			}
    		}
		}) ;
	} ,
	// editdomainwindow
	onEditWindowAfterrender : function(cmp){
		if(cmp.uid == 0 ){
			cmp.data = {} ; 
			return ; 
		}
		cmp.setLoading('Loading') ;   
		Ext.Ajax.request({
			success: function(response, opts){
				cmp.setLoading(false) ;   
				var data = Ext.decode(response.responseText); 
	 			Ext.getCmp(cmp.id + '-domain').setValue(data.domain) ;
	 			Ext.getCmp(cmp.id + '-url').setValue(data.url_privacy) ;
	 			Ext.getCmp(cmp.id + '-option').setValue(parseInt( data.privacy_option)) ;
	 			cmp.data = data ; 
			}, 
			url: 'index.php?eID=monoloopaccounting&pid=480&cmd=domain/load',
			method: 'POST',
			params: {
				'uid' : cmp.uid  
			}
		});
	} , 
	
	onEditWindowClose : function(cmp){
		Ext.util.History.add('');
	} , 
	
	onEditWindowSave : function(cmp){
		var f = cmp.findParentByType('window').query('form')[0].getForm();  
		var w = cmp.up('editdomainwindow');
		if(f.isValid()){
			 cmp.findParentByType('window').setLoading('Saving') ; 
			 f.submit({
			 	url: 'index.php?eID=monoloopaccounting&pid=480&cmd=domain/save',
                success: function(form, action) {
                	cmp.findParentByType('window').setLoading(false) ; 
                	cmp.findParentByType('window').close() ;   
					Ext.getCmp('main-panel').child('[xtype=domaingrid]').store.load(); 
					Ext.util.History.add('');
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Failed', action.result.msg);
                },
                params : {
                	'i1' : w.data.i1 , 
                	'p1' : w.data.p1 , 
                	'p2' : w.data.p2 , 
                	'p3' : w.data.p3 , 
                	'p4' : w.data.p4 , 
                	'p5' : w.data.p5 , 
                	'd1' : w.data.d1 , 
                	'd2' : w.data.d2 , 
                	'd3' : w.data.d3 , 
                	'd4' : w.data.d4 , 
                	'privacy_usecustom' : w.data.privacy_usecustom
                }
            });
		}
	} , 
	
	onLoadPrivacyWindow : function(btn){
		Ext.create('Domain.view.PrivacyWindow',{
			data : btn.up('editdomainwindow').data , 
			parentWindow : btn.up('editdomainwindow')
		}).show() ; 
	} , 
	
	// Privacy window 
	
	onPrivacyWindowAfterrender : function(win){
		var me = this ; 
		if( win.data.i1 == ''){
			me.loadDefaultPrivacy(win) ; 
		}else{
			win.loadData() ; 
		}
	} , 
	
	loadDefaultPrivacy : function(win){
		win.setLoading('Loading') ;   
		Ext.Ajax.request({
			success: function(response, opts){
				win.setLoading(false) ;   
				var data = Ext.decode(response.responseText);  
				win.data.i1 = data.i1 ; 
				win.data.p1 = data.p1 ;
				win.data.p2 = data.p2 ; 
				win.data.p3 = data.p3 ; 
				win.data.p4 = data.p4 ; 
				win.data.p5 = data.p5 ; 
				win.data.d1 = data.d1 ; 
				win.data.d2 = data.d2 ; 
				win.data.d3 = data.d3 ; 
				win.data.d4 = data.d4 ; 
				win.data.privacy_usecustom = 0 ; 
				win.loadData() ; 
			}, 
			url: 'privacy_center/default.json',
			method: 'POST' 
		});
	} , 
	
	onPrivacyWindowLoadDefault : function(btn){
		var me = this ; 
		var win = btn.up('privacywindow') ; 
		me.loadDefaultPrivacy(win) ; 
	} , 
	
	onPrivacyWindowOK : function(btn){
		var win = btn.up('privacywindow') ; 
		win.saveToParentData() ; 
		win.close() ; 
	} , 
	
	onCreateNewDomain : function(){
		var url = DomainApp.router.generate('domainNew', {controller: 'Domain', action: 'new' });
		Ext.util.History.add(url);
	} , 
	
	reTestAllDomain : function(){
		var me = this ; 
		var store = Ext.getCmp('main-panel').child('grid').store ;
		var total = store.getTotalCount() ; 
 		if(total > 0){
 			var domain = store.getAt(0).get('domain') ; 
 			me.testDomain(domain , 0)
 		}
	},
	
	testDomain : function(domain , i ){
		var me = this  ; 
		var panel = Ext.getCmp('main-panel') ; 
		var store = Ext.getCmp('main-panel').child('grid').store ;
		var total = store.getTotalCount() ; 
		panel.setLoading('Test domain : ' + domain) ;  
		Ext.Ajax.request({
			success: function(response, opts){
				panel.setLoading(false) ;  
				i++ ; 
				if(i<total){
					var domain = store.getAt(i).get('domain') ; 
					me.testDomain(domain,i) ;
				}else{
					Ext.getCmp('main-panel').child('[xtype=domaingrid]').store.load(); 
				}
			}, 
			url: 'index.php?eID=monoloopaccounting&pid=480&cmd=domain/test',
			method: 'GET',
			params: {
				'domain' : domain
			}
		});
	}
}) ; 