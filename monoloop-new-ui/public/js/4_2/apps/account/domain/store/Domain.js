Ext.define('Domain.store.Domain', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: 'uid', type: 'int'},
        {name: 'status' , type : 'int'} , 
    	{name: 'domain'} , 
        {name: 'url_privacy'} , 
        {name: 'privacy_option', type: 'int'}, 
        {name: 'privacy_usecustom', type: 'int'} , 
        {name: 'i1'} , {name: 'p1'} , {name: 'p2'} , {name: 'p3'} , {name: 'p4'} , {name: 'p5'} , {name: 'd1'} , {name: 'd2'} , {name: 'd3'} , {name: 'd4'}
    ] ,
    remoteSort: true,
    pageSize : 1500 ,   
    proxy: {
        type: 'ajax',
        url: 'index.php?eID=monoloopaccounting&pid=480&cmd=domainList' , 
        reader: {
            idProperty  : 'uid' , 
            type: 'json',
            root: 'topics', 
            totalProperty: 'totalCount'  
        } , 
        actionMethods: {
            read: 'POST'
        }
    } , 
    sorters: [{
        property: 'domain',
        direction: 'ASC'
    }] , 
    autoLoad : true 
    
});