Ext.define('Account.controller.Account', {
	extend: 'Ext.app.Controller',
	  
	
	init: function() {
        var me = this; 
        
        me.control({ 
			accountgrid : {
				editAccount : 'onEditAccount' , 
				setStatusAccount : 'onSetAccountStatus' , 
				deleteAccount : 'onDeleteAccount'
			} , 
			
			editaccountwindow : {
				afterrender : 'onEditWindowAfterrender' , 
				beforeclose : 'onEditWindowClose'
			} , 
			
			'editaccountwindow button[text="Save"]' : {
				'click' : me.onEditAccountSave 
			} , 
			
			'terminatewindow button[text="OK"]' : {
				click : 'onTerminateWindowSave'
			} , 
			
			'terminatewindow button[text="CANCEL"]' : {
				click : 'onTerminateWindowCancel'
			} , 
			'#add-new-account' : {
				click : 'onCreateNewAccount'
			}
        });
    },
    
    onLaunch: function(){
         
    },
    
    edit : function(config){
    	Ext.create('Account.view.EditAccountWindow',{
    		title : 'Edit Account' , 
    		uid : config.id 
    	}).show() ; 	
    }, 
    
    index: function(config){
        //alert(123); 
    },
    
    newaccount : function(config){
    	Ext.create('Account.view.EditAccountWindow',{
    		title : 'Create New Account' , 
    		uid : 0
    	}).show() ; 
    } , 
    // Event 
    // Account Grid
    onEditAccount : function(cmp , uid){
    	var url = AccountApp.router.generate('accountEdit', {controller: 'Account', action: 'edit', id: uid});
		Ext.util.History.add(url);
    }, 
    onSetAccountStatus : function(cmd , uid , hidden ){ 
		Ext.getCmp('main-panel').setLoading('Saving') ;   
		Ext.Ajax.request({
			success: function(response, opts){
				Ext.getCmp('main-panel').setLoading(false) ;   
				Ext.getCmp('main-panel').child('[xtype=accountgrid]').store.load();
			}, 
			url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/setAccountHidden',
			method: 'POST',
			params: {
				'uid' : uid  , 
				'hidden' : hidden
			}
		});
	} , 
    // Account Edit Window
    onEditWindowAfterrender : function(panel){ 
    	//cmp.setLoading('XXX') ; 
    	if( panel.uid != 0 ){
			panel.setLoading('Loading') ; 
			Ext.Ajax.request({
				success: function(response, opts){
					panel.setLoading(false) ;   
					var data = Ext.decode(response.responseText); 
					if(data.success == true ){
						Ext.getCmp(panel.id + '-name').setValue(data.data.name) ; 
						Ext.getCmp(panel.id + '-company').setValue(data.data.company) ; 
						Ext.getCmp(panel.id + '-contactname').setValue(data.data.contactName) ; 
						Ext.getCmp(panel.id + '-contactemail').setValue(data.data.contactEmail) ; 
						Ext.getCmp(panel.id + '-invoicereseller').setValue(data.data.isReseller) ; 
						Ext.getCmp(panel.id + '-acid').store.load(function(){
							Ext.getCmp(panel.id + '-acid').setValue(parseInt( data.data.acid)) ; 
						}) ;  
					}
				 
				}, 
				url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/getAccountDetail',
				method: 'POST',
				params: {
					'uid' : panel.uid  
				}
			});
		}
    } , 
    
    onDeleteAccount : function(cmp ,name , uid){
    	Ext.create('Account.view.TerminateWindow',{
    		name : name , 
    		uid : uid 
    	}).show() ; 
    } , 
    // Edit Window
    onEditWindowClose : function(){
    	Ext.util.History.add('');
    } , 
    
    onEditAccountSave : function(btn){
    	var frm =	btn.up('window').down('form') ;
		if(frm.getForm().isValid()){ 
			btn.up('window').setLoading('Saving') ; 
			var url = 'index.php?eID=monoloopaccounting&pid=480&cmd=account/saveAccountEdit' ; 
			if(btn.up('window').uid == 0){
				url = 'index.php?eID=monoloopaccounting&pid=480&cmd=account/saveAccountNew' ;  
			}
			frm.submit({
			 	url: url,
			 	timeout : 600 , 
                success: function(form, action) {
                	btn.up('window').setLoading(false) ; 
                	btn.up('window').close() ;   
					Ext.getCmp('main-panel').child('[xtype=accountgrid]').store.load(); 
					Ext.util.History.add('');
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Failed', action.result.msg);
                    btn.up('window').setLoading(false) ; 
                }
            });
     	}
    } , 
    
    // Terminate Window 
    onTerminateWindowSave : function(btn){
    	var w = btn.up('terminatewindow') ; 
    	var confirm = w.down('textfield').getValue() ; 
    	w.setLoading('Terminating ...');
    	Ext.Ajax.request({
			success: function(response, opts){
				w.setLoading(false) ;   
				var data = Ext.decode(response.responseText); 
				if(data.success == true ){
					Ext.getCmp('main-panel').child('[xtype=accountgrid]').store.load(); 
	 				w.close();
				}else{
					Ext.Msg.alert('Failed', 'Invalid Request');
					w.close();
				}
			 
			}, 
			url: 'index.php?eID=monoloopaccounting&pid=480&cmd=account/terminatingfromagency',
			method: 'POST',
			params: {
				'password' : confirm   , 
				'did' : w.uid    
			}
		});
    } , 
    
    onTerminateWindowCancel : function(btn){
   		btn.up('terminatewindow').close() ; 
    } , 
    
    onCreateNewAccount : function(){
    	var url = AccountApp.router.generate('accountNew', {controller: 'Account', action: 'edit' });
		Ext.util.History.add(url);
    }
}) ; 