Ext.define('Account.view.TerminateWindow', { 
    extend: 'Ext.window.Window',
    alias: 'widget.terminatewindow',
    xtype : 'terminatewindow', 
    name : '' ,
    uid : 0 ,
    
    initComponent: function() {
    	var me = this ; 
    	Ext.apply(this, { 
    		closable:true,
            width : 880 ,   
            height : 460 ,
            resizable : false ,
            title : 'Are you sure you want to terminate '+me.name+' account?' ,
            modal : true , 
            bodyStyle : 'background : rgb(255,203,1) ; padding : 13px ;' , 
            layout : 'fit' , 
            items : [{
                bodyStyle : 'background : white ; padding-top : 40px'  ,
                border : false , 
                layout:'column', 
                items:[{
                	columnWidth:.25,  
                    border : false ,
                    items : [{
                        xtype : 'displayfield' ,
						width : '100%' ,  
                        fieldStyle : 'text-align: center ; font-size : 18px ; font-weight : bold ;' , 
                        value : 'IMPORTANT<BR>NOTIFICATION<BR><BR><img src="./typo3conf/ext/t3p_base/image/warning_icon.gif" />'
                    }]
                },{
                	columnWidth : .65,
                	border : false ,
                	items : [{
                        xtype : 'displayfield' , 
                        fieldStyle : ' font-size : 14px ; line-height:180%' , 
                        value : '<p>Once you terminate your account you will lose access to all of '+me.name+' profile data, reports, tracker definitions, defined business logic, content elements and assets. This content cannot be recoverd once '+me.name+' account is terminated.</p><p>If you terminate the Service before the end of '+me.name+' current paid-up subscription period, '+me.name+' termination will take effect immediately and you will not be charged again.Enter the word "TERMINATE" in the box below and click OK. </p><p><b>If you do not want to terminate '+me.name+' account click CANCEL.</b></p><br>'
                    },{
                    	xtype : 'fieldcontainer' , 
                    	layout : 'hbox' , 
                    	width : '100%' , 
                    	items : [{
                    		xtype : 'textfield' , 
                            id : me.id + '-terminate-pass' , 
                            width : 150
                    	},{
                    		xtype : 'displayfield' , 
                    		value : '&nbsp;&nbsp;'
                    	},{
                    		xtype : 'button' , 
                    		cls : 'monoloop-submit-form' , 
                    		text : 'OK'
                    	},{
                            xtype : 'displayfield' , 
                            value : '&nbsp;&nbsp;&nbsp;&nbsp;'
                        },{
                            xtype : 'button' , 
                            cls : 'monoloop-submit-form' , 
                            text : 'CANCEL' 
                        }]
                    }]
                }]
            }]
   		}) ;
		this.callParent(arguments);  
	}
}) ; 