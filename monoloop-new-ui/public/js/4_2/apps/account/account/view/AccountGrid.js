Ext.define('Account.view.AccountGrid', { 
    extend: 'Ext.grid.Panel',
    alias: 'widget.accountgrid',
    xtype : 'accountgrid', 
    
    initComponent: function() {
    	var me = this ; 
    	var store = Ext.create('Account.store.Account') ; 
    	
    	Ext.apply(this, { 
    		disableSelection: true,
    		store : store , 
    		columns: [
              {header:'Company',  flex : 1  , dataIndex: 'company', sortable: true },  
              {header:'Account Name',  width : 200 , dataIndex: 'name', sortable: true },
			  {header:'Account Type' , width : 200 , dataIndex: 'type_name', sortable: true},
			  {header:'Email' , width : 200 , dataIndex: 'contact_email', sortable: true},
			  {header:'Action' , width : 80, renderer : function(value, p, r){
              		var edit = '<td><div onClick="Ext.getCmp(\''+me.id+'\').editAccount(\''+r.data['uid']+'\')" title="Edit" class="placement-property"></div></td>' ;
		            var deleteData = '<td><div onClick="Ext.getCmp(\''+me.id+'\').deleteAccount(\''+r.data['uid']+'\')" title="Delete" class="placement-delete"></div></td>' ; 
			    	var ret =  '<table width="100%" ><tr align="center"> '  
		                         + edit  
			    			     + deleteData
		                         +
							   '</tr></table>'  ; 
					return ret ;   
              }  } , 
			  {header:'Status' , width : 80 , dataIndex: 'hidden', sortable: true, renderer : function(value, p, r){
              		var hidden = '' ;
		            if( r.data['hidden'] == 0 ){
						hidden = '<td><div onClick="Ext.getCmp(\''+me.id+'\').setHidden('+r.data['uid']+' , 1)"  class="placement-active"></div></td>' ; 
					}else{
						hidden = '<td><div onClick="Ext.getCmp(\''+me.id+'\').setHidden('+r.data['uid']+' , 0)"   class="placement-inactive" ></div></td>' ; 
					}
		            	var ret =  '<table width="100%" ><tr align="center"> ' 
								 + hidden +
							   '</tr></table>'  ; 
					return ret ;	
              }	} 
           ] , 
		   bbar: Ext.create('Ext.PagingToolbar', {
	            store: store,
	            displayInfo: true,
	            displayMsg: 'Displaying  {0} - {1} of {2}',
	            emptyMsg: "No data to display",
	            items:[]
	        }) 
   		}) ; 
   		
   		this.callParent(arguments); 
   	} , 
   	
   	editAccount : function(uid){
   		var me = this ;  
   		me.fireEvent('editAccount' , this , uid ) ; 
   	} , 
   	
   	deleteAccount : function(uid){
   		var me = this ; 
   		var rec = me.getStore().getById( uid ) ; 
   		//console.debug(rec.get('name') ) ; 
   		me.fireEvent('deleteAccount' , this , rec.get('name') , uid ) ; 
   	} , 
   	
   	setHidden : function(uid , hidden){
   		var me = this ;  
   		me.fireEvent('setStatusAccount' , this , uid , hidden ) ; 
   	}
}) ; 