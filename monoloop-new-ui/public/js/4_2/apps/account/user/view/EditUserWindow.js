Ext.define('User.view.EditUserWindow', { 
    extend: 'Ext.window.Window',
    alias: 'widget.edituserwindow',
    xtype : 'edituserwindow', 
    
    uid : 0 , 
    
    initComponent: function() {
    	var me = this ; 
    	var selectorStore = Ext.create('Ext.data.Store', {
		    fields : ['name',  {name: 'value', type: 'int'}],
		    data : [
		        {name : 'Permanent ',   value: 1 },
				{name : 'Fix until date',   value: 2 }  
		        
		    ]
		});
		
    	Ext.apply(this, { 
    		closable:true,
            width:370, 
			overflowY: 'auto' , 
            height:455,
    		modal : true , 
    		items : [{
    			xtype : 'form' , 
    			id : me.id + '-form' ,
    			border : false , 
    			bodyPadding: 15 ,  
				layout: 'anchor',
			    defaults: {
			        anchor: '95%'
			    },
			    items : [{
			    	xtype : 'textfield' , 
			    	allowBlank: false  , 
			    	name : 'userf[name]' , 
			    	fieldLabel : 'Name *' , 
			    	id : 'userf-name' 
			    },{
			    	xtype : 'textfield' , 
					allowBlank: false  , 
					name : 'userf[email]' , 
			    	fieldLabel : 'Email *' 	, 
			    	id : 'userf-email'  
			    },{
			    	xtype : 'textfield' ,  
			    	name : 'userf[company]' , 
			    	id : 'userf-company'  , 
			    	fieldLabel : 'Organization' 	
			    },{
			    	xtype : 'checkbox' , 
			    	name : 'userf[disable]' , 
			    	fieldLabel : 'Disable' , 
			    	id : 'userf-disable'
			    },{
			    	xtype : 'checkbox' , 
			    	name : 'userf[isadmin]' , 
			    	fieldLabel : 'Is Admin' , 
			    	id : 'userf-isadmin' 
			    },{
			    	xtype : 'treepanel'  ,
			    	id : 'userf-rights' ,
			    	fieldLabel : 'Rights' , 
			    	root: { 
	                    nodeType: 'async'
	                }, 
	                store : Ext.create('UI.shared.store.account.Rights') , 
	                autoScroll:true,
	                animate:true,
	                containerScroll: true,
	                rootVisible: false,
			    	height: 200 
			    },{
			    	xtype : 'fieldset' , 
					title : 'Monoloop Support' , 
					defaults : {
						anchor : '100%'	
					} ,
			    	items : [{
			    		xtype : 'combo' , 
			    		fieldLabel : 'Type' ,  
			    		name : 'userf[support_type]' , 
			    		emptyText  : 'Please Select...' , 
			    		store: selectorStore,
				    	queryMode: 'local',
					    displayField: 'name',
					    valueField: 'value',
					    store :  selectorStore  , 
					    listeners : {
					    	change : function(cb , newV , oldV ,eOpts){
					    		if(newV == 2){
					    			cb.nextSibling('datefield').setVisible(true) ; 
					    		}else{
					    			cb.nextSibling('datefield').setVisible(false) ; 
					    		}
					    	}
					    }
			    	},{
			    		xtype : 'datefield' ,
						fieldLabel : 'Enable Until' ,  
						name : 'userf[support_enddate]' , 
						hidden : true , 
			    		format: 'd/m/Y' 
			    	}]
			    }]
    		}]  , 
    		buttons : [
    			{
    				'text' : 'Save' ,
    				cls : 'monoloop-submit-form' , 
    				'f' : me.id + '-form' ,
    				'id' : 'save-edituserwindow-btn'
    			}
    		]
   		}) ; 
    	this.callParent(arguments); 
   	}
}) ; 