Ext.define('User.view.UserGrid', { 
    extend: 'Ext.grid.Panel',
    alias: 'widget.usergrid',
    xtype : 'usergrid', 
    
    initComponent: function() {
    	var me = this ; 
    	var store = Ext.create('User.store.User') ; 
    	
    	Ext.apply(this, { 
    		disableSelection: true,
    		store : store , 
    		columns: [
              {header:'User Name',width : 200 , dataIndex: 'username', sortable:true , flex : 1 }, 
              {header:'Email',width : 200 , sortable : true , dataIndex : 'email' ,  flex : 1 } , 
              {header: 'Action' , dataIndex : '_id' ,  sortable : false , renderer : function(value, p, r){
              		var edit = '<td><div onClick="Ext.getCmp(\''+me.id+'\').editUser(\''+r.data['_id']+'\')" title="Edit" class="placement-property"></div></td>' ;
		            var deleteData = '<td><div onClick="Ext.getCmp(\''+me.id+'\').deleteUser(\''+r.data['_id']+'\')" title="Delete" class="placement-delete"></div></td>' ; 
			    	var ret =  '<table width="100%" ><tr align="center"> '  
		                         + edit  
			    			     + deleteData
		                         +
							   '</tr></table>'  ; 
					return ret ;   
              }  } , 
              {header: 'Status' , dataIndex : '_id' , sortable : true , renderer : function(value, p, r){
              		var hidden = '' ;
		            if( r.data['disable'] == 0 ){
						hidden = '<td><div onClick="Ext.getCmp(\''+me.id+'\').setHidden(\''+r.data['_id']+'\' , 1)"  class="placement-active"></div></td>' ; 
					}else{
						hidden = '<td><div onClick="Ext.getCmp(\''+me.id+'\').setHidden(\''+r.data['_id']+'\' , 0)"   class="placement-inactive" ></div></td>' ; 
					}
		            	var ret =  '<table width="100%" ><tr align="center"> ' 
								 + hidden +
							   '</tr></table>'  ; 
					return ret ;	
              }	}
           ] , 
		   bbar: Ext.create('Ext.PagingToolbar', {
	            store: store,
	            displayInfo: true,
	            displayMsg: 'Displaying users {0} - {1} of {2}',
	            emptyMsg: "No users to display",
	            items:[]
	        }) 
   		}) ; 
   		
   		this.callParent(arguments); 
   	} , 
   	
   	editUser : function(uid){
   		var me = this ;   
   		var rec =  me.getStore().getById(uid) ;  
   		//console.debug(rec.get('isMonoloopSupport')) ; 
   		me.fireEvent('editUser' , this , uid , rec.get('isMonoloopSupport') ) ; 
   	} , 
   	
   	deleteUser : function(uid){
   		var me = this ;  
   		me.fireEvent('deleteUser' , this , uid ) ; 
   	} , 
   	
   	setHidden : function(uid , status){
   		var me = this ;  
   		me.fireEvent('setHidden' , this , uid  ,  status ) ; 
   	}
}) ; 