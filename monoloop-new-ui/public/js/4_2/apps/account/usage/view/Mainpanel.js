Ext.define('Usage.view.Mainpanel', {
	alias: 'widget.uagemainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	
	requires: [
        'Usage.view.UsageGrid' 
    ],
	
 
    
    initComponent: function() { 
        Ext.apply(this, {
        	title : 'Overview' , 
        	border : false ,  
        	layout : 'fit' , 
            items: [{
            	xtype : 'usagegrid'
            }]  
        });
        this.callParent();
    }
}) ; 