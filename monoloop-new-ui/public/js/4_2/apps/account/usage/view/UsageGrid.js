Ext.define('Usage.view.UsageGrid', { 
    extend: 'Ext.grid.Panel',
    alias: 'widget.usagegrid',
    xtype : 'usagegrid', 
    
    initComponent: function() {
    	var me = this ; 
    	//var store = Ext.create('Integration.store.Integration') ; 
    	
    	Ext.apply(this, { 
    		disableSelection: true,
    		//store : store , 
    		columns: [
              {header:'Product Type',  flex : 1 },  
              {header:'Allocated',  width : 200 },
			  {header:'Additional' , width : 200},
			  {header:'Total' , width : 200},
			  {header:'Total <span class="icon-euro"></span>' , width : 200} 
           ] , 
		   bbar: Ext.create('Ext.PagingToolbar', {
	            //store: store,
	            displayInfo: true,
	            displayMsg: 'Displaying  {0} - {1} of {2}',
	            emptyMsg: "No data to display",
	            items:[]
	        }) 
   		}) ; 
   		
   		this.callParent(arguments); 
   	} 
}) ; 