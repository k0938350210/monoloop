Ext.define('Api.controller.Api', {
	extend: 'Ext.app.Controller',
	
	init: function() {
        var me = this; 
        
        me.control({
            'copybutton' : {  
                click  : function(){
                	//
                }
            } 

        });
    },
    
    onLaunch: function(){
         
    },
    
    index: function(config){ 
    	Ext.getCmp('main-panel').setLoading('Load data') ; 
    	var generalRequest = Ext.create('UI.shared.store.account.General',{
    		success : function(response, opts){ 
				var decodedString = Ext.decode(response.responseText); 
				//console.debug(decodedString) ; 
				Ext.getCmp('main-panel-host').setValue(decodedString['apihost']) ; 
				Ext.getCmp('main-panel-cid').setValue(decodedString['cid']) ; 
				Ext.getCmp('main-panel-token').setValue(decodedString['apitoken']) ; 
				Ext.getCmp('main-panel').setLoading(false) ; 
    		}
    	}) ; 
    	//generalRequest.request() ; 
    } 
}) ; 