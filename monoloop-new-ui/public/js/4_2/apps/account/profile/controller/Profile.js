Ext.define('Profile.controller.Profile', {
	extend: 'Ext.app.Controller',
	
	mainLoading : false , 
	
	init: function() {
        var me = this; 
        
        me.control({ 
	 		'form' : {
	 			afterrender : me.afterrender
	 		} , 
	 		
	 		'form button' : {
	 			click : me.formSave
	 		}
        });
    } , 
    
    afterrender : function(panel){
    	var me = this ;
		panel.setLoading('Loading ...') ;    
    	Ext.Ajax.request({
			success: function(response, opts){
				panel.setLoading(false) ;   
				var data = Ext.decode(response.responseText);  
				panel.down('textfield[name="email"]').setValue(data.config.email) ; 
	 		 	panel.down('textfield[name="name"]').setValue(data.config.name) ; 
	 		 	panel.down('displayfield[name="username"]').setValue(data.user.username) ; 
			}, 
			url: '/api/profile',
			method: 'GET' 
		});
    } , 
    
    formSave : function(btn){   
    	var form = btn.up('form').getForm();
        if (form.isValid()) {
            form.submit({
            	url : '/api/profile' , 
                success: function(form, action) {
                   Ext.Msg.alert('Success', action.result.msg);
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Failed', action.result.msg);
                }
            });
        }
    }
}) ; 