Ext.apply(Ext.form.VTypes, {
    password : function(val, field) {
        if (field.initialPassField) {
           var pwd = Ext.getCmp(field.initialPassField);
           return (val == pwd.getValue());
        }
        return true;
     },
    passwordText : 'Enter 2 times the same password!'
});

Ext.define('Profile.view.Mainpanel', {
	alias: 'widget.profilemainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440,
	bodyPadding: 20,  
	 
    
    initComponent: function() {
    	var me = this ; 
        Ext.apply(this, {
        	border : false , 
        	title : 'Profile' , 
            items: [{
            	xtype : 'form' , 
            	id : me.id + '-form'  ,
            	defaults : {
            		anchor : '100%'	, 
            		labelWidth : 180
            	} ,
            	width : 500 , 
            	border : false , 
            	items : [{
            		xtype : 'displayfield' , 
					id : 'error-msg' , 
					cls : 'error-msg' , 
					hidden : true 
            	},{
            		xtype : 'displayfield' , 
            		fieldLabel : 'Username' , 
            		name : 'username'
            	},{
            		xtype : 'textfield' , 
            		fieldLabel : 'Email' , 
            		name : 'email' , 
            		vtype: 'email' 
            	},{
            		xtype : 'textfield' , 
            		name : 'name' , 
            		allowBlank : false  , 
            		fieldLabel : 'Full name'
            	},{
            		xtype : 'textfield' , 
            		inputType : 'password'  , 
            		name : 'password' , 
            		id : 'password' , 
            		vtype: 'password',
            		minLength : 4 , 
            		fieldLabel : 'New password'
            	},{
            		xtype : 'textfield' , 
            		inputType : 'password'  , 
            		name : 'repassword' , 
            		id : 'repassword' ,
            		vtype: 'password',
            		initialPassField: 'password' ,
            		fieldLabel : 'Re-type new password'
            	},{
					xtype : 'container' , 
					layout : 'column' , 
					style : 'padding : 20px 0 0 0 ; ' , 
					items : [{
						xtype : 'displayfield' , 
						value : '&nbsp;' , 
						width : 240
					},{
						xtype : 'button' , 
						cls : 'monoloop-submit-form' , 
						text : 'SAVE PROFILE SETTINGS' , 
						width : 220
					}]
				}]
			}] 
        });
        this.callParent();
    }
}) ; 