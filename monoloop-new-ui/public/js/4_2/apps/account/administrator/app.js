var AdminApp = null ; 

Ext.onReady(function() { 
	 
	
	AdminApp = Ext.create('Util.AbstractApplication', {
	    
	    name: 'Administrator', 
	  	controllers: ['Administrator'], 
	  	
	    //autoCreateViewport: true,
	    
	    defaultHistoryToken: '',
 
	    
	    initRoutes: function(router) {
			Ext.create('Administrator.view.Mainpanel', { 
				id : 'main-panel' , 
	        	renderTo: Ext.get('main-content') 	        		        	
	        }); 
 
	    }
	    
	});
}) ; 