Ext.define('Administrator.view.Mainpanel', {
	alias: 'widget.administratormainpanel',
	extend: 'Ext.form.Panel', 
	height: 340, 
	
 
 
    
    initComponent: function() { 
        Ext.apply(this, { 
        	border : false ,  
        	
            items: [{
				xtype : 'container' , 
				layout : 'column' ,  
				items : [
				{
	            	xtype : 'container' , 
	            	columnWidth : 0.49 , 
	            	items : [{
	            		xtype : 'fieldset' , 
	            		title: 'Your Status',
	            		items : [{
	            			xtype : 'fieldcontainer' ,  
	        				layout : 'hbox' ,
	        				style : 'padding : 0 0 10px 0 ;' , 
	        				items : [{
	        					xtype : 'radio' ,  
	        					boxLabel : 'Private' , 
	        					name : 'antiflicker',
	        					inputValue : 1 , 
	        					width : 100
	        				},{
	        					xtype : 'radio' , 
	        					boxLabel : 'Business' ,  
	        					name : 'antiflicker',
	        					inputValue : 2 , 
	        					width : 100
	        				},{
	        					xtype : 'textfield' , 
	        					emptyText : 'Business Name' ,  
	        					flex : 1 
	        				}]
	            		}]
	            	},{
	            		xtype : 'fieldset' , 
	            		title : 'Your Contact Information' , 
	            		defaults : {
	            			labelAlign : 'right' 
	            		} , 
	            		items : [{
	            			xtype : 'fieldcontainer' , 
	            			layout : 'hbox' , 
	            			
	            			fieldLabel : 'Name' , 
	            			items : [{
	            				xtype : 'textfield' , 
	            				emptyText : 'First' , 
	            				flex : 1 
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp;&nbsp;'
	            			},{
	            				xtype : 'textfield' , 
	            				emptyText : 'Last' , 
	            				flex : 1 
	            			}]
	            		},{
	            			xtype : 'fieldcontainer' , 
	            			layout : 'hbox' , 
	            			fieldLabel : 'Email Address' , 
	            			items : [{
	            				xtype : 'textfield' , 
	            				width : 200
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp;Phone Number:'
	            			},{
	            				xtype : 'textfield' , 
	            				emptyText : 'xxx-xxx-xxxx' , 
	            				flex : 1 
	            			}]
	            		}]
	            	},{
	            		xtype : 'fieldset' , 
	            		defaults : {
	            			labelAlign : 'right' , 
	            			anchor : '100%'
	            		} ,
	            		title : 'Mailing Address' , 
	            		items :[{
	            			xtype : 'textfield' , 
	            			fieldLabel : 'Street Address'
	            		},{
	            			xtype : 'textfield' , 
	            			fieldLabel : '&nbsp;'
	            		},{
	            			xtype: 'fieldcontainer',
	            			fieldLabel : 'City' , 
	            			layout : 'hbox' , 
	            			items : [{
	            				xtype : 'textfield'
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp;&nbsp;State:&nbsp;&nbsp;'
	            			},{
	            				xtype : 'combo' , 
	            				flex : 1 
	            			}]
	            		},{
	            			xtype : 'fieldcontainer' , 
	            			fieldLabel : 'Postal Code' , 
	            			layout : 'hbox' , 
	            			items : [{
	            				xtype : 'textfield' , 
	            				width : 60
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp;&nbsp;Country:&nbsp;&nbsp;'
	            			},{
	            				xtype : 'combo' , 
	            				flex : 1 
	            			}]
	            		}]
	            	}]
	            },{
	            	xtype : 'displayfield' , 
	            	columnWidth : 0.02 , 
	            	value : '&nbsp;'
	            },{
	            	xtype : 'container' , 
	            	columnWidth : 0.49 , 
	            	items : [{
	            		xtype : 'fieldset' , 
	            		style : 'padding-bottom:10px;' , 
	            		title : 'Billing Address' , 
	            		layout : 'form' ,
	            		defaults : {
	            			labelAlign : 'right' , 
	            			anchor : '100%'
	            		} ,
	            		items : [{
	            			xtype : 'checkbox' , 
	            			boxLabel : 'Same as Mailing Address?'
	            		},{
	            			xtype : 'textfield' , 
	            			fieldLabel : 'Street Address'
	            		},{
	            			xtype : 'textfield' , 
	            			fieldLabel : '&nbsp;'
	            		},{
	            			xtype: 'fieldcontainer',
	            			fieldLabel : 'City' , 
	            			layout : 'hbox' , 
	            			items : [{
	            				xtype : 'textfield'
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp;&nbsp;State:&nbsp;&nbsp;'
	            			},{
	            				xtype : 'combo' , 
	            				flex : 1 
	            			}]
	            		},{
	            			xtype : 'fieldcontainer' , 
	            			fieldLabel : 'Postal Code' , 
	            			layout : 'hbox' , 
	            			items : [{
	            				xtype : 'textfield' , 
	            				width : 60
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp;&nbsp;Country:&nbsp;&nbsp;'
	            			},{
	            				xtype : 'combo' , 
	            				flex : 1 
	            			}]
	            		}]
	            	},{
	            		xtype : 'fieldset' , 
	            		title : 'Payment' , 
	            		defaults : {
	            			labelAlign : 'right' , 
	            			anchor : '100%'
	            		} , 
	            		items : [{
	            			xtype : 'fieldcontainer' , 
	            			fieldLabel : 'Method' , 
	            			height : 30 , 
	            			layout : 'hbox' , 
	            			items : [{
	            				xtype : 'radio' , 
	            				boxLabel : '<img src="./fileadmin/monoloop_ui/images/visa.png" />' , 
	            				name : 'cardtype' , 
	            				width : 70
	            			},{
	            				xtype : 'radio' , 
	            				boxLabel : '<img src="./fileadmin/monoloop_ui/images/master.png" />' , 
	            				name : 'cardtype' , 
	            				width : 70 
	            			},{
	            				xtype : 'radio' , 
	            				boxLabel : '<img src="./fileadmin/monoloop_ui/images/amex.png" />' , 
	            				name : 'cardtype' , 
	            				width : 70
	            			},{
	            				xtype : 'displayfield', 
	            				fieldLabel : 'Other:' 
	            			},{
	            				xtype : 'textfield' , 
	            				flex : 1 
	            			}]
	            		},{
	            			xtype : 'textfield'  , 
	            			fieldLabel : 'Name On Card' , 
	            			value : ''
	            		},{
	            			xtype : 'fieldcontainer'  , 
	            			fieldLabel : 'Card Number' , 
	            			layout : 'hbox' , 
	            			items : [{
	            				xtype : 'textfield' , 
	            				flex : 1
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp;Expires:&nbsp;'
	            			},{
	            				xtype : 'combo' , 
	            				emptyText : 'Month' , 
	            				width : 80
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp'
	            			},{
	            				xtype : 'combo' , 
	            				emptyText : 'Year' , 
	            				width : 80
	            			},{
	            				xtype : 'displayfield' , 
	            				value : '&nbsp;CVC:&nbsp;'
	            			},{
	            				xtype : 'textfield' , 
	            				width : 80
	            			}]
	            		}]
	            	}]
            	}]
            },{
            	xtyle : 'fieldcontainer' , 
            	border : false ,
            	layout : 'hbox' , 
            	items : [{
            		xtype : 'displayfield' , 
            		value : '&nbsp;' , 
            		flex : 1
            	},{
            		xtype : 'button' , 
            		cls : 'monoloop-submit-form' , 
            		text : 'RESET'
            	},{
            		xtype : 'displayfield' , 
            		value : '&nbsp;&nbsp;&nbsp;'
            	},{
            		xtype : 'button' ,
					cls : 'monoloop-submit-form' ,  
            		text : 'SAVE'
            	},{
            		xtype : 'displayfield' , 
            		value : '&nbsp;' , 
            		flex : 1
            	}]
            }]  
        });
        this.callParent();
    }
}) ; 