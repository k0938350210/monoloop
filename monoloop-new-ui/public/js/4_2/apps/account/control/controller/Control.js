Ext.define('Control.controller.Control', {
	extend: 'Ext.app.Controller',
	
	mainLoading : false , 
	
	init: function() {
        var me = this; 
        
        me.control({ 
			controlmainpanel : {
				afterrender : 'onMainPanelRender' 
			} , 
			'controlmainpanel button[text="SAVE"]' : {
				click : 'onSaveClick'
			} , 
			'controlmainpanel button[text="RESET"]' : {
				click : 'onResetClick'
			}
        });
    },
    
    onLaunch: function(){
        
    } , 
    
    // Event ; 
    
    onMainPanelRender : function(panel){
    	panel.setLoading('Loading data') ; 
    	var generalRequest = Ext.create('UI.shared.store.account.General',{
    		success : function(response, opts){ 
				var decodedString = Ext.decode(response.responseText); 
				//console.debug(decodedString) ;  
				Ext.getCmp(panel.id + '-enable').setValue(decodedString.control_group.enable) ; 
				Ext.getCmp(panel.id + '-size').setValue(decodedString.control_group.size) ; 
				Ext.getCmp(panel.id + '-day').setValue(decodedString.control_group.days) ;   
				panel.setLoading(false) ; 
    		}
    	}) ;  
    } , 
    
    onSaveClick : function(btn){
    	var frm = btn.up('form') ; 
    	if(frm.getForm().isValid()){ 
			btn.up('controlmainpanel').setLoading('Saving') ; 
			frm.submit({
			 	url: '/api/account/controllgroup',
                success: function(form, action) {
                	btn.up('controlmainpanel').setLoading(false) ;  
					Ext.Msg.alert('Status', 'Changes saved successfully.');
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Failed', action.result.msg);
                    btn.up('controlmainpanel').setLoading(false) ; 
                }
            });
     	}
    } , 
    
    onResetClick : function(btn){
    	alert('on reset process') ; 
    }
}) ; 