Ext.define('Control.view.Mainpanel', {
	alias: 'widget.controlmainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440,
	bodyPadding: 20,  
	 
    
    initComponent: function() {
    	var me = this ; 
        Ext.apply(this, {
        	border : false , 
        	title : 'Overview' , 
            items: [{
            	xtype : 'form' , 
            	id : me.id + '-form'  ,
            	border : false , 
            	items : [{
            		xtype : 'container' , 
            		layout : 'column' , 
            		style : 'padding : 0 0 10px 0 ;' , 
            		border : false , 
            		items : [{
            			xtype : 'displayfield' , 
            			value : 'Enable control group' ,
            			width : 220		
           			},{
           				xtype : 'checkbox' , 
           				id : me.id + '-enable' ,
           				name : 'enable-ab' ,  
						width : 200  
           			}]
				},{
					xtype : 'container' , 
					layout : 'column' , 
					style : 'padding : 0 0 10px 0 ;' , 
					border : false , 
					items : [{
						xtype : 'displayfield' , 
						value : 'Size (%) of the control group:' , 
						width : 220		 
					},{
						xtype : 'textfield' , 
						name : 'size' ,
						id : me.id + '-size' , 
						width : 100
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;&nbsp;%'
					}]
				},{
					xtype : 'container' , 
					layout : 'column' , 
					style : 'padding : 0 0 10px 0 ;' , 
					border : false , 
					items : [{
						xtype : 'displayfield' , 
						value : 'No. of days in control group?' , 
						width : 220		 
					},{
						xtype : 'textfield' , 
						name : 'days' ,
						id : me.id + '-day' , 
						width : 100
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;&nbsp;days'
					}]
				},{
					xtype : 'container' , 
					layout : 'column' , 
					style : 'padding : 20px 0 0 0 ; ' , 
					items : [{
						xtype : 'displayfield' , 
						value : '&nbsp;' , 
						width : 190
					},{
						xtype : 'button' , 
						cls : 'monoloop-submit-form' , 
						text : 'RESET' , 
						width : 80
					},{
						xtype : 'displayfield' , 
						value : '&nbsp;' , 
						width : 10 
					},{
						xtype : 'button' , 
						cls : 'monoloop-submit-form' , 
						text : 'SAVE' , 
						width : 80
					}]
				}]
			}] 
        });
        this.callParent();
    }
}) ; 