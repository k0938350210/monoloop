Ext.define('Datapush.view.Mainpanel', {
	alias: 'widget.datapushmainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	
	requires: [
        'Datapush.view.MainGrid'
    ],
	 
    initComponent: function() { 
        Ext.apply(this, { 
        	border : false ,  
        	layout : 'fit' , 
            items: [{
            	xtype : 'maingrid'
            }]  
        });
        this.callParent();
    }
}) ; 