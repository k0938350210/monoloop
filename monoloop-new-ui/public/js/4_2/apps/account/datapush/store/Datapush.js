Ext.define('Datapush.store.Datapush', {
    extend : 'Ext.data.Store' ,
    fields: [ 
        {name: 'uid', type: 'int'} , 
        {name: 'access_type', type: 'int'},  	
        {name: 'status_type', type: 'int'},
		{name : 'name'} , 
        {name: 'tstamp' , mapping: 'tstamp', type: 'date', dateFormat: 'timestamp'} , 
        {name: 'description'}  
    ] ,
    remoteSort: true,
    pageSize : 15 ,   
    proxy: {
        type: 'ajax',
        url : 'index.php/?eID=ml_application',
        reader: {
            idProperty  : 'uid' , 
            type: 'json',
            root: 'data', 
            totalProperty: 'totalCount'  
        } , 
        actionMethods: {
            read: 'POST'
        }
    } , 
    sorters: [{
        property: 'tstamp',
        direction: 'DESC'
    }] , 
    autoLoad : true 
    
});