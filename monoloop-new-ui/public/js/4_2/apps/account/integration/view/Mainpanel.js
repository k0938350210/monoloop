Ext.define('Integration.view.Mainpanel', {
	alias: 'widget.apimainpanel',
	extend: 'Ext.panel.Panel', 
	height: 440, 
	
	requires: [
        'Integration.view.IntegrationGrid' 
    ],
	
 
    
    initComponent: function() { 
        Ext.apply(this, {
        	title : 'Overview' , 
        	border : false ,  
        	layout : 'fit' , 
            items: [{
            	xtype : 'integrationgrid'
            }]  
        });
        this.callParent();
    }
}) ; 