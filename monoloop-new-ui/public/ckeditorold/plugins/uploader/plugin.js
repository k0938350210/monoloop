CKEDITOR.plugins.add( 'uploader', {
    icons: 'uploader',
    init: function( editor ) {
      editor.addCommand( 'insertUploader',
      {
        exec : function( editor )
        {
          var xapth = CKEDITOR.config.mappedTo,
              selector = monoloop_select_view.getSelectorByXPath(xapth),
              rootSelector = selector,
              id = selector.attr('id'),
              rootId = id,
              tagName = selector.prop("tagName").toLowerCase(),
              rootTagName = tagName,
              src = "";
              switch (tagName) {
                case 'img':
                  src = selector.attr('src');
                  break;
                default:
                  var selector = MONOloop.jq(editor.getSelection().getStartElement().$);
                  if(selector){
                    tagName = selector.prop('tagName').toLowerCase();
                    if(tagName === 'img'){
                      id = selector.attr('id');
                      if(!id){
                        selector.attr('id', monoloop_select_view.randomID('inline_img'));
                        id = selector.attr('id');
                        src = selector.attr('src');
                      }
                    }
                  }
                  break;

              }



              var listener = function (e) {
                     var data = e.data;
                     switch (data.t) {
                      //  case "image-is-uploaded":
                      //     // window.removeEventListener(messageEvent, listener, false);
                      //     console.log("image-is-uploaded", data);
                      //     if(data.data.status === 'server'){
                      //       if(tagName === 'img'){
                      //         selector.attr('src', data.data.url + '?' + Math.random());
                      //         selector.load(function(){
                      //           // monoloop_select_view.pushChangedElement(editor, xapth, rootSelector, rootTagName);
                      //           monoloop_select_view.pushActionsOnXapth(xapth);
                      //         });
                       //
                      //       } else {
                      //         editor.focus();
                      // 				editor.fire( 'saveSnapshot' );
                      // 				editor.insertHtml('<img  src="' + data.data.url + '" />');
                      // 				editor.fire( 'saveSnapshot' );
                      //       }
                      //     }
                       //
                      //     break;
                        case "remove-iframe-event":
                          // window.removeEventListener(messageEvent, listener, false);
                          break;
                       default:
                       break;

                     }
                     // Do whatever you want to do with the data got from IFrame in Parent form.
              };

              // listen from the child
              var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
              var eventer = window[eventMethod];

              var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";


              // Listen to message from child IFrame window
              eventer(messageEvent, listener, false);


          parent.postMessage(JSON.stringify({t: "open-image-uploader", data: {id: id, tagName: tagName, src: src}}), MONOloop.domain)
        }
      });

      editor.ui.addButton( 'Uploader',
      {
      	label: 'Upload',
      	command: 'insertUploader',
        toolbar: 'uploader,1',
      	icon: this.path + 'icons/uploader.png'
      } );
    }
});
