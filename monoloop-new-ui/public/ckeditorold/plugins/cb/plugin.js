CKEDITOR.plugins.add( 'cb', {
    icons: 'cb',
    init: function( editor ) {
      editor.addCommand( 'insertCb',
      {
        exec : function( editor , options)
        {
          var type, condition_content;
          if(typeof options === 'object'){
            type = options.type;
            condition_content = options.condition;
          } else {
            type = options;
          }
          var listener = function(e){
            var data = e.data;
            switch (data.t) {
              case "condition-selected":
                // if(data.type !== undefined && data.type === 'variation'){
                if(true){

                  if(MONOloop.jq('.webui-popover').length > 0){
    								var iframe_edit = MONOloop.jq('.webui-popover iframe')[0];
                    iframe_edit.contentWindow.postMessage(JSON.stringify({t: 'set-condition', data: {condition: data.returnCondition.logical}}), iframe_edit.src);
    							}

                  var selection = editor.getSelection();

                  if (!selection) {
                    return;
                  }
                  var text = selection.getSelectedText();
                  if(text.trim().length > 0){
                    var element = selection.getStartElement();
                    if(element.getAttribute('condition') !== null && element.getAttribute('condition') !== undefined){
                      element.setAttribute('condition', data.returnCondition.logical);
                      editor.insertHtml(element.getHtml());
                    } else {
                      editor.insertHtml('[<div style="display:inline;color:green;" title="'+ data.returnCondition.logical +'" condition="'+ data.returnCondition.logical +'">' + text + '</div>]');
                    }
                  } else {
                    monoloop_select_view.addConditionToChangedElements(data.returnCondition.logical);
                  }
                  window.removeEventListener(messageEvent, listener, false);
                } else {

                }
              default:
              break;
            }
          };

          // listen from the child
          var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
          var eventer = window[eventMethod];

          var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
          // Listen to message from child IFrame window
          eventer(messageEvent, listener, false);

          var selection = editor.getSelection();
          var text = selection.getSelectedText();
          var element = selection.getStartElement();

          var condition = condition_content ? condition_content : "if ( ) {|}";

          if(text.trim().length  === 0){
          } else {
            if(element.getAttribute('condition') !== null && element.getAttribute('condition') !== undefined){
              condition = element.getAttribute('condition');
            }
          }
          window.parent.postMessage(JSON.stringify({t: "open-condition-builder", type: type, condition: condition}), MONOloop.domain);
        }
      });
      editor.ui.addButton( 'Cb',
      {
      	label: 'Insert condition builder',
      	command: 'insertCb',
        toolbar: 'ePlugins,1',
      	icon: this.path + 'icons/cb.png'
      } );
    }
});
