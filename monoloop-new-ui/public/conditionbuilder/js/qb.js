var returnCondition = {};
returnCondition.logical = "";
returnCondition.json = "";
parent.iFrameWin = window;
parent.iFrameConditionBuilderWin = window;

var tempCountry = [];

var level = 0;
var levelNodes = [];

angular.module('APP', ['ngDragDrop'])

.directive('collection', function() {
    return {
      restrict: "E",
      replace: true,
      scope: {
        collection: '=',
        config: '=',
        group: '='
      },
      templateUrl: function() {
        return 'memberInCollection.html';
      },
      link: function(scope, element, attrs) {
        // log("collection.link.scope", scope);
      }
    }
  })
  .directive('member', function($compile) {
    return {
      restrict: "E",
      replace: true,
      scope: {
        member: '=',
        collection: '=',
        config: '=',
        group: '='
      },
      templateUrl: function() {
        return 'member.html';
      },
      link: function(scope, element, attrs, ngModel) {

        var collectionSt = '<collection ng-model="collection" collection="member.rules" group="group" config="config" ></collection>';

        if (angular.isArray(scope.member.rules)) {

          $compile(collectionSt)(scope, function(cloned, scope) {
            element.append(cloned);
          });

        }


        scope.linkingLogicalOperator = undefined;

        /**************************Draggable events****************************/


        /* update node with requied parameters
         * @param node updating node with required paramters
         * @return void
         */
        function createNewNode(node) {
          switch (node.Type) {
            case "integer":
              node.binaryConditions = integerBinaryConditions;
              node.binaryConditionsDefault = '==';
              node.value = "";
              break;
            case "string":
              node.binaryConditions = stringBinaryConditions;
              node.binaryConditionsDefault = 'is';
              node.value = "";
              break;
            case "single":
              node.binaryConditions = stringBinaryConditions;
              node.binaryConditionsDefault = 'is';
              node.listValues = node.Values;
              // node.value = node.Values[0];
              node.value = '';

              break;
            case "single remote":
              node.binaryConditions = stringBinaryConditions;
              node.binaryConditionsDefault = 'is';
              node.listValues = node.Values;
              node.value = "";
              break;
            case "single remote2":
              node.binaryConditions = stringBinaryConditions;
              node.binaryConditionsDefault = 'is';
              node.listValues = node.Values;
              node.value = "";
              break;
            case "bool":
              node.binaryConditions = 'is';
              node.binaryConditionsDefault = 'is';
              node.value = true;
              break;
            case "function":
              switch (node.ReturnValue) {
                //console.log();
                case "integer":
                  node.binaryConditions = integerBinaryConditions;
                  node.binaryConditionsDefault = '==';
                  node.value = "";
                  break;
                case "string":
                  node.binaryConditions = stringBinaryConditions;
                  node.binaryConditionsDefault = 'is';
                  node.value = "";
                  break;
                case "single":
                  node.binaryConditions = stringBinaryConditions;
                  node.binaryConditionsDefault = 'is';
                  node.listValues = node.Values;
                  node.value = node.Values[0];

                  break;
                case "single remote":
                  node.binaryConditions = stringBinaryConditions;
                  node.binaryConditionsDefault = 'is';
                  node.listValues = node.Values;
                  node.value = node.Values[0];
                  break;
                case "single remote2":
                  node.binaryConditions = stringBinaryConditions;
                  node.binaryConditionsDefault = 'is';
                  node.listValues = node.Values;
                  node.value = node.Values[0];
                  break;
                case "bool":
                  node.binaryConditions = 'is';
                  node.binaryConditionsDefault = 'is';
                  node.value = true;
                  break;
                default:
                  break;
              }

              node.extendedListParams = node.Values;

              break;
            default:
              break;
          }
          return node;
        }

        /*
         * Style current element to make relationship with child or same level nodes
         * @params styles
         * @return void
         */
        function createMemberStyles(rules, styles) {

          var customStylesAnd = findNodeStylesIfAnd(scope.dndDragItem);
          styles.and = customStylesAnd.and;
          var customStylesOr = findNodeStylesIfOr(scope.dndDragItem);
          styles.or = customStylesOr.or;
          findIfHasNodeOnNextIndex(scope.group.rules, scope.dndDragItem.uid);
          // styles.or.isEnabled = findIfHasNodeOnNextIndex(scope.group.rules, scope.dndDragItem.uid);
          refreshStyles(rules);
        }

        /* finding node's parent and telling if this node have another node next to it
         * @param rules recursive childern rules
         * @param uid unique id in tree for node
         * @return void
         */
        function findIfHasNodeOnNextIndex(rules, uid) {

          for (var i = 0; i < rules.length; i++) {
            // log("rules[" + i + "]", rules[i]);
            if (i > 0) { // enabling previod node OR link
              rules[i - 1].styles.or.isEnabled = true;
              // log("prev::styles", rules[i - 1]);
            }
            if (rules[i].uid === uid) {

              if (rules[i + 1] !== undefined) { // if have node next to it
                rules[i].styles.or.isEnabled = true;
                return true;
              } else {
                return false;
              }
            } else {
              parent = rules[i]; // store last node to parent
              findIfHasNodeOnNextIndex(rules[i].rules, uid);
            }
          }
          return false;
        }
        /* refresh styles after dropping new node
         * @param rules recursive childern rules
         * @param uid unique id in tree for node
         * @return void
         */
        function refreshStyles(rules) {
          for (var i = 0; i < rules.length; i++) {

            if (rules[i].rules.length > 0) {
              refreshStyles(rules[i].rules)
            }
            var customStylesOr = findNodeStylesIfOr(rules[i]);
            rules[i].styles.or.icon = customStylesOr.or.icon;
            rules[i].styles.or.line = customStylesOr.or.line;
          }
        }


        /* finding parent node in the overall rules and injective new dragged condition if relation is AND */
        function findParentNodeAndInsertIfAnd(rules, uid) {
          if (scope.member.rules.length < 1) {
            scope.dndDragItem.added = true;
            scope.member.rules.unshift(scope.dndDragItem);
          } else {
            scope.dndDragItem.added = true;
            scope.$parent.member.linkingLogicalOperator = "AND";
            scope.member.rules[0].linkingLogicalOperator = "OR";
            scope.member.rules.unshift(scope.dndDragItem);
          }
          makeParentNeigbhourRelationAnd(rules, uid);
        }

        function makeParentNeigbhourRelationAnd(rules, uid){
          for (var i = 0; i < rules.length; i++) {
            if (rules[i].uid === uid) {

              if(rules[i-1] !== undefined){
                if(rules[i-1].rules.length < 1){
                  rules[i-1].linkingLogicalOperator = "OR";
                }
              }
              return true;
            } else {
              // log("rules", rules[i]);
              makeParentNeigbhourRelationAnd(rules[i].rules, uid);
            }
          }
        }

        /* finding parent node in the overall rules and injecting new dragged condition if relation is OR */
        function findParentNodeAndInsertIfOr(rules, uid) {

          // if (scope.$parent.member.rules.length > 0) {
          //   (scope.$parent.member.rules[0].linkingLogicalOperator === "AND") && (scope.dndDragItem.added = true) && (scope.member.rules.unshift(scope.dndDragItem));
          // } else {
            for (var i = 0; i < rules.length; i++) {
              if (rules[i].uid === uid) {
                var r = rules[i].rules;
                rules[i].rules = [];
                scope.dndDragItem.added = true;
                rules.splice(i + 1, 0, scope.dndDragItem);
                rules[i+1].rules = r;
                return true;
              } else {
                // log("rules", rules[i]);
                findParentNodeAndInsertIfOr(rules[i].rules, uid);
              }
            }
          // }
        }


        // start drag from inner condition list
        scope.onInnerConditionStart = function(event, ui, $index) {
          // log("onConditionParameterDrag conditionParameter", draggedConditionParameter);
          // log("onInnerConditionStart $index", $index);
          scope.config.hideConditionLogicalOperators = false;
          scope.config.hideConditionLogicalOperatorsClass = 'linksActiveState';
          scope.config.hideLogicalRelations = true;
          scope.$apply();
        };

        // drag from inner condition list
        scope.onInnerConditionDrag = function(event, ui) {
          // log("onConditionParameterDrag conditionParameter", draggedConditionParameter);
          // log("onInnerConditionStart ui", ui);
          scope.config.hideConditionLogicalOperators = false;
          scope.config.hideConditionLogicalOperatorsClass = 'linksActiveState';
          scope.config.hideLogicalRelations = true;
          scope.$apply();
        };
        // Stop dragged condition parameter
        scope.onInnerConditionStop = function(event, ui) {
          // log("onInnerConditionStart event", event);
          scope.config.hideConditionLogicalOperators = true;
          scope.config.hideConditionLogicalOperatorsClass = 'linksHiddenState';
          scope.config.hideLogicalRelations = false;
          scope.$apply();
        };


        /**************************Droppable events****************************/

        // drop on condition list
        scope.onConditionDrop = function(event, ui, $index) {
          level = 0;
          levelNodes = [];
          // log("scope.$parent.member", scope.$parent.member);
          // log("scope.member", scope.member);
          // log("scope", scope);
          // if (typeof(scope.dndDragItem.added) === 'undefined' || scope.dndDragItem.added === false) {
            // log("scope.dndDragItem.rules", scope.dndDragItem.rules);
            if (typeof(scope.dndDragItem.rules) === 'undefined') {
              scope.dndDragItem.rules = [];
            }

            scope.dndDragItem.linkingLogicalOperator = scope.linkingLogicalOperator;
            scope.dndDragItem.uid = generateUID();
            scope.dndDragItem.hideConditionLogicalOperators = true;
            scope.config.hideConditionLogicalOperatorsClass = 'linksHiddenState';
            createNewNode(scope.dndDragItem);

            scope.$parent.member.linkingLogicalOperator = scope.linkingLogicalOperator;
            if (scope.dndDragItem.linkingLogicalOperator === "AND") {

              findParentNodeAndInsertIfAnd(scope.group.rules, scope.$parent.member.uid);

            } else if (scope.dndDragItem.linkingLogicalOperator === "OR") {
              scope.$parent.member.linkingLogicalOperator = "AND";
              findParentNodeAndInsertIfOr(scope.group.rules, scope.$parent.member.uid);

            }

            // style relationship of node
            scope.dndDragItem.styles = {};
            createMemberStyles(scope.group.rules, scope.dndDragItem.styles);
            // log("scope.group.rules", scope.group.rules);

            angular.element(event.target).removeClass("well_hoverd");

            scope.$apply();

            // console.log("scope.dndDragItem",scope.dndDragItem);
            // lookupFields(scope.dndDragItem);


          // } else {
          //   alert("from list");
          // }
          // log("scope.member", scope.member);
          // log("scope.$parent.member", scope.$parent.member);


          // console.log("second");

          $(".integer").numeric();

          // drawOrLines();



        };
        // Mouse over on condition list
        scope.onConditionOver = function(event, ui) {
          // log("ONOverONOverONOverONOver", event);
          angular.element(event.target).addClass("well_hoverd");
          if (angular.element(event.target).hasClass("and")) {
            scope.linkingLogicalOperator = "AND";
          } else if (angular.element(event.target).hasClass("or")) {
            scope.linkingLogicalOperator = "OR";
          } else {
            scope.linkingLogicalOperator = undefined;
          }

          scope.$apply();
          // log("scope.linkingLogicalOperator OnOver", scope.linkingLogicalOperator);

        };
        // Mouse out condition list
        scope.onConditionOut = function(event, ui) {
          angular.element(event.target).removeClass("well_hoverd");
          if (angular.element(event.target).hasClass("and")) {

          } else if (angular.element(event.target).hasClass("or")) {

          } else {

          }
          scope.$apply();
        };

        scope.removeCondition = function(uid) {
          var parent = {};
          var childRules = [];

          findNodeAndDelete(scope.group.rules, uid); // delete and move it's childern to its parent

          refreshStyles(scope.group.rules);

          if (scope.group.rules.length < 1) {
            scope.config.mainContainerDrop = true;
          }


          /* refresh styles after dropping new node
           * @param rules recursive childern rules
           * @param uid unique id in tree for node
           * @return void
           */
          function refreshStyles(rules) {

            for (var i = 0; i < rules.length; i++) {

              if (rules[i].rules.length > 0) {
                refreshStyles(rules[i].rules)
              }
              var customStylesOr = findNodeStylesIfOr(rules[i]);

              rules[i].styles.or.icon = customStylesOr.or.icon;
              rules[i].styles.or.line = customStylesOr.or.line;
              if(rules[i + 1] !== undefined){
                rules[i].styles.or.isEnabled = true;
              } else {
                rules[i].styles.or.isEnabled = false;
              }
            }

          }

          /* finding node with uid in the overall rules and deleting
           * @param rules recursive childern rules
           * @param uid unique id in tree for node to be deleted
           * @return void
           */
          function findNodeAndDelete(rules, uid) {

            for (var i = 0; i < rules.length; i++) {

              if (rules[i].uid === uid) {
                childRules = rules[i].rules;
                rules.splice(i, 1); // removing selected condition
                if (childRules.length > 0) { // moving deleted node's child to parent node
                  appendChildRoutesToParent(scope.group.rules, parent.uid, childRules);
                }
                return true;
              } else {
                parent = rules[i]; // store last node to parent so we can append deleted node's childern to it
                findNodeAndDelete(rules[i].rules, uid);
              }
            }
          }

          /* finding parent and appending deleted node's child to parent condition rules
           * @param rules recursive childern rules
           * @param uid unique id in tree for node to be deleted
           * @param childRules rules to be appended to parent
           * @return void
           */
          function appendChildRoutesToParent(rules, uid, childRules) {

            // if parent doesn't not exists e.g. a root node
            if (uid === undefined) {
              if(childRules.length > 0){
                // scope.group.rules = rules.concat(childRules);
                scope.group.rules = childRules.concat(rules);
                if(scope.group.rules.length > 0){
                  scope.group.rules[0].styles.or.isEnabled = true;
                }
              }
              return true;
            }
            // if parent found
            for (var i = 0; i < rules.length; i++) {
              if (rules[i].uid === uid) {

                rules[i].rules = rules[i].rules.concat(childRules);
                return true;
              } else {
                appendChildRoutesToParent(rules[i].rules, uid, childRules);
              }
            }
          }

        }

      }
    }
  });

function ConditionParameterController($scope) {

}

function initializeGuidesAndTipsOnConditionBuilder(mode){
    var _condiitonBuilderMode = mode || '';

    // tooltips

    guidelines.page.pageId = (_condiitonBuilderMode);

    // guidelines
    var _guideLinesURl = monolop_api_base_url+'/api/guidelines?page_id=' + guidelines.page.pageId;
    webix.ajax().get(_guideLinesURl, { }, function(text, xml, xhr){
      var r = JSON.parse(text);

      var cg = undefined;
      if(r.currentGuide){
        cg = JSON.parse(r.currentGuide);
        guidelines.startedBefore = true;
      }
      guidelines[guidelines.page.pageId] = r.guidelines;

      if(cg){
        if(cg.currentGuide){

          guidelines.page.currentGuide = cg.currentGuide;
        } else {
          guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
        }
      } else {
        guidelines.page.currentGuide = guidelines.first(guidelines[guidelines.page.pageId]);
      }
      if(guidelines.page.currentGuide){
        if(guidelines.startedBefore === false){
          setTimeout(function(){guidelines.generate(guidelines.page.currentGuide, {placement: 'right', offsetTop: 70});},2000);
        }else{
          if($('#toggleGuides').hasClass('fa-toggle-on')){
            $('#toggleGuides').removeClass('fa-toggle-on');
            $('#toggleGuides').addClass('fa-toggle-off');
            $('#toggleGuides').attr('title', 'Show Guides');
          }
        }
      }else{
        if($('#toggleGuides').hasClass('fa-toggle-on')){
          $('#toggleGuides').removeClass('fa-toggle-on');
          $('#toggleGuides').addClass('fa-toggle-off');
          $('#toggleGuides').attr('title', 'Show Guides');
        }
      }
      guidelines.startedBefore = false;
    });
}

function QueryBuilderController($scope, $http, $sce) {
  initializeGuidesAndTipsOnConditionBuilder('conditionBuilder');
  // guidelines.startGuideline('conditionBuilder', {placement: 'right', offsetTop: 70});
  $scope.config = {};
  $scope.config.mainContainerDrop = true;
  $scope.config.hideConditionLogicalOperators = true;
  $scope.config.hideConditionLogicalOperatorsClass = 'linksHiddenState';
  $scope.config.hideLogicalRelations = false;
  $scope.message = {};
  $scope.message.success = '';
  $scope.group = {};
  $scope.group.rules = [];
  $scope.instructions = true;

  var queryBuilder = {
    parser: function(query){
      //console.log(query);
      var parseed_arr = this.findSubstr(query);
      //console.log("parser = > ",   JSON.stringify(parseed_arr));
      var anyLogicalExists = true;
        for (var i = 0; i < parseed_arr.length; i++) {
          if(parseed_arr[i].isLogical){
            if(parseed_arr[i].rules.length === 0){
              var substring = parseed_arr[i].value;
              var substring1 = substring.substr(1, substring.length - 2);
              //var parse = this.findSubstr(substring1);
              var parse = this.parser(substring1);
              if(parse.length === 1){
                parseed_arr[i] = parse[0];
              }
              else{
                parseed_arr[i].rules = parse;
              }

              //parseed_arr[i].value = parse[0].value;
              //parseed_arr[i].isLogical = parse[0].value.isLogical;
              //parseed_arr[i].ruels =
            }//ENd inner if
          }//ENd outer if
          else if(parseed_arr[i].rules.length > 0) {
            for (var j = 0; j < parseed_arr[i].rules.length; j++) {
              if(parseed_arr[i].rules[j].isLogical){
                var substring = parseed_arr[i].rules[j].value;
                var substring1 = substring.substr(1, substring.length - 2);
                //var parse = this.findSubstr(substring1);
                var parse = this.parser(substring1);
                //parseed_arr[i].rules[j].rules = parse;
                if(parse.length === 1){
                  parseed_arr[i].rules[j] = parse[0];
                }
                else{
                  parseed_arr[i].rules = parse;
                }
              }//ENd if
            }//ENd inner for
          }//ENd inner else
        }//ENd for
      return parseed_arr;
    },
    findOperator: function(query){

    },
    removeParentBrackets: function(query, query_main){

      query = query.split('');

      var length = query.length;
      var uid;
      for (var i = 0; i < length; i++) {
        uid = this.getUniqueId();
        if(this.isOpeningBracket(query[i])){
          query[i] = {levelId: uid, value: query[i]};
          for (var k = i + 1, t = 0; k < length; k++) {
            if(this.isOpeningBracket(query[k])){
              t++;
              continue;
            }
            if(this.isClosingBracket(query[k])){
              if(t !== 0){
                t--;
                continue;
              } else {
                query[k] = {levelId: uid, value: query[k]};
                break;
              }
            }
          }
        }
      }
      if(query[0].levelId === query[query.length -1].levelId){
        query_main = query_main.substr(1, query_main.length -1);
        query.shift();
        query.pop();
        // this.removeParentBrackets(query_main, query_main);
      } else {


      }
      return query_main;
    },
    findSubstr: function(query){

      var stack = [];
      var substrings = [];
      var index = 0;
      var indexIntialized = false;

      //var nextPush = false;
      //var nextIndex = 0;
      var prevPush = true;
      var prevIndex = 0;

      for(var i = 0; i < query.length; i++){
        var curChar = query[i];
        if(this.isOpeningBracket(curChar)){
          stack.push(curChar);
          if(indexIntialized === false){
            index = i;
            indexIntialized  = true;
          }
        }
        else if(this.isClosingBracket(curChar)){
          stack.pop();

          if(stack.length === 0) {
            // console.log("index => ", i);
            var ss = query.substr(index, (i + 1 - index));

            // ss = this.removeParentBrackets(ss,ss);

            if(prevPush){
              substrings.push({value: ss, linkingLogicalOperator: 'AND', isLogical: this.isLogicalExpression(ss), rules: []});
            }
            else {
              substrings[prevIndex].rules.push({value: ss, linkingLogicalOperator: 'AND', isLogical: this.isLogicalExpression(ss), rules: []});
            }

            var operator = query.substr(i+1,1);
            var operator2 = query.substr(i+1,2);

            if(operator === '|' && operator2 !== '||'){
              prevPush = true;
              prevIndex = substrings.length - 1;
            }
            else if(operator === '&'){
              prevPush = false;
              prevIndex = substrings.length - 1;
            }
            else {

            }

            indexIntialized  = false;

          }//End inner if

        }//End else if

      }//End for


      return substrings;

    },

    // if(!this.isLogicalExpression(ss)){
    //   finalRules.push({condition: ss, rules: []});
    // } else {
    //   this.findSubstr(ss.substr(1, ss.length -1));
    // }

    isLogicalExpression: function(exppression){
      if((exppression.indexOf('&') > -1 || exppression.indexOf('|') > -1) && (exppression.indexOf('||') === -1 )){
        return true;
      } else {
        return false;
      }
    },
    stringToStack: function(query){
      query = query.replace(/&&/g, '&').replace(/\|\|/g, '|').replace(/ +/g, '').replace(/["]/g, "").trim(); // remove white spaces and replace && and || with & and | for simplicity

      var rules = [];
      var stack = [];
      var allowedCharsInStack = ['(',')','&','|'];


      for(var i = 0; i < query.length; i++){
        var curChar = query[i];
        if(allowedCharsInStack.indexOf(curChar) > -1){
          stack.push(curChar);
        } else {
          // stack.pop();
          var tempQuery = query.substr(i);
          var paramsBracketClosed = false;
          for (var k = 0, t = 0; k < tempQuery.length; k++) {
            var tempCurChar = tempQuery[k];
            if(this.isOpeningBracket(tempCurChar)){
              t++;
              continue;
            }

            if(this.isClosingBracket(tempCurChar)){
              if (t === 1 && paramsBracketClosed === false){
                paramsBracketClosed = true;
                continue;
              }
              stack.push(tempQuery.substr(0, k));
              stack.push(tempCurChar);
              i = i+k;
              break;
            }
          }
        }
      }
      return stack;
    },
    isExpression: function(node){
      return (node.length > 1);
    },
    isAnd: function(node){
      return (node === '&');
    },
    isOr: function(node){
      return (node === '|');
    },
    isOpeningBracket: function(node){
      return (node === '(');
    },
    isClosingBracket: function(node){
      return (node === ')');
    },
    getUniqueId: function(){
      return Math.floor((Math.random() * 5000 * 5000 * 5000) + 500);
    },
    defineStackNodeLevels: function(stack){

      var length = stack.length;
      var uid;
      for (var i = 0; i < length; i++) {
        uid = this.getUniqueId();
        if(this.isOpeningBracket(stack[i])){
          stack[i] = {levelId: uid, value: stack[i]};
          for (var k = i + 1, t = 0; k < length; k++) {
            if(this.isOpeningBracket(stack[k])){
              t++;
              continue;
            }
            if(this.isClosingBracket(stack[k])){
              if(t !== 0){
                t--;
                continue;
              } else {
                stack[k] = {levelId: uid, value: stack[k]};
                break;
              }
            }
          }
        }
      }

      return stack;
    },
    findNodeValue: function(node){
      var parseNode = jsep(node.value);
      var nodeId, nodeOperator, nodeValue;
      switch (parseNode.type) {
        case "BinaryExpression":
          switch (parseNode.left.type) {
            case "MemberExpression":
              if(parseNode.left.property.type === 'Identifier'){
                nodeId = parseNode.left.property.name;

              }else if(parseNode.left.property.type === 'Literal'){
                if( parseNode.left.object.object.property.name === 'cv' || parseNode.left.object.object.property.name === 'form'){
                  nodeId = parseNode.left.object.object.property.name + '[' + parseNode.left.object.property.raw + '][' + parseNode.left.property.raw + ']';
                }
                if( parseNode.left.object.object.property.name === 'co'){
                  nodeId = parseNode.left.object.object.property.name + '[' + parseNode.left.object.property.raw + '][' + parseNode.left.property.raw + ']';
                }
              }
              nodeOperator = parseNode.operator;
              nodeValue = parseNode.right.value;
              break;
            case "CallExpression":
              nodeId = parseNode.left.callee.property.name;
              nodeOperator = parseNode.operator;
              nodeValue = parseNode.right.value;


              break;
            default:
              break;
          }

          break;
        case "CallExpression":
          if(parseNode.arguments[0].property.type === 'Literal'){
            if(parseNode.arguments[0].object.object.property.name === 'cv' || parseNode.arguments[0].object.object.property.name === 'form'){
              nodeId = parseNode.arguments[0].object.object.property.name + '[' + parseNode.arguments[0].object.property.raw + '][' +parseNode.arguments[0].property.raw + ']';
            }
            if( parseNode.arguments[0].object.object.property.name === 'co'){
              nodeId = parseNode.arguments[0].object.object.property.name + '[' + parseNode.arguments[0].object.property.raw + '][' + parseNode.arguments[0].property.raw + ']';
            }

          }else{
            nodeId = parseNode.arguments[0].property.name;

          }
          nodeOperator = parseNode.callee.name;
          nodeValue = parseNode.arguments[1].value;

          break;
        default:
          nodeId = parseNode.arguments[0].property.name;
        break;
      }


      var newNode = createNewNode($scope.conditionParametersArray[nodeId]);

      switch (newNode.Type) {
        case "integer":
          newNode.binaryConditionsDefault = nodeOperator;
          break;
        case "string":
          newNode.binaryConditionsDefault = _.findKey(stringBinaryConditionsSymbols, (val) => val === nodeOperator);
          break;
        case "single":
          newNode.binaryConditionsDefault = _.findKey(stringBinaryConditionsSymbols, (val) => val === nodeOperator);
          break;
        case "single remote":
          newNode.binaryConditionsDefault = _.findKey(stringBinaryConditionsSymbols, (val) => val === nodeOperator);
          break;
        case "single remote2":
          newNode.binaryConditionsDefault = _.findKey(stringBinaryConditionsSymbols, (val) => val === nodeOperator);
          break;
        case "bool":
          break;
        case "function":
          switch (newNode.ReturnValue) {
            case "integer":
              newNode.binaryConditionsDefault = nodeOperator;
              break;
            case "string":
              break;
            case "single":
              break;
            case "single remote":
              break;
            case "single remote2":
              break;
            case "bool":
              break;
            default:
              break;
          }
          newNode.extendedListParams = newNode.Values;
          if(newNode.extendedListParams !== undefined){

            for (var i = 0; i < newNode.extendedListParams.length; i++) {
              newNode.extendedListParams[i].data = parseNode.left.arguments[i].value;
            }

          }
          break;
        default:
          break;
      }
      newNode.value = nodeValue;
      return newNode;

    },

    createRules: function(rules){
      for (var i = 0; i < rules.length; i++) {
        var index=0,val, node = this.findNodeValue(rules[i]);
        //console.log("node = > ",   JSON.stringify(node));
        var list;
        if(node.listValues){
          if(node.listValues.length>0){
            for(var j=0;j<node.listValues.length;j++){
            list=node.listValues[j].value;
            if(list){
                if(list.replace(/ +/g, '') === node.value){
                    node.value = node.listValues[j].value;
                  }
              }
            }
          }
        }

        if(node.key==='Country'){
          val=node.value;
          node.value = getCountries(val);
          if(!node.value){
            node.value=val;
          }
        }
        rules[i].Group = node.Group;
        rules[i].Label = node.Label;
        rules[i].Type = node.Type;
        rules[i].binaryConditions = node.binaryConditions;
        rules[i].binaryConditionsDefault = node.binaryConditionsDefault;
        rules[i].key = node.key;
        rules[i].qtipCfg = node.qtipCfg;
        rules[i].listValues = node.listValues;
        // rules[i].hideConditionLogicalOperators = true;
        rules[i].uid = generateUID();
        rules[i].value = node.value;
        node.ReturnValue !== undefined && (rules[i].ReturnValue = node.ReturnValue);



switch (rules[i].Type) {
          case "integer":
            break;
          case "string":
            break;
          case "single":
            break;
          case "single remote":
            break;
          case "single remote2":
            break;
          case "bool":
            break;
          case "function":
            switch (rules[i].ReturnValue) {
              case "integer":
                break;
              case "string":
                break;
              case "single":
                break;
              case "single remote":
                break;
              case "single remote2":
                break;
              case "bool":
                break;
              default:
                break;
            }
            rules[i].Values = node.Values;
            rules[i].extendedListParams = node.Values;
            break;
          default:
            break;
        }
        $scope.dndDragItem = rules[i];

        $scope.dndDragItem.styles = {};
        createMemberStyles($scope.group.rules, $scope.dndDragItem.styles);
        if(rules[i].rules.length > 0){
          this.createRules(rules[i].rules);
        }

      }

      return rules;
    }
  }

  if (window.addEventListener) {
    // For standards-compliant web browsers
    window.addEventListener("message", receiveCondition);
  }
  else {
    window.attachEvent("onmessage", receiveCondition);
  }


  function receiveCondition(evt){
    _condition = evt.data;
    $http.get(profilePropertiesUrl, {
      headers: {
        "Content-type": ajaxHeaders['Content-Type'],
  			"ML-Token": ajaxHeaders['ML-Token']
      }
    }).
    then(function(response) {

      $scope.conditionParametersArray = responseData = response.data;
      // grouping condition pararmeters into mentioned groups in responseData array
      $scope.conditionParameters = _.groupBy(responseData, function(d, k) {
        return d.Group
      });

      //console.log("_condition", _condition);

      if(_condition){
        _condition = _condition.replace('if (','');
        _condition = _condition.replace('){|}', '');
        _condition = _condition.replace(') {|}', '');
      }
      //console.log("_condition",_condition);
      var input = _condition;
      input = input.replace(/ &&/g, '&').replace(/ \|\|/g, '|').replace(/ +/g, '').replace(/["]/g, "").trim(); // remove white spaces and replace && and || with & and | for simplicity
      var rules = queryBuilder.parser(input);
      //console.log("rules = > ",   JSON.stringify(rules));
      var finalRules = queryBuilder.createRules(rules);
      $scope.group.rules = finalRules;
      refreshStyles($scope.group.rules);
      if($scope.group.rules.length > 0){
        $scope.config.mainContainerDrop = false;
      }
      // $http.post(monolop_api_base_url+"/api/condition-builder/parse", {c: _condition}, {headers: {'Content-Type': ajaxHeaders['Content-Type'], 'ML-Token': ajaxHeaders['ML-Token']} })
      //     .then(function (response) {
      //       var input = response.data;
      //       input = input.replace(/&&/g, '&').replace(/\|\|/g, '|').replace(/ +/g, '').replace(/["]/g, "").trim(); // remove white spaces and replace && and || with & and | for simplicity
      //       var rules = queryBuilder.parser(input);
      //
      //       var finalRules = queryBuilder.createRules(rules);
      //       $scope.group.rules = finalRules;
      //       refreshStyles($scope.group.rules);
      //       if($scope.group.rules.length > 0){
      //         $scope.config.mainContainerDrop = false;
      //       }
      // });



    }, function(response) {
    });




    // if (_condition !== null && _condition.rules.length > 0) {
    //   $scope.group.rules = _condition.rules;
    //   $scope.config.mainContainerDrop = false;
    //   $scope.$apply();
    // }
  }

  function createArrayForBuilder(data){


    for(var i = 0; i < data.length; i++ ){


      if(data[i].rules.length != ""){
        createArrayForBuilder(data[i].rules);
      } else {
        var _node = $scope.conditionParametersArray[data[i].fields.id];
        _node = createNewNode(_node);
        data[i].NodeProperties = _node;
      }

    }

  }

  var items = [];
  function getBranch(data, nRules){
    for(var i = 0; i < data.length ; i++) {

      if(data[i].rules.length > 0) {
        getBranch(data[i].rules, nRules);
      } else {
      }


    }

  }

  // condition form this.user
  $scope.conditionForm = {
    id: _id,
    condition: _condition,
    source: _source,
    action: _action
  };

  // save condition
  $scope.submit = function($event) {
    $http({
      method: 'POST',
      url: $scope.conditionForm.action,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      transformRequest: function(obj) {
        var str = [];
        for (var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      },
      data: {
        source: $scope.conditionForm.id,
        condition: $scope.conditionForm.condition,
        condition_json: JSON.stringify($scope.group)
      }
    }).success(function() {
      $scope.message.success = 'Saved.';
      window.top.closeConditionBuilder();

    });


    return false;
  };

  // drag from condition parameter list
  $scope.onConditionParameterDrag = function(event, ui) {
    // log("onConditionParameterDrag conditionParameter", draggedConditionParameter);
    $scope.config.hideConditionLogicalOperators = false;
    $scope.config.hideConditionLogicalOperatorsClass = 'linksActiveState';
    $scope.config.hideLogicalRelations = true;
    // $scope.$apply();
  };
  // Stop dragged condition parameter
  $scope.onConditionParameterStop = function(event, ui) {
    $scope.config.hideConditionLogicalOperators = true;
    $scope.config.hideConditionLogicalOperatorsClass = 'linksHiddenState';
    $scope.config.hideLogicalRelations = false;
    $scope.instructions = false;
    // log("Dragged $scope.group.rules", $scope.group.rules);
    // $scope.$apply();
  };

  /**************************Droppable events****************************/

  // drop on condition list
  $scope.onConditionDrop = function(event, ui, $index) {

    // log("scope.dndDragItem.rules", scope.dndDragItem.rules);
    if (typeof($scope.dndDragItem.rules) === 'undefined') {
      $scope.dndDragItem.rules = [];
    }
    $scope.dndDragItem.linkingLogicalOperator = "";
    $scope.dndDragItem.uid = generateUID();
    $scope.dndDragItem.hideConditionLogicalOperators = true;
    $scope.config.hideConditionLogicalOperatorsClass = 'linksHiddenState';
    createNewNode($scope.dndDragItem);

    // style relationship of node
    $scope.dndDragItem.styles = {};
    createMemberStyles($scope.group.rules, $scope.dndDragItem.styles);
    // log("$scope.dndDragItem", $scope.dndDragItem);
    $scope.group.rules.push($scope.dndDragItem);



    $scope.config.mainContainerDrop = false;

    // log("$scope.group.rules", $scope.group.rules);

    // $scope.$apply();

    // lookupFields($scope.dndDragItem);
    $(".integer").numeric();
  };
  // Mouse over on condition list
  $scope.onConditionOver = function(event, ui) {
    $scope.linkingLogicalOperator = undefined;
  };
  // Mouse out condition list
  $scope.onConditionOut = function(event, ui) {
    $scope.linkingLogicalOperator = undefined;
  };
  // lookup for if scope group changes
  $scope.$watch('group', function(updatedGroup) {
    var str = "";
    var html = 'if (' + computedCondition($scope.group.rules, str) + '){|}';
    $scope.output = $sce.trustAsHtml(html);
    $scope.conditionForm.condition = html;
    returnCondition.logical = html;
    returnCondition.json = JSON.stringify($scope.group);
    log("watch-scope.group", $scope.group);
    lookupFields();

  }, true);

  /* update node with requied parameters
   * @param node updating node with required paramters
   * @return void
   */
  function createNewNode(node) {
    // log("$scope.dndDragItem", $scope.dndDragItem);

    if(node){
      switch (node.Type) {
        case "integer":
          node.binaryConditions = integerBinaryConditions;
          node.binaryConditionsDefault = '==';
          node.value = "";
          break;
        case "string":
          node.binaryConditions = stringBinaryConditions;
          node.binaryConditionsDefault = 'is';
          node.value = "";
          break;
        case "single":
          node.binaryConditions = stringBinaryConditions;
          node.binaryConditionsDefault = 'is';
          node.listValues = node.Values;
          // node.value = node.Values[0];
          node.value = '';

          break;
        case "single remote":
          node.binaryConditions = stringBinaryConditions;
          node.binaryConditionsDefault = 'is';
          node.listValues = node.Values;
          node.value = "";
          break;
        case "single remote2":
          node.binaryConditions = stringBinaryConditions;
          node.binaryConditionsDefault = 'is';
          node.listValues = node.Values;
          node.value = "";
          break;
        case "bool":
          node.binaryConditions = 'is';
          node.binaryConditionsDefault = 'is';
          node.value = true;
          break;
        case "function":
          switch (node.ReturnValue) {
            case "integer":
              node.binaryConditions = integerBinaryConditions;
              node.binaryConditionsDefault = '==';
              node.value = "";
              break;
            case "string":
              node.binaryConditions = stringBinaryConditions;
              node.binaryConditionsDefault = 'is';
              node.value = "";
              break;
            case "single":
              node.binaryConditions = stringBinaryConditions;
              node.binaryConditionsDefault = 'is';
              node.listValues = node.Values;
              node.value = node.Values[0];

              break;
            case "single remote":
              node.binaryConditions = stringBinaryConditions;
              node.binaryConditionsDefault = 'is';
              node.listValues = node.Values;
              node.value = node.Values[0];
              break;
            case "single remote2":
              node.binaryConditions = stringBinaryConditions;
              node.binaryConditionsDefault = 'is';
              node.listValues = node.Values;
              node.value = node.Values[0];
              break;
            case "bool":
              node.binaryConditions = 'is';
              node.binaryConditionsDefault = 'is';
              node.value = true;
              break;
            default:
              break;
          }
          // log("node.Values", node.Values);
          node.extendedListParams = node.Values;

          break;
        default:
          break;
      }
      return node;
    }
    return node;
  }

  /*
   * Style current element to make relationship with child or same level nodes
   * @params styles
   * @return void
   */
  function createMemberStyles(rules, styles) {

    var customStylesAnd = findNodeStylesIfAnd($scope.dndDragItem);
    // log("customStylesAnd", customStylesAnd);
    styles.and = customStylesAnd.and;
    var customStylesOr = findNodeStylesIfOr($scope.dndDragItem);
    styles.or = customStylesOr.or;
    findIfHasNodeOnNextIndex($scope.group.rules, $scope.dndDragItem.uid);
    // styles.or.isEnabled = findIfHasNodeOnNextIndex(scope.group.rules, scope.dndDragItem.uid);
    refreshStyles(rules);
  }

  /* finding node's parent and telling if this node have another node next to it
   * @param rules recursive childern rules
   * @param uid unique id in tree for node
   * @return void
   */
  function findIfHasNodeOnNextIndex(rules, uid) {

    for (var i = 0; i < rules.length; i++) {
      // log("rules[" + i + "]", rules[i]);
      if (i > 0) { // enabling previod node OR link
        rules[i - 1].styles.or.isEnabled = true;
        // log("prev::styles", rules[i - 1]);
      }
      if (rules[i].uid === uid) {

        if (rules[i + 1] !== undefined) { // if have node next to it
          rules[i].styles.or.isEnabled = true;
          return true;
        } else {
          return false;
        }
      } else {
        parent = rules[i]; // store last node to parent
        findIfHasNodeOnNextIndex(rules[i].rules, uid);
      }
    }
    return false;
  }
  /* refresh styles after dropping new node
   * @param rules recursive childern rules
   * @param uid unique id in tree for node
   * @return void
   */
  function refreshStyles(rules) {
    for (var i = 0; i < rules.length; i++) {

      if (rules[i].rules.length > 0) {
        refreshStyles(rules[i].rules)
      }
      var customStylesOr = findNodeStylesIfOr(rules[i]);
      rules[i].styles.or.icon = customStylesOr.or.icon;
      rules[i].styles.or.line = customStylesOr.or.line;

      findIfHasNodeOnNextIndex(rules, rules[i].uid)
    }
  }
  /* getCountries
   *
   * @param val unique code or name for country
   * @return string
   */
  function getCountries(val,flag){
    var country_info;
    flag = flag || 0;
    if(val){
    jQuery.ajax({
      url: '/api/countries',
      success: function(countries) {
        for(var j=0;j<countries.length;j++){
            if(flag){
              countries[j].name="'"+ countries[j].name+"'";
              if( countries[j].name == val){
                 country_info="'"+ countries[j].code+"'";

              }
            }else{
              if(countries[j].code == val){
                 country_info= countries[j].name;
              }
            }
        };
      },
      async:false
    });
    }
      return country_info;
}

  /*
   * @function computedCondition compute advance condition from condition builder
   * @param rules recursive conditions
   * @return string advance conditiona as string
   */
  function computedCondition(rules, str) {

    if (!rules) return "";

    for (var i = 0, str = ''; i < rules.length; i++) {


      var labelKey, value, binaryOperator;
      // assiging label key and changing if it's not default "MonoloopProfile"
      if(rules[i].Group !== undefined && rules[i].Group === 'Audiences'){
        labelKey = 'Segments.' + rules[i].key + '()';
      } else {
        labelKey = (typeof(rules[i].labelKey) !== 'undefined') ? rules[i].labelKey : labelKeyDefault + '.' + rules[i].key;
      }

      switch (rules[i].Type) {
        case "integer":
          binaryOperator = rules[i].binaryConditionsDefault;
          value = "'" + rules[i].value + "'";
          break;
        case "string":
          binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
          value = "'" + rules[i].value + "'";
          break;
        case "single":
          binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
          value = "'" + rules[i].value + "'";

          break;
        case "single remote":
          binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
          value = "'" + rules[i].value + "'";
          break;
        case "single remote2":
          binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
          value = "'" + rules[i].value + "'";
          break;
        case "bool":
          binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
          value = rules[i].value;
          break;
        case "function":
          switch (rules[i].ReturnValue) {
            case "integer":
              binaryOperator = rules[i].binaryConditionsDefault;
              value = "'" + rules[i].value + "'";
              break;
            case "string":
              binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
              value = "'" + rules[i].value + "'";
              break;
            case "single":
              binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
              value = "'" + rules[i].value + "'";
              break;
            case "single remote":
              binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
              value = "'" + rules[i].value + "'";
              break;
            case "single remote2":
              binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
              value = "'" + rules[i].value + "'";
              break;
            case "bool":
              binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
              value = rules[i].value;
              break;
            default:
              binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
              value = "'" + rules[i].value + "'";
              break;
          }

          // appending extendedListParams with key (left side of Binary Expression)
          if (typeof(rules[i]) !== 'undefined') {
            labelKey += '(';
            for (var k = 0; k < rules[i].Values.length; k++) {
              labelKey += '\'' + rules[i].Values[k].data + '\'';
              (k + 1 !== rules[i].Values.length) && (labelKey += ', ');
            }
            labelKey += ')';
          }

          break;
        default:
          binaryOperator = stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault];
          value = "'" + rules[i].value + "'";
          break;
      }

      var code;
      if(labelKey === 'MonoloopProfile.Country'){
        if(value.length > 0){
          for (var j = 0; j < tempCountry.length; j++) {
            var ns  = "'" + tempCountry[j].text + "'";
            if(value == ns){
              value = "'" + tempCountry[j].value + "'";

              break;
            }
          }
        }
        code=getCountries(value,1);
        if(code){
          value=code;
        }
      }

      // appending condition
      var conditionTemp = '';
      if (typeof(stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault]) !== 'undefined' &&
        stringBinaryConditionsSymbols[rules[i].binaryConditionsDefault].indexOf("ml_") > -1) {
        conditionTemp = ' ( ' + binaryOperator + '(' + labelKey + ', ' + value + ' ) ' + ' ) ';
      } else {
          conditionTemp = ' ( ' + labelKey + " " + binaryOperator + " " + value + ' ) ';

      }

      // find if current condition have further childern
      if (rules[i].rules.length > 0) {
        str += ' ( ';
        str += conditionTemp;

        if(rules[i].rules.length > 0){
          // console.log("logicalOperatorSymbols[rules[i].rules[0].linkingLogicalOperator]",logicalOperatorSymbols[rules[i].rules[0].linkingLogicalOperator]);
          str += logicalOperatorSymbols[rules[i].rules[0].linkingLogicalOperator];
        } else {
          // console.log("logicalOperatorSymbols[rules[i].rules[0].linkingLogicalOperator]", logicalOperatorSymbols[rules[i].linkingLogicalOperator]);
          str += logicalOperatorSymbols[rules[i].linkingLogicalOperator];
        }

        str += ' ( ';
        str += computedCondition(rules[i].rules, str);
        str += ' ) ';
        str += ' ) ';
      } else {
        str += conditionTemp;
      }
      // append logical operator for next condition on same level
      if (rules[i + 1] !== undefined) {
        // str += logicalOperatorSymbols[rules[i + 1].linkingLogicalOperator];
        str += logicalOperatorSymbols["OR"];
        // console.log("logicalOperatorSymbols[rules[i + 1].linkingLogicalOperator]",logicalOperatorSymbols[rules[i + 1].linkingLogicalOperator]);
      }
    }
    return str;
  }

}

/*
 * logging
 * @param name a key
 * @param data value to log
 */
function log(name, data) {
  // console.log('++++++++' + name + '++++++++');
  // console.log(data);
}
/*
 * logging
 * @return uid unique ID (being used for every node in the condition builder tree)
 */
function generateUID() {
  var d = new Date().getTime();
  var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}
/*
 * Find node styles if relationship is AND (top, left, height)
 * @param node
 * @return styles
 */
function findNodeStylesIfAnd(node) {
  styles = {};
  styles.and = {};
  styles.and.line = {};
  styles.and.icon = {};
  switch (node.Type) {
    case "function":

      var valuesLength = node.Values.length;
      switch (valuesLength) {
        case 1:
          styles.and.line.top = 127 + 52;
          styles.and.icon.top = 127 + 52;
          break;
        case 2:
          styles.and.line.top = 167 + 52;
          styles.and.icon.top = 167 + 52;
          break;
        case 3:
          styles.and.line.top = 202 + 52;
          styles.and.icon.top = 202 + 52;
          break;
        case 4:
          styles.and.line.top = 237 + 52;
          styles.and.icon.top = 237 + 52;
          break;
        default:
          styles.and.line.top = 97 + 52;
          styles.and.icon.top = 97 + 52;
          break;
      }
      break;
    default:
      styles.and.line.top = 97 + 52;
      styles.and.icon.top = 97 + 52;
      break;
  }
  styles.and.line.left = 62;
  styles.and.line.height = 30;
  styles.and.icon.left = 48;

  return styles;
}
/*
 * Find node styles if relationship is OR (top, left, height)
 * @param node
 * @return styles
 */
function findNodeStylesIfOr(node) {
  styles = {};
  styles.or = {};
  styles.or.line = {};
  styles.or.line.lineUpper = {};
  styles.or.line.lineMiddle = {};
  styles.or.line.lineLower = {};
  styles.or.icon = {};
  styles.or.icon.left = 0;

  styles.or.line.lineUpper.top = 0;
  styles.or.line.lineUpper.left = 0;
  styles.or.line.lineUpper.width = 0;

  styles.or.line.lineMiddle.top = 0;
  styles.or.line.lineMiddle.left = 0;
  styles.or.line.lineMiddle.height = 0;

  styles.or.line.lineLower.top = 0;
  styles.or.line.lineLower.left = 0;
  styles.or.line.lineLower.width = 0;

  styles = calculateNodeStyles(node, styles)


  return styles;
}

function calculateNodeStyles(node, styles) {
  // log("styles", styles);
  log("node", node);

  var no_of_childs = node.rules.length;

  switch (node.Type) {
    case "function":

      var valuesLength = node.Values.length;
      switch (valuesLength) {
        case 1:
          styles.or.line.lineUpper.top = 21;
          styles.or.line.lineUpper.left = -8;
          styles.or.line.lineUpper.width = 8;

          styles.or.line.lineMiddle.top = 21;
          styles.or.line.lineMiddle.left = -8;
          styles.or.line.lineMiddle.height += 211;

          styles.or.line.lineLower.top += 231;
          styles.or.line.lineLower.left = -8;
          styles.or.line.lineLower.width = 8;
          break;
        case 2:
          styles.or.line.lineUpper.top = 21;
          styles.or.line.lineUpper.left = -8;
          styles.or.line.lineUpper.width = 8;

          styles.or.line.lineMiddle.top = 21;
          styles.or.line.lineMiddle.left = -8;
          styles.or.line.lineMiddle.height += 249;

          styles.or.line.lineLower.top += 269;
          styles.or.line.lineLower.left = -8;
          styles.or.line.lineLower.width = 8;
          break;
        case 3:
          styles.or.line.lineUpper.top = 21;
          styles.or.line.lineUpper.left = -8;
          styles.or.line.lineUpper.width = 8;

          styles.or.line.lineMiddle.top = 21;
          styles.or.line.lineMiddle.left = -8;
          styles.or.line.lineMiddle.height += 283;

          styles.or.line.lineLower.top += 304;
          styles.or.line.lineLower.left = -8;
          styles.or.line.lineLower.width = 8;
          break;
        case 4:
          styles.or.line.lineUpper.top = 21;
          styles.or.line.lineUpper.left = -8;
          styles.or.line.lineUpper.width = 8;

          styles.or.line.lineMiddle.top = 21;
          styles.or.line.lineMiddle.left = -8;
          styles.or.line.lineMiddle.height += 318;

          styles.or.line.lineLower.top += 339;
          styles.or.line.lineLower.left = -8;
          styles.or.line.lineLower.width = 8;
          break;
        default:
          styles.or.line.lineUpper.top = 21;
          styles.or.line.lineUpper.left = -8;
          styles.or.line.lineUpper.width = 8;

          styles.or.line.lineMiddle.top = 21;
          styles.or.line.lineMiddle.left = -8;
          styles.or.line.lineMiddle.height += 179;

          styles.or.line.lineLower.top += 200;
          styles.or.line.lineLower.left = -8;
          styles.or.line.lineLower.width = 8;
          break;
      }
      break;
    default:
      styles.or.line.lineUpper.top = 21;
      styles.or.line.lineUpper.left = -8;
      styles.or.line.lineUpper.width = 8;

      styles.or.line.lineMiddle.top = 21;
      styles.or.line.lineMiddle.left = -8;
      styles.or.line.lineMiddle.height += 179;

      styles.or.line.lineLower.top += 200;
      styles.or.line.lineLower.left = -8;
      styles.or.line.lineLower.width = 8;

      break;
  }

  if(no_of_childs > 0){
    styles.or.line.lineLower.top = styles.or.line.lineLower.top - no_of_childs * 21;
  }

  styles.or.icon.left = -19;
  if (node.rules.length > 0) {
    for (var i = 0; i < node.rules.length; i++) {
      // log("node.rules["+i+"]", node.rules[i]);
      calculateNodeStyles(node.rules[i], styles);
    }
  }

  return styles;
}

function drawOrLines(){
  var _nodes = $(".collectionlist").first().children();

  levelNodes.push(_nodes);

  drawLine(_nodes, level);

  $.each(levelNodes, function(index){
    if(levelNodes[index].length > 1){


      $.each(levelNodes[index], function(index2){
        // console.log($(this).offset().top);

        $('.conditionbuilderPanel').append(
          $('<div />').addClass('bar').css({
                width: 10
            ,   height: $(this).offset().top
            // ,   left: $(this).offset().left
          })
        );


      });

    }
  });
}

function haveChild(li){
	if($(li).find('.collectionlist').length > 0){
  	return true;
  } else {
  	return false;
  }
}

function drawLine(nodes, lev) {
	$.each(nodes, function(index){

    if(haveChild(this) === true){
			var _nodes = $(this).find(".collectionlist").first().children();
      levelNodes.push(_nodes);
      lev++;
  		drawLine(_nodes, lev);
    } else {
    }
  });
}


/*
 * lookup fields
 * @param object
 * @return void
 */
function lookupFields(obj) {
  // switch (obj.key) {
  //   case 'City':
    $('.City').typeahead({
        ajax: '/api/cities',
        scrollBar:true,
    });
    //   break;
    // case 'Country':
    $('.Country').typeahead({
        ajax: '/api/countries',
        scrollBar:true,
        displayField: 'name',
        valueField: 'code',
        onSelect: function(v){
          var f = false;
          for (var i = 0; i < tempCountry.length; i++) {
            if(v.id === tempCountry[i].id){
              f = true;
            }
          }
          if(!f){
            tempCountry.push(v);
          }
        },
    });


    setTimeout(function(){ $(".integer").numeric(); }, 1000);



  //       break;
  //   default:
  //   break;
  //
  // }
}
/*
 * get key name from array  by its value
 * @param array
 * @param value
 * @return string
 */
function getKeyByValue(obj, value) {
  var matchedKey = '';
  var keys = $.map(obj, function(item, key) {
    if (item === value) {
      matchedKey = key;
    }
  });
  return matchedKey;
}
$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
       guidelines.saveInSession();
    }
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(document).ready(function(){
  var inheritCondition = getParameterByName('inherit_condition');
  if(inheritCondition == 'true'){
    document.body.style.pointerEvents = 'none';
  }
});
