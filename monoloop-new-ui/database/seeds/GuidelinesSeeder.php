<?php

use Illuminate\Database\Seeder;
use Jenssegers\Mongodb\Model;

use App\Guideline;

class GuidelinesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

    // run laravel: composer dump-autoload -o to show this class to terminal

		// Model::unguard();

    $file = __DIR__.'/'.'guidelines.csv';
    Excel::load($file, function($reader) {
      // reader methods
      // Getting all results
      $results = $reader->get();

      // ->all() is a wrapper for ->get() and will work the same
      $results = $reader->all();

      foreach ($results as $key => $value) {
      }

    });

	}

}
