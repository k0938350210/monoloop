var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */
/*
elixir(function(mix) {
    mix.less('app.less');
});
*/

elixir.config.sourcemaps = true;

//login

elixir(function(mix) {
  mix.sass('monoloop.scss' , 'public/build/css/monoloop.css');
  //mix.sass('extjs3.scss' , 'public/css/extjs3.css');
});

//admin

var bowerDir = './bower_components/';
elixir(function(mix) {
  mix.copy(bowerDir + 'font-awesome/fonts', 'public/build/admin/fonts');
  mix.copy(bowerDir + 'bootstrap-sass/assets/fonts', 'public/build/admin/fonts');
  mix.sass('admin.scss' , 'public/admin/css/admin.css');
  mix.scripts([
    'jquery/dist/jquery.min.js',
    'bootstrap-sass/assets/javascripts/bootstrap.min.js',
    'PACE/pace.min.js',
    'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'
  ], 'public/admin/js/core.js', bowerDir);
  mix.styles([
    'font-awesome/css/font-awesome.min.css',
    '../public/admin/css/admin.css',
    'bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css'
  ],'public/admin/css/core.css',bowerDir);
});

//main UI

elixir(function(mix) {
  mix.scripts([
    'js/jquery.js',
    'webix/webix.js',
    'webix/filemanager/filemanager.js',
    'js/jstz.min.js',
    'js/clipboard.min.js',
    'js/moment/moment.min.js',
    'js/moment/moment-timezone-with-data-2010-2020.js',
    'webix/d3/d3.v4.js',
    'webix/d3/d3-funnel.min.js',
    'js/jquery.counterup.min.js',
    'js/waypoints.min.js',
    'webui_popover/jquery.webui-popover.js',
    'js/tooltips.js',
    'js/guidelines.js'
  ],'public/build/vendor.js','./public/');


  mix.styles([
    'webix/webix.css',
    'webix/filemanager/filemanager.css',
    'css/font-awesome.min.css',
    'css/webix.css',
    '/webui_popover/jquery.webui-popover.css',
  ],'public/build/app.css','./public/')
});

//Condition Builder
elixir(function(mix) {
  mix.scripts([
    'conditionbuilder/js/bootstrap.min.js',
    'conditionbuilder/js/jqueryui.js',
    'conditionbuilder/js/angular1218.js',
    'conditionbuilder/js/angular-dragndrop.js',
    'conditionbuilder/js/esprima.js',
    'conditionbuilder/js/jsep.min.js',
    'js/underscore.js',
    'conditionbuilder/js/jquery.numeric.min.js',
    'conditionbuilder/js/bootstrap-typeahead.js',
    'conditionbuilder/js/init.js'
  ],'public/build/conditionbuilder.js','./public/');


  mix.styles([
    'webix/webix.css',
    'webix/filemanager/filemanager.css',
    'css/font-awesome.min.css',
    'css/webix.css',
    '/webui_popover/jquery.webui-popover.css',
  ],'public/build/conditionbuilder.css','./public/')
});

elixir(function(mix) {
  mix.version([
    'public/build/vendor.js',
    'public/build/app.css',
    'public/build/css/monoloop.css',
    'public/build/conditionbuilder.js',
    'public/admin/js/core.js',
    'public/admin/css/core.css',
    'public/css/webix.css',
    'public/css/placementwindow/selectView.css',
    'public/webix/webix.css',
    'public/webix/filemanager/filemanager.css',
    'public/webui_popover/jquery.webui-popover.css',
    'public/js/webix/page/invitation/user.js',
    'public/webix/webix.js',
    'public/js/webix/layout/webix.js',
    'public/js/validaion.js',
    'public/webui_popover/jquery.webui-popover.js',
    'public/js/guidelines.js',
    'public/js/webix/component/placement_window.js',
    'public/conditionbuilder/css/bootstrap.min.css',
    'public/conditionbuilder/css/styles.css',
    'conditionbuilder/js/jquery.js',
    'public/js/invocation/editor-bundle.js',
    'public/js/placementwindow/selectView.js',
    'public/js/ejs/ejs_production.js',
    'public/js/placementwindow/testbenchView.js',
    'public/js/placementwindow/tracker_config.js',
    'public/css/placementwindow/tracker.css',
    'public/js/placementwindow/previewView.js',
    'public/js/placementwindow/category_preview.js',
    'public/js/placementwindow/category_xpath.js',
    'public/js/placementwindow/basket.js',
    'public/js/placementwindow/tracker_search.js',
    'public/admin/js/admin.js',
  ]);
});
