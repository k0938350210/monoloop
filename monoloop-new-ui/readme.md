# README #
## Monoloop UI Requirements ##

* PHP 5.6.x ( php 7 not support )

* Mbstring PHP Extension
apt-get install php-mbstring

* Curl PHP Extension
apt-get install php-curl

* GD PHP Extension
apt-get install php-gd

* Mongo Extension ( old mongo extension )
	http://php.net/manual/en/mongo.installation.php


### System dependencies ###
* [Composer](https://getcomposer.org/)

* Initial setup
	git clone -b staging_ui https://monoloop.git.beanstalkapp.com/monoloop-new-ui.git ui
	cd ui
	chmod -R 777 storage
	composer install

* Environment Configuration
	rename .env.example to .env

* Deployment 
 add [deploy: production-ui] to commit file