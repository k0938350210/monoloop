# Monoloop

To run Monoloop project via docker-compose:

docker-compose up


If you need to rebuild services use:

docker-compose build


DNS names by which containers reachable each other in docker-compose:

1.invoke.monoloop.com		frontend
2.app.monoloop.com		UI	
3.uiapiv2.monoloop.com	UI_API	
4.authv2.monoloop.com		Auth	

UI_API and Auth services are on 1 container(uiapiv2.monoloop.com)


1 MongoDB container that is reachable by dns name "mongo" from another containers
1 Redis instance reachable by dns name "redis" from another containers

3 redis-sentinel process are running on frontend container on ports: 26379, 26380, 26381 and monitor "redis" container

"frontend" container also has local redis-server running
"frontend" applications use config file by path monoloop-frontend/config/testing.json


After launching docker-compose env login on container by commands:


docker-compose ps

docker exec -it *container_name* /bin/bash 


To test response from each contanier use curl after container login:

For example  curl -v -L -I app.monoloop.com

